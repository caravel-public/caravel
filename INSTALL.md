INSTALLATION OF CARAVEL
========================

This file describes the installation of Caravel in some detail.

The discussion will be split into 4 main parts:

0. [Conventions](#0-conventions)
1. [Description of the mandatory dependencies required for building Caravel](#1-mandatory-dependencies)
2. [Installation of Caravel](#2-install-caravel)
3. [Installation of optional dependencies for Caravel](#3-optional-dependencies)

It has been tested on `linux` systems such as 

- `Ubuntu 22.04` (g++ v.9.4.0, g++ v.10.3.0, g++ v.11.2.0, g++ v.12.0.1, clang++ v.12.0.1, clang++ v.13.0.1, & clang++ v.14.0.0),
- `Ubuntu 20.04` (g++ v.7.5.0, g++ v.8.4.0, g++ v.9.3.0, clang++ v.8.0.1-9, clang++ v.9.0.1-12, & clang++ v.10.0.0-4ubuntu1), 
- `Debian 10` (g++ v.8.3.0 & clang++ v.7.0.1) 
- `OpenSuse Leap-15.1-GNOME-Live-x86_64` (g++ v.7.50)
- `CentOS (RedHat) 8.1.1911-x86_64` (g++ v.8.3.1, & clang++ v.9.0.1)
- `Fedora (RedHat) Workstation-Live-x86_64-31-1.9` (g++ v.9.2.1, & clang++ v.9.0.1), 
- `Manjaro (Arch) gnome-19.0.1` (g++ v.9.2.1, g++ v.10.2.0, & clang++ v.10.0.1) 

as well as on `macOS`

- `macOS Catalina` (g++ v.9.2.0). 

`C++17`-compliant compilers are required for the installation. 

***

## 0. Conventions

For the sake of simplicity, we assume that all dependencies will either be
available system wide or installed in a common directory. For further reference,
the location of this install directory will be stored in an environment variable
`INSTALLPATH`.  Before starting any of the instructions listed below, you should
create this environment variable (we assume `Bash` shell)

```bash
$ export INSTALLPATH=<path>
```

where you can insert your personal choice for \<path\>. 

If typing 

```bash
$ echo $INSTALLPATH
```

prints your choice for the installation path, you are good to go.

While compiling `Caravel` and during runtime, the dependencies of `Caravel` need
to be found. Most of the time we rely on `pkg-config` to determine the location
of these dependencies.

This means that each time, a dependency is installed, it will generate a file
called `dependency_name.pc` which will be stored in
`$INSTALLPATH/lib/pkgconfig`. 

When `Caravel` is being compiled and the build system sees the dependency
`dependency_name` is needed it will look into this file to learn about where the
headers and shared libraries are stored. In order to be able to do this, it
needs to be able to find these files, which is why we store their location in an
environment variable as such

```bash
$ export PKG_CONFIG_PATH=$INSTALLPATH/lib/pkgconfig:$PKG_CONFIG_PATH
```

Notice that this will only hold for the current terminal session. If you want to
store it permanently, just add it to the bottom of your *.bashrc* file which can
be opened as follows:

```bash
$ vim $HOME/.bashrc
```

Replace `vim` by your favorite text editor.

This is the reason why we want to install all dependencies in a common place. If
you decide to install the dependencies in different places, all of them should
be included in `$PKG_CONFIG_PATH`.

At runtime, `Caravel` needs to link against the shared libraries of the
dependencies. In order for this to work, it again needs to know where they can
be found. For this reason, we add the location to the environment variable
`LD_LIBRARY_PATH` as such 

```bash
$ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALLPATH/lib
```

Notice again: To make this not only valid for the current terminal session but
to make it permanent, add it to the bottom of the *.bashrc* file as described
above. If you choose to install your dependencies in different places, all
should be included in `$LD_LIBRARY_PATH`. 

We also assume that we download all of our Software into a common directory:

```bash
$ export SOFTWARE=<path>
```

Again the user can choose where he or she wants to store it and replace
\<path\>.

***

## 1. Mandatory dependencies:



Building Caravel has 5 mandatory dependencies:

A. [Python 3](#a-python-3)

B. [The meson (frontend) and ninja (backend) build system](#b-ninja-and-meson)

C. [GMP for exact arithmetic](#c-exact-arithmetics)

D. [MPFR for arbitrary precision floating-point arithmetic](#d-arbitrary-precision-floating-point-arithmetics)


In the following, the installation of each one of the dependencies will be
discussed.


### A. Python 3

We use `meson` as a build system and this is based on Python 3. Python 3 should
be available on all major platforms. While Python 2 is still widely popular in
science, as support has been dropped in January 2020, now is a good time to
transition to Python 3 and benefit from the many improvements that it offers. 

If Python 3 is not available on your system, you can install it via your
favorite package manager or download it from the official webpage:

https://www.python.org/downloads/

An interesting alternative is to install the Anaconda distribution from
Continuum Analytics. It comes with many important scientific packages
pre-installed and the powerful package manager `conda`. It is especially useful
to choose this option when working on a system where no administrator privileges
are available. It also allows to create different co-existing environments based
on different python versions.  If you choose this alternative, an install script
can be downloaded from:

https://www.anaconda.com/distribution/

Once the script is downloaded, make it executable and run it:

```bash
$ chmod +x Anaconda3-<version>-Linux-x86_64.sh
$ ./Anaconda3-<version>-Linux-x86_64.sh
```

(Replace \<version\> by the version number in the name of the file you have
downloaded.)

At the beginning you can even specify in which directory the distribution shall
be installed.

When asked, in the end, whether to initialize Anaconda3 by running conda init,
answer `yes`. This will allow you to bypass the system installation and use the
anaconda version by just typing

```bash
$ python 
```

or 

```bash
$ python3
```

To check whether you are indeed using the anaconda distribution you can check 

```bash
$ which python 
```

or 

```bash
$ which python3
```


### B. Ninja and meson


`Meson` is a modern and easy-to-use build system that supports several
languages.  If you are tired of ugly Makefiles (including the TAB vs SPACE mess)
and the syntax of `autotools` and `cmake` have a look at 

[https://mesonbuild.com/](https://mesonbuild.com/)

It is easy to learn and you'll never want to go back.

You can check whether meson is available on your system by typing 

```bash
$ which meson
```

If there is no output, or if the output of 

```bash
$ meson --version 
```

is smaller than **0.56**, you need to upgrade or to install. An upgrade can be
performed via 

```bash
$ pip3 install meson --upgrade [--user]
```

The `--user` flag is optional and needs to be specified if no administrator
privileges are available.

`Meson` can be installed as a python package, so to install you only need  

```bash
$ pip3 install meson [--user]
```

or 

```bash
$ conda install meson 
```

The flag `--user` needs to be specified for a system python when no
administrator privileges are available. In this case the location of the
installed `meson` script might potentially have to be added to the `PATH`
environment variable, which can be done (still assuming `Bash` shell) by running

```bash
$ export PATH=$PATH:$HOME/.local/bin
```

Again, to make this permanent, add the statement to the bottom of your *.bashrc*
file, as described above.
 

Installing `meson`, should also install the lightweight and fast build system
`ninja` which `meson` uses as backend. If this is not the case or if 

```bash
$ which ninja
```

produces no output, you can get it from:

[https://ninja-build.org/](https://ninja-build.org/)

Notice that when installing `ninja` using pip or conda together with `meson` it
does not have autocompletion for the command-line. To achieve this (on
Debian/Ubuntu) run 

```bash
$ sudo apt-get install ninja-build
```

You might need to ask your system administrator to install it for you if you do
not have root privileges.


### C. Exact arithmetics 

Exact arithmetics in `Caravel` is built with the usage of `GMP` for big integer
types. `GMP` can be installed as follows

```bash
$ cd $SOFTWARE
$ wget https://gmplib.org/download/gmp/gmp-6.3.0.tar.xz
$ tar -xf gmp-6.3..tar.xz
$ cd gmp-6.3.1/
$ mkdir build
$ cd build
$ ../configure --prefix=$INSTALLPATH --enable-cxx
$ make -j 2
$ make -j 2 check
$ make install
```

Running

```bash
$ ls $INSTALLPATH/lib/pkgconfig
```

the file `gmpxx.pc` should be present.

To use exact arithmetics in Caravel, go the Caravel build directory and run 

```bash
$ cd $CARAVEL_BUILD
$ meson configure -Dfinite-fields=true 
$ ninja -j 2
$ ninja install 
```

And optionally 

```bash
$ ninja test 
```

which enables many long tests. 


### D. Arbitrary precision floating-point arithmetics

`Caravel` allows usage of arbitrary-precision floating-point
arithmetic. The installation of
`GMP` can be performed as described in [section C](#c-exact-arithmetics). `MPFR`
can be installed as follows:

```bash
$ cd $SOFTWARE
$ wget https://www.mpfr.org/mpfr-current/mpfr-4.2.1.tar.gz
$ tar -xzf mpfr-4.2.1.tar.gz
$ cd mpfr-4.2.1/
$ mkdir build
$ cd build
$ ../configure --prefix=$INSTALLPATH --with-gmp=$INSTALLPATH
$ make -j 2
$ make -j 2 check
$ make install
```

Notice that we require an `MPFR` version >= 4.0.0.

To instantiate certain types with arbitrary precision floating-point types run

```bash
$ cd $CARAVEL_BUILD
$ meson configure -Dprecision-arbitrary=true
$ ninja
$ ninja install
```


***

## 2. Install Caravel



The first step is to clone the `Caravel` repository. Perform this step in a
directory where you want the Caravel code to be stored:

```bash
$ git clone https://gitlab.com/caravel-public/caravel.git
```

using *http*

or 

```sh
$ git clone git@gitlab.com:caravel-public/caravel.git
```

using *ssh*.

Then we can proceed with a basic install 

```bash
$ cd caravel 
$ mkdir build 
$ cd build
$ export CARAVEL_BUILD=$PWD
$ meson .. -Dprefix=$PWD
$ ninja -j 2
$ ninja install 
```

Notice that in `meson` it is mandatory to create a build directory and it is not
possible to perform an in-source build. This is a good thing and not an
annoyance! This way the code is kept separate from the build. If something
breaks, you just remove the build directory and repeat and don't have to
disentangle build files from source files.

__Notice__ that `ninja -j 2` specifies that only two cores should be used for
compilation. `Caravel` is a big library and compilation can require a lot of
memory. Therefore, on machines with low RAM it is beneficial to restrict the
number of cores as `meson` will by default use all available ones.

During configuration, we specified that we want to install `Caravel` in the
build directory (`-Dprefix=$PWD`).  More configuration options can be specified
according to

```
$ meson .. -Dprefix=$PWD -D<option1>=<value1> -D<option2>=<value2>
```

Configurations can be changed at a later point by running

```
$ meson configure -D<option1>=<value1> -D<option2>=<value2>
```
However, we recommend to start with a fresh `build` directory if configuration
options on an already existing installation are to be changed.

To see the optional configuration flags run 

```bash
$ meson configure
```

Running `ninja` is equivalent to `make` in `autotools`.  

To check installation, you can run `Caravel` tests via 

```bash
$ ninja test
```

or 

```bash
$ meson test 
```

Notice that this might take a while (especially on a first run, when warmup
information is created). During development you might only want to run the
rather quick unit tests and skip the longer ones until you decide to commit.  To
target their separate evaluation, you can run: 

```bash
$ meson test --suite Caravel:unit_test
$ meson test --suite Caravel:big_test
$ meson test --suite Caravel:integrals
$ meson test --suite Caravel:full_amplitudes
```

***

## 3. Optional Dependencies 



`Caravel` can be extended with more functionalities. This requires to link
against other libraries. Next we present the following instructions for
dependencies:

A. [QD for high-precision floating-point](#a-high-precision-floating-point-arithmetics)

B. [CLN and GiNaC fo multi-loop integrals with Goncharov polylogarithms](#b-integrals-in-terms-of-goncharovs)

C. [Pentagon library for multi-loop integrals with pentagon functions](#c-integrals-in-terms-of-pentagon-functions)

__NOTE__: In the repository [caravel-deps-setup](https://gitlab.com/caravel-public/caravel-deps-setup),
we provide a command-line tool written in `Rust` that takes care of installing all dependencies (including the mandatory `GMP` and `MPFR`). 
So far it has been used on `Ubuntu` and `Debian` systems and is known __not__ to work on `macOS`.



### A. High-Precision Floating-Point Arithmetics

When going beyond double precision, `Caravel` relies mainly on `QD` to provide
the high-precision types and on `Eigen` for corresponding linear algebra. 

To install `QD`, follow the procedure 

```bash
$ cd $SOFTWARE
$ wget https://www.davidhbailey.com/dhbsoftware/qd-2.3.24.tar.gz
$ tar -xzf qd-2.3.24.tar.gz
$ cd qd-2.3.24
$ autoreconf -i
$ ./configure --prefix=$INSTALLPATH --enable-shared=yes
$ make -j 2 
$ make install
```

(__NOTE__: The autoreconf -i step is important because the configure script
distributed with the tarball is not up to date and needs to be regenerated.) If
you have more cores available increase the 2 to your needs to speed up
compilation.

Optionally the tests can be run to check whether both the build and the
installation work correctly 

```bash
$ make -j 2 check 
```

When running 

```bash
$ ls $INSTALLPATH/lib/pkgconfig
```

you should see the file `qd.pc`.

`Eigen` can be installed as follows 

```bash
$ cd $SOFTWARE 
$ git clone https://gitlab.com/libeigen/eigen.git eigen
$ cd eigen
$ mkdir build 
$ cd build 
$ cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALLPATH -DPKGCONFIG_INSTALL_DIR=$INSTALLPATH/lib/pkgconfig
$ make install
```

Running

```bash
$ ls $INSTALLPATH/lib/pkgconfig
```

you should see that `eigen3.pc` among the other.

To use this in `Caravel` we do: 

```bash
$ cd $CARAVEL_BUILD 
$ meson configure -Dprecision-QD=HP
$ # or
$ # meson configure -Dprecision-QD=VHP
$ # or
$ # meson configure -Dprecision-QD=all
$ ninja -j 2
$ ninja install 
```


### B. Integrals in terms of Goncharovs 

`Caravel` comes with a library of one and two-loop integrals. To be able to use
those that are expressed in terms of Goncharov polylogarithms, both `cln` and
`ginac` need to be installed. This can be done as follows 

```bash
$ cd $SOFTWARE 
$ wget https://www.ginac.de/CLN/cln-1.3.7.tar.bz2
$ tar xvjf cln-1.3.7.tar.bz2
$ cd cln-1.3.7
$ ./configure --prefix=$INSTALLPATH
$ make -j 2 
$ make install 
```

for `cln`. To run the tests do 

```bash
$ make -j 2 check
```

Upon running 

```bash
$ ls $INSTALLPATH/lib/pkgconfig 
```

the `cln.pc` should appear. 

To install `GiNaC` we run:

```bash
$ cd $SOFTWARE 
$ wget https://ginac.de/ginac-1.8.7.tar.bz2
$ tar xvjf ginac-1.8.7.tar.bz2
$ cd ginac-1.8.7
$ ./configure --prefix=$INSTALLPATH
$ make -j 2 
$ make install 
```

Optionally, the tests can again be run using 

```bash
$ make -j 2 check 
```

Upon running 

```bash
$ ls $INSTALLPATH/lib/pkgconfig 
```

the file `ginac.pc` should appear.

To link to the integrals in `Caravel` we do 

```bash
$ cd $CARAVEL_BUILD
$ meson configure -Dintegrals=goncharovs 
$ ninja install 
```

### C. Integrals in terms of Pentagon Functions 

Another option is to link `Caravel` to the integrals expressed in terms of
[pentagon functions](https://arxiv.org/abs/1807.09812). To do this both the
installation of `CLN` and `GiNaC` as described in the previous step is necessary
as well as that of a modified version of the `PentagonLibrary`.

The `PentagonLibrary` depends on `GSL` which should be available on Linux
distributions. On macOS, `GSL` can be installed by running


```sh
$ brew install gsl
```

First let us clone the pentagon function library: 

```bash
$ cd $SOFTWARE
$ git clone git@gitlab.com:caravel-public/pentagon-library.git
$ cd pentagon-library/
$ mkdir build
$ cd build 
$ meson .. -Dprefix=$INSTALLPATH -Dlibdir=lib
$ ninja 
$ ninja install
```

Upon running 

```bash
$ ls $INSTALLPATH/lib/pkgconfig 
```

the file `pentagon.pc` should appear. 

To be able to use the integrals inside `Caravel` we run 

```bash
$ cd $CARAVEL_BUILD 
$ meson configure -Dintegrals=pentagons 
$ # or: meson configure -Dintegrals=all
$ ninja 
$ ninja install 
```






