# Contributing to Caravel

In this file we describe a few useful points when it comes to contributing to
the development of `Caravel`. These are guiding principles that should allow
long-term maintenance of the code, and which can of course be changed and
adapted when required to specific cases. Developers are encouraged to follow
them as much as possible!

## Git Repositories

All development of `Caravel` code is done through corresponding git
repositories. Developers should get acquainted with git repositories,
there are plenty of tutorials online (for example from [Atlassian](https://www.atlassian.com/git/tutorials)).

Branches in the `Caravel` repo should follow a strategy like the one highlighted
in this [figure](https://nvie.com/img/git-model@2x.png) (see the discussion in
[this article](https://nvie.com/posts/a-successful-git-branching-model/)). For
historical reasons the main developing branch is `master` and the named released
versions are contained in the branch `release`.

## Commit Messages

* Write meaningful and concise commit messages consisting of a short first summary
line targeted at `git log --oneline` followed by a more extensive description.

## Naming Conventions for Classes, Methods and Functions

Names associated to types (like classes, structures, enumerations) are written
using [**CamelCase**](https://simple.wikipedia.org/wiki/CamelCase) practice.
Methods, functions, variable names on the other hand are written in 
[**snake_case**](https://en.wikipedia.org/wiki/Snake_case) style.

## Indentation 

In order to have a uniform appearance of the code shipped in `Caravel`, we 
provide a [*.clang-format*](https://clang.llvm.org/docs/ClangFormat.html) file in the root directory of the repository. 
Contributors are encouraged to the set up their editors to perform the 
indentation in an automated way. 

## Few Concepts to Consider

While building software for `Caravel` try to identify cases for which you can
write:

1. *Abstract* structures in the code which are ``smart'', general and probably
time consuming for construction time.
2. *Numerical* routines which are key to optimising for run time efficiency.
3. *Data* which can be stored either for structure (results from
*Abstract* algorithms, e.g. a cut hierachy, a forest etc) or from
kinematical analysis (results from *Numerical* code)

## (Mostly) Header-only Library

As a way of reusability of source code in `Caravel`, we structure it as a
header-only library, with instantiation definitions and declarations that should
make compiling more efficient.

Header files are expected to have the structure

```cpp
//----------------------
// MyHeader.h:
//----------------------

#pragma once

template <class T> class MyTemplate {
  /* declaration of MyTemplate */
};

/* ... */

#include "MyTemplate.hpp"
```

Notice that we move away from making up header-guard-names and instead rely on
the non-standard but widely used ```#pragma once``` statements. As signalled,
header files for class templates are expected to only have declarations and
(complicated) implementations shall be included in a file with extension `.hpp`.

An `.hpp` implementation will contain declarations of instantiations, like for
example:

```cpp
//----------------------
// MyTemplate.hpp:
//----------------------

/* implementations of MyTemplate class template */

// should match what's done in MyTemplate.cpp
extern template class MyTemplate<MyType>;
```

and then the corresponding object `.cpp` files will have the instantiation
definitions:

```cpp
//----------------------
// MyTemplate.cpp:
//----------------------

// INSTANTIATIONS for myTemplate class template
template class MyTemplate<MyType>;
```

Always think whether your class templates are ready to be instantiated *on the
fly* with a new type used in a `my_programs` (see below) program.

## Documentation 

Functions, classes and methods should be documented using the doxygen comments
from which html documentation can be generated. Here a small example 

```cpp

/**
 * A simple `Series` class. 
 *
 * `T` is the template for the types of series coefficients.
 */
template <typename T>
class Series {
    private:
        short int m_start_order {};     /**< The starting order of the series */
        short int m_end_order {};       /**< The order at which the series ends */
        std::vector<T> coefficients {}; /**< Contains the coefficients in the series */\
    
    public:
        /**
         * Construct a series.
         *
         * @param start         The starting power of the series.
         * @param end           The highest power in the series.
         * @param coefficients  The coefficients of the series.
         */
        Series<T>(int start, int end, std::vector<T>&& coefficients);
        
        /**
         * Access the coefficient of the term associated to power `order`.
         *
         * @param order  The order the variable whose coefficient we are interested in.
         * @return       The series-coefficient multiplying the variable to power `order`.
         */
        T const& operator[](int order) const;
};
```

## The my\_programs directory

The `my_programs` directory can be used to quickly compile and link testing
programs to `Caravel`.  Developers should restrain from committing such programs
to the repository as they are outdated quickly. 

The source file `prog.cpp` for such a program can be put in the subdirectory
`my_programs` of the source directory and can include any `Caravel` headers. In
order to make the build system aware of the file, it suffices to run in the
`build/` directory

```
$ ninja reconfigure
```

Once this is done, compilation and running the program can be done by

```
$ ninja my_programs/prog
$ ./my_programs/prog
```

from the build directory. For convenience, both the compilation and the
subsequent execution can be triggered by the command (tab completion in the
shell is enabled)

```
$ ninja prog.r
```

from the build directory. The name of the programs that are to be executed with
MPI should either be prefixed by `MPI_`” or `mpi_` or be postfixed by `_MPI` or
`_mpi` (e.g. `prog_MPI.cpp`).

If a `my_programs` file becomes so polished that it could be committed, add nice
documentation strings and add it to the `examples` directory. Remember to wrap
the build instruction in the `meson.build` file in the correct configuration
flags.

## Testing 

Unit tests should be run frequently

```bash
$ meson test --suite Caravel:unit_test
```

The big tests should always be run before pushing to master, e.g.

```bash
$ meson test --suite Caravel:big_test
$ meson test --suite Caravel:integrals
$ meson test --suite Caravel:full_amplitudes
```

Running the full set of tests can be achieved either by

```bash
$ meson test
```

or 

```bash
$ ninja test
```

It is recommended to check that the tests pass for three sets of configuration
flags, namely

* basic (do not enable any additional features)
* intermediate: `-Dintegrals=all -Dfinite-fields=true`
* extended: `-Dintegral=all -Dfinite-fields=true -Dfield-ext-fermions=true -Ddouble-inverter=eigen`

## Additional debug output

`Caravel` allows to trigger output of detailed and extensive debug messages.
This is overkill for general usage since it can result in a tremendous amount of output
that would just clutter the terminal and hide the interesting output.
However, it can be useful to understand the detailed path a program is taking. To enable this feature,
`Caravel` has to be configured with

```sh
> meson configure -Dcaravel-debug=true
```

Setting this configuration flag alone will not yet result in debug messages being printed during program execution.
For this to happen, the user needs to specify the names of the files whose debug messages shall be shown.
This allows to fine-tune the debug output by constraining it to only come from files whose names
(__without__ the full path) appear in a file named `debug.dat` that is located in the directory
where the binary is run. One file name should be listed per line and lines starting with `#` are considered to be
comments and ignored.

As a simple example, consider a basic `Caravel` configuration that only has the `debug` flag enabled:

```sh
> mkdir build && cd build
> meson .. -Dprefix=$PWD -Dcaravel-debug=true
> ninja install
> cd examples
```

In the next step we create the `debug.dat` file with the following content in the `build/examples` directory:

```txt
# We only want the debug output that is being triggered from Builder.hpp and Forest.cpp
Builder.hpp
Forest.cpp
```

Once this is done, we can run the example `treeamp`

```sh
> ./treeamp "Particles[Particle[u,1,qm],Particle[ub,2,qbp],Particle[gluon,3,m],Particle[gluon,4,p]]"
```

To see the difference without the debug output, change the content of the `debug.dat` file to 

```txt
# We only want the debug output that is being triggered from Builder.hpp and Forest.cpp
# Builder.hpp
# Forest.cpp
```

and run again.

## Asking questions

Questions can be asked on the public gitter chatroom:

[![Gitter](https://badges.gitter.im/caravel-amps/community.svg)](https://gitter.im/caravel-amps/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
