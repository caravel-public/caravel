/**
 * This example shows the evaluation of two-loop 4 and 5-parton amplitudes.
 *
 * The rational expressions for the master integral coefficients are
 * reconstructed from Finite Field evaluations and afterwards combined with
 * the master integrals.
 *
 * The resulting amplitudes are shown up to O[ep^0] and are normalized by the
 * tree amplitude if it is non-vanishing.
 *
 * Run the program with: ./amplitude_evaluator_2l [PartialAmplitudeInput] [twistor_variables]
 * See the README for further explananation of the input
 *
 */
#include "amplitude_evaluator_utils.h"

#include "Core/CaravelInput.h"
#include "Core/settings.h"
#include "Core/RationalReconstruction.h"
#include "Core/Timing.h"
#include "Core/precision_cast.h"

#include "Forest/Model.h"

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"

#include "IntegralLibrary/IntegralProvider.h"



int main(int argc, char* argv[]) {
    using namespace Caravel::Integrals;
    using namespace Caravel::IntegrandHierarchy;
    using F = F32;

    if (argc < 2) {
        _WARNING("ERROR: expecting some input! Execute for example: ./amplitude_evaluator_2l PartialAmplitudeInput[Particles[Particle[gluon,1,m],Particle[gluon,2,m],Particle[gluon,3,p],Particle[gluon,4,p]]]");
        std::exit(1);
    }

    // enable check of fitted numerator ansaetze (N=N check)
    settings::use_setting("general::do_n_eq_n_test yes");
    // use all available cores for computing integrand coefficients
    settings::use_setting("IntegrandHierarchy::parallelize_topology_fitting always");

    // parse the command line arguments
    InputParser input{argc,argv};

    // read the input specifying a partial amplitude
    auto  pa = input.get_cmd_option<PartialAmplitudeInput>("PartialAmplitudeInput");
    const size_t nparticles = pa.get_multiplicity();

    // Let the user set verbosity level to All (includes info of coefficient computations) or Remainder (shows functions with cancelled poles)
    if(input.cmd_option_exists("Verbosity")){
        if( input.get_cmd_option("Verbosity")[0] == "All" )
            verbosity_settings.show_all();
    }

    set_settings_for_standard_choice_of_integrals(nparticles);

    // Override settings from file (for developers) if found
    settings::read_settings_from_file();

    // The kinematic point is given by the twistor parametrization.
    // We read the (rational) parameters from the command line.
    // If nothing is given, we just choose some default point.
    std::vector<BigRat> xs;

    if(input.cmd_option_exists("TwistorParameters")){
        xs = Caravel::detail::read_twistor_parameters(input.get_cmd_option("TwistorParameters"));
    }
    else{
        switch (nparticles) {
            case 4: xs = {BigRat(-3, 4), BigRat(-1, 4)}; break;
            case 5: xs = {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17), BigRat(-1)}; break;
            case 6: xs = {BigRat(-1), BigRat(4, 3), BigRat(1, 5), BigRat(8, 7), BigRat(7, 17), 5, -3, 2}; break;
            default: _WARNING_RED(nparticles); std::exit(1);
        }
    }
    auto physical_point = rational_mom_conf<BigRat>(nparticles, xs);
    std::vector<size_t> mom_indices = pa.get_momentum_indices();
    is_euclidean<BigRat, 4>(physical_point, mom_indices);

    // Use the Model SM_color_ordered, and construct the amplitude object.
    std::shared_ptr<Model> model2use{model_from_name(input.get_cmd_option<std::string>("Model","SM_color_ordered"))};

    START_TIMER(construction);
    // Possibly specify a slice, which restricts the hierarchy (for expert users)
    // If the slice is not given, do nothing.
    auto ampl = (input.cmd_option_exists("SliceSelector"))
                    ? ColourExpandedAmplitude<F, 2>{*model2use, pa, input.get_cmd_option<slice_selector>("SliceSelector")}
                    : ColourExpandedAmplitude<F, 2>{*model2use, pa};
    STOP_TIMER(construction);

    // Some additional settings can be passed from command line, like permutations of momenta or the value for D_s (only for developers).
    if(input.cmd_option_exists("CaravelSettingsInput")){
        auto csi = input.get_cmd_option<CaravelSettingsInput>("CaravelSettingsInput");
        auto new_ps = physical_point;
        for(auto&& [i, obj] : enumerate(csi.perm)){
            new_ps.set_p(physical_point.p(obj),i+1);
        }
        physical_point = new_ps;

        // set Ds value if provided
        if (!csi.Ds.is_zero()) { ampl.set_Ds_value(csi.Ds); }
    }

    if(!input.get_unused_tokens().empty()){
        _WARNING_RED("The following input arguments were ignored:"); 
        for(auto&& it : input.get_unused_tokens()){
            _MESSAGE("\t",it);
        }
    }

    // Construct a function that returns the evaluation of the amplitude in a given point in a given finite field
    std::function<std::vector<DenseRational<F>>(const momentumD_configuration<F, 4>&)> f_generic = [&ampl](const momentumD_configuration<F, 4>& mom_conf) {
        return ampl.get_master_coefficients(mom_conf).front();
    };

    // check if warmup run is required (we show this step explicitly for completeness)
    if( !ampl.has_warmup() ) {
        _MESSAGE(Color::Code::FG_BLUE,"Performing warmup run:");
        f_generic(static_cast<momentumD_configuration<F,4>>(physical_point));
        _MESSAGE(Color::Code::FG_BLUE,"End of warmup run");
    }

    // Run rational reconstruction of master-integral coefficients from finite-field evaluations
    START_TIMER(coefficients);
    _MESSAGE(Color::Code::FG_BLUE,"Performing rational reconstruction of master-integral coefficients:");
    auto result_coeffs = RationalReconstruction::rational_reconstruct(physical_point, f_generic);
    _MESSAGE(Color::Code::FG_BLUE,"End of rational reconstruction of master-integral coefficients");
    STOP_TIMER(coefficients);


    // Get information on associated integrals to master coefficients
    auto info_integrals = ampl.get_integral_graphs();

    if (pa.get_nc_order() >= 1) {

        // Print the amplitude as \sum coeff * integral
        std::ostringstream theAmplitude;
        theAmplitude << "(";
        for(size_t ii = 0; ii < result_coeffs.size(); ii++ ){
            theAmplitude << result_coeffs[ii] << " * ";
            theAmplitude << info_integrals[ii];
            if (ii != result_coeffs.size() - 1) {
                theAmplitude << " + " << std::endl;
            }
        }
        theAmplitude << ")";
        _MESSAGE(Color::Code::FG_BLUE,"Result:");
        std::cout << theAmplitude.str() << std::endl;

    } else {

        // Construct an integral provider
        sIntegralProvider<C> integrals( info_integrals );
        
        // Cast the external momenta complex double
        auto momenta = to_precision<C>(physical_point);

        Series<C> amp;

        START_TIMER(combine_with_integrals);
        _MESSAGE(Color::Code::FG_BLUE,"Computing master integrals");
        // choose between keeping special functions unevaluated until all integrals are summed with the coefficients,
        // or evaluating numerically each master integral individually
        if (true) {
            // get a semi-analytic representation of integrals, with special functions kept unevaluated
            auto result_integrals = integrals.get_abstract_integral(momenta);

            // evaluate special functions and combine with the corresponding coefficients
            amp = combine_with_integrals<C>(momenta, result_integrals, result_coeffs);
        }
        else {
            auto result_integrals = integrals.compute(momenta);
            Series<C> result(-4, 0);
            for (auto [i, ci] : enumerate(result_coeffs)) { result += to_precision<C>(ci.taylor_expand_around_zero(4)) * result_integrals[i]; }
            result *= two_loop_norm_caravel_to_physical<C>;
            amp = std::move(result);
        }
        _MESSAGE(Color::Code::FG_BLUE,"Done with master integrals");
        STOP_TIMER(combine_with_integrals);

        _MESSAGE(Color::Code::FG_BLUE,"Result:");
        _MESSAGE("-----------------------------------------------------------------------------------------------");
        std::cout << std::setprecision(12);
        //_MESSAGE("\nampl[\"",pa.get_id_string(),"\"] = ", math_string(amp),"\n");
        _MESSAGE(amp);
        _MESSAGE("-----------------------------------------------------------------------------------------------");
    
    }

    return 0;
}
