/**
 * This example shows the evaluation of one-loop 4 and 5-parton amplitudes.
 *
 * The rational expressions for the master integral coefficients are
 * reconstructed from Finite Field evaluations and afterwards combined with
 * the master integrals.
 *
 * The resulting amplitudes are shown up to O[ep^2] and are normalized by the
 * tree amplitude if it is non-vanishing.
 *
 * Run the program with: ./amplitude_evaluator_1l [PartialAmplitudeInput] [twistor_variables]
 * See the README for further explananation of the input
 *
 * Note: 5 parton integrals are considerably slower than 4-parton integrals!
 */
#include "amplitude_evaluator_utils.h"

#include "AmpEng/CoefficientEngine.h"
#include "Amplitude.h"
#include "Core/CaravelInput.h"
#include "Core/Particle.h"
#include "Core/Utilities.h"
#include "Core/settings.h"
#include "IntegralLibrary/IntegralProvider.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

using namespace Caravel;
using namespace AmpEng;

int main(int argc, char* argv[]) {
    using T = C;   // Complex type
    using F = F32; // Finite-field type

    if (argc < 2) {
        _WARNING("ERROR: expecting some input! Execute for example: ./amplitude_evaluator_1l PartialAmplitudeInput[Particles[Particle[gluon,1,m],Particle[gluon,2,m],Particle[gluon,3,p],Particle[gluon,4,p]]]");
        std::exit(1);
    }

    // this is very important: for 1-loop amplitudes we do not use process library and compute fermion traces explicitly,
    // so we need to set the Tr[I] = 2^(Ds/2) correctly in D_s-dependent way
    settings::use_setting("BG::partial_trace_normalization full_Ds");

    // parse the command line arguments
    InputParser input{argc, argv};

    // read the input specifying a partial amplitude
    PartialAmplitudeInput amp = input.get_cmd_option<PartialAmplitudeInput>("PartialAmplitudeInput");
    const size_t nparticles = amp.get_multiplicity();

    // The kinematic point is given by the twistor parametrization.
    // We read the (rational) parameters from the command line, otherwise use default point.
    _MESSAGE(Color::Code::FG_BLUE, "Generating a ", nparticles, "-particle phase space point");
    std::vector<BigRat> xs;

    if (input.cmd_option_exists("TwistorParameters")) { 
        xs = Caravel::detail::read_twistor_parameters(input.get_cmd_option("TwistorParameters")); 
    }
    else {
        switch (nparticles) {
            case 4: xs = {BigRat(-3, 4), BigRat(-1, 4)}; break;
            case 5: xs = {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17), BigRat(-1)}; break;
            default: _WARNING_RED("No default phase space point for N = ", nparticles, " particles!"); std::exit(1);
        }
    }

    // Generate a complex phase space point from twistor variables and check if the point is in the euclidean region
    auto physical_point = get_mom_conf<T>(nparticles, xs);
    for (size_t n = 0; n < physical_point.size(); n++) _MESSAGE(n + 1, ": ", physical_point[n + 1]);
    std::vector<size_t> mom_indices = amp.get_momentum_indices();
    is_euclidean<T, 4>(physical_point, mom_indices);

    if (!input.get_unused_tokens().empty()) {
        _WARNING_RED("The following input arguments were ignored:");
        for (auto&& it : input.get_unused_tokens()) { _MESSAGE("\t", it); }
    }

    // Calculations are performed in the color ordered Standard Model
    std::shared_ptr<Model> model2use{model_from_name("SM_color_ordered")};

    // Here we provide a function that allows the evaluation of the master integral coefficients in a Finite Field
    // The std::function serves as input for the rational reconstruction (see rat_result) to evaluate the integral coefficients
    // on multiple finite-field phase space points in order to reconstruct their rational values
    CoefficientEngine<F> coefficient_provider(model2use, amp);
    std::function<std::vector<DenseRational<F>>(const momentumD_configuration<F, 4>&)> coeff_function = [&coefficient_provider](const auto& ps) {
        return coefficient_provider.compute(ps);
    };

    // check if warmup run is required
    if( !coefficient_provider.has_warmup() ) {
        _MESSAGE(Color::Code::FG_BLUE,"Performing warmup run:");
        auto physical_point_ff = rational_mom_conf<BigRat>(nparticles, xs);
        coefficient_provider.warmup_run(static_cast<momentumD_configuration<F,4>>(physical_point_ff));
        coefficient_provider.reload_warmup();
        _MESSAGE(Color::Code::FG_BLUE,"End of warmup run");
    }

    // Create a rational phase space used in the finite-field evaluations
    // Note, that rational_point is equivalent to the complex physical_point after taking
    // into account the change of the metric (+,-,-,-) -> (+,-,+,-)
    auto rational_point = ps_parameterizations::rational_mom_conf<BigRat>(nparticles, xs, std::vector<size_t>(), true);

    // The rational numbers of the master integral coefficents are reconstructed using Finite Field evaluations
    _MESSAGE(Color::Code::FG_BLUE, "Reconstructing the master integral coefficients");
    START_TIMER(coefficients);
    auto rat_result = RationalReconstruction::rational_reconstruct(rational_point, coeff_function);
    STOP_TIMER(coefficients);

    // Evaluation of the master integrals using the complex physical_point
    _MESSAGE(Color::Code::FG_BLUE, "Evaluating master integrals");
    set_settings_for_standard_choice_of_integrals(nparticles);
    Integrals::sIntegralProvider<T> integral_provider(coefficient_provider.get_integral_graphs());
    START_TIMER(integrals);
    auto integrals = integral_provider.compute(physical_point);

    // sum up coefficient*integral
    _MESSAGE(Color::Code::FG_BLUE, "Combining coefficients and master integrals");
    Series<T> result(-2, 2);
    for (auto&& [i, ci] : enumerate(rat_result)) { result += to_precision<T>(ci.taylor_expand_around_zero(4)) * integrals[i]; }

    STOP_TIMER(integrals)

    // add normalization factors to match the paper values
    result *= one_loop_norm_caravel_to_physical<T>;
    // For closed fermion loops the factor of (-1) has to be included explicitly
    if (amp.get_nf_powers() == 1) result *= T(-1);

    _MESSAGE(Color::Code::FG_BLUE, "Result:");
    _MESSAGE("-----------------------------------------------------------------------------------------------");
    std::cout << std::setprecision(12);
    _MESSAGE(result);
    _MESSAGE("-----------------------------------------------------------------------------------------------");

    return 0;
}

