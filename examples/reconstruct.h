namespace Caravel{
/// Writes a file in Mathematica format that provides a list of remappings of
/// the human readable integral names to a format which allows the drawing of
/// the graph in Mathematica.
void write_integral_info(std::string dir, std::string process, const std::vector<lGraph::IntegralGraph>& integrals){
  std::string filename = dir + "/" + process + ".m";
  std::cout << "Printing integral info to " << filename << std::endl;
  std::ofstream file(filename);

  bool first = true;
  file << "{";
  for (auto& integral : integrals){
    if (!first){ file << ", " << std::endl;}
    first = false;
    file << integral.get_readable_graph() << " -> " << integral;
  }
  file << "}" << std::endl;

}

}
