/**
 * This program computes the finite remainder for a five-point two-loop amplitude on a fixed 
 * phase-space point and checks that all poles correctly cancel.
 * 
 * To compute the remainder for the two-loop five-gluon +-+-+ Nf^2 amplitude one can execute the program as 
 * follows
 * 
 * ./finite_remainder_2l PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,m],Particle[gluon,3,p],Particle[gluon,4,m],Particle[gluon,5,m]],NfPower[2]]
 *
 * To increase the verbosity of the program add the command-line option Verbosity[All] or Verbosity[Remainder] 
 *
 */
#include <cassert>
#include <map>
#include <random>
#include <sstream>
#include <vector>

#include <functional>
#include <iostream>
#include <iterator>

#include "Core/CaravelInput.h"

#include "Core/settings.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/ResidueEvaluator.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "FunctionSpace/MasterIntegrands.h"

#include "IntegralLibrary/MapToBasis.h"

#include "PoleStructure/Remainder.h"

#include "misc/TestUtilities.hpp"

#include "Core/Timing.h"

int main(int argc, char* argv[]) {
    using namespace Caravel;
    using namespace FunctionSpace::MasterIntegrands;
    using F = F32;

    if (argc < 2) {
        _WARNING("ERROR: expecting some input! Execute for example: ./finite_remainder_2l PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p],Particle[gluon,5,p]],NfPower[2]]");
        exit(1);
    }

    // use all available cores for computing integrand coefficients
    settings::use_setting("IntegrandHierarchy::parallelize_topology_fitting always");

    // parse the command line arguments
    InputParser input{argc,argv};

    // Set the cardinality of the finite field
    size_t cardinality(moduli.front());
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    // Let the user set verbosity level to All (includes info of coefficient computations) or Remainder (shows functions with cancelled poles)
    if(input.cmd_option_exists("Verbosity")){
        if( input.get_cmd_option("Verbosity")[0] == "All" )
            verbosity_settings.show_all();
        else if( input.get_cmd_option("Verbosity")[0] == "Remainder" )
            verbosity_settings.remainder_pole_check = true;
    }
    
    // Choose the underlying model. Should work for all 5-parton processes
    SM_color_ordered model;

    // read the input specifying a partial amplitude
    auto  pa = input.get_cmd_option<PartialAmplitudeInput>("PartialAmplitudeInput");

    // Below we construct a five-point phase space point, so make sure we provided a five point process.
    if (pa.get_multiplicity() != 5) {
        std::cerr << "Please provide a massless five-point process." << std::endl;
        std::cerr << "Got: " << pa.get_id_string() << std::endl;
        return 1;
    }

    // For 5-point processes express the integrals in terms of pentagon functions.
    set_settings_for_standard_choice_of_integrals(pa.get_multiplicity());

    // Print the information about the amplitude whose remainder we are computing.
    _MESSAGE(Color::Code::FG_BLUE,"Amplitude considered: ",pa.get_id_string());


    // The kinematic point is given by the twistor parametrization.
    // We read the (rational) parameters from the command line, otherwise use default point.
    _MESSAGE(Color::Code::FG_BLUE, "Generating a 5-particle phase space point");
    std::vector<BigRat> xs;

    if (input.cmd_option_exists("TwistorParameters")) { 
        xs = Caravel::detail::read_twistor_parameters(input.get_cmd_option("TwistorParameters")); 
    }
    else {
        xs = {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17), BigRat(-1)}; // 5pt std point
    }

    auto physical_point_rational = rational_mom_conf<BigRat>(5, xs);

    momentumD_configuration<F, 4> physical_point_finite_field = static_cast<momentumD_configuration<F, 4>>(physical_point_rational);

    START_TIMER(construction);
    _MESSAGE(Color::Code::FG_BLUE,"Constructing remainder");
    // Construct a remainder from the partial amplitude input in the chosen model.
    FiniteRemainder::TwoLoopRemainder<F,C> remainder(model,pa);
    _MESSAGE(Color::Code::FG_BLUE,"Constructing mapper to pentagon function basis");
    // Construct a mapper that maps all the pentagon functions appearing in the amplitude to a basis.
    Integrals::MapToBasis<F, Integrals::StdBasisFunction<C, C>> mapper(pa);
    STOP_TIMER(construction);

    START_TIMER(evaluation);
    // Evaluate the amplitude on the phase space point in the finite field and map the pentagon functions to a basis.
    _MESSAGE(Color::Code::FG_BLUE,"Computing remainder (will do warmup run if required)");
    auto res = remainder(physical_point_finite_field);
    auto mapped_res = mapper(res);
    STOP_TIMER(evaluation);

    _MESSAGE("");

    size_t err_count = 0;

    // Check that the remainder has no poles.
    if( check_if_poles_vanish(mapped_res) ){
        _MESSAGE(Color::Code::FG_GREEN, "ALL POLES CANCEL", Color::Code::FG_DEFAULT);
    }
    else{
        _WARNING_RED("POLES DON'T CANCEL");
        ++err_count;
        return err_count;
    }

    // Construct funcion list on current finite field.

    _MESSAGE(Color::Code::FG_BLUE, "Rational reconstruction of remainder");

    auto remainder_functions = res.get_all_basis_functions();
    std::function<std::vector<DenseRational<F>>(const momentumD_configuration<F, 4>&)> coeffs_function = [&](const auto& ps) {
        auto toret = remainder(ps);
        auto series = toret.get_all_coeffs();
        std::vector<DenseRational<F>> t;
        for (auto& s : series) t.push_back(DenseRational<F>(DensePolynomial<F>(std::vector<F>{s[0]}), DensePolynomial<F>(std::vector<F>{F(1)})));
        return t;
    };

    auto rat_result = RationalReconstruction::rational_reconstruct(physical_point_rational, coeffs_function);
    std::vector<Series<C>> rat_result_series;
    for (auto& el: rat_result){
       std::vector<C> temp {C(el.numerator.coefficients.at(0))};
       rat_result_series.emplace_back(0, 0, temp);
    }

    // Reassemble the coefficients back into evaluatable form.
    Integrals::SemiAnalyticExpression<C, C> reconstructed_remainder_evaluator(remainder_functions, rat_result_series);
 
    _MESSAGE(Color::Code::FG_BLUE,"Rational reconstruction of subtraction");
    std::function<std::vector<DenseRational<F>>(const momentumD_configuration<F, 4>&)> subtraction_coeffs_function = [&remainder](const auto& ps) {
        auto toret = remainder.evaluate_subtraction(ps);
        auto series = toret.get_all_coeffs();
        std::vector<DenseRational<F>> t;
        for (auto& s : series){
            t.push_back(DenseRational<F>(DensePolynomial<F>(std::vector<F>{s[-4], s[-3], s[-2], s[-1], s[0]}), DensePolynomial<F>(std::vector<F>{F(0), F(0), F(0), F(0), F(1)})));
        }
        return t;
    };

    auto rat_subtraction = RationalReconstruction::rational_reconstruct(physical_point_rational, subtraction_coeffs_function);
    auto subtraction_functions = remainder.evaluate_subtraction(physical_point_finite_field).get_all_basis_functions();

    std::vector<Series<C>> rat_subtraction_series;
    for (auto& el: rat_subtraction){
       rat_subtraction_series.push_back(to_precision<C>(el.taylor_expand_around_zero(0)));
    }


    // Reassemble the coefficients back into evaluatable form.
    Integrals::SemiAnalyticExpression<C, C> reconstructed_subtraction_evaluator(subtraction_functions, rat_subtraction_series); 

    // Evaluate remainders.
    auto s12 = physical_point_rational.s(1, 2);
    auto s23 = physical_point_rational.s(2, 3);
    auto s34 = physical_point_rational.s(3, 4);
    auto s45 = physical_point_rational.s(4, 5);
    auto s51 = physical_point_rational.s(5, 1);
    auto tr5 = levi(physical_point_rational[1], physical_point_rational[2], physical_point_rational[3], physical_point_rational[4]);

    std::cout << "Evaluating " << remainder_functions.size() << " special functions for subtraction." << std::endl;

    // Put results into physical normaliztion.
    auto subtraction_eval = reconstructed_subtraction_evaluator({C(s12), C(s23), C(s34), C(s45), C(s51), C(tr5), C(1)}, {"s12", "s23", "s34", "s45", "s15", "tr51234", "signtr51234"});
    std::cout << "Subtraction = " << two_loop_norm_caravel_to_physical<C>*subtraction_eval << std::endl;

    std::cout << "Evaluating " << subtraction_functions.size() << " special functions for remainder." << std::endl;
    auto remainder_eval = reconstructed_remainder_evaluator({C(s12), C(s23), C(s34), C(s45), C(s51), C(tr5), C(1)}, {"s12", "s23", "s34", "s45", "s15", "tr51234", "signtr51234"});
    std::cout << "Remainder = " << two_loop_norm_caravel_to_physical<C>*remainder_eval << std::endl;
    std::cout << "Amplitude = " << two_loop_norm_caravel_to_physical<C>*subtraction_eval + remainder_eval << std::endl;



    return err_count;
}

