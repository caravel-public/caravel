#include <algorithm>
#include <cassert>
#include <cmath>
#include <exception>
#include <iostream>
#include <map>
#include <random>
#include <sstream>
#include <string>
#include <vector>

#include "Core/Debug.h"
#include "Core/momD_conf.h"
#include "Core/type_traits_extra.h"
#include "Core/typedefs.h"
#include "misc/TestUtilities.hpp"

#include "FunctionSpace/MasterIntegrands.h"

using namespace Caravel;

template <typename T> std::enable_if_t<std::is_same<T, BigRat>::value, bool> is_positive(const T& v) {
    if (v < BigRat(0)) return false;
    return true;
}

template <typename T> std::enable_if_t<!std::is_same<T, BigRat>::value, bool> is_positive(const T& v) { return v.real() > 0; }

// check if all s[i,i+1] < 0
template <typename T, size_t Ds> void is_euclidean(Caravel::momD_conf<T, Ds>& mc, const std::vector<size_t>& indices) {
    using namespace FunctionSpace::MasterIntegrands;
    bool is_euclid = true;
    assert(mc.size() == indices.size());

    if (mc.size() == 4) {
        if (is_positive(mc[indices[0]] * mc[indices[1]])) { is_euclid = false;  std::cout<<"WARNING: s"<<indices[0]<<indices[1]<<" = "<<mc[indices[0]] * mc[indices[1]]<<std::endl; } // s
        if (is_positive(mc[indices[1]] * mc[indices[2]])) { is_euclid = false;  std::cout<<"WARNING: s"<<indices[1]<<indices[2]<<" = "<<mc[indices[1]] * mc[indices[2]]<<std::endl; } // t
    }
    else if (mc.size() == 5) {
        if (is_positive(mc[indices[0]] * mc[indices[1]])) { is_euclid = false;  std::cout<<"WARNING: s"<<indices[0]<<indices[1]<<" = "<<mc[indices[0]] * mc[indices[1]]<<std::endl; } // s12
        if (is_positive(mc[indices[1]] * mc[indices[2]])) { is_euclid = false;  std::cout<<"WARNING: s"<<indices[1]<<indices[2]<<" = "<<mc[indices[1]] * mc[indices[2]]<<std::endl; } // s23
        if (is_positive(mc[indices[2]] * mc[indices[3]])) { is_euclid = false;  std::cout<<"WARNING: s"<<indices[2]<<indices[3]<<" = "<<mc[indices[2]] * mc[indices[3]]<<std::endl; } // s34
        if (is_positive(mc[indices[3]] * mc[indices[4]])) { is_euclid = false;  std::cout<<"WARNING: s"<<indices[3]<<indices[4]<<" = "<<mc[indices[3]] * mc[indices[4]]<<std::endl; } // s45
        if (is_positive(mc[indices[4]] * mc[indices[0]])) { is_euclid = false;  std::cout<<"WARNING: s"<<indices[4]<<indices[0]<<" = "<<mc[indices[4]] * mc[indices[0]]<<std::endl; } // s15
        auto tr5 = levi(mc[indices[0]], mc[indices[1]], mc[indices[2]], mc[indices[3]]);
        if (!is_positive(tr5 * tr5)) { is_euclid = false;  std::cout<<"WARNING: tr5 = "<< tr5 <<std::endl; } // tr5^2
    }

    if (!is_euclid) {
        _WARNING_RED("Integrals are only provided in the Euclidean region, sij < 0! Momentum indices received: ", indices);
        // std::exit(1);
    }
}
