# Examples

The programs contained in this directory aim to exemplify the functionalities of
the `Caravel` library. In what follows, we give instructions on how to run the
programs and to interpret the corresponding output. The following examples are
provided with `Caravel`
1. [treeamp](#treeamp) - A tree-level evaluator for N-parton or N-graviton processes
2. [amplitude_evaluator_1l](#amplitude_evaluator_1l) - A one-loop evaluator for four- and five-parton processes in the euclidean region
3. [amplitude_evaluator_2l](#amplitude_evaluator_2l) - A two-loop evaluator for four- and five-parton processes in the euclidean region
4. [finite_remainder_2l](#finite_remainder_2l) - Computes the finite remainder for two-loop five-parton amplitudes
5. [4parton_2loop_analytics_MPI](#4parton_2loop_analytics_mpi) - Example to analytically reconstruct two-loop four parton amplitudes
6. [5parton_1loop_analytics_MPI](#5parton_1loop_analytics_mpi) - Example to analytically reconstruct one-loop five parton amplitudes

***
## [Specifying scattering amplitudes]()
Many of the programs shown here rely on input provided by the user that
specifies completely the desired scattering amplitude.  In `Caravel` this is
done by passing a string similarly formatted as headed lists in Mathematica that
specify the particle content and other parameters of the corresponding
amplitudes. In the following we will discuss the syntax to build up the user
input.

First, we present how to specify a particle in the example programs interface.
A particle will be described by a string `Particle[field,index,state]`, where
`field`  describes the type of the particle, `index` refers to the momentum
index of the particle that ranges from 1 to N in an N-point amplitude. And
finally, the keyword `state` assigns the polarization state of the particle.
The possible input values for the different keywords are shown in the table
below

Type       | field               | state
-----------|---------------------|------------------------
Gluon      | `gluon`             | `p`, `m`
Quark      | `q`, `u`, `d`, `c`, `s`, `b`       | `qbp`, `qm`
Anti-Quark | `qb`, `ub`, `db`, `cb`, `sb`, `bb` | `qbp`, `qm`
Graviton   | `G`                 | `hpp`, `hmm`

The easiest amplitudes to build out of these particles are tree-level scattering
amplitudes.  The input for tree amplitudes are represented by simple list of
particles. For example

```
Particles[Particle[gluon,1,p],Particle[gluon,2,m],Particle[gluon,3,p],Particle[gluon,4,m]]
```

is the input for the color-ordered four-gluon tree-level scattering amplitude
with helicity configuration `+-+-`.

Finally, loop amplitudes are represented by color-ordered partial amplitudes and
the number of closed fermion loops. For example

```
PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p]],NfPower[1]]
```

represents the loop amplitude for the four gluon all-plus helicity amplitude
with one closed fermion loop.  More specific `NfPower[n]` represents the `nF^n`
contribution to the loop amplitude, where `nF` is the number of light fermion
flavors. For amplitudes without closed fermion loops the additional argument
`NfPower` can be dropped completely instead of specifying `NfPowes[0]`.

For graviton scattering amplitudes neither the ordering of the external
particles nor the number of closed fermion loops play a role.

## [treeamp](examples/treeamp.cpp)

Required setup:

Configuration option | Values    | Comment
---------------------|-----------|-----------------------
`finite-fields`      | `true`    | In case amplitudes are computed using finite field arithmetic
`gravity-model`      | `Cubic`   | In case graviton amplitudes are computed
`precision-QD`       | `HP`, `VHP`, `all`   | In case amplitudes are computed using high-precision floating-point arithmetic

The program computes tree-level amplitudes for n-parton color-ordered processes
or for n-graviton processes.


The program can be executed for example through the command

```sh
$ ./examples/treeamp "Particles[Particle[u,1,qm],Particle[ub,2,qbp],Particle[gluon,3,m],Particle[gluon,4,p],Particle[gluon,5,m],Particle[gluon,6,p]]"
```

which will compute 6-parton scattering amplitude of an up quark with negative
and an anti-up with positive helicity and four gluons with alternating
helicities.  See [above](#specifying-scattering-amplitudes) for details about
the structure of the argument. The quotes around the `Particles` list are
optional, as long as no spaces are included within it.


The user can specify the phase space point to be used by providing a textfile
`treeampPSP.dat` (in the same directory from where the program is run), which
contains the desired external momenta. Each line should contain exactly one
momentum in the form `E, px, py, pz`. If no file is present, the program will
choose a random phase space point.


The program prints the number of off-shell Berends-Giele currents used in the
computation of the specified scattering amplitude together with the employed
phase space point and the value of the amplitude.


The user may want to change the computation to higher floating point precision
or exact evaluations. To do so pass an additional input string
`Precision[prec]`, where `prec` is either `HP` for double-dobule floating-point
precision or `VHP` for quad-double floating-point precision. 


Finally, the user can also evaluate tree level amplitudes using finite-fields
arithmetics. In that case the user has to pass the cardinality of the
finite-field by appending to the amplitude input the keyword `Cardinality[k]`,
where `k` is a prime number smaller than 2^31. In this case the user is forced
to provide a phase space point via the `treeampPSP.dat` file. Notice that for
finite field evaluations the space time metric has an alternating signature:
`(+,-,+,-)`.

***

## [amplitude_evaluator_1l](examples/amplitude_evaluator_1l.cpp)

Required setup:

Configuration option | Values
---------------------|-----------------------------------
`finite-fields`      | `true`
`field-ext-fermions`     | `true`
`integrals`          | `all`

The program numerically evaluates one-loop four- and five-parton helicity
amplitudes up to order **ep^2** in the dimensional regulator, as required for
the calculation of the so-called finite remainders of the corresponding two-loop
amplitudes. **Restriction**: this program runs only on the *Euclidean* region of
phase space, as master integrals are so far included only for such region.

By default the program runs on phase space points which allow the reproduction
of the results  presented in tables 3 and 4 of
[arXiv:1809.09067](https://arxiv.org/abs/1809.09067). If required, the program
performs a *warmup* run to find vanishing integrand coefficients, then the
master-integral coefficients are rationally reconstructed from finite field
evaluations, integrals are computed and finally the amplitude is obtained and
printed to the standard output. The computations run sequentially in a single
thread.

The program can be executed from the build directory of the `Caravel` library
for example with the command:

```sh
$ ./examples/amplitude_evaluator_1l "PartialAmplitudeInput[Particles[Particle[gluon,1,m],Particle[gluon,2,m],Particle[gluon,3,p],Particle[gluon,4,p]]]"
```

where the required command-line argument specifies the desired amplitude. In the
example it represents the one-loop four-gluon color-ordered helicity amplitude,
with the first two particles of negative helicity and the last two of positive
helicity.

More details on the specification of amplitudes are given
[above](#specifying-scattering-amplitudes).

The optional argument `TwistorParameters` allows to specify a phase space point,
for example:

```sh
$ ./examples/amplitude_evaluator_1l PartialAmplitudeInput[...] TwistorParameters[-1/3,1/2,1/13,7/4,9]
```

where the contents of the list are twistor variables as specified in
[1812.04586](https://arxiv.org/abs/1812.04586). Notice that for four-point
amplitudes, only two twistor parameters are required that parametrize the
corresponding Mandelstam variables.

A final remark on the evaluation of five-point amplitudes: Compared to the
evaluation of four-point master integrals the five-point integrals take
considerably longer to evaluate.

***

## [amplitude_evaluator_2l](examples/amplitude_evaluator_2l.cpp)

Required setup:

Configuration option | Values
---------------------|-----------------------------------
`finite-fields`      | `true`
`field-ext-fermions` | `true`
`integrals`          | `all`

The program numerically evaluates the two-loop four- and five-parton helicity
amplitudes up to finite pieces in the dimensional regulator. **Restriction**:
this program runs only on the *Euclidean* region of phase space, as master
integrals are so far included only for such region.

By default the program runs on phase space points which allow the reproduction
of the results  presented in tables 1 and 2 of
[arXiv:1809.09067](https://arxiv.org/abs/1809.09067). If required, the program
performs a *warmup* run for finding vanishing integrand coefficients as well as
resolving the analytic structure in the dimensional regulator for the
master-integral coefficients. Then the master-integral coefficients are
rationally reconstructed from finite field evaluations, and then integrals and
coefficients are combined into a semi-analytic object which is expressed in
terms of unevaluated pentagon functions. Next, those special functions are
evaluated to produce the amplitude which is then printed to the standard output.
The computation of the master integral coefficients is performed in parallel,
using all available threads in the CPU, then the special functions are evaluated
sequentially in a single thread.

The program expect an argument specifying the requested amplitude as described
[above](#specifying-scattering-amplitudes). For example:

```sh
$ ./examples/amplitude_evaluator_2l "PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[gluon,3,m],Particle[gluon,4,p],Particle[gluon,5,p]],NfPower[1]]"
```

The evaluation of these two-loop amplitudes is considerably more involved than
the corresponding one-loop amplitudes. Consequently, the runtime of the previous
example is about 4 and a half minutes to rationally reconstruct all master
integral coefficients, while the computation of the special functions can take
about 20 minutes (employing a 12-core Intel i7 processor).

The program allows for optional arguments. First, as in the [one-loop
case](#amplitude_evaluator_1l) the phase space point can be specified by a
`TwistorParameters` headed list with twistor parameters. Second, the argument
`Verbosity[All]` can also be passed in order to print to the standard output
extra information of the computations performed.

***

## [finite_remainder_2l](examples/finite_remainder_2l.cpp)

Required setup:

Configuration option | Values
---------------------|-----------------------------------
`finite-fields`      | `true`
`field-ext-fermions` | `true`
`integrals`          | `pentagons` or `all`

The program computes the finite remainder of planar two-loop five-parton
amplitudes, and checks explicitly that poles in the dimensional regulator
cancel. This was employed in [1904.00945](https://arxiv.org/abs/1904.00945) in
order to reconstruct the analytic form of the planar two-loop five-parton
amplitudes.

The program proceeds by building the requested two-loop amplitude and its
corresponding infrared subtraction. Then the constructed objects representing
the employed pentagon functions are rewritten in terms of a minimal set of
independent functions. Next the one- and two-loop amplitudes are computed, and a
check of exactly vanishing poles in the **ep** dimensional regulator is
performed. A warmup run would be performed if required for the two-loop
amplitude. The computation of the two-loop master integral coefficients is
performed in parallel, using all available threads in the CPU.

The program can be executed for example through the command

```sh
$ ./examples/finite_remainder_2l "PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p],Particle[gluon,5,p]],NfPower[2]]"
```

The argument specifying the amplitude follows the same pattern as described
[above](#specifying-scattering-amplitudes).


The optional argument `Verbosity[All]` or `Verbosity[Remainder]` can be passed
in the command line to request the printing of extra information.

***

## [4parton_2loop_analytics_MPI](examples/4parton_2loop_analytics_MPI.cpp)

**Required setup:** MPI installed in the system and the configuration options:

Configuration option | Values
---------------------|-----------------------------------
`finite-fields`      | `true`
`field-ext-fermions` | `true`

The massless four-point amplitude depends only on the Mandelstam variables `s`
and `t`. The program computes the analytic form the four-parton two-loop
amplitudes for `s=1` and `x=t/s` using Thiele's formula for the reconstruction.
The proper mass dimensions of the reconstructed master-integral coefficients is
then recovered from dimensional analysis.


The program can be executed for example through the command

```sh
$ mpirun -np 12 ./examples/4parton_2loop_analytics_MPI "PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p]],NfPower[1]]"
```

The argument specifying the amplitude follows the same pattern as described
[above](#specifying-scattering-amplitudes).

Once the analytic form of the master coefficients are reconstructed in the
finite field, the amplitude will be saved as a text file in the local directory
`analytics/amplitudes_XY/`, where `XY` refers to the cardinality of the finite
field.

Notice, that the program also attempts the rational reconstruction of numerical
coefficients using a single finite field.  The program, will numerically check
for a phase space point if the reconstructed analytic coefficients match with
the direct computation of these coefficients. However, in rare cases the
reconstruction algorithm can fail.  In the case of a successful reconstruction
the amplitude is again saved in a text file under
`analytics/amplitudes_rational/`.

***

## [5parton_1loop_analytics_MPI](examples/5parton_1loop_analytics_MPI.cpp)

**Required setup:** MPI installed in the system and the configuration options:

Configuration option | Values
---------------------|-----------------------------------
`finite-fields`      | `true`
`field-ext-fermions` | `true`

The program computes the analytic form of five-parton one-loop amplitudes, using
multivariate functional reconstruction. The five-parton amplitudes depend on
five twistor parameters `x0,...,x4`. The problem is reduced to a
four-dimensional reconstruction by setting `x4 = 1`. Therefore, the proper mass
dimension of the final result is then recovered from a dimensional analysis
using the `x4` Twistor variable.

The program can be executed for example through the command

```sh
$ mpirun -np 12 ./examples/5parton_1loop_analytics_MPI "PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p],Particle[gluon,5,p]]]"
```

to obtain an analytic result for the five-gluon all plus helicity amplitude.


The argument specifying the amplitude follows the pattern described
[above](#specifying-scattering-amplitudes).

Once the analytic form of the master coefficients are reconstructed in the
finite field, the amplitude will be saved as a text file in the local directory
`analytics/amplitudes_XY/`, where `XY` refers to the cardinality of the finite
field.

Notice, that the program also attempts the rational reconstruction of the
coefficients using a single finite field.  The program, will numerically check
for a phase space point if the reconstructed analytic coefficients match with
the direct computation of these coefficients. However, in rare cases the
reconstruction algorithm can fail.  In the case of a successful reconstruction
the amplitude is again saved in a text file under
`analytics/amplitudes_rational/`.

***

