/**
 * Computes tree-level amplitudes of the type:
 *
 * 1) n partons color ordered
 *
 * 2) n gravitons (build option 'gravity-model' needs to be 'Cubic' or 'all')
 *
 * Argument must be passed in the form of a 'Particles' headed list, like for 
 * example:
 *
 * ./treeamps Particles[Particle[q,1,qbp],Particle[qb,2,qm],Particle[gluon,3,p],Particle[gluon,4,m]]
 *
 * for a two-quark two-gluon amplitude.
 *
 * The program can be run in (high-precision) floating point arithmetic or using 
 * modular arithmetic. By default, it runs in single-double precision. For 
 * other choices add the argument:
 *
 * FF)	Cardinality[nnnnn] ('finite-fields' required, 'nnnnn' is a prime < 2^31)
 *
 * HP)	HighPrecision[HP] ('precision-QD' set to 'HP' required)
 *
 * VHP)	HighPrecision[VHP] ('precision-QD' set to 'VHP' required)
 *
 * Phase space point read from file "treeampPSP.dat", if not it is chosen 
 * randomly (for floating point cases). 
 * Momenta provided in treeampPSP.dat should have one momentum per line
 * in the form: E px py pz
 * 
 * For more documentation, see:
 *
 * examples/README.md
 *
 */
#include "treeamp.h"

using namespace std;
using namespace Caravel;

int main(int argc, char* argv[]) {

    if (argc < 2) {
        _WARNING("WARNING: 'treeamp' expects some input. Execute for example: ./treeamp "
                 "\"Particles[Particle[u,1,qm],Particle[ub,2,qbp],Particle[gluon,3,m],Particle[gluon,4,p]]\"");
        std::exit(1);
    }

    // parse the command line arguments
    InputParser input{argc, argv};

    // the list of particles passed
    std::vector<Particle> particles;
    for (auto it : input.get_cmd_option("Particles")) particles.push_back(Particle(MathList(it, "Particle")));

    size_t nparticles(particles.size());

    // apply checks to the type of process requested and infer couplings
    auto process = process_request(particles);

    if (!process) {
        std::cerr << "ERROR: 'treeamp' can't handle the requested process: " << input.get_cmd_option("Particles") << std::endl;
        std::cerr << "       see examples/README.md for details!" << std::endl;
        std::exit(1);
    }

    auto& couplings = process.value().couplings;
    auto& model = process.value().model;
    auto& forest_proc = process.value().process;

    _MESSAGE(Color::Code::FG_BLUE, "Construct Forest of currents");
    // construct Forest
    Forest forest(model);
    int locate(-1);

    try {
        locate = forest.add_process(forest_proc, couplings);
    }
    catch (const std::exception& e) {
        std::cerr << "ERROR: model " << model.get_name() << " could not build a tree-level amplitude for your request " << particles << std::endl;
        std::cerr << "       perhaps process is disallowed or zero. (" << e.what() << ")" << std::endl;
        std::exit(1);
    }

    forest.define_internal_states_and_close_forest({4});
    std::cout << "Forest constructed containing " << forest.get_total_number_of_tree_currents() << " currents" << std::endl;

    if (input.cmd_option_exists("Cardinality")) {
#ifdef USE_FINITE_FIELDS
        treeamp_evaluator<F32>(forest, nparticles, locate, std::stoul(input.get_cmd_option("Cardinality")[0]));
#else
        _MESSAGE(Color::Code::FG_RED, "ERROR: to evaluate in finite fields, enable option 'finite-fields' and recompile");
        std::exit(1);
#endif
    }
    else if (input.cmd_option_exists("HighPrecision")) {
        if (input.get_cmd_option("HighPrecision")[0] == "HP") {
#ifdef HIGH_PRECISION
        treeamp_evaluator<CHP>(forest, nparticles, locate);
#else
        _MESSAGE(Color::Code::FG_RED, "ERROR: to evaluate in HP floating point, set option 'precision-QD' to 'HP' or 'all'");
        std::exit(1);
#endif
        }
        else if (input.get_cmd_option("HighPrecision")[0] == "VHP") {
#ifdef VERY_HIGH_PRECISION
        treeamp_evaluator<CVHP>(forest, nparticles, locate);
#else
        _MESSAGE(Color::Code::FG_RED, "ERROR: to evaluate in VHP floating point, set option 'precision-QD' to 'VHP' or 'all'");
        std::exit(1);
#endif
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "ERROR: high-precision floating point only for 'HP' and 'VHP' types, received: ", input.get_cmd_option("HighPrecision")[0]);
            std::exit(1);
        }
    }
    else {
        treeamp_evaluator<C>(forest, nparticles, locate);
    }

    return 0;
}

