/**
 * This example showcases the analytic reconstruction of a 4-parton amplitudes.
 *
 * The amplitude depends on two variables, the mandelstams `s` and `t`.
 */
#include <functional>
#include <iostream>
#include <cstdlib>
#include <random>
#include <sstream>
#include <vector>
#include <memory>
#include <string>

#include "Core/settings.h"
#include "Core/typedefs.h"
#include "Core/Utilities.h"

#include "AmplitudeCoefficients/IntegrandHierarchy.h"

#include "FunctionalReconstruction/ThieleFunction.h"
#include "FunctionalReconstruction/DimensionReconstruction.h"
#include "FunctionalReconstruction/SparseMultivariatePolynomial.h"
#include "FunctionalReconstruction/CoefficientReconstructionAid.h"
#include "FunctionalReconstruction/MPI_ParallelReconstruction.h"

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "misc/TestUtilities.hpp"
#include "reconstruct.h"

using namespace Caravel;
using namespace Caravel::Reconstruction;

using F = F32;          // The finite-field type.

int main(int argc, char* argv[]) {

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of independent threads running
    int n_mpi_threads;
    MPI_Comm_size(MPI_COMM_WORLD, &n_mpi_threads);

    // Get the index assigned to the local thread
    int thread_number;
    MPI_Comm_rank(MPI_COMM_WORLD, &thread_number);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);
    std::cout << "Thread # " << thread_number << " is initialized in " << processor_name << " out of " << n_mpi_threads << " threads " << std::endl;

    // more cardinalities in misc/Moduli.hpp
    int64_t cardinality(2147483629);
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    // check for input arguments
    if (thread_number == 0) {
        if (argc < 2) {
            std::string process("PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p]],"
                                "NfPower[2]]");
            std::cerr << "ERROR: need to pass a process as an argument like " << process << std::endl;
            return 1;
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);

    // parse the command line arguments
    InputParser input{argc, argv};

    // Some additional settings can be passed from command line, (only for developers).
    if(input.cmd_option_exists("CaravelSettingsInput")){
        auto csi = input.get_cmd_option<CaravelSettingsInput>("CaravelSettingsInput");
    }

    if (input.cmd_option_exists("Verbosity")){
        auto verbosity_level = input.get_cmd_option("Verbosity")[0];
        if ( verbosity_level == "All")
          verbosity_settings.show_all();
    }
    else{
        verbosity_settings.go_quiet();
    }

    std::unique_ptr<PartialAmplitudeInput> pamp = std::make_unique<PartialAmplitudeInput>(input.get_cmd_option<PartialAmplitudeInput>("PartialAmplitudeInput"));
    // identifying string
    std::string filename = pamp->get_id_string();
    size_t nparticles = pamp->get_particles().size();

    // make sure it is a 4-particle process
    if (nparticles != 4) {
        std::cerr << "ERROR: '4parton_2loop_analytics_MPI' works only for 4-particle processes" << std::endl;
        std::exit(1);
    }
    set_settings_for_standard_choice_of_integrals(4);

    // The model underlying the process under consideration is the color-ordered SM.
    SM_color_ordered model;

    // make sure a warmup file is present
    if (thread_number == 0) {
        auto amplitude = std::make_unique<IntegrandHierarchy::ColourExpandedAmplitude<F, 2>>(model, *pamp);
        if (!amplitude->has_warmup()) {
            _MESSAGE(Color::Code::FG_BLUE, "Performing warmup run");

            // show progress
            verbosity_settings.show_all();

            // Perform the warmup on a rational phase space point
            auto phase_space_pt_rat = rational_mom_conf<BigRat>(4, std::vector<BigRat>{BigRat(-237) / BigRat(57), BigRat(-139) / BigRat(67)});
            // Map the rational phase space point into the finite field
            auto phase_space_pt_ff = static_cast<momentumD_configuration<F, 4>>(phase_space_pt_rat);

            // Perform the warmup
            amplitude->warmup_run(phase_space_pt_ff);

            verbosity_settings.silence_all();
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);

    // Create a unique_ptr of the 2-loop amplitude for the functional reconstruction
    auto amplitude = std::make_unique<IntegrandHierarchy::ColourExpandedAmplitude<F, 2>>(model, *pamp);

    // get information of associated master integrals
    auto integrals = amplitude->get_integral_graphs();

    MPI_Barrier(MPI_COMM_WORLD);

    // load warmup info (to extract rational structure of master-integral coeffs)
    auto warmup_info = load_warmups(amplitude->get_warmup_filename());
    // extract powers of dimensional regulator in numerators of master-integral coeffs
    auto ep_power_numerators = std::get<2>(warmup_info[0]);
    // extract denominators
    auto denominators = std::get<1>(warmup_info[0]);

    // Construct the vector-valued function to reconstruct as computed from the list of params for the PS generator
    std::function<std::vector<F>(std::vector<F>)> f = [&]( std::vector<F> point ) {
        auto psp = rational_mom_conf<F>(nparticles, point);

        // `front()` because we are using the first color structure
        auto result = (amplitude->get_master_coefficients(psp)).front();
        // now flatten master-integral coeffs into a vector
        auto toret = flatten_mi_coefficients<F>(result, ep_power_numerators);

        return toret;
    };

    // Restrict it to univariate
    std::function<std::vector<F>(F)> f_univariate = [&](F x){
       std::vector<F> point = { x, F(1) };
        return f(point);
    };

    // Determine the dimension of all master integral coefficients to restore later
    auto dimensions = determine_dimension(f, 2);

    // Construct the random number generators that are used to generate random phase space points.
    std::mt19937 mt1(42);
    std::function<F()> x_gen = [mt1]() mutable { return F(mt1()); };

    if (thread_number == 0)
        _MESSAGE(Color::Code::FG_BLUE,"Reconstructing functions");

    Caravel::VectorThieleFunction<F> my_rational(x_gen);

    // let everybody wait to reach this point
    MPI_Barrier(MPI_COMM_WORLD);

    MPI_parallel_reconstruct(my_rational, f_univariate, thread_number, n_mpi_threads);

    // let everybody wait to reach this point
    MPI_Barrier(MPI_COMM_WORLD);

    // Outputting
    if (thread_number == 0) {

        // Bring reconstructed result in canonical form
        auto canonicalFF_univariate = my_rational.to_canonical();

        // Restore mass dimension
        std::vector<Ratio<SparseMultivariatePolynomial<F>>> canonicalFF;
        for (size_t i = 0; i < canonicalFF_univariate.size(); i++){
          canonicalFF.push_back(restore_dimension(canonicalFF_univariate.at(i), dimensions.at(i), {"s", "t"}, 1));
        }

        // Attempt to rational guess the result.
        std::function<BigRat(F)> rational_guesser = [cardinality](const F& x) {
            return RationalReconstruction::rational_guess_alternative({BigInt(x), BigInt(cardinality)});
        };

        std::vector<Ratio<SparseMultivariatePolynomial<BigRat>>> canonical_guess;
        for (auto& c : canonicalFF) {canonical_guess.push_back(fmap(rational_guesser, c));}

        _MESSAGE(Color::Code::FG_BLUE,"Testing and storing reconstructed result");

        std::string dirguess = "analytics/amplitudes_rational";
        std::string dirff = "analytics/amplitudes_" + std::to_string(cardinality);
        std::string dirinfo = "analytics/integral_info";
        int syscheck(-1);
        if (system("mkdir -p analytics") > 0) syscheck++;
        if (system(("mkdir -p " + dirguess).c_str()) > 0) syscheck++;
        if (system(("mkdir -p " + dirff).c_str()) > 0) syscheck++;
        if (system(("mkdir -p " + dirinfo).c_str())>0) syscheck++;
        if (syscheck >= 0) _MESSAGE(Color::Code::FG_RED, "Failed creating directories to print results!");

        auto canonical_guess_mi = recover_mi_coefficients<BigRat>( canonical_guess, ep_power_numerators, denominators );
        auto canonicalFF_mi = recover_mi_coefficients<F>( canonicalFF, ep_power_numerators, denominators );
        assert(integrals.size() == canonicalFF_mi.size());

        // perform a check on canonicalFF
        std::vector<F> xsFF = {F(std::rand()), F(std::rand())};
        auto targetFF = f(xsFF);

        assert(targetFF.size() == canonicalFF.size());
        size_t errorsFF(0);
        for (size_t ii = 0; ii < targetFF.size(); ii++) {
            auto resultFF = (canonicalFF.at(ii))(xsFF);
            if (targetFF[ii] != resultFF) {
                _MESSAGE("reconstructed[", ii, "]: ", resultFF, "   numerics[", ii, "]: ", targetFF[ii]);
                errorsFF++;
            }
        }

        if (errorsFF == 0) {
            _MESSAGE(Color::Code::FG_GREEN, "PASSED check of reconstructed result in the same finite field (with cardinality ", cardinality, ")");
            std::string towrite = dirff + "/2L_" + filename + ".dat";
            std::ofstream file(towrite);
            _MESSAGE(Color::Code::FG_BLUE,"Printing analytic finite-field result to ",towrite);
            for (size_t ii = 0; ii < integrals.size(); ii++) {
              file << integrals[ii].get_readable_graph() << "* (" << canonicalFF_mi[ii]<<")";
                if (ii + 1 != integrals.size()) file << "+\n";
            }
            write_integral_info(dirinfo, filename, integrals);
        }
        else
            _MESSAGE(Color::Code::FG_RED, "FAILED check of reconstructed result in the same finite field (with cardinality ", cardinality, ") with ", errorsFF,
                     " errors!");

        // more cardinalities in misc/Moduli.hpp
        cardinality = 2147481509;
        cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

        // perform a check on canonical_guess
        std::vector<BigRat> xsRat = {BigRat(std::rand(), std::rand()), BigRat(std::rand(), std::rand())};
        std::vector<F> xsRatFF = {F( xsRat[0] ), F( xsRat[1] )};
        auto targetRatFF = f(xsRatFF);

        assert(targetRatFF.size() == canonical_guess.size());
        size_t errorsRatFF(0);
        for (size_t ii = 0; ii < targetRatFF.size(); ii++) {
            auto resultRat = canonical_guess.at(ii)(xsRat);
            F resultRatFF = F(resultRat);
            if (targetRatFF[ii] != resultRatFF) {
                _MESSAGE("reconstructed_rational_guess[", ii, "]: ", resultRatFF, "   numerics[", ii, "]: ", targetRatFF[ii]);
                errorsRatFF++;
            }
        }

        if (errorsRatFF == 0) {
            _MESSAGE(Color::Code::FG_GREEN, "PASSED check of analytic result with rational reconstruction, against a result in a second finite field with cardinality ", cardinality);
            std::string towrite = dirguess + "/2L_" + filename + ".dat";
            std::ofstream file(towrite);
            _MESSAGE(Color::Code::FG_BLUE,"Printing analytic result to ",towrite);
            for (size_t ii = 0; ii < integrals.size(); ii++) {
              file << integrals[ii].get_readable_graph() << "* (" << canonical_guess_mi[ii]<<")";
                if (ii + 1 != integrals.size()) file << "+\n";
            }
            write_integral_info(dirinfo, filename, integrals);
        }
        else
            _MESSAGE(Color::Code::FG_ORANGE, "Could not rationally reconstruct the analytic result employing a single finite field! Using a second cardinality ", cardinality,
                     " obtained ", errorsRatFF, " errors");
    } // thread_number 0

    _MESSAGE(Color::Code::FG_DEFAULT, "Thread # ", thread_number, " reached end!");

    // let everybody wait to reach this point
    MPI_Barrier(MPI_COMM_WORLD);

    // Finalize the MPI environment
    MPI_Finalize();

    return 0;
}
