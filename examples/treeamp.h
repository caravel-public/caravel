#include <cmath>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <functional>
#include <iostream>
#include <iomanip>
#include <memory>
#include <optional>
#include <random>
#include <string>
#include <sstream>
#include <vector>

#include "Core/CaravelInput.h"

#include "Core/cutTopology.h"
#include "Core/settings.h"
#include "Core/typedefs.h"
#include "Core/type_traits_extra.h"

#include "Forest/Forest.h"
#include "Forest/Builder.h"
#include "Forest/cardinality.h"

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "Amplitude.h"
#include "IntegralsBH/ProviderBH.h"

#include "Core/InversionStrategies.h"
#include "misc/TestUtilities.hpp"

using namespace Caravel;
using couplings_t = cutTopology::couplings_t;

struct process_info {
    couplings_t couplings;
    Model model;
    Process process;
};

std::optional<process_info> process_request(std::vector<Particle>& vparticles) {
    if (vparticles.size() < 3) {
        std::cerr << "ERROR: 'treeamp' needs at least three particles in request!" << std::endl;
        return {};
    }

    bool partons(true), gravitons(true);
    for (auto& p : vparticles) {
        if (!p.is_colored()) partons = false;
        if (p.get_flavor() != ParticleFlavor::G) gravitons = false;
    }

    // check request is for n-parton or n-graviton amp
    if (!partons && !gravitons) {
        std::cerr << "ERROR: 'treeamp' so far handles only n-parton or n-graviton tree-level amplitudes!" << std::endl;
        return {};
    }

    process_info tret;

    if (partons){ tret.couplings = { {"gs", int(vparticles.size() - 2)} }; tret.model = SM_color_ordered(); }
    else{ tret.couplings = { {"K", int(vparticles.size() - 2)} }; tret.model = CubicGravity(); }

    std::vector<Particle*> tconst;
    for(auto& p: vparticles) tconst.push_back(&p);
    tret.process = Process(tconst);

    return tret;
}

template <class T>
std::enable_if_t<is_exact<T>::value, momD_conf<T, 4>> get_treeamp_momD_conf_random(size_t nparticles) {
    _MESSAGE(Color::Code::FG_RED, "ERROR: no random phase-space generation for exact number field. Include your phase-space point in the file 'treeampPSP.dat'");
    std::exit(1);
}

template <class T>
std::enable_if_t<!is_exact<T>::value, momD_conf<T, 4>> get_treeamp_momD_conf_random(size_t nparticles) {
    using TR = typename T::value_type;

    _MESSAGE(Color::Code::FG_BLUE, "Construct a random phase space point");
    // pick a random phase space point
    PhaseSpace<TR> tph(nparticles, TR(1.));
    tph.set_reset_seed(false);
    srand(1110);
    tph.generate();
    auto moms = tph.mom();
    return to_momDconf<TR, 4>(moms);
}

template <typename T> void check_momD_conf(momD_conf<T, 4> mc, size_t nparticles) {
    momD<T, 4> qtot = momD<T, 4>(T(0), T(0), T(0), T(0));

    // Check number of momenta
    if (mc.size() != nparticles) {
        _MESSAGE(Color::Code::FG_RED, "Wrong number of momenta provided! Expected: ", nparticles, " got: ", mc.size());
        std::exit(1);
    }

    // Check on-shellness
    for (auto p : mc) {
        qtot += p;
        if (!is_zero(p * p)) {
            _MESSAGE(Color::Code::FG_RED, "Momentum: ", p, " not onshell! p^2 = ", p * p);
            std::exit(1);
        }
    }

    // Check Momentum conservation
    for (auto c : qtot) {
        if (!is_zero(c)) {
            _MESSAGE(Color::Code::FG_RED, "Momentum conservation violated! q = ", qtot);
            std::exit(1);
        }
    }
}

template <class T>
momD_conf<T, 4> get_treeamp_momD_conf(size_t nparticles) {
    // check if treeampPSP.dat exist
    std::ifstream stream ("treeampPSP.dat");
    if (stream.is_open()) {
	_MESSAGE(Color::Code::FG_ORANGE,"Reading momenta from file treeampPSP.dat");
        stream.close();
        // use some old utility
        momD_conf_reader<T, 4> reader("treeampPSP.dat", nparticles);
        reader.next();
        check_momD_conf(reader, nparticles);
        return std::move(reader);
    }
    else {
        return get_treeamp_momD_conf_random<T>(nparticles);
    }
}

template <class T>
std::enable_if_t<is_exact<T>::value> treeamp_set_cardinality(size_t cardinality) {
    cardinality_update_manager<T>::get_instance()->set_cardinality(cardinality);
}

template <class T>
std::enable_if_t<!is_exact<T>::value> treeamp_set_cardinality(size_t cardinality) {}

template <class T>
void treeamp_evaluator(Forest& forest, size_t nparticles, int locate, size_t cardinality = 0) {
    // set cardinality (if necessary)
    treeamp_set_cardinality<T>(cardinality);

    // FIXME: 	for T = F32 non-vanishing amps with quarks return zero in special 
    //		momentum configurations, like in eq (4.1) of arXiv:1809.09067 
    //		(e.g. "Particles[Particle[u,1,qm],Particle[ub,2,qbp],Particle[gluon,3,p],Particle[gluon,4,m]]")
    //		A random boost of the following momconf 'fixes' this, returning a non-zero result!
    auto momconf = get_treeamp_momD_conf<T>(nparticles);

    std::cout << std::setprecision(12);
    std::cout << "Momenta:\n" << momconf << std::endl;

    // prepare to evaluate
    Builder<T, 4> bob(&forest, nparticles);
    bob.set_p(momconf);
    _MESSAGE(Color::Code::FG_BLUE, "Compute the tree");
    bob.compute();
    bob.contract();

    std::cout << "Tree: " << bob.get_tree(locate) << std::endl;

}
