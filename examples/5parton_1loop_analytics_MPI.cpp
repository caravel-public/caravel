/**
 * This example showcases the analytic reconstruction of 5-parton amplitudes.
 *
 * The amplitude depends on five variables, the twistor variables x0,...,x4.
 */

#include <algorithm>
#include <functional>
#include <iostream>
#include <cstdlib>
#include <random>
#include <sstream>
#include <vector>

#include "Core/settings.h"
#include "Core/typedefs.h"
#include "Core/Utilities.h"

#include "AmpEng/CoefficientEngine.h"

#include "FunctionalReconstruction/PeraroFunction.h"
#include "FunctionalReconstruction/DimensionReconstruction.h"
#include "FunctionalReconstruction/SparseMultivariatePolynomial.h"
#include "FunctionalReconstruction/MPI_ParallelReconstruction.h"
#include "FunctionalReconstruction/Alphabets/PentagonAlphabetPlanar.h"
#include "FunctionalReconstruction/AlphabetTools.h"
#include "FunctionalReconstruction/CoefficientReconstructionAid.h"


#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "misc/TestUtilities.hpp"
#include "reconstruct.h"

using namespace Caravel;
using namespace Reconstruction;
using namespace AmpEng;
using F = F32;          // The finite-field type.

int main(int argc, char* argv[]) {

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of independent threads running
    int n_mpi_threads;
    MPI_Comm_size(MPI_COMM_WORLD, &n_mpi_threads);

    // Get the index assigned to the local thread
    int thread_number;
    MPI_Comm_rank(MPI_COMM_WORLD, &thread_number);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);
    std::cout << "Thread # " << thread_number << " is initialized in " << processor_name << " out of " << n_mpi_threads << " threads " << std::endl;

    // more cardinalities in misc/Moduli.hpp
    int64_t cardinality(2147483629);
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    // check for input arguments
    if (thread_number == 0) {
        if (argc < 2) {
            std::string process("PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p],Particle[gluon,5,p]],"
                                "NfPower[1]]");
            std::cerr << "ERROR: need to pass a process as an argument like " << process << std::endl;
            return 1;
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);

    // parse the command line arguments
    InputParser input{argc, argv};

    // Some additional settings can be passed from command line, (only for developers).
    if(input.cmd_option_exists("CaravelSettingsInput")){
        auto csi = input.get_cmd_option<CaravelSettingsInput>("CaravelSettingsInput");
    }

    if (input.cmd_option_exists("Verbosity")){
      auto verbosity_level = input.get_cmd_option("Verbosity")[0];
      if ( verbosity_level == "All")
        verbosity_settings.show_all();
    }
    else{
      verbosity_settings.go_quiet();
    }

    settings::use_setting("BG::partial_trace_normalization full_Ds");
    // 1-loop coeffs are linear in epsilon
    settings::use_setting("IntegrandHierarchy::number_of_Ds_values 2");

    std::unique_ptr<PartialAmplitudeInput> pamp = std::make_unique<PartialAmplitudeInput>(input.get_cmd_option<PartialAmplitudeInput>("PartialAmplitudeInput"));
    // identifying string
    std::string filename = pamp->get_id_string();
    size_t nparticles = pamp->get_particles().size();

    // make sure it is a 5-particle process
    if (nparticles != 5) {
        std::cerr << "ERROR: '5parton_1loop_analytics_MPI' works only for 5-particle processes" << std::endl;
        std::exit(1);
    }

    // The model to use
    std::shared_ptr<Model> SM{model_from_name("SM_color_ordered")};

    // Construct the one-loop amplitude.
    CoefficientEngine<F> ampl(SM, *pamp);

    // check if warmup present
    if (thread_number == 0) {
        if (!ampl.has_warmup()) {
            _MESSAGE(Color::Code::FG_BLUE, "Performing warmup run");
            std::vector<F> xs;
            for (size_t ii = 0; ii < 3 * nparticles - 10; ii++) xs.push_back(F(std::rand()));
            momD_conf<F, 4> psp_warmup = rational_mom_conf<F>(nparticles, xs);
            ampl.warmup_run(psp_warmup);
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    // everybody loads warmup
    ampl.reload_warmup();

    // load warmup info (to extract rational structure of master-integral coeffs)
    auto warmup_info = load_warmups(ampl.get_warmup_filename());
    // extract powers of dimensional regulator in numerators of master-integral coeffs
    auto ep_power_numerators = std::get<2>(warmup_info[0]);

    // build lambda function for reconstruction
    constexpr size_t _N = 4;
    std::function<std::vector<F>(std::vector<F>)> f = [&](std::vector<F> point) {
        auto psp = rational_mom_conf<F>(nparticles, point);
        auto coeffsFep = ampl.compute(psp);
        return flatten_mi_coefficients<F>(coeffsFep, ep_power_numerators);
    };

    // Restrict to x4 = 1.
    std::function<std::vector<F>(std::array<F, _N>)> f_restricted = [&](std::array<F, _N> xs) {
           // set first variable to 1
           std::vector<F> point(std::begin(xs), std::end(xs));
           point.push_back(F(1));
           return f(point);
   };

    // Determine the dimension of all master integral coefficients to restore later.
    std::function<std::vector<F>(std::vector<F>)> dim_f = [&](std::vector<F> xs) {
          // set all dimensionless variables to random numbers
          return f({ F(2039480), F(28084080), F(90494920), F(11188822), xs[0] });
      };
    auto dimensions = determine_dimension(dim_f, 1);

    // use the planar pentagon alphabet in terms of twistor variables
    PentagonAlphabetPlanar alphabet;

    if (thread_number == 0) _MESSAGE(Color::Code::FG_BLUE, "Extracting factors from pentagon alphabet");

    auto info_f_factors = remove_factors_in_alphabet<F, F, 4>(f_restricted, &alphabet);
    auto& f_no_factors = info_f_factors.function;
    auto& factors = info_f_factors.factors;

    // random number generators
    std::array<std::function<F()>, _N> x_gens;

    size_t seed(42);
    for (size_t ii = 0; ii < _N; ii++) {
        // we use mersenne_twister_engine as random number generators
        std::mt19937 mt(seed++);
        x_gens[ii] = [mt]() mutable { return F(mt()); };
    }

    if (thread_number == 0)
        _MESSAGE(Color::Code::FG_BLUE,"Reconstructing functions");

    VectorPeraroFunction<F, _N> my_rational(x_gens, true);

    my_rational.verbose = false;
    auto prepared_f = my_rational.prepare_function(f_no_factors);

    // let everybody wait to reach this point
    MPI_Barrier(MPI_COMM_WORLD);

    int syscheck(-1);
    if (thread_number == 0) {
        if(system("mkdir -p analytics")>0) syscheck++;
    }

    MPI_parallel_reconstruct(my_rational, prepared_f, thread_number, n_mpi_threads);

    // let everybody wait to reach this point
    MPI_Barrier(MPI_COMM_WORLD);

    // Outputting
    if (thread_number == 0) {

        // Bring reconstructed result in canonical form
        auto canonicalFF = my_rational.to_canonical();

        // Restore mass dimension
        for (size_t i = 0; i < canonicalFF.size(); i++) {
            if (dimensions.at(i) >= 0) {
                canonicalFF.at(i).numerator.add_variable(dimensions.at(i), _N);
                canonicalFF.at(i).denominator.add_variable(0, _N);
            }
            else {
                canonicalFF.at(i).numerator.add_variable(0, _N);
                canonicalFF.at(i).denominator.add_variable(size_t(std::abs(dimensions.at(i))), _N);
            }
            canonicalFF.at(i).numerator.variable_names = {"x0", "x1", "x2", "x3", "x4"};
            canonicalFF.at(i).denominator.variable_names = {"x0", "x1", "x2", "x3", "x4"};
        }

        // Attempt to rational guess the result.
        std::function<BigRat(F)> rational_guesser = [cardinality](const F& x) {
            return RationalReconstruction::rational_guess_alternative({BigInt(x), BigInt(cardinality)});
        };

        // Restore mass dimension
        std::vector<Ratio<SparseMultivariatePolynomial<BigRat>>> canonical_guess;
        for (auto& c : canonicalFF) {
          canonical_guess.push_back(fmap(rational_guesser, c));
        }

        _MESSAGE(Color::Code::FG_BLUE,"Testing and storing reconstructed result");

        const auto& integrals = ampl.get_integral_graphs();
        // 2 account for the ep^0 and ep^1 terms
        assert(2*integrals.size() == canonicalFF.size());

        std::string dirguess = "analytics/amplitudes_rational";
        std::string dirff = "analytics/amplitudes_" + std::to_string(cardinality);
        std::string dirinfo = "analytics/integral_info";
        if(system(("mkdir -p " + dirguess).c_str())>0) syscheck++;
        if(system(("mkdir -p " + dirff).c_str())>0) syscheck++;
        if(system(("mkdir -p " + dirinfo).c_str())>0) syscheck++;
        if (syscheck >= 0) _MESSAGE(Color::Code::FG_RED, "Failed creating directories to print results!");

        // perform a check on canonicalFF
        std::vector<F> xsFF;
        for (size_t ii = 0; ii < _N+1; ii++) xsFF.push_back(std::rand());
        auto targetFF = f(xsFF);

        assert(targetFF.size() == canonicalFF.size());
        size_t errorsFF(0);
        for (size_t ii = 0; ii < targetFF.size(); ii++) {
            std::vector<F> xsFFrestricted(xsFF.begin(), xsFF.end() - 1);
            auto resultFF = canonicalFF.at(ii)(xsFF) * alphabet.evaluate_letter(factors[ii], xsFFrestricted);
            if (targetFF[ii] != resultFF) {
                _MESSAGE("reconstructed[", ii, "]: ", resultFF, "   numerics[", ii, "]: ", targetFF[ii]);
                errorsFF++;
            }
        }

        if (errorsFF == 0) {
            _MESSAGE(Color::Code::FG_GREEN, "PASSED check of reconstructed result in the same finite field (with cardinality ", cardinality, ")");
            std::string towrite = dirff + "/1L_" + filename + ".dat";
            std::ofstream file(towrite);
            _MESSAGE(Color::Code::FG_BLUE,"Printing analytic finite-field result to ",towrite);
            for (size_t ii = 0; ii < integrals.size(); ii++) {
              file << integrals[ii].get_readable_graph() << "* ( (" << canonicalFF[2*ii]<<") * (" << factors[2*ii] << ") + ( " << canonicalFF[2*ii+1]<<") * (" << factors[2*ii+1] << ") * ep ) ";
                if (ii + 1 != integrals.size()) file << "+\n";
            }

            write_integral_info(dirinfo, filename, integrals);
        }
        else
            _MESSAGE(Color::Code::FG_RED, "FAILED check of reconstructed result in the same finite field (with cardinality ", cardinality, ") with ", errorsFF,
                     " errors!");


        // more cardinalities in misc/Moduli.hpp
        cardinality = 2147481509;
        cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

        // perform a check on rational reconstruction of the coefficients (canonical_guess)
        std::vector<BigRat> xsRat;
        for (size_t ii = 0; ii < _N+1; ii++) xsRat.push_back(BigRat(std::rand(), std::rand()));
        std::vector<F> xsRatFF;
        for (auto& el : xsRat){ xsRatFF.push_back(F(el)); }
        auto targetRatFF = f(xsRatFF);

        assert(targetRatFF.size() == canonical_guess.size());
        size_t errorsRatFF(0);
        for (size_t ii = 0; ii < targetRatFF.size(); ii++) {
            auto resultRat = canonical_guess.at(ii)(xsRat);
            std::vector<F> xsRatRestricted(xsRat.begin(), xsRat.end() - 1);
            F resultRatFF = F(resultRat) * alphabet.evaluate_letter(factors[ii], xsRatRestricted);
            if (targetRatFF[ii] != resultRatFF) {
                _MESSAGE("reconstructed_rational_guess[", ii, "]: ", resultRatFF, "   numerics[", ii, "]: ", targetRatFF[ii]);
                errorsRatFF++;
            }
        }

        if (errorsRatFF == 0) {
            _MESSAGE(Color::Code::FG_GREEN, "PASSED check of analytic result with rational reconstruction, against a result in a second finite field with cardinality ", cardinality);
            std::string towrite = dirguess + "/1L_" + filename + ".dat";
            std::ofstream file(towrite);
            _MESSAGE(Color::Code::FG_BLUE,"Printing analytic result to ",towrite);
            for (size_t ii = 0; ii < integrals.size(); ii++) {
              file << integrals[ii].get_readable_graph() << "* ( (" << canonical_guess[2*ii]<<") * (" << factors[2*ii] << ") + ( " << canonical_guess[2*ii+1]<<") * (" << factors[2*ii+1] << ") * ep ) ";
                if (ii + 1 != integrals.size()) file << "+\n";
            }
            write_integral_info(dirinfo, filename, integrals);
        }
        else
            _MESSAGE(Color::Code::FG_ORANGE, "Could not rationally reconstruct the analytic result employing a single finite field! Using a second cardinality ", cardinality,
                     " obtained ", errorsRatFF, " errors");
    } // thread_number 0

    _MESSAGE(Color::Code::FG_DEFAULT, "Thread # ", thread_number, " reached end!");

    // let everybody wait to reach this point
    MPI_Barrier(MPI_COMM_WORLD);

    // Finalize the MPI environment
    MPI_Finalize();

    return 0;
}

