#include "catch.hpp"
#include "Core/BigRat.h"

#include "PoleStructure/PoleStructure.h"

using namespace Caravel;
using namespace Caravel::PoleStructure;

TEST_CASE("Amplitude Pole Structure", "[Catani][Exponential]"){

  auto i1 = cataniI1Operator<BigRat,C>(2,0,std::vector<cataniGtype>{5,cataniGtype::gg});
  Integrals::StdBasisFunction<C,C> constant_function([](std::vector<C> in){return C(1);}, "T(1)", {});
  auto constant_coeff = i1.get_coeff(constant_function);
  CHECK(constant_coeff[-2] == BigRat(-5));
  CHECK(constant_coeff[-1] == BigRat(-55)/BigRat(6));
}
