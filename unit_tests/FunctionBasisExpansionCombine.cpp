#include "catch.hpp"

#include <vector>
#include <string>
#include <cmath>

#include "IntegralLibrary/FunctionBasisExpansion.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "Core/Series.h"

using namespace Caravel;
using namespace Caravel::Integrals;

TEST_CASE("EvaluateCombination", "[integrals][basis][expansion][combine]"){
    CHECK(true);

	typedef R In;                                             	// The input type
    typedef C Out;                                            	// The output type
    typedef StdBasisFunction<Out, In> BaseFunc;                 // The basis function type.
    typedef FBCoefficient<Series<Out>, In> BaseCoeff;           // The basis coefficient type.
    typedef FunctionBasisExpansion<BaseCoeff, BaseFunc> FBE;    // The type of the basis.

	// Create the first Basis:
	std::vector<std::string> names1 {"s23", "s12"};
	FBE fbe1;

	BaseFunc f1_1([](const std::vector<In>& args){ return std::log(args[0]); }, "log(args[0])", names1);
	BaseFunc f1_2([](const std::vector<In>& args){ return M_PI; }, "Pi", names1);

	BaseCoeff c1_1([](const std::vector<In>& args){ return Series<Out>(-2, -1, Out(0), Out(args[1]));  });
	BaseCoeff c1_2([](const std::vector<In>& args){ return Series<Out>(-2, -1, Out(args[1]), Out(0)); });

	fbe1.add_basis_element(f1_1, c1_1);
	fbe1.add_basis_element(f1_2, c1_2);

	auto res1 = fbe1.eval_complete({2.3, 1.2});
	CHECK(res1[-1].real() == Approx(0.9994909475221246));
	CHECK(res1[-2].real() == Approx(3.7699111843077517));
	CHECK(fbe1.n_basis_funcs() == 2);

	// Create the second basis
	std::vector<std::string> names2 {"s34", "s23"};
    FBE fbe2;

    BaseFunc f2_1([](const std::vector<In>& args){ return std::log(args[1]); }, "log(args[1])", names2);
    BaseFunc f2_2([](const std::vector<In>& args){ return std::log(args[0]); }, "log(args[0])", names2);

    BaseCoeff c2_1([](const std::vector<In>& args){ return Series<Out>(-2, -1, Out(args[0]), Out(0));  });
    BaseCoeff c2_2([](const std::vector<In>& args){ return Series<Out>(-2, -1, Out(args[1]), Out(0)); });

    fbe2.add_basis_element(f2_1, c2_1);
    fbe2.add_basis_element(f2_2, c2_2);

    auto res2 = fbe2.eval_complete({3.4, 2.3});
    CHECK(res2[-1].real() == Approx(0));
    CHECK(res2[-2].real() == Approx(5.646574510710219));
    CHECK(fbe2.n_basis_funcs() == 2);

	// Now extract the NumericFunctionBasisExpansions and combine them
	auto amp = fbe1.eval_coeffs({2.3, 1.2}) + fbe2.eval_coeffs({3.4, 2.3});
    CHECK(amp.n_basis_funcs() == 3);

    // Evaluation of the complete combined numeric function basis expansion: way 1
    auto amp_evaluated = amp({5.1, 1.2, 2.3, 4.5, 3.4}, {"s51", "s12", "s23", "s45", "s34"});
    CHECK(amp_evaluated[-1].real() == Approx(0.9994909475221246));
    CHECK(amp_evaluated[-2].real() == Approx(9.416485695017972));

    // Evaluation of the complete combined numeric function basis expansion: way 2
    Series<Out> amp_eval2(amp.leading(), amp.last());
    for(const auto& elem: amp){
        amp_eval2 += elem.second * elem.first({1.2, 2.3, 3.4}, {"s12", "s23", "s34"});
    }
    CHECK(amp_eval2[-1].real() == Approx(0.9994909475221246));
    CHECK(amp_eval2[-2].real() == Approx(9.416485695017972));
}
