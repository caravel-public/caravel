#include "catch.hpp"

#include "Core/typedefs.h"
#include "misc/DeltaZero.h"

template <typename T> void quick_field_operations() {
    T a, b, c, d, e, f;
    a = T(1234);
    b = T(-5678);

    // c = a+b;
    c = T(-4444);

    REQUIRE(a + b == c);

    // d = a-b;
    d = T(6912);

    REQUIRE(a - b == d);

    // e = a*b;
    e = T(-7006652);

    REQUIRE(a * b == e);

    b = T(-617);

    // f = a/b;
    f = T(-2);

    REQUIRE(a / b == f);
};

template <typename T> void quick_floating_point_operations() {
    using Caravel::DeltaZero;
    T a, b, c, d, e, f;
    a = T(1234);
    b = T(-5678);

    // c = a+b;
    c = T(-4444);

    REQUIRE((((a + b) - c < DeltaZero<T>()) && ((a + b) - c > -DeltaZero<T>())));

    // d = a-b;
    d = T(6912);

    REQUIRE((((a - b) - d < DeltaZero<T>()) && ((a - b) - d > -DeltaZero<T>())));

    // e = a*b;
    e = T(-7006652);

    REQUIRE((((a * b) - e < DeltaZero<T>()) && ((a * b) - e > -DeltaZero<T>())));

    b = T(-617);

    // f = a/b;
    f = T(-2);

    REQUIRE((((a / b) - f < DeltaZero<T>()) && ((a / b) - f > -DeltaZero<T>())));
};

TEST_CASE("BigInt operations", "[BigInt][ArithmeticOperations]"){
    quick_field_operations<Caravel::BigInt>();
}

TEST_CASE("BigRat operations", "[BigRat][ArithmeticOperations]"){
    quick_field_operations<Caravel::BigRat>();
}

TEST_CASE("F32 operations", "[F32][ArithmeticOperations]"){
    quick_field_operations<Caravel::F32>();
}

TEST_CASE("R operations", "[R][ArithmeticOperations]"){
    quick_floating_point_operations<Caravel::R>();
}

TEST_CASE("RGMP 200 operations", "[RGMP200][ArithmeticOperations]"){
    // some 60 base 10 digits (2^200)
    RGMP::set_precision(200);
    quick_floating_point_operations<RGMP>();
}

TEST_CASE("RGMP 1000 operations", "[RGMP1000][ArithmeticOperations]"){
    // some 300 base 10 digits (2^1000)
    RGMP::set_precision(1000);
    quick_floating_point_operations<RGMP>();
}

