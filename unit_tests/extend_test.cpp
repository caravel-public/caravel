#include "catch.hpp"

#include "PhaseSpace/PhaseSpace.h"
#include "Core/extend.h"
#include "Core/typedefs.h"

#include "compare_tools.hpp"

using namespace Caravel;
using namespace tests;

namespace tests{
    template <class T1, class T2> bool float_equal(const T1& a, const T2& b){
        return is_zero(a-b);
    }
}

#define REQUIRE_FL(X,Y)\
    REQUIRE(tests::float_equal((X),(Y)))

#ifdef HIGH_PRECISION
TEST_CASE("Extend phase space to HP", "[Extend]"){

    PhaseSpace<R> tph(10,1.);
    tph.set_reset_seed(false);
    srand(1110);
    tph.generate();

    auto moms = tph.mom_double();
    auto momconf_process = to_momDconf<R,4>(moms);
    auto momext = change_precision<CHP>(momconf_process);

    momD<CHP,4> sum{};
    for(const auto& it: momext){
        sum += it;
        REQUIRE(is_zero(it*it));
    }

    REQUIRE(is_zero(sum));

}
#endif

#ifdef VERY_HIGH_PRECISION
TEST_CASE("Extend phase space to VHP", "[Extend]"){

    PhaseSpace<R> tph(10,1.);
    tph.set_reset_seed(false);
    srand(3110);
    tph.generate();

    auto moms = tph.mom_double();
    auto momconf_process = to_momDconf<R,4>(moms);
    auto momext = change_precision<CVHP>(momconf_process);

    momD<CVHP,4> sum{};
    for(const auto& it: momext){
        sum += it;
        REQUIRE(is_zero(it*it));
    }

    REQUIRE(is_zero(sum));

}
#endif
