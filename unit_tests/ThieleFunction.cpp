/**
 * Example for unit testing with Catch 2.
 *
 * For more information take a look at:
 * https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#top
 */
#include "catch.hpp"

#include <sstream>
#include <random>
#include "FunctionalReconstruction/ThieleFunction.h"
#include "FunctionalReconstruction/ParallelReconstruction.h"
#include "AmplitudeCoefficients/NumericalTools.h"
using namespace Caravel;

TEST_CASE("Vector Univariate Rational Function", "[UnivariateRational][Vector]"){
    using F = F32;

    std::function<F()> x_gen;

    std::mt19937 mt(rand());
    x_gen = [mt]() mutable {return random_scaled_number(F(1), mt);};

    SECTION("Two component function"){
      std::function<std::vector<F>(F)> f = [](F x){
	    F numa = x + F(9)*x*x;
	    F numb = x + F(4)*x*x;
	    F den = F(1) + F(7)*x + x*x + x*x*x + x*x*x*x;
    return std::vector<F>{numa/den, numb/den};
	};

	VectorThieleFunction<F> my_rational(f, x_gen);

	auto canonical = my_rational.to_canonical();

	std::stringstream buffer; buffer << canonical.at(0);
	REQUIRE(buffer.str() == std::string("(1*x^1 + 9*x^2)/(1 + 7*x^1 + 1*x^2 + 1*x^3 + 1*x^4)"));
    }

    SECTION("One component parallel"){
      std::function<std::vector<F>(F)> f = [](F x){
	    F numa = x + F(9)*x*x;
	    //F numb = x + F(4)*x*x;
	    F den = F(1) + F(7)*x + x*x + x*x*x + x*x*x*x;
	    return std::vector<F>{numa/den};
	};

	VectorThieleFunction<F> my_rational(x_gen);

        Reconstruction::parallel_reconstruct(my_rational, f);

        

	auto canonical = my_rational.to_canonical();

	std::stringstream buffer; buffer << canonical.at(0);
	REQUIRE(buffer.str() == std::string("(1*x^1 + 9*x^2)/(1 + 7*x^1 + 1*x^2 + 1*x^3 + 1*x^4)"));
    }

}
