#include "catch.hpp"

#include "IntegralLibrary/AnalyticallyContinuedInts.h"
#include "IntegralLibrary/integral_collection/2_loop/Masters2LGoncharovs/DoubleBox.h"
#include "IntegralLibrary/FunctionBasisExpansion.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "Core/typedefs.h"

#include <iostream>

using namespace Caravel;
using namespace Caravel::Integrals;
using namespace Caravel::Integrals::Masters2LGoncharovs;
using namespace std;


TEST_CASE("real", "[analytically continued real]"){
    typedef FBCoefficient<Series<C>, R> CType;
    typedef StdBasisFunction<C, R> FType;

    // Create the region chooser for the BubbleBox which is only known in the
    // euclidean region for inputs s and x=t/s.
    auto region_chooser = [](const std::vector<R>& args){
        if(args[0] < R(0) && args[1] < R(0)){
            return 0;
        }
        else {
            return 1;
        }
    };

    RegionSpecifier<R> region_specifier(region_chooser);

    // Create the analytically continued integral which only contains the
    // basis for the euclidean region.
    AnalyticCntndIntegral<CType, FType> my_int({DoubleBox<CType, FType>({"s", "t"}).get_basis()}, region_specifier);

    // Evaluate in the correct region.
    auto result = my_int.eval_complete({ R(-1.213), R(-3.563) });

    // CHECK real parts
    if (abs(result[-4].real()) < R(3e-14)){
        CHECK(abs(result[-4].real() - R(-0.762996537719900178)) < R(3e-14));
    }
    else {
        CHECK(abs(result[-4].real() - R(-0.762996537719900178)) / R(-0.762996537719900178) < R(3e-14));
    }
    if (abs(result[-3].real()) < R(3e-14)){
        CHECK(abs(result[-3].real() - R(1.32233104921823181)) < R(3e-14));
    }
    else {
        CHECK(abs(result[-3].real() - R(1.32233104921823181)) / R(1.32233104921823181) < R(3e-14));
    }
    if (abs(result[-2].real()) < R(3e-14)){
        CHECK(abs(result[-2].real() - R(3.80984253336566808)) < R(3e-14));
    }
    else {
        CHECK(abs(result[-2].real() - R(3.80984253336566808)) / R(3.80984253336566808) < R(3e-14));
    }
    if (abs(result[-1].real()) < R(3e-14)){
        CHECK(abs(result[-1].real() - R(-1.95272847759073439)) < R(3e-14));
    }
    else {
        CHECK(abs(result[-1].real() - R(-1.95272847759073439)) / R(-1.95272847759073439) < R(3e-14));
    }
    if (abs(result[0].real()) < R(3e-14)){
        CHECK(abs(result[0].real() - R(-14.8394005601262564)) < R(3e-14));
    }
    else {
        CHECK(abs(result[0].real() - R(-14.8394005601262564)) / R(-14.8394005601262564) < R(3e-14));
    }

}
