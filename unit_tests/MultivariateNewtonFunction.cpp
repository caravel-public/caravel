/**
 * Example for unit testing with Catch 2.
 *
 * For more information take a look at:
 * https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#top
 */
#include "catch.hpp"

#include <sstream>
#include <random>

#include "FunctionalReconstruction/MultivariateNewtonFunction.h"
#include "AmplitudeCoefficients/NumericalTools.h"
#include "Core/typedefs.h"
using namespace Caravel;

TEST_CASE("Scalar Multivariate Polynomial Functions", "[MultivariatePolynomial][Scalar]"){
    using F = F32;

    std::array<std::function<F()>,1> x_gens_1;
    std::array<std::function<F()>,2> x_gens_2;
    std::array<std::function<F()>,3> x_gens_3;

    srand(7);

    std::mt19937 mt1(rand()); std::mt19937 mt2(rand());
    std::mt19937 mt3(rand()); std::mt19937 mt4(rand());
    x_gens_3[0] = [mt1]() mutable {return random_scaled_number(F(1), mt1);};
    x_gens_3[1] = [mt2]() mutable {return random_scaled_number(F(1), mt2);};
    x_gens_3[2] = [mt3]() mutable {return random_scaled_number(F(1), mt3);};
    
    x_gens_2[0] = x_gens_3[0];
    x_gens_2[1] = x_gens_3[1];

    x_gens_1[0] = x_gens_3[0];

    SECTION("Univariate"){
	std::function<F(std::array<F,1>)> f = [](std::array<F,1> xs){
	    F val = prod_pow(xs[0],3) + F(3)*xs[0] + F(2);
	    return val;
	};

	MultivariateNewtonFunction<F,1> my_polynomial(f, x_gens_1);
	REQUIRE(my_polynomial({F(3)}) == F(38));

	MultivariateNewtonFunction<F,1> my_polynomial_max_degree(f, x_gens_1, 3);

	REQUIRE(my_polynomial_max_degree({F(3)}) == F(38));
    }

    SECTION("Two-variable"){
	std::function<F(std::array<F,2>)> f = [](std::array<F,2> xs){
	  F val = xs[0]*xs[1] + xs[1]*xs[1];
	  return val;
	};

	MultivariateNewtonFunction<F,2> my_polynomial(f, x_gens_2);
	REQUIRE(my_polynomial({F(3),F(7)}) == F(70));

	MultivariateNewtonFunction<F,2> my_polynomial_max_degree(f, x_gens_2, 2);
	REQUIRE(my_polynomial_max_degree({F(3),F(7)}) == F(70));
    }

    SECTION("Guess points one variable"){
	std::function<F(std::array<F,1>)> f = [](std::array<F,1> xs){
	  F val = xs[0]*xs[0] + F(2)*xs[0];
	  return val;
	};


	MultivariateNewtonFunction<F,1> poly(x_gens_1);

        while (!poly.finished){
        	// Precompute a guessed set of points (at most 10)
        	auto potential_points = poly.guess_points(1);

		std::vector<F> potential_fs;
		for (auto& x : potential_points){
		  potential_fs.push_back(f(x));
		}

        	for (size_t i = 0; i < potential_points.size(); i++){
		    auto new_x = poly.guess_points(1).at(0);
        	    // Only add if the guess was correct.
        	    if (new_x == potential_points.at(i)){
		        poly.next_point(); // Move the counter forward
        	        poly.add_point(potential_fs.at(i), new_x); 
        		if (poly.finished){break;}
        	    }
        	    else{
		      break;
		    }
        	}
        }
	

	REQUIRE(poly({F(3)}) == F(15));
    }

    SECTION("Guess points two variable"){
	std::function<F(std::array<F,2>)> f = [](std::array<F,2> xs){
	  F val = xs[0]*xs[1] + xs[1]*xs[1];
	  return val;
	};


	MultivariateNewtonFunction<F,2> poly(x_gens_2,2);

        while (!poly.finished){
        	// Precompute a guessed set of points (at most 10)
        	auto potential_points = poly.guess_points(1);

		std::vector<F> potential_fs;
		for (auto& x : potential_points){
		  potential_fs.push_back(f(x));
		}

        	for (size_t i = 0; i < potential_points.size(); i++){
		    auto new_x = poly.guess_points(1).at(0);
        	    // Only add if the guess was correct.
        	    if (new_x == potential_points.at(i)){
		        poly.next_point(); // Move the counter forward
        	        poly.add_point(potential_fs.at(i), new_x); 
        		if (poly.finished){break;}
        	    }
        	    else{
		      break;
		    }
        	}
        }

	REQUIRE(poly({F(3),F(7)}) == F(70));
    }

    SECTION("Three variable"){
	std::function<F(std::array<F,3>)> f = [](std::array<F,3> xs){
	    F val = prod_pow(xs[0],3)*xs[1] + F(3)*xs[1] + F(2) - xs[2]*xs[1];
	    return val;
	};

	MultivariateNewtonFunction<F,3> my_polynomial(f, x_gens_3);

	REQUIRE(my_polynomial({F(3),F(7),F(9)}) == F(149));
    }

    SECTION("Factorizable"){
	std::function<F(std::array<F,3>)> f = [](std::array<F,3> xs){
	    F val = prod_pow(xs[0]*xs[1]*xs[2],5);
	    return val;
	};

	MultivariateNewtonFunction<F,3> my_polynomial(f, x_gens_3);

	REQUIRE(my_polynomial({F(1),F(1),F(1)}) == F(1));
    }

    SECTION("Constant function "){
	std::function<F(std::array<F,3>)> f = [](std::array<F,3> xs){
	  return F(1);
	};

	MultivariateNewtonFunction<F,3> my_polynomial(f, x_gens_3);

	REQUIRE(my_polynomial({F(1),F(1),F(1)}) == F(1));
    }


}
