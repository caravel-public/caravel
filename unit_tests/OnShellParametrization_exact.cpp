#include <cstdlib>
#include "misc/CaravelCereal.hpp"

#include "Graph/Graph.h"
#include "Graph/GraphKin.h"
#include "catch.hpp"
#include "compare_tools.hpp"
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/typedefs.h"

#include "OnShellStrategies/OnShellStrategies.h"
#include "OnShellStrategies/LinearAlgebra.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"



#define _DIM 6 

namespace Caravel {

std::pair<std::vector<std::vector<size_t>>, std::vector<std::vector<size_t>>> get_left_right_indices(const lGraph::xGraph& gr) {
    auto momchoice = gr.get_internal_momenta();
    if (momchoice.size() != 2) {
        std::cerr << "Error: recieved " << momchoice.size() << " loop momenta. Expected 2." << std::endl;
        exit(1);
    }

    // TODO: condensate. Maybe introduce get_loop_momentum_strand() method
    size_t ind1 = momchoice[0].get_connection().first;
    size_t ind2 = momchoice[0].get_connection().second;
    size_t ind3 = momchoice[0].get_strand();
    size_t ind4 = momchoice[1].get_connection().first;
    size_t ind5 = momchoice[1].get_connection().second;
    size_t ind6 = momchoice[1].get_strand();
    auto strand1 = gr.get_base_graph().get_connection(ind1, ind2)[ind3];
    auto strand2 = gr.get_base_graph().get_connection(ind4, ind5)[ind6];

    std::vector<std::vector<size_t>> left_momenta;
    std::vector<std::vector<size_t>> right_momenta;

    for (auto& b : strand1.get_beads()) { left_momenta.push_back(b.get_momentum_indices()); }
    for (auto& b : strand2.get_beads()) { right_momenta.push_back(b.get_momentum_indices()); }
    return std::make_pair(left_momenta, right_momenta);
}

template <typename T, size_t D>
void ckeck_transversality(const lGraph::xGraph& gr_in, const std::vector<std::vector<momD<T, D>>>& transverse, const momD_conf<T, D>& mom_conf) {
    //! update to new logic independent of left/right
    std::vector<std::vector<size_t>> left_indices;
    std::vector<std::vector<size_t>> right_indices;
    // get the loop-momenta assignments used for the parametrization to make transversality checks
    auto& gr = gr_in.get_special_routing();
    std::tie(left_indices, right_indices) = get_left_right_indices(gr);

    // get the momenta in the corners
    std::vector<momD<T, D>> l_mom;
    std::vector<momD<T, D>> r_mom;

    for (auto& i_vec : left_indices) { l_mom.push_back(mom_conf.Sum(i_vec)); }
    for (auto& i_vec : right_indices) { r_mom.push_back(mom_conf.Sum(i_vec)); }

    // check that the transversal vectors are orthogonal to the momenta
    for (auto& p : l_mom) {
        for (auto& v : transverse[0]) { CHECK_ZERO(v * p); }
    }
    for (auto& p : r_mom) {
        for (auto& v : transverse[1]) { CHECK_ZERO(v * p); }
    }
}

template <typename T, size_t D>
T evaluate_propagator(const lGraph::MomentumRouting& mom_rout, const momD_conf<T, D>& external, const OnShellPoint<T, D>& l_moms) {

    std::vector<size_t> ext_in_plus(mom_rout.get_external().plus_momenta.begin(), mom_rout.get_external().plus_momenta.end());
    std::vector<size_t> ext_in_minus(mom_rout.get_external().minus_momenta.begin(), mom_rout.get_external().minus_momenta.end());
    std::vector<size_t> int_in_plus(mom_rout.get_internal().plus_momenta.begin(), mom_rout.get_internal().plus_momenta.end());
    std::vector<size_t> int_in_minus(mom_rout.get_internal().minus_momenta.begin(), mom_rout.get_internal().minus_momenta.end());

    momD<T, D> mom_flow;

    mom_flow += external.Sum(ext_in_plus) - external.Sum(ext_in_minus);
    for (auto& i : int_in_plus) { mom_flow += l_moms[i-1]; }
    for (auto& i : int_in_minus) { mom_flow -= l_moms[i-1]; }

    return mom_flow * mom_flow;
}

template <typename T, size_t D> void ckeck_on_shell_strand(const lGraph::Strand& strand, const momD_conf<T, D>& mom_conf, const OnShellPoint<T, D>& l_moms) {
    auto routings = strand.get_routings();
    for (auto& r : routings) {
        // check that all links are on-shell
        if(evaluate_propagator(r, mom_conf, l_moms)!=T(0)){
            std::cout<<"FAILED on-shell condition for propagator: ("<<r<<")^2"<<std::endl;
        }
        CHECK_ZERO(evaluate_propagator(r, mom_conf, l_moms));
    }
}

template <typename T, size_t D>
void ckeck_on_shell_parametrization(const lGraph::xGraph& gr, const momD_conf<T, D>& mom_conf, const OnShellPoint<T, D>& l_moms) {
    _PRINT(gr);
    for (const auto& it : gr.get_base_graph().get_connection_map()) {
        const auto& con = it.second;
        for (const auto& stnd : con) { ckeck_on_shell_strand(stnd, mom_conf, l_moms); }
    }
}

template <typename T, size_t D>
void check_basis(const lGraph::lGraphKin::GraphKin<T,D>& gr_kin) {

    auto dual_bases = gr_kin.get_strand_by_strand_common_dual();
    auto common_transverse = gr_kin.get_common_transverse();

    auto basis_l1=dual_bases[0];
    auto basis_l2=dual_bases[1];

    basis_l1.insert(basis_l1.end(), common_transverse.begin(), common_transverse.end());
    basis_l2.insert(basis_l2.end(), common_transverse.begin(), common_transverse.end());

    // Check that there are D vectors
    CHECK(basis_l1.size()==D);
    CHECK(basis_l2.size()==D);

    auto gram_det_l1 =OnShellStrategies::gram(basis_l1);
    auto gram_det_l2 =OnShellStrategies::gram(basis_l2);

    // Check that the vectors are independent, i.e. form a basis 
    CHECK(gram_det_l1!=0);
    CHECK(gram_det_l2!=0);
}


} // namespace Caravel

using namespace Caravel;

TEST_CASE("Check of the on-shell parametrization in a finite field", "[OSParametrization],[LoopMomentumConvention]") {

    
    using namespace lGraph;
    using namespace lGraphKin;
    using F = F32;
    size_t cardinality(2147483629);

	cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    // non-planar comented for now
    std::vector<xGraph> graphs;
    {
        auto input  = read_serialized<std::vector<std::string>>("./test_data/topos.json");
        for(auto& it : input){
            graphs.emplace_back(MathList{it});
        }
    }

    std::vector<xGraph> graphs_different_convention;
    {
        auto input  = read_serialized<std::vector<std::string>>("./test_data/topos_alt.json");
        for(auto& it : input){
            graphs_different_convention.emplace_back(MathList{it});
        }
    }

    auto physical_point_4 = rational_mom_conf<BigRat>(4, {BigRat(-1, 4), BigRat(-3, 4)});
    auto physical_point_5 = rational_mom_conf<BigRat>(5, {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17), BigRat(-1)});

    momD_conf<F, 6> momconf_process_4 = momD_conf<F, 4>(physical_point_4);
    momD_conf<F, 6> momconf_process_5 = momD_conf<F, 4>(physical_point_5);

    std::vector<xGraph> four_point_graphs;
    std::vector<xGraph> five_point_graphs;

    std::vector<xGraph> four_point_graphs_different_convention;
    std::vector<xGraph> five_point_graphs_different_convention;

    std::vector<GraphKin<F, 6>> four_point_grKin;
    std::vector<GraphKin<F, 6>> five_point_grKin;

    std::vector<std::function<FunctionSpace::TopologyCoordinates<F>(OnShellPoint<F, 6>& )>>  four_point_coordinate_evalutors;
    std::vector<std::function<FunctionSpace::TopologyCoordinates<F>(OnShellPoint<F, 6>& )>>  five_point_coordinate_evalutors;

    // just to avoid spurious 1/0
    EpsilonBasis<F>::squares[0]=F(1);
    EpsilonBasis<F>::squares[1]=F(1);
    
     //topology_coordinates_evaluator_2L(graph, kinematics)
    for (auto& g : graphs) {
        if (g.get_n_legs() == 4) {
            four_point_graphs.push_back(g);
            four_point_grKin.push_back(GraphKin<F, 6>(g, momconf_process_4));
            four_point_coordinate_evalutors.push_back(FunctionSpace::topology_coordinates_evaluator_2L(four_point_graphs.back(),four_point_grKin.back()));
        }
        if (g.get_n_legs() == 5) {
            five_point_graphs.push_back(g);
            five_point_grKin.push_back(GraphKin<F, 6>(g, momconf_process_5));
            five_point_coordinate_evalutors.push_back(FunctionSpace::topology_coordinates_evaluator_2L(five_point_graphs.back(),five_point_grKin.back()));
        }
    }
    
    for (auto& g : graphs_different_convention) {
        if (g.get_n_legs() == 4) {
            four_point_graphs_different_convention.push_back(g);
        }
        if (g.get_n_legs() == 5) {
            five_point_graphs_different_convention.push_back(g);
        }
    }

    SECTION("Check transversal spaces of the strands") {
        for (size_t i = 0; i < four_point_graphs.size(); i++) {
            ckeck_transversality(four_point_graphs[i], four_point_grKin[i].get_transverse(), momconf_process_4);
        }
        for (size_t i = 0; i < five_point_graphs.size(); i++) {
            ckeck_transversality(five_point_graphs[i], five_point_grKin[i].get_transverse(), momconf_process_5);
        }
    }
    SECTION("Check on-shell parametrization") { //! for now just a dummy OS parametrization
        for (size_t i = 0; i < four_point_graphs.size(); i++) {
            auto lgKin = GraphKin<F, 6>(four_point_graphs[i]);
            OnShellMomentumParameterization<F,6> compute_loop_momenta = surd_parameterization( four_point_graphs[i], lgKin );

            // TODO: maybe this should be stored in GraphKin
            auto transverse_dimensions = four_point_graphs[i].n_OS_variables_with_special_routing();

            // sample random variables accordingly
            std::vector<std::vector<F>> os_variables;
            for(auto count:transverse_dimensions){
                os_variables.push_back( std::vector<F>() );
                for(size_t ii=0;ii<count;ii++)
                    os_variables.back().push_back( F(rand()) );
            }

            // update GraphKin
            lgKin.change_point( momconf_process_4 );
            auto momenta = compute_loop_momenta( os_variables );

            ckeck_on_shell_parametrization(four_point_graphs[i], momconf_process_4, std::vector<momD<F, 6>>( momenta ));

            auto conventions_map_loop = OnShellStrategies::OnShellPointMap<F,6>(four_point_graphs[i],four_point_graphs_different_convention[i]);
            auto momenta_different_convention = conventions_map_loop(std::vector<momD<F, 6>>(momenta),momconf_process_4);

            ckeck_on_shell_parametrization(four_point_graphs_different_convention[i], momconf_process_4, momenta_different_convention);

            //_PRINT(four_point_coordinate_evalutors[i](momenta).propagators);
        }
        for (size_t i = 0; i < five_point_graphs.size(); i++) {
            auto lgKin = GraphKin<F, 6>(five_point_graphs[i]);
            OnShellMomentumParameterization<F,6> compute_loop_momenta = surd_parameterization( five_point_graphs[i], lgKin );

            // TODO: maybe this should be stored in GraphKin
            auto transverse_dimensions = five_point_graphs[i].n_OS_variables_with_special_routing();

            // sample random variables accordingly
            std::vector<std::vector<F>> os_variables;
            for(auto count:transverse_dimensions){
                os_variables.push_back( std::vector<F>() );
                for(size_t ii=0;ii<count;ii++)
                    os_variables.back().push_back( F(rand()) );
            }

            // update GraphKin
            lgKin.change_point( momconf_process_5 );
            auto momenta = compute_loop_momenta( os_variables );
            ckeck_on_shell_parametrization(five_point_graphs[i], momconf_process_5, std::vector<momD<F, 6>>( momenta ));

            auto conventions_map_loop = OnShellStrategies::OnShellPointMap<F,6>(five_point_graphs[i],five_point_graphs_different_convention[i]);
            auto momenta_different_convention = conventions_map_loop(std::vector<momD<F, 6>>(momenta),momconf_process_5);

            ckeck_on_shell_parametrization(five_point_graphs_different_convention[i], momconf_process_5, momenta_different_convention);

            //_PRINT(five_point_coordinate_evalutors[i](momenta).propagators);
        }
    }
    SECTION("Check that the vectors in GrKin define a basis") {
        for(auto& grk:four_point_grKin ){
            check_basis(grk);
        }
        for(auto& grk:five_point_grKin ){
            check_basis(grk);
        }
    }
}
