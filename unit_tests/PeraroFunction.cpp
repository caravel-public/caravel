/**
 * Example for unit testing with Catch 2.
 *
 * For more information take a look at:
 * https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#top
 */
#include "catch.hpp"

#include <sstream>
#include <random>
#include "FunctionalReconstruction/PeraroFunction.h"
#include "FunctionalReconstruction/SerialReconstruction.h"
#include "AmplitudeCoefficients/NumericalTools.h"
#include "Core/typedefs.h"
using namespace Caravel;
using namespace Caravel::Reconstruction;

TEST_CASE("Scalar Multivariate Rational Functions", "[MultivariateRational][Scalar]"){

    using F = F32;

    std::array<std::function<F()>,4> x_gens;

    std::mt19937 mt1(rand()); std::mt19937 mt2(rand());
    std::mt19937 mt3(rand()); std::mt19937 mt4(rand());
    x_gens[0] = [mt1]() mutable {return random_scaled_number(F(1), mt1);};
    x_gens[1] = [mt2]() mutable {return random_scaled_number(F(1), mt2);};
    x_gens[2] = [mt3]() mutable {return random_scaled_number(F(1), mt3);};
    x_gens[3] = [mt4]() mutable {return random_scaled_number(F(1), mt4);};

    PeraroFunction<F,4> my_rational(x_gens);

    SECTION("High rank large denominators"){
	std::function<F(std::array<F,4>)> f = [](std::array<F,4> xs){
	    int power = 3;
	    F num = 1;
	    F den = pow(xs[0],power)*pow(xs[1],power)*pow(xs[2],power)*pow(xs[3],power);
	    return num/den;
	};

        auto prepared_f = my_rational.prepare_function(f);
        serial_reconstruct(my_rational, prepared_f);

	auto canonical = my_rational. to_canonical();

	std::stringstream buffer; buffer << canonical;
	REQUIRE(buffer.str() == std::string("(1)/(x0^3*x1^3*x2^3*x3^3)"));
    }

    // Checks that the variables get named correctly.
    SECTION("Asymmetric function"){
	std::function<F(std::array<F,4>)> f = [](std::array<F,4> xs){
	    F num = 1;
	    F den = pow(xs[0],5)*pow(xs[1],4)*pow(xs[2],3)*pow(xs[3],2);
	    return num/den;
	};

        auto prepared_f = my_rational.prepare_function(f);
        serial_reconstruct(my_rational, prepared_f);

	auto canonical = my_rational. to_canonical();

	std::stringstream buffer; buffer << canonical;
	REQUIRE(buffer.str() == std::string("(1)/(x0^5*x1^4*x2^3*x3^2)"));
    }

    SECTION("0 Function"){
	std::function<F(std::array<F,4>)> f = [](std::array<F,4> xs){
	  return F(0);
	};

        auto prepared_f = my_rational.prepare_function(f);
        serial_reconstruct(my_rational, prepared_f);

	auto canonical = my_rational.to_canonical();

	std::stringstream buffer; buffer << canonical;
	REQUIRE(buffer.str() == std::string("(0)/(1)"));
    }

    SECTION("Non-trivial numerator"){
	std::function<F(std::array<F,4>)> f = [](std::array<F,4> xs){
	    F num = xs[0]*xs[0];
	    F den = (F(1) + xs[1] + xs[0]*xs[1]);
	    return num/den;
	};

        auto prepared_f = my_rational.prepare_function(f);
        serial_reconstruct(my_rational, prepared_f);

	auto canonical = my_rational. to_canonical();

	std::stringstream buffer; buffer << canonical;
	REQUIRE(buffer.str() == std::string("(x0^2)/(1 + x1 + x0*x1)"));
    }
}

TEST_CASE("Vector Multivariate Rational Functions", "[MultivariateRational][Vector]"){
    using F = F32;

    std::array<std::function<F()>,4> x_gens;

    std::mt19937 mt1(rand()); std::mt19937 mt2(rand());
    std::mt19937 mt3(rand()); std::mt19937 mt4(rand());
    x_gens[0] = [mt1]() mutable {return random_scaled_number(F(1), mt1);};
    x_gens[1] = [mt2]() mutable {return random_scaled_number(F(1), mt2);};
    x_gens[2] = [mt3]() mutable {return random_scaled_number(F(1), mt3);};
    x_gens[3] = [mt4]() mutable {return random_scaled_number(F(1), mt4);};

    VectorPeraroFunction<F,4> my_rational(x_gens);

    // Checks that the variables get named correctly.
    SECTION("Asymmetric function"){
      std::function<std::vector<F>(std::array<F,4>)> f = [](std::array<F,4> xs){
	    F num = 1;
	    F denA = pow(xs[0],1)*pow(xs[1],1)*pow(xs[2],1);
	    F denB = pow(xs[2],3);

	    return std::vector<F> {num/denA, num/denB};
	};

        auto prepared_f = my_rational.prepare_function(f);
        serial_reconstruct(my_rational, prepared_f);

	auto canonicals = my_rational.to_canonical();

	std::stringstream buffer;
        buffer << canonicals.at(0);
	REQUIRE(buffer.str() == std::string("(1)/(x0*x1*x2)"));
        buffer.str("");
        buffer << canonicals.at(1);
	REQUIRE(buffer.str() == std::string("(1)/(x2^3)"));
    }

    SECTION("Parallelized Asymmetric Function"){
      std::function<std::vector<F>(std::array<F,4>)> f = [](std::array<F,4> xs){
	    F num = 1;
	    F denA = pow(xs[0],1)*pow(xs[1],1)*pow(xs[2],1);
	    F denB = pow(xs[2],3);

	    return std::vector<F> {num/denA, num/denB};
	};

        auto prepared_f = my_rational.prepare_function(f);
        Reconstruction::parallel_reconstruct(my_rational, prepared_f);

	auto canonicals = my_rational.to_canonical();

	std::stringstream buffer;
        buffer << canonicals.at(0);
	REQUIRE(buffer.str() == std::string("(1)/(x0*x1*x2)"));
        buffer.str("");
        buffer << canonicals.at(1);
	REQUIRE(buffer.str() == std::string("(1)/(x2^3)"));
    }

    SECTION("0 Function"){
      std::function<std::vector<F>(std::array<F,4>)> f = [](std::array<F,4> xs){
          return std::vector<F> {F(0), F(0), F(0)};
        };

        auto prepared_f = my_rational.prepare_function(f);
        serial_reconstruct(my_rational, prepared_f);

        auto canonicals = my_rational.to_canonical();

        std::stringstream buffer; buffer << canonicals.at(0);
        REQUIRE(buffer.str() == std::string("(0)/(1)"));
    }

}
