#include "catch.hpp"
#include "IntegralLibrary/integral_collection/DummyIntegral.h"

#include "Core/typedefs.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"


using namespace Caravel;
using namespace Caravel::Integrals;

template <typename Out, typename In>
using Coeff = FBCoefficient<Series<Out>, In>;

template <typename Out, typename In>
using Func = StdBasisFunction<Out, In>;



TEST_CASE("DummyIntegral", "[Integral][Dummy][DummyIntegral]"){
    DummyIntegral<Coeff<C, R>, Func<C, R>> dummy(-4, 0);

    CHECK(dummy.get_leading_eps() == -4);
    CHECK(dummy.get_last_eps() == 0);

    auto res = dummy.get_basis().eval_complete({});
    CHECK(res[-4] == C(0));
    CHECK(res[-3] == C(0));
    CHECK(res[-2] == C(0));
    CHECK(res[-1] == C(0));
    CHECK(res[0] == C(0));

    auto coeffs = dummy.get_basis().eval_coeffs({});
    CHECK(coeffs.n_basis_funcs() == 1);
    CHECK(coeffs.begin()->second[-4] == C(0));
}
