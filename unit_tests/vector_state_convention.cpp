#include "Core/Utilities.h"
#include "Core/momD_conf.h"
#include "Core/spinor/clifford_algebra.h"
#include "Core/typedefs.h"
#include "Forest/Contact/States_Vector_Boson.hpp"
#include "catch.hpp"
#include "compare_tools.hpp"

using namespace Caravel;
using namespace clifford_algebra;
using namespace tests;

TEST_CASE("Match conventions [pos. Energy]", "[FLOAT]") {
    using T = C;
    momD<T, 4> p = momD<T, 4>(T(2.53439217188832259e-01, 0.00000000000000000e+00), T(-2.26174796178327132e-01, 0.00000000000000000e+00),
                              T(1.13941843400268384e-01, 0.00000000000000000e+00), T(-9.67753612904563091e-03, 0.00000000000000000e+00));
    momD<T, 4> n = p.get_parity_conjugated();

    // Caravels polarization vectors ( without 1/sqrt(2) )
    std::vector<T> ep_p = externalWF_vectorboson_p_momentum_default<T, T, 4>(p);
    std::vector<T> ep_m = externalWF_vectorboson_m_momentum_default<T, T, 4>(p);

    // Polarization vectors from hep-ph/9601359 ( without 1/sqrt(2) )
    std::vector<T> ep_p_dixon = (T(2) * LvAB(n, p) / spaa(n, p)).get_vector();
    std::vector<T> ep_m_dixon = (-T(2) * LvBA(n, p) / spbb(n, p)).get_vector();

    // Compare components
    for (size_t ii = 0; ii < 4; ii++) {
        REQUIRE(is_zero(ep_p[ii] - ep_p_dixon[ii]));
        REQUIRE(is_zero(ep_m[ii] - ep_m_dixon[ii]));
    }
}

TEST_CASE("Match conventions [pos. Energy, non-std aux vector]", "[FLOAT]") {
    using T = C;
    momD<T, 4> p = momD<T, 4>(T(219.0131633959449, 0), T(198.7470754232639, 0), T(-91.19060495509737, 0), T(12.27352106454014, 0));
    momD<T, 4> n = momD<T, 4>(T(427.4136260272104, 0), T(-364.0712943105093, 0), T(-219.9342023360971, 0), T(-41.99341633617514, 0));

    // Caravels polarization vectors ( without 1/sqrt(2) )
    std::vector<T> ep_p = externalWF_vectorboson_p_momenta<T, T, 4>(p,n);
    std::vector<T> ep_m = externalWF_vectorboson_m_momenta<T, T, 4>(p,n);

    // Polarization vectors from hep-ph/9601359 ( without 1/sqrt(2) )
    std::vector<T> ep_p_dixon = (T(2) * LvAB(n, p) / spaa(n, p)).get_vector();
    std::vector<T> ep_m_dixon = (-T(2) * LvBA(n, p) / spbb(n, p)).get_vector();

    // Compare components
    for (size_t ii = 0; ii < 4; ii++) {
        REQUIRE(is_zero(ep_p[ii] - ep_p_dixon[ii]));
        REQUIRE(is_zero(ep_m[ii] - ep_m_dixon[ii]));
    }
}

TEST_CASE("Match conventions [neg. Energy]", "[FLOAT]") {
    using T = C;
    momD<T, 4> p = momD<T, 4>(T(-4.99999999999999889e-01, 0.00000000000000000e+00), T(4.22627660143561745e-01, 0.00000000000000000e+00),
                              T(2.28978938890502130e-01, 0.00000000000000000e+00), T(-1.37675366083252713e-01, 0.00000000000000000e+00));
    momD<T, 4> n = p.get_parity_conjugated();

    // Caravels polarization vectors ( without 1/sqrt(2) )
    std::vector<T> ep_p = externalWF_vectorboson_p_momentum_default<T, T, 4>(p);
    std::vector<T> ep_m = externalWF_vectorboson_m_momentum_default<T, T, 4>(p);

    // Polarization vectors from hep-ph/9601359 ( without 1/sqrt(2) )
    std::vector<T> ep_p_dixon = (T(2) * LvAB(n, p) / spaa(n, p)).get_vector();
    std::vector<T> ep_m_dixon = (-T(2) * LvBA(n, p) / spbb(n, p)).get_vector();

    // Compare components
    for (size_t ii = 0; ii < 4; ii++) {
        REQUIRE(is_zero(ep_p[ii] - ep_p_dixon[ii]));
        REQUIRE(is_zero(ep_m[ii] - ep_m_dixon[ii]));
    }
}

TEST_CASE("Match conventions [neg. Energy, non-std aux vector]", "[FLOAT]") {
    using T = C;
    momD<T, 4> p = momD<T, 4>(T(-219.0131633959449, 0), T(-198.7470754232639, 0), T(91.19060495509737, 0), T(-12.27352106454014, 0));
    momD<T, 4> n = momD<T, 4>(T(427.4136260272104, 0), T(-364.0712943105093, 0), T(-219.9342023360971, 0), T(-41.99341633617514, 0));

    // Caravels polarization vectors ( without 1/sqrt(2) )
    std::vector<T> ep_p = externalWF_vectorboson_p_momenta<T, T, 4>(p,n);
    std::vector<T> ep_m = externalWF_vectorboson_m_momenta<T, T, 4>(p,n);

    // Polarization vectors from hep-ph/9601359 ( without 1/sqrt(2) )
    std::vector<T> ep_p_dixon = (T(2) * LvAB(n, p) / spaa(n, p)).get_vector();
    std::vector<T> ep_m_dixon = (-T(2) * LvBA(n, p) / spbb(n, p)).get_vector();

    // Compare components
    for (size_t ii = 0; ii < 4; ii++) {
        REQUIRE(is_zero(ep_p[ii] - ep_p_dixon[ii]));
        REQUIRE(is_zero(ep_m[ii] - ep_m_dixon[ii]));
    }
}

#ifdef USE_FINITE_FIELDS
TEST_CASE("Match conventions_F32", "[F32]") {
    using T = F32;
    momD<T, 4> p = momD<T, 4>(T(4), T(-4), T(-2), T(2));
    momD<T, 4> n = p.get_parity_conjugated();

    // Caravels polarization vectors ( without 1/sqrt(2) )
    std::vector<T> ep_p = externalWF_vectorboson_p_momentum_default<T, T, 4>(p);
    std::vector<T> ep_m = externalWF_vectorboson_m_momentum_default<T, T, 4>(p);

    // Polarization vectors from hep-ph/9601359 ( without 1/sqrt(2) )
    std::vector<T> ep_p_dixon = (T(2) * LvAB(n, p) / spaa(n, p)).get_vector();
    std::vector<T> ep_m_dixon = (-T(2) * LvBA(n, p) / spbb(n, p)).get_vector();

    // Compare components
    for (size_t ii = 0; ii < 4; ii++) {
        REQUIRE(is_zero(ep_p[ii] - ep_p_dixon[ii]));
        REQUIRE(is_zero(ep_m[ii] - ep_m_dixon[ii]));
    }
}

TEST_CASE("Match conventions_F32 [non-std aux-vector]", "[F32]") {
    using T = F32;
    momD<T, 4> p = momD<T, 4>(T(4), T(-4), T(-2), T(2));
    momD<T, 4> n = momD<T, 4>(T(-4) / T(3), T(1) / T(16), T(1) / T(16), T(-4) / T(3));

    // Caravels polarization vectors ( without 1/sqrt(2) )
    std::vector<T> ep_p = externalWF_vectorboson_p_momenta<T, T, 4>(p,n);
    std::vector<T> ep_m = externalWF_vectorboson_m_momenta<T, T, 4>(p,n);

    // Polarization vectors from hep-ph/9601359 ( without 1/sqrt(2) )
    std::vector<T> ep_p_dixon = (T(2) * LvAB(n, p) / spaa(n, p)).get_vector();
    std::vector<T> ep_m_dixon = (-T(2) * LvBA(n, p) / spbb(n, p)).get_vector();

    // Compare components
    for (size_t ii = 0; ii < 4; ii++) {
        REQUIRE(is_zero(ep_p[ii] - ep_p_dixon[ii]));
        REQUIRE(is_zero(ep_m[ii] - ep_m_dixon[ii]));
    }
}
#endif
