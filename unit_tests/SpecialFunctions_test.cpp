#include "catch.hpp"

#include <chrono>
using namespace std::chrono;

#include "Core/typedefs.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/SpecialFunctions/access_special_functions.h"

using namespace Caravel;
using namespace Caravel::Integrals;

TEST_CASE("Goncharov"){

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
    unsigned int oldcw;
    fpu_fix_start(&oldcw);
#endif

    // Compare against ginsh
    C res1 = G<C>(R(1.1), R(3.3));
    C com1 = C(0.6931471805599453093, 3.1415926535897932385);
    CHECK(abs(res1 - com1) < R(1e-15));
    C res2 = G<C>(R(3.0), R(2.0));
    C com2 = C(-1.0986122886681096916, 0);
    CHECK(abs(res2 - com2) < R(1e-15));
    C res3 = G<C>(R(3.0), R(2.0), R(4.0));
    C com3 = C(-5.9654569339333380186,-2.1775860903036021302);
    CHECK(abs(res3 - com3) < R(1e-15));
    C res4 = G<C>(R(3.0), R(3.3), R(4.4), R(5.5));
    C com4 = C(1.9931074416989289432,-3.4359144266591351924);
    CHECK(abs(res4 - com4) < R(1e-15));
    C res5 = G<C>(R(3.0), R(3.3), R(4.4), R(5.5), R(5.5));
    C com5 = C(0.9603855031338974194, 1.8945966962078643099);
    CHECK(abs(res5 - com5) < R(1e-15));
    C res6 = G<C>(R(3.0), R(3.3), R(4.4), R(5.5), R(5.5), R(5.5));
    C com6 = C(-0.93396124253799061405, -0.25549637527687103967);
    CHECK(abs(res6 - com6) < R(1e-15));
    C res7 = G<C>(R(3.0), R(3.3), R(4.4), R(5.5), R(5.5), R(5.5), R(5.5));
    C com7 = C(0.35336390070172646083,-0.16791348731688111545);
    CHECK(abs(res7 - com7) < R(1e-15));

#ifdef VERY_HIGH_PRECISION
    // auto start = std::chrono::high_resolution_clock::now();
    long long nmax = 1e+2;

    for (long long  i = 0; i < nmax; ++i){

        // Go higher precision
        CVHP resh1 = G<CVHP>(RVHP("1.1"), RVHP("3.3"));
        CVHP comh1 = CVHP("0.69314718055994530941723212145817656807550013436025525412068000949339362196968", "3.1415926535897932384626433832795028841971693993751058209749445923078164062862");
        CHECK(abs(resh1 - comh1) < RVHP("1e-63"));

        CHP resh3 = G<CHP>(RHP("1.1"), RHP("3.3"));
        CHP comh3 = CHP("0.69314718055994530941723212145817656807550013436025525412068000949339362196968", "3.1415926535897932384626433832795028841971693993751058209749445923078164062862");
        CHECK(abs(resh3 - comh3) < RHP("1e-31"));

        CVHP resh2 = G<CVHP>(RVHP("3"), RVHP("2"), RVHP("4"));
        CVHP comh2 = CVHP("-5.965456933933338017762966226013529997235318119775720867142768107300017827537", "-2.17758609030360213050068889823761394733858370036928629432579525319430854917674");
        CHECK(abs(resh2 - comh2) < RHP("5e-63"));
    }
#endif

    // auto finish = std::chrono::high_resolution_clock::now();
    // auto elapsed = duration_cast<seconds>(finish - start).count();

}

TEST_CASE("Goncharov Shuffle", "[special][goncharov][shuffle]"){
    CHECK(abs(G<C>(-1., 0., -2.85338) - C(-6.86965232053329977,1.93839722206186815)) < 1e-14);
    CHECK(abs(G<C>(-1., 0., 0., -2.85338) - C(-3.86354807016942381,-6.0785109227535834)) < 1e-14);
    CHECK(abs(G<C>(-1., -1., 0., 0., -2.85338) - C(6.40844793190375217,-1.46980792865503451)) < 1e-14);
    CHECK(abs(G<C>(-1., 0., 0., 0., -7.40429) - C(-5.81779937194144114,-7.61981674298030942)) < 1e-14);
    CHECK(abs(G<C>(0., -1., 0., 0., -7.40429)- C(1.06932945987744787,-4.40079590198045167)) < 1e-14);

}

TEST_CASE("PolyLog") {
    C res1 = PolyLog<C>(2, R(1.));
    CHECK(res1.real() == Approx(1.64493406684822641).epsilon(1e-16));
}

TEST_CASE("Zeta") {
    C res1 = zeta<C>(2);
    CHECK(res1.real() == Approx(1.6449340668482264366));
}

// TEST_CASE("Pi"){
//     cout << Pi<R> << endl;
//     cout << Pi<RHP>.to_string() << endl;
//     cout << Pi<RVHP>.to_string() << endl;
//     cout << Pi<C> << endl;
//     cout << Pi<CVHP>.real().to_string() << "I * " << Pi<CVHP>.imag().to_string() << endl;
// }
