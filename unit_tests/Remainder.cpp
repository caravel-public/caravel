#include "misc/CaravelCereal.hpp"
#include "catch.hpp"

#include <unordered_map>
#include <string>

#include "PoleStructure/Remainder.h"
#include "Core/typedefs.h"
#include "IntegralLibrary/BasisElementFunction.h"


using namespace Caravel;

using F = F32;

TEST_CASE("Pole vanish", "[Remainder][Poles][Check]"){
    std::unordered_map<std::string, Series<F>> b1{};
    b1.insert({"f1", Series<F>(-2, 1, F(-2), F(-1), F(10), F(20))});
    b1.insert({"f2", Series<F>(-2, 1, F(-2), F(-1), F(100), F(200))});
    Integrals::NumericFunctionBasisExpansion<Series<F>,std::string> sab1(b1);


    std::unordered_map<std::string, Series<F>> b2{};
    b2.insert({"f1", Series<F>(-2, 1, F(0), F(0), F(10), F(20))});
    b2.insert({"f2", Series<F>(-2, 1, F(0), F(0), F(100), F(200))});
    Integrals::NumericFunctionBasisExpansion<Series<F>,std::string> sab2(b2);

    std::unordered_map<std::string, Series<F>> b3{};
    b3.insert({"f1", Series<F>(-2, -1, F(0), F(0))});
    b3.insert({"f2", Series<F>(-2, -1, F(0), F(0))});
    Integrals::NumericFunctionBasisExpansion<Series<F>,std::string> sab3(b3);

    bool check_poles1 = check_if_poles_vanish(sab1);
    bool check_poles2 = check_if_poles_vanish(sab2);
    bool check_poles3 = check_if_poles_vanish(sab3);

    CHECK(!check_poles1);
    CHECK(check_poles2);
    CHECK(check_poles3);

}

TEST_CASE("Remove poles", "[Remainder][Poles][Remove]"){
    std::unordered_map<Integrals::StdBasisFunction<C,C>, Series<F>> b1{};
    b1.insert({Integrals::StdBasisFunction<C,C>([](const std::vector<C>&){return C(1);}, "f1", {"x"}), Series<F>(-2, 1, F(-2), F(-1), F(10), F(20))});
    b1.insert({Integrals::StdBasisFunction<C,C>([](const std::vector<C>&){return C(1);}, "f2", {"x"}), Series<F>(-2, 1, F(-2), F(-1), F(100), F(200))});
    Integrals::SemiAnalyticExpression<F,C> sab1(b1);


    std::unordered_map<Integrals::StdBasisFunction<C,C>, Series<F>> b2{};
    b2.insert({Integrals::StdBasisFunction<C,C>([](const std::vector<C>&){return C(1);}, "f1", {"x"}), Series<F>(-2, 1, F(0), F(0), F(10), F(20))});
    b2.insert({Integrals::StdBasisFunction<C,C>([](const std::vector<C>&){return C(1);}, "f2", {"x"}), Series<F>(-2, 1, F(0), F(0), F(100), F(200))});
    Integrals::SemiAnalyticExpression<F,C> sab2(b2);

    std::unordered_map<Integrals::StdBasisFunction<C,C>, Series<F>> b3{};
    b3.insert({Integrals::StdBasisFunction<C,C>([](const std::vector<C>&){return C(1);}, "f1", {"x"}), Series<F>(-2, -1, F(0), F(0))});
    b3.insert({Integrals::StdBasisFunction<C,C>([](const std::vector<C>&){return C(1);}, "f2", {"x"}), Series<F>(-2, -1, F(0), F(0))});
    Integrals::SemiAnalyticExpression<F,C> sab3(b3);

    auto no_poles1 = remove_poles(std::move(sab1));
    auto no_poles2 = remove_poles(std::move(sab2));

    CHECK_THROWS_AS(remove_poles(std::move(sab3)), std::runtime_error);

    CHECK(sab1.leading() == 0);
    CHECK(sab2.leading() == 0);

    CHECK(sab1.last() == 0);
    CHECK(sab2.last() == 0);

    CHECK(no_poles1.get_coeff(Integrals::StdBasisFunction<C,C>([](const std::vector<C>&){return C(1);}, "f1", {"x"}))[0] == 10);
    CHECK(no_poles2.get_coeff(Integrals::StdBasisFunction<C,C>([](const std::vector<C>&){return C(1);}, "f2", {"x"}))[0] == 100);
}

#if defined USE_FINITE_FIELDS && defined WITH_PENTAGONS
TEST_CASE("Catani operators", "[Poles][CataniJ]") {
    using PA = PartialAmplitudeInput;

    std::vector<PA> input = {
        PA{"PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p],Particle[gluon,5,p]],"
           "ColourStructure[NfPowers[0]]]"},
        PA{"PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p],Particle[gluon,5,p]],"
           "ColourStructure[NfPowers[1]]]"},
        PA{"PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p],Particle[gluon,5,p]],"
           "ColourStructure[NfPowers[2]]]"},

        PA{"PartialAmplitudeInput[Particles[Particle[gluon,1,m],Particle[gluon,2,p],Particle[gluon,3,m],Particle[gluon,4,p],Particle[gluon,5,p]],"
           "ColourStructure[NfPowers[0]]]"},
        PA{"PartialAmplitudeInput[Particles[Particle[gluon,1,m],Particle[gluon,2,p],Particle[gluon,3,m],Particle[gluon,4,p],Particle[gluon,5,p]],"
           "ColourStructure[NfPowers[1]]]"},
        PA{"PartialAmplitudeInput[Particles[Particle[gluon,1,m],Particle[gluon,2,p],Particle[gluon,3,m],Particle[gluon,4,p],Particle[gluon,5,p]],"
           "ColourStructure[NfPowers[2]]]"},

        PA{"PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[gluon,3,p],Particle[gluon,4,m],Particle[gluon,5,p]],ColourStructure["
           "NfPowers[0]]]"},
        PA{"PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[gluon,3,p],Particle[gluon,4,m],Particle[gluon,5,p]],ColourStructure["
           "NfPowers[1]]]"},
        PA{"PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[gluon,3,p],Particle[gluon,4,m],Particle[gluon,5,p]],ColourStructure["
           "NfPowers[2]]]"},

        PA{"PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[d,3,qbp],Particle[db,4,qm],Particle[gluon,5,p]],ColourStructure["
           "NfPowers[0]]]"},
        PA{"PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[d,3,qbp],Particle[db,4,qm],Particle[gluon,5,p]],ColourStructure["
           "NfPowers[1]]]"},
        PA{"PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[d,3,qbp],Particle[db,4,qm],Particle[gluon,5,p]],ColourStructure["
           "NfPowers[2]]]"},
    };

    using NT = decltype(PoleStructure::poleJ2Operator<BigRat, C>(0, std::declval<PartialAmplitudeInput>()));

    std::vector<NT> results;

    for (auto pa : input) {
        auto r = PoleStructure::poleJ2Operator<BigRat, C>(0, pa);
        results.push_back(r);
    }

    std::string filename = "test_data/" + GENERATE_FILENAME(poleJ2operator_results.dat);

#ifdef GENERATE_TARGETS_MODE
    write_serialized(results, filename);
#endif

    auto target = read_serialized<decltype(results)>(filename);

#define FOR(i, j, X, Y) for (auto(i) = (X).begin(), (j) = (Y).begin(); (i) != (X).end() or (j) != (Y).end(); ++(i), ++(j))

    FOR(i, j, results, target) {
        std::map<NT::FunctionsType, NT::CoefficientsType> res(i->begin(), i->end());
        std::map<NT::FunctionsType, NT::CoefficientsType> targ(j->begin(), j->end());
        REQUIRE(res == targ);
    }
}
#endif

