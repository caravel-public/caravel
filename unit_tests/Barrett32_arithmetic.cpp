#include "catch.hpp"

#include <sstream>
#include <iostream>
#include <random>

#include "Core/Barrett32.h"
#include "Core/ModularInverse.h"

#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

TEST_CASE("Field operations", "[Barrett32Arithmetic][FieldOperations]"){

  using Caravel::BigRat;
  using Caravel::BigInt;

  using F = Caravel::Barrett32;

    uint64_t p = 2147483629;
    F::set_p(p);

    int64_t a_res = 92179;
    F a(a_res);

    int64_t b_res = 1183029;
    F b(b_res);


    SECTION("Ring operations"){

      // Note, choice of a_res and b_res means that nothing overflows.
	    REQUIRE( a + b == F(a_res+b_res % p) );
	    REQUIRE( a - b == F(a_res - b_res) );
	    REQUIRE( -a == F(-a) );

	    REQUIRE( a * b == F(a_res*b_res % p) );
	    REQUIRE( a / a == F(1) );

	    REQUIRE( pow(b, 2) == F(pow(b,2)) );
      
	    REQUIRE( pow(b, 111092) == F(674797929) );


    }

    SECTION("Negative result in EEA"){
      F x = 2094548590;

	    F numa = x + F(9)*x*x;
      F numa_pre = 940974732;

	    F den = F(1) + F(7)*x + x*x + x*x*x + x*x*x*x;
      F den_pre = 988073864;

      REQUIRE( numa == numa_pre  );
      REQUIRE( den == den_pre  );

      F ratio = 1621597467;
      REQUIRE( numa/den == ratio );


    }

    SECTION("BigInt"){

      BigInt a = 1;
      BigInt p = 2147483587;

      BigInt ainv = Caravel::Core::modular_inverse(a, p);

      REQUIRE( ainv == BigInt(1) );
    }

    SECTION("Relation to rational"){


      std::vector<BigRat> xs;
      std::cout << "Building some rationals" << std::endl;
      //size_t nparticles = 5;
      //xs = {BigRat(-1), BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17)}; // 5pt std point
      size_t nparticles = 4;
      xs = {BigRat(-1), BigRat(4, 3)}; // 5pt std point
      std::cout << "Some arithmetic = " << xs[0]/xs[1] << std::endl;
      std::cout << "Built some rationals" << std::endl;

      BigRat s(xs.at(0));
      BigRat t(xs.at(1));
      std::cout << "Built s and t" << std::endl;
      std::vector<BigRat> k1 {BigRat(1), BigRat(1), BigRat(-1), BigRat(1)};
      std::cout << "Built k1" << std::endl;
      std::vector<BigRat> k2 {s / BigRat(4), BigRat(0), BigRat(0), -s / BigRat(4)};
      std::cout << "Built k2" << std::endl;
      std::vector<BigRat> k3 {-s / BigRat(4) + t / s, BigRat(-1) + t / BigRat(4), BigRat(1) + t / BigRat(4), s / BigRat(4) + t / s};
      std::cout << "Built k3" << std::endl;
      std::vector<BigRat> k4 {BigRat(-1) - t / s, t / BigRat(-4), t / BigRat(-4), BigRat(-1) - t / s};
      std::cout << "Built k4" << std::endl;

      Caravel::momentumD_configuration<BigRat,4> result_conf(k1,k2,k3,k4);

      std::cout << "Built some momenta" << std::endl;
      auto p1_rat = result_conf.p(1);
      std::cout << "Checking rational on-shellness" << std::endl;

      REQUIRE( p1_rat*p1_rat == BigRat(0) );
      auto physical_point_rat = Caravel::rational_mom_conf<BigRat>(nparticles, xs);
      Caravel::momD_conf<F, 6> physical_point = static_cast<Caravel::momentumD_configuration<F,4>>(physical_point_rat);

      auto p1 = physical_point.p(1);

      std::cout << "Checking on-shellness" << std::endl;

      REQUIRE( p1*p1 == F(0) );
    }

    SECTION("Conversion operators"){
      uint64_t p = 2147483629;
      F::set_p(p);

      F num = F(uint64_t(-70187355530l));
      F den = F(7891499);

      F target = num/den;

      std::cout << "num = " << num << std::endl;
      std::cout << "den = " << den << std::endl;
      std::cout << "target = " << target << std::endl;


      REQUIRE(target == F(968835482));
    }

}

TEST_CASE("Tonelli Shanks algorithm", "[Barrett32Arithmetic][TonelliShanks]") {
    using F = Caravel::Barrett32;
    uint64_t p = 2147483629;
    F::set_p(p);

    // Quadratic residues
    std::vector<F> x = {F(881306321),  F(1545153190), F(395970031), F(110137813), F(1950058195),
                        F(1206190395), F(1850455554), F(310152270), F(258169440), F(423947540)};
    // Sqrt[x] mod p 
    std::vector<F> targets = {F(21268043),  F(453418267), F(42031144),  F(798606466), F(81450345),
                              F(524612445), F(973486127), F(356700927), F(435263797), F(995827704)};

    for (size_t ii = 0; ii < x.size(); ii++) {
        REQUIRE(is_quadratic_residue(x[ii]));
        REQUIRE(mod_sqrt(x[ii]) == targets[ii]);
        REQUIRE(targets[ii] * targets[ii] == x[ii]);
    }

    // Test non quadratic residues
    std::vector<F> y = {F(1090791960), F(666285112), F(992926317),  F(1951920545), F(1260507963),
                        F(205478023),  F(640956912), F(1563588329), F(947749990),  F(1052047166)};
    for (size_t ii = 0; ii < y.size(); ii++) REQUIRE(!is_quadratic_residue(y[ii]));
}
