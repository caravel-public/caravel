#include "catch.hpp"

#include <string>
#include <sstream>

#include "IntegralLibrary/InputVariables.h"


using namespace Caravel::Integrals;

TEST_CASE("InputVariable", "[Integrals][Input][InputVariable]"){
    InputVariable iv(InvariantType::Mandelstam, {1,1});
    std::ostringstream oss;
    oss << iv;
    CHECK(oss.str() == std::string("Mandelstam[1,1]"));
}