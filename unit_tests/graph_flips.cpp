#include "Graph/Graph.h"
#include "catch.hpp"

using namespace Caravel;
using namespace lGraph;

TEST_CASE("Check if flipped graph input is recognized as equivalent", "[Graph]") {

    // construct a graph

    std::vector<Node> graph1_nodes;
    std::vector<Strand> graph1_strands;
    std::map<std::pair<size_t, size_t>, Connection<Strand>> graph1_map;

    graph1_strands.push_back(Strand({Bead(Leg(true, 0, 1)), Bead(Leg(true, 0, 2))}, {LoopMomentum{1}, Link{}, Link{}}));
    graph1_strands.push_back(Strand({Bead(Leg(true, 0, 3)), Bead(Leg(true, 0, 4))}, {LoopMomentum{2}, Link{}, Link{}}));
    graph1_strands.push_back(Strand({Bead(Leg(true, 0, 5)), Bead(Leg(true, 0, 6)), Bead(Leg(true, 0, 7))}, {Link(0), Link(0), Link(0), Link(0)}));

    graph1_nodes.push_back(Node(Leg(true, 0, 8)));
    graph1_nodes.push_back(Node(Leg(true, 0, 9)));

    graph1_map[std::make_pair(0, 1)] = Connection<Strand>(graph1_strands);

    auto graph1 = Graph<Node, Connection<Strand>>(graph1_nodes, graph1_map);
    auto xGraph1 = xGraph(graph1);

    // construct the left-right flipped

    std::vector<Node> graph2_nodes;
    std::vector<Strand> graph2_strands;
    std::map<std::pair<size_t, size_t>, Connection<Strand>> graph2_map;

    graph2_strands.push_back(Strand({Bead(Leg(true, 0, 5)), Bead(Leg(true, 0, 6)), Bead(Leg(true, 0, 7))}, {Link(0), Link(0), Link(0), Link(0)}));
    graph2_strands.push_back(Strand({Bead(Leg(true, 0, 3)), Bead(Leg(true, 0, 4))}, {LoopMomentum{2}, Link{}, Link{}}));
    graph2_strands.push_back(Strand({Bead(Leg(true, 0, 1)), Bead(Leg(true, 0, 2))}, {LoopMomentum{1}, Link{}, Link{}}));
    graph2_nodes.push_back(Node(Leg(true, 0, 8)));
    graph2_nodes.push_back(Node(Leg(true, 0, 9)));

    graph2_map[std::make_pair(0, 1)] = Connection<Strand>(graph2_strands);

    auto graph2 = Graph<Node, Connection<Strand>>(graph2_nodes, graph2_map);
    auto xGraph2 = xGraph(graph2);

    // construct the up-down flipped

    std::vector<Node> graph3_nodes;
    std::vector<Strand> graph3_strands;
    std::map<std::pair<size_t, size_t>, Connection<Strand>> graph3_map;

    graph3_strands.push_back(Strand({Bead(Leg(true, 0, 2)), Bead(Leg(true, 0, 1))}, {Link{}, Link{}, LoopMomentum{-1}}));
    graph3_strands.push_back(Strand({Bead(Leg(true, 0, 4)), Bead(Leg(true, 0, 3))}, {Link{}, Link{}, LoopMomentum{-2}}));
    graph3_strands.push_back(Strand({Bead(Leg(true, 0, 7)), Bead(Leg(true, 0, 6)), Bead(Leg(true, 0, 5))}, {Link(0), Link(0), Link(0), Link(0)}));

    graph3_nodes.push_back(Node(Leg(true, 0, 9)));
    graph3_nodes.push_back(Node(Leg(true, 0, 8)));

    graph3_map[std::make_pair(0, 1)] = Connection<Strand>(graph3_strands);

    auto graph3 = Graph<Node, Connection<Strand>>(graph3_nodes, graph3_map);
    auto xGraph3 = xGraph(graph3);
    CHECK(xGraph1.get_base_graph()==xGraph2.get_base_graph());
    CHECK(xGraph1.get_base_graph()==xGraph3.get_base_graph());
}
