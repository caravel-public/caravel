#include "catch.hpp"

#include "Core/Series.h"
#include "Core/type_traits_extra.h"
#include "IntegralLibrary/BasisElementFunction.h"

using namespace Caravel;

TEST_CASE("is_series"){
    Series<double> s;
    CHECK(std::is_same<decltype(s), Series<double>>::value == true);
    CHECK(is_series<decltype(s)>::value == true);
    CHECK(is_series<Series<double>>::value == true);
    CHECK(is_series<int>::value == false);
    CHECK(is_series<double>::value == false);

    CHECK(!Integrals::is_std_basis_function<double>::value);
    CHECK(Integrals::is_std_basis_function<Integrals::StdBasisFunction<double, double>>::value);
}
