#include "catch.hpp"
#include <tuple>
#include <random>
#include "compare_tools.hpp"
#include "Core/typedefs.h"
#include "Core/settings.h"

#include "AmplitudeCoefficients/IntegrandHierarchy.h"

using namespace std;
using namespace Caravel;
using namespace Caravel::IntegrandHierarchy;

TEST_CASE("Dimensional reconstruction","[Ds_dependence]"){
    typedef F32 F;
    DensePolynomial<F>  one ({F(1)});

    SECTION("Dss={6,8,10}"){
        vector<F> coeffs{5,-2,7};
        vector<size_t> Dss = {6,8,10};

        auto dsfun = [&coeffs](auto Ds){
            return coeffs[0] + coeffs[1]*F(int64_t(Ds-4)) + coeffs[2]*F(int64_t(Ds-4))*F(int64_t(Ds-4));
        };

        Vec<F> v(3);
        for(unsigned i=0; i<=2; i++){ v[i] =dsfun(Dss[i]); }

        auto result = Caravel::IntegrandHierarchy::internal::dimensional_reconstruct_quadratic(std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>{Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>(DensePolynomial<Vec<F>,F>({v}),one)},Dss);

        CHECK(result.front().numerator.coefficients.at(0) == coeffs[0]);
        CHECK(result.front().numerator.coefficients.at(1) == -F(2)*coeffs[1]);
        CHECK(result.front().numerator.coefficients.at(2) == F(4)*coeffs[2]);
    }

    SECTION("Setting a specific Ds value"){
        vector<F> coeffs{5,-2,7};
        vector<size_t> Dss = {6,8,10};
        vector<F> DssF = {F(6),F(8),F(10)};

        auto dsfun = [&coeffs](auto Ds){
            return coeffs[0] + coeffs[1]*Ds + coeffs[2]*Ds*Ds;
        };

        Vec<F> v(3);
        for(unsigned i=0; i<=2; i++){ v[i] =dsfun(DssF[i]); }

        DensePolynomial<F> Dsvalue({4,-2,10,1});

        auto result = Caravel::IntegrandHierarchy::internal::dimensional_reconstruct_quadratic(std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>{Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>(DensePolynomial<Vec<F>,F>({v}),one)},Dss,Dsvalue);

        CHECK(result.front().numerator == dsfun(Dsvalue));
    }

    SECTION("Dss={6,7,8}"){
        vector<F> coeffs{-1062,431,258};
        vector<size_t> Dss = {6,7,8};
        vector<F> DssF = {F(6),F(7),F(8)};

        auto dsfun = [&coeffs](auto Ds){
            return coeffs[0] + coeffs[1]*(Ds-4) + coeffs[2]*(Ds-4)*(Ds-4);
        };

        Vec<F> v(3);
        for(unsigned i=0; i<=2; i++){ v[i] =dsfun(DssF[i]); }

        auto result = Caravel::IntegrandHierarchy::internal::dimensional_reconstruct_quadratic(std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>{Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>(DensePolynomial<Vec<F>,F>({v}),one)},Dss);

        CHECK(result.front().numerator.coefficients.at(0) == coeffs[0]);
        CHECK(result.front().numerator.coefficients.at(1) == -F(2)*coeffs[1]);
        CHECK(result.front().numerator.coefficients.at(2) == F(4)*coeffs[2]);
    }
}

