#include "catch.hpp"

#include "IntegralLibrary/MapToBasis.h"
#include "Core/Series.h"

using namespace Caravel;
using namespace Caravel::Integrals;

TEST_CASE("2q3y Map", "[MapToPentagonsNew][Integrals][2q3y]"){

    settings::use_setting("integrals::integral_family_2L pentagon_functions_new");
    F32::set_p(2147483587);
    // Use the default map stored under share/Caravel
    MapToBasis<F32, StdBasisFunction<C, R>> m(PartialAmplitudeInput(MathList("PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[photon,3,m],Particle[photon,4,p],Particle[photon,5,p]]]")));

    CHECK(m.n_replacements() == 3075);

    momentumD_configuration<F32,4> mcf=static_cast<momentumD_configuration<F32,4>>(rational_mom_conf<BigRat>(5,{BigRat(-1), BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17)}));

    auto rules1 = m.get_replacement_rule_for_function("Fac[1,1,1]*MI[\"Bubble\",1,-1,s13]*bci[1]*bci[1]");

    CHECK(rules1.size() == 2);
	
    CHECK(rules1[0].first == "bci[1]*bci[1]*bci[1]");
    CHECK(rules1[0].second(mcf,false) == F32(2147483586));
    CHECK(rules1[1].first == "F[1,1,1]*bci[1]*bci[1]");
    CHECK(rules1[1].second(mcf,true) == F32(1));
}
