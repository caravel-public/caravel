#include "Forest/Contact/States_Vector_Boson.hpp"
#include "Forest/Contact/States_Massive_Vector_Boson.hpp"
#include "Forest/Contact/Tensor.hpp"
#include "catch.hpp"
#include "compare_tools.hpp"

//#define DEBUG_TEST

using namespace Caravel;
using namespace BG;
using namespace Massive_vector_states;

template <size_t N> using int_ = Caravel::_utilities_private::int_<N>;
template <typename T, size_t Ds>
inline void add_trivial(std::vector<std::vector<T>>& ep, std::vector<std::vector<T>>& epC, int_<4>, momD_conf<T, Ds>& m, size_t l, size_t r) {}
template <typename T, size_t Ds>
inline void add_trivial(std::vector<std::vector<T>>& ep, std::vector<std::vector<T>>& epC, int_<5>, momD_conf<T, Ds>& m, size_t l, size_t r) {}
template <typename T, size_t Ds>
inline void add_trivial(std::vector<std::vector<T>>& ep, std::vector<std::vector<T>>& epC, int_<6>, momD_conf<T, Ds>& m, size_t l, size_t r) {}
template <typename T, size_t N, size_t Ds>
inline void add_trivial(std::vector<std::vector<T>>& ep, std::vector<std::vector<T>>& epC, int_<N>, momD_conf<T, Ds>& m, size_t l, size_t r) {
    ep.push_back(externalWF_vectorboson_trivial_Ds<T, T, Ds, N - 2>(m, l, r));
    epC.push_back(externalWF_vectorboson_trivial_conjugated_Ds<T, T, Ds, N - 2>(m, l, r));
    add_trivial(ep, epC, int_<N - 1>{}, m, l, r);
}

template <typename T>
void get_states_4D(std::vector<std::vector<T>>& ep, std::vector<std::vector<T>>& epC, const momD<T, 4>& l, bool bref, const momD<T, 4>& mref, const T& mass) {
    // prepare momD_conf
    momD<T, 4> ml(-l);
    momD_conf<T, 4> mc(l, ml);
    size_t ref(0);
    if (bref)
        ref = mc.insert(mref);
    else {
        momD<T, 4> local_ref(T(1), T(1), T(0), T(0));
        ref = mc.insert(local_ref);
    }

    // Compute states
    ep.clear();
    epC.clear();

    // 4D states
    ep.push_back(externalWF_L<T, T, 4>(mc[1], mc[ref], mass));
    ep.push_back(externalWF_p<T, T, 4>(mc[1], mc[ref], mass));
    ep.push_back(externalWF_m<T, T, 4>(mc[1], mc[ref], mass));

    // 4D conjugated states
    epC.push_back(externalWF_conj_L<T, T, 4>(mc[1], mc[ref], mass));
    epC.push_back(externalWF_conj_p<T, T, 4>(mc[1], mc[ref], mass));
    epC.push_back(externalWF_conj_m<T, T, 4>(mc[1], mc[ref], mass));

    // The +- states have a factor of 1/sqrt(2) factored out. We include it
    // back in the conjugated states
    for (size_t n = 1; n < epC.size(); n++)
        for (size_t i = 0; i < epC[n].size(); i++) epC[n][i] /= T(2);

#ifdef DEBUG_TEST
    std::cout << "4D Polarisation vectors for Ds = " << 4 << std::endl;
    std::cout << "States:" << std::endl;
    for (auto p : ep) std::cout << p << std::endl;
    std::cout << "States conjugated:" << std::endl;
    for (auto p : epC) std::cout << p << std::endl;
#endif
}

template <typename T, size_t Ds>
void get_states_D(std::vector<std::vector<T>>& ep, std::vector<std::vector<T>>& epC, const momD<T, Ds>& l, bool bref, const momD<T, Ds>& mref, const T& mass) {
    // prepare momD_conf
    momD<T, Ds> ml(-l);
    momD_conf<T, Ds> mc(l, ml);
    size_t ref(0);
    if (bref)
        ref = mc.insert(mref);
    else {
        momD<T, 4> local_ref(T(1), T(1), T(0), T(0));
        ref = mc.insert(local_ref);
    }

    // Compute states
    ep.clear();
    epC.clear();

    // D states
    ep.push_back(externalWF_vectorboson_mD<T, T, Ds>(mc, 1, ref));
    ep.push_back(externalWF_vectorboson_pD<T, T, Ds>(mc, 1, ref));
    ep.push_back(externalWF_vectorboson_sD<T, T, Ds>(mc, 1, ref));
    ep.push_back(WF_massive_vectorboson_extraD<T, T, Ds>(mc, 1, ref));

    // 4D conjugated states
    epC.push_back(externalWF_vectorboson_pD<T, T, Ds>(mc, 2, ref));
    epC.push_back(externalWF_vectorboson_mD<T, T, Ds>(mc, 2, ref));
    epC.push_back(externalWF_vectorboson_scD<T, T, Ds>(mc, 2, ref));
    epC.push_back(WF_massive_vectorboson_extracD<T, T, Ds>(mc, 2, ref));

    if constexpr (Ds > 5) {
        ep.push_back(externalWF_vectorboson_tD<T, T, Ds>(mc, 1, ref));
        epC.push_back(externalWF_vectorboson_tcD<T, T, Ds>(mc, 2, ref));
    }

    // For Ds > 4 fill states with trivial states
    // mom_indeces arbitrary as these are constant vectors
    add_trivial(ep, epC, int_<Ds>{}, mc, 1, ref);

#ifdef DEBUG_TEST
    std::cout << "D Polarisation vectors for Ds = " << Ds << std::endl;
    std::cout << "States:" << std::endl;
    for (auto p : ep) std::cout << p << std::endl;
    std::cout << "States conjugated:" << std::endl;
    for (auto p : epC) std::cout << p << std::endl;
#endif
}

template <typename T, size_t Ds> void checks(std::vector<std::vector<T>> ep, std::vector<std::vector<T>> epC, momD<T, Ds> k, T mass) {
    Tensor<T, Ds, 2> eta(0);
    const std::vector<T> metric(eta.get_metric_extended());
    for (size_t mu = 0; mu < Ds; mu++) eta[mu][mu] = T(1) / metric[mu];

    // On-shell?
#ifdef DEBUG_TEST
    std::cout << "k^2 - m^2 = " << k * k - mass * mass << std::endl;
#endif
    CHECK_ZERO(k * k - mass * mass);

    // Check Orthonormality
    for (size_t ii = 0; ii < ep.size(); ii++)
        for (size_t jj = 0; jj < epC.size(); jj++) {
            T v = Tensor<T, Ds, 1>(ep[ii]) * Tensor<T, Ds, 1>(epC[jj]);
#ifdef DEBUG_TEST
            std::cout << "ep[" << ii << "]*epC[" << jj << "] = " << v << std::endl;
#endif
            if (ii == jj) CHECK_ZERO(v + T(1));
            if (ii != jj) CHECK_ZERO(v);
        }

    // Check Transversality
    for (size_t ii = 0; ii < ep.size(); ii++) {
        T v = Tensor<T, Ds, 1>(k) * Tensor<T, Ds, 1>(ep[ii]);
#ifdef DEBUG_TEST
        std::cout << "k*ep[" << ii << "] = " << v << std::endl;
#endif
        CHECK_ZERO(v);
    }

    for (size_t ii = 0; ii < epC.size(); ii++) {
        T v = Tensor<T, Ds, 1>(k) * Tensor<T, Ds, 1>(epC[ii]);
#ifdef DEBUG_TEST
        std::cout << "k*epC[" << ii << "] = " << v << std::endl;
#endif
        CHECK_ZERO(v);
    }

    // Check Polarization Tensor
    Tensor<T, Ds, 2> P(0), Q(0);
    for (size_t mu = 0; mu < Ds; mu++) {
        for (size_t nu = 0; nu < Ds; nu++) {
            P[mu][nu] = -eta[mu][nu] + k[mu] * k[nu] / (mass * mass);
            for (size_t ii = 0; ii < ep.size(); ii++) Q[mu][nu] += ep[ii][mu] * epC[ii][nu];
        }
    }

#ifdef DEBUG_TEST
    std::cout << "P = " << P << std::endl;
    std::cout << "Q = " << Q << std::endl;
#endif

    for (size_t mu = 0; mu < Ds; mu++)
        for (size_t nu = 0; nu < Ds; nu++) CHECK_ZERO(P[mu][nu] - Q[mu][nu]);

}

TEST_CASE("Completeness relation for vector states: 4-D and floating point", "[States]") {
    constexpr unsigned Ds = 4;
    using T = C;
    T mass = T(5);
    std::vector<momD<T, Ds>> moms = {
        momD<T, Ds>(sqrt(T(3) + mass * mass), T(1), T(1), T(1)),
        momD<T, Ds>(sqrt(T(50) + mass * mass), T(3), T(-4), T(5)),
        momD<T, Ds>(sqrt(T(9) + mass * mass), T(0), T(0), T(3)),
    };
    momD<T, Ds> mref(sqrt(T(3)), T(1), T(1), T(1));
    momD_conf<T, Ds> mc(moms);

    // check relations for all momenta in the given PS point
    for (size_t ii = 1; ii <= mc.n(); ii++) {

        SECTION("4D vector completeness using default reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_4D(ep, epC, l, false, mref, mass);
            checks(ep, epC, l, mass);
        }

        SECTION("4D vector completeness with a given reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_4D(ep, epC, l, true, mref, mass);
            checks(ep, epC, l, mass);
        }
    }
}

TEST_CASE("Completeness relation for cut vector states: 5-D and floating point", "[States]") {
    constexpr unsigned Ds = 5;
    using T = C;
    T mass = T(7);
    std::vector<momD<T, Ds>> moms = {
        momD<T, Ds>(sqrt(T(13) + mass * mass), T(1, 0), T(3, 0), T(0, 1), T(2, 0)),
        momD<T, Ds>(sqrt(T(-4) + mass * mass), T(2, 0), T(0, -1), T(0, 4), T(3, 0)),
        momD<T, Ds>(sqrt(T(-7) + mass * mass), T(0, -2), T(3, 0), T(-2, 0), T(0, 4)),
    };
    momD<T, Ds> mref(sqrt(T(3)), T(1), T(1), T(1));
    momD_conf<T, Ds> mc(moms);

    // check relations for all momenta in the given PS point
    for (size_t ii = 1; ii <= mc.n(); ii++) {
        SECTION("5D vector completeness using default reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n() ) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, false, mref, mass);
            checks(ep, epC, l, mass);
        }

        SECTION("5D vector completeness with a given reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n() ) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, true, mref, mass);
            checks(ep, epC, l, mass);
        }
    }
}

TEST_CASE("Completeness relation for cut vector states: 6-D and floating point", "[States]") {
    constexpr unsigned Ds = 6;
    using T = C;
    T mass = T(3);
    std::vector<momD<T, Ds>> moms = {
        momD<T, Ds>(sqrt(T(12) + mass * mass), T(1, 0), T(3, 0), T(0, 1), T(2, 0), T(0, 1)),
        momD<T, Ds>(sqrt(T(-8) + mass * mass), T(2, 0), T(0, -1), T(0, 4), T(3, 0), T(0, -2)),
        momD<T, Ds>(sqrt(T(18) + mass * mass), T(0, -2), T(3, 0), T(-2, 0), T(0, 4), T(5, 0)),
    };
    momD<T, Ds> mref(sqrt(T(3)), T(1), T(1), T(1));
    momD_conf<T, Ds> mc(moms);

    // check relations for all momenta in the given PS point
    for (size_t ii = 1; ii <= mc.n(); ii++) {
        SECTION("6D vector completeness using default reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, false, mref, mass);
            checks(ep, epC, l, mass);
        }

        SECTION("6D vector completeness with a given reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, true, mref, mass);
            checks(ep, epC, l, mass);
        }
    }
}

TEST_CASE("Completeness relation for cut vector states: 12-D and floating point", "[States]") {
    constexpr unsigned Ds = 12;
    using T = C;
    T mass = T(3);
    std::vector<momD<T, Ds>> moms = {
        momD<T, Ds>(sqrt(T(12) + mass * mass), T(1, 0), T(3, 0), T(0, 1), T(2, 0), T(0, 1)),
        momD<T, Ds>(sqrt(T(-8) + mass * mass), T(2, 0), T(0, -1), T(0, 4), T(3, 0), T(0, -2)),
        momD<T, Ds>(sqrt(T(18) + mass * mass), T(0, -2), T(3, 0), T(-2, 0), T(0, 4), T(5, 0)),
    };
    momD<T, Ds> mref(sqrt(T(3)), T(1), T(1), T(1));
    momD_conf<T, Ds> mc(moms);

    // check relations for all momenta in the given PS point
    for (size_t ii = 1; ii <= mc.n(); ii++) {
        SECTION("6D vector completeness using default reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, false, mref, mass);
            checks(ep, epC, l, mass);
        }

        SECTION("6D vector completeness with a given reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, true, mref, mass);
            checks(ep, epC, l, mass);
        }
    }
}

#ifdef USE_FINITE_FIELDS
TEST_CASE("Completeness relation for vector states: 4-D and finite fields", "[States]") {
    constexpr unsigned Ds = 4;
    using T = F32;
    T mass = T(15);
    std::vector<momD<T, Ds>> moms = {
        momD<T, Ds>(T(1280621656), T(292097531), T(1956746128), T(1388468631)),
        momD<T, Ds>(T(1455785193), T(763596905), T(2129514748), T(1154954819)),
        momD<T, Ds>(T(2138422252), T(1051635444), T(1802537681), T(2087074249)),
        momD<T, Ds>(T(132187822), T(748649381), T(2094274), T(59536770)),
    };
    momD<T, Ds> mref(T(-5), T(10), T(-10), T(-5));
    momD_conf<T, Ds> mc(moms);
    EpsilonBasis<T>::_make_sure_squares_are_initialized();

    // check relations for all momenta in the given PS point
    for (size_t ii = 1; ii <= mc.n(); ii++) {

        SECTION("4D vector completeness using default reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_4D(ep, epC, l, false, mref, mass);
            checks(ep, epC, l, mass);
        }

        SECTION("4D vector completeness with a given reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            std::vector<std::vector<T>> ep, epC;
            get_states_4D(ep, epC, l, true, mref, mass);
            checks(ep, epC, l, mass);
        }
    }
}

TEST_CASE("Completeness relation for cut vector states: 5-D and finite fields", "[States]") {
    constexpr unsigned Ds = 5;
    using T = F32;
    T mass = T(10);
    std::vector<momD<T, Ds>> moms = {
        momD<T, Ds>(T(159101313), T(3244021), T(271719675), T(63524251), T(1)),
        momD<T, Ds>(T(1019942052), T(509267890), T(59291256), T(1346707414), T(1)),
        momD<T, Ds>(T(501497444), T(669508587), T(1323268186), T(1072521032), T(1)),
        momD<T, Ds>(T(524878389), T(1499670065), T(1765356266), T(924715423), T(1)),
    };
    std::vector<T> mu = {T(1277653935), T(644359923), T(1242130543), T(1524009114)};
    EpsilonBasis<T>::squares[1] = T(0);
    momD<T, Ds> mref(T(-5), T(10), T(-10), T(-5));
    momD_conf<T, Ds> mc(moms);

    // check relations for all momenta in the given PS point
    for (size_t ii = 1; ii <= mc.n(); ii++) {
        SECTION("5D vector completeness using default reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            EpsilonBasis<T>::squares[0] = mu[ii - 1];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, false, mref, mass);
            checks(ep, epC, l, mass);
        }

        SECTION("5D vector completeness with a given reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            EpsilonBasis<T>::squares[0] = mu[ii - 1];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, true, mref, mass);
            checks(ep, epC, l, mass);
        }
    }
}
TEST_CASE("Completeness relation for cut vector states: 6-D and finite field", "[States]") {
    constexpr unsigned Ds = 6;
    using T = F32;
    T mass = T(5);
    std::vector<momD<T, Ds>> moms = {
        momD<T, Ds>(T(1687660144), T(467026743), T(878600496), T(403256143), T(1989102980), T(2147483628)),
        momD<T, Ds>(T(240307342), T(30867778), T(735701834), T(232443582), T(728932208), T(2147483628)),
        momD<T, Ds>(T(617547705), T(629038752), T(654057503), T(1019826651), T(586795254), T(2147483628)),
        momD<T, Ds>(T(2098083222), T(951841855), T(1091087583), T(1135042903), T(1801595590), T(2147483628)),
    };
    std::vector<T> mu1 = {T(2028296006), T(1582691586), T(324919630), T(746760514)};
    std::vector<T> mu2 = {T(1896020034), T(1488324260), T(527245221), T(1696476052)};
    momD<T, Ds> mref(T(-5), T(10), T(-10), T(-5));
    momD_conf<T, Ds> mc(moms);

    // check relations for all momenta in the given PS point
    for (size_t ii = 1; ii <= mc.n(); ii++) {
        SECTION("6D vector completeness using default reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            EpsilonBasis<T>::squares[0] = mu1[ii - 1];
            EpsilonBasis<T>::squares[1] = mu2[ii - 1];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, false, mref, mass);
            checks(ep, epC, l, mass);
        }

        SECTION("6D vector completeness with a given reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            EpsilonBasis<T>::squares[0] = mu1[ii - 1];
            EpsilonBasis<T>::squares[1] = mu2[ii - 1];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, true, mref, mass);
            checks(ep, epC, l, mass);
        }
    }
}
TEST_CASE("Completeness relation for cut vector states: 12-D and finite field", "[States]") {
    constexpr unsigned Ds = 12;
    using T = F32;
    T mass = T(5);
    std::vector<momD<T, Ds>> moms = {
        momD<T, Ds>(T(1687660144), T(467026743), T(878600496), T(403256143), T(1989102980), T(2147483628)),
        momD<T, Ds>(T(240307342), T(30867778), T(735701834), T(232443582), T(728932208), T(2147483628)),
        momD<T, Ds>(T(617547705), T(629038752), T(654057503), T(1019826651), T(586795254), T(2147483628)),
        momD<T, Ds>(T(2098083222), T(951841855), T(1091087583), T(1135042903), T(1801595590), T(2147483628)),
    };
    std::vector<T> mu1 = {T(2028296006), T(1582691586), T(324919630), T(746760514)};
    std::vector<T> mu2 = {T(1896020034), T(1488324260), T(527245221), T(1696476052)};
    momD<T, Ds> mref(T(-5), T(10), T(-10), T(-5));
    momD_conf<T, Ds> mc(moms);

    // check relations for all momenta in the given PS point
    for (size_t ii = 1; ii <= mc.n(); ii++) {
        SECTION("6D vector completeness using default reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            EpsilonBasis<T>::squares[0] = mu1[ii - 1];
            EpsilonBasis<T>::squares[1] = mu2[ii - 1];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, false, mref, mass);
            checks(ep, epC, l, mass);
        }

        SECTION("6D vector completeness with a given reference vector (case " + std::to_string(ii) + "/" + std::to_string(mc.n()) + ")") {
            momD<T, Ds> l = mc[ii];
            EpsilonBasis<T>::squares[0] = mu1[ii - 1];
            EpsilonBasis<T>::squares[1] = mu2[ii - 1];
            std::vector<std::vector<T>> ep, epC;
            get_states_D(ep, epC, l, true, mref, mass);
            checks(ep, epC, l, mass);
        }
    }
}
#endif
