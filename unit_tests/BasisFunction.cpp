#include "catch.hpp"

#include <sstream>
#include <iostream>
#include <random>

#include "FunctionSpace/FunctionSpace.h"

using namespace Caravel;
using namespace Caravel::FunctionSpace;

// This is a trivial looking test case. However, this once caused a seg
// fault on the mac due to the copy constructor causing a stack
// overflow. 
TEST_CASE("Copying", "[BasisFunction][Copying]"){

  std::function<std::vector<F32>(const TopologyCoordinates<F32>&, const std::vector<F32>& d_values)> f =
    [](const TopologyCoordinates<F32>&, const std::vector<F32>&){return std::vector<F32>{F32(1)};};

  FunctionSpace::BasisFunction<F32> basis_f(f);

  auto basis_f2 = basis_f;
}
