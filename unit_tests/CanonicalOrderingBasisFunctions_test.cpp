#include "catch.hpp"

#include <string>

#include "IntegralLibrary/CanonicalOrderingBasisFunctions.h"

using namespace Caravel;
using namespace Caravel::Integrals;

TEST_CASE("CanonicalOrderingBasisFunctions", "[Integrals][Canonical][Ordering]"){
    // Define a few basis functions hash strings.
    std::string f1("G<Out>(T(0),s34*T(-1))*G<Out>(T(0),s45*T(-1))");
    std::string f2("G<Out>(T(0),s45*T(-1))*G<Out>(T(0),s34*T(-1))");
    std::string f3("T(1)");
    std::string f4("G<Out>(T(0),s45*T(-1))*G<Out>(T(0),s34*T(-1))*Pi<Out>*Pi<Out>");
    std::string f5("G<Out>(T(0),s45*T(-1)+s34*T(-1))*G<Out>(T(0),s23*T(-1)+s12*T(-1))");

    CHECK(canonical_ordering_hash_string(f1) == std::string("G[T[0],s34*T[-1]]*G[T[0],s45*T[-1]]"));
    CHECK(canonical_ordering_hash_string(f2) == std::string("G[T[0],s34*T[-1]]*G[T[0],s45*T[-1]]"));
    CHECK(canonical_ordering_hash_string(f3) == std::string("T[1]"));
    CHECK(canonical_ordering_hash_string(f4) == std::string("G[T[0],s34*T[-1]]*G[T[0],s45*T[-1]]*Pi*Pi"));
    CHECK(canonical_ordering_hash_string(f5) == std::string("G[T[0],s12*T[-1]+s23*T[-1]]*G[T[0],s34*T[-1]+s45*T[-1]]"));
}
