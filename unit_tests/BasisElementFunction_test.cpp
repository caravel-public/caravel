#include "catch.hpp"

#include <stdexcept>
#include <unordered_map>

#include "IntegralLibrary/BasisElementFunction.h"

using namespace Caravel::Integrals;

TEST_CASE("DummyBasisFunction", "[dummy]"){
    // Create three dummy basis functions:
    DummyBasisFunction dbf1("log(x)");
    DummyBasisFunction dbf2("Li2(y)");
    DummyBasisFunction dbf3("log(x)");

    // Check the string representations
    CHECK(dbf1.as_string() == "log(x)");
    CHECK(dbf2.as_string() == "Li2(y)");

    // Compare
    CHECK((dbf1 == dbf1) == true);
    CHECK((dbf1 == dbf2) == false);
    CHECK((dbf1 == dbf3) == true);

    // Check whether the hash function works in an unordered_map
    std::unordered_map<DummyBasisFunction, int> map;
    map.insert({dbf1, 1});
    CHECK(map.size() == 1);
    map.insert({dbf2, 2});
    CHECK(map.size() == 2);
    map.insert({dbf3, 3});
    CHECK(map.size() == 2);
}

TEST_CASE("StdBasisFunction", "[std]"){

    auto f = [](std::vector<double> args){return args[0];};
    // Create StdBasisFunction
    StdBasisFunction<double, double> b1(f, "args[0]", {"t1"});

    StdBasisFunction<double, double> b2(f, "G(T(0),args[0]*T(-1))*G(T(0),args[1]*T(-1))", {"s", "t"});

    StdBasisFunction<double, double> b3(f, "G(T(0),args[0]*T(-1))*G(T(0),args[1]*T(-1))", {"t", "s"});

    StdBasisFunction<double, double> b4(f, "T(1)", {"t", "s"});

    StdBasisFunction<double, double> p1(f, "f1(args[0])", {"s"});
    StdBasisFunction<double, double> p2(f, "f1(args[0])", {"t"});


    SECTION("Canonical Ordering"){
        CHECK(b2.as_string() == "G[T[0],s*T[-1]]*G[T[0],t*T[-1]]");
        CHECK(b3.as_string() == "G[T[0],s*T[-1]]*G[T[0],t*T[-1]]");
        CHECK(b4.as_string() == "T[1]");
    }

    SECTION("String representation"){
        CHECK(b1.as_string() == "t1");
    }

    SECTION("Evaluation"){
        // Direct evaluation
        CHECK(b1({2}) == 2);
    }

    SECTION("Hashing"){
        std::unordered_map<StdBasisFunction<double, double>, double> map;
        map.insert({b1, 2.});
        CHECK(map.size() == 1);
        CHECK(map.find(b1) != map.end());
        CHECK(map[b1] == Approx(2.));
    }

    SECTION("Products"){
      CHECK((p1*p2).as_string() == "f1[s]*f1[t]");
    }
}
