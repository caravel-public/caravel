#include "catch.hpp"

#include "FunctionalReconstruction/Alphabets/PentagonAlphabetPlanar.h"

#include <functional>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include "AmplitudeCoefficients/NumericalTools.h"
#include "Core/typedefs.h"
#include "FunctionalReconstruction/DensePolynomial.h"
#include "FunctionalReconstruction/DenseRational.h"
#include "FunctionalReconstruction/NewtonFunction.h"
#include "FunctionalReconstruction/ReconstructionTools.h"
#include "FunctionalReconstruction/ThieleFunction.h"
#include "IntegralLibrary/SpecialFunctions/Power.h"

using Caravel::DensePolynomial;
using Caravel::Integrals::prodpow;

using namespace Caravel;
using F = Caravel::F32;

F dummy_function(const std::vector<F>& x) {
    return (x[0] - x[2] + x[3]) * (x[1] + x[3]) / ((F(1) + x[2] - x[3]) * prodpow(x[0] * x[1] - x[1] * x[2] + x[3] + x[1] * x[3], 2));
}


using Caravel::Slice;

TEST_CASE("Alphabet", "[denom][guess][alphabet]") {

    F::set_p(2147483629);

    PentagonAlphabetPlanar alphabet;

    // Check the number of letters in the alphabet.
    CHECK(alphabet.n_letters() == 25);

    // Check the name of the alphabet.
    CHECK(alphabet.name() == "PentagonPlanar");

    // Reconstruct the letters of the alphabet on a slice.
    Slice slice1 = [](const F& x) {
        F t1 = 1714636915, a1 = 1649760492;
        F t2 = 1957747793, a2 = 596516649;
        F t3 = 424238335, a3 = 1189641421;
        F t4 = 719885386, a4 = 1025202362;
        return std::vector<F>({x * t1 - a1, x * t2 - a2, x * t3 - a3, x * t4 - a4});
    };

    auto letters_on_slice = alphabet.reconstruct_on_slice(slice1);

    // Check the degree
    CHECK(letters_on_slice[0].get_degree() == 1);
    CHECK(letters_on_slice[4].get_degree() == 1);
    CHECK(letters_on_slice[24].get_degree() == 4);

    // Reconstruct the Dummy Function on the univariate slice:
    auto evaluator = [&slice1](F x) { return std::vector<F>{dummy_function(slice1(x))}; };
    std::mt19937 mt1(rand());
    std::function<F()> x_gen = [mt1]() mutable { return Caravel::random_scaled_number(F(1), mt1); };
    Caravel::VectorThieleFunction<F> my_rational(std::move(evaluator), x_gen, "x");
    auto result = my_rational.components[0].to_canonical();

    // Print the result:
    std::cout << std::endl << "Reconstructed dummy function on uslice: " << std::endl;
    std::cout << result << std::endl;

    // Guess only the denominator
    auto denominator = alphabet.factorize_letters(result.denominator, slice1);
    CHECK(denominator.as_string() == "((1+x2-x3)*(x0*x1-x1*x2+x3+x1*x3)^2)");

    auto ip1_res = denominator.get_index_powers();
    Caravel::IndexPower ip1_ref({{12, 1}, {17, 2}});

    for (std::size_t i = 0; i < denominator.get_index_powers().size(); ++i) {
        CHECK(ip1_res[i].first == ip1_ref[i].first);
        CHECK(ip1_res[i].second == ip1_ref[i].second);
    }

    // Evaluate the reconstructed denominator
    std::vector<F> point = {F(1), F(2), F(6), F(4), F(8)};
    auto numeric_denom = alphabet.evaluate_letter(denominator, point);
    CHECK(numeric_denom == F(12));

    // Guess the rational function
    auto rat = alphabet.factorize_letters(result, slice1);
    CHECK(rat.as_string() == "(x0-x2+x3)/((1+x2-x3)*(x0*x1-x1*x2+x3+x1*x3)^2)");

    auto ip2_res = rat.get_index_powers();
    Caravel::IndexPower ip2_ref({{13, 1}, {12, -1}, {17, -2}});

    for (std::size_t i = 0; i < rat.get_index_powers().size(); ++i) {
        CHECK(ip2_res[i].first == ip2_ref[i].first);
        CHECK(ip2_res[i].second == ip2_ref[i].second);
    }

    // Check output to MathList
    auto mathlist = rat.as_mathlist();
    CHECK(mathlist ==
          "LetterMonomial[Alphabet[PentagonPlanar],StringRep[(x0-x2+x3)/"
          "((1+x2-x3)*(x0*x1-x1*x2+x3+x1*x3)^2)],IndexPowers[IndexPower[Index[13],Power[1]],IndexPower[Index[12],Power[-1]],IndexPower[Index[17],Power[-2]]]]");

    auto numeric_rat = alphabet.evaluate_letter(rat, point);
    CHECK(numeric_rat == F(178956969));

    // Read LetterMonomial from Mathlist
    LetterMonomial reconstructed(mathlist);

    CHECK(reconstructed == rat);
    CHECK(reconstructed != denominator);
    
    CHECK(reconstructed.get_alphabet_name() == rat.get_alphabet_name());
    CHECK(reconstructed.as_string() == rat.as_string());
    CHECK(reconstructed.as_mathlist() == rat.as_mathlist());

    for (std::size_t i = 0; i < rat.get_index_powers().size(); ++i) {
        CHECK(ip2_res[i].first == reconstructed.get_index_powers()[i].first);
        CHECK(ip2_res[i].second == reconstructed.get_index_powers()[i].second);
    }

    CHECK(alphabet.evaluate_letter(reconstructed, point) == F(178956969));

    // Check writing to stream
    std::ostringstream oss;
    oss << rat;
    CHECK(oss.str() == "(x0-x2+x3)/((1+x2-x3)*(x0*x1-x1*x2+x3+x1*x3)^2)");

    // Write some coefficients to file and read them back:
    std::vector<LetterMonomial> v({denominator, rat});

    write_letter_monomials_to_file("test_denom_guessing_out.txt", v);
    auto read = read_letter_monomials_from_file("test_denom_guessing_out.txt");

    CHECK(read.size() == 2);
    CHECK(v[0] == read[0]);
    CHECK(v[1] == read[1]);

    auto read_num = alphabet.evaluate_letters(read, point);
    CHECK(read_num[0] == F(12));
    CHECK(read_num[1] == F(178956969));
}

TEST_CASE("PentagonInvariantSpace", "[Pentagon][Alphabet][Invariant][Space]") {

    F::set_p(2147483629);

    PentagonAlphabetPlanarInvSpace alphabet;

    F x0(uint64_t(1649760492));
    F x1(uint64_t(5965216649));
    F x2(uint64_t(1189641421));
    F x3(uint64_t(1025202362));

    std::vector<F> input({x0, x1, x2, x3});

    std::cout << "input point: " << input << std::endl;
    std::cout << "alphabet: " << alphabet.letter_strings << std::endl;

    auto eval = alphabet.eval_letters_on_psp(input);

    CHECK(eval.size() == 20);

    CHECK(eval[0]  == F(1189641421));
    CHECK(eval[1]  == F(1601938066)/F(274960082));
    CHECK(eval[2]  == F(1025202362));
    CHECK(eval[3]  == F(868047599));
    CHECK(eval[4]  == F(800560464)/F(274960082));
    CHECK(eval[5]  == F(1893249961));
    CHECK(eval[6]  == F(868047600));
    CHECK(eval[7]  == F(1189641422));
    CHECK(eval[8]  == F(1060291834)/F(274960082));
    CHECK(eval[9]  == F(1122281268));
    CHECK(eval[10] == F(321593822));
    CHECK(eval[11] == F(1326977984)/F(274960082));
    CHECK(eval[12] == F(1983044570));
    CHECK(eval[13] == F(848616691)/F(274960082));
    CHECK(eval[14] == F(1983044569));
    CHECK(eval[15] == F(1390262923)/F(274960082));
    CHECK(eval[16] == F(1621883247)/F(274960082));
    CHECK(eval[17] == F(1443875089));
    CHECK(eval[18] == F(1023906856)/F(274960082));
    CHECK(eval[19] == F(1173817064)/F(771246576));    
}


