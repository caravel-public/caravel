#include "catch.hpp"

#include <string>

#include "Core/typedefs.h"
#include "FunctionalReconstruction/RationalFlattener.h"

using F = Caravel::F32;

using namespace Caravel;
using namespace Caravel::Reconstruction;

TEST_CASE("RationalFlattener_equal_no_zeros", "[equal][no_zeros]") {
    auto rational1 = unflatten(5, 4, std::vector<F>({F(1), F(7), F(2), F(3), F(4), F(5), F(3), F(2), F(1)}));
    auto rational2 = unflatten(5, 4, std::vector<F>({F(4), F(7), F(3), F(2), F(1), F(5), F(3), F(2), F(1)}));

    auto flattener1 = make_rational_flattener(std::vector<DenseRational<F>>({rational1}));
    auto flattener2 = make_rational_flattener(std::vector<DenseRational<F>>({rational2}));

    CHECK(flattener2.flattened_output_size() == 9);
    CHECK(flattener1.number_of_zeros() == 0);
    CHECK(flattener1 == flattener2);
}

TEST_CASE("RationalFlattener_equal_with_zeros", "[equal][with_zeros]") {
    auto rational1 = unflatten(5, 4, std::vector<F>({F(1), F(7), F(2), F(0), F(4), F(5), F(0), F(2), F(1)}));
    auto rational2 = unflatten(5, 4, std::vector<F>({F(4), F(7), F(3), F(0), F(1), F(5), F(0), F(2), F(1)}));

    auto flattener1 = make_rational_flattener(std::vector<DenseRational<F>>({rational1}));
    auto flattener2 = make_rational_flattener(std::vector<DenseRational<F>>({rational2}));

    CHECK(flattener2.flattened_output_size() == 7);
    CHECK(flattener1.number_of_zeros() == 2);
    CHECK(flattener1 == flattener2);
}

TEST_CASE("RationalFlattener_not_equal_with_zeros", "[equal][with_zeros]") {
    auto rational1 = unflatten(5, 4, std::vector<F>({F(1), F(7), F(2), F(0), F(4), F(5), F(0), F(2), F(1)}));
    auto rational2 = unflatten(5, 4, std::vector<F>({F(4), F(7), F(0), F(3), F(1), F(5), F(0), F(2), F(1)}));

    auto flattener1 = make_rational_flattener(std::vector<DenseRational<F>>({rational1}));
    auto flattener2 = make_rational_flattener(std::vector<DenseRational<F>>({rational2}));

    CHECK(flattener2.flattened_output_size() == 7);
    CHECK(flattener1.number_of_zeros() == 2);
    CHECK(flattener1 != flattener2);
}

TEST_CASE("RationalFlattener_several_equal_with_zeros", "[equal][with_zeros]") {
    auto rational1 = unflatten(5, 4, std::vector<F>({F(1), F(7), F(2), F(1), F(4), F(5), F(2), F(2), F(1)}));
    auto rational2 = unflatten(5, 4, std::vector<F>({F(4), F(7), F(0), F(3), F(1), F(5), F(0), F(2), F(1)}));

    auto rational3 = unflatten(5, 4, std::vector<F>({F(6), F(3), F(1), F(7), F(4), F(5), F(2), F(2), F(1)}));
    auto rational4 = unflatten(5, 4, std::vector<F>({F(4), F(8), F(0), F(3), F(8), F(5), F(0), F(11), F(4)}));

    auto flattener1 = make_rational_flattener(std::vector<DenseRational<F>>({rational1, rational2}));
    auto flattener2 = make_rational_flattener(std::vector<DenseRational<F>>({rational3, rational4}));

    CHECK(flattener2.flattened_output_size() == 16);
    CHECK(flattener1.number_of_zeros() == 2);
    CHECK(flattener1 == flattener2);
}

TEST_CASE("RationalFlattener_several_not_equal_with_zeros", "[equal][with_zeros]") {
    auto rational1 = unflatten(5, 4, std::vector<F>({F(1), F(7), F(2), F(1), F(4), F(5), F(2), F(2), F(1)}));
    auto rational2 = unflatten(5, 4, std::vector<F>({F(4), F(0), F(7), F(3), F(1), F(5), F(0), F(2), F(1)}));

    auto rational3 = unflatten(5, 4, std::vector<F>({F(6), F(3), F(1), F(7), F(4), F(5), F(2), F(2), F(1)}));
    auto rational4 = unflatten(5, 4, std::vector<F>({F(4), F(8), F(0), F(3), F(8), F(5), F(0), F(11), F(4)}));

    auto flattener1 = make_rational_flattener(std::vector<DenseRational<F>>({rational1, rational2}));
    auto flattener2 = make_rational_flattener(std::vector<DenseRational<F>>({rational3, rational4}));

    CHECK(flattener2.flattened_output_size() == 16);
    CHECK(flattener1.number_of_zeros() == 2);
    CHECK(flattener1 != flattener2);
}

TEST_CASE("RationalFlattener_call", "[equal][with_zeros][call]") {
    auto rational1 = unflatten(5, 4, std::vector<F>({F(1), F(7), F(2), F(1), F(4), F(5), F(2), F(2), F(1)}));
    auto rational2 = unflatten(5, 4, std::vector<F>({F(4), F(7), F(0), F(3), F(1), F(5), F(0), F(2), F(1)}));

    auto flattener = make_rational_flattener(std::vector<DenseRational<F>>({rational1, rational2}));

    auto flat = flattener(std::vector<DenseRational<F>>({rational1, rational2}));

    CHECK(flat[0] == F(1));
    CHECK(flat[1] == F(7));
    CHECK(flat[2] == F(2));
    CHECK(flat[3] == F(1));
    CHECK(flat[4] == F(4));
    CHECK(flat[5] == F(5));
    CHECK(flat[6] == F(2));
    CHECK(flat[7] == F(2));
    CHECK(flat[8] == F(1));
    CHECK(flat[9] == F(4));
    CHECK(flat[10] == F(7));
    CHECK(flat[11] == F(3));
    CHECK(flat[12] == F(1));
    CHECK(flat[13] == F(5));
    CHECK(flat[14] == F(2));
    CHECK(flat[15] == F(1));
}

// Need to create own string class that can be constructed from int 
// in order to be able to create DenseRational<MyString>.
class MyString {
  public:
    MyString(): _string{"0"} {};
    MyString(int i) : _string{std::to_string(i)} {}
    MyString(const std::string& s) : _string{s} {}
    MyString(std::string&& s) : _string{std::move(s)} {}
    std::string operator()() const { return _string; }
  private:
    std::string _string;
};

bool operator==(const MyString& s1, const MyString& s2) { return s1() == s2(); }
bool operator!=(const MyString& s1, const MyString& s2) { return !(s1 == s2); }

std::ostream& operator<<(std::ostream& stream, const MyString& s){
    return stream << s();
}

/*TEST_CASE("Unflatten", "[unflatten]") {

    auto rational1 = unflatten(3, 4, std::vector<F>({F(1), F(1), F(2), F(1), F(0), F(0), F(3)}));
    auto rational2 = unflatten(2, 1, std::vector<F>({F(0), F(3), F(2)}));

    auto flattener = make_rational_flattener(std::vector<DenseRational<F>>({rational1, rational2}));

    auto flat = std::vector<MyString>({MyString("1"), MyString("1"), MyString("2"), MyString("1"), MyString("3"), MyString("3"), MyString("2")});
    // auto flat = std::vector<int>({1, 1, 2, 1, 3, 3, 2});

    auto unflattened = flattener.unflatten(flat);

    auto target1 = unflatten(3, 4, std::vector<MyString>({MyString(1), MyString(1), MyString(2), MyString(1), MyString(0), MyString(0), MyString(3)}));
    auto target2 = unflatten(2, 1, std::vector<MyString>({MyString("0"), MyString("3"), MyString("2")}));
    
    // auto target1 = unflatten(3, 4, std::vector<int>({1,1,2,1,0,0,3}));
    // auto target2 = unflatten(2, 1, std::vector<int>({0,3,2}));

    // std::cout << unflattened.size() << std::endl;
    CHECK(unflattened[0] == target1);
    CHECK(unflattened[1] == target2);
}*/
