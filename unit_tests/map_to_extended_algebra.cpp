#include "catch.hpp"
#include "Core/typedefs.h"
#include "Core/Utilities.h"
#include "Core/Debug.h"

#include "Core/extended_t.h"

#include "compare_tools.hpp"

using namespace std;
using namespace Caravel;
using namespace tests;

TEST_CASE("Map from momenta in modified basis to extended algebra", "[mapped momenta]") {
    using T = F32;
    using Tcur = extended_t<T>;
    constexpr size_t Ds = 8;

    momentumD<T,Ds> l1(T(1),T(1),T(3),T(3),T(10),T(-2),T(100),T(1297));
    momentumD<T,Ds> l2(T(2),T(1),T(6),T(-93),T(1249),T(-2213),T(175),T(167));


    EpsilonBasis<T>::squares[0] = T(120);
    EpsilonBasis<T>::squares[1] = T(-5);


    momentumD<Tcur,Ds> ml1{to_extended_t<std::array<Tcur,Ds>,Ds>(l1)};
    momentumD<Tcur,Ds> ml2{to_extended_t<std::array<Tcur,Ds>,Ds>(l2)};

    CHECK_ZERO(l1*l1 - get_e0(ml1*ml1));
    CHECK_ZERO(l2*l2 - get_e0(ml2*ml2));
    CHECK_ZERO(l1*l2 - get_e0(ml1*ml2));
}
