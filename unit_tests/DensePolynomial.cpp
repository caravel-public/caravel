#include "catch.hpp"
#include "FunctionalReconstruction/DensePolynomial.h"
#include "Core/typedefs.h"

#include "compare_tools.hpp"

using namespace Caravel;
using namespace std;


TEST_CASE("Multiplication and division","[]"){
    DensePolynomial<F32> p1({5,9,-60,19,400,12});
    DensePolynomial<F32> p2({0,7,45,-129});

    DensePolynomial<F32> p3({0,1,123,2,6,7});

    CHECK(polynomial_divide(p1,p1).first == DensePolynomial<F32>(std::vector<F32>{1}));

    {
        auto p12 = p1*p2;
        
        CHECK(polynomial_divide(p12,p1).first == p2);
        CHECK(polynomial_divide(p12,p2).first == p1);

    }

    auto r = polynomial_divide(p3,p2);
    CHECK(p3 == r.first*p2+r.second);
}

TEST_CASE("GCD","[]"){
    DensePolynomial<F32> p1({5,9,60,10,78888,12});
    DensePolynomial<F32> p2({10,7,45,-129});
    DensePolynomial<F32> p3({1,1});

    CHECK(polynomial_GCD(p1,p1) == p1);

    CHECK(polynomial_GCD(p1,p3) == polynomial_GCD(p3,p1));

    CHECK(polynomial_GCD(p1*p2,p1) == p1);
    CHECK(polynomial_GCD(p1*p2,p2) == p2);

    CHECK(polynomial_GCD(p1,p2) == polynomial_GCD(p2,p1+p3*p2));
}
