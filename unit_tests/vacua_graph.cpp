/**
 * vacua_graph.cpp
 *
 * First created on 25.2.2019 from a big test; unit test from 2.7.2019
 *
 * Building a function to create multi-loop vacuum graphs
 *
 */

#include "catch.hpp"

#include "AmpEng/Diagrammatica.h"

using namespace Caravel;
using namespace lGraph;
using namespace AmpEng;

TEST_CASE("Vacuum graphs for 1-loop graphs", "[Vacua]") {
    // Construct all vacuum graphs with n loops
    Vacua oneLoops(1);
    CHECK(oneLoops.get_all_vacuum_graphs().size()==1);    // All 1-loop vacum graphs have a single node
    size_t all_vaccum_graphs(0);
    size_t local = oneLoops.get_vacuum_graphs(1).size();
    all_vaccum_graphs+=local;
    CHECK(oneLoops.get_vacuum_graphs(1).size()==1);       // just the bubble
    CHECK(all_vaccum_graphs==1);
}

TEST_CASE("Vacuum graphs for 2-loop graphs", "[Vacua]") {
    // Construct all vacuum graphs with n loops
    Vacua twoLoops(2);
    CHECK(twoLoops.get_all_vacuum_graphs().size()==2);    // 2-loop vacum graphs have one or two nodes
    size_t all_vaccum_graphs(0);

    size_t local = twoLoops.get_vacuum_graphs(1).size();
    all_vaccum_graphs+=local;
    CHECK(twoLoops.get_vacuum_graphs(1).size()==1);       // one graph with one node

    local = twoLoops.get_vacuum_graphs(2).size();
    all_vaccum_graphs+=local;
    CHECK(twoLoops.get_vacuum_graphs(2).size()==1);       // one graph with two nodes

    CHECK(all_vaccum_graphs==2);
}

TEST_CASE("Vacuum graphs for 3-loop graphs", "[Vacua]") {
    // Construct all vacuum graphs with n loops
    Vacua threeLoops(3);
    CHECK(threeLoops.get_all_vacuum_graphs().size()==4);    // 2-loop vacum graphs have one or two nodes
    size_t all_vaccum_graphs(0);

    size_t local = threeLoops.get_vacuum_graphs(1).size();
    all_vaccum_graphs+=local;
    CHECK(threeLoops.get_vacuum_graphs(1).size()==1);       // one graph with one node

    local = threeLoops.get_vacuum_graphs(2).size();
    all_vaccum_graphs+=local;
    CHECK(threeLoops.get_vacuum_graphs(2).size()==3);       // three graphs with two nodes

    local = threeLoops.get_vacuum_graphs(3).size();
    all_vaccum_graphs+=local;
    CHECK(threeLoops.get_vacuum_graphs(3).size()==2);       // two graphs with three nodes

    local = threeLoops.get_vacuum_graphs(4).size();
    all_vaccum_graphs+=local;
    CHECK(threeLoops.get_vacuum_graphs(4).size()==2);       // two graphs with four nodes (mercedes & ladder)

    CHECK(all_vaccum_graphs==8);
}

// WARNING: 4-loop numbers unchecked!
TEST_CASE("Vacuum graphs for 4-loop graphs", "[Vacua]") {
    // Construct all vacuum graphs with n loops
    Vacua fourLoops(4);
    CHECK(fourLoops.get_all_vacuum_graphs().size()==6);    
    size_t all_vaccum_graphs(0);

    size_t local = fourLoops.get_vacuum_graphs(1).size();
    all_vaccum_graphs+=local;
    CHECK(fourLoops.get_vacuum_graphs(1).size()==1);       

    local = fourLoops.get_vacuum_graphs(2).size();
    all_vaccum_graphs+=local;
    CHECK(fourLoops.get_vacuum_graphs(2).size()==5);       

    local = fourLoops.get_vacuum_graphs(3).size();
    all_vaccum_graphs+=local;
    CHECK(fourLoops.get_vacuum_graphs(3).size()==11);       

    local = fourLoops.get_vacuum_graphs(4).size();
    all_vaccum_graphs+=local;
    CHECK(fourLoops.get_vacuum_graphs(4).size()==16);       

    local = fourLoops.get_vacuum_graphs(5).size();
    all_vaccum_graphs+=local;
    CHECK(fourLoops.get_vacuum_graphs(5).size()==10);       

    local = fourLoops.get_vacuum_graphs(6).size();
    all_vaccum_graphs+=local;
    CHECK(fourLoops.get_vacuum_graphs(6).size()==6);       

    CHECK(all_vaccum_graphs==49);
}

// WARNING: 5-loop numbers unchecked!
#if 0
// We comment away as in oslo it takes about 50 seconds to run (so too long for unit test)
TEST_CASE("Vacuum graphs for 5-loop graphs", "[Vacua]") {
    // Construct all vacuum graphs with n loops
    Vacua fiveLoops(5);
    CHECK(fiveLoops.get_all_vacuum_graphs().size()==8);    
    size_t all_vaccum_graphs(0);

    size_t local = fiveLoops.get_vacuum_graphs(1).size();
    all_vaccum_graphs+=local;
    CHECK(fiveLoops.get_vacuum_graphs(1).size()==1);       

    local = fiveLoops.get_vacuum_graphs(2).size();
    all_vaccum_graphs+=local;
    CHECK(fiveLoops.get_vacuum_graphs(2).size()==8);       

    local = fiveLoops.get_vacuum_graphs(3).size();
    all_vaccum_graphs+=local;
    CHECK(fiveLoops.get_vacuum_graphs(3).size()==28);       

    local = fiveLoops.get_vacuum_graphs(4).size();
    all_vaccum_graphs+=local;
    CHECK(fiveLoops.get_vacuum_graphs(4).size()==78);       

    local = fiveLoops.get_vacuum_graphs(5).size();
    all_vaccum_graphs+=local;
    CHECK(fiveLoops.get_vacuum_graphs(5).size()==117);       

    local = fiveLoops.get_vacuum_graphs(6).size();
    all_vaccum_graphs+=local;
    CHECK(fiveLoops.get_vacuum_graphs(6).size()==112);       

    local = fiveLoops.get_vacuum_graphs(7).size();
    all_vaccum_graphs+=local;
    CHECK(fiveLoops.get_vacuum_graphs(7).size()==54);       

    local = fiveLoops.get_vacuum_graphs(8).size();
    all_vaccum_graphs+=local;
    CHECK(fiveLoops.get_vacuum_graphs(8).size()==20);       

    CHECK(all_vaccum_graphs==418);
}
#endif
