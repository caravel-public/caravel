#include "Core/spinor/clifford_algebra.h"
#include "catch.hpp"
#include <tuple>
#include <random>
#include "compare_tools.hpp"

namespace std{

    // some linear algebraic stuff with vectors
    template <typename T> std::vector<T>& operator+=(std::vector<T>& v1, const std::vector<T>& v2){
        std::transform(v1.begin(),v1.end(),v2.begin(),v1.begin(),std::plus<T>());
        return v1;
    }
    template <typename T> std::vector<T>& operator-=(std::vector<T>& v1, const std::vector<T>& v2){
        std::transform(v1.begin(),v1.end(),v2.begin(),v1.begin(),std::minus<T>());
        return v1;
    }
    template <typename T> std::vector<T> operator-=(std::vector<T>&& v1, const std::vector<T>& v2){
        std::transform(v1.begin(),v1.end(),v2.begin(),v1.begin(),std::minus<T>());
        return std::move(v1);
    }
    template <typename T> std::vector<T>& operator*=(std::vector<T>& v1, const T& s){
        using namespace std::placeholders;
        std::transform(v1.begin(), v1.end(), v1.begin(), std::bind(std::multiplies<T>(),s,_1));
        return v1;
    }
    template <typename T> std::vector<T>& operator/=(std::vector<T>& v1, const T& s){
        using namespace std::placeholders;
        std::transform(v1.begin(), v1.end(), v1.begin(), std::bind(std::divides<T>(),_1, s));
        return v1;
    }

}

using namespace Caravel;
using namespace clifford_algebra;
using namespace clifford_algebra::_internal;


using std::complex;
using std::get;
using std::tuple;
using std::make_tuple;
using std::vector;

template <unsigned Ds, typename T> class Gamma_explicit;
template <unsigned Ds, typename T> Gamma_explicit<Ds,T> operator* (const Gamma_explicit<Ds,T>& m1, const Gamma_explicit<Ds,T>& m2);

template <unsigned Ds, typename T> class Gamma_explicit : public Gamma<Ds>{
    public:
        using Gamma<Ds>::Dtt;

        Gamma_explicit(const unsigned i);
        Gamma_explicit() : Gamma<Ds>() {};
        Gamma_explicit(Identity* id);

        //! 0 based
        const T& operator()(unsigned i, unsigned j) const{ return elements[Dtt*i+j]; }

        Gamma_explicit<Ds,T>& operator+=(const Gamma_explicit<Ds,T>& m);
        Gamma_explicit<Ds,T>& operator-=(const Gamma_explicit<Ds,T>& m);

    private:
        std::array<T,Dtt*Dtt> elements{};
        T& el(unsigned i, unsigned j){ return elements[Dtt*i+j]; } 
        friend Gamma_explicit<Ds,T> operator* <>(const Gamma_explicit<Ds,T>& m1, const Gamma_explicit<Ds,T>& m2);
};


template <unsigned Ds, typename T> Gamma_explicit<Ds,T>::Gamma_explicit(const unsigned i) : Gamma<Ds>(i) {
    for (unsigned i=0; i<Dtt; i++){
        for (unsigned j=0; j<Dtt; j++){
            el(i,j) = explicit_sEntry<T>(Gamma<Ds>::entry(i+1,j+1));
        }
    }
}

template <unsigned Ds, typename T> Gamma_explicit<Ds,T>::Gamma_explicit(Identity* id) : Gamma<Ds>(id) {
    for (unsigned i=0; i<Dtt; i++){
        for (unsigned j=0; j<Dtt; j++){
            el(i,j) = explicit_sEntry<T>(Gamma<Ds>::entry(i+1,j+1));
        }
    }
}

template <unsigned Ds, typename T> Gamma_explicit<Ds,T>& Gamma_explicit<Ds,T>::operator+=(const Gamma_explicit<Ds,T>& m){
    for (unsigned i=0; i<Dtt; i++){
        for (unsigned j=0; j<Dtt; j++){
            el(i,j) += m(i,j);
        }
    }
    return *this;
}
template <unsigned Ds, typename T> Gamma_explicit<Ds,T>& Gamma_explicit<Ds,T>::operator-=(const Gamma_explicit<Ds,T>& m){
    for (unsigned i=0; i<Dtt; i++){
        for (unsigned j=0; j<Dtt; j++){
            el(i,j) -= m(i,j);
        }
    }
    return *this;
}

template <unsigned Ds, typename T> Gamma_explicit<Ds,T> operator* (const Gamma_explicit<Ds,T>& m1, const Gamma_explicit<Ds,T>& m2){
    Gamma_explicit<Ds,T> res;

    for(unsigned i=0;i<res.Dtt;i++){
        for(unsigned j=0;j<res.Dtt;j++){
            for(unsigned k=0;k<res.Dtt;k++){
                res.el(i,j) += m1(i,k)*m2(k,j);
            }
        }
    }

    return res;
}

template <unsigned Ds, typename T> Gamma_explicit<Ds,T> operator+ (Gamma_explicit<Ds,T> m1, const Gamma_explicit<Ds,T>& m2){ return m1+=m2; }
template <unsigned Ds, typename T> Gamma_explicit<Ds,T> operator- (Gamma_explicit<Ds,T> m1, const Gamma_explicit<Ds,T>& m2){ return m1-=m2; }



namespace std{
    template <unsigned Ds, typename T> std::ostream& operator<<(std::ostream& s, const Gamma_explicit<Ds,T>& g){
        constexpr unsigned limit = g.Dtt;
        for(unsigned ii=0;ii<limit;ii++){
            //s<<"\t";
            for(unsigned jj=0;jj<limit;jj++) s<< g(ii,jj)<<" ";
            s<<"\n";
        }
        return s;
    }
}

/**
 * Function template to extract entries in a product of two Gamma<D>'s.
 * This function is only as a testing tool!
 *
 * @param unsigned representing the row of the entry requested
 * @param unsigned representing the column of the entry requested
 * @param Gamma<D> matrix in the left of the product
 * @param Gamma<D> matrix in the right of the product
 */
template <typename T,unsigned D> T gamma_gamma_multiply(unsigned k,unsigned l,const Gamma<D>& A,const Gamma<D>& B){
    T toret(0,0);
    const unsigned& limit = Gamma<D>::Dtt;
    for(unsigned m=0;m<limit;m++){
        sEntry local(sEntry_multiply(A.entry(k+1,m+1),B.entry(m+1,l+1)));
        if(local==sEntry::One)
            toret+=T(1);
        else if(local==sEntry::mOne)
            toret-=T(1);
        else if(local==sEntry::I)
            toret=T(0,1);
        else if(local==sEntry::mI)
            toret-=T(0,1);
        //std::cout<<"toret: "<<toret<<" k,l: "<<k<<","<<l<<" local sEntry: "<<local<<std::endl;
    }
    return toret;
}


template <typename T, unsigned D> bool is_zero(const Gamma_explicit<D,T>& g1){
    for(unsigned i=0;i<g1.Dtt;i++){
        for(unsigned j=0;j<g1.Dtt;j++){
            if (g1(i,j)!=T(0)) return false;
        }
    }
    return true;
}

template <typename T, unsigned D> Gamma_explicit<D,T> anti_commute(const Gamma_explicit<D,T>& g1, const Gamma_explicit<D,T>& g2){
    return (g1*g2)+=(g2*g1);
}


template <unsigned D> std::vector<std::tuple<unsigned,unsigned>> get_partitions(unsigned first=0, unsigned last=D-1){
    assert(last>first);
    assert(last-first+1<=D);

    std::vector<std::tuple<unsigned,unsigned>> ret;

    for(unsigned i=first;i<=last-1;i++){
        for(unsigned j=i+1;j<=last;j++){
            ret.emplace_back(i,j);
        }
    }

    return ret;
}


TEST_CASE("Dtt Gamma matrices in Ds dimensions [float]","[GammaDs]"){

    typedef complex<double> T;

    SECTION("Algebra properties in Ds=6"){
        constexpr unsigned Ds = 6;
        Gamma_explicit<Ds,T> id((Identity*)nullptr);

        sparse_smatrix<Ds,T,true>::init();
        sparse_smatrix<Ds,T,false>::init();

        vector<Gamma_explicit<Ds,T>> gs;
        Gamma_explicit<Ds,T> gstar(Ds);

        for(unsigned i=4;i<Ds;i++){
            gs.emplace_back(i);
        }

        REQUIRE(is_zero((gstar*gstar)-=id));
        for(auto& it:gs){
            REQUIRE(is_zero((it*it)+=id));
            REQUIRE(is_zero(anti_commute(it,gstar)));
        }

        auto part = get_partitions<Ds-4>();

        for(auto& it: part){
            REQUIRE(is_zero(anti_commute(gs.at(get<0>(it)),gs.at(get<1>(it)))));
        }
    }

    SECTION("Algebra properties in Ds=8"){
        constexpr unsigned Ds = 8;
        Gamma_explicit<Ds,T> id((Identity*)nullptr);

        sparse_smatrix<Ds,T,true>::init();
        sparse_smatrix<Ds,T,false>::init();

        vector<Gamma_explicit<Ds,T>> gs;
        Gamma_explicit<Ds,T> gstar(Ds);

        for(unsigned i=4;i<Ds;i++){
            gs.emplace_back(i);
        }

        REQUIRE(is_zero((gstar*gstar)-=id));
        for(auto& it:gs){
            REQUIRE(is_zero((it*it)+=id));
            REQUIRE(is_zero(anti_commute(it,gstar)));
        }

        auto part = get_partitions<Ds-4>();

        for(auto& it: part){
            REQUIRE(is_zero(anti_commute(gs.at(get<0>(it)),gs.at(get<1>(it)))));
        }
    }

    SECTION("Algebra properties in Ds=10"){
        constexpr unsigned Ds = 10;
        Gamma_explicit<Ds,T> id((Identity*)nullptr);

        sparse_smatrix<Ds,T,true>::init();
        sparse_smatrix<Ds,T,false>::init();

        vector<Gamma_explicit<Ds,T>> gs;
        Gamma_explicit<Ds,T> gstar(Ds);

        for(unsigned i=4;i<Ds;i++){
            gs.emplace_back(i);
        }

        REQUIRE(is_zero((gstar*gstar)-=id));
        for(auto& it:gs){
            REQUIRE(is_zero((it*it)+=id));
            REQUIRE(is_zero(anti_commute(it,gstar)));
        }

        auto part = get_partitions<Ds-4>();

        for(auto& it: part){
            REQUIRE(is_zero(anti_commute(gs.at(get<0>(it)),gs.at(get<1>(it)))));
        }
    }


}

TEST_CASE("Sparse slashed matrix operations [float]","[Sparse smatrix]"){
    typedef complex<double> T;

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 engine(340); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dist(-5, 5);
    auto gen1 = std::bind(dist, engine);

    auto gen = [&](){return T(gen1(),gen1());};

    constexpr unsigned Ds = 10;
    constexpr unsigned Dt = two_to_the(Ds/2-1);

    std::vector<T> in(Dt);
    std::vector<T> zero(Dt,0);
    std::generate(in.begin(),in.end(),gen);

    std::vector<T> momDsm4_components(Ds-4);
    std::generate(momDsm4_components.begin(),momDsm4_components.end(),gen);
    momDsm4_components.insert(momDsm4_components.begin(),4,0);
    vector<T> momD_components(Ds);
    vector<T> momD_components2(Ds);
    std::generate(momD_components.begin(),momD_components.end(),gen);
    std::generate(momD_components2.begin(),momD_components2.end(),gen);

    momentumD<T,Ds> l4d(gen(),gen(),gen(),gen());
    momentumD<T,Ds> lDsm4(momDsm4_components);
    momentumD<T,Ds> lDs(momD_components);
    momentumD<T,Ds> lDs2(momD_components2);

    SECTION("smatrix(l)^2 = l^2"){

        auto appl = [&](auto& l, auto& in, auto& f1, auto& f2){
            auto out = f2(l.get_vector(),f1(l.get_vector(),in));
            out/=(l*l);
            return out;
        };
        CHECK_ZERO(appl(l4d,in,multiply_4D_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds,T>,    multiply_4D_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds,T>) -= in);
        CHECK_ZERO(appl(l4d,in,multiply_4D_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds,T>,   multiply_4D_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds,T>) -= in);
        CHECK_ZERO(appl(l4d,in,multiply_4D_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds,T>,    multiply_4D_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds,T>) -= in);
        CHECK_ZERO(appl(l4d,in,multiply_4D_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds,T>,   multiply_4D_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds,T>) -= in);

        CHECK_ZERO(appl(lDs,in,multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds,T,decltype(lDs)>,multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds,T, decltype(lDs)>) -= in);
        CHECK_ZERO(appl(lDs,in,multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds,T, decltype(lDs)>,multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds,T,decltype(lDs)>) -= in);
        CHECK_ZERO(appl(lDs,in,multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds,T,decltype(lDs)>,multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds,T, decltype(lDs)>) -= in);
        CHECK_ZERO(appl(lDs,in,multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds,T, decltype(lDs)>,multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds,T,decltype(lDs)>) -= in);

    }

    std::generate(in.begin(),in.end(),gen);

    SECTION("sm(l1)*sm(l2) + sm(l2)*sm(l1) = 2(l2*l1)"){

        auto appl = [&](auto& l1,auto& l2, auto& in, auto& f1, auto& f2){
            auto out = f2(l2.get_vector(),f1(l1.get_vector(),in));
            out += f2(l1.get_vector(),f1(l2.get_vector(),in));
            out/=(T(2)*(l1*l2));
            return out;
        };

        CHECK_ZERO(appl(lDs,lDs2,in,multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds,T, decltype(lDs)>,    multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds,T,decltype(lDs)>) -= in);
        CHECK_ZERO(appl(lDs,lDs2,in,multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds,T,decltype(lDs)>,   multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds,T,  decltype(lDs)>) -= in);
        CHECK_ZERO(appl(lDs,lDs2,in,multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds,T, decltype(lDs)>,    multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds,T,decltype(lDs)>) -= in);
        CHECK_ZERO(appl(lDs,lDs2,in,multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds,T,decltype(lDs)>,   multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds,T,  decltype(lDs)>) -= in);

    }

    SECTION("consistency with spinor library"){
        smatrix<T> sm(lDs2);
        la<T> la1(in.at(0),in.at(1));
        lat<T> lat1(in.at(0),in.at(1));

        auto out = multiply_4D_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds,T>(lDs2,in);
        auto out2 = (sm*la1).get_vector();

        CHECK_ZERO(out[0] - out2[0]);
        CHECK_ZERO(out[1] - out2[1]);

        out = multiply_4D_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds,T>(lDs2,in);
        out2 = (sm*lat1).get_vector();

        CHECK_ZERO(out[0] - out2[0]);
        CHECK_ZERO(out[1] - out2[1]);

        out = multiply_4D_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds,T>(lDs2,in);
        out2 = (la1*sm).get_vector();

        CHECK_ZERO(out[0] - out2[0]);
        CHECK_ZERO(out[1] - out2[1]);

        out = multiply_4D_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds,T>(lDs2,in);
        out2 = (lat1*sm).get_vector();

        CHECK_ZERO(out[0] - out2[0]);
        CHECK_ZERO(out[1] - out2[1]);
    }
}

using namespace tests;

TEST_CASE("States on the cut [float]","[States]"){
    typedef complex<double> T;

    constexpr unsigned Ds = 8;
    constexpr unsigned Dt = two_to_the(Ds/2-1);

    Caravel::momentumD<T,Ds> lDs(T(1),T(4),T(0),T(0),T(1),T(0,4),T(3),T(0,-3));
    Caravel::momentumD<T,Ds> lDs2(T(1),T(-4),T(-10),T(0,10),T(1),T(0,4));
    Caravel::momentumD<T,Ds> l4d(T(1),T(-1),T(3),T(0,3),T(0),T(0));

    REQUIRE_ZERO(lDs*lDs);
    REQUIRE_ZERO(lDs2*lDs2);
    REQUIRE_ZERO(l4d*l4d);

    SECTION("Light cone decomposition"){
        auto lfl = lightcone_decompose(lDs);
        REQUIRE(is_zero(lfl*lfl));

        lfl = lightcone_decompose(l4d);
        REQUIRE(is_zero(Caravel::momentumD<T,Ds>(lfl)-l4d));
    }

    std::cout.precision(2);

    SECTION("Dirac equation"){
#define ______check(N)\
        {\
            auto q = cut_WF_q<repr::La,N,T>(lDs);\
            auto qb = cut_WF_qb<repr::Lat,N,T>(lDs);\
            CHECK( is_zero(multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds>(lDs.get_vector(),q) ));\
            CHECK( is_zero(multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds>(lDs.get_vector(),qb) ));\
        }\

        ______check(1);
        ______check(2);
        ______check(3);
        ______check(4);
#undef ______check
    }

    SECTION("Completeness relation"){
        vector<T> in{T(4),T(-2),T(1),T(0),T(5,-2),T(0),T(0,1),T(1,1)};
        //vector<T> in{T(1),T(2),T(0),T(0),T(0),T(0),T(0),T(0),};

        vector<vector<T>> qs;
        vector<vector<T>> qbs;

#define __do(N)\
        qs.push_back(cut_WF_q<repr::La,N,T>(lDs));\
        qbs.push_back(cut_WF_qb<repr::Lat,N,T>(-lDs));\

        __do(1);
        __do(2);
        __do(3);
        __do(4);
#undef __do

        {
            auto out1 = multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds>(lDs.get_vector(),in);

            vector<T> out2(Dt,T(0));

            for(unsigned i=0; i<Dt/2; i++){
                T c(0);
                unroll_sp_prod<repr::La,Dt>(qs.at(i),in,c,int_<Dt/2>{});
                auto v = qbs.at(i);
                v*=c;
                out2+=v;
            }

            CHECK(is_zero(out1-=out2));
        }
        {
            auto out1 = multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds>(lDs.get_vector(),in);

            vector<T> out2(Dt,T(0));

            for(unsigned i=0; i<Dt/2; i++){
                T c(0);
                unroll_sp_prod<repr::Lat,Dt>(in,qbs.at(i),c,int_<Dt/2>{});
                auto v = qs.at(i);
                v*=c;
                out2+=v;
            }

            CHECK(is_zero(out1-=out2));
        }
    }

    SECTION("Vector from spinors"){
        {

            auto q = cut_WF_q<repr::La,2,T>(lDs);
            auto qb = cut_WF_qb<repr::Lat,2,T>(-lDs);

            auto v = LvAB_Ds<Ds>(q,qb);
            momentumD<T,Ds> m(v);

            CHECK(is_zero(m*m));
            CHECK(is_zero(m*lDs));
            CHECK(is_zero(lDs-m));
        }

        {
            auto q = cut_WF_q<repr::La,3,T>(lDs);
            auto qb = cut_WF_qb<repr::Lat,3,T>(lDs2);

            auto v = LvAB_Ds<Ds>(q,qb);
            momentumD<T,Ds> m(v);

            CHECK(is_zero((m*lDs)/abs(_spinor_private::plus(lDs))));
            CHECK(is_zero((m*lDs2)/abs(_spinor_private::plus(lDs2))));
        }

    }

}
