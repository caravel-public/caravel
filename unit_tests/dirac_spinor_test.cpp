#include "Core/spinor/dirac_algebra.h"
#include "Core/spinor/DSpinor.h"
#include "catch.hpp"
#include "compare_tools.hpp"
#include <iostream>

 
using namespace std;
using namespace Caravel;
using namespace clifford_algebra;

bool is_my_zero( C x ){
 const R eps = R(1e-10);
 R z1 = abs(x.real());
 R z2 = abs(x.imag());
 if( z1 > eps || z2 > eps ) cout << z1 << "\t" << z2 << endl;
 if( z1 > eps || z2 > eps ) return false;
 else return true;
}

// Return (p_slash - m)_ij
template<typename T, size_t Ds> std::vector<std::vector<T>> polsum_explicit(const momentumD<T,Ds>&p, const T& mass){
  sparse_DGamma<Ds,T>::init();
  unsigned Dt = two_to_the(Ds/2);
  std::vector<std::vector<T>> res;
  res.clear();

  for(unsigned i = 0; i < Dt; i++)
    res.push_back(std::vector<T>(Dt,T(0)));

  // 1) res_ij = p_mu *[gamma^mu]_ij
  for(int mu = 0; mu < (int) Ds; mu++){
      auto const vals = sparse_DGamma<Ds, T,false>::values[mu];
      for (auto s : vals) {
          int i = std::get<0>(s) - 1;
          int j = std::get<1>(s) - 1;
          T gij = std::get<2>(s); // [gamma^mu]_ij
          res[i][j] += p[mu] * T(get_metric_signature<T>(mu)) * gij;
      }
  };

    // 2) res_ij -= mass * delta_ij
    for (size_t i = 0; i < Dt; i++) 
    	res[i][i] -= mass;

    return res;
}

// checks if the polarisation sum is correct
template<typename T, size_t Ds> bool polsum(const momentumD<T,Ds>& p, const T& mass, const vector<vector<T>>& Sp, const vector<vector<T>>& Sm, const vector<vector<T>>& Sbp, const vector<vector<T>>& Sbm){
  vector<vector<T>> result = polsum_explicit(p,mass);
  const unsigned Dt = two_to_the(Ds/2);
 
  for(unsigned Dtt_index = 0; Dtt_index < Sp.size(); Dtt_index++){
    vector<T> Up = Sp[Dtt_index];
    vector<T> Um = Sm[Dtt_index];
    vector<T> Ubarp = Sbp[Dtt_index];
    vector<T> Ubarm = Sbm[Dtt_index];
#if WEYL
    T norm = T(1);
#else 
    T norm = T(2);
#endif   
    for (unsigned i = 0; i < Dt; i++) {
       for (unsigned j = 0; j < Dt; j++) { result[i][j] -= (Up[i] * Ubarp[j] + Um[i] * Ubarm[j])/norm; }
    }
  }

  for (unsigned i = 0; i < Dt; i++) {
      for (unsigned j = 0; j < Dt; j++) { 
        if(!is_my_zero(result[i][j])) { 
/*
           std::cout << "------------------" << std::endl;
           for( unsigned k = 0; k < Dt; k++){
             for( unsigned l = 0; l < Dt; l++){
               std::cout << result[k][l] << "\t";
             }
	     std::cout << endl;
           }
           return true; 
*/
	  return false;
        }
      }
  }

  return true;
}

// checks (p_slash -m)|Psi> = 0 and <Psi|(p_slash -m) = 0
template<mult_from MF, typename T, size_t Ds> bool dirac(const momentumD<T,Ds>& p, const T& mass, const std::vector<T>& sp){
  std::vector<T> out = multiply_sparse_DGamma<MF,T>(p.get_components(),sp);
  for(unsigned i = 0; i < out.size(); i++)
	out[i] -= mass*sp[i];

    for (auto s : out){
        if (!is_my_zero(s)) { for(auto i : out) cout << i << endl;};
        if (!is_my_zero(s)) return false;
   }

    return true;
}



/***********************************************************************************/
/* MASSLESS SPINORS					                           */
/***********************************************************************************/
TEST_CASE("Check Ds = 4 dimensional massless spinors"){
  
  constexpr unsigned Ds = 4;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(0);
  momentumD<C,Ds>  p = momentumD<C,Ds>( C(sqrt(3),0), C(1,0), C(1,0), C(1,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_Up<C>(p, idx));
      Um.push_back(DSpinor_Um<C>(p, idx));
      Ubarp.push_back(DSpinor_Ubarp<C>(p, idx));
      Ubarm.push_back(DSpinor_Ubarm<C>(p, idx));
      Vp.push_back(DSpinor_Vp<C>(p, idx));
      Vm.push_back(DSpinor_Vm<C>(p, idx));
      Vbarp.push_back(DSpinor_Vbarp<C>(p, idx));
      Vbarm.push_back(DSpinor_Vbarm<C>(p, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 6 dimensional massless spinors"){
  
  constexpr unsigned Ds = 6;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(0);
  momentumD<C,Ds>  p = momentumD<C,Ds>( C(sqrt(3),0), C(1,0), C(1,0), C(1,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_Up<C>(p, idx));
      Um.push_back(DSpinor_Um<C>(p, idx));
      Ubarp.push_back(DSpinor_Ubarp<C>(p, idx));
      Ubarm.push_back(DSpinor_Ubarm<C>(p, idx));
      Vp.push_back(DSpinor_Vp<C>(p, idx));
      Vm.push_back(DSpinor_Vm<C>(p, idx));
      Vbarp.push_back(DSpinor_Vbarp<C>(p, idx));
      Vbarm.push_back(DSpinor_Vbarm<C>(p, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 8 dimensional massless spinors"){
  
  constexpr unsigned Ds = 8;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(0);
  momentumD<C,Ds>  p = momentumD<C,Ds>( C(sqrt(3),0), C(1,0), C(1,0), C(1,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_Up<C>(p, idx));
      Um.push_back(DSpinor_Um<C>(p, idx));
      Ubarp.push_back(DSpinor_Ubarp<C>(p, idx));
      Ubarm.push_back(DSpinor_Ubarm<C>(p, idx));
      Vp.push_back(DSpinor_Vp<C>(p, idx));
      Vm.push_back(DSpinor_Vm<C>(p, idx));
      Vbarp.push_back(DSpinor_Vbarp<C>(p, idx));
      Vbarm.push_back(DSpinor_Vbarm<C>(p, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 10 dimensional massless spinors"){
  
  constexpr unsigned Ds = 10;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(0);
  momentumD<C,Ds>  p = momentumD<C,Ds>( C(sqrt(3),0), C(1,0), C(1,0), C(1,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_Up<C>(p, idx));
      Um.push_back(DSpinor_Um<C>(p, idx));
      Ubarp.push_back(DSpinor_Ubarp<C>(p, idx));
      Ubarm.push_back(DSpinor_Ubarm<C>(p, idx));
      Vp.push_back(DSpinor_Vp<C>(p, idx));
      Vm.push_back(DSpinor_Vm<C>(p, idx));
      Vbarp.push_back(DSpinor_Vbarp<C>(p, idx));
      Vbarm.push_back(DSpinor_Vbarm<C>(p, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 12 dimensional massless spinors"){
  
  constexpr unsigned Ds = 12;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(0);
  momentumD<C,Ds>  p = momentumD<C,Ds>( C(sqrt(3),0), C(1,0), C(1,0), C(1,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_Up<C>(p, idx));
      Um.push_back(DSpinor_Um<C>(p, idx));
      Ubarp.push_back(DSpinor_Ubarp<C>(p, idx));
      Ubarm.push_back(DSpinor_Ubarm<C>(p, idx));
      Vp.push_back(DSpinor_Vp<C>(p, idx));
      Vm.push_back(DSpinor_Vm<C>(p, idx));
      Vbarp.push_back(DSpinor_Vbarp<C>(p, idx));
      Vbarm.push_back(DSpinor_Vbarm<C>(p, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 14 dimensional massless spinors"){
  
  constexpr unsigned Ds = 14;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(0);
  momentumD<C,Ds>  p = momentumD<C,Ds>( C(sqrt(3),0), C(1,0), C(1,0), C(1,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_Up<C>(p, idx));
      Um.push_back(DSpinor_Um<C>(p, idx));
      Ubarp.push_back(DSpinor_Ubarp<C>(p, idx));
      Ubarm.push_back(DSpinor_Ubarm<C>(p, idx));
      Vp.push_back(DSpinor_Vp<C>(p, idx));
      Vm.push_back(DSpinor_Vm<C>(p, idx));
      Vbarp.push_back(DSpinor_Vbarp<C>(p, idx));
      Vbarm.push_back(DSpinor_Vbarm<C>(p, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 4 dimensional massive spinors"){
  
  constexpr unsigned Ds = 4;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(5);
  momentumD<C,Ds>  p = momentumD<C,Ds>( sqrt(C(3)+mass*mass), C(1,0), C(1,0), C(1,0) );
  momentumD<C,Ds>  n = momentumD<C,Ds>( C(1,0), C(1,0), C(0,0), C(0,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
        Up.push_back(DSpinor_Up<C>(p, n, mass, idx));
        Um.push_back(DSpinor_Um<C>(p, n, mass, idx));
        Ubarp.push_back(DSpinor_Ubarp<C>(p, n, mass, idx));
        Ubarm.push_back(DSpinor_Ubarm<C>(p, n, mass, idx));
        Vp.push_back(DSpinor_Vp<C>(p, n, mass, idx));
        Vm.push_back(DSpinor_Vm<C>(p, n, mass, idx));
        Vbarp.push_back(DSpinor_Vbarp<C>(p, n, mass, idx));
        Vbarm.push_back(DSpinor_Vbarm<C>(p, n, mass, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 6 dimensional massive spinors"){
  
  constexpr unsigned Ds = 6;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(5);
  momentumD<C,Ds>  p = momentumD<C,Ds>( sqrt(C(3)+mass*mass), C(1,0), C(1,0), C(1,0) );
  momentumD<C,Ds>  n = momentumD<C,Ds>( C(1,0), C(1,0), C(0,0), C(0,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
        Up.push_back(DSpinor_Up<C>(p, n, mass, idx));
        Um.push_back(DSpinor_Um<C>(p, n, mass, idx));
        Ubarp.push_back(DSpinor_Ubarp<C>(p, n, mass, idx));
        Ubarm.push_back(DSpinor_Ubarm<C>(p, n, mass, idx));
        Vp.push_back(DSpinor_Vp<C>(p, n, mass, idx));
        Vm.push_back(DSpinor_Vm<C>(p, n, mass, idx));
        Vbarp.push_back(DSpinor_Vbarp<C>(p, n, mass, idx));
        Vbarm.push_back(DSpinor_Vbarm<C>(p, n, mass, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 8 dimensional massive spinors"){
  
  constexpr unsigned Ds = 8;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(5);
  momentumD<C,Ds>  p = momentumD<C,Ds>( sqrt(C(3)+mass*mass), C(1,0), C(1,0), C(1,0) );
  momentumD<C,Ds>  n = momentumD<C,Ds>( C(1,0), C(1,0), C(0,0), C(0,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
        Up.push_back(DSpinor_Up<C>(p, n, mass, idx));
        Um.push_back(DSpinor_Um<C>(p, n, mass, idx));
        Ubarp.push_back(DSpinor_Ubarp<C>(p, n, mass, idx));
        Ubarm.push_back(DSpinor_Ubarm<C>(p, n, mass, idx));
        Vp.push_back(DSpinor_Vp<C>(p, n, mass, idx));
        Vm.push_back(DSpinor_Vm<C>(p, n, mass, idx));
        Vbarp.push_back(DSpinor_Vbarp<C>(p, n, mass, idx));
        Vbarm.push_back(DSpinor_Vbarm<C>(p, n, mass, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 10 dimensional massive spinors"){
  
  constexpr unsigned Ds = 10;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(5);
  momentumD<C,Ds>  p = momentumD<C,Ds>( sqrt(C(3)+mass*mass), C(1,0), C(1,0), C(1,0) );
  momentumD<C,Ds>  n = momentumD<C,Ds>( C(1,0), C(1,0), C(0,0), C(0,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
        Up.push_back(DSpinor_Up<C>(p, n, mass, idx));
        Um.push_back(DSpinor_Um<C>(p, n, mass, idx));
        Ubarp.push_back(DSpinor_Ubarp<C>(p, n, mass, idx));
        Ubarm.push_back(DSpinor_Ubarm<C>(p, n, mass, idx));
        Vp.push_back(DSpinor_Vp<C>(p, n, mass, idx));
        Vm.push_back(DSpinor_Vm<C>(p, n, mass, idx));
        Vbarp.push_back(DSpinor_Vbarp<C>(p, n, mass, idx));
        Vbarm.push_back(DSpinor_Vbarm<C>(p, n, mass, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}
TEST_CASE("Check Ds = 12 dimensional massive spinors"){
  
  constexpr unsigned Ds = 12;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(5);
  momentumD<C,Ds>  p = momentumD<C,Ds>( sqrt(C(3)+mass*mass), C(1,0), C(1,0), C(1,0) );
  momentumD<C,Ds>  n = momentumD<C,Ds>( C(1,0), C(1,0), C(0,0), C(0,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
        Up.push_back(DSpinor_Up<C>(p, n, mass, idx));
        Um.push_back(DSpinor_Um<C>(p, n, mass, idx));
        Ubarp.push_back(DSpinor_Ubarp<C>(p, n, mass, idx));
        Ubarm.push_back(DSpinor_Ubarm<C>(p, n, mass, idx));
        Vp.push_back(DSpinor_Vp<C>(p, n, mass, idx));
        Vm.push_back(DSpinor_Vm<C>(p, n, mass, idx));
        Vbarp.push_back(DSpinor_Vbarp<C>(p, n, mass, idx));
        Vbarm.push_back(DSpinor_Vbarm<C>(p, n, mass, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}
TEST_CASE("Check Ds = 14 dimensional massive spinors"){
  
  constexpr unsigned Ds = 14;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(5);
  momentumD<C,Ds>  p = momentumD<C,Ds>( sqrt(C(3)+mass*mass), C(1,0), C(1,0), C(1,0) );
  momentumD<C,Ds>  n = momentumD<C,Ds>( C(1,0), C(1,0), C(0,0), C(0,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
        Up.push_back(DSpinor_Up<C>(p, n, mass, idx));
        Um.push_back(DSpinor_Um<C>(p, n, mass, idx));
        Ubarp.push_back(DSpinor_Ubarp<C>(p, n, mass, idx));
        Ubarm.push_back(DSpinor_Ubarm<C>(p, n, mass, idx));
        Vp.push_back(DSpinor_Vp<C>(p, n, mass, idx));
        Vm.push_back(DSpinor_Vm<C>(p, n, mass, idx));
        Vbarp.push_back(DSpinor_Vbarp<C>(p, n, mass, idx));
        Vbarm.push_back(DSpinor_Vbarm<C>(p, n, mass, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 6 dimensional massive complex cut wavefunction"){
  
  constexpr unsigned Ds = 6;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(173);
  momentumD<C,Ds> p = momentumD<C,Ds>( C(-661.5111538461495,0),C(-634.3127015220766,0),C(-571.7654622631831,0),C(-334.1855831631034,0),C(0,658.2403394509222),C(0,0) );
//  momentumD<C,Ds> p = momentumD<C,Ds>( C(-434.001620235658,-50.85754601112217),C(-219.4574497968909,-92.73686125475105),C(-370.0504697883969,-45.09142693107127),C(-32.2011447657658,-67.50568746156134),C(105.7240466636737,-162.1145082665724),C(0,0) );


  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
        Up.push_back(DSpinor_cut_Qbp<C>(-p, mass, idx));
        Um.push_back(DSpinor_cut_Qbm<C>(-p, mass, idx));
        Ubarp.push_back(DSpinor_cut_Qp<C>(p, mass, idx));
        Ubarm.push_back(DSpinor_cut_Qm<C>(p, mass, idx));
        Vp.push_back(DSpinor_cut_Qbp<C>(p, mass, idx));
        Vm.push_back(DSpinor_cut_Qbm<C>(p, mass, idx));
        Vbarp.push_back(DSpinor_cut_Qp<C>(-p, mass, idx));
        Vbarm.push_back(DSpinor_cut_Qm<C>(-p, mass, idx));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(-p,-mass, Vp, Vm, Vbarp, Vbarm));
}

TEST_CASE("Check Ds = 14 dimensional massive complex wavefunction"){
  
  constexpr unsigned Ds = 14;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  C mass = C(5);
  momentumD<C,Ds> p = momentumD<C,Ds>( sqrt( C(70) + mass*mass), C(0,10), C(6,0), C(-5,0), C(10,0), C(0,7), C(9,0), C(-7,0), C(0,9), C(3,0), C(6,0), C(0,-6) );
  momentumD<C,Ds> n = momentumD<C,Ds>( C(sqrt(56),0), C(-2,0), C(4,0), C(6,0) );

  vector<vector<C>> Up, Um, Ubarp, Ubarm;
  vector<vector<C>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
        Up.push_back(DSpinor_cut_Qbp<C>(-p, mass, idx));
        Um.push_back(DSpinor_cut_Qbm<C>(-p, mass, idx));
        Ubarp.push_back(DSpinor_cut_Qp<C>(p, mass, idx));
        Ubarm.push_back(DSpinor_cut_Qm<C>(p, mass, idx));
        Vp.push_back(DSpinor_cut_Qbp<C>(p, mass, idx));
        Vm.push_back(DSpinor_cut_Qbm<C>(p, mass, idx));
        Vbarp.push_back(DSpinor_cut_Qp<C>(-p, mass, idx));
        Vbarm.push_back(DSpinor_cut_Qm<C>(-p, mass, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm));
  REQUIRE(polsum(-p,-mass, Vp, Vm, Vbarp, Vbarm));
}
