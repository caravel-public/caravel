#include "catch.hpp"

#include <random>
#include <sstream>

#include "Core/typedefs.h"
#include "IntegralLibrary/CLN_QD_conversion.h"

using namespace Caravel;
using namespace Caravel::Integrals;

constexpr long npoints = 100;

TEST_CASE("Real conversion from qd to cln and back."){

    std::default_random_engine rand_gen;
    std::uniform_real_distribution<double> distribution(0.0, 1.);

    SECTION("R"){
        for (size_t i = 0; i < npoints; ++i){
            // create cln number from double
            R qd_in = distribution(rand_gen);
            // convert to qd
            auto cln_conv = qd_to_cln(qd_in);
            // convert back to cln
            R qd_out = cln_to_qd<R>(cln_conv);

            CHECK(std::abs(qd_out-qd_in) < R(1e-18));
        }
    }

#ifdef HIGH_PRECISION
    SECTION("RHP"){
        for (size_t i = 0; i < npoints; ++i){
            // create cln number from double
            RHP qd_in = distribution(rand_gen);
            // convert to qd
            auto cln_conv = qd_to_cln(qd_in);
            // convert back to cln
            RHP qd_out = cln_to_qd<RHP>(cln_conv);

            CHECK(abs(qd_out-qd_in) < RHP("1e-31"));
        }
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("RVHP"){
        for (size_t i = 0; i < npoints; ++i){
            // create cln number from double
            RVHP qd_in = distribution(rand_gen);
            // convert to qd
            auto cln_conv = qd_to_cln(qd_in);
            // convert back to cln
            RVHP qd_out = cln_to_qd<RVHP>(cln_conv);

            CHECK(abs(qd_out-qd_in) < RVHP("1e-63"));
        }
    }
#endif

    SECTION("Scientific R"){
        R qd_in = 1.273478395478489e+7;
        auto cln_conv = qd_to_cln(qd_in);
        R qd_out = cln_to_qd<R>(cln_conv);
        CHECK(std::abs(qd_out-qd_in) < R(1e-18));
    }

#ifdef HIGH_PRECISION
    SECTION("Scientific RHP"){
        RHP qd_in = "1.273478395478489e+33";
        auto cln_conv = qd_to_cln(qd_in);
        RHP qd_out = cln_to_qd<RHP>(cln_conv);
        CHECK(abs(qd_out-qd_in) < RHP("1e-32"));
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("Scientific RVHP"){
        RVHP qd_in = "1.24857458745948793847734788475938475987598675986795395478489e+64";
        auto cln_conv = qd_to_cln(qd_in);
        RVHP qd_out = cln_to_qd<RVHP>(cln_conv);
        CHECK(abs(qd_out-qd_in) < RVHP("1e-64"));
    }
#endif
}


TEST_CASE("Complex conversion from qd to cln and back."){

    std::default_random_engine rand_gen;
    std::uniform_real_distribution<double> distribution(0.0, 1.);

    SECTION("C"){
        for (size_t i = 0; i < npoints; ++i){
            // create cln number from double
            C qd_in(distribution(rand_gen), distribution(rand_gen));
            // convert to qd
            auto cln_conv = qd_to_cln(qd_in);
            // convert back to cln
            C qd_out = cln_to_qd<C>(cln_conv);

            CHECK(abs(qd_out-qd_in) < R(1e-18));
        }
    }

#ifdef HIGH_PRECISION
    SECTION("CHP"){
        for (size_t i = 0; i < npoints; ++i){
            // create cln number from double
            CHP qd_in(distribution(rand_gen), distribution(rand_gen));
            // convert to qd
            auto cln_conv = qd_to_cln(qd_in);
            // convert back to cln
            CHP qd_out = cln_to_qd<CHP>(cln_conv);

            CHECK(abs(qd_out-qd_in) < RHP("1e-31"));
        }
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("CVHP"){
        for (size_t i = 0; i < npoints; ++i){
            // create cln number from double
            CVHP qd_in(distribution(rand_gen), distribution(rand_gen));
            // convert to qd
            auto cln_conv = qd_to_cln(qd_in);
            // convert back to cln
            CVHP qd_out = cln_to_qd<CVHP>(cln_conv);

            CHECK(abs(qd_out-qd_in) < RVHP("1e-31"));
        }
    }
#endif
}

TEST_CASE("Real conversion from CLN to QD and back."){
    std::default_random_engine rand_gen;
    std::uniform_real_distribution<double> distribution(0.0, 1.);

    SECTION("R"){
        for (size_t i = 0; i < npoints; ++i){
            // Create a cl_LF number:
            std::ostringstream oss;
            oss.precision(20);
            oss << distribution(rand_gen);
            cln::cl_LF cln_in = (oss.str()+"_20").c_str();
            auto qd_conv = cln_to_qd<R>(cln_in);
            cln::cl_LF cln_out = qd_to_cln(qd_conv);
            CHECK(abs(cln_out - cln_in) < cln::cl_LF("1e-18"));
        }
    }

#ifdef HIGH_PRECISION
    SECTION("RHP"){
        for (size_t i = 0; i < npoints; ++i){
            // Create a cl_LF number:
            std::ostringstream oss;
            oss.precision(34);
            oss << distribution(rand_gen);
            cln::cl_LF cln_in = (oss.str()+"_34").c_str();
            auto qd_conv = cln_to_qd<RHP>(cln_in);
            cln::cl_LF cln_out = qd_to_cln(qd_conv);
            CHECK(abs(cln_out - cln_in) < cln::cl_LF("1e-31"));
        }
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("RVHP"){
        for (size_t i = 0; i < npoints; ++i){
            // Create a cl_LF number:
            std::ostringstream oss;
            oss.precision(66);
            oss << distribution(rand_gen);
            cln::cl_LF cln_in = (oss.str()+"_66").c_str();
            auto qd_conv = cln_to_qd<RVHP>(cln_in);
            cln::cl_LF cln_out = qd_to_cln(qd_conv);
            CHECK(abs(cln_out - cln_in) < cln::cl_LF("1e-63"));
        }
    }
#endif
}

TEST_CASE("Complex conversion from CLN to QD and back."){
    std::default_random_engine rand_gen;
    std::uniform_real_distribution<double> distribution(0.0, 1.);

    SECTION("C"){
        for (size_t i = 0; i < npoints; ++i){
            // Create a cl_LF number:
            std::ostringstream real;
            real.precision(20);
            real << distribution(rand_gen);
            std::ostringstream imag;
            imag.precision(20);
            imag << distribution(rand_gen);
            cln::cl_N cln_in = cln::complex(cln::cl_LF((real.str()+"_20").c_str()), cln::cl_LF((imag.str()+"_20").c_str()));
            auto qd_conv = cln_to_qd<C>(cln_in);
            cln::cl_N cln_out = qd_to_cln(qd_conv);
            CHECK(abs(cln_out - cln_in) < cln::cl_LF("1e-18"));
        }
    }

#ifdef HIGH_PRECISION
    SECTION("CHP"){
        for (size_t i = 0; i < npoints; ++i){
            // Create a cl_LF number:
            std::ostringstream real;
            real.precision(34);
            real << distribution(rand_gen);
            std::ostringstream imag;
            imag.precision(34);
            imag << distribution(rand_gen);
            cln::cl_N cln_in = cln::complex((real.str()+"_34").c_str(), (imag.str()+"_34").c_str());
            auto qd_conv = cln_to_qd<CHP>(cln_in);
            cln::cl_N cln_out = qd_to_cln(qd_conv);
            CHECK(abs(cln_out - cln_in) < cln::cl_LF("1e-31"));
        }
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("CVHP"){
        for (size_t i = 0; i < npoints; ++i){
            // Create a cl_LF number:
            std::ostringstream real;
            real.precision(66);
            real << distribution(rand_gen);
            std::ostringstream imag;
            imag.precision(66);
            imag << distribution(rand_gen);
            cln::cl_N cln_in = cln::complex((real.str()+"_66").c_str(), (imag.str()+"_66").c_str());
            auto qd_conv = cln_to_qd<CVHP>(cln_in);
            cln::cl_N cln_out = qd_to_cln(qd_conv);
            CHECK(abs(cln::realpart(cln_out - cln_in)) < cln::cl_LF("1e-63"));
        }
    }
#endif
}


TEST_CASE("Special Cases"){

    SECTION("R"){
        R qd_in(2);
        auto cln = qd_to_cln(qd_in);
        R qd_out = cln_to_qd<R>(cln);
        CHECK(std::abs(qd_out - qd_in) < R(1e-16));
    }

#ifdef HIGH_PRECISION
    SECTION("RHP"){
        RHP qd_in(2);
        auto cln = qd_to_cln(qd_in);
        RHP qd_out = cln_to_qd<RHP>(cln);
        CHECK(abs(qd_out - qd_in) < RHP(1e-32));
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("RVHP"){
        RVHP qd_in(2);
        auto cln = qd_to_cln(qd_in);
        RVHP qd_out = cln_to_qd<RVHP>(cln);
        CHECK(abs(qd_out - qd_in) < RVHP(1e-64));
    }
#endif

}
