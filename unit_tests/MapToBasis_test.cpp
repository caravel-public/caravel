#include "catch.hpp"

#include "IntegralLibrary/MapToBasis.h"
#include "Core/Series.h"

using namespace Caravel;
using namespace Caravel::Integrals;

TEST_CASE("Simple Basis Map", "[integrals][map][MapToBasis][simple]"){

    // First create a numeric function basis expansion which still contains
    // non basis functions.
    // We say that the amplitude is given by:
    // a * G[-s, -t] + b * G[-t, -s]
    // where G[-s, -t] is a basis function but G[-t, -s] is not.
    //
    // The replacement rule for the non-basis function is
    // G[-t, -s] = c * G[-s, -t] + d * Pi^2
    // where Pi^2 belongs to the basis.
    //
    // In general a and b are Series in epsilon while c and d are rational
    // numbers. Therefore we choose
    //
    // a = 4 ep^-4 + 3 ep^-3 + 2 ep^-2 + 1 ep^-1 + 5
    // b = 5 ep^-4 + 1 ep^-3 + 2 ep^-2 + 3 ep^-1 + 4
    //
    // and
    //
    // c = 7
    // d = 8

    Series<BigRat> a(-4, 0, BigRat(4), BigRat(3), BigRat(2), BigRat(1), BigRat(5));
    Series<BigRat> b(-4, 0, BigRat(5), BigRat(1), BigRat(2), BigRat(3), BigRat(4));
    StdBasisFunction<C, R> f1([](const std::vector<R>&) -> C {return C(0);}, "G(-s, -t)", {"-s", "-t"});
    StdBasisFunction<C, R> f2([](const std::vector<R>&) -> C {return C(0);}, "G(-t, -s)", {"-t", "-s"});
    BigRat c(7);
    BigRat d(8);

    // Create the amplitude
    NumericFunctionBasisExpansion<Series<BigRat>, StdBasisFunction<C, R>> amplitude({{f1, a} , {f2, b}});
    // Create the replacement rules
    std::unordered_map<std::string, std::vector<std::pair<std::string, BigRat>>> replacements({{"G[-t, -s]", {{"G[-s, -t]", c}, {"Pi^2", d}}}});

    // Map onto the basis
    MapToBasis<BigRat, StdBasisFunction<C, R>> mapper(replacements);

    auto res = mapper(amplitude);
    std::cout << "RES = " << res << std::endl;

    // Check
    CHECK(res.get_coeff("G[-s, -t]")[-4] == BigRat(39));
    CHECK(res.get_coeff("G[-s, -t]")[-3] == BigRat(10));
    CHECK(res.get_coeff("G[-s, -t]")[-2] == BigRat(16));
    CHECK(res.get_coeff("G[-s, -t]")[-1] == BigRat(22));
    CHECK(res.get_coeff("G[-s, -t]")[0] == BigRat(33));

    CHECK(res.get_coeff("Pi^2")[-4] == BigRat(40));
    CHECK(res.get_coeff("Pi^2")[-3] == BigRat(8));
    CHECK(res.get_coeff("Pi^2")[-2] == BigRat(16));
    CHECK(res.get_coeff("Pi^2")[-1] == BigRat(24));
    CHECK(res.get_coeff("Pi^2")[0] == BigRat(32));

}

TEST_CASE("4g Map", "[MapToBasis][Integrals][4g]"){

    // The four gluon process.
    std::vector<Particle> process = {
        Particle("g", ParticleType("gluon"), SingleState("m"), 1),
        Particle("g", ParticleType("gluon"), SingleState("m"), 2),
        Particle("g", ParticleType("gluon"), SingleState("p"), 3),
        Particle("g", ParticleType("gluon"), SingleState("p"), 4),
    };

    // Use the default map stored under share/Caravel
    MapToBasis<F32, StdBasisFunction<C, R>> m(PartialAmplitudeInput{process});
    CHECK(m.n_replacements() == 19);

    auto rules1 = m.get_replacement_rule_for_function("G[s12,s23*T[-1]]*Pi*Pi");
    CHECK(rules1.size() == 3);
    CHECK(rules1[0].first == "G[T[0],s12*T[-1]]*Pi*Pi");
    CHECK(rules1[0].second(momentumD_configuration<F32, 4>(),false) == F32(-1));
    CHECK(rules1[1].first == "G[T[0],s23*T[-1]]*Pi*Pi");
    CHECK(rules1[1].second(momentumD_configuration<F32, 4>(),false) == F32(1));
    CHECK(rules1[2].first == "G[s23,s12*T[-1]]*Pi*Pi");
    CHECK(rules1[2].second(momentumD_configuration<F32, 4>(),false) == F32(1));

    auto rules2 = m.get_replacement_rule_for_function("G[T[0],s12*T[-1]]*G[s12,s23*T[-1]]*Pi*Pi");
    CHECK(rules2.size() == 4);
    CHECK(rules2[0].first == "G[T[0],s12*T[-1]]*G[T[0],s23*T[-1]]*Pi*Pi");
    CHECK(rules2[0].second(momentumD_configuration<F32, 4>(),false) == F32(1));
    CHECK(rules2[1].first == "G[T[0],T[0],s12*T[-1]]*Pi*Pi");
    CHECK(rules2[1].second(momentumD_configuration<F32, 4>(),false) == F32(-2));
    CHECK(rules2[2].first == "G[T[0],s23,s12*T[-1]]*Pi*Pi");
    CHECK(rules2[2].second(momentumD_configuration<F32, 4>(),false) == F32(1));
    CHECK(rules2[3].first == "G[s23,T[0],s12*T[-1]]*Pi*Pi");
    CHECK(rules2[3].second(momentumD_configuration<F32, 4>(),false) == F32(1));

    auto rules3 = m.get_replacement_rule_for_function("G[T[0],T[0],s12*T[-1]]*G[s12,s23*T[-1]]");
    CHECK(rules3.size() == 5);
    CHECK(rules3[4].second(momentumD_configuration<F32, 4>(),false) == F32(1));
    CHECK(rules3[1].second(momentumD_configuration<F32, 4>(),false) == F32(-3));

}


TEST_CASE("5g Map", "[MapToBasis][Integrals][5g]"){

    // The four gluon process.
    std::vector<Particle> process = {
        Particle("g", ParticleType("gluon"), SingleState("m"), 1),
        Particle("g", ParticleType("gluon"), SingleState("m"), 2),
        Particle("g", ParticleType("gluon"), SingleState("p"), 3),
        Particle("g", ParticleType("gluon"), SingleState("p"), 4),
        Particle("g", ParticleType("gluon"), SingleState("p"), 5),
    };

    // Use the default map stored under share/Caravel
    MapToBasis<F32, StdBasisFunction<C, R>> m(PartialAmplitudeInput{process});
    CHECK(m.n_replacements() == 1604);

    auto rules1 = m.get_replacement_rule_for_function("G[s34*T[-1],s15*T[-1]]*Pi*Pi");
    CHECK(rules1.size() == 4);
    CHECK(rules1[0].first == "Pi*Pi*Pi*icomplex");
    CHECK(rules1[0].second(momentumD_configuration<F32, 4>(),false) == F32(-1));
    CHECK(rules1[1].first == "G[T[0],s15*T[-1]]*Pi*Pi");
    CHECK(rules1[1].second(momentumD_configuration<F32, 4>(),false) == F32(1));
    CHECK(rules1[2].first == "G[T[0],s34*T[-1]]*Pi*Pi");
    CHECK(rules1[2].second(momentumD_configuration<F32, 4>(),false) == F32(-1));
    CHECK(rules1[3].first == "G[s15*T[-1],s34*T[-1]]*Pi*Pi");
    CHECK(rules1[3].second(momentumD_configuration<F32, 4>(),false) == F32(1));

}
