#include "Core/spinor/dirac_algebra.h"
#include "catch.hpp"
#include "compare_tools.hpp"
#include <iostream>

 
using namespace std;
using namespace Caravel;
using namespace clifford_algebra;

/*
 * This generates a vector of (i,i,val), which corresponds to 2*g^(mu,nu)*Idendity for D vector dimensions
 */
template <size_t D, typename T> vector<tuple<size_t, size_t, T>> anti_commute_res(const int mu, const int nu) {
    // Resulting matrix
    size_t Dt = two_to_the(D / 2);
    vector<tuple<size_t, size_t, T>> res;
    res.clear();

    // no non-zero values for mu != nu
    if (mu != nu) return res;

    // 2*g^(00)
    if (mu == 0 && nu == 0) {
        for (size_t i = 1; i <= Dt; i++) res.emplace_back(i, i, T(2));
    }

    // 2*g^(ii)
    if (mu > 0 && nu > 0 && mu == nu) {
        for (size_t i = 1; i <= Dt; i++) res.emplace_back(i, i, T(-2));
    }

    return res;
}

// Computes the anti-commutator {gamma^mu,gamma^\nu} for a D dimensional representation
template <size_t D, typename T> vector<tuple<size_t, size_t, T>> anti_commute(const int mu, const int nu) {
    // Initialize Dirac matrices and get values
    sparse_DGamma<D, T>::init();
    auto A = sparse_DGamma<D, T>::values[mu];
    auto B = sparse_DGamma<D, T>::values[nu];

    // Resulting matrix
    vector<tuple<size_t, size_t, T>> res;
    res.clear();

    // Compute A_ik*B_kj + B_ik*A_kj
    for (size_t i = 1; i <= sparse_DGamma<D, T>::Dt; i++) {
        for (size_t j = 1; j <= sparse_DGamma<D, T>::Dt; j++) {

            T s = T(0);

            // A_ik*B_kj
            for (auto f : A) {
                if (get<0>(f) != i) continue;
                for (auto g : B) {
                    if (get<1>(f) != get<0>(g) || get<1>(g) != j) continue;
                    s += get<2>(f) * get<2>(g);
                }
            }

            // B_ik*A_kj
            for (auto f : B) {
                if (get<0>(f) != i) continue;
                for (auto g : A) {
                    if (get<1>(f) != get<0>(g) || get<1>(g) != j) continue;
                    s += get<2>(f) * get<2>(g);
                }
            }
            if (s != T(0)) res.emplace_back(i, j, s);
        }
    }
    return res;
}

/* 
 * Checks {gamma^mu,gamma^nu} = 2*g^{munu}
 * return true if correct
 */
template <size_t D> bool check_clifford() {
    for (int mu = 0; mu < (int) D; mu++) {
        for (int nu = 0; nu < (int) D; nu++) {
            if (anti_commute<D, C>(mu, nu) != anti_commute_res<D, C>(mu, nu)) return false;
        }
    }
    return true;
}

/*
 * Checks gstar*gstart = 1, gstar = gamma5 in 4D 
 * Returns true if correct
 */
template <size_t D> bool check_gstar() {
    // Initialize Dirac matrices and get values
    sparse_DGamma<D, C>::init();
    auto A = sparse_DGamma<D, C>::values[D];

    // Compute A_ik*A_kj 
    for (size_t i = 1; i <= sparse_DGamma<D, C>::Dt; i++) {
        for (size_t j = 1; j <= sparse_DGamma<D, C>::Dt; j++) {

            C s = C(0);

            // A_ik*A_kj
            for (auto f : A) {
                if (get<0>(f) != i) continue;
                for (auto g : A) {
                    if (get<1>(f) != get<0>(g) || get<1>(g) != j) continue;
                    s += get<2>(f) * get<2>(g);
                }
            }

	    if( i != j && s != C(0) ) return false;
            if( i == j && s != C(1) ) return false;
        }
    }

    return true;
}

TEST_CASE("Check Ds dimensional clifford algebra with explicit Dirac Gamma matrices"){

  SECTION("Algbera properties in Ds = 6"){
	constexpr size_t Ds = 6;
	REQUIRE(check_clifford<Ds>);	
	REQUIRE(check_gstar<Ds>);	
  }
  SECTION("Algbera properties in Ds = 8"){
	constexpr size_t Ds = 8;
	REQUIRE(check_clifford<Ds>);	
	REQUIRE(check_gstar<Ds>);	
  }
  SECTION("Algbera properties in Ds = 10"){
	constexpr size_t Ds = 10;
	REQUIRE(check_clifford<Ds>);	
	REQUIRE(check_gstar<Ds>);	
  }
  SECTION("Algbera properties in Ds = 12"){
	constexpr size_t Ds = 12;
	REQUIRE(check_clifford<Ds>);	
	REQUIRE(check_gstar<Ds>);	
  }
  SECTION("Algbera properties in Ds = 14"){
	constexpr size_t Ds = 14;
	REQUIRE(check_clifford<Ds>);	
	REQUIRE(check_gstar<Ds>);	
  }
}

