/**
 * tree_4G.cpp
 *
 * First created on 2.1.2018
 *
 * Construct an example of a tree-level amplitude with 4 external gravitons using CubicGravity model
 *
*/

#include <cmath>
#include <exception>
#include <iostream>
#include <string>
#include <type_traits>
#include <vector>

#include "Core/Debug.h"
#include "Core/Particle.h"
#include "Core/momD_conf.h"
#include "Core/type_traits_extra.h"
#include "Core/typedefs.h"
#include "Forest/Builder.h"
#include "Forest/Forest.h"
#include "Forest/Model.h"
#include "PhaseSpace/PhaseSpace.h"
#include "catch.hpp"
#include "compare_tools.hpp"

#define _DIM_Ds 10
#define _DIM_D 10

#define _CTYPE C
#define _ITYPE 0

#define _SEE_DEBUG_DETAILS_TCOMPLETENESS 1

using namespace std;
using namespace Caravel;
using namespace BG;


//TODO threshold temporarily lowered such that test passes
template <class T> inline bool is_zero(const T& x,typename std::enable_if_t<Caravel::is_floating_point<T>::value>* = nullptr) {
	using std::abs;
	if (abs(x) < 10e-7)
		return true;
	else
		return false;
}

template <class T> inline bool is_zero(const T& x,typename std::enable_if_t<Caravel::is_exact<T>::value>* = nullptr) {
	if (x==T(0)) return true;
	else return false;
}

template <class T> inline bool is_zero(const std::complex<T>& x) {
	if (is_zero(x.real()) and is_zero(x.imag())) return true;
	else return false;
}


template <class T,size_t D,size_t Rank> std::enable_if_t<(Rank>1),bool> is_zero_tensor (Tensor<T,D,Rank>& ten){
	std::vector<T> vec(ten.vectorize());
	for(auto const& value: vec) {
		if (!is_zero(value) ){
			std::cout << "value is " <<value << endl;
			std::cout << "bool is " <<is_zero(value) << endl;
			return false;
		}
}
		return true;
}

template <class T,size_t D,size_t Rank> std::enable_if_t<(Rank==1),bool> is_zero_tensor (Tensor<T,D,Rank>& ten){

	if (Rank==1) {
		for (size_t ii = 0; ii < D; ii++) {
			if (!is_zero(ten[ii])) {
				std::cout << "value is " <<ten[ii] << endl;
				std::cout << "bool is " <<is_zero(ten[ii]) << endl;
				return false;
			}
		}
	}
		return true;
}

template <typename T> T truncateSmall(T x){
	if(is_zero(x)){return T(0);}
	return x;
}


template <class T,size_t D> void get_tensor_states_D(std::vector<Tensor<T,D,2>>& polarization_tensors,std::vector<Tensor<T,D,2>>& polarization_tensors_conjugate,const momD<T,D>& l,bool bref,const momD<T,D>& mref){
	static_assert(D>4,"In function completeness_floating_point D should be greater than 4");

	// hold the negative of the input momentum
	momD<T,D> ml(-l);
	// build the momD_conf { l, -l }
	momD_conf<T,D> mconf(l,ml);
	// in case ref is passed
	size_t ref(0);
	if(bref){
		ref=mconf.insert(mref);
	}

	Tensor<T,D,2> hmmD(externalWF_tensorboson_mmD<T,T,D>(mconf,1,ref));
	Tensor<T,D,2> hppD(externalWF_tensorboson_ppD<T,T,D>(mconf,1,ref));
	Tensor<T,D,2> hmmcD(externalWF_tensorboson_mmcD<T,T,D>(mconf,2,ref));
	Tensor<T,D,2> hppcD(externalWF_tensorboson_ppcD<T,T,D>(mconf,2,ref));
	polarization_tensors.push_back(hmmD);
	polarization_tensors.push_back(hppD);
	polarization_tensors_conjugate.push_back(hmmcD);
	polarization_tensors_conjugate.push_back(hppcD);
	if constexpr (D >= 5 ){
		Tensor<T,D,2> hmID(externalWF_tensorboson_mID<T,T,D,3>(mconf,1,ref));
		Tensor<T,D,2> hpID(externalWF_tensorboson_pID<T,T,D,3>(mconf,1,ref));
		Tensor<T,D,2> hIID(externalWF_tensorboson_IJD<T,T,D,3,3>(mconf,1,ref));

		Tensor<T,D,2> hmIcD(externalWF_tensorboson_mIcD<T,T,D,3>(mconf,2,ref));
		Tensor<T,D,2> hpIcD(externalWF_tensorboson_pIcD<T,T,D,3>(mconf,2,ref));
		Tensor<T,D,2> hIIcD(externalWF_tensorboson_IJcD<T,T,D,3,3>(mconf,2,ref));

		polarization_tensors.push_back(hmID);
		polarization_tensors.push_back(hpID);
		polarization_tensors.push_back(hIID);

		polarization_tensors_conjugate.push_back(hmIcD);
		polarization_tensors_conjugate.push_back(hpIcD);
		polarization_tensors_conjugate.push_back(hIIcD);
	}
	if constexpr ( D >= 6 ){
		Tensor<T,D,2> hmJD(externalWF_tensorboson_mID<T,T,D,4>(mconf,1,ref));
		Tensor<T,D,2> hpJD(externalWF_tensorboson_pID<T,T,D,4>(mconf,1,ref));
		Tensor<T,D,2> hIJD(externalWF_tensorboson_IJD<T,T,D,3,4>(mconf,1,ref));
		Tensor<T,D,2> hJJD(externalWF_tensorboson_IJD<T,T,D,4,4>(mconf,1,ref));

		Tensor<T,D,2> hmJcD(externalWF_tensorboson_mIcD<T,T,D,4>(mconf,2,ref));
		Tensor<T,D,2> hpJcD(externalWF_tensorboson_pIcD<T,T,D,4>(mconf,2,ref));
		Tensor<T,D,2> hIJcD(externalWF_tensorboson_IJcD<T,T,D,3,4>(mconf,2,ref));
		Tensor<T,D,2> hJJcD(externalWF_tensorboson_IJcD<T,T,D,4,4>(mconf,2,ref));

		polarization_tensors.push_back(hmJD);
		polarization_tensors.push_back(hpJD);
		polarization_tensors.push_back(hIJD);
		polarization_tensors.push_back(hJJD);

		polarization_tensors_conjugate.push_back(hmJcD);
		polarization_tensors_conjugate.push_back(hpJcD);
		polarization_tensors_conjugate.push_back(hIJcD);
		polarization_tensors_conjugate.push_back(hJJcD);
	}
	if constexpr ( D >= 7 ){
		Tensor<T,D,2> hmKD(externalWF_tensorboson_mID<T,T,D,5>(mconf,1,ref));
		Tensor<T,D,2> hpKD(externalWF_tensorboson_pID<T,T,D,5>(mconf,1,ref));
		Tensor<T,D,2> hIKD(externalWF_tensorboson_IJD<T,T,D,3,5>(mconf,1,ref));
		Tensor<T,D,2> hJKD(externalWF_tensorboson_IJD<T,T,D,4,5>(mconf,1,ref));
		Tensor<T,D,2> hKKD(externalWF_tensorboson_IJD<T,T,D,5,5>(mconf,1,ref));

		Tensor<T,D,2> hmKcD(externalWF_tensorboson_mIcD<T,T,D,5>(mconf,2,ref));
		Tensor<T,D,2> hpKcD(externalWF_tensorboson_pIcD<T,T,D,5>(mconf,2,ref));
		Tensor<T,D,2> hIKcD(externalWF_tensorboson_IJcD<T,T,D,3,5>(mconf,2,ref));
		Tensor<T,D,2> hJKcD(externalWF_tensorboson_IJcD<T,T,D,4,5>(mconf,2,ref));
		Tensor<T,D,2> hKKcD(externalWF_tensorboson_IJcD<T,T,D,5,5>(mconf,2,ref));

		polarization_tensors.push_back(hmKD);
		polarization_tensors.push_back(hpKD);
		polarization_tensors.push_back(hIKD);
		polarization_tensors.push_back(hJKD);
		polarization_tensors.push_back(hKKD);

		polarization_tensors_conjugate.push_back(hmKcD);
		polarization_tensors_conjugate.push_back(hpKcD);
		polarization_tensors_conjugate.push_back(hIKcD);
		polarization_tensors_conjugate.push_back(hJKcD);
		polarization_tensors_conjugate.push_back(hKKcD);
	}
	if constexpr (D>=8){
		Tensor<T,D,2> hmLD(externalWF_tensorboson_mID<T,T,D,6>(mconf,1,ref));
		Tensor<T,D,2> hpLD(externalWF_tensorboson_pID<T,T,D,6>(mconf,1,ref));
		Tensor<T,D,2> hILD(externalWF_tensorboson_IJD<T,T,D,3,6>(mconf,1,ref));
		Tensor<T,D,2> hJLD(externalWF_tensorboson_IJD<T,T,D,4,6>(mconf,1,ref));
		Tensor<T,D,2> hKLD(externalWF_tensorboson_IJD<T,T,D,5,6>(mconf,1,ref));
		Tensor<T,D,2> hLLD(externalWF_tensorboson_IJD<T,T,D,6,6>(mconf,1,ref));

		Tensor<T,D,2> hmLcD(externalWF_tensorboson_mIcD<T,T,D,6>(mconf,2,ref));
		Tensor<T,D,2> hpLcD(externalWF_tensorboson_pIcD<T,T,D,6>(mconf,2,ref));
		Tensor<T,D,2> hILcD(externalWF_tensorboson_IJcD<T,T,D,3,6>(mconf,2,ref));
		Tensor<T,D,2> hJLcD(externalWF_tensorboson_IJcD<T,T,D,4,6>(mconf,2,ref));
		Tensor<T,D,2> hKLcD(externalWF_tensorboson_IJcD<T,T,D,5,6>(mconf,2,ref));
		Tensor<T,D,2> hLLcD(externalWF_tensorboson_IJcD<T,T,D,6,6>(mconf,2,ref));

		polarization_tensors.push_back(hmLD);
		polarization_tensors.push_back(hpLD);
		polarization_tensors.push_back(hILD);
		polarization_tensors.push_back(hJLD);
		polarization_tensors.push_back(hKLD);
		polarization_tensors.push_back(hLLD);

		polarization_tensors_conjugate.push_back(hmLcD);
		polarization_tensors_conjugate.push_back(hpLcD);
		polarization_tensors_conjugate.push_back(hILcD);
		polarization_tensors_conjugate.push_back(hJLcD);
		polarization_tensors_conjugate.push_back(hKLcD);
		polarization_tensors_conjugate.push_back(hLLcD);
	}
	if constexpr (D>=9){
		Tensor<T,D,2> hmMD(externalWF_tensorboson_mID<T,T,D,7>(mconf,1,ref));
		Tensor<T,D,2> hpMD(externalWF_tensorboson_pID<T,T,D,7>(mconf,1,ref));
		Tensor<T,D,2> hIMD(externalWF_tensorboson_IJD<T,T,D,3,7>(mconf,1,ref));
		Tensor<T,D,2> hJMD(externalWF_tensorboson_IJD<T,T,D,4,7>(mconf,1,ref));
		Tensor<T,D,2> hKMD(externalWF_tensorboson_IJD<T,T,D,5,7>(mconf,1,ref));
		Tensor<T,D,2> hLMD(externalWF_tensorboson_IJD<T,T,D,6,7>(mconf,1,ref));
		Tensor<T,D,2> hMMD(externalWF_tensorboson_IJD<T,T,D,7,7>(mconf,1,ref));

		Tensor<T,D,2> hmMcD(externalWF_tensorboson_mIcD<T,T,D,7>(mconf,2,ref));
		Tensor<T,D,2> hpMcD(externalWF_tensorboson_pIcD<T,T,D,7>(mconf,2,ref));
		Tensor<T,D,2> hIMcD(externalWF_tensorboson_IJcD<T,T,D,3,7>(mconf,2,ref));
		Tensor<T,D,2> hJMcD(externalWF_tensorboson_IJcD<T,T,D,4,7>(mconf,2,ref));
		Tensor<T,D,2> hKMcD(externalWF_tensorboson_IJcD<T,T,D,5,7>(mconf,2,ref));
		Tensor<T,D,2> hLMcD(externalWF_tensorboson_IJcD<T,T,D,6,7>(mconf,2,ref));
		Tensor<T,D,2> hMMcD(externalWF_tensorboson_IJcD<T,T,D,7,7>(mconf,2,ref));

		polarization_tensors.push_back(hmMD);
		polarization_tensors.push_back(hpMD);
		polarization_tensors.push_back(hIMD);
		polarization_tensors.push_back(hJMD);
		polarization_tensors.push_back(hKMD);
		polarization_tensors.push_back(hLMD);
		polarization_tensors.push_back(hMMD);

		polarization_tensors_conjugate.push_back(hmMcD);
		polarization_tensors_conjugate.push_back(hpMcD);
		polarization_tensors_conjugate.push_back(hIMcD);
		polarization_tensors_conjugate.push_back(hJMcD);
		polarization_tensors_conjugate.push_back(hKMcD);
		polarization_tensors_conjugate.push_back(hLMcD);
		polarization_tensors_conjugate.push_back(hMMcD);
	}
	if constexpr (D>= 10){
		Tensor<T,D,2> hmND(externalWF_tensorboson_mID<T,T,D,8>(mconf,1,ref));
		Tensor<T,D,2> hpND(externalWF_tensorboson_pID<T,T,D,8>(mconf,1,ref));
		Tensor<T,D,2> hIND(externalWF_tensorboson_IJD<T,T,D,3,8>(mconf,1,ref));
		Tensor<T,D,2> hJND(externalWF_tensorboson_IJD<T,T,D,4,8>(mconf,1,ref));
		Tensor<T,D,2> hKND(externalWF_tensorboson_IJD<T,T,D,5,8>(mconf,1,ref));
		Tensor<T,D,2> hLND(externalWF_tensorboson_IJD<T,T,D,6,8>(mconf,1,ref));
		Tensor<T,D,2> hMND(externalWF_tensorboson_IJD<T,T,D,7,8>(mconf,1,ref));
		Tensor<T,D,2> hNND(externalWF_tensorboson_IJD<T,T,D,8,8>(mconf,1,ref));

		Tensor<T,D,2> hmNcD(externalWF_tensorboson_mIcD<T,T,D,8>(mconf,2,ref));
		Tensor<T,D,2> hpNcD(externalWF_tensorboson_pIcD<T,T,D,8>(mconf,2,ref));
		Tensor<T,D,2> hINcD(externalWF_tensorboson_IJcD<T,T,D,3,8>(mconf,2,ref));
		Tensor<T,D,2> hJNcD(externalWF_tensorboson_IJcD<T,T,D,4,8>(mconf,2,ref));
		Tensor<T,D,2> hKNcD(externalWF_tensorboson_IJcD<T,T,D,5,8>(mconf,2,ref));
		Tensor<T,D,2> hLNcD(externalWF_tensorboson_IJcD<T,T,D,6,8>(mconf,2,ref));
		Tensor<T,D,2> hMNcD(externalWF_tensorboson_IJcD<T,T,D,7,8>(mconf,2,ref));
		Tensor<T,D,2> hNNcD(externalWF_tensorboson_IJcD<T,T,D,8,8>(mconf,2,ref));

		polarization_tensors.push_back(hmND);
		polarization_tensors.push_back(hpND);
		polarization_tensors.push_back(hIND);
		polarization_tensors.push_back(hJND);
		polarization_tensors.push_back(hKND);
		polarization_tensors.push_back(hLND);
		polarization_tensors.push_back(hMND);
		polarization_tensors.push_back(hNND);

		polarization_tensors_conjugate.push_back(hmNcD);
		polarization_tensors_conjugate.push_back(hpNcD);
		polarization_tensors_conjugate.push_back(hINcD);
		polarization_tensors_conjugate.push_back(hJNcD);
		polarization_tensors_conjugate.push_back(hKNcD);
		polarization_tensors_conjugate.push_back(hLNcD);
		polarization_tensors_conjugate.push_back(hMNcD);
		polarization_tensors_conjugate.push_back(hNNcD);
	}
}

template <class T,size_t D> void get_statesum(std::vector<Tensor<T,D,2>>& polarization_tensors,std::vector<Tensor<T,D,2>>& polarization_tensors_conjugate,Tensor<T,D,4>& statesum){
	size_t basis_size=polarization_tensors.size();
	for (size_t ii = 0; ii < basis_size; ii++) {
		statesum += concatenate_tensors(polarization_tensors.at(ii),polarization_tensors_conjugate.at(ii));
	}
}

template <class T,size_t D> bool check_tensor_basissize(std::vector<Tensor<T,D,2>>& polarization_tensors,std::vector<Tensor<T,D,2>>& polarization_tensors_conjugate){
	if (polarization_tensors.size() == (D*(D-3))/2 &&polarization_tensors_conjugate.size() == (D*(D-3))/2 ){
		return true;
	}
	std::cout << "Tensor basis has incorrect dimension" << '\n';
	std::cout << "#(tensor_states)" << polarization_tensors.size()  << '\n';
	std::cout << "Ds*(Ds-3)/2" << (D*(D-3))/2 << '\n';
	return false;
}

template <class T,size_t D> bool check_tensor_symmetry(std::vector<Tensor<T,D,2>>& polarization_tensors,std::vector<Tensor<T,D,2>>& polarization_tensors_conjugate){
	Tensor<T,D,2> antysmmetric_part(0);
	Tensor<T,D,2> antysmmetric_partC(0);
	bool isSymmetric=true;
	size_t basis_size=polarization_tensors.size();
	for (size_t ii = 0; ii < basis_size; ii++) {
			antysmmetric_part = polarization_tensors.at(ii)-swap_indices<0,1>(polarization_tensors.at(ii));
			antysmmetric_partC = polarization_tensors_conjugate.at(ii)-swap_indices<0,1>(polarization_tensors_conjugate.at(ii));
		if(!is_zero_tensor(antysmmetric_part)){
			isSymmetric = false;
			std::cout << "state h(" << ii+1 << ") not symmetric" << std::endl;
            std::cout << antysmmetric_part << std::endl;
		}
		if(!is_zero_tensor(antysmmetric_partC)){
			isSymmetric = false;
			std::cout << "state hC(" << ii+1 << ") not symmetric" << std::endl;
            std::cout << antysmmetric_partC << std::endl;
		}
	}
	return isSymmetric;
}

template <class T,size_t D> bool check_tensor_trace(std::vector<Tensor<T,D,2>>& polarization_tensors,std::vector<Tensor<T,D,2>>& polarization_tensors_conjugate){
	T trace(0);
	T traceC(0);
	bool isTraceless=true;
	size_t basis_size=polarization_tensors.size();
	for (size_t ii = 0; ii < basis_size; ii++) {
			trace = truncateSmall(trace_rank2(polarization_tensors.at(ii)));
			traceC = truncateSmall(trace_rank2(polarization_tensors_conjugate.at(ii)));
		if(!is_zero(trace)){
			isTraceless = false;
			#if _SEE_DEBUG_DETAILS_TCOMPLETENESS
			std::cout << "state h(" << ii+1 << ") not traceless" << endl;
			std::cout << "tr(h) = " << trace << endl;
			#endif
		}
		if(!is_zero(traceC)){
			isTraceless = false;
			#if _SEE_DEBUG_DETAILS_TCOMPLETENESS
			std::cout << "state hC(" << ii+1 << ") not traceless" << endl;
			std::cout << "tr(hC) = " << traceC << endl;
			#endif
		}
	}
	return isTraceless;
}

template <class T,size_t D> bool check_tensor_transversality(std::vector<Tensor<T,D,2>>& polarization_tensors,std::vector<Tensor<T,D,2>>& polarization_tensors_conjugate,const momD<T,D>& l){

	Tensor<T,D,1> l_h,l_hC;
	bool isTransversal=true;
	size_t basis_size=polarization_tensors.size();
	   for (size_t ii = 0; ii < basis_size; ii++) {
			 l_h = index_contraction<0>(l,polarization_tensors.at(ii));
			 l_hC = index_contraction<0>(l,polarization_tensors_conjugate.at(ii));
				if(!is_zero_tensor(l_h)||!is_zero_tensor(l_h)){
				isTransversal = false;
					#if _SEE_DEBUG_DETAILS_TCOMPLETENESS
						std::cout << "l*h(" << ii <<") = "<< l_h << '\n';
						std::cout << "l*hC(" << ii <<") = "<< l_hC << '\n';
						#endif
				}
	}
	return isTransversal;
}

template <class T,size_t D> bool check_tensor_orthonormality(std::vector<Tensor<T,D,2>>& polarization_tensors,std::vector<Tensor<T,D,2>>& polarization_tensors_conjugate,const T& norm){

	size_t basis_size=polarization_tensors.size();
	vector<vector<T>> gram(basis_size, vector<T>(basis_size));
  bool isOrthonormalBasis = true;

  for (size_t ii = 0; ii < basis_size; ii++) {
 	for (size_t jj = 0; jj < basis_size; jj++) {
 		gram[ii][jj]=truncateSmall(IP_rank2(polarization_tensors.at(ii),polarization_tensors_conjugate.at(jj)));
 		if (ii==jj&& !is_zero(gram[ii][jj]-norm)) {
 			isOrthonormalBasis = false;

 		}
 		if (ii!=jj&& !is_zero(gram[ii][jj])) {
 			isOrthonormalBasis = false;
 		}
 	}
  }
	#if _SEE_DEBUG_DETAILS_TCOMPLETENESS
	if (!isOrthonormalBasis) {
		std::cout << "states are not orthonormal" << '\n';
		std::cout << "Gram matrix is" << '\n';
		for (size_t ii = 0; ii < basis_size; ii++) {
				for (size_t jj = 0; jj < basis_size; jj++) {
					std::cout << gram[ii][jj]<< ' ';
				}
				std::cout<< '\n';
			}
	}
	#endif
	return isOrthonormalBasis;
}

template <class T,size_t D> bool check_tensor_completeness(std::vector<Tensor<T,D,2>>& polarization_tensors,std::vector<Tensor<T,D,2>>& polarization_tensors_conjugate,const momD<T,D>& l,bool bref,const momD<T,D>& mref){

// hold the negative of the input momentum
	momD<T,D> ml(-l);
	// build the momD_conf { l, -l }
	momD_conf<T,D> mconf(l,ml);
	// in case ref is passed
	size_t ref(0);
	if(bref)
		ref=mconf.insert(mref);

	Tensor<T,D,4> proj2(transversal_traceless_Projector<T,T,D>(mconf,1,ref));
	Tensor<T,D,4> statesum(0);
	get_statesum<T,D>(polarization_tensors,polarization_tensors_conjugate,statesum);

	Tensor<T,D,4> diffTensor(0);
	diffTensor=(statesum-proj2);
	bool isCompleteBas= is_zero_tensor(diffTensor);
	#if _SEE_DEBUG_DETAILS_TCOMPLETENESS
  if(!isCompleteBas){
 		std::cout << "basis does not satisfy completeness relation" << '\n';
 		std::cout << "Difference is:" << '\n';
 		std::cout << diffTensor<< '\n';
  }
	#endif
	return isCompleteBas;
}

template<typename T,size_t D> enable_if_t<is_complex<T>::value && !Caravel::is_exact<T>::value,bool> check_fixed_Ds(){

 vector<T> momvec;

	if (D == 4) {
		momvec ={T(3),T(2),T(1),T(2)};
	}
	if (D == 5) {
		momvec ={T(3),T(2),T(0),T(1),T(2)};
	}
	if (D >=6) {
	//	momvec ={T(3),T(2),T(0),T(0),T(1),T(2)};
		momvec ={T(18),T(8),T(7),T(3),T(9),T(11)};
	}

	const momD<T,D> l(momvec);
	// const momD<T,D> l(momvec2);

	momD<T,D> ml(-l);
	momD_conf<T,D> mconf(l,ml);
	//std::cout <<"l*l="<< l*l << '\n';

	std::vector<T> rmom(D,T(0));
	ASSIGN_lightcone_ref_2(&rmom[0]);
	auto mref = momD<T,D>(rmom);

	// polarization tensors
	std::vector<Tensor<T,D,2>> polarization_tensors;
	std::vector<Tensor<T,D,2>> polarization_tensors_conjugate;
	get_tensor_states_D<T,D>(polarization_tensors,polarization_tensors_conjugate,l,true,mref);
	bool b1,b2,b3,b4,b5,b6;

	b1=check_tensor_basissize<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b2=check_tensor_symmetry<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b3=check_tensor_trace<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b4=check_tensor_transversality<T,D>(polarization_tensors,polarization_tensors_conjugate,l);
	b5=check_tensor_orthonormality<T,D>(polarization_tensors,polarization_tensors_conjugate,T(1));
	b6=check_tensor_completeness<T,D>(polarization_tensors,polarization_tensors_conjugate,l,true,mref);
	return (b1&&b2&&b3&&b4&&b5&&b6);
}

template<typename T,size_t D> enable_if_t<!is_complex<T>::value && !Caravel::is_exact<T>::value,bool> check_fixed_Ds(){

 	vector<T> momvec;
	if(D == 4){
		momvec ={T(7),T(9),T(6),T(2)};
	}
	if(D == 5){
		momvec ={T(1),T(25),T(16),T(91),T(93)};
	}
	if(D >=6){
			momvec ={T(28),T(37),T(28),T(18),T(63),T(62)};
	}
	const momD<T,D> l(momvec);
	// const momD<T,D> l(momvec2);

	momD<T,D> ml(-l);
	momD_conf<T,D> mconf(l,ml);
	//std::cout <<"l*l="<< l*l << '\n';


	std::vector<T> rmom(D,T(0));
	ASSIGN_lightcone_ref_2(&rmom[0]);
	auto mref = momD<T,D>(rmom);


	// polarization tensors
	std::vector<Tensor<T,D,2>> polarization_tensors;
	std::vector<Tensor<T,D,2>> polarization_tensors_conjugate;
	get_tensor_states_D<T,D>(polarization_tensors,polarization_tensors_conjugate,l,true,mref);
	bool b1,b2,b3,b4,b5,b6;

	b1=check_tensor_basissize<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b2=check_tensor_symmetry<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b3=check_tensor_trace<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b4=check_tensor_transversality<T,D>(polarization_tensors,polarization_tensors_conjugate,l);
	b5=check_tensor_orthonormality<T,D>(polarization_tensors,polarization_tensors_conjugate,T(1));
	b6=check_tensor_completeness<T,D>(polarization_tensors,polarization_tensors_conjugate,l,true,mref);
	return (b1&&b2&&b3&&b4&&b5&&b6);
}

template<typename T,size_t D> enable_if_t<!is_complex<T>::value && Caravel::is_exact<T>::value,bool> check_fixed_Ds(){
	size_t cardinality(2147483137);
 	vector<T> momvec;

	if(D == 4){
		momvec ={T(7),T(9),T(6),T(2)};
	}
	if(D == 5){
		EpsilonBasis<T>::squares[0]=T(2006987996);
		momvec = {T(1585725295),T(1146154864),T(1471034993),T(1405893991),T(1)};
	}
	if(D >=6){
		EpsilonBasis<T>::squares[0]=T(2);
		EpsilonBasis<T>::squares[1]=T(4);
		momvec ={T(9),T(12),T(9),T(2),T(1),T(2)};
	}
	cardinality_update_manager<T>::get_instance()->set_cardinality(cardinality);
	const momD<T,D> l(momvec);
	// const momD<T,D> l(momvec2);

momD<T,D> ml(-l);
momD_conf<T,D> mconf(l,ml);
//std::cout <<"l*l="<< l*l << '\n';


std::vector<T> rmom(D,T(0));
ASSIGN_lightcone_ref_2(&rmom[0]);
auto mref = momD<T,D>(rmom);

// polarization tensors
std::vector<Tensor<T,D,2>> polarization_tensors;
std::vector<Tensor<T,D,2>> polarization_tensors_conjugate;
get_tensor_states_D<T,D>(polarization_tensors,polarization_tensors_conjugate,l,true,mref);
	bool b1,b2,b3,b4,b5,b6;

	b1=check_tensor_basissize<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b2=check_tensor_symmetry<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b3=check_tensor_trace<T,D>(polarization_tensors,polarization_tensors_conjugate);
	b4=check_tensor_transversality<T,D>(polarization_tensors,polarization_tensors_conjugate,l);
	b5=check_tensor_orthonormality<T,D>(polarization_tensors,polarization_tensors_conjugate,T(1));
	b6=check_tensor_completeness<T,D>(polarization_tensors,polarization_tensors_conjugate,l,true,mref);
	return (b1&&b2&&b3&&b4&&b5&&b6);
}


template<typename T, size_t D1, size_t D2> std::enable_if_t<(D2==D1),bool>  check_all_Ds(){
	return check_fixed_Ds<T,D1>();
}

template<typename T, size_t D1, size_t D2>  std::enable_if_t<(D2>D1),bool>  check_all_Ds(){
	 if(!check_fixed_Ds<T,D2>()){
	 	return false;
	 }
	return check_all_Ds<T,D1,D2-1>();
}


TEST_CASE("Completeness relation for tensor states:","[States]"){
	CHECK(check_all_Ds<C,5,10>());
	CHECK(check_all_Ds<R,5,10>());
	CHECK(check_all_Ds<F32,5,10>());
}
