/**
 * vector_states_completeness.cpp
 *
 * First created on 1.4.2018
 *
 * Checking vector states completeness relation
 *
*/

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>
#include <cassert>

#include "catch.hpp"
#include "Core/typedefs.h"
#include "Core/momD_conf.h"
#include "Forest/Model.h"
#include "Core/Debug.h"
#include "Forest/Contact/vectorboson_utilities.hpp"
#include "Core/Utilities.h"
#include "Core/type_traits_extra.h"
#include "PhaseSpace/PhaseSpace.h"
#include "compare_tools.hpp"

using namespace Caravel;

#define _SEE_DEBUG_DETAILS_VCOMPLETENESS 0

#define _LARGE_VAL 1000000000
template <class T> std::enable_if_t<!Caravel::is_exact<T>::value,std::complex<T>> CHOP_VS(std::complex<T> in){
	T r(in.real());
	T c(in.imag());
	if(abs(in.real())<T(1)/T(_LARGE_VAL))
		r=0;
	if(abs(in.imag())<T(1)/T(_LARGE_VAL))
		c=0;
	return std::complex<T>(r,c);
}

template <class T> std::enable_if_t<Caravel::is_exact<T>::value,T> CHOP_VS(T in){
	return in;
}

#ifdef USE_FINITE_FIELDS
std::string CHECK_MINUS_ONE(const F32& in){
	if(in==F32(-1))
		return "-1";
	else
		return std::to_string(int(in));
}
#endif

template <class T,size_t D> std::complex<T> GD(const momD<T,D>& p1,const momD<T,D>& p2,const momD<T,D>& p3,const momD<T,D>& p4){
	T gd2(T(2)*(T(4)*(p1*p2+p1*p3)*(p4*p2+p4*p3))-T(2)*(T(4)*(p1*p4+p2*p3)));
	return sqrt(gd2);
}

template <class T,size_t D,size_t i> std::enable_if_t<i<=D-2> ADD_TRIVIAL_STATE(std::vector<std::vector<T>>& e,size_t ii,const momD_conf<T,D>& mconf,size_t ref){
	if(ii>1){
		std::cout<<"Did nothing because i: "<<ii<<" should be 0 (pol vector) or 1 (conjugated)"<<std::endl;
		return;
	}
	else if(ii==0)
		e.push_back(externalWF_vectorboson_trivial_Ds<T,T,D,i>(mconf,1,ref));
	else
		e.push_back(externalWF_vectorboson_trivial_conjugated_Ds<T,T,D,i>(mconf,2,ref));
}

template <class T,size_t D,size_t i> std::enable_if_t<i>=D-1> ADD_TRIVIAL_STATE(std::vector<std::vector<T>>& e,size_t ii,const momD_conf<T,D>& mconf,size_t ref){
	// do nothing
}

template <class T> std::enable_if_t<!Caravel::is_exact<T>::value> NORMALIZE_EXACT(size_t i,T& normalize){
	// do nothing for floating-point types
}

template <class T> std::enable_if_t<Caravel::is_exact<T>::value> NORMALIZE_EXACT(size_t i,T& normalize){
	if(i!=4&&i!=5){
		std::cout<<"ERROR! In NORMALIZE_EXACT(.) unsigned integer should be 4 or 5 and got: "<<i<<std::endl;
		exit(1);
	}
	// multiply by proper factor
	normalize*=EpsilonBasis<T>::squares[i-4];
}


template <class T,size_t D> void get_induced_metric_from_vector_states_D(std::vector<std::vector<T>>& GMUNU,const std::vector<std::vector<T>>& eps,const std::vector<std::vector<T>>& epsc,
		const momD<T,D>& l){
	// resize GMUNU
	GMUNU.clear();
	for(size_t ii=0;ii<D;++ii)
		GMUNU.push_back(std::vector<T>(D,T(0)));

	T mass4D(0);
	_momD_internal::scalar_product_impl<4>(l.begin(),l.begin(),mass4D);
	T massD(0);
	_momD_internal::scalar_product_impl<D,4>(l.begin(),l.begin(),massD);

	// container for \sum_{states} eps^{mu} * epsc^{nu}
	T sumstates[D][D];
	// container for 4-D l^{mu}l^{nu}/mass4D
	T complement4D[D][D];
	// container for D-dim components in momentum for l^{mu}l^{nu}/massD
	T complementD[D][D];
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			// add over the four correlated states
			sumstates[ii][jj]=T(0);
			for(size_t kk=0;kk<eps.size();++kk)	sumstates[ii][jj]+=eps[kk][ii]*epsc[kk][jj];
			if(ii<4&&jj<4)
				complement4D[ii][jj]=(l.pi(ii)*l.pi(jj))/mass4D;
			else
				complement4D[ii][jj]=T(0);
			if(ii>=4&&jj>=4)
				complementD[ii][jj]=(l.pi(ii)*l.pi(jj))/massD;
			else
				complementD[ii][jj]=T(0);
		}
	}
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			GMUNU[ii][jj]=(-sumstates[ii][jj]+complement4D[ii][jj]+complementD[ii][jj]);
			if(ii==jj&&(ii==4||ii==5))
				NORMALIZE_EXACT(ii,GMUNU[ii][jj]);
		}
	}
#if _SEE_DEBUG_DETAILS_VCOMPLETENESS
	std::cout<<"================="<<std::endl;
	std::cout<<"sumstates:"<<std::endl;
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			std::cout<<CHOP_VS(sumstates[ii][jj])<<" ";
		}
		std::cout<<std::endl;
	}
	std::cout<<"================="<<std::endl;
	std::cout<<"light-cone complement4D:"<<std::endl;
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			std::cout<<CHOP_VS(complement4D[ii][jj])<<" ";
		}
		std::cout<<std::endl;
	}
	std::cout<<"================="<<std::endl;
	std::cout<<"light-cone complementD:"<<std::endl;
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			std::cout<<CHOP_VS(complementD[ii][jj])<<" ";
		}
		std::cout<<std::endl;
	}
	std::cout<<"================="<<std::endl;
	std::cout<<"g_{munu} = - sumstates + light-cone complement4D + complementD:"<<std::endl;
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			std::cout<<CHOP_VS(GMUNU[ii][jj])<<" ";
		}
		std::cout<<std::endl;
	}
#endif
}

template <class T,size_t D> void get_vector_states_D(std::vector<std::vector<T>>& eps,std::vector<std::vector<T>>& epsc,const momD<T,D>& l,bool bref,const momD<T,D>& mref){
	static_assert(D>4,"In function completeness_floating_point D should be greater than 4");
	// hold the negative of the input momentum
	momD<T,D> ml(-l);
	// build the momD_conf { l, -l }
	momD_conf<T,D> mconf(l,ml);
	// in case ref is passed
	size_t ref(0);
	if(bref)
		ref=mconf.insert(mref);

	// add all states
	// m
	eps.push_back(externalWF_vectorboson_mD<T,T,D>(mconf,1,ref));
	eps.push_back(externalWF_vectorboson_pD<T,T,D>(mconf,1,ref));
	eps.push_back(externalWF_vectorboson_sD<T,T,D>(mconf,1,ref));
	if(D>5)
		eps.push_back(externalWF_vectorboson_tD<T,T,D>(mconf,1,ref));
	for(size_t ii=7;ii<=D;ii++)
		if(ii==7)
			ADD_TRIVIAL_STATE<T,D,5>(eps,0,mconf,ref);
		else if(ii==8)
			ADD_TRIVIAL_STATE<T,D,6>(eps,0,mconf,ref);
		else if(ii==9)
			ADD_TRIVIAL_STATE<T,D,7>(eps,0,mconf,ref);
		else if(ii==10)
			ADD_TRIVIAL_STATE<T,D,8>(eps,0,mconf,ref);
	epsc.push_back(externalWF_vectorboson_pD<T,T,D>(mconf,2,ref));
	epsc.push_back(externalWF_vectorboson_mD<T,T,D>(mconf,2,ref));
	epsc.push_back(externalWF_vectorboson_scD<T,T,D>(mconf,2,ref));
	if(D>5)
		epsc.push_back(externalWF_vectorboson_tcD<T,T,D>(mconf,2,ref));
	for(size_t ii=7;ii<=D;ii++)
		if(ii==7)
			ADD_TRIVIAL_STATE<T,D,5>(epsc,1,mconf,ref);
		else if(ii==8)
			ADD_TRIVIAL_STATE<T,D,6>(epsc,1,mconf,ref);
		else if(ii==9)
			ADD_TRIVIAL_STATE<T,D,7>(epsc,1,mconf,ref);
		else if(ii==10)
			ADD_TRIVIAL_STATE<T,D,8>(epsc,1,mconf,ref);
}

template <class T,size_t D> void get_induced_metric_from_vector_states_and_reference(std::vector<std::vector<T>>& GMUNU,const std::vector<std::vector<T>>& eps,const std::vector<std::vector<T>>& epsc,
		const momD<T,D>& l,const momD<T,D>& ref){
	// resize GMUNU
	GMUNU.clear();
	for(size_t ii=0;ii<D;++ii)
		GMUNU.push_back(std::vector<T>(D,T(0)));

	// container for \sum_{states} eps^{mu} * epsc^{nu}
	T sumstates[D][D];
	// container for (l^{mu}ref^{nu}+l^{nu}ref^{mu})/l.ref
	T complementD[D][D];
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			// add over the four correlated states
			sumstates[ii][jj]=T(0);
			for(size_t kk=0;kk<eps.size();++kk)	sumstates[ii][jj]+=eps[kk][ii]*epsc[kk][jj];
			complementD[ii][jj]=(l.pi(ii)*ref.pi(jj)+l.pi(jj)*ref.pi(ii))/(l*ref);
		}
	}
	// fill GMUNU = - sumstates + complementD
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			GMUNU[ii][jj]=(-sumstates[ii][jj]+complementD[ii][jj]);
			if(ii==jj&&(ii==4||ii==5))
				NORMALIZE_EXACT(ii,GMUNU[ii][jj]);
		}
	}
#if _SEE_DEBUG_DETAILS_VCOMPLETENESS
	std::cout<<"================="<<std::endl;
	std::cout<<"projective sumstates:"<<std::endl;
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			std::cout<<CHOP_VS(sumstates[ii][jj])<<" ";
		}
		std::cout<<std::endl;
	}
	std::cout<<"================="<<std::endl;
	std::cout<<"projective light-cone complementD:"<<std::endl;
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			std::cout<<CHOP_VS(complementD[ii][jj])<<" ";
		}
		std::cout<<std::endl;
	}
	std::cout<<"================="<<std::endl;
	std::cout<<"g_{munu} = - sumstates + complementD:"<<std::endl;
	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
			std::cout<<CHOP_VS(GMUNU[ii][jj])<<" ";
		}
		std::cout<<std::endl;
	}
#endif
}

template <class T> void get_vector_states_4D(std::vector<std::vector<T>>& eps,std::vector<std::vector<T>>& epsc,const momD<T,4>& l,bool bref,const momD<T,4>& mref){
	// hold the negative of the input momentum
	momD<T,4> ml(-l);
	// build the momD_conf { l, -l }
	momD_conf<T,4> mconf(l,ml);
	// in case ref is passed
	size_t ref(0);
	if(bref)
		ref=mconf.insert(mref);
	momD<T,4> local_ref;
	if(bref)
		local_ref=mref;
	else{
		// the default choice is parity conjugated momentum
		std::vector<T> rmom(4,T(0));
		rmom[0]=l.pi(0);
		rmom[1]=-l.pi(1);
		rmom[2]=-l.pi(2);
		rmom[3]=-l.pi(3);
		local_ref=momD<T,4>(rmom);
	}
#if _SEE_DEBUG_DETAILS_VCOMPLETENESS
std::cout<<"Input momenta in get_vector_states_4D(.):"<<std::endl;
std::cout<<"	 l  = "<<l<<std::endl;
std::cout<<"	-l  = "<<ml<<std::endl;
std::cout<<"	ref = "<<local_ref<<std::endl;
#endif

	// clear any existing states
	eps.clear();
	epsc.clear();

	// add all states
	// m
	eps.push_back(externalWF_vectorboson_m<T,T,4>(mconf,1,ref));
	eps.push_back(externalWF_vectorboson_p<T,T,4>(mconf,1,ref));

	epsc.push_back(externalWF_vectorboson_p<T,T,4>(mconf,1,ref));
	epsc.push_back(externalWF_vectorboson_m<T,T,4>(mconf,1,ref));

#if _SEE_DEBUG_DETAILS_VCOMPLETENESS
std::cout<<"4D polarization states: "<<std::endl;
std::cout<<"	e^{-}(l)  = "<<eps[0]<<std::endl;
std::cout<<"	e^{+}(l)  = "<<eps[1]<<std::endl;
std::cout<<"	ec^{+}(l)= "<<epsc[0]<<std::endl;
std::cout<<"	ec^{-}(l)= "<<epsc[1]<<std::endl;
#endif

	// external 4-D states each have missing (factored out) 1/sqrt(2) factor. We include it through the epsc states
	for(size_t ii=0;ii<epsc.size();++ii)
		for(size_t jj=0;jj<epsc[ii].size();++jj)
			epsc[ii][jj]*=T(1)/T(2);
}


template <class T,size_t D,size_t i> std::enable_if_t<i<=D> ADD_PROJECTIVE_STATE(std::vector<std::vector<T>>& e,size_t ii,const momD_conf<T,D>& mconf,size_t ref){
	if(ii>1){
		std::cout<<"Did nothing because i: "<<ii<<" should be 0 (pol vector) or 1 (conjugated)"<<std::endl;
		return;
	}
	else if(ii==0)
		e.push_back(externalWF_vectorboson_mui<T,T,D,i-1>(mconf,1,ref));
	else
		e.push_back(externalWF_vectorboson_conti<T,T,D,i-1>(mconf,2,ref));
}

template <class T,size_t D,size_t i> std::enable_if_t<i>=D+1> ADD_PROJECTIVE_STATE(std::vector<std::vector<T>>& e,size_t ii,const momD_conf<T,D>& mconf,size_t ref){
	// do nothing
}

template <class T,size_t D> void get_projective_vector_states(std::vector<std::vector<T>>& eps,std::vector<std::vector<T>>& epsc,const momD<T,D>& l,bool bref,momD<T,D>& mref){
	static_assert(D>3,"In function completeness_projective_floating_point D should be greater than 3");
	// hold the negative of the input momentum
	momD<T,D> ml(-l);
	// build the momD_conf { l, -l }
	momD_conf<T,D> mconf(l,ml);
	if(!bref){
		std::vector<T> rmom(D,T(0));
		ASSIGN_lightcone_ref_1(&rmom[0]);
		mref=momD<T,D>(rmom);
	}
	// in case ref is passed
	size_t ref(0);
	if(bref)
		ref=mconf.insert(mref);

	for(size_t ii=1;ii<=D;ii++){
		if(ii==1){
			ADD_PROJECTIVE_STATE<T,D,1>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,1>(epsc,1,mconf,ref);
		}
		if(ii==2){
			ADD_PROJECTIVE_STATE<T,D,2>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,2>(epsc,1,mconf,ref);
		}
		if(ii==3){
			ADD_PROJECTIVE_STATE<T,D,3>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,3>(epsc,1,mconf,ref);
		}
		if(ii==4){
			ADD_PROJECTIVE_STATE<T,D,4>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,4>(epsc,1,mconf,ref);
		}
		if(ii==5){
			ADD_PROJECTIVE_STATE<T,D,5>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,5>(epsc,1,mconf,ref);
		}
		if(ii==6){
			ADD_PROJECTIVE_STATE<T,D,6>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,6>(epsc,1,mconf,ref);
		}
		if(ii==7){
			ADD_PROJECTIVE_STATE<T,D,7>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,7>(epsc,1,mconf,ref);
		}
		if(ii==8){
			ADD_PROJECTIVE_STATE<T,D,8>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,8>(epsc,1,mconf,ref);
		}
		if(ii==9){
			ADD_PROJECTIVE_STATE<T,D,9>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,9>(epsc,1,mconf,ref);
		}
		if(ii==10){
			ADD_PROJECTIVE_STATE<T,D,10>(eps,0,mconf,ref);
			ADD_PROJECTIVE_STATE<T,D,10>(epsc,1,mconf,ref);
		}
	}
}

namespace {
    template <typename T> std::enable_if_t<Caravel::is_complex<T>::value && !Caravel::is_exact<T>::value,bool> CHECK_IS_DELTAZERO(const T& in){
        return (abs(in)<DeltaZero<typename T::value_type>());
    }

    template <typename T> std::enable_if_t<Caravel::is_exact<T>::value,bool> CHECK_IS_DELTAZERO(const T& in){
        return (in==T(0));
    }

template <class T,size_t D> bool compare_to_metric(const std::vector<std::vector<T>>& GMUNU){
	// Check diagonal of completeness relation
	for(size_t jj=0;jj<D;++jj){
		int value(get_metric_signature<T>(jj));
		if(!CHECK_IS_DELTAZERO((GMUNU[jj][jj]-T(value)))){
#if _SEE_DEBUG_DETAILS_VCOMPLETENESS
std::cout<<"Diagonal check g_{"<<jj<<","<<jj<<"} in compare failed! in: "<<GMUNU[jj][jj]<<" target: "<<T(value)<<" difference: "<<GMUNU[jj][jj]-T(value)<<std::endl;
#endif
			return false;
		}
	}
	// Check off diagonal of completeness relation
	for(size_t jj=0;jj<D;jj++){
		for(size_t kk=0;kk<D;kk++){
			if(jj!=kk){
				if(!CHECK_IS_DELTAZERO(GMUNU[jj][kk])){
#if _SEE_DEBUG_DETAILS_VCOMPLETENESS
std::cout<<"Off-diagonal check g_{"<<jj<<","<<kk<<"} in compare failed! in: "<<GMUNU[jj][kk]<<" target: 0"<<std::endl;
#endif
					return false;
				}
			}
		}
	}
	return true;
}

}

TEST_CASE("Completeness relation for vector states: 4-D and floating point","[States]"){
	// get a random PS point
	PhaseSpace<R> tph(6,1.);
	tph.set_reset_seed(false);
	srand(1110);
	tph.generate();
	auto moms = tph.mom_double();
	momD_conf<C,4> momconf_process = to_momDconf<R,4>(moms);
	momD<C,4> mom_extra(C(13),C(36),C(0,84),C(77));
	momconf_process.insert(mom_extra);
	momD<C,4> mref_4D(C(5),C(3),C(0),C(4));
	// check relations for all momenta in the given PS point
	for(size_t ii=1;ii<=momconf_process.n();++ii){

		SECTION("4D vector completeness using default reference vector (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<C,4>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// the default reference choice is parity conjugated momentum
			momD<C,4> local_ref(l.pi(0),-l.pi(1),-l.pi(2),-l.pi(3));

			// on-shell currents
			std::vector<std::vector<C>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<C>> epsc;
			// set the polarization vectors (with 'false' the ref momentum passed is disregarded)
			get_vector_states_4D<C>(eps,epsc,l,false,local_ref);
			// check normalizations
			// m
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[0],eps[0]));
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[0],epsc[0]));
			// p
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[1],eps[1]));
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[1],epsc[1]));
			// transversity
			for(size_t tt=0;tt<eps.size();++tt){
				// switched to CHECK_IS_DELTAZERO instead of CHECK_ZERO just to avoid a failure that just comes above the threshold of CHECK_ZERO
				CHECK_IS_DELTAZERO(contract_vector_currents_from_0_to<4>(eps[tt],l.get_vector(4)));
				CHECK_IS_DELTAZERO(contract_vector_currents_from_0_to<4>(epsc[tt],(-l).get_vector(4)));
			}

			// get g_{mu,nu}
			std::vector<std::vector<C>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<C,4>(GMUNU,eps,epsc,l,local_ref);
			// check result
			CHECK(compare_to_metric<C,4>(GMUNU));
		}

		SECTION("4D vector completeness with a given reference vector (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<C,4>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// on-shell currents
			std::vector<std::vector<C>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<C>> epsc;
			// set the polarization vectors
			get_vector_states_4D<C>(eps,epsc,l,true,mref_4D);
			// check normalizations
			// m
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[0],eps[0]));
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[0],epsc[0]));
			// p
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[1],eps[1]));
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[1],epsc[1]));
			// transversity
			for(size_t tt=0;tt<eps.size();++tt){
				CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[tt],l.get_vector(4)));
				CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[tt],(-l).get_vector(4)));
			}

			// get g_{mu,nu}
			std::vector<std::vector<C>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<C,4>(GMUNU,eps,epsc,l,mref_4D);
			// check result
			CHECK(compare_to_metric<C,4>(GMUNU));
		}

		SECTION("4D vector completeness using default reference vector and (light-cone) projective states (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<C,4>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// the default reference choice 
			momD<C,4> local_ref;

			// on-shell currents
			std::vector<std::vector<C>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<C>> epsc;
			// set the polarization vectors (with 'false' the ref momentum passed is set accordingly to default choice)
			get_projective_vector_states<C,4>(eps,epsc,l,false,local_ref);

			// get g_{mu,nu}
			std::vector<std::vector<C>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<C,4>(GMUNU,eps,epsc,l,local_ref);
			// check result
			CHECK(compare_to_metric<C,4>(GMUNU));
		}

		SECTION("4D vector completeness with a given reference vector and (light-cone) projective states (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<C,4>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// on-shell currents
			std::vector<std::vector<C>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<C>> epsc;
			// set the polarization vectors
			get_projective_vector_states<C,4>(eps,epsc,l,true,mref_4D);

			// get g_{mu,nu}
			std::vector<std::vector<C>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<C,4>(GMUNU,eps,epsc,l,mref_4D);
			// check result
			CHECK(compare_to_metric<C,4>(GMUNU));
		}
	}
}

#ifdef USE_FINITE_FIELDS
TEST_CASE("Completeness relation for vector states: 4-D and exact","[States]"){

	using F = F32; 
#define _L_CARDINALITY 2147483579
	F::set_p( _L_CARDINALITY );

	momD<F,4> mom_p1_F(F(13),F(36),F(84),F(77));
	momD<F,4> mom_p2_F(F(-16),F(33),F(63),F(56));
	momD<F,4> mom_p3_F(F(17),F(24),F(144),F(143));
	momD_conf<F,4> momconf_process(std::vector<momD<F,4>>({mom_p1_F,mom_p2_F,mom_p3_F}));
	momD<F,4> mref_4D(F(5),F(3),F(0),F(4));

	// check relations for all momenta in the given PS point
	for(size_t ii=1;ii<=momconf_process.n();++ii){

		SECTION("4D vector completeness using default reference vector (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<F,4>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// the default reference choice is parity conjugated momentum
			momD<F,4> local_ref(l.pi(0),-l.pi(1),-l.pi(2),-l.pi(3));

			// on-shell currents
			std::vector<std::vector<F>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<F>> epsc;
			// set the polarization vectors (with 'false' the ref momentum passed is disregarded)
			get_vector_states_4D<F>(eps,epsc,l,false,local_ref);
			// check normalizations
			// m
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[0],eps[0]));
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[0],epsc[0]));
			// p
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[1],eps[1]));
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[1],epsc[1]));
			// transversity
			for(size_t tt=0;tt<eps.size();++tt){
				CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[tt],l.get_vector(4)));
				CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[tt],(-l).get_vector(4)));
			}

			// get g_{mu,nu}
			std::vector<std::vector<F>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<F,4>(GMUNU,eps,epsc,l,local_ref);
			// check result
			CHECK(compare_to_metric<F,4>(GMUNU));
		}

		SECTION("4D vector completeness with a given reference vector (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<F,4>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// on-shell currents
			std::vector<std::vector<F>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<F>> epsc;
			// set the polarization vectors
			get_vector_states_4D<F>(eps,epsc,l,true,mref_4D);
			// check normalizations
			// m
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[0],eps[0]));
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[0],epsc[0]));
			// p
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[1],eps[1]));
			CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[1],epsc[1]));
			// transversity
			for(size_t tt=0;tt<eps.size();++tt){
				CHECK_ZERO(contract_vector_currents_from_0_to<4>(eps[tt],l.get_vector(4)));
				CHECK_ZERO(contract_vector_currents_from_0_to<4>(epsc[tt],(-l).get_vector(4)));
			}

			// get g_{mu,nu}
			std::vector<std::vector<F>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<F,4>(GMUNU,eps,epsc,l,mref_4D);
			// check result
			CHECK(compare_to_metric<F,4>(GMUNU));
		}

		SECTION("4D vector completeness using default reference vector and (light-cone) projective states (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<F,4>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// the default reference choice 
			momD<F,4> local_ref;

			// on-shell currents
			std::vector<std::vector<F>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<F>> epsc;
			// set the polarization vectors (with 'false' the ref momentum passed is set accordingly to default choice)
			get_projective_vector_states<F,4>(eps,epsc,l,false,local_ref);

			// get g_{mu,nu}
			std::vector<std::vector<F>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<F,4>(GMUNU,eps,epsc,l,local_ref);
			// check result
			CHECK(compare_to_metric<F,4>(GMUNU));
		}

		SECTION("4D vector completeness with a given reference vector and (light-cone) projective states (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<F,4>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// on-shell currents
			std::vector<std::vector<F>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<F>> epsc;
			// set the polarization vectors
			get_projective_vector_states<F,4>(eps,epsc,l,true,mref_4D);

			// get g_{mu,nu}
			std::vector<std::vector<F>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<F,4>(GMUNU,eps,epsc,l,mref_4D);
			// check result
			CHECK(compare_to_metric<F,4>(GMUNU));
		}
	}
}
#endif

TEST_CASE("Completeness relation for vector states: D dimensions and floating point","[States]"){
	// some 5-D (loop) momentum (embedded in 8 dimensions)
	C cc1(-3.60288,-1.137045);
	C cc2(-1.87472,-1.54869);
	C cc3(3.14215,1.635333);
	C cc4(0.56148,-1.558988);
	C cc5(0);
	C cc6(0);
	C cc7(0);
	C cc8(0);
	// ensure on-shellness
	cc5=sqrt(cc1*cc1-cc2*cc2-cc3*cc3-cc4*cc4);
	momD<C,8> l5D(cc1,cc2,cc3,cc4,cc5,cc6,cc7,cc8);
	// some 6-D (loop) momentum (embedded in 8 dimensions)
	C c1(-3.60288,-0.137045);
	C c2(1.87472,-0.54869);
	C c3(3.14215,0.635333);
	C c4(0.56148,-0.558988);
	C c5(-0.496214,0.214381);
	C c6(0);
	C c7(0);
	C c8(0);
	// ensure on-shellness
	c6=sqrt(c1*c1-c2*c2-c3*c3-c4*c4-c5*c5);
	momD<C,8> l6D(c1,c2,c3,c4,c5,c6,c7,c8);
	momD_conf<C,8> momconf_process(std::vector<momD<C,8>>({l5D,l6D}));
	momD<C,8> mref(C(5),C(3),C(0),C(4),C(0),C(0),C(0),C(0));
	// check relations for all momenta in the given PS point
	for(size_t ii=1;ii<=momconf_process.n();++ii){

		SECTION("8D vector completeness using default reference vector (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<C,8>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// the default reference (so empty local_ref)
			momD<C,8> local_ref;

			// on-shell currents
			std::vector<std::vector<C>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<C>> epsc;
			// set the polarization vectors (with 'false' the ref momentum passed is disregarded)
			get_vector_states_D<C,8>(eps,epsc,l,false,local_ref);
			// check normalizations
			// m
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[0],eps[0]));
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[0],epsc[0]));
			// p
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[1],eps[1]));
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[1],epsc[1]));
			for(size_t tt=2;tt<eps.size();++tt)
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[tt],eps[tt])
					*contract_vector_currents_from_0_to<8>(epsc[tt],epsc[tt])
					-C(1));
			// transversity
			for(size_t tt=0;tt<eps.size();++tt){
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[tt],l.get_vector(8)));
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[tt],(-l).get_vector(8)));
			}

			// get g_{mu,nu}
			std::vector<std::vector<C>> GMUNU;
			get_induced_metric_from_vector_states_D<C,8>(GMUNU,eps,epsc,l);
			// check result
			CHECK(compare_to_metric<C,8>(GMUNU));
		}

		SECTION("8D vector completeness with a given reference vector (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<C,8>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// on-shell currents
			std::vector<std::vector<C>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<C>> epsc;
			// set the polarization vectors
			get_vector_states_D<C,8>(eps,epsc,l,false,mref);
			// check normalizations
			// m
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[0],eps[0]));
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[0],epsc[0]));
			// p
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[1],eps[1]));
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[1],epsc[1]));
			for(size_t tt=2;tt<eps.size();++tt)
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[tt],eps[tt])
					*contract_vector_currents_from_0_to<8>(epsc[tt],epsc[tt])
					-C(1));
			// transversity
			for(size_t tt=0;tt<eps.size();++tt){
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[tt],l.get_vector(8)));
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[tt],(-l).get_vector(8)));
			}

			// get g_{mu,nu}
			std::vector<std::vector<C>> GMUNU;
			get_induced_metric_from_vector_states_D<C,8>(GMUNU,eps,epsc,l);
			// check result
			CHECK(compare_to_metric<C,8>(GMUNU));
		}

		SECTION("8D vector completeness using default reference vector and (light-cone) projective states (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<C,8>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// the default reference choice 
			momD<C,8> local_ref;

			// on-shell currents
			std::vector<std::vector<C>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<C>> epsc;
			// set the polarization vectors (with 'false' the ref momentum passed is disregarded)
			get_projective_vector_states<C,8>(eps,epsc,l,false,local_ref);

			// get g_{mu,nu}
			std::vector<std::vector<C>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<C,8>(GMUNU,eps,epsc,l,local_ref);
			// check result
			CHECK(compare_to_metric<C,8>(GMUNU));
		}

		SECTION("8D vector completeness with a given reference vector and (light-cone) projective states (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<C,8>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// on-shell currents
			std::vector<std::vector<C>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<C>> epsc;
			// set the polarization vectors
			get_projective_vector_states<C,8>(eps,epsc,l,true,mref);

			// get g_{mu,nu}
			std::vector<std::vector<C>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<C,8>(GMUNU,eps,epsc,l,mref);
			// check result
			CHECK(compare_to_metric<C,8>(GMUNU));
		}
	}
}

#ifdef USE_FINITE_FIELDS
TEST_CASE("Completeness relation for vector states: D dimensions and exact","[States]"){

	using F = F32; 
#define _L2_CARDINALITY 2147483579
	F::set_p( _L2_CARDINALITY );

	// some 6-D (loop) momentum (embedded in 8 dimensions)
	F f1(2050597495);
	F f2(1984505441);
	F f3(1879278111);
	F f4(183614989);
	F f5(1676111971);
	F f6(1);
	// set the finite field epsilon basis
	EpsilonBasis<F>::squares[0]=2127758403;
	EpsilonBasis<F>::squares[1]=1452919809;
	momD<F,8> l6D(f1,f2,f3,f4,f5,f6,F(0),F(0));
	momD_conf<F,8> momconf_process(l6D);
	momD<F,8> mref(F(5),F(3),F(0),F(4),F(0),F(0),F(0),F(0));
	// check relations for all momenta in the given PS point
	for(size_t ii=1;ii<=momconf_process.n();++ii){

		SECTION("8D vector completeness using default reference vector (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<F,8>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// the default reference (so empty local_ref)
			momD<F,8> local_ref;

			// on-shell currents
			std::vector<std::vector<F>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<F>> epsc;
			// set the polarization vectors (with 'false' the ref momentum passed is disregarded)
			get_vector_states_D<F,8>(eps,epsc,l,false,local_ref);
			// check normalizations
			// m
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[0],eps[0]));
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[0],epsc[0]));
			// p
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[1],eps[1]));
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[1],epsc[1]));
			for(size_t tt=2;tt<eps.size();++tt)
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[tt],eps[tt])
					*contract_vector_currents_from_0_to<8>(epsc[tt],epsc[tt])
					-F(1));
			// transversity
			for(size_t tt=0;tt<eps.size();++tt){
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[tt],l.get_vector(8)));
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[tt],(-l).get_vector(8)));
			}

			// get g_{mu,nu}
			std::vector<std::vector<F>> GMUNU;
			get_induced_metric_from_vector_states_D<F,8>(GMUNU,eps,epsc,l);
			// check result
			CHECK(compare_to_metric<F,8>(GMUNU));
		}

		SECTION("8D vector completeness with a given reference vector (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<F,8>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// on-shell currents
			std::vector<std::vector<F>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<F>> epsc;
			// set the polarization vectors
			get_vector_states_D<F,8>(eps,epsc,l,false,mref);
			// check normalizations
			// m
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[0],eps[0]));
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[0],epsc[0]));
			// p
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[1],eps[1]));
			CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[1],epsc[1]));
			for(size_t tt=2;tt<eps.size();++tt)
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[tt],eps[tt])
					*contract_vector_currents_from_0_to<8>(epsc[tt],epsc[tt])
					-F(1));
			// transversity
			for(size_t tt=0;tt<eps.size();++tt){
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(eps[tt],l.get_vector(8)));
				CHECK_ZERO(contract_vector_currents_from_0_to<8>(epsc[tt],(-l).get_vector(8)));
			}

			// get g_{mu,nu}
			std::vector<std::vector<F>> GMUNU;
			get_induced_metric_from_vector_states_D<F,8>(GMUNU,eps,epsc,l);
			// check result
			CHECK(compare_to_metric<F,8>(GMUNU));
		}

		SECTION("8D vector completeness using default reference vector and (light-cone) projective states (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<F,8>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// the default reference choice 
			momD<F,8> local_ref;

			// on-shell currents
			std::vector<std::vector<F>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<F>> epsc;
			// set the polarization vectors (with 'false' the ref momentum passed is disregarded)
			get_projective_vector_states<F,8>(eps,epsc,l,false,local_ref);

			// get g_{mu,nu}
			std::vector<std::vector<F>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<F,8>(GMUNU,eps,epsc,l,local_ref);
			// check result
			CHECK(compare_to_metric<F,8>(GMUNU));
		}

		SECTION("8D vector completeness with a given reference vector and (light-cone) projective states (case "+std::to_string(ii)+"/"+std::to_string(momconf_process.n())+")"){
			const momD<F,8>& l(momconf_process.p(ii));
			// momentum should be on-shell (massless)
			REQUIRE_ZERO(l*l);

			// on-shell currents
			std::vector<std::vector<F>> eps;
			// 'conjugated' polarization vectors
			std::vector<std::vector<F>> epsc;
			// set the polarization vectors
			get_projective_vector_states<F,8>(eps,epsc,l,true,mref);

			// get g_{mu,nu}
			std::vector<std::vector<F>> GMUNU;
			get_induced_metric_from_vector_states_and_reference<F,8>(GMUNU,eps,epsc,l,mref);
			// check result
			CHECK(compare_to_metric<F,8>(GMUNU));
		}
	}
}
#endif
