/**
 * Example for unit testing with Catch 2.
 *
 * For more information take a look at:
 * https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#top
 */
#include "catch.hpp"

#include <sstream>
#include <stdexcept>

#include "Core/Series.h"

// A test case takes as arguments a name and tags. The tags can be used to
// just run a specific TEST_CASE from the command line.
TEST_CASE("Zero Constructor", "[Constructor][ZeroConstr]"){
    Caravel::Series<double> s0(-2, 0);
    REQUIRE(s0.get_term().size() == 3);
}

TEST_CASE("Variadic Template Constructor", "[Constructor][VarTempConstr]"){

    SECTION("Called with valid arguments"){
        Caravel::Series<double> s(-2, 0, 1., 2., 3.);
        REQUIRE(s.get_term().size() == 3);
    }

    SECTION("Called with invalid arguments"){
        REQUIRE_THROWS_AS(Caravel::Series<double>(-2, 0, 2.), std::range_error);
        REQUIRE_THROWS_WITH(Caravel::Series<double>(-2, 0, 2.),
                            "Number of provided coefficients is incompatible with range.");
    }

}

TEST_CASE("Constructor with vector", "[Constructor][Vector]"){

    SECTION("Call with an explicit vector"){
        Caravel::Series<double> s(-2, 0, std::vector<double>({1., 2., 3.}));
        REQUIRE(s.get_term().size() == 3);
    }

    SECTION("Call with an explicit initializer_list"){
        Caravel::Series<double> s(-2, 0, {1., 2., 3.});
        REQUIRE(s.get_term().size() == 3);
    }

}

TEST_CASE("Copy Constructor", "[Constructor][Copy]"){

    Caravel::Series<double> s1(-2, 0, 1., 2., 3.);
    auto s2(s1);
    REQUIRE(s2.leading() == -2);
    REQUIRE(s2.last() == 0);
    REQUIRE(s2[-2] == 1.);
    REQUIRE(s2[-1] == 2.);
    REQUIRE(s2[0] == 3.);

}

TEST_CASE("Move Constructor", "[Constructor][Move]"){

    auto s2(Caravel::Series<double>(-2, 0, 1., 2., 3.));
    REQUIRE(s2.leading() == -2);
    REQUIRE(s2.last() == 0);
    REQUIRE(s2[-2] == 1.);
    REQUIRE(s2[-1] == 2.);
    REQUIRE(s2[0] == 3.);

}

TEST_CASE("Copy assignment"){

}

TEST_CASE("Arithmetics", "[Arithmetic]"){

    Caravel::Series<double> s1(-2, 0, 1., 2., 3.);
    Caravel::Series<double> s2(-2, 0, 2., 4., 6.);
    Caravel::Series<double> s3(-1, 1, 7., 8., 9.);
    Caravel::Series<double> s4(-2, -1, 11., 12.);
    Caravel::Series<double> s5(0, 1, 13., 14.);

    SECTION("Equality") {
        REQUIRE(s1 == Caravel::Series<double>(-2,0,1.,2.,3.));
        REQUIRE(s1 != Caravel::Series<double>(-2,0,1.,2.,4.));
    }

    SECTION("Addition"){

        // Check addition on equal range series
        auto s12 = s1 + s2;
        REQUIRE(s12.get_term() == std::vector<double>({3., 6., 9.}));
        REQUIRE(s12.leading() == -2);
        REQUIRE(s12.last() == 0);

        // Check addition on different but overlapping range series
        auto s23 = s2 + s3;
        REQUIRE(s23.get_term() == std::vector<double>({2., 11., 14.}));
        REQUIRE(s23.leading() == -2);
        REQUIRE(s23.last() == 0);

        // Check addition with non-overlapping range series
        auto s45 = s4 + s5;
        REQUIRE(s45.get_term() == std::vector<double>({11., 12.}));
        REQUIRE(s45.leading() == -2);
        REQUIRE(s45.last() == -1);

        // Check += operator
        auto s_plus = s2;
        s_plus += s3;
        REQUIRE(s_plus.get_term() == std::vector<double>({2., 11., 14}));
        REQUIRE(s_plus.leading() == -2);
        REQUIRE(s_plus.last() == 0);

        // Add a scalar
        auto s_scalar = s1 + 2.;
        REQUIRE(s_scalar[0] == 5.);

        s2 += 3.;
        REQUIRE(s2[0] == 9.);
    }

    SECTION("Subtraction"){

        // Check subtraction on equal range series
        auto s12 = s1 - s2;
        REQUIRE(s12.get_term() == std::vector<double>({-1., -2., -3.}));
        REQUIRE(s12.leading() == -2);
        REQUIRE(s12.last() == 0);

        // Check subtraction on different but overlapping range series
        auto s23 = s2 - s3;
        REQUIRE(s23.get_term() == std::vector<double>({2., -3., -2.}));
        REQUIRE(s23.leading() == -2);
        REQUIRE(s23.last() == 0);

        // Check subtraction with non-overlapping range series
        auto s45 = s4 - s5;
        REQUIRE(s45.get_term() == std::vector<double>({11., 12.}));
        REQUIRE(s45.leading() == -2);
        REQUIRE(s45.last() == -1);

        // Check -= operator
        auto s_plus = s2;
        s_plus -= s3;
        REQUIRE(s_plus.get_term() == std::vector<double>({2., -3., -2}));
        REQUIRE(s_plus.leading() == -2);
        REQUIRE(s_plus.last() == 0);

        // Subtract a scalar
        auto s_scalar = s1 - 4.;
        REQUIRE(s_scalar[0] == -1);

        auto s_scal = 4. - s2;
        REQUIRE(s_scal[-2] == -2.);
        REQUIRE(s_scal[-1] == -4.);
        REQUIRE(s_scal[0] == -2.);

        s2 -= 7.;
        REQUIRE(s2[0] == -1);

    }

    SECTION("Multiplication"){

        // Multiply two Series
        auto s23 = s2 * s3;
        REQUIRE(s23.leading() == -3);
        REQUIRE(s23.last() == -1);
        REQUIRE(s23[-3] == 14.0);
        REQUIRE(s23[-2] == 44.0);
        REQUIRE(s23[-1] == 92.0);
        //REQUIRE(s23[0] == 42);

        // Multiply using *=
        s2 *= s3;
        REQUIRE(s2.leading() == -3);
        REQUIRE(s2.last() == -1);
        REQUIRE(s2[-3] == 14.0);
        REQUIRE(s2[-2] == 44.0);
        REQUIRE(s2[-1] == 92.0);

        // Multiply by a scalar
        auto s = s1 * 2.;
        REQUIRE(s[-2] == 2.);
        REQUIRE(s[-1] == 4.);
        REQUIRE(s[0] == 6.);

        s4 *= 2.;
        REQUIRE(s4[-2] == 22.);
        REQUIRE(s4[-1] == 24.);
    }

    SECTION("Division"){
        auto s = s2 / 2.;
        REQUIRE(s[-2] == 1.);
        REQUIRE(s[-1] == 2.);
        REQUIRE(s[0] == 3.);

        s5 /= 2.;
        REQUIRE(s5[0] == 6.5);
        REQUIRE(s5[1] == 7.0);
    }

    SECTION("Power"){
        auto s = s4^2;
        REQUIRE(s.leading() == -4);
        REQUIRE(s.last() == -3);
        REQUIRE(s[-4] == 121.);
        REQUIRE(s[-3] == 264.);
    }

}

TEST_CASE("Accessors", "[Access]"){

    Caravel::Series<double> s(-2, 0, 1., 2., 3.);

    REQUIRE_THROWS_AS(s[-3], std::range_error);
    REQUIRE(s[-2] == 1.);
    REQUIRE(s[-1] == 2.);
    REQUIRE(s[0] == 3.);

    REQUIRE(s(-3) == 0.);
    REQUIRE(s(-2) == 1.);
    REQUIRE(s(-1) == 2.);
    REQUIRE(s(0) == 3.);

    REQUIRE_THROWS_AS(s[1], std::range_error);
    REQUIRE_THROWS_WITH(s[1], "Request for a coefficient of higher order!");
}

TEST_CASE("Output", "[out]"){
    std::stringstream oss;
    Caravel::Series<double> s(-2, 0, 1., 2., 3.);
    oss << s;
    REQUIRE(oss.str() == "( 1*ep^-2 + 2*ep^-1 + 3*ep^0 + O[ep]^1 )");
}

template <typename... Cs>
Caravel::Series<double> get_Series(int s, int e, double first, Cs... coeffs){
    return Caravel::Series<double>::drop_excess(s, e, first, coeffs...);
}

TEST_CASE("Slicing", "[slice]"){
    auto s = get_Series(-2, 1, 1., 2., 3., 4., 5., 6.);
    CHECK(s.leading() == -2);
    CHECK(s.last() == 1);
    CHECK(s[1] == Approx(4.));
}
