#include "Core/spinor/clifford_algebra.h"
#include "catch.hpp"
#include <tuple>
#include <cmath>
#include <random>
#include "compare_tools.hpp"
#include "Core/typedefs.h"
#include "Core/extended_t.h"

#include "Core/settings.h"

namespace std{

    // some linear algebraic stuff with vectors
    template <typename T> std::vector<T>& operator+=(std::vector<T>& v1, const std::vector<T>& v2){
        std::transform(v1.begin(),v1.end(),v2.begin(),v1.begin(),std::plus<T>());
        return v1;
    }
    template <typename T> std::vector<T>& operator-=(std::vector<T>& v1, const std::vector<T>& v2){
        std::transform(v1.begin(),v1.end(),v2.begin(),v1.begin(),std::minus<T>());
        return v1;
    }
    template <typename T> std::vector<T>& operator*=(std::vector<T>& v1, const T& s){
        using namespace std::placeholders;
        std::transform(v1.begin(), v1.end(), v1.begin(), std::bind(std::multiplies<T>(),s,_1));
        return v1;
    }
    template <typename T> std::vector<T>& operator/=(std::vector<T>& v1, const T& s){
        using namespace std::placeholders;
        std::transform(v1.begin(), v1.end(), v1.begin(), std::bind(std::divides<T>(),_1,s));
        return v1;
    }

}

using namespace Caravel;
using namespace clifford_algebra;
using namespace clifford_algebra::_internal;


using std::complex;
using std::get;
using std::tuple;
using std::make_tuple;
using std::vector;

template <unsigned Ds, typename T> class Gamma_explicit;
template <unsigned Ds, typename T> Gamma_explicit<Ds,T> operator* (const Gamma_explicit<Ds,T>& m1, const Gamma_explicit<Ds,T>& m2);

template <unsigned Ds, typename T> class Gamma_explicit : public Gamma<Ds>{
    public:
        using Gamma<Ds>::Dtt;

        Gamma_explicit(const unsigned i);
        Gamma_explicit() : Gamma<Ds>() {};
        Gamma_explicit(Identity* id);

        //! 0 based
        const T& operator()(unsigned i, unsigned j) const{ return elements[Dtt*i+j]; }

        Gamma_explicit<Ds,T>& operator+=(const Gamma_explicit<Ds,T>& m);
        Gamma_explicit<Ds,T>& operator-=(const Gamma_explicit<Ds,T>& m);

    private:
        std::array<T,Dtt*Dtt> elements{};
        T& el(unsigned i, unsigned j){ return elements[Dtt*i+j]; } 
        friend Gamma_explicit<Ds,T> operator* <>(const Gamma_explicit<Ds,T>& m1, const Gamma_explicit<Ds,T>& m2);
};


template <unsigned Ds, typename T> Gamma_explicit<Ds,T>::Gamma_explicit(const unsigned i) : Gamma<Ds>(i) {
    for (unsigned i=0; i<Dtt; i++){
        for (unsigned j=0; j<Dtt; j++){
            el(i,j) = explicit_sEntry<T>(Gamma<Ds>::entry(i+1,j+1));
        }
    }
}

template <unsigned Ds, typename T> Gamma_explicit<Ds,T>::Gamma_explicit(Identity* id) : Gamma<Ds>(id) {
    for (unsigned i=0; i<Dtt; i++){
        for (unsigned j=0; j<Dtt; j++){
            el(i,j) = explicit_sEntry<T>(Gamma<Ds>::entry(i+1,j+1));
        }
    }
}

template <unsigned Ds, typename T> Gamma_explicit<Ds,T>& Gamma_explicit<Ds,T>::operator+=(const Gamma_explicit<Ds,T>& m){
    for (unsigned i=0; i<Dtt; i++){
        for (unsigned j=0; j<Dtt; j++){
            el(i,j) += m(i,j);
        }
    }
    return *this;
}
template <unsigned Ds, typename T> Gamma_explicit<Ds,T>& Gamma_explicit<Ds,T>::operator-=(const Gamma_explicit<Ds,T>& m){
    for (unsigned i=0; i<Dtt; i++){
        for (unsigned j=0; j<Dtt; j++){
            el(i,j) -= m(i,j);
        }
    }
    return *this;
}

template <unsigned Ds, typename T> Gamma_explicit<Ds,T> operator* (const Gamma_explicit<Ds,T>& m1, const Gamma_explicit<Ds,T>& m2){
    Gamma_explicit<Ds,T> res;

    for(unsigned i=0;i<res.Dtt;i++){
        for(unsigned j=0;j<res.Dtt;j++){
            for(unsigned k=0;k<res.Dtt;k++){
                res.el(i,j) += m1(i,k)*m2(k,j);
            }
        }
    }

    return res;
}

template <unsigned Ds, typename T> Gamma_explicit<Ds,T> operator+ (Gamma_explicit<Ds,T> m1, const Gamma_explicit<Ds,T>& m2){ return m1+=m2; }
template <unsigned Ds, typename T> Gamma_explicit<Ds,T> operator- (Gamma_explicit<Ds,T> m1, const Gamma_explicit<Ds,T>& m2){ return m1-=m2; }



namespace std{
    template <unsigned Ds, typename T> std::ostream& operator<<(std::ostream& s, const Gamma_explicit<Ds,T>& g){
        constexpr unsigned limit = g.Dtt;
        for(unsigned ii=0;ii<limit;ii++){
            //s<<"\t";
            for(unsigned jj=0;jj<limit;jj++) s<< g(ii,jj)<<" ";
            s<<"\n";
        }
        return s;
    }
}

/**
 * Function template to extract entries in a product of two Gamma<D>'s.
 * This function is only as a testing tool!
 *
 * @param unsigned representing the row of the entry requested
 * @param unsigned representing the column of the entry requested
 * @param Gamma<D> matrix in the left of the product
 * @param Gamma<D> matrix in the right of the product
 */
template <typename T,unsigned D> T gamma_gamma_multiply(unsigned k,unsigned l,const Gamma<D>& A,const Gamma<D>& B){
    T toret(0,0);
    const unsigned& limit = Gamma<D>::Dtt;
    for(unsigned m=0;m<limit;m++){
        sEntry local(sEntry_multiply(A.entry(k+1,m+1),B.entry(m+1,l+1)));
        if(local==sEntry::One)
            toret+=T(1);
        else if(local==sEntry::mOne)
            toret-=T(1);
        else if(local==sEntry::I)
            toret=T(0,1);
        else if(local==sEntry::mI)
            toret-=T(0,1);
        //std::cout<<"toret: "<<toret<<" k,l: "<<k<<","<<l<<" local sEntry: "<<local<<std::endl;
    }
    return toret;
}


template <typename T, unsigned D> bool is_zero(const Gamma_explicit<D,T>& g1){
    for(unsigned i=0;i<g1.Dtt;i++){
        for(unsigned j=0;j<g1.Dtt;j++){
            if (g1(i,j)!=T(0)) return false;
        }
    }
    return true;
}

template <typename T, unsigned D> Gamma_explicit<D,T> anti_commute(const Gamma_explicit<D,T>& g1, const Gamma_explicit<D,T>& g2){
    return (g1*g2)+=(g2*g1);
}


template <unsigned D> std::vector<std::tuple<unsigned,unsigned>> get_partitions(unsigned first=0, unsigned last=D-1){
    assert(last>first);
    assert(last-first+1<=D);

    std::vector<std::tuple<unsigned,unsigned>> ret;

    for(unsigned i=first;i<=last-1;i++){
        for(unsigned j=i+1;j<=last;j++){
            ret.emplace_back(i,j);
        }
    }

    return ret;
}


// This test fails to compile on mac, so we remove it. It uses complex
// exact spinors, which seems esoteric.
#ifndef __APPLE__
TEST_CASE("Dtt Gamma matrices in Ds dimensions","[GammaDs]"){

    typedef complex<F32> T;

    SECTION("Algebra properties in Ds=6"){
        constexpr unsigned Ds = 6;
        Gamma_explicit<Ds,T> id((Identity*)nullptr);

        vector<Gamma_explicit<Ds,T>> gs;
        Gamma_explicit<Ds,T> gstar(Ds);

        for(unsigned i=4;i<Ds;i++){
            gs.emplace_back(i);
        }

        REQUIRE(is_zero((gstar*gstar)-=id));
        for(auto& it:gs){
            REQUIRE(is_zero((it*it)+=id));
            REQUIRE(is_zero(anti_commute(it,gstar)));
        }

        auto part = get_partitions<Ds-4>();

        for(auto& it: part){
            REQUIRE(is_zero(anti_commute(gs.at(get<0>(it)),gs.at(get<1>(it)))));
        }
    }

    SECTION("Algebra properties in Ds=8"){
        constexpr unsigned Ds = 8;
        Gamma_explicit<Ds,T> id((Identity*)nullptr);

        vector<Gamma_explicit<Ds,T>> gs;
        Gamma_explicit<Ds,T> gstar(Ds);

        for(unsigned i=4;i<Ds;i++){
            gs.emplace_back(i);
        }

        REQUIRE(is_zero((gstar*gstar)-=id));
        for(auto& it:gs){
            REQUIRE(is_zero((it*it)+=id));
            REQUIRE(is_zero(anti_commute(it,gstar)));
        }

        auto part = get_partitions<Ds-4>();

        for(auto& it: part){
            REQUIRE(is_zero(anti_commute(gs.at(get<0>(it)),gs.at(get<1>(it)))));
        }
    }

    SECTION("Algebra properties in Ds=10"){
        constexpr unsigned Ds = 10;
        Gamma_explicit<Ds,T> id((Identity*)nullptr);

        vector<Gamma_explicit<Ds,T>> gs;
        Gamma_explicit<Ds,T> gstar(Ds);

        for(unsigned i=4;i<Ds;i++){
            gs.emplace_back(i);
        }

        REQUIRE(is_zero((gstar*gstar)-=id));
        for(auto& it:gs){
            REQUIRE(is_zero((it*it)+=id));
            REQUIRE(is_zero(anti_commute(it,gstar)));
        }

        auto part = get_partitions<Ds-4>();

        for(auto& it: part){
            REQUIRE(is_zero(anti_commute(gs.at(get<0>(it)),gs.at(get<1>(it)))));
        }
    }


}
#endif


TEST_CASE("Sparse slashed matrix operations with real types","[Sparse smatrix real]"){
    typedef F32 T;
    typedef extended_t<T> Tc;

    sparse_smatrix<6,T,true>::init();
    sparse_smatrix<6,T,false>::init();
    sparse_smatrix<8,T,true>::init();
    sparse_smatrix<8,T,false>::init();
    sparse_smatrix<10,T,false>::init();
    sparse_smatrix<10,T,true>::init();



    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 engine(340); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dist(-5, 5);
    auto gen1 = std::bind(dist, engine);

    auto gen = [&](){return T(gen1());};

    constexpr unsigned Ds = 6;
    constexpr unsigned Dt = two_to_the(Ds/2-1);

    std::vector<T> in(Dt);
    std::vector<T> zero(Dt,T(0));
    std::generate(in.begin(),in.end(),gen);

    std::vector<T> momDsm4_components(Ds-4);
    std::generate(momDsm4_components.begin(),momDsm4_components.end(),gen);
    momDsm4_components.insert(momDsm4_components.begin(),4,T(0));
    vector<T> momD_components(Ds);
    vector<T> momD_components2(Ds);
    std::generate(momD_components.begin(),momD_components.end(),gen);
    std::generate(momD_components2.begin(),momD_components2.end(),gen);

    momentumD<T,Ds> l4d(gen(),gen(),gen(),gen());
    momentumD<T,Ds> lDsm4(momDsm4_components);
    momentumD<T,Ds> lDs(momD_components);
    momentumD<T,Ds> lDs2(momD_components2);

    SECTION("smatrix(l)^2 = l^2"){

        EpsilonBasis<T>::squares[0]=T(3);
        EpsilonBasis<T>::squares[1]=T(-7);
        vector<Tc> in{Tc(1),Tc(2),Tc(3),Tc(4)};

        {
            vector<Tc> out(Dt,Tc(0));
            vector<Tc> out2(Dt,Tc(0));
            vector<Tc> lDs_in = to_extended_t<vector<Tc>,Ds>(lDs);

            multiply_sparse_smatrix_Weyl_impl<mult_from::L,repr::Lat,Ds,Tc>(lDs_in,in.begin(),out.begin());
            multiply_sparse_smatrix_Weyl_impl<mult_from::L,repr::La,Ds,Tc>(lDs_in,out.begin(),out2.begin());
            //multiply_sparse_smatrix_Weyl_impl<mult_from::L,repr::Lat,Ds>(lDs_in,in.begin(),out.begin());
            //multiply_sparse_smatrix_Weyl_impl<mult_from::L,repr::La,Ds>(lDs_in,out.begin(),out2.begin());

            for(auto& it: out2){ it/=(lDs*lDs); }

            CHECK(out2==in);
        }
        {
            vector<Tc> out(Dt,Tc(0));
            vector<Tc> out2(Dt,Tc(0));
            vector<Tc> lDs_in = to_extended_t<vector<Tc>,Ds>(lDs);

            multiply_sparse_smatrix_Weyl_impl<mult_from::R,repr::La,Ds,Tc>(lDs_in,in.begin(),out.begin());
            multiply_sparse_smatrix_Weyl_impl<mult_from::R,repr::Lat,Ds,Tc>(lDs_in,out.begin(),out2.begin());
            //multiply_sparse_smatrix_Weyl_impl<mult_from::R,repr::Lat,Ds>(lDs_in,in.begin(),out.begin());
            //multiply_sparse_smatrix_Weyl_impl<mult_from::R,repr::La,Ds>(lDs_in,out.begin(),out2.begin());

            for(auto& it: out2){ it/=(lDs*lDs); }

            REQUIRE(out2==in);
        }


    }

    SECTION("sm(l1)*sm(l2) + sm(l2)*sm(l1) = 2(l2*l1)"){
        EpsilonBasis<T>::squares[0]=T(123);
        EpsilonBasis<T>::squares[1]=T(-10);
        vector<Tc> in{Tc(1),Tc(2),Tc(3),Tc(4)};


        {
            vector<Tc> out(Dt,Tc(0));
            vector<Tc> out2(Dt,Tc(0));
            vector<Tc> out21(Dt,Tc(0));
            vector<Tc> out22(Dt,Tc(0));
            vector<Tc> lDs_in1 = to_extended_t<vector<Tc>,Ds>(lDs);
            vector<Tc> lDs_in2 = to_extended_t<vector<Tc>,Ds>(lDs2);

            multiply_sparse_smatrix_Weyl_impl<mult_from::L,repr::Lat,Ds,Tc>(lDs_in1,in.begin(),out.begin());
            multiply_sparse_smatrix_Weyl_impl<mult_from::L,repr::La,Ds,Tc>(lDs_in2,out.begin(),out2.begin());

            multiply_sparse_smatrix_Weyl_impl<mult_from::L,repr::Lat,Ds,Tc>(lDs_in2,in.begin(),out21.begin());
            multiply_sparse_smatrix_Weyl_impl<mult_from::L,repr::La,Ds,Tc>(lDs_in1,out21.begin(),out22.begin());

            out2 += out22;

            for(auto& it: out2){ it/=(T(2)*(lDs*lDs2)); }

            CHECK(out2==in);
        }
        {
            vector<Tc> out(Dt,Tc(0));
            vector<Tc> out2(Dt,Tc(0));
            vector<Tc> out21(Dt,Tc(0));
            vector<Tc> out22(Dt,Tc(0));
            vector<Tc> lDs_in1 = to_extended_t<vector<Tc>,Ds>(lDs);
            vector<Tc> lDs_in2 = to_extended_t<vector<Tc>,Ds>(lDs2);

            multiply_sparse_smatrix_Weyl_impl<mult_from::R,repr::Lat,Ds,Tc>(lDs_in1,in.begin(),out.begin());
            multiply_sparse_smatrix_Weyl_impl<mult_from::R,repr::La,Ds,Tc>(lDs_in2,out.begin(),out2.begin());

            multiply_sparse_smatrix_Weyl_impl<mult_from::R,repr::Lat,Ds,Tc>(lDs_in2,in.begin(),out21.begin());
            multiply_sparse_smatrix_Weyl_impl<mult_from::R,repr::La,Ds,Tc>(lDs_in1,out21.begin(),out22.begin());

            out2 += out22;

            for(auto& it: out2){ it/=(T(2)*(lDs*lDs2)); }

            REQUIRE(out2==in);
        }


    }

}


TEST_CASE("States on the cut (real finite fields)","[States real]"){
    typedef F32 T;
    typedef extended_t<T> Tc;

    constexpr unsigned Ds = 8;
    constexpr unsigned Dt = two_to_the(Ds/2-1);

    EpsilonBasis<T>::squares[0]=T(4);
    EpsilonBasis<T>::squares[1]=T(3);
    Caravel::momentumD<T,Ds> lDs(T(3),T(5),T(0),T(0),T(2),T(1),T(2),T(1));
    Caravel::momentumD<T,Ds> lDs2(T(3),T(-3),T(-10),T(10),T(0),T(-2),T(-4),T(2));

    Caravel::momentumD<T,Ds> l4d(T(1),T(-1),T(3),T(3),T(0),T(0));

    REQUIRE_ZERO(lDs*lDs);
    REQUIRE_ZERO(lDs2*lDs2);

    SECTION("Light cone decomposition"){
        auto lfl = lightcone_decompose(lDs);
        REQUIRE_ZERO(lfl*lfl);

        lfl = lightcone_decompose(l4d);
        REQUIRE_ZERO((Caravel::momentumD<T,Ds>(lfl)-l4d));
    }


    SECTION("Dirac equation"){
        vector<Tc> lDs_in1 = to_extended_t<vector<Tc>,Ds>(lDs);
        vector<Tc> lDs_in2 = to_extended_t<vector<Tc>,Ds>(lDs2);

#define ______check(N)\
        {\
            auto q = cut_WF_q<repr::La,N,Tc>(lDs);\
            auto qb = cut_WF_qb<repr::Lat,N,Tc>(lDs);\
            CHECK_ZERO((multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,Ds>(lDs_in1,q) ));\
            CHECK_ZERO((multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,Ds>(lDs_in1,qb) ));\
        }\

        ______check(1);
        ______check(2);
        ______check(3);
        ______check(4);
#undef ______check
    }

    SECTION("Completeness relation"){
        vector<Tc> in{T(4),T(-2),T(1),T(0),T(5),T(0),T(1),T(-3)};
        //vector<T> in{T(1),T(2),T(0),T(0),T(0),T(0),T(0),T(0),};

        vector<vector<Tc>> qs;
        vector<vector<Tc>> qbs;

        vector<Tc> lDs_in1 = to_extended_t<vector<Tc>,Ds>(lDs);
        vector<Tc> lDs_in2 = to_extended_t<vector<Tc>,Ds>(lDs2);

#define __do(N)\
        qs.push_back(cut_WF_q<repr::La,N,Tc>(lDs));\
        qbs.push_back(cut_WF_qb<repr::Lat,N,Tc>(-lDs));\

        __do(1);
        __do(2);
        __do(3);
        __do(4);
#undef __do

        {
            auto out1 = multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,Ds>(lDs_in1,in);

            vector<Tc> out2(Dt,Tc(0));

            for(unsigned i=0; i<Dt/2; i++){
                Tc c(0);
                unroll_sp_prod<repr::La,Dt>(qs.at(i),in,c,int_<Dt/2>{});
                auto v = qbs.at(i);
                for(auto& it:v) it = _mult(it,c);
                out2+=v;
            }

            CHECK_ZERO(out1-=out2);
        }
        {
            auto out1 = multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,Ds>(lDs_in1,in);

            vector<Tc> out2(Dt,T(0));

            for(unsigned i=0; i<Dt/2; i++){
                Tc c(0);
                unroll_sp_prod<repr::Lat,Dt>(in,qbs.at(i),c,int_<Dt/2>{});
                auto v = qs.at(i);
                for(auto& it:v) it = _mult(it,c);
                out2+=v;
            }

            CHECK_ZERO(out1-=out2);
        }
    }

    SECTION("Vector from spinors"){
        {

            auto q = cut_WF_q<repr::La,2,Tc>(lDs);
            auto qb = cut_WF_qb<repr::Lat,2,Tc>(-lDs);

            auto v = LvAB_Ds<Ds>(q,qb);

            momentumD<Tc,Ds> m(v);

            decltype(m) lDs_ext {to_extended_t<std::array<Tc,Ds>,Ds>(lDs.get_components())};

            REQUIRE_ZERO(m*m);
            REQUIRE_ZERO(m*lDs_ext);
            REQUIRE_ZERO(lDs_ext-m);
        }
        {
            auto q = cut_WF_q<repr::La,3,Tc>(lDs);
            auto qb = cut_WF_qb<repr::Lat,3,Tc>(lDs2);

            auto v = LvAB_Ds<Ds>(q,qb);

            momentumD<Tc,Ds> m(v);

            decltype(m) lDs_ext {to_extended_t<std::array<Tc,Ds>,Ds>(lDs.get_components())};
            decltype(m) lDs2_ext {to_extended_t<std::array<Tc,Ds>,Ds>(lDs2.get_components())};

            CHECK_ZERO(m*lDs_ext);
            CHECK_ZERO(m*lDs2_ext);
        }
    }

}

