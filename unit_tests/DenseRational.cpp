#include "catch.hpp"

#include "Core/typedefs.h"
#include "FunctionalReconstruction/DenseRational.h"
using namespace Caravel;

TEST_CASE("Dense Rational Taylor Expansion", "[DenseRational][Taylor]"){
    using F = F32;
    F::set_p( 2147483579 );

    SECTION("Case 1"){
      DensePolynomial<F> numerator({F(1), F(3), F(2), F(0), F(77)});
      DensePolynomial<F> denominator({F(0), F(1), F(3), F(4), F(9), F(0), F(-100)});

      DenseRational<F> rational(numerator, denominator);

      auto taylor_expansion = rational.taylor_expand_around_zero(8);

      REQUIRE(taylor_expansion[-1] == F(1));
      REQUIRE(taylor_expansion[0]  == F(0));
      REQUIRE(taylor_expansion[1]  == F(-2));
      REQUIRE(taylor_expansion[2]  == F(-3));
      REQUIRE(taylor_expansion[3]  == F(94));
    }

    SECTION("Case 2"){
      using F = BigRat;
      DensePolynomial<F> numerator({0,0,BigRat(5,2),BigRat(-17,18),0,0,0,BigRat(125,156),BigRat(-12,13)});
      DensePolynomial<F> denominator({1,0,0,BigRat(3,16),BigRat(17,19),0,-100});

      DenseRational<F> rational(numerator, denominator);

      auto taylor_expansion = rational.taylor_expand_around_zero(13);

      Series<F> target(0, 13,
                       {0,0,BigRat(5, 2), BigRat(-17, 18), 0, BigRat(-15, 32), BigRat(-3757, 1824), BigRat(14639, 8892), BigRat(1658441, 6656),
                        BigRat(int64_t(-8198267), int64_t(87552)), BigRat(int64_t(1382453), 901056), BigRat(int64_t(-2530269527), 26615808), BigRat(int64_t(-2497039985), 6070272),
                        BigRat(int64_t(10731601537), 43250688)});

      _PRINT(target.leading());
      _PRINT(target.last());

      _PRINT(taylor_expansion.leading());
      _PRINT(taylor_expansion.last());

      REQUIRE(taylor_expansion == target);
    }


    SECTION("Case 3"){
      using F = BigRat;
      DensePolynomial<F> numerator({BigRat(5,2),BigRat(-17,18),0,0,0,BigRat(125,156),BigRat(-12,13)});
      DensePolynomial<F> denominator({0,0,0,0,0,0,0,0,0,0,1});

      DenseRational<F> rational(numerator, denominator);

      _PRINT(rational);

      auto taylor_expansion = rational.taylor_expand_around_zero(0);

      Series<F> target(-10, 0, {BigRat(5, 2), BigRat(-17, 18), 0, 0, 0, BigRat(125, 156), BigRat(-12, 13), 0, 0,0,0});

      REQUIRE(taylor_expansion == target);
    }

    SECTION("Case 4"){
      using F = BigRat;
      DensePolynomial<F> numerator({BigRat(5,2),BigRat(-17,18),0,0,0,BigRat(125,156),BigRat(-12,13)});
      DensePolynomial<F> denominator({0,0,0,0,0,0,BigRat(12,5),BigRat(16,8),BigRat(7,23)});

      DenseRational<F> rational(numerator, denominator);

      _PRINT(rational);

      auto taylor_expansion = rational.taylor_expand_around_zero(5);

      Series<F> target(-6, 5, {BigRat(25,24), BigRat(-545,432), BigRat(3425,3726), BigRat(-9425,15552), BigRat(19175125,49362048), BigRat(167492375,1925119872), BigRat(int64_t(-269056170595),int64_t(531333084672)), BigRat(int64_t(1310107454225),int64_t(3187998508032)), BigRat(int64_t(-1177033262725),int64_t(4230228789504)), BigRat(int64_t(474496013923375),int64_t(2639662764650496)), BigRat(int64_t(-41713838372231125),int64_t(364273461521768448)), BigRat(int64_t(79373555199600625),int64_t(1092820384565305344))});

      REQUIRE(taylor_expansion == target);
    }

    SECTION("Generic"){
      using F = BigRat;
      DensePolynomial<F> numerator({-8,-9,10,-2,-3});
      DensePolynomial<F> denominator({-2,1,-2,-1,-7,-5,-9,-8,1,2,4,-1,5,-5,5,1,2,-2,9,-9,3,0,-7,-8,3,-9,4,1,7,-5,-8});

      DenseRational<F> rational(numerator, denominator);

      auto taylor_expansion = rational.taylor_expand_around_zero(10);

      Series<F> target(0, 10,
                       {4, BigRat(13, 2), BigRat(-23, 4), BigRat(-83, 8), BigRat(-243, 16), BigRat(-867, 32), BigRat(-467, 64), BigRat(4669, 128),
                        BigRat(30733, 256), BigRat(121309, 512), BigRat(199597, 1024)});

      REQUIRE(taylor_expansion == target);
    }

    SECTION("yyyCoeff37"){
        F::set_p(2147483587);

	DensePolynomial<F> numerator({F(585677391), F(405762757), F(252993727), F(2130253888), F(1860291428), F(1207471570)});
	DensePolynomial<F> denominator({F(0), F(0), F(0), F(0), F(2), F(2147483577), F(16), F(2147483579)});

	DenseRational<F> rational(numerator, denominator);

	auto taylor_expansion = rational.taylor_expand_around_zero(4);

	REQUIRE(taylor_expansion[-4] == F(1366580489));
	REQUIRE(taylor_expansion[-3]  == F(1667074856));
	REQUIRE(taylor_expansion[-2]  == F(750452612));
	REQUIRE(taylor_expansion[-1]  == F(1242080286));
	REQUIRE(taylor_expansion[0]  == F(1362774911));
    }


}
