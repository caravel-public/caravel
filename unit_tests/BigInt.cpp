#include "catch.hpp"

#include <sstream>
#include <iostream>
#include <random>

#include "Core/BigInt.h"

TEST_CASE("Ring operations", "[BigIntArithmetic][FieldOperations]"){

  Caravel::BigInt a, b, c;

  a = 1234;
  b = -5678;

  //c = a+b;
  c = -4444;

  REQUIRE(a+b == c);
  REQUIRE(int64_t(a+b) == -4444);
}
