#include <cstdlib>

#include "Graph/Graph.h"
#include "Graph/GraphKin.h"
#include "catch.hpp"
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/typedefs.h"
#include "AmpEng/hierarchy_tools.h"
#include "AmpEng/cutF.h"
#include "OnShellStrategies/OnShellStrategies.h"
#include "OnShellStrategies/LinearAlgebra.h"
#include "PhaseSpace/PhaseSpace.h"
using namespace Caravel;
using namespace lGraph;
using namespace lGraphKin;

typedef RHP T;
typedef CHP F;

bool is_my_zero( F x ){
 const T eps = T(3e-20);
 if( std::abs( x ) > eps ) return false;
 else return true;
}

template <typename T, size_t D>
T evaluate_propagator(const lGraph::MomentumRouting& mom_rout, const momD_conf<T, D>& external, const OnShellPoint<T, D>& l_moms, const T& mass) {

    std::vector<size_t> ext_in_plus(mom_rout.get_external().plus_momenta.begin(), mom_rout.get_external().plus_momenta.end());
    std::vector<size_t> ext_in_minus(mom_rout.get_external().minus_momenta.begin(), mom_rout.get_external().minus_momenta.end());
    std::vector<size_t> int_in_plus(mom_rout.get_internal().plus_momenta.begin(), mom_rout.get_internal().plus_momenta.end());
    std::vector<size_t> int_in_minus(mom_rout.get_internal().minus_momenta.begin(), mom_rout.get_internal().minus_momenta.end());

    momD<T, D> mom_flow;

    mom_flow += external.Sum(ext_in_plus) - external.Sum(ext_in_minus);
    for (auto& i : int_in_plus) { mom_flow += l_moms[i-1]; }
    for (auto& i : int_in_minus) { mom_flow -= l_moms[i-1]; }

    return mom_flow * mom_flow - mass*mass;
}

template <typename T, size_t D> void ckeck_on_shell_strand(const lGraph::Strand& strand, const momD_conf<T, D>& mom_conf, const OnShellPoint<T, D>& l_moms) {
    auto routings = strand.get_routings();
    auto midx = strand.get_link_mass_indices();

    for( unsigned n = 0; n < routings.size(); n++){
      auto r = routings[n];
      auto m = T(0);
      if( midx[n] != 0 ) m = ParticleMass::particle_mass_container[midx[n]-1].get_mass(T(1));
        // check that all links are on-shell
        bool onshell = is_my_zero( evaluate_propagator(r, mom_conf, l_moms, m) );
        if(!onshell){
            std::cout<<"FAILED on-shell condition for propagator: ("<<r<<")^2 - m["<<midx[n]<<"]^2 = " << evaluate_propagator(r,mom_conf,l_moms,m) <<std::endl;
        }
        CHECK(onshell);
    }
}

template <typename T, size_t D>
void ckeck_on_shell_parametrization(const lGraph::xGraph& gr, const momD_conf<T, D>& mom_conf, const OnShellPoint<T, D>& l_moms) {
//    _PRINT(gr);
    for (const auto& it : gr.get_base_graph().get_connection_map()) {
        const auto& con = it.second;
        for (const auto& stnd : con) { ckeck_on_shell_strand(stnd, mom_conf, l_moms); }
    }
}



TEST_CASE("Check of the on-shell parametrization for massive topologies") {

    // the external particles
    Particle t("t", ParticleType("t"), SingleState("Qp"), 1, 2);
    Particle g1("g1", ParticleType("gluon"), SingleState("p"), 2, 0);
    Particle g2("g2", ParticleType("gluon"), SingleState("p"), 3, 0);
    Particle g3("g3", ParticleType("gluon"), SingleState("p"), 4, 0);
    Particle g4("g4", ParticleType("gluon"), SingleState("p"), 5, 0);
    Particle tb("tb", ParticleType("tb"), SingleState("Qbm"), 6, 3);

    // Get a Phase space point
    unsigned N = 6;
    std::vector<T> masses = { t.get_mass(T(1)), g1.get_mass(T(1)), g2.get_mass(T(1)), g3.get_mass(T(1)), g4.get_mass(T(1)), tb.get_mass(T(1)) };
    T energy = T(1000);
    PhaseSpace<T> Tph(N,energy,masses);
    Tph.set_reset_seed(false);
    srand(1010);
    Tph.generate();
    auto moms = Tph.mom();
    auto mc = to_momDconf<T,5>(moms);


    // Assigning internal indicies
   // u.set_internal_index(1);
   // ub.set_internal_index(1);

    // the loop particless
    Particle gL("gL",ParticleType("gluon"),SingleState("default"));
    Particle tbL("tbL",ParticleType("tb"),SingleState("default"));

    std::vector<std::vector<Particle>> External{{g1}, {g2}, {g3}, {g4}, {tb}, {t}};
    std::vector<std::vector<std::vector<Particle>>> vExternal { External  } ;
    std::vector<Particle> Internal = {gL, gL, gL, gL, gL, tbL};

    using couplings_t = std::map<std::string, int>;
    couplings_t s_coup; s_coup["gs"]=1;
    std::vector<couplings_t> oneloop_couplings = { s_coup, s_coup, s_coup, s_coup, s_coup, s_coup };

    // out of an (array) of external/internal particles (and couplings) get the 'parent' diagram(s)
    std::vector<AmpEng::cutF> theParents = AmpEng::get_1loop_parents( vExternal, {Internal}, {oneloop_couplings} );

    // now get all maximal cuts
    std::vector<AmpEng::cutF> toconst_hier( AmpEng::CutHierarchy::get_maximal_cuts(std::vector<AmpEng::cutF>(theParents)) );
  
    // produce hierarchy
    AmpEng::CutHierarchy hierarchy(toconst_hier);
    std::vector<std::vector<AmpEng::cutF>> topos = hierarchy.get_hierarchy();

    //xGraph Pentagon = toconst_hier[3].get_graph(); 
    //std::cout << topos[1] << std::endl;
    for( unsigned ii = 0; ii < topos.size(); ii++){
      for( unsigned jj = 0; jj < topos[ii].size(); jj++){
        xGraph Topology = topos[ii][jj].get_graph();
        GraphKin<F,5> grKin(Topology,mc);
        OnShellMomentumParameterization<F,5> compute_loop_momenta = surd_massive_parameterization( Topology, grKin );
        std::vector<std::vector<F>> os_variables;
        auto transverse_dimensions = Topology.n_OS_variables_with_special_routing();
       for( auto count : transverse_dimensions){
         os_variables.push_back( std::vector<F>() );
         for(size_t ii = 0; ii <count; ii++)
   	  os_variables.back().push_back( F(rand() % 173 ) );
       }
       auto momenta = compute_loop_momenta( os_variables ); 
       ckeck_on_shell_parametrization( Topology, mc, std::vector<momD<F,5>>( momenta ) );
      }
    }

}
