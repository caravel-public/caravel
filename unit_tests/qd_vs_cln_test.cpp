#include "catch.hpp"

#include <iostream>
#include <cmath>
using std::cout;
using std::endl;

#include "Core/typedefs.h"
#include "IntegralLibrary/CLN_QD_converter.h"

using namespace Caravel;
using namespace Caravel::Integrals;

TEST_CASE("cln to qd real", "[real]"){
    // Lets create a real cln number:
    cln::cl_R clf = "0.271828182845904523536028747135266249775724709369996e+0_40";

    SECTION("Conversion to double"){
        R qdf = cln_to_qd<R>(clf);
        CHECK(fabs(qdf - 0.271828182845904542) < 1e-16 );
    }

#ifdef HIGH_PRECISION
    SECTION("Conversion to double double"){
        auto qdhp = cln_to_qd<RHP>(clf);
        RHP absdiff = qdhp - RHP("0.27182818284590452353602874713527");
        if (absdiff < RHP(0.0))
            absdiff = RHP(-1.) * absdiff;
        CHECK( absdiff < RHP("1e-32") );
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("Conversion to quadruple double"){
        auto qdvhp = cln_to_qd<RVHP>(clf);
        RVHP absdiff = qdvhp - RVHP("0.271828182845904523536028747135266249775724709369996");
        if (absdiff < RVHP(0.0))
            absdiff = RVHP(-1.) * absdiff;
        CHECK( absdiff < RVHP("1e-64") );
    }
#endif
}

TEST_CASE("cln to qd complex", "[complex]"){
    // Let's create a complex cln number.
    cln ::cl_N clc = cln::complex("0.271828182845904523536028747135266249775724709369996e+0_40",
                                  "3.271828182845904523536028747135266249775724709369996e+0_40");

    SECTION("Conversion to double"){
        auto qdc = cln_to_qd<C>(clc);
        C comp(0.271828182845904523, 3.27182818284590452);
        CHECK(abs(qdc-comp) < R(1e-16));
    }

#ifdef HIGH_PRECISION
    SECTION("Conversion to CHP"){
        auto qdchp = cln_to_qd<CHP>(clc);
        CHP comp("0.271828182845904523536028747135266", "3.271828182845904523536028747135266");
        CHECK(abs(qdchp - comp) < RHP(5e-31));
        CHECK(abs(comp.imag() - qdchp.imag()) < RHP(5e-31));
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("Conversion to CVHP"){
        auto qdcvhp = cln_to_qd<CVHP>(clc);
        CVHP comp("0.271828182845904523536028747135266249775724709369996",
                  "3.271828182845904523536028747135266249775724709369996");
        CHECK(abs(qdcvhp - comp) < RVHP(1e-64));
        CHECK(abs(comp.imag() - qdcvhp.imag()) < RVHP(1e-64));
    }
#endif
}

TEST_CASE("qd to cln real", "[real]"){
    // Let's create a real qd number.
    R qdr(0.271828182845904524);
    cln::cl_R clr = qd_to_cln<cln::cl_R>(qdr);
    cln::cl_R comp("0.271828182845904524_18");
    CHECK(abs(clr-comp) < cln::cl_R("1e-16"));

#if defined(HIGH_PRECISION) && defined(VERY_HIGH_PRECISION)
    SECTION("non double"){
        RHP qd("0.271828182845904523536028747135266249775724709369996");
        RVHP qdvhp("0.271828182845904523536028747135266249775724709369996");

        SECTION("RHP"){
            cln::cl_R clrhp = qd_to_cln<cln::cl_R>(qd);
            cln::cl_R comp("0.27182818284590452353602874713527");
            CHECK(abs(clrhp-comp) < cln::cl_R(7e-31));
        }

        SECTION("RVHP"){
            cln::cl_R clrvhp = qd_to_cln<cln::cl_R>(qdvhp);
            cln::cl_R comp("0.271828182845904523536028747135266249775724709369996000000000000_64");
            CHECK(abs(clrvhp-comp) < cln::cl_R(1e-64));
        }
    }
#endif

}

TEST_CASE("qd to cln complex", "[complex]"){

    SECTION("double"){
        C qdc(0.271828182845904524, 3.27182818284590452);
        cln::cl_N clc = qd_to_cln<cln::cl_N>(qdc);
        cln::cl_N comp = cln::complex("0.271828182845904524", "3.27182818284590452");
        CHECK(abs(comp-clc) < cln::cl_R("5e-15"));
    }

#ifdef HIGH_PRECISION
    SECTION("RHP"){
        CHP qdchp("0.271828182845904523536028747135266", "3.271828182845904523536028747135266");
        cln::cl_N clc = qd_to_cln<cln::cl_N>(qdchp);
        cln::cl_N comp = cln::complex("0.271828182845904523536028747135266", "3.271828182845904523536028747135266");
        CHECK(abs(comp-clc) < cln::cl_R("5e-31"));
        CHECK(abs(cln::imagpart(comp)-cln::imagpart(clc)) < cln::cl_R("5e-31"));
    }
#endif

#ifdef VERY_HIGH_PRECISION
    SECTION("RVHP"){
        // We need this because we get so close to zero that it cannot be represented internally.
        //cln::cl_inhibit_floating_point_underflow = true;
        CVHP qdcvhp("0.271828182845904523536028747135266249775724709369997000000000000", "3.271828182845904523536028747135266249775724709369996");
        cln::cl_N clc = qd_to_cln<cln::cl_N>(qdcvhp);
        cln::cl_N comp = cln::complex("0.271828182845904523536028747135266249775724709369997_64", "3.271828182845904523536028747135266249775724709369996_64");
        CHECK(abs(comp-clc) < cln::cl_R(1e-64));
        CHECK(abs(cln::imagpart(comp)-cln::imagpart(clc)) < cln::cl_R(1e-64));
    }
#endif
}
