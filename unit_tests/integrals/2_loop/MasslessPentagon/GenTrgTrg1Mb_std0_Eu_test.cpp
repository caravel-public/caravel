/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#include "catch.hpp"

#include <iostream>

#include "Core/typedefs.h"
#include "Graph/Graph.h"
#include "misc/DeltaZero.h"
#include "Core/settings.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/integral_collection/2_loop/MasslessPentagon/GenTrgTrg1Mb_std0_Eu.h"

using namespace Caravel;
using namespace Caravel::Integrals;
using namespace Caravel::Integrals::loop_2::MasslessPentagon;

template <typename Out, typename In>
using Coeff = FBCoefficient<Series<Out>, In>;

template <typename Out, typename In>
using Func = StdBasisFunction<Out, In>;

using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;

TEST_CASE("GenTrgTrg1Mb_std0_Eu", "[GenTrgTrg1Mb_std0_Eu][double]"){

    // Generate the integral.
    GenTrgTrg1Mb_std0_Eu<Coeff<C, R>, Func<C, R>> integral({ "v1", "v2", "v4"}, 0);

    // Check whether the integral has been successfully tested numerically.
    if(!integral.successfully_tested_numerically()){
        WARN("The integral 'GenTrgTrg1Mb_std0_Eu' has not successfully been cross checked. Use at your own RISK!");
    }

    // Print integral information
    std::cout << "Insertion Name: " << "std0" << std::endl;
    gType input{ "CaravelGraph[Nodes[Node[Leg[2, 0]], Node[Leg[4]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[]]]]" };
    std::cout << "Graph information: \n" << input << std::endl;

    // Check against the provided phase space point result obtained in Mathematica.
    // Compute with the provided point
    auto result = integral.compute({ R(-2.3), R(-0.98), R(-0.728) });

    // If the target is almost zero compare absolute values (Otherwise dividing by target will yield a large number.).
    // If the target is non-zero, compute the relative error.

    // CHECK real parts
    std::cout << "Real parts: " << std::endl << std::endl;
    CHECK(check(result[-2].real(), R(3.14599346418162806), 5.0));
    std::cout << " --- Target= " << 3.14599346418162806 << std::endl
              << "     Result= " << result[-2].real() << std::endl;
    CHECK(check(result[-1].real(), R(2.57588509580957359), 5.0));
    std::cout << " --- Target= " << 2.57588509580957359 << std::endl
              << "     Result= " << result[-1].real() << std::endl;
    CHECK(check(result[0].real(), R(-0.0276521007121174230), 5.0));
    std::cout << " --- Target= " << -0.0276521007121174230 << std::endl
              << "     Result= " << result[0].real() << std::endl;
    

    // CHECK imaginary parts
    std::cout << "Imaginary parts: " << std::endl << std::endl;
    CHECK(check(result[-2].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-2].imag() << std::endl;
    CHECK(check(result[-1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-1].imag() << std::endl;
    CHECK(check(result[0].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[0].imag() << std::endl;
    
}
