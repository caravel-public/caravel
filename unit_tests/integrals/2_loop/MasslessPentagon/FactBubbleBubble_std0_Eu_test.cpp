/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#include "catch.hpp"

#include <iostream>

#include "Core/typedefs.h"
#include "Graph/Graph.h"
#include "misc/DeltaZero.h"
#include "Core/settings.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/integral_collection/2_loop/MasslessPentagon/FactBubbleBubble_std0_Eu.h"

using namespace Caravel;
using namespace Caravel::Integrals;
using namespace Caravel::Integrals::loop_2::MasslessPentagon;

template <typename Out, typename In>
using Coeff = FBCoefficient<Series<Out>, In>;

template <typename Out, typename In>
using Func = StdBasisFunction<Out, In>;

using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;

TEST_CASE("FactBubbleBubble_std0_Eu", "[FactBubbleBubble_std0_Eu][double]"){

    // Generate the integral.
    FactBubbleBubble_std0_Eu<Coeff<C, R>, Func<C, R>> integral({ "v1"}, 2);

    // Check whether the integral has been successfully tested numerically.
    if(!integral.successfully_tested_numerically()){
        WARN("The integral 'FactBubbleBubble_std0_Eu' has not successfully been cross checked. Use at your own RISK!");
    }

    // Print integral information
    std::cout << "Insertion Name: " << "std0" << std::endl;
    gType input{ "CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]" };
    std::cout << "Graph information: \n" << input << std::endl;

    // Check against the provided phase space point result obtained in Mathematica.
    // Compute with the provided point
    auto result = integral.compute({ R(-1.213) });

    // If the target is almost zero compare absolute values (Otherwise dividing by target will yield a large number.).
    // If the target is non-zero, compute the relative error.

    // CHECK real parts
    std::cout << "Real parts: " << std::endl << std::endl;
    CHECK(check(result[-2].real(), R(1.00000000000000000), 5.0));
    std::cout << " --- Target= " << 1.00000000000000000 << std::endl
              << "     Result= " << result[-2].real() << std::endl;
    CHECK(check(result[-1].real(), R(-0.386193259923826027), 5.0));
    std::cout << " --- Target= " << -0.386193259923826027 << std::endl
              << "     Result= " << result[-1].real() << std::endl;
    CHECK(check(result[0].real(), R(-1.57036144984293102), 5.0));
    std::cout << " --- Target= " << -1.57036144984293102 << std::endl
              << "     Result= " << result[0].real() << std::endl;
    CHECK(check(result[1].real(), R(-4.98393624579633965), 5.0));
    std::cout << " --- Target= " << -4.98393624579633965 << std::endl
              << "     Result= " << result[1].real() << std::endl;
    CHECK(check(result[2].real(), R(-3.63754789040434900), 5.0));
    std::cout << " --- Target= " << -3.63754789040434900 << std::endl
              << "     Result= " << result[2].real() << std::endl;
    

    // CHECK imaginary parts
    std::cout << "Imaginary parts: " << std::endl << std::endl;
    CHECK(check(result[-2].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-2].imag() << std::endl;
    CHECK(check(result[-1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-1].imag() << std::endl;
    CHECK(check(result[0].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[0].imag() << std::endl;
    CHECK(check(result[1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[1].imag() << std::endl;
    CHECK(check(result[2].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[2].imag() << std::endl;
    
}
