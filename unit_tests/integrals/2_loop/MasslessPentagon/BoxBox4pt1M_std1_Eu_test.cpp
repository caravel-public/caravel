/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#include "catch.hpp"

#include <iostream>

#include "Core/typedefs.h"
#include "Graph/Graph.h"
#include "misc/DeltaZero.h"
#include "Core/settings.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/integral_collection/2_loop/MasslessPentagon/BoxBox4pt1M_std1_Eu.h"

using namespace Caravel;
using namespace Caravel::Integrals;
using namespace Caravel::Integrals::loop_2::MasslessPentagon;

template <typename Out, typename In>
using Coeff = FBCoefficient<Series<Out>, In>;

template <typename Out, typename In>
using Func = StdBasisFunction<Out, In>;

using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;

TEST_CASE("BoxBox4pt1M_std1_Eu", "[BoxBox4pt1M_std1_Eu][double]"){

    // Generate the integral.
    BoxBox4pt1M_std1_Eu<Coeff<C, R>, Func<C, R>> integral({ "v5", "v1", "v3"}, 0);

    // Check whether the integral has been successfully tested numerically.
    if(!integral.successfully_tested_numerically()){
        WARN("The integral 'BoxBox4pt1M_std1_Eu' has not successfully been cross checked. Use at your own RISK!");
    }

    // Print integral information
    std::cout << "Insertion Name: " << "std1" << std::endl;
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[], Leg[1, 0], Link[]]]]" };
    std::cout << "Graph information: \n" << input << std::endl;

    // Check against the provided phase space point result obtained in Mathematica.
    // Compute with the provided point
    auto result = integral.compute({ R(-2.28), R(-2.3), R(-0.128) });

    // If the target is almost zero compare absolute values (Otherwise dividing by target will yield a large number.).
    // If the target is non-zero, compute the relative error.

    // CHECK real parts
    std::cout << "Real parts: " << std::endl << std::endl;
    CHECK(check(result[-3].real(), R(3.09846955001848823), 5.0));
    std::cout << " --- Target= " << 3.09846955001848823 << std::endl
              << "     Result= " << result[-3].real() << std::endl;
    CHECK(check(result[-2].real(), R(0.237429284030999277), 5.0));
    std::cout << " --- Target= " << 0.237429284030999277 << std::endl
              << "     Result= " << result[-2].real() << std::endl;
    CHECK(check(result[-1].real(), R(-34.3992938868293976), 5.0));
    std::cout << " --- Target= " << -34.3992938868293976 << std::endl
              << "     Result= " << result[-1].real() << std::endl;
    CHECK(check(result[0].real(), R(-109.034625924080203), 5.0));
    std::cout << " --- Target= " << -109.034625924080203 << std::endl
              << "     Result= " << result[0].real() << std::endl;
    

    // CHECK imaginary parts
    std::cout << "Imaginary parts: " << std::endl << std::endl;
    CHECK(check(result[-3].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-3].imag() << std::endl;
    CHECK(check(result[-2].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-2].imag() << std::endl;
    CHECK(check(result[-1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-1].imag() << std::endl;
    CHECK(check(result[0].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[0].imag() << std::endl;
    
}
