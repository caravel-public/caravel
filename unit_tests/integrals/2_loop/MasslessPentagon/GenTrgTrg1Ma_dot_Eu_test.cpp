/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#include "catch.hpp"

#include <iostream>

#include "Core/typedefs.h"
#include "Graph/Graph.h"
#include "misc/DeltaZero.h"
#include "Core/settings.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/integral_collection/2_loop/MasslessPentagon/GenTrgTrg1Ma_dot_Eu.h"

using namespace Caravel;
using namespace Caravel::Integrals;
using namespace Caravel::Integrals::loop_2::MasslessPentagon;

template <typename Out, typename In>
using Coeff = FBCoefficient<Series<Out>, In>;

template <typename Out, typename In>
using Func = StdBasisFunction<Out, In>;

using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;

TEST_CASE("GenTrgTrg1Ma_dot_Eu", "[GenTrgTrg1Ma_dot_Eu][double]"){

    // Generate the integral.
    GenTrgTrg1Ma_dot_Eu<Coeff<C, R>, Func<C, R>> integral({ "v1", "v5", "v3"}, 1);

    // Check whether the integral has been successfully tested numerically.
    if(!integral.successfully_tested_numerically()){
        WARN("The integral 'GenTrgTrg1Ma_dot_Eu' has not successfully been cross checked. Use at your own RISK!");
    }

    // Print integral information
    std::cout << "Insertion Name: " << "dot" << std::endl;
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[]]]]" };
    std::cout << "Graph information: \n" << input << std::endl;

    // Check against the provided phase space point result obtained in Mathematica.
    // Compute with the provided point
    auto result = integral.compute({ R(-2.3), R(-2.28), R(-0.128) });

    // If the target is almost zero compare absolute values (Otherwise dividing by target will yield a large number.).
    // If the target is non-zero, compute the relative error.

    // CHECK real parts
    std::cout << "Real parts: " << std::endl << std::endl;
    CHECK(check(result[-3].real(), R(-0.99999999999999978), 5.0));
    std::cout << " --- Target= " << -0.99999999999999978 << std::endl
              << "     Result= " << result[-3].real() << std::endl;
    CHECK(check(result[-2].real(), R(7.42561916192794502), 5.0));
    std::cout << " --- Target= " << 7.42561916192794502 << std::endl
              << "     Result= " << result[-2].real() << std::endl;
    CHECK(check(result[-1].real(), R(14.1458322664972460), 5.0));
    std::cout << " --- Target= " << 14.1458322664972460 << std::endl
              << "     Result= " << result[-1].real() << std::endl;
    CHECK(check(result[0].real(), R(-26.2322148019680021), 5.0));
    std::cout << " --- Target= " << -26.2322148019680021 << std::endl
              << "     Result= " << result[0].real() << std::endl;
    CHECK(check(result[1].real(), R(-187.765797543131697), 5.0));
    std::cout << " --- Target= " << -187.765797543131697 << std::endl
              << "     Result= " << result[1].real() << std::endl;
    

    // CHECK imaginary parts
    std::cout << "Imaginary parts: " << std::endl << std::endl;
    CHECK(check(result[-3].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-3].imag() << std::endl;
    CHECK(check(result[-2].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-2].imag() << std::endl;
    CHECK(check(result[-1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-1].imag() << std::endl;
    CHECK(check(result[0].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[0].imag() << std::endl;
    CHECK(check(result[1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[1].imag() << std::endl;
    
}
