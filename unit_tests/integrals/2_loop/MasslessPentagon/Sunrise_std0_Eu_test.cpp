/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#include "catch.hpp"

#include <iostream>

#include "Core/typedefs.h"
#include "Graph/Graph.h"
#include "misc/DeltaZero.h"
#include "Core/settings.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/integral_collection/2_loop/MasslessPentagon/Sunrise_std0_Eu.h"

using namespace Caravel;
using namespace Caravel::Integrals;
using namespace Caravel::Integrals::loop_2::MasslessPentagon;

template <typename Out, typename In>
using Coeff = FBCoefficient<Series<Out>, In>;

template <typename Out, typename In>
using Func = StdBasisFunction<Out, In>;

using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;

TEST_CASE("Sunrise_std0_Eu", "[Sunrise_std0_Eu][double]"){

    // Generate the integral.
    Sunrise_std0_Eu<Coeff<C, R>, Func<C, R>> integral({ "v1"}, 3);

    // Check whether the integral has been successfully tested numerically.
    if(!integral.successfully_tested_numerically()){
        WARN("The integral 'Sunrise_std0_Eu' has not successfully been cross checked. Use at your own RISK!");
    }

    // Print integral information
    std::cout << "Insertion Name: " << "std0" << std::endl;
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2]]]]" };
    std::cout << "Graph information: \n" << input << std::endl;

    // Check against the provided phase space point result obtained in Mathematica.
    // Compute with the provided point
    auto result = integral.compute({ R(-1.213) });

    // If the target is almost zero compare absolute values (Otherwise dividing by target will yield a large number.).
    // If the target is non-zero, compute the relative error.

    // CHECK real parts
    std::cout << "Real parts: " << std::endl << std::endl;
    CHECK(check(result[-1].real(), R(-0.250000000000000000), 5.0));
    std::cout << " --- Target= " << -0.250000000000000000 << std::endl
              << "     Result= " << result[-1].real() << std::endl;
    CHECK(check(result[0].real(), R(-1.52845168501904349), 5.0));
    std::cout << " --- Target= " << -1.52845168501904349 << std::endl
              << "     Result= " << result[0].real() << std::endl;
    CHECK(check(result[1].real(), R(-6.16734559016304829), 5.0));
    std::cout << " --- Target= " << -6.16734559016304829 << std::endl
              << "     Result= " << result[1].real() << std::endl;
    CHECK(check(result[2].real(), R(-18.6545791721142535), 5.0));
    std::cout << " --- Target= " << -18.6545791721142535 << std::endl
              << "     Result= " << result[2].real() << std::endl;
    CHECK(check(result[3].real(), R(-49.1033894793294721), 5.0));
    std::cout << " --- Target= " << -49.1033894793294721 << std::endl
              << "     Result= " << result[3].real() << std::endl;
    

    // CHECK imaginary parts
    std::cout << "Imaginary parts: " << std::endl << std::endl;
    CHECK(check(result[-1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-1].imag() << std::endl;
    CHECK(check(result[0].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[0].imag() << std::endl;
    CHECK(check(result[1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[1].imag() << std::endl;
    CHECK(check(result[2].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[2].imag() << std::endl;
    CHECK(check(result[3].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[3].imag() << std::endl;
    
}
