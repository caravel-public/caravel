/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#include "catch.hpp"

#include <iostream>

#include "Core/typedefs.h"
#include "Graph/Graph.h"
#include "misc/DeltaZero.h"
#include "Core/settings.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/integral_collection/2_loop/Goncharovs/Sunrise_scalar_Eu.h"

using namespace Caravel;
using namespace Caravel::Integrals;
using namespace Caravel::Integrals::loop_2::Goncharovs;

template <typename Out, typename In>
using Coeff = FBCoefficient<Series<Out>, In>;

template <typename Out, typename In>
using Func = StdBasisFunction<Out, In>;

using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;

TEST_CASE("Sunrise_scalar_Eu", "[Sunrise_scalar_Eu][double]"){

    // Generate the integral.
    Sunrise_scalar_Eu<Coeff<C, R>, Func<C, R>> integral({ "s"}, 4);

    // Check whether the integral has been successfully tested numerically.
    if(!integral.successfully_tested_numerically()){
        WARN("The integral 'Sunrise_scalar_Eu' has not successfully been cross checked. Use at your own RISK!");
    }

    // Print integral information
    std::cout << "Insertion Name: " << "scalar" << std::endl;
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2]]]]" };
    std::cout << "Graph information: \n" << input << std::endl;

    // Check against the provided phase space point result obtained in Mathematica.
    // Compute with the provided point
    auto result = integral.compute({ R(-1.213) });

    // If the target is almost zero compare absolute values (Otherwise dividing by target will yield a large number.).
    // If the target is non-zero, compute the relative error.

    // CHECK real parts
    std::cout << "Real parts: " << std::endl << std::endl;
    CHECK(check(result[-1].real(), R(0.303250000000000020), 12.0));
    std::cout << " --- Target= " << 0.303250000000000020 << std::endl
              << "     Result= " << result[-1].real() << std::endl;
    CHECK(check(result[0].real(), R(1.85401189392809984), 12.0));
    std::cout << " --- Target= " << 1.85401189392809984 << std::endl
              << "     Result= " << result[0].real() << std::endl;
    CHECK(check(result[1].real(), R(7.48099020086777866), 12.0));
    std::cout << " --- Target= " << 7.48099020086777866 << std::endl
              << "     Result= " << result[1].real() << std::endl;
    CHECK(check(result[2].real(), R(22.6280045357745934), 12.0));
    std::cout << " --- Target= " << 22.6280045357745934 << std::endl
              << "     Result= " << result[2].real() << std::endl;
    CHECK(check(result[3].real(), R(59.5624114384266576), 12.0));
    std::cout << " --- Target= " << 59.5624114384266576 << std::endl
              << "     Result= " << result[3].real() << std::endl;
    CHECK(check(result[4].real(), R(139.817513162487302), 12.0));
    std::cout << " --- Target= " << 139.817513162487302 << std::endl
              << "     Result= " << result[4].real() << std::endl;
    

    // CHECK imaginary parts
    std::cout << "Imaginary parts: " << std::endl << std::endl;
    CHECK(check(result[-1].imag(), R(0), 12.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-1].imag() << std::endl;
    CHECK(check(result[0].imag(), R(0), 12.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[0].imag() << std::endl;
    CHECK(check(result[1].imag(), R(0), 12.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[1].imag() << std::endl;
    CHECK(check(result[2].imag(), R(0), 12.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[2].imag() << std::endl;
    CHECK(check(result[3].imag(), R(0), 12.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[3].imag() << std::endl;
    CHECK(check(result[4].imag(), R(0), 12.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[4].imag() << std::endl;
    
}
