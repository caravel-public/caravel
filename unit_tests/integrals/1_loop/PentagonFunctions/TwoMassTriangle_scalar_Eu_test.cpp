/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#include "catch.hpp"

#include <iostream>

#include "Core/typedefs.h"
#include "Graph/Graph.h"
#include "misc/DeltaZero.h"
#include "Core/settings.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/integral_collection/1_loop/PentagonFunctions/TwoMassTriangle_scalar_Eu.h"

using namespace Caravel;
using namespace Caravel::Integrals;
using namespace Caravel::Integrals::loop_1::PentagonFunctions;

template <typename Out, typename In>
using Coeff = FBCoefficient<Series<Out>, In>;

template <typename Out, typename In>
using Func = StdBasisFunction<Out, In>;

using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;

TEST_CASE("TwoMassTriangle_scalar_Eu", "[TwoMassTriangle_scalar_Eu][double]"){

    // Generate the integral.
    TwoMassTriangle_scalar_Eu<Coeff<C, R>, Func<C, R>> integral({ "v1", "v3"}, 2);

    // Check whether the integral has been successfully tested numerically.
    if(!integral.successfully_tested_numerically()){
        WARN("The integral 'TwoMassTriangle_scalar_Eu' has not successfully been cross checked. Use at your own RISK!");
    }

    // Print integral information
    std::cout << "Insertion Name: " << "scalar" << std::endl;
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[], Leg[3, 0], Link[]]]]" };
    std::cout << "Graph information: \n" << input << std::endl;

    // Check against the provided phase space point result obtained in Mathematica.
    // Compute with the provided point
    auto result = integral.compute({ R(-1.55), R(-10.12) });

    // If the target is almost zero compare absolute values (Otherwise dividing by target will yield a large number.).
    // If the target is non-zero, compute the relative error.

    // CHECK real parts
    std::cout << "Real parts: " << std::endl << std::endl;
    CHECK(check(result[-1].real(), R(0.218933341065130016), 5.0));
    std::cout << " --- Target= " << 0.218933341065130016 << std::endl
              << "     Result= " << result[-1].real() << std::endl;
    CHECK(check(result[0].real(), R(-0.301336412818320853), 5.0));
    std::cout << " --- Target= " << -0.301336412818320853 << std::endl
              << "     Result= " << result[0].real() << std::endl;
    CHECK(check(result[1].real(), R(0.0594252851178258337), 5.0));
    std::cout << " --- Target= " << 0.0594252851178258337 << std::endl
              << "     Result= " << result[1].real() << std::endl;
    CHECK(check(result[2].real(), R(-0.505568853288859477), 5.0));
    std::cout << " --- Target= " << -0.505568853288859477 << std::endl
              << "     Result= " << result[2].real() << std::endl;
    

    // CHECK imaginary parts
    std::cout << "Imaginary parts: " << std::endl << std::endl;
    CHECK(check(result[-1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[-1].imag() << std::endl;
    CHECK(check(result[0].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[0].imag() << std::endl;
    CHECK(check(result[1].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[1].imag() << std::endl;
    CHECK(check(result[2].imag(), R(0), 5.0));
    std::cout << " --- Target= " << 0 << std::endl
              << "     Result= " << result[2].imag() << std::endl;
    
}
