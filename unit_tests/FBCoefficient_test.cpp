#include "catch.hpp"

#include <vector>
#include <string>
#include <stdexcept>
#include <functional>

#include "IntegralLibrary/FBCoefficient.h"

using namespace Caravel;
using namespace Caravel::Integrals;

TEST_CASE("FBCoefficient"){

    // Create an integral coefficient who depends as follows on the
    // kinematical variables x1 and x2:
    // ep^-2 * x1 + ep^-1 * x2 + x1 / x2
    FBCoefficient<Series<double>, double> coeff([](std::vector<double> x){
        return Series<double>(-2, 0, x[0], x[1], x[0]/x[1]);});

    // Check whether after a computation, the values are correctly stored.
    coeff.compute({4., 2.});
    CHECK(coeff.get_value()[-2] == Approx(4).epsilon(1e-12));
    CHECK(coeff.get_value()[-1] == Approx(2).epsilon(1e-12));
    CHECK(coeff.get_value()[ 0] == Approx(2).epsilon(1e-12));

    // Check what happens when trying to access a non-present term in the
    // expansion.
    CHECK_THROWS_AS(coeff.get_value()[ 1], std::range_error);

    // Check whether the values are correctly returned upon computation:
    auto result = coeff.compute({9., 3.});
    CHECK(result[-2] == Approx(9).epsilon(1e-12));
    CHECK(result[-1] == Approx(3).epsilon(1e-12));
    CHECK(result[ 0] == Approx(3).epsilon(1e-12));

    // Check whether it is also possible to instantiate a type which is
    // not pre_instantiated.
    FBCoefficient<Series<int>, int> not_such_a_useful_type([](std::vector<int> x){
        return Series<int>(0,0, 4);
    });
    CHECK(not_such_a_useful_type.compute({3})[0] == 4);

    // Check something different:
    // A coefficient is called with a vector of strings and returns the number
    // of strings.
    FBCoefficient<size_t, std::string> stringcoeff([](std::vector<std::string> x){
        return x.size();
    });

    CHECK(stringcoeff.compute({"hello", "world"}) == 2);

}
