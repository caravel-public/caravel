#include "catch.hpp"

#include <functional>
#include <unordered_map>

#include "Core/Series.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/FunctionBasisExpansion.h"

using namespace Caravel;
using namespace Caravel::Integrals;

template <typename T>
using NBFE = NumericFunctionBasisExpansion<T, DummyBasisFunction>;

TEST_CASE("Simple Basis", "[simple]"){


    // Create several dummy functions.
    DummyBasisFunction f1("log(x)");
    DummyBasisFunction f2("Li2(y)");
    DummyBasisFunction f3("y/x");
    DummyBasisFunction f4("log(x)"); // Equal to f1!

    // Create the corresponding values for the coefficients
    Series<double> c1(-2, 0, 1., 2., 3.);
    Series<double> c2(-2, 0, 4., 5. ,6.);
    Series<double> c3(-2, 0, 7., 8., 9.);
    Series<double> c4(-2, 0, 3., 2., 1.);

    // Create an unordered map
    std::unordered_map<DummyBasisFunction, Series<double>> map;
    std::unordered_map<DummyBasisFunction, Series<double>> map2;

    CHECK(map.size() == 0);

    map.insert({f1, c1});
    map.insert({f2, c2});
    map.insert({f3, c3});

    map2.insert({f4, c4});

    CHECK(map.size() == 3);
    CHECK(map2.size() == 1);


    // Create a basis function from this.
    NBFE<Series<double>> basis(map);
    NBFE<Series<double>> basis2(map2);
    // Check the number of functions contained in the basis
    CHECK(basis.n_basis_funcs() == 3);
    CHECK(basis2.n_basis_funcs() == 1);

    // Check whether specific functions are contained in the basis.
    CHECK(basis.contains(f1) == true);
    CHECK(basis.contains(f2) == true);
    CHECK(basis.contains(f3) == true);
    CHECK(basis.contains(DummyBasisFunction("const")) == false);

    // Get values of coefficients
    auto coeff2 = basis.get_coeff(f2);
    CHECK(coeff2[-2] == Approx(4.).epsilon(1e-12));
    CHECK(coeff2[-1] == Approx(5.).epsilon(1e-12));
    CHECK(coeff2[ 0] == Approx(6.).epsilon(1e-12));

    CHECK(basis.get_coeff(f1)[-2] == Approx(1.).epsilon(1e-12));
    CHECK(basis.get_coeff(f1)[-1] == Approx(2.).epsilon(1e-12));
    CHECK(basis.get_coeff(f1)[ 0] == Approx(3.).epsilon(1e-12));

    CHECK(basis.get_coeff(f2)[-2] == Approx(4.).epsilon(1e-12));
    CHECK(basis.get_coeff(f2)[-1] == Approx(5.).epsilon(1e-12));
    CHECK(basis.get_coeff(f2)[ 0] == Approx(6.).epsilon(1e-12));

    CHECK(basis.get_coeff(f3)[-2] == Approx(7.).epsilon(1e-12));
    CHECK(basis.get_coeff(f3)[-1] == Approx(8.).epsilon(1e-12));
    CHECK(basis.get_coeff(f3)[ 0] == Approx(9.).epsilon(1e-12));

    SECTION("Multiply"){
        SECTION("*="){
            basis *= 3.;
            coeff2 = basis.get_coeff(f2);
            CHECK(coeff2[-2] == Approx(12).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(15).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(18).epsilon(1e-12));


        }
        SECTION("*= Series"){
            Series<double> s(-2, -1, 1., 3.);
            basis *= s;
            coeff2 = basis.get_coeff(f1);
            CHECK(coeff2.leading() == -4);
            CHECK(coeff2.last() == -3);
            CHECK(coeff2[-4] == Approx(1.).epsilon(1e-12));
            CHECK(coeff2[-3] == Approx(5.).epsilon(1e-12));

        }
        SECTION("*"){
            // Scalar to the right
            auto prod1 = basis * 3.;
            coeff2 = prod1.get_coeff(f2);
            CHECK(coeff2[-2] == Approx(12).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(15).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(18).epsilon(1e-12));

            // Scalar to the left
            auto prod2 = 2. * basis;
            coeff2 = prod2.get_coeff(f2);
            CHECK(coeff2[-2] == Approx(8).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(10).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(12).epsilon(1e-12));

            // Check that the previous stuff left basis untouched
            CHECK(basis.get_coeff(f1)[-2] == Approx(1.).epsilon(1e-12));
            CHECK(basis.get_coeff(f1)[-1] == Approx(2.).epsilon(1e-12));
            CHECK(basis.get_coeff(f1)[ 0] == Approx(3.).epsilon(1e-12));

            CHECK(basis.get_coeff(f2)[-2] == Approx(4.).epsilon(1e-12));
            CHECK(basis.get_coeff(f2)[-1] == Approx(5.).epsilon(1e-12));
            CHECK(basis.get_coeff(f2)[ 0] == Approx(6.).epsilon(1e-12));

            CHECK(basis.get_coeff(f3)[-2] == Approx(7.).epsilon(1e-12));
            CHECK(basis.get_coeff(f3)[-1] == Approx(8.).epsilon(1e-12));
            CHECK(basis.get_coeff(f3)[ 0] == Approx(9.).epsilon(1e-12));
        }
    }

    SECTION("Divide"){
        SECTION("/="){
            basis /= 2.;
            coeff2 = basis.get_coeff(f2);
            CHECK(coeff2[-2] == Approx(2).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(2.5).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(3).epsilon(1e-12));

        }
        SECTION("/"){
            auto divide = basis / 2.;
            coeff2 = divide.get_coeff(f2);

            CHECK(coeff2[-2] == Approx(2).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(2.5).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(3).epsilon(1e-12));

            // Check that the previous stuff left basis untouched
            CHECK(basis.get_coeff(f1)[-2] == Approx(1.).epsilon(1e-12));
            CHECK(basis.get_coeff(f1)[-1] == Approx(2.).epsilon(1e-12));
            CHECK(basis.get_coeff(f1)[ 0] == Approx(3.).epsilon(1e-12));

            CHECK(basis.get_coeff(f2)[-2] == Approx(4.).epsilon(1e-12));
            CHECK(basis.get_coeff(f2)[-1] == Approx(5.).epsilon(1e-12));
            CHECK(basis.get_coeff(f2)[ 0] == Approx(6.).epsilon(1e-12));

            CHECK(basis.get_coeff(f3)[-2] == Approx(7.).epsilon(1e-12));
            CHECK(basis.get_coeff(f3)[-1] == Approx(8.).epsilon(1e-12));
            CHECK(basis.get_coeff(f3)[ 0] == Approx(9.).epsilon(1e-12));
        }
    }

    SECTION("Subtract"){
        SECTION("-="){
            basis2 -= basis;
            CHECK(basis2.n_basis_funcs() == 3);
            coeff2 = basis2.get_coeff(f2);
            CHECK(coeff2[-2] == Approx(-4).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(-5).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(-6).epsilon(1e-12));

            coeff2 = basis2.get_coeff(f1);
            CHECK(coeff2[-2] == Approx(2).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(0).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(-2).epsilon(1e-12));

        }
        SECTION("-"){
            auto diff = basis2 - basis;
            CHECK(diff.n_basis_funcs() == 3);
            coeff2 = diff.get_coeff(f2);
            CHECK(coeff2[-2] == Approx(-4).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(-5).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(-6).epsilon(1e-12));

            coeff2 = diff.get_coeff(f1);
            CHECK(coeff2[-2] == Approx(2).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(0).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(-2).epsilon(1e-12));

            // Check that the previous stuff left basis untouched
            CHECK(basis.get_coeff(f1)[-2] == Approx(1.).epsilon(1e-12));
            CHECK(basis.get_coeff(f1)[-1] == Approx(2.).epsilon(1e-12));
            CHECK(basis.get_coeff(f1)[ 0] == Approx(3.).epsilon(1e-12));

            CHECK(basis.get_coeff(f2)[-2] == Approx(4.).epsilon(1e-12));
            CHECK(basis.get_coeff(f2)[-1] == Approx(5.).epsilon(1e-12));
            CHECK(basis.get_coeff(f2)[ 0] == Approx(6.).epsilon(1e-12));

            CHECK(basis.get_coeff(f3)[-2] == Approx(7.).epsilon(1e-12));
            CHECK(basis.get_coeff(f3)[-1] == Approx(8.).epsilon(1e-12));
            CHECK(basis.get_coeff(f3)[ 0] == Approx(9.).epsilon(1e-12));
        }
    }

    SECTION("Add"){
        SECTION("+="){
            basis2 += basis;
            CHECK(basis2.n_basis_funcs() == 3);
            coeff2 = basis2.get_coeff(f2);
            CHECK(coeff2[-2] == Approx(4).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(5).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(6).epsilon(1e-12));

            coeff2 = basis2.get_coeff(f1);
            CHECK(coeff2[-2] == Approx(4).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(4).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(4).epsilon(1e-12));
        }
        SECTION("+"){
            auto sum = basis2 + basis;
            CHECK(sum.n_basis_funcs() == 3);
            coeff2 = sum.get_coeff(f2);
            CHECK(coeff2[-2] == Approx(4).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(5).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(6).epsilon(1e-12));

            coeff2 = sum.get_coeff(f1);
            CHECK(coeff2[-2] == Approx(4).epsilon(1e-12));
            CHECK(coeff2[-1] == Approx(4).epsilon(1e-12));
            CHECK(coeff2[ 0] == Approx(4).epsilon(1e-12));

            // Check that the previous stuff left basis untouched
            CHECK(basis.get_coeff(f1)[-2] == Approx(1.).epsilon(1e-12));
            CHECK(basis.get_coeff(f1)[-1] == Approx(2.).epsilon(1e-12));
            CHECK(basis.get_coeff(f1)[ 0] == Approx(3.).epsilon(1e-12));

            CHECK(basis.get_coeff(f2)[-2] == Approx(4.).epsilon(1e-12));
            CHECK(basis.get_coeff(f2)[-1] == Approx(5.).epsilon(1e-12));
            CHECK(basis.get_coeff(f2)[ 0] == Approx(6.).epsilon(1e-12));

            CHECK(basis.get_coeff(f3)[-2] == Approx(7.).epsilon(1e-12));
            CHECK(basis.get_coeff(f3)[-1] == Approx(8.).epsilon(1e-12));
            CHECK(basis.get_coeff(f3)[ 0] == Approx(9.).epsilon(1e-12));
        }
    }
}

// Add a string class that can be added and multiplied such that it is in principle
// a valid basis coefficient.
// Forward declaration:
class mystring;
mystring operator-(const mystring& s);

class mystring {
public:
    mystring(std::string s) : str{s} {}
    std::string get() const {return str;}
    size_t length() const {return str.length();}
    mystring operator*=(size_t n){
        std::string toret("");
        for(size_t i = 0; i < n; ++i){
            toret += str;
        }
        str = toret;
        return *this;
    }
    mystring operator+=(mystring other){
        str += other.get();
        return *this;
    }
    // Subtracting a string adds the second string in reversed order.
    mystring operator-=(mystring other){
        str += (-other).get();
        return *this;
    }
    char operator[](int i) const {
        return str[i];
    }
private:
    std::string str;
};

mystring operator-(const mystring& s){
    std::string toreturn("");
    for(int i = s.length() - 1; i >= 0; --i){
        toreturn += s[i];
    }
    return mystring(toreturn);
}

mystring operator+(const mystring& s1, const mystring& s2){
    auto toreturn(s1);
    toreturn += s2;
    return toreturn;
}

mystring operator-(const mystring& s1, const mystring& s2){
    auto toreturn(s1);
    toreturn -= s2;
    return toreturn;
}

mystring operator*(const mystring& s, size_t n){
    auto toreturn(s);
    toreturn *= n;
    return toreturn;
}

mystring operator*(size_t n, const mystring& s){
    auto toreturn(s);
    toreturn *= n;
    return toreturn;
}

TEST_CASE("Basis of strings"){
    // This is not of big use I suppose, I just want to check the templating:

    // Create several dummy functions.
    DummyBasisFunction f1("log(x)");
    DummyBasisFunction f2("Li2(y)");
    DummyBasisFunction f3("y/x");
    DummyBasisFunction f4("log(x)");

    // Create the boolean coefficients
    mystring c1("c1");
    mystring c2("c2");
    mystring c3("c3");

    std::unordered_map<DummyBasisFunction, mystring> map1;
    map1.insert({f1, c1});
    map1.insert({f2, c2});
    map1.insert({f3, c3});

    NBFE<mystring> basis1(map1);

    std::unordered_map<DummyBasisFunction, mystring> map2;
    map2.insert({f4, mystring("c4")});

    NBFE<mystring> basis2(map2);

    CHECK(basis1.n_basis_funcs() == 3);
    CHECK(basis2.n_basis_funcs() == 1);


    SECTION("Multiplication"){
        auto basis = basis1 * 2;
        CHECK(basis.n_basis_funcs() == 3);
        CHECK(basis.get_coeff(f1).get() == "c1c1");
        CHECK(basis.get_coeff(f2).get() == "c2c2");
        CHECK(basis.get_coeff(f3).get() == "c3c3");
    }

    SECTION("Addition"){
        auto sum = basis1 + basis2;
        // f1 == f4, so number of basis functions is not increased,
        // only the coefficients should be added.
        CHECK(sum.n_basis_funcs() == 3);
        CHECK(sum.get_coeff(f1).get() == "c1c4");
        CHECK(sum.get_coeff(f2).get() == "c2");
        CHECK(sum.get_coeff(f3).get() == "c3");
    }

    SECTION("Difference"){
        SECTION("-"){
            auto diff = basis2 - basis1;
            CHECK(diff.n_basis_funcs() == 3);
            CHECK(diff.get_coeff(f1).get() == "c41c");
            CHECK(diff.get_coeff(f2).get() == "2c");
            CHECK(diff.get_coeff(f3).get() == "3c");
        }
        SECTION("-="){
            basis2 -= basis1;
            CHECK(basis2.n_basis_funcs() == 3);
            CHECK(basis2.get_coeff(f1).get() == "c41c");
            CHECK(basis2.get_coeff(f2).get() == "2c");
            CHECK(basis2.get_coeff(f3).get() == "3c");
        }
    }

}


TEST_CASE("Abstract FunctionBasisExpansion", "abstract"){

    // typedef for ease of use.
    using CType = FBCoefficient<Series<double>, double>;

    // Create a FunctionBasisExpansion based on Coefficients with Series<double>
    // and DummyBasisFunction's.
    FunctionBasisExpansion<CType, DummyBasisFunction> basis;

    // Now create the functions to add to this basis:
    DummyBasisFunction f1("log(x)");
    DummyBasisFunction f2("Li2(y)");
    DummyBasisFunction f3("y/x");
    DummyBasisFunction f4("log(x)"); // Equal to f1!

    // Create the coefficients of these basis functions
    CType c1([](std::vector<double> x){
        return Series<double>(-2, 0, x[0], x[1], x[0] + x[1]);
    });

    CType c2([](std::vector<double> x){
        return Series<double>(-2, 0, x[1], x[0], x[0] * x[1]);
    });

    CType c3([](std::vector<double> x){
        return Series<double>(-2, 0, 0., 1., x[0] - x[1]);
    });

    CType c4([](std::vector<double> x){
        return Series<double>(-2, 0, 0.0, 0.0, x[0] / x[1]);
    });

    // Add elements to the basis
    basis.add_basis_element(f1, c1);
    basis.add_basis_element(f2, c2);
    basis.add_basis_element(f3, c3);

    // Add the abstract level cannot add additional coefficient for contained
    // basis function
    CHECK_THROWS_AS(basis.add_basis_element(f4, c4), std::runtime_error);

    CHECK(basis.n_basis_funcs() == 3);

    // NumericFunctionBasisExpansion<Series<double>, DummyBasisFunction>
    auto numbasis = basis.eval_coeffs(std::vector<double>({4., 2.}));

    CHECK(numbasis.n_basis_funcs() == 3);
    CHECK(numbasis.contains(f1) == true);

    CHECK(numbasis.get_coeff(f1)[-2] == Approx(4.).epsilon(1e-12));
    CHECK(numbasis.get_coeff(f1)[-1] == Approx(2.).epsilon(1e-12));
    CHECK(numbasis.get_coeff(f1)[ 0] == Approx(6.).epsilon(1e-12));

    CHECK(numbasis.get_coeff(f2)[-2] == Approx(2.).epsilon(1e-12));
    CHECK(numbasis.get_coeff(f2)[-1] == Approx(4.).epsilon(1e-12));
    CHECK(numbasis.get_coeff(f2)[ 0] == Approx(8.).epsilon(1e-12));

    CHECK(numbasis.get_coeff(f3)[-2] == Approx(0.).epsilon(1e-12));
    CHECK(numbasis.get_coeff(f3)[-1] == Approx(1.).epsilon(1e-12));
    CHECK(numbasis.get_coeff(f3)[ 0] == Approx(2.).epsilon(1e-12));
}

TEST_CASE("Eps Powers"){
    // Create several dummy functions.
    DummyBasisFunction f1("log(x)");
    DummyBasisFunction f2("Li2(y)");
    DummyBasisFunction f3("y/x");
    DummyBasisFunction f4("log(x)"); // Equal to f1!

    // Create the corresponding values for the coefficients
    Series<double> c1(-2, 0, 1., 2., 0.);
    Series<double> c2(-2, 0, 4., 5. ,6.);
    Series<double> c3(-2, 0, 7., 8., 0.);
    Series<double> c4(-2, 0, 3., 2., 1.);

    // Create a NumericFunctionBasisExpansion with double coefficient.
    // In this case one cannot ask for an epsilon coefficient.
    std::unordered_map<DummyBasisFunction, double> map_error;
    map_error.insert({f1, 1.1});
    map_error.insert({f2, 2.2});
    map_error.insert({f3, 3.3});
    NumericFunctionBasisExpansion<double, DummyBasisFunction> error(map_error);
    CHECK(error.n_basis_funcs() == 3);
    // This results in a compile time error since the one can only ask for an
    // epsilon coefficient if the Coefficient return type is a Series.
    // error.get_eps_pow_coeff(-2);

    // Create an unordered map
    std::unordered_map<DummyBasisFunction, Series<double>> map;
    std::unordered_map<DummyBasisFunction, Series<double>> map2;

    CHECK(map.size() == 0);

    map.insert({f1, c1});
    map.insert({f2, c2});
    map.insert({f3, c3});
    map.insert({f4, c4});

    CHECK(map.size() == 3);


    // Create a basis function from this.
    NBFE<Series<double>> basis(map);

    // In this basis we can ask for epsilon coefficients:
    NumericFunctionBasisExpansion<double, DummyBasisFunction> coeff_0 = basis.get_eps_pow_coeff(0);
    CHECK(coeff_0.n_basis_funcs() == 1);
    CHECK(coeff_0.get_coeff(f2) == 6.);
    CHECK(coeff_0.contains(f3) == false);

    NumericFunctionBasisExpansion<double, DummyBasisFunction> coeff_m1 = basis.get_eps_pow_coeff(-1);
    CHECK(coeff_m1.n_basis_funcs() == 3);
    CHECK(coeff_m1.get_coeff(f1) == Approx(2.));
    CHECK(coeff_m1.get_coeff(f2) == Approx(5.));
    CHECK(coeff_m1.get_coeff(f3) == Approx(8.));
}
