#include "catch.hpp"

#include <cmath>
#include <utility>
#include <stdexcept>
#include <functional>

#include "Core/Series.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/MasterIntegral.h"

using namespace Caravel;
using namespace Caravel::Integrals;

/*
In this test we consider and example the decomposition of the Master Integrals
into a basis of special functions.

To do so, we consider a hypothetical amplitude which is given by 2 Master
Integrals:

        A = c_1 * I_1 + c_2 * I_2

In this expression we choose for definiteness :

        c_1 = 2
        c_2 = 3

Furthermore, we decompose each integral in a basis of special functions, namely

        f1 = log(x)
        f2 = Li2(y)
        f3 = Pi^2 / 6

Here x and y can be thought of as kinematical invariant which the process
depends on.

In this basis we have:

        I_1 = b_1 * f1 + b_2 * f2
        I_2 = b_3 * f3 + b_4 * f1

We note that f1 appears in both integrals, so at the moment where the amplitude
is expressed in a basis of special functions, the coefficient of f1 should be
given by

        b_1 + b_4

Finally we fix the exact functional form of the coefficients on x and y. Notice
that these coefficients are Series expansions in the dimensional regulator
epsilon (abbreviated by ep).

        b_1 = ep^-2 * x + ep^-1 * y + (x + y)
        b_2 = ep^-2 * y + ep^-1 * x + (x * y)
        b_3 = ep^-2 * 0 + ep^-1 * 1 + (x - y)
        b_4 = ep^-2 * 0 + ep^-1 * 0 + x / y

 */

TEST_CASE("Example", "example"){
    // Create typedef for ease of use:
    using CType = FBCoefficient<Series<double>, double>;

    // Create the functions to add to this basis:
    DummyBasisFunction f1("log(x)");
    DummyBasisFunction f2("Li2(y)");
    DummyBasisFunction f3("Pi^2/6");

    // Create the coefficients of these basis functions
    CType b1([](std::vector<double> x){
        return Series<double>(-2, 0, x[0], x[1], x[0] + x[1]);
    });

    CType b2([](std::vector<double> x){
        return Series<double>(-2, 0, x[1], x[0], x[0] * x[1]);
    });

    CType b3([](std::vector<double> x){
        return Series<double>(-2, 0, 0., 1., x[0] - x[1]);
    });

    CType b4([](std::vector<double> x){
        return Series<double>(-2, 0, 0.0, 0.0, x[0] / x[1]);
    });

    // Create Master integrals and add basis elements:
    // I1 is creates using a method add_basis_element while I2 is constructed
    // with a special constructor.

    // I1 = b_1 * f1 + b_2 * f2
    MasterIntegral<CType, DummyBasisFunction> I1;
    I1.add_basis_element(f1, b1);
    I1.add_basis_element(f2, b2);

    // I2 = b_3 * f3 + b_4 * f1
    using vpair_func_coeff = std::vector<std::pair<DummyBasisFunction, CType > >;
    vpair_func_coeff basis = {{f3, b3}, {f1, b4}};
    MasterIntegral<CType, DummyBasisFunction> I2(basis);

    // Now associate each Integral with its Master Integral Coefficient
    // and plug them in a vector
    // A = c1 * I1 + c2 * I2
    //   = 2 * I1 + 3 * I2
    using pair_coeff_mi = std::vector<std::pair<double, MasterIntegral<CType, DummyBasisFunction> > >;
    pair_coeff_mi amplitude_decomp({std::make_pair(2., I1), std::make_pair(3., I2)});

    // Next we compute the integrals for x = 4. and y = 2. and then
    // take a look at the amplitude in the basis of special functions:
    // Therefore we separately evaluate the basis of both integrals numerically,
    // multiply them by the integral coefficient and then add the results
    // to obtain the amplitude in the basis of special functions:
    using num_basis = NumericFunctionBasisExpansion<Series<double>, DummyBasisFunction>;
    num_basis amp_in_basis = amplitude_decomp[0].first * amplitude_decomp[0].second.eval_basis_coeffs({4., 2.});
    amp_in_basis += amplitude_decomp[1].first * amplitude_decomp[1].second.eval_basis_coeffs({4., 2.});

    // Check whether there are indeed 3 functions in the basis
    CHECK(amp_in_basis.n_basis_funcs() == 3);

    // Check whether specific functions are contained
    CHECK(amp_in_basis.contains(f1) == true);
    CHECK(amp_in_basis.contains(f2) == true);
    CHECK(amp_in_basis.contains(f3) == true);
    CHECK(amp_in_basis.contains(DummyBasisFunction("EllipticDisaster")) == false);

    // Now it is time to check whether the coefficients appear
    //      For f1, the coefficients of I1 and I2 are merged: b_1 + b_4
    CHECK(amp_in_basis.get_coeff(f1)[-2] == Approx(8.));
    CHECK(amp_in_basis.get_coeff(f1)[-1] == Approx(4.));
    CHECK(amp_in_basis.get_coeff(f1)[ 0] == Approx(18.));
    //      For f2 and f3 the coefficients are just 2 * b_2 and  3 * b_3
    CHECK(amp_in_basis.get_coeff(f2)[-2] == Approx(4.));
    CHECK(amp_in_basis.get_coeff(f2)[-1] == Approx(8.));
    CHECK(amp_in_basis.get_coeff(f2)[ 0] == Approx(16.));

    CHECK(amp_in_basis.get_coeff(f3)[-2] == Approx(0.));
    CHECK(amp_in_basis.get_coeff(f3)[-1] == Approx(3.));
    CHECK(amp_in_basis.get_coeff(f3)[ 0] == Approx(6.));
}

/*
In this test we consider and example the decomposition of the Master Integrals
into a basis of special functions.

To do so, we consider a hypothetical amplitude which is given by 2 Master
Integrals:

        A = c_1 * I_1 + c_2 * I_2

In this expression we choose for definiteness :

        c_1 = 2
        c_2 = 3

Furthermore, we decompose each integral in a basis of special functions, namely

        f1 = log(x)
        f2 = log(x) * log(y)
        f3 = Pi^2 / 6

Here x and y can be thought of as kinematical invariant which the process
depends on.

In this basis we have:

        I_1 = b_1 * f1 + b_2 * f2
        I_2 = b_3 * f3 + b_4 * f1

We note that f1 appears in both integrals, so at the moment where the amplitude
is expressed in a basis of special functions, the coefficient of f1 should be
given by

        b_1 + b_4

Finally we fix the exact functional form of the coefficients on x and y. Notice
that these coefficients are Series expansions in the dimensional regulator
epsilon (abbreviated by ep).

        b_1 = ep^-2 * x + ep^-1 * y + (x + y)
        b_2 = ep^-2 * y + ep^-1 * x + (x * y)
        b_3 = ep^-2 * 0 + ep^-1 * 1 + (x - y)
        b_4 = ep^-2 * 0 + ep^-1 * 0 + x / y

 */

TEST_CASE("Example w/ StdBasisFunction", "[example]"){
    // Create typedef for ease of use:
    using CType = FBCoefficient<Series<double>, double>;
    using FType = StdBasisFunction<double, double>;

    // Create the functions to add to this basis:
    FType f1([](std::vector<double> args){return std::log(args[0]);}, "log(args[0])", {"x", "y"});
    FType f2([](std::vector<double> args){return std::log(args[0])*log(args[1]);}, "log(args[0])log(args[1])", {"x", "y"});
    FType f3([](std::vector<double> args){return std::acos(-1) * std::acos(-1) / 6.;}, "Pi^2/6", {"x", "y"});

    CHECK(f1.as_string() == "log[x]");
    CHECK(f2.as_string() == "log[x]log[y]");
    CHECK(f3.as_string() == "Pi^2/6");

    // Create the coefficients of these basis functions
    CType b1([](std::vector<double> x){
        return Series<double>(-2, 0, x[0], x[1], x[0] + x[1]);
    });

    CType b2([](std::vector<double> x){
        return Series<double>(-2, 0, x[1], x[0], x[0] * x[1]);
    });

    CType b3([](std::vector<double> x){
        return Series<double>(-2, 0, 0., 1., x[0] - x[1]);
    });

    CType b4([](std::vector<double> x){
        return Series<double>(-2, 0, 0.0, 0.0, x[0] / x[1]);
    });

    // Create Master integrals and add basis elements:
    // I1 is creates using a method add_basis_element while I2 is constructed
    // with a special constructor.

    // I1 = b_1 * f1 + b_2 * f2
    MasterIntegral<CType, FType> I1;
    I1.add_basis_element(f1, b1);
    I1.add_basis_element(f2, b2);

    // I2 = b_3 * f3 + b_4 * f1
    using vpair_func_coeff = std::vector<std::pair<FType, CType > >;
    vpair_func_coeff basis = {{f3, b3}, {f1, b4}};
    MasterIntegral<CType, FType> I2(basis);

    // Now associate each Integral with its Master Integral Coefficient
    // and plug them in a vector
    // A = c1 * I1 + c2 * I2
    //   = 2 * I1 + 3 * I2
    using pair_coeff_mi = std::vector<std::pair<double, MasterIntegral<CType, FType > > >;
    pair_coeff_mi amplitude_decomp({std::make_pair(2., I1), std::make_pair(3., I2)});

    // Next we compute the integrals for x = 4. and y = 2. and then
    // take a look at the amplitude in the basis of special functions:
    // Therefore we separately evaluate the basis of both integrals numerically,
    // multiply them by the integral coefficient and then add the results
    // to obtain the amplitude in the basis of special functions:
    using num_basis = NumericFunctionBasisExpansion<Series<double>, FType>;
    num_basis amp_in_basis = amplitude_decomp[0].first * amplitude_decomp[0].second.eval_basis_coeffs({4., 2.});
    amp_in_basis += amplitude_decomp[1].first * amplitude_decomp[1].second.eval_basis_coeffs({4., 2.});

    // Check whether there are indeed 3 functions in the basis
    CHECK(amp_in_basis.n_basis_funcs() == 3);

    // Check whether specific functions are contained
    CHECK(amp_in_basis.contains(f1) == true);
    CHECK(amp_in_basis.contains(f2) == true);
    CHECK(amp_in_basis.contains(f3) == true);
    CHECK(amp_in_basis.contains(FType([](std::vector<double> x){return 1;}, "EllipticDisaster", {"x"})) == false);

    // Now it is time to check whether the coefficients appear
    //      For f1, the coefficients of I1 and I2 are merged: b_1 + b_4
    CHECK(amp_in_basis.get_coeff(f1)[-2] == Approx(8.));
    CHECK(amp_in_basis.get_coeff(f1)[-1] == Approx(4.));
    CHECK(amp_in_basis.get_coeff(f1)[ 0] == Approx(18.));
    //      For f2 and f3 the coefficients are just 2 * b_2 and  3 * b_3
    CHECK(amp_in_basis.get_coeff(f2)[-2] == Approx(4.));
    CHECK(amp_in_basis.get_coeff(f2)[-1] == Approx(8.));
    CHECK(amp_in_basis.get_coeff(f2)[ 0] == Approx(16.));

    CHECK(amp_in_basis.get_coeff(f3)[-2] == Approx(0.));
    CHECK(amp_in_basis.get_coeff(f3)[-1] == Approx(3.));
    CHECK(amp_in_basis.get_coeff(f3)[ 0] == Approx(6.));
}

TEST_CASE("MasterIntegral"){
    // Typedef for convenience.
    using CType = FBCoefficient<Series<double>, double>;

    // Create empty Master Integrals
    MasterIntegral<CType, DummyBasisFunction> I1;
    MasterIntegral<CType, DummyBasisFunction> I2;

    // Create the coefficients and functions that enter the basis
    // Now create the functions to add to this basis:
    DummyBasisFunction f1("log(x)");
    DummyBasisFunction f2("Li2(y)");
    DummyBasisFunction f3("Pi^2/6");

    // Create the coefficients of these basis functions
    CType b1([](std::vector<double> x){
        return Series<double>(-2, 0, x[0], x[1], x[0] + x[1]);
    });

    CType b2([](std::vector<double> x){
        return Series<double>(-2, 0, x[1], x[0], x[0] * x[1]);
    });

    CType b3([](std::vector<double> x){
        return Series<double>(-2, 0, 0., 1., x[0] - x[1]);
    });

    CType b4([](std::vector<double> x){
        return Series<double>(-2, 0, 0.0, 0.0, x[0] / x[1]);
    });

    // Add the basis elements to the corresponding integrals
    // I1 = b_1 * f1 + b_2 * f2
    I1.add_basis_element(f1, b1);
    I1.add_basis_element(f2, b2);

    // I2 = b_3 * f3 + b_4 * f1
    I2.add_basis_element(f3, b3);
    I2.add_basis_element(f1, b4);

    CHECK(I1.contains_basis_function(f1) == true);
    CHECK(I1.contains_basis_function(f2) == true);
    CHECK(I1.contains_basis_function(f3) == false);

    CHECK(I2.contains_basis_function(f1) == true);
    CHECK(I2.contains_basis_function(f2) == false);
    CHECK(I2.contains_basis_function(f3) == true);

    CHECK(I1.n_basis_funcs() == 2);
    CHECK(I2.n_basis_funcs() == 2);

    // Cannot add f1 again to I1
    CHECK_THROWS_AS(I1.add_basis_element(f1, b3), std::runtime_error);

    // Extract the underlying basis
    auto basis = I1.get_basis();
    CHECK(basis.contains(f1) == true);
    CHECK(basis.contains(f2) == true);
    CHECK(basis.contains(f3) == false);
    CHECK(basis.n_basis_funcs() == 2);
}
#include <iostream>
using std::cout;
using std::endl;
TEST_CASE("Master Integral with integral input") {

    SECTION("C<double, double> F<double, double>"){
        // Integral has both Functions and Coefficients taking and returning
        // double values. This way its coefficients can be evaluated separately but
        // it can also be evaluated completely together with the basis functions.
        MasterIntegral<FBCoefficient<double, double>, StdBasisFunction<double, double>> I1;

        FBCoefficient<double, double> c1([](std::vector<double> x){
            return 1.1;
        });

        StdBasisFunction<double, double> f1([](std::vector<double> x){
            return 2. * x[0];
        }, "f1", {"x"});


        I1.add_basis_element(f1, c1);

        // Evaluate only the basis coefficients.
        auto coeffs1 = I1.eval_basis_coeffs({double(2.)});
        CHECK(coeffs1.get_coeff(f1) == 1.1);

        // Evaluate the integral as a whole!
        auto res1 = I1.compute({double(2.)});
        CHECK(res1 == Approx(4.4));

        // Get the underlying basis
        auto basis = I1.get_basis();
        CHECK(basis.n_basis_funcs() == 1);

        CHECK(basis.eval_coeffs({double(2.)}).get_coeff(f1) == 1.1);
        CHECK(basis.contains(f1) == true);

    }

#ifdef HIGH_PRECISION
    SECTION("C<double, double> F<double, RHP>"){
        // Integral has Function that take an RHP and return a double
        // and Coefficients taking and returning
        // double values. This way its coefficients can be evaluated separately but
        // the integral cannot be computed as a whole since the input types of Coefficients
        // and basis functions differ.
        MasterIntegral<FBCoefficient<double, double>, StdBasisFunction<double, RHP>> I1;

        FBCoefficient<double, double> c1([](std::vector<double> x){
            return 1.1;
        });

        StdBasisFunction<double, RHP> f1([](std::vector<RHP> x){
            return 2.0;
        }, "f1", {"x"});


        I1.add_basis_element(f1, c1);

        // Evaluate only the basis coefficients.
        auto coeffs1 = I1.eval_basis_coeffs({double(2.)});
        CHECK(coeffs1.get_coeff(f1) == 1.1);

        // This gives a compilation error since this method is disabled if the
        // input types for basis functions and coefficients differ..
        // auto res1 = I1.compute({RHP("2.")});

    }
#endif


    SECTION("C<double, int> F<double, int>"){
        // First Integral has both Functions and Coefficients taking and returning
        // int values. This way its coefficients can be evaluated separately but
        // the integral cannot be computed as a whole since integral input for
        // basis functions is disabled.
        MasterIntegral<FBCoefficient<double, int>, StdBasisFunction<double, int>> I1;

        FBCoefficient<double, int> c1([](std::vector<int> x){
            return 1.1;
        });

        StdBasisFunction<double, int> f1([](std::vector<int> x){
            return 2.0 * x[0];
        }, "f1", {"x"});


        I1.add_basis_element(f1, c1);

        // Evaluate only the basis coefficients.
        auto coeffs1 = I1.eval_basis_coeffs({2});
        CHECK(coeffs1.get_coeff(f1) == 1.1);

        // This gives a compilation error since this method is disabled if the
        // input type for the basis function is of integral type.
        //auto res1 = I1.compute({2});

    }


}
