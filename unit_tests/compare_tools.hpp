#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif
#include "Core/type_traits_extra.h"
#include "Core/momD.h"
#include <vector>
#include <array>
#include "Core/extended_t.h"

namespace tests{
    template <class T> inline T _epsilon();
    template <> inline double _epsilon<double>() { return double(3e-14); }
#ifdef HIGH_PRECISION
    template <> inline dd_real _epsilon<dd_real>() { return dd_real(2e-30); }
#endif
#ifdef VERY_HIGH_PRECISION
    template <> inline qd_real _epsilon<qd_real>() { return qd_real(2e-61); }
#endif

    template <class T> inline bool is_zero(const T& x,typename std::enable_if_t<Caravel::is_floating_point<T>::value>* = nullptr) {
        using std::abs;
        if (abs(x) < _epsilon<T>())
            return true;
        else
            return false;
    }

    template <class T> inline bool is_zero(const T& x,typename std::enable_if_t<Caravel::is_exact<T>::value>* = nullptr) {
        if (x==T(0)) return true;
        else return false;
    }

    template <class T> inline bool is_zero(const std::complex<T>& x) {
        if (is_zero(x.real()) and is_zero(x.imag())) return true;
        else return false;
    }


    template <class T> inline bool is_zero(const T& x, typename T::iterator* = nullptr) {
        bool r = true;
        for(const auto& it: x){
            r = r && is_zero(it);
        }
        return r;
    }

}

#define CHECK_ZERO(X) CHECK(tests::is_zero((X)))
#define REQUIRE_ZERO(X) REQUIRE(tests::is_zero((X)))
