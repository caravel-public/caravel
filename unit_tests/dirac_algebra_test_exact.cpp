#include "Core/spinor/clifford_algebra.h"
#include "Core/spinor/DSpinor.h"
#include "catch.hpp"
#include "compare_tools.hpp"
#include <iostream>

 
using namespace std;
using namespace Caravel;
using namespace clifford_algebra;

/*
 * This generates a vector of (i,i,val), which corresponds to 2*g^(mu,nu)*Idendity for D vector dimensions
 */
template <size_t D, typename T> vector<tuple<size_t, size_t, T>> anti_commute_res(const int mu, const int nu) {
    // Resulting matrix
    size_t Dt = two_to_the(D / 2);
    vector<tuple<size_t, size_t, T>> res;
    res.clear();

    // no non-zero values for mu != nu
    if (mu != nu) return res;
     
    for (size_t i = 1; i <= Dt; i++) res.emplace_back(i, i, T(2)*T(get_metric_signature<T>(mu)));

    return res;
}

// Computes the anti-commutator {gamma^mu,gamma^\nu} for a D dimensional representation
template <size_t D, typename T> vector<tuple<size_t, size_t, T>> anti_commute(const int mu, const int nu) {
    // Initialize Dirac matrices and get values
    sparse_DGamma<D, T>::init();
    auto A = sparse_DGamma<D, T>::values[mu];
    auto B = sparse_DGamma<D, T>::values[nu];

    // Resulting matrix
    vector<tuple<size_t, size_t, T>> res;
    res.clear();

    // Compute A_ik*B_kj + B_ik*A_kj
    for (size_t i = 1; i <= sparse_DGamma<D, T>::Dt; i++) {
        for (size_t j = 1; j <= sparse_DGamma<D, T>::Dt; j++) {

            T s = T(0);

            // A_ik*B_kj
            for (auto f : A) {
                if (get<0>(f) != i) continue;
                for (auto g : B) {
                    if (get<1>(f) != get<0>(g) || get<1>(g) != j) continue;
                    s += get<2>(f) * get<2>(g);
                }
            }

            // B_ik*A_kj
            for (auto f : B) {
                if (get<0>(f) != i) continue;
                for (auto g : A) {
                    if (get<1>(f) != get<0>(g) || get<1>(g) != j) continue;
                    s += get<2>(f) * get<2>(g);
                }
            }
            if (s != T(0)) res.emplace_back(i, j, s);
        }
    }
    return res;
}

/* 
 * Checks {gamma^mu,gamma^nu} = 2*g^{munu}
 * return true if correct
 */
template <size_t Ds, typename T> bool check_clifford() {
    for (int mu = 0; mu < (int) Ds; mu++) {
        for (int nu = 0; nu < (int) Ds; nu++) {
            if (anti_commute<Ds, T>(mu, nu) != anti_commute_res<Ds, T>(mu, nu)) return false;
        }
    }
    return true;
}

/*
 * Checks gstar*gstart = 1, gstar = gamma5 in 4D 
 * Returns true if correct
 */
template <size_t Ds, typename T> bool check_gstar() {
    // Initialize Dirac matrices and get values
    sparse_DGamma<Ds, T>::init();
    auto A = sparse_DGamma<Ds, T>::values[Ds];
    const unsigned Dt = sparse_DGamma<Ds,T>::Dt;

    // Compute A_ik*A_kj 
    for (size_t i = 1; i <= Dt; i++) {
        for (size_t j = 1; j <= Dt; j++) {

            T s = T(0);

            // A_ik*A_kj
            for (auto f : A) {
                if (get<0>(f) != i) continue;
                for (auto g : A) {
                    if (get<1>(f) != get<0>(g) || get<1>(g) != j) continue;
                    s += get<2>(f) * get<2>(g);
                }
            }

	    if( i != j && s != T(0) ) return false;
            if( i == j && s != T(1) ) return false;
        }
    }

    return true;
}

template <size_t Ds, typename T> bool check_hermiticity(){
  sparse_DGamma<Ds,T,true>::init();
  sparse_DGamma<Ds,T,false>::init();
  const unsigned Dt = sparse_DGamma<Ds,T,true>::Dt;

  for (unsigned mu = 0; mu < Ds; mu++){
    auto A = sparse_DGamma<Ds,T,true>::values[mu];
    auto B = sparse_DGamma<Ds,T,false>::values[mu];

    // check if A^mu_ij = B_mu_ij
    T Aij, Bij;
    for( size_t i = 1; i <= Dt; i++){
      for( size_t j = 1; j <= Dt; j++){
        Aij = T(0); 
        Bij = T(0);
        for( auto f:A)
          if( get<0>(f) == i && get<1>(f) == j) Aij = get<2>(f);
        for( auto g:B)
          if( get<0>(g) == i && get<1>(g) == j) Bij = get<2>(g);

        if( Aij - T(get_metric_signature<T>(mu))*Bij != T(0) ) return false;
      }
    }
  }

  return true;
}

// checks (p_slash -m)|Psi> = 0 and <Psi|(p_slash -m) = 0
template<mult_from MF, typename T, size_t Ds, typename TT> bool dirac(const momentumD<T,Ds>& p, const T& mass, const std::vector<TT>& sp){
  
  std::vector<TT> out = multiply_sparse_DGamma<MF,TT>(p.get_components(),sp);
//  std::vector<T> out = multiply_sparse_DGamma<MF,Ds,T>(p,sp);
  for(unsigned i = 0; i < out.size(); i++)
	out[i] -= mass*sp[i];

    for (auto s : out){
        if ( !(s == TT(0)) ) { for(auto i : out) cout << i << endl;};
        if ( !(s == TT(0)) ) return false;
   }

    return true;
}

// Return (p_slash - m)_ij
template<typename TE, typename T, size_t Ds> std::vector<std::vector<TE>> polsum_explicit(const momentumD<T,Ds>&p, const T& mass){
  sparse_DGamma<Ds,T>::init();
  unsigned Dt = two_to_the(Ds/2);
  std::vector<std::vector<TE>> res;
  res.clear();

  for(unsigned i = 0; i < Dt; i++)
    res.push_back(std::vector<TE>(Dt,TE(0)));

  auto p_ext = to_extended_t<std::array<TE,Ds>,Ds>(p.get_components());

  // 1) res_ij = p_mu *[gamma^mu]_ij
  for(int mu = 0; mu < (int) Ds; mu++){
      auto const vals = sparse_DGamma<Ds, T,false>::values[mu];
      for (auto s : vals) {
          int i = std::get<0>(s) - 1;
          int j = std::get<1>(s) - 1;
          TE gij = TE(std::get<2>(s)); // [gamma^mu]_ij
          res[i][j] += p_ext[mu] * TE(get_metric_signature<TE>(mu)) * gij;
      }
  };

    // 2) res_ij -= mass * delta_ij
    for (size_t i = 0; i < Dt; i++) 
    	res[i][i] -= mass;

    return res;
}

// checks if the polarisation sum is correct
template<typename T, size_t Ds, typename TT> bool polsum(const momentumD<T,Ds>& p, const T& mass, const vector<vector<TT>>& Sp, const vector<vector<TT>>& Sm, const vector<vector<TT>>& Sbp, const vector<vector<TT>>& Sbm, const T& normP, const T& normM){
  vector<vector<TT>> result = polsum_explicit<TT>(p,mass);
  const unsigned Dt = two_to_the(Ds/2);
 
  for(unsigned Dtt_index = 0; Dtt_index < Sp.size(); Dtt_index++){
    vector<TT> Up = Sp[Dtt_index];
    vector<TT> Um = Sm[Dtt_index];
    vector<TT> Ubarp = Sbp[Dtt_index];
    vector<TT> Ubarm = Sbm[Dtt_index];
    for (unsigned i = 0; i < Dt; i++) {
        for (unsigned j = 0; j < Dt; j++) { result[i][j] -= (Up[i] * Ubarp[j] / normP + Um[i] * Ubarm[j] / normM); }
    }
  }

  for (unsigned i = 0; i < Dt; i++) {
      for (unsigned j = 0; j < Dt; j++) { 
        if( !(result[i][j] == TT(0)) ) { 
           std::cout << "------------------" << std::endl;
           for( unsigned k = 0; k < Dt; k++){
             for( unsigned l = 0; l < Dt; l++){
               std::cout << result[k][l] << "\t";
             }
	     std::cout << endl;
           }
	  return false;
        }
      }
  }

  return true;
}

template <size_t Ds, typename T> bool check_massless_spinors(){
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  T mass = T(0);
  momentumD<T,Ds>  p = momentumD<T,Ds>( T(1876863770), T(649330712),T(1500954637),T(877694639) );
  REQUIRE(p * p == T(0));

  vector<vector<T>> Up, Um, Ubarp, Ubarm;
  vector<vector<T>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_Up<T>(p, idx));
      Um.push_back(DSpinor_Um<T>(p, idx));
      Ubarp.push_back(DSpinor_Ubarp<T>(p, idx));
      Ubarm.push_back(DSpinor_Ubarm<T>(p, idx));
      Vp.push_back(DSpinor_Vp<T>(p, idx));
      Vm.push_back(DSpinor_Vm<T>(p, idx));
      Vbarp.push_back(DSpinor_Vbarp<T>(p, idx));
      Vbarm.push_back(DSpinor_Vbarm<T>(p, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Since, external spinors are not normalised we have to introduce the normalisation here to check completeness
#if WEYL
  const T normP = p[0] + p[3];
  const T normM = p[0] + p[3];
#else
  const T normP = T(2)*(p[0] + p[3]);
  const T normM = T(2)*(p[0] + p[3]);
#endif

  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm, normP, normM));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm, normM, normP));

  return true;
}

template <size_t Ds, typename T> bool check_massive_spinors(){
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  T mass = T(150);
  momentumD<T,Ds>  p = momentumD<T,Ds>( T(1146673)/T(40), T(751517542)/T(11353),T(751513042)/T(11353),T(536584239)/T(10) );
  momentumD<T,Ds>  q = momentumD<T,Ds>( T(1876863770), T(649330712),T(1500954637),T(877694639) );
  REQUIRE(p * p == mass*mass);
  REQUIRE(q * q == T(0));

  vector<vector<T>> Up, Um, Ubarp, Ubarm;
  vector<vector<T>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_Up<T>(p, q, mass, idx));
      Um.push_back(DSpinor_Um<T>(p, q, mass, idx));
      Ubarp.push_back(DSpinor_Ubarp<T>(p, q, mass, idx));
      Ubarm.push_back(DSpinor_Ubarm<T>(p, q, mass, idx));
      Vp.push_back(DSpinor_Vp<T>(p, q, mass, idx));
      Vm.push_back(DSpinor_Vm<T>(p, q, mass, idx));
      Vbarp.push_back(DSpinor_Vbarp<T>(p, q, mass, idx));
      Vbarm.push_back(DSpinor_Vbarm<T>(p, q, mass, idx));
  }

  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Since, external spinors are not normalised we have to introduce the normalisation here to check completeness
#if WEYL
  const T normP = (q[0] + q[3])*T(2)*T(p*q);
  const T normM = (q[0] + q[3])*T(2)*T(p*q);
#else
  const T normP = T(2)*(q[0] + q[3])*T(2)*T(p*q);
  const T normM = T(2)*(q[0] + q[3])*T(2)*T(p*q);
#endif


  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum(p, -mass, Up, Um, Ubarp, Ubarm, normP, normM));
  REQUIRE(polsum(p, mass, Vp, Vm, Vbarp, Vbarm, normM, normP));

  return true;
}

template <size_t Ds, typename T> bool check_cut_wavefunctions(){
  typedef extended_t<T> TE;
  constexpr unsigned Dt = two_to_the(Ds/2);
  constexpr unsigned Dtt = Dt/4;

  EpsilonBasis<T>::squares[0]=T(261826812);
  EpsilonBasis<T>::squares[1]=T(0);
  T mass = T(173);
  momentumD<T,Ds>  p = momentumD<T,6>( T(1103307266), T(1565997353), T(462777045), T(1868942145), T(2147483628) );
  REQUIRE(p * p == mass*mass);

  vector<vector<TE>> Up, Um, Ubarp, Ubarm;
  vector<vector<TE>> Vp, Vm, Vbarp, Vbarm;

  for (unsigned idx = 1; idx <= Dtt; idx++) {
      Up.push_back(DSpinor_cut_Qbp<TE>(-p, mass, idx));
      Um.push_back(DSpinor_cut_Qbm<TE>(-p, mass, idx));
      Ubarp.push_back(DSpinor_cut_Qp<TE>(p, mass, idx));
      Ubarm.push_back(DSpinor_cut_Qm<TE>(p, mass, idx));
      Vp.push_back(DSpinor_cut_Qbp<TE>(p, mass, idx));
      Vm.push_back(DSpinor_cut_Qbm<TE>(p, mass, idx));
      Vbarp.push_back(DSpinor_cut_Qp<TE>(-p, mass, idx));
      Vbarm.push_back(DSpinor_cut_Qm<TE>(-p, mass, idx));
  }


  // Dirac Equation
  for (unsigned i = 0; i < Dtt; i++) {
      REQUIRE(dirac<mult_from::R>(p, mass, Up[i]));
      REQUIRE(dirac<mult_from::R>(p, mass, Um[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarp[i]));
      REQUIRE(dirac<mult_from::L>(p, mass, Ubarm[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vp[i]));
      REQUIRE(dirac<mult_from::R>(p, -mass, Vm[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarp[i]));
      REQUIRE(dirac<mult_from::L>(p, -mass, Vbarm[i]));
  }

  // Since, external spinors are not normalised we have to introduce the normalisation here to check completeness
  const T normP = T(1);
  const T normM = T(1);


  // Polarisation Sum (polsum implements p_slash -m)
  REQUIRE(polsum( p, -mass, Up, Um, Ubarp, Ubarm, normP, normM));
  REQUIRE(polsum(-p, -mass, Vp, Vm, Vbarp, Vbarm, normM, normP));
  return true;
}

TEST_CASE("Check Ds dimensional clifford algebra and Dirac matrices"){
    SECTION("Algebra properties in Ds = 4") {
        constexpr size_t Ds = 4;
        REQUIRE(check_gstar<Ds, F32>());
        REQUIRE(check_clifford<Ds, F32>());
        REQUIRE(check_hermiticity<Ds, F32>());
    }
    SECTION("Algebra properties in Ds = 6") {
        constexpr size_t Ds = 6;
        REQUIRE(check_gstar<Ds, F32>());
        REQUIRE(check_clifford<Ds, F32>());
        REQUIRE(check_hermiticity<Ds, F32>());
    }
    SECTION("Algebra properties in Ds = 8") {
        constexpr size_t Ds = 8;
        REQUIRE(check_gstar<Ds, F32>());
        REQUIRE(check_clifford<Ds, F32>());
        REQUIRE(check_hermiticity<Ds, F32>());
    }
    SECTION("Algebra properties in Ds = 10") {
        constexpr size_t Ds = 10;
        REQUIRE(check_gstar<Ds, F32>());
        REQUIRE(check_clifford<Ds, F32>());
        REQUIRE(check_hermiticity<Ds, F32>());
    }
    SECTION("Algebra properties in Ds = 12") {
        constexpr size_t Ds = 12;
        REQUIRE(check_gstar<Ds, F32>());
        REQUIRE(check_clifford<Ds, F32>());
        REQUIRE(check_hermiticity<Ds, F32>());
    }
    SECTION("Algebra properties in Ds = 14") {
        constexpr size_t Ds = 14;
        REQUIRE(check_gstar<Ds, F32>());
        REQUIRE(check_clifford<Ds, F32>());
        REQUIRE(check_hermiticity<Ds, F32>());
    }
}
TEST_CASE("Check DS dimensional Spinor properties"){
    SECTION("Spinor properties in Ds = 4") {
        constexpr size_t Ds = 4;
        REQUIRE(check_massless_spinors<Ds, F32>());
        REQUIRE(check_massive_spinors<Ds, F32>());
    }
    SECTION("Spinor properties in Ds = 6") {
        constexpr size_t Ds = 6;
        REQUIRE(check_massless_spinors<Ds, F32>());
        REQUIRE(check_massive_spinors<Ds, F32>());
        REQUIRE(check_cut_wavefunctions<Ds, F32>());
    }
    SECTION("Spinor properties in Ds = 8") {
        constexpr size_t Ds = 8;
        REQUIRE(check_massless_spinors<Ds, F32>());
        REQUIRE(check_massive_spinors<Ds, F32>());
        REQUIRE(check_cut_wavefunctions<Ds, F32>());
    }
    SECTION("Spinor properties in Ds = 10") {
        constexpr size_t Ds = 10;
        REQUIRE(check_massless_spinors<Ds, F32>());
        REQUIRE(check_massive_spinors<Ds, F32>());
        REQUIRE(check_cut_wavefunctions<Ds, F32>());
    }
    SECTION("Spinor properties in Ds = 12") {
        constexpr size_t Ds = 12;
        REQUIRE(check_massless_spinors<Ds, F32>());
        REQUIRE(check_massive_spinors<Ds, F32>());
        REQUIRE(check_cut_wavefunctions<Ds, F32>());
    }
    SECTION("Spinor properties in Ds = 14") {
        constexpr size_t Ds = 14;
        REQUIRE(check_massless_spinors<Ds, F32>());
        REQUIRE(check_massive_spinors<Ds, F32>());
        REQUIRE(check_cut_wavefunctions<Ds, F32>());
    }
}
