/**
 * Graph_test.cpp
 *
 * First created on 19.2.2019, major upgrade on 8.7.2019
 *
 * A unit test program to check on graph hierarchies
 *
 */

#include "catch.hpp"

#include "AmpEng/Diagrammatica.h"
#include "AmpEng/FilterGraphs.h"

using namespace Caravel;
using namespace lGraph;
using namespace AmpEng;

TEST_CASE("Size of a 1-loop 6-pt planar (ordered) hierarchy", "[HierarchySize]") {
    // Construct the 1-loop diagrams with 6 massless legs
    Walkers nLoopswLegs(1,6);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Filter hexagons (and above)"<<std::endl;
    nLoopswLegs.filter(drop_hexagons_and_above);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Filter scaleless graphs"<<std::endl;
    nLoopswLegs.filter(scaleless_graphs_filter);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Only planar with proper ordering"<<std::endl;
    nLoopswLegs.filter(nonplanar_ordering_filter({1,2,3,4,5,6}));
    
    // total number of graphs
    CHECK(nLoopswLegs.size()==50);
    CHECK(nLoopswLegs.topology_size()==8);	// 1-mass pentagon; 1-mass, 2-mass-easy, 2-mass-hard boxes; 1-, 2-, 3-mass triangle; bubble
}

TEST_CASE("Size of a 2-loop 4-pt planar (ordered) hierarchy", "[HierarchySize]") {
    // Construct the 2-loop diagrams with 4 massless legs
    Walkers nLoopswLegs(2,4);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Filter scaleless graphs"<<std::endl;
    nLoopswLegs.filter(scaleless_graphs_filter);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Only planar with proper ordering"<<std::endl;
    nLoopswLegs.filter(nonplanar_ordering_filter({1,2,3,4}));
    
    // total number of graphs
    CHECK(nLoopswLegs.get_xgraphs(1)[2].size()==2);	// 1-node 2-points
    CHECK(nLoopswLegs.get_xgraphs(1)[3].size()==4);	// 1-node 3-points
#if 0
    CHECK(nLoopswLegs.get_xgraphs(1)[4].size()==2);	// 1-node 4-points
#endif
    CHECK(nLoopswLegs.get_xgraphs(2)[2].size()==10);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_xgraphs(2)[3].size()==72);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_xgraphs(2)[4].size()==32);	// 2-nodes 4-points
#if 0
    CHECK(nLoopswLegs.size()==122);
#endif

    // total number of topologies
    CHECK(nLoopswLegs.get_topologies(1)[2].size()==1);	// 1-node 2-points
    CHECK(nLoopswLegs.get_topologies(1)[3].size()==1);	// 1-node 3-points
    CHECK(nLoopswLegs.get_topologies(1)[4].size()==1);	// 1-node 4-points
    CHECK(nLoopswLegs.get_topologies(2)[2].size()==4);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_topologies(2)[3].size()==11);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_topologies(2)[4].size()==7);	// 2-nodes 4-points
    CHECK(nLoopswLegs.topology_size()==25);	// There are 24 diagrams in Figure 1 of arXiv:1703.05273, but here we include (so far) all subleading-pole-related diagrams
						// (the extra one corresponds to the diagram which is subleading-pole related to diagram # 5 in row 3 of the figure)
}

TEST_CASE("Size of a 2-loop 5-pt planar (ordered) hierarchy", "[HierarchySize]") {
    // Construct the 2-loop diagrams with 5 massless legs
    Walkers nLoopswLegs(2,5);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Filter scaleless graphs"<<std::endl;
    nLoopswLegs.filter(scaleless_graphs_filter);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Only planar with proper ordering"<<std::endl;
    nLoopswLegs.filter(nonplanar_ordering_filter({1,2,3,4,5}));
    
    // total number of graphs
    CHECK(nLoopswLegs.get_xgraphs(1)[2].size()==5);	// 1-node 2-points
    CHECK(nLoopswLegs.get_xgraphs(1)[3].size()==20);	// 1-node 3-points
#if 0
    CHECK(nLoopswLegs.get_xgraphs(1)[4].size()==25);	// 1-node 4-points
    CHECK(nLoopswLegs.get_xgraphs(1)[5].size()==10);	// 1-node 6-points
#endif
    CHECK(nLoopswLegs.get_xgraphs(2)[2].size()==25);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_xgraphs(2)[3].size()==180);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_xgraphs(2)[4].size()==160);	// 2-nodes 4-points
    CHECK(nLoopswLegs.get_xgraphs(2)[5].size()==50);	// 2-nodes 5-points
#if 0
    CHECK(nLoopswLegs.size()==475);
#endif

    // total number of topologies
    CHECK(nLoopswLegs.get_topologies(1)[2].size()==1);	// 1-node 2-points
    CHECK(nLoopswLegs.get_topologies(1)[3].size()==3);	// 1-node 3-points
    CHECK(nLoopswLegs.get_topologies(1)[4].size()==3);	// 1-node 4-points
    CHECK(nLoopswLegs.get_topologies(1)[5].size()==2);	// 1-node 5-points
    CHECK(nLoopswLegs.get_topologies(2)[2].size()==4);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_topologies(2)[3].size()==22);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_topologies(2)[4].size()==18);	// 2-nodes 4-points
    CHECK(nLoopswLegs.get_topologies(2)[5].size()==8);	// 2-nodes 5-points
    CHECK(nLoopswLegs.topology_size()==61);	// There are 60 diagrams in Figure 2 of arXiv:1712.03946v2, but there is a typo in that figure as we explain next in this comment.
						// Here we include (so far) all subleading-pole-related diagrams (for a total of 61). If we were to drop them, counting would be 58.
						// Notice that in arXiv:1712.03946v2 also all subleading-pole-related diagrams are included, EXCEPT that the subleading-pole-related
                                                // diagram to the BoxBubble in diagram # 6 of row 8 is missing! (I think we meant NOT to write subleading-pole-related diagrams
						// but we actually end up writing ALMOST all of them.)
}

#if 0
// WARNING: untested numbers
TEST_CASE("Size of a 2-loop 5-pt 1-mass planar (ordered) hierarchy", "[HierarchySize]") {
    std::cout<<"WARNING: untested numbers for 2-loop 5-pt 1-mass planar hierarchy"<<std::endl;
    // Construct the 2-loop diagrams with 4 massless legs and one massive
    Walkers nLoopswLegs(2,{0,0,0,0,1});
    std::cout<<"================================"<<std::endl;
    std::cout<<"Filter scaleless graphs"<<std::endl;
    nLoopswLegs.filter(scaleless_graphs_filter);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Only planar with proper ordering"<<std::endl;
    nLoopswLegs.filter(nonplanar_ordering_filter({1,2,3,4,5}));
    
    // total number of graphs
    CHECK(nLoopswLegs.get_xgraphs(1)[2].size()==6);	// 1-node 2-points
    CHECK(nLoopswLegs.get_xgraphs(1)[3].size()==28);	// 1-node 3-points
    CHECK(nLoopswLegs.get_xgraphs(1)[4].size()==35);	// 1-node 4-points
    CHECK(nLoopswLegs.get_xgraphs(1)[5].size()==13);	// 1-node 6-points
    CHECK(nLoopswLegs.get_xgraphs(2)[2].size()==30);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_xgraphs(2)[3].size()==180);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_xgraphs(2)[4].size()==160);	// 2-nodes 4-points
    CHECK(nLoopswLegs.get_xgraphs(2)[5].size()==50);	// 2-nodes 5-points
    CHECK(nLoopswLegs.size()==502);

    // total number of topologies
    CHECK(nLoopswLegs.get_topologies(1)[2].size()==2);	// 1-node 2-points
    CHECK(nLoopswLegs.get_topologies(1)[3].size()==10);	// 1-node 3-points
    CHECK(nLoopswLegs.get_topologies(1)[4].size()==14);	// 1-node 4-points
    CHECK(nLoopswLegs.get_topologies(1)[5].size()==7);	// 1-node 5-points
    CHECK(nLoopswLegs.get_topologies(2)[2].size()==9);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_topologies(2)[3].size()==51);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_topologies(2)[4].size()==68);	// 2-nodes 4-points
    CHECK(nLoopswLegs.get_topologies(2)[5].size()==28);	// 2-nodes 5-points
    CHECK(nLoopswLegs.topology_size()==189);	
}
#endif
#if 0
// WARNING: untested numbers
TEST_CASE("Size of a 2-loop 5-pt 2-mass planar (ordered) hierarchy", "[HierarchySize]") {
    std::cout<<"WARNING: untested numbers for 2-loop 5-pt 2-mass planar hierarchy"<<std::endl;
    // Construct the 2-loop diagrams with 3 massless legs and two massive
    Walkers nLoopswLegs(2,{0,0,0,1,2});
    std::cout<<"================================"<<std::endl;
    std::cout<<"Filter scaleless graphs"<<std::endl;
    nLoopswLegs.filter(scaleless_graphs_filter);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Only planar with proper ordering"<<std::endl;
    nLoopswLegs.filter(nonplanar_ordering_filter({1,2,3,4,5}));
    
    // total number of graphs
    CHECK(nLoopswLegs.get_xgraphs(1)[2].size()==7);	// 1-node 2-points
    CHECK(nLoopswLegs.get_xgraphs(1)[3].size()==37);	// 1-node 3-points
    CHECK(nLoopswLegs.get_xgraphs(1)[4].size()==45);	// 1-node 4-points
    CHECK(nLoopswLegs.get_xgraphs(1)[5].size()==16);	// 1-node 6-points
    CHECK(nLoopswLegs.get_xgraphs(2)[2].size()==35);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_xgraphs(2)[3].size()==180);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_xgraphs(2)[4].size()==160);	// 2-nodes 4-points
    CHECK(nLoopswLegs.get_xgraphs(2)[5].size()==50);	// 2-nodes 5-points
    CHECK(nLoopswLegs.size()==530);

    // total number of topologies
    CHECK(nLoopswLegs.get_topologies(1)[2].size()==3);	// 1-node 2-points
    CHECK(nLoopswLegs.get_topologies(1)[3].size()==23);	// 1-node 3-points
    CHECK(nLoopswLegs.get_topologies(1)[4].size()==38);	// 1-node 4-points
    CHECK(nLoopswLegs.get_topologies(1)[5].size()==15);	// 1-node 5-points
    CHECK(nLoopswLegs.get_topologies(2)[2].size()==14);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_topologies(2)[3].size()==98);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_topologies(2)[4].size()==146);	// 2-nodes 4-points
    CHECK(nLoopswLegs.get_topologies(2)[5].size()==50);	// 2-nodes 5-points
    CHECK(nLoopswLegs.topology_size()==387);	
}
#endif

#if 0
// WARNING: untested numbers
TEST_CASE("Size of a 3-loop 4-pt non-planar hierarchy", "[HierarchySize]") {
    std::cout<<"WARNING: untested numbers for 3-loop 4-pt non-planar hierarchy"<<std::endl;
    // Construct the 3-loop diagrams with 4 massless legs
    Walkers nLoopswLegs(3,4);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Filter scaleless graphs"<<std::endl;
    nLoopswLegs.filter(scaleless_graphs_filter);
    
    // total number of graphs
    CHECK(nLoopswLegs.get_xgraphs(1)[2].size()==0);	// 1-node 2-points
    CHECK(nLoopswLegs.get_xgraphs(1)[3].size()==0);	// 1-node 3-points
    CHECK(nLoopswLegs.get_xgraphs(1)[4].size()==0);	// 1-node 4-points
    CHECK(nLoopswLegs.get_xgraphs(2)[2].size()==39);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_xgraphs(2)[3].size()==246);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_xgraphs(2)[4].size()==212);	// 2-nodes 4-points
    CHECK(nLoopswLegs.get_xgraphs(3)[2].size()==87);	// 3-node 2-points
    CHECK(nLoopswLegs.get_xgraphs(3)[3].size()==1020);	// 3-node 3-points
    CHECK(nLoopswLegs.get_xgraphs(3)[4].size()==1275);	// 3-node 4-points
    CHECK(nLoopswLegs.get_xgraphs(4)[2].size()==78);	// 4-nodes 2-points
    CHECK(nLoopswLegs.get_xgraphs(4)[3].size()==1224);	// 4-nodes 3-points
    CHECK(nLoopswLegs.get_xgraphs(4)[4].size()==1950);	// 4-nodes 4-points
    CHECK(nLoopswLegs.size()==6131);

    // total number of topologies
    CHECK(nLoopswLegs.get_topologies(1)[2].size()==0);	// 1-node 2-points
    CHECK(nLoopswLegs.get_topologies(1)[3].size()==0);	// 1-node 3-points
    CHECK(nLoopswLegs.get_topologies(1)[4].size()==0);	// 1-node 4-points
    CHECK(nLoopswLegs.get_topologies(2)[2].size()==9);	// 2-nodes 2-points
    CHECK(nLoopswLegs.get_topologies(2)[3].size()==26);	// 2-nodes 3-points
    CHECK(nLoopswLegs.get_topologies(2)[4].size()==20);	// 2-nodes 4-points
    CHECK(nLoopswLegs.get_topologies(3)[2].size()==17);	// 3-node 2-points
    CHECK(nLoopswLegs.get_topologies(3)[3].size()==95);	// 3-node 3-points
    CHECK(nLoopswLegs.get_topologies(3)[4].size()==74);	// 3-node 4-points
    CHECK(nLoopswLegs.get_topologies(4)[2].size()==19);	// 4-nodes 2-points
    CHECK(nLoopswLegs.get_topologies(4)[3].size()==115);	// 4-nodes 3-points
    CHECK(nLoopswLegs.get_topologies(4)[4].size()==117);	// 4-nodes 4-points
    CHECK(nLoopswLegs.topology_size()==492);	
}
#endif

