# my_programs


## Purpose

This directory provides an easy way to add files which can be used to play with implementations.
Adding the file *series_example.cpp* to the `my_programs` directory is enough to compile it and link it 
to all the `Caravel` libraries by simply running (in the build directory)

```bash 
$ ninja reconfigure # To make the build system aware of the newly added program
$ ninja my_programs/series_example
```

or alternatively 

```bash 
$ ninja reconfigure # To make the build system aware of the newly added program
$ ninja series_example.r
```

The first version just creates the binary, while the second also executes it. 
The first version has to be used if command-line arguments are to be provided. 
One then first compiles the binary and after that calls it explicitly with the 
the given command-line arguments.

## Programs with MPI dependence

If a program depends on the `MPI` library, a way is needed to tell the build system 
to perform the corresponding linkage. This is done by using the following convention:

Any program depdening on `MPI` should have a name either starting with the prefixes 
**MPI\_** or **mpi\_** or ending in the suffixes **\_MPI** or **\_mpi**. 

## Caution

The build targets in the `my_programs` directory are not built by default. On 
the contrary, the build is only triggered when the user explicitly calls it 
(as described by the method above). 

Hence, the programs are not always compiled and might depend on configuration
settings that are not available in the current build directory. For this reason, 
it is not allowed to commit any files to the `my_programs` directory because these 
files tend to be quickly outdated due to the lack of stringent checks imposed by 
compilation. The *.gitignore* file ignores all files in the `my_programs` directory. 

## Note for developers

We store previous my_programs files in the private branch `feature-my-programs`.