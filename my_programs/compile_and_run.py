#!/usr/bin/env python3

# This script uses the meson environment variables to
# switch to  the build directory, call ninja and compile 
# the program which is specified by a command-line argument.
# After that, it will switch to the my_programs directory
# in the build folder and execute the program.
# In contrast to compiling and running a program directly
# via a run_target where the working directory is 
# unspecified, this approach allows to execute the program
# in a definite build directory which can then also 
# contain the debug.dat file which is read to trigger
# debug output.

import subprocess

import sys
import os
import signal

name = sys.argv[1]

print('Compiling and running program {}'.format(name))
# Change to the build directory
os.chdir(os.environ['MESON_BUILD_ROOT'])
# Compile the program specified by a command line argument
return_value = subprocess.call(['ninja', os.path.join('my_programs', name)])
# If compilation failed and quit, otherwise it continues.
if return_value != 0:
    print("--- Error: Compilation of '{}' failed!".format(name))
    sys.exit(return_value)
# Change to the my_programs directory
os.chdir('my_programs')
# Execute the program
return_value = subprocess.call(['./'+name])
# Check whether a segfault occured and let the user know
# This message is usually printed by the shell but since
# the call is passed through a shell we have to do it 
# ourselves.
if return_value == -signal.SIGSEGV:
    print("Segmentation fault")
if return_value != 0:
    print("Failed to run program '{}'. "
          "Got error code {}".format(name, return_value))
# Let meson know whether the program finished normally.
sys.exit(return_value)

