#!/usr/bin/env python3

import os

for f in os.listdir('.'):
    if os.path.splitext(f)[-1] == '.cpp':
        print(f)
