/// A my_programs example!
/// The build system automatically links the Caravel libraries such that Caravel::Series can be used.

#include <iostream>

#include "Core/Series.h"

int main() {
    // In C++17, the template parameter is inferred from the constructor arguments.    
    Caravel::Series series(-2,0, -2.0, -1., 0.);
    std::cout << "Series: " << series << std::endl;
    return 0;
}
