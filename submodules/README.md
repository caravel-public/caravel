Submodules quick guide
=============================

The single most important fact about git submodules is that a **submodule** in a
git repository is a _reference_ to a specific commit of another (referenced)
_independent_ git repository.

This reference _will not_ automatically track updates from the referenced
repository.  Which is a good thing.

If you go to a checked out submodule, issuing git commands from there will look
like working in the git of the referenced git instead of its parent.

Adding a new submodule
----------------------------

This is very simple, the command is:

```Shell
git submodule add -b [branch name] [repository location] [folder where to put it]
git commit
```

For example:

```Shell
git submodule add -b master git@gitlab.com:caravel-private/surfaceterms.git submodules/SurfaceTerms
git commit
```

This will create a new commit with the information about the added submodule.

**NOTE**: do _not_ put your username in the url address of the repository!  This
might create problems for others.

`-b [branch name]` declares that the module can be updated according to the
remote branch [branch name], see about updating a submodule below


Checking out submodules
----------------------------
When simple

```Shell
git pull
```

is issued submodules are **not** checked out.  The targeted command is

```Shell
git submodule update --init [name]
```

where [name] is the folder for a submodule. If [name] is empty, then all
submodules will be checked out.


To clone the repository with all submodules:

```Shell
git clone --recursive
```


Updating a submodule
----------------------------

Manually update the reference to the currently checked-out commit for a
submodule in [folder of submodule]:

```Shell
git add [folder]
git commit
```

If a tracking branch is configured for a submodule (added with _-b_), the
command

```Shell
git submodule update --remote [folder of submodule]
git commit
```

will move the referenced commit to the current tip of the remote branch.


Good practices
----------------------------

For not advanced users of git it is strongly suggested to use submodules only in
_read-only_ mode.  Which means do not commit and push anything from checked out
submodule.  To develop for the submodule better to have a standalone working
folder for it (like a regular git repository).

There's a nice short
[tutorial](https://medium.com/@porteneuve/mastering-git-submodules-34c65e940407)
on submodules for those who wants to know more.

Most of the submodules in **Caravel** contain scripts and programs which
generate cetain input files and code for **Caravel**, such as process libraries,
integral collections, etc. Those repositories might live there own separate
lives, but it is **strongy advised** that at all times each of them is set to
the commit, which results in the code generated from the submodule reproducing
exactly (perhaps up to timestamp) the code committed to the main repository.

