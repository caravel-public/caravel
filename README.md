# CARAVEL
[![Gitter](https://badges.gitter.im/caravel-amps/community.svg)](https://gitter.im/caravel-amps/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

![Caravel Logo](doxygen/CARAVEL_LOGO.jpg)

***

`Caravel` is a C++ 17 numerical unitarity framework to explore multi-loop
amplitudes in the Standard Model and beyond. To take a peek at results obtained
with it, take a look at the reference section [below](#references).

If you use `Caravel` for a publication, please cite the paper 
["Caravel: A C++ Framework for the Computation of Multi-Loop Amplitudes with Numerical Unitarity"](https://arxiv.org/abs/2009.11957),
corresponding to reference [[8]](#references). 


***

## Developers

Samuel Abreu  
Jerry Dormans  
Fernando Febres Cordero  
Harald Ita  
Manfred Kraus  
Ben Page  
Evgenij Pascual  
Michael Ruf  
Vasily Sotnikov

***

## Table of Contents
- [Install](#install)
- [Examples](#examples)
- [Tests](#tests)
- [Documentation](#documentation)
- [Configuration-Options](#configuration-options)
- [References](#references)
- [Licensing](#licensing)


*** 

## Install

This is a short description on how to install `Caravel`. More details can be
found in the file [INSTALL.md](INSTALL.md).

Note that this guide is written for Unix-based systems. However, it has been used in exactly the same way to install `Caravel` on Windows using WSL.

### Mandatory Dependencies

`Caravel` uses [meson] together with the [ninja] backend as build system.
`meson` is a Python 3 package that can easily be installed via `pip3` by running 

```
$ sudo pip3 install meson
```

to install globally (requires root privileges), or:

```
$ pip3 install --user meson
```

to install it locally for a given user.

Notice that in the latter case, the location of the executable meson script
might have to be added to the __PATH__ environment variable. `ninja` can be
installed from the website in the above link if it is not already available on
your system.  

`Caravel` provides tools for arithmetic in finite fields, rational numbers and
arbitrary-precision floating point, and for that it needs [GMP] as mandatory
dependency (together with [mpfr] >= 4.0.0).


### Optional Dependencies

The optional dependencies for `Caravel` include

* __QD__: [qd] is a library implementing high-precision floating-point types, 
including double-double and quad-double precision.

* __Pentagon library__: A modified version of the library that provides 
numerical access to the so-called pentagon functions required for the numerical
evaluation of the planar two-loop massless five-point integrals computed in
[arXiv:1807.09812].

* __GINAC__ & __CLN__: For the evaluation of *Goncharov polylogarithms*
in high precision, `Caravel` relies on [ginac] which in turn requires [cln] for
high-precision arithmetics.

* __EIGEN__: For linear system solving in floating-point arithmetics we rely on 
[Eigen], especially for *high-precision* floating-point arithmetics.

* __LAPACK__: As an alternative to `Eigen`, [Lapack] can be used to 
perform the inversions of linear systems in double-precision floating-point
arithmetics.

__NOTE__: In the repository [caravel-deps-setup](https://gitlab.com/caravel-public/caravel-deps-setup),
we provide a command-line tool written in `Rust` that takes care of installing all dependencies. 
So far it has been used on `Ubuntu` and `Debian` systems and is known __not__ to work on `macOS`.

### PKGCONFIG

In this section we discuss how meson finds the dependencies on your system since
it might be a bit unusual if you are used to specify the locations of all your
dependencies at configuration time with `autotools`.  

Instead of providing the location of each dependency explicitly, meson finds
them by looking into files of the form *dependency\_name.pc* which include both
the necessary compiler and linker flags. The directories in which meson looks
for these files have to be added to the environment variable
__PKG\_CONFIG\_PATH__, otherwise they will not be found. 

We recommend to install all the dependencies in a common place and in general
the corresponding *.pc* files will appear in
*/path/to/dependencies/lib/pkgconfig* such that it is sufficient to add this
path to the environment as follows (assuming `Bash` shell)

```
$ export PKG_CONFIG_PATH=/path/to/dependencies/lib/pkgconfig:PKG_CONFIG_PATH
```

To make this permanent, consider adding the previous export statement to your
*.bashrc* file.

Notice that this behavior is satisfied by the dependencies *`QD`* (thanks to the
authors for incorporating this upon our request), *`Eigen`*, *`CLN`*, *`GiNaC`*,
*`GMP`* ( >= v.6.2.0) as well as by the custom distribution of the *`pentagon
functions`* that we provide.

Notice that in order for the dependencies to be found at runtime, the location
of the shared libraries needs to be provided in the __LD\_LIBRARY\_PATH__
environment variable. Since this is standard, it is not further elaborated.

### Build & Install

When all the dependencies are available, `Caravel` can be installed.

The first step is to create a build-directory because `meson` does not support
in-source builds. Starting from the top-level `Caravel` directory, this is done
by typing 

```
$ mkdir build
$ cd build
$ meson .. -Dprefix=$PWD -Dbuildtype=release
```

Here we already specified two configuration options (we have identified the
install-directory with the current build-directory using the `prefix` option).
To see all the available options, type

```
$ meson configure
```

If you want to specify a different compiler to the default one, this can be
specified as follows

```
$ CXX=MyCompiler meson .. -Dprefix=$PWD
```

on the first call to `meson` (and the choice will be cached for later use).

After that you can still add individual or several configuration options by
running

```
$ meson configure -D<option1>=<value1> -D<option2>=<value2> ...
```

from the build directory. Unless you want to change individual options, from
this point on, `meson` will never need to be invoked directly anymore, and it
detects all changes in the source code behind the scenes. The final command for
the building of the code will now always be 

```
$ ninja
```

or, since `Caravel` depends on some installed data files, they need to be
installed via

```
$ ninja install
```

By default, `ninja` will make use of all the cores on the machine. If you wish
to use less, run with

```
$ ninja -j <NCORES>
$ ninja -j <NCORES> install
```

__NOTE__: On computers or laptops with restricted RAM it is advised to install
using a limited amount of cores, as the available memory can be easily exhausted
during a multi-threaded compilation.

Visit the [meson] homepage to learn more about this amazing build system.

***

## Examples

The __examples__ directory contains several examples that display the usage of
the library.

As a guide, the file [examples/README.md](examples/README.md) provides a brief
explanation of the purpose of each program and shows how it can be executed. For
more details on the inner workings, we refer to the comments in the program
files.

***

## Tests

The simplest way to run all the tests shipped with `Caravel` is to type

```
$ ninja test
```

which includes both the *unit tests* and the *integration tests*. Since the
latter can take a while to finish, they are separated into several *suites*
whose individual execution can be triggered via

```
$ meson test --suite Caravel:unit_test
```

for the unit tests and 

```
$ meson test --suite Caravel:big_test
$ meson test --suite Caravel:integrals
$ meson test --suite Caravel:full_amplitudes
```

for the long tests. Notice that the first time some long tests are run, warmup
information is produced. Subsequent executions then are considerably faster. 

If the user wants to only run one specific test, this can be achieved by typing

```
$ meson test <testname>
```

where \<testname\> is the name of the test as it is for example shown in the
final output when all tests are run.

For unit-testing we rely on the [Catch2] framework.

***

## Documentation

The code is documented using [doxygen]. An html version of the documentation can
be generated by selecting the corresponding configuration option.

```
$ meson configure -Ddoxygen=true
$ ninja install
$ # Open documentation for example in the Firefox browser
$ firefox doxygen/html/index.html &
```

This requires `doxygen` to be available on your system.

***

## Configuration Options

In this section, we provide more details on some of the configuration options.
All of them can be seen by running 

```bash
$ meson configure
```

in the build directory (after configuration).

Here we give a brief summary for all of them:

* __caravel-debug__: Allow additional debugging output. The debug output of
specific files can be targeted by adding their filenames to a file ``debug.dat``
which has to be located in the directory from which the binary is called.  

* __double-inverter__: Choose between `eigen` or `lapack` to solve linear systems
in double precision. The default is `none` which disables system solving in
double precision. As soon as `-Dprecision-QD=HP`, `-Dprecision-QD=VHP` or
`-Dprecision-QD=all` are set (see below) `eigen` will be automatically set for
double-inversion if `lapack` is not chosen (since it will be linked anyway).

* __doxygen__:      Whether or not to generate the *html* documentation of the
code upon installation.

* __ds-strategy__: Strategy used to extract the Ds dependence of the cuts. The
default is `decomposition` which is more efficient. 

* __field-ext-fermions__: Enables a field extension to evaluate multi-loop
amplitudes with fermions using finite field arithmetic.  Requires
`-Dfinite-fields=true`.

* __finite-fields__: Indicate whether to enable evaluations using finite fields.

* __gravity-model__: Choose the gravity model to compile. The default is `none`
and should only be changed if gravity calculations shall be performed as this
increases compilation times.

* __instantiate-rational__: Perform some some explicit instantiations with
rational types. 

* __integrals__: Choose which integrals to be used. The default value is `none`,
which means that no master integrals are compiled (except for a complete set of
one-loop scalar master integrals). Other choices are `goncharovs` or `pentagons`
to compile the master integrals expressed in terms of Goncharov polylogarithms
or in terms of Pentagon functions, respectively. If set to `all` both versions
will be compiled and can be chosen at runtime.

* __lapack-path__: Allow specifying the path to LAPACK if it is installed in a
non-standard location.

* __precision-QD__: Allow high-precision floating-point evaluation of amplitudes
using types from the `QD` library. Possible options are `HP` for double-double
floating-point arithmetics, `VHP` for quad-double floating-point arithmetics or
`all` for both. This requires [eigen] and [qd]. The default is `none`. 

* __precision-arbitrary__: Perform some explicit instantiations with arbitrary
precision floating-point types.

* __timing__: Allows printing the timing of the different steps in a computation
at the end. This is useful for rough profiling and to see which parts of the
computation take most time. The default is `false`.


***

## References

Few references that have been produced with the usage of `Caravel`!


[1] S. Abreu, F. Febres Cordero, H. Ita, M. Jaquier, B. Page, and M. Zeng,   
    "Two-Loop Four-Gluon Amplitudes with the Numerical Unitarity Method",  
    Phys. Rev. Lett.__119__, 142001 (2017), [1703.05273].

[2] S. Abreu, F. Febres Cordero, H. Ita, M. Jaquier, and B. Page,   
    "Subleading Poles in the Numerical Unitarity Method at Two Loops",  
    Phys. Rev.D95, 096011 (2017), [1703.05255].

[3] S. Abreu, F. Febres Cordero, H. Ita, B. Page, and M. Zeng,  
    "Planar Two-Loop Five-Gluon Amplitudes from Numerical Unitarity",  
    Phys. Rev.D97, 116014 (2018), [1712.03946]  

[4] S. Abreu, F. Febres Cordero, H. Ita, B. Page, and V. Sotnikov,  
    "Planar Two-Loop Five-Parton Amplitudes from Numerical Unitarity",  
    JHEP 11, 116 (2018), [1809.09067].

[5] S. Abreu, J. Dormans, F. Febres Cordero, H. Ita, and B. Page,  
    "Analytic Form of the Planar Two-Loop Five-Gluon Scattering Amplitudes in QCD",  
    Phys. Rev. Lett. __122__ (2019) 082002, [1812.04586].

[6] S. Abreu, J. Dormans, F. Febres Cordero, H. Ita, B. Page and V. Sotnikov,  
    "Analytic Form of the Planar Two-Loop Five-Parton Scattering Amplitudes in QCD",  
    JHEP 1905 (2019) 084,  [1904.00945]

[7] S. Abreu, F. Febres Cordero, H. Ita, M. Jaquier, B. Page, M. S. Ruf, V. Sotnikov,  
    "The Two-Loop Four-Graviton Scattering Amplitudes",  
    Phys. Rev. Lett. __124__ (2020), 211601, [2002.12374]

[8] S. Abreu, J. Dormans, F. Febres Cordero, H. Ita, M. Kraus, B. Page, E. Pascual,
    M. S. Ruf, V. Sotnikov,  
    "Caravel: A C++ Framework for the Computation of Multi-Loop Amplitudes with Numerical Unitarity",  
    Computer Physics Communications, Volume 267, 2021, 108069, [2009.11957]

[9] S. Abreu, B. Page, E. Pascual, V. Sotnikov,  
    "Leading-Color Two-Loop QCD Corrections for Three-Photon Production at Hadron Colliders",  
    JHEP 01 (2021) 078,  [2010.15834]

[10] S. Abreu, F. Febres Cordero, H. Ita, B. Page, V. Sotnikov,  
    "Leading-Color Two-Loop QCD Corrections for Three-Jet Production at Hadron Colliders",  
    JHEP 07 (2021) 095, [2102.13609]

[11] S. Abreu, F. Febres Cordero, H. Ita, M. Klinkert, B. Page, V. Sotnikov,  
    "Leading-Color Two-Loop Amplitudes for Four Partons and a W Boson in QCD",  
    JHEP 04 (2022) 042, [2110.07541]

[12] F. Febres Cordero, M. Kraus, G. Lin, M. S. Ruf, M. Zeng,  
    "Conservative Binary Dynamics with a Spinning Black Hole at $`\mathcal{O}(G^3)`$ from Scattering Amplitudes",  
    [2205.07357]

***

## Licensing
`Caravel` is distributed under the terms of the GNU General Public License
(version 3). See [LICENSE](LICENSE).


[meson]: https://mesonbuild.com/  
[ninja]: https://ninja-build.org/  
[qd]: http://crd-legacy.lbl.gov/~dhbailey/mpdist/  
[doxygen]: http://doxygen.nl/
[arXiv:1807.09812]: https://arxiv.org/abs/1807.09812
[ginac]: https://ginac.de/
[cln]: https://www.ginac.de/CLN/
[Catch2]: https://github.com/catchorg/Catch2
[Eigen]: http://eigen.tuxfamily.org/index.php?title=Main_Page
[Lapack]: http://www.netlib.org/lapack/
[GMP]: https://gmplib.org/
[mpfr]: https://www.mpfr.org/


[1703.05255]: https://arxiv.org/abs/1703.05255
[1703.05273]: https://arxiv.org/abs/1703.05273
[1712.03946]: https://arxiv.org/abs/1712.03946
[1809.09067]: https://arxiv.org/abs/1809.09067
[1812.04586]: https://arxiv.org/abs/1812.04586
[1904.00945]: https://arxiv.org/abs/1904.00945
[2002.12374]: https://arxiv.org/abs/2002.12374
[2009.11957]: https://arxiv.org/abs/2009.11957
[2010.15834]: https://arxiv.org/abs/2010.15834
[2102.13609]: https://arxiv.org/abs/2102.13609
[2110.07541]: https://arxiv.org/abs/2110.07541
[2205.07357]: https://arxiv.org/abs/2205.07357

