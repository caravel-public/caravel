#!/bin/sh

filename="$2"

cd "$1"
if [ ! -f "$filename" ]; then
    echo "File $filename found!"
    exit 1;
fi
tar -xzvf "$filename"
rm -f "$filename"
