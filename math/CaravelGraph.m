BeginPackage["CaravelGraph`"];

Leg::usage = "
    Leg[i]
        external leg with momentum i
    Leg[i,mi]
        external leg with momentum i and mass index mi (mi=0 is reserved for massless)
";

Link::usage = "
    Link[]
        represents a massless propagator
    Link[mi]
        represents a propagator with mass index mi
    Link[LoopMomentum[i]]
        represents a massless propagator which carries a loop momentum i
    Link[LoopMomentum[i],mi]
        represents a propagator with mass index mi which carries a loop momentum i
"

LoopMomentum::usage = "
    LoopMomentum[i]
        represents a loop momentum with index i. The sign of i show the direction inside the Strand.
"

Node::usage = "
    Represents an abstract for nodes of the graph which connect >=3 internal links.
    Node[]
        an empty node
    Node[leg1,leg2,...]
        a node with legs attached to it
"

Bead::usage = "
    Same as node, but connects only exactly 2 internal links. Cannot be empy.
"

Strand::usage = "
    Strand[link1,bead1,link2,bead2,...,linkN]
        represents a Strand --- a consecutive list of Link and Bead, starting and ending with Link.
    Strand[] === Strand[Link[]]
        represents an Strand consisting of just one Link.

    One or zero links in a Strand can carry a LoopMomentum.
    If a Bead inside Strand consists of a single Leg, then Bead can be omitted.
"

Connection::usage = "
    Connection[Strand1,Strand2,...]
        represents a list of strands connecting two nodes
"

Nodes::usage = "
    Nodes[Node1,Node2,...]
        represents a list of nodes
"

CaravelGraph::usage = "
    CaravelGraph[nodes,connection]
        represents a graph with nodes, connected by a connection.
        In this form only one or two nodes make sense. Then the single connection
        either connects the single node on itself, or connects from the first to the second node.
    CaravelGraph[nodes,connection,name]
        give the graph a custom name
"

getMathGraph::usage = "Obtain a Mathematica Graph from CaravelGraph in a representation which gets drawn nicely (most of the time).";


Begin["`Private`"];

Bead[] := (Print["Empty Bead encountered!"]; Abort[]);
Leg[] := (Print["Empty Leg encountered!"]; Abort[]);


fullForm[g_CaravelGraph] := With[
    {
        nodes = FirstCase[g, _Nodes],
        strands = Replace[Strand[] -> Strand[Link[]]] /@ FirstCase[g, _Connection]
    },
    CaravelGraph[
        Replace[l_Leg :> Node[l]] /@ nodes,
        Map[Replace[{l_LoopMomentum :> Link[l], l_Leg :> Bead[l]}], strands, {2}]
    ]
];


strandToEdgeList[s_Strand, Nodes[nf_, nl_]] := With[
    { all = Partition[{nf, Sequence @@ s, nl}, 3, 2] },
    Replace[{
        {a_, Link[]|Link[0], b_} :> UndirectedEdge[a, b],
        {a_, Link[mi_?IntegerQ], b_} :> Labeled[Style[UndirectedEdge[a, b],Thick], Subscript[Global`m, mi]],
        {a_, Link[LoopMomentum[i_],Optional[0,0]], b_} :> Labeled[DirectedEdge @@ If[i > 0, {a, b}, {b, a}], Subscript[Global`\[ScriptL], Abs[i]]],
        {a_, Link[LoopMomentum[i_], mi_?Positive], b_} :> Labeled[Style[DirectedEdge @@ If[i > 0, {a, b}, {b, a}],Thick], Subscript[Global`\[ScriptL], Abs[i]] Text["," Subscript[Global`m, mi]]]
    }] /@ all
];

strandToEdgeList[s_Strand, Nodes[n_]] := strandToEdgeList[s, Nodes[n, n]];


externalEdge[v_,l:Leg[i_?Positive,mi_?Positive]] := Style[UndirectedEdge[v,l],Thick] ;
externalEdge[v_,l:Leg[i_?Positive,0]] :=Style[UndirectedEdge[v,l],Small];
externalEdge[v_,l:Leg[i_?Positive]] :=Style[UndirectedEdge[v,l],Small];

getMathGraph[g_CaravelGraph] := With[
    {gin = fullForm[g]},
    {
        nodes = Block[{i = 100}, Replace[Node[] :> Node[ind$[i++]]] /@ FirstCase[gin, _Nodes]],
        strands = FirstCase[gin, _Connection]
    },
   {edgeList = Join[
      Flatten[List @@ (strandToEdgeList[#, nodes] & /@ strands)],
      (Sequence @@ Map[Function[x, externalEdge[#, x]], #]) & /@ Cases[gin, _Bead, Infinity],
       Flatten[{Cases[nodes, n : Node[_Leg ..] :> (List @@ (externalEdge[n, #] & /@ n))]}]
      ]
    },
    {vertexList = Sort[VertexList[ Graph[edgeList]], ((#1 /. Leg[i_, ___] :> i) < (#2 /.  Leg[i_, ___] :> i)) &]},
    Graph[
        vertexList,
        edgeList,
        VertexShape -> ((# -> Null) & /@ Cases[gin, _Leg, Infinity]),
        VertexLabels -> ((# -> (# /. {Leg[i_] :> Style[i, Bold], Leg[i_, 0] :> i, Leg[i_, os_] :> Style[i, Bold] Text["," Subscript[Global`m, os]]})) & /@ Cases[gin, _Leg, Infinity]),
        EdgeLabelStyle -> Directive[14],
        VertexLabelStyle -> Directive[14],
        VertexStyle -> List @@ ((# -> Red) & /@ nodes)
    ]
];

End[];

Protect["CaravelGraph`*"];
Protect["CaravelGraph`Private`*"];

EndPackage[];
