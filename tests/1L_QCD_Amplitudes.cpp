#include <map>
#include "Core/momD_conf.h"
#include "Core/Particle.h"

#include "Core/typedefs.h"
#include "Core/Utilities.h"
#include "Core/settings.h"

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "AmpEng/CoefficientEngine.h"
#include "IntegralLibrary/IntegralProvider.h"

#include "Amplitude.h"

#include "misc/TestUtilities.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>
#include <algorithm>

#include "Core/CaravelInput.h"


using namespace std;
using namespace Caravel;

//#define GENERATE_TARGETS_MODE

int main(){
    
    using F = F32;
    using T = CHP;

    Particle p("g",ParticleType(ParticleStatistics::vector, ParticleFlavor::gluon, ParticleMass(), true),SingleState("p"));
    Particle m("g",ParticleType(ParticleStatistics::vector, ParticleFlavor::gluon, ParticleMass(), true),SingleState("m"));
    Particle u(("u"),ParticleType(ParticleStatistics::fermion, ParticleFlavor::u, ParticleMass(), true),SingleState("qbp"));
    Particle ub(("ub"),ParticleType(ParticleStatistics::antifermion, ParticleFlavor::ub, ParticleMass(), true),SingleState("qm"));
    Particle d(("d"),ParticleType(ParticleStatistics::fermion, ParticleFlavor::d, ParticleMass(), true),SingleState("qbp"));
    Particle db(("db"),ParticleType(ParticleStatistics::antifermion, ParticleFlavor::db, ParticleMass(), true),SingleState("qm"));
    Particle dP(("d"),ParticleType(ParticleStatistics::fermion, ParticleFlavor::d, ParticleMass(), true),SingleState("qm"));
    Particle dbP(("db"),ParticleType(ParticleStatistics::antifermion, ParticleFlavor::db, ParticleMass(), true),SingleState("qbp"));

    std::vector<std::vector<Particle>> input = {
        {p,p,m,m},
        {m,p,m,p,p},
#ifdef USE_EXTENDED_T
        {u,ub,p,m},
        {u,ub,d,db},
        {u,ub,dP,dbP},
        {u,ub,p,m,p},
        {u,ub,d,db,p},
        {u,ub,dP,dbP,m},
#endif
    };

    std::vector<PartialAmplitudeInput> amplitudes;

    // add Nf=0 and Nf=1
    for (auto& it : input) { amplitudes.emplace_back(it, ColourStructureInput(0,0)); }

#ifdef USE_EXTENDED_T
    for (auto& it : input) { amplitudes.emplace_back(it, ColourStructureInput(1,0)); }
#endif

    // targets are in the order amplitudes are added to input
#ifdef GENERATE_TARGETS_MODE
    std::vector<std::map<int, remove_complex_t<T>>> targets; 
#else
    std::vector<std::map<int, remove_complex_t<T>>> targets = {{{-2, "-3.999999999999999999999999559926e+00"},
                                                                {-1, "-1.482985386208104148869954798079e+01"},
                                                                {0, "-2.150563509925665191478975326584e+01"},
                                                                {1, "-4.242972632442786340856599792420e+00"},
                                                                {2, "3.945669987183274583987368517735e+01"}},
                                                               {{-2, "-4.999999999999999999999999999978e+00"},
                                                                {-1, "-1.788291385858927033013499735192e+01"},
                                                                {0, "-2.950855172731836065191469946436e+01"},
                                                                {1, "-3.492963560973223682686756637698e+01"}},
                                                               {{-2, "-2.999999999999999999999999999829e+00"},
                                                                {-1, "-1.042169654089483596222228600300e+01"},
                                                                {0, "-1.375537910453304095780294519761e+01"},
                                                                {1, "-2.227311547459992881533126020424e+00"},
                                                                {2, "1.567564906776633960431805388393e+01"}},
                                                               {{-2, "-1.999999999999999999999999999936e+00"},
                                                                {-1, "-6.013539219708630435745020828541e+00"},
                                                                {0, "-1.512122299775771744403706087621e+00"},
                                                                {1, "2.296961380469672504900477040467e+01"},
                                                                {2, "5.755706218267703216605531885359e+01"}},
                                                               {{-2, "-1.999999999999999999999918777905e+00"},
                                                                {-1, "-6.013539219708630435726093833566e+00"},
                                                                {0, "4.503971305116845178917403995485e+00"},
                                                                {1, "5.527734017096133086350651599868e+01"},
                                                                {2, "1.563375209150049906330076200475e+02"}},
                                                               {{-2, "-3.999999999999999999999999999431e+00"},
                                                                {-1, "-1.376243860985484573109695100733e+01"},
                                                                {0, "-1.797203102977551377584464020121e+01"},
                                                                {1, "1.496892270737453036408556984488e+00"}},
                                                               {{-2, "-3.000000000000000000000000000348e+00"},
                                                                {-1, "-8.843501369786224286743904113530e+00"},
                                                                {0, "-4.411871382385792089015751562434e+00"},
                                                                {1, "2.632328221051362307007007243240e+01"}},
                                                               {{-2, "-3.000000000000000000000000058344e+00"},
                                                                {-1, "-8.843501369786224286744030816816e+00"},
                                                                {0, "3.429945174448623146202935029671e+02"},
                                                                {1, "1.000539159518347752704938531810e+03"}},
                                                               {{-2, "0"},
                                                                {-1, "6.666666666666666666666662895011e-01"},
                                                                {0, "3.337846406569543478581672622675e+00"},
                                                                {1, "7.778113385923395057047709803084e+00"},
                                                                {2, "9.642499788167797628347916631057e+00"}},
                                                               {{-2, "0"},
                                                                {-1, "6.666666666666666666666664299702e-01"},
                                                                {0, "3.475701079798337951815594635819e+00"},
                                                                {1, "8.982161551319729163935828138094e+00"}},
                                                               {{-2, "0"},
                                                                {-1, "0"},
                                                                {0, "0"},
                                                                {1, "0"},
                                                                {2, "0"}},
                                                               {{-2, "0.000000000000000000000000000000e+00"},
                                                                {-1, "-6.666666666666666666666686186236e-01"},
                                                                {0, "-2.605438214124137017651518107053e+00"},
                                                                {1, "-5.691068008245852010677441185534e+00"},
                                                                {2, "-8.728233618583174153930025362614e+00"}},
                                                               {{-2, "0.000000000000000000000000000000e+00"},
                                                                {-1, "-6.666666666666666666666565386912e-01"},
                                                                {0, "-2.605438214124137017651469187102e+00"},
                                                                {1, "-5.691068008245852010677329672644e+00"},
                                                                {2, "-8.728233618583174153929844221928e+00"}},
                                                               {{-2, "0"},
                                                                {-1, "0"},
                                                                {0, "-8.015520164338514408040266007823e-04"},
                                                                {1, "-4.344237791059403642789780834942e-03"},
                                                                {2, "-1.257682159274598300309051173828e-02"}},
                                                               {{-2, "0.000000000000000000000000000000e+00"},
                                                                {-1, "-6.666666666666666666666666666730e-01"},
                                                                {0, "-2.933154493914605121586388334073e+00"},
                                                                {1, "-7.055606900019308616091176176646e+00"},
                                                                {2, "-1.186563785680416150216865803804e+01"}},
                                                               {{-2, "0.000000000000000000000000000000e+00"},
                                                                {-1, "-6.666666666666666666666665683347e-01"},
                                                                {0, "-5.749865762022069948153834769783e+01"},
                                                                {1, "-2.592491529529248542983379150945e+02"},
                                                                {2, "-6.684609807502444941930773123043e+02"}}};
#endif

    settings::use_setting("integrals::integral_family_1L goncharovs");
    //settings::use_setting("integrals::integral_family_2L pentagon_functions");
    //settings::use_setting("IntegrandHierarchy::amplitude_normalization tree");
    settings::use_setting("BG::partial_trace_normalization full_Ds");


    SM_color_ordered model2use;

    size_t n_of_errors = 0;

    for (size_t i__=0; i__<amplitudes.size();++i__) {
        const auto& ampl = amplitudes[i__];
        const size_t nparticles = ampl.get_multiplicity();

        // standard paper points
        std::vector<BigRat> xs;
        switch (nparticles) {
            case 4: xs = {BigRat(-3, 4), BigRat(-1, 4)}; break;
            case 5: xs = {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17), BigRat(-1)}; break;
            default: _WARNING_RED(nparticles); std::exit(1);
        }

        auto physical_point = get_mom_conf<T>(nparticles, xs);

        _MESSAGE("\n\nAmplitude\t",ampl);

        AmpEng::CoefficientEngine<T> coefficient_provider(std::make_shared<Model>(model2use), ampl);
        if(!coefficient_provider.has_warmup()) {
            AmpEng::CoefficientEngine<F> cprovider(std::make_shared<Model>(model2use), ampl);
            std::vector<F> lxs;
            size_t nn(4);
            if(xs.size() == 2) lxs = {F(524137), F(87215673)};
            else { lxs = {F(524137), F(87215673), F(221245), F(98776138), F(343)}; nn = 5; }
            auto momc = rational_mom_conf<F>(nn, lxs);
            cprovider.compute(momc);
            coefficient_provider.reload_warmup();
        }

        Amplitude<T, decltype(coefficient_provider), Integrals::sIntegralProvider<T>> one_loop_ampl(std::move(coefficient_provider));
        // we set to 4 the maximum, such that when combined with the integrals it ranges from -2 to 2
        one_loop_ampl.set_laurent_range(-2,4);

        //evaluate coefficients floating point

        START_TIMER_T(evaluation,CHP)
        // now get the amplitude
        auto result = one_loop_ampl.compute(physical_point);
        auto coeffsRat = one_loop_ampl.coefficients.compute(physical_point);
        STOP_TIMER(evaluation)

        std::vector<T> coeffs(coeffsRat.size());
        std::transform(coeffsRat.begin(), coeffsRat.end(), coeffs.begin(), [](const DenseRational<T>& c) { return c(T(0)); });

        //evlaute exact coefficients
        {
            AmpEng::CoefficientEngine<F> coefficient_provider(std::make_shared<Model>(model2use), ampl);
            std::function<std::vector<F>(const momentumD_configuration<F, 4>&)> coeff_function = [&coefficient_provider](const auto& ps) {
                auto resRat = coefficient_provider.compute(ps);
                std::vector<F> toret(resRat.size());
                std::transform(resRat.begin(), resRat.end(), toret.begin(), [](const DenseRational<F>& c) { return c(F(0)); });
                return toret;
            };

            std::vector<BigRat> rat_result;
            auto rational_point = ps_parameterizations::rational_mom_conf<BigRat>(nparticles, xs, std::vector<size_t>(), true);

            START_TIMER_T(evaluation, BigRat);
            RationalReconstruction::rational_reconstruct(rational_point, coeff_function, rat_result);
            STOP_TIMER(evaluation);

            //_MESSAGE("----");
            //_MESSAGE("Check that exact and floating point coefficients match.");
            //_MESSAGE("----");
            //_MESSAGE("Relative errors of coefficients in floating point vs Rational:");

            typename remove_complex<T>::type worst_relative_error_coefficients{-1};

            auto rel_err = [](auto x, auto y) {
                using std::abs;
                using result_type = decltype((abs(x - y)));
                if (abs(x) > DeltaZero<result_type>() and abs(y) > DeltaZero<result_type>()) { return static_cast<result_type>(2) * abs(x - y) / abs(x + y); }
                else {
                    return abs(x - y);
                }
            };

            auto cast_coeffs = to_precision<T>(rat_result);
            for (auto& c : cast_coeffs) c *= add_complex_t<T>{0, -1};

            for (unsigned i = 0; i < rat_result.size(); i++) {
                cout << scientific;
                auto e = rel_err(cast_coeffs.at(i), coeffs.at(i));
                if (e > worst_relative_error_coefficients) worst_relative_error_coefficients = e;
                if (e < typename remove_complex<T>::type{1e-10}) { /*_MESSAGE("coeff ", i, ": ", e);*/ }
                else {
                    _MESSAGE(Color::Code::FG_RED, "coeff ", i, ": ", e);
                    _MESSAGE("\t", "FP: ", coeffs.at(i), " != ", cast_coeffs.at(i), " :Rational");
                    ++n_of_errors;
                }
            }

            _PRINT(worst_relative_error_coefficients);

            //_PRINT(coeffs);
            //_PRINT(cast_coeffs);
        }


        result *= one_loop_norm_caravel_to_physical<T>;
        result *= add_complex_t<T>{0,1};

        if (ampl.get_nf_powers() == 1) result *= T(-1);

        _MESSAGE("ampl = ", result);

#ifndef GENERATE_TARGETS_MODE
        const auto& target = targets.at(i__);

        {
            int max_power = std::min(2,result.last());

            //FIXME: we check only until eps^1 for these because integrals are not implemented yet
            if(ampl.get_multiplicity()==5 ) max_power =1;

            for (int i = result.leading(); i <= max_power; ++i) {
                if (check(result[i].real(), target.at(i), 10.)) { _MESSAGE("eps^", i, ":\t\t", Color::Code::FG_GREEN, "PASS", Color::Code::FG_DEFAULT); }
                else {
                    ++n_of_errors;
                    _MESSAGE("eps^", i, ":\t\t", Color::Code::FG_RED, "FAIL", Color::Code::FG_DEFAULT);
                    _MESSAGE("\t\t\t>", result[i].real(), " != target ", target.at(i), "; acc = ", compute_accuracy(result[i].real(), target.at(i)));
                }
            }
        }
#else
        typename decltype(targets)::value_type to_push;
        for(int i = -2; i <= 2; ++i){
            if (i <= result.last()) to_push[i] = result[i].real();
        }
        targets.push_back(to_push);
        // just a vim macro to quote the copy-pasted output
        //:'<,'>s/\([0-9\.\-]\+e[+\-][0-9]\+\)/"\1"/g
#endif
    }

#ifdef GENERATE_TARGETS_MODE
    cout << scientific;
    cout << setprecision(30);
    _PRINT(targets);
#endif

    return n_of_errors;
}


