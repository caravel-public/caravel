#include <map>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "AmpEng/CoefficientEngine.h"
#include "IntegralLibrary/IntegralProvider.h"
#include "Amplitude.h"

#include "Core/typedefs.h"
#include "Core/momD_conf.h"
#include "Core/Particle.h"

#include "Core/extend.h"

#include "misc/TestUtilities.hpp"

#define SMALL_VALUE 1e-5

using namespace std;
using namespace Caravel;

int main(void) {

  settings::use_setting("BG::partial_trace_normalization full_Ds");

  Particle u1(("u1"),ParticleType(ParticleStatistics::fermion, ParticleFlavor::u, ParticleMass(), true),SingleState("qm"),1,0);
  Particle y2("y2p",ParticleType("photon"),SingleState("p"),2,0);
  Particle y4("y4m",ParticleType("photon"),SingleState("m"),4,0);
  Particle y5("y5p",ParticleType("photon"),SingleState("p"),5,0);
  Particle sg2("sg2p",ParticleType("gluon"),SingleState("p"),2,0);
  Particle sg4("sg4m",ParticleType("gluon"),SingleState("m"),4,0);
  Particle sg5("sg5p",ParticleType("gluon"),SingleState("p"),5,0);
  Particle g2("g2p",ParticleType("gluon"),SingleState("p"),2,0);
  Particle g4("g4m",ParticleType("gluon"),SingleState("m"),4,0);
  Particle g5("g5p",ParticleType("gluon"),SingleState("p"),5,0);
  Particle ub3(("ub3"),ParticleType(ParticleStatistics::antifermion, ParticleFlavor::ub, ParticleMass(), true),SingleState("qbp"),3,0);
  Particle d4(("d4"),ParticleType(ParticleStatistics::fermion, ParticleFlavor::d, ParticleMass(), true),SingleState("qm"),4,0);
  Particle db5(("db5"),ParticleType(ParticleStatistics::antifermion, ParticleFlavor::db, ParticleMass(), true),SingleState("qbp"),5,0);

  Particle gl("gl",ParticleType(ParticleStatistics::vector,
				ParticleFlavor::gluon, ParticleMass(),
				true),SingleState("default"));
  Particle ul("ul",ParticleType(ParticleStatistics::fermion,
				ParticleFlavor::u, ParticleMass(),
				true),SingleState("default"));
  Particle dl("dl",ParticleType(ParticleStatistics::fermion,
				ParticleFlavor::d, ParticleMass(),
				true),SingleState("default"));
  Particle ubl("ubl",ParticleType(ParticleStatistics::antifermion,
				  ParticleFlavor::ub, ParticleMass(),
				  true),SingleState("default"));

  vector<vector<vector<Particle>>>External_photons
    {{{u1},{y2},{ub3},{d4},{db5}},
	{{u1},{y2},{ub3},{g4},{g5}},
	    {{ub3},{y2},{y4},{y5},{u1}}};
  vector<vector<vector<vector<Particle>>>>External_gluons
    {{{{u1},{sg2},{ub3},{d4},{db5}},
	  {{u1},{ub3},{d4},{sg2},{db5}}},
	{{{u1},{sg2},{ub3},{g4},{g5}}},
	  {{{ub3},{sg2},{sg4},{sg5},{u1}},
	      {{ub3},{sg2},{sg5},{sg4},{u1}},
		{{ub3},{sg4},{sg2},{sg5},{u1}},
		  {{ub3},{sg4},{sg5},{sg2},{u1}},
		    {{ub3},{sg5},{sg2},{sg4},{u1}},
		      {{ub3},{sg5},{sg4},{sg2},{u1}}}};
  vector<vector<Particle>> Internal_photons
    {{gl,ul,ul,gl,dl},
	{gl,ul,ul,gl,gl},
	    {gl,ubl,ubl,ubl,ubl}};
  vector<vector<vector<Particle>>> Internal_gluons
    {{{gl,ul,ul,gl,dl},
	  {gl,ul,gl,dl,dl}},
	{{gl,ul,ul,gl,gl}},
	    {{gl,ubl,ubl,ubl,ubl},
		{gl,ubl,ubl,ubl,ubl},
		  {gl,ubl,ubl,ubl,ubl},
		    {gl,ubl,ubl,ubl,ubl},
		      {gl,ubl,ubl,ubl,ubl},
			{gl,ubl,ubl,ubl,ubl}}};
  vector<map<string,int>> tree_couplings_photons
  {{{"gs",2},{"gw",1}},
      {{"gs",2},{"gw",1}},
	  {{"gw",3}}},
    tree_couplings_gluons
      {{{"gs",3}},
	  {{"gs",3}},
	      {{"gs",3}}};
    map<string,int> v_coupsW{{"gw",1}},v_coupsS{{"gs",1}};
    vector<vector<map<string,int>>> v_couplings_photons
      {{v_coupsS,v_coupsW,v_coupsS,v_coupsS,v_coupsS},
	  {v_coupsS,v_coupsW,v_coupsS,v_coupsS,v_coupsS},
	      {v_coupsS,v_coupsW,v_coupsW,v_coupsW,v_coupsS}},
      v_couplings_gluons
	{{v_coupsS,v_coupsS,v_coupsS,v_coupsS,v_coupsS},
	    {v_coupsS,v_coupsS,v_coupsS,v_coupsS,v_coupsS},
		{v_coupsS,v_coupsS,v_coupsS,v_coupsS,v_coupsS}};
      //Because of missing left/righthanded -1 factor at three photons
      vector<C>factor{C(2),C(2),C(-8)};
      
      settings::use_setting("IntegrandHierarchy::amplitude_normalization none");

      SM_color_ordered model2use;
      
#include "misc/std_momenta.hpp"
      momD_conf<C,4>momconf_process(mcs_4D[1]);

      for(size_t i=0;i<Internal_photons.size();++i) {
	//size_t nparticles(0);
	//for(auto&v:External_photons[i])nparticles+=v.size();
	Amplitude<C, AmpEng::CoefficientEngine<C>, Integrals::sIntegralProvider<C> >
	  ampl_photons(AmpEng::CoefficientEngine<C>(std::make_shared<Model>(model2use),
                                 AmpEng::ParentDiagrams{{External_photons[i]},
							{Internal_photons[i]},
                                                        {v_couplings_photons[i]},
							 tree_couplings_photons[i]}));
        if(!ampl_photons.coefficients.has_warmup()) {
	    AmpEng::CoefficientEngine<F32> coeffs_photons(AmpEng::CoefficientEngine<F32>(std::make_shared<Model>(model2use),
                                 AmpEng::ParentDiagrams{{External_photons[i]},
							{Internal_photons[i]},
                                                        {v_couplings_photons[i]},
							 tree_couplings_photons[i]}));
            std::vector<F32> xs;
            if(External_photons[i].size() == 4) xs = {F32(524137), F32(87215673)};
            else xs = {F32(524137), F32(87215673), F32(221245), F32(98776138), F32(343)};
            auto momc = rational_mom_conf<F32>(External_photons[i].size(), xs);
            coeffs_photons.compute(momc);
            ampl_photons.coefficients.reload_warmup();
        }
	ampl_photons.set_laurent_range(-2,3);    
	Series<C> Series_photons(-2,2),Series_gluons(-2,2);
	Series_photons = ampl_photons.compute(momconf_process);
	for(size_t j=0;j<External_gluons[i].size();++j) {
	  Amplitude<C, AmpEng::CoefficientEngine<C>, Integrals::sIntegralProvider<C> >
	    ampl_gluons(AmpEng::CoefficientEngine<C>(std::make_shared<Model>(model2use),
                                  AmpEng::ParentDiagrams{{External_gluons[i][j]},
							 {Internal_gluons[i][j]},
							 {v_couplings_gluons[i]},
							  tree_couplings_gluons[i]}));
        if(!ampl_gluons.coefficients.has_warmup()) {
	    AmpEng::CoefficientEngine<F32> coeffs_gluons(AmpEng::CoefficientEngine<F32>(std::make_shared<Model>(model2use),
                                 AmpEng::ParentDiagrams{{External_gluons[i][j]},
							{Internal_gluons[i][j]},
                                                        {v_couplings_gluons[i]},
							 tree_couplings_gluons[i]}));
            std::vector<F32> xs;
            if(External_photons[i].size() == 4) xs = {F32(524137), F32(87215673)};
            else xs = {F32(524137), F32(87215673), F32(221245), F32(98776138), F32(343)};
            auto momc = rational_mom_conf<F32>(External_photons[i].size(), xs);
            coeffs_gluons.compute(momc);
            ampl_gluons.coefficients.reload_warmup();
        }
	  ampl_gluons.set_laurent_range(-2,3);    
	  if(!j) Series_gluons=ampl_gluons.compute(momconf_process);
	  else Series_gluons+=ampl_gluons.compute(momconf_process);
	}
	for(int j=Series_photons.leading();j<Series_photons.last();++j)
	  if(abs(Series_photons[j]-factor[i]*Series_gluons[j])>SMALL_VALUE)
	    return 1;

      }
    
      return 0;
}
