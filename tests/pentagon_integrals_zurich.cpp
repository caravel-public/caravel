#include <vector>
#include "pentagon/header.h"
#include "misc/TestUtilities.hpp"
#include <iomanip>
#include <string>
#include <time.h>
#include <fstream>
#include <sstream>


std::vector<std::complex<double> > evaluate_pentagonfunctions(double v[5], double error);
std::vector<std::vector<std::vector<std::complex<double> > > > evaluate_pentagonintegrals(std::vector<std::complex<double> > pentagonfunctions);

int main (void)
{

    using namespace std;

    double v[5];
    v[0] = 0.9876543209876;  v[1] = 0.790123456790; v[2] = -1.80267859636; v[3] = 3.16049382716; v[4] = -2.211585663744;

   // set the integration error of the pentagon functions' evaluation
    double error = 1e-11;
    double error_check = 10;

    vector<complex<double> > pentagonfunctions = evaluate_pentagonfunctions(v, error);
    vector<vector<vector<complex<double> > > > pentagonintegrals = evaluate_pentagonintegrals(pentagonfunctions);

    if (pentagonintegrals.size() != 61 && pentagonintegrals[0].size() != 5 and pentagonintegrals[0][0].size() != 5){
        return 1;
    }
    else {
        // Check the first entry
        if(!Caravel::check(pentagonintegrals[0][0][0],complex<double>(-1), error_check)){
            return 1;
        }
        if(!Caravel::check(pentagonintegrals[0][1][0], complex<double>(-0.4711321426258, -6.28318530718), error_check)) {
            return 1;
        }
        if(!Caravel::check(pentagonintegrals[0][2][0], complex<double>(21.27316012112, -2.960210556287), error_check)) {
            return 1;
        }
        if(!Caravel::check(pentagonintegrals[0][3][0], complex<double>(22.87926816853, 50.9798026295), error_check)){
            return 1;
        }
        if(!Caravel::check(pentagonintegrals[0][4][0], complex<double>(-73.57393867175, 104.7998720829), error_check)){
            return 1;
        }


    }


   return 0;
}
