#include <vector>
#include <string>

#include "Core/settings.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "FunctionalReconstruction/DenseRational.h"
#include "FunctionalReconstruction/PeraroFunction.h"
#include "IntegralLibrary/FunctionBasisExpansion.h"
#include "FunctionalReconstruction/ReconstructionTools.h"

#include "IntegralLibrary/IntegralProvider.h"

#include "misc/TestUtilities.hpp"

using namespace std;
using namespace Caravel;
using namespace Caravel::Integrals;

int main(int argc, char* argv[]) {

    // Run on just two threads not to overload CPU too much but still allowing to
    // see multithreading in action.
    size_t num_threads = 2;

    using std::string;
    using std::vector;
    const size_t L = 2;

    // External process, put in order of momentum indices, this is important.
    std::vector<Particle> process = {
        Particle("g", ParticleType("gluon"), SingleState("m"), 1),
        Particle("g", ParticleType("gluon"), SingleState("m"), 2),
        Particle("g", ParticleType("gluon"), SingleState("p"), 3),
        Particle("g", ParticleType("gluon"), SingleState("p"), 4),
    };

    settings::use_setting("IntegrandHierarchy::amplitude_normalization tree");
    settings::use_setting("general::master_basis alternative");
    settings::use_setting("integrals::integral_family_2L goncharovs");

    // Model to be used

    YM model;

    using F = F32;
    

    // Create a unique_ptr of the amplitude to pass to the input provider for the functional reconstruction.
    auto ampl = std::make_unique<IntegrandHierarchy::ColourExpandedAmplitude<F, L>>(model, PartialAmplitudeInput{process});

    // Construct a rational physical point.
    auto physical_point1 = rational_mom_conf<BigRat>(
        4, std::vector<BigRat>{BigRat(-237) / BigRat(57), BigRat(-139) / BigRat(67)});

    momentumD_configuration<F, 4> tmp1 = static_cast<momentumD_configuration<F, 4>>(physical_point1);

    // The input provider for the rational/functional reconstruction.

    ampl->warmup_run(tmp1);
    sIntegralProvider<F> integrals( ampl->get_integral_graphs() );

    auto basic_function = [&](const momD_conf<F, 4>& conf) {
        auto result_integrals = integrals.get_abstract_integral(conf);
        auto result_coeffs =  to_Series(ampl->compute(conf));
        return std::inner_product(result_coeffs.begin(),result_coeffs.end(),result_integrals.begin(),decltype((result_coeffs[0]*result_integrals[0])){});
    };

    MapToBasis<F32, StdBasisFunction<C, C>> mapper;
    // Only reconstruct the Laurent series expansion coefficients of weight one functions.
    std::vector<std::string> funcs_to_reconstruct = {"T[1]"};
    ReconstructionInputProvider<F, 2> input_provider(basic_function, std::move(mapper), tmp1, funcs_to_reconstruct);

    cout << "\n\nConstructed input provider!\n\n" << endl;

    // Determine the positions of the weight zero terms for the different powers in epsilon of the output.
    auto test_point =
        rational_mom_conf<BigRat>(4, std::vector<BigRat>{BigRat(-3) / BigRat(4), BigRat(-1) / BigRat(4)});

    momentumD_configuration<F, 4> test_pt = static_cast<momentumD_configuration<F, 4>>(physical_point1);

    // Find the positions of the weight zero Laurent series expansion coefficients.
    std::size_t eps_m4_w0 = input_provider.get_position_of("T[1]", -4);
    std::size_t eps_m3_w0 = input_provider.get_position_of("T[1]", -3);
    std::size_t eps_m2_w0 = input_provider.get_position_of("T[1]", -2);
    std::size_t eps_m1_w0 = input_provider.get_position_of("T[1]", -1);
    std::size_t eps_m0_w0 = input_provider.get_position_of("T[1]", 0);

    std::cout << "Position of ep^-4 coefficient of weight 0: " << eps_m4_w0 << std::endl;
    std::cout << "Position of ep^-3 coefficient of weight 0: " << eps_m3_w0 << std::endl;
    std::cout << "Position of ep^-2 coefficient of weight 0: " << eps_m2_w0 << std::endl;
    std::cout << "Position of ep^-1 coefficient of weight 0: " << eps_m1_w0 << std::endl;
    std::cout << "Position of ep^-0 coefficient of weight 0: " << eps_m0_w0 << std::endl;

    std::mt19937 mt1(rand());
    std::function<F()> x_gen = [mt1]() mutable { return random_scaled_number(F(1), mt1); };

    vector<function<vector<F>(F)>> functions;
    {
        for (size_t i = 0; i < num_threads; i++) {
            auto thread_function = [thread_ampl = ampl->get_new_master_coefficients_evaluator(), thread_integrals = integrals](const momD_conf<F, 4>& conf) {
                auto result_integrals = thread_integrals.get_abstract_integral(conf);
                auto result_coeffs = to_Series(thread_ampl(conf).front());
                return std::inner_product(result_coeffs.begin(),result_coeffs.end(),result_integrals.begin(),decltype((result_coeffs[0]*result_integrals[0])){});
            };

            auto new_evaluator = input_provider.get_new_reconstruction_input_evaluator(thread_function);

            functions.push_back([ev = std::move(new_evaluator)](F x) {
                auto point = rational_mom_conf<F>(4, std::vector<F>{F(1), x});
                return ev(point);
            });
        }
    }

    VectorThieleFunction<F> TF(std::move(functions), x_gen, "t", true);
    cout << "\nNumber of fitted functions: " << TF.components.size() << endl;

    int toreturn = 0;

    using RationalReconstruction::rational_guess;

    // Check ep^-4 coefficient
    _MESSAGE("Compute ep^-4 coefficient weight zero part to be: ",
             rational_guess(TF.components[eps_m4_w0].to_canonical().numerator.coefficients[0]));
    if (rational_guess(TF.components[eps_m4_w0].to_canonical().numerator.coefficients[0]) != BigRat(8)) {
        _MESSAGE("Weight zero term in the ep^-4 coefficient is incorrect. Test failed.");
        toreturn = 1;
    }

    // Check ep^-3 coefficient
    _MESSAGE("Compute ep^-3 coefficient weight zero part to be: ",
             rational_guess(TF.components[eps_m3_w0].to_canonical().numerator.coefficients[0]));
    if (rational_guess(TF.components[eps_m3_w0].to_canonical().numerator.coefficients[0]) != BigRat(11)) {
        _MESSAGE("Weight zero term in the ep^-3 coefficient is incorrect. Test failed.");
        toreturn = 1;
    }

    // Check ep^-2 coefficient
    _MESSAGE("Compute ep^-2 coefficient weight zero part to be: ",
             rational_guess(TF.components[eps_m2_w0].to_canonical().numerator.coefficients[0]));
    if (rational_guess(TF.components[eps_m2_w0].to_canonical().numerator.coefficients[0]) !=
        BigRat(67) / BigRat(3)) {
        _MESSAGE("Weight zero term in the ep^-2 coefficient is incorrect. Test failed.");
        toreturn = 1;
    }

    // Check ep^-1 coefficient
    _MESSAGE("Compute ep^-1 coefficient weight zero part to be: ",
             rational_guess(TF.components[eps_m1_w0].to_canonical().numerator.coefficients[0]));
    if (rational_guess(TF.components[eps_m1_w0].to_canonical().numerator.coefficients[0]) !=
        BigRat(359) / BigRat(9)) {
        _MESSAGE("Weight zero term in the ep^-1 coefficient is incorrect. Test failed.");
        toreturn = 1;
    }

    // Check ep^0 coefficient
    _MESSAGE("Compute ep^-4 coefficient weight zero t^0 part to be: ",
             rational_guess(TF.components[eps_m0_w0].to_canonical().numerator.coefficients[0]));
    if (rational_guess(TF.components[eps_m0_w0].to_canonical().numerator.coefficients[0]) !=
        BigRat(2777) / BigRat(54)) {
        _MESSAGE("Weight zero term in the ep^0 coefficient is incorrect. Test failed.");
        toreturn = 1;
    }

    _MESSAGE("Compute ep^-4 coefficient weight zero t^1 part to be: ",
             rational_guess(TF.components[4].to_canonical().numerator.coefficients[1]));
    if (rational_guess(TF.components[4].to_canonical().numerator.coefficients[1]) != BigRat(50) / BigRat(9)) {
        _MESSAGE("Weight zero term in the ep^0 coefficient is incorrect. Test failed.");
        toreturn = 1;
    }

    _MESSAGE("Compute ep^-4 coefficient weight zero t^2 part to be: ",
             rational_guess(TF.components[4].to_canonical().numerator.coefficients[2]));
    if (rational_guess(TF.components[4].to_canonical().numerator.coefficients[2]) != BigRat(1) / BigRat(9)) {
        _MESSAGE("Weight zero term in the ep^0 coefficient is incorrect. Test failed.");
        toreturn = 1;
    }

    return toreturn;
}
