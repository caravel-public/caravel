#include <cstdlib>

#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "Core/typedefs.h"
#include "Core/CaravelInput.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "AmpEng/CoefficientEngine.h"
#include "IntegralsBH/ProviderBH.h"
#include "Amplitude.h"
#include "misc/TestUtilities.hpp"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"

using namespace std;
using namespace Caravel;
using namespace AmpEng;
using namespace IntegralsBH;
using namespace IntegrandHierarchy;

template <typename T> struct proc {
    vector<vector<Particle>> procs; // List of processes to be summed (Same particles, different helicities)
    vector<unsigned> mass_indices;  // mass indices of ext. particles
    int nl, nh;                     // Either light or heavy fermion loop
    bool norm;                      // Normalize by tree
    T tree;                         // Reference tree value from massive BlackHat
    map<int, T> loop;               // Reference one-loop values from massive BlackHat (normalized by tree if non-zero)
};

template <typename T> momD_conf<T,4> get_psp(unsigned N, vector<unsigned> m);
momD_conf<F32, 4> get_random_psp(unsigned N, vector<unsigned> m);
template <typename T> vector<proc<T>> get_inputs();

int main(){
    using T = CVHP;

    // Top-quark Mass has to be mT = 171.2 GeV for this test
    Particle Top("top", ParticleType("t"), SingleState("Qp"));
    ParticleMass::set_container_mass( Top.get_mass_index(), 1712, 10);

    settings::use_setting("integrals::integral_family_1L integrals_BH");
    settings::use_setting("BG::partial_trace_normalization full_Ds");
    settings::use_setting("IntegrandHierarchy::amplitude_normalization none");

    SM_color_ordered model;
    unsigned n_of_errors = 0;
    vector<proc<T>> processes = get_inputs<T>();
    const double tree_tol = 60.;
    const double loop_tol = 50.;

    for (auto p : processes) {
        T tree = T(0);
        Series<T> loop = Series<T>(-2, 0, T(0), T(0), T(0));
        auto physical_point = get_psp<T>(p.procs[0].size(), p.mass_indices);

        // Compute matrix elements
        for (unsigned n = 0; n < p.procs.size(); n++) {
            PartialAmplitudeInput ampl = PartialAmplitudeInput(p.procs[n], ColourStructureInput(p.nl, p.nh));
            _MESSAGE("\n\nAmplitude\t", ampl);

            CoefficientEngine<T> coefficient_provider(std::make_shared<Model>(model), ampl);
            if(!coefficient_provider.has_warmup()){
                CoefficientEngine<F32> coefficient_providerFF(std::make_shared<Model>(model), ampl);
                auto physical_pointFF =  get_random_psp(p.procs[0].size(), p.mass_indices);
                coefficient_providerFF.compute(physical_pointFF);
                coefficient_provider.reload_warmup();
            }
            Amplitude<T, decltype(coefficient_provider), ProviderBH<T>> one_loop_ampl(std::move(coefficient_provider));
            // we set to 4 the maximum, such that when combined with the integrals it ranges from -2 to 2
            one_loop_ampl.set_laurent_range(-2, 4);

            // evaluate coefficients floating point
            // now get the amplitude
            auto result = one_loop_ampl.compute(physical_point);
            //one_loop_ampl.coefficients.show_coefficients();
            //auto integrals = one_loop_ampl.integrals.compute(physical_point);
            //for(auto s : integrals) std::cout << s << std::endl;

            // *(-1) for closed fermion loops and *I for different BH and Caravel normalisation
            if (ampl.get_nf_powers() == 1 || ampl.get_nh_powers() == 1) result *= T(-1);
            result *= add_complex_t<T>{0, 1};

            // Sum tree and loop results
            tree += one_loop_ampl.coefficients.get_tree(physical_point);
            loop += result;
        }

        // Normalize by tree
        if (p.norm) loop /= tree;

        // Check tree
        if (check(tree, p.tree, tree_tol)) { _MESSAGE("Tree:\t\t", Color::Code::FG_GREEN, "PASS", Color::Code::FG_DEFAULT); }
        else {
            n_of_errors++;
            _MESSAGE("Tree:\t\t", Color::Code::FG_RED, "FAIL", Color::Code::FG_DEFAULT);
            _MESSAGE("\t\t\t>", tree, " != target ", p.tree, "; acc = ", compute_accuracy(tree, p.tree));
        }

        // Check the loop [normalized to tree!]
        for (int i = loop.leading(); i <= 0; i++) {
            if (check(loop[i], p.loop.at(i), loop_tol)) { _MESSAGE("eps^", i, ":\t\t", Color::Code::FG_GREEN, "PASS", Color::Code::FG_DEFAULT); }
            else {
                n_of_errors++;
                _MESSAGE("eps^", i, ":\t\t", Color::Code::FG_RED, "FAIL", Color::Code::FG_DEFAULT);
                _MESSAGE("\t\t\t>", loop[i], " != target ", p.loop.at(i), "; acc = ", compute_accuracy(loop[i], p.loop.at(i)));
            }
        }
    }

    std::cout << "------------------------------------" << std::endl;
    std::cout << "Total number of errors: " << n_of_errors << std::endl;
    std::cout << "------------------------------------" << std::endl;
    return n_of_errors;
}

/**
 * Here we list all proceses that should be computed
 * For top-quark processes, we sum over ext. polarisation 
 */
template <typename T> vector<proc<T>> get_inputs() {
    vector<proc<T>> input(17);

    // Particle content
    // gluons
    Particle p("gp", ParticleType("gluon"), SingleState("p"));
    Particle m("gm", ParticleType("gluon"), SingleState("m"));
    // Light quarks
    Particle qbp("qbp", ParticleType("qb"), SingleState("qbp"));
    Particle qm("qm", ParticleType("q"), SingleState("qm"));
    Particle ubp("ubp", ParticleType("ub"), SingleState("qbp"));
    Particle um("um", ParticleType("u"), SingleState("qm"));
    Particle dbp("dbp", ParticleType("db"), SingleState("qbp"));
    Particle dm("dm", ParticleType("d"), SingleState("qm"));
    // Heavy quarks
    Particle Qbp("Qbp", ParticleType("tb"), SingleState("Qbp"));
    Particle Qbm("Qbm", ParticleType("tb"), SingleState("Qbm"));
    Particle Qp("Qp", ParticleType("t"), SingleState("Qp"));
    Particle Qm("Qm", ParticleType("t"), SingleState("Qm"));

    // LIST OF PROCEESES
    const T GLOBAL_FACTOR = T(-1);

    // (g-,g-,g+,g+) nl
    input[0].procs.push_back(vector<Particle>{m, m, p, p});
    input[0].mass_indices = vector<unsigned>{0, 0, 0, 0};
    input[0].nl = 1;
    input[0].nh = 0;
    input[0].norm = true;
    input[0].tree =
        T("-1.280241820588577882167342789798060560308683265890926428893168470e+00", "-5.248797206938590602608660053873611465097133972748163513959685906e-01");
    input[0].loop = {{-2, T("0")},
                     {-1, T("6.666666666666666666666666666666666666666666666666666666666666675e-01")},
                     {0, T("-7.882740657381749696237991960369145279962414024428538049208095009e+00")}};

    // (g-,g-,g+,g+) nh
    input[1].procs.push_back(vector<Particle>{m, m, p, p});
    input[1].mass_indices = vector<unsigned>{0, 0, 0, 0};
    input[1].nl = 0;
    input[1].nh = 1;
    input[1].norm = true;
    input[1].tree =
        T("-1.280241820588577882167342789798060560308683265890926428893168470e+00", "-5.248797206938590602608660053873611465097133972748163513959685906e-01");
    input[1].loop = {{-2, T("0")},
                     {-1, T("6.666666666666666666666666666666666666666666666666666666666666185e-01")},
                     {0, T("-8.287862081431992277203365449123623847747389529591203119709460177e+00",
                           "5.370119261967059624954006561121331194032594845669497033834916943e-01")}};

    // (qb+,q-,g+,g+) nh
    input[2].procs.push_back(vector<Particle>{qbp, qm, p, p});
    input[2].mass_indices = vector<unsigned>{0, 0, 0, 0};
    input[2].nl = 0;
    input[2].nh = 1;
    input[2].norm = false;
    input[2].tree = T(0);
    input[2].loop = {{-2, T("0")},
                     {-1, T("0")},
                     {0, GLOBAL_FACTOR * T("1.610265098423993738289360847828688775356376292520607403132374590e-01",
                                           "-6.058670163925473239987230057926755599624186612859540070455563716e-02")}};

    // (g-,g+,g-,g+,g+) nl
    input[3].procs.push_back(vector<Particle>{m, p, m, p, p});
    input[3].mass_indices = vector<unsigned>{0, 0, 0, 0, 0};
    input[3].nl = 1;
    input[3].nh = 0;
    input[3].norm = true;
    input[3].tree =
        T("-6.690914272398665456720139162405738758188746705275743653544526786e-04", "1.267732245958348908886648734476607647911265966225801406096417515e-04");
    input[3].loop = {{-2, T("0")},
                     {-1, T("6.666666666666666666666666666666666666666666666666666666666688537e-01")},
                     {0, T("-7.735437324204863963091195193033208038163206042754591617587366644e+00",
                           "9.187208252581035318064237859016157768898830558147898091440136697e-01")}};

    // (g-,g+,g-,g+,g+) nh
    input[4].procs.push_back(vector<Particle>{m, p, m, p, p});
    input[4].mass_indices = vector<unsigned>{0, 0, 0, 0, 0};
    input[4].nl = 0;
    input[4].nh = 1;
    input[4].norm = true;
    input[4].tree =
        T("-6.690914272398665456720139162405738758188746705275743653544526786e-04", "1.267732245958348908886648734476607647911265966225801406096417515e-04");
    input[4].loop = {{-2, T("0")},
                     {-1, T("6.666666666666666666666666666666666666666666666666666666667269467e-01")},
                     {0, T("-7.597961126765727845471705946168847499286632618948130516373599177e+00",
                           "8.685399212501835666810147735528164130041834711502078132798252280e-01")}};

    // (ub+,u-,db+,d-,g-) nh
    input[5].procs.push_back(vector<Particle>{ubp, um, dbp, dm, m});
    input[5].mass_indices = vector<unsigned>{0, 0, 0, 0, 0};
    input[5].nl = 0;
    input[5].nh = 1;
    input[5].norm = true;
    input[5].tree =
        T("8.482272943685355005398879418479566547291187685411675033492648408e-04", "1.286699251241369221020701875140651447052939300481024547101145174e-03");
    input[5].loop = {{-2, T("0")},
                     {-1, T("-6.666666666666666666666666666666666666666666666666666666666666670e-01")},
                     {0, T("8.012992756861974453884344972216282452303646123784755782226307441e+00",
                           "-2.019419949029663998697352146823938328885430723670679539272963320e+00")}};

    // (g-,g-,g+,g+,g+,g+) nh
    input[6].procs.push_back(vector<Particle>{m, m, p, p, p, p});
    input[6].mass_indices = vector<unsigned>{0, 0, 0, 0, 0, 0};
    input[6].nl = 0;
    input[6].nh = 1;
    input[6].norm = true;
    input[6].tree =
        T("8.642042648698544444343190379285417846285297276093573348522502646e-05", "-1.562598954035514525617398292802706022755156081010688447536551173e-06");
    input[6].loop = {{-2, T("0")},
                     {-1, T("6.666666666666666666666666666666666666666666666666666666666666942e-01")},
                     {0, T("-8.223865042781958920592375507601725374634610549855642840146199185e+00",
                           "7.867204089499648273356851656350052889784620239522123719388267473e-01")}};

    // (Q+,g+,g-,Qb+) nh
    input[7].procs.push_back(vector<Particle>{Qp, p, m, Qbp});
    input[7].mass_indices = vector<unsigned>{1, 0, 0, 1};
    input[7].nl = 0;
    input[7].nh = 1;
    input[7].norm = false;
    input[7].tree = T("6.127050871646533364500610883114444127205513033249096185046193706e-03",
                      "4.231972105165099077638681699686875279477541514829186258583632546e-02"); //++
    input[7].loop = {{-2, T("0")}, {-1, T("0")}, {0, T("0")}};

    // (Q-,g+,g-,Qb+) nh
    input[8].procs.push_back(vector<Particle>{Qm, p, m, Qbp});
    input[8].mass_indices = vector<unsigned>{1, 0, 0, 1};
    input[8].nl = 0;
    input[8].nh = 1;
    input[8].norm = false;
    input[8].tree = T("8.969586174181414336100628947187919896485419145122132511272634222e-01",
                      "-3.254417378866231333596095224340475851621426814120118116459030804e-01"); //-+
    input[8].loop = {{-2, T("0")}, {-1, T("0")}, {0, T("0")}};

    // (Q+,g+,g-,Qb-) nh
    input[9].procs.push_back(vector<Particle>{Qp, p, m, Qbm});
    input[9].mass_indices = vector<unsigned>{1, 0, 0, 1};
    input[9].nl = 0;
    input[9].nh = 1;
    input[9].norm = false;
    input[9].tree = T("-4.346611356230420373536870576918973241881131539141577066527546702e-02",
                      "4.053947033996940507858719707469643862331642454931091812115889832e-01"); // +-
    input[9].loop = {{-2, T("0")}, {-1, T("0")}, {0, T("0")}};

    // (Q-,g+,g-,Qb-) nh
    input[10].procs.push_back(vector<Particle>{Qm, p, m, Qbm});
    input[10].mass_indices = vector<unsigned>{1, 0, 0, 1};
    input[10].nl = 0;
    input[10].nh = 1;
    input[10].norm = false;
    input[10].tree = T("4.693526807089790631071269166907796625217244071344090971747587385e-01",
                      "2.313487957747971557674082666281902223634171865064104547473744383e-01"); // --
    input[10].loop = {{-2, T("0")}, {-1, T("0")}, {0, T("0")}};

    // (Q+,g+,g+,Qb+) nh
    input[11].procs.push_back(vector<Particle>{Qp, p, p, Qbp});
    input[11].mass_indices = vector<unsigned>{1, 0, 0, 1};
    input[11].nl = 0;
    input[11].nh = 1;
    input[11].norm = false;
    input[11].tree = T("0");
    input[11].loop = {{-2, T("0")},
                      {-1, T("0")},
                      {0, T("7.760969922745215342972947976520810239469047664679061402899263943e-03",
                            "-3.897532313798319698027529274848823427056817785357917793103185752e-02")}};
    // (Q+,g+,g+,Qb-) nh
    input[12].procs.push_back(vector<Particle>{Qp, p, p, Qbm});
    input[12].mass_indices = vector<unsigned>{1, 0, 0, 1};
    input[12].nl = 0;
    input[12].nh = 1;
    input[12].norm = true;
    input[12].tree =
        T("-3.713470883279914971515425879079781834599746539383881490237019498e-02", "4.642042870962257843631839240681655214590668002954061464702058163e-02");
    input[12].loop = {{-2, T("0")},
                      {-1, T("0")},
                      {0, T("-3.502021820339466569213765100090001434048290009712575552117680139e-01",
                            "2.837209796255808531454493913789222419723208418954889914533428107e+00")}};
    // (Q-,g+,g+,Qb+) nh
    input[13].procs.push_back(vector<Particle>{Qm, p, p, Qbp});
    input[13].mass_indices = vector<unsigned>{1, 0, 0, 1};
    input[13].nl = 0;
    input[13].nh = 1;
    input[13].norm = true;
    input[13].tree =
        T("7.905982757136410075162661866480671266915567668233374025253248882e-02", "-1.515727731471030285510051808110950968517886991740680416127288931e-01");
    input[13].loop = {{-2, T("0")},
                      {-1, T("0")},
                      {0, T("9.544144240124455826353911051470481378427482220909434628898873454e-01",
                           "-2.780207319416350557803384827955407699662886149999033983797670696e-01")}};

    // (Q-,g+,g+,Qb-) nh
    input[14].procs.push_back(vector<Particle>{Qm, p, p, Qbm});
    input[14].mass_indices = vector<unsigned>{1, 0, 0, 1};
    input[14].nl = 0;
    input[14].nh = 1;
    input[14].norm = true;
    input[14].tree =
        T("-5.403600012735647265103218442196467408952494469854560978447653695e-01", "-4.071440954313015923996731890219553449690507414267224122376117292e-02");
    input[14].loop = {{-2, T("0")},
                      {-1, T("0")},
                      {0, T("-5.498520046416145704037805727672370004418620622655805337518140628e-02",
                            "-4.852721131718600795541759819438504297091800910154781382530150591e-02")}};

    // (Q-,g-,g+,g-,g+,Qb+) nl
    input[15].procs.push_back(vector<Particle>{Qm, m, p, m, p, Qbp});
    input[15].mass_indices = vector<unsigned>{1, 0, 0, 0, 0, 1};
    input[15].nl = 1;
    input[15].nh = 0;
    input[15].norm = true;
    input[15].tree =
        T("-3.523268083453157222988922683557967353034523652292700274391040839e-07", "1.982935852732785013499135563669685938724114705705184249310178571e-06");
    input[15].loop = {{-2, T("0")},
                      {-1, T("0")},
                      {0, T("-2.306069069373200772361497309355630757893105087055285521768116068e+00",
                            "1.313934759753651363308348327343586943315319902708764499637560453e+00")}};

    // (Q-,g-,g+,g-,g+,Qb+) nh
    input[16].procs.push_back(vector<Particle>{Qm, m, p, m, p, Qbp});
    input[16].mass_indices = vector<unsigned>{1, 0, 0, 0, 0, 1};
    input[16].nl = 0;
    input[16].nh = 1;
    input[16].norm = true;
    input[16].tree =
        T("-3.523268083453157222988922683557967353034523652292700274391040839e-07", "1.982935852732785013499135563669685938724114705705184249310178571e-06");
    input[16].loop = {{-2, T("0")},
                      {-1, T("0")},
                      {0, T("-8.247031685528881175622326729532445514214538861585670729603602282e-01",
                            "2.611627677142829672094145089525941454088812093548881459596753356e+00")}};
    return input;
}

/**
 * The Phase space points to be used 
 */
template <typename T> momD_conf<T, 4> get_psp(unsigned N, vector<unsigned> m) {
    vector<momD<T, 4>> momenta(N);
    switch (N) {
        case 4: {
            if (m == vector<unsigned>{0, 0, 0, 0}) {
                momenta[0] = momD<T, 4>(T("-5.000000000000000719459451937554007297551820346936947816654907631e+02"),
                                        T("4.226276601435619132766601663222183938581272215533794436468847944e+02"),
                                        T("2.289789388905022003695858836248409200108393652379113166806835766e+02"),
                                        T("-1.376753660832527437201524451781297019306138028733835095934702128e+02"));
                momenta[1] = momD<T, 4>(T("-5.000000000000000989099882824057122461509640207712463898264603282e+02"),
                                        T("-4.226276601435619405598034039498868917292376512270649958902257737e+02"),
                                        T("-2.289789388905022269567727464621671160569951002115702512647310574e+02"),
                                        T("1.376753660832527136748698563545949385238144861305710005299667367e+02"));
                momenta[2] = momD<T, 4>(T("5.000000000000000758226801856038398874981006367403136392924122751e+02"),
                                        T("-4.274559005043950002268387124095533640598827860657989530652049309e+02"),
                                        T("2.243973235109920045730406404567471220342009261404060267974389529e+02"),
                                        T("-1.301049358213593407367930612460111250552287075387416783411848922e+02"));
                momenta[3] = momD<T, 4>(T("5.000000000000000950332532905572730884080454187246275321995388163e+02"),
                                        T("4.274559005043950275099819500372218619309932157394845053085459102e+02"),
                                        T("-2.243973235109919779858537776194209259880451911667470922133914722e+02"),
                                        T("1.301049358213593707820756500695458884620280242815541874046883682e+02"));
                break;
            }
            else if (m == vector<unsigned>{1, 0, 0, 1}) {
                momenta[0] = momD<T, 4>(T("5.000000000000000218862511730769717287799311642263194400183817836e+02"),
                                        T("-4.016180196776823556079503592884300361782661421843005074434133843e+02"),
                                        T("2.108334651202930554527049743360441121319697072937996617043910771e+02"),
                                        T("-1.222406489493038550370355324719141007214923884703797202064106041e+02"));
                momenta[1] = momD<T, 4>(T("-5.000000000000000719459451937554007297551820346936947816654907631e+02"),
                                        T("4.226276601435619132766601663222183938581272215533794436468847944e+02"),
                                        T("2.289789388905022003695858836248409200108393652379113166806835766e+02"),
                                        T("-1.376753660832527437201524451781297019306138028733835095934702128e+02"));
                momenta[2] = momD<T, 4>(T("-5.000000000000001756219016148340371722045732875984410441226585508e+02"),
                                        T("-4.226276601435621026674713218776907904623450348093220075230992877e+02"),
                                        T("-2.289789388905020949897776630159843921740149927101283338447166875e+02"),
                                        T("1.376753660832527141283626436455090653112641046215407947118442184e+02"));
                momenta[3] = momD<T, 4>(T("5.000000000000002256815956355124661731798241580658163857697675303e+02"),
                                        T("4.016180196776825449987615148439024327824839554402430713196278776e+02"),
                                        T("-2.108334651202931608325131949449006399687940798215826445403579662e+02"),
                                        T("1.222406489493038846288253340045347373408420867222224350880365985e+02"));
                break;
            }
            cout << "Mass configuration m = " << m << " not implemented!" << endl;
            exit(1);
            break;
        }
        case 5: {
            if (m == vector<unsigned>{0, 0, 0, 0, 0}) {
                momenta[0] = momD<T, 4>(T("-5.000000000000000719459451937554007297551820346936947816654907631e+02"),
                                        T("4.226276601435619132766601663222183938581272215533794436468847944e+02"),
                                        T("2.289789388905022003695858836248409200108393652379113166806835766e+02"),
                                        T("-1.376753660832527437201524451781297019306138028733835095934702128e+02"));
                momenta[1] = momD<T, 4>(T("-5.000000000000000989099882824057122461509640207712463898264603282e+02"),
                                        T("-4.226276601435619405598034039498868917292376512270649958902257737e+02"),
                                        T("-2.289789388905022269567727464621671160569951002115702512647310574e+02"),
                                        T("1.376753660832527136748698563545949385238144861305710005299667367e+02"));
                momenta[2] = momD<T, 4>(T("4.808104625620141903950691882624342324780029976118065585411615048e+02"),
                                        T("-4.291920707157374420714193486318028901950272016890439944619695112e+02"),
                                        T("2.052304915957412385144151045264176173369895393251341854726210734e+02"),
                                        T("-6.966572086188346519528779811215797734105886186694388370783947995e+01"));
                momenta[3] = momD<T, 4>(T("2.981245312331848160419483039256090541394926726008257958055132445e+02"),
                                        T("2.183971431588198023479427116567884321625639480489411482422723440e+02"),
                                        T("-1.628701020681665934284881200530031552883987371187298309306286126e+02"),
                                        T("1.210547555256593529400380759316565585439876947195027420313282110e+02"));
                momenta[4] = momD<T, 4>(T("2.210650062048011644189159839730696892886503852523088171452763418e+02"),
                                        T("2.107949275569176670066198746026829559035736833137883984630381465e+02"),
                                        T("-4.236038952757461849874012163608826600243506723274541995794498006e+01"),
                                        T("-5.138903466377585769946768899596381779612951610974634925998525487e+01"));
                break;
            }
            cout << "Mass configuration m = " << m << " not implemented!" << endl;
            exit(1);
            break;
        }
        case 6: {
            if (m == vector<unsigned>{0, 0, 0, 0, 0, 0}) {
                momenta[0] = momD<T, 4>(T("-5.000000000000000719459451937554007297551820346936947816654907631e+02"),
                                        T("4.226276601435619132766601663222183938581272215533794436468847944e+02"),
                                        T("2.289789388905022003695858836248409200108393652379113166806835766e+02"),
                                        T("-1.376753660832527437201524451781297019306138028733835095934702128e+02"));
                momenta[1] = momD<T, 4>(T("-5.000000000000000989099882824057122461509640207712463898264603282e+02"),
                                        T("-4.226276601435619405598034039498868917292376512270649958902257737e+02"),
                                        T("-2.289789388905022269567727464621671160569951002115702512647310574e+02"),
                                        T("1.376753660832527136748698563545949385238144861305710005299667367e+02"));
                momenta[2] = momD<T, 4>(T("3.549457003977863612462520541610949904476240309891367582713982251e+02"),
                                        T("-3.512137070370991492940196178781801345941474293026482260664938861e+02"),
                                        T("2.071964485143200633118096477396827480586433614223667456599557724e+01"),
                                        T("-4.696891032762348101774018011428221623602798757069756206922948568e+01"));
                momenta[3] = momD<T, 4>(T("2.542855179645937016545238114345602792179093711352647994651575772e+02"),
                                        T("1.599201516095334189547412828600232715136749915371047398092267666e+02"),
                                        T("-1.762343110047311654775305385554209379401825564754584810744572621e+02"),
                                        T("8.959987377442695719882924898475155013904418887406008355906144452e+01"));
                momenta[4] = momD<T, 4>(T("1.673271639757778318880484244021784775168352521802148278205091499e+02"),
                                        T("1.500324311973931447434402568838778580938940141612146782768273963e+02"),
                                        T("-6.401915914817035140985072741533665286708570760293446372837513674e+01"),
                                        T("-3.728534102214396051516635794548956732686040214537871439260061180e+01"));
                momenta[5] = momD<T, 4>(T("2.234416176618422760671091861632792287237774011603247859348861390e+02"),
                                        T("4.126112423017261287898131576194750285768885327801436022378070254e+01"),
                                        T("2.195338253014695371433871640341155120475596629098152048208843023e+02"),
                                        T("-5.345622424659485620640122101445003169356482415171298033727870945e+00"));
                break;
            }
            else if (m == vector<unsigned>{1, 0, 0, 0, 0, 1}) {
                momenta[0] = momD<T, 4>(T("-5.146547200000000082877703998501635432139025928409658042743183090e+02"),
                                        T("4.102406800962437502764369667062258479556628903729129751932028056e+02"),
                                        T("2.222676944198273362324544354976280369732902342069419834621352037e+02"),
                                        T("-1.336401782015575637033536308345349031355307044610936808157381354e+02"));
                momenta[1] = momD<T, 4>(T("-4.853452800000000692383268939319492824638128025290241863862411922e+02"),
                                        T("-4.102406800962438059748179427514400081824665744191674252461937700e+02"),
                                        T("-2.222676944198273771271091473151491835202924559523576903091561519e+02"),
                                        T("1.336401782015575442470243648427312367905841248810521884263094808e+02"));
                momenta[2] = momD<T, 4>(T("3.332999368992301130325741804416455850372609971129188047085604584e+02"),
                                        T("-3.297955328446623621179534781065576773200932483765706378781024100e+02"),
                                        T("1.945609233698950772937848495218237503126909091812565019523340037e+01"),
                                        T("-4.410459073283122923993858588060019051351516671418431928610874625e+01"));
                momenta[3] = momD<T, 4>(T("2.387783455244121635216996421819347183549721298640664645773334235e+02"),
                                        T("1.501676915106680387870109771542762331663559370454913516833815590e+02"),
                                        T("-1.654869594744427930974494113812104364795932485373037372478575990e+02"),
                                        T("8.413577694201515599193755407596971273777513144817035667757422479e+01"));
                momenta[4] = momD<T, 4>(T("1.571229997494053077166124876742800226692211443392742764889586603e+02"),
                                        T("1.408829570125456009022099286174912615775297985801606661954371975e+02"),
                                        T("-6.011505895271866036206507014739977305348753698166720436703920359e+01"),
                                        T("-3.501155753124971628467877200531478833431120921374589979956252867e+01"));
                momenta[5] = momD<T, 4>(T("2.707987178269524932552109834842524996162611240537304448857069591e+02"),
                                        T("3.874488432144877812711354838000434280301119679717307005227461781e+01"),
                                        T("2.061459260901719866247907083939489810488139163462609982666843504e+02"),
                                        T("-5.019628677934191010990930198251067545002175940198645202474295210e+00"));
                break;
            }
            cout << "Mass configuration m = " << m << " not implemented!" << endl;
            exit(1);
            break;
        }
        default: _MESSAGE("Number of final state momenta N = ", N, " not implemented!"); exit(1);
    }

    momD_conf<T, 4> physical_point = momD_conf<T, 4>(momenta);
    //for (auto s : physical_point) cout << s << "\t p^2 = " << s * s << endl;
    return physical_point;
}

momD_conf<F32, 4> get_random_psp(unsigned N, vector<unsigned> m) {
    vector<F32> xs;
    for (size_t ii = 0; ii < 3 * N - 10; ii++) xs.push_back(F32(std::rand()));
    vector<size_t> mm;
    for (auto i : m) mm.push_back(size_t(i));
    vector<size_t> zero(mm.size(), 0);
    if (zero == mm)
        return rational_mom_conf(N, xs, {});
    else
        return rational_mom_conf(N, xs, mm);
}

