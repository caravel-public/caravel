#include <map>
#include "Core/momD_conf.h"
#include "Core/Particle.h"

#include "Core/typedefs.h"
#include "Core/Utilities.h"
#include "Core/settings.h"

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "AmpEng/CoefficientEngine.h"
#include "IntegralsBH/ProviderBH.h"

#include "Amplitude.h"

#include "misc/TestUtilities.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>
#include <algorithm>

#include "Core/CaravelInput.h"


using namespace std;
using namespace Caravel;
using namespace IntegralsBH;

int main(int argc, char* argv[]){

    // Create the external particles (all massless)

    // mpmpp from Table 4 in arXiv:1809.09067
     PartialAmplitudeInput input("PartialAmplitudeInput[Particles[Particle[gluon,1,m],Particle[gluon,2,p],Particle[gluon,3,m],Particle[gluon,4,p],Particle[gluon,5,p]]]");

    SM_color_ordered model2use;

    const size_t nparticles = input.get_multiplicity();

    settings::use_setting("integrals::integral_family_1L integrals_BH");
    settings::use_setting("BG::partial_trace_normalization full_Ds");

    // Generate point with the internal rational generator from twistor variables
    
    using F = F32;
    using T = CHP;

    size_t cardinality(2147483629);
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    std::vector<BigRat> xs;
    if(argc >= 3){
        xs = Caravel::detail::read_twistor_parameters(argv[2]);
    }
    else{
        switch (nparticles) {
            case 4: xs = {BigRat(-3, 4), BigRat(-1, 4)}; break;
            case 5: xs = {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17), BigRat(-1)}; break;
            default: _WARNING_RED(nparticles); std::exit(1);
        }
    }

    auto physical_point = get_mom_conf<T>(nparticles, xs);

    AmpEng::CoefficientEngine<T> coefficient_provider(std::make_shared<Model>(model2use), input);
    if (!coefficient_provider.has_warmup()) {
        AmpEng::CoefficientEngine<F> cprovider(std::make_shared<Model>(model2use), input);
        std::vector<F> lxs;
        if (nparticles == 4)
            lxs = {F(524137), F(87215673)};
        else
            lxs = {F(524137), F(87215673), F(221245), F(98776138), F(343)};
        auto momc = rational_mom_conf<F>(nparticles, lxs);
        cprovider.compute(momc);
        coefficient_provider.reload_warmup();
    }

    Amplitude<T, decltype(coefficient_provider), ProviderBH<T>> one_loop_ampl(std::move(coefficient_provider));
    // we set to 4 the maximum, such that when combined with the integrals it ranges from -2 to 2
    one_loop_ampl.set_laurent_range(-2,4);

    


    START_TIMER_T(evaluation,CHP)
    auto result = one_loop_ampl.compute(physical_point);
    STOP_TIMER(evaluation)

    result *= one_loop_norm_caravel_to_physical<T>;
    result *= add_complex_t<T>{0,1};

    size_t errors(0);

    _PRINT(result);

    if(abs(result[-2]-T(-5))>typename T::value_type(0.000001))   errors++;
    if(abs(result[-1]-T(-17.88291386))>typename T::value_type(0.000001))   errors++;
    if(abs(result[0]-T(-29.50855173))>typename T::value_type(0.000001))   errors++;

    if(errors>0){
        std::cout<<"ERROR: did not match the target in Table 4 of arXiv:1809.09067!"<<std::endl;
    }

    return errors;
}

