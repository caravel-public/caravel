#include "Core/Debug.h"
#include "Core/Particle.h"
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/typedefs.h"
#include "Forest/Builder.h"
#include "Forest/Forest.h"
#include "Forest/Model.h"
#include "PhaseSpace/PhaseSpace.h"
#include "misc/TestUtilities.hpp"
#include "Core/InversionStrategies.h"
#include "misc/DeltaZero.h"
#include "EH_tree_scalars.hpp"

#define DEBUG_TEST

using namespace Caravel;

// Generate 3-particle on-shell phase space point with complex momenta
template <typename T, size_t Ds> momD_conf<T, Ds> OS_3point(const T& mass, bool random_boost = false) {
    T msq = mass * mass;

    // First construct momenta in (+,-.-,-) signature
    std::vector<std::vector<T>> moms(3);
    if (std::abs(mass) > std::abs(T(0))) {
        moms[0] = {(T(-1) - msq) / T(2), (T(2) - T(3) * msq) / T(4), T(0, 1) * (T(-2) + T(3) * msq) / T(4), (T(1) - msq) / T(2)};
        moms[2] = {(T(-2) + msq + T(10) * msq * msq) / (T(8) * msq), -(T(1) + T(3) * msq / T(8)), T(0, 1) * T(3) * (T(4) + msq) / T(8),
                   (T(-1) + T(2) / msq + T(10) * msq) / T(8)};
        moms[1] = {(T(3) + T(2) / msq - T(6) * msq) / T(8), (T(4) + T(9) * msq) / T(8), -T(0, 1) * (T(1) + T(9) * msq / T(8)),
                   -(T(3) + T(2) / msq + T(6) * msq) / T(8)};
    }
    else {
        T energy = T(50);
        moms[0] = {-energy, T(0), T(0), -energy};
        moms[1] = {energy / T(2), T(1), T(0, 1), energy / T(2)};
        moms[2] = {energy / T(2), T(-1), T(0, -1), energy / T(2)};
    }

    std::vector<momD<T, Ds>> qmom;
    if (random_boost) {
        const T r1 = T(rand()) / T(RAND_MAX), r2 = T(M_PI) * T(rand()) / T(RAND_MAX), r3 = T(2) * T(M_PI) * T(rand()) / T(RAND_MAX);
        const T bx = r1 * cos(r3) * sin(r2), by = r1 * sin(r3) * sin(r2), bz = r1 * cos(r2);
        const T bsq = bx * bx + by * by + bz * bz, gamma = T(1) / sqrt(T(1) - bsq);
        const std::vector<std::vector<T>> L = {
            {gamma, -gamma * bx, -gamma * by, -gamma * bz},
            {-gamma * bx, T(1) + (gamma - T(1)) * bx * bx / bsq, (gamma - T(1)) * bx * by / bsq, (gamma - T(1)) * bx * bz / bsq},
            {-gamma * by, (gamma - T(1)) * by * bx / bsq, T(1) + (gamma - T(1)) * by * by / bsq, (gamma - T(1)) * by * bz / bsq},
            {-gamma * bz, (gamma - T(1)) * bz * bx / bsq, (gamma - T(1)) * bz * by / bsq, T(1) + (gamma - T(1)) * bz * bz / bsq},
        };
        for (size_t n = 0; n < 3; n++) {
            std::vector<T> q = {T(0), T(0), T(0), T(0)};
            for (size_t i = 0; i < 4; i++) {
                for (size_t j = 0; j < 4; j++) q[i] += L[i][j] * moms[n][j];
            }
            qmom.push_back(momD<T, Ds>(q));
        }
    }
    else {
        for (size_t n = 0; n < 3; n++) qmom.push_back(momD<T, Ds>(moms[n]));
    }
    momD_conf<T, 4> pspoint = momD_conf<T, 4>(qmom);
    return pspoint;
}

// Generate random N-particle phase space point
template <typename T> momD_conf<std::complex<T>, 4> OS_Npoint(const std::vector<T>& mass, size_t seed = 42) {
    const T energy = T(500);
    PhaseSpace<T> Tph(mass.size(), energy, mass);
    Tph.set_reset_seed(false);
    srand(seed);
    Tph.generate();
    auto moms = Tph.mom();
    return to_momDconf<T, 4>(moms);
}

#define _GET_BUILDER(cK, N)                                                                                                                                    \
    EHGravity model2use;                                                                                                                                       \
    Forest SW(&model2use);                                                                                                                                     \
    unsigned track = SW.add_process(Process(input), {{"K", cK}});                                                                                              \
    std::vector<size_t> dims = {4};                                                                                                                            \
    SW.define_internal_states_and_close_forest(dims);                                                                                                          \
    Builder<T, 4> bob(&SW, N);                                                                                                                                 \
    bob.set_p(pspoint);                                                                                                                                        \
    bob.compute();                                                                                                                                             \
    bob.contract();

// Forward declaration
template <typename T, typename TR> int check_3pt();
template <typename T, typename TR> int check_3pt_reordered();
template <typename T, typename TR> int check_4pt();
template <typename T, typename TR> int check_4pt_reordered();
template <typename T, typename TR> int check_5pt();
template <typename T, typename TR> int check_5pt_reordered();
template <typename T, typename TR> int check_4scalar_amp();

int main(int argc, char* argv[]) {
    int n_errors = 0;

    n_errors += check_3pt<C, R>();
    n_errors += check_3pt_reordered<C, R>();
    n_errors += check_4pt<C, R>();
    n_errors += check_4pt_reordered<C, R>();
    n_errors += check_5pt<C, R>();
    n_errors += check_5pt_reordered<C, R>();
    n_errors += check_4scalar_amp<C, R>();

    if (n_errors == 0)
        _MESSAGE(Color::Code::FG_GREEN, "EH_tree_scalar test PASSED!");
    else
        _MESSAGE(Color::Code::FG_RED, "EH_tree_scalar test FAILED with ", n_errors, " errors!");

    return n_errors;
}

/**
 * Check 3-pt amplitudes M(GGG) and M(sGs)
 */
template <typename T, typename TR> int check_3pt() {
    int n_errors = 0;
    double tol = 10.0;

    // M3(1G+,2G+,3G-)
    {
        Particle G1("G2", ParticleType("G"), SingleState("hpp"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hmm"), 3, 0);
        std::vector<Particle*> input = {&G1, &G2, &G3};
        auto pspoint = OS_3point<T, 4>(T(0), true);

        // Setup Builder
        _GET_BUILDER(1, 3)

        // Comparison
        T klt = M3_ppm(pspoint[1], pspoint[2], pspoint[3]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(G+,G+,G-) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(G+,G+,G-) passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(G+,G+,G-) failed!");
            n_errors++;
        }
    }
    // M3(1G-,2G-,3G+)
    {
        Particle G1("G2", ParticleType("G"), SingleState("hmm"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hmm"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        std::vector<Particle*> input = {&G1, &G2, &G3};
        auto pspoint = OS_3point<T, 4>(T(0), true);

        // Setup Builder
        _GET_BUILDER(1, 3)

        // Comparison
        T klt = M3_mmp(pspoint[1], pspoint[2], pspoint[3]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(G-,G-,G+) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(tree, T(0), tol) && check(klt, T(0), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(G-,G-,G+) passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(G-,G-,G+) failed!");
            n_errors++;
        }
    }

    // M3(1s,2G+,3s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle S3("S3", ParticleType("Phi1"), SingleState("RealSC"), 3, 0);
        std::vector<Particle*> input = {&S1, &G2, &S3};
        auto pspoint = OS_3point<T, 4>(S1.get_mass(T(1)));

        // Setup Builder
        _GET_BUILDER(1,3)

        // Comparison
        T klt = M3_sps(pspoint[1], pspoint[2], pspoint[3]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,3s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,3s) passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,3s) failed!");
            n_errors++;
        }
    }

    // M3(1s,2G-,3s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hmm"), 2, 0);
        Particle S3("S3", ParticleType("Phi1"), SingleState("RealSC"), 3, 0);
        std::vector<Particle*> input = {&S1, &G2, &S3};
        auto pspoint = OS_3point<T, 4>(S1.get_mass(T(1)));

        // Setup Builder
        _GET_BUILDER(1, 3)

        // Comparison
        T klt = M3_sms(pspoint[1], pspoint[2], pspoint[3]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G-,3s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S1.get_mass(TR(1)))) {
            if (is_zero(klt) && is_zero(tree))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,3s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G-,3s) failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,3s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G-,3s) failed!");
                n_errors++;
            }
        }
    }

    return n_errors;
}

/**
 * Check 4-pt amplitudes M(sGGs)
 */
template <typename T, typename TR> int check_4pt() {
    int n_errors = 0;
    double tol = 9.0;
    // M4(1s,2G+,3G+,4s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_spps(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G+,4s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S1.get_mass(TR(1)))) { // Amplitude vanishes in the massless case
            if (check(tree, T(0), tol) && check(klt, T(0), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,4s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,4s) failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,4s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,4s) failed!");
                n_errors++;
            }
        }
    }
    // M4(1s,2G+,3G-,4s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hmm"), 3, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_spms(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G-,4s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G-,4s) passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G-,4s) failed!");
            n_errors++;
        }
    }
    // M4(1s,2G-,3G+,4s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hmm"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_smps(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G-,G+,4s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,G+,4s) passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G-,G+,4s) failed!");
            n_errors++;
        }
    }
    // M4(1s,2G-,3G-,4s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hmm"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hmm"), 3, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_smms(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G-,G-,4s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S1.get_mass(TR(1)))) { // Amplitude vanishes in the massless case
            if (check(tree, T(0), tol) && check(klt, T(0), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,G-,4s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G-,G-,4s) failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,G-,4s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G-,G-,4s) failed!");
                n_errors++;
            }
        }
    }
    // M4(1G+,2G+,3s,4s)
    {
        Particle G1("G1", ParticleType("G"), SingleState("hpp"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle S3("S3", ParticleType("Phi1"), SingleState("RealSC"), 3, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        std::vector<Particle*> input = {&G1, &G2, &S3, &S4};
        std::vector<TR> masses = {G1.get_mass(TR(1)), G2.get_mass(TR(1)), S3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_ppss(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(G+,G+,3s,4s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S4.get_mass(TR(1)))) { // Amplitude vanishes in the massless case
            if (check(tree, T(0), tol) && check(klt, T(0), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(G+,G+,3s,4s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(G+,G+,3s,4s) failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(G+,G+,3s,4s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(G+,G+,3s,4s) failed!");
                n_errors++;
            }
        }
    }

    return n_errors;
}
/**
 * Check 5-pt amplitudes M(sGGGs)
 */
template <typename T, typename TR> int check_5pt() {
    int n_errors = 0;
    double tol = 8.0;

    // M5(1s,2G+,3G+,4G+,5s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState("hpp"), 4, 0);
        Particle S5("S5", ParticleType("Phi1"), SingleState("RealSC"), 5, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &G4, &S5};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1)), S5.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(3, 5)

        // Comparison
        T klt = M5_sppps(pspoint[1], pspoint[2], pspoint[3], pspoint[4], pspoint[5]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G+,G+,5s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S1.get_mass(TR(1)))) { // Amplitude vanishes in the massless case
            if (check(tree, T(0), tol) && check(klt, T(0), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,G+,5s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,G+,5s) failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,G+,5s) passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,G+,5s) failed!");
                n_errors++;
            }
        }
    }
    // M5(1s,2G+,3G+,4G-,5s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState("hmm"), 4, 0);
        Particle S5("S5", ParticleType("Phi1"), SingleState("RealSC"), 5, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &G4, &S5};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1)), S5.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(3, 5)

        // Comparison
        T klt = M5_sppms(pspoint[1], pspoint[2], pspoint[3], pspoint[4], pspoint[5]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G+,G-,5s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,G-,5s) passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,G-,5s) failed!");
            n_errors++;
        }
    }
    // M5(1s,2G+,3G-,4G+,5s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hmm"), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState("hpp"), 4, 0);
        Particle S5("S5", ParticleType("Phi1"), SingleState("RealSC"), 5, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &G4, &S5};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1)), S5.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(3, 5)

        // Comparison
        T klt = M5_spmps(pspoint[1], pspoint[2], pspoint[3], pspoint[4], pspoint[5]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G-,G+,5s) KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G-,G+,5s) passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G-,G+,5s) failed!");
            n_errors++;
        }
    }

    return n_errors;
}

/**
 * Check 3-pt amplitudes M(sGs) with reordered recursion
 */
template <typename T, typename TR> int check_3pt_reordered() {
    int n_errors = 0;
    double tol = 10.0;

    // M3(1s,2G+,3s)
    {
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S3("S3", ParticleType("Phi1"), SingleState("RealSC"), 3, 0);
        std::vector<Particle*> input = {&S1, &G2, &S3};
        auto pspoint = OS_3point<T, 4>(S1.get_mass(T(1)));

        // Setup Builder
        _GET_BUILDER(1,3)

        // Comparison
        T klt = M3_sps(pspoint[1], pspoint[2], pspoint[3]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,3s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,3s) [re-ordered] passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,3s) [re-ordered] failed!");
            n_errors++;
        }
    }

    // M3(1s,2G-,3s)
    {
        Particle G2("G2", ParticleType("G"), SingleState("hmm"), 2, 0);
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S3("S3", ParticleType("Phi1"), SingleState("RealSC"), 3, 0);
        std::vector<Particle*> input = {&S1, &G2, &S3};
        auto pspoint = OS_3point<T, 4>(S1.get_mass(T(1)));

        // Setup Builder
        _GET_BUILDER(1, 3)

        // Comparison
        T klt = M3_sms(pspoint[1], pspoint[2], pspoint[3]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G-,3s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S1.get_mass(TR(1)))) {
            if (is_zero(klt) && is_zero(tree))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,3s) [re-ordered] passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G-,3s) [re-ordered] failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,3s) [re-ordered] passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G-,3s) [re-ordered] failed!");
                n_errors++;
            }
        }
    }

    return n_errors;
}

/**
 * Check 4-pt amplitudes M(GGss) with reordered recursions
 */
template <typename T, typename TR> int check_4pt_reordered() {
    int n_errors = 0;
    double tol = 9.0;

    // M4(1s,2G+,3G+,4s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_spps(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G+,4s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,4s) [re-ordered] passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,4s) [re-ordered] failed!");
            n_errors++;
        }
    }
    // M4(1s,2G+,3G-,4s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hmm"), 3, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_spms(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G-,4s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G-,4s) [re-ordered] passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G-,4s) [re-ordered] failed!");
            n_errors++;
        }
    }
    // M4(1s,2G-,3G+,4s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hmm"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_smps(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G-,G+,4s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,G+,4s) [re-ordered] passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G-,G+,4s) [re-ordered] failed!");
            n_errors++;
        }
    }
    // M4(1s,2G-,3G-,4s)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hmm"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hmm"), 3, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_smms(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G-,G-,4s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S1.get_mass(TR(1)))) { // Amplitude vanishes in the massless case
            if (check(tree, T(0), tol) && check(klt, T(0), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,G-,4s) [re-ordered] passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G-,G-,4s) [re-ordered] failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G-,G-,4s) [re-ordered] passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G-,G-,4s) [re-ordered] failed!");
                n_errors++;
            }
        }
    }

    // M4(1G+,2G+,3s,4s)
    {
        Particle S3("S3", ParticleType("Phi1"), SingleState("RealSC"), 3, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        Particle G1("G1", ParticleType("G"), SingleState("hpp"), 1, 0);
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        std::vector<Particle*> input = {&G1, &G2, &S3, &S4};
        std::vector<TR> masses = {G1.get_mass(TR(1)), G2.get_mass(TR(1)), S3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4_ppss(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(G+,G+,3s,4s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S4.get_mass(TR(1)))) { // Amplitude vanishes in the massless case
            if (check(tree, T(0), tol) && check(klt, T(0), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(G+,G+,3s,4s) [re-ordered] passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(G+,G+,3s,4s) [re-ordered] failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(G+,G+,3s,4s) [re-ordered] passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(G+,G+,3s,4s) [re-ordered] failed!");
                n_errors++;
            }
        }
    }

    return n_errors;
}

/**
 * Check 5-pt amplitudes M(sGGGs) with reordered recursions
 */
template <typename T, typename TR> int check_5pt_reordered() {
    int n_errors = 0;
    double tol = 8.0;

    // M5(1s,2G+,3G+,4G+,5s)
    {
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState("hpp"), 4, 0);
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S5("S5", ParticleType("Phi1"), SingleState("RealSC"), 5, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &G4, &S5};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1)), S5.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(3, 5)

        // Comparison
        T klt = M5_sppps(pspoint[1], pspoint[2], pspoint[3], pspoint[4], pspoint[5]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G+,G+,5s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (is_zero(S1.get_mass(TR(1)))) { // Amplitude vanishes in the massless case
            if (check(tree, T(0), tol) && check(klt, T(0), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,G+,5s) [re-ordered] passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,G+,5s) [re-ordered] failed!");
                n_errors++;
            }
        }
        else {
            if (check(ratio, T(1), tol))
                _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,G+,5s) [re-ordered] passed!");
            else {
                _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,G+,5s) [re-ordered] failed!");
                n_errors++;
            }
        }
    }
    // M5(1s,2G+,3G+,4G-,5s)
    {
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hpp"), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState("hmm"), 4, 0);
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S5("S5", ParticleType("Phi1"), SingleState("RealSC"), 5, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &G4, &S5};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1)), S5.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(3, 5)

        // Comparison
        T klt = M5_sppms(pspoint[1], pspoint[2], pspoint[3], pspoint[4], pspoint[5]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G+,G-,5s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G+,G-,5s) [re-ordered] passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G+,G-,5s) [re-ordered] failed!");
            n_errors++;
        }
    }
    // M5(1s,2G+,3G-,4G+,5s)
    {
        Particle G2("G2", ParticleType("G"), SingleState("hpp"), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState("hmm"), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState("hpp"), 4, 0);
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S5("S5", ParticleType("Phi1"), SingleState("RealSC"), 5, 0);
        std::vector<Particle*> input = {&S1, &G2, &G3, &G4, &S5};
        std::vector<TR> masses = {S1.get_mass(TR(1)), G2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1)), S5.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(3, 5)

        // Comparison
        T klt = M5_spmps(pspoint[1], pspoint[2], pspoint[3], pspoint[4], pspoint[5]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(klt / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(1s,G+,G-,G+,5s) [re-ordered] KLT: ", klt, " Tree: ", tree, " ", ratio, " ", klt / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(1s,G+,G-,G+,5s) [re-ordered] passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(1s,G+,G-,G+,5s) [re-ordered] failed!");
            n_errors++;
        }
    }

    return n_errors;
}

/**
 * Check amplitude for S1(p1) S2(p2) -> S2(p3) S1(p4)
 */
template <typename T, typename TR> int check_4scalar_amp() {
    int n_errors = 0;
    double tol = 8.0;

    // M4(1S1,2S2,3S2,4S4)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S2("S2", ParticleType("Phi2"), SingleState("RealSC"), 2, 0);
        Particle S3("S3", ParticleType("Phi2"), SingleState("RealSC"), 3, 0);
        Particle S4("S4", ParticleType("Phi1"), SingleState("RealSC"), 4, 0);
        std::vector<Particle*> input = {&S1, &S2, &S3, &S4};
        std::vector<TR> masses = {S1.get_mass(TR(1)), S2.get_mass(TR(1)), S3.get_mass(TR(1)), S4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T amp = M4_ssss(pspoint[1], pspoint[2], pspoint[3], pspoint[4]);
        T tree = bob.get_tree(track);
        T ratio = std::abs(amp / tree);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "M(S1,S2,S2,S1) Amp: ", amp, " Tree: ", tree, " ", ratio, " ", amp / tree);
#endif
        if (check(ratio, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(S1,S2,S2,S1) passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(S1,S2,S2,S1) failed!");
            n_errors++;
        }
    }
    return n_errors;
}
