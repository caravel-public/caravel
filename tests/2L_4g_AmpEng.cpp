/**
 * 2L_4g_AmpEng.cpp
 *
 * First created on 15.1.2018
 *
 * Test program for a 4-gluon amplitude at two loops using planar hierarchy from libAmpEng. Checks the numerical, floating point extraction of a sunrise coefficient in Ds=8 dimensions and D=2+I
 *
*/

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>

#include "Core/typedefs.h"
#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "AmpEng/cstructure.h"
#include "AmpEng/cutF.h"
#include "AmpEng/hierarchy_tools.h"
#include "Forest/Builder.h"
#include "AmpEng/cutF_kin.h"
#include "Forest/Model.h"
#include "Core/extend.h"
#include "Core/Debug.h"
#include "Forest/Forest.h"
#include "Core/settings.h"

#include "AmplitudeCoefficients/IntegrandHierarchy.h"

#define _DIM_Ds1 6
#define _DIM_Ds2 7
#define _DIM_Ds3 8
#define _DIM_D 6

#define _CTYPE CHP
#define _ITYPE 0

using namespace std;
using namespace Caravel;

int main(int argc, char* argv[]){
        using namespace ::Caravel::AmpEng;

        settings::use_setting("general::cut_propagator_numerators states");

	// Create the external particles (all massless)
	Particle g1("g1m",ParticleType("gluon"),SingleState("m"),1,0);
	Particle g2("g3p",ParticleType("gluon"),SingleState("p"),3,0);
	Particle g3("g2m",ParticleType("gluon"),SingleState("m"),2,0);
	Particle g4("g4p",ParticleType("gluon"),SingleState("p"),4,0);



	// Vector to store all maximal cuts
	std::vector<cutF> maximals;

	// Vector to rotate particles
	std::vector<Particle*> particles({&g1,&g2,&g3,&g4});

	// Now double boxes
	
	// quark pair contained in right rung
	maximals.push_back( get_simple_2loop_maximal_cut(particles,2) );
	maximals.back().show();
	// quark pair crosses rungs
	particles=std::vector<Particle*>({&g4,&g1,&g2,&g3});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,2) );
	maximals.back().show();


	// Now penta triangles
	
	particles=std::vector<Particle*>({&g1,&g2,&g3,&g4});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,3) );
	maximals.back().show();

	particles=std::vector<Particle*>({&g2,&g3,&g4,&g1});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,3) );
	maximals.back().show();

	particles=std::vector<Particle*>({&g3,&g4,&g1,&g2});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,3) );
	maximals.back().show();

	particles=std::vector<Particle*>({&g4,&g1,&g2,&g3});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,3) );
	maximals.back().show();


	// Now hexa bubbles
	
	particles=std::vector<Particle*>({&g1,&g2,&g3,&g4});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,4) );
	maximals.back().show();

	particles=std::vector<Particle*>({&g2,&g3,&g4,&g1});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,4) );
	maximals.back().show();

	particles=std::vector<Particle*>({&g3,&g4,&g1,&g2});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,4) );
	maximals.back().show();

	particles=std::vector<Particle*>({&g4,&g1,&g2,&g3});
	maximals.push_back( get_simple_2loop_maximal_cut(particles,4) );
	maximals.back().show();

	// Generating cut_hierarchy
	cout << endl << " > Generating cut hierarchy: " << endl << endl;

	// and using all maximal cuts, construct the full inheritance-tree
	// of the cut
	CutHierarchy cutFcontainer2loop(maximals);


	// show the inheritance tree of the cuts
	cout 	<< endl
		<< " > Print the inheritance-tree:" << endl << endl;
	cutFcontainer2loop.print_all_cuts();
	// and explore it
	//cutFcontainer2loop.explore_cut_inheritance();


	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------


	// Building the forest
	cout << endl << " > Building the forest: " << endl << endl;
	// build forest for cuts

	// Generic pointer to Model
	Model * model2use;
	// pick the model
	model2use=new SM_color_ordered();

	// Now construct the Forest
	Forest SW(model2use);

	// add to the forest all necessary cuts
	cutFcontainer2loop.grow_forest(SW);

	// Close forest
	vector<size_t> dims { _DIM_Ds3 };
	SW.define_internal_states_and_close_forest(dims);

	print_all_currents(SW);
	//explore_current_inheritance(SW);


        IntegrandHierarchy::load_surface_terms(*model2use, PartialAmplitudeInput{{g1,g2,g3,g4}});

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------

	// mom conf for kinematics
	#include "misc/std_momenta.hpp"
	// Now pick momconf (coming from std_momenta)
	momD_conf<C,4> momconf_process_C(mcs_4D[0]);
        momD_conf<_CTYPE,4> momconf_process = change_precision<_CTYPE>(momconf_process_C);

	// Now build Builder
	Builder<_CTYPE,_DIM_D> treecutSource(&SW,momconf_process.size());

        treecutSource.set_p(momconf_process);

	// compute all tree currents
	treecutSource.compute();
	// Make contractions to compute trees
	treecutSource.contract();

	// Build now from cutFcontainer2loop the kinematic interface, taking a reference to the Builder (it stores its pointer!)
	cut_kin_engine<_CTYPE,_DIM_D> cutKinematics(cutFcontainer2loop,&treecutSource,dims,true);

#if 0
	// Compute all coefficients
	cutKinematics.set_all_integrands(10000,-4);
#else
	// Compute coefficients of a given cut
	size_t level(4);
	size_t givencut(0);
	//std::cout<<"Level of cut to be fitted with all its parents: ";
	//std::cin>>level;
	//std::cout<<"Index of cut: ";
	//std::cin>>givencut;
	cutKinematics.compute_cut_and_all_parents(level,givencut,1000000,-5);	
#endif
	std::cout<<"The sunrise scalar coefficient is: "<<cutKinematics.get_integrand_coeffs(level,givencut,0)<<" and it should be 189.945+I*123.681"<<std::endl;

	int toreturn(0);
	if(abs(cutKinematics.get_integrand_coeffs(level,givencut,0).c[0].real()-R(189.945))>R(0.2))
		// real part failed at the 10^(-4) level
		toreturn++;
	if(abs(cutKinematics.get_integrand_coeffs(level,givencut,0).c[0].imag()-R(123.681))>R(0.2))
		// real part failed at the 10^(-4) level
		toreturn++;

        if(toreturn==0)
            std::cout << "Reached end: TEST PASSED!" << std::endl;
        else
            std::cout << "Reached end: TEST FAILED!" << std::endl;
	return toreturn;
}
