#include <map>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "AmpEng/CoefficientEngine.h"
#include "IntegralLibrary/IntegralProvider.h"
#include "Amplitude.h"

#include "Core/typedefs.h"
#include "Core/momD_conf.h"
#include "Core/Particle.h"

#include "Core/extend.h"

#include "misc/TestUtilities.hpp"

// CVHP is nescessary to reasonably check graviton amplitudes
#define _PRECISION CHP

// Define how many digits to print
#define _DIGITS_to_PRINT 32

#define check_rational 1
#define check_MHV 1

using namespace std;
using namespace Caravel;
using namespace AmpEng;

int main(void) {

    std::cout <<std::setprecision(_DIGITS_to_PRINT);

    // 1-loop 4-massless-legs hierarchy
    Walkers diagrams(1, 4);
    // remove all scaleless diagrams
    diagrams.filter(scaleless_graphs_filter);

    // Create the structures for defining the amplitudes
    GraphCutMap map_pppp;
    map_pppp.external = { {1, std::make_shared<Particle>("G1pp", ParticleType("G"), SingleState("hpp"), 1, 0)},
                     {2, std::make_shared<Particle>("G2pp", ParticleType("G"), SingleState("hpp"), 2, 0)},
                     {3, std::make_shared<Particle>("G3pp", ParticleType("G"), SingleState("hpp"), 3, 0)},
                     {4, std::make_shared<Particle>("G4pp", ParticleType("G"), SingleState("hpp"), 4, 0)},
                    };
    map_pppp.internal = { {0, ParticleType("G")} };
    map_pppp.coup = "K";
    Genealogy hierarchy_pppp(diagrams, map_pppp, true, "CubicGravity");

    GraphCutMap map_mppp;
    map_mppp.external = { {1, std::make_shared<Particle>("G1mm", ParticleType("G"), SingleState("hmm"), 1, 0)},
                     {2, std::make_shared<Particle>("G2pp", ParticleType("G"), SingleState("hpp"), 2, 0)},
                     {3, std::make_shared<Particle>("G3pp", ParticleType("G"), SingleState("hpp"), 3, 0)},
                     {4, std::make_shared<Particle>("G4pp", ParticleType("G"), SingleState("hpp"), 4, 0)},
                    };
    map_mppp.internal = { {0, ParticleType("G")} };
    map_mppp.coup = "K";
    Genealogy hierarchy_mppp(diagrams, map_mppp, true, "CubicGravity");

    GraphCutMap map_mmpp;
    map_mmpp.external = { {1, std::make_shared<Particle>("G1mm", ParticleType("G"), SingleState("hmm"), 1, 0)},
                     {2, std::make_shared<Particle>("G2mm", ParticleType("G"), SingleState("hmm"), 2, 0)},
                     {3, std::make_shared<Particle>("G3pp", ParticleType("G"), SingleState("hpp"), 3, 0)},
                     {4, std::make_shared<Particle>("G4pp", ParticleType("G"), SingleState("hpp"), 4, 0)},
                    };
    map_mmpp.internal = { {0, ParticleType("G")} };
    map_mmpp.coup = "K";
    Genealogy hierarchy_mmpp(diagrams, map_mmpp, true, "CubicGravity");

    // pick normalization 'none' to compare to the full amplitude
    settings::use_setting("general::do_n_eq_n_test yes");
    settings::use_setting("IntegrandHierarchy::amplitude_normalization none");

    size_t nparticles(4);

    // generate point with the internal rational generator from twistor variables
    

    std::vector<BigRat> xs;
    xs = {BigRat(-3) / BigRat(4), BigRat(-1) / BigRat(4)}; // 4pt std
    
    // this gives rational mom conf
    auto ps_rat = ps_parameterizations::rational_mom_conf<BigRat>(nparticles, xs, vector<size_t>(), false);

    // ps_parameterizations::print_mom_conf_invariants(ps_rat);

    momD_conf<_PRECISION, 4> m_gen = to_precision<_PRECISION>(ps_rat);

    momD_conf<_PRECISION, 4> momconf_process(m_gen);

    CubicGravity model2use;

    // For warmups
    using F = F32;
    size_t cardinality(2147483629);
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);
    auto physical_pointR = rational_mom_conf<BigRat>(nparticles, { BigRat(-1)/BigRat(2), BigRat(-3) });
    momD_conf<F, 4> physical_point(physical_pointR);

#if check_rational
    CoefficientEngine<_PRECISION> coefficient_provider_pppp(hierarchy_pppp, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
    if(!coefficient_provider_pppp.has_warmup()){
        CoefficientEngine<F> lcoefficient_provider_pppp(hierarchy_pppp, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
        lcoefficient_provider_pppp.compute(physical_point);
        coefficient_provider_pppp.reload_warmup();
    };

    CoefficientEngine<_PRECISION> coefficient_provider_mppp(hierarchy_mppp, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
    if(!coefficient_provider_mppp.has_warmup()){
        CoefficientEngine<F> lcoefficient_provider_mppp(hierarchy_mppp, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
        lcoefficient_provider_mppp.compute(physical_point);
        coefficient_provider_mppp.reload_warmup();
    };
#endif 

#if check_MHV
    CoefficientEngine<_PRECISION> coefficient_provider_mmpp(hierarchy_mmpp, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
    if(!coefficient_provider_mmpp.has_warmup()){
        CoefficientEngine<F> lcoefficient_provider_mmpp(hierarchy_mmpp, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
        lcoefficient_provider_mmpp.compute(physical_point);
        coefficient_provider_mmpp.reload_warmup();
    };
#endif 

    settings::use_setting("integrals::integral_family_1L goncharovs");
#if check_rational
    Amplitude<_PRECISION,CoefficientEngine<_PRECISION>, Integrals::sIntegralProvider<_PRECISION> > ampl_pppp(std::move(coefficient_provider_pppp));
    Amplitude<_PRECISION,CoefficientEngine<_PRECISION>, Integrals::sIntegralProvider<_PRECISION> > ampl_mppp(std::move(coefficient_provider_mppp));
    ampl_pppp.set_laurent_range(-2,2);    
    ampl_mppp.set_laurent_range(-2,2);    
#endif
#if check_MHV
    Amplitude<_PRECISION,CoefficientEngine<_PRECISION>, Integrals::sIntegralProvider<_PRECISION> > ampl_mmpp(std::move(coefficient_provider_mmpp));
    ampl_mmpp.set_laurent_range(-2,2);    
#endif

// Normalize by 2^6 * (-1)
  _PRECISION normalization = _PRECISION(-1) * _PRECISION(2) * _PRECISION(1)/_PRECISION(64);

#if check_rational
    Series<_PRECISION> Series_pppp(-2,1);
    Series<_PRECISION> Series_mppp(-2,1);
    Series_pppp = ampl_pppp.compute(momconf_process);
    Series_mppp = ampl_mppp.compute(momconf_process);
    Series_pppp *= normalization;
    Series_mppp *= normalization;
#endif
#if check_MHV
    Series<_PRECISION> Series_mmpp(-2,1);
    Series_mmpp = ampl_mmpp.compute(momconf_process);
    Series_mmpp *= normalization;
#endif

 // numerical evaluation of analytic expressions for the amplitudes
  Series<_PRECISION> ref_pppp(-2, 0,
    _PRECISION(0),
    _PRECISION(0),
    _PRECISION(-39)/_PRECISION(20971520)/_PRECISION(2)
  );

  Series<_PRECISION> ref_mppp(-2, 0,
    _PRECISION(0),
    _PRECISION(0),
    _PRECISION(13)/_PRECISION(46080)/_PRECISION(2)
  );

  Series<_PRECISION> ref_mmpp(-2, 0,
    _PRECISION(0),
    _PRECISION(0.000260634308132878,0.001456083326000823)/_PRECISION(2),
    _PRECISION(0.000485583620620669,0.003415951195613409)/_PRECISION(2)
  );

  int toreturn = 0;
  
#if check_rational
    if(abs(Series_pppp[-2]-ref_pppp[-2]) >= 10e-10){
      _MESSAGE("ep^-2 part of (++,++,++,++) does not match.");
      _MESSAGE(Series_pppp[-2], " != ", ref_pppp[-2]);
      toreturn++;
    }
      if(abs(Series_pppp[-1]-ref_pppp[-1]) >= 10e-9){
      _MESSAGE("ep^-1 part of (++,++,++,++) does not match.");
      _MESSAGE(Series_pppp[-1], " != ", ref_pppp[-1]);
      toreturn++;
    }
    if(abs(Series_pppp[0]-ref_pppp[0]) >= 10e-8){
      _MESSAGE("ep^0 part of (++,++,++,++) does not match.");
      _MESSAGE(Series_pppp[0], " != ", ref_pppp[0]);
      toreturn++;
    }
    if(abs(Series_mppp[-2]-ref_mppp[-2]) >= 10e-10){
      _MESSAGE("ep^-2 part of (--,++,++,++) does not match.");
      _MESSAGE(Series_mppp[-2], " != ", ref_mppp[-2]);
      toreturn++;
    }
      if(abs(Series_mppp[-1]-ref_mppp[-1]) >= 10e-9){
      _MESSAGE("ep^-1 part of (--,++,++,++) does not match.");
      _MESSAGE(Series_mppp[-1], " != ", ref_mppp[-1]);
      toreturn++;
    }
    if(abs(Series_mppp[0]-ref_mppp[0]) >= 10e-8){
      _MESSAGE("ep^0 part of (--,++,++,++) does not match.");
      _MESSAGE(Series_mppp[0], " != ", ref_mppp[0]);
      toreturn++;
    }
#endif

#if check_MHV
    if(abs(Series_mmpp[-2]-ref_mmpp[-2]) >= 10e-10){
      _MESSAGE("ep^-2 part of (--,--,++,++) does not match.");
      _MESSAGE(Series_mmpp[-2], " != ", ref_mmpp[-2]);
      toreturn++;
    }
    if(abs(Series_mmpp[-1]-ref_mmpp[-1]) >= 10e-9){
      _MESSAGE("ep^-1 part of (--,--,++,++) does not match.");
      _MESSAGE(Series_mmpp[-1], " != ", ref_mmpp[-1]);
      toreturn++;
    }
    if(abs(Series_mmpp[0]-ref_mmpp[0]) >= 10e-8){
      _MESSAGE("ep^0 part of (--,--,++,++) does not match.");
      _MESSAGE(Series_mmpp[0], " != ", ref_mmpp[0]);
      toreturn++;
    }
#endif

  if(toreturn > 0)
    _MESSAGE("one-loop 4-graviton full-amplitude test had ", Color::Code::FG_RED, std::to_string(toreturn)+" fails", Color::Code::FG_DEFAULT);
  else
    _MESSAGE("one-loop 4-graviton full-amplitude test", Color::Code::FG_GREEN," passed!", Color::Code::FG_DEFAULT);
  return toreturn;
}
