#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>

#include "Core/typedefs.h"
#include "IntegralsBH/polylog.h"
#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "Forest/Builder.h"
#include "Forest/Model.h"
#include "Core/extend.h"

#include "Core/Debug.h"
#include "Forest/Forest.h"

#define SMALL_VALUE 1e-8

using namespace std;
using namespace Caravel;

int main(int argc, char* argv[]){
  vector<vector<Particle*> >partPerm;
  partPerm.push_back(vector<Particle*>(
    {new Particle("q1m",ParticleType("q"),SingleState("qm"),1,0),
	new Particle("qb2p",ParticleType("qb"),SingleState("qbp"),2,0),
	new Particle("g3m",ParticleType("gluon"),SingleState("m"),3,0),
	new Particle("y4p",ParticleType("photon"),SingleState("p"),4,0)}));
  partPerm.push_back(vector<Particle*>(
    {new Particle("q1m",ParticleType("q"),SingleState("qm"),1,0),
	new Particle("qb2p",ParticleType("qb"),SingleState("qbp"),2,0),
	new Particle("g3p",ParticleType("gluon"),SingleState("p"),3,0),
	new Particle("y4m",ParticleType("photon"),SingleState("m"),4,0)}));
  partPerm.push_back(vector<Particle*>(
    {new Particle("qb1p",ParticleType("qb"),SingleState("qbp"),1,0),
	new Particle("q2m",ParticleType("q"),SingleState("qm"),2,0),
	new Particle("g3m",ParticleType("gluon"),SingleState("m"),3,0),
	new Particle("y4p",ParticleType("photon"),SingleState("p"),4,0)}));
  partPerm.push_back(vector<Particle*>(
    {new Particle("qb1p",ParticleType("qb"),SingleState("qbp"),1,0),
	new Particle("q2m",ParticleType("q"),SingleState("qm"),2,0),
	new Particle("g3p",ParticleType("gluon"),SingleState("p"),3,0),
	new Particle("y4m",ParticleType("photon"),SingleState("m"),4,0)}));

  //Factor for coupling
  C factor(1);
  
  Model * model2use=new SM_color_ordered();
  Forest SW(model2use);
#include "misc/std_momenta.hpp"
  momD_conf<C,4> momconf_process(mcs_4D[0]);
  map<string,int> tree_couplings;
  vector<int>track_tree_indices;
  for(auto&particles:partPerm)
    track_tree_indices.push_back(SW.add_process(Process(particles),map<string,int>{{"gs",1},{"gw",1}}));
  SW.define_internal_states_and_close_forest({4});
  Builder<C,4> treecutSource(&SW,partPerm.front().size());
  treecutSource.set_p(momconf_process);
  treecutSource.compute();
  treecutSource.contract();
  C sum(0);
  for(auto&track_tree:track_tree_indices)
    sum+=treecutSource.get_tree(track_tree)*conj(treecutSource.get_tree(track_tree));
  if(abs(sum-/*factor for coupling*/factor*/*tree value from hep-ph/0201274*/C(8)*(momconf_process.s(size_t(1),size_t(3))/momconf_process.s(size_t(2),size_t(3))+momconf_process.s(size_t(2),size_t(3))/momconf_process.s(size_t(1),size_t(3))))<SMALL_VALUE)
    return 0;
  else return 1;
}
