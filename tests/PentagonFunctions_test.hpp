#include <string>
#include <vector>
#include <utility>

#include "misc/TestUtilities.hpp"
#include "IntegralLibrary/SpecialFunctions/access_special_functions.h"
#include "Core/PhysRegSelector.h"

using namespace Caravel;
using namespace Caravel::Integrals;

size_t check_pentagon_functions_by_region(const std::string& name, const std::pair<size_t, std::vector<size_t>>& region, const std::vector<double>& v) {

    using C = std::complex<double>;

    const std::string vn1 = "s12";
    const std::string vn2 = "s23";
    const std::string vn3 = "s34";
    const std::string vn4 = "s45";
    const std::string vn5 = "s15";

    size_t errors(0);

    PhysRegSelector::set_region(region.first, region.second);

    const double v1 = v[0];
    const double v2 = v[1];
    const double v3 = v[2];
    const double v4 = v[3];
    const double v5 = v[4];

    // Check that functions can take care of permutations
    if (!check(pentagon_f_1_1<C>(v1), evaluatelower(1, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 1 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_1_1<C>(v2), evaluatelower(2, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 2 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_1_1<C>(v3), evaluatelower(3, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 3 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_1_1<C>(v4), evaluatelower(4, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 4 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_1_1<C>(v5), evaluatelower(5, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 5 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_2_1<C>(v1, v3), evaluatelower(6, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 6 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_2_1<C>(v2, v4), evaluatelower(7, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 7 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_2_1<C>(v3, v5), evaluatelower(8, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 8 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_2_1<C>(v4, v1), evaluatelower(9, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 9 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_2_1<C>(v5, v2), evaluatelower(10, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 10 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reversed) permutations
    if (!check(pentagon_f_2_1<C>(v3, v1), evaluatelower(6, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 11 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_2_1<C>(v4, v2), evaluatelower(10, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 12 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_2_1<C>(v5, v3), evaluatelower(9, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 13 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_2_1<C>(v1, v4), evaluatelower(8, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 14 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_2_1<C>(v2, v5), evaluatelower(7, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 15 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_3_1<C>(v1, v3), evaluatelower(11, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 16 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_1<C>(v2, v4), evaluatelower(12, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 17 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_1<C>(v3, v5), evaluatelower(13, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 18 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_1<C>(v4, v1), evaluatelower(14, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 19 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_1<C>(v5, v2), evaluatelower(15, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 20 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reversed) permutations
    if (!check(pentagon_f_3_1<C>(v3, v1), evaluatelower(11, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 21 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_1<C>(v4, v2), evaluatelower(15, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 22 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_1<C>(v5, v3), evaluatelower(14, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 23 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_1<C>(v1, v4), evaluatelower(13, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 24 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_1<C>(v2, v5), evaluatelower(12, v[2], v[1], v[0], v[4], v[3]))) {
        std::cout << "Failed test # 25 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_3_2<C>(v1, v3), evaluatelower(16, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 26 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_2<C>(v2, v4), evaluatelower(17, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 27 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_2<C>(v3, v5), evaluatelower(18, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 28 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_2<C>(v4, v1), evaluatelower(19, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 29 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_2<C>(v5, v2), evaluatelower(20, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 30 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_3_2<C>(v1, v3), evaluatelower(16, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 31 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_2<C>(v2, v4), evaluatelower(17, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 32 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_2<C>(v3, v5), evaluatelower(18, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 33 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_2<C>(v4, v1), evaluatelower(19, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 34 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_2<C>(v5, v2), evaluatelower(20, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 35 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_3_3<C>(v1, v2, v4), evaluatelower(21, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 36 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_3<C>(v2, v3, v5), evaluatelower(22, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 37 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_3<C>(v3, v4, v1), evaluatelower(23, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 38 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_3<C>(v4, v5, v2), evaluatelower(24, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 39 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_3<C>(v5, v1, v3), evaluatelower(25, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 40 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_3_3<C>(v2, v1, v4), evaluatelower(21, v[1], v[0], v[4], v[3], v[2]))) {
        std::cout << "Failed test # 41 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_3<C>(v3, v2, v5), evaluatelower(25, v[1], v[0], v[4], v[3], v[2]))) {
        std::cout << "Failed test # 42 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_3<C>(v4, v3, v1), evaluatelower(24, v[1], v[0], v[4], v[3], v[2]))) {
        std::cout << "Failed test # 43 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_3<C>(v5, v4, v2), evaluatelower(23, v[1], v[0], v[4], v[3], v[2]))) {
        std::cout << "Failed test # 44 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_3<C>(v1, v5, v3), evaluatelower(22, v[1], v[0], v[4], v[3], v[2]))) {
        std::cout << "Failed test # 45 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_3_4<C>(v1, v2, v3, v4, v5, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 46 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_4<C>(v2, v3, v4, v5, v1, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 47 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_4<C>(v3, v4, v5, v1, v2, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 48 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_4<C>(v4, v5, v1, v2, v3, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 49 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_4<C>(v5, v1, v2, v3, v4, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 50 in region: " << name << std::endl;
        errors++;
    }

    // And now reflections
    if (!check(pentagon_f_3_4<C>(v5, v4, v3, v2, v1, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 51 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_4<C>(v4, v3, v2, v1, v5, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 52 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_4<C>(v3, v2, v1, v5, v4, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 53 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_4<C>(v2, v1, v5, v4, v3, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 54 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_3_4<C>(v1, v5, v4, v3, v2, R(1.)), evaluatelower(26, v[0], v[1], v[2], v[3], v[4]))) {
        std::cout << "Failed test # 55 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_1<C>(v1, v3, vn1, vn3), integrate(1, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 56 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_1<C>(v2, v4, vn2, vn4), integrate(2, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 57 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_1<C>(v3, v5, vn3, vn5), integrate(3, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 58 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_1<C>(v4, v1, vn4, vn1), integrate(4, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 59 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_1<C>(v5, v2, vn5, vn2), integrate(5, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 60 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reversed) permutations
    if (!check(pentagon_f_4_1<C>(v3, v1, vn3, vn1), integrate(1, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 61 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_1<C>(v4, v2, vn4, vn2), integrate(5, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 62 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_1<C>(v5, v3, vn5, vn3), integrate(4, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 63 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_1<C>(v1, v4, vn1, vn4), integrate(3, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 64 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_1<C>(v2, v5, vn2, vn5), integrate(2, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 65 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_2<C>(v1, v3, vn1, vn3), integrate(6, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 66 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_2<C>(v2, v4, vn2, vn4), integrate(7, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 67 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_2<C>(v3, v5, vn3, vn5), integrate(8, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 68 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_2<C>(v4, v1, vn4, vn1), integrate(9, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 69 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_2<C>(v5, v2, vn5, vn2), integrate(10, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 70 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reversed) permutations
    if (!check(pentagon_f_4_2<C>(v3, v1, vn3, vn1), integrate(6, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 71 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_2<C>(v4, v2, vn4, vn2), integrate(10, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 72 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_2<C>(v5, v3, vn5, vn3), integrate(9, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 73 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_2<C>(v1, v4, vn1, vn4), integrate(8, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 74 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_2<C>(v2, v5, vn2, vn5), integrate(7, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 75 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_3<C>(v1, v3, vn1, vn3), integrate(11, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 76 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_3<C>(v2, v4, vn2, vn4), integrate(12, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 77 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_3<C>(v3, v5, vn3, vn5), integrate(13, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 78 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_3<C>(v4, v1, vn4, vn1), integrate(14, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 79 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_3<C>(v5, v2, vn5, vn2), integrate(15, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 80 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reversed) permutations
    if (!check(pentagon_f_4_3<C>(v3, v1, vn3, vn1), integrate(11, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 81 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_3<C>(v4, v2, vn4, vn2), integrate(15, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 82 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_3<C>(v5, v3, vn5, vn3), integrate(14, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 83 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_3<C>(v1, v4, vn1, vn4), integrate(13, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 84 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_3<C>(v2, v5, vn2, vn5), integrate(12, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 85 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_4<C>(v1, v2, v4), integrate(16, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 86 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_4<C>(v2, v3, v5), integrate(17, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 87 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_4<C>(v3, v4, v1), integrate(18, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 88 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_4<C>(v4, v5, v2), integrate(19, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 89 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_4<C>(v5, v1, v3), integrate(20, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 90 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reverse) permutations
    if (!check(pentagon_f_4_4<C>(v2, v1, v4), integrate(16, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 91 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_4<C>(v3, v2, v5), integrate(20, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 92 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_4<C>(v4, v3, v1), integrate(19, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 93 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_4<C>(v5, v4, v2), integrate(18, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 94 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_4<C>(v1, v5, v3), integrate(17, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 95 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_5<C>(v1, v2, v4), integrate(21, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 96 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_5<C>(v2, v3, v5), integrate(22, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 97 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_5<C>(v3, v4, v1), integrate(23, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 98 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_5<C>(v4, v5, v2), integrate(24, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 99 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_5<C>(v5, v1, v3), integrate(25, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 100 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reverse) permutations
    if (!check(pentagon_f_4_5<C>(v2, v1, v4), integrate(21, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 101 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_5<C>(v3, v2, v5), integrate(25, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 102 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_5<C>(v4, v3, v1), integrate(24, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 103 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_5<C>(v5, v4, v2), integrate(23, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 104 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_5<C>(v1, v5, v3), integrate(22, v[1], v[0], v[4], v[3], v[2], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 105 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_6<C>(v1, v3, v5), integrate(26, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 106 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_6<C>(v2, v4, v1), integrate(27, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 107 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_6<C>(v3, v5, v2), integrate(28, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 108 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_6<C>(v4, v1, v3), integrate(29, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 109 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_6<C>(v5, v2, v4), integrate(30, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 110 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reverse) permutations
    if (!check(pentagon_f_4_6<C>(v5, v3, v1), integrate(26, v[4], v[3], v[2], v[1], v[0], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 111 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_6<C>(v1, v4, v2), integrate(30, v[4], v[3], v[2], v[1], v[0], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 112 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_6<C>(v2, v5, v3), integrate(29, v[4], v[3], v[2], v[1], v[0], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 113 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_6<C>(v3, v1, v4), integrate(28, v[4], v[3], v[2], v[1], v[0], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 114 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_6<C>(v4, v2, v5), integrate(27, v[4], v[3], v[2], v[1], v[0], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 115 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_7<C>(v1, v3, v4), integrate(31, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 116 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_7<C>(v2, v4, v5), integrate(32, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 117 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_7<C>(v3, v5, v1), integrate(33, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 118 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_7<C>(v4, v1, v2), integrate(34, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 119 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_7<C>(v5, v2, v3), integrate(35, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 120 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reverse) permutations
    if (!check(pentagon_f_4_7<C>(v1, v4, v3), integrate(31, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 121 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_7<C>(v2, v5, v4), integrate(35, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 122 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_7<C>(v3, v1, v5), integrate(34, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 123 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_7<C>(v4, v2, v1), integrate(33, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 124 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_7<C>(v5, v3, v2), integrate(32, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 125 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_8<C>(v2, v4, v5), integrate(36, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 126 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_8<C>(v3, v5, v1), integrate(37, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 127 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_8<C>(v4, v1, v2), integrate(38, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 128 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_8<C>(v5, v2, v3), integrate(39, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 129 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_8<C>(v1, v3, v4), integrate(40, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 130 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reverse) permutations
    if (!check(pentagon_f_4_8<C>(v2, v5, v4), integrate(36, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 131 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_8<C>(v3, v1, v5), integrate(40, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 132 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_8<C>(v4, v2, v1), integrate(39, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 133 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_8<C>(v5, v3, v2), integrate(38, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 134 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_8<C>(v1, v4, v3), integrate(37, v[2], v[1], v[0], v[4], v[3], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 135 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_9<C>(v1, v3, v4), integrate(41, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 136 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_9<C>(v2, v4, v5), integrate(42, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 137 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_9<C>(v3, v5, v1), integrate(43, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 138 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_9<C>(v4, v1, v2), integrate(44, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 139 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_9<C>(v5, v2, v3), integrate(45, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 140 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of (reverse) permutations
    if (!check(pentagon_f_4_9<C>(v1, v4, v3), integrate(41, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 141 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_9<C>(v2, v5, v4), integrate(45, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 142 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_9<C>(v3, v1, v5), integrate(44, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 143 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_9<C>(v4, v2, v1), integrate(43, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 144 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_9<C>(v5, v3, v2), integrate(42, v[0], v[4], v[3], v[2], v[1], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 145 in region: " << name << std::endl;
        errors++;
    }

    // WARNING: we explicitly drop the following tests in the Euclidean regions as they take very long and often die with a GSL/CLN error report
    // Notice that in any case, these 5-variable checks are rather simple (the pentagon_f_4_1x just make the corresponding call to integrate)
    if (name == "euclidean") return errors;

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_10<C>(v1, v2, v3, v4, v5, R(1.)), integrate(46, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 146 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_10<C>(v2, v3, v4, v5, v1, R(1.)), integrate(47, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 147 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_10<C>(v3, v4, v5, v1, v2, R(1.)), integrate(48, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 148 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_10<C>(v4, v5, v1, v2, v3, R(1.)), integrate(49, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 149 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_10<C>(v5, v1, v2, v3, v4, R(1.)), integrate(50, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 150 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_11<C>(v1, v2, v3, v4, v5), integrate(51, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 151 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_11<C>(v2, v3, v4, v5, v1), integrate(52, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 152 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_11<C>(v3, v4, v5, v1, v2), integrate(53, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 153 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_11<C>(v4, v5, v1, v2, v3), integrate(54, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 154 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_11<C>(v5, v1, v2, v3, v4), integrate(55, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 155 in region: " << name << std::endl;
        errors++;
    }

    // Check that functions can take care of permutations
    if (!check(pentagon_f_4_12<C>(v1, v2, v3, v4, v5, R(1.)), integrate(56, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 156 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_12<C>(v2, v3, v4, v5, v1, R(1.)), integrate(56, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 157 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_12<C>(v3, v4, v5, v1, v2, R(1.)), integrate(56, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 158 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_12<C>(v4, v5, v1, v2, v3, R(1.)), integrate(56, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 159 in region: " << name << std::endl;
        errors++;
    }
    if (!check(pentagon_f_4_12<C>(v5, v1, v2, v3, v4, R(1.)), integrate(56, v[0], v[1], v[2], v[3], v[4], _PRECISION, 1e7), 7)) {
        std::cout << "Failed test # 160 in region: " << name << std::endl;
        errors++;
    }

    return errors;
}
