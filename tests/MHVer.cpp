/**
 * Numerical checks of tree-level n-gluon and 2-q (n-2) gluon amplitudes
 * against MHV expressions
 */

#include <functional>
#include <iostream>
#include <cstdlib>
#include <random>
#include <sstream>
#include <vector>
#include <cmath>

#include "Core/settings.h"
#include "Core/typedefs.h"

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "Forest/Builder.h"
#include "IntegralsBH/ProviderBH.h"
#include "Amplitude.h"

#include "misc/TestUtilities.hpp"

using namespace std;
using namespace Caravel;

// i <m2 m1>^4 / ( <n n-1> ... <2 1> <1 n> ) (e.g. eq (301) arXiv:1610.05318, no sqrt(2) factors, with counterclockwise ordering)
template <typename T> T MHV(const momD_conf<T, 4>& mc, size_t m1, size_t m2) {
    T denom(1);
    size_t n = mc.size();
    assert( m1 > 0 && m2 > 0);
    assert( m1 <= n && m2 <= n);
    for (size_t i = n; i >= 1; i--) {
        if (i != 1)
            denom *= spaa(mc.p(i), mc.p(i - 1));
        else
            denom *= spaa(mc.p(1), mc.p(n));
    }
    return T(0, 1) * spaa(mc.p(m2), mc.p(m1)) * spaa(mc.p(m2), mc.p(m1)) * spaa(mc.p(m2), mc.p(m1)) * spaa(mc.p(m2), mc.p(m1)) / denom;
}

// i [m1 m2]^4 / ( [1 2] ... [n-1 n] [n 1] ) (e.g. eq (302) arXiv:1610.05318, no sqrt(2) factors, with counterclockwise ordering)
template <typename T> T BMHV(const momD_conf<T, 4>& mc, size_t m1, size_t m2) {
    T denom(1);
    size_t n = mc.size();
    assert( m1 > 0 && m2 > 0);
    assert( m1 <= n && m2 <= n);
    for (size_t i = 1; i <= n; i++) {
        if (i != n)
            denom *= spbb(mc.p(i), mc.p(i + 1));
        else
            denom *= spbb(mc.p(n), mc.p(1));
    }
    return T(0, 1) * spbb(mc.p(m1), mc.p(m2)) * spbb(mc.p(m1), mc.p(m2)) * spbb(mc.p(m1), mc.p(m2)) * spbb(mc.p(m1), mc.p(m2)) / denom;
}

// i <m1 1>^3 <m1 2> / ( <n n-1> ... <2 1> <1 n> )
template <typename T> T qqbMHV(const momD_conf<T, 4>& mc, size_t m1) {
    T denom(1);
    size_t n = mc.size();
    assert( m1 > 0);
    assert( m1 <= n);
    for (size_t i = n; i >= 1; i--) {
        if (i != 1)
            denom *= spaa(mc.p(i), mc.p(i - 1));
        else
            denom *= spaa(mc.p(1), mc.p(n));
    }
    return T(0, 1) * spaa(mc.p(m1), mc.p(1)) * spaa(mc.p(m1), mc.p(1)) * spaa(mc.p(m1), mc.p(1)) * spaa(mc.p(m1), mc.p(2)) / denom;
}

// i [1 m1]^3 [2 m1] / ( [1 2] ... [n-1 n] [n 1] )
template <typename T> T qqbBMHV(const momD_conf<T, 4>& mc, size_t m1) {
    T denom(1);
    size_t n = mc.size();
    assert( m1 > 0);
    assert( m1 <= n);
    for (size_t i = 1; i <= n; i++) {
        if (i != n)
            denom *= spbb(mc.p(i), mc.p(i + 1));
        else
            denom *= spbb(mc.p(n), mc.p(1));
    }
    return T(0, 1) * spbb(mc.p(1), mc.p(m1)) * spbb(mc.p(1), mc.p(m1)) * spbb(mc.p(1), mc.p(m1)) * spbb(mc.p(2), mc.p(m1)) / denom;
}

int main(int argc, char* argv[]) {

    Particle m1("m1", ParticleType("gluon"), SingleState("m"), 1, 0);
    Particle p2("p2", ParticleType("gluon"), SingleState("p"), 2, 0);

    Particle qm1("qm1", ParticleType("q"), SingleState("qm"), 1, 0);
    Particle qp2("qbp2", ParticleType("qb"), SingleState("qbp"), 2, 0);

    Particle p3("p3", ParticleType("gluon"), SingleState("p"), 3, 0);
    Particle p4("p4", ParticleType("gluon"), SingleState("p"), 4, 0);
    Particle p5("p5", ParticleType("gluon"), SingleState("p"), 5, 0);
    Particle p6("p6", ParticleType("gluon"), SingleState("p"), 6, 0);
    Particle p7("p7", ParticleType("gluon"), SingleState("p"), 7, 0);
    Particle p8("p8", ParticleType("gluon"), SingleState("p"), 8, 0);
    Particle p9("p9", ParticleType("gluon"), SingleState("p"), 9, 0);
    Particle p10("p10", ParticleType("gluon"), SingleState("p"), 10, 0);

    std::vector<Particle*> to_prepare_ngMHV = {&m1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10 };
    std::vector<Particle*> to_prepare_2qngMHV = {&qm1, &qp2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10 };

    Particle p1("p1", ParticleType("gluon"), SingleState("p"), 1, 0);
    Particle m2("m2", ParticleType("gluon"), SingleState("m"), 2, 0);

    Particle qp1("qp1", ParticleType("q"), SingleState("qbp"), 1, 0);
    Particle qm2("qbb2", ParticleType("qb"), SingleState("qm"), 2, 0);

    Particle m3("m3", ParticleType("gluon"), SingleState("m"), 3, 0);
    Particle m4("m4", ParticleType("gluon"), SingleState("m"), 4, 0);
    Particle m5("m5", ParticleType("gluon"), SingleState("m"), 5, 0);
    Particle m6("m6", ParticleType("gluon"), SingleState("m"), 6, 0);
    Particle m7("m7", ParticleType("gluon"), SingleState("m"), 7, 0);
    Particle m8("m8", ParticleType("gluon"), SingleState("m"), 8, 0);
    Particle m9("m9", ParticleType("gluon"), SingleState("m"), 9, 0);
    Particle m10("m10", ParticleType("gluon"), SingleState("m"), 10, 0);

    std::vector<Particle*> to_prepare_ngBMHV = {&p1, &m2, &m3, &m4, &m5, &m6, &m7, &m8, &m9, &m10 };
    std::vector<Particle*> to_prepare_2qngBMHV = {&qp1, &qm2, &m3, &m4, &m5, &m6, &m7, &m8, &m9, &m10 };

    // Model
    SM_color_ordered model2use;

    size_t errors(0);
    for (size_t n = 4; n < 11; n++) {
        std::vector<Particle*> particles_ngMHV;
        std::vector<Particle*> particles_2qngMHV;
        std::vector<Particle*> particles_ngBMHV;
        std::vector<Particle*> particles_2qngBMHV;
        for (size_t ii = 0; ii < n; ii++) {
            particles_ngMHV.push_back(to_prepare_ngMHV[ii]);
            particles_2qngMHV.push_back(to_prepare_2qngMHV[ii]);
            particles_ngBMHV.push_back(to_prepare_ngBMHV[ii]);
            particles_2qngBMHV.push_back(to_prepare_2qngBMHV[ii]);
        }
        // replace oposite helicity gluon
        Particle m("m", ParticleType("gluon"), SingleState("m"), n - 1, 0);
        Particle p("p", ParticleType("gluon"), SingleState("p"), n - 1, 0);
        particles_ngMHV[n - 2] = &m;
        particles_2qngMHV[n - 2] = &m;
        particles_ngBMHV[n - 2] = &p;
        particles_2qngBMHV[n - 2] = &p;

        std::cout<<"Will compute (and compare to analytic expressions) the following trees: "<<std::endl;
        std::cout<<"	MHV:	 "; for(auto& pp: particles_ngMHV) std::cout<<pp->get_long_name()<<" "; std::cout<<std::endl;
        std::cout<<"	qqbMHV:	 "; for(auto& pp: particles_2qngMHV) std::cout<<pp->get_long_name()<<" "; std::cout<<std::endl;
        std::cout<<"	BMHV:	 "; for(auto& pp: particles_ngBMHV) std::cout<<pp->get_long_name()<<" "; std::cout<<std::endl;
        std::cout<<"	qqbBMHV: "; for(auto& pp: particles_2qngBMHV) std::cout<<pp->get_long_name()<<" "; std::cout<<std::endl;

        // Now construct the Forest
        Forest SW(model2use);

        SW.add_process(Process(particles_ngMHV), {{"gs", (int)n - 2}});
        SW.add_process(Process(particles_2qngMHV), {{"gs", (int)n - 2}});
        SW.add_process(Process(particles_ngBMHV), {{"gs", (int)n - 2}});
        SW.add_process(Process(particles_2qngBMHV), {{"gs", (int)n - 2}});

        vector<size_t> dims{4};
        SW.define_internal_states_and_close_forest(dims);

        PhaseSpace<R> tph(n, 1.);
        tph.set_reset_seed(false);
        srand(1010);

        tph.generate();
        //_MESSAGE(tph);
        // for(auto s: tph.mom()) std::cout << std::setprecision(16) << s << std::endl;

        auto moms = tph.mom_double();
        auto momconf_process = to_momDconf<R, 4>(moms);

        // Now build Builder
        Builder<C, 4> treecutSource(&SW, n);

        treecutSource.set_p(momconf_process);
        // compute all tree currents
        treecutSource.compute();
        // Make contractions to compute trees
        treecutSource.contract();

        C ngMHV = MHV(momconf_process, 1, n - 1);
        C qqbngMHV = qqbMHV(momconf_process, n - 1);
        C ngBMHV = BMHV(momconf_process, 1, n - 1);
        C qqbngBMHV = qqbBMHV(momconf_process, n - 1);

        // checks
        if (std::abs(treecutSource.get_tree(1) - ngMHV) > R(0.00001)) {
            std::cout << "ERROR: " << n << "-gluon MHV fail: " << treecutSource.get_tree(1) << " target: " << ngMHV << std::endl;
            errors++;
        }
        if (std::abs(treecutSource.get_tree(2) - qqbngMHV) > R(0.00001)) {
            std::cout << "ERROR: 2-quark " << n - 2 << "-gluon qqbMHV fail: " << treecutSource.get_tree(2) << " target: " << qqbngMHV << std::endl;
            errors++;
        }
        if (std::abs(treecutSource.get_tree(3) - ngBMHV) > R(0.00001)) {
            std::cout << "ERROR: " << n << "-gluon BMHV fail: " << treecutSource.get_tree(3) << " target: " << ngBMHV << std::endl;
            errors++;
        }
        if (std::abs(treecutSource.get_tree(4) - qqbngBMHV) > R(0.00001)) {
            std::cout << "ERROR: 2-quark " << n - 2 << "-gluon qqbBMHV fail: " << treecutSource.get_tree(4) << " target: " << qqbngBMHV << std::endl;
            errors++;
        }
    }

    return errors;
}

