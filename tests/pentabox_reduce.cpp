#include "Core/settings.h"
#include "Forest/Builder.h"
#include "AmplitudeCoefficients/Hierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "Core/RationalReconstruction.h"
#include "Forest/cardinality.h"

using namespace std;
using namespace Caravel;

// Loading four point hierarchy
int main(int argc, char* argv[]){

    using std::vector; using std::string; 
    const size_t L = 2;
    size_t num_point = 5;

    using namespace IntegrandHierarchy;

    settings::use_setting("general::master_basis standard");

    using F = F32;
    

    cardinality_update_manager<F>::get_instance()->set_cardinality(2147483629);


    constexpr size_t D = 6;

    HierarchyPos pentabox_pos {5,8};

    AmplitudeLikeContribution<F,L> con(num_point,slice_selector(pentabox_pos));

    // add any number of function for numerators
    con.add_numerator_evaluator(pentabox_pos, [](const OnShellPoint<F,D>& p,const momD_conf<F,D>&){
      auto mu11 = compute_Dsm4_prod(p.at(0), p.at(0));
      auto mu22 = compute_Dsm4_prod(p.at(1), p.at(1));
      auto mu12 = compute_Dsm4_prod(p.at(0), p.at(1));
      return (mu11*mu22-mu12*mu12);
    });

    con.close();


    // Choice of point
    std::vector<BigRat> xs;
    if (num_point == 5) { xs = {BigRat(10), BigRat(-5) / BigRat(3), BigRat(7), BigRat(42) / BigRat(5), BigRat(99)}; } // Euclidean other
    // if (num_point == 5){ xs = {BigRat(4),BigRat(1),BigRat(8),BigRat(7),BigRat(-1)}; } // Euclidean (main paper)
    if (num_point == 4) { xs = {BigRat(-1) / BigRat(4), BigRat(-3) / BigRat(4)}; }
    auto physical_point = rational_mom_conf<BigRat>(num_point, xs);

    // Complicated definition of modulo_ibp_reduce
    std::function<vector<DenseRational<F>>(const momentumD_configuration<F,4>&)>
      modulo_ibp_reduce = [&con](const momentumD_configuration<F,4>& mom_conf){

	return con.get_master_coefficients(mom_conf);
    };
    

    auto result_coeffs = RationalReconstruction::rational_reconstruct(physical_point, modulo_ibp_reduce);

    BigRat target = BigRat(25)/BigRat(17190954);

    _PRINT(target);
    _MESSAGE("result: ",result_coeffs.at(2).numerator.coefficients.at(0));

    // Test example. Note that this depends heavily on the basis
    // choice. In the future this is to be avoided.
    if (result_coeffs.at(2).numerator.coefficients.at(0) != target){
        std::cerr << "Error: Expected coefficient not found." << std::endl;
        return 1;
    }

    return 0;
} 
