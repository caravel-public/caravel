/**
 * OnShellMap_tests.cpp
 *
 * First created on 12.9.2019
 *
 * Testing Graph maps between diagrams
 */

#include <algorithm>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <functional>
#include <random>

#include "Core/Debug.h"
#include "Graph/Graph.h"
#include "Graph/GraphKin.h"
#include "AmpEng/Diagrammatica.h"
#include "AmpEng/FilterGraphs.h"

#include "Core/momD_conf.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "OnShellStrategies/OnShellStrategies.h"

#include "Core/typedefs.h"

using namespace Caravel;
using namespace lGraph;
using namespace lGraphKin;
using namespace AmpEng;

template <typename T, size_t D>
bool onshel_check(const xGraph& g, const momD_conf<T, D>& mc, const std::vector<momD<T, D>>& ospoint){
    for(auto& conn:g.get_base_graph().get_connection_map()){
        for(auto& strand:(conn.second).get_edges()){
            for(auto& momrouting:strand.get_routings()){
                // construct evaluator
                auto eval = MomentumRoutingEvaluator<T, D>(momrouting);
                auto total = eval(ospoint, mc);
                if(total*total != T(0)){
                    std::cout<<"Bad on-shell momentum for link: "<<momrouting<<" = "<<total<<" ^2 = "<<total*total<<std::endl;
                    return false;
                }
            }
        }
    }
    return true;
}

void set_loop_momentum(int index, int loop, Strand& s){
    size_t length = s.size();
    if(loop == 1)
        s.set_momentum_label(0, index);
    else if(loop == -1)
        s.set_momentum_label(0, -index);
    else if(loop == 2)
        s.set_momentum_label(length, index);
    else if(loop == -2)
        s.set_momentum_label(length, -index);
    else{
        std::cerr<<"Wrong loop in set_loop_momentum "<<loop<<std::endl;
        std::exit(1);
    }
}

xGraph produce_alternative_routing(const xGraph& in){
    std::map<std::pair<size_t, size_t>, Connection<Strand>> new_map;
    // we tranverse in a standard way and make corresponding assignments
    for(const auto& conn: in.get_base_graph().get_connection_map()){
        const auto& theconnection = conn.second;
        size_t strandcounter(0);
        std::vector<Strand> new_strands;
        for(const auto& strand: theconnection.get_edges()){
            std::vector<Bead> toconst;
            for(const auto& b: strand.get_beads()){
                toconst.push_back(b);
            }
            new_strands.push_back(Strand(toconst));

            // Shall we assign a momentum
            if(in.get_base_graph().is_factorizable()){
                set_loop_momentum( int(strandcounter)+1, 1, new_strands.back() );
            }
            else{
                if(strandcounter>0){
                    if(strandcounter==1)
                        set_loop_momentum( 2, 1, new_strands.back() );
                    else
                        set_loop_momentum( 1, 1, new_strands.back() );
                }
            }

            strandcounter++;
        }
        new_map[conn.first]=Connection<Strand>(new_strands);
    }
    std::vector<Node> new_nodes;
    for (const auto& node : in.get_base_graph().get_nodes()) {
        new_nodes.push_back(node);
    }
    Graph<Node, Connection<Strand>> toretg(new_nodes, new_map);
    xGraph toret(toretg);
    DEBUG(
        std::cout<<"produce_alternative_routing(.):"<<std::endl;
        std::cout<<"FROM:"<<std::endl;
        in.show();
        std::cout<<"GOT:"<<std::endl;
        toret.show();
    );
    return toret;
}

// TODO: massless propagators and only massless external on-shell particles assumed (can be extended later on!)
xGraph produce_a_candidate(const std::vector<std::pair<int,int>>& loops, const std::vector<size_t>& permutation, const xGraph& in, std::vector<std::vector<size_t>>& corresponding_momenta){
    //std::cout<<"loops: "<<loops<<" permutation: "<<permutation<<std::endl;
    std::map<std::pair<size_t, size_t>, Connection<Strand>> new_map;
    corresponding_momenta = std::vector<std::vector<size_t>>(permutation.size());
    size_t momcounter(0);
    // we tranverse in a standard way and make corresponding assignments
    for(const auto& conn: in.get_base_graph().get_connection_map()){
        const auto& theconnection = conn.second;
        size_t strandcounter(0);
        std::vector<Strand> new_strands;
        for(const auto& strand: theconnection.get_edges()){
            std::vector<Bead> toconst;
            for(const auto& b: strand.get_beads()){
                if(b.size()==1){
                    toconst.push_back(Bead(Leg(true, /* zero mass index*/ 0, permutation[momcounter])));
                    corresponding_momenta[permutation[momcounter]-1]=b.get_momentum_indices();
                    momcounter++;
                }
                else{
                    toconst.push_back(Bead(Leg(false, 0, permutation[momcounter])));
                    corresponding_momenta[permutation[momcounter]-1]=b.get_momentum_indices();
                    momcounter++;
                }
            }
            new_strands.push_back(Strand(toconst));

            // Shall we assign a momentum
            if(loops[strandcounter].first != 0){
                set_loop_momentum( loops[strandcounter].second, loops[strandcounter].first , new_strands.back() );
            }

            strandcounter++;
        }
        new_map[conn.first]=Connection<Strand>(new_strands);
    }
    std::vector<Node> new_nodes;
    for (const auto& node : in.get_base_graph().get_nodes()) {
        if (node.size() == 1) {
            new_nodes.push_back(Node(Leg(true, /* zero mass index*/ 0, permutation[momcounter])));
            corresponding_momenta[permutation[momcounter]-1]=node.get_momentum_indices();
            momcounter++;
        }
        else if (node.size() == 0) {
            new_nodes.push_back(Node());
        }
        else {
            new_nodes.push_back(Node(Leg(false, 0, permutation[momcounter])));
            corresponding_momenta[permutation[momcounter]-1]=node.get_momentum_indices();
            momcounter++;
        }
    }
    Graph<Node, Connection<Strand>> toretg(new_nodes, new_map);
    xGraph toret(toretg);
    DEBUG(
        std::cout<<"FROM:"<<std::endl;
        in.show();
        std::cout<<"GOT:"<<std::endl;
        toret.show();
        std::cout<<"WITH corresponding_momenta: "<<corresponding_momenta<<std::endl;
    );
    return toret;
}

int main(void){
    
    using F = F32;

    // this is a global cardinality that will not really be used
    cardinality_update_manager<F>::get_instance()->set_cardinality(2147483629);

    // a kinematic point
    size_t nparticles(5);
    std::vector<BigRat> xs;
    xs = {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17), BigRat(-1)}; // 5pt std point
    auto physical_point_rat = rational_mom_conf<BigRat>(nparticles, xs);
    momD_conf<F, 6> physical_point = static_cast<momentumD_configuration<F,4>>(physical_point_rat);
    EpsilonBasis<F>::squares[0]=F(1);
    EpsilonBasis<F>::squares[1]=F(1);

    // loop momentum locations (top: +/-1, bottom: +/-2, non: 0) (for factorized, '0' should be in third) ('second' in pair is loop momentum i ('li'))
    std::vector<std::vector<std::pair<int,int>>> loop_locations = {
        { { 1, 1}, { 2, 2}, { 0, 0} },
        { { 1, 1}, {-2, 2}, { 0, 0} },
        { {-1, 1}, { 2, 2}, { 0, 0} },
        { {-1, 1}, {-2, 2}, { 0, 0} },
        { { 2, 2}, { 1, 1}, { 0, 0} },
        { { 2, 2}, {-1, 1}, { 0, 0} },
        { {-2, 2}, { 1, 1}, { 0, 0} },
        { {-2, 2}, {-1, 1}, { 0, 0} },
        { { 1, 1}, { 0, 0}, { 2, 2} },
        { { 1, 1}, { 0, 0}, {-2, 2} },
        { {-1, 1}, { 0, 0}, { 2, 2} },
        { {-1, 1}, { 0, 0}, {-2, 2} },
        { { 2, 2}, { 0, 0}, { 1, 1} },
        { { 2, 2}, { 0, 0}, {-1, 1} },
        { {-2, 2}, { 0, 0}, { 1, 1} },
        { {-2, 2}, { 0, 0}, {-1, 1} },
        { { 0, 0}, { 1, 1}, { 2, 2} },
        { { 0, 0}, { 1, 1}, {-2, 2} },
        { { 0, 0}, {-1, 1}, { 2, 2} },
        { { 0, 0}, {-1, 1}, {-2, 2} },
        { { 0, 0}, { 2, 2}, { 1, 1} },
        { { 0, 0}, { 2, 2}, {-1, 1} },
        { { 0, 0}, {-2, 2}, { 1, 1} },
        { { 0, 0}, {-2, 2}, {-1, 1} },
        { { 1, 1}, { 1, 2}, { 0, 0} },
        { { 1, 1}, {-1, 2}, { 0, 0} },
        { {-1, 1}, { 1, 2}, { 0, 0} },
        { {-1, 1}, {-1, 2}, { 0, 0} },
        { { 1, 1}, { 1, 2}, { 0, 0} },
        { { 1, 1}, {-1, 2}, { 0, 0} },
        { {-1, 1}, { 1, 2}, { 0, 0} },
        { {-1, 1}, {-1, 2}, { 0, 0} },
        { { 1, 1}, { 0, 0}, { 1, 2} },
        { { 1, 1}, { 0, 0}, {-1, 2} },
        { {-1, 1}, { 0, 0}, { 1, 2} },
        { {-1, 1}, { 0, 0}, {-1, 2} },
        { { 2, 1}, { 0, 0}, { 2, 2} },
        { { 2, 1}, { 0, 0}, {-2, 2} },
        { {-2, 1}, { 0, 0}, { 2, 2} },
        { {-2, 1}, { 0, 0}, {-2, 2} },
        { { 0, 0}, { 2, 1}, { 2, 2} },
        { { 0, 0}, { 2, 1}, {-2, 2} },
        { { 0, 0}, {-2, 1}, { 2, 2} },
        { { 0, 0}, {-2, 1}, {-2, 2} },
        { { 0, 0}, { 2, 1}, { 2, 2} },
        { { 0, 0}, { 2, 1}, {-2, 2} },
        { { 0, 0}, {-2, 1}, { 2, 2} },
        { { 0, 0}, {-2, 1}, {-2, 2} },
        { { 1, 2}, { 2, 1}, { 0, 0} },
        { { 1, 2}, {-2, 1}, { 0, 0} },
        { {-1, 2}, { 2, 1}, { 0, 0} },
        { {-1, 2}, {-2, 1}, { 0, 0} },
        { { 2, 1}, { 1, 2}, { 0, 0} },
        { { 2, 1}, {-1, 2}, { 0, 0} },
        { {-2, 1}, { 1, 2}, { 0, 0} },
        { {-2, 1}, {-1, 2}, { 0, 0} },
        { { 1, 2}, { 0, 0}, { 2, 1} },
        { { 1, 2}, { 0, 0}, {-2, 1} },
        { {-1, 2}, { 0, 0}, { 2, 1} },
        { {-1, 2}, { 0, 0}, {-2, 1} },
        { { 2, 1}, { 0, 0}, { 1, 2} },
        { { 2, 1}, { 0, 0}, {-1, 2} },
        { {-2, 1}, { 0, 0}, { 1, 2} },
        { {-2, 1}, { 0, 0}, {-1, 2} },
        { { 0, 0}, { 1, 2}, { 2, 1} },
        { { 0, 0}, { 1, 2}, {-2, 1} },
        { { 0, 0}, {-1, 2}, { 2, 1} },
        { { 0, 0}, {-1, 2}, {-2, 1} },
        { { 0, 0}, { 2, 1}, { 1, 2} },
        { { 0, 0}, { 2, 1}, {-1, 2} },
        { { 0, 0}, {-2, 1}, { 1, 2} },
        { { 0, 0}, {-2, 1}, {-1, 2} },
        { { 1, 2}, { 1, 1}, { 0, 0} },
        { { 1, 2}, {-1, 1}, { 0, 0} },
        { {-1, 2}, { 1, 1}, { 0, 0} },
        { {-1, 2}, {-1, 1}, { 0, 0} },
        { { 1, 2}, { 1, 1}, { 0, 0} },
        { { 1, 2}, {-1, 1}, { 0, 0} },
        { {-1, 2}, { 1, 1}, { 0, 0} },
        { {-1, 2}, {-1, 1}, { 0, 0} },
        { { 1, 2}, { 0, 0}, { 1, 1} },
        { { 1, 2}, { 0, 0}, {-1, 1} },
        { {-1, 2}, { 0, 0}, { 1, 1} },
        { {-1, 2}, { 0, 0}, {-1, 1} },
        { { 2, 2}, { 0, 0}, { 2, 1} },
        { { 2, 2}, { 0, 0}, {-2, 1} },
        { {-2, 2}, { 0, 0}, { 2, 1} },
        { {-2, 2}, { 0, 0}, {-2, 1} },
        { { 0, 0}, { 2, 2}, { 2, 1} },
        { { 0, 0}, { 2, 2}, {-2, 1} },
        { { 0, 0}, {-2, 2}, { 2, 1} },
        { { 0, 0}, {-2, 2}, {-2, 1} },
        { { 0, 0}, { 2, 2}, { 2, 1} },
        { { 0, 0}, { 2, 2}, {-2, 1} },
        { { 0, 0}, {-2, 2}, { 2, 1} },
        { { 0, 0}, {-2, 2}, {-2, 1} }
    };

    // Construct the 2-loop diagrams with 5 massless legs
    Walkers nLoopswLegs(2,5);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Filter scaleless graphs"<<std::endl;
    nLoopswLegs.filter(scaleless_graphs_filter);
    std::cout<<"================================"<<std::endl;
    std::cout<<"Only planar with proper ordering"<<std::endl;
    nLoopswLegs.filter(nonplanar_ordering_filter({1,2,3,4,5}));
    
    auto allgraphs = nLoopswLegs.get_all_xgraphs();
    
    using sGraph = Graph<Node, Connection<Strand>>;
    std::vector<sGraph> topologies;

    size_t xgcounter(0);
#define _INCLUDE_ALT_CHECK 1
#if  _INCLUDE_ALT_CHECK
    size_t xgcounter_alt(0);
#endif
    size_t checkcounter(0);
    for(const auto& xgraph:allgraphs){
        const auto& topology = xgraph.get_topology();
        if(std::find(topologies.begin(),topologies.end(),topology)==topologies.end())
            topologies.push_back(topology);
        else
            // we already tried a similar graph
            continue;
        xgcounter++;

        // place for caching
        GraphKin<F, 6> gkin(xgraph);
        gkin.change_point( physical_point );

        // construct an on-shell PS generator
        OnShellMomentumParameterization<F, 6> get_momenta = surd_parameterization<F>(xgraph, gkin);

        // radom seeds
        auto trans_dims = xgraph.n_OS_variables_with_special_routing();
        std::vector<std::vector<F>> coordinates;
        for(size_t ii=0;ii<trans_dims.size();ii++){
            coordinates.push_back(std::vector<F>());
            for(size_t jj=0;jj<trans_dims[ii];jj++)
                coordinates.back().push_back(F(std::rand()));
        }

        auto onshellpoint = get_momenta(coordinates);

        // test on-shell PS generation
        if(!onshel_check<F, 6>(xgraph, physical_point, onshellpoint)){
            std::cerr<<"ERROR: on-shell phase-space generation failure"<<std::endl;
            std::cerr<<"	momDconf = "<<physical_point<<std::endl;
            std::cerr<<"	graph: "<<xgraph<<std::endl;
            std::exit(1);
        }
        checkcounter++;

        // the number of legs in the topology
        auto nlegs = topology.get_n_legs();
        std::vector<size_t> legs;
        for(size_t ii=1;ii<=nlegs;ii++)  legs.push_back(ii);
        std::vector<std::vector<size_t>> leg_permutations;
        size_t max_permute(20);
        size_t counter(0);
        std::random_device rd;
        // create a series of different permutations of the legs
        while(counter<max_permute){
            counter++;
            std::mt19937 g(rd());
            auto llegs = legs;
            std::shuffle(llegs.begin(), llegs.end(), g);
            if(std::find(leg_permutations.begin(), leg_permutations.end(), llegs) == leg_permutations.end())
                leg_permutations.push_back(llegs);
        }

        // Now we loop over all leg permutations and loop-momentum assignments
        for(auto& loops: loop_locations){
            if(xgraph.get_base_graph().is_factorizable() && loops[2].first!=0)
                // make sure that for factorizable cases the momenta is assigned to the first two strands
                continue;
            for(auto& permutation: leg_permutations){
                // here we store corresponding (permuted) momenta
                std::vector<std::vector<size_t>> corresponding_momenta;

                // get the new graph to map
                xGraph candidate = produce_a_candidate( loops, permutation, xgraph, corresponding_momenta );

                // and produce the maps
                xGraph copy_relabeled = xgraph.get_relabeled_copy(candidate);

                auto conventions_map_loop = OnShellStrategies::OnShellPointMap<F, 6>(xgraph, copy_relabeled);
                auto conventions_map_ext = lGraph::lGraphKin::external_momentum_mapper<F, 6>(copy_relabeled, candidate);

                auto loop_momenta_mapped = conventions_map_loop(onshellpoint, physical_point);
                auto ext_momenta_mapped = conventions_map_ext(physical_point);

                // test on-shell PS generation
                if(!onshel_check<F, 6>(candidate, ext_momenta_mapped, loop_momenta_mapped)){
                    std::cerr<<"ERROR: mapping failure!"<<std::endl;
                    std::cerr << "p1^2 = " << physical_point.p(1)*physical_point.p(2) << std::endl;
                    std::cerr << "Basis squares = " << EpsilonBasis<F>::squares[0] << "," << EpsilonBasis<F>::squares[1] << std::endl;
                    std::cerr<<"	momDconf        = "<<physical_point<<std::endl;
                    std::cerr<<"	momDconf mapped = "<<ext_momenta_mapped<<std::endl;
                    std::cerr<<"	loop        = "<<onshellpoint<<std::endl;
                    std::cerr<<"	loop mapped = "<<loop_momenta_mapped<<std::endl;
                    std::cerr<<"	graph: "<<xgraph<<std::endl;
                    std::cerr<<"	candidate: "<<candidate<<std::endl;
                    std::exit(1);
                }
                checkcounter++;
            }
        }
        std::cout<<"finished testing xgraph # "<<xgcounter<<std::endl;

#if  _INCLUDE_ALT_CHECK
        xGraph xgraph_alt = produce_alternative_routing( xgraph );
        xgcounter_alt++;

        // place for caching
        GraphKin<F, 6> gkin_alt(xgraph_alt);
        gkin_alt.change_point( physical_point );

        // construct an on-shell PS generator
        OnShellMomentumParameterization<F, 6> get_momenta_alt = surd_parameterization<F>(xgraph_alt, gkin_alt);

        // radom seeds
        auto trans_dims_alt = xgraph_alt.n_OS_variables_with_special_routing();
        std::vector<std::vector<F>> coordinates_alt;
        for(size_t ii=0;ii<trans_dims_alt.size();ii++){
            coordinates_alt.push_back(std::vector<F>());
            for(size_t jj=0;jj<trans_dims_alt[ii];jj++)
                coordinates_alt.back().push_back(F(std::rand()));
        }

        auto onshellpoint_alt = get_momenta_alt(coordinates_alt);

        // test on-shell PS generation
        if(!onshel_check<F, 6>(xgraph_alt, physical_point, onshellpoint_alt)){
            std::cerr<<"ERROR: on-shell phase-space generation failure"<<std::endl;
            std::cerr<<"	momDconf = "<<physical_point<<std::endl;
            std::cerr<<"	graph: "<<xgraph_alt<<std::endl;
            std::exit(1);
        }
        checkcounter++;

        // Now we loop over all leg permutations and loop-momentum assignments
        for(auto& loops: loop_locations){
            if(xgraph_alt.get_base_graph().is_factorizable() && loops[2].first!=0)
                // make sure that for factorizable cases the momenta is assigned to the first two strands
                continue;
            for(auto& permutation: leg_permutations){
                // here we store corresponding (permuted) momenta
                std::vector<std::vector<size_t>> corresponding_momenta;

                // get the new graph to map
                xGraph candidate = produce_a_candidate( loops, permutation, xgraph_alt, corresponding_momenta );

                // and produce the maps
                xGraph copy_relabeled = xgraph_alt.get_relabeled_copy(candidate);

                auto conventions_map_loop = OnShellStrategies::OnShellPointMap<F, 6>(xgraph_alt, copy_relabeled);
                auto conventions_map_ext = lGraph::lGraphKin::external_momentum_mapper<F, 6>(copy_relabeled, candidate);

                auto loop_momenta_mapped = conventions_map_loop(onshellpoint_alt, physical_point);
                auto ext_momenta_mapped = conventions_map_ext(physical_point);

                // test on-shell PS generation
                if(!onshel_check<F, 6>(candidate, ext_momenta_mapped, loop_momenta_mapped)){
                    std::cerr<<"ERROR: mapping failure!"<<std::endl;
                    std::cerr<<"	momDconf        = "<<physical_point<<std::endl;
                    std::cerr<<"	momDconf mapped = "<<ext_momenta_mapped<<std::endl;
                    std::cerr<<"	loop        = "<<onshellpoint<<std::endl;
                    std::cerr<<"	loop mapped = "<<loop_momenta_mapped<<std::endl;
                    std::cerr<<"	graph: "<<xgraph_alt<<std::endl;
                    std::cerr<<"	candidate: "<<candidate<<std::endl;
                    std::exit(1);
                }
                checkcounter++;
            }
        }
        std::cout<<"finished testing xgraph_alt # "<<xgcounter_alt<<std::endl;
#endif


    }
    std::cout<<"all "<<checkcounter<<" on-shell map tests passed"<<std::endl;
    return 0;
}

