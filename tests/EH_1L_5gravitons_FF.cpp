#include <algorithm>
#include <map>

#include "Core/typedefs.h"
#include "Core/Utilities.h"
#include "Core/settings.h"

#include "misc/TestUtilities.hpp"

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "AmpEng/CoefficientEngine.h"
#include "AmpEng/Diagrammatica.h"
#include "AmpEng/Parenthood.h"

using namespace std;
using namespace Caravel;
using namespace AmpEng;
using namespace lGraph;

int main(int argc, char* argv[]){

    settings::use_setting("general::do_n_eq_n_test yes");
    settings::use_setting("IntegrandHierarchy::amplitude_normalization tree");

    // kinematics
    size_t cardinality(2147483629);
    cardinality_update_manager<F32>::get_instance()->set_cardinality(cardinality);

    std::vector<BigRat> xs;
    xs = {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17), BigRat(-1)}; // 5pt std point

    auto physical_point = rational_mom_conf<BigRat>(5, xs);
    momD_conf<F32, 4> mconf(physical_point);
    _PRINT(mconf);

    // 1-loop 5-massless-legs hierarchy
    Walkers diagrams(1, 5);
    // remove all scaleless diagrams
    diagrams.filter(scaleless_graphs_filter);
    GraphCutMap map;
    map.external = { {1, std::make_shared<Particle>("G1mm", ParticleType("G"), SingleState("hmm"), 1, 0)},
                     {2, std::make_shared<Particle>("G2mm", ParticleType("G"), SingleState("hmm"), 2, 0)},
                     {3, std::make_shared<Particle>("G3pp", ParticleType("G"), SingleState("hpp"), 3, 0)},
                     {4, std::make_shared<Particle>("G4pp", ParticleType("G"), SingleState("hpp"), 4, 0)},
                     {5, std::make_shared<Particle>("G5pp", ParticleType("G"), SingleState("hpp"), 5, 0)},
                    };
    map.internal = { {0, ParticleType("G")} };
    map.coup = "K";
    Genealogy hierarchy(diagrams, map, true, "EHGravity");
    //hierarchy.explore();

    EHGravity model2use;
    CoefficientEngine<F32> coefficient_provider(hierarchy, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 3}}, true);
    if (!coefficient_provider.has_warmup()) {
        auto physical_point1 = rational_mom_conf<BigRat>(5, {BigRat(44564), BigRat(1456456), BigRat(86546), BigRat(745645), BigRat(-1545)});
        momentumD_configuration<F32, 4> psp_warmup = static_cast<momentumD_configuration<F32, 4>>(physical_point1);
        coefficient_provider.compute(psp_warmup);
        coefficient_provider.reload_warmup();
    }
    auto integrals = coefficient_provider.get_integral_graphs();
    auto result = coefficient_provider.compute(mconf);
    //_PRINT(integrals);
    //_PRINT(result);

    // check zero for mu^4 s12 bubble
    IntegralGraph b12(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[2,0]]],Connection[Strand[Link[0],Bead[Leg[3,0],Leg[4,0],Leg[5,0]],Link[LoopMomentum[1],0]]]]"), "mu4");
    auto match_b12 = [&b12](const IntegralGraph& a) {
        return a.get_graph().get_base_graph() == b12.get_graph().get_base_graph() && a.get_insertion() == b12.get_insertion();
    };

    auto it1 = std::find_if(integrals.begin(), integrals.end(), match_b12);
    int i1 = it1 - integrals.begin();
    unsigned errors = 0;
    if (result[i1] != DenseRational<F32>(0, "ep")) {
        errors++;
        std::cerr << "Non zero value: " << result[i1] << std::endl;
    }

    // check value of eps^0 (1,2,3,4,5) pentagon (mu^2) integral
    IntegralGraph p12345(MathList("CaravelGraph[Nodes[Node[Leg[1,0]]],Connection[Strand[Link[0],Bead[Leg[2,0]],Link[0],Bead[Leg[3,0]],Link[0],Bead[Leg[4,0]],Link[0],Bead[Leg[5,0]],Link[LoopMomentum[1],0]]]]"), "mu2");
    auto match_p12345 = [&p12345](const IntegralGraph& a) {
        return a.get_graph().get_base_graph() == p12345.get_graph().get_base_graph() && a.get_insertion() == p12345.get_insertion();
    };

    auto it2 = std::find_if(integrals.begin(), integrals.end(), match_p12345);
    int i2 = it2 - integrals.begin();
    // OJO: added normalization factor (-2) 9/14/2022
    if (F32(-2) * result[i2].numerator.coefficients[0] != F32(BigRat("65808651911428304984755310566941457205/84554857159158352290037537943611974196"))) {
        errors++;
        std::cerr << "Value: " << result[i2].numerator.coefficients[0]
                  << " does not match: (-1/2) * 65808651911428304984755310566941457205/84554857159158352290037537943611974196 (finite field cardinality = "
                  << Barrett32::get_p() << ") " << std::endl;
    }

    if (errors == 0) _MESSAGE("All tests (one coefficient of a pentagon integral and one of a bubble integral)", Color::Code::FG_GREEN, " PASSED", Color::Code::FG_DEFAULT);
    else if (errors == 1) _MESSAGE("Detected 1 error! Test", Color::Code::FG_RED, " FAILED", Color::Code::FG_DEFAULT);
    else _MESSAGE("Detected 2 errors! Test", Color::Code::FG_RED, " FAILED", Color::Code::FG_DEFAULT);

    return errors;
}
