/**
 * tree_gravitons.cpp
 *
 * First created on 10.10.2018
 *
 * Construct an example of a tree-level amplitude with 4 external gravitons using CubicGravity model
 *
*/

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>

#include "Core/typedefs.h"
#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "Forest/Builder.h"
#include "Forest/Model.h"
#include "Core/Debug.h"
#include "Forest/Forest.h"
#include "PhaseSpace/PhaseSpace.h"

#define _DIM_Ds 4
#define _DIM_D 4

#define _CTYPE C
#define _ITYPE 0

using namespace std;
using namespace Caravel;

int main(int argc, char* argv[]){

	// Create the external particles (all massless)
	Particle G1mm("G1mm",ParticleType("G"),SingleState("hmm"),1,0);
	Particle G1pp("G1pp",ParticleType("G"),SingleState("hpp"),1,0);
	Particle G2mm("G2mm",ParticleType("G"),SingleState("hmm"),2,0);
	Particle G2pp("G2pp",ParticleType("G"),SingleState("hpp"),2,0);
	Particle G3mm("G3mm",ParticleType("G"),SingleState("hmm"),3,0);
	Particle G3pp("G3pp",ParticleType("G"),SingleState("hpp"),3,0);
	Particle G4mm("G4mm",ParticleType("G"),SingleState("hmm"),4,0);
	Particle G4pp("G4pp",ParticleType("G"),SingleState("hpp"),4,0);
	Particle G5mm("G5mm",ParticleType("G"),SingleState("hmm"),5,0);
	Particle G5pp("G5pp",ParticleType("G"),SingleState("hpp"),5,0);
	Particle G6mm("G6mm",ParticleType("G"),SingleState("hmm"),6,0);
	Particle G6pp("G6pp",ParticleType("G"),SingleState("hpp"),6,0);

	// Vector of particle pointers

	std::vector<Particle*> all_particles4mmpp({&G1mm,&G2mm,&G3pp,&G4pp});
	std::vector<Particle*> particles4mmpp;

	for(size_t ii=0;ii<4;ii++){
		particles4mmpp.push_back(all_particles4mmpp[ii]);
	}

	std::vector<Particle*> all_particles5mmppp({&G1mm,&G2mm,&G3pp,&G4pp,&G5pp});
	std::vector<Particle*> particles5mmppp;

	for(size_t ii=0;ii<5;ii++){
		particles5mmppp.push_back(all_particles5mmppp[ii]);
	}

	std::vector<Particle*> all_particles6mppppm({&G1mm,&G2pp,&G3pp,&G4pp,&G5pp,&G6mm});
	std::vector<Particle*> all_particles6mppppp({&G1mm,&G2pp,&G3pp,&G4pp,&G5pp,&G6pp});
	std::vector<Particle*> all_particles6pppppp({&G1pp,&G2pp,&G3pp,&G4pp,&G5pp,&G6pp});

	std::vector<Particle*> particles6mppppm,particles6mppppp,particles6pppppp;

	for(size_t ii=0;ii<6;ii++){
		particles6mppppm.push_back(all_particles6mppppm[ii]);
		particles6mppppp.push_back(all_particles6mppppp[ii]);
		particles6pppppp.push_back(all_particles6pppppp[ii]);
	}

	Process process_tree4mmpp(particles4mmpp);
	Process process_tree5mmppp(particles5mmppp);
	Process process_tree6mppppm(particles6mppppm);
	Process process_tree6mppppp(particles6mppppp);
	Process process_tree6pppppp(particles6pppppp);

        cutTopology::couplings_t treecouplings4{{"K",2}};
        cutTopology::couplings_t treecouplings5{{"K",3}};
        cutTopology::couplings_t treecouplings6{{"K",4}};

        // construct the 'trivial' cutTopology for this tree

	cutTopology tree4mmppp(process_tree4mmpp,treecouplings4);
	cutTopology tree5mmppp(process_tree5mmppp,treecouplings5);
	cutTopology tree6mppppm(process_tree6mppppm,treecouplings6);
	cutTopology tree6mppppp(process_tree6mppppp,treecouplings6);
	cutTopology tree6pppppp(process_tree6pppppp,treecouplings6);

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------

	// pick the model
	CubicGravity model2use;

	// Now construct the Forest
	Forest SW4(&model2use);
	Forest SW5(&model2use);
	Forest SW6(&model2use);

	int track_tree4mmpp;
	int track_tree5mmppp;
	int track_tree6mppppm, track_tree6mppppp, track_tree6pppppp;

	// best to use add_process(.) for trees (for simpler access to info)

	track_tree4mmpp=SW4.add_process(process_tree4mmpp,treecouplings4);
	track_tree5mmppp=SW5.add_process(process_tree5mmppp,treecouplings5);
	track_tree6mppppm=SW6.add_process(process_tree6mppppm,treecouplings6);
	track_tree6mppppp=SW6.add_process(process_tree6mppppp,treecouplings6);
	track_tree6pppppp=SW6.add_process(process_tree6pppppp,treecouplings6);

	// Close forest
	vector<size_t> dims { _DIM_Ds };

	SW4.define_internal_states_and_close_forest(dims);
	SW5.define_internal_states_and_close_forest(dims);
	SW6.define_internal_states_and_close_forest(dims);

	// using the PhaseSpace utilities
	PhaseSpace<R> tph4(4,1.);
	tph4.set_reset_seed(false);
	srand(1110);
	tph4.generate();
	auto moms4= tph4.mom_double();
	momD_conf<C,4> momconf_process4 = to_momDconf<R,4>(moms4);

	PhaseSpace<R> tph5(5,1.);
	tph5.set_reset_seed(false);
	srand(1110);
	tph5.generate();
	auto moms5= tph5.mom_double();
	momD_conf<C,4> momconf_process5 = to_momDconf<R,4>(moms5);

	PhaseSpace<R> tph(6,1.);
	tph.set_reset_seed(false);
	srand(1110);
	tph.generate();
	auto moms = tph.mom_double();
	momD_conf<C,4> momconf_process6 = to_momDconf<R,4>(moms);

	// Now build Builder

	Builder<_CTYPE,_DIM_D> treecutSource4(&SW4,4);
	Builder<_CTYPE,_DIM_D> treecutSource5(&SW5,5);
	Builder<_CTYPE,_DIM_D> treecutSource6(&SW6,6);

	treecutSource4.set_p(momconf_process4);
	treecutSource5.set_p(momconf_process5);
	treecutSource6.set_p(momconf_process6);

	// compute all tree currents

	treecutSource4.compute();
	treecutSource5.compute();
	treecutSource6.compute();

	// Make contractions to compute trees

	treecutSource4.contract();
	treecutSource5.contract();
	treecutSource6.contract();

	std::cout << setprecision(16);

	std::complex<double> A4mmpp(treecutSource4.get_tree(track_tree4mmpp));

	std::complex<double> A5mmppp(treecutSource5.get_tree(track_tree5mmppp));

	std::complex<double> A6pppppp(treecutSource6.get_tree(track_tree6pppppp));
	std::complex<double> A6mppppp(treecutSource6.get_tree(track_tree6mppppp));
	std::complex<double> A6mppppm(treecutSource6.get_tree(track_tree6mppppm));

	// Show result
	// std::cout << "M(-,-,+,+) = "<< A4mmpp << std::endl;
	// std::cout << "M(-,-,+,+,+) = "<< A5mmppp << std::endl;
	// std::cout << "M(+,+,+,+,+,+) = "<< A6pppppp << std::endl;
	// std::cout << "M(-,+,+,+,+,+) = "<< A6mppppp << std::endl;
	// std::cout << "M(-,+,+,+,+,-) = "<< A6mppppm << std::endl;

	// reference expression calculated using KLT [as in arXiv:0805.3993 (2.14)]
	// Numerics via S@M at default phase space point

	std::complex<double> refA6mppppm(-0.3961839721363347,0.2110848121819386);
	std::complex<double> refA5mmppp(-9.39620120997682,26.79027686346788);
	std::complex<double> refA4mmpp(5.509539710761961,-4.514514346616079);

	std::complex<double> ratioA6mppppm(A6mppppm/refA6mppppm);
	std::complex<double> ratioA5mmppp(A5mmppp/refA5mmppp);
	std::complex<double> ratioA4mmpp(A4mmpp/refA4mmpp);

	int toret(0);
	if(abs(A6pppppp)>R(0.0000000001)||
		abs(A6mppppp)>R(0.0000000001)){
		toret++;
		std::cout<<"all plus or signgle plus amplitudes are non-vanishing!"<<std::endl;
	}

	if(abs(ratioA6mppppm-C(1))>R(0.0000000001)||
		abs(ratioA5mmppp-C(1))>R(0.0000000001)||
		abs(ratioA4mmpp-C(1))>R(0.0000000001)){
		std::cout<<"Trees are not correctly normalized"<< std::endl;
		std::cout<<"M(-,+,+,+,+,-) / KLT (NORMALIZED) = "<< ratioA6mppppm << std::endl;
		std::cout<<"M(-,-,+,+,+) / KLT (NORMALIZED) = "<< ratioA5mmppp << std::endl;
		std::cout<<"M(-,-,+,+) / KLT (NORMALIZED) = "<< ratioA4mmpp << std::endl;
		toret++;

	}

	if (toret>0) {
		std::cout<<"ERROR: the 6-graviton checks failed!"<< std::endl;
	}


	return toret;
}
