/*
 * EH_tree_vectors.hpp
 *
 * This file implements the 3 to 5 point Gravity amplitudes for
 * Graviton scattering in association with massive vectors
 * via KLT relations
 */

namespace Caravel {

template <typename T, size_t Ds, bool KLT = true> std::vector<T> get_state(const momD_conf<T, Ds>& mc, const Particle& P) {
    const std::string Pol = P.external_state().get_name();
    const momD<T, Ds> p = mc[P.mom_index()];
    const T mass = P.get_mass(T(1));
    momD<T, Ds> r;
    switch (P.ref_index()) {
        case 0: r = clifford_algebra::get_helper_momD<T, Ds>(1); break;
        default: r = mc[P.ref_index()];
    }

    if (P.get_statistics() == ParticleStatistics::vector && P.is_massive()) {
        std::vector<T> state;
        if (Pol == "p") { state = Massive_vector_states::externalWF_p<T, T, Ds>(p, r, mass); }
        else if (Pol == "m") {
            state = Massive_vector_states::externalWF_m<T, T, Ds>(p, r, mass);
        }
        else if (Pol == "L") {
            state = Massive_vector_states::externalWF_L<T, T, Ds>(p, r, mass);
        }
        else if (Pol == "Ward") {
            state = p.get_vector();
        }
        // Builder includes a factor 1/sqrt(2) for each of these states
        for (auto& c : state) c /= sqrt(T(2));

        return state;
    }
    if (P.get_statistics() == ParticleStatistics::vector && P.is_massless()) {
        std::vector<T> state;
        if (Pol == "p") { state = externalWF_vectorboson_p<T, T, Ds>(mc, P.mom_index(), P.ref_index()); }
        else if (Pol == "m") {
            state = externalWF_vectorboson_m<T, T, Ds>(mc, P.mom_index(), P.ref_index());
        }
        else if (Pol == "Ward") {
            state = externalWF_vectorboson_Ward<T, T, Ds>(mc, P.mom_index(), P.ref_index());
        }
        // Builder includes a factor 1/sqrt(2) for each of these states
        for (auto& c : state) c /= sqrt(T(2));

        return state;
    }

    else if (P.get_statistics() == ParticleStatistics::tensor) {
        std::vector<T> state;
        if constexpr (KLT) {
            // Note that because of KLT relations we map Tensor Gravitons to Yang-Mills vector bosons
            if (Pol == "hpp") { state = externalWF_vectorboson_p<T, T, Ds>(mc, P.mom_index(), P.ref_index()); }
            else if (Pol == "hmm") {
                state = externalWF_vectorboson_m<T, T, Ds>(mc, P.mom_index(), P.ref_index());
            }
            else if (Pol == "hWard") {
                state = externalWF_vectorboson_Ward<T, T, Ds>(mc, P.mom_index(), P.ref_index());
            }
            // Builder includes a factor 1/sqrt(2) for each of these states
            for (auto& c : state) c /= sqrt(T(2));
        }
        else {
            if (Pol == "hpp") { state = BG::externalWF_tensorboson_pp<T, T, Ds>(mc, P.mom_index(), P.ref_index()); }
            else if (Pol == "hmm") {
                state = BG::externalWF_tensorboson_mm<T, T, Ds>(mc, P.mom_index(), P.ref_index());
            }
            else if (Pol == "hWard") {
                state = BG::externalWF_tensorboson_ward<T, T, Ds>(mc, P.mom_index(), P.ref_index());
            }
            // Builder includes a factor 1/2 for each of graviton state
            for (auto& c : state) c /= T(2);
        }

        return state;
    }

    std::cout << "get_state: You shouldn't reach here!" << std::endl;
    std::exit(1);
}

/*
 * 3-pt G(p1) -> V(p2) V(p3) scattering amplitude
 * A factor of kappa/2 is excluded
 */
template <typename T, size_t Ds> T Mgvv(const momD_conf<T, Ds>& mc, const Particle& G1, const Particle& V2, const Particle& V3) {
    const momD<T, Ds> p1 = mc[G1.mom_index()];
    const momD<T, Ds> p2 = mc[V2.mom_index()];
    const momD<T, Ds> p3 = mc[V3.mom_index()];
    const T msq = V2.get_mass(T(1)) * V2.get_mass(T(1));

    // Polarization vectors
    using namespace BG;
    const Tensor<T, Ds, 2> H = Tensor<T, Ds, 2>(get_state<T, Ds, false>(mc, G1));
    const momD<T, Ds> ep2 = momD<T, Ds>(get_state(mc, V2));
    const momD<T, Ds> ep3 = momD<T, Ds>(get_state(mc, V3));

    const T H23 = Tensor<T, Ds, 1>(p3) * (index_contraction<0>(p2, H));
    const T H2ep3 = Tensor<T, Ds, 1>(ep3) * (index_contraction<0>(p2, H));
    const T H3ep2 = Tensor<T, Ds, 1>(ep2) * (index_contraction<0>(p3, H));
    const T Hep3ep2 = Tensor<T, Ds, 1>(ep2) * (index_contraction<0>(ep3, H));
    const T trH = trace_rank2(H);

    T amp = +(p2 * ep3) * (p3 * ep2) * trH

            + (ep2 * ep3) * (-msq - (p2 * p3)) * trH

            + H23 * (ep2 * ep3) * T(2)

            + H2ep3 * (p3 * ep2) * T(-2)

            + H3ep2 * (p2 * ep3) * T(-2)

            + Hep3ep2 * T(2) * (msq + (p2 * p3));

    amp *= T(0, -1);

    return amp;
}

/*
 * 4-pt S1(p1) S2(p2) -> V1(p3) V2(p4) scattering amplitude
 * Explicit Feynman-Diagram computation
 */
template <typename T, size_t Ds> T Mssvv(const momD_conf<T, Ds>& mc, const Particle& S1, const Particle& S2, const Particle& V1, const Particle& V2) {
    const momD<T, Ds> p1 = mc[S1.mom_index()];
    const momD<T, Ds> p2 = mc[S2.mom_index()];
    const momD<T, Ds> p3 = mc[V1.mom_index()];
    const momD<T, Ds> p4 = mc[V2.mom_index()];

    // Polarization vectors
    const momD<T, Ds> ep3 = momD<T, Ds>(get_state(mc, V1));
    const momD<T, Ds> ep4 = momD<T, Ds>(get_state(mc, V2));

    // Masses and invariants
    const T m1sq = p1 * p1;
    const T m2sq = p3 * p3;
    const T s = (p1 + p2) * (p1 + p2);

    T amp = (p1 * ep3) * (p2 * ep4) * (-T(2) * s) + (p1 * ep3) * (p3 * ep4) * (T(4) * (p2 * p4)) + (p1 * ep4) * (p2 * ep3) * (-T(2) * s) +
            (p1 * ep4) * (p4 * ep3) * (T(4) * (p1 * p4)) + (p2 * ep3) * (p3 * ep4) * (T(4) * (p1 * p4)) + (p2 * ep4) * (p4 * ep3) * (T(4) * (p2 * p4)) +
            (p3 * ep4) * (p4 * ep3) * (-T(2) * s + T(4) * m1sq) +
            (ep3 * ep4) * (s * s - T(2) * m1sq * s + T(4) * m1sq * m2sq - T(4) * (p1 * p4) * (p1 * p4) - T(4) * (p2 * p4) * (p2 * p4));

    amp *= T(0, -1) / (T(2) * s);

    return amp;
}

/*
 * 4-pt V1(p1) V2(p2) -> V2(p3) V1(p4) scattering amplitude
 * Explicit Feynman-Diagram computation
 */
template <typename T, size_t Ds> T Mvvvv(const momD_conf<T, Ds>& mc, Particle V1, Particle V2, Particle V3, Particle V4) {
    assert(V1.get_statistics() == ParticleStatistics::vector);
    assert(V2.get_statistics() == ParticleStatistics::vector);
    assert(V3.get_statistics() == ParticleStatistics::vector);
    assert(V4.get_statistics() == ParticleStatistics::vector);
    assert(V1.get_mass_index() == V4.get_mass_index());
    assert(V2.get_mass_index() == V3.get_mass_index());

    const momD<T, Ds> p1 = mc[V1.mom_index()];
    const momD<T, Ds> p2 = mc[V2.mom_index()];
    const momD<T, Ds> p3 = mc[V3.mom_index()];
    const momD<T, Ds> p4 = mc[V4.mom_index()];

    // Polarization vectors
    const momD<T, Ds> ep1 = momD<T, Ds>(get_state(mc, V1));
    const momD<T, Ds> ep2 = momD<T, Ds>(get_state(mc, V2));
    const momD<T, Ds> ep3 = momD<T, Ds>(get_state(mc, V3));
    const momD<T, Ds> ep4 = momD<T, Ds>(get_state(mc, V4));

    // Masses and invariants
    const T m1sq = p1 * p1;
    const T m2sq = p2 * p2;
    const T s = (p1 + p2) * (p1 + p2);
    const T t = (p1 + p4) * (p1 + p4);
    const T u = (p1 + p3) * (p1 + p3);


    T amp =        + (p1*ep2)*(p2*ep3)*(p3*ep4)*(p4*ep1) * T(  - 2 )

       + (p1*ep2)*(p2*ep3)*(ep1*ep4) * (  - m2sq - m1sq + s )

       + (p1*ep2)*(p4*ep1)*(ep3*ep4) * ( t )

       + (p1*ep2)*(p4*ep3)*(ep1*ep4) * (  - t )

       + (p1*ep3)*(p2*ep4)*(p3*ep2)*(p4*ep1) * T(  - 2 )

       + (p1*ep3)*(p3*ep2)*(ep1*ep4) * (  - m2sq - m1sq + u )

       + (p1*ep3)*(p4*ep1)*(ep2*ep4) * ( t )

       + (p1*ep3)*(p4*ep2)*(ep1*ep4) * (  - t )

       + (p1*ep4)*(p2*ep1)*(p3*ep2)*(p4*ep3) * T(  - 2 )

       + (p1*ep4)*(p2*ep1)*(ep2*ep3) * (  - m2sq - m1sq + s )

       + (p1*ep4)*(p2*ep3)*(p3*ep1)*(p4*ep2) * T(  - 2 )

       + (p1*ep4)*(p2*ep3)*(p3*ep2)*(p4*ep1) * T( 4 )

       + (p1*ep4)*(p2*ep3)*(ep1*ep2) * ( m2sq + m1sq - s )

       + (p1*ep4)*(p3*ep1)*(ep2*ep3) * (  - m2sq - m1sq + u )

       + (p1*ep4)*(p3*ep2)*(ep1*ep3) * ( m2sq + m1sq - u )

       + (p1*ep4)*(p4*ep1)*(ep2*ep3) * ( T(2)*m2sq - T(2)*t )

       + (p1*ep4)*(p4*ep2)*(ep1*ep3) * ( t )

       + (p1*ep4)*(p4*ep3)*(ep1*ep2) * ( t )

       + (p2*ep1)*(p3*ep2)*(ep3*ep4) * ( t )

       + (p2*ep1)*(p3*ep4)*(ep2*ep3) * (  - t )

       + (p2*ep3)*(p3*ep1)*(ep2*ep4) * ( t )

       + (p2*ep3)*(p3*ep2)*(ep1*ep4) * ( T(2)*m1sq - T(2)*t )

       + (p2*ep3)*(p3*ep4)*(ep1*ep2) * ( t )

       + (p2*ep3)*(p4*ep1)*(ep2*ep4) * ( m2sq + m1sq - u )

       + (p2*ep3)*(p4*ep2)*(ep1*ep4) * (  - m2sq - m1sq + u )

       + (p2*ep4)*(p3*ep1)*(ep2*ep3) * (  - t )

       + (p2*ep4)*(p3*ep2)*(ep1*ep3) * ( t )

       + (p2*ep4)*(p4*ep1)*(ep2*ep3) * (  - m2sq - m1sq + u )

       + (p3*ep2)*(p4*ep1)*(ep3*ep4) * ( m2sq + m1sq - s )

       + (p3*ep2)*(p4*ep3)*(ep1*ep4) * (  - m2sq - m1sq + s )

       + (p3*ep4)*(p4*ep1)*(ep2*ep3) * (  - m2sq - m1sq + s )

       + (ep1*ep2)*(ep3*ep4) * (  - t*t/T(2) )

       + (ep1*ep3)*(ep2*ep4) * (  - t*t/T(2) )

       + (ep1*ep4)*(ep2*ep3) * (  - m2sq*m2sq - m1sq*m1sq + u*m2sq + u*m1sq - u*u/T(2) - 
         t*m2sq - t*m1sq + t*t + s*m2sq + s*m1sq - s*s/T(2) );

    amp *= T(0, 1) / (T(4) * t);
    // Caravel removes (kappa/2)^2 factor
    amp *= T(4);

    return amp;
}

/*
 * 4-pt V1(p1) V2(p2) -> G3(p3) G4(p4) scattering amplitude
 * Taken from: arXiv:2005.03071 Eqs. (4.16) - (4.18)
 */
template <typename T, size_t Ds> T M4(const momD_conf<T, Ds>& mc, const Particle& V1, const Particle& V2, const Particle& G3, const Particle& G4) {
    assert(V1.get_statistics() == ParticleStatistics::vector);
    assert(V2.get_statistics() == ParticleStatistics::vector);
    assert(G3.get_statistics() == ParticleStatistics::tensor);
    assert(G4.get_statistics() == ParticleStatistics::tensor);
    assert(V1.get_mass_index() == V2.get_mass_index());

    const momD<T, Ds> p1 = mc[V1.mom_index()];
    const momD<T, Ds> p2 = mc[V2.mom_index()];
    const momD<T, Ds> p3 = mc[G3.mom_index()];
    const momD<T, Ds> p4 = mc[G4.mom_index()];
    const T msq = V1.get_mass(T(1)) * V1.get_mass(T(1));
    const T t = (p1 + p2) * (p1 + p2), s = (p1 + p3) * (p1 + p3);

    // Polarization vectors
    const momD<T, Ds> ep1 = momD<T, Ds>(get_state(mc, V1));
    const momD<T, Ds> ep2 = momD<T, Ds>(get_state(mc, V2));
    const momD<T, Ds> ep3 = momD<T, Ds>(get_state(mc, G3));
    const momD<T, Ds> ep4 = momD<T, Ds>(get_state(mc, G4));
    auto sp = [](momD<T, Ds> p1, momD<T, Ds> p2) { return (p1 * p2); };

    // This is the amplitude computed explicitly from Feynman Diagrams (by Mao)
    T ampVV = (((msq - s) * (msq - s - t) * sp(ep3, ep4) -
                T(2) * ((msq - s) * sp(ep3, p4) * sp(ep4, p1) + sp(ep3, p1) * (t * sp(ep4, p1) + (-msq + s + t) * sp(ep4, p3)))) *
               (msq * msq * sp(ep1, ep2) * sp(ep3, ep4) - T(2) * msq * s * sp(ep1, ep2) * sp(ep3, ep4) + s * s * sp(ep1, ep2) * sp(ep3, ep4) -
                msq * t * sp(ep1, ep2) * sp(ep3, ep4) + s * t * sp(ep1, ep2) * sp(ep3, ep4) + T(2) * msq * sp(ep1, p4) * sp(ep2, p3) * sp(ep3, ep4) -
                T(2) * s * sp(ep1, p4) * sp(ep2, p3) * sp(ep3, ep4) - T(2) * msq * sp(ep1, p3) * sp(ep2, p4) * sp(ep3, ep4) +
                T(2) * s * sp(ep1, p3) * sp(ep2, p4) * sp(ep3, ep4) + T(2) * t * sp(ep1, p3) * sp(ep2, p4) * sp(ep3, ep4) +
                T(2) * t * sp(ep1, p4) * sp(ep2, ep4) * sp(ep3, p1) + T(2) * msq * sp(ep1, p3) * sp(ep2, ep4) * sp(ep3, p4) -
                T(2) * s * sp(ep1, p3) * sp(ep2, ep4) * sp(ep3, p4) - T(2) * t * sp(ep1, p3) * sp(ep2, ep4) * sp(ep3, p4) +
                T(2) * msq * sp(ep1, p4) * sp(ep2, ep4) * sp(ep3, p4) - T(2) * s * sp(ep1, p4) * sp(ep2, ep4) * sp(ep3, p4) +
                sp(ep1, ep4) * ((msq - s) * t * sp(ep2, ep3) -
                                T(2) * ((msq - s) * sp(ep2, p3) * sp(ep3, p4) + sp(ep2, p4) * (t * sp(ep3, p1) + (msq - s) * sp(ep3, p4)))) +
                T(2) * t * sp(ep1, p3) * sp(ep2, ep3) * sp(ep4, p1) - T(2) * t * sp(ep1, ep2) * sp(ep3, p1) * sp(ep4, p1) -
                T(2) * msq * sp(ep1, ep2) * sp(ep3, p4) * sp(ep4, p1) + T(2) * s * sp(ep1, ep2) * sp(ep3, p4) * sp(ep4, p1) -
                T(2) * msq * sp(ep1, p3) * sp(ep2, ep3) * sp(ep4, p3) + T(2) * s * sp(ep1, p3) * sp(ep2, ep3) * sp(ep4, p3) +
                T(2) * t * sp(ep1, p3) * sp(ep2, ep3) * sp(ep4, p3) - T(2) * msq * sp(ep1, p4) * sp(ep2, ep3) * sp(ep4, p3) +
                T(2) * s * sp(ep1, p4) * sp(ep2, ep3) * sp(ep4, p3) + T(2) * msq * sp(ep1, ep2) * sp(ep3, p1) * sp(ep4, p3) -
                T(2) * s * sp(ep1, ep2) * sp(ep3, p1) * sp(ep4, p3) - T(2) * t * sp(ep1, ep2) * sp(ep3, p1) * sp(ep4, p3) +
                sp(ep1, ep3) * (t * (-msq + s + t) * sp(ep2, ep4) -
                                T(2) * ((-msq + s + t) * sp(ep2, p4) * sp(ep4, p3) + sp(ep2, p3) * (t * sp(ep4, p1) + (-msq + s + t) * sp(ep4, p3)))))) /
              (T(16) * (msq - s) * (msq - s - t) * t);

    // The analytic result above already assumed H^{munu} = 1/2*ep^mu*ep^nu while Caravel assumed H^{munu} = ep^mu*ep^nu,
    // thus to undo this we have to include a factor 4.
    // Then in Caravel's convention the amplitude has an overall coupling of (kappa/2)^2, thus we have to include another factor of 4.
    T amp = ampVV * T(0, 16);

    return amp;
}
} // namespace Caravel
