#include "Forest/Forest.h"
#include "PhaseSpace/PhaseSpace.h"
#include "Forest/Builder.h"
#include "Core/Particle.h"
#include "Core/momD_conf.h"
#include "Core/Utilities.h"
#include "Core/typedefs.h"
#include "Core/type_traits_extra.h"
#include "wise_enum.h"
#include <algorithm>
#include <cmath>
#include <exception>
#include <iostream>
#include <string>
#include <vector>


using namespace std;
using namespace Caravel;

namespace Color {
enum struct Code { FG_RED = 31, FG_GREEN = 32, FG_BLUE = 34, FG_DEFAULT = 39, BG_RED = 41, BG_GREEN = 42, BG_BLUE = 44, BG_DEFAULT = 49 };
}
namespace std {
std::ostream& operator<<(std::ostream& os, const Color::Code& mod) { return os << "\033[" << static_cast<int>(mod) << "m"; }
} // namespace std

Particle* qm(int i) { return new Particle("qm ", ParticleType("q"), SingleState("qm"), i, 0); }
Particle* qbp(int i) { return new Particle("qbp ", ParticleType("qb"), SingleState("qbp"), i, 0); }
Particle* p(int i) { return new Particle("p ", ParticleType("gluon"), SingleState("p"), i, 0); }
Particle* m(int i) { return new Particle("m ", ParticleType("gluon"), SingleState("m"), i, 0); }
Particle* W(int i) { return new Particle("Ward ", ParticleType("gluon"), SingleState("Ward"), i, 0); }
Particle* Tp(int i, int r = 0) { return new Particle("Tp ", ParticleType("t"), SingleState("Qp"), i, r); }
Particle* Tm(int i, int r = 0) { return new Particle("Tm ", ParticleType("t"), SingleState("Qm"), i, r); }
Particle* Tbp(int i, int r = 0) { return new Particle("Tbp ", ParticleType("tb"), SingleState("Qbp"), i, r); }
Particle* Tbm(int i, int r = 0) { return new Particle("Tbm ", ParticleType("tb"), SingleState("Qbm"), i, r); }
Particle* H(int i) { return new Particle("H ", ParticleType("H"), SingleState("SC"), i, 0); }

vector<C> getTargets_4(momD_conf<C, 4> mc, C mass, size_t rt, size_t rtb) {

    vector<C> target;
    target.clear();

    // overall factor of 1/sqrt(2)^n from color decomposition
    C col = C(1) / C(2);

    momD<C,4> q1, q4;
    if( rt == 0 ) q1 = clifford_algebra::get_helper_momD<C,4>(1);
    else q1 = mc[rt];
    if( rtb == 0 ) q4 = clifford_algebra::get_helper_momD<C,4>(1);
    else q4 = mc[rtb];

    auto p1b = mc[1] - mass * mass / (C(2) * C(mc[1] * q1)) * q1;
    auto p4b = mc[4] - mass * mass / (C(2) * C(mc[4] * q4)) * q4;

    // A4[t(1,+),g(2,+),g(3,+),tb(4,+)]
    C amp = -C(0, 2) * mass * spaa(q1, q4) / spaa(q1, p1b) / spaa(p4b, q4) * spbb(mc[2], mc[3]) / spaa(mc[2], mc[3]) * mass * mass / (C(2) * C(mc[1] * mc[2]));
    target.push_back(amp * col);

    // A4[t(1,+),g(2,+),g(3,+),tb(4,-)]
    amp = C(0, 2) * spbb(mc[2], mc[3]) / spaa(mc[2], mc[3]) * mass * mass / (C(2) * C(mc[1] * mc[2])) * spab(q1, mc[4], q4) / spaa(q1, p1b) / spbb(p4b, q4);
    target.push_back(amp * col);

    // A4[t(1,-),g(2,+),g(3,+),tb(4,+)]
    amp = -C(0, 2) * spbb(mc[2], mc[3]) / spaa(mc[2], mc[3]) * mass * mass / (C(2) * C(mc[1] * mc[2])) * spba(q1, mc[1], q4) / spbb(q1, p1b) / spaa(p4b, q4);
    target.push_back(amp * col);

    // A4[t(1,-),g(2,+),g(3,+),tb(4,-)]
    amp = C(0, 2) * spbb(mc[2], mc[3]) / spaa(mc[2], mc[3]) * mass / (C(2) * C(mc[1] * mc[2])) * spbb(q1, mc[1], mc[4], q4) / spbb(q1, p1b) / spbb(p4b, q4);
    target.push_back(amp * col);

    // A4[t(1,+),g(2,-),g(3,+),tb(4,+)]
    amp = C(0, 2) * mass / spaa(q1, p1b) / spaa(p4b, q4) * spab(mc[2], mc[4], mc[3]) / (C(2) * C(mc[1] * mc[2]));
    amp = amp * (spaa(q1, mc[2]) * spaa(mc[2], q4) / spaa(mc[2], mc[3]) - spab(mc[2], mc[4], mc[3]) * spaa(q1, q4) / (C(2) * C(mc[2] * mc[3])));
    target.push_back(amp * col);

    // A4[t(1,+),g(2,-),g(3,+),tb(4,-)]
    amp = C(0, 2) / spaa(q1, p1b) / spbb(p4b, q4) * spab(mc[2], mc[4], mc[3]) / (C(2) * C(mc[1] * mc[2]));
    amp = amp * (spab(mc[2], mc[4], mc[3]) * spab(q1, mc[4], q4) / (C(2) * C(mc[2] * mc[3])) - spaa(q1, mc[2]) * spab(mc[2], mc[4], q4) / spaa(mc[2], mc[3]));
    target.push_back(amp * col);

    return target;
}

vector<C> getTargets_5(momD_conf<C, 4> mc, C mass, size_t rt, size_t rtb) {

    vector<C> target;
    target.clear();

    // overall factor of 1/sqrt(2)^n from color decomposition
    double n = 5;
    C col = C(1. / pow(sqrt(2), n - 2));

    auto q1 = mc[rt];
    auto q5 = mc[rtb];
    auto p1b = mc[1] - mass * mass / (C(2) * C(mc[1] * q1)) * q1;
    auto p5b = mc[5] - mass * mass / (C(2) * C(mc[5] * q5)) * q5;

    // scalar amplitude
    auto P12 = mc[1] + mc[2];
    auto P13 = P12 + mc[3];
    auto P23 = mc[2] + mc[3];
    auto P24 = P23 + mc[4];
    C y12 = P12 * P12 - mass * mass;
    C y13 = P13 * P13 - mass * mass;
    C A5 = C(0, pow(2, n / 2. - 1)) * mass * mass * spbb(mc[2], mc[1], P23, mc[4]) / (y12 * y13 * spaa(mc[2], mc[3]) * spaa(mc[3], mc[4]));

    // A5[t(1,+),g(2,+),g(3,+),g(4,+),tb(5,-)]
    C amp = spaa(p5b, q1) / spaa(p1b, q1) * A5;
    target.push_back(amp * col);

    // A5[t(1,-),g(2,+),g(3,+),g(4,+),tb(5,-)]
    amp = spaa(p1b, p5b) / mass * A5;
    target.push_back(amp * col);

    // A5[t(1,+),g(2,-),g(3,+),g(4,+),tb(5,-)]
    amp = C(0, pow(2, n / 2. - 1)) * spaa(p5b, mc[2]) / spaa(p1b, mc[2]) / (spaa(mc[2], mc[3]) * spaa(mc[3], mc[4]));
    amp = amp * (spaa(mc[2], mc[1], P24, mc[2]) * spaa(mc[2], mc[1], P24, mc[2]) / (C(P24 * P24) * spaa(mc[2], mc[1], P24, mc[4])) +
                 mass * mass * spab(mc[2], P23, mc[4]) * spaa(mc[3], mc[4]) / (y13 * spaa(mc[2], mc[1], P23, mc[4])) * spaa(mc[2], mc[1], P23, mc[2]) *
                     spaa(mc[2], mc[1], P23, mc[2]) / (C(P23 * P23) * spaa(mc[2], mc[1], P23, mc[3])));
    target.push_back(amp * col);

    // A5[t(1,-),g(2,-),g(3,+),g(4,+),tb(5,-)]
    C X = C(1) + C(P24 * P24) * spaa(mc[2], p5b) / spaa(mc[2], P24, p1b, p5b);
    C Y = C(1) + C(P23 * P23) * spaa(mc[2], p5b) / spaa(mc[2], P23, p1b, p5b);
    amp = C(0, pow(2, n / 2. - 1)) * spaa(p1b, p5b) / mass / (spaa(mc[2], mc[3]) * spaa(mc[3], mc[4]));
    amp = amp * (spaa(mc[2], mc[1], P24, mc[2]) * spaa(mc[2], mc[1], P24, mc[2]) / (C(P24 * P24) * spaa(mc[2], mc[1], P24, mc[4])) * X +
                 mass * mass * spab(mc[2], P23, mc[4]) * spaa(mc[3], mc[4]) / (y13 * spaa(mc[2], mc[1], P23, mc[4])) * spaa(mc[2], mc[1], P23, mc[2]) *
                     spaa(mc[2], mc[1], P23, mc[2]) / (C(P23 * P23) * spaa(mc[2], mc[1], P23, mc[3])) * Y);
    target.push_back(amp * col);

    return target;
}

size_t checkME(vector<vector<Particle*>> processes) {

    vector<Particle*> p = processes[0];
    vector<double> masses;
    masses.clear();

    // find masses and couplings
    unsigned N = p.size();
    int Nscalar = 0, Nfermion = 0, Ngluon = 0;
    size_t rt = 0, rtb = 0;
    for (unsigned i = 0; i < N; i++) {
        ParticleStatistics spin = p.at(i)->get_statistics();
        if (spin == ParticleStatistics::vector) Ngluon++;
        if (spin == ParticleStatistics::fermion) Nfermion++;
        if (spin == ParticleStatistics::antifermion) Nfermion++;
        if (spin == ParticleStatistics::scalar) Nscalar++;
        masses.push_back(p.at(i)->get_mass(0.0));
        if (p.at(i)->get_flavor() == ParticleFlavor::t) rt = p.at(i)->ref_index();
        if (p.at(i)->get_flavor() == ParticleFlavor::tb) rtb = p.at(i)->ref_index();
    }
    int yTpow = Nscalar;
    int gspow = Ngluon + Nfermion - 2;

    SM_color_ordered model;    
    Forest SW(&model);

    vector<unsigned> tracks(processes.size(), 0);
    for (unsigned i = 0; i < processes.size(); i++) { tracks[i] = SW.add_process(Process(processes[i]), {{"gs", gspow}, {"yt", yTpow}}); }

    vector<size_t> dims{4};
    SW.define_internal_states_and_close_forest(dims);
    Builder<C, 4> tree(&SW, N);

    double energy = 1300.0;
    PhaseSpace<R> Tph(N, energy, masses);
    Tph.set_reset_seed(false);
    srand(1010);
    Tph.generate();
    auto moms = Tph.mom_double();
    auto mc = to_momDconf<R, 4>(moms);

    tree.set_p(mc);
    tree.compute();
    tree.contract();

    vector<C> target;
    target.clear();
    if (N == 4) target = getTargets_4(mc, masses[0], rt, rtb);
    if (N == 5) target = getTargets_5(mc, masses[0], rt, rtb);

    size_t err = 0;
    double tolerance = 1e-11;
    for (unsigned i = 0; i < processes.size(); i++) {
        C amp = tree.get_tree(tracks[i]);
        C ratio = target[i] / amp;
        if (abs(amp) < tolerance && abs(target[i]) < tolerance) continue;
        if (fabs(abs(ratio) - 1.0) > tolerance) {
            err++;
            cout << "Not a phase! " << i << "\t" << amp << "\t" << target[i] << "\t" << ratio << "\t" << abs(ratio) - 1.0 << endl;
        }
    }

    return err;
}

size_t flavRotation(vector<Particle*> p) {
    _MESSAGE("");
    _MESSAGE("Checking process ", Process(p));

    vector<vector<Particle*>> processes = {p};
    vector<double> masses;
    masses.clear();

    // find masses and couplings
    unsigned N = p.size();
    int Nscalar = 0, Nfermion = 0, Ngluon = 0;
    for (unsigned i = 0; i < N; i++) {
        ParticleStatistics spin = p.at(i)->get_statistics();
        if (spin == ParticleStatistics::vector) Ngluon++;
        if (spin == ParticleStatistics::fermion) Nfermion++;
        if (spin == ParticleStatistics::antifermion) Nfermion++;
        if (spin == ParticleStatistics::scalar) Nscalar++;
        masses.push_back(p.at(i)->get_mass(0.0));
    }
    int yTpow = Nscalar;
    int gspow = Ngluon + Nfermion - 2;

    // Flavor rotate
    for (unsigned i = 0; i < N; i++) {
        rotate(p.begin(), p.begin() + 1, p.end());
        processes.push_back(p);
    }

    SM_color_ordered model;
    Forest SW(&model);

    vector<unsigned> tracks(N + 1, 0);
    for (unsigned i = 0; i < processes.size(); i++) { tracks[i] = SW.add_process(Process(processes[i]), {{"gs", gspow}, {"yt", yTpow}}); }

    vector<size_t> dims{4};
    SW.define_internal_states_and_close_forest(dims);
    Builder<C, 4> tree(&SW, N);

    double energy = 1300.0;
    PhaseSpace<R> Tph(N, energy, masses);
    Tph.set_reset_seed(false);
    srand(1010);
    Tph.generate();
    auto moms = Tph.mom_double();
    auto mc = to_momDconf<R, 4>(moms);

    tree.set_p(mc);
    tree.compute();
    tree.contract();

    size_t err = 0;
    double tolerance = 1e-11;
    for (unsigned i = 1; i < processes.size(); i++) {
        auto diff = tree.get_tree(tracks[0]) - tree.get_tree(tracks[i]);
        if (fabs(diff.real()) > tolerance || fabs(diff.imag()) > tolerance) {
            err++;
            cout << i << "\t" << diff << endl;
        }
    }

    return err;
}

int main(int argc, char* argv[]) {

    // number of errors
    size_t err = 0;

    vector<vector<Particle*>> procs = {
        // 0 -> ttgg
        {Tp(1, 3), Tbp(2, 4), p(3), p(4)},
        {Tp(1, 3), Tbm(2, 4), p(3), p(4)},
        {Tm(1, 3), Tbm(2, 4), p(3), p(4)},
        {Tm(1, 3), Tbp(2, 4), p(3), p(4)},
        {Tm(1, 3), Tbp(2, 4), p(3), m(4)},
        {Tp(1, 3), Tbm(2, 4), m(3), p(4)},
        {Tp(1, 3), Tbp(2, 4), p(3), W(4)}, // WARD
        {Tp(1, 3), Tbm(2, 4), W(3), p(4)}, // WARD
        // 0 -> ttggg
        {Tp(1, 3), Tbp(2, 4), p(3), p(4), p(5)},
        {Tp(1, 3), Tbm(2, 5), p(3), p(4), m(5)},
        {Tm(1, 4), Tbm(2, 5), p(3), p(4), p(5)},
        {Tm(1, 3), Tbp(2, 4), p(3), p(4), m(5)},
        {Tm(1, 5), Tbp(2, 4), p(3), m(4), p(5)},
        {Tp(1, 3), Tbm(2, 4), m(3), p(4), m(5)},
        // 0 -> ttgggg
        {Tp(1, 3), Tbp(2, 4), p(3), p(4), p(5), p(6)},
        {Tp(1, 3), Tbm(2, 5), p(3), p(4), m(5), p(6)},
        {Tm(1, 4), Tbm(2, 5), p(3), p(4), p(5), m(6)},
        {Tm(1, 3), Tbp(2, 4), p(3), p(4), m(5), p(6)},
        {Tm(1, 5), Tbp(2, 4), p(3), m(4), p(5), m(6)},
        {Tp(1, 3), Tbm(2, 6), m(3), p(4), m(5), m(6)},
        {Tp(1, 3), Tbm(2, 6), m(3), p(4), W(5), m(6)}, // WARD
        // 0 -> ttqq
        {Tp(1, 3), Tbp(2, 4), qm(3), qbp(4)},
        {Tp(1, 3), Tbm(2, 4), qbp(3), qm(4)},
        {Tm(1, 3), Tbm(2, 4), qbp(3), qm(4)},
        {Tm(1, 3), Tbp(2, 4), qm(3), qbp(4)},
        {Tm(1, 3), Tbp(2, 4), qbp(3), qm(4)},
        {Tp(1, 3), Tbm(2, 4), qm(3), qbp(4)},
        // 0 -> ttqqg
        {Tp(1, 3), Tbp(2, 4), qm(3), qbp(4), p(5)},
        {Tp(1, 3), Tbm(2, 5), qbp(3), qm(4), m(5)},
        {Tm(1, 3), Tbm(2, 4), qbp(3), qm(4), p(5)},
        {Tm(1, 3), Tbp(2, 4), qm(3), qbp(4), m(5)},
        {Tm(1, 5), Tbp(2, 4), qbp(3), qm(4), p(5)},
        {Tp(1, 3), Tbm(2, 5), qm(3), qbp(4), m(5)},
        // 0 -> ttqqgg
        {Tp(1, 6), Tbp(2, 4), qm(3), qbp(4), p(5), m(6)},
        {Tp(1, 3), Tbm(2, 5), qbp(3), qm(4), m(5), p(6)},
        {Tm(1, 3), Tbm(2, 4), qbp(3), qm(4), p(5), m(6)},
        {Tm(1, 6), Tbp(2, 4), qm(3), qbp(4), m(5), p(6)},
        {Tm(1, 5), Tbp(2, 4), qbp(3), qm(4), p(5), m(6)},
        {Tp(1, 3), Tbm(2, 5), qm(3), qbp(4), m(5), p(6)},
        // 0 -> ttqqqq
        {Tp(1, 6), Tbp(2, 4), qm(3), qbp(4), qbp(5), qm(6)},
        {Tp(1, 3), Tbm(2, 5), qbp(3), qm(4), qm(5), qbp(6)},
        {Tm(1, 3), Tbm(2, 4), qbp(3), qm(4), qbp(5), qm(6)},
        {Tm(1, 6), Tbp(2, 4), qm(3), qbp(4), qm(5), qbp(6)},
        {Tm(1, 5), Tbp(2, 4), qbp(3), qm(4), qbp(5), qm(6)},
        {Tp(1, 3), Tbm(2, 5), qm(3), qbp(4), qm(5), qbp(6)},
        // 0 -> ttggH
        {Tp(1, 3), Tbp(2, 4), p(3), p(4), H(5)},
        {Tp(1, 3), Tbm(2, 4), p(3), p(4), H(5)},
        {Tm(1, 3), Tbm(2, 4), p(3), p(4), H(5)},
        {Tm(1, 3), Tbp(2, 4), p(3), p(4), H(5)},
        {Tm(1, 3), Tbp(2, 4), p(3), m(4), H(5)},
        {Tp(1, 3), Tbm(2, 4), m(3), p(4), H(5)},
        {Tp(1, 3), Tbm(2, 4), m(3), W(4), H(5)}, // WARD
        // 0 -> ttqqH
        {Tp(1, 3), Tbp(2, 4), qm(3), qbp(4), H(5)},
        {Tp(1, 3), Tbm(2, 5), qbp(3), qm(4), H(5)},
        {Tm(1, 3), Tbm(2, 4), qbp(3), qm(4), H(5)},
        {Tm(1, 3), Tbp(2, 4), qm(3), qbp(4), H(5)},
        {Tm(1, 5), Tbp(2, 4), qbp(3), qm(4), H(5)},
        {Tp(1, 3), Tbm(2, 5), qm(3), qbp(4), H(5)},
        // 0 -> ttttgg
        {Tp(1, 5), Tbp(2, 6), Tp(3, 5), Tbp(4, 6), p(5), p(6)},
        {Tm(1, 5), Tbp(2, 6), Tp(3, 5), Tbm(4, 6), p(5), m(6)},
        {Tp(1, 5), Tbm(2, 6), Tm(3, 5), Tbp(4, 6), m(5), p(6)},
        {Tp(1, 5), Tbm(2, 6), Tm(3, 5), Tbm(4, 6), m(5), m(6)},
        {Tp(1, 5), Tbm(2, 6), Tm(3, 5), Tbm(4, 6), m(5), W(6)}, // WARD
    };

    for (unsigned n = 0; n < procs.size(); n++) { err += flavRotation(procs[n]); }

    // Test 4 particle amplitudes
    vector<vector<Particle*>> HMEttgg = {
        {Tp(1, 2), p(2), p(3), Tbp(4, 3)}, {Tp(1, 2), p(2), p(3), Tbm(4, 3)}, {Tm(1, 2), p(2), p(3), Tbp(4, 3)},
        {Tm(1, 2), p(2), p(3), Tbm(4, 3)}, {Tp(1, 2), m(2), p(3), Tbp(4, 3)}, {Tp(1, 2), m(2), p(3), Tbm(4, 3)},
    };
    err += checkME(HMEttgg);

    // Test 4 particle amplitudes with standard reference vector
    vector<vector<Particle*>> HMEttgg_test = {
        {Tp(1), p(2), p(3), Tbp(4)}, {Tp(1), p(2), p(3), Tbm(4)}, {Tm(1), p(2), p(3), Tbp(4)},
        {Tm(1), p(2), p(3), Tbm(4)}, {Tp(1), m(2), p(3), Tbp(4)}, {Tp(1), m(2), p(3), Tbm(4)},
    };
    err += checkME(HMEttgg_test);

    // Test 5 particle amplitudes
    vector<vector<Particle*>> HMEttggg = {
        {Tp(1, 2), p(2), p(3), p(4), Tbm(5, 2)},
        {Tm(1, 2), p(2), p(3), p(4), Tbm(5, 2)},
        {Tp(1, 2), m(2), p(3), p(4), Tbm(5, 2)},
        {Tm(1, 2), m(2), p(3), p(4), Tbm(5, 2)},
    };
    err += checkME(HMEttggg);

    _MESSAGE("");
    _MESSAGE("===============================");
    _MESSAGE("TOTAL NUMBER OF ERRORS: ", err);
    _MESSAGE("===============================");

    return err;
}
