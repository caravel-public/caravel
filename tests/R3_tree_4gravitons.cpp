/**
 * R3_tree_4gravitons.cpp
 *
 * 4-pt of R3 counterterms for 2-loop 4-graviton calculation in EHGravity model.
 * Only checks vanishing of contribution for MHV configuration, and non vanishing
 * for the rest!
*/

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>

#include "Core/typedefs.h"
#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "Forest/Builder.h"
#include "Forest/Model.h"
#include "Core/Debug.h"
#include "Forest/Forest.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

using namespace std;
using namespace Caravel;

int main(int argc, char* argv[]) {
    using F = F32;

    // Create the external particles (all massless)
    Particle G1mm("G1mm", ParticleType("G"), SingleState("hmm"), 1, 0);
    Particle G1pp("G1pp", ParticleType("G"), SingleState("hpp"), 1, 0);
    Particle G2mm("G2mm", ParticleType("G"), SingleState("hmm"), 2, 0);
    Particle G2pp("G2pp", ParticleType("G"), SingleState("hpp"), 2, 0);
    Particle G3mm("G3mm", ParticleType("G"), SingleState("hmm"), 3, 0);
    Particle G3pp("G3pp", ParticleType("G"), SingleState("hpp"), 3, 0);
    Particle G4mm("G4mm", ParticleType("G"), SingleState("hmm"), 4, 0);
    Particle G4pp("G4pp", ParticleType("G"), SingleState("hpp"), 4, 0);

    // Vector of particle pointers

    std::vector<Particle*> particles4pppp = {&G1pp, &G2pp, &G3pp, &G4pp};
    std::vector<Particle*> particles4mppp = {&G1mm, &G2pp, &G3pp, &G4pp};
    std::vector<Particle*> particles4mmpp = {&G1mm, &G2mm, &G3pp, &G4pp};

    Process process_tree4pppp(particles4pppp);
    Process process_tree4mppp(particles4mppp);
    Process process_tree4mmpp(particles4mmpp);

    cutTopology::couplings_t treecouplings4{{"K", 6}, {"cR3", 1}};

    // construct the 'trivial' cutTopology for this tree

    cutTopology tree4ppppp(process_tree4pppp, treecouplings4);
    cutTopology tree4mpppp(process_tree4mppp, treecouplings4);
    cutTopology tree4mmppp(process_tree4mmpp, treecouplings4);

    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------

    // pick the model
    EHGravity model2use;

    // Now construct the Forest
    Forest SW4(&model2use);

    int track_tree4pppp;
    int track_tree4mppp;
    int track_tree4mmpp;

    // best to use add_process(.) for trees (for simpler access to info)

    track_tree4pppp = SW4.add_process(process_tree4pppp, treecouplings4);
    track_tree4mppp = SW4.add_process(process_tree4mppp, treecouplings4);
    track_tree4mmpp = SW4.add_process(process_tree4mmpp, treecouplings4);

    // Close forest
    vector<size_t> dims{4};

    SW4.define_internal_states_and_close_forest(dims);

    std::vector<BigRat> xs = {BigRat(-3) / BigRat(14), BigRat(-11) / BigRat(15)};

    auto ps_rat_r = ps_parameterizations::rational_mom_conf<BigRat>(4, xs /*, false*/);

    std::cout << "MOMENTA: " << ps_rat_r << std::endl;
    std::cout << " s12 = " << ps_rat_r.s(1, 2) << std::endl;
    std::cout << " s23 = " << ps_rat_r.s(2, 3) << std::endl;
    std::cout << " s13 = " << ps_rat_r.s(1, 3) << std::endl;

    momD_conf<F, 4> momconf_process(ps_rat_r);

    // Now build Builder

    Builder<F, 4> treecutSource4(&SW4, 4);

    treecutSource4.set_p(momconf_process);

    // compute all tree currents
    treecutSource4.compute();

    // Make contractions to compute trees
    treecutSource4.contract();

    auto A4pppp = treecutSource4.get_tree(track_tree4pppp);
    auto A4mppp = treecutSource4.get_tree(track_tree4mppp);
    auto A4mmpp = treecutSource4.get_tree(track_tree4mmpp);

    std::cout << "Trees computed: " << A4pppp << " " << A4mppp << " " << A4mmpp << std::endl;

    int toret(0);

    if (A4mmpp != F(0)) toret++;
    if (A4pppp == F(0)) toret++;
    if (A4mppp == F(0)) toret++;

    return toret;
}
