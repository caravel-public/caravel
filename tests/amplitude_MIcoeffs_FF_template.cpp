#include "Core/RationalReconstruction.h"
#include "Forest/Model.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "misc/CaravelCereal.hpp"

#include "Core/MathInterface.h"
#include "Core/Timing.h"
#include "misc/TestUtilities.hpp"
#include "Core/CaravelInput.h"

#include <algorithm>

//#define GENERATE_TARGETS_MODE

int main(int argc, char* argv[]) {
    
    using namespace Caravel;
    using namespace Caravel::Integrals;
    using namespace Caravel::IntegrandHierarchy;
    using F = F32;

    if (argc < 2) {
        _WARNING("ERROR: expecting some input!");
        std::exit(1);
    }

    verbosity_settings.show_all();

    settings::use_setting("general::do_n_eq_n_test yes");
    settings::use_setting("IntegrandHierarchy::parallelize_topology_fitting warmup");
    settings::IntegrandHierarchy::topology_fitting_max_nthreads.value = std::min(4, (int)std::thread::hardware_concurrency());


    InputParser input{argc,argv};
    auto  pa = input.get_cmd_option<PartialAmplitudeInput>("PartialAmplitudeInput");
    size_t nparticles = pa.get_multiplicity();

    set_settings_for_standard_choice_of_integrals(nparticles);

    if (input.cmd_option_exists("CaravelSettingsInput")) {
        input.get_cmd_option<CaravelSettingsInput>("CaravelSettingsInput");
    }

    cardinality_update_manager<F>::get_instance()->set_cardinality(input.get_cmd_option<uint64_t>("PrimeF",2147483629));

    auto xs = Caravel::detail::read_twistor_parameters(input.get_cmd_option("xs"));

    SM_color_ordered model2use;

    auto point = get_mom_conf<F>(nparticles, xs);

    START_TIMER(construction);
    ColourExpandedAmplitude<F, 2> ampl(model2use,pa);
    STOP_TIMER(construction);

    START_TIMER(coefficients);
    auto result_coeffs = ampl.get_master_coefficients(point).front();
    STOP_TIMER(coefficients);


    size_t err=0;

    std::string filename = "test_data/" +pa.get_id_string() + std::string("_ampl_MIcoeffs_FF.dat");
#ifdef GENERATE_TARGETS_MODE
    write_serialized(result_coeffs,filename);
#endif
    auto target = read_serialized<decltype(result_coeffs)>(filename);

    for(auto [i,ci] : enumerate(target)) {
        if (ci != result_coeffs[i]) {
            _WARNING_RED("Coefficient ",i," doesn't match the target");
            _WARNING("got:\n",result_coeffs[i]);
            _WARNING("target:\n",ci);
            err++;
        }
    }

    if(err){
        _WARNING_RED(err,"/",target.size()," coefficients didn't match");
    }

    return err;
}


