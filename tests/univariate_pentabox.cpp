#include "Forest/Builder.h"
#include "FunctionalReconstruction/FunctionalReconstruction.h"
#include "AmplitudeCoefficients/Hierarchy.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"
#include <algorithm>
#include <unordered_map>
#include "Core/RationalReconstruction.h"

using namespace std;
using namespace Caravel;

// Loading four point hierarchy
int main(int argc, char* argv[]) {

    using namespace IntegrandHierarchy;
    

    constexpr size_t L = 2;
    // constexpr size_t D = 6;

    settings::use_setting("general::master_basis standard");

    size_t num_threads = 4;

    if (argc == 2) {
        num_threads = std::atoi(argv[1]);
        if (num_threads < 40) {
            _MESSAGE("Will run ", num_threads, " threads.");
        } else {
            _WARNING("Requested too many threads. I consider that as an intput error and run with 4 threads instead.");
        }
    } else if (argc > 2) {
        _WARNING("Too many arguments");
    } else {
        _MESSAGE("Will run ", num_threads, " threads.");
    }

    // number of threads to run

    // External process, put in order of momenta indices, this is important
    std::vector<Particle> process = {
        Particle("g", ParticleType("gluon"), SingleState("m"), 1),
        Particle("g", ParticleType("gluon"), SingleState("p"), 2),
        Particle("g", ParticleType("gluon"), SingleState("m"), 3),
        Particle("g", ParticleType("gluon"), SingleState("p"), 4),
        Particle("g", ParticleType("gluon"), SingleState("p"), 5),
    };

    settings::use_setting("IntegrandHierarchy::amplitude_normalization spinor_weight");

    auto model = make_unique<YM>();

    using F = F32;
    auto cardinality = 2147483629;
    F::set_p(cardinality);
    std::cout << "Running with cardinality " << cardinality << std::endl;

    ColourExpandedAmplitude<F, L> ampl(*model, PartialAmplitudeInput{process}, slice_selector(HierarchyPos({5, 8})));

    // do the warmup run if necessary
    {
        auto physical_point = rational_mom_conf<BigRat>(5, {BigRat(4), BigRat(1), BigRat(8), BigRat(7), BigRat(-1)});
        ampl.warmup_run(static_cast<momD_conf<F, 4>>(physical_point));
    }

    // Set up of univariate slice reconstruction

    function<F()> x_gen;
    std::mt19937 mt(rand());
    x_gen = [mt]() mutable {
        F result = random_scaled_number(F(1), mt);
        return result;
    };

    F t1 = 1714636915;
    F a1 = 1649760492;
    F t2 = 1957747793;
    F a2 = 596516649;
    F t3 = 424238335;
    F a3 = 1189641421;
    F t4 = 719885386;
    F a4 = 1025202362;

    std::cout << "Slice :" << std::endl;
    std::cout << "x0 = x*" << t1 << "-" << a1 << std::endl;
    std::cout << "x1 = x*" << t2 << "-" << a2 << std::endl;
    std::cout << "x2 = x*" << t3 << "-" << a3 << std::endl;
    std::cout << "x3 = x*" << t4 << "-" << a4 << std::endl;

    // Turn modulo_ibp_reduce into some vector valued univariate rational function

    vector<function<vector<F>(F)>> first_pentabox_coefficient_in_epsilon;
    {
        for (size_t i = 0; i < num_threads; i++) {
            auto new_evaluator = ampl.get_new_master_coefficients_evaluator();

            first_pentabox_coefficient_in_epsilon.push_back([&, ev = std::move(new_evaluator)](F x) {
                std::vector<F> five_xs = {x * t1 - a1, x * t2 - a2, x * t3 - a3, x * t4 - a4, F(1)};
                auto conf = rational_mom_conf<F>(5, five_xs);

                return ev(conf).front().front().numerator.coefficients;
            });
        }
    }

    // Perform the reconstruction. If the final argument is true, this
    // will be performed in parallel
    VectorThieleFunction<F> result(std::move(first_pentabox_coefficient_in_epsilon), x_gen, "t", true);
    vector<DenseRational<F>> canonicals;
    for (auto& component : result.components) { canonicals.push_back(component.to_canonical()); }

    std::cout << "canonicals = " << canonicals << std::endl;
    // Test value. This is quite arbitrary, and only true in the given
    // finite field cardinality.
    if (canonicals.back().denominator.coefficients.back() != F(1182620496)) {
        _MESSAGE("Unexpected value ",canonicals.back().denominator.coefficients.back()," in reconstruction.");
        exit(1);
    }

    return 0;
}
