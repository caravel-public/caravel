#include "Forest/Forest.h"
#include "PhaseSpace/PhaseSpace.h"

#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "Core/CaravelInput.h"
#include "Core/typedefs.h"

#include "AmpEng/CoefficientEngine.h"
#include "Forest/Builder.h"

#include "Core/Utilities.h"
#include "Core/settings.h"

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>
#include <algorithm>
#include <cassert>

#include "Core/type_traits_extra.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

using namespace std;
using namespace Caravel;
using namespace Caravel::AmpEng;
using namespace Caravel::lGraph;

#define _DIM_D 5

namespace Color {
enum struct Code {
    FG_RED = 31,
    FG_GREEN = 32,
    FG_BLUE = 34,
    FG_DEFAULT = 39,
    BG_RED = 41,
    BG_GREEN = 42,
    BG_BLUE = 44,
    BG_DEFAULT = 49
};
}
namespace std{
    std::ostream& operator<<(std::ostream& os, const Color::Code& mod) {
        return os << "\033[" << static_cast<int>(mod) << "m";
    }
}

Particle qm(){
    return Particle("qm ",ParticleType("q"),SingleState("qm"));
}
Particle qbp(){
    return Particle("qbp ",ParticleType("qb"),SingleState("qbp"));
}
Particle p(){
    return Particle("p ",ParticleType("gluon"),SingleState("p"));
}
Particle m(){
    return Particle("m ",ParticleType("gluon"),SingleState("m"));
}

C get_result_for_integral_coeff(const IntegralGraph& integral, const std::vector<IntegralGraph>& allintegrals, const std::vector<DenseRational<C>>& result) {
    auto match_integral = [&integral](const IntegralGraph& a) {
        return a.get_graph().get_base_graph() == integral.get_graph().get_base_graph() && a.get_insertion() == integral.get_insertion();
    };

    auto it = std::find_if(allintegrals.begin(), allintegrals.end(), match_integral);
    int ii = it - allintegrals.begin();

    return result[ii].numerator.coefficients[0];
}

int main(int argc, char* argv[]){

    _MESSAGE("=============================================================================================");
    _MESSAGE("Test one loop amplitudes with massless quarks. Comparing with BHlib_Massive targets.");
    _MESSAGE("=============================================================================================");
    _MESSAGE("We have a strange minus sign for mu2 coeffs");
    _MESSAGE("=============================================================================================");

    settings::use_setting("general::cut_propagator_numerators states");
    settings::use_setting("BG::partial_trace_normalization full_Ds");
    settings::use_setting("IntegrandHierarchy::amplitude_normalization none");

    // number of errors
    size_t err = 0;

    // Put processes to test here
    vector<vector<Particle>> all = {
        //{m(),p(),qm(),qbp()},
        {m(),m(),p(),qm(),qbp()},
        //{m(),p(),m(),p()},
    };


    for(unsigned n =0; n<all.size(); n++){
        auto& particles = all.at(n);
        // Building the forest
        cout << Color::Code::FG_BLUE;
        _MESSAGE("");
        _MESSAGE("Checking process ", particles);
        _MESSAGE(" > Building the forest: ");
        cout << Color::Code::FG_DEFAULT;
        // build forest for cuts

        //set all momenta indices trivially
        for(unsigned i=1; i<=particles.size(); i++){
            particles.at(i-1).set_momentum_index(i);
        }
        PartialAmplitudeInput pa{particles};

        _PRINT(pa);
        std::vector<momD<C,4>> particle_momenta {
            // p1
            momD<C,4>(1.86058205404180962,0.22322341633486098,1.29381396176358134,1.31832557381242467),
                // p2
                momD<C,4>(2.22702886899219324,-1.97843090981622047,0.18090821896703733,1.00635030417771722),
                // p3
                momD<C,4>(1.77552105445334353,-1.61888957780145734,0.46152052119123634,-0.56450895317284531),
                // p4
                momD<C,4>(-4.59937300385497873,4.17419448896838902,-0.96173464232271022,-1.67493249852413707),
                // p5
                momD<C,4>(-1.26375897363236766,-0.80009741768557219,-0.97450805959914479,-0.08523442629315951)
        };

        //C tree(-3.80804348475593468e-01,7.03631713969181560e-01);

        vector<pair<C,C>> values = {
            {C(4.49325311166651709e+00,2.14968749857620711e+00),C(-1.20438080914375689e-03,8.18917790399449098e-03) },
            {C(-7.33910997785774377e+00,-2.82081619105325920e+00),0},
            {C(1.91717194052284223e+00,4.06458148398178398e-01),0},
            {C(-1.26762645285644959e-01,-3.06535978634502049e-01),C(9.02214687010522744e-03,2.31761678296065927e-02)},
            {0.,C(2.05221660253739127e-02,-9.78573286094099881e-02)}
        };
        // integrals corresponding to 'values'
        vector<pair<IntegralGraph, IntegralGraph>> refintegrals = {
            {IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[4,0],Leg[5,0]]],Connection[Strand[Link[0],Bead[Leg[2,0],Leg[3,0]],Link[LoopMomentum[1],0]]]]"), "scalar"),
              IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[4,0],Leg[5,0]]],Connection[Strand[Link[0],Bead[Leg[2,0],Leg[3,0]],Link[LoopMomentum[1],0]]]]"), "mu2")},
            {IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[2,0],Leg[5,0]]],Connection[Strand[Link[0],Bead[Leg[3,0],Leg[4,0]],Link[LoopMomentum[1],0]]]]"), "scalar"),
              IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[2,0],Leg[5,0]]],Connection[Strand[Link[0],Bead[Leg[3,0],Leg[4,0]],Link[LoopMomentum[1],0]]]]"), "mu2")},
            {IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[5,0]]],Connection[Strand[Link[0],Bead[Leg[2,0],Leg[3,0],Leg[4,0]],Link[LoopMomentum[1],0]]]]"), "scalar"),
              IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[5,0]]],Connection[Strand[Link[0],Bead[Leg[2,0],Leg[3,0],Leg[4,0]],Link[LoopMomentum[1],0]]]]"), "mu2")},
            {IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[2,0],Leg[3,0]]],Connection[Strand[Link[0],Bead[Leg[4,0],Leg[5,0]],Link[LoopMomentum[1],0]]]]"), "scalar"),
              IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[2,0],Leg[3,0]]],Connection[Strand[Link[0],Bead[Leg[4,0],Leg[5,0]],Link[LoopMomentum[1],0]]]]"), "mu2")},
            {IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[2,0]]],Connection[Strand[Link[0],Bead[Leg[3,0],Leg[4,0],Leg[5,0]],Link[LoopMomentum[1],0]]]]"), "scalar"),
              IntegralGraph(MathList("CaravelGraph[Nodes[Node[Leg[1,0],Leg[2,0]]],Connection[Strand[Link[0],Bead[Leg[3,0],Leg[4,0],Leg[5,0]],Link[LoopMomentum[1],0]]]]"), "mu2")},
        };

        //auto moms = tph.mom_double();
        //auto momconf_process = to_momDconf<R,4>(moms);

        momD_conf<C,4> momconf_process(particle_momenta);

        // The model to use
        SM_color_ordered SM;
        CoefficientEngine<C> ampl(std::make_shared<Model>(SM), pa);
        // check if warmup is available
        if(!ampl.has_warmup()) {
            CoefficientEngine<F32> amplF(std::make_shared<Model>(SM), pa);
            _MESSAGE("Doing warmup run");
            momD_conf<F32, 4> psp_warmup = Caravel::ps_parameterizations::random_rational_mom_conf<F32>(particles.size());
            amplF.compute(psp_warmup);
            ampl.reload_warmup();
        }

        auto integrals = ampl.get_integral_graphs();
        auto result = ampl.compute(momconf_process);

        for(unsigned i1 =0; i1< values.size(); i1++){
            {
                auto a = compute_accuracy(get_result_for_integral_coeff(refintegrals[i1].first, integrals, result),values[i1].first);
                if(a>7.){
                    _MESSAGE("Bubble (cut coeff) ",i1," \t\t",Color::Code::FG_GREEN,"PASS",Color::Code::FG_DEFAULT, "\t acc: ",a);
                }
                else{
                    err++;
                    _MESSAGE("Bubble (cut coeff) ",i1," \t\t",Color::Code::FG_RED,"FAIL",Color::Code::FG_DEFAULT, "\t acc: ",a);
                }
            }

            {
                //!!!!!! For some reason additional factor only for mu2 coefficients is required.
                constexpr double bhscalarfac = -1;
                auto a = compute_accuracy(get_result_for_integral_coeff(refintegrals[i1].second, integrals, result),bhscalarfac*values[i1].second);
                if(a>7.){
                    _MESSAGE("Bubble (rat coeff) ",i1," \t\t",Color::Code::FG_GREEN,"PASS",Color::Code::FG_DEFAULT, "\t acc: ",a);
                }
                else{
                    err++;
                    _MESSAGE("Bubble (rat coeff) ",i1," \t\t",Color::Code::FG_RED,"FAIL",Color::Code::FG_DEFAULT, "\t acc: ",a);
                }
            }
        }
    }


    _MESSAGE("");
    _MESSAGE("===============================");
    _MESSAGE("TOTAL NUMBER OF ERRORS: ",err);
    _MESSAGE("===============================");

    return err;
}


