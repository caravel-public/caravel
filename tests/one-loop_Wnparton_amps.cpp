/**
 * Numerical test of 1-loop W+2q+{2,3}g amplitudes to O(ep^0)
 * reproducing Table I in arXiv:0808.0941
 */

#include <functional>
#include <iostream>
#include <cstdlib>
#include <random>
#include <sstream>
#include <vector>
#include <cmath>

#include "Core/settings.h"
#include "Core/typedefs.h"

#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "AmpEng/CoefficientEngine.h"
#include "IntegralsBH/ProviderBH.h"
#include "Amplitude.h"

#include "misc/TestUtilities.hpp"

using namespace std;
using namespace Caravel;
#ifdef USE_FINITE_FIELDS
#ifdef USE_EXTENDED_T
using F = F32;
#endif
#endif

struct check_amp {
    std::vector<Particle> gluons; /**< gluons to be trapped between quarks */
    C mu;
    C double_pole;
    C single_pole;
    C finite;
    R threshold;
};

momD_conf<C, 4> get_BBDFK_psp(size_t n);

int main(int argc, char* argv[]) {

    // choose the integral family and normalization
    settings::use_setting("integrals::integral_family_1L integrals_BH");
    settings::use_setting("BG::partial_trace_normalization full_Ds");
    settings::use_setting("IntegrandHierarchy::amplitude_normalization tree");

#ifdef USE_FINITE_FIELDS
#ifdef USE_EXTENDED_T
    // this is in case warmup necessary
    size_t cardinality(2147483629);
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);
#endif
#endif

    // data extracted from Table I of arXiv:0808.0941
    std::vector<check_amp> amplitudes = {
        {{Particle("g2", ParticleType("gluon"), SingleState("p"), 2, 0), Particle("g3", ParticleType("gluon"), SingleState("p"), 3, 0)},
         C(6),
         C(-3),
         C(-5.86428, -6.28319),
         C(-7.50750, -12.90848),
         R(0.001)},

        {{Particle("g2", ParticleType("gluon"), SingleState("p"), 2, 0), Particle("g3", ParticleType("gluon"), SingleState("m"), 3, 0)},
         C(6),
         C(-3),
         C(-5.86428, -6.28319),
         C(0.10598, -11.74860),
         R(0.001)},

        {{Particle("g2", ParticleType("gluon"), SingleState("m"), 2, 0), Particle("g3", ParticleType("gluon"), SingleState("p"), 3, 0)},
         C(6),
         C(-3),
         C(-5.86428, -6.28319),
         C(1.37392, -14.17999),
         R(0.001)},

        {{Particle("g2", ParticleType("gluon"), SingleState("p"), 2, 0), Particle("g3", ParticleType("gluon"), SingleState("p"), 3, 0),
          Particle("g4", ParticleType("gluon"), SingleState("p"), 4, 0)},
         C(7),
         C(-4),
         C(-10.43958, -9.42478),
         C(2.10030, -33.97042),
         R(0.001)},

        {{Particle("g2", ParticleType("gluon"), SingleState("p"), 2, 0), Particle("g3", ParticleType("gluon"), SingleState("p"), 3, 0),
          Particle("g4", ParticleType("gluon"), SingleState("m"), 4, 0)},
         C(7),
         C(-4),
         C(-10.43958, -9.42478),
         C(-9.25860, -33.31407),
         R(0.001)},

        {{Particle("g2", ParticleType("gluon"), SingleState("m"), 2, 0), Particle("g3", ParticleType("gluon"), SingleState("p"), 3, 0),
          Particle("g4", ParticleType("gluon"), SingleState("p"), 4, 0)},
         C(7),
         C(-4),
         C(-10.43958, -9.42478),
         C(-5.51046, -33.55522),
         R(0.01)},

        {{Particle("g2", ParticleType("gluon"), SingleState("m"), 2, 0), Particle("g3", ParticleType("gluon"), SingleState("p"), 3, 0),
          Particle("g4", ParticleType("gluon"), SingleState("m"), 4, 0)},
         C(7),
         C(-4),
         C(-10.43958, -9.42478),
         C(-6.36853, -29.29380),
         R(0.001)},
    };

    size_t errors(0);
    for (const auto& amplitude : amplitudes) {

        size_t n_gluons = amplitude.gluons.size();
        // we construct one-loop amplitude from explicit parent diagram
        Particle u("u1", ParticleType("u"), SingleState("qbp"), 1, 0);
        Particle db("db", ParticleType("db"), SingleState("qm"), n_gluons + 2, 0);
        Particle e("e", ParticleType("e"), SingleState("qm"), n_gluons + 3, 0);
        Particle veb("veb", ParticleType("veb"), SingleState("qbp"),  n_gluons + 4, 0);

        // loop particles
        Particle gL("gL", ParticleType("gluon"), SingleState("default"));
        Particle uL("uL", ParticleType("u"), SingleState("default"));
        Particle dL("dL", ParticleType("d"), SingleState("default"));

        std::vector<std::vector<Particle>> External = {{u}, {e, veb}, {db}};
        std::vector<Particle> Internal = {gL, uL, dL};

        using couplings_t = std::map<std::string, int>;
        couplings_t tree = {{"gs", n_gluons}, {"gw", 2}};
        std::vector<couplings_t> oneloop = {{{"gs", 1}}, {{"gw", 2}}, {{"gs", 1}}};

        // add gluons
        for(auto itg = amplitude.gluons.rbegin(); itg != amplitude.gluons.rend(); ++itg){
            External.push_back( { *itg } );
            Internal.push_back( gL );
            oneloop.push_back( {{"gs", 1}} );
        }

        size_t nparticles = 4 + n_gluons;

        // The model to use
        SM_color_ordered SM;

        // Construct the one-loop coefficient provider
        AmpEng::CoefficientEngine<C> coeffs(std::make_shared<Model>(SM), AmpEng::ParentDiagrams{{External}, {Internal}, {oneloop}, tree});

        // check if warmup present (limit in nparticles is just due to available exact n-pt parametrizations)
        if (!coeffs.has_warmup()) {
            // Construct the two-loop amplitude.
            AmpEng::CoefficientEngine<F> coeffs_exact(std::make_shared<Model>(SM), AmpEng::ParentDiagrams{{External}, {Internal}, {oneloop}, tree});
            _MESSAGE("Doing warmup run");
            momD_conf<F, 4> psp_warmup = Caravel::ps_parameterizations::random_rational_mom_conf<F>(nparticles);
            coeffs_exact.compute(psp_warmup);
            coeffs.reload_warmup();
        }

        // construct the amplitude
        Amplitude<C, decltype(coeffs), IntegralsBH::ProviderBH<C>> ampl(std::move(coeffs));
        ampl.set_laurent_range(-2, 2);

        ampl.integrals.set_mu_squared(amplitude.mu * amplitude.mu);
        auto psp = get_BBDFK_psp(nparticles);
        auto result = ampl.compute(psp);

        auto norm = C(0, -1);

        result /= norm;

        // correct from HV to FDH (factor of 1/2 Tree)
        Series<C> HVtoFDH{-2, 0, C(0), C(0), C(0.5)};

        result += HVtoFDH;

        result *= one_loop_norm_caravel_to_BH<C>;

        _PRINT(result);

        using std::abs;
        // checks
        if( abs(result[-2] - amplitude.double_pole) > amplitude.threshold ){
            _MESSAGE(Color::Code::FG_RED, "Wrong double pole: target = ", amplitude.double_pole, " obtained = ", result[-2]);
            errors++;
        }
        if( abs(result[-1] - amplitude.single_pole) > amplitude.threshold ){
            _MESSAGE(Color::Code::FG_RED, "Wrong single pole: target = ", amplitude.single_pole, " obtained = ", result[-1]);
            errors++;
        }
        if( abs(result[0] - amplitude.finite) > amplitude.threshold ){
            _MESSAGE(Color::Code::FG_RED, "Wrong finite piece: target = ", amplitude.finite, " obtained = ", result[0]);
            errors++;
        }
    }

    if(errors == 0)
        _MESSAGE(Color::Code::FG_GREEN, "All tests passed in one-loop_Wnparton_amps");
    else{
        _MESSAGE(Color::Code::FG_RED, "Test program one-loop_Wnparton_amps failed with ", errors, (errors==1?" error":" errors"));
    }

    return errors;
}

momD_conf<C, 4>
get_BBDFK_psp(size_t n) {
    using std::sin;
    using std::cos;
    if (n != 6 && n != 7) {
        std::cerr << "No " << n << "-pt BBDFK PSP coded!" << std::endl;
        std::exit(1);
    }
    else if (n == 6) {
        // PSP from eq. (9.1) in hep-ph/0604195
        C mu(6);
        C Pi(3.14159265358979324);
        C theta(Pi / 4.);
        C phi(Pi / 6.);
        C alpha(Pi / 3.);
        C beta(1.94810635915464587);
        momD<C, 4> k1(C(-mu / 2.), C(mu / 2. * sin(theta)), C(mu / 2. * cos(theta) * sin(phi)), C(mu / 2. * cos(theta) * cos(phi)));
        momD<C, 4> k2(C(-mu / 2.), C(-mu / 2. * sin(theta)), C(-mu / 2. * cos(theta) * sin(phi)), C(-mu / 2. * cos(theta) * cos(phi)));
        momD<C, 4> k3(C(mu / 3.), C(mu / 3.), C(0), C(0));
        momD<C, 4> k4(C(mu / 7.), C(mu / 7.) * cos(beta), C(mu / 7.) * sin(beta), C(0));
        momD<C, 4> k5(C(mu / 6.), C(mu / 6.) * cos(beta) * cos(alpha), C(mu / 6.) * sin(beta) * cos(alpha), C(mu / 6.) * sin(alpha));
        momD<C, 4> k6 = -k1 - k2 - k3 - k4 - k5;

        momD_conf<C, 4> BBDFK6pt = {k1, k2, k3, k4, k5, k6};

        //for(size_t ii=1 ; ii<= BBDFK6pt.n(); ii++)
        //    std::cout<<"k_"<<ii<<"^2 = "<<BBDFK6pt.p(ii)*BBDFK6pt.p(ii)<<std::endl;

        return BBDFK6pt;
    }
    else{
        // PSP from eq. (9.3) in hep-ph/0604195
        C mu(7);
        C Pi(3.14159265358979324);
        C theta(Pi / 4.);
        C phi(Pi / 6.);
        C alpha(Pi / 3.);
        C gamma(2. * Pi / 3.); 
        C beta(1.86404371318557881);
        momD<C, 4> k1(C(-mu / 2.), C(mu / 2. * sin(theta)), C(mu / 2. * cos(theta) * sin(phi)), C(mu / 2. * cos(theta) * cos(phi)));
        momD<C, 4> k2(C(-mu / 2.), C(-mu / 2. * sin(theta)), C(-mu / 2. * cos(theta) * sin(phi)), C(-mu / 2. * cos(theta) * cos(phi)));
        momD<C, 4> k3(C(mu / 3.), C(mu / 3.), C(0), C(0));
        momD<C, 4> k4(C(mu / 8.), C(mu / 8.) * cos(beta), C(mu / 8.) * sin(beta), C(0));
        momD<C, 4> k5(C(mu / 10.), C(mu / 10.) * cos(beta) * cos(alpha), C(mu / 10.) * sin(beta) * cos(alpha), C(mu / 10.) * sin(alpha));
        momD<C, 4> k6(C(mu / 12.), C(mu / 12.) * cos(beta) * cos(gamma), C(mu / 12.) * sin(beta) * cos(gamma), C(mu / 12.) * sin(gamma));
        momD<C, 4> k7 = -k1 - k2 - k3 - k4 - k5 - k6;

        momD_conf<C, 4> BBDFK7pt = {k1, k2, k3, k4, k5, k6, k7};

        //for(size_t ii=1 ; ii<= BBDFK7pt.n(); ii++)
        //    std::cout<<"k_"<<ii<<"^2 = "<<BBDFK7pt.p(ii)*BBDFK7pt.p(ii)<<std::endl;

        return BBDFK7pt;
    }
}


