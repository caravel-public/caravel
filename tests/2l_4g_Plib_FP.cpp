#include "Core/settings.h"
#include "Forest/Builder.h"
#include "AmplitudeCoefficients/Hierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "Core/momD.h"
#include "Core/precision_cast.h"

#include "misc/TestUtilities.hpp"

#include <limits>


using namespace std;
using namespace Caravel;

int main(int argc, char* argv[]){
    using std::vector; using std::string; 
    const size_t L = 2;

    // Get the settings!
    settings::read_settings_from_file();
    srand(15);

    // External process, put in order of momenta indices, this is important
    std::vector<Particle> process = {
        Particle("g",ParticleType("gluon"),SingleState("p"),1),
        Particle("g",ParticleType("gluon"),SingleState("m"),2),
        Particle("g",ParticleType("gluon"),SingleState("p"),3),
        Particle("g",ParticleType("gluon"),SingleState("m"),4),
    };

    // Used model
    YM model;

    using F = F32;
    
    auto physical_point = rational_mom_conf<BigRat>(4, {BigRat(-1)/BigRat(2), BigRat(-3)});

    vector<DenseRational<BigRat>> rational_coeffs_gen;
    {
        IntegrandHierarchy::ColourExpandedAmplitude<F,L> ampl(model,PartialAmplitudeInput{process});
        ampl.warmup_run(static_cast<momD_conf<F,4>>(physical_point));

        //std::function<vector<DenseRational<F>>(const momentumD_configuration<F, 4>&)> modulo_ibp_reduce =
            //[&ampl](const momentumD_configuration<F, 4>& mom_conf) { return ampl.get_master_coefficients(mom_conf).front(); };
        //rational_coeffs_gen = RationalReconstruction::rational_reconstruct(physical_point, modulo_ibp_reduce);
    }

    using T = CHP;


    //std::vector<momD<T,4>> ms;
    //{
        //auto casted = static_cast<momD_conf<T,4>>(to_precision<T::value_type>(physical_point));
        //for(auto& it: casted){
            //vector<T> v(it.get_vector());
            //v[2]*=T(0,1);
            //ms.emplace_back(v);
        //}
    //}
    momD_conf<T,4> m_fl(to_precision<T>(physical_point));

    IntegrandHierarchy::ColourExpandedAmplitude<T,L> ampl_fl(model,PartialAmplitudeInput{process});

    START_TIMER_T(evaluation_fl,T);
    auto fl_coeffs = ampl_fl.get_master_coefficients(m_fl).front();
    STOP_TIMER(evaluation_fl);

    //auto input_form = [](DenseRational<BigRat>& in) {
        //cout << "DensePolynomial<BigRat>({";
        //for (auto& it : in.numerator.coefficients) {
            //cout << "BigRat(" << it.nume() << "," << it.deno() << "),";
        //}
        //cout << "})";
    //};

    //cout << "{";
    //for (auto& it : rational_coeffs_gen) {
        //input_form(it);
        //cout << ",\n";
    //}
    //cout << "}\n";

    //return 0;

    // values from exact evaluation
    vector<DensePolynomial<BigRat>> rational_coeffs = {
        DensePolynomial<BigRat>({
            BigRat(BigInt(-4719), BigInt(9604)), BigRat(BigInt(42361), BigInt(8232)), BigRat(BigInt(-4671539), BigInt(230496)),
            BigRat(BigInt(321527), BigInt(8232)), BigRat(BigInt(-568315), BigInt(14406)), BigRat(BigInt(2355673), BigInt(115248)),
            BigRat(BigInt(-1086149), BigInt(230496)), BigRat(BigInt(10629), BigInt(38416)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(207), BigInt(2401)), BigRat(BigInt(10747), BigInt(8232)), BigRat(BigInt(-2563915), BigInt(230496)),
            BigRat(BigInt(40619), BigInt(1372)), BigRat(BigInt(-2063629), BigInt(57624)), BigRat(BigInt(2412043), BigInt(115248)),
            BigRat(BigInt(-1242953), BigInt(230496)), BigRat(BigInt(46259), BigInt(115248)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-14157), BigInt(4802)), BigRat(BigInt(5349), BigInt(98)), BigRat(BigInt(-816059), BigInt(2401)),
            BigRat(BigInt(337958), BigInt(343)), BigRat(BigInt(-3632355), BigInt(2401)), BigRat(BigInt(3099976), BigInt(2401)),
            BigRat(BigInt(-1410784), BigInt(2401)), BigRat(BigInt(269544), BigInt(2401)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(7452), BigInt(2401)), BigRat(BigInt(51078), BigInt(343)), BigRat(BigInt(-3486530), BigInt(2401)),
            BigRat(BigInt(1706696), BigInt(343)), BigRat(BigInt(-20034096), BigInt(2401)), BigRat(BigInt(18081556), BigInt(2401)),
            BigRat(BigInt(-8547568), BigInt(2401)), BigRat(BigInt(1674768), BigInt(2401)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-1104), BigInt(2401)), BigRat(BigInt(74335), BigInt(43218)), BigRat(BigInt(327865), BigInt(43218)),
            BigRat(BigInt(-3209299), BigInt(57624)), BigRat(BigInt(3483635), BigInt(28812)), BigRat(BigInt(-16186697), BigInt(172872)),
            BigRat(BigInt(-502253), BigInt(12348)), BigRat(BigInt(20761513), BigInt(172872)), BigRat(BigInt(-324001), BigInt(4116)),
            BigRat(BigInt(1173647), BigInt(57624)), BigRat(BigInt(-14411), BigInt(9604)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-6624), BigInt(2401)), BigRat(BigInt(-173100), BigInt(2401)), BigRat(BigInt(2505960), BigInt(2401)),
            BigRat(BigInt(-12283786), BigInt(2401)), BigRat(BigInt(29095840), BigInt(2401)), BigRat(BigInt(-32745726), BigInt(2401)),
            BigRat(BigInt(809752), BigInt(343)), BigRat(BigInt(28223484), BigInt(2401)), BigRat(BigInt(-4789928), BigInt(343)),
            BigRat(BigInt(16361728), BigInt(2401)), BigRat(BigInt(-3116544), BigInt(2401)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-6624), BigInt(2401)), BigRat(BigInt(-173100), BigInt(2401)), BigRat(BigInt(2505960), BigInt(2401)),
            BigRat(BigInt(-12283786), BigInt(2401)), BigRat(BigInt(29095840), BigInt(2401)), BigRat(BigInt(-32745726), BigInt(2401)),
            BigRat(BigInt(809752), BigInt(343)), BigRat(BigInt(28223484), BigInt(2401)), BigRat(BigInt(-4789928), BigInt(343)),
            BigRat(BigInt(16361728), BigInt(2401)), BigRat(BigInt(-3116544), BigInt(2401)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-1104), BigInt(2401)), BigRat(BigInt(74335), BigInt(43218)), BigRat(BigInt(327865), BigInt(43218)),
            BigRat(BigInt(-3209299), BigInt(57624)), BigRat(BigInt(3483635), BigInt(28812)), BigRat(BigInt(-16186697), BigInt(172872)),
            BigRat(BigInt(-502253), BigInt(12348)), BigRat(BigInt(20761513), BigInt(172872)), BigRat(BigInt(-324001), BigInt(4116)),
            BigRat(BigInt(1173647), BigInt(57624)), BigRat(BigInt(-14411), BigInt(9604)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-2484), BigInt(343)), BigRat(BigInt(-283705), BigInt(1764)), BigRat(BigInt(118284791), BigInt(49392)),
            BigRat(BigInt(-386255753), BigInt(32928)), BigRat(BigInt(37779723), BigInt(1372)), BigRat(BigInt(-3002822519), BigInt(98784)),
            BigRat(BigInt(108079919), BigInt(24696)), BigRat(BigInt(2668000375), BigInt(98784)), BigRat(BigInt(-64091999), BigInt(2058)),
            BigRat(BigInt(491634161), BigInt(32928)), BigRat(BigInt(-15406865), BigInt(5488)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-2484), BigInt(343)), BigRat(BigInt(-283705), BigInt(1764)), BigRat(BigInt(118284791), BigInt(49392)),
            BigRat(BigInt(-386255753), BigInt(32928)), BigRat(BigInt(37779723), BigInt(1372)), BigRat(BigInt(-3002822519), BigInt(98784)),
            BigRat(BigInt(108079919), BigInt(24696)), BigRat(BigInt(2668000375), BigInt(98784)), BigRat(BigInt(-64091999), BigInt(2058)),
            BigRat(BigInt(491634161), BigInt(32928)), BigRat(BigInt(-15406865), BigInt(5488)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(828), BigInt(2401)), BigRat(BigInt(-11719), BigInt(2058)), BigRat(BigInt(2836291), BigInt(57624)),
            BigRat(BigInt(-107515), BigInt(686)), BigRat(BigInt(524014), BigInt(2401)), BigRat(BigInt(-4213429), BigInt(28812)),
            BigRat(BigInt(900455), BigInt(19208)), BigRat(BigInt(-173807), BigInt(28812)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(828), BigInt(2401)), BigRat(BigInt(-2921), BigInt(147)), BigRat(BigInt(407884), BigInt(2401)),
            BigRat(BigInt(-197745), BigInt(343)), BigRat(BigInt(21068266), BigInt(21609)), BigRat(BigInt(-19278088), BigInt(21609)),
            BigRat(BigInt(9273200), BigInt(21609)), BigRat(BigInt(-1854464), BigInt(21609)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(276), BigInt(2401)), BigRat(BigInt(26401), BigInt(43218)), BigRat(BigInt(-637949), BigInt(43218)),
            BigRat(BigInt(28111), BigInt(392)), BigRat(BigInt(-2886497), BigInt(19208)), BigRat(BigInt(11080421), BigInt(86436)),
            BigRat(BigInt(2105833), BigInt(86436)), BigRat(BigInt(-22531709), BigInt(172872)), BigRat(BigInt(1914935), BigInt(19208)),
            BigRat(BigInt(-248237), BigInt(7203)), BigRat(BigInt(3533), BigInt(686)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(276), BigInt(2401)), BigRat(BigInt(-79371), BigInt(4802)), BigRat(BigInt(5354239), BigInt(28812)),
            BigRat(BigInt(-878858), BigInt(1029)), BigRat(BigInt(84851383), BigInt(43218)), BigRat(BigInt(-5168784), BigInt(2401)),
            BigRat(BigInt(26359433), BigInt(86436)), BigRat(BigInt(27750193), BigInt(14406)), BigRat(BigInt(-47983255), BigInt(21609)),
            BigRat(BigInt(2548356), BigInt(2401)), BigRat(BigInt(-67916), BigInt(343)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-138), BigInt(343)), BigRat(BigInt(-189859), BigInt(4802)), BigRat(BigInt(14830121), BigInt(28812)),
            BigRat(BigInt(-36487273), BigInt(14406)), BigRat(BigInt(132784478), BigInt(21609)), BigRat(BigInt(-2112739), BigInt(294)),
            BigRat(BigInt(134791973), BigInt(86436)), BigRat(BigInt(86312369), BigInt(14406)), BigRat(BigInt(-161241226), BigInt(21609)),
            BigRat(BigInt(8959032), BigInt(2401)), BigRat(BigInt(-1731488), BigInt(2401)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-138), BigInt(343)), BigRat(BigInt(-467767), BigInt(86436)), BigRat(BigInt(27450497), BigInt(345744)),
            BigRat(BigInt(-26264481), BigInt(76832)), BigRat(BigInt(6252843), BigInt(9604)), BigRat(BigInt(-44363939), BigInt(98784)),
            BigRat(BigInt(-52401529), BigInt(172872)), BigRat(BigInt(498517597), BigInt(691488)), BigRat(BigInt(-1097169), BigInt(2401)),
            BigRat(BigInt(25616147), BigInt(230496)), BigRat(BigInt(-218483), BigInt(38416)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-27600), BigInt(2401)), BigRat(BigInt(-3673556), BigInt(21609)), BigRat(BigInt(58430935), BigInt(21609)),
            BigRat(BigInt(-192862465), BigInt(14406)), BigRat(BigInt(231247040), BigInt(7203)), BigRat(BigInt(-1616199223), BigInt(43218)),
            BigRat(BigInt(187390754), BigInt(21609)), BigRat(BigInt(1309162079), BigInt(43218)), BigRat(BigInt(-277682480), BigInt(7203)),
            BigRat(BigInt(282016417), BigInt(14406)), BigRat(BigInt(-9200641), BigInt(2401)),
        }),
        DensePolynomial<BigRat>({
            BigRat(BigInt(-4600), BigInt(2401)), BigRat(BigInt(160529), BigInt(21609)), BigRat(BigInt(424195), BigInt(86436)),
            BigRat(BigInt(155875), BigInt(57624)), BigRat(BigInt(-21099980), BigInt(64827)), BigRat(BigInt(507082439), BigInt(518616)),
            BigRat(BigInt(-121459711), BigInt(129654)), BigRat(BigInt(-139559047), BigInt(518616)), BigRat(BigInt(75099895), BigInt(64827)),
            BigRat(BigInt(-417231763), BigInt(518616)), BigRat(BigInt(16123459), BigInt(86436)),
        })};

    using TR = remove_complex<T>::type;

    unsigned nerr = 0;

    TR mean_error(0);
    TR worst_error(numeric_limits<TR>::digits10);

    _MESSAGE("");
    _MESSAGE("Smallest possible accuracy is ", worst_error);
    _MESSAGE("");
    _MESSAGE("Reporting relative errors (compared to exact result):");

    for(unsigned i=0; i< rational_coeffs.size(); i++){
            _MESSAGE("coeff ",i);
            //_MESSAGE("\tnumerator:");
        for (unsigned j=0; j< rational_coeffs[i].coefficients.size(); j++){
            TR e = compute_accuracy(fl_coeffs[i].numerator.coefficients.at(j), to_precision<T>(rational_coeffs[i].coefficients.at(j)));
            if (e > 7){
                _MESSAGE("\tep^",j,": ",e);
            }
            else{
                _WARNING("\tep^",j," FAILED:\t", fl_coeffs[i].numerator.coefficients.at(j) , " != ", to_precision<T>(rational_coeffs[i].coefficients.at(j)),"\taccuracy = ",e);
                ++nerr;
            }
            mean_error += e;
            if (e < worst_error ) worst_error = e;
        }
    }

    mean_error /= rational_coeffs.size();

    _MESSAGE("");

    _MESSAGE("MEAN ERROR: ", mean_error);
    _MESSAGE("WORST ERROR: ", worst_error);

    _MESSAGE("--------");


    return nerr;
}
