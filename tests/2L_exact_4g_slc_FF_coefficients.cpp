#include <random>
#include <sstream>
#include <vector>

#include "misc/CaravelCereal.hpp"
#include "Core/RationalReconstruction.h"
#include "AmplitudeCoefficients/Hierarchy.h"
#include "Forest/Model.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"

#include "Core/typedefs.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "FunctionalReconstruction/DenseRational.h"
#include "Core/settings.h"

#include "Core/MathInterface.h"

#include "misc/TestUtilities.hpp"
#include "Core/CaravelInput.h"


using namespace Caravel;
using namespace std;
using namespace Caravel::Integrals;

int main(int argc, char* argv[]) {

    std::string command_input("PartialAmplitudeInput[Particles[Particle[gluon,1,p],Particle[gluon,2,p],Particle[gluon,3,m],Particle[gluon,4,m]],ColourStructure[NcOrder[1]]]");

    verbosity_settings.show_all();

    settings::use_setting("general::do_n_eq_n_test yes");

    settings::use_setting("general::master_basis standard");

    //settings::use_setting("IntegrandHierarchy::amplitude_normalization spinor_weight");
    //settings::use_setting("IntegrandHierarchy::amplitude_normalization tree");

    using F = F32;
    using namespace IntegrandHierarchy;

    // this is a global cardinality that will not really be used
    cardinality_update_manager<F>::get_instance()->set_cardinality(2147483629);

    PartialAmplitudeInput pa(command_input);

    auto xs = Caravel::detail::read_twistor_parameters(MathList("xs[1,-9/25]"));

    const auto& process = pa.get_particles();

    _MESSAGE(Color::Code::FG_BLUE, pa.get_id_string(), Color::Code::FG_DEFAULT);

    SM_color_ordered model2use;

    auto physical_point = rational_mom_conf<BigRat>(process.size(), xs);

    auto ps = static_cast<momentumD_configuration<F,4>>(physical_point);

    ColourExpandedAmplitude<F, 2> ampl(model2use, pa);

    auto result_coeffs =  ampl.get_master_coefficients(ps).front();

    // std::cout << result_coeffs << std::endl;

    size_t err = 0;

    {
        std::string filename = "test_data/" + pa.get_id_string() + "_saved_master_FF_coeffs.dat";
        // write_serialized(result_coeffs, filename);
        auto target = read_serialized<decltype(result_coeffs)>(filename);
        for (size_t i = 0; i < result_coeffs.size(); ++i) {
            if (result_coeffs.at(i) != target.at(i)) {
                ++err;
                _MESSAGE("Coefficient ", i, " doesn't agree! ", result_coeffs.at(i), " != ", target.at(i), "(target)");
            }
        }
    }

    _MESSAGE("\nChecking that all coefficients are the same as when the test was written...");

    if(err>0){
        _MESSAGE(Color::Code::FG_RED, "FAIL");
    }
    else{
        _MESSAGE(Color::Code::FG_GREEN, "PASS");
    }

    _MESSAGE("");

    return err;
}

