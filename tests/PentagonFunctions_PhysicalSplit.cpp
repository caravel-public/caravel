#include "PentagonFunctions_test.hpp"

using namespace Caravel;
using namespace Caravel::Integrals;

int main() {

    // Fix the phase space point
    std::vector<std::vector<double>> vv = {{-0.9876543209876, -0.790123456790, -1.80267859636, -3.16049382716, -2.211585663744},
                                          {0.9876543209876, 0.790123456790, -1.80267859636, 3.16049382716, -2.211585663744},
                                          {0.9876543209876, -0.790123456790, -1.80267859636, -3.16049382716, -2.211585663744}};
    std::vector<std::string> region_names = {"euclidean", "split", "alternating"};

    std::vector<std::pair<size_t, std::vector<size_t>>> region_define = {{5, {}}, {5, {4, 5}}, {5, {3, 5}}};

    return check_pentagon_functions_by_region( region_names[1], region_define[1], vv[1] );
}
