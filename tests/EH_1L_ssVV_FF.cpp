#include "Core/Debug.h"

#include "Core/Particle.h"
#include "Core/momD_conf.h"

#include "Core/Utilities.h"
#include "Core/settings.h"
#include "Core/typedefs.h"
#include "misc/TestUtilities.hpp"

#include "AmpEng/CoefficientEngine.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "Amplitude.h"

#include <algorithm>
#include <cmath>
#include <exception>
#include <iostream>
#include <string>
#include <vector>

using namespace Caravel;
using namespace AmpEng;
using namespace lGraph;
using F = F32;

#if 0
template <typename T, size_t Ds> std::vector<T> get_basis_formfactors(const std::vector<std::vector<Particle>> procs, const momD_conf<T, Ds>& mc) {
    const size_t basis_dim = procs.size();
    std::vector<T> A(basis_dim * basis_dim, T(0));

    momD<T, 4> q = mc[1] + mc[4];
    momD<T, 4> p2b = mc[3] + q / F(2);

    for (size_t n = 0; n < basis_dim; n++) {
        // get polarization states
        Particle V1 = procs[n][0], V4 = procs[n][3];
        momD<T, 4> ep1(get_state(mc, V1)), ep4(get_state(mc, V4));

        // The factor 1/2 accounts for the sqrt(2) normalization of the polarization states
        A[n] = (ep1 * ep4) / F(2);
        A[n + basis_dim] = (ep1 * q) * (ep4 * q) / F(2);
        A[n + 2 * basis_dim] = (ep1 * p2b) * (ep4 * p2b) / F(2);
        A[n + 3 * basis_dim] = ((ep4 * q) * (ep1 * p2b) - (ep4 * p2b) * (ep1 * q)) / F(2);
    }

    return A;
}
#endif

int main(int argc, char* argv[]) {

    // more cardinalities in misc/Moduli.hpp
    int64_t cardinality(2147483629);
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    // one-loop hierarchy
    std::vector<xGraph> maximals_1L;

    // just two boxes
    maximals_1L.push_back(xGraph(
        MathList("CaravelGraph[Nodes[Node[Leg[1,1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[4,1], Link[0], Leg[2,2], Link[2], Leg[3,2], Link[0]]]]")));
    maximals_1L.push_back(xGraph(
        MathList("CaravelGraph[Nodes[Node[Leg[4,1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[1,1], Link[0], Leg[2,2], Link[2], Leg[3,2], Link[0]]]]")));

    Pincher hier_1L(maximals_1L);

    std::cout << "================================" << std::endl;
    std::cout << "Filter scaleless graphs" << std::endl;
    hier_1L.filter(scaleless_graphs_filter);

    std::cout << "================================" << std::endl;
    std::cout << "Filter four-matter contact graphs" << std::endl;
    hier_1L.filter(four_massive_particle_filter);

    std::cout << "================================" << std::endl;
    std::cout << "At least one matter propagators per loop" << std::endl;
    hier_1L.filter(subloop_one_matter_line_filter);

    GraphCutMap map_pp;
    // OJO: necessarily the index should match the momentum of the particle!
    map_pp.external = { {2, std::make_shared<Particle>("s1", ParticleType("Phi1"), SingleState("RealSC"), 2, 0)},
                     {3, std::make_shared<Particle>("s2", ParticleType("Phi1"), SingleState("RealSC"), 3, 0)},
                     {1, std::make_shared<Particle>("v1_p", ParticleType("V1"), SingleState("p"), 1, 0)},
                     {4, std::make_shared<Particle>("v2_p", ParticleType("V1"), SingleState("p"), 4, 0)},
                    };
    map_pp.internal = { {0, ParticleType("G")},
                     {2, ParticleType("Phi1")},
                     {1, ParticleType("V1")},
                    };
    map_pp.coup = "K";

    GraphCutMap map_mm(map_pp);
    map_mm.external = { {2, std::make_shared<Particle>("s1", ParticleType("Phi1"), SingleState("RealSC"), 2, 0)},
                     {3, std::make_shared<Particle>("s2", ParticleType("Phi1"), SingleState("RealSC"), 3, 0)},
                     {1, std::make_shared<Particle>("v1_m", ParticleType("V1"), SingleState("m"), 1, 0)},
                     {4, std::make_shared<Particle>("v2_m", ParticleType("V1"), SingleState("m"), 4, 0)},
                    };

    GraphCutMap map_pm(map_pp);
    map_pm.external = { {2, std::make_shared<Particle>("s1", ParticleType("Phi1"), SingleState("RealSC"), 2, 0)},
                     {3, std::make_shared<Particle>("s2", ParticleType("Phi1"), SingleState("RealSC"), 3, 0)},
                     {1, std::make_shared<Particle>("v1_p", ParticleType("V1"), SingleState("p"), 1, 0)},
                     {4, std::make_shared<Particle>("v2_m", ParticleType("V1"), SingleState("m"), 4, 0)},
                    };

    GraphCutMap map_mp(map_pp);
    map_mp.external = { {2, std::make_shared<Particle>("s1", ParticleType("Phi1"), SingleState("RealSC"), 2, 0)},
                     {3, std::make_shared<Particle>("s2", ParticleType("Phi1"), SingleState("RealSC"), 3, 0)},
                     {1, std::make_shared<Particle>("v1_m", ParticleType("V1"), SingleState("m"), 1, 0)},
                     {4, std::make_shared<Particle>("v2_p", ParticleType("V1"), SingleState("p"), 4, 0)},
                    };

    size_t nparticles(4);
    Particle ss("ss", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
    Particle vv("vv", ParticleType("V1"), SingleState("p"), 1, 0);
    size_t mindex_ss = ss.get_mass_index();
    size_t mindex_vv = vv.get_mass_index();
    std::vector<size_t> masses = {mindex_vv, mindex_ss, mindex_ss, mindex_vv};
    settings::use_setting("IntegrandHierarchy::amplitude_normalization none"); // do not normalize on massive tree! It has an explicit Ds dependence!
    settings::use_setting("general::do_n_eq_n_test yes");
    //settings::use_setting("general::one_loop_master_basis only_scalar");

    Genealogy hierarchy_pp(hier_1L, map_pp, true, "EHGravity");
    Genealogy hierarchy_mm(hier_1L, map_mm, true, "EHGravity");
    Genealogy hierarchy_pm(hier_1L, map_pm, true, "EHGravity");
    Genealogy hierarchy_mp(hier_1L, map_mp, true, "EHGravity");

    EHGravity model2use;

    CoefficientEngine<F32> coefficient_provider_pp(hierarchy_pp, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
    CoefficientEngine<F32> coefficient_provider_mm(hierarchy_mm, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
    CoefficientEngine<F32> coefficient_provider_pm(hierarchy_pm, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
    CoefficientEngine<F32> coefficient_provider_mp(hierarchy_mp, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
    coefficient_provider_pp.set_Ds_function_basis(powers_of_Dsm2<F>(2, 0));
    coefficient_provider_mm.set_Ds_function_basis(powers_of_Dsm2<F>(2, 0));
    coefficient_provider_pm.set_Ds_function_basis(powers_of_Dsm2<F>(2, 0));
    coefficient_provider_mp.set_Ds_function_basis(powers_of_Dsm2<F>(2, 0));

    // compute warmup if necessary
    std::vector<F> xs = {F(std::rand()), F(std::rand())};
    momD_conf<F, 4> psp_warmup = rational_mom_conf<F>(nparticles, xs, masses);
    if (!coefficient_provider_pp.has_warmup()) {
        coefficient_provider_pp.warmup_run(psp_warmup);
        // reload warmup
        coefficient_provider_pp.reload_warmup();
    }
    if (!coefficient_provider_mm.has_warmup()) {
        coefficient_provider_mm.warmup_run(psp_warmup);
        // reload warmup
        coefficient_provider_mm.reload_warmup();
    }
    if (!coefficient_provider_pm.has_warmup()) {
        coefficient_provider_pm.warmup_run(psp_warmup);
        // reload warmup
        coefficient_provider_pm.reload_warmup();
    }
    if (!coefficient_provider_mp.has_warmup()) {
        coefficient_provider_mp.warmup_run(psp_warmup);
        // reload warmup
        coefficient_provider_mp.reload_warmup();
    }

    // RECONSTRUCTION
    /*
     * Number of variables: 4 - massive one-loop
     *                      x0 = x, x1 = 23, x2 = m1, x3 = m2
     *                      with 4*m3^2*m4^2/(s12 - m3^2 - m4^2)^2 = 4*x^2/(1+x^2)^2
     */
    constexpr size_t _N = 4;

    // random number generators
    std::array<F, _N> xxs = {F(BigRat(4, 3)), F(BigRat(8, 13)), F(BigRat(25, 1)), F(BigRat(11, 1))};

    // return the master coefficients
    auto f = [&](std::array<F, _N> xs) {
        std::vector<F> point;
        momD_conf<F, 4> psp;

        BigRat Mass1 = RationalReconstruction::rational_guess(std::pair<BigInt, BigInt>{BigInt(xs[2]), BigInt(cardinality)});
        BigRat Mass2 = RationalReconstruction::rational_guess(std::pair<BigInt, BigInt>{BigInt(xs[3]), BigInt(cardinality)});
        ParticleMass::set_container_mass(mindex_ss, int(Mass1.num()), int(Mass1.den()));
        ParticleMass::set_container_mass(mindex_vv, int(Mass2.num()), int(Mass2.den()));
        point = {xs[0], xs[1]};
        psp = rational_mom_conf<F>(nparticles, point, masses);

        auto result_pp = coefficient_provider_pp.compute_full_Ds(psp);
        auto result_mm = coefficient_provider_mm.compute_full_Ds(psp);
        auto result_pm = coefficient_provider_pm.compute_full_Ds(psp);
        auto result_mp = coefficient_provider_mp.compute_full_Ds(psp);

        return std::vector<decltype(result_pp)>{result_pp, result_mm, result_pm, result_mp,};
    };

    std::vector<F> target = {
        F(1838319749), F(476720018),  F(0),          F(0),          F(0),          F(1951172057), F(476720018),  F(0),          F(0),          F(0),
        F(1838319749), F(476720018),  F(0),          F(0),          F(0),          F(1951172057), F(476720018),  F(0),          F(0),          F(0),
        F(1241286733), F(1652515186), F(0),          F(0),          F(235809470),  F(0),          F(0),          F(0),          F(1241286733), F(1652515186),
        F(0),          F(0),          F(235809470),  F(0),          F(0),          F(0),          F(1006414531), F(920236988),  F(0),          F(0),
        F(0),          F(579286352),  F(920236988),  F(0),          F(0),          F(0),          F(1006414531), F(920236988),  F(0),          F(0),
        F(0),          F(579286352),  F(920236988),  F(0),          F(0),          F(0),          F(2099880519), F(264710208),  F(0),          F(0),
        F(514126280),  F(0),          F(0),          F(0),          F(2099880519), F(264710208),  F(0),          F(0),          F(514126280),  F(0),
        F(0),          F(0),          F(1555052570), F(1634335963), F(0),          F(0),          F(0),          F(891701460),  F(1634335963), F(0),
        F(0),          F(0),          F(1555052570), F(1634335963), F(0),          F(0),          F(0),          F(891701460),  F(1634335963), F(0),
        F(0),          F(0),          F(2014306115), F(0),          F(0),          F(0),          F(1082083076), F(0),          F(0),          F(0),
        F(2014306115), F(0),          F(0),          F(0),          F(1082083076), F(0),          F(0),          F(0),          F(900921192),  F(1819917831),
        F(0),          F(0),          F(0),          F(67341023),   F(1819917831), F(0),          F(0),          F(0),          F(900921192),  F(1819917831),
        F(0),          F(0),          F(0),          F(67341023),   F(1819917831), F(0),          F(0),          F(0),          F(266333584),  F(0),
        F(0),          F(0),          F(1597471691), F(0),          F(0),          F(0),          F(266333584),  F(0),          F(0),          F(0),
        F(1597471691), F(0),          F(0),          F(0),          F(1281683006), F(682252254),  F(0),          F(0),          F(0),          F(110649841),
        F(682252254),  F(0),          F(0),          F(0),          F(1281683006), F(682252254),  F(0),          F(0),          F(0),          F(110649841),
        F(682252254),  F(0),          F(0),          F(0),          F(817049525),  F(639757780),  F(742957672),  F(0),          F(1323247807), F(951000706),
        F(0),          F(0),          F(817049525),  F(639757780),  F(742957672),  F(0),          F(1323247807), F(951000706),  F(0),          F(0),
        F(392784968),  F(512670322),  F(0),          F(0),          F(0),          F(1072951673), F(512670322),  F(0),          F(0),          F(0),
        F(392784968),  F(512670322),  F(0),          F(0),          F(0),          F(1072951673), F(512670322),  F(0),          F(0),          F(0),
        F(1167168628), F(1519594105), F(2000898288), F(0),          F(1442976810), F(1073456315), F(0),          F(0),          F(1167168628), F(1519594105),
        F(2000898288), F(0),          F(1442976810), F(1073456315), F(0),          F(0),          F(707510071),  F(87267898),   F(0),          F(0),
        F(0),          F(1405268665), F(87267898),   F(0),          F(0),          F(0),          F(707510071),  F(87267898),   F(0),          F(0),
        F(0),          F(1405268665), F(87267898),   F(0),          F(0),          F(0),          F(598246943),  F(1088415641), F(1073745875), F(0),
        F(627423829),  F(1938339290), F(0),          F(0),          F(598246943),  F(1088415641), F(1073745875), F(0),          F(627423829),  F(1938339290),
        F(0),          F(0),          F(471865937),  F(463022835),  F(0),          F(0),          F(0),          F(329252849),  F(463022835),  F(0),
        F(0),          F(0),          F(471865937),  F(463022835),  F(0),          F(0),          F(0),          F(329252849),  F(463022835),  F(0),
        F(0),          F(0),          F(666770783),  F(1017113504), F(1561498524), F(0),          F(421611237),  F(446135146),  F(0),          F(0),
        F(666770783),  F(1017113504), F(1561498524), F(0),          F(421611237),  F(446135146),  F(0),          F(0),          F(1500628913), F(38981106),
        F(0),          F(0),          F(0),          F(97573519),   F(1879201125), F(0),          F(0),          F(0),          F(1500628913), F(38981106),
        F(0),          F(0),          F(0),          F(97573519),   F(1879201125), F(0),          F(0),          F(0),          F(839615587),  F(878040466),
        F(1776004793), F(0),          F(799691256),  F(1863477520), F(0),          F(0),          F(839615587),  F(878040466),  F(1776004793), F(0),
        F(799691256),  F(1863477520), F(0),          F(0),          F(697814722),  F(462202420),  F(0),          F(0),          F(0),          F(909943563),
        F(310191783),  F(0),          F(0),          F(0),          F(697814722),  F(462202420),  F(0),          F(0),          F(0),          F(909943563),
        F(310191783),  F(0),          F(0),          F(0),          F(1264890274), F(1442710814), F(1147034485), F(0),          F(1907834993), F(1569599009),
        F(0),          F(0),          F(1264890274), F(1442710814), F(1147034485), F(0),          F(1907834993), F(1569599009), F(0),          F(0),
        F(1952329685), F(96148781),   F(0),          F(0),          F(0),          F(195710709),  F(1452924512), F(0),          F(0),          F(0),
        F(1952329685), F(96148781),   F(0),          F(0),          F(0),          F(195710709),  F(1452924512), F(0),          F(0),          F(0),
        F(585873079),  F(1685876329), F(536868877),  F(0),          F(1109395848), F(1695863772), F(0),          F(0),          F(585873079),  F(1685876329),
        F(536868877),  F(0),          F(1109395848), F(1695863772), F(0),          F(0),          F(1059050056), F(1272354281), F(0),          F(0),
        F(0),          F(345148307),  F(1996555369), F(0),          F(0),          F(0),          F(1059050056), F(1272354281), F(0),          F(0),
        F(0),          F(345148307),  F(1996555369), F(0),          F(0),          F(0),          F(549986946),  F(2083161398), F(1366734367), F(0),
        F(578980624),  F(600077120),  F(0),          F(0),          F(549986946),  F(2083161398), F(1366734367), F(0),          F(578980624),  F(600077120),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(561839735),  F(1721941415), F(1776004793), F(0),          F(0),          F(0),          F(0),          F(0),
        F(561839735),  F(1721941415), F(1776004793), F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(169227764),  F(216562038),
        F(1147034485), F(0),          F(0),          F(0),          F(0),          F(0),          F(169227764),  F(216562038),  F(1147034485), F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(1060157038), F(529533994),  F(536868877),  F(0),          F(0),          F(0),
        F(0),          F(0),          F(1060157038), F(529533994),  F(536868877),  F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(635499207),  F(1638926877), F(1366734367), F(0),          F(0),          F(0),          F(0),          F(0),          F(635499207),  F(1638926877),
        F(1366734367), F(0),          F(0),          F(0),          F(0),          F(0)};

    auto cfs = f(xxs);
    _PRINT(cfs);

#if 0
    size_t errors = 0;
    // return the master coefficients
    std::function<std::vector<F>(std::array<F, _N>)> f = [&](std::array<F, _N> xs) {
        // Set new masses
        BigRat Mass1 = RationalReconstruction::rational_guess(std::pair<BigInt, BigInt>{BigInt(xs[2]), BigInt(cardinality)});
        BigRat Mass2 = RationalReconstruction::rational_guess(std::pair<BigInt, BigInt>{BigInt(xs[3]), BigInt(cardinality)});
        ParticleMass::set_container_mass(v1_p.get_mass_index(), int(Mass1.num()), int(Mass1.den()));
        ParticleMass::set_container_mass(s1.get_mass_index(), int(Mass2.num()), int(Mass2.den()));

        // Get momenta
        std::vector<F> point = {xs[0], xs[1]};
        momD_conf<F, 4> psp = rational_mom_conf<F>(nparticles, point, masses);

        // The returned coefficients are given for { (Ds-4)^0, -1/2*(Ds-4)^1, 1/4*(Ds-4)^2 } = { 1, ep, ep^2 }
        coefficient_provider_pp.eval(psp);
        coefficient_provider_mm.eval(psp);
        coefficient_provider_pm.eval(psp);
        coefficient_provider_mp.eval(psp);
        std::vector<F> coeffs_pp = coefficient_provider_pp.get_all_coeffs_normalized();
        std::vector<F> coeffs_mm = coefficient_provider_mm.get_all_coeffs_normalized();
        std::vector<F> coeffs_pm = coefficient_provider_pm.get_all_coeffs_normalized();
        std::vector<F> coeffs_mp = coefficient_provider_mp.get_all_coeffs_normalized();
        coefficient_provider_789_pp.eval(psp);
        coefficient_provider_789_mm.eval(psp);
        coefficient_provider_789_pm.eval(psp);
        coefficient_provider_789_mp.eval(psp);
        std::vector<F> coeffs_789_pp = coefficient_provider_789_pp.get_all_coeffs_normalized();
        std::vector<F> coeffs_789_mm = coefficient_provider_789_mm.get_all_coeffs_normalized();
        std::vector<F> coeffs_789_pm = coefficient_provider_789_pm.get_all_coeffs_normalized();
        std::vector<F> coeffs_789_mp = coefficient_provider_789_mp.get_all_coeffs_normalized();

        // Reconstruct function values in Ds = {5,6,7}
        std::vector<F> f5_pp = get_func<5>(coeffs_pp);
        std::vector<F> f6_pp = get_func<6>(coeffs_pp);
        std::vector<F> f7_pp = get_func<7>(coeffs_pp);
        std::vector<F> f5_mm = get_func<5>(coeffs_mm);
        std::vector<F> f6_mm = get_func<6>(coeffs_mm);
        std::vector<F> f7_mm = get_func<7>(coeffs_mm);
        std::vector<F> f5_pm = get_func<5>(coeffs_pm);
        std::vector<F> f6_pm = get_func<6>(coeffs_pm);
        std::vector<F> f7_pm = get_func<7>(coeffs_pm);
        std::vector<F> f5_mp = get_func<5>(coeffs_mp);
        std::vector<F> f6_mp = get_func<6>(coeffs_mp);
        std::vector<F> f7_mp = get_func<7>(coeffs_mp);
        // Reconstruct function values in Ds = {7,8,9}
        std::vector<F> cf7_pp = get_func<7>(coeffs_789_pp);
        std::vector<F> f8_pp = get_func<8>(coeffs_789_pp);
        std::vector<F> f9_pp = get_func<9>(coeffs_789_pp);
        std::vector<F> cf7_mm = get_func<7>(coeffs_789_mm);
        std::vector<F> f8_mm = get_func<8>(coeffs_789_mm);
        std::vector<F> f9_mm = get_func<9>(coeffs_789_mm);
        std::vector<F> cf7_pm = get_func<7>(coeffs_789_pm);
        std::vector<F> f8_pm = get_func<8>(coeffs_789_pm);
        std::vector<F> f9_pm = get_func<9>(coeffs_789_pm);
        std::vector<F> cf7_mp = get_func<7>(coeffs_789_mp);
        std::vector<F> f8_mp = get_func<8>(coeffs_789_mp);
        std::vector<F> f9_mp = get_func<9>(coeffs_789_mp);
        const size_t n_coeffs = f5_pp.size();
        for (size_t ii = 0; ii < n_coeffs; ii++) {
            if (cf7_pp[ii] != f7_pp[ii]) errors++;
            if (cf7_mm[ii] != f7_mm[ii]) errors++;
            if (cf7_pm[ii] != f7_pm[ii]) errors++;
            if (cf7_mp[ii] != f7_mp[ii]) errors++;
        }

        // Compute form factors in Ds = {5,6,7,8,9}
        std::vector<F> A = get_basis_formfactors(procs, psp);
        std::vector<F> g5(4 * n_coeffs, F(0)), g6(4 * n_coeffs, F(0)), g7(4 * n_coeffs, F(0)), g8(4 * n_coeffs, F(0)), g9(4 * n_coeffs, F(0));

        for (size_t ii = 0; ii < n_coeffs; ii++) {
            // Numerical tensors
            std::vector<F> A1 = A, A2 = A, A3 = A, A4 = A, A5 = A;

            // Solve system for form factors in Ds = 5
            std::vector<F> t5 = {f5_pp[ii], f5_mm[ii], f5_pm[ii], f5_mp[ii]};
            std::vector<F> e5 = plu_solver(std::move(A1), std::move(t5));
            for (size_t n = 0; n < 4; n++) g5[ii + n * n_coeffs] = e5[n];

            // Solve system for form factors in Ds = 6
            std::vector<F> t6 = {f6_pp[ii], f6_mm[ii], f6_pm[ii], f6_mp[ii]};
            std::vector<F> e6 = plu_solver(std::move(A2), std::move(t6));
            for (size_t n = 0; n < 4; n++) g6[ii + n * n_coeffs] = e6[n];

            // Solve system for form factors in Ds = 7
            std::vector<F> t7 = {f7_pp[ii], f7_mm[ii], f7_pm[ii], f7_mp[ii]};
            std::vector<F> e7 = plu_solver(std::move(A3), std::move(t7));
            for (size_t n = 0; n < 4; n++) g7[ii + n * n_coeffs] = e7[n];

            // Solve system for form factors in Ds = 8
            std::vector<F> t8 = {f8_pp[ii], f8_mm[ii], f8_pm[ii], f8_mp[ii]};
            std::vector<F> e8 = plu_solver(std::move(A4), std::move(t8));
            for (size_t n = 0; n < 4; n++) g8[ii + n * n_coeffs] = e8[n];

            // Solve system for form factors in Ds = 9
            std::vector<F> t9 = {f9_pp[ii], f9_mm[ii], f9_pm[ii], f9_mp[ii]};
            std::vector<F> e9 = plu_solver(std::move(A5), std::move(t9));
            for (size_t n = 0; n < 4; n++) g9[ii + n * n_coeffs] = e9[n];
        }

        // Compute new coefficients for Ansatz: f(Ds) = [ c0 + c1*(Ds-2) + c2*(Ds-2)^2 + c4*(Ds-2)^3 ]/(Ds-2)^2
        const auto P_5678 = fit_Ds_polynomial<5, 6, 7, 8, F>(g5, g6, g7, g8);
        const auto P_6789 = fit_Ds_polynomial<6, 7, 8, 9, F>(g6, g7, g8, g9);
        for (size_t ii = 0; ii < P_5678.size(); ii++)
            if (P_5678[ii] != P_6789[ii]) errors++;

        /*
         * The output has the following form:
         *  T1*[c^(0)_1...,c^(0)_36], ..., T4*[c^(0)_1,...,c^(0)_36], T1*[c^(1)_1,...,c^(1)_36], ..., T4*[c^(1)_1,...,c^(1)_36],T1*[c^(2)_1,...c^(2)_36], ...,
         T4*[c^(2)_1,...c^(2)_36],T1*[c^(3)_1,...c^(3)_36], ..., T4*[c^(3)_1,...c^(3)_36]

         *  36 coeffs * 4 tensors * 4 Ds powers = 576 numbers
         */
        return P_5678;
    };

    std::vector<F> target = {
        F(1838319749), F(476720018),  F(0),          F(0),          F(0),          F(1951172057), F(476720018),  F(0),          F(0),          F(0),
        F(1838319749), F(476720018),  F(0),          F(0),          F(0),          F(1951172057), F(476720018),  F(0),          F(0),          F(0),
        F(1241286733), F(1652515186), F(0),          F(0),          F(235809470),  F(0),          F(0),          F(0),          F(1241286733), F(1652515186),
        F(0),          F(0),          F(235809470),  F(0),          F(0),          F(0),          F(1006414531), F(920236988),  F(0),          F(0),
        F(0),          F(579286352),  F(920236988),  F(0),          F(0),          F(0),          F(1006414531), F(920236988),  F(0),          F(0),
        F(0),          F(579286352),  F(920236988),  F(0),          F(0),          F(0),          F(2099880519), F(264710208),  F(0),          F(0),
        F(514126280),  F(0),          F(0),          F(0),          F(2099880519), F(264710208),  F(0),          F(0),          F(514126280),  F(0),
        F(0),          F(0),          F(1555052570), F(1634335963), F(0),          F(0),          F(0),          F(891701460),  F(1634335963), F(0),
        F(0),          F(0),          F(1555052570), F(1634335963), F(0),          F(0),          F(0),          F(891701460),  F(1634335963), F(0),
        F(0),          F(0),          F(2014306115), F(0),          F(0),          F(0),          F(1082083076), F(0),          F(0),          F(0),
        F(2014306115), F(0),          F(0),          F(0),          F(1082083076), F(0),          F(0),          F(0),          F(900921192),  F(1819917831),
        F(0),          F(0),          F(0),          F(67341023),   F(1819917831), F(0),          F(0),          F(0),          F(900921192),  F(1819917831),
        F(0),          F(0),          F(0),          F(67341023),   F(1819917831), F(0),          F(0),          F(0),          F(266333584),  F(0),
        F(0),          F(0),          F(1597471691), F(0),          F(0),          F(0),          F(266333584),  F(0),          F(0),          F(0),
        F(1597471691), F(0),          F(0),          F(0),          F(1281683006), F(682252254),  F(0),          F(0),          F(0),          F(110649841),
        F(682252254),  F(0),          F(0),          F(0),          F(1281683006), F(682252254),  F(0),          F(0),          F(0),          F(110649841),
        F(682252254),  F(0),          F(0),          F(0),          F(817049525),  F(639757780),  F(742957672),  F(0),          F(1323247807), F(951000706),
        F(0),          F(0),          F(817049525),  F(639757780),  F(742957672),  F(0),          F(1323247807), F(951000706),  F(0),          F(0),
        F(392784968),  F(512670322),  F(0),          F(0),          F(0),          F(1072951673), F(512670322),  F(0),          F(0),          F(0),
        F(392784968),  F(512670322),  F(0),          F(0),          F(0),          F(1072951673), F(512670322),  F(0),          F(0),          F(0),
        F(1167168628), F(1519594105), F(2000898288), F(0),          F(1442976810), F(1073456315), F(0),          F(0),          F(1167168628), F(1519594105),
        F(2000898288), F(0),          F(1442976810), F(1073456315), F(0),          F(0),          F(707510071),  F(87267898),   F(0),          F(0),
        F(0),          F(1405268665), F(87267898),   F(0),          F(0),          F(0),          F(707510071),  F(87267898),   F(0),          F(0),
        F(0),          F(1405268665), F(87267898),   F(0),          F(0),          F(0),          F(598246943),  F(1088415641), F(1073745875), F(0),
        F(627423829),  F(1938339290), F(0),          F(0),          F(598246943),  F(1088415641), F(1073745875), F(0),          F(627423829),  F(1938339290),
        F(0),          F(0),          F(471865937),  F(463022835),  F(0),          F(0),          F(0),          F(329252849),  F(463022835),  F(0),
        F(0),          F(0),          F(471865937),  F(463022835),  F(0),          F(0),          F(0),          F(329252849),  F(463022835),  F(0),
        F(0),          F(0),          F(666770783),  F(1017113504), F(1561498524), F(0),          F(421611237),  F(446135146),  F(0),          F(0),
        F(666770783),  F(1017113504), F(1561498524), F(0),          F(421611237),  F(446135146),  F(0),          F(0),          F(1500628913), F(38981106),
        F(0),          F(0),          F(0),          F(97573519),   F(1879201125), F(0),          F(0),          F(0),          F(1500628913), F(38981106),
        F(0),          F(0),          F(0),          F(97573519),   F(1879201125), F(0),          F(0),          F(0),          F(839615587),  F(878040466),
        F(1776004793), F(0),          F(799691256),  F(1863477520), F(0),          F(0),          F(839615587),  F(878040466),  F(1776004793), F(0),
        F(799691256),  F(1863477520), F(0),          F(0),          F(697814722),  F(462202420),  F(0),          F(0),          F(0),          F(909943563),
        F(310191783),  F(0),          F(0),          F(0),          F(697814722),  F(462202420),  F(0),          F(0),          F(0),          F(909943563),
        F(310191783),  F(0),          F(0),          F(0),          F(1264890274), F(1442710814), F(1147034485), F(0),          F(1907834993), F(1569599009),
        F(0),          F(0),          F(1264890274), F(1442710814), F(1147034485), F(0),          F(1907834993), F(1569599009), F(0),          F(0),
        F(1952329685), F(96148781),   F(0),          F(0),          F(0),          F(195710709),  F(1452924512), F(0),          F(0),          F(0),
        F(1952329685), F(96148781),   F(0),          F(0),          F(0),          F(195710709),  F(1452924512), F(0),          F(0),          F(0),
        F(585873079),  F(1685876329), F(536868877),  F(0),          F(1109395848), F(1695863772), F(0),          F(0),          F(585873079),  F(1685876329),
        F(536868877),  F(0),          F(1109395848), F(1695863772), F(0),          F(0),          F(1059050056), F(1272354281), F(0),          F(0),
        F(0),          F(345148307),  F(1996555369), F(0),          F(0),          F(0),          F(1059050056), F(1272354281), F(0),          F(0),
        F(0),          F(345148307),  F(1996555369), F(0),          F(0),          F(0),          F(549986946),  F(2083161398), F(1366734367), F(0),
        F(578980624),  F(600077120),  F(0),          F(0),          F(549986946),  F(2083161398), F(1366734367), F(0),          F(578980624),  F(600077120),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(561839735),  F(1721941415), F(1776004793), F(0),          F(0),          F(0),          F(0),          F(0),
        F(561839735),  F(1721941415), F(1776004793), F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(169227764),  F(216562038),
        F(1147034485), F(0),          F(0),          F(0),          F(0),          F(0),          F(169227764),  F(216562038),  F(1147034485), F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(1060157038), F(529533994),  F(536868877),  F(0),          F(0),          F(0),
        F(0),          F(0),          F(1060157038), F(529533994),  F(536868877),  F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),          F(0),
        F(635499207),  F(1638926877), F(1366734367), F(0),          F(0),          F(0),          F(0),          F(0),          F(635499207),  F(1638926877),
        F(1366734367), F(0),          F(0),          F(0),          F(0),          F(0)};

    std::vector<F> cfs = f(xxs);
    for (size_t ii = 0; ii < cfs.size(); ii++)
        if (target[ii] != cfs[ii]) {
            errors++;
            _MESSAGE("target[", ii, "] = ", target[ii], " coeff[", ii, "] = ", cfs[ii]);
        }

    if (errors == 0) _MESSAGE(Color::Code::FG_GREEN, "Test passed!");
    return errors;
#endif
return 0;
}
