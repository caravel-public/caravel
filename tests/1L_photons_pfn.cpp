#include <map>
#include <sstream>
#include <random>
#include <vector>

#include <functional>
#include <iostream>
#include <iterator>

#include "Core/settings.h"
#include "AmpEng/CoefficientEngine.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/ResidueEvaluator.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "FunctionalReconstruction/PeraroFunction.h"
#include "FunctionalReconstruction/ReconstructionTools.h"

#include "PoleStructure/Remainder.h"

#include "IntegralLibrary/MapToBasis.h"

#include "misc/TestUtilities.hpp"

using namespace std;
using namespace Caravel;
using namespace Caravel::Integrals;
using F = F32;
using TF = C;

int main(int argc, char* argv[]) {

    size_t cardinality(moduli.front());
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    //verbosity_settings.show_all();

    settings::use_setting("general::do_n_eq_n_test yes");
    settings::use_setting("BG::partial_trace_normalization full_Ds");
    settings::use_setting("general::master_basis pentagon_functions_new");
    settings::use_setting("integrals::integral_family_2L pentagon_functions_new");
    settings::use_setting("integrals::integral_family_1L pentagon_functions_new");
    settings::use_setting("IntegrandHierarchy::amplitude_normalization none");

    
    SM_color_ordered model;

    PhysRegSelector::set_region(5,{1,2});
    
    std::vector<BigRat> xs;
    if (argc >= 3) { xs = Caravel::detail::read_twistor_parameters(argv[2]); }
    else {
        xs = {BigRat(4, 3), BigRat(1, 11), BigRat(8, 13), BigRat(7, 17),BigRat(-1)}; // 5pt std point
    }


    
    //using theIntProvider = Integrals::IntegralProvider<Integrals::FBCoefficient<F, F>, Integrals::StdBasisFunction<Integrals::floating_partner_t<F>, Integrals::floating_partner_t<F>>>;
    //using OneLoopAmplType = Amplitude<F, AmpEng::CoefficientEngine<F>, theIntProvider>;

    Particle u1(("u1"),ParticleType(ParticleStatistics::fermion, ParticleFlavor::u, ParticleMass(), true),SingleState("qbp"),1,0);
    Particle ub2(("ub2"),ParticleType(ParticleStatistics::antifermion, ParticleFlavor::ub, ParticleMass(), true),SingleState("qm"),2,0);
    vector<Particle>y3pm{Particle("y3p",ParticleType("photon"),SingleState("p"),3,0),
			 Particle("y3m",ParticleType("photon"),SingleState("m"),3,0)};
    Particle y4("y4p",ParticleType("photon"),SingleState("p"),4,0);
    Particle y5("y5p",ParticleType("photon"),SingleState("p"),5,0);
    vector<Particle>g3pm{Particle("g3p",ParticleType("gluon"),SingleState("p"),3,0),
			 Particle("g3m",ParticleType("gluon"),SingleState("m"),3,0)};
    Particle g4("g4p",ParticleType("gluon"),SingleState("p"),4,0);
    Particle g5("g5p",ParticleType("gluon"),SingleState("p"),5,0);
    Particle gl("gl",ParticleType(ParticleStatistics::vector,
				  ParticleFlavor::gluon, ParticleMass(),
				  true),SingleState("default"));
    Particle ubl("ubl",ParticleType(ParticleStatistics::antifermion,
				    ParticleFlavor::ub, ParticleMass(),
				    true),SingleState("default"));

    START_TIMER(load_mapper);
    std::cout<<"loading mapper ..."<<std::endl;
    Integrals::MapToBasis<F, Integrals::StdBasisFunction<TF, TF>> mapper(std::string(DATADIR) + "/1L_qqggg_pentagon_new_replacements.txt");
    std::cout<<"mapper loaded"<<std::endl;
    STOP_TIMER(load_mapper);

    auto physical_point = rational_mom_conf<BigRat>(5, xs);
    momentumD_configuration<F, 4> tmp1 = static_cast<momentumD_configuration<F, 4>>(physical_point);

    vector<Particle> Internal_photons
	{ubl,gl,ubl,ubl,ubl};
    vector<Particle> Internal_gluons
	{ubl,gl,ubl,ubl,ubl};
    map<string,int> tree_couplings_photons {{"gw",3}}, tree_couplings_gluons {{"gs",3}};
    map<string,int> v_coupsW{{"gw",1}},v_coupsS{{"gs",1}};
    vector<map<string,int>> v_couplings_photons {v_coupsS,v_coupsS,v_coupsW,v_coupsW,v_coupsW}, v_couplings_gluons {v_coupsS,v_coupsS,v_coupsS,v_coupsS,v_coupsS};
    //gw versus gs coupling factor
    F factor(-8);
      
    size_t error_count=0;

    for(size_t iHel=0;iHel<y3pm.size();++iHel) {
	Particle y3(y3pm[iHel]),g3(g3pm[iHel]);
    
	vector<vector<Particle>>External_photons
	    {{u1},{ub2},{y3},{y4},{y5}};
	vector<vector<vector<Particle>>>External_gluons
	    {{{u1},{ub2},{g3},{g4},{g5}},
	     {{u1},{ub2},{g3},{g5},{g4}},
	     {{u1},{ub2},{g4},{g3},{g5}},
	     {{u1},{ub2},{g4},{g5},{g3}},
	     {{u1},{ub2},{g5},{g3},{g4}},
	     {{u1},{ub2},{g5},{g4},{g3}}};

	NumericFunctionBasisExpansion<Series<F>, std::string> res_photons;
    
	{
 
	    Amplitude<F, AmpEng::CoefficientEngine<F>, Integrals::sIntegralProvider<F> >
		ampl(AmpEng::CoefficientEngine<F>(std::make_shared<Model>(model),
                                AmpEng::ParentDiagrams{{External_photons},
						       {Internal_photons},
						       {v_couplings_photons},
						       tree_couplings_photons}));

	    auto res = ampl.integrate(tmp1);
    
	    START_TIMER(apply_mapper_photons);
	    res_photons = mapper(res,tmp1);
	    STOP_TIMER(apply_mapper_photons);
	}

	NumericFunctionBasisExpansion<Series<F>, std::string> res_gluons;
    
	for(size_t i=0;i<6;++i) {

	    Amplitude<F, AmpEng::CoefficientEngine<F>, Integrals::sIntegralProvider<F> >
		ampl(AmpEng::CoefficientEngine<F>(std::make_shared<Model>(model),
                                AmpEng::ParentDiagrams{{External_gluons[i]},
						       {Internal_gluons},
						       {v_couplings_gluons},
                                                       tree_couplings_gluons}));

	    auto res = ampl.integrate(tmp1);
    
	    START_TIMER(apply_mapper_gluons);
	    auto res_mapped = mapper(res,tmp1);
	    STOP_TIMER(apply_mapper_gluons);

	    res_gluons+=factor*res_mapped;
	
	}
	std::cout<<"photons-gluons for helicity configuration "<<iHel<<":"<<std::endl
		 <<res_photons-res_gluons<<std::endl;

	for(auto&it:(res_photons-res_gluons).get_all_coeffs())
	    if(it!=Series<F>(it.leading(),it.last()))
		error_count++;
	if(error_count)
	    _WARNING_RED("NO AGREEMENT in helicity configuration "
			 +std::to_string(iHel));
	else
	    _MESSAGE(Color::Code::FG_GREEN, "AGREEMENT", Color::Code::FG_DEFAULT);
    }
    _MESSAGE("total errors:",error_count);
    
    return error_count;
}


