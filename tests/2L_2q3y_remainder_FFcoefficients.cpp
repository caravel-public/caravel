#include "Core/CaravelInput.h"

#include "Core/settings.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "IntegralLibrary/MapToBasis.h"

#include "PoleStructure/Remainder.h"

#include "misc/CaravelCereal.hpp"
#include "misc/TestUtilities.hpp"

#include "Core/Timing.h"

using namespace Caravel;
using namespace std;
using namespace Caravel::Integrals;

int main(int argc, char* argv[]) {

    std::string command_input("PartialAmplitudeInput[Particles[Particle[u,1,qbp],Particle[ub,2,qm],Particle[photon,3,m],Particle[photon,4,p],Particle[photon,5,p]]]");

    verbosity_settings.show_all();

    settings::use_setting("general::do_n_eq_n_test yes");

    // use all available cores for computing integrand coefficients
    settings::use_setting("IntegrandHierarchy::parallelize_topology_fitting warmup");

    using F = F32;
    using namespace IntegrandHierarchy;
    
    cardinality_update_manager<F>::get_instance()->set_cardinality(2147483629);

    PartialAmplitudeInput pa(command_input);

    const auto& process = pa.get_particles();

    std::cout << Color::Code::FG_BLUE;
    _MESSAGE(pa.get_id_string());
    std::cout << Color::Code::FG_DEFAULT;

    // For 5-point processes express the integrals in terms of pentagon functions.
    set_settings_for_standard_choice_of_integrals(pa.get_multiplicity());
    settings::use_setting("integrals::integral_family_1L pentagon_functions_new");
    settings::use_setting("integrals::integral_family_2L pentagon_functions_new");
    settings::use_setting("general::master_basis pentagon_functions_new");

    PhysRegSelector::set_region(5,{1,2});

    SM_color_ordered model;

    auto physical_point = rational_mom_conf<BigRat>(process.size(), {BigRat(44564), BigRat(1456456), BigRat(86546), BigRat(745645),BigRat(-1545)});

    auto ps = static_cast<momentumD_configuration<F,4>>(physical_point);

    START_TIMER(construction);
    _MESSAGE(Color::Code::FG_BLUE,"Constructing remainder");
    // Construct a remainder from the partial amplitude input in the chosen model.
    FiniteRemainder::TwoLoopRemainder<F,C> remainder(model,pa);
    _MESSAGE(Color::Code::FG_BLUE,"Constructing mapper to pentagon function basis");
    // Construct a mapper that maps all the pentagon functions appearing in the amplitude to a basis.
    Integrals::MapToBasis<F, Integrals::StdBasisFunction<C, C>> mapper(pa);
    STOP_TIMER(construction);

    START_TIMER(evaluation);
    // Evaluate the amplitude on the phase space point in the finite field and map the pentagon functions to a basis.
    _MESSAGE(Color::Code::FG_BLUE,"Computing remainder (will do warmup run if required)");
    auto result_coeffs = mapper(remainder(ps),ps);
    STOP_TIMER(evaluation);

    _MESSAGE("");

    size_t err_count = 0;

    // Check that the remainder has no poles.
    if( check_if_poles_vanish(result_coeffs) ){
        _MESSAGE(Color::Code::FG_GREEN, "ALL POLES CANCEL", Color::Code::FG_DEFAULT);
    }
    else{
        _WARNING_RED("POLES DON'T CANCEL");
        ++err_count;
    }

    _MESSAGE("\nChecking that all coefficients are the same as when the test was written...");

    {
        std::string filename = "test_data/" + pa.get_id_string() + "_saved_F_coeffs.dat";
        //write_serialized(result_coeffs, filename);
        auto target = read_serialized<decltype(result_coeffs)>(filename);
        for (auto& [f,c] : target) {
            if (result_coeffs.get_coeff(f)[0] != c[0]) {
                ++err_count;
                _MESSAGE(Color::Code::FG_RED,"Coefficient of ", f, " doesn't agree! ", result_coeffs.get_coeff(f)[0], " != ", c[0], "(target)");
            }
        }
    }


    if(err_count>0){
        _MESSAGE(Color::Code::FG_RED, "FAIL");
    }
    else{
        _MESSAGE(Color::Code::FG_GREEN, "PASS");
    }

    _MESSAGE("");

    return err_count;
}


