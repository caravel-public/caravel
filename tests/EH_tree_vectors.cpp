#include "Core/Debug.h"
#include "Core/Particle.h"
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/typedefs.h"
#include "Forest/Builder.h"
#include "Forest/Forest.h"
#include "Forest/Model.h"
#include "PhaseSpace/PhaseSpace.h"
#include "misc/TestUtilities.hpp"
#include "Core/InversionStrategies.h"
#include "misc/DeltaZero.h"
#include "EH_tree_vectors.hpp"

#define DEBUG_TEST

using namespace Caravel;

// Generate 3-particle on-shell phase space point with complex momenta
template <typename T, size_t Ds> momD_conf<T, Ds> OS_3point(const T& mass, bool random_boost = false) {
    T msq = mass * mass;

    // First construct momenta in (+,-.-,-) signature
    std::vector<std::vector<T>> moms(3);
    if (std::abs(mass) > std::abs(T(0))) {
        moms[0] = {(T(-1) - msq) / T(2), (T(2) - T(3) * msq) / T(4), T(0, 1) * (T(-2) + T(3) * msq) / T(4), (T(1) - msq) / T(2)};
        moms[2] = {(T(-2) + msq + T(10) * msq * msq) / (T(8) * msq), -(T(1) + T(3) * msq / T(8)), T(0, 1) * T(3) * (T(4) + msq) / T(8),
                   (T(-1) + T(2) / msq + T(10) * msq) / T(8)};
        moms[1] = {(T(3) + T(2) / msq - T(6) * msq) / T(8), (T(4) + T(9) * msq) / T(8), -T(0, 1) * (T(1) + T(9) * msq / T(8)),
                   -(T(3) + T(2) / msq + T(6) * msq) / T(8)};
    }
    else {
        T energy = T(500);
        moms[0] = {-energy, T(0), T(0), -energy};
        moms[1] = {energy / T(2), T(1), T(0, 1), energy / T(2)};
        moms[2] = {energy / T(2), T(-1), T(0, -1), energy / T(2)};
    }

    std::vector<momD<T, Ds>> qmom;
    if (random_boost) {
        const T r1 = T(rand()) / T(RAND_MAX), r2 = T(M_PI) * T(rand()) / T(RAND_MAX), r3 = T(2) * T(M_PI) * T(rand()) / T(RAND_MAX);
        const T bx = r1 * cos(r3) * sin(r2), by = r1 * sin(r3) * sin(r2), bz = r1 * cos(r2);
        const T bsq = bx * bx + by * by + bz * bz, gamma = T(1) / sqrt(T(1) - bsq);
        const std::vector<std::vector<T>> L = {
            {gamma, -gamma * bx, -gamma * by, -gamma * bz},
            {-gamma * bx, T(1) + (gamma - T(1)) * bx * bx / bsq, (gamma - T(1)) * bx * by / bsq, (gamma - T(1)) * bx * bz / bsq},
            {-gamma * by, (gamma - T(1)) * by * bx / bsq, T(1) + (gamma - T(1)) * by * by / bsq, (gamma - T(1)) * by * bz / bsq},
            {-gamma * bz, (gamma - T(1)) * bz * bx / bsq, (gamma - T(1)) * bz * by / bsq, T(1) + (gamma - T(1)) * bz * bz / bsq},
        };
        for (size_t n = 0; n < 3; n++) {
            std::vector<T> q = {T(0), T(0), T(0), T(0)};
            for (size_t i = 0; i < 4; i++) {
                for (size_t j = 0; j < 4; j++) q[i] += L[i][j] * moms[n][j];
            }
            qmom.push_back(momD<T, Ds>(q));
        }
    }
    else {
        for (size_t n = 0; n < 3; n++) qmom.push_back(momD<T, Ds>(moms[n]));
    }
    momD_conf<T, 4> pspoint = momD_conf<T, 4>(qmom);
    return pspoint;
}

// Generate random N-particle phase space point
template <typename T> momD_conf<std::complex<T>, 4> OS_Npoint(const std::vector<T>& mass, bool random_boost = false, size_t seed = 42) {
    const T energy = T(500);
    const size_t N = mass.size();
    PhaseSpace<T> Tph(N, energy, mass);
    Tph.set_reset_seed(false);
    srand(seed);
    Tph.generate();
    auto moms = Tph.mom();

    std::vector<momD<T, 4>> qmom;
    if (random_boost) {
        const T r1 = T(rand()) / T(RAND_MAX), r2 = T(M_PI) * T(rand()) / T(RAND_MAX), r3 = T(2) * T(M_PI) * T(rand()) / T(RAND_MAX);
        const T bx = r1 * cos(r3) * sin(r2), by = r1 * sin(r3) * sin(r2), bz = r1 * cos(r2);
        const T bsq = bx * bx + by * by + bz * bz, gamma = T(1) / sqrt(T(1) - bsq);
        const std::vector<std::vector<T>> L = {
            {gamma, -gamma * bx, -gamma * by, -gamma * bz},
            {-gamma * bx, T(1) + (gamma - T(1)) * bx * bx / bsq, (gamma - T(1)) * bx * by / bsq, (gamma - T(1)) * bx * bz / bsq},
            {-gamma * by, (gamma - T(1)) * by * bx / bsq, T(1) + (gamma - T(1)) * by * by / bsq, (gamma - T(1)) * by * bz / bsq},
            {-gamma * bz, (gamma - T(1)) * bz * bx / bsq, (gamma - T(1)) * bz * by / bsq, T(1) + (gamma - T(1)) * bz * bz / bsq},
        };
        for (size_t n = 0; n < N; n++) {
            std::vector<T> q = {T(0), T(0), T(0), T(0)};
            for (size_t i = 0; i < 4; i++) {
                for (size_t j = 0; j < 4; j++) q[i] += L[i][j] * moms[n][j];
            }
            qmom.push_back(momD<T, 4>(q));
        }
    }
    else {
        for (size_t n = 0; n < N; n++) qmom.push_back(momD<T, 4>(moms[n]));
    }

    return to_momDconf<T, 4>(qmom);
}

#define _GET_BUILDER(cK, N)                                                                                                                                    \
    EHGravity model2use;                                                                                                                                       \
    Forest SW(&model2use);                                                                                                                                     \
    unsigned track = SW.add_process(Process(input), {{"K", cK}});                                                                                              \
    std::vector<size_t> dims = {4};                                                                                                                            \
    SW.define_internal_states_and_close_forest(dims);                                                                                                          \
    Builder<T, 4> bob(&SW, N);                                                                                                                                 \
    bob.set_p(pspoint);                                                                                                                                        \
    bob.compute();                                                                                                                                             \
    bob.contract();

// Forward declaration
template <typename T, typename TR> int check_2S2V_amp(const std::string&, const std::string&);
template <typename T, typename TR> int check_1G2V_amp(const std::string&, const std::string&, const std::string&);
template <typename T, typename TR> int check_4V_amp(const std::string&, const std::string&, const std::string&, const std::string&);
template <typename T, typename TR> int check_2V2G_amp(const std::string&, const std::string&, const std::string&, const std::string&);
template <typename T, typename TR> int check_2V3G_amp(const std::string&, const std::string&, const std::string&, const std::string&, const std::string&);

int main(int argc, char* argv[]) {
    const std::vector<std::string> Vpol = {"p", "m", "L"}, Hpol = {"hpp", "hmm"};
    int n_errors = 0, n_tests = 0;

    // Perform tests on G -> VV
    for (auto& s1 : Hpol) {
        for (auto& s2 : Vpol) {
            for (auto& s3 : Vpol) {
                n_tests += 2;
                n_errors += check_1G2V_amp<C, R>(s1, s2, s3);
            }
        }
    }

    // Perform tests on SS -> VV
    for (auto& s1 : Vpol) {
        for (auto& s2 : Vpol) {
            n_tests += 2;
            n_errors += check_2S2V_amp<C, R>(s1, s2);
        }
    }

#ifdef HIGH_PRECISION
    // Perform tests on VV -> VV
    for (auto& s1 : Vpol) {
        for (auto& s2 : Vpol) {
            for (auto& s3 : Vpol) {
                for (auto& s4 : Vpol) {
                    n_tests++; // no re-ordering
                    n_errors += check_4V_amp<CHP, RHP>(s1, s2, s3, s4);
                }
            }
        }
    }

    // Perform tests on VV -> GG
    for (auto& s1 : Vpol) {
        for (auto& s2 : Vpol) {
            for (auto& s3 : Hpol) {
                for (auto& s4 : Hpol) {
                    n_tests += 2;
                    n_errors += check_2V2G_amp<CHP, RHP>(s1, s2, s3, s4);
                }
            }
        }
    }

    // Perform tests on VV -> GG [Ward]
    for (auto& s1 : Vpol) {
        for (auto& s2 : Vpol) {
            for (auto& s3 : Hpol) {
                n_tests += 4;
                n_errors += check_2V2G_amp<CHP, RHP>(s1, s2, s3, "hWard");
                n_errors += check_2V2G_amp<CHP, RHP>(s1, s2, "hWard", s3);
            }
            n_tests += 2;
            n_errors += check_2V2G_amp<CHP, RHP>(s1, s2, "hWard", "hWard");
        }
    }

    // Perform tests on VV -> GGG [Ward]
    for (auto& s1 : Vpol) {
        for (auto& s2 : Vpol) {
            for (auto& s3 : Hpol) {
                for (auto& s4 : Hpol) {
                    n_tests += 6;
                    n_errors += check_2V3G_amp<CHP, RHP>(s1, s2, s3, s4, "hWard");
                    n_errors += check_2V3G_amp<CHP, RHP>(s1, s2, s3, "hWard", s4);
                    n_errors += check_2V3G_amp<CHP, RHP>(s1, s2, "hWard", s3, s4);
                }
                n_tests += 6;
                n_errors += check_2V3G_amp<CHP, RHP>(s1, s2, s3, "hWard", "hWard");
                n_errors += check_2V3G_amp<CHP, RHP>(s1, s2, "hWard", s3, "hWard");
                n_errors += check_2V3G_amp<CHP, RHP>(s1, s2, "hWard", "hWard", s3);
            }
#ifdef VERY_HIGH_PRECISION
            n_tests += 2;
            n_errors += check_2V3G_amp<CVHP, RVHP>(s1, s2, "hWard", "hWard", "hWard");
#endif
        }
    }

#endif

    if (n_errors == 0)
        _MESSAGE(Color::Code::FG_GREEN, "EH_tree_vector: ", n_tests, " tests PASSED!");
    else
        _MESSAGE(Color::Code::FG_RED, "EH_tree_vector test FAILED.  ", n_errors, "/", n_tests, " tests failed!");

    return n_errors;
}

/**
 * Check 5-pt amplitudes M(VVGGG)
 */
template <typename T, typename TR>
int check_2V3G_amp(const std::string& V1pol, const std::string& V2pol, const std::string& H1pol, const std::string& H2pol, const std::string& H3pol) {
    int n_errors = 0;
    double tol = 9.0;

    // M5(V,V,G,G,G)
    {
        Particle V1("V1", ParticleType("V1"), SingleState(V1pol), 1, 0);
        Particle V2("V2", ParticleType("V1"), SingleState(V2pol), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState(H1pol), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState(H2pol), 4, 0);
        Particle G5("G5", ParticleType("G"), SingleState(H3pol), 5, 0);
        std::vector<Particle*> input = {&V1, &V2, &G3, &G4, &G5};
        std::vector<TR> masses = {V1.get_mass(TR(1)), V2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1)), G5.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(3, 5)

        // Comparison
        T klt = T(0);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "V1(", V1pol, ") V2(", V2pol, ") -> G3(", H1pol, ") G4(", H2pol, ") G5(", H3pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(V,V,G,G,G) KLT: ", klt, " Tree: ", tree, " ", klt / tree);
#endif
        if (check(klt / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(V,V,G,G,G) passed!");
        else if (check(klt, T(0), tol) && check(tree, T(0), tol)) {
            _MESSAGE(Color::Code::FG_GREEN, "M(V,V,G,G,G) passed!");
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "M(V,V,G,G,G) failed!");
            n_errors++;
        }
    }

    // M5(V,V,G,G,G) - [re-ordered]
    {
        Particle G3("G3", ParticleType("G"), SingleState(H1pol), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState(H2pol), 4, 0);
        Particle G5("G5", ParticleType("G"), SingleState(H3pol), 5, 0);
        Particle V1("V1", ParticleType("V1"), SingleState(V1pol), 1, 0);
        Particle V2("V2", ParticleType("V1"), SingleState(V2pol), 2, 0);
        std::vector<Particle*> input = {&V1, &V2, &G3, &G4, &G5};
        std::vector<TR> masses = {V1.get_mass(TR(1)), V2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1)), G5.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(3, 5)

        // Comparison
        T klt = T(0);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "V1(", V1pol, ") V2(", V2pol, ") -> G3(", H1pol, ") G4(", H2pol, ") G5(", H3pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(V,V,G,G,G) [re-orderd] KLT: ", klt, " Tree: ", tree, " ", klt / tree);
#endif
        if (check(klt / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(V,V,G,G,G) [re-ordered] passed!");
        else if (check(klt, T(0), tol) && check(tree, T(0), tol)) {
            _MESSAGE(Color::Code::FG_GREEN, "M(V,V,G,G,G) [re-ordered] passed!");
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "M(V,V,G,G,G) [re-ordred] failed!");
            n_errors++;
        }
    }
    return n_errors;
}

/**
 * Check 4-pt amplitudes M(VVGG)
 */
template <typename T, typename TR> int check_2V2G_amp(const std::string& V1pol, const std::string& V2pol, const std::string& H1pol, const std::string& H2pol) {
    int n_errors = 0;
    double tol = 9.0;

    // M4(V,V,G,G)
    {
        Particle V1("V1", ParticleType("V1"), SingleState(V1pol), 1, 0);
        Particle V2("V2", ParticleType("V1"), SingleState(V2pol), 2, 0);
        Particle G3("G3", ParticleType("G"), SingleState(H1pol), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState(H2pol), 4, 0);
        std::vector<Particle*> input = {&V1, &V2, &G3, &G4};
        std::vector<TR> masses = {V1.get_mass(TR(1)), V2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4(pspoint, V1, V2, G3, G4);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "V1(", V1pol, ") V2(", V2pol, ") -> G3(", H1pol, ") G4(", H2pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(V,V,G,G) KLT: ", klt, " Tree: ", tree, " ", klt / tree);
#endif
        if (check(klt / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(V,V,G,G) passed!");
        else if (check(klt, T(0), tol) && check(tree, T(0), tol)) {
            _MESSAGE(Color::Code::FG_GREEN, "M(V,V,G,G) passed!");
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "M(V,V,G,G) failed!");
            n_errors++;
        }
    }

    // M4(V,V,G,G) - [re-ordered]
    {
        Particle G3("G3", ParticleType("G"), SingleState(H1pol), 3, 0);
        Particle G4("G4", ParticleType("G"), SingleState(H2pol), 4, 0);
        Particle V1("V1", ParticleType("V1"), SingleState(V1pol), 1, 0);
        Particle V2("V2", ParticleType("V1"), SingleState(V2pol), 2, 0);
        std::vector<Particle*> input = {&V1, &V2, &G3, &G4};
        std::vector<TR> masses = {V1.get_mass(TR(1)), V2.get_mass(TR(1)), G3.get_mass(TR(1)), G4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T klt = M4(pspoint, V1, V2, G3, G4);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "V1(", V1pol, ") V2(", V2pol, ") -> G3(", H1pol, ") G4(", H2pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(V,V,G,G) [re-orderd] KLT: ", klt, " Tree: ", tree, " ", klt / tree);
#endif
        if (check(klt / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(V,V,G,G) [re-ordered] passed!");
        else if (check(klt, T(0), tol) && check(tree, T(0), tol)) {
            _MESSAGE(Color::Code::FG_GREEN, "M(V,V,G,G) [re-ordered] passed!");
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "M(V,V,G,G) [re-ordred] failed!");
            n_errors++;
        }
    }
    return n_errors;
}

/**
 * Check amplitude for S1(p1) S2(p2) -> V1(p3) V2(p4)
 */
template <typename T, typename TR> int check_2S2V_amp(const std::string& V1pol, const std::string& V2pol) {
    int n_errors = 0;
    double tol = 8.0;

    // M4(S1,S2,V1,V2)
    {
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S2("S2", ParticleType("Phi1"), SingleState("RealSC"), 2, 0);
        Particle V1("V1", ParticleType("V2"), SingleState(V1pol), 3, 0);
        Particle V2("V2", ParticleType("V2"), SingleState(V2pol), 4, 0);
        std::vector<Particle*> input = {&S1, &S2, &V1, &V2};
        std::vector<TR> masses = {S1.get_mass(TR(1)), S2.get_mass(TR(1)), V1.get_mass(TR(1)), V2.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T amp = Mssvv(pspoint, S1, S2, V1, V2);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "S1 S2 -> V1(", V1pol, ") V2(", V2pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(S1,S2,V1,V2) Amp: ", amp, " Tree: ", tree, " ", amp / tree);
#endif
        if (check(amp / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(S1,S2,V1,V2) passed!");
        else if (check(amp, T(0), tol) && check(tree, T(0), tol)) {
            _MESSAGE(Color::Code::FG_GREEN, "M(S1,S2,V1,V2) passed!");
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "M(S1,S2,V1,V2) failed!");
            n_errors++;
        }
    }

    // M4(S1,S2,V1,V2) re-ordered
    {
        Particle V1("V1", ParticleType("V2"), SingleState(V1pol), 3, 0);
        Particle V2("V2", ParticleType("V2"), SingleState(V2pol), 4, 0);
        Particle S1("S1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
        Particle S2("S2", ParticleType("Phi1"), SingleState("RealSC"), 2, 0);
        std::vector<Particle*> input = {&S1, &S2, &V1, &V2};
        std::vector<TR> masses = {S1.get_mass(TR(1)), S2.get_mass(TR(1)), V1.get_mass(TR(1)), V2.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T amp = Mssvv(pspoint, S1, S2, V1, V2);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "S1 S2 -> V1(", V1pol, ") V2(", V2pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(S1,S2,V1,V2) Amp: ", amp, " Tree: ", tree, " ", amp / tree);
#endif
        if (check(amp / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(S1,S2,V1,V2) [re-ordered] passed!");
        else {
            _MESSAGE(Color::Code::FG_RED, "M(S1,S2,V1,V2) [re-ordred] failed!");
            n_errors++;
        }
    }

    return n_errors;
}

/**
 * Check amplitude for V1(p1) V2(p2) -> V2(p3) V1(p4)
 */
template <typename T, typename TR> int check_4V_amp(const std::string& V1pol, const std::string& V2pol, const std::string& V3pol, const std::string& V4pol) {
    int n_errors = 0;
    double tol = 8.0, tolzero = 6.0;

    {
        Particle V1("V1", ParticleType("V1"), SingleState(V1pol), 1, 0);
        Particle V2("V2", ParticleType("V2"), SingleState(V2pol), 2, 0);
        Particle V3("V3", ParticleType("V2"), SingleState(V3pol), 3, 0);
        Particle V4("V4", ParticleType("V1"), SingleState(V4pol), 4, 0);
        std::vector<Particle*> input = {&V1, &V2, &V3, &V4};
        std::vector<TR> masses = {V1.get_mass(TR(1)), V2.get_mass(TR(1)), V3.get_mass(TR(1)), V4.get_mass(TR(1))};
        auto pspoint = OS_Npoint(masses);

        // Setup Builder
        _GET_BUILDER(2, 4)

        // Comparison
        T amp = Mvvvv(pspoint, V1, V2, V3, V4);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "V1(", V1pol, ") V2(", V2pol, ") -> V3(", V3pol, ") V4(", V4pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(V1,V2,V2,V1) Amp: ", amp, " Tree: ", tree, " ", amp / tree);
#endif
        if (check(amp / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(V1,V2,V2,V1) passed!");
        else if (check(amp, T(0), tolzero) && check(tree, T(0), tol)) {
            _MESSAGE(Color::Code::FG_GREEN, "M(S1,S2,V1,V2) passed!");
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "M(V1,V2,V2,V1) failed!");
            n_errors++;
        }
    }

    return n_errors;
}

template <typename T, typename TR> int check_1G2V_amp(const std::string& Hpol, const std::string& V1pol, const std::string& V2pol) {
    int n_errors = 0;
    double tol = 9.0;

    // M3(G,V,V)
    {
        Particle G1("G1", ParticleType("G"), SingleState(Hpol), 2, 0);
        Particle V2("V2", ParticleType("V1"), SingleState(V1pol), 1, 0);
        Particle V3("V3", ParticleType("V1"), SingleState(V2pol), 3, 0);
        std::vector<Particle*> input = {&G1, &V2, &V3};
        const T mass = V2.get_mass(T(1));
        auto pspoint = OS_3point<T, 4>(mass);

        // Setup Builder
        _GET_BUILDER(1, 3)

        // Comparison
        T klt = Mgvv(pspoint, G1, V2, V3);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "G(", Hpol, ") -> V1(", V1pol, ") V2(", V2pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(G,V,V) KLT: ", klt, " Tree: ", tree, " ", klt / tree);
#endif
        if (check(klt / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(G,V,V) passed!");
        else if (check(klt, T(0), tol) && check(tree, T(0), tol)) {
            _MESSAGE(Color::Code::FG_GREEN, "M(G,V,V) passed!");
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "M(G,V,V) failed!");
            n_errors++;
        }
    }

    // M3(G,V,V) - [re-ordered]
    {
        Particle V2("V2", ParticleType("V1"), SingleState(V1pol), 1, 0);
        Particle V3("V3", ParticleType("V1"), SingleState(V2pol), 3, 0);
        Particle G1("G1", ParticleType("G"), SingleState(Hpol), 2, 0);
        std::vector<Particle*> input = {&G1, &V2, &V3};
        const T mass = V2.get_mass(T(1));
        auto pspoint = OS_3point<T, 4>(mass);

        // Setup Builder
        _GET_BUILDER(1, 3)

        // Comparison
        T klt = Mgvv(pspoint, G1, V2, V3);
        T tree = bob.get_tree(track);
#ifdef DEBUG_TEST
        _MESSAGE(Color::Code::FG_BLUE, "G(", Hpol, ") -> V1(", V1pol, ") V2(", V2pol, ")");
        _MESSAGE(Color::Code::FG_BLUE, "M(G,V,V) KLT: ", klt, " Tree: ", tree, " ", klt / tree);
#endif
        if (check(klt / tree, T(1), tol))
            _MESSAGE(Color::Code::FG_GREEN, "M(G,V,V) [re-ordered] passed!");
        else if (check(klt, T(0), tol) && check(tree, T(0), tol)) {
            _MESSAGE(Color::Code::FG_GREEN, "M(G,V,V) [re-ordered] passed!");
        }
        else {
            _MESSAGE(Color::Code::FG_RED, "M(G,V,V) [re-ordered] failed!");
            n_errors++;
        }
    }
    return n_errors;
}
