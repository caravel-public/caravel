#include "Forest/Forest.h"
#include "PhaseSpace/PhaseSpace.h"

#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "Forest/Builder.h"

#include "Core/typedefs.h"
#include "Core/Utilities.h"

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>
#include <algorithm>

#include "Core/type_traits_extra.h"

#include "wise_enum.h"

// use hard coded moments instead of generated ones
#define FIXED_MOMENTA 1

using namespace std;
using namespace Caravel;

namespace Color {
enum struct Code {
    FG_RED = 31,
    FG_GREEN = 32,
    FG_BLUE = 34,
    FG_DEFAULT = 39,
    BG_RED = 41,
    BG_GREEN = 42,
    BG_BLUE = 44,
    BG_DEFAULT = 49
};
}
namespace std{
    std::ostream& operator<<(std::ostream& os, const Color::Code& mod) {
        return os << "\033[" << static_cast<int>(mod) << "m";
    }
}
Particle* qm(ParticleFlavor f = ParticleFlavor::q){
    return new Particle(std::string(wise_enum::to_string(f))+std::string(" "),ParticleType(ParticleStatistics::fermion, f, ParticleMass(), true),SingleState("qm"));
}
Particle* qbp(ParticleFlavor f = ParticleFlavor::qb){
    return new Particle(std::string(wise_enum::to_string(f))+std::string(" "),ParticleType(ParticleStatistics::antifermion, f, ParticleMass(), true),SingleState("qbp"));
}
Particle* p(){
    return new Particle("p ",ParticleType("gluon"),SingleState("p"));
}
Particle* m(){
    return new Particle("m ",ParticleType("gluon"),SingleState("m"));
}

momD_conf<C,4> get_psp(unsigned N);

int main(int argc, char* argv[]){

    _MESSAGE("=============================================================================================");
    _MESSAGE("Test tree amplitudes with massless quarks. Testing self-consistency (reflection, rotation) and comparing with BHlib_Massive targets.");
    _MESSAGE("=============================================================================================");
    _MESSAGE("We have a (-1)^{# particles} * (-1)^{# q-qb pairs} 'SIGN' compared to BHlib_Massive for all amplitudes! Counterclockwise (Caravel) vs. Clockwise (BH) and little group scaling for fermions");
    _MESSAGE("=============================================================================================");

    // number of errors
    size_t err = 0;

    // Put processes to test here
    vector<vector<Particle*>> all = {
        {qm(),m(),p(),m(),qbp()},
        {qm(),m(),p(),p(),p(),qbp()},
        {qm(),m(),p(),qbp(),m(),p()},
        {qbp(),m(),qm(),p(),m(),p()},
        {qm(ParticleFlavor::u),qbp(ParticleFlavor::ub),qm(ParticleFlavor::d),qbp(ParticleFlavor::db)},
        {qm(ParticleFlavor::u),qm(ParticleFlavor::d),qbp(ParticleFlavor::db),qbp(ParticleFlavor::ub)},
        {qm(ParticleFlavor::u),qbp(ParticleFlavor::ub),m(),qm(ParticleFlavor::d),qbp(ParticleFlavor::db)},
        {qm(ParticleFlavor::u),qbp(ParticleFlavor::ub),m(),qm(ParticleFlavor::d),qbp(ParticleFlavor::db),p(),m(),p()},
        {qm(ParticleFlavor::u),qbp(ParticleFlavor::ub),qm(ParticleFlavor::d),qbp(ParticleFlavor::db),qm(ParticleFlavor::s),qbp(ParticleFlavor::sb)},
        {qm(ParticleFlavor::u),qbp(ParticleFlavor::db),qm(ParticleFlavor::d),qm(ParticleFlavor::s),qbp(ParticleFlavor::sb),qbp(ParticleFlavor::ub)},
        {qm(ParticleFlavor::u),m(),qbp(ParticleFlavor::db),qm(ParticleFlavor::d),qm(ParticleFlavor::s),qbp(ParticleFlavor::sb),qbp(ParticleFlavor::ub)},
        {qm(ParticleFlavor::u),p(),qbp(ParticleFlavor::db),qm(ParticleFlavor::d),qm(ParticleFlavor::s),qbp(ParticleFlavor::sb),qbp(ParticleFlavor::ub),qbp(ParticleFlavor::cb),qm(ParticleFlavor::c)},
    };

    vector<C> results = {
        {1.07238288869734566e+00,1.14879062906284801e+00},
        {-1.61892690549278093e+01,1.96102916451328859e+01},
        {-3.84358487416376526e+00,-1.80962850649864180e+01},
        {-1.37532358910866215e+01,-1.34483621624233862e+00},
        {-2.22381928977562870e-01,1.65620877085856960e-01},
        {1.14908081836880172e+00,-7.70799009623458597e-01},
        {1.47121719641628124e-01,1.05682269583174615e-01},
        {-4.71718553514372729e+00,-8.53019231312828197e+00},
        {-8.37982301999144807e-01,-3.45673735608108712e-01},
        {6.04483479553286074e-03,-5.09766541726900169e-01},
        {8.43190202200773520e+00,-1.50322818959377660e+01},
        {3.06334655904424210e+02,-2.17169318078667203e+02}
    };


    for(unsigned n =0; n<all.size(); n++){
        auto& particles = all.at(n);
        // Building the forest
        cout << Color::Code::FG_BLUE;
        _MESSAGE("");
        _MESSAGE("Checking process ", Process(particles));
        _MESSAGE(" > Building the forest: ");
        cout << Color::Code::FG_DEFAULT;
        // build forest for cuts

        // Generic pointer to Model
        Model * model2use;
        // pick the model
        model2use=new SM_color_ordered();

        // Now construct the Forest
        Forest SW(model2use);

        std::vector<pair<unsigned,Process>> processes;
        std::vector<pair<unsigned,Process>> processes_rev;


        //set all momenta indices trivially
        for(unsigned i=1; i<=particles.size(); i++){
            particles.at(i-1)->set_momentum_index(i);
        }

        unsigned N = particles.size();

        for(unsigned i=1; i<=particles.size(); i++){
            Process p(particles);
            unsigned track = SW.add_process(Process(particles),{{"gs",(int)N-2}});
            processes.emplace_back(track,p);

            rotate(particles.begin(),particles.begin()+1,particles.end());
        }

        reverse(begin(particles),end(particles));

        for(unsigned i=1; i<=particles.size(); i++){
            Process p(particles);
            unsigned track = SW.add_process(Process(particles),{{"gs",(int)N-2}});
            processes_rev.emplace_back(track,p);

            rotate(particles.begin(),particles.begin()+1,particles.end());
        }

        //const std::vector<Particle*>& particles({get_qm(1),get_p(2),get_qbp(3),get_qbp(4),get_qm(5),get_p(6),get_m(7)});


        //_PRINT(processes);
        //_PRINT(processes_rev);

        //print_all_currents(SW);

        vector<size_t> dims { 4 };
        SW.define_internal_states_and_close_forest(dims);

        cout << Color::Code::FG_BLUE;
        _MESSAGE(" > Evaluating: ");
        cout << Color::Code::FG_DEFAULT;
#if FIXED_MOMENTA
	auto momconf_process = get_psp(N);
#else 
        PhaseSpace<R> tph(N,1.);
        tph.set_reset_seed(false);
        srand(1010);

        tph.generate();
        //_MESSAGE(tph);
        //for(auto s: tph.mom()) std::cout << std::setprecision(16) << s << std::endl;

        auto moms = tph.mom_double();
        auto momconf_process = to_momDconf<R,4>(moms);
#endif
        // Now build Builder
        Builder<C,4> treecutSource(&SW,N);

        treecutSource.set_p(momconf_process);
        // compute all tree currents
        treecutSource.compute();
        // Make contractions to compute trees
        treecutSource.contract();

        auto target = treecutSource.get_tree(processes.at(0).first);
        int sign = 1;
        if (N%2 != 0) sign = -1;

        cout<<scientific;
        cout<<setprecision(16);

        _MESSAGE("* Rotated");
        for(auto& it:processes){
            auto x = treecutSource.get_tree(it.first);
            if (check(x,target)){
                _MESSAGE("Process ",it.second," \t\t",Color::Code::FG_GREEN,"PASS",Color::Code::FG_DEFAULT);
            }
            else{
                err++;
                _MESSAGE("Process ",it.second," \t\t",Color::Code::FG_RED,"FAIL",Color::Code::FG_DEFAULT);
                _MESSAGE("> ",x," != target ", target);
            }
        }
        _MESSAGE("");
        _MESSAGE("* Reversed + rotated");
        for(auto& it:processes_rev){
            auto x = treecutSource.get_tree(it.first);
            x *= decltype(x)(sign);
            if (check(x,target)){
                _MESSAGE("Process ",it.second," \t\t",Color::Code::FG_GREEN,"PASS",Color::Code::FG_DEFAULT);
            }
            else{
                err++;
                _MESSAGE("Process ",it.second," \t\t",Color::Code::FG_RED,"FAIL",Color::Code::FG_DEFAULT);
                _MESSAGE("> ",x," != target ", target);
            }
        }

        if(n<results.size()){
            // adjust for the little group scaling
            {
                int n_q = 0;
                int n_qb = 0;
                for (auto& it : particles) {
                    if (it->get_statistics() == ParticleStatistics::fermion && it->is_colored() == true) ++n_q;
                    if (it->get_statistics() == ParticleStatistics::antifermion && it->is_colored() == true) ++n_qb;
                }
                assert(n_q == n_qb);
                // 'SIGN' account for (-1)^(# q-qb pairs)
                if (n_q % 2 == 1) sign *=  -1;
            }

            // 'SIGN' account for (-1)^(N)
            if(N%2 == 1)
                sign *= -1;

            auto stored = results.at(n);
            stored *= decltype(stored)(sign);

            if (check(target,stored)){
                _MESSAGE("");
                _MESSAGE("* Value ","\t\t\t\t",Color::Code::FG_GREEN,"PASS",Color::Code::FG_DEFAULT);
            }
            else{
                err++;
                _MESSAGE("* Value "," \t\t\t\t",Color::Code::FG_RED,"FAIL",Color::Code::FG_DEFAULT);
                _MESSAGE("> ",target," != stored target ", stored);
            }
        }

    }

    _MESSAGE("");
    _MESSAGE("===============================");
    _MESSAGE("TOTAL NUMBER OF ERRORS: ",err);
    _MESSAGE("===============================");

    return err;
}

momD_conf<C, 4> get_psp(unsigned N) {
    vector<momD<C, 4>> momenta(N);
    switch (N) {
        case 4: {
            momenta[0] = momD<C, 4>(-5.0000000000000000e-01, 4.2262766014356190e-01, 2.2897893889050211e-01, -1.3767536608325262e-01);
            momenta[1] = momD<C, 4>(-5.0000000000000000e-01, -4.2262766014356179e-01, -2.2897893889050222e-01, 1.3767536608325270e-01);
            momenta[2] = momD<C, 4>(4.9999999999999983e-01, -4.2745590050439514e-01, 2.2439732351099204e-01, -1.3010493582135926e-01);
            momenta[3] = momD<C, 4>(5.0000000000000000e-01, 4.2745590050439514e-01, -2.2439732351099204e-01, 1.3010493582135929e-01);
            break;
        }
        case 5: {
            momenta[0] = momD<C, 4>(-0.5, 0.4226276601435619, 0.2289789388905021, -0.1376753660832526);
            momenta[1] = momD<C, 4>(-0.5, -0.4226276601435618, -0.2289789388905022, 0.1376753660832527);
            momenta[2] = momD<C, 4>(0.4808104625620142, -0.4291920707157376, 0.2052304915957414, -0.06966572086188338);
            momenta[3] = momD<C, 4>(0.2981245312331837, 0.218397143158819, -0.162870102068166, 0.1210547555256588);
            momenta[4] = momD<C, 4>(0.2210650062048021, 0.2107949275569186, -0.04236038952757539, -0.05138903466377545);
            break;
        }
        case 6: {
            momenta[0] = momD<C, 4>(-5.0000000000000000e-01, 4.2262766014356190e-01, 2.2897893889050211e-01, -1.3767536608325262e-01);
            momenta[1] = momD<C, 4>(-5.0000000000000000e-01, -4.2262766014356179e-01, -2.2897893889050222e-01, 1.3767536608325270e-01);
            momenta[2] = momD<C, 4>(3.5494570039778628e-01, -3.5121370703709931e-01, 2.0719644851432066e-02, -4.6968910327623389e-02);
            momenta[3] = momD<C, 4>(2.5428551796459387e-01, 1.5992015160953349e-01, -1.7623431100473130e-01, 8.9599873774426991e-02);
            momenta[4] = momD<C, 4>(1.6732716397577782e-01, 1.5003243119739312e-01, -6.4019159148170357e-02, -3.7285341022143932e-02);
            momenta[5] = momD<C, 4>(2.2344161766184240e-01, 4.1261124230172821e-02, 2.1953382530146956e-01, -5.3456224246596672e-03);
            break;
        }
        case 7: {
            momenta[0] = momD<C, 4>(-5.0000000000000000e-01, 4.2262766014356190e-01, 2.2897893889050211e-01, -1.3767536608325262e-01);
            momenta[1] = momD<C, 4>(-5.0000000000000000e-01, -4.2262766014356179e-01, -2.2897893889050222e-01, 1.3767536608325270e-01);
            momenta[2] = momD<C, 4>(2.5343921718883233e-01, -2.2617479617832714e-01, 1.1394184340026846e-01, -9.6775361290455872e-03);
            momenta[3] = momD<C, 4>(1.6832147461775596e-01, 1.2342067712693180e-01, -8.5247929409413378e-02, 7.6369141715965275e-02);
            momenta[4] = momD<C, 4>(1.1933939373504301e-01, 1.1544913694916407e-01, -2.1827060760902198e-02, -2.0903757874618234e-02);
            momenta[5] = momD<C, 4>(2.2148864524138998e-01, 6.4916909859642172e-02, 2.1170560459826493e-01, 4.8735784414032602e-03);
            momenta[6] = momD<C, 4>(2.3741126921697897e-01, -7.7611927757410715e-02, -2.1857245782821774e-01, -5.0661426153704725e-02);
            break;
        }
        case 8: {
            momenta[0] = momD<C, 4>(-5.0000000000000000e-01, 4.2262766014356190e-01, 2.2897893889050211e-01, -1.3767536608325262e-01);
            momenta[1] = momD<C, 4>(-5.0000000000000000e-01, -4.2262766014356179e-01, -2.2897893889050222e-01, 1.3767536608325270e-01);
            momenta[2] = momD<C, 4>(1.8721070174533436e-01, -1.7968447767000298e-01, 5.2540388222720329e-02, -9.1811646569176192e-04);
            momenta[3] = momD<C, 4>(1.6374265196419152e-01, 1.2095933208792300e-01, -8.8319994025037651e-02, 6.6182132849638739e-02);
            momenta[4] = momD<C, 4>(1.1100922096141125e-01, 1.0613401788913372e-01, -2.8175059875196973e-02, -1.6272165995881830e-02);
            momenta[5] = momD<C, 4>(1.6438328672534289e-01, 5.6994661927495992e-02, 1.5396688876524725e-01, 8.2262160406054302e-03);
            momenta[6] = momD<C, 4>(2.2594737258327763e-01, -3.6642548778436868e-02, -2.1977445452151659e-01, -3.7533024606021509e-02);
            momenta[7] = momD<C, 4>(1.4770676602044264e-01, -6.7760985456112643e-02, 1.2976223143378363e-01, -1.9685041822649081e-02);
            break;
        }
        case 9: {
            momenta[0] = momD<C, 4>(-5.0000000000000000e-01, 4.2262766014356190e-01, 2.2897893889050211e-01, -1.3767536608325262e-01);
            momenta[1] = momD<C, 4>(-5.0000000000000000e-01, -4.2262766014356179e-01, -2.2897893889050222e-01, 1.3767536608325270e-01);
            momenta[2] = momD<C, 4>(1.8064510483788276e-01, -1.7409136870476041e-01, 4.7565571453059501e-02, 7.8971929656515838e-03);
            momenta[3] = momD<C, 4>(1.6038539463293983e-01, 1.1686755893571436e-01, -8.6345008211855687e-02, 6.7896892689325181e-02);
            momenta[4] = momD<C, 4>(1.0612275381902933e-01, 1.0150848201440725e-01, -2.7987253486656670e-02, -1.3220461396588442e-02);
            momenta[5] = momD<C, 4>(1.5648152320710038e-01, 5.3484531705008984e-02, 1.4651711029857462e-01, 1.2593981231743257e-02);
            momenta[6] = momD<C, 4>(2.1915106281440930e-01, -3.5913343032518856e-02, -2.1442841621782530e-01, -2.7529519487794168e-02);
            momenta[7] = momD<C, 4>(1.4042405020242904e-01, -6.6763825767160639e-02, 1.2283519593063925e-01, -1.3153709927925129e-02);
            momenta[8] = momD<C, 4>(3.6790110486209567e-02, 4.9079648493093389e-03, 1.1842800234064321e-02, -3.4484376074412265e-02);
            break;
        }
        default: _MESSAGE("Number of final state momenta N = ", N, " not implemented!"); exit(1);
    }
    momD_conf<C, 4> physical_point = momD_conf<C, 4>(momenta);
    return physical_point;
}
