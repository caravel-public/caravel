#include "misc/CaravelCereal.hpp"
#include "Core/RationalReconstruction.h"
#include "AmplitudeCoefficients/Hierarchy.h"
#include "Forest/Model.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"

#include "Core/typedefs.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "FunctionalReconstruction/DenseRational.h"
#include "IntegralLibrary/FunctionBasisExpansion.h"
#include "IntegralLibrary/IntegralProvider.h"
#include "Core/settings.h"

#include "Core/MathInterface.h"

#include "Core/Timing.h"

#include "misc/TestUtilities.hpp"

#include "Core/CaravelInput.h"

#include "Core/Series.h"
#include "Core/precision_cast.h"


int main(int argc, char* argv[]) {
    
    using namespace Caravel;
    using namespace Caravel::Integrals;
    using namespace Caravel::IntegrandHierarchy;
    using F = F32;

    if (argc < 2) {
        _WARNING("ERROR: expecting some input!");
        std::exit(1);
    }

    verbosity_settings.show_all();

    settings::use_setting("general::do_n_eq_n_test yes");

    InputParser input{argc,argv};

#ifdef WITH_PENTAGONS
    if (input.cmd_option_exists("Tolerance")) {
        using settings::integrals::pentagon_functions_integration_precision;
        auto new_accuracy = exp(-input.get_cmd_option<double>("Tolerance"));
        if (pentagon_functions_integration_precision < new_accuracy){
            _MESSAGE("Pentagon functions integration accuracy is set to ", new_accuracy);
            pentagon_functions_integration_precision.value = new_accuracy;
        }
    };
#endif

    auto  pa = input.get_cmd_option<PartialAmplitudeInput>("PartialAmplitudeInput");

    size_t nparticles = pa.get_multiplicity();

    set_settings_for_standard_choice_of_integrals(nparticles);

    // OVERRIDE SETTINGS FROM FILE
    settings::read_settings_from_file();

    auto xs = Caravel::detail::read_twistor_parameters(input.get_cmd_option("xs"));

    SM_color_ordered model2use;

    auto physical_point = rational_mom_conf<BigRat>(nparticles, xs);

    START_TIMER(construction);
    ColourExpandedAmplitude<F, 2> ampl(model2use,pa);
    STOP_TIMER(construction);

    START_TIMER(coefficients);
    auto result_coeffs = [&]() {
        if (input.cmd_option_exists("ReadCoefficientsFromFile")) {
            return read_serialized<std::vector<DenseRational<BigRat>>>("test_data/"+pa.get_id_string()+std::string("_saved_master_coeffs.dat"));
        }
        else {
            // Construct a function that returns the evaluation of the amplitude (or whatever other structure) in a given point in a given finite field
            std::function<std::vector<DenseRational<F>>(const momentumD_configuration<F, 4>&)> f_generic =
                [&ampl](const momentumD_configuration<F, 4>& mom_conf) { return ampl.get_master_coefficients(mom_conf).front(); };

            return RationalReconstruction::rational_reconstruct(physical_point, f_generic);
        }
    } ();
    STOP_TIMER(coefficients);

    sIntegralProvider<C> integrals( ampl.get_integral_graphs() );

    auto momenta = to_precision<C>(physical_point);

    // evaluate integrals
    auto result_integrals = integrals.get_abstract_integral(momenta);
START_TIMER(combine_with_integrals);
    auto amp = combine_with_integrals<C>(momenta, result_integrals, result_coeffs);
STOP_TIMER(combine_with_integrals);

    // drop higher orders in eps
    amp.truncate(0);

    std::cout << std::setprecision(12);

    //_MESSAGE("\nampl[\"",pa.get_id_string(),"\"] = ",amp,"\n");

    size_t err = 0;

    {
        std::string filename = "test_data/" +pa.get_id_string() + std::string("_ampl.dat");
#ifdef GENERATE_TARGETS_MODE
        write_serialized(result_coeffs,"test_data/"+pa.get_id_string()+std::string("_saved_master_coeffs.dat"));
        write_serialized(amp, filename);
#endif
        auto target = read_serialized<Series<C>>(filename);

        _PRINT(amp);
        _PRINT(target);

        if(amp.leading() != target.leading()){
            _WARNING("Leading order of the series is not equal!");
            _WARNING(amp.leading()," != ",target.leading()," (target)");
            ++err;
        }
        if(amp.last() != target.last()){
            _WARNING("Last order of the series is not equal!");
            _WARNING(amp.leading()," != ",target.leading()," (target)");
            ++err;
        }

        double tolerance = 10.;

        if (input.cmd_option_exists("Tolerance")){
            tolerance = std::stod(input.get_cmd_option("Tolerance")[0]);
        }

        for(int i=target.leading(); i <=target.last() ; i++){
            if(check(amp[i],target[i],tolerance)){
                _MESSAGE("eps^",i,":\t\t",Color::Code::FG_GREEN,"PASS",Color::Code::FG_DEFAULT);
            }
            else{
                err++;
                _MESSAGE("eps^",i,":\t\t", Color::Code::FG_RED, "FAIL", Color::Code::FG_DEFAULT);
                _MESSAGE("\t\t\t>",amp[i]," != target ", target[i],"; acc = ",compute_accuracy(amp[i],target[i]));
            }
        }
    }

    return err;
}

