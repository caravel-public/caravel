#include "Core/Debug.h"

#include "Core/Particle.h"
#include "Core/momD_conf.h"

#include "Core/Utilities.h"
#include "Core/settings.h"
#include "Core/typedefs.h"
#include "misc/TestUtilities.hpp"

#include "AmpEng/CoefficientEngine.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "Amplitude.h"

#include <algorithm>
#include <cmath>
#include <exception>
#include <iostream>
#include <string>
#include <vector>


using namespace Caravel;
using namespace AmpEng;
using namespace lGraph;
using F = F32;

int main(int argc, char* argv[]) {

    // more cardinalities in misc/Moduli.hpp
    int64_t cardinality(2147483629);
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    // one-loop hierarchy
    std::vector<xGraph> maximals_1L;

    // just two boxes
    maximals_1L.push_back(xGraph(
        MathList("CaravelGraph[Nodes[Node[Leg[1,1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[4,1], Link[0], Leg[2,2], Link[2], Leg[3,2], Link[0]]]]")));
    maximals_1L.push_back(xGraph(
        MathList("CaravelGraph[Nodes[Node[Leg[4,1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[1,1], Link[0], Leg[2,2], Link[2], Leg[3,2], Link[0]]]]")));

    Pincher hier_1L(maximals_1L);

    std::cout << "================================" << std::endl;
    std::cout << "Filter scaleless graphs" << std::endl;
    hier_1L.filter(scaleless_graphs_filter);

    std::cout << "================================" << std::endl;
    std::cout << "Filter four-matter contact graphs" << std::endl;
    hier_1L.filter(four_massive_particle_filter);

    std::cout << "================================" << std::endl;
    std::cout << "At least one matter propagators per loop" << std::endl;
    hier_1L.filter(subloop_one_matter_line_filter);

    GraphCutMap map;
    // OJO: necessarily the index should match the momentum of the particle!
    map.external = { {1, std::make_shared<Particle>("s1", ParticleType("Phi1"), SingleState("RealSC"), 1, 0)},
                     {4, std::make_shared<Particle>("s2", ParticleType("Phi1"), SingleState("RealSC"), 4, 0)},
                     {2, std::make_shared<Particle>("c1", ParticleType("Phi2"), SingleState("RealSC"), 2, 0)},
                     {3, std::make_shared<Particle>("c2", ParticleType("Phi2"), SingleState("RealSC"), 3, 0)},
                    };
    map.internal = { {0, ParticleType("G")},
                     {1, ParticleType("Phi1")},
                     {2, ParticleType("Phi2")},
                    };
    map.coup = "K";

    size_t nparticles(4);
    Particle ss("ss", ParticleType("Phi1"), SingleState("RealSC"), 1, 0);
    Particle cc("cc", ParticleType("Phi2"), SingleState("RealSC"), 1, 0);
    size_t mindex_ss = ss.get_mass_index();
    size_t mindex_cc = cc.get_mass_index();
    std::vector<size_t> masses = {mindex_ss, mindex_cc, mindex_cc, mindex_ss};
    settings::use_setting("IntegrandHierarchy::amplitude_normalization none"); // do not normalize on massive tree! It has an explicit Ds dependence!
    settings::use_setting("general::do_n_eq_n_test yes");
    //settings::use_setting("general::one_loop_master_basis only_scalar");

    Genealogy hierarchy(hier_1L, map, true, "EHGravity");

    EHGravity model2use;

    CoefficientEngine<F32> coefficient_provider(hierarchy, std::make_shared<Model>(model2use), {5, 6, 7}, {{"K", 2}}, true);
    coefficient_provider.set_Ds_function_basis(powers_of_Dsm2<F>(2, 0));

    // compute warmup if necessary
    std::vector<F> xs = {F(std::rand()), F(std::rand())};
    momD_conf<F, 4> psp_warmup = rational_mom_conf<F>(nparticles, xs, masses);
    if (!coefficient_provider.has_warmup()) {
        coefficient_provider.warmup_run(psp_warmup);
        // reload warmup
        coefficient_provider.reload_warmup();
    }

    // RECONSTRUCTION
    /*
     * Number of variables: 4 - massive one-loop
     *                      x0 = x, x1 = s23, x2 = m1, x3 = m2
     *                      with 4*m3^2*m4^2/(s12 - m3^2 - m4^2)^2 = 4*x^2/(1+x^2)^2
     */
    constexpr size_t _N = 4;

    std::array<F, _N> xxs = {F(BigRat(4, 3)), F(BigRat(8, 13)), F(BigRat(25, 1)), F(BigRat(11, 1))};

    // return the master coefficients
    auto f = [&](std::array<F, _N> xs) {
        std::vector<F> point;
        momD_conf<F, 4> psp;

        BigRat Mass1 = RationalReconstruction::rational_guess(std::pair<BigInt, BigInt>{BigInt(xs[2]), BigInt(cardinality)});
        BigRat Mass2 = RationalReconstruction::rational_guess(std::pair<BigInt, BigInt>{BigInt(xs[3]), BigInt(cardinality)});
        ParticleMass::set_container_mass(mindex_ss, int(Mass1.num()), int(Mass1.den()));
        ParticleMass::set_container_mass(mindex_cc, int(Mass2.num()), int(Mass2.den()));
        point = {xs[0], xs[1]};
        psp = rational_mom_conf<F>(nparticles, point, masses);

        auto result = coefficient_provider.compute_full_Ds(psp);

        return result;
    };

    std::vector<F> target = {
        F(1311937582), F(0),          F(0), F(0),          F(0),         F(1311937582), F(0), F(0), F(0),          F(0),          F(1311937582), F(0),
        F(0),          F(0),          F(0), F(1311937582), F(0),         F(0),          F(0), F(0), F(146410000),  F(0),          F(0),          F(0),
        F(756250000),  F(0),          F(0), F(0),          F(146410000), F(0),          F(0), F(0), F(756250000),  F(0),          F(0),          F(0),
        F(1153325194), F(0),          F(0), F(0),          F(0),         F(1969838318), F(0), F(0), F(0),          F(0),          F(1153325194), F(0),
        F(0),          F(0),          F(0), F(1969838318), F(0),         F(0),          F(0), F(0), F(759548364),  F(1196541487), F(0),          F(0),
        F(2145432064), F(615646869),  F(0), F(0),          F(759548364), F(1196541487), F(0), F(0), F(2145432064), F(615646869),  F(0),          F(0),
        F(1074931652), F(0),          F(0), F(0),          F(0),         F(1328572047), F(0), F(0), F(0),          F(0),          F(1074931652), F(0),
        F(0),          F(0),          F(0), F(1328572047), F(0),         F(0),          F(0), F(0), F(846146602),  F(1173438103), F(0),          F(0),
        F(2051159959), F(1754332721), F(0), F(0),          F(846146602), F(1173438103), F(0), F(0), F(2051159959), F(1754332721), F(0),          F(0)};

    size_t errors = 0;
    auto cfs = f(xxs);
    _PRINT(cfs);
#if 0
    for (size_t ii = 0; ii < cfs.size(); ii++)
        if (target[ii] != cfs[ii]) {
            errors++;
            _MESSAGE("target[", ii, "] = ", target[ii], " coeff[", ii, "] = ", cfs[ii]);
        }

    if (errors == 0) _MESSAGE(Color::Code::FG_GREEN, "Test passed!");
#endif
    return errors;
}
