// #include <map>
// #include <sstream>
// #include <random>
#include <vector>

#include <functional>
#include <iostream>
#include <iterator>

#include "Core/settings.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/ResidueEvaluator.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "IntegralLibrary/MapToBasis.h"
#include "AmpEng/CoefficientEngine.h"
#include "Amplitude.h"
#include "PoleStructure/Remainder.h"
#include "misc/TestUtilities.hpp"
#include "IntegralLibrary/IntegralProvider.h"

// using namespace std;
using namespace Caravel;
using namespace Caravel::Integrals;

int main(int argc, char* argv[]) {

    verbosity_settings.show_all();

    // Two-loop process interface
    std::vector<Particle> process = {
        Particle("g", ParticleType("gluon"), SingleState("m"), 1),
        Particle("g", ParticleType("gluon"), SingleState("p"), 2),
        Particle("g", ParticleType("gluon"), SingleState("p"), 3),
        Particle("g", ParticleType("gluon"), SingleState("p"), 4),
        Particle("g", ParticleType("gluon"), SingleState("p"), 5),
    };

    settings::use_setting("general::master_basis standard");
    settings::use_setting("IntegrandHierarchy::amplitude_normalization spinor_weight");
    settings::integrals::integral_family_1L.value = settings::integrals::family_types_1l::pentagon_functions;
    settings::integrals::integral_family_2L.value = settings::integrals::family_types_2l::pentagon_functions;

    using F = F32;
    size_t cardinality(2147483629);
    cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

    

    // A complicated rational point should be used for the warmup.
    // auto physical_point1 = rational_mom_conf<BigRat>(5, {BigRat(-1545), BigRat(44564), BigRat(1456456), BigRat(86546), BigRat(745645)});
    auto physical_point1 = rational_mom_conf<BigRat>(5, {BigRat(4), BigRat(1), BigRat(8), BigRat(7), BigRat(-1)});
    momentumD_configuration<F, 4> tmp1 = static_cast<momentumD_configuration<F, 4>>(physical_point1);

    //ps_parameterizations::print_mom_conf_invariants(physical_point1);

    YM model;

    PartialAmplitudeInput pa{process};

    AmpEng::CoefficientEngine<F> one_loop_coeff(std::make_shared<Model>(model), pa);
    Amplitude<F, decltype(one_loop_coeff), Integrals::sIntegralProvider<F>> one_loop_ampl(std::move(one_loop_coeff));
    // we set to 4 the maximum, such that when combined with the integrals it ranges from -2 to 2
    one_loop_ampl.set_laurent_range(-2,4);

    IntegrandHierarchy::ColourExpandedAmplitude<F,2> two_loop_ampl(model,pa);

    auto integral_graphs_2L = two_loop_ampl.get_integral_graphs();

    // Get the master integrals
    // heavy construction of integrals (someone should in general hold this)
    // we use the simplified class template 'sIntegralProvider' (if needed, access directly IntegralProvider<CType, SFType>)
    IntegralProvider<FBCoefficient<F, F>, StdBasisFunction<C, C>> integrals_2L( integral_graphs_2L );


    auto remainder = [&](const momD_conf<F, 4>& conf) {

        auto mi_coeffs_2L = two_loop_ampl.compute(tmp1);
        // Transform coefficients to series
        auto mi_coeffs_series_2L = to_Series(mi_coeffs_2L);

        // evaluate integrals
        auto result_integrals_2L = integrals_2L.get_abstract_integral(tmp1);

        auto amp_2L = mi_coeffs_series_2L[0] * result_integrals_2L[0];
        assert(result_integrals_2L.size() == mi_coeffs_series_2L.size());
        for (size_t i = 1; i < result_integrals_2L.size(); ++i) {
            amp_2L += mi_coeffs_series_2L[i] * result_integrals_2L[i];
        }

        auto one_loop = one_loop_ampl.integrate(conf);
        auto pole_operator = PoleStructure::finiteOneLoopPoleOperator<F,C>(2);
        auto subtraction = pole_operator * one_loop;

        auto remainder = subtraction - amp_2L;

        return remainder;
    };

    MapToBasis<F, StdBasisFunction<C, C>> mapper(pa);

    auto res = mapper(remainder(tmp1));

    // // Now compare to targets

    std::cout << std::endl;

    size_t err_count = 0;

    // Make sure there are only 27 basis functions in the final result
    std::size_t contributing_basis_function_counter = 0;
    for (const auto& term: res) {
        // Don't check for non-vanishing coefficients above ep^0
        for (int p = term.second.leading(); p < 1 && p <= term.second.last(); ++p) {
            if (term.second[p] != F(0)){
                ++contributing_basis_function_counter;
                _MESSAGE(" --- ", term.first, ": ", term.second);
                break;
            }
        }
    } 
    if ( !(contributing_basis_function_counter == 27)) {
        _MESSAGE("Are there 27 basis functions: ", contributing_basis_function_counter, " --- ", Color::Code::FG_RED, "FAIL", Color::Code::FG_DEFAULT);
        ++err_count;
    } else {
        _MESSAGE("Are there 27 basis functions: ", contributing_basis_function_counter, " --- ", Color::Code::FG_GREEN, "PASS", Color::Code::FG_DEFAULT);
    }

    auto check = [&res,&err_count](std::string func, F target) {
        auto x = res.get_coeff(func)[0];
        if (x == target) {
            _MESSAGE("Coefficient of ", func, ":\t\t", Color::Code::FG_GREEN, "PASS", Color::Code::FG_DEFAULT);
        } else {
            _MESSAGE("Coefficient of ", func, ":\t\t", Color::Code::FG_RED, "FAIL", Color::Code::FG_DEFAULT);
            _MESSAGE("\t\t\t>", x, " != target ", target);
            ++err_count;
        }
    };

    // There are other pieces, but they can be discarded to get the full thing.
    const std::vector<std::pair<std::string,F>> targets = {
        {"T[1]",F(int64_t(-8116406272))/F(535815)},
        {"Pi*Pi",F(-25885336)/F(18225)},
        {"pentagon_f_1_1[s12]",F(28892992)/F(19683)},
        {"pentagon_f_1_1[s12]*pentagon_f_1_1[s12]",F(3160)/F(3)},
        {"pentagon_f_1_1[s12]*pentagon_f_1_1[s15]",F(182624)/F(243)},
        {"pentagon_f_1_1[s12]*pentagon_f_1_1[s23]",F(128)/F(3)},
        {"pentagon_f_1_1[s12]*pentagon_f_1_1[s34]",F(-182624)/F(243)},
        {"pentagon_f_1_1[s12]*pentagon_f_1_1[s45]",F(-6448)/F(3)},
        {"pentagon_f_1_1[s15]",F(1169248768)/F(694575)},
        {"pentagon_f_1_1[s15]*pentagon_f_1_1[s15]",F(2533136)/F(6075)},
        {"pentagon_f_1_1[s15]*pentagon_f_1_1[s23]",F(-1696)/F(9)},
        {"pentagon_f_1_1[s15]*pentagon_f_1_1[s34]",F(-118912)/F(75)},
        {"pentagon_f_1_1[s15]*pentagon_f_1_1[s45]",F(1696)/F(9)},
        {"pentagon_f_1_1[s23]",F(-889984)/F(225)},
        {"pentagon_f_1_1[s23]*pentagon_f_1_1[s23]",F(-157168)/F(225)},
        {"pentagon_f_1_1[s23]*pentagon_f_1_1[s34]",F(118912)/F(75)},
        {"pentagon_f_1_1[s23]*pentagon_f_1_1[s45]",F(-128)/F(3)},
        {"pentagon_f_1_1[s34]",F(int64_t(109516764544))/F(33756345)},
        {"pentagon_f_1_1[s34]*pentagon_f_1_1[s34]",F(-169832)/F(243)},
        {"pentagon_f_1_1[s34]*pentagon_f_1_1[s45]",F(6448)/F(3)},
        {"pentagon_f_1_1[s45]",F(-592960)/F(243)},
        {"pentagon_f_1_1[s45]*pentagon_f_1_1[s45]",F(-656)/F(9)},
        {"pentagon_f_2_1[s12,s34]",F(339664)/F(243)},
        {"pentagon_f_2_1[s15,s23]",F(314336)/F(225)},
        {"pentagon_f_2_1[s23,s45]",F(1312)/F(9)},
        {"pentagon_f_2_1[s34,s15]",F(-5066272)/F(6075)},
        {"pentagon_f_2_1[s45,s12]",F(-6320)/F(3)},
    };

    for(auto& it : targets) check(it.first,it.second);

    std::cout << std::endl;

    if(err_count == 0){
        _MESSAGE(__FILE__," passed.");
    }
    else{
        _WARNING_RED(__FILE__," failed!");
    }

    return err_count;
    
}
