/**
 * EH_tree_gravitons.cpp
 *
 * First created on 17.07.2022
 *
 * Checks 3-pt, 4-pt and 5-pt Graviton amplitudes via KLT relations
 *
*/

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <exception>

#include "Core/typedefs.h"
#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "Forest/Builder.h"
#include "Forest/Model.h"
#include "Core/Debug.h"
#include "Forest/Forest.h"
#include "PhaseSpace/PhaseSpace.h"
#include "misc/TestUtilities.hpp"

using namespace std;
using namespace Caravel;

using T = C;
using TR = R;

// Generate 3-particle on-shell phase space point with complex momenta
template <typename T, size_t Ds> momD_conf<T, Ds> OS_3point(const T& mass, bool random_boost = false) {
    T msq = mass * mass;

    // First construct momenta in (+,-.-,-) signature
    std::vector<std::vector<T>> moms(3);
    if (std::abs(mass) > std::abs(T(0))) {
        moms[0] = {(T(-1) - msq) / T(2), (T(2) - T(3) * msq) / T(4), T(0, 1) * (T(-2) + T(3) * msq) / T(4), (T(1) - msq) / T(2)};
        moms[2] = {(T(-2) + msq + T(10) * msq * msq) / (T(8) * msq), -(T(1) + T(3) * msq / T(8)), T(0, 1) * T(3) * (T(4) + msq) / T(8),
                   (T(-1) + T(2) / msq + T(10) * msq) / T(8)};
        moms[1] = {(T(3) + T(2) / msq - T(6) * msq) / T(8), (T(4) + T(9) * msq) / T(8), -T(0, 1) * (T(1) + T(9) * msq / T(8)),
                   -(T(3) + T(2) / msq + T(6) * msq) / T(8)};
    }
    else {
        T energy = T(50);
        moms[0] = {-energy, T(0), T(0), -energy};
        moms[1] = {energy / T(2), T(1), T(0, 1), energy / T(2)};
        moms[2] = {energy / T(2), T(-1), T(0, -1), energy / T(2)};
    }

    std::vector<momD<T, Ds>> qmom;
    if (random_boost) {
        const T r1 = T(rand()) / T(RAND_MAX), r2 = T(M_PI) * T(rand()) / T(RAND_MAX), r3 = T(2) * T(M_PI) * T(rand()) / T(RAND_MAX);
        const T bx = r1 * cos(r3) * sin(r2), by = r1 * sin(r3) * sin(r2), bz = r1 * cos(r2);
        const T bsq = bx * bx + by * by + bz * bz, gamma = T(1) / sqrt(T(1) - bsq);
        const std::vector<std::vector<T>> L = {
            {gamma, -gamma * bx, -gamma * by, -gamma * bz},
            {-gamma * bx, T(1) + (gamma - T(1)) * bx * bx / bsq, (gamma - T(1)) * bx * by / bsq, (gamma - T(1)) * bx * bz / bsq},
            {-gamma * by, (gamma - T(1)) * by * bx / bsq, T(1) + (gamma - T(1)) * by * by / bsq, (gamma - T(1)) * by * bz / bsq},
            {-gamma * bz, (gamma - T(1)) * bz * bx / bsq, (gamma - T(1)) * bz * by / bsq, T(1) + (gamma - T(1)) * bz * bz / bsq},
        };
        for (size_t n = 0; n < 3; n++) {
            std::vector<T> q = {T(0), T(0), T(0), T(0)};
            for (size_t i = 0; i < 4; i++) {
                for (size_t j = 0; j < 4; j++) q[i] += L[i][j] * moms[n][j];
            }
            qmom.push_back(momD<T, Ds>(q));
        }
    }
    else {
        for (size_t n = 0; n < 3; n++) qmom.push_back(momD<T, Ds>(moms[n]));
    }
    momD_conf<T, 4> pspoint = momD_conf<T, 4>(qmom);
    return pspoint;
}

int main(int argc, char* argv[]) {
    std::cout << setprecision(16);

    // Create the external particles (all massless)
    Particle G1mm("G1mm", ParticleType("G"), SingleState("hmm"), 1, 0);
    Particle G2mm("G2mm", ParticleType("G"), SingleState("hmm"), 2, 0);
    Particle G3pp("G3pp", ParticleType("G"), SingleState("hpp"), 3, 0);
    Particle G4pp("G4pp", ParticleType("G"), SingleState("hpp"), 4, 0);
    Particle G5pp("G5pp", ParticleType("G"), SingleState("hpp"), 5, 0);
  
    Particle G1pp("G1pp", ParticleType("G"), SingleState("hpp"), 1, 0);
    Particle G2pp("G2pp", ParticleType("G"), SingleState("hpp"), 2, 0);
    Particle G3mm("G3mm", ParticleType("G"), SingleState("hmm"), 3, 0);

    std::vector<Particle*> grav3 = {&G1pp, &G2pp, &G3mm};
    std::vector<Particle*> grav4 = {&G1mm, &G2mm, &G3pp, &G4pp};
    std::vector<Particle*> grav5 = {&G1mm, &G2mm, &G3pp, &G4pp, &G5pp};

    // And the gluons
    Particle g1m("g1m", ParticleType("gluon"), SingleState("m"), 1, 0);
    Particle g2m("g2m", ParticleType("gluon"), SingleState("m"), 2, 0);
    Particle g3p("g3p", ParticleType("gluon"), SingleState("p"), 3, 0);
    Particle g4p("g4p", ParticleType("gluon"), SingleState("p"), 4, 0);
    Particle g5p("g5p", ParticleType("gluon"), SingleState("p"), 5, 0);

    Particle g1p("g1p", ParticleType("gluon"), SingleState("p"), 1, 0);
    Particle g2p("g2p", ParticleType("gluon"), SingleState("p"), 2, 0);
    Particle g3m("g3m", ParticleType("gluon"), SingleState("m"), 3, 0);


    std::vector<Particle*> glu3 = {&g1p, &g2p, &g3m};
    std::vector<std::vector<Particle*>> glu4 = {{&g1m, &g2m, &g3p, &g4p}, {&g1m, &g2m, &g4p, &g3p}};
    std::vector<std::vector<Particle*>> glu5 = {
        {&g1m, &g2m, &g3p, &g4p, &g5p},
        {&g2m, &g1m, &g4p, &g3p, &g5p},
        {&g1m, &g3p, &g2m, &g4p, &g5p},
        {&g3p, &g1m, &g4p, &g2m, &g5p},
    };

    T ratio3, ratio4, ratio5;

    // check M(-,-,+)
    {
        momD_conf<T, 4> pspoint = OS_3point<T, 4>(T(0), true);

        // Gravity
        EHGravity GravityModel;
        Forest SW(&GravityModel);
        unsigned track = SW.add_process(Process(grav3), {{"K", 1}});
        std::vector<size_t> dims = {4};
        SW.define_internal_states_and_close_forest(dims);
        Builder<T, 4> bob(&SW, 3);
        bob.set_p(pspoint);
        bob.compute();
        bob.contract();
        T gravTree = bob.get_tree(track);

        // Yang-Mills
        YM QCDModel;
        Forest SW2(&QCDModel);
        unsigned track1 = SW2.add_process(Process(glu3), {{"gs", 1}});
        SW2.define_internal_states_and_close_forest(dims);
        Builder<T, 4> bob2(&SW2, 3);
        bob2.set_p(pspoint);
        bob2.compute();
        bob2.contract();
        T tree1 = bob2.get_tree(track1);

        // arXiv:1908.01493 Eq. (3.2)
        T KLT = T(0, 1) * tree1 * tree1;
        ratio3 = gravTree / KLT;
        _MESSAGE(Color::Code::FG_BLUE, "M3: Tree = ", gravTree, " KLT = ", KLT, " ratio = ", ratio3);
    }

    // Check M(-,-,+,+)
    {
	// Phase Space point
	PhaseSpace<TR> tph(4,1.);
	tph.set_reset_seed(false);
	srand(1110);
	tph.generate();
	auto moms= tph.mom_double();
	momD_conf<T,4> pspoint = to_momDconf<R,4>(moms);

        // Gravity
        EHGravity GravityModel;
        Forest SW(&GravityModel);
        unsigned track = SW.add_process(Process(grav4), {{"K", 2}});
        std::vector<size_t> dims = {4};
        SW.define_internal_states_and_close_forest(dims);
        Builder<T, 4> bob(&SW, 4);
        bob.set_p(pspoint);
        bob.compute();
        bob.contract();
        T gravTree = bob.get_tree(track);

        // Yang-Mills
        YM QCDModel;
        Forest SW2(&QCDModel);
        unsigned track1 = SW2.add_process(Process(glu4[0]), {{"gs", 2}});
        unsigned track2 = SW2.add_process(Process(glu4[1]), {{"gs", 2}});
        SW2.define_internal_states_and_close_forest(dims);
        Builder<T, 4> bob2(&SW2, 4);
        bob2.set_p(pspoint);
        bob2.compute();
        bob2.contract();
        T tree1 = bob2.get_tree(track1);
        T tree2 = bob2.get_tree(track2);

        T s12 = T(2) * pspoint[1] * pspoint[2];
        // arXiv:1908.01493 Eq. (3.2)
        T KLT = T(0, -1) * s12 * tree1 * tree2;

        ratio4 = gravTree / KLT;
        _MESSAGE(Color::Code::FG_BLUE, "M4: Tree = ", gravTree, " KLT = ", KLT, " ratio = ", ratio4);
    }

    // Check M(-,-,+,+,+)
    {
	// Phase Space point
	PhaseSpace<TR> tph(5,1.);
	tph.set_reset_seed(false);
	srand(1110);
	tph.generate();
	auto moms= tph.mom_double();
	momD_conf<T,4> pspoint = to_momDconf<R,4>(moms);

        // Gravity
        EHGravity GravityModel;
        Forest SW(&GravityModel);
        unsigned track = SW.add_process(Process(grav5), {{"K", 3}});
        std::vector<size_t> dims = {4};
        SW.define_internal_states_and_close_forest(dims);
        Builder<T, 4> bob(&SW, 5);
        bob.set_p(pspoint);
        bob.compute();
        bob.contract();
        T gravTree = bob.get_tree(track);

        // Yang-Mills
        YM QCDModel;
        Forest SW2(&QCDModel);
        unsigned track1 = SW2.add_process(Process(glu5[0]), {{"gs", 3}});
        unsigned track2 = SW2.add_process(Process(glu5[1]), {{"gs", 3}});
        unsigned track3 = SW2.add_process(Process(glu5[2]), {{"gs", 3}});
        unsigned track4 = SW2.add_process(Process(glu5[3]), {{"gs", 3}});
        SW2.define_internal_states_and_close_forest(dims);
        Builder<T, 4> bob2(&SW2, 5);
        bob2.set_p(pspoint);
        bob2.compute();
        bob2.contract();
        T tree1 = bob2.get_tree(track1);
        T tree2 = bob2.get_tree(track2);
        T tree3 = bob2.get_tree(track3);
        T tree4 = bob2.get_tree(track4);

        T s12 = T(2) * pspoint[1] * pspoint[2];
        T s34 = T(2) * pspoint[3] * pspoint[4];
        T s13 = T(2) * pspoint[1] * pspoint[3];
        T s24 = T(2) * pspoint[2] * pspoint[4];
        // arXiv:1908.01493 Eq. (3.2)
        T KLT = T(0, 1) * s12 * s34 * tree1 * tree2 + T(0, 1) * s13 * s24 * tree3 * tree4;

        ratio5 = gravTree / KLT;
        _MESSAGE(Color::Code::FG_BLUE, "M5: Tree = ", gravTree, " KLT = ", KLT, " ratio = ", ratio5);
    }

    int toret = 0;
    if (abs(ratio3 - T(1)) > TR(0.000000000001)) {
        std::cout << "Incorrect tree" << std::endl;
        std::cout << "M(+,+,-) / KLT (NORMALIZED) = " << ratio3 << std::endl;
	toret++;
    }
    if (abs(ratio4 - T(1)) > TR(0.000000000001)) {
        std::cout << "Incorrect tree" << std::endl;
        std::cout << "M(-,-,+,+) / KLT (NORMALIZED) = " << ratio4 << std::endl;
	toret++;
    }
    if (abs(ratio5 - T(1)) > TR(0.00000000001)) {
        std::cout << "Incorrect tree" << std::endl;
        std::cout << "M(-,-,+,+,+) / KLT (NORMALIZED) = " << ratio5 << std::endl;
	toret++;
    }
    if (toret > 0) { std::cout << "ERROR: the EH tree-graviton check failed!" << std::endl; }

    return toret;
}
