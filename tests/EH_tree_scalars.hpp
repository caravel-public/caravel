/*
 * EH_tree_scalars.hpp
 *
 * This file implements the 3 to 5 point Gravity amplitudes for
 * Graviton scattering in association with massive scalars
 * via KLT relations
 */

namespace Caravel {
namespace internal {

/*
 * 3-pt Yang-Mills helicity Amplitudes
 */
// Implementation of A3(1s,2g^+,3s)
template <typename T, size_t Ds> T A3_sps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3) {
    momD<T, Ds> n = momD<T, Ds>(T(1), T(1), T(0), T(0));
    return T(0, 1) * spab(n, p1, p2) / spaa(n, p2);
}

// Implementation of A3(1s,2g^-,3s)
template <typename T, size_t Ds> T A3_sms(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3) {
    momD<T, Ds> n = momD<T, Ds>(T(1), T(1), T(0), T(0));
    return T(0, -1) * spab(p2, p1, n) / spbb(n, p2);
}

// Implementation of A3(1g^+,2g^+,3g^-)
template <typename T, size_t Ds> T A3_ppm(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3) {
    return T(0, 1) * spbb(p1, p2) * spbb(p1, p2) * spbb(p1, p2) / (spbb(p2, p3) * spbb(p3, p1));
}
// Implementation of A3(1g^-,2g^-,3g^+)
template <typename T, size_t Ds> T A3_mmp(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3) {
    return T(0, 1) * spaa(p1, p2) * spaa(p1, p2) * spaa(p1, p2) / (spaa(p2, p3) * spaa(p3, p1));
}
/*
 * 4-pt Yang-Mills helicity Amplitudes
 */
// Implementation of A4(1s,2g^+,3g^+,4s)
template <typename T, size_t Ds> T A4_spps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    T msq = p4 * p4;
    T s12 = (p1 + p2) * (p1 + p2);
    T s23 = (p2 + p3) * (p2 + p3);
    return T(0, 1) * msq * spbb(p2, p3) * spbb(p2, p3) / (s23 * (s12 - msq));
}
// Implementation of A4(1s,2g^+,4s,3g^+)
template <typename T, size_t Ds> T A4_spsp(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    T msq = p4 * p4;
    T s12 = (p1 + p2) * (p1 + p2);
    T s13 = (p1 + p3) * (p1 + p3);
    T s23 = (p2 + p3) * (p2 + p3);
    return T(0, -1) * msq * spbb(p2, p3) * spbb(p2, p3) / s23 * (T(1) / (s12 - msq) + T(1) / (s13 - msq));
}
// Implementation of A4(1s,2g^+,3g^-,4s)
template <typename T, size_t Ds> T A4_spms(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    T msq = p4 * p4;
    T s12 = (p1 + p2) * (p1 + p2);
    T s23 = (p2 + p3) * (p2 + p3);
    return T(0, -1) * spab(p3, p1, p2) * spab(p3, p1, p2) / (s23 * (s12 - msq));
}

/*
 * 5-pt Yang-Mills helicity Amplitudes
 */
// Implementation of A5(1s,2g^+,3g^+,4g^+,5s)
template <typename T, size_t Ds> T A5_sppps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4, const momD<T, Ds>& p5) {
    T msq = p5 * p5;
    T P1 = (p1 + p2) * (p1 + p2) - msq;
    T P2 = (p4 + p5) * (p4 + p5) - msq;
    return T(0, 1) * msq * spbb(p4, p2 + p3, p1, p2) / (spaa(p2, p3) * spaa(p3, p4) * P1 * P2);
}
// Implementation of A5(1s,2g^+,3g^+,4g^-,5s)
template <typename T, size_t Ds> T A5_sppms(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4, const momD<T, Ds>& p5) {
    T msq = p5 * p5;
    T P1 = (p1 + p2) * (p1 + p2) - msq;
    T P2 = (p4 + p5) * (p4 + p5) - msq;
    T s234 = (p2 + p3 + p4) * (p2 + p3 + p4);
    T amp = -spab(p4, p5, p2 + p3, p1, p2) * spab(p4, p5, p2 + p3, p1, p2) / (P1 * P2 * spaa(p2, p3) * spaa(p3, p4) * spbb(p4, p2 + p3, p1, p2)) +
            msq * spbb(p2, p3) * spbb(p2, p3) * spbb(p2, p3) / (s234 * spbb(p3, p4) * spbb(p4, p2 + p3, p1, p2));
    return T(0, 1) * amp;
}
// Implementation of A5(1s,2g^+,3g^-,4g^+,5s)
template <typename T, size_t Ds> T A5_spmps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4, const momD<T, Ds>& p5) {
    T msq = p5 * p5;
    T P1 = (p1 + p2) * (p1 + p2) - msq;
    T P2 = (p4 + p5) * (p4 + p5) - msq;
    T s234 = (p2 + p3 + p4) * (p2 + p3 + p4);
    T amp = -spab(p3, p1, p2) * spab(p3, p1, p2) * spab(p3, p5, p4) * spab(p3, p5, p4) / (P1 * P2 * spaa(p2, p3) * spaa(p3, p4) * spbb(p4, p2 + p3, p1, p2)) +
            msq * spbb(p2, p4) * spbb(p2, p4) * spbb(p2, p4) * spbb(p2, p4) / (s234 * spbb(p2, p3) * spbb(p3, p4) * spbb(p4, p2 + p3, p1, p2));
    return T(0, 1) * amp;
}

} // namespace internal

/*
 * 3-pt Gravity helicity amplitudes M(1G,2G,3G)
 */
template <typename T, size_t Ds> T M3_ppm(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3) {
    using namespace internal;
    return T(0, 1) * A3_ppm(p1, p2, p3) * A3_ppm(p1, p2, p3);
}
template <typename T, size_t Ds> T M3_mmp(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3) {
    using namespace internal;
    return T(0, 1) * A3_mmp(p1, p2, p3) * A3_mmp(p1, p2, p3);
}

/*
 * 3-pt Gravity helicity amplitudes M(1_s,2_G,3_s)
 */
template <typename T, size_t Ds> T M3_sps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3) {
    using namespace internal;
    return T(0, 1) * A3_sps(p1, p2, p3) * A3_sps(p1, p2, p3);
}
template <typename T, size_t Ds> T M3_sms(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3) {
    using namespace internal;
    return T(0, 1) * A3_sms(p1, p2, p3) * A3_sms(p1, p2, p3);
}
/*
 * 4-pt Gravity helicity amplitudes M(1s,2s,3G,4G)
 */
template <typename T, size_t Ds> T M4_sspp(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    using namespace internal;
    // A4(1s,3G,2s,4G)
    T amp = -A4_spps(p1, p3, p4, p2) - A4_spps(p1, p4, p3, p2);
    return T(0, -2) * (p1 * p3) * (p1 * p4) / (p3 * p4) * amp * amp;
}

/*
 * 4-pt Gravity helicity amplitudes M(1s,2G,3G,4s)
 */
template <typename T, size_t Ds> T M4_spps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    using namespace internal;
    T msq = p1 * p1;
    T s12 = (p1 + p2) * (p1 + p2);
    // A4(1s,2+,3+,4s)
    T amp1 = A4_spps(p1, p2, p3, p4);
    // A4(1s,2+,4s,3+)
    T amp2 = -A4_spps(p1, p2, p3, p4) - A4_spps(p1, p3, p2, p4);
    return T(0, -1) * (s12 - msq) * amp1 * amp2;
}
template <typename T, size_t Ds> T M4_spms(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    using namespace internal;
    T msq = p1 * p1;
    T s12 = (p1 + p2) * (p1 + p2);
    // A4(1s,2+,3-,4s)
    T amp1 = A4_spms(p1, p2, p3, p4);
    // A4(1s,2+,4s,3-)
    T amp2 = -A4_spms(p1, p2, p3, p4) + conj(A4_spms(p1, p3, p2, p4));
    return T(0, -1) * (s12 - msq) * amp1 * amp2;
}
template <typename T, size_t Ds> T M4_smps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    using namespace internal;
    T msq = p1 * p1;
    T s12 = (p1 + p2) * (p1 + p2);
    // A4(1s,2-,3+,4s)
    T amp1 = -conj(A4_spms(p1, p2, p3, p4));
    // A4(1s,2-,4s,3+)
    T amp2 = conj(A4_spms(p1, p2, p3, p4)) - A4_spms(p1, p3, p2, p4);
    return T(0, -1) * (s12 - msq) * amp1 * amp2;
}
template <typename T, size_t Ds> T M4_smms(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    using namespace internal;
    T msq = p1 * p1;
    T s12 = (p1 + p2) * (p1 + p2);
    // A4(1s,2-,3-,4s)
    T amp1 = -conj(A4_spps(p1, p2, p3, p4));
    // A4(1s,2-,4s,3-)
    T amp2 = conj(A4_spps(p1, p2, p3, p4)) + conj(A4_spps(p1, p3, p2, p4));
    return T(0, -1) * (s12 - msq) * amp1 * amp2;
}
/*
 * 5-pt Gravity helicity amplitudes M(1s,2G,3G,4G,5s)
 */
template <typename T, size_t Ds> T M5_sppps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4, const momD<T, Ds>& p5) {
    using namespace internal;
    T msq = p1 * p1;
    T s12 = (p1 + p2) * (p1 + p2), s34 = (p3 + p4) * (p3 + p4), s13 = (p1 + p3) * (p1 + p3), s24 = (p2 + p4) * (p2 + p4);
    // A5(1s,2+,3+,4+,5s)
    T amp1 = A5_sppps(p1, p2, p3, p4, p5);
    // A5(2+,1s,4+,3+,5s)
    T amp2 = -A5_sppps(p1, p4, p3, p2, p5) - A5_sppps(p1, p2, p4, p3, p5) - A5_sppps(p1, p4, p2, p3, p5);
    // A5(1s,3+,2+,4+,5s)
    T amp3 = A5_sppps(p1, p3, p2, p4, p5);
    // A5(3+,1s,4+,2+,5s)
    T amp4 = -A5_sppps(p1, p4, p2, p3, p5) - A5_sppps(p1, p3, p4, p2, p5) - A5_sppps(p1, p4, p3, p2, p5);
    return T(0, 1) * ((s12 - msq) * s34 * amp1 * amp2 + (s13 - msq) * s24 * amp3 * amp4);
}
template <typename T, size_t Ds> T M5_sppms(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4, const momD<T, Ds>& p5) {
    using namespace internal;
    T msq = p1 * p1;
    T s12 = (p1 + p2) * (p1 + p2), s34 = (p3 + p4) * (p3 + p4), s13 = (p1 + p3) * (p1 + p3), s24 = (p2 + p4) * (p2 + p4);
    // A5(1s,2+,3+,4-,5s)
    T amp1 = A5_sppms(p1, p2, p3, p4, p5);
    // A5(2+,1s,4-,3+,5s)
    T amp2 = A5_sppms(p5, p2, p3, p4, p1) + A5_sppms(p5, p3, p2, p4, p1) - A5_spmps(p1, p2, p4, p3, p5);
    // A5(1s,3+,2+,4-,5s)
    T amp3 = A5_sppms(p1, p3, p2, p4, p5);
    // A5(3+,1s,4-,2+,5s)
    T amp4 = A5_sppms(p5, p3, p2, p4, p1) + A5_sppms(p5, p2, p3, p4, p1) - A5_spmps(p1, p3, p4, p2, p5);
    return T(0, 1) * ((s12 - msq) * s34 * amp1 * amp2 + (s13 - msq) * s24 * amp3 * amp4);
}
template <typename T, size_t Ds> T M5_spmps(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4, const momD<T, Ds>& p5) {
    using namespace internal;
    T msq = p1 * p1;
    T s12 = (p1 + p2) * (p1 + p2), s34 = (p3 + p4) * (p3 + p4), s13 = (p1 + p3) * (p1 + p3), s24 = (p2 + p4) * (p2 + p4);
    // A5(1s,2+,3-,4+,5s)
    T amp1 = A5_spmps(p1, p2, p3, p4, p5);
    // A5(2+,1s,4+,3-,5s)
    T amp2 = -A5_spmps(p1, p4, p3, p2, p5) - A5_sppms(p1, p4, p2, p3, p5) - A5_sppms(p1, p2, p4, p3, p5);
    // A5(1s,3-,2+,4+,5s)
    T amp3 = -A5_sppms(p5, p4, p2, p3, p1);
    // A5(3-,1s,4+,2+,5s)
    T amp4 = A5_sppms(p5, p2, p4, p3, p1) - A5_sppms(p1, p4, p2, p3, p5) - A5_spmps(p1, p4, p3, p2, p5);
    return T(0, 1) * ((s12 - msq) * s34 * amp1 * amp2 + (s13 - msq) * s24 * amp3 * amp4);
}

/*
 * 4-pt Gravity helicity amplitudes M(1G,2G,3s,4s)
 */
template <typename T, size_t Ds> T M4_ppss(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    using namespace internal;
    T s12 = (p1 + p2) * (p1 + p2);
    // A4(4s,1+,2+,3s)
    T amp1 = A4_spps(p4, p1, p2, p3);
    // A4(3s,1+,2+,4s)
    T amp2 = A4_spps(p3, p1, p2, p4);
    return T(0, -1) * s12 * amp1 * amp2;
}
template <typename T, size_t Ds> T M4_pmss(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    using namespace internal;
    T s12 = (p1 + p2) * (p1 + p2);
    // A4(4s,1+,2-,3s)
    T amp1 = A4_spms(p4, p1, p2, p3);
    // A4(3s,1+,2-,4s)
    T amp2 = A4_spms(p3, p1, p2, p4);
    return T(0, -1) * s12 * amp1 * amp2;
}

/*
 * 4-pt S1(p1) S2(p2) -> S2(p3) S1(p4) scattering amplitude
 */
template <typename T, size_t Ds> T M4_ssss(const momD<T, Ds>& p1, const momD<T, Ds>& p2, const momD<T, Ds>& p3, const momD<T, Ds>& p4) {
    T s = (p1 + p2) * (p1 + p2);
    T t = (p1 + p4) * (p1 + p4);
    T u = (p1 + p3) * (p1 + p3);
    T m1sq = p1 * p1;
    T m2sq = p2 * p2;
    T amp = -m2sq * m2sq - m1sq * m1sq + u * m2sq + u * m1sq - u * u / T(2) + t * t / T(2) + s * m2sq + s * m1sq - s * s / T(2);
    amp *= T(0, 1) / (T(4) * t);
    // Caravel removes (kappa/2)^2 factor
    amp *= T(4);
    return amp;
}

} // namespace Caravel
