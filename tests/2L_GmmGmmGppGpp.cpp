/**
 * Check of two-loop 4-graviton amplitude using CubicGravity. It compares 
 * against an analytic expression collected in the '4graviton' repository.
 * The comparison involves the generic bubble-box coefficient of the 
 * 2-loop amplitude with MHV helicity.
 *
 * In the process a non-planar subtraction/coeff is indirectly checked.
 */

#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "Core/settings.h"

#include "Forest/cardinality.h"

using namespace std;
using namespace Caravel;

// Loading four point hierarchy
int main(int argc, char* argv[]) {
    using F = F32;
    using IntegrandHierarchy::slice_selector;

    cardinality_update_manager<F>::get_instance()->set_cardinality(2147483629);

    verbosity_settings.show_all();

    settings::use_setting("general::do_n_eq_n_test yes");
    settings::use_setting("general::numerator_decomposition master_surface");
    settings::use_setting("general::master_basis standard");
    settings::use_setting("IntegrandHierarchy::parallelize_topology_fitting always");
    //settings::use_setting("IntegrandHierarchy::number_of_Ds_values 5");
    settings::use_setting("IntegrandHierarchy::optimize_warmup_for_cuts no");

    settings::read_settings_from_file();

    CubicGravity model;

    slice_selector ss(MathList("SliceSelector[Type[with_ancestors_slice],HierarchyPos[2,4]]"));

    PartialAmplitudeInput pa{"PartialAmplitudeInput[Particles[Particle[G,1,hmm],Particle[G,2,hmm],Particle[G,3,hpp],Particle[G,4,hpp]]]"};
    settings::use_setting("IntegrandHierarchy::amplitude_normalization tree");

    IntegrandHierarchy::load_surface_terms(model,pa);


    IntegrandHierarchy::ColourExpandedAmplitude<F, 2> amplitude(model, pa, ss);
    std::cout << "created ampl" << std::endl;

    auto warmup_info = load_warmups( amplitude.get_warmup_filename() );

    std::vector<BigRat> xs = {BigRat(-3) / BigRat(14), BigRat(-11) / BigRat(15)};

    auto ps_rat_r = ps_parameterizations::rational_mom_conf<BigRat>(4, xs/*, false*/);

    std::cout << "MOMENTA: " << ps_rat_r << std::endl;
    std::cout << " s12 = "<<ps_rat_r.s(1,2) << std::endl;
    std::cout << " s23 = "<<ps_rat_r.s(2,3) << std::endl;
    std::cout << " s13 = "<<ps_rat_r.s(1,3) << std::endl;

    momD_conf<F, 4> momconf_process(ps_rat_r);

    auto result_v = amplitude.get_master_coefficients_full_Ds(momconf_process);
    assert(result_v.size()==1);
    auto result = result_v[0];
    //_PRINT(result);
    
    // get the integrals 
    auto integrals = amplitude.get_integral_graphs();

    for(size_t ii = 0; ii < result.size(); ii++ ){
        std::cout<<result[ii]<<" * "<<integrals[ii];
        if(ii+1<result.size())
            std::cout<<" +";
        std::cout<<std::endl;
    }

    //                           ep = 1/7     Ds=6
    auto check = ((result.back())(F(1)/F(7)))(F(6));
    auto target = F(1197578472)/F(4);
    // Notice that in the (nested) functor evaluations before
    // the Ds = 6 works even when ds-strategy=decomposition, as 
    // actually only Ds=6 is computed (and gives the 'constant'
    // factor)

    // Analytic expression taken from line 31 of the file:
    //    ~analytics/twoloop/mmpp/reconstructed_(G[m],G[m],G[p],G[p])_2147483629.txt
    // in the '4graviton' repository commit # 7257cc9e2ed487854
    std::cout<<"Bubble-box coeff for Ds = 6, ep = 1/7, and x = t/s = 154/45"<<std::endl;
    std::cout<<"	from analytics (fixing normalization by s^3): 	"<< target <<std::endl;
    std::cout<<"	from computation:		 		"<< check <<std::endl;

    if( check == target )
        return 0;
    else
        return 1;

}
