/**
 * @file Amplitude.h
 *
 * @date 21.11.2018
 *
 * @brief A class template structuring interactions between coefficient providers and integral providers in Caravel
 */
#ifndef AMPLITUDE_H_
#define AMPLITUDE_H_

#include <vector>
#include <utility>
#include <functional>

#include "Graph/IntegralGraph.h"
#include "Core/Series.h"
#include "Core/momD_conf.h"
#include "IntegralLibrary/IntegralProvider.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/BasisElementFunction.h"

namespace Caravel {
/**
 * The class coefficientProvider is assumed to contain the methods:
 *
 * std::vector<IntegralGraph> get_integral_graphs() const
 * std::vector<CoeffType> compute(const momD_conf&)
 * std::function<std::vector<CoeffType>(const momD_conf&)> get_coeff_evaluator()
 *
 * NOTICE: CoeffType.taylor_expand_around_zero(int) must exist.
 *
 *
 * The class integralProvider is assumed to contain the methods:
 *
 * void set_mu_squared(const T&)
 * std::vector<Series<T>> compute(const momD_conf&) const
 * std::vector<NFBExpansion> get_abstract_integral(const momD_conf&) (read below for using declaration of NFBExpansion)
 * std::function<std::vector<NFBExpansion>(const momD_conf&)> get_int_evaluator()
 *
 */
template <typename T, class coefficientProvider, class integralProvider> class Amplitude {
    int min_n_eps{-2}; /**< When doing this->compute(), this is the minimum exponent in the Laurent series to be computed (can be changed by
                          this->set_laurent_range(.)) */
    int max_n_eps{
        0}; /**< When doing this->compute(), this is the maximum exponent in the Laurent series to be computed (can be changed by this->set_laurent_range(.)) */
  public:
    coefficientProvider coefficients;
    integralProvider integrals;

    Amplitude() = default;

    /**
     * Main constructor. We move the argument into the 'coefficients' element. From coefficients.get_integral_graphs() we construct the element 'integrals'.
     * Notice we set by default at construction time mu^2 = 1
     */
    Amplitude(coefficientProvider&&);

    /**
     * Changes the range of the Laurent series that is computed by this->compute()
     */
    void set_laurent_range(int min, int max) {
        min_n_eps = min;
        max_n_eps = max;
    }
    /**
     * Given a phase space point it evaluates the coefficients and the integrals and returns result 
     * (through coefficients.eval(.)+coefficients.get_coeffs() and integrals.eval(.))
     */
    Series<T> compute(const momD_conf<T, 4>&);

    /**
     * The type used for the coefficients in the function basis
     * expansion of the integral.
     */
    using CType = Integrals::FBCoefficient<Series<T>, T>;
    /**
     * The type of special functions used in
     * the master integral evaluation.
     */
    using SFType = Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>;
    /**
     * The type that a function Caravel::Integrals::master<CType,SFType>.get_basis() returns
     * when evaluated on a PS point (a set of invariants extracted from a given momD_conf)
     */
    using NFBExpansion = Integrals::NumericFunctionBasisExpansion<Series<T>, SFType>;

    using CoefficientProviderType = coefficientProvider;

    /**
     * Evaluates the coefficients and combines with master
     * integrals to give a (semi-analytic) integrated
     * expression.
     * Currently specialized to double precision integrals.
     *
     * A single object, instead of a vector as in ProcessLibrary.h, because here
     * we don't have several hierarchies
     */
    NFBExpansion integrate(const momD_conf<T, 4>&);
    /**
     * This method basically clones *this and returns an std::function (a lambda) which is equivalent to this->integrate(.)
     */
    std::function<NFBExpansion(const momD_conf<T, 4>&)> get_new_integrate_evaluator() const;
};

// IMPLEMENTATIONS
#include "Amplitude.hpp"

} // namespace Caravel
#endif // AMPLITUDE_H_
