#include "mathlink.h"
#include "WolframLibrary.h"

#include <vector>

template<typename T>
std::vector<std::vector<T>> vector_vector_from_mtensor(WolframLibraryData libData, const MTensor& tensor){

  //int err = LIBRARY_FUNCTION_ERROR;

  mint* data = libData -> MTensor_getIntegerData(tensor);
  const mint* dimensions = libData -> MTensor_getDimensions(tensor);

  std::vector<std::vector<T>> res;

  for (int64_t i = 0; i < dimensions[0]; i++){
    std::vector<T> vec(dimensions[1], T(0));

    for (int64_t j = 0; j < dimensions[1]; j++){
      vec[j] = data[i*dimensions[1] + j];
    }

    res.push_back(vec);
  }

  return res;

}

template<typename T>
int int64_mtensor_from_vector_vector(WolframLibraryData libData, MTensor& t, const std::vector<std::vector<T> >& v){

  int err = LIBRARY_FUNCTION_ERROR;

  mint n = v.size();
  mint m = v.at(0).size();

  mint indices_rank = 2;
  mint indices_dimensions[2] = {n, m};
  err = (libData -> MTensor_new(MType_Integer, indices_rank, indices_dimensions, &t));
  if (err){return err;}

  int64_t* t_data = libData -> MTensor_getIntegerData(t);
  for (int64_t i = 0; i < n; i++){
    for (int64_t j = 0; j < m; j++){
      t_data[i*m + j] = int64_t(v.at(i).at(j));
    }
  }

  return LIBRARY_NO_ERROR;
}

template<typename T>
int int64_mtensor_from_vector(WolframLibraryData libData, MTensor& t, const std::vector<T>& v){

  int err = LIBRARY_FUNCTION_ERROR;

  mint n = v.size();
  mint indices_rank = 1;
  mint indices_dimensions[1] = {n};

  err = (libData -> MTensor_new(MType_Integer, indices_rank, indices_dimensions, &t));
  if (err){return err;}
  int64_t* t_data = libData -> MTensor_getIntegerData(t);
  for (int64_t i = 0; i < n; i++){
    t_data[i] = int64_t(v.at(i));
  }

  return LIBRARY_NO_ERROR;
}

void message(WolframLibraryData libData, const char* msg_name, const char* msg){
  int pkt;
  MLINK link = libData->getMathLink(libData);
  MLPutFunction(link, "EvaluatePacket", 1);
  MLPutFunction(link, "Message", 2);
  MLPutFunction(link, "MessageName", 2);
  MLPutSymbol(link, "LibraryFunction");
  MLPutString(link, msg_name);
  MLPutString(link, msg);
  libData->processMathLink(link);
  pkt = MLNextPacket(link);
  if (pkt == RETURNPKT)
    MLNewPacket(link);
}

void message(WolframLibraryData libData, const char* msg_name, const std::string& msg){
  message(libData, msg_name, msg.c_str());
}

inline void print(WolframLibraryData libData, const char* msg) {
  if (libData->AbortQ()) return; // trying to use the MathLink connection during an abort appears to break it

  MLINK link = libData->getMathLink(libData);
  MLPutFunction(link, "EvaluatePacket", 1);
  MLPutFunction(link, "Print", 1);
  MLPutString(link, msg);
  libData->processMathLink(link);
  int pkt = MLNextPacket(link);
  if (pkt == RETURNPKT) MLNewPacket(link);
}

inline void print(WolframLibraryData libData, const std::string& msg) { print(libData, msg.c_str()); }
