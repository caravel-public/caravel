#ifndef MISC_MATH_H_INC
#define MISC_MATH_H_INC
namespace Caravel{
    template<typename T>
    inline T prod_pow(T x, size_t n){
	T result (1);
	for (size_t i = 0; i < n; i++){result *=x;}
	return result;
    }
}
#endif //MISC_MATH_H_INC
