#ifndef PARALLEL_TOOLS_H_INC
#define PARALLEL_TOOLS_H_INC

#include <thread>
#include <future>
#include <vector>
#include <functional>

namespace Caravel {

/**
 * Will use threads to paralellize a function call, calling in
 * chunks. Computes std::thread_hardware_concurrency() - 1 calls in one go.
 */
template <typename A, typename B> std::function<A(B)> block_computer(const std::function<A(B)>& f, std::function<B()> b_gen);

/**
 * Will use threads to paralellize a loop, calling in
 * chunks. Computes std::thread_hardware_concurrency() calls in one go.
 */
template <typename A, typename B, typename F> std::vector<A> parallel_map(F f, const std::vector<B>& xs);

/**
 * Distribute array {0,...,n-1} into bunches of size no more than nimax (defaule is # of cores)
 * Can be used to not launch more threads than cores available
 */
inline std::vector<std::vector<size_t>> split_into_bunches(size_t n, size_t nimax = std::thread::hardware_concurrency());

} // namespace Caravel

#include "ParallelTools.hpp"

#endif
