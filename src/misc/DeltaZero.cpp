#include "DeltaZero.h"

namespace Caravel {

#ifdef INSTANTIATE_GMP
template<> RGMP MaxDigits<RGMP>(){return RGMP(exp10(RGMP::get_current_nbr_digits()));}
#endif

}