/**
 * MPI_RationalReconstruction.h
 *
 * First created on 28.6.2018
 *
 * Header file to facilitate rational reconstructions in Caravel
*/

#include "Core/settings.h"

#include "Utilities.h"
#include "RationalReconstruction.h"
#include "Forest/cardinality.h"
#include "FunctionalReconstruction/DenseRational.h"

#include <mpi.h>

using namespace std;
using namespace Caravel;

namespace {

template <typename F> std::vector<uint64_t> convert_vector_to_uint64_t(const std::vector<F>& in) {
    std::vector<uint64_t> tret(in.size());
    std::transform(in.begin(), in.end(), tret.begin(), [](const F& c) { return uint64_t(c); });
    return tret;
}

template <typename F> std::vector<F> convert_vector_to_F(const std::vector<uint64_t>& in) {
    std::vector<F> tret(in.size());
    std::transform(in.begin(), in.end(), tret.begin(), [](const F& i) { return F(i); });
    return tret;
}

// from a
template <typename F>
void rational_reconstruction_step(bool& reconstructed, const std::vector<DenseRational<F>>& modulo_result, const BigInt& base_cardinality,
                                  const BigInt& composed_cardinality, std::vector<DenseRational<BigInt>>& composite_modulus_result,
                                  std::vector<DenseRational<BigRat>>& current_rational_guess) {

    std::function<BigInt(BigInt, F)> chinese_remainder_composite_modulus = [base_cardinality](BigInt x, F y) {
        return RationalReconstruction::chinese_remainder({x, base_cardinality}, y);
    };

    for (size_t ii = 0; ii < modulo_result.size(); ii++) {
        auto& yi = modulo_result.at(ii);
        auto& xi = composite_modulus_result.at(ii);

        for (size_t j = 0; j < xi.numerator.coefficients.size(); j++) {
            auto coeff = chinese_remainder_composite_modulus(xi.numerator.coefficients.at(j), yi.numerator.coefficients.at(j));
            xi.numerator.coefficients.at(j) = coeff;
        }

        for (size_t j = 0; j < xi.denominator.coefficients.size(); j++) {
            auto coeff = chinese_remainder_composite_modulus(xi.denominator.coefficients.at(j), yi.denominator.coefficients.at(j));
            xi.denominator.coefficients.at(j) = coeff;
        }
    }

    // Lift to rational result, and see if it remains constant
    std::function<BigRat(BigInt)> rational_guesser = [composed_cardinality](const BigInt& x) { return RationalReconstruction::rational_guess({x, composed_cardinality}); };

    bool unchanged(true);
    for (size_t ii = 0; ii < composite_modulus_result.size(); ii++) {
        auto& modulo_f = composite_modulus_result.at(ii);

        auto num = fmap(rational_guesser, modulo_f.numerator);
        auto denom = fmap(rational_guesser, modulo_f.denominator);

        DenseRational<BigRat> result(num, denom);

        auto& current_rational_f = current_rational_guess.at(ii);
        if (result != current_rational_f) { unchanged = false; }
        current_rational_f = result;
    }
    if (unchanged) { reconstructed = true; }
    // std::cout<<"Current rational guess: "<<current_rational_guess<<std::endl;
}

// this function makes a sequential check from [start,end) if one can rationally reconstruct the DenseRational with the passed data
template <typename F>
unsigned block_check_rational_reconstruction(bool& reconstructed, int start, int end, const std::vector<std::vector<std::vector<uint64_t>>>& all_evaluations,
                                             const std::vector<BigInt>& composite_moduli,
                                             std::vector<DenseRational<BigInt>>& composite_modulus_result,
                                             std::vector<DenseRational<BigRat>>& current_rational_guess,
                                             const std::vector<DenseRational<F>>& rational_structure, const std::vector<uint64_t>& moduli) {

    unsigned ntotal = 0;

    for (int ii = start; ii < end; ii++) {
        // adjust properly the modulus of the current finite field
        cardinality_update_manager<F>::get_instance()->set_cardinality(moduli.at(ii));

        // unflatten corresponding result
        std::vector<DenseRational<F>> modulo_result(rational_structure.size());
        for (size_t jj = 0; jj < rational_structure.size(); jj++)
            modulo_result[jj] = unflatten(rational_structure[jj].numerator.coefficients.size(),rational_structure[jj].denominator.coefficients.size(),convert_vector_to_F<F>(all_evaluations[ii][jj]));

        // if the result is vanishing we assume that something went wrong and skip
        bool vanishing = true;
        for (auto& it : modulo_result) {
            if (!it.numerator.is_zero()) {
                vanishing = false;
                break;
            }
        }

        if (vanishing) {
            _MESSAGE("Got vanishing modulo result. Ignoring.");
            continue;
        } else {
            // check if with this step we are done
            rational_reconstruction_step(reconstructed, modulo_result, composite_moduli.at(ii - 1), composite_moduli.at(ii), composite_modulus_result,
                                         current_rational_guess);
            ++ntotal;
            // if reconstructed, stop
            if (reconstructed) return ntotal;
        }
    }
    return ntotal;
}

    template <typename AmplType> bool _check_warmup_present(const AmplType& ampl){ return ampl.has_warmup(); }
    template <typename AmplType> bool _check_warmup_present(const std::vector<std::unique_ptr<AmplType>>& ampl){ return ampl.front()->has_warmup(); }

    template <typename AmplType> void _reload_warmup(AmplType& ampl){
        ampl.reload_warmup();
    }
    template <typename AmplType> void _reload_warmup(std::vector<std::unique_ptr<AmplType>>& ampl) {
        for (auto& it : ampl) { it->reload_warmup(); }
    }

}

/**
 * MPI Rational lifting function. Abstracted for any function
 * types.
 *
 * Similar structure of 'rational_reconstruct' in ~src/RationalReconstruction.h
 */
template <typename Field, template <class> class OutContainer, typename InTypeRational, typename InTypePrime, typename AmplType>
void MPI_rational_reconstruct(std::vector<OutContainer<BigRat>>& result, const InTypeRational& rational_x,
                              std::function<std::vector<OutContainer<StaticElement<Field>>>(const InTypePrime&)> modulo_f, std::vector<uint64_t> moduli,
                              int thread_number, int n_mpi_threads, AmplType& ampl) {

    using F = StaticElement<Field>;

    if (n_mpi_threads < 2) {
        std::cout << "We need at least 2 threads in MPI_rational_reconstruct, received " << n_mpi_threads << " --- exit 92" << std::endl;
        exit(92);
    }

    assert(moduli.size() > 2);

    bool have_warmup = _check_warmup_present(ampl);

    // the master thread
    if (thread_number == 0) {
        int max_worker_threads = n_mpi_threads - 1;

        if ((size_t)n_mpi_threads > moduli.size() - 2) {
            _WARNING("WARNING: not enough moduli (", moduli.size(), ") to start ", n_mpi_threads, " threads! (2 reserved for master)");
            max_worker_threads = (int)moduli.size() - 2;
            _WARNING("\twill start ", max_worker_threads, " threads instead");
        }

        int start_point(2);

        if (have_warmup) {
            // will send an order to all workers to compute with new cardinalities
            for (int ww = 1; ww <= max_worker_threads; ww++) {
                uint64_t the_cardinality(moduli.at(start_point + ww - 1));
                MPI_Send(&the_cardinality, 1, MPI_LONG_LONG, ww, 0, MPI_COMM_WORLD);
            }
        } else {
            _MESSAGE("Master will do warmup run first.");
        }

        decltype(modulo_f(declval<InTypePrime>())) modulo_result;

        // stores composite moduli
        std::vector<BigInt> composite_moduli;
        composite_moduli.push_back(BigInt(1));

        // Here we store the last composite evaluation (as we only have one at this point, we copy it)
        std::vector<OutContainer<BigInt>> composite_modulus_result;

        try {
            cardinality_update_manager<F>::get_instance()->set_cardinality(moduli.at(0));
            InTypePrime modulo_x(rational_x);
            modulo_result = modulo_f(modulo_x);
            // make the one reserved available again since we didn't need it
            moduli.push_back(moduli.at(1));
            moduli.erase(moduli.begin() + 1);
        } catch (Caravel::DivisionByZeroException) {
            _WARNING_R("Caravel::DivisionByZeroException in master thread while evaluating with cardinality ", moduli.at(0), "\nWill try one more time.");
            moduli.erase(moduli.begin());
            cardinality_update_manager<F>::get_instance()->set_cardinality(moduli.at(0));
            InTypePrime modulo_x(rational_x);
            modulo_result = modulo_f(modulo_x);
        }
        start_point -= 1;

        _MESSAGE("Thread ", thread_number, " finished computation mod ", moduli.at(0));
        composite_moduli.back() *= moduli.at(0);

        if (!have_warmup) {
            _MESSAGE("Master finished warming up.");
            // will send an order to all workers to compute with new cardinalities
            for (int ww = 1; ww <= max_worker_threads; ww++) {
                uint64_t the_cardinality(moduli.at(start_point + ww - 1));
                MPI_Send(&the_cardinality, 1, MPI_LONG_LONG, ww, 0, MPI_COMM_WORLD);
            }
        }

        for (auto& it : modulo_result) composite_modulus_result.push_back(OutContainer<BigInt>(it));

        // stores evaluations for each one of the moduli in uint64_t format
        std::vector<std::vector<std::vector<uint64_t>>> all_evaluations;
        all_evaluations.push_back(std::vector<std::vector<uint64_t>>());
        for (size_t ii = 0; ii < modulo_result.size(); ii++) all_evaluations[0].push_back(convert_vector_to_uint64_t(modulo_result[ii].flatten()));

        // Lift to rational result, and see if it remains constant
        BigInt lcm(composite_moduli.back());
        std::function<BigRat(BigInt)> rational_guesser = [lcm](const BigInt& x) { return RationalReconstruction::rational_guess({x, lcm}); };

        // Here we store the last corresponding rational guess
        std::vector<DenseRational<BigRat>> current_rational_guess;

        for (size_t i = 0; i < composite_modulus_result.size(); i++) {
            auto& lmodulo_f = composite_modulus_result.at(i);

            auto num = fmap(rational_guesser, lmodulo_f.numerator);
            auto denom = fmap(rational_guesser, lmodulo_f.denominator);

            DenseRational<BigRat> result(num, denom);

            // and fill current_rational_guess
            current_rational_guess.push_back(result);
        }

        int size_of_vector(modulo_result.size());
        std::vector<int> size_of_entries;
        for (int ll = 0; ll < size_of_vector; ll++) {
            size_t numerator_size(modulo_result[ll].numerator.coefficients.size());
            size_t denominator_size(modulo_result[ll].denominator.coefficients.size());
            size_of_entries.push_back(int(numerator_size + denominator_size));
        }

        unsigned ntotal = 1;

        bool reconstructed(false);
        while (true) {

            int current_offset = start_point;

            // add space to all_evaluations
            for (int ww = 1; ww <= max_worker_threads; ww++) {
                // all_evaluations.push_back(std::vector<std::vector<uint64_t>>());
                composite_moduli.push_back(composite_moduli.back());
                composite_moduli.back() *= moduli.at(current_offset + ww - 1);
            }
            // retrieve the data (we do it sequentially, as the whole process is as slow as the slowest thread)
            for (int ww = 1; ww <= max_worker_threads; ww++) {
                std::vector<std::vector<uint64_t>> thread_results(size_of_vector);
                // retrive element by element
                for (int tt = 0; tt < size_of_vector; tt++) {
                    // resize accordingly
                    thread_results[tt] = std::vector<uint64_t>(size_of_entries[tt]);
                    MPI_Status stat;
                    MPI_Recv(thread_results[tt].data(), size_of_entries[tt], MPI_LONG_LONG, ww, 1, MPI_COMM_WORLD, &stat);
                    int count;
                    MPI_Get_count(&stat, MPI_LONG_LONG, &count);
                    if (count == 0) { thread_results[tt] = std::vector<uint64_t>(size_of_entries[tt], 0); }
                }
                // store evaluation
                all_evaluations.push_back(thread_results);
            }
#if 0
std::cout<<"SHOWING stored values: "<<std::endl;
for(size_t ii=0;ii<all_evaluations.size();ii++)
	std::cout<<"	entry: "<<ii<<" as polynomial "<<all_evaluations[ii]<<" and lmodulus "<<moduli[ii]<<" composite: "<<composite_moduli[ii]<<std::endl;
#endif

            // now check if we have enough data to make rational reconstruction
            ntotal += block_check_rational_reconstruction(reconstructed, current_offset, current_offset + max_worker_threads, all_evaluations, composite_moduli,
                                                          composite_modulus_result, current_rational_guess, modulo_result, moduli);

            if (reconstructed) {
                _MESSAGE("***********");
                _MESSAGE("Required ", ntotal, " finite fields for reconstruction");
                _MESSAGE("***********");
                break;
            }

            int moduli_left = (int)moduli.size() - start_point - max_worker_threads;

            if (moduli_left <= 0) {
                _WARNING("ERROR: rational reconstruction failed! ", moduli.size(), " moduli was not enough!");
                break;
            }

            // increase starting point, in case latter needed (notice the -1, because master doesn't compute)
            start_point += max_worker_threads;

            if (max_worker_threads > moduli_left) {
                _WARNING("WARNING: not enough moduli left (", moduli_left, ") to start ", max_worker_threads, " threads!\n\tStarting ", moduli_left,
                         " threads.");
                max_worker_threads = moduli_left;
            }

            // will send an order to all workers to compute with new cardinalities
            for (int ww = 1; ww <= max_worker_threads; ww++) {
                uint64_t the_cardinality(moduli.at(start_point + ww - 1));
                MPI_Send(&the_cardinality, 1, MPI_LONG_LONG, ww, 0, MPI_COMM_WORLD);
            }
        }
        // pass to external
        result = current_rational_guess;
        // shutdown the workers
        for (int ww = 1; ww < n_mpi_threads; ww++) {
            uint64_t shutdown(0);
            MPI_Send(&shutdown, 1, MPI_LONG_LONG, ww, 0, MPI_COMM_WORLD);
        }
        std::cout << "Master done!" << std::endl;
    }
    // the workers
    else {
        bool first_run = true;
        // keep working until order to stop
        while (true) {

            uint64_t cardinality(0);
            MPI_Recv(&cardinality, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            // check if stop
            if (cardinality == 0) break;
            if (first_run && !have_warmup) {
                _reload_warmup(ampl);
                first_run = false;
            }

            _MESSAGE("Thread #", thread_number, " computes mod ", cardinality);
            // compute!

            cardinality_update_manager<F>::get_instance()->set_cardinality(cardinality);

            InTypePrime modulo_x(rational_x);
            decltype(modulo_f(modulo_x)) modulo_result;

            try {
                modulo_result = modulo_f(modulo_x);
            } catch (Caravel::DivisionByZeroException) {
                _WARNING_R("Caravel::DivisionByZeroException while evaluating with cardinality ", cardinality,
                           ".\nWill retry when a new evaluation order is received from the master thread.");
                for (int kk = 0; kk < int(modulo_result.size()); kk++) {
                    std::vector<uint64_t> lresult;
                    MPI_Send(lresult.data(), lresult.size(), MPI_LONG_LONG, 0, 1, MPI_COMM_WORLD);
                }
                continue;
            }

            std::cout << "Thread " << thread_number << " finished computation mod " << cardinality << std::endl;

            for (int kk = 0; kk < int(modulo_result.size()); kk++) {
                // prepare to send each entry of modulo_result
                std::vector<uint64_t> lresult(convert_vector_to_uint64_t(modulo_result[kk].flatten()));
                MPI_Send(lresult.data(), lresult.size(), MPI_LONG_LONG, 0, 1, MPI_COMM_WORLD);
            }
        }
        std::cout << "Stop worker # " << thread_number << std::endl;
    }
}
