#include "Core/Utilities.h"
#include <cassert>

namespace Caravel{

template <typename A, typename B> std::function<A(B)> block_computer(std::vector<std::function<A(B)>>&& f, std::function<B()> b_gen) {
    std::vector<std::pair<bool, A>> as;
    std::vector<B> bs;

    size_t pos = 0;

    const size_t num_threads = f.size();

    if (num_threads == 0) {throw std::runtime_error("No functors are passed to create threads");}

    return [f = std::move(f), as, bs, pos, b_gen, num_threads](B b) mutable {

        // Run chunk if we have exhausted last one (covers initial condition)
        if (pos >= as.size()) {
            auto chunk_start = as.size();

            // Compute chunk
            as.resize(as.size() + num_threads);

            for (size_t i = 0; i < num_threads; i++) {
                // Set all success bools to failure.
                as[chunk_start + i].first = false;
                // Allocate the bs in order
                bs.push_back(b_gen());
            }

            // Start parallel jobs
            std::vector<std::future<A>> futures;
            for (size_t i = 0; i < num_threads; i++) { futures.push_back(std::async(std::launch::async, f.at(i), bs[chunk_start + i])); }

            // Collect results
            for (size_t i = 0; i < num_threads; i++) {
                try {
                    as[chunk_start + i].second = futures.at(i).get();
                    as[chunk_start + i].first = true; // Report success
                } catch (const std::exception&) { continue; }
            }
        }

        if (!as.at(pos).first) {
            // If asking for a failed evaluation, throw:
            throw std::exception();
        }
        if (bs.at(pos) != b) {
            std::cerr << "ERROR: Misaligned cache in block computer." << std::endl;
            exit(1);
        }

        return as.at(pos++).second;

    };
}

template<typename A, typename B>
std::function<A(B)> block_computer(const std::function<A(B)>& f, std::function<B()> b_gen){
  std::vector<std::pair<bool,A>> as;
  std::vector<B> bs;

  size_t pos = 0;

  return [&f, as, bs, pos, b_gen](B b) mutable{
    auto num_threads = std::thread::hardware_concurrency();

    if (num_threads == 0){
      std::cerr << "Error: Unable to detect number of threads. " << std::endl;
    }

    const size_t chunk_size = num_threads-1;

    // Run chunk if we have exhausted last one (covers initial condition)
    if (pos >= as.size()){
      auto chunk_start = as.size();
      
      // Compute chunk
      as.resize(as.size() + chunk_size);

      for (size_t i = 0; i < chunk_size; i++){
	// Set all success bools to failure.
	as[chunk_start + i].first = false;
        // Allocate the bs in order
	bs.push_back(b_gen());
      }

      // Start parallel jobs
      std::vector<std::future<A>> futures;
      for (size_t i = 0; i < chunk_size; i++){
	futures.push_back(std::async(std::launch::async, f, bs[chunk_start+i]));
      }

      // Collect results
      for (size_t i = 0; i < chunk_size; i++){
	try{
	  as[chunk_start + i].second = futures.at(i).get();
	  as[chunk_start + i].first = true; // Report success
	}
	catch(const std::exception&){continue;}
      }
    }

    if (!as.at(pos).first){
      // If asking for a failed evaluation, throw:
      throw std::exception();
    }
    if (bs.at(pos) != b){
      std::cerr << "ERROR: Misaligned cache in block computer." << std::endl;
      exit(1);
    }

    return as.at(pos++).second;

  };

}

inline std::vector<std::vector<size_t>> split_into_bunches(size_t n, size_t nimax) {
    if (nimax == 0) {
        _WARNING("Asked to split into bunches of ", nimax, " which doesn't make sense! Will use 1 unstead!");
        nimax = 1;
    }
    if(n==0) return {};

    size_t n_bunches = n / nimax;
    if (n % nimax != 0) n_bunches += 1;

    std::vector<size_t> n_in_bunch(n_bunches,n/n_bunches);

    for(size_t i = 0; i < n%n_bunches; ++i){
        ++n_in_bunch[i];
    }

    std::vector<std::vector<size_t>> bunches;

    size_t i = 0;
    for(auto& b : n_in_bunch){
        std::vector<size_t> next_bunch;
        for(size_t c =0; c< b; ++c){
            next_bunch.push_back(i++);
        }
        bunches.push_back(next_bunch);
    }

    assert(bunches.back().back() == n-1);

    return bunches;
}


template <typename A, typename B, typename F> std::vector<A> parallel_map(F f, const std::vector<B>& xs) {
    std::vector<A> results;
    if (xs.size() == 0) { return results; }

    const auto bunches = split_into_bunches(xs.size());

    for (auto& b : bunches) {
        std::vector<std::future<A>> futures;
        for (auto i : b) {
            auto& x = xs.at(i);
            futures.push_back(std::async(std::launch::async, f, x));
        }
        // Collect results
        for (auto& future : futures) { results.push_back(future.get()); }
    }

    return results;
}

/**
 * Implementation of parallel_map when we have multiple copies of
 * f. Assumes that all of the functions in the input vector are
 * threadsafe copies of the same function f.
 */
template <typename A, typename B> std::vector<A> parallel_map(const std::vector<std::function<A(B)>>& fs, const std::vector<B>& xs) {
    if (fs.empty()) throw std::runtime_error("No functions provided!");
    if (fs.size() > std::thread::hardware_concurrency()) _WARNING_R("number of functions provided is larger than hardware_concurrency, will not use all of them");

    std::vector<A> results;
    if (xs.size() == 0) { return results; }

    const auto bunches = split_into_bunches(xs.size(), std::min(fs.size(),(size_t)std::thread::hardware_concurrency()));

    for (auto& b : bunches) {
        std::vector<std::future<A>> futures;
        for (auto i : b) {
            auto& f = fs.at(i%fs.size());
            auto& x = xs.at(i);
            futures.push_back(std::async(std::launch::async, f, x));
        }
        // Collect results
        for (auto& future : futures) { results.push_back(future.get()); }
    }

    return results;
}

} // namespace Caravel
