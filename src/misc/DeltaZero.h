#pragma once

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif
#include "Core/type_traits_extra.h"
#include "Core/typedefs.h"
#include <complex>
#include <cmath>

namespace Caravel{

/*
 * Functions for setting the distance to zero in a type safe way
 */

template <class T> inline T DeltaZero();
template<> inline R DeltaZero(){return R(1e-13);}
#ifdef HIGH_PRECISION
template<> inline RHP DeltaZero(){return RHP(1e-29);}
#endif
#ifdef VERY_HIGH_PRECISION
template<> inline RVHP DeltaZero(){return RVHP(1e-61);}
#endif
template<> inline C DeltaZero(){return C(1e-13);}
#ifdef HIGH_PRECISION
template<> inline CHP DeltaZero(){return CHP(1e-29);}
#endif
#ifdef VERY_HIGH_PRECISION
template<> inline CVHP DeltaZero(){return CVHP(1e-61);}
#endif


template <class T> inline T DeltaZeroSqr();
template<> inline R DeltaZeroSqr(){return R(1e-7);}
#ifdef HIGH_PRECISION
template<> inline RHP DeltaZeroSqr(){return RHP(1e-15);}
#endif
#ifdef VERY_HIGH_PRECISION
template<> inline RVHP DeltaZeroSqr(){return RVHP(1e-31);}
#endif
template<> inline C DeltaZeroSqr(){return C(1e-7);}
#ifdef HIGH_PRECISION
template<> inline CHP DeltaZeroSqr(){return CHP(1e-15);}
#endif
#ifdef VERY_HIGH_PRECISION
template<> inline CVHP DeltaZeroSqr(){return CVHP(1e-31);}
#endif

/*
 * A function to return the maximum number of digits a particular type has to work with
 */

template <class T> inline T MaxDigits();
template<> inline R MaxDigits<R>(){return R(16);}
#ifdef HIGH_PRECISION
template<> inline RHP MaxDigits<RHP>(){return RHP(32);}
#endif
#ifdef VERY_HIGH_PRECISION
template<> inline RVHP MaxDigits<RVHP>(){return RVHP(64);}
#endif

#if INSTANTIATE_GMP
// template<> inline RGMP MaxDigits<RGMP>() {return RGMP(exp10(RGMP::get_current_nbr_digits()));}
template<> RGMP MaxDigits<RGMP>();
#endif

template <typename T, typename TR = remove_complex_t<T>> TR compute_accuracy(const T& x, const T& target) {
    using std::abs;
    using std::log10;
    if (abs(x - target) == TR(0)) return Caravel::MaxDigits<TR>();

    if (abs(target) > Caravel::DeltaZero<TR>()) {
        return -log10(abs(x - target) / abs(target));
    } else
        return -log10(abs(x - target));
}

template <typename T, typename TR = remove_complex_t<T>, typename... Ts> bool check(const T& x, const T& target, double tol = 12.5) {
    using std::pow;
    using std::abs;
    auto zero_thr = pow(TR(10),TR(-tol));
    if (abs(target) < zero_thr && abs(x) < zero_thr) return true;
    if (compute_accuracy(x, target) > TR(tol)) return true;
    return false;
}

}
