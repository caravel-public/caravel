#pragma once

#include "cereal/cereal.hpp"

#include "cereal/types/complex.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/map.hpp"
#include "cereal/types/unordered_map.hpp"
#include "cereal/types/set.hpp"
#include "cereal/types/unordered_set.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/functional.hpp"
#include "cereal/types/memory.hpp"

#include "cereal/archives/json.hpp"

#include "Core/momD.h"
#include "Core/momD_conf.h"

#include "Core/typedefs.h"

#include "Core/Particle.h"
#include "Core/Series.h"
#include "Core/BigInt.h"
#include "Core/BigRat.h"

#include "IntegralLibrary/FunctionBasisExpansion.h"

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define GENERATE_FILENAME(...) (std::string(__FILENAME__)+"_"+std::string(#__VA_ARGS__))

namespace std {
template <typename A, typename D> void save(A& ar, const std::chrono::duration<D>& d) { ar(cereal::make_nvp("count", d.count())); }
} // namespace Cereal


#ifdef USE_FINITE_FIELDS
namespace Caravel {
  template <class A> void save(A& ar, const Caravel::BigInt& e) { ar(e.operator std::string()); }

  template <class A> void load(A& ar, Caravel::BigInt& e) {
    std::string s;
    ar(s);
    e = Caravel::BigInt(s.c_str());
}

  template <class A> void save(A& ar, const Caravel::BigRat& e) { ar(e.num(),e.den()); }

  template <class A> void load(A& ar, Caravel::BigRat& e) {
  Caravel::BigInt num,den;
    ar(num,den);
    e = Caravel::BigRat(num,den);
}

} // namespace Caravel

#endif

namespace Caravel {

#ifdef USE_FINITE_FIELDS
template <class A> void save(A& ar, const F32& e) { ar(e.operator uint64_t()); }
template <class A> void load(A& ar, F32& e) {
    uint64_t input;
    ar(input);
    e = F32{input};
}
#endif

template <class A, typename T> void save(A& ar, const Series<T>& e){
    ar(e.leading(),e.last(),e.get_term());
}
template <class A, typename T> void load(A& ar, Series<T>& e){
    short int f,l;
    std::vector<T> terms;
    ar(f,l,terms);
    e = Series<T>{f,l,terms};
}



template <class A,class T,size_t D> void save(A& ar, const momentumD<T,D>& m){
    ar(m.get_components());
}
template <class A,class T,size_t D> void load(A& ar, momentumD<T,D>& m){
    std::array<T,D> components;
    ar(components);
    m = decltype(m){components};
}

template <class A, class T, size_t D> void save(A& ar, const momentumD_configuration<T, D>& m) {
    std::vector<momentumD<T, D>> moms;
    moms.reserve(m.size());
    for (auto& p : m) { moms.push_back(p); }
    ar(moms);
}
template <class A,class T,size_t D> void load(A& ar, momentumD_configuration<T,D>& m){
    std::vector<momentumD<T, D>> moms;
    ar(moms);
    m = decltype(m){moms};
}


namespace Integrals{

template <class A,typename CType, typename FType> void save(A& a, const NumericFunctionBasisExpansion<CType,FType>& n) {
    std::map<FType,CType> ordered(n.begin(),n.end());
    a(ordered);
}
template <class A,typename CType, typename FType> void load(A& a, NumericFunctionBasisExpansion<CType,FType>& n) {
    std::unordered_map<FType,CType> ordered;
    a(ordered);
    n = NumericFunctionBasisExpansion<CType,FType>{ordered};
}

template <class A,typename CType, typename FType> void save(A& a, const FunctionBasisExpansion<CType,FType>& n) {
    std::map<FType,CType> ordered(n.begin(),n.end());
    a(ordered);
}
template <class A,typename CType, typename FType> void load(A& a, FunctionBasisExpansion<CType,FType>& n) {
    std::unordered_map<FType,CType> ordered;
    a(ordered);
    n = FunctionBasisExpansion<CType,FType>{ordered};
}

} // namespace Integrals
} // namespace Caravel


namespace Caravel{
    template <typename T> void write_serialized(const T& something, std::string filename){
        std::ofstream f(filename,std::ios::out);
        if(!f) throw std::runtime_error("Unable to open the file "+filename+"!");
        cereal::JSONOutputArchive ar(f,cereal::JSONOutputArchive::Options::NoIndent());
        ar(something);
    }
    template <typename T> void read_serialized( T& something, std::string filename){
        std::ifstream f(filename);
        if(!f) throw std::runtime_error("Unable to open the file "+filename+"!");
        cereal::JSONInputArchive ar(f);
        ar(something);
    }
    template <typename T> T read_serialized(std::string filename){
        T something;
        std::ifstream f(filename);
        if(!f) throw std::runtime_error("Unable to open the file "+filename+"!");
        cereal::JSONInputArchive ar(f);
        ar(something);
        return something;
    }
}
