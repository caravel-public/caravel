#pragma once

#include <thread>

#include "Core/type_traits_extra.h"

#include "PhaseSpace/PhaseSpaceParameterizations.h"
#include "Core/precision_cast.h"

#include "FunctionalReconstruction/DenseRational.h"

#ifdef COMPILE_INTEGRALS
#include "IntegralLibrary/FunctionBasisExpansion.h"
#ifdef WITH_PENTAGONS
#include "IntegralLibrary/SpecialFunctions/access_special_functions.h"
#endif
#endif

#include "IntegralLibrary/SpecialFunctions/Pi.h"
#include "FunctionSpace/MasterIntegrands.h"

#include "misc/DeltaZero.h"

#include "Core/settings.h"

#include "Core/enumerate.h"

namespace Caravel{


namespace Color {
enum struct Code {
    FG_RED = 31,
    FG_GREEN = 32,
    FG_ORANGE = 33,
    FG_BLUE = 34,
    FG_DEFAULT = 39,
    BG_RED = 41,
    BG_GREEN = 42,
    BG_BLUE = 44,
    BG_DEFAULT = 49
};

std::ostream& operator<<(std::ostream& os, Color::Code mod) { return os << "\033[" << static_cast<int>(mod) << "m"; }

}

template <typename... Targs> void _MESSAGE(Color::Code mod,Targs... args){
    static std::mutex m;
    std::lock_guard<std::mutex> lock(m);
    std::cout << mod;
    _MESSAGE(args...);
    std::cout << Color::Code::FG_DEFAULT;
}

template <typename... Targs> void _WARNING_RED(Targs... args){
    static std::mutex m;
    std::lock_guard<std::mutex> lock(m);
    std::cout << Color::Code::FG_RED;
    _WARNING("WARNING: ",args...);
    std::cout << Color::Code::FG_DEFAULT;
}

namespace {
    template <typename T> inline std::enable_if_t<!is_complex<T>::value,T> stof(std::string);
    //template <typename T> inline std::enable_if_t<is_complex<T>::value,T> stof(std::string);

    //template <typename T> inline std::enable_if_t<is_complex<T>::value,T> stof(std::string s) { return T(typename remove_complex<T>::type{s.c_str()}); }
    template <typename T> inline std::enable_if_t<!is_complex<T>::value,T> stof(std::string s) { return T(s.c_str()); }

    template <> inline double stof<double>(std::string s) { return std::stod(s); }
    //template <> inline std::complex<double> stof<std::complex<double>>(std::string s) { return std::complex<double>(std::stod(s)); }

    template <typename T> inline std::enable_if_t<!is_complex<T>::value,T> real(T r){return r;}
    template <typename T> inline std::enable_if_t<is_complex<T>::value,T> real(T r){return r.real();}
}

// exp(-gamma*L*ep)*(4*Pi)^(L*ep)
template <typename T>
static const Series<T> two_loop_norm_caravel_to_physical{
    0,
    7,
    stof<remove_complex_t<T>>("1.000000000000000000000000000000000000000000000000000000000000000000000"),
    stof<remove_complex_t<T>>("3.907617164135515864742759008374018833512271491391796961898431712"),
    stof<remove_complex_t<T>>("7.634735950723245567072289820636424602531244354026529572698730686"),
    stof<remove_complex_t<T>>("9.944541748229546812041161294588005093449213743415937750816884567"),
    stof<remove_complex_t<T>>("9.714865506210996727288829805858940609742294796505051753542559029"),
    stof<remove_complex_t<T>>("7.592395039867631563407088598505828534077896201550075975871614994"),
    stof<remove_complex_t<T>>("4.944695529114018560748998345789127322779366393790226245897327972"),
    stof<remove_complex_t<T>>("2.760282445855726475930700451357330121140898243413309407105600314"),
};
template <typename T>
static const Series<T> one_loop_norm_caravel_to_physical{0,
                                     7,
                                     stof<remove_complex_t<T>>("1.000000000000000000000000000000000000000000000000000000000000000000"),
                                     stof<remove_complex_t<T>>("1.95380858206775793237137950418700941675613574569589848094921585557"),
                                     stof<remove_complex_t<T>>("1.90868398768081139176807245515910615063281108850663239317468266991"),
                                     stof<remove_complex_t<T>>("1.24306771852869335150514516182350063668115171792699221885211057051"),
                                     stof<remove_complex_t<T>>("0.607179094138187295455551862866183788108893424781565734596409939359"),
                                     stof<remove_complex_t<T>>("0.237262344995863486356471518703307141689934256298439874245987968132"),
                                     stof<remove_complex_t<T>>("0.0772608676424065400117030991529551144184275999029722850921457495423"),
                                     stof<remove_complex_t<T>>("0.0215647066082478630932085972762291415714132675266664797430125024271")};

// exp(-gamma*ep)*(4*Pi)^(ep) / c_\Gamma = exp(-gamma*ep)*(4*Pi)^(ep) / ( GAMMA(1+epsilon)*GAMMA(1-epsilon)^2/GAMMA(1-2*epsilon)/(4*Pi)^(-epsilon) )
template <typename T>
static const Series<T> one_loop_norm_caravel_to_BH{0,
                                     7,
                                     stof<remove_complex_t<T>>("1.000000000000000000000000000000000000000000000000000000000000000000"),
                                     stof<remove_complex_t<T>>("0"),
                                     stof<remove_complex_t<T>>("0.822467033424113218236207583323012594609474950603399218867779114"),
                                     stof<remove_complex_t<T>>("2.804799440705719999266055710193383311784968015461164057515300300"),
                                     stof<remove_complex_t<T>>("3.855776520095929807275763168927910653635050266210464608620602782"),
                                     stof<remove_complex_t<T>>("8.735807157235739014484399031099591676049745078682016011051787270"),
                                     stof<remove_complex_t<T>>("17.26223366182219429102824264016760186950549098050469938451705853"),
                                     stof<remove_complex_t<T>>("34.39661786351170313940449308338175336822862458357758526124083696")};

/**
 * This sets the global settings regarding the choices of master integrands and corresponding intergrals to the default values,
 * depending on the multiplicity of the process.
 */
inline void set_settings_for_standard_choice_of_integrals(size_t multiplicity) {
    switch (multiplicity) {
        case 4:
            settings::use_setting("general::master_basis alternative");
            settings::use_setting("integrals::integral_family_1L goncharovs");
            settings::use_setting("integrals::integral_family_2L goncharovs");
            break;
        case 5:
            settings::use_setting("general::master_basis standard");
            settings::use_setting("integrals::integral_family_1L pentagon_functions");
            settings::use_setting("integrals::integral_family_2L pentagon_functions");
            break;
        default: 
            _WARNING_R("Not implemented for multiplicity ", multiplicity, "!"); 
            std::exit(1);
    }
}

#ifdef USE_FINITE_FIELDS
template <typename T>
std::enable_if_t<std::is_same<T, BigRat>::value, momD_conf<T, 4>> get_mom_conf(unsigned N, const std::vector<BigRat>& xs,
                                                                               const std::vector<size_t>& mi = std::vector<size_t>()) {
    return ps_parameterizations::rational_mom_conf<BigRat>(N, xs, mi, false);
}

template <typename T>
std::enable_if_t<is_floating_point<T>::value, momD_conf<T, 4>> get_mom_conf(unsigned N, const std::vector<BigRat>& xs,
                                                                            const std::vector<size_t>& mi = std::vector<size_t>()) {
    auto ps_rat = ps_parameterizations::rational_mom_conf<BigRat>(N, xs, mi, true);
    return to_precision<T>(ps_rat);
}
template <typename T>
std::enable_if_t<is_finite_field<T>::value, momD_conf<T, 4>> get_mom_conf(unsigned N, const std::vector<BigRat>& xs,
                                                                          const std::vector<size_t>& mi = std::vector<size_t>()) {
    auto ps_rat = ps_parameterizations::rational_mom_conf<BigRat>(N, xs, mi, true);
    return static_cast<momD_conf<T, 4>>(ps_rat);
}
#endif

template <typename T, typename TC = std::complex<T>> std::enable_if_t<!is_complex<T>::value,Series<TC>> complexify(const Series<T>& in) {
    Series<TC> out(in.leading(), in.last());
    for (int c = in.leading(); c <= in.last(); ++c) { out[c] = in[c]; }
    return out;
}
template <typename T> std::enable_if_t<is_complex<T>::value,Series<T>> complexify(const Series<T>& in) { return in; }

#ifdef COMPILE_INTEGRALS

template <typename T, typename Tres, typename IntType, template <class...> class Container>
Series<add_complex_t<T>> combine_with_integrals(const momD_conf<T, 4>& physical_point, const std::vector<IntType>& result_integrals,const std::vector<Container<Tres>>& result_coeffs, size_t loop_order = 2) {
    if(result_integrals.size()!=result_coeffs.size()){
        std::cerr<<"ERROR: different size integral/coeff containers in combine_with_integrals!"<<std::endl;
        std::exit(1);
    }
    DEBUG(for (size_t ii = 0; ii < result_coeffs.size(); ii++) std::cout << "coef_" << ii << ": " << result_coeffs[ii] << std::endl;);

    if(result_integrals.size()==0)
        return Series<T>(-4, 0);

    std::vector<Series<T>> master_coeffs;
    {
        std::vector<Series<Tres>> series_coeffs = to_Series(result_coeffs);
        for (const auto& elem : series_coeffs) { master_coeffs.push_back(complexify(to_precision<T>(elem))); }
    }

    auto amp = result_integrals[0]*master_coeffs[0];
    for(size_t ii=1;ii<result_integrals.size();ii++)
        amp += result_integrals[ii]*master_coeffs[ii];

    if(physical_point.size()<4){
        _WARNING_R("Size of mom conf is ", physical_point.size());
        std::exit(1);
    }

    // TODO 
    // the code below only works for 4,5 and 5+1 external mass kinematics,
    // could be generalized later

    // we simply provide all cyclic invariants for evaluation
    const size_t n_invariants = physical_point.size();

    std::vector<T> v(n_invariants);
    std::vector<std::string> names(n_invariants,"s");

    for (auto&& [i, obj]: enumerate(v)) {
        int i1 = i+1;
        int i2 = (i + 1) % n_invariants + 1;
        obj = to_precision<T>(physical_point.s(i1, i2)); 
        if(i2>i1){
            names.at(i) += std::to_string(i1) + std::to_string(i2) ;
        }
        else{
            names.at(i) += std::to_string(i2) + std::to_string(i1) ;
        }
    }


    switch (physical_point.size()){
        case 4: break;
        case 5: case 6:
            //FIXME: this probably doesn't work sometimes, we don't know here what is the order of arguments expected (e.g. 1-loop pentagon has it differently)
            v.push_back( FunctionSpace::MasterIntegrands::sign_tr5(physical_point.p(1), physical_point.p(2), physical_point.p(3), physical_point.p(4)) );
            names.push_back("signtr51234");
            v.push_back( T(4)*FunctionSpace::MasterIntegrands::levi(physical_point.p(1), physical_point.p(2), physical_point.p(3), physical_point.p(4)) );
            names.push_back("tr51234");
            break;
        default:
            _WARNING_R("Not implented!");
            std::exit(1);
    }

    auto eval = amp(v,names);

    if(loop_order == 1)
        return eval*one_loop_norm_caravel_to_physical<T>;
    else if(loop_order == 2)
        return eval*two_loop_norm_caravel_to_physical<T>;
    else {
        std::cerr<<"ERROR: combine_with_integrals not handling "<< loop_order << "-loop nomalization!"<<std::endl;
        std::exit(1);
    } 
}

#endif

}

