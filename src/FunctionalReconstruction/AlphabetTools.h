#pragma once
#include <functional>
#include <random>
#include <string>
#include <vector>

#include "FunctionalReconstruction/Alphabet.h"
#include "FunctionalReconstruction/ThieleFunction.h"

namespace Caravel {

/**
 * This structure holds the information outputted by the function 'remove_factors_in_alphabet'
 */
template <typename Output, typename Input, size_t N> struct FunctionFactors {
    std::function<std::vector<Output>(std::array<Input, N>)> function; /**< Vector valued function removing the 'factors' in its entries */
    std::vector<LetterMonomial> factors;                               /**< Corresponding factors which are removed in 'function' */
};

template <typename Output, typename Input, size_t N>
FunctionFactors<Output, Input, N> remove_factors_in_alphabet(std::function<std::vector<Output>(std::array<Input, N>)>& f, Alphabet* A, size_t seed = 42) {
    std::mt19937 mt1(seed);
    std::vector<Input> ts;
    std::vector<Input> as;
    for (size_t ii = 0; ii < N; ii++) {
        ts.push_back(Input(mt1()));
        as.push_back(Input(mt1()));
    }
    Slice slice = [&](const Input& x) {
        std::vector<Input> toret;
        for (size_t ii = 0; ii < N; ii++) toret.push_back(x * ts[ii] - as[ii]);
        return toret;
    };
    std::function<std::vector<Output>(Input)> g = [&](Input x) {
        auto vx = slice(x);
        std::array<Input, N> input;
        for (size_t ii = 0; ii < N; ii++) input[ii] = vx[ii];
        return f(input);
    };
    std::function<Input()> x_gen = [mt1]() mutable { return Input(mt1()); };
    VectorThieleFunction rec(g, x_gen);

    std::vector<Ratio<DensePolynomial<Output>>> canonical;
    for (std::size_t i = 0; i < rec.components.size(); ++i) canonical.push_back(rec.components[i].to_canonical());

    auto monomials = A->factorize_letters(canonical, slice);
    return {[=, mons = monomials](std::array<Input, N> in) {
                                 auto full = f(in);
                                 std::vector<Input> inletter(N);
                                 for (size_t ii = 0; ii < N; ii++) inletter[ii] = in[ii];
                                 for (size_t ii = 0; ii < full.size(); ii++) { full[ii] /= A->evaluate_letter(mons[ii], inletter); }
                                 return full;
                             },
                             monomials};
}

template <typename Output, typename Input>
FunctionFactors<Output, Input, 4> _return_helper_remove_factors_in_alphabetInvSpace(std::function<std::vector<Output>(std::array<Input, 4>)>& f, Alphabet* A,
                                                                                    const std::vector<LetterMonomial>& monomials) {
    // transform near_sijs {s23, s45, s51, x} to xs {x0, x1, x2, x3} (with x4 = s12 = 1)
    auto to_xs = [](const std::vector<Input>& near_sijs) {
        auto s23 = near_sijs.at(0);
        auto s45 = near_sijs.at(1);
        auto s51 = near_sijs.at(2);
        auto x = near_sijs.at(3);
        return std::vector<Input>{x, s51 / (x - s23 + s45), s23, s45};
    };

    return {[=, monms = monomials](std::array<Input, 4> in) {
                auto full = f(in);
                std::vector<Input> inletter(4);
                for (size_t ii = 0; ii < 4; ii++) inletter[ii] = in[ii];
                for (size_t ii = 0; ii < full.size(); ii++) { full[ii] /= A->evaluate_letter(monms[ii], to_xs(inletter)); }
                return full;
            },
            monomials};
}

/** 
 * This function is tailored for removing factors in invariant space (with massless 5-point kinematics)
 * according to arXiv:1904.00945
 */
template <typename Output, typename Input>
FunctionFactors<Output, Input, 4> remove_factors_in_alphabetInvSpace(std::function<std::vector<Output>(std::array<Input, 4>)>& f, Alphabet* A,
                                                                          std::string key = "", size_t seed = 42) {
    // standard naming of factors
    std::string filename = "reconstruction_results/factors/" + key + ".dat";
    std::vector<LetterMonomial> monomials;

    // check if results are stored
    if (key != "") {
        // make sure directory structure exists
        std::system("mkdir -p reconstruction_results/factors");
        // Check if monomials exist
        bool read(false);
        {
            std::ifstream file(filename);
            if (file.is_open()) read = true;
        }
        if (read) {
            monomials = read_letter_monomials_from_file(filename);
            return _return_helper_remove_factors_in_alphabetInvSpace(f, A, monomials);
        }
    }
    std::mt19937 mt1(seed);
    std::vector<Input> ts;
    std::vector<Input> as;
    for (size_t ii = 0; ii < 4; ii++) {
        ts.push_back(Input(mt1()));
        as.push_back(Input(mt1()));
    }
    Slice slice = [&](const Input& x) {
        return std::vector<Input>({as[0], as[1], x * ts[2] - as[2], x * ts[3] - as[3]});
    };

    // transform xs {x0, x1, x2, x3} (with x4 = s12 = 1) to near_sijs {s23, s45, s51, x}
    auto to_sijs = [](const std::vector<Input>& xs) {
        auto x0 = xs.at(0);
        auto x1 = xs.at(1);
        auto x2 = xs.at(2);
        auto x3 = xs.at(3);
        return std::vector<Input>{x2, x3, x1 * (x0 - x2 + x3), x0};
    };

    std::function<std::vector<Output>(Input)> g = [&](Input x) {
        auto sijs = to_sijs(slice(x));
        std::array<Input, 4> input;
        for (size_t ii = 0; ii < 4; ii++) input[ii] = sijs[ii];
        return f(input);
    };
    std::function<Input()> x_gen = [mt1]() mutable { return Input(mt1()); };

    VectorThieleFunction rec(g, x_gen);

    std::vector<Ratio<DensePolynomial<Output>>> canonical;
    for (std::size_t i = 0; i < rec.components.size(); ++i) canonical.push_back(rec.components[i].to_canonical());

    monomials = A->factorize_letters(canonical, slice);

    if (key != "") {
        // for storing
        write_letter_monomials_to_file(filename, monomials);
    }

    return _return_helper_remove_factors_in_alphabetInvSpace(f, A, monomials);
}

} // namespace Caravel
