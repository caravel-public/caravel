#ifndef INVERTING_RATIONAL_RECONSTRUCTOR_H_INC
#define INVERTING_RATIONAL_RECONSTRUCTOR_H_INC
#include <functional>
#include <vector>
#include <string>
#include "DenseRational.h"
namespace Caravel{
/**
 * Class representing a univariate rational function reconstructed
 * using simple linear inversion, once the degree is known.
 */
template< typename F >
class InvertingRationalReconstructor{
  std::function<F()> x_gen;
  size_t numerator_degree;
  size_t denominator_degree;
  public:
  size_t points_genned{0};
  std::optional<F> next_point();
  void add_point(F new_f, F new_x);
  bool finished{false};

  DenseRational<F> canonical_form;
  std::vector<std::pair<F,F>> inputs;
  std::string var_name;

  /**
   * Constructor which sets up a function to be "fed" input data through add_point.
   */
  InvertingRationalReconstructor(std::function<F()> x_gen, size_t numerator_degree,
				 size_t denominator_degree, std::string _var_name = "x");

 /**
  * Evaluates the reconstructed function on a given point x.
  */
  F operator ()(F x);

  /**
   * Converts the result to "canonical form", i.e. a ratio of two "canonical"
   * polynomials. Furthermore, the lowest order term in the denominator
   * polynomial is normalized to have unit coefficient.
   */
  DenseRational<F> to_canonical() const;

};
}

#include "InvertingRationalReconstructor.hpp"

#endif //INVERTING_RATIONAL_RECONSTRUCTOR_H_INC
