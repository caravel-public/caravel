#pragma once

#include <string>
#include <utility>
#include <vector>

#include "DenseRational.h"

namespace Caravel {
namespace Reconstruction {

template <typename T> class RationalFlattener {
  public:
    /**
     * Construct a RationalFlattener by passing a vector of rational functions.
     *
     * The shape of the vector and the contained rational functions is stored
     * such that subsequent calls to the call operator will flatten the passed
     * vector in the same way.
     *
     * Also a flattened vector can be unflattened by using the stored information.
     *
     * @param input  A vector containing rational functions.
     */
    RationalFlattener(std::vector<DenseRational<T>>&& input);

    /**
     * Flatten the given input vector of rational functions.
     */
    std::vector<T> operator()(const std::vector<DenseRational<T>>& input) const;

    /**
     * Compare 2 rational flatteners.
     *
     * @param other The RationalFlattener to be compared to *this.
     * @result      true if the RationalFlatteners are equal, else false;
     */
    bool operator==(const RationalFlattener<T>& other) const;

    /**
     * Compare 2 rational flatteners.
     *
     * @param other The RationalFlattener to be compared to *this.
     * @result      true if the RationalFlatteners are equal, else false;
     */
    bool operator!=(const RationalFlattener<T>& other) const;

    /// Return the number of dropped zeros
    std::size_t number_of_zeros() const { return dropped_zeros; }

    /// Return the number of elements in the flattened output vector.
    std::size_t flattened_output_size() const { return n_flattened; }

    template <typename REC>
    std::vector<DenseRational<REC>> unflatten(const std::vector<REC>& input, const std::string& variable="ep") const;

  private:
    /// Number of rational functions that are flattened.
    std::size_t n_rational_functions;

    /// Tag which entries are zeros
    std::vector<bool> zero_tagging;

    /// Store the numerator & denominator degrees for each rational function.
    std::vector<std::pair<std::size_t, std::size_t>> num_denom_degrees;

    /// Number of entries in the flattenend vector.
    std::size_t n_flattened;

    /// Store the number of dropped zeros.
    std::size_t dropped_zeros;
};


/// Convenience function to construct a RationalFlattener without having to provide template parameters.
template <typename T> RationalFlattener<T> make_rational_flattener(std::vector<DenseRational<T>>&& input) { return RationalFlattener<T>(std::move(input)); }

} // namespace Reconstruction
} // namespace Caravel

#include "RationalFlattener.hpp"
