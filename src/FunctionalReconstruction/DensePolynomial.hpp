#include <cassert>

namespace Caravel {

template <typename FF, typename FX> template <typename FF2, typename FX2> DensePolynomial<FF, FX>::DensePolynomial(const DensePolynomial<FF2, FX2>& other) : coefficients{} {
    var_name = other.var_name;
    coefficients.reserve(other.coefficients.size());
    for (auto& coeff : other.coefficients) { coefficients.push_back(static_cast<FF>(coeff)); }
}


template <typename FF, typename FX>
DensePolynomial<FF, FX>::DensePolynomial(const MathList& input) : var_name(input[0]) {
    assert_head(input, "DensePolynomial");
    if(input.size() != 2) throw std::invalid_argument("MathList "+math_string(input)+" cannot be interpreted as DensePolynomial");

    coefficients = InputParser::parse_MathList<std::vector<FF>>(input[1]);

    if (coefficients.empty()) {
        throw std::invalid_argument("DensePolynomial coefficients cannot be empy!");
    }
}

template <typename FF, typename FX>
DensePolynomial<FF, FX>::DensePolynomial(std::vector<FF> fs, std::vector<FX> xs, std::string _var_name, size_t min_power) : var_name(_var_name) {
    using std::vector;
    size_t basis_dim = fs.size();
    vector<FX> A(basis_dim * basis_dim, FX(0));
    for (size_t i = 0; i < basis_dim; i++) {
        vector<FX> basis;
        auto x = xs.at(i);
        for (size_t j = 0; j < basis_dim; j++) { basis.push_back(prod_pow(x, j + min_power)); }

        for (size_t j = 0; j < basis_dim; j++) { A.at(i + j * basis_dim) = basis.at(j); }
    }

    coefficients = std::vector<FF>(min_power, FF(0));
    auto basis_coefficients = plu_solver(std::move(A), std::move(fs));
    coefficients.insert(coefficients.end(), basis_coefficients.begin(), basis_coefficients.end());
}

template <typename FF, typename FX> FF DensePolynomial<FF, FX>::operator()(FX x) const {
    FF result(0);
    for (size_t i = 0; i < coefficients.size(); i++) { result += coefficients.at(i) * prod_pow(x, i); }
    return result;
}

template <typename FF, typename FX> DensePolynomial<FF, FX> DensePolynomial<FF, FX>::multiply_with_linear(FX x) {

    auto new_coefficients = coefficients;
    new_coefficients.push_back(coefficients.back());
    for (int i = coefficients.size() - 1; i > 0; i--) { new_coefficients.at(i) = x * new_coefficients.at(i) + new_coefficients.at(i - 1); }
    new_coefficients.at(0) = x * new_coefficients.at(0);

    return DensePolynomial<FF, FX>(new_coefficients, var_name);
}

template <typename FF, typename FX> DensePolynomial<FF, FX> DensePolynomial<FF, FX>::operator-() const {
    DensePolynomial<FF, FX> toret(*this);
    for (auto& it : toret.coefficients) it *= FF(-1);
    return toret;
}

template <typename FF, typename FX> DensePolynomial<FF, FX>& DensePolynomial<FF, FX>::operator+=(const FF& p) {
    if(coefficients.empty()){
        coefficients.push_back(p);
    }
    else{
        coefficients[0] += p;
    }
    return *this;
}
template <typename FF, typename FX> DensePolynomial<FF, FX>& DensePolynomial<FF, FX>::operator-=(const FF& p) {
    if(coefficients.empty()){
        coefficients.push_back(p);
    }
    else{
        coefficients[0] -= p;
    }
    return *this;
}

template <typename FF, typename FX> DensePolynomial<FF, FX>& DensePolynomial<FF, FX>::operator+=(const DensePolynomial<FF, FX>& p) {
    const size_t lim = std::min(coefficients.size(), p.coefficients.size());
    for (size_t i = 0; i < lim; i++) { coefficients.at(i) += p.coefficients.at(i); }
    for (size_t i = lim; i < p.coefficients.size(); i++) { coefficients.push_back(p.coefficients.at(i)); }
    check_degree();
    return *this;
}

template <typename FF, typename FX> DensePolynomial<FF, FX>& DensePolynomial<FF, FX>::operator-=(const DensePolynomial<FF, FX>& p) {
    const size_t lim = std::min(coefficients.size(), p.coefficients.size());
    for (size_t i = 0; i < lim; i++) { coefficients.at(i) -= p.coefficients.at(i); }
    for (size_t i = lim; i < p.coefficients.size(); i++) { coefficients.push_back(-p.coefficients.at(i)); }
    check_degree();
    return *this;
}

template <typename FF, typename FX> DensePolynomial<FF, FX>& DensePolynomial<FF, FX>::operator*=(const DensePolynomial<FF, FX>& other) {
    const int s = get_degree();
    const int s2 = other.get_degree();
    assert(var_name == other.var_name);

    const int new_degree = s + s2;

    std::vector<FF> new_coeffs(new_degree + 1, FF(0));
    new_coeffs.shrink_to_fit();

    for (int k = 0; k <= new_degree; k++) {
        for (int i = 0; i <= k; i++) {
            if (i <= s and (k - i) <= s2) new_coeffs.at(k) += coefficients.at(i) * other.coefficients.at(k - i);
        }
    }

    std::swap(coefficients, new_coeffs);

    check_degree();

    return *this;
}

template <typename FF, typename FX> DensePolynomial<FF, FX>& DensePolynomial<FF, FX>::operator*=(FX x) {
    for (auto& coeff : coefficients) { coeff *= x; }
    check_degree();
    return *this;
}

template <typename FF, typename FX> DensePolynomial<FF, FX>& DensePolynomial<FF, FX>::operator/=(FX x) {
    for (auto& coeff : coefficients) { coeff /= x; }
    check_degree();
    return *this;
}

template <typename FF, typename FX> DensePolynomial<FF, FX> DensePolynomial<FF, FX>::derivative() {
    std::vector<FF> derivative_coefficients;
    for (size_t i = 1; i < coefficients.size(); i++) { derivative_coefficients.push_back(FF(int(i)) * coefficients.at(i)); }

    return DensePolynomial<FF, FX>(derivative_coefficients, var_name);
}

template <typename FF, typename FX> bool operator==(const DensePolynomial<FF, FX>& a, const DensePolynomial<FF, FX>& b) {
    if (a.coefficients.size() != b.coefficients.size()) { return false; }

    if (a.get_degree() == 0 and b.get_degree() == 0) {
        if (a.var_name != b.var_name) {
            _WARNING("DensePolynomial::operator== for two polynomials with zero degrees in different variables is invoked. Result of comparing \"scalar\" "
                     "coefficents of type ",
                     _typeid(FF), " is returned.");
        }
        return a.coefficients.at(0) == b.coefficients.at(0);
    }

    for (size_t i = 0; i < a.coefficients.size(); i++) {
        if (a.coefficients.at(i) != b.coefficients.at(i)) { return false; }
    }

    // TODO: this warning might be dropped after some time since it can be considered expected behavior
    if(a.var_name != b.var_name) {
        _WARNING("DensePolynomial::operator== encountered polynomials which are different only by their variable names ",a.var_name," and ", b.var_name,". They are considered NOT EQUAL.");
        return false;
    }

    return true;
}

template <typename FFA, typename FXA, typename FFB, typename FXB>
DensePolynomial<FFB, FXB> fmap(const std::function<FFB(FFA)>& f, const DensePolynomial<FFA, FXA>& x) {
    std::vector<FFB> result_coeffs;
    for (size_t i = 0; i < x.coefficients.size(); i++) { result_coeffs.push_back(f(x.coefficients.at(i))); }

    return DensePolynomial<FFB, FXB>(result_coeffs, x.var_name);
}

template <typename FF, typename FX>
std::pair<DensePolynomial<FF, FX>, DensePolynomial<FF, FX>> polynomial_divide(const DensePolynomial<FF, FX>& a, const DensePolynomial<FF, FX>& b) {
    if (b.get_degree() < 0) throw std::runtime_error("Division by an empty polynomial.");
    if (a.get_degree() < b.get_degree()) throw std::runtime_error("Division by a polynomial of higher degree.");

    if (a.var_name != b.var_name) _WARNING("WARNING: dividing polynomials with different variable name.");

    DensePolynomial<FF, FX> q({FF(0)}, a.var_name);
    DensePolynomial<FF, FX> r(a);

    int d = b.get_degree();
    FF c = b.get_leading_coefficient();

    assert(c != FF(0));

    while (r.get_degree() >= d and !r.is_zero()) {
        std::vector<FF> s_coeffs(r.get_degree() - d + 1, FF(0));
        s_coeffs.back() = r.get_leading_coefficient() / c;

        DensePolynomial<FF, FX> s(std::move(s_coeffs), a.var_name);

        q += s;
        r -= (s *= b);
    }

    return std::make_pair(q, r);
}

template  <class FF, class FX>
std::enable_if_t< is_floating_point<FF>::value || is_exact<FF>::value, std::string>
ENCLOSING_CHARACTER(size_t i){ return ""; }

template  <class FF, class FX>
std::enable_if_t< !(is_floating_point<FF>::value || is_exact<FF>::value), std::string>
ENCLOSING_CHARACTER(size_t i){ if( i==0 ) return "("; else return ")"; }

template <class FF, class FX> std::ostream& operator<<(std::ostream& out, const DensePolynomial<FF, FX>& p) {
    std::string s = ENCLOSING_CHARACTER<FF, FX>(0);
    std::string e = ENCLOSING_CHARACTER<FF, FX>(1);

    if (p.get_degree() < 0)
        return out << FF(0);
    else if (p.get_degree() == 0)
        return out << s << p.coefficients.at(0) << e;

    for (size_t i = 0; i < p.coefficients.size(); i++) {
        auto& coeff = p.coefficients.at(i);

        if(is_exact<FF>::value and coeff == FF(0)) continue;

        out << s << coeff << e;
        if (i > 0) { out << "*" << p.var_name << "^" << i; }
        if (i < p.coefficients.size() - 1) { out << " + "; }
    }
    return out;
}

template <typename FF, typename FX> std::istream& operator>>(std::istream& in, DensePolynomial<FF, FX>& p) {
    MathList l;
    in >> l;

    if (l.head != "DensePolynomial") {
        _WARNING("Received head ", l.head, " while reading DensePolynomial");
        in.setstate(std::ios::failbit);
    }

    DensePolynomial<FF, FX> out;

    for (auto it : l) {
        std::stringstream ss(it);
        FF coeff;
        ss >> coeff;
        out.coefficients.push_back(coeff);

        if (ss.fail()) {
            _WARNING("Failed to read coefficient, got: ", it);
            in.setstate(std::ios::failbit);
        }
    }

    p = out;

    return in;
}

template <class FF, class FX> std::string math_string(const DensePolynomial<FF, FX>& p) {
    return math_list_with_head("DensePolynomial", p.var_name, math_list_with_head("Coefficients",p.coefficients));
}

}

// specialize hasher
namespace std{
template <typename T> struct hash<Caravel::DensePolynomial<T>> {
  public:
    size_t operator()(const Caravel::DensePolynomial<T>& xs) const {
        size_t result = 2;
        for (auto& it : xs.coefficients ) { Caravel::hash_combine(result, it); }
        return result;
    }
};
}
