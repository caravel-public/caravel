#ifndef RECONSTRUCTION_TYPE_TRAITS_H_INC
#define RECONSTRUCTION_TYPE_TRAITS_H_INC

#include <type_traits>
#include <cstddef>


namespace Caravel{

  namespace Reconstruction{
    // Forward declarations
    template<typename T> class VectorThieleFunction;
    template<typename T, size_t N> class PeraroFunction;

  template <typename T> struct is_reconstruction_algorithm : std::false_type {};

  template <typename F> struct is_reconstruction_algorithm<VectorThieleFunction<F>> : std::true_type {};
  template <typename F, size_t N> struct is_reconstruction_algorithm<PeraroFunction<F,N>> : std::true_type {};
    }


}

#endif
