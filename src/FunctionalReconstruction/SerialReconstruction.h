#ifndef SERIAL_RECONSTRUCTION_H_INC
#define SERIAL_RECONSTRUCTION_H_INC

#include "FunctionalReconstruction/type_traits.h"
#include <functional>

namespace Caravel{
namespace Reconstruction{

  template<typename Algorithm, typename Y, typename X, typename = std::enable_if<is_reconstruction_algorithm<Algorithm>::value>>
  void serial_reconstruct(Algorithm& algorithm, const std::function<Y(X)>& f){
        while (!algorithm.finished){

            auto new_x = algorithm.next_point();
            if (!new_x){algorithm.finished = true; break;}

            auto f_val = f(new_x.value()); 
            algorithm.add_point(f_val, new_x.value()); 
        }
  }
}
}

#endif //SERIAL_RECONSTRUCTION_H_INC
