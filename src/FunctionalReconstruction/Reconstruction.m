(* ::Package:: *)

(* ::Input::Initialization:: *)
BeginPackage["ReconstructionInterface`"];


(* ::Input::Initialization:: *)
$ReconstructorLocation::usage="Location of the ReconstructionInterface C++ binary. Must be set before reconstruct.";

reconstruct::usage="reconstruct[function, numVariables, vectorLength] takes a mathematica function and reconstructs the analytic form. If vectorLength is ommited, it is assumed that function is a scalar valued function.";

RequestPoints::usage = "Head for the request exchange";
Result::usage = "Head for the result exchange";
Cardinality::usage = "Head for cardinality";

(* ::Input::Initialization:: *)
Begin["`Private`"];


(* ::Input::Initialization:: *)
toField[i_Integer,cardinality_]:=Mod[i,cardinality];
toField[Rational[a_,b_],cardinality_]:=Mod[Mod[a,cardinality]*ModularInverse[b,cardinality],cardinality];


(* ::Input::Initialization:: *)
reconstructorResponse::usage="Takes a vector valued function f, and returns
the response to the reconstruction program";

reconstructorResponse[f_,request_RequestPoints,Cardinality[cardinality_]]:=Responses@@Response@@@(f@@@request/.x:(_Integer|_Rational):>toField[x,cardinality]);


(* ::Input::Initialization:: *)

reconstruct[function_, numVariables_Integer] := reconstruct[{function[##]}&, numVariables, 1] // First;

reconstruct[function_, numVariables_Integer, vectorLength_Integer] :=
        Module[{reconstructor = StartProcess[{$ReconstructorLocation, ToString@numVariables, ToString@vectorLength}],
                cardinality, request},

  (* Get settings *)
  cardinality=ToExpression[ReadLine[reconstructor]];

  (* Talk to the subprocess *)
  While[True,
    request=ToExpression[ReadLine[reconstructor]];
      Switch[Head[request],
        RequestPoints, With[{response = reconstructorResponse[function, request, cardinality]}, WriteLine[reconstructor, response];],
        Result, Return[First@request]
      ]
    ]
];


(* ::Input::Initialization:: *)
End[];
EndPackage[];


(* ::Input:: *)
(*$ReconstructorLocation="/Users/benjaminpage/Physics/Caravel/build/my_programs/ReconstructInterface";*)
