#ifndef UNIVARIATE_RECONSTRUCTION_H_INC
#define UNIVARIATE_RECONSTRUCTION_H_INC

// Utility header for the inclusion of only univariate techniques

#include "ThieleFunction.h"
#include "NewtonFunction.h"
#include "DensePolynomial.h"
#include "DenseRational.h"
#include "Core/Vec.h"
namespace Caravel{

  /**
   * Simplistic interface function for (vector-valued) univariate
   * rational function reconstruction.  Implementation for cases where
   * output type is input field.
   */
template<typename T>
std::vector<Ratio<DensePolynomial<T>,DensePolynomial<T>>>
reconstruct_vector_univariate_function(const std::function<std::vector<T>(T)>& f, std::function<T()> x_gen, std::string var_name = "x"){

    VectorThieleFunction<T> reconstructed_functions(f, x_gen, var_name);

    std::vector<Ratio<DensePolynomial<T>, DensePolynomial<T>>> result_functions;
    for (auto& component : reconstructed_functions.components){
      result_functions.push_back(component.to_canonical());
    }

    return result_functions;
}

/* For std::function<Caravel::Vec<T>(T)>*/
template<typename T>
std::vector<Ratio<DensePolynomial<T>,DensePolynomial<T>>>
reconstruct_vector_univariate_function(const std::function<Vec<T>(T)>& f, std::function<T()> x_gen, std::string var_name = "x"){

    std::function<std::vector<T>(T)> g = [f](T x) {return f(x).get_components(); };

    return reconstruct_vector_univariate_function(g, x_gen, var_name);
}

/* For std::function<T(T)>*/
template<typename T>
std::vector<Ratio<DensePolynomial<T>,DensePolynomial<T>>>
reconstruct_vector_univariate_function(const std::function<T(T)>& f, std::function<T()> x_gen, std::string var_name = "x"){

    std::function<std::vector<T>(T)> g = [f](T x) {std::vector<T> y = {f(x)}; return y; };

    return reconstruct_vector_univariate_function(g, x_gen, var_name);
}

  /**
   * Simplistic interface function for (vector-valued) univariate
   * rational function reconstruction.  Implementation for cases where
   * output type is vector space over input field.
   */
template<typename T>
  std::vector<Ratio<DensePolynomial<Vec<T>,T>,DensePolynomial<T>>>
  reconstruct_vector_univariate_function(const std::function<std::vector<Vec<T>>(T)>& f, std::function<T()> x_gen, std::string var_name = "x"){

    // Strategy is to unpack the vector components and run it through
    // the scalar version, and pack up again. Drastically simple idea,
    // but not too easy to express in C++.

    using std::function; using std::vector;
    using VectorRational = Ratio<DensePolynomial<Vec<T>,T>, DensePolynomial<T>>;

    size_t num_components = 0;
    size_t D = 0;
    function<vector<T>(T)> f_unpacked = [&f, &num_components, &D](T x){
	auto val = f(x);
	num_components = val.size();
        // we loop over entries to find the coeff with entries (that is not empty)
        for(auto r: val)  if(r.size() > D)  D = r.size();

	vector<T> results;
	for (auto& component : val){
	    for (size_t i = 0; i < D; i++){ 
                if(i >= component.size()) results.push_back(T(0));
                else    results.push_back(component[i]);
            }
	}

	return results;
    };

    auto unpacked_results = reconstruct_vector_univariate_function(f_unpacked, x_gen, var_name);

    vector<VectorRational> packed_results;
    for (size_t i = 0; i < num_components; i++){
        const size_t pos_s = D*i;

        // Find smallest common (amongst different Ds) denominator first
        std::vector<DensePolynomial<T>> den_Ds;

        for (size_t j = 0; j < D; j++){
            den_Ds.push_back(unpacked_results.at(pos_s+j).denominator);
        }

        bool all_equal = true;
        for(unsigned j=1; j<D; j++){
            if (den_Ds.front()!=den_Ds.at(j)){
                all_equal = false;
                break;
            }
        }

        DEBUG_PRINT(all_equal);

        DensePolynomial<T> denom;

        // if not all denominators are equal, we find the smallest common one
        if (!all_equal){
            denom = polynomial_LCM(den_Ds);

            for (size_t j = 0; j < D; j++) {
                auto fac = polynomial_divide(denom, den_Ds[j]);
                assert(fac.second.is_zero());
                DEBUG_PRINT(fac);
                unpacked_results.at(pos_s + j).denominator = denom;
                unpacked_results.at(pos_s + j).numerator *= fac.first;
            }
        }
        else{
            denom = den_Ds.front();
        }


        // Get number of coeffs
        size_t max_degree = 0;
        for (size_t j = 0; j < D; j++){
            auto& current_num = unpacked_results.at(D*i+j).numerator;
            if (current_num.coefficients.size() > max_degree){max_degree = current_num.coefficients.size();}
        }

        // Default initialize all to zero
        vector<Vec<T>> numerator_coeffs(max_degree, Vec<T>(D));
        for (size_t j = 0; j < D; j++){
            auto& current_num = unpacked_results.at(D*i+j).numerator;
            for (size_t k = 0; k < current_num.coefficients.size(); k++){
                numerator_coeffs.at(k)[j] = current_num.coefficients.at(k);
            }
        }

        DEBUG_PRINT(denom);

        packed_results.emplace_back(DensePolynomial<Vec<T>,T>{numerator_coeffs,var_name}, denom);
    }


    return packed_results;
}
    
}

#endif
