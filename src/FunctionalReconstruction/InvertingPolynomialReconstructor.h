#ifndef INVERTING_POLYNOMIAL_RECONSTRUCTOR_H_INC
#define INVERTING_POLYNOMIAL_RECONSTRUCTOR_H_INC
#include <functional>
#include <vector>
#include <string>
#include "DensePolynomial.h"
#include <optional>
namespace Caravel{
/**
 * Class representing a univariate rational function reconstructed
 * using simple linear inversion, once the degree is known.
 */
template< typename F >
class InvertingPolynomialReconstructor{
  std::function<F()> x_gen;
  size_t max_degree;
  size_t min_degree;
  public:
  size_t points_genned{0};
  std::optional<F> next_point();
  void add_point(F new_f, F new_x);
  bool finished{false};

  DensePolynomial<F> canonical_form;
  std::vector<F> fs;
  std::vector<F> xs;
  std::string var_name;

  /**
   * Constructor which sets up a function to be "fed" input data through add_point.
   */
  InvertingPolynomialReconstructor(std::function<F()> x_gen, size_t max_degree,
				   size_t min_degree = 0, std::string _var_name = "x");

 /**
  * Evaluates the reconstructed function on a given point x.
  */
  F operator ()(F x);

  /**
   * Converts the result to "canonical form", i.e. a ratio of two "canonical"
   * polynomials. Furthermore, the lowest order term in the denominator
   * polynomial is normalized to have unit coefficient.
   */
  DensePolynomial<F> to_canonical() const;

};
}

#include "InvertingPolynomialReconstructor.hpp"

#endif //INVERTING_POLYNOMIAL_RECONSTRUCTOR_H_INC
