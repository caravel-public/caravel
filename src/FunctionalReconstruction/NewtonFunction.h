#ifndef NEWTON_FUNCTION_H_INC
#define NEWTON_FUNCTION_H_INC

#include <functional>
#include <string>
#include <utility>
#include <vector>

#include "DensePolynomial.h"
#include <optional>

namespace Caravel {
/**
 * Given a set of "newton coordinates", specifying the function, and a
 * point x, will evaluate the function.
 * 
 * The "newton coordinates" are pairs of interpolation points and reconstructed coefficients.
 * In the notation of equation (3.5) of Peraro's paper they are of the form
 * <y_i, a_i>.
 */
template <typename F> F newton_function_evaluate(const std::vector<std::pair<F, F>>& coordinates, F x);

/**
 * Based upon section 3.2 of Peraro's paper. Class for reconstructing
 * a univariate polynomial function, using Newton's method.
 * 
 * The template type `F` refers to the type of the coefficients in the polynomial.
 */
template <typename F> class NewtonFunction {

    /**
     * Random generator of function arguments. Is used to decide which
     * point to evaluate the function under reconstruction.
     */
    std::function<F()> x_gen;

    /**
     * Creates a copy of the reconstruction object, but forgetting
     * all point data. Useful for guess_points.
     */
    NewtonFunction<F> clone_no_data() const;

  public:
    /**
     * If the user knows the maximum rank beforehand, then we take
     * this into account to avoid overcomputation.
     */
    int max_rank{-1};

	/// Track the number of generated points.
    size_t num_genned{0};

	/// Flag to check whether or not the reconstruction has finished.
    bool finished{false};

    std::string var_name{"x"};

	
	/// The coordinates defining the Newton function.
	/// They are given pairs of the form <y_i, a_i> in the notation of equation (3.5) from Peraro's Paper.
    std::vector<std::pair<F, F>> coordinates;

#ifdef DEBUG
	/// Store the input points for debugging.
    std::vector<std::pair<F, F>> inputs;
#endif // DEBUG

    /**
     * Makes a guess of the next n points that the reconstructor will
     * ask for. Only a guess because it may terminate, or terminate a
     * sub-reconstruction during the list.
     */
    std::vector<F> guess_points(size_t n) const;

    /**
     * Maybe returns the next point upon which the function under
     * reconstruction should be evaluated on. Can be called
     * multiple times, but after the first point, following points
     * are just guesses. Will be empty if no more points are needed.
     */
    std::optional<F> next_point();

    /**
     * Feed the reconstruction algorithm with a new sample.
	 * The function checks whether the evaluation of the function reconstructed 
	 * so far on `x_val` reproduces `f_val` and if this is the case sets `finished` 
	 * to `true`.
	 * 
	 * If the reconstruction is not finished, a new pair of newton coordinates is 
	 * being computed and stored.
	 * 
     * @param f_val Value of the function evaluated at x_val,
     * i.e. f_val = f(x_val).
     * @param x_val Value of argument such that f_val =
     * f(x_val). Should correspond to the point most recently
     * requested by next_point.
     */
    void add_point(F f_val, F x_val);

    /**
     * Constructor which reconstructs the function f by sampling it over
     * the points provided by x_gen.
     */
    NewtonFunction(std::function<F(F)> f, std::function<F()> x_gen, int max_rank = -1, std::string _var_name = "x");

    /**
     * Constructor for empty NewtonFunction which asks for samples.
     * @param x_gen Generator of points which the reconstructor sampels the function on.
     * @param max_rank If non-negative, the algorithm assumes that there are no terms of higher degree than max_rank, and does not figure this out through
     * computation.
     * @param name of variable
     */
    NewtonFunction(std::function<F()> x_gen, int max_rank = -1, std::string _var_name = "x");

    
    /// Evaluates the interpolated function at a given point `x`.
    F operator()(F x);

    
    /// Convert the polynomial to canonical form.
    DensePolynomial<F> to_canonical() const;
};

/**
 * A class which performs Newton's algorithm on a vector valued univariate function.
 */
template <class F> class VectorNewtonFunction {
    std::function<F()> x_gen;
    std::string var_name;

  public:
    /**
     * Boolean announcing whether or not the reconstruction has been completed.
     */
    bool finished{false};

    // TODO: Make private
    std::vector<NewtonFunction<F>> components;

    /**
     * Constructor which for running algorithm through add_point/next_point idiom.
     * @param x_gen random point generator
     * @param var_name optional name of variable
     */
    VectorNewtonFunction(std::function<F()> x_gen, std::string var_name = "x");

    /**
     * Constructor which automatically reconstructs the vector valued function f.
     * @param f function to reconstruct
     * @param x_gen random point generator
     * @param var_name optional name of variable
     * @param run_in_parallel optional flag to parallelize the calls to f. Assumes f to be thread-safe.
     */
    VectorNewtonFunction(std::function<std::vector<F>(F)> f, std::function<F()> x_gen, std::string var_name = "x", bool run_in_parallel = false);

    /**
     * Constructor which automatically reconstructs the vector valued function f.
     * @param vector of thread safe copies of f
     */
    VectorNewtonFunction(std::vector<std::function<std::vector<F>(F)>>&& f, std::function<F()> x_gen, std::string var_name = "x", bool run_in_parallel = false);

    /**
     * Converts the results to DensePolynomials. Only to be used when finished is true.
     */
    std::vector<DensePolynomial<F>> to_canonical();

    /**
     * Add a computed value of the function to the reconstruction in
     * progress. x_val must mach the value returned by the call of
     * next_point() performed after the last call to add_point().
     * @param f_val The vector valued output of the function being reconstructed.
     * @param x_val The value of x upon which the function was evaluated.
     */
    void add_point(const std::vector<F>& f_val, F x_val);

    /**
     * Returns the next point on which the function is to be evaluated
     */
    std::optional<F> next_point();

    /**
     * Guesses n points which we need to evaluate next in order to
     * continue the reconstruction. Useful for precomputation when
     * parallelizing.
     */
    std::vector<F> guess_points(size_t n) const;
};

} // namespace Caravel
#include "NewtonFunction.hpp"

#endif
