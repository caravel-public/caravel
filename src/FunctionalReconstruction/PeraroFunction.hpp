#include <utility>
#include <algorithm>
#include "misc/ParallelTools.h"
#include "SparseMultivariatePolynomial.h"
#include "ParallelReconstruction.h"

namespace Caravel{
namespace Reconstruction{

template <typename Y, typename F, size_t N>
std::function<Y(std::array<F, N>)> peraro_shift_function(const std::function<Y(std::array<F, N>)>& f, std::array<F, N> shift) {

    std::function<Y(std::array<F, N>)> shifted_f = [&f, shift](std::array<F, N> xs) {
        std::array<F, N> xsp;
        for (size_t i = 0; i < N; i++) { xsp[i] = xs[i] + shift[i]; }
        return f(xsp);
    };

    return shifted_f;
}

// Takes slice and point on slice, and undoes Peraro transform. In the
// presence of the optional transform, this must be taken into account.
template<typename F, size_t N>
std::array<F,N> PeraroFunction<F,N>::to_x_space(const std::array<F,N>& t_space){
    std::array<F,N> xs;

    xs[projective_slice] = t_space[N-1];

    auto s = point_transform ? point_transform.value()(t_space) : t_space[N-1];
    size_t j = 0;
    for (size_t i = 0; i < N; i++){
      if (i != projective_slice){
          xs[i] = t_space[j];
          if (rescale){xs[i] *= s;}
          j++;
      }
    }

    return xs;
}

template<typename F, size_t N>
size_t PeraroFunction<F,N>::slice_position() const{
     if (stage == Stage::PREPARATION){return preparation_reconstructor.num_genned;}
     if (stage == Stage::KNOWN_DENOMINATOR){return known_denominator_reconstructor.points_genned;}
     if (stage == Stage::KNOWN_DEGREE){return known_degree_reconstructor.points_genned;}

     throw std::runtime_error("Unknown stage in PeraroFunction::slice_position");
}

template<typename F, size_t N>
std::array<size_t, N> PeraroFunction<F,N>::get_current_position() const{
    std::array<size_t, N-1> sub_position = get_active_component_const().get_current_position();
    std::array<size_t, N> position;

    for (size_t i = 0; i < N-1; i++){position[i] = sub_position[i];}
    position[N-1] = slice_position();

    return position;
}


template<typename F, size_t N>
std::array<F,N> PeraroFunction<F,N>::find_shift(const std::function<F(std::array<F,N>)>& f) const{
    if (verbose){
	std::cout << "Searching for coordinate translation." << std::endl;
    }

    std::array<F,N> shift_attempt;
    auto gen_copy = x_gens[N-1];
    // First, find a point for which the function is defined, in order to use it as a shift.
    for (int shift_i = 1; shift_i <= 100; shift_i++){
	for (int i = 0; i < int(N); i++){
	    shift_attempt[i] = gen_copy();
	}

	// If the shift is possible, then break the loop.
	try{
	    f(shift_attempt);
	    if (verbose){std::cout << "Success! Shift = " << shift_attempt << std::endl;}
	    break;
	}
	catch(DivisionByZeroException&){continue;}
	// possibly coming from degenerate matrix in LUPDecompose
	catch (const std::runtime_error& error){continue;}
    }

    if (shift_attempt[0] == F(100)){
	throw std::runtime_error("ERROR: Function evaluation failed on 100 different origins. It is obtusely singular.");
    }

    return shift_attempt;
}

template<typename F, size_t N>
void PeraroFunction<F,N>::slice_add_point(F f_val, F s_val){
  if (stage == Stage::PREPARATION){
    preparation_reconstructor.add_point(f_val, s_val);
  }
  else if (stage == Stage::KNOWN_DEGREE){
    known_degree_reconstructor.add_point(f_val, s_val);
  }
  else /*if (stage == Stage::KNOWN_DENOMINATOR)*/{
    // Convert the rational function value into a polynomial function value
    std::vector<F> denominator_coefficients;
    for (auto& order : denominator_coordinates){
	denominator_coefficients.push_back(order(current_slice));
    }
    DensePolynomial<F> denominator(denominator_coefficients);
    auto den_val = denominator(s_val);

    auto num_val = f_val*den_val;

    // Remove the parts that are already known
    for (size_t i = 0; i < reconstruction_position.second; i++){
	num_val -= numerator_coordinates.at(i)(current_slice)*prod_pow(s_val,i);
    }
    
    known_denominator_reconstructor.add_point(num_val, s_val);
  }
}

template<typename F, size_t N>
std::optional<F> PeraroFunction<F,N>::slice_next_point(){
  std::optional<F> return_point;
  if (stage == Stage::PREPARATION){
    return_point = preparation_reconstructor.next_point();
  }
  else if (stage == Stage::KNOWN_DEGREE){
    return_point = known_degree_reconstructor. next_point();
  }
  else /*if (stage == Stage::KNOWN_DENOMINATOR)*/{
    return_point = known_denominator_reconstructor.next_point();
  }
  return return_point;
}

template<typename F, size_t N>
bool PeraroFunction<F,N>::slice_finished() const{
  if (stage == Stage::PREPARATION){
    return preparation_reconstructor.finished;
  }
  else if (stage == Stage::KNOWN_DEGREE){
    return known_degree_reconstructor. finished;
  }
  else /*if (stage == Stage::KNOWN_DENOMINATOR)*/{
    return known_denominator_reconstructor.finished;
  }
}

template<typename F, size_t N>
DenseRational<F> PeraroFunction<F,N>::slice_canonical(){
  if (stage == Stage::PREPARATION){
    return preparation_reconstructor.to_canonical();
  }
  else if (stage == Stage::KNOWN_DEGREE){
    return known_degree_reconstructor. to_canonical();
  }
  else /*if (stage == Stage::KNOWN_DENOMINATOR)*/{
    // Now that we know the denominator, we will never ask for it, so
    // we can put whatever we want there...
    auto canonical = DenseRational<F>(known_denominator_reconstructor.to_canonical(), DensePolynomial<F>({F(1)}));
    return canonical;
  }
}

template<typename F, size_t N>  
MultivariateNewtonFunction<F,N-1>& PeraroFunction<F,N>::get_active_component(){
    auto& active_section = reconstruction_position.first
      ? denominator_coordinates : numerator_coordinates;
    auto& active_component = active_section.at(reconstruction_position.second);
    return active_component;
}

template<typename F, size_t N>  
const MultivariateNewtonFunction<F,N-1>& PeraroFunction<F,N>::get_active_component_const() const{
    auto& active_section = reconstruction_position.first
      ? denominator_coordinates : numerator_coordinates;
    const auto& active_component = active_section.at(reconstruction_position.second);
    return active_component;
}

template<typename F, size_t N>  
void PeraroFunction<F,N>::add_slice(const std::array<F, N-1>& ts, const DenseRational<F>& canonical){

    slice_cache[ts] = canonical;

    auto& numerator = canonical.numerator.coefficients;
    auto& denominator = canonical.denominator.coefficients;

    if (stage == Stage::PREPARATION){
        // If working out univariate degrees 
        if (compute_individual_degrees){
            if (degrees.numerator_univariate.size() < N){
              size_t uni_num_degree, uni_den_degree;
              uni_num_degree = numerator.size() - 1;
              uni_den_degree = denominator.size() - 1;

              if (verbose){
                size_t var_num = degrees.numerator_univariate.size();
                std::cout << name << "Univariate numerator degree of variable " << var_num << " ==" << uni_num_degree << std::endl;
                std::cout << name << "Univariate denominator degree of variable " << var_num << " ==" << uni_den_degree << std::endl;
              }
              degrees.numerator_univariate.push_back(uni_num_degree);
              degrees.denominator_univariate.push_back(uni_den_degree);
              // Leave because this slice information isn't generic enough.
              return;
            }
        }

	degrees.numerator_total = numerator.size() - 1;
	degrees.denominator_total = denominator.size() - 1;

        if (!compute_individual_degrees){
            // As long as we are rescaling *FIXME*, the individual
            // variable degrees are *at least* limited by the total.
            degrees.numerator_univariate = std::vector<size_t>(N, degrees.numerator_total);
            degrees.denominator_univariate = std::vector<size_t>(N, degrees.denominator_total);
        }

	if (verbose){
          std::cout << name << "Total numerator degree == " << degrees.numerator_total << std::endl;
          std::cout << name << "Total denominator degree == " << degrees.denominator_total << std::endl;
	}

	// Initialize all of the constructors
	std::array<std::function<F()>,N-1> t_gens;
	for (size_t i = 0; i < N-1; i++){t_gens[i] = x_gens[i];}
        
        // Denominators with rank information
        std::vector<std::optional<int>> den_degrees;
        for (size_t i = 0; i < degrees.denominator_univariate.size()-1; i++){
          den_degrees.push_back(std::optional<int>(degrees.denominator_univariate.at(i)));
        }
        std::reverse(den_degrees.begin(), den_degrees.end());
	for (size_t i = 1; i <= degrees.denominator_total; i++){
          auto tot_degree = (rescale) ? i : -1;
	  denominator_coordinates.emplace_back(t_gens, tot_degree, den_degrees);
	}

        // Numerators with rank information
        std::vector<std::optional<int>> num_degrees;
        for (size_t i = 0; i < degrees.numerator_univariate.size()-1; i++){
          num_degrees.push_back(std::optional<int>(degrees.numerator_univariate.at(i)));
        }
        std::reverse(num_degrees.begin(), num_degrees.end());
	for (size_t i = 0; i <= degrees.numerator_total; i++){
          auto tot_degree = (rescale) ? i : -1;
	  numerator_coordinates.emplace_back(t_gens, tot_degree, num_degrees);
	}

        done_prep = true;
    }

    // Add points to current component (general for all components)

    auto& active_section = reconstruction_position.first
      ? denominator_coordinates : numerator_coordinates;
    auto& active_component = get_active_component();

    auto& active_val = (reconstruction_position.first ? denominator : numerator)
      .at(reconstruction_position.second);

    active_component.add_point(active_val, ts);

    if (active_component.finished){
      if (verbose){
	std::cout << name << "Done " << ((reconstruction_position.first) ? "denominator" : "numerator")
		  << " degree " << reconstruction_position.second << "/" << ((reconstruction_position.first) ? degrees.denominator_total : degrees.numerator_total) << std::endl;
      }

      // If finished a section
      if (active_section.size() == reconstruction_position.second + 1){
	// If we just finished the numerator, then we are done!
	if (reconstruction_position.first == false){
	    finished = true;
            std::string filename(name);
            if (verbose){
                std::cout << name << "Finished Reconstruction " <<filename<< std::endl;
            }
	}
	else{
	    if (verbose){
              std::cout << name << "Computed the "
			<< ((reconstruction_position.first) ? "denominator" : "numerator") << std::endl;
	    }
	}
      }
    }
}

template<typename F, size_t N>
void PeraroFunction<F,N>::add_point(F f_val, std::array<F,N> xs){
  if (xs != to_x_space(next_expected)){
    std::cerr << "Expected:" << next_expected << std::endl;
    std::cerr << "Got:" << xs << std::endl;
    throw std::runtime_error("Unexpected point in PeraroFunction<F,N>::add_point.");
  }

  // Use cache to avoid going back to "t space" by dividing and implementing again.
  xs = next_expected;

  auto s = point_transform ? point_transform.value()(xs) : xs[N-1];

  slice_add_point(f_val, s);

  // If the slice reconstruction is done, we can now feed the multivariatepolynomials
  if (slice_finished()){
    auto canonical = slice_canonical();
    std::array<F,N-1> ts;
    for (size_t i = 0; i < N-1; i++){ts[i] = xs[i];}
    add_slice(ts, canonical);
  }
}

template<typename F, size_t N>  
std::optional<std::pair<bool, size_t>> PeraroFunction<F,N>::next_component() const{
    auto result = reconstruction_position;

    auto& active_section = reconstruction_position.first
      ? denominator_coordinates : numerator_coordinates;

    // Move to next component
    result.second++;

    // If finished a section
    if (active_section.size() == result.second){
      if (result.first){
	// Move to the next section.
	result.second = 0;
	result.first = false;
      }
      else{
	return {}; // No next component
      }
    }

    return {result};
}

template<typename F, size_t N>  
PeraroFunction<F,N> PeraroFunction<F,N>::clone_no_data() const{
  // TODO: Implement so that it calls the clone_no_data methods of the sub-reconstructions.
  auto copy = (*this);
  copy.simulation = true;
  return copy;
}

template<typename F, size_t N>
std::optional<std::array<F,N>> PeraroFunction<F,N>::next_point(){

  if (finished){return std::optional<std::array<F,N>>();}

  // If univariate slice finished, reset it and get coordinates of
  // next slice from multivariatepolynomials.
  auto new_s = slice_next_point();
  if (!new_s){
    // In a simulation, the slice was successful, so "cache" it for later.
    if (simulation){slice_cache[current_slice] = {};}

    // We return to the beginning with a goto, when the value is in the
    // cache.  This avoids a stack overflow.
    next_slice_start:
    if (stage == Stage::PREPARATION){
      if (done_prep) stage = Stage::KNOWN_DEGREE;
      else preparation_reconstructor = new_preparation_reconstructor();
    }

    if (stage != Stage::PREPARATION){
        auto& active_component = get_active_component();

        auto maybe_current_slice = active_component.next_point();

        if (!maybe_current_slice){
            auto maybe_reconstruction_position = next_component();
            if (!maybe_reconstruction_position){return {};}
            reconstruction_position = maybe_reconstruction_position.value();

            if (reconstruction_position.second == 0){ // If we just moved to the first order
                stage = Stage::KNOWN_DENOMINATOR;
            }

            //if (simulation){
            //    std::cout << "Simulation: ";
            //}
            //std::cout << "Changing component to " << reconstruction_position << std::endl;

            auto& active_component = get_active_component();
            maybe_current_slice = active_component.next_point();
        }
        current_slice = maybe_current_slice.value();

        // If this new slice is cached, add it and ask for a new slice.
        auto cached_val = slice_cache.find(current_slice);
        if (cached_val != slice_cache.end()){
            if (!simulation){add_slice(current_slice, cached_val -> second);}
            goto next_slice_start;
        }
    }

    // Reset the slice reconstructor
    if (stage == Stage::KNOWN_DEGREE){
      known_degree_reconstructor = new_known_degree_reconstructor();
    }

    if (stage == Stage::KNOWN_DENOMINATOR){
      known_denominator_reconstructor = new_known_denominator_reconstructor();
    }

    new_s = slice_next_point();
  }

  std::array<F,N> new_xs;
  new_xs[N-1] = new_s.value();
  for (size_t i = 0; i < N-1; i++){new_xs[i] = current_slice[i];}
  
  next_expected = new_xs;

  new_xs = to_x_space(new_xs);
  return std::optional<std::array<F,N>>(new_xs);
  
}

template<typename F, size_t N>
InvertingPolynomialReconstructor<F> PeraroFunction<F,N>::new_known_denominator_reconstructor() const{
  return InvertingPolynomialReconstructor<F>(x_gens[N-1], degrees.numerator_total, reconstruction_position.second);
}

template<typename F, size_t N>
InvertingRationalReconstructor<F> PeraroFunction<F,N>::new_known_degree_reconstructor() const{
  return InvertingRationalReconstructor<F>(x_gens[N-1], degrees.numerator_total, degrees.denominator_total);
}

template<typename F, size_t N>
ThieleFunction<F> PeraroFunction<F,N>::new_preparation_reconstructor(){
  // If working out variable degrees
  if (compute_individual_degrees && degrees.numerator_univariate.size() < N){
    current_slice.fill(F(0));
    projective_slice = degrees.numerator_univariate.size();
  }
  else{
    current_slice = denominator_coordinates.at(0).next_point().value();
    projective_slice = N-1;
  }
  
  return ThieleFunction<F>(x_gens[N-1]);
}

template<typename F, size_t N>
std::vector<std::array<F,N>> PeraroFunction<F,N>::guess_points(size_t n) const{
    auto copy = clone_no_data();
    std::vector<std::array<F,N>> results;

    for (size_t i = 0; i < n; i++){
	auto new_point = copy.next_point();
	if (!new_point){break;}
	results.push_back(new_point.value());
    }

    return results;
}

template<typename F, size_t N>
std::function<F(std::array<F,N>)> PeraroFunction<F,N>::prepare_function(const std::function<F(std::array<F,N>)>& f){
    // Find shift if we don't have one yet.
    if (!shift){ shift = find_shift(f); }

    return peraro_shift_function(f, shift.value());
}


template<typename F, size_t N>
PeraroFunction<F,N>::PeraroFunction(std::array<std::function<F()>,N> _x_gens, bool _compute_individual_degrees)
  : compute_individual_degrees(_compute_individual_degrees),
    x_gens(_x_gens),
    preparation_reconstructor(_x_gens[N-1]),
    known_denominator_reconstructor(_x_gens[N-1], 0, 0),
    known_degree_reconstructor(_x_gens[N-1], 0, 0){
    using std::array; using std::function;

    array<function<F()>,N-1> t_gens;
    for (size_t i = 0; i < N-1; i++){t_gens[i] = x_gens[i];}

    // Add the first denominator component (only guaranteed non-zero function)
    MultivariateNewtonFunction<F,N-1> denominator_constant(t_gens, 0);
    denominator_coordinates.push_back(denominator_constant);

    // FIXME. Shouldn't need to do this twice. If we try to do the total
    // rank before the individuals, this will naturally be fine.
    preparation_reconstructor = new_preparation_reconstructor();

}

template<typename F, size_t N>
Ratio<SparseMultivariatePolynomial<F>> PeraroFunction<F,N>::to_canonical(){
    if (!finished){
        throw std::runtime_error("ERROR in PeraroFunction::to_canonical(): Attempted to canoncalized unfinished reconstruction.");
    }

    // Each component of either the numerator or denominator, when one
    // puts the appropriate power of x_N back in, becomes a
    // homogeneous polynomial. In this way, we very naturally, and in
    // order build up a new sparse polynomial by homogenization.

    using std::vector; 

    auto undo_rescaling = [](SparseMultivariatePolynomial<F> poly){

      SparseMultivariatePolynomial<F> result;
      for (auto& poly_term : poly.polynomials_by_degree){
        for (auto& term : poly_term){
          auto monomial = term.first;
          auto& coefficient = term.second;

          size_t degree = monomial.back();
          size_t denom_degree = std::accumulate(monomial.begin(), monomial.end() - 1, 0);
          if (denom_degree > degree){
            throw std::runtime_error("ERROR: When undoing the rescaling, map generated a denominator.");
          }

          monomial.back() -= denom_degree;

          result += PolynomialTerm<F>(monomial, coefficient);
        }
      }

      return result;
    };

    auto canonicalize = [undo_rescaling, shift = shift.value(), rescale =  rescale](const vector<MultivariateNewtonFunction<F,N-1>>& polynomials){

      SparseMultivariatePolynomial<F> unshifted;

      for (size_t i = 0; i < polynomials.size(); i++){
        // Polynomial in N-1 variables
        SparseMultivariatePolynomial<F> poly_coefficient = polynomials.at(i).to_canonical();

        // Multiply by t^i.
        poly_coefficient.add_variable(i);

        // If using rescaling then rescale (roughly action of homogenize)
        if (rescale){
            unshifted += undo_rescaling(poly_coefficient);
        }
        else{
            unshifted += poly_coefficient;
        }
      }

      unshifted.shift(shift);

      return unshifted;
    };

    auto unnormed_numerator = canonicalize(numerator_coordinates);
    auto unnormed_denominator = canonicalize(denominator_coordinates);

    auto leading_term_coeff = [&unnormed_denominator](){
      for (auto& homo_poly : unnormed_denominator.polynomials_by_degree){
	for (auto& term : homo_poly){
	  if (term.second != F(0)){return term.second;}
	}
      }

      return F(0); // Cause a failure if we get here. Should never happen.
    }();

    unnormed_numerator /= leading_term_coeff;
    unnormed_denominator /= leading_term_coeff;

    unnormed_numerator.variable_names = variable_names;
    unnormed_denominator.variable_names = variable_names;
    
    return Ratio<SparseMultivariatePolynomial<F>>(unnormed_numerator, unnormed_denominator); 
}

template<typename F>
PeraroFunction<F,1>::PeraroFunction(std::function<F(std::array<F,1>)> f, std::array<std::function<F()>,1> x_gens)
  : univariate_f([&](F x){
      std::array<F,1> xs {{x}};
    return f(xs);
  }, x_gens[0]){}

template<typename F>
Ratio<SparseMultivariatePolynomial<F>> PeraroFunction<F,1>::to_canonical(){
  auto dense_uni = univariate_f.to_canonical();
  SparseMultivariatePolynomial<F> num(dense_uni.numerator);
  SparseMultivariatePolynomial<F> denom(dense_uni.denominator);
  Ratio<SparseMultivariatePolynomial<F>> result(num,denom);
  return result;
}

template<typename F, size_t N>  
VectorPeraroFunction<F,N> VectorPeraroFunction<F,N>::clone_no_data() const{
  // TODO: Actually implement this properly, to avoid huge copies.
  auto copy = (*this);
  copy.simulation = true;
  for (auto& element : copy.components){element.simulation = true;}
  return copy;
}

template<typename F, size_t N>
std::vector<std::array<F,N>> VectorPeraroFunction<F,N>::guess_points(size_t n) const{
    auto copy = clone_no_data();
    std::vector<std::array<F,N>> results;

    for (size_t i = 0; i < n; i++){
	auto new_point = copy.next_point();
	if (!new_point){break;}
	results.push_back(new_point.value());
    }

    return results;
}

  template<typename F, size_t N>
  void VectorPeraroFunction<F,N>::populate_components(size_t dimension){
    proposals.resize(dimension, std::optional<std::array<F,N>>());

    for (size_t i = 0; i < dimension; i++){
      PeraroFunction<F,N> component(x_gens, compute_individual_degrees);
      component.point_transform = point_transform;
      component.verbose = verbose;
      component.shift = shift;
      component.name = "<" + std::to_string(i) + ">: ";
      components.push_back(component);
    }
  }


template<typename F, size_t N>
std::vector<std::function<std::vector<F>(std::array<F,N>)>> VectorPeraroFunction<F,N>::prepare_function(const std::vector<std::function<std::vector<F>(std::array<F,N>)>>& fs){
  // We build an auxiliary PeraroFunction and delegate to its find
  // shift method. The results of the find shift allow us to correctly
  // populate all components (even if we don't use the shift)

  size_t dimension;

  std::function<F(std::array<F,N>)> dimension_saving_f = [&](std::array<F,N> xs){
    auto f_val = fs.front()(xs);
    dimension = f_val.size();
    return f_val.at(0);
  };

  PeraroFunction<F,N> auxiliary(x_gens, compute_individual_degrees);
  auxiliary.verbose = verbose;
  auto new_shift = auxiliary.find_shift(dimension_saving_f);
  if (!shift){shift = new_shift;}

  populate_components(dimension);

  std::vector<std::function<std::vector<F>(std::array<F,N>)>> results;
  for (auto& f : fs){results.push_back(peraro_shift_function(f, shift.value()));}

  return results;
}

template<typename F, size_t N>
std::function<std::vector<F>(std::array<F,N>)> VectorPeraroFunction<F,N>::prepare_function(const std::function<std::vector<F>(std::array<F,N>)>& f){
  // We build an auxiliary PeraroFunction and delegate to its find
  // shift method. The results of the find shift allow us to correctly
  // populate all components (even if we don't use the shift)

  size_t dimension;

  std::function<F(std::array<F,N>)> dimension_saving_f = [&](std::array<F,N> xs){
    auto f_val = f(xs);
    dimension = f_val.size();
    return f_val.at(0);
  };

  PeraroFunction<F,N> auxiliary(x_gens, compute_individual_degrees);
  auxiliary.verbose = verbose;
  auto new_shift = auxiliary.find_shift(dimension_saving_f);
  if (!shift){shift = new_shift;}

  populate_components(dimension);

  return peraro_shift_function(f, shift.value());
}

template<typename F, size_t N>
std::optional<std::array<F,N>> VectorPeraroFunction<F,N>::next_point(){
    using std::array; using std::vector;

    //if (!simulation){
    //  for (auto& c : components){ 
    //      if (!c.finished){ std::cout << c.name <<": " <<c.get_current_position() << std::endl; }
    //  }
    //}

 label1:

    if (finished){return {};}

    for (size_t i = 0; i < components.size(); i++){
        if (!proposals.at(i)){
                proposals.at(i) = components.at(i).next_point();
        }
    }

    // If nobody proposes anything, we're done.
    if (std::all_of(proposals.begin(), proposals.end(), [](auto x){return !x;})){
      finished = true;
      return {};
    }

    // Here we are guaranteed that there is at least one proposal
    size_t earliest_pos = 0;
    for (size_t i = 1; i < components.size(); i++){
        // Non-empty beats empty.
        if (!proposals.at(i)){continue;}
        if (!proposals.at(earliest_pos) && proposals.at(i)){
            earliest_pos = i;
            continue;
        }

        // Preparation beats non preparation
        auto earliest_stage = components.at(earliest_pos).stage;
        auto this_stage = components.at(i).stage;
        auto PREPARATION = PeraroFunction<F,N>::Stage::PREPARATION;
        if (earliest_stage == PREPARATION && this_stage != PREPARATION){continue;}
        if (earliest_stage != PREPARATION && this_stage == PREPARATION){
            earliest_pos = i; continue;
        }
        
        // Order the preparation steps.
        if (earliest_stage == PREPARATION && this_stage == PREPARATION){
              auto this_projective = components.at(i).projective_slice;
              auto earliest_projective = components.at(earliest_pos).projective_slice;
              if (this_projective < earliest_projective){earliest_pos = i; continue;}
              if (this_projective > earliest_projective){continue;}

              // Otherwise same stage, just different positions.
              auto this_slice_pos = components.at(i).slice_position();
              auto earliest_slice_pos = components.at(earliest_pos).slice_position();

              if (this_slice_pos < earliest_slice_pos){earliest_pos = i; continue;}
              if (this_slice_pos > earliest_slice_pos){continue;}

              // Otherwise, genuinely at the same point, so either as
              // good as the other.
              continue;
        }
        
        // Otherwise standard ordering.
        auto earliest_current_pos = components.at(earliest_pos).get_current_position();
        auto this_current_pos = components.at(i).get_current_position();

        // If this is earlier than current then current = this
        for (size_t j = 0; j < N; j++){
            if (this_current_pos[j] < earliest_current_pos[j]){earliest_pos = i; break;}
            if (this_current_pos[j] > earliest_current_pos[j]){break;}
        }
    }

    auto earliest = proposals.at(earliest_pos);
    
    last_point = earliest.value();
    //if (!simulation){std::cout << "Earliest = " << earliest_pos << std::endl << std::endl;}

    std::array<F,N> returned_point = earliest.value();

    // If we are simulating then assume that we get the point, so clear
    // the proposals
    if (simulation){
        for (auto& proposal : proposals){
            if (proposal && proposal.value() == returned_point){
                proposal = std::optional<std::array<F,N>>{};
            }
        }
    }

    // If we already know this point, then grab result from cache, and
    // ask for the next point
    {
      auto cached_val = function_cache.find(returned_point);
      if (cached_val != function_cache.end()){
        if (!simulation){add_point(cached_val -> second, returned_point);}
        goto label1;
      }
    }

    return returned_point;
}

template<typename F, size_t N>
void VectorPeraroFunction<F,N>::add_point(const std::vector<F>& f_val, const std::array<F,N>& x_val){

  function_cache[x_val] = f_val;

  // Add the point to anyone who needed it. 
  bool all_finished = true;
  for (size_t i = 0; i < components.size(); i++){
	if (proposals.at(i) && proposals.at(i).value() == x_val){
            components.at(i).add_point(f_val.at(i), x_val);
            // Proposal is no longer current, so remove
            proposals.at(i) = std::optional<std::array<F,N>>{};
	}
      if (!components.at(i).finished){all_finished = false;}
  }

  if (all_finished){finished = true;}

}


template<typename F, size_t N>
VectorPeraroFunction<F,N>::VectorPeraroFunction(std::array<std::function<F()>,N>
_x_gens, bool _compute_individual_degrees) : x_gens(_x_gens), compute_individual_degrees(_compute_individual_degrees){
  // Initialize the canonical orders with the first point
  for (size_t i = 0; i < N; i++){
    canonical_orders[i].push_back(_x_gens[i]());
  }
}

template<typename F, size_t N>
std::vector<Ratio<SparseMultivariatePolynomial<F>>> VectorPeraroFunction<F,N>::to_canonical() {
  if (!finished){
    throw std::runtime_error("ERROR in VectorPeraroFunction::to_canonical(): Attempted to canoncalized unfinished reconstruction.");
  }

  std::vector<Ratio<SparseMultivariatePolynomial<F>>> result;

  for (auto& component : components){
    component.variable_names = variable_names;
    result.push_back(component.to_canonical());
  }

  return result;
}

}
}
