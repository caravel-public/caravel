#ifndef FUNCTIONAL_RECONSTRUCTION_H_INC
#define FUNCTIONAL_RECONSTRUCTION_H_INC

#include <functional>
#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

#include "Core/Utilities.h"
#include "Core/InversionStrategies.h"
#include "AmplitudeCoefficients/NumericalTools.h"

#include "FunctionalReconstruction/DensePolynomial.h"
#include "FunctionalReconstruction/ThieleFunction.h"
#include "FunctionalReconstruction/DenseRational.h"
#include "FunctionalReconstruction/SparseMultivariatePolynomial.h"


namespace Caravel{

template<typename F>
Ratio<int> guess_rational(F x){
  using std::floor;
  using std::abs;
  
  bool negative = x < 0;
  if (negative) x = -x;

  std::vector<int> cumulants;
  
  while(true){

    auto x_integer = detail::convert_to_int(floor(x));
      cumulants.push_back(x_integer);

      auto next_x_inv = x - x_integer;
      if (abs(next_x_inv) < 1e-7){break;}

      x = 1/next_x_inv;
  }

  int numerator = cumulants.back();
  int denominator = 1;
  
  for (int i = cumulants.size() -2; i >= 0; i--){

    int temp = denominator;
    denominator = numerator;
    numerator = cumulants.at(i)*numerator + temp;
  }

  Ratio<int> result((negative) ? -numerator : numerator, denominator);
  return result;
}

}

#include "FunctionalReconstruction/UnivariateReconstruction.h"
#include "FunctionalReconstruction/NewtonFunction.h"
#include "FunctionalReconstruction/MultivariateNewtonFunction.h"

#ifdef USE_FINITE_FIELDS
#include "FunctionalReconstruction/PeraroFunction.h"
#endif //USE_FINITE_FIELDS


// Some specializations to speed up compilation
#ifdef USE_FINITE_FIELDS
#include "Core/typedefs.h"
namespace Caravel{
  namespace Reconstruction{
  extern template class PeraroFunction<F32,4>;
  extern template class PeraroFunction<F32,3>;
  extern template class PeraroFunction<F32,2>;
  //extern template class PeraroFunction<F32,1>;

  extern template class VectorPeraroFunction<F32,4>;
  extern template class VectorPeraroFunction<F32,3>;
  extern template class VectorPeraroFunction<F32,2>;
  //extern template class VectorPeraroFunction<F32,1>;
  }

  extern template class DensePolynomial<F32>;
}
#endif


#endif //FUNCTIONAL_RECONSTRUCTION_H_INC
