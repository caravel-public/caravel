#include "ReconstructionTools.h"

#include <algorithm>

namespace Caravel {

std::string mathematica_brackets(std::string s) {
    std::replace(s.begin(), s.end(), '<', '[');
    std::replace(s.begin(), s.end(), '>', ']');
    std::replace(s.begin(), s.end(), '(', '[');
    std::replace(s.begin(), s.end(), ')', ']');

    return s;
}

void output_to_file(const Integrals::NumericFunctionBasisExpansion<Series<BigRat>, std::string>& nfbe, const std::string& filename) {

    // Determine the range of the powers in epsilon.
    auto eps_min = nfbe.leading();
    auto eps_max = nfbe.last();

    // Create the string for the output.
    std::ostringstream output;

    // Loop over all the powers and for each power create an expression
    for (int eps_power = eps_min; eps_power <= eps_max; ++eps_power) {
        std::ostringstream oss;

        oss << "\neps" << ((eps_power < 0) ? "M" : "") << abs(eps_power) << " = ";

        // Get all the basis functions that contribute to the given power in epsilon
        auto funcs = nfbe.get_eps_pow_coeff(eps_power);
        // Loop over all the functions
        for (const auto& entry : funcs) {
            if (entry.second != BigRat(0)) oss << entry.second << " * " << mathematica_brackets(entry.first) << " + \n";
        }

        output << oss.str() << 0 << ";";
    }

    std::ofstream outfile(filename);
    if (outfile.good()) {
        outfile << output.str();
    } else {
        throw std::runtime_error("Failed to write amplitude to file!");
    }

    outfile.close();
}

// ---------------------------------------------------------------------------
// ---------------------- Explicit instantiations ----------------------------
// ---------------------------------------------------------------------------
#define INSTANTIATE_INPUT_PROVIDER(T, L)                                                                                                                       \
    template class ReconstructionInputProvider<T, L>;                                                                                                          \
    template class ReconstructionInputEvaluator<T, L>;                                                                                                         \
    template void output_to_file(const Integrals::NumericFunctionBasisExpansion<Series<Ratio<DensePolynomial<T>>>, std::string>& nfbe,                         \
                                 const std::string& filename);

INSTANTIATE_INPUT_PROVIDER(F32, 2)

} // namespace Caravel
