/**
 * @file CoefficientReconstructionAid.h
 *
 * @date 26.09.2019
 *
 * @brief Helper for interfacing master-integral coefficients to reconstruction tools
 *
 * This file contain a series of functions which allow to flatten and unflatten 
 * information from master-integral coefficients from IntegrandHierarchy, given 
 * corresponding information from warmup files
 */

#pragma once

#include <string>
#include <utility>
#include <vector>

#include "Core/type_traits_extra.h"
#include "Core/settings.h"
#include "DensePolynomial.h"
#include "DenseRational.h"

namespace Caravel {
namespace Reconstruction {

template <typename T, typename CType>
std::enable_if_t< std::is_same<CType, DenseRational<T>>::value >
add_entries_from_coefficient(std::vector<T>& flat, const CType& coeff, const int& ep_power, size_t n_entries_per_coeff){
    // though not required, good to let the user know that we assume here n_entries_per_coeff should be 1
    assert(n_entries_per_coeff == 1);
    assert(ep_power >= 0);
    assert(settings::IntegrandHierarchy::number_of_Ds_values >= 1);
    const auto& num = coeff.numerator.coefficients;
    // the "+ number_of_Ds_values - 1" is to account for a possible increase in degree when doing a substitution like Ds=4-2*ep in the coefficients
    for(size_t ii=0; ii<=size_t(ep_power + settings::IntegrandHierarchy::number_of_Ds_values - 1) ; ii++){
        if(ii>=num.size())
            flat.push_back( T(0) );
        else
            flat.push_back( num[ii] );
    }
}

template <typename T, typename CType>
std::enable_if_t< std::is_same<CType, DenseRational<DensePolynomial<T>, T>>::value >
add_entries_from_coefficient(std::vector<T>& flat, const CType& coeff, const int& ep_power, size_t n_entries_per_coeff){
    assert(ep_power >= 0);
    const auto& num = coeff.numerator.coefficients;
    for(size_t ii=0; ii<=size_t(ep_power) ; ii++){
        if(ii>=num.size())
            for(size_t jj=0; jj<n_entries_per_coeff; jj++) flat.push_back( T(0) );
        else{
            const auto& entries = num[ii].coefficients;
            for(size_t jj=0; jj<n_entries_per_coeff; jj++){
                if(jj>=entries.size())
                    flat.push_back( T(0) );
                else
                    flat.push_back( entries[jj] );
            }
        }
    }
}

// meant for output of CoefficientEngine::compute_coefficients_full_Ds()
template <typename T, typename CType>
std::enable_if_t<std::is_same<CType, std::vector<DenseRational<T>>>::value> add_entries_from_coefficient(std::vector<T>& flat, const CType& coeff,
                                                                                                         const int& ep_power, size_t n_Ds_functions) {
    assert(n_Ds_functions > 0);
    assert(ep_power >= 0);
    // each Ds coefficient
    for (size_t jj = 0; jj < n_Ds_functions; jj++) {
        const auto& num = coeff[jj].numerator.coefficients;
        // unwind rational eps dependence
        for (size_t ii = 0; ii <= size_t(ep_power); ii++) {
            if (ii >= num.size())
                flat.push_back(T(0));
            else
                flat.push_back(num[ii]);
        }
    }
}

/**
 * Simple function to flatten master-integral coefficients given information (assumed coming from corresponding 'load_warmups(warmup_filename)' call).
 * The output is meant to be used for reconstruction algorithms like VectorThiele or VectorPeraro (through properly-constructed lambda).
 *
 * The type T represents the numerical type used for the evaluation of the corresponding functions, and CType the type of the given master-integral
 * coefficient.
 *
 * @param coefficients produced by a get_master_coefficients(.)[0] or get_master_coefficients_full_Ds(.)[0] call
 * @param ep power of numerators (as in third entry of the HierarchyWarmupInfo tuple)
 * @param # of entry per ep coefficient (e.g. get_master_coefficients(.) result is one (and then ignored), for get_master_coefficients_full_Ds(.) is settings::IntegrandHierarchy::number_of_Ds_values)
 *
 * @result a vector containing only the numerator information of the integral coeffs (denominators are assumed to be constant and retrieved from load_warmups(warmup_filename))
 */
template <typename T, typename CType>
std::vector<T> flatten_mi_coefficients(const std::vector<CType>& result, const std::vector<int>& ep_power_numerators, size_t n_entries_per_coeff = 1){
    assert(result.size() == ep_power_numerators.size());

    std::vector<T> toret;

    for(size_t ii = 0; ii < result.size(); ii++)
        add_entries_from_coefficient<T, CType>( toret, result[ii], ep_power_numerators[ii], n_entries_per_coeff );

    return toret;
}

/**
 * Function to recover master-integral coefficients reconstructed with the aid of the function flatten_mi_coefficients(.), using
 * provided information (assumed coming from corresponding 'load_warmups(warmup_filename)' call).
 *
 * This function is meant for coeffs with only ep dependence (as produced by get_master_coefficients(.)[0]). For full Ds dependence
 * use the function recover_mi_coefficients_full_Ds
 *
 * @param vector of reconstructed functions
 * @param ep power of numerators (as in third entry of the HierarchyWarmupInfo tuple)
 * @param denominator information (as in second entry of the HierarchyWarmupInfo tuple) 
 * @param string for output dense function with ep_power_numerators (default "ep")
 *
 * @result a vector containing analytic form of master-integral coeffs with full "ep" dependence
 */
template <typename T, typename InType>
std::vector< DenseRational<InType, T> > recover_mi_coefficients(const std::vector<InType>& result, const std::vector<int>& ep_power_numerators, const std::vector<DensePolynomial<Ratio<int>>>& denominators, const std::string& var = "ep" ){
    assert(ep_power_numerators.size() == denominators.size());
    assert(settings::IntegrandHierarchy::number_of_Ds_values >= 1);
    std::vector< DenseRational<InType, T> > toret(ep_power_numerators.size());

    size_t entry(0);
    for(size_t ii = 0; ii<ep_power_numerators.size(); ii++){
        assert(ep_power_numerators[ii]>=0);

        std::vector<InType> vnumerator;
        // the "+ number_of_Ds_values - 1" is to account for a possible increase in degree when doing a substitution like Ds=4-2*ep in the coefficients
        for(size_t jj = 0; jj <= size_t(ep_power_numerators[ii] + settings::IntegrandHierarchy::number_of_Ds_values - 1); jj++)
            vnumerator.push_back( result[entry++] );

        std::vector<T> vdenominator;
        for(auto& q: denominators[ii].coefficients)    vdenominator.push_back( T(q.numerator)/T(q.denominator) );

        toret[ii] = DenseRational<InType, T>( DensePolynomial<InType, T>(vnumerator, var), DensePolynomial<T>(vdenominator, var) );
    }

    return toret;
}
/**
 * FIXME: this is a dirty hack forwarding
 */
template <typename T, typename InType>
std::vector< DenseRational<InType, T> > recover_mi_coefficients(const std::vector<InType>& result, const std::vector<int>& ep_power_numerators, const std::vector<DensePolynomial<BigRat>>& denominators, const std::string& var = "ep" ){
    std::vector<DensePolynomial<Ratio<int>>> denominator_int;
    denominator_int.reserve(denominators.size());

    for(auto& di :denominators){
        std::vector<Ratio<int>> coeffs;
        for(auto& ci: di.coefficients){
            coeffs.emplace_back((int)ci.num(), (int)ci.den());
        }
        denominator_int.emplace_back(coeffs, di.var_name);
    }

    return recover_mi_coefficients<T>(result, ep_power_numerators, denominator_int, var);
}

/**
 * Function to recover master-integral coefficients reconstructed with the aid of the function flatten_mi_coefficients(.), using
 * provided information (assumed coming from corresponding 'load_warmups(warmup_filename)' call).
 *
 * This function is meant for coeffs with Ds and ep dependence (as produced by get_master_coefficients_full_Ds(.)[0]). For only ep
 * dependence use the function recover_mi_coefficients
 *
 * @param vector of reconstructed functions
 * @param ep power of numerators (as in third entry of the HierarchyWarmupInfo tuple)
 * @param denominator information (as in second entry of the HierarchyWarmupInfo tuple) 
 * @param size_t for how many Ds powers are included
 * @param string for output dense-rational function with ep_power_numerators (default "ep")
 * @param string for output dense-polynomial function (default "Ds")
 *
 * @result a vector containing analytic form of master-integral coeffs with full "ep" and "Ds" dependence
 */
template <typename T, typename InType>
std::vector< DenseRational< DensePolynomial<InType, T>, T> > recover_mi_coefficients_full_Ds(const std::vector<InType>& result, const std::vector<int>& ep_power_numerators, const std::vector<DensePolynomial<Ratio<int>>>& denominators,
                                                                        const size_t& n_entries_per_coeff = 1, const std::string& var_ep = "ep", const std::string& var_Ds = "Ds" ){
    // make it fail for scalar
    assert(n_entries_per_coeff>1);
    assert(ep_power_numerators.size() == denominators.size());
    std::vector< DenseRational< DensePolynomial<InType, T>, T> > toret(ep_power_numerators.size());

    size_t entry(0);
    for(size_t ii = 0; ii<ep_power_numerators.size(); ii++){
        assert(ep_power_numerators[ii]>=0);

        std::vector<DensePolynomial<InType, T>> vnumerator;
        for(size_t jj = 0; jj <= size_t(ep_power_numerators[ii]); jj++){
            std::vector<InType> Dscoeffs;
            for(size_t kk = 0; kk < n_entries_per_coeff; kk++)
                Dscoeffs.push_back( result[entry++] );
            vnumerator.push_back( DensePolynomial<InType, T>( Dscoeffs, var_Ds )  );
        }

        std::vector<T> vdenominator;
        for(auto& q: denominators[ii].coefficients)    vdenominator.push_back( T(q.numerator)/T(q.denominator) );

        toret[ii] = DenseRational< DensePolynomial<InType, T>, T>( DensePolynomial< DensePolynomial<InType, T>, T>(vnumerator, var_ep), DensePolynomial<T>(vdenominator, var_ep) );
    }

    return toret;
}

/**
 * Function to recover master-integral coefficients reconstructed with the aid of the function flatten_mi_coefficients(.), using
 * provided information (assumed coming from corresponding 'load_warmups(warmup_filename)' call). 
 *
 * This function is meant for coeffs with Ds and ep dependence as produced by CoefficientEngine::compute_coefficients_full_Ds(). 
 *
 * @param vector of reconstructed functions
 * @param ep power of numerators (as in third entry of the HierarchyWarmupInfo tuple)
 * @param denominator information (as in second entry of the HierarchyWarmupInfo tuple) 
 * @param size_t for how many coefficients of Ds functions are included
 * @param string for output dense-rational function with ep_power_numerators (default "ep")
 *
 * @result a vector (entry corresponds to master integral) containing vectors of analytic form in "ep" for the coefficients of each Ds-dependence function in CoefficientEngine::get_Ds_functions()
 */
template <typename T, typename InType>
std::vector<std::vector<DenseRational<InType, T>>> recover_mi_coefficients_Ds_functions(const std::vector<InType>& result,
                                                                                        const std::vector<int>& ep_power_numerators,
                                                                                        const std::vector<DensePolynomial<BigRat>>& denominators,
                                                                                        const size_t& n_Ds_functions, const std::string& var_ep = "ep") {
    assert(ep_power_numerators.size() == denominators.size());
    std::vector<std::vector<DenseRational<InType, T>>> toret(ep_power_numerators.size());

    size_t entry(0);
    for (size_t mi = 0; mi < ep_power_numerators.size(); mi++) {
        assert(ep_power_numerators[mi] >= 0);

        auto& Ds_function_coeffs_vector = toret[mi];
        Ds_function_coeffs_vector = std::vector<DenseRational<InType, T>>(n_Ds_functions);

        // same denominator for all Ds function coefficients
        std::vector<T> vdenominator;
        for (auto& q : denominators[mi].coefficients) vdenominator.push_back(T(q.num()) / T(q.den()));

        for (size_t Ds_entry = 0; Ds_entry < n_Ds_functions; Ds_entry++) {

            std::vector<InType> vnumerator(ep_power_numerators[mi] + 1);
            for (size_t ep_power = 0; ep_power <= size_t(ep_power_numerators[mi]); ep_power++) { vnumerator[ep_power] = result[entry++]; }

            Ds_function_coeffs_vector[Ds_entry] =
                DenseRational<InType, T>(DensePolynomial<InType, T>(vnumerator, var_ep), DensePolynomial<T>(vdenominator, var_ep));
        }
    }

    return toret;
}


/**
 * FIXME: this is a dirty hack forwarding
 */
template <typename T, typename InType>
std::vector< DenseRational< DensePolynomial<InType, T>, T> > recover_mi_coefficients_full_Ds(const std::vector<InType>& result, const std::vector<int>& ep_power_numerators, const std::vector<DensePolynomial<BigRat>>& denominators,
                                                                        const size_t& n_entries_per_coeff = 1, const std::string& var_ep = "ep", const std::string& var_Ds = "Ds" ){
    std::vector<DensePolynomial<Ratio<int>>> denominator_int;
    denominator_int.reserve(denominators.size());

    for(auto& di :denominators){
        std::vector<Ratio<int>> coeffs;
        for(auto& ci: di.coefficients){
            coeffs.emplace_back((int)ci.num(), (int)ci.den());
        }
        denominator_int.emplace_back(coeffs, di.var_name);
    }

    return recover_mi_coefficients_full_Ds<T>(result, ep_power_numerators, denominator_int,n_entries_per_coeff,  var_ep, var_Ds);
}

} // namespace Reconstruction
} // namespace Caravel

