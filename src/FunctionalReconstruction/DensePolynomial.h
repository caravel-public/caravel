#ifndef DENSE_POLYNOMIAL_H_INC
#define DENSE_POLYNOMIAL_H_INC

#include "Core/InversionStrategies.h"
#include "Core/Vec.h"
#include "Core/type_traits_extra.h"
#include "Core/MathOutput.h"
#include "Core/MathInterface.h"
#include "misc/MiscMath.h"
#include "Core/InputParser.h"

#include <initializer_list>
#include <vector>
#include <numeric>

namespace Caravel {

template <typename FF, typename FX = FF> class DensePolynomial;

template<typename>
struct is_dense_polynomial : std::false_type {};

template<typename FF, typename FX>
struct is_dense_polynomial<DensePolynomial<FF, FX>> : std::true_type {};

template <typename FF, typename FX = FF> class DenseRational;

template<typename>
struct is_dense_rational : std::false_type {};

template<typename FF, typename FX>
struct is_dense_rational<DenseRational<FF, FX>> : std::true_type {};

/**
 * Class representing densely packed univariate
 * polynomial. The variable lives the field FX. Coefficients (and
 * therefore the function) live in the type FF which is a vector
 * space over FX.
 */
template <typename FF, typename FX> class DensePolynomial {
  public:
    std::vector<FF> coefficients{0};
    std::string var_name{"x"};

    DensePolynomial() = default;
    DensePolynomial(const DensePolynomial&) = default;
    DensePolynomial(DensePolynomial&&) = default;

    DensePolynomial& operator= (const DensePolynomial&) = default;
    DensePolynomial& operator= (DensePolynomial&&) = default;

    // Add this constructor because Series needs to do T(0) to get the zero element.
    DensePolynomial(int z) : coefficients{static_cast<FF>(z)} {}
    explicit DensePolynomial(std::string _var_name) : var_name(_var_name){};
    DensePolynomial(const FF& z, std::string _var_name) : coefficients{z}, var_name(_var_name) {}
    DensePolynomial(FF&& z, std::string _var_name) : coefficients{std::move(z)}, var_name(_var_name) {}
    DensePolynomial(const std::vector<FF>& _coefficients) : coefficients(_coefficients) { check_degree(); }
    DensePolynomial(std::vector<FF>&& _coefficients) : coefficients(_coefficients) { check_degree(); }

    DensePolynomial(const std::vector<FF>& _coefficients, std::string _var_name) : coefficients(_coefficients), var_name(_var_name) { check_degree(); }
    DensePolynomial(std::vector<FF>&& _coefficients, std::string&& _var_name) : coefficients(_coefficients), var_name(_var_name) { check_degree(); }

    DensePolynomial(std::initializer_list<FF>&& l, std::string _var_name = "x") : coefficients(l), var_name(_var_name) { check_degree(); }
    /**
     * MathList interface
     *
     * input format:
     * DensePolynomial[var_name, Coefficients[...]]
     */
    DensePolynomial(const MathList&);

    /**
     * Copy constructor, but also the conversion from other type constructor.
     */
    template <typename FF2, typename FX2> DensePolynomial(const DensePolynomial<FF2, FX2>& other);

    // Returns a polynomial f(x) for given values f(x1) = fs1, ..., f(xN) = fsN
    DensePolynomial(std::vector<FF> fs, std::vector<FX> xs, std::string _var_name = "x", size_t min_power = 0);

    FF operator()(FX x) const;

    // We do this explicitly to improve cache locality
    DensePolynomial<FF, FX> multiply_with_linear(FX x);

    DensePolynomial<FF, FX> operator-() const;

    DensePolynomial<FF, FX>& operator+=(const DensePolynomial<FF, FX>& p);
    DensePolynomial<FF, FX>& operator-=(const DensePolynomial<FF, FX>& p);
    DensePolynomial<FF, FX>& operator*=(const DensePolynomial<FF, FX>& other);
    DensePolynomial<FF, FX>& operator*=(FX x);
    DensePolynomial<FF, FX>& operator/=(FX x);
    DensePolynomial<FF, FX>& operator+=(const FF& p);
    DensePolynomial<FF, FX>& operator-=(const FF& p);

    int get_degree() const { return static_cast<int>(coefficients.size()) - 1; }
    FF get_leading_coefficient() const { return coefficients.back(); }
    bool is_zero() const {
        if (coefficients.size() == 0) {
            std::cerr << "is_zero() called with empty coefficients!" << std::endl;
            std::exit(1);
        }
        FF zero = get_zero();
        if (get_degree() == 0 && coefficients.at(0) == zero) return true;
        return false;
    }
    // FIXME: ill-defined FF(1) constructor for generic FF types, up to the user to judiciously use this method
    bool is_one() const {
        if (get_degree() == 0 && coefficients.at(0) == FF(1)) return true;
        return false;
    }

    DensePolynomial<FF, FX> derivative();

  private:
    // check if leading coefficient became zero and lower the degree if this is the case
    void check_degree() {
        if constexpr (!is_exact<FX>::value) {
            // for floating point this doesn't make sense
            // do nothing
            return;
        }
        // exact cases follow
        else {
            if( coefficients.size() == 0 ) return; //FIXME
            FF zero = get_zero();
            while (coefficients.size() > 1 and coefficients.back() == zero) { coefficients.pop_back(); }
        }
    }
    FF get_zero() const {
        // FIXME: handling of instances with coefficients.size() = 0
        FF zero(0);
        if constexpr (is_dense_rational<FF>::value) { 
            zero = FF{coefficients.at(0).numerator.var_name}; 
        }
        else if constexpr (is_dense_polynomial<FF>::value) {
            zero = FF{coefficients.at(0).var_name};
        }
        else if constexpr (std::is_same<FF, Caravel::Vec<FX>>::value) {
            zero = FF{int(coefficients.at(0).size())};
        }
        return zero;
    }

  public:
    template <typename A> void serialize(A& ar) { ar(var_name, coefficients); }
};

template <typename FF, typename FX> bool operator==(const DensePolynomial<FF, FX>& a, const DensePolynomial<FF, FX>& b);
template <typename FF, typename FX> bool operator!=(const DensePolynomial<FF, FX>& a, const DensePolynomial<FF, FX>& b) { return !(a == b); }

template <typename FF, typename FX> DensePolynomial<FF, FX> operator*(DensePolynomial<FF, FX> p, FX x) { return p *= x; }
template <typename FF, typename FX> DensePolynomial<FF, FX> operator/(DensePolynomial<FF, FX> p, FX x) { return p /= x; }
template <typename FF, typename FX> DensePolynomial<FF, FX> operator*(FX x, DensePolynomial<FF, FX> p) { return p *= x; }
template <typename FF, typename FX> DensePolynomial<FF, FX> operator+(DensePolynomial<FF, FX> p1, const DensePolynomial<FF, FX>& p2) { return p1 += p2; }
template <typename FF, typename FX> DensePolynomial<FF, FX> operator-(DensePolynomial<FF, FX> p1, const DensePolynomial<FF, FX>& p2) { return p1 -= p2; }
template <typename FF, typename FX> DensePolynomial<FF, FX> operator*(DensePolynomial<FF, FX> p1, const DensePolynomial<FF, FX>& p2) { return p1 *= p2; }

template <typename FF, typename FX> DensePolynomial<FF, FX> operator+(DensePolynomial<FF, FX> p1, const FF& p2) { return p1 += p2; }
template <typename FF, typename FX> DensePolynomial<FF, FX> operator-(DensePolynomial<FF, FX> p1, const FF& p2) { return p1 -= p2; }
template <typename FF, typename FX> DensePolynomial<FF, FX> operator+(const FF& p2,DensePolynomial<FF, FX> p1) { return p1 += p2; }
template <typename FF, typename FX> DensePolynomial<FF, FX> operator-(const FF& p2,DensePolynomial<FF, FX> p1) { return p1 -= p2; }

template <typename FFA, typename FXA, typename FFB, typename FXB = FFB>
DensePolynomial<FFB, FXB> fmap(const std::function<FFB(FFA)>& f, const DensePolynomial<FFA, FXA>& x);

template <class FF, class FX> std::ostream& operator<<(std::ostream& out, const DensePolynomial<FF, FX>& p);
template <typename FF, typename FX> std::istream& operator>>(std::istream& in, DensePolynomial<FF,FX>& p);

/**
 * Divide polynomials
 * return pair: {quotient, remainder}
 */
template <typename FF, typename FX>
std::pair<DensePolynomial<FF, FX>, DensePolynomial<FF, FX>> polynomial_divide(const DensePolynomial<FF, FX>& a, const DensePolynomial<FF, FX>& b);

/**
 * Compute greatest common divisor of two polynomials
 */
template <typename FF, typename FX> DensePolynomial<FF, FX> polynomial_GCD(const DensePolynomial<FF, FX>& a, const DensePolynomial<FF, FX>& b) {
    if (a.get_degree() < b.get_degree()) return polynomial_GCD(b, a);
    if (b.is_zero()) {
        if (a.get_degree()==0) return DensePolynomial<FF,FX>{FF{1}, a.var_name}; 
        return a;
    }
    return polynomial_GCD(b, polynomial_divide(a, b).second);
}

/**
 * Compute greatest common divisor of any number polynomials (recursively)
 */
template <typename FF, typename FX, typename... Ts>
DensePolynomial<FF, FX> polynomial_GCD(const DensePolynomial<FF, FX>& a, const DensePolynomial<FF, FX>& b, const DensePolynomial<FF, FX>& c,
                                       const Ts&... rest) {
    return polynomial_GCD(polynomial_GCD(a, b), c, rest...);
}
template <typename FF, typename FX, typename... Ts> DensePolynomial<FF, FX> polynomial_GCD(const std::vector<DensePolynomial<FF, FX>>& a) {
    assert(a.size() >= 2);
    return std::accumulate(a.begin() + 2, a.end(), polynomial_GCD(a[0], a[1]),
                           [](const DensePolynomial<FF, FX>& p1, const DensePolynomial<FF, FX>& p2) { return polynomial_GCD(p1, p2); });
}

/**
 * Compute least common multiple
 */
template <typename FF, typename FX> DensePolynomial<FF, FX> polynomial_LCM(const DensePolynomial<FF, FX>& a, const DensePolynomial<FF, FX>& b) {
    return polynomial_divide(a * b, polynomial_GCD(a, b)).first;
}

template <typename FF, typename FX, typename... Ts> DensePolynomial<FF, FX> polynomial_LCM(const std::vector<DensePolynomial<FF, FX>>& a) {
    assert(a.size() >= 2);
    return std::accumulate(a.begin() + 2, a.end(), polynomial_LCM(a[0], a[1]),
                           [](const DensePolynomial<FF, FX>& p1, const DensePolynomial<FF, FX>& p2) { return polynomial_LCM(p1, p2); });
}

template <class FF, class FX> std::string math_string(const DensePolynomial<FF, FX>&);

} // namespace Caravel

#include "DensePolynomial.hpp"

#endif
