#ifndef PARALLEL_RECONSTRUCTION_H_INC
#define PARALLEL_RECONSTRUCTION_H_INC

#include "FunctionalReconstruction/type_traits.h"
#include <optional>
#include <functional>

namespace Caravel{
namespace Reconstruction{

  /**
   * Helper which does the work of organizing which points to evaluate
   * for in blocks, when the algorithm might not want all of them.
   */
  template<typename Algorithm, typename Y, typename X, typename = std::enable_if<is_reconstruction_algorithm<Algorithm>::value>>
    void parallel_reconstruct_helper(Algorithm& algorithm,
                                     const std::function<std::vector<Y>(const std::vector<X>&)>& evaluator,
                                     size_t num_threads){

        std::optional<X> missed_point;

	size_t num_requested = 0;
	size_t num_used = 0;
	size_t num_repeated = 0;
	size_t num_readded = 0;

	std::unordered_map<X,bool> seen;
	std::unordered_map<X,bool> added;

        while (!algorithm.finished){
            // Compute in batches
            auto potential_points = algorithm.guess_points(num_threads);
	    num_requested += potential_points.size();

	    for (auto& point : potential_points){
		if (seen.find(point) != seen.end()){
		    num_repeated++;
		}
		else{
		    seen[point] = true;
		}
	    }

            // Put any missed point evaluation on the end
            if (missed_point){
                // Make space if necessary
                if (potential_points.size() == num_threads){potential_points.pop_back();}
                potential_points.push_back(missed_point.value());
            }

            // Pass cached_f by reference, to make it update the cache.
            auto potential_fs = evaluator(potential_points);

            // Now add the missed point
            if (missed_point){
                missed_point = std::optional<X>();
                auto point = potential_points.back();
                algorithm.add_point(potential_fs.back(), potential_points.back());

                if (added.find(point) != added.end()){num_readded++;}
                else{added[point] = true;}

		num_used++;
                potential_fs.pop_back(); potential_points.pop_back();
                if (algorithm.finished){break;}
            }

            size_t pos = 0;
            while (true){
                auto maybe_new_xs = algorithm.next_point();
                // All remaining points were in the cache!
                if (!maybe_new_xs){break;}
                auto new_xs = maybe_new_xs.value();
                auto it = std::find(potential_points.begin(), potential_points.end(), new_xs);
                if (it == potential_points.end()){
                    missed_point = std::optional<X>(new_xs);
                    break;
                }
                else{
                    pos = it - potential_points.begin();
                    algorithm.add_point(potential_fs.at(pos), new_xs); 
                    if (added.find(new_xs) != added.end()){num_readded++;}
                    else{added[new_xs] = true;}
		    num_used++;
                    if (algorithm.finished){break;}
                }
            }

        }

	std::cout << "num_requested = " << num_requested << std::endl;
	std::cout << "num_used = " << num_used << std::endl;
	std::cout << "num_repeated = " << num_repeated << std::endl;
	std::cout << "num_readded = " << num_readded << std::endl;

  }
  /**
   * Function which takes a reconstruction algorithm, implementing the
   * correct interface, and the function to be reconstructed and
   * performs the evaluation in parallel. Inteface for threadsafe
   * function.
   *
   * Modifies algorithm in place.
   */
  template<typename Algorithm, typename Y, typename X, typename = std::enable_if<is_reconstruction_algorithm<Algorithm>::value>>
    void parallel_reconstruct(Algorithm& algorithm, const std::function<Y(X)>& f,    size_t num_threads = [](){return std::thread::hardware_concurrency() - 1;}()){
    if (std::thread::hardware_concurrency() == 0){
        std::cerr << "Error: Unable to detect number of threads. " << std::endl;
        exit(1);
    }

    std::function<std::vector<Y>(const std::vector<X>&)> g = 
        [&](const std::vector<X>& in){return parallel_map<Y,X>(f,in);};

    parallel_reconstruct_helper(algorithm, g, num_threads);

  }

  /**
   * Function which takes a reconstruction algorithm, implementing the
   * correct interface, and the function to be reconstructed and
   * performs the evaluation in parallel. Inteface for multiple copies
   * of same function to allow thread safety.
   *
   * Modifies algorithm in place.
   */
  template<typename Algorithm, typename Y, typename X, typename = std::enable_if<is_reconstruction_algorithm<Algorithm>::value>>
    void parallel_reconstruct(Algorithm& algorithm, const std::vector<std::function<Y(X)>>& fs){

    size_t num_threads = fs.size();

    std::function<std::vector<Y>(const std::vector<X>&)> g = [&fs](const std::vector<X>& xs){return parallel_map<Y,X>(fs, xs);};

    parallel_reconstruct_helper(algorithm, g, num_threads);

  }


}
}

#endif 
