#ifndef DENSE_RATIONAL_H_INC
#define DENSE_RATIONAL_H_INC
#include "DensePolynomial.h"
#include "Core/Series.h"
#include "misc/MiscMath.h"
#include "Core/Utilities.h"
#include "Core/MathOutput.h"

#include <vector>
#include <ostream>
#include <cstdlib>

namespace Caravel{

template <class N, class D = N> class Ratio {
  public:
    N numerator;
    D denominator;
    Ratio(int i) : numerator(N(i)), denominator(N(1)) {}

    template <typename U = N> Ratio(N _numerator, typename std::enable_if<std::is_same<U, int>::value>::type* = 0) : numerator(_numerator), denominator(N(1)) {}
    Ratio(N _numerator, D _denominator) : numerator(_numerator), denominator(_denominator) {}

    template <typename... types> auto operator()(types... args) const {
        return numerator(std::forward<types...>(args...)) / denominator(std::forward<types...>(args...));
    }
};

template <class N, class D> bool operator==(const Ratio<N, D>& a, const Ratio<N, D>& b);
template <class N, class D> bool operator!=(const Ratio<N, D>& a, const Ratio<N, D>& b) {return !(a==b);}

template<class N, class D> std::ostream& operator <<(std::ostream& out, const Ratio<N,D>& r);

template<template<class> class P, class F1, class F2> 
Ratio<P<F2>> fmap(const std::function<F2(F1)>& f, const Ratio<P<F1>>& x){
    return Ratio<P<F2>>(fmap(f, x.numerator), fmap(f, x.denominator));
}

/**
 * Class representing densely packed univariate rational function. 
 * The variable lives the field FX. Coefficients of the denominator
 * also live in FX. Coefficients of the numerator (and
 * therefore the function) live in the type FF which is a vector
 * space over FX.
 */
template <typename FF, typename FX>
class DenseRational{
 public:
  DensePolynomial<FF, FX> numerator;
  DensePolynomial<FX> denominator;

 DenseRational(int i, std::string var_name = "x") : numerator({FF(i)}, var_name), denominator({FX(1)}, var_name){}
 DenseRational(std::string var_name = "x") : numerator(var_name), denominator({FX(1)}, var_name){}

  DenseRational(const DensePolynomial<FF, FX>& _numerator, const DensePolynomial<FX>& _denominator) : numerator(_numerator), denominator(_denominator){}
  DenseRational(DensePolynomial<FF, FX>&& _numerator, DensePolynomial<FX>&& _denominator) : numerator(_numerator), denominator(_denominator){}

  DenseRational(const Ratio<DensePolynomial<FF, FX>, DensePolynomial<FX>>& rational) : numerator(rational.numerator), denominator(rational.denominator){}

  template<typename FF2, typename FX2>
  DenseRational(const DenseRational<FF2, FX2>& other) : numerator(other.numerator), denominator(other.denominator){}

  DenseRational<FF, FX>& operator+=(const DenseRational<FF, FX>&);
  DenseRational<FF, FX>& operator-=(const DenseRational<FF, FX>&);

  DenseRational<FF, FX> operator-() const { return DenseRational<FF, FX>(-numerator,denominator);}

  FF operator()(FX x) const {return numerator(x)/denominator(x);}

  template <typename T = FX> std::enable_if_t<std::is_same<FF, T>::value, Series<FF>> taylor_expand_around_zero(int order) const;
  template <typename T = FX> std::enable_if_t<!(std::is_same<FF, T>::value), Series<FF>> taylor_expand_around_zero(int order) const;

  /**
   * Vectors of coefficients in numerator and denominator are concatenated to give a single vector associated to *this
   */
  template <typename T = FX> std::enable_if_t<std::is_same<FF, T>::value, std::vector<FF>> flatten() const;
  template <typename T = FX> std::enable_if_t<!(std::is_same<FF, T>::value), std::vector<FF>> flatten() const;

  public:
    template <typename A> void serialize(A& ar) { ar(numerator, denominator); }
};

template <typename FF, typename FX = FF> bool operator==(const DenseRational<FF, FX>& a, const DenseRational<FF, FX>& b);
template <typename FF, typename FX = FF> bool operator!=(const DenseRational<FF, FX>& a, const DenseRational<FF, FX>& b) { return !(a==b); }

template <typename FF, typename FX = FF>  DenseRational<FF, FX> operator+(DenseRational<FF, FX> a, const DenseRational<FF, FX>& b){return a+=b;}
template <typename FF, typename FX = FF>  DenseRational<FF, FX> operator-(DenseRational<FF, FX> a, const DenseRational<FF, FX>& b){return a+=(-b);}


template<typename FF, typename FX = FF> std::ostream& operator <<(std::ostream& out, const DenseRational<FF, FX>& r);

template<typename F> Ratio<DensePolynomial<F>> fit_univariate_rational(const std::vector<std::pair<F,F>>& coordinates, size_t numerator_rank, size_t denominator_rank);

/**
 * From the input vector reconstructs a DenseRational given the number of numerator and denominator coefficients.
 */
template <typename F> DenseRational<F> unflatten(size_t num_size, size_t den_size,const std::vector<F>& in);

template <typename FF, typename FX> std::string math_string(const DenseRational<FF,FX>&);

/**
 * Substitutes 'polynom' variable according to 'relation'
 */
template <typename FX> DensePolynomial<FX> substitute(const DensePolynomial<FX>& relation, const DensePolynomial<FX>& polynom);
/**
 * Substitutes 'rational' variable according to 'relation'
 */
template <typename FX> DenseRational<FX> substitute(const DensePolynomial<FX>& relation, const DenseRational<FX>& rational);
/**
 * Substitutes most inner variable in 'in' according to 'relation' (checks matching variable names)
 */
template <typename FX> DenseRational<FX> substitute(const DensePolynomial<FX>& relation, const DenseRational<DensePolynomial<FX>, FX>& in);
/**
 * Substitutes most inner variable in 'in' according to 'relation' (checks matching variable names)
 */
template <typename FX> DenseRational<FX> substitute(const DensePolynomial<FX>& relation, const DenseRational<DenseRational<FX>, FX>& in);

}

#include "DenseRational.hpp"

#endif
