#include <algorithm>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>

namespace Caravel {

// ----------------------------------------------------------------------------
// ------------------------- ReconstructionInputEvaluator ---------------------
// ----------------------------------------------------------------------------

// Default constructor
template <typename T, size_t L> ReconstructionInputEvaluator<T, L>::ReconstructionInputEvaluator() = default;

template <typename T, size_t L> ReconstructionInputEvaluator<T, L>::ReconstructionInputEvaluator(ReconstructionInputEvaluator<T, L>&& other) = default;

template <typename T, size_t L> ReconstructionInputEvaluator<T, L>::ReconstructionInputEvaluator(const ReconstructionInputEvaluator<T, L>&) = default;

template <typename T, size_t L>
ReconstructionInputEvaluator<T, L>& ReconstructionInputEvaluator<T, L>::operator=(ReconstructionInputEvaluator<T, L>&& other) = default;

// Call operator
template <typename T, size_t L> std::vector<T> ReconstructionInputEvaluator<T, L>::operator()(const momentumD_configuration<T, 4>& mom_conf) const {

    auto result = function(mom_conf);

    auto mapped_result = basis_map(result);

    // Convert the result into a vector that is being returned.
    std::vector<T> vector_coeffs = map_coefficients_to_vector(mapped_result);

    return vector_coeffs;
}

template <typename T, size_t L> bool ReconstructionInputEvaluator<T, L>::contains(const std::string& name) const {
    for (const auto& entry : function_to_position) {
        if (entry.first.first == name) { return true; }
    }
    return false;
}

template <typename T, size_t L>
std::vector<T>
ReconstructionInputEvaluator<T, L>::map_coefficients_to_vector(const Integrals::NumericFunctionBasisExpansion<Series<T>, std::string>& nfbe) const {

    // The return vector that will store the coefficients
    std::vector<T> coefficients(number_of_coefficients);

    // At 2-loops the minimal power in epsilon is -4, at 1-loop it is -2.
    // int min_power = (L == 2)?-4:-2;

    // Loop over all basis functions and extract their coefficients epsilon series coefficients.
    for (const auto& entry : nfbe) {
        // This is only done if the entry is among the requested functions.
        if (function_names.size() == 0 || std::find(function_names.begin(), function_names.end(), entry.first) != function_names.end()) {
            for (int power = entry.second.leading(); power <= 0; ++power) {
                try {
                    coefficients.at(function_to_position.at({entry.first, power})) = entry.second[power];
                } catch (const std::range_error&) { coefficients.at(function_to_position.at({entry.first, power})) = T(0); }
            }
        }
    }

    return coefficients;
}

// ----------------------------------------------------------------------------
// ------------------------- ReconstructionInputProvider ----------------------
// ----------------------------------------------------------------------------

// Constructor

template <typename T, size_t L>
ReconstructionInputProvider<T, L>::ReconstructionInputProvider(
    std::function<Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<C, C>>(const momD_conf<T, 4>&)> _function,
    Integrals::MapToBasis<T, FType>&& bmap, const momentumD_configuration<T, 4>& moms, const std::vector<std::string>& functions)
    : function(std::move(_function)), basis_map{std::move(bmap)}, function_names{functions} {

    // Make sure that we get the correct type.
    static_assert(is_exact<T>::value, "Provided type needs to be finite field type.");

    // Build the maps.
    // This is done by evaluating the amplitude for the provided point.
    this->operator()(moms);
}

template <typename T, size_t L>
ReconstructionInputProvider<T, L>::ReconstructionInputProvider(
    std::function<Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<C, C>>(const momD_conf<T, 4>&)> _function,
    Integrals::MapToBasis<T, FType>&& bmap, const std::vector<momentumD_configuration<T, 4>>& mom_configs, const std::vector<std::string>& functions)
    : function(std::move(_function)), basis_map{std::move(bmap)}, function_names{functions} {

    // Make sure that we get the correct type.
    static_assert(is_exact<T>::value, "Provided type needs to be finite field type.");

    // We need at least one momentum configurations
    std::size_t n_configs = mom_configs.size();
    if (n_configs == 0) { throw std::runtime_error("Error: Need at least one momentum configuration to evaluate!"); }
    std::cout << "Requiring a coefficient to vanish " << n_configs << " times before discarding it!\n";

    // Count how many times a function's coefficient does not completely vanish.
    std::unordered_map<std::string, std::size_t> non_vanish_counter{};

    for (const auto& config : mom_configs) {
        // Evaluate with the current momentum configuration and map to basis.
        const auto tmp = basis_map(function(config));

        for (const auto& entry : tmp) {
            // If function coefficient is Series with vanishing coeffs set its vanish counter or increase it.
            bool is_non_zero = false;
            for (int i = entry.second.leading(); i <= entry.second.last() && i < 1; ++i) {
                if (entry.second[i] != T(0)) {
                    is_non_zero = true;
                    break;
                }
            }

            if (is_non_zero) {
                if (non_vanish_counter.find(entry.first) == non_vanish_counter.end()) {
                    non_vanish_counter.insert({entry.first, 1});
                } else {
                    non_vanish_counter.at(entry.first) += 1;
                }
            }
        }
    }

    // Add functions whose coefficients vanished `n_configs` times to the veto list
    // (if not already contained)
    unsigned long counter = 0;
    for (const auto& entry : non_vanish_counter) {
        if (entry.second > 0 && std::find(function_names.begin(), function_names.end(), entry.first) == function_names.end()) {
            function_names.push_back(entry.first);
            counter++;
        }
    }
    std::cout << "Found " << counter << " contributing functions!\n";

    // Build the maps.
    // This is done by evaluating the amplitude for the first momentum configuration.
    this->operator()(mom_configs[0]);
}

template <typename T, size_t L> unsigned long ReconstructionInputProvider<T, L>::get_n_coefficients() const { return number_of_coefficients; }

template <typename T, size_t L>
template <typename Coeff>
Integrals::NumericFunctionBasisExpansion<Series<Coeff>, std::string> ReconstructionInputProvider<T, L>::to_basis(const std::vector<Coeff>& coeffs) const {

    std::unordered_map<std::string, Series<Coeff>> toreturn;

    // For each function collect all the coefficients.
    typedef std::map<int, Coeff> maptype;
    std::unordered_map<std::string, maptype> tmp_map;

    for (size_t i = 0; i < coeffs.size(); ++i) {
        auto basis_function = position_to_function.at(i).first;
        auto power = position_to_function.at(i).second;

        // Look for function in the map
        auto search = tmp_map.find(basis_function);
        // If function not yet in the map add it
        if (search == tmp_map.end()) {
            tmp_map.insert({basis_function, maptype({{power, coeffs[i]}})});
        }
        // If it is already tracked, just add the new coefficient.
        else {
            search->second.insert({power, coeffs[i]});
        }
    }

    // Now, we can construct the output
    for (const auto& elem : tmp_map) {
        // The powers in epsilon
        std::vector<int> powers;
        // The coefficients
        std::vector<Coeff> coeffs_ff;

        for (const auto& entry : elem.second) {
            powers.push_back(entry.first);
            coeffs_ff.push_back(entry.second);
        }

        Series<Coeff> s(powers.front(), powers.back(), coeffs_ff);
        toreturn.insert({elem.first, s});
    }

    return Integrals::NumericFunctionBasisExpansion<Series<Coeff>, std::string>(toreturn);
}

template <typename T, size_t L>
ReconstructionInputEvaluator<T, L> ReconstructionInputProvider<T, L>::get_new_reconstruction_input_evaluator(
    std::function<Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<C, C>>(const momD_conf<T, 4>&)> thread_safe_function_copy)
    const {

    ReconstructionInputEvaluator<T, L> new_evaluator;

    // Copy the data to each evaluator.
    new_evaluator.function = thread_safe_function_copy;
    new_evaluator.function_to_position = function_to_position;
    new_evaluator.position_to_function = position_to_function;
    new_evaluator.basis_map = basis_map;
    new_evaluator.multiplicity = multiplicity;
    new_evaluator.number_of_coefficients = number_of_coefficients;
    new_evaluator.function_names = function_names;

    return new_evaluator;
}

template <typename T, size_t L> unsigned long ReconstructionInputProvider<T, L>::get_position_of(const std::string& id, const int& power) const {
    auto pos = function_to_position.find({id, power});
    if (pos != function_to_position.end()) {
        return pos->second;
    } else {
        throw std::runtime_error(std::string("Error! Function'" + id + "' is not found in the map!"));
    }
}

template <typename T, size_t L> std::pair<std::string, int> ReconstructionInputProvider<T, L>::get_function_and_power(const std::size_t& index) const {
    auto pos = position_to_function.find(index);
    if (pos != position_to_function.end()) {
        return pos->second;
    } else {
        throw std::runtime_error(std::string("Error! Index'" + std::to_string(index) + "' is not found in the map!"));
    }
}

template <typename T, size_t L> void ReconstructionInputProvider<T, L>::print_map_index_to_function(std::ofstream& stream) const {
    if (!constructed_lookup_tables) { throw std::runtime_error("Unable to print map to stream! Lookup tables have not been constructed yet!"); }
    stream << "Position \t ep-power \t Basis Function\n\n";
    for (const auto& entry : position_to_function) { stream << entry.first << "\t" << entry.second.second << "\t" << entry.second.first << std::endl; }
}

template <typename T, size_t L> bool ReconstructionInputProvider<T, L>::contains(const std::string& name) const {
    for (const auto& entry : function_to_position) {
        if (entry.first.first == name) { return true; }
    }
    return false;
}

// Call operator
template <typename T, size_t L> std::vector<T> ReconstructionInputProvider<T, L>::operator()(const momentumD_configuration<T, 4>& mom_conf) {

    auto result = function(mom_conf);

    auto mapped_result = basis_map(result);

    // Convert the result into a vector that is being returned.
    std::vector<T> vector_coeffs = map_coefficients_to_vector(mapped_result);

    return vector_coeffs;
}

template <typename T, size_t L>
std::vector<T> ReconstructionInputProvider<T, L>::map_coefficients_to_vector(const Integrals::NumericFunctionBasisExpansion<Series<T>, std::string>& nfbe) {

    // If it is the first phase space point, the look-up tables need to be constructed.
    if (!constructed_lookup_tables) { construct_lookup_tables(nfbe); }

    // The return vector that will store the coefficients
    std::vector<T> coefficients(number_of_coefficients);

    // At 2-loops the minimal power in epsilon is -4, at 1-loop it is -2.
    // int min_power = (L == 2)?-4:-2;

    // Loop over all basis functions and extract their coefficients epsilon series coefficients.
    for (const auto& entry : nfbe) {
        // This is only done if the entry is among the requested functions.
        if (function_names.size() == 0 || std::find(function_names.begin(), function_names.end(), entry.first) != function_names.end()) {
            for (int power = entry.second.leading() /*min_power*/; power <= 0; ++power) {
                try {
                    coefficients.at(function_to_position.at({entry.first, power})) = entry.second[power];
                } catch (const std::range_error&) { coefficients.at(function_to_position.at({entry.first, power})) = T(0); }
            }
        }
    }

    return coefficients;
}

template <typename T, size_t L>
void ReconstructionInputProvider<T, L>::construct_lookup_tables(const Integrals::NumericFunctionBasisExpansion<Series<T>, std::string>& nfbe) {

    unsigned long counter = -1;

    // Loop through all of the functions in the NumericFunctionBasisExpansion
    for (const auto& entry : nfbe) {
        // Loop over all the possible powers in epsilon the coefficient could have.
        // This is only done if the entry is among the requested functions.
        if (function_names.size() == 0 || std::find(function_names.begin(), function_names.end(), entry.first) != function_names.end()) {
            for (int power = entry.second.leading(); power <= 0; ++power) {
                auto index = ++counter;
                function_to_position.insert({{entry.first, power}, index});
                position_to_function.insert({index, {entry.first, power}});
            }
        }
    }

    // Remember that the lookup tables have been constructed.
    constructed_lookup_tables = true;

    // Remember the number of coefficients
    number_of_coefficients = function_to_position.size();
}

// ----------------------------------------------------------------------------
// -------------------------- Output Function ---------------------------------
// ----------------------------------------------------------------------------

std::string mathematica_brackets(std::string s);

template <typename T>
void output_to_file(const Integrals::NumericFunctionBasisExpansion<Series<Ratio<DensePolynomial<T>>>, std::string>& nfbe, const std::string& filename) {

    // Determine the range of the powers in epsilon.
    auto eps_min = nfbe.leading();
    auto eps_max = nfbe.last();

    // Create the string for the output.
    std::ostringstream output;

    // Loop over all the powers and for each power create an expression
    for (int eps_power = eps_min; eps_power <= eps_max; ++eps_power) {
        std::ostringstream oss;

        oss << "\neps" << ((eps_power < 0) ? "M" : "") << abs(eps_power) << " = ";

        // Get all the basis functions that contribute to the given power in epsilon
        auto funcs = nfbe.get_eps_pow_coeff(eps_power);
        // Loop over all the functions
        for (const auto& entry : funcs) {
          if (!(entry.second.numerator.coefficients.size() == 1 && entry.second.numerator.coefficients[0] == BigRat(0)))
                oss << entry.second << " * " << mathematica_brackets(entry.first) << " + \n";
        }

        output << oss.str() << 0 << ";";
    }

    std::ofstream outfile(filename);
    if (outfile.good()) {
        outfile << output.str();
    } else {
        throw std::runtime_error("Failed to write amplitude to file!");
    }

    outfile.close();
}

// ----------------------------------------------------------------------------
// ------------------ Declare external instantiations -------------------------
// ----------------------------------------------------------------------------

#define EXTERN_INSTANTIATE_INPUT_PROVIDER(T, L)                                                                                                                \
    extern template class ReconstructionInputProvider<T, L>;                                                                                                   \
    extern template class ReconstructionInputEvaluator<T, L>;                                                                                                  \
    extern template void output_to_file(const Integrals::NumericFunctionBasisExpansion<Series<Ratio<DensePolynomial<T>>>, std::string>& nfbe,                  \
                                        const std::string& filename);

EXTERN_INSTANTIATE_INPUT_PROVIDER(F32, 2)

} // namespace Caravel
