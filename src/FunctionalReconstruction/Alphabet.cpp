#include "Alphabet.h"
#include "AmplitudeCoefficients/NumericalTools.h"
#include "Core/MathInterface.h"
#include "FunctionalReconstruction/NewtonFunction.h"

#include <sstream>
#include <iterator>

namespace Caravel {

namespace {

F prodpow(const F& x, std::size_t n) {
    F result(1);
    for (size_t i = 0; i < n; i++) { result *= x; }
    return result;
}

} // anonymous namespace

// ####################################################################################################################
// #                                      LetterMonomial                                                              #
// ####################################################################################################################

LetterMonomial::LetterMonomial(const std::string& _alphabet_name, const std::string& _string_rep, const IndexPower& _ip)
    : alphabet_name{_alphabet_name}, string_rep{_string_rep}, index_powers{_ip} {}

LetterMonomial::LetterMonomial(const std::string& mathlist) {
    auto everything = MathList(mathlist, "LetterMonomial");

    // We need the correct size
    assert(everything.tail.size() == 3 && "MathList needs to have 3 entries!");

    // Read the name of the alphabet the letters in the monomial belong to.
    auto _alphabet = MathList(everything.tail[0], "Alphabet").tail[0];

    // Read its string representation
    auto _string_rep = MathList(everything.tail[1], "StringRep").tail[0];

    // Now read the indices and their powers in the map.
    std::vector<std::string> math_ip = MathList(everything.tail[2], "IndexPowers").tail;
    IndexPower _index_powers{};

    for (const auto& ip : math_ip) {
        auto index_power = MathList(ip, "IndexPower").tail;
        assert(index_power.size() == 2 && "index_power must contain both an index and a power!");
        std::size_t index = std::stoul(MathList(index_power[0], "Index").tail[0]);
        int power = std::stoul(MathList(index_power[1], "Power").tail[0]);
        _index_powers.push_back({index, power});
    }

    alphabet_name = _alphabet;
    string_rep = _string_rep;
    index_powers = _index_powers;
}

std::string LetterMonomial::as_string() const { return string_rep; }

std::string LetterMonomial::as_mathlist() const {
    std::ostringstream oss;
    oss << "LetterMonomial[Alphabet[" << alphabet_name << "],StringRep[" << string_rep << "],IndexPowers[";

    for (const auto& ip : index_powers) {
        oss << "IndexPower[Index[" << ip.first << "],Power[" << ip.second << "]]";
        if (ip != index_powers.back()) { oss << ","; }
    }

    oss << "]]";

    return oss.str();
}

std::ostream& operator<<(std::ostream& stream, const LetterMonomial& monomial) {
    stream << monomial.as_string();
    return stream;
}

// Get the decomposition of the Monomial into its letters and their powers
const IndexPower& LetterMonomial::get_index_powers() const { return index_powers; }

// Return the name of the alphabet.
std::string LetterMonomial::get_alphabet_name() const { return alphabet_name; }

// Compare letter monomials
bool operator == (const LetterMonomial& l1, const LetterMonomial& l2) {
    if (l1.alphabet_name != l2.alphabet_name) return false;
    if (l1.string_rep != l2.string_rep) return false;
    if (l1.index_powers.size() != l2.index_powers.size()) return false;
    for (std::size_t i = 0; i < l1.index_powers.size(); ++i){
        if (l1.index_powers[i].first != l2.index_powers[i].first) return false;
        if (l1.index_powers[i].second != l2.index_powers[i].second) return false;
    }
    return true;
}

// Compare letter monomials
bool operator != (const LetterMonomial& l1, const LetterMonomial& l2) {
    return !(l1 == l2);
}

// ####################################################################################################################
// #                                            Output Functions                                                      #
// ####################################################################################################################

// Read a vector of letter monomials from file.
std::vector<LetterMonomial> read_letter_monomials_from_file(const std::string& filename) {
    // Try to open file
    std::ifstream file(filename);

    // The vector to return
    std::vector<LetterMonomial> to_return;
 
    if (file.is_open()) {
        // Read file content into string
        std::string buffer(std::istreambuf_iterator<char>{file}, {});
        auto content = MathList(buffer).tail;
        for (const auto& str: content) { to_return.push_back(LetterMonomial(str)); }
    }
    else {
        std::ostringstream oss;
        oss << "Unable to read from file '" << filename << "'\n";
        throw std::runtime_error(oss.str());
    }

    return to_return;
}

// Write a vector of letter monomials to a file.
void write_letter_monomials_to_file(const std::string& filename, std::vector<LetterMonomial> monomials) {
    // Try to open file
    std::ofstream file(filename);

    if (file.is_open()) {
        file << "MathList[";
        for (std::size_t i = 0; i < monomials.size(); ++i) { 
            file << monomials[i].as_mathlist();
            if(i != monomials.size() - 1){ file << ","; }
        }
        file << "]";
    }
    else {
        std::ostringstream oss;
        oss << "Unable to write to file '" << filename << "'\n";
        throw std::runtime_error(oss.str());
    }
}

// ####################################################################################################################
// #                                            Alphabet                                                              #
// ####################################################################################################################
std::size_t Alphabet::n_letters() const { return letters.size(); }

std::string Alphabet::name() const { return _name; }

std::vector<DensePolynomial<F>> Alphabet::reconstruct_on_slice(const Slice& slice) const {
    std::vector<DensePolynomial<F>> to_return;

    std::mt19937 mt1(rand());
    std::function<F()> x_gen = [mt1]() mutable { return random_scaled_number(F(1), mt1); };

    for (const auto& letter : letters) {
        auto evaluator = [&slice, &letter](F x) { return letter(slice(x)); };

        to_return.push_back(NewtonFunction<F>(std::move(evaluator), x_gen).to_canonical());
    }

    return to_return;
}

// Factorize the letters appearing in the coefficient on the provided slice.
LetterMonomial Alphabet::factorize_letters(const Poly& coeff, const Slice& slice) {
    const auto letters = reconstruct_on_slice(slice);
    return _factorize_letters(coeff, letters);
}

// Factorize the letters appearing in the coefficients on the provided slice.
std::vector<LetterMonomial> Alphabet::factorize_letters(const std::vector<Poly>& coeffs, const Slice& slice) {
    // Reconstruct the letters on the slice
    const auto letters = reconstruct_on_slice(slice);

    std::vector<LetterMonomial> to_return;
    for (const auto& coeff : coeffs) { to_return.push_back(_factorize_letters(coeff, letters)); }

    return to_return;
}

// Factorize the letters appearing in the coefficient on the provided slice.
LetterMonomial Alphabet::factorize_letters(const Rat& coeff, const Slice& slice) {
    // Reconstruct the letters on the slice
    const auto letters = reconstruct_on_slice(slice);

    return _factorize_letters(coeff, letters);
}

// Factorize the letters appearing in the coefficients on the provided slice.
std::vector<LetterMonomial> Alphabet::factorize_letters(const std::vector<Rat>& coeffs, const Slice& slice) {
    // Reconstruct the letters on the slice
    const auto letters = reconstruct_on_slice(slice);

    std::vector<LetterMonomial> to_return;
    for (const auto& coeff : coeffs) { to_return.push_back(_factorize_letters(coeff, letters)); }

    return to_return;
}

// Evaluate a LetterMonomial on a specific phase space point.
F Alphabet::evaluate_letter(const LetterMonomial& letter, const std::vector<F>& xs) const {
    // Make sure the letter originates from the alphabet, otherwise not definite
    assert(letter.alphabet_name == _name);

    F numerator = F(1);
    F denominator = F(1);
    for (const auto& factor : letter.index_powers) {
        (factor.second > 0) ? numerator *= prodpow(letters[factor.first](xs), factor.second)
                            : denominator *= prodpow(letters[factor.first](xs), -factor.second);
    }

    return numerator / denominator;
}

// Evaluate several LetterMonomials on a specific phase space point.
std::vector<F> Alphabet::evaluate_letters(const std::vector<LetterMonomial>& letter_vec, const std::vector<F>& xs) const {
    std::vector<F> to_return;

    for (const auto& letter : letter_vec) { to_return.push_back(evaluate_letter(letter, xs)); }

    return to_return;
}

// Evaluate all letters in the alphabet on a phase space point.
std::vector<F> Alphabet::eval_letters_on_psp(const std::vector<F>& x) const {
    std::vector<F> to_return;

    for (const auto& f: letters){
        to_return.push_back(f(x));
    }

    return to_return;
}

std::string Alphabet::_stringify_polynomial(const IndexPower& polynomial) {
    // If empty
    if (polynomial.size() == 0) { return "(1)"; }

    // Stream to write the output to
    std::ostringstream output;

    if (polynomial.size() > 1) { output << "("; }
    for (const auto& letter : polynomial) {
        output << "(";
        output << letter_strings[letter.first];
        output << ")";
        if (letter.second > 1) { output << "^" << letter.second; }
        output << "*";
    }
    std::string to_return = output.str();
    if (polynomial.size() > 1) { to_return.back() = ')'; }
    else {
        to_return.pop_back();
    }
    return to_return;
}

// Factorize the provided letters appearing in the coefficient.
LetterMonomial Alphabet::_factorize_letters(const Poly& coefficient, const std::vector<Poly>& letters) {
    IndexPower to_return;

    // Loop over all the letters
    for (std::size_t i = 0; i < letters.size(); ++i) {

        // Check whether polynomial to divide by has lower degree.
        if (coefficient.get_degree() < letters[i].get_degree()) { continue; }

        auto quotient_remainder = polynomial_divide(coefficient, letters[i]);

        if (quotient_remainder.second.is_zero()) { to_return.push_back({i, 0}); }

        while (quotient_remainder.second.is_zero()) {

            to_return.back().second += 1;

            if (quotient_remainder.first.get_degree() < letters[i].get_degree()) { break; }
            quotient_remainder = polynomial_divide(quotient_remainder.first, letters[i]);
        }
    }

    LetterMonomial lm(_name, _stringify_polynomial(to_return), std::move(to_return));

    return lm;
}

// Factorize the provided letters appearing in the coefficient.
LetterMonomial Alphabet::_factorize_letters(const Rat& coeff, const std::vector<Poly>& letters) {
    // Reconstruct numerator
    auto numer = _factorize_letters(coeff.numerator, letters);

    // Reconstruct denominator
    const auto denom = _factorize_letters(coeff.denominator, letters);

    // Combine

    numer.string_rep += "/";
    numer.string_rep += denom.string_rep;

    // Here we assume that no common factors appear in the numerator and denominator
    for (const auto& ip : denom.index_powers) { numer.index_powers.push_back({ip.first, -ip.second}); }

    return numer;
}

} // namespace Caravel
