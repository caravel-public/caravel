
namespace Caravel{


template <class N, class D> bool operator==(const Ratio<N, D>& a, const Ratio<N, D>& b) {
    return (a.numerator == b.numerator) && (a.denominator == b.denominator);
}

template<class N, class D>
std::ostream& operator <<(std::ostream& out, const Ratio<N,D>& r){
    out << "(" << r.numerator << ")/(" << r.denominator << ")";
    return out;
}



template <typename FF, typename FX> bool operator==(const DenseRational<FF, FX>& a, const DenseRational<FF, FX>& b) {
    return (a.numerator == b.numerator) && (a.denominator == b.denominator);
}

template <typename FF, typename FX> DenseRational<FF, FX>& DenseRational<FF, FX>::operator+=(const DenseRational<FF, FX>& a){
    if(denominator == a.denominator){
        numerator += a.numerator;
        return *this;
    }

    DensePolynomial<FX> new_den = polynomial_LCM(denominator,a.denominator);

    // normalize such that the smallest power in the denominator has coefficient 1
    {
        FX norm{FX(1)};
        for(auto& it: new_den.coefficients){
            if(it != FX(0)){ norm = it; break; }
        }
        for(auto& it: new_den.coefficients){
            it /= norm;
        }
    }

    auto f1 = polynomial_divide(new_den,denominator);

    assert(f1.second.is_zero());
    auto f2 = polynomial_divide(new_den,a.denominator);
    assert(f2.second.is_zero());

    f2.first *= a.numerator;

    numerator *= f1.first;
    numerator += f2.first;

    denominator = new_den;

    return *this;
}


template <typename FF, typename FX> template <typename T> 
std::enable_if_t<std::is_same<FF, T>::value, std::vector<FF>> DenseRational<FF, FX>::flatten() const {
    using F = FF;
    std::vector<F> tret(numerator.coefficients);
    tret.insert(tret.end(), denominator.coefficients.begin(), denominator.coefficients.end());
    return tret;
}

template <typename FF, typename FX> template <typename T> 
std::enable_if_t<!(std::is_same<FF, T>::value), std::vector<FF>> DenseRational<FF, FX>::flatten() const {
    std::cerr<<"ERROR: called DenseRational::flatten() when numerator/denominator types are different, exit 1"<<std::endl;
    std::exit(1);
}

template <typename FF, typename FX> template <typename T> 
std::enable_if_t<std::is_same<FF, T>::value, Series<FF>> DenseRational<FF, FX>::taylor_expand_around_zero(int order) const {
    int first_non_zero_denom_order = 0;
    for (auto& coeff : denominator.coefficients) {
        if (coeff != FF(0)) { break; }
        first_non_zero_denom_order++;
    }
    order+=first_non_zero_denom_order;

    auto num_coeffs = numerator.coefficients;

    for (int i = 0; i <= order - int(numerator.coefficients.size()); i++) { num_coeffs.push_back(FF(0)); }
    Series<FF> taylor_numerator(0, order, num_coeffs);

    // We work with the denominator, factoring out any possible powers of x
    auto& den_coeffs = denominator.coefficients;
    std::vector<FF> relevant_den_coefficients(den_coeffs.begin() + first_non_zero_denom_order, den_coeffs.end());
    auto current_derivative = DensePolynomial<FF>(relevant_den_coefficients);

    std::vector<FF> den_derivatives_at_zero;

    for (int i = 0; i <= order; i++) {
        den_derivatives_at_zero.push_back(current_derivative(FF(0)));
        current_derivative = current_derivative.derivative();
    }

    auto inv_f_at_zero = FF(1) / den_derivatives_at_zero.at(0);
    std::vector<FF> inv_denominator_coefficients{inv_f_at_zero};

    for (int n = 1; n <= order; n++) {
        FF next_derivative(0);
	std::vector<std::vector<int>> mlist;
	std::function<void(int,std::vector<int>)> fill_mlist
	    =[&fill_mlist, &mlist, &n](int ni, std::vector<int>ilist){
		 if (ni == 0) {
		     ilist.resize(n, 0);
		     mlist.push_back(ilist);
		 }
		 else if(ni > 0 && (int)ilist.size() <= n)
		     for (int i = n/((int)ilist.size()+1); i >= 0; --i)
			 {
			     ilist.push_back(i);
			     int left = n;
			     for(size_t j = 0; j < ilist.size(); ++j)
				 left -= ilist[j] * (j+1);
			     fill_mlist(left, ilist);
			     ilist.pop_back();
			 }
	     };
	fill_mlist(n, std::vector<int>());
	for (auto& m : mlist) {
	    FF next_derivative_prod = inv_f_at_zero;
	    int k = 0;
	    for(auto & mj : m)
		k += mj;
	    for(int j = 1; j <= k; ++j)
		next_derivative_prod *= -FF(j) * inv_f_at_zero;
	    for(int j = 1; j <= n; ++j)
		for(int l = 1; l <= m[j-1]; ++l) {
		    for(int fac_j = 1; fac_j <= j; ++fac_j)
			next_derivative_prod /= FF(fac_j);
		    next_derivative_prod *= den_derivatives_at_zero[j] / FF(l);
		}
	    next_derivative += next_derivative_prod;
	}
        inv_denominator_coefficients.push_back(next_derivative);
    }

    Series<FF> taylor_inv_denominator(0, order, inv_denominator_coefficients);

    return taylor_numerator * (taylor_inv_denominator.multiply_by_power(-first_non_zero_denom_order));
}

template <typename FF, typename FX>  template <typename T>
std::enable_if_t<!(std::is_same<FF, T>::value), Series<FF>> DenseRational<FF, FX>::taylor_expand_around_zero(int order) const {
    std::cerr<<"ERROR: called DenseRational::taylor_expand_around_zero(.) when numerator/denominator types are different, not implemented yet"<<std::endl;
    std::exit(1);
}

template <typename FF, typename FX> std::ostream& operator<<(std::ostream& out, const DenseRational<FF, FX>& r) {
    if ((r.numerator.is_zero() and !r.denominator.is_zero()) or r.denominator.is_one()) return out << r.numerator;
    return out << "(" << r.numerator << ")/(" << r.denominator << ")";
}

template <typename FF, typename FX> std::string math_string(const DenseRational<FF,FX>& r) {
    if ((r.numerator.is_zero() and !r.denominator.is_zero()) or r.denominator.is_one()) return math_string(r.numerator);
    std::string out("("+math_string(r.numerator) + ")/(" + math_string(r.denominator) + ")");
    return out;
}


template <typename F> DenseRational<F> unflatten(size_t num_size, size_t den_size,const std::vector<F>& in){
    if (in.size() != num_size + den_size) {
        std::cout << "ERROR trying to unflatten DenseRational with wrong size vector" << std::endl;
        return DenseRational<F>();
    }
    std::vector<F> fn;
    fn.insert(fn.end(), in.begin(), in.begin() + num_size);
    std::vector<F> fd;
    fd.insert(fd.end(), in.begin() + num_size, in.begin() + num_size + den_size);
    return DenseRational<F>(fn, fd);
}

template<typename F>
  Ratio<DensePolynomial<F>> fit_univariate_rational(const std::vector<std::pair<F,F>>& coordinates, size_t numerator_rank, size_t denominator_rank){

      auto all_powers_up_to = [](F x, size_t n){
        std::vector<F> powers(n+1);

        powers[0] = F(1);
        for (size_t i = 1; i <= n; i++){
          powers[i] = powers[i-1]*x;
        }

        return powers;
      };

      size_t basis_dim = coordinates.size();
      std::vector<F> A(basis_dim*basis_dim, F(0));

      for (size_t i = 0; i < coordinates.size(); i++){
	std::vector<F> basis(basis_dim);
	auto z = coordinates[i].first;
	auto f = coordinates[i].second;

        auto zPowers = all_powers_up_to(z, std::max(numerator_rank, denominator_rank));
	for (size_t j = 0; j <= numerator_rank; j++){
	  basis[j] = zPowers[j];
	}
	for (size_t j = 1; j <= denominator_rank; j++){
	  basis[numerator_rank+j] = -zPowers[j]*f;
	}

	for (size_t j = 0; j < basis_dim; j++){
	  A[i+j*basis_dim] = basis[j];
	}
      }

      std::vector<F> b;
      for (auto& el : coordinates){
	b.push_back(el.second);
      }

      auto coefficients = plu_solver(std::move(A),std::move(b));

      std::vector<F> num_coeffs(numerator_rank+1);
      for (size_t i = 0; i <= numerator_rank; i++){
	num_coeffs[i] = coefficients[i];
      }

      std::vector<F> denom_coeffs(denominator_rank+1);
      denom_coeffs[0] = F(1);
      for (size_t i = 0; i < denominator_rank; i++){
	denom_coeffs[i+1] = coefficients[numerator_rank+i+1];
      }

      return Ratio<DensePolynomial<F>>(num_coeffs, denom_coeffs);
}

template <typename FX> DensePolynomial<FX> substitute(const DensePolynomial<FX>& relation, const DensePolynomial<FX>& in) {
    DensePolynomial<FX> result({FX(0)}, relation.var_name);
    DensePolynomial<FX> factor({FX(1)}, relation.var_name);
    for(auto& coeff: in.coefficients) {
        result += coeff * factor;
        factor *= relation;
    }
    return result;
}

template <typename FX> DenseRational<FX> substitute(const DensePolynomial<FX>& relation, const DenseRational<FX>& in) {
    auto num = substitute(relation, in.numerator);
    auto den = substitute(relation, in.denominator);
    return DenseRational<FX>(std::move(num), std::move(den));
}

template <typename FX> DenseRational<FX> substitute(const DensePolynomial<FX>& relation, const DenseRational<DensePolynomial<FX>, FX>& in) {
    // relation should give a result compatible with outer variable
    assert(relation.var_name == in.denominator.var_name);
    // regular 'zero' constructor works as FF = FX
    DensePolynomial<FX> num(in.denominator.var_name);
    
    DensePolynomial<FX> factor({FX(1)}, relation.var_name);
    DensePolynomial<FX> varval({FX(0), FX(1)}, relation.var_name);
    for(auto& function: in.numerator.coefficients) {
        auto subs = substitute(relation, function);
        num += subs * factor;
        factor *= varval;
    }

    if (num.is_zero()) return DenseRational<FX>(relation.var_name);

    auto& den = in.denominator;

    if constexpr (is_exact_v<FX>) {
        // simplify
        auto GCD = polynomial_GCD(num, den);
        if (!GCD.is_one()) {
            num = polynomial_divide(num, GCD).first;
            den = polynomial_divide(den, GCD).first;
        }

        // set to 1 leading denominator coeff
        FX lead(1);
        for (auto& c : den.coefficients) {
            if (c != FX(0)) {
                lead = c;
                break;
            }
        }
        if (lead != FX(1)) {
            num /= lead;
            den /= lead;
        }
    }

    return DenseRational<FX>(std::move(num), std::move(den));
}

template <typename FX> DenseRational<FX> substitute(const DensePolynomial<FX>& relation, const DenseRational<DenseRational<FX>, FX>& in) {
    // relation should give a result compatible with outer variable
    assert(relation.var_name == in.denominator.var_name);
    // regular 'zero' constructor works as FF = FX
    DenseRational<FX> fullnum(in.denominator.var_name);
    
    DensePolynomial<FX> factor({FX(1)}, relation.var_name);
    DensePolynomial<FX> varval({FX(0), FX(1)}, relation.var_name);
    for(auto& function: in.numerator.coefficients) {
        auto subs = substitute(relation, function);
        fullnum += DenseRational<FX>(std::move(subs.numerator * factor), std::move(subs.denominator));
        factor *= varval;
    }

    auto num = fullnum.numerator;
    auto den = in.denominator * fullnum.denominator;

    if (num.is_zero()) return DenseRational<FX>(relation.var_name);

    if constexpr (is_exact_v<FX>) {
        // simplify
        auto GCD = polynomial_GCD(num, den);
        if (!GCD.is_one()) {
            num = polynomial_divide(num, GCD).first;
            den = polynomial_divide(den, GCD).first;
        }

        // set to 1 leading denominator coeff
        FX lead(1);
        for (auto& c : den.coefficients) {
            if (c != FX(0)) {
                lead = c;
                break;
            }
        }
        if (lead != FX(1)) {
            num /= lead;
            den /= lead;
        }
    }
    return DenseRational<FX>(std::move(num), std::move(den));
}
}


// specialize hasher
namespace std{
template <typename T> struct hash<Caravel::DenseRational<T>> {
  public:
    size_t operator()(const Caravel::DenseRational<T>& xs) const {
        hash<size_t> hasher;
        size_t result = hasher(2);
        Caravel::hash_combine(result, xs.numerator);
        Caravel::hash_combine(result, xs.denominator);
        return result;
    }
};
}

