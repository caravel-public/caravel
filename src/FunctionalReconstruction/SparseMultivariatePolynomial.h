#ifndef SPARSE_MULTIVARIATE_POLYNOMIAL_H_INC
#define SPARSE_MULTIVARIATE_POLYNOMIAL_H_INC

#include <unordered_map>
#include "FunctionalReconstruction/DensePolynomial.h"

namespace Caravel{
/**
 * Multivariate polynomial class. Stores polynomial as a vector of
 * unordered maps. In each unordered map, the degree of each monomial
 * is the same.
 */

template<class F> class SparseMultivariatePolynomial;
template<class F>
std::ostream& operator <<(std::ostream& out, const SparseMultivariatePolynomial<F>& p);

using Monomial = std::vector<size_t>;

template<typename F>
using PolynomialTerm = std::pair<Monomial,F>;

template<typename F>
using HomogeneousPolynomial = std::unordered_map<Monomial, F>;

template<typename F>
class SparseMultivariatePolynomial{
    private:
  // TODO: Make HomogeneousPolynomial a class?
    void homogeneous_ax(F a, HomogeneousPolynomial<F>& x);
    void homogeneous_sum(HomogeneousPolynomial<F>& x, const HomogeneousPolynomial<F>& y);
    HomogeneousPolynomial<F> multiply_by_variable(const HomogeneousPolynomial<F>& p, size_t z_i);

    public:
        std::vector<std::string> variable_names;
        std::vector<HomogeneousPolynomial<F>> polynomials_by_degree;

        SparseMultivariatePolynomial(const MathList& schematic);

        SparseMultivariatePolynomial() = default;

	SparseMultivariatePolynomial(const DensePolynomial<F>& p){
          variable_names.push_back(p.var_name);

	  for (size_t i = 0; i < p.coefficients.size(); i++){
	    HomogeneousPolynomial<F> coordinates;
	    coordinates[{i}] = p.coefficients.at(i);
	    polynomials_by_degree.push_back(coordinates);
	  }
	}

	SparseMultivariatePolynomial(const std::vector<HomogeneousPolynomial<F>>& polynomials){
	  polynomials_by_degree = polynomials;
	}


	void multiply_with_linear(size_t z_i, F z_0);


        F operator()(const std::vector<F>& variables) const;
        void operator += (const SparseMultivariatePolynomial& other);
        void operator += (const PolynomialTerm<F>& term);

	void operator *=(const Monomial& mon){
          size_t mon_deg = 0;
          for (auto& n : mon) mon_deg += n;
          HomogeneousPolynomial<F> empty {};
          std::vector<HomogeneousPolynomial<F>> new_polys_by_degree(mon_deg, empty);

	  for (auto& homo_poly : polynomials_by_degree){
            HomogeneousPolynomial<F> new_homo_poly;
	    for (auto& term : homo_poly){
              Monomial new_term_monomial = term.first;
              for (size_t i = 0; i < new_term_monomial.size(); i++){new_term_monomial.at(i) += mon.at(i);};
              new_homo_poly[new_term_monomial] = term.second;
	    }
            new_polys_by_degree.push_back(new_homo_poly);
	  }
          polynomials_by_degree = new_polys_by_degree;
	}

	void operator *=(const F& scalar){
	  for (auto& homo_poly : polynomials_by_degree){
	    for (auto& term : homo_poly){
	      term.second *= scalar;
	    }
	  }
	}

	void operator /=(const F& scalar){
	  for (auto& homo_poly : polynomials_by_degree){
	    for (auto& term : homo_poly){
	      term.second /= scalar;
	    }
	  }
	}

  /**
   * Adds variabe to multivariate polynomial effectively multiplying it by
   * var^degree. The position of var within the polynomial is specified by the
   * second argument, which defaults to the end.
   */
  void add_variable(size_t degree = 0, std::optional<size_t> pos = {});

	friend std::ostream& operator <<<>(std::ostream& out, const SparseMultivariatePolynomial<F>& p);

	/**
	 * Shift the arguments of the polynomial by a constant vector.
	 */
	template<size_t N>
	void shift(std::array<F,N> z);

      /**
       * Returns a version of the polynomial with an extra variable such
       * that the the polynomial is now of (minimal) homogeneous
       * degree.
       */
  SparseMultivariatePolynomial<F> homogenize(std::vector<std::string>, size_t new_var_pos);
};

 template<typename F>
   bool operator == (const SparseMultivariatePolynomial<F>& pA, const SparseMultivariatePolynomial<F>& pB);

 template<typename F>
   bool operator != (const SparseMultivariatePolynomial<F>& pA, const SparseMultivariatePolynomial<F>& pB) {return !(pA==pB);}

/**
 * Applys a binary function to a pair of polynomials, elementwise.
 */
template<typename A, typename B>
SparseMultivariatePolynomial<A> lift(const std::function<A(A,B)>& f, SparseMultivariatePolynomial<A> pa,
				     const SparseMultivariatePolynomial<B>& pb);

/**
 * Applys a function to each coefficient of the polynomial. Maintains variable names.
 */
template<typename A, typename B>
  SparseMultivariatePolynomial<B> fmap(const std::function<B(A)>& f, SparseMultivariatePolynomial<A> pa);

}

#include "SparseMultivariatePolynomial.hpp" // Implementations
#endif 
