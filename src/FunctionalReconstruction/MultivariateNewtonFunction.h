#ifndef MULTIVARIATE_NEWTON_FUNCTION_H_INC
#define MULTIVARIATE_NEWTON_FUNCTION_H_INC

#include "FunctionalReconstruction/NewtonFunction.h"
#include "FunctionalReconstruction/SparseMultivariatePolynomial.h"
#include <iostream>
#include <optional>

namespace Caravel {
/**
 * Based upon section 3.4 of Peraro's paper.
 */

template <class F, size_t N> class MultivariateNewtonFunction;

template <class F, size_t N> std::ostream& operator<<(std::ostream& out, const MultivariateNewtonFunction<F, N>& p);

template <class F> std::ostream& operator<<(std::ostream& out, const MultivariateNewtonFunction<F, 1>& p);

/**
 * Multivariate Newton Polynomials.
 *
 * The template type `F` refers to the type of the coefficients in the multivariate polynomial and `N`
 * is the number of variables.
 * 
 * The reconstruction is performed at the level of the coefficients in the Newton representation and 
 * not directly at the level of the monomial coefficients. Only after the full reconstruction is the 
 * conversion to canonical form performed.
 */
template <typename F, size_t N> class MultivariateNewtonFunction {

    /// Cache the generated next point to compare it to what is being fed to add_point.
    std::array<F, N> next_expected;

    /// TODO: Remove this? Does not seem to be in use.
    // MultivariateNewtonFunction<F, N - 1> new_slice() const;

    /// Container for the point generators in each one of the N variables.
    std::array<std::function<F()>, N> x_gens;

    /// Tracker for the number of generated interpolation points.
    size_t num_self_genned{0};

    /// Contains the reconstruction of the current a_i(y,z) to be reconstructed.
    /// Once it is reconstructed, it is being moved to coordinates together with
    /// `current_x`.
    MultivariateNewtonFunction<F, N - 1> current_reconstruction;

    /// Current interpolation point for the variable associated to the
    /// current reconstruction.
    /// Once the associated multivariate polynomial in the remaining variables
    /// is reconstructed it will be added to `coordinates` together with
    /// `current_reconstruction`.
    F current_x;

    /**
     * If set to a positive value then the reconstruction assumes that
     * the (total) rank of the function is no higher than this value,
     * potentially saving evaluations.
     */
    int total_rank{-1};

    /// Store the maximal ranks in each variable.
    /// TODO: currently does not seem to be used.
    std::vector<std::optional<int>> variable_ranks{N, std::optional<int>{}};

    /// TODO: Misleading name: The data is cloned!
    MultivariateNewtonFunction<F, N> clone_no_data() const;

  public:
    // Calculates the current "position" in the reconstruction. These
    // are naturally (and trivially) totally ordered.
    std::array<size_t, N> get_current_position() const;

    /// Return the variable ranks.
    /// TODO: Currently always seems to contain empty entries.
    std::vector<std::optional<int>> get_variable_ranks() const { return variable_ranks; }

    /// Flag to indicate whether the reconstruction is finished.
    bool finished{false};

    /**
     * Pairs of evaluation points y_i and coefficients a_i(y)
     * where the coefficients are considered as multivariate polynomials in
     * one less variable.
     */
    std::vector<std::pair<F, MultivariateNewtonFunction<F, N - 1>>> coordinates;

    /// Return the total the degree of the multivariate polynomial
    size_t degree() const {
        auto deg = coordinates.size();
        if (deg == 0) { return 0; }
        else {
            return deg - 1;
        }
    }

    /**
     * Makes a guess of the next n points that the reconstructor will
     * ask for. Only a guess because it may terminate, or terminate a
     * sub-reconstruction during the list. Only returns maximum n
     * points, as it may be certain that the reconstruction terminates
     * within that many.
     */
    std::vector<std::array<F, N>> guess_points(size_t n) const;

    /**
     * Returns the next point upon which the function under
     * reconstruction should be evaluated on.
     */
    std::optional<std::array<F, N>> next_point();

    /**
     * Feed the reconstruction algorithm with a new sample.
     * @param f_val Value of the function evaluated at x_vals,
     * i.e. f_val = f(x_vals).
     * @param x_vals Value of argument such that f_val =
     * f(x_val). MUST correspond to the point most recently
     * requested by next_point.
     */
    void add_point(F f_val, std::array<F, N> x_vals);

    /**
     * @param f Function of N parameters passed as an array.
     * @param x_gens array of N point generators, each of which are equivalent and independent upon copying.
     */
    MultivariateNewtonFunction(std::function<F(std::array<F, N>)> f, std::array<std::function<F()>, N> x_gens, int total_rank = -1,
                               std::vector<std::optional<int>> variable_ranks = std::vector<std::optional<int>>(N, std::optional<int>{}));

    /**
     * Constructor for empty MultivariateNewtonFunction which asks for samples.
     * @param x_gens List of random point generators for the reconstruction
     * @param max_rank If set to a non-negative value, then the
     * reconstruction will assume that there exist no terms of higher
     * rank, avoiding figuring this out through computation.
     */
    MultivariateNewtonFunction(std::array<std::function<F()>, N> x_gens, int total_rank = -1,
                               std::vector<std::optional<int>> variable_ranks = std::vector<std::optional<int>>(N, std::optional<int>{}));

    MultivariateNewtonFunction<F, N> operator-() const;
    void operator-=(MultivariateNewtonFunction<F, N> other);
    void operator/=(F x);

    /// Evaluate the reconstructed function on a phase space point.
    F operator()(const std::array<F, N>& xs);

    /// Query whether the multivariate polynomial is the zero polynomial.
    bool isZero() const;

    /// Return the number of generated points.
    size_t get_num_generated() const;

    /// Convert a multivariate polynomial expressed in terms of Newton coordinates 
    /// to canonical form.
    SparseMultivariatePolynomial<F> to_canonical() const;

    friend std::ostream& operator<<<>(std::ostream& out, const MultivariateNewtonFunction<F, N>& p);
};

template <class F, size_t N> std::ostream& operator<<(std::ostream& out, const MultivariateNewtonFunction<F, N>& p);

/**
 * MultivariateNewtonFunction specialization for univariate case. The
 * implementation is recursive, and so this base case stops the
 * recursion by wrapping the functionality of NewtonFunction.
 */
template <typename F> class MultivariateNewtonFunction<F, 1> {
  public:
    bool finished{false};

  private:
    NewtonFunction<F> univariate_f;

  public:
    // Calculates the current "position" in the reconstruction. These
    // are naturally (and trivially) totally ordered.
    std::array<size_t, 1> get_current_position() const { return {univariate_f.num_genned}; }

    std::vector<std::optional<int>> get_variable_ranks() const { return {std::optional<int>(univariate_f.max_rank)}; }

    /**
     * Makes a guess of the next n points that the reconstructor will
     * ask for. Only a guess because it may terminate, or terminate a
     * sub-reconstruction during the list.
     */
    std::vector<std::array<F, 1>> guess_points(size_t n) const;

    /**
     * Returns the next point upon which the function under
     * reconstruction should be evaluated on.
     */
    std::optional<std::array<F, 1>> next_point();

    /**
     * Feed the reconstruction algorithm with a new sample.
     * @param f_val Value of the function evaluated at x_val,
     * i.e. f_val = f(x_val).
     * @param x_val Value of argument such that f_val =
     * f(x_val). Should correspond to the point most recently
     * requested by next_point.
     */
    void add_point(F f_val, std::array<F, 1> x_val);

    /**
     * Constructor for automatically reconstructing the supplied
     * univariate function with the supplied point generator.
     */
    MultivariateNewtonFunction(std::function<F(std::array<F, 1>)> f, std::array<std::function<F()>, 1> x_gens, int total_rank = -1,
                               std::vector<std::optional<int>> variable_ranks = std::vector<std::optional<int>>(1, std::optional<int>{}));

    /**
     * Constructor for empty MultivariateNewtonFunction which asks for samples.
     */
    MultivariateNewtonFunction(std::array<std::function<F()>, 1> x_gens, int total_rank = -1,
                               std::vector<std::optional<int>> variable_ranks = std::vector<std::optional<int>>(1, std::optional<int>{}));

    MultivariateNewtonFunction<F, 1> operator-() const;
    void operator-=(const MultivariateNewtonFunction<F, 1>& other);
    void operator/=(F x) {
        for (auto& coord : univariate_f.coordinates) { coord.second /= x; }
    }

    bool isZero() const;

    size_t degree() const {
        auto deg = univariate_f.coordinates.size();
        if (deg == 0) { return 0; }
        else {
            return deg - 1;
        };
    }

    SparseMultivariatePolynomial<F> to_canonical() const { return SparseMultivariatePolynomial<F>(univariate_f.to_canonical()); }

    friend std::ostream& operator<<<>(std::ostream& out, const MultivariateNewtonFunction<F, 1>& p);

    /**
     * Evaluates the reconstructed function on the point.
     */
    F operator()(const std::array<F, 1>& xs);
};

template <class F> std::ostream& operator<<(std::ostream& out, const MultivariateNewtonFunction<F, 1>& p);

/**
 * Class for performing reconstruction of vector valued multivariate polynomial functions.
 */
template <class F, size_t N> class VectorMultivariateNewtonFunction {

  public:
    std::vector<MultivariateNewtonFunction<F, N>> components;

    /**
     * Constructor which automatically reconstructs the provided
     * function using the provided list of point generators.
     */
    VectorMultivariateNewtonFunction(std::function<std::vector<F>(std::array<F, N>)> f, const std::array<std::function<F()>, N>& x_gens);
};

} // namespace Caravel

#include "FunctionalReconstruction/MultivariateNewtonFunction.hpp" // Implementations
#endif
