#include <algorithm>
namespace Caravel{
    template<typename F>
    DensePolynomial<F> InvertingPolynomialReconstructor<F>::to_canonical() const{
	return canonical_form;
    }
  
    template<typename F>
    void InvertingPolynomialReconstructor<F>::add_point(F new_f, F new_x){
      fs.push_back(new_f);
      xs.push_back(new_x);

      if (fs.size() == max_degree - min_degree + 1){
	canonical_form = DensePolynomial<F>(fs, xs, "s", min_degree);
	finished = true;
      }
    }

    template<typename F>
    std::optional<F> InvertingPolynomialReconstructor<F>::next_point(){
      if (finished || (points_genned >= max_degree - min_degree + 1)){
	return std::optional<F>();
      }

      points_genned++;
      return std::optional<F>(x_gen());
    }

  template<typename F>
  InvertingPolynomialReconstructor<F>::InvertingPolynomialReconstructor(std::function<F()> _x_gen,
								        size_t _max_degree,
								        size_t _min_degree,
								        std::string _var_name)
    : x_gen(_x_gen), max_degree(_max_degree), min_degree(_min_degree), var_name(_var_name) {}
}
