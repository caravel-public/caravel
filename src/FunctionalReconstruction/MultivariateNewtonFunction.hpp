namespace Caravel {

// Evaluate the reconstructed function.
template <typename F> F MultivariateNewtonFunction<F, 1>::operator()(const std::array<F, 1>& xs) { return univariate_f(xs[0]); }

// Check whether the multivariate polynomial is zero.
template <typename F> bool MultivariateNewtonFunction<F, 1>::isZero() const {
    for (auto& coord : univariate_f.coordinates) {
        if (!(coord.second == F(0))) { return false; }
    }
    return true;
}

// Evaluate a multivariate newton polynomial on a phase space point.
template <typename F, size_t N> F MultivariateNewtonFunction<F, N>::operator()(const std::array<F, N>& xs) {
    auto x = xs[N - 1];
    std::array<F, N - 1> sub_xs;

    for (size_t i = 0; i < N - 1; i++) { sub_xs[i] = xs[i]; }

    // check that coordinates isn't empty before accesing it
    if (coordinates.size() == 0) return F(0);

    auto val = coordinates.back().second(sub_xs);
    for (int i = coordinates.size() - 2; i >= 0; i--) {
        auto& coord = coordinates.at(i);
        // NOTE: Inverse of Thiele procedure
        val = val * (x - coord.first) + coord.second(sub_xs);
    }
    return val;
}

// ----
template <typename F, size_t N> std::array<size_t, N> MultivariateNewtonFunction<F, N>::get_current_position() const {
    std::array<size_t, N - 1> sub_position = current_reconstruction.get_current_position();
    std::array<size_t, N> position;
    position[0] = num_self_genned;
    for (size_t i = 1; i < N; i++) { position[i] = sub_position[i - 1]; }

    return position;
}

// ---
template <typename F, size_t N> MultivariateNewtonFunction<F, N> MultivariateNewtonFunction<F, N>::clone_no_data() const {
    // Note. Cannot clone *without* the data as the sub reconstructions
    // are stored as a pair *with* the data. To improve memory usage,
    // rearrange this.
    return (*this);
}

// Generic implementation
template <typename F, size_t N> std::vector<std::array<F, N>> MultivariateNewtonFunction<F, N>::guess_points(size_t n) const {
    auto copy = clone_no_data();
    std::vector<std::array<F, N>> results;

    for (size_t i = 0; i < n; i++) {
        auto new_point = copy.next_point();
        if (!new_point) { break; }
        results.push_back(new_point.value());
    }

    return results;
}

int calculate_max_rank(int total_rank, std::optional<int> variable_rank) {
    if (variable_rank && total_rank >= 0) { return std::min(variable_rank.value(), total_rank); }
    else if (variable_rank) {
        return variable_rank.value();
    }
    else if (total_rank >= 0) {
        return total_rank;
    }

    return -1;
}

template <typename F, size_t N> std::optional<std::array<F, N>> MultivariateNewtonFunction<F, N>::next_point() {
    if (finished) { return std::optional<std::array<F, N>>(); }

    auto sub_next_point = current_reconstruction.next_point();
    // If the sub-reconstruction is done and so are we, then we have no more points.
    auto max_rank = calculate_max_rank(total_rank, variable_ranks.at(0));
    if (!sub_next_point && (max_rank >= 0 && int(num_self_genned) > max_rank)) { return std::optional<std::array<F, N>>(); }

    // If no more points in sub_reconstruction then start the next one.
    if (!sub_next_point) {
        std::array<std::function<F()>, N - 1> reduced_x_gens;
        std::vector<std::optional<int>> reduced_variable_ranks(N - 1);
        for (size_t i = 0; i < N - 1; i++) {
            reduced_x_gens[i] = x_gens[i];
            reduced_variable_ranks[i] = variable_ranks[i + 1];
        }
        auto reduced_total_rank = (total_rank == -1) ? -1 : total_rank - num_self_genned;
        current_reconstruction = MultivariateNewtonFunction<F, N - 1>(reduced_x_gens, reduced_total_rank, reduced_variable_ranks);
        sub_next_point = current_reconstruction.next_point();
        current_x = x_gens[N - 1]();
        num_self_genned++;
    }

    std::array<F, N> new_point;
    for (size_t i = 0; i < N - 1; i++) { new_point[i] = sub_next_point.value()[i]; }
    new_point[N - 1] = current_x;

    // Remember for cross check
    next_expected = new_point;

    return std::optional<std::array<F, N>>(new_point);
}

template <typename F, size_t N> void MultivariateNewtonFunction<F, N>::add_point(F f_val, std::array<F, N> x_vals) {
    if (x_vals != next_expected) { throw std::runtime_error("Unexpected point in add_point"); }

    std::array<F, N - 1> sub_x_vals;
    for (size_t i = 0; i < N - 1; i++) { sub_x_vals[i] = x_vals[i]; }

    // Convert f_val into a_val
    auto a_val = f_val;
    for (auto& coord : coordinates) {
        a_val -= coord.second(sub_x_vals);
        a_val /= (current_x - coord.first);
    }

    current_reconstruction.add_point(a_val, sub_x_vals);

    // If this finished the reconstruction of the current component,
    // then turn it into a Newton coefficient, and add it to the set.
    if (current_reconstruction.finished) {

        // If the coefficient is actually zero, then we are finished, and can throw this away.
        // TODO: try more than once?
        auto max_rank = calculate_max_rank(total_rank, variable_ranks.at(0));
        if (coordinates.size() > 0) {
            if (current_reconstruction.isZero()) {
                // std::cout << "Newton<" << N << ">: found cancellation. max_rank == " << max_rank << ", size() == " << coordinates.size() << std::endl;
                finished = true;
                return;
            }
        }

        coordinates.push_back({current_x, current_reconstruction});

        // If we just reconstructed the term of highest possible rank,
        // then we do not need to check if we are done.

        if (max_rank >= 0) {
            if ((int)(coordinates.size()) == max_rank + 1) { finished = true; }
        }
    }
}

// We set up the "sub reconstructor" with a subset of the point generators.
template <typename F, size_t N>
MultivariateNewtonFunction<F, N>::MultivariateNewtonFunction(std::array<std::function<F()>, N> _x_gens, int _total_rank,
                                                             std::vector<std::optional<int>> _variable_ranks)
    : x_gens(_x_gens), current_reconstruction(
                           [_x_gens] {
                               std::array<std::function<F()>, N - 1> reduced_x_gens;
                               for (size_t i = 0; i < N - 1; i++) { reduced_x_gens[i] = _x_gens[i]; }
                               return reduced_x_gens;
                           }(),
                           _total_rank, [_variable_ranks] { return std::vector<std::optional<int>>(_variable_ranks.begin() + 1, _variable_ranks.end()); }()),
      total_rank(_total_rank), variable_ranks(_variable_ranks) {

    current_x = x_gens[N - 1]();
    num_self_genned++;
}

template <typename F, size_t N>
MultivariateNewtonFunction<F, N>::MultivariateNewtonFunction(std::function<F(std::array<F, N>)> f, std::array<std::function<F()>, N> _x_gens, int _total_rank,
                                                             std::vector<std::optional<int>> _variable_ranks)
    : MultivariateNewtonFunction(_x_gens, _total_rank, _variable_ranks) {

    while (!finished) {
        auto new_x = next_point().value();
        F new_f = f(new_x);
        add_point(new_f, new_x);

        if (coordinates.size() >= 100) {
            std::cout << "WARNING (MultivariateNewtonFunction<F," << N << "> constructor), current_rank = " << coordinates.size() << std::endl;
        }
    }
}

template <typename F, size_t N> bool MultivariateNewtonFunction<F, N>::isZero() const {
    for (auto& coord : coordinates) {
        if (!coord.second.isZero()) { return false; }
    }
    return true;
}

template <typename F, size_t N> SparseMultivariatePolynomial<F> MultivariateNewtonFunction<F, N>::to_canonical() const {
    auto poly = coordinates.back().second.to_canonical(); // leading term
    poly.add_variable();

    for (int i = coordinates.size() - 2; i >= 0; i--) {
        auto& coord = coordinates.at(i);
        poly.multiply_with_linear(N, -coord.first);
        auto contribution = coord.second.to_canonical();
        contribution.add_variable();
        poly += contribution;
    }

    // Remove any zeros
    for (auto& homopoly : poly.polynomials_by_degree) {
        for (auto it = homopoly.begin(); it != homopoly.end();) {
            if (it->second == F(0)) { it = homopoly.erase(it); }
            else {
                it++;
            }
        }
    }

    return poly;
}

template <typename F, size_t N> MultivariateNewtonFunction<F, N> MultivariateNewtonFunction<F, N>::operator-() const {
    auto result = (*this);
    for (auto& coordinate : result.coordinates) { coordinate.second = -coordinate.second; }
    return result;
}

template <typename F, size_t N> void MultivariateNewtonFunction<F, N>::operator-=(MultivariateNewtonFunction<F, N> other) {
    // Algorithmically, we assume that this and the other use the
    // same basis of polynomials. This is not valid apart from when
    // it is used in the constructor.
    if (other.coordinates.size() <= coordinates.size()) {
        for (size_t i = 0; i < other.coordinates.size(); i++) { coordinates.at(i).second -= other.coordinates.at(i).second; }
    }
    else {
        for (size_t i = 0; i < coordinates.size(); i++) { coordinates.at(i).second -= other.coordinates.at(i).second; }
        for (size_t i = coordinates.size(); i < other.coordinates.size(); i++) {
            auto& new_coordinate = other.coordinates.at(i);
            new_coordinate.second = -new_coordinate.second;
            coordinates.push_back(new_coordinate);
        }
    }
}

template <typename F, size_t N> void MultivariateNewtonFunction<F, N>::operator/=(F x) {
    for (auto& coord : coordinates) { coord.second /= x; }
}

template <typename F, size_t N> size_t MultivariateNewtonFunction<F, N>::get_num_generated() const {
    return coordinates.size();
}

template <typename A, typename B> std::string coordinates_string(const std::vector<std::pair<A, B>>& coordinates, size_t N) {
    auto riffle = [](const std::vector<std::string>& list, std::string delimiter) {
        std::string result = "";
        bool first = true;
        for (auto& el : list) {
            if (!first) { result += delimiter; }
            first = false;
            result += el;
        }
        return result;
    };

    std::vector<std::string> term_strings;
    std::vector<std::string> monomial_strings;

    for (auto& coord : coordinates) {
        std::stringstream buffer;
        buffer << coord.second;
        if (monomial_strings.size() > 0) { buffer << "*"; }
        buffer << riffle(monomial_strings, "*");
        term_strings.push_back(buffer.str());

        buffer.str("");
        buffer.clear();
        buffer << coord.first;
        monomial_strings.push_back("(x_" + std::to_string(N - 1) + " - " + buffer.str() + ")");
    }

    return std::string("(") + riffle(term_strings, " + ") + ")";
}

template <class F, size_t N> std::ostream& operator<<(std::ostream& out, const MultivariateNewtonFunction<F, N>& p) {
    out << coordinates_string(p.coordinates, N);
    return out;
}

////////////////////////////////////////////////////////////////////////////
// Code related to the univariate specialization of the recursion.
////////////////////////////////////////////////////////////////////////////

template <typename F> std::vector<std::array<F, 1>> MultivariateNewtonFunction<F, 1>::guess_points(size_t n) const {
    std::vector<std::array<F, 1>> guesses;
    for (auto& guess : univariate_f.guess_points(n)) { guesses.push_back({guess}); }
    return guesses;
}

template <typename F> std::optional<std::array<F, 1>> MultivariateNewtonFunction<F, 1>::next_point() {
    auto p = univariate_f.next_point();
    if (!p) { return std::optional<std::array<F, 1>>(); }
    else {
        std::array<F, 1> result{p.value()};
        return std::optional<std::array<F, 1>>(result);
    }
}

template <typename F> void MultivariateNewtonFunction<F, 1>::add_point(F f_val, std::array<F, 1> x_val) {
    univariate_f.add_point(f_val, x_val[0]);
    finished = univariate_f.finished;
}

template <typename F>
MultivariateNewtonFunction<F, 1>::MultivariateNewtonFunction(std::function<F(std::array<F, 1>)> f, std::array<std::function<F()>, 1> x_gens, int total_rank,
                                                             std::vector<std::optional<int>> variable_ranks)
    : finished(true), univariate_f([f](F x) { return f({{x}}); }, x_gens[0], calculate_max_rank(total_rank, variable_ranks.at(0))) {}

// Here the coordinates are not given by other polynomials.
template <typename F>
MultivariateNewtonFunction<F, 1>::MultivariateNewtonFunction(std::array<std::function<F()>, 1> x_gens, int total_rank,
                                                             std::vector<std::optional<int>> variable_ranks)
    : univariate_f(x_gens[0], calculate_max_rank(total_rank, variable_ranks.at(0))) {}

template <typename F> MultivariateNewtonFunction<F, 1> MultivariateNewtonFunction<F, 1>::operator-() const {
    auto result = (*this);
    for (auto& coordinate : result.univariate_f.coordinates) { coordinate.second = -coordinate.second; }
    return result;
}

template <typename F> void MultivariateNewtonFunction<F, 1>::operator-=(const MultivariateNewtonFunction<F, 1>& other) {
    // Algorithmically, we assume that this and the other use the
    // same basis of polynomials. This is not valid apart from when
    // it is used in the constructor.
    if (other.univariate_f.coordinates.size() <= univariate_f.coordinates.size()) {
        for (size_t i = 0; i < other.univariate_f.coordinates.size(); i++) {
            univariate_f.coordinates.at(i).second -= other.univariate_f.coordinates.at(i).second;
        }
    }
    else {
        for (size_t i = 0; i < univariate_f.coordinates.size(); i++) { univariate_f.coordinates.at(i).second -= other.univariate_f.coordinates.at(i).second; }
        for (size_t i = univariate_f.coordinates.size(); i < other.univariate_f.coordinates.size(); i++) {
            auto new_coordinate = other.univariate_f.coordinates.at(i);
            new_coordinate.second = -new_coordinate.second;
            univariate_f.coordinates.push_back(new_coordinate);
        }
    }
}

template <class F> std::ostream& operator<<(std::ostream& out, const MultivariateNewtonFunction<F, 1>& p) {
    out << coordinates_string(p.univariate_f.coordinates, 1);
    return out;
}

////////////////////////////////////////////////////////////////////////////
// Code related to the vector valued multivariate polynomial reconstruction
////////////////////////////////////////////////////////////////////////////

template <class F, size_t N>
VectorMultivariateNewtonFunction<F, N>::VectorMultivariateNewtonFunction(std::function<std::vector<F>(std::array<F, N>)> f,
                                                                         const std::array<std::function<F()>, N>& x_gens) {

    auto cached_f = memoize(f);

    auto zeroth_component = [&cached_f](std::array<F, N> xs) mutable { return cached_f(xs).at(0); };
    MultivariateNewtonFunction<F, N> component0(zeroth_component, x_gens);
    components.push_back(component0);

    // Get length of the function by peaking into the table. Note
    // that we must copy the x_gens, in order not to move them
    // forward.

    auto x_gens_copy = x_gens;
    std::array<F, N> xs_val;
    for (size_t i = 0; i < N; i++) { xs_val[i] = x_gens_copy[i](); };
    auto f_val = cached_f(xs_val);
    auto length = f_val.size();

    // Reconstruct all other components.
    for (size_t i = 1; i < length; i++) {
        auto f_component = [&cached_f, i](std::array<F, N> xs) mutable { return cached_f(xs).at(i); };
        components.push_back(MultivariateNewtonFunction<F, N>(f_component, x_gens));
    }
}

} // namespace Caravel
