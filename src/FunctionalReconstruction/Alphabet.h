#ifndef _ALPHABET_H_
#define _ALPHABET_H_

#include <fstream>
#include <functional>
#include <string>
#include <vector>
#include <assert.h>

#include "Core/typedefs.h"
#include "FunctionalReconstruction/DensePolynomial.h"
#include "FunctionalReconstruction/DenseRational.h"

namespace Caravel {

using F = Caravel::F32;

/// An alias for a univariate slice.
using Slice = std::function<std::vector<F>(const F&)>;

/**
 * An alias for a monomial in terms of letters.
 *
 * It contains in a vector for each letter contributing to the
 * monomial its index in the alphabet as well as its power in
 * the monomial
 */
using IndexPower = std::vector<std::pair<std::size_t, int>>;

// Forward declaration
class Alphabet;

class LetterMonomial {

    friend class Alphabet;
    friend bool operator == (const LetterMonomial& l1, const LetterMonomial& l2);

  public:
    /// Constructor
    LetterMonomial(const std::string& _alphabet_name, const std::string& _string_rep, const IndexPower& _ip);
    
    /// Construct from string.
    LetterMonomial(const std::string& mathlist);

    /// Return the string form of the LetterMonomial.
    std::string as_string() const;

    /// Return the LetterMonomial as MathList
    std::string as_mathlist() const;

    /// Get the decomposition of the Monomial into its letters and their powers
    const IndexPower& get_index_powers() const;

    /// Return the name of the alphabet the letters in the monomial belong to.
    std::string get_alphabet_name() const;

  private:
    /// The name of the alphabet the letter belongs to.
    std::string alphabet_name;

    /// The string representation of the letter.
    std::string string_rep;

    /// Store, for each letter contributing to the monomial, its index
    /// in the alphabet and its power in the monomial. Denominator powers
    /// are counted negatively.
    IndexPower index_powers;
};

/// Allow to compare letter monomials for equality.
bool operator == (const LetterMonomial& l1, const LetterMonomial& l2);

/// Allow to compare letter monomials for inequality.
bool operator != (const LetterMonomial& l1, const LetterMonomial& l2);

/// Read a vector of letter monomials from file.
std::vector<LetterMonomial> read_letter_monomials_from_file(const std::string& filename);

/// Write a vector of letter monomials to a file.
void write_letter_monomials_to_file(const std::string& filename, std::vector<LetterMonomial> monomials);

/**
 * Print the LetterMonomial to a stream.
 *
 * @param stream   The stream the monomial shall be printed to.
 * @param monomial The monomial to be printed.
 * @return         The filled stream.
 */
std::ostream& operator<<(std::ostream& stream, const LetterMonomial& monomial);

class Alphabet {

    using Poly = DensePolynomial<F>;
    using Rat = Ratio<Poly>;

  public:
    /// Virtual destructor since this a virtual basis class.
    virtual ~Alphabet() = default;

    /// Get number of letters.
    std::size_t n_letters() const;

    /// Reconstruct the letters of the alphabet on a univariate slice.
    std::vector<Poly> reconstruct_on_slice(const Slice& slice) const;

    /// Return the name of the alphabet.
    std::string name() const;

    /// Factorize the letters appearing in the coefficient on the provided slice.
    LetterMonomial factorize_letters(const Poly& coeff, const Slice& slice);

    /// Factorize the letters appearing in the coefficients on the provided slice.
    std::vector<LetterMonomial> factorize_letters(const std::vector<Poly>& coeffs, const Slice& slice);

    /// Factorize the letters appearing in the coefficient on the provided slice.
    LetterMonomial factorize_letters(const Rat& coeff, const Slice& slice);

    /// Factorize the letters appearing in the coefficients on the provided slice.
    std::vector<LetterMonomial> factorize_letters(const std::vector<Rat>& coeffs, const Slice& slice);

    /// Evaluate a LetterMonomial on a specific phase space point.
    F evaluate_letter(const LetterMonomial& letter, const std::vector<F>& xs) const;

    /// Evaluate several LetterMonomials on a specific phase space point.
    std::vector<F> evaluate_letters(const std::vector<LetterMonomial>& letter_vec, const std::vector<F>& xs) const;

     /// Evaluate all the letters in the alphabet on a given phase space point.
    std::vector<F> eval_letters_on_psp(const std::vector<F>& x) const;

    /// Store string representations of letters.
    std::vector<std::string> letter_strings{};

  protected:
    /// Store the letters as functions that can be evaluated.
    std::vector<std::function<F(const std::vector<F>&)>> letters{};

    /// The name of the alphabet
    std::string _name;

    /**
     * Given a monomial construct its string representation.
     *
     * @param monomial Contains for each letter contributing to the monomial, its index in the alphabet and its power
     *                 in the monomial.
     * @return         The string representation of the monomial.
     */
    std::string _stringify_polynomial(const IndexPower& polynomial);

    /// Factorize the provided letters appearing in the coefficient.
    LetterMonomial _factorize_letters(const Poly& coefficient, const std::vector<Poly>& letters);

    /// Factorize the provided letters appearing in the coefficient.
    LetterMonomial _factorize_letters(const Rat& coeff, const std::vector<Poly>& letters);
};

} // namespace Caravel

#endif // _ALPHABET_H_
