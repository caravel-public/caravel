#ifndef THIELE_FUNCTION_H_INC
#define THIELE_FUNCTION_H_INC

#include <complex>
#include <vector>
#include <functional>
#include "DenseRational.h"
#include <optional>

namespace Caravel{

/**
 * Class representing a univariate rational function reconstructed using Thiele's algorithm.
 */
template< typename F >
class ThieleFunction{
    std::function<F()> x_gen;

    /**
      * Creates a copy of the reconstruction object, but forgetting
      * all point data. Useful for guess_points.
      */
    ThieleFunction<F> clone_no_data() const;
  public:
    size_t num_genned{0};

  /**
   * Guesses the next n points which the algorithm will
   * request. Cannot know beforehand if the algorithm will need fewer.
   */
  std::vector<F> guess_points(size_t n) const;

  std::optional<F> next_point();
  void add_point(F new_f, F new_x);
  bool finished{false};

  std::vector<std::pair<F,F>> coordinates;
  std::vector<std::pair<F,F>> inputs;
  std::string var_name;

  /**
   * Constructor which automatically reconstructs the given function
   */
  [[deprecated("Use non automatic version instead.")]]
  ThieleFunction(std::function<F(F)> f, std::function<F()> x_gen, std::string var_name = "x");

  /**
   * Constructor which sets up a function to be "fed" input data through add_point.
   */
  ThieleFunction(std::function<F()> x_gen, std::string _var_name = "x");

 /**
  * Evaluates the reconstructed function on a given point x.
  */
  F operator ()(F x);

  /**
   * Converts the result to "canonical form", i.e. a ratio of two "canonical"
   * polynomials. Furthermore, the lowest order term in the denominator
   * polynomial is normalized to have unit coefficient.
   */
  Ratio< DensePolynomial<F> > to_canonical() const;

};

/**
 * A class which performs Thiele's algorithm on a vector valued univariate function.
 */
template< class F >
class VectorThieleFunction{
    std::function<F()> x_gen;
    std::string var_name;

public:

  /**
   * Boolean announcing whether or not the reconstruction has been completed.
   */
    bool finished{false};
  
  
  // TODO: Make private
  std::vector<ThieleFunction<F>> components;

  /**
   * Constructor which for running algorithm through add_point/next_point idiom.
   * @param x_gen random point generator
   * @param var_name optional name of variable
   */
  VectorThieleFunction(std::function<F()> x_gen, std::string var_name = "x");

  /**
   * Constructor which automatically reconstructs the vector valued function f.
   * @param f function to reconstruct
   * @param x_gen random point generator
   * @param var_name optional name of variable
   * @param run_in_parallel optional flag to parallelize the calls to f. Assumes f to be thread-safe.
   */
  VectorThieleFunction(std::function<std::vector<F>(F)> f, std::function<F()> x_gen, std::string var_name = "x", bool run_in_parallel = false);

  /**
   * Constructor which automatically reconstructs the vector valued function f.
   * @param vector of thread safe copies of f
   */
  VectorThieleFunction(std::vector<std::function<std::vector<F>(F)>>&& f, std::function<F()> x_gen, std::string var_name = "x", bool run_in_parallel = false);


  /**
   * Converts the results to DenseRationals. Only to be used when finished is true.
   */
  std::vector<DenseRational<F>> to_canonical();

  /**
   * Add a computed value of the function to the reconstruction in
   * progress. x_val must mach the value returned by the call of
   * next_point() performed after the last call to add_point().
   * @param f_val The vector valued output of the function being reconstructed.
   * @param x_val The value of x upon which the function was evaluated.
   */
  void add_point(const std::vector<F>& f_val, F x_val);

  /**
   * Returns the next point on which the function is to be evaluated
   */
  std::optional<F> next_point();

  /**
   * Guesses n points which we need to evaluate next in order to
   * continue the reconstruction. Useful for precomputation when
   * parallelizing.
   */
  std::vector<F> guess_points(size_t n) const;
 };

}

#include "ThieleFunction.hpp"

#endif
