namespace Caravel {
namespace Reconstruction {

template <typename T> RationalFlattener<T>::RationalFlattener(std::vector<DenseRational<T>>&& input) : n_rational_functions{input.size()}, n_flattened{0} {

    for (auto&& rational : input) {
        // Extract and store the numerator and denominator degree of each rational function.
        auto num_denom_degree = std::make_pair(rational.numerator.get_degree(), rational.denominator.get_degree());
        num_denom_degrees.push_back(num_denom_degree);

        // Flatten the coefficients and check for zeros
        auto flatten = rational.flatten();
        for (const auto& coeff : flatten) {
            if (coeff == T(0)) {
                dropped_zeros += 1;
                zero_tagging.push_back(true);
            }
            else {
                n_flattened += 1;
                zero_tagging.push_back(false);
            }
        }
    }

    // Count the number of dropped zeros.
    dropped_zeros = 0;
    for (const auto& elem : zero_tagging) {
        if (elem) { dropped_zeros += 1; }
    }
}

template <typename T> bool RationalFlattener<T>::operator==(const RationalFlattener<T>& other) const {
    if (n_rational_functions != other.n_rational_functions) { return false; }
    if (n_flattened != other.n_flattened) { return false; }
    if (dropped_zeros != other.dropped_zeros) { return false; }
    if (zero_tagging != other.zero_tagging) { return false; }
    if (num_denom_degrees != other.num_denom_degrees) { return false; }
    return true;
}

template <typename T> bool RationalFlattener<T>::operator!=(const RationalFlattener<T>& other) const {
    return !(*this == other);
}

template <typename T> std::vector<T> RationalFlattener<T>::operator()(const std::vector<DenseRational<T>>& input) const {
    std::vector<T> to_return;

    assert(input.size() == n_rational_functions && "Size of input is not compatible with this RationalFlattener!");

    std::size_t position = 0;

    for (const auto& rational : input) {
        auto flattened = rational.flatten();
        // Add each coefficient in the flattened vector to the output vector if it is not tagged as zero.
        for (const auto& coeff : flattened) {
            if (!zero_tagging[position]) { to_return.push_back(coeff); }
            ++position;
        }
    }

    assert(position == zero_tagging.size() && "RationalFlattener<T>::operator() something went wrong in the counting!");

    return to_return;
}

template <typename T>
template <typename REC>
std::vector<DenseRational<REC>> RationalFlattener<T>::unflatten(const std::vector<REC>& input, const std::string& variable) const {
    // Consistency
    assert(input.size() == n_flattened && "Input size not compatible with this flattener. Error in RationalFlattener<T>::unflatten()!");
    
    // First, reinsate the zeros.
    std::vector<REC> with_zeros;

    // Track position in input
    std::size_t input_position = 0;

    for (auto flag: zero_tagging) {
        if (flag) {
            with_zeros.emplace_back(0);
        } else {
            with_zeros.push_back(input[input_position]);
            ++input_position;
        }
    }

    assert(input_position == n_flattened);

    // Collect terms for each coefficient
    std::vector<DenseRational<REC>> to_return;

    std::size_t with_zeros_position = 0;


    for (const auto& elem: num_denom_degrees) {
        auto start = with_zeros.begin() + with_zeros_position;
        auto end = with_zeros.begin() + with_zeros_position + elem.first + elem.second + 2;
        auto tmp = Caravel::unflatten(elem.first + 1, elem.second + 1, std::vector<REC>(start, end));
        tmp.numerator.var_name = variable;
        tmp.denominator.var_name = variable;
        to_return.push_back(tmp);
        with_zeros_position += elem.first + elem.second + 2;
    }


    return to_return;
}

} // namespace Reconstruction.
} // namespace Caravel
