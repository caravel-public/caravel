#include "Core/typedefs.h"
#include "misc/ParallelTools.h"

namespace Caravel{
  template<typename F>
  Ratio< DensePolynomial<F> > ThieleFunction<F>::to_canonical() const{
    DensePolynomial<F> numerator({coordinates.back().second}, var_name);
    DensePolynomial<F> denominator({F(1)}, var_name);

    for (int i = coordinates.size() - 2; i >= 0; i--){
      auto& coord = coordinates.at(i);
      auto linear_multiplied_denom = denominator.multiply_with_linear(-coord.first);
      denominator = numerator;
      numerator = coord.second * numerator + linear_multiplied_denom;
    }

    // Find leading denominator coefficient
    auto leading_denom_coeff = [&denominator](){
      for (auto& coord : denominator.coefficients){
	if (coord != F(0)){return coord;}
      }
      std::cerr << "ERROR: No non-zero term in denominator." << std::endl;
      exit(1);
    }();

    // Normalize by it.
    auto norm = F(1)/leading_denom_coeff;
    for (auto& el: numerator.coefficients  ){el*=norm;}
    for (auto& el: denominator.coefficients){el*=norm;}

    // Chop off unnecessary zeros
    while (numerator.coefficients.size() > 0 && numerator.coefficients.back() == F(0)){
      numerator.coefficients.pop_back();
    }
    while (denominator.coefficients.size() > 0 && denominator.coefficients.back() == F(0)){
      denominator.coefficients.pop_back();
    }
    if (numerator.coefficients.size() == 0){
      numerator.coefficients.push_back(F(0));
    }

    Ratio<DensePolynomial<F>> canonical_rational(numerator, denominator);
    
    return canonical_rational;
    
  }

namespace detail{

  /**
   * Finite field templated are_equals.
   */
    #ifdef USE_FINITE_FIELDS
    template<typename T> std::enable_if_t<is_exact<T>::value, bool>
    are_equal(T x, T y){return x == y;}

  //template<typename T> std::enable_if_t<is_exact<T>::value, bool>
  //  are_equal(std::complex<T>& x, std::complex<T>& y){
  //  return x.real() == y.real() and x.imag() == y.imag();
  //}
    #endif //USE_FINITE_FIELDS

    template<typename T> std::enable_if_t<!is_exact<T>::value, bool>
	  are_equal(T x, T y){
      return std::abs(x-y) < 1e-10;
    }
}

template< typename F >
F thiele_function_evaluate(const std::vector<std::pair<F,F>>& coordinates, F x){
	auto val = coordinates.back().second;
	for (int i = coordinates.size()-2; i >= 0; i--){
	  auto& coord = coordinates.at(i);
	  val = (x - coord.first)/val + coord.second;
	}
	return val;
}

template<typename F>
void ThieleFunction<F>::add_point(F new_f, F new_x){
    inputs.push_back({new_x, new_f});

    // Check if completed.
    if (coordinates.size() > 0){
        auto guess_val = thiele_function_evaluate(coordinates, new_x);
	if (detail::are_equal(new_f, guess_val)){
            finished=true;
            return;
        }
    }

    // Calculate next a
    F new_a = new_f;
    for (auto& coord : coordinates){
        new_a = (new_x - coord.first)/(new_a - coord.second);
    }

    coordinates.push_back({new_x, new_a});

    if (inputs.size() > 1000){
	std::cout << "WARNING (ThieleFunction constructor), requires > 1000 function samples to reconstruct" << std::endl;
    }
}

template<typename F>
ThieleFunction<F> ThieleFunction<F>::clone_no_data() const{
  ThieleFunction<F> result(*this);
  result.coordinates.clear();
  result.inputs.clear();
  return result;
}

template<typename F>
std::vector<F> ThieleFunction<F>::guess_points(size_t n) const{
  auto copy = clone_no_data();
  std::vector<F> guesses;

  for (size_t i = 0; i < n; i++){
    guesses.push_back(copy.next_point().value());
  }
   
  return guesses;
}

template<typename F>
std::optional<F> ThieleFunction<F>::next_point(){
  if (finished){return std::optional<F>();}
  return std::optional<F>(x_gen());
}

template<typename F>
  ThieleFunction<F>::ThieleFunction(std::function<F()> _x_gen, std::string _var_name) : x_gen(_x_gen), var_name(_var_name){}

template<typename F>
ThieleFunction<F>::ThieleFunction(std::function<F(F)> f, std::function<F()> _x_gen, std::string _var_name) : ThieleFunction(_x_gen, _var_name){

    while (!finished){
	try{
	    auto new_x = next_point().value();
	    F new_f = f(new_x);
            add_point(new_f, new_x);
        }
        catch (const DivisionByZeroException& div_by_zero) {
            // Throw away x value and try again, if things go awry.
            inputs.pop_back();
            continue;
        }
        catch (const NonQuadraticResidueException& non_quad_residue) {
            // Throw away x value and try again, if things go awry.
            inputs.pop_back();
            continue;
        }
    }
}

template<typename F>
  F ThieleFunction<F>::operator ()(F x){
    if (finished){
	return thiele_function_evaluate(coordinates, x);
    }
    else{
      throw std::runtime_error("Error in ThieleFunction - attempted to evaluate when reconstruction not finished.");
    }
  }

  template<typename F>
  std::optional<F> VectorThieleFunction<F>::next_point(){
    return std::optional<F>(x_gen());
  }

  template<typename F>
  std::vector<F> VectorThieleFunction<F>::guess_points(size_t n) const{
    // Simply copy the point generator and run it forward. Easy in this case
    auto copy = x_gen;
    std::vector<F> result;
    for (size_t i = 0; i < n; i++){result.push_back(copy());}
    return result;
  }


  template<typename F>
  void VectorThieleFunction<F>::add_point(const std::vector<F>& f_val, F x_val){
    // When adding the first point, we don't yet know the length
    // of the vector, so we have to set up here.
    if (components.size() == 0) {
        // Note that x_gen is copied in for everyone. Guaranteeing alignment, which is not checked.
        components.resize(f_val.size(), ThieleFunction<F>(x_gen, var_name));
    }

    // Feed each component with a new point
    for (size_t i = 0; i < f_val.size(); i++) {
        components.at(i).add_point(f_val.at(i), x_val);
    }

    // We are finished if all of the components are finished.
    bool all_done = true;
    for (auto& component : components) {
        if (!component.finished) { all_done = false; }
    }

    if (all_done) { finished = true; }
  }

  template<typename F>
  VectorThieleFunction<F>::VectorThieleFunction(std::function<F()> _x_gen, std::string _var_name)
    : x_gen(_x_gen), var_name(_var_name){}

  template<typename F>
    VectorThieleFunction<F>::VectorThieleFunction(std::function<std::vector<F>(F)> g, std::function<F()> _x_gen, std::string _var_name, bool run_in_parallel) : VectorThieleFunction(_x_gen, _var_name){
    auto f = g;
    if (run_in_parallel){f = block_computer(g, x_gen);}

    while (!finished) {
        try {
            auto x_val = next_point().value();
            auto f_val = f(x_val);
            add_point(f_val, x_val);
        }
        catch (const DivisionByZeroException& div_by_zero) {
            continue;
        }
        catch (const NonQuadraticResidueException& non_quad_residue) {
            continue;
        }
    }
 }

  template<typename F>
  VectorThieleFunction<F>::VectorThieleFunction(std::vector<std::function<std::vector<F>(F)>>&& g, std::function<F()> _x_gen, std::string _var_name, bool run_in_parallel) : VectorThieleFunction(_x_gen, _var_name){

      typename std::remove_reference<decltype(g.front())>::type f;

      if (run_in_parallel) {
          f = block_computer(std::move(g), x_gen);
      } else {
          f = std::move(g.front());
      }

      while (!finished) {
          auto x_val = next_point().value();
          auto f_val = f(x_val);
          add_point(f_val, x_val);
    }
    
 }

  template<typename F>
  std::vector<DenseRational<F>> VectorThieleFunction<F>::to_canonical(){
    std::vector<DenseRational<F>> result;
    for (auto& f : components){
      if (!f.finished){
        throw std::runtime_error("Error in VectorThieleFunction::to_canonical: attempted to canonicalize incomplete reconstruction.");
      }

      result.push_back(f.to_canonical());
    }

    return result;
  }

  

}
