#include <algorithm>
namespace Caravel{
    template<typename F>
    DenseRational<F> InvertingRationalReconstructor<F>::to_canonical() const{
	return canonical_form;
    }
  
    template<typename F>
    void InvertingRationalReconstructor<F>::add_point(F new_f, F new_x){
      inputs.push_back({new_x, new_f});

      if (inputs.size() == numerator_degree + denominator_degree + 1){
	canonical_form = fit_univariate_rational(inputs, numerator_degree, denominator_degree);
	finished = true;
      }
    }

    template<typename F>
    std::optional<F> InvertingRationalReconstructor<F>::next_point(){
      if (finished || points_genned >= numerator_degree + denominator_degree + 1){
	return std::optional<F>();
      }
      points_genned++;
      return std::optional<F>(x_gen());
    }

  template<typename F>
  InvertingRationalReconstructor<F>::InvertingRationalReconstructor(std::function<F()> _x_gen,
								    size_t _numerator_degree,
								    size_t _denominator_degree,
								    std::string _var_name)
    : x_gen(_x_gen), numerator_degree(_numerator_degree), denominator_degree(_denominator_degree),
      var_name(_var_name) {}
}
