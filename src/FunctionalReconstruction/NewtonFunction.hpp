#include <algorithm>
#include <exception>
#include <functional>

namespace Caravel {

template <typename F> F newton_function_evaluate(const std::vector<std::pair<F, F>>& coordinates, F x) {
    auto val = coordinates.back().second;
    for (int i = coordinates.size() - 2; i >= 0; i--) {
        auto& coord = coordinates.at(i);
        // NOTE: Inverse of Thiele procedure
        val = val * (x - coord.first) + coord.second;
    }
    return val;
}

template <typename F> NewtonFunction<F>::NewtonFunction(std::function<F()> _x_gen, int _max_rank, std::string _var_name) : x_gen(_x_gen), max_rank(_max_rank), var_name(_var_name) {}

template <typename F> NewtonFunction<F>::NewtonFunction(std::function<F(F)> f, std::function<F()> _x_gen, int _max_rank, std::string _var_name) : NewtonFunction(_x_gen, _max_rank, _var_name) {
    while (!finished) {
        auto new_x = next_point();
        F new_f = f(new_x.value());
        add_point(new_f, new_x.value());

        if (coordinates.size() == 1000) { std::cout << "WARNING (NewtonFunction constructor), requires >= 1000 function samples to reconstruct" << std::endl; }
    }
}

template <typename F> NewtonFunction<F> NewtonFunction<F>::clone_no_data() const {
    auto result = NewtonFunction<F>(*this);
    result.coordinates.clear();
    result.inputs.clear();

    return result;
}

// Generic implementation
template <typename F> std::vector<F> NewtonFunction<F>::guess_points(size_t n) const {
    auto copy = clone_no_data();
    std::vector<F> results;

    for (size_t i = 0; i < n; i++) {
        auto new_point = copy.next_point();
        if (!new_point) { break; }
        results.push_back(new_point.value());
    }

    return results;
}

template <typename F> std::optional<F> NewtonFunction<F>::next_point() {
    if (finished || (max_rank >= 0 && int(num_genned) > max_rank)) { return std::optional<F>(); }

    num_genned++;
    return std::optional<F>(x_gen());
}

template <typename F> void NewtonFunction<F>::add_point(F new_f, F new_x) {
#ifdef DEBUG
    inputs.push_back({new_x, new_f});
#endif // DEBUG

    // Check if completed.
    if (coordinates.size() > 0) {
        auto guess_val = newton_function_evaluate(coordinates, new_x);
        // NOTE: Need some way to deal with accidental zeros.
        if (new_f == guess_val) {
            // if ((int)(coordinates.size()) != max_rank){
            //  std::cout << "Newton<1>: Found cancellation. max_rank = " << max_rank << ", coordinates.size() == " << coordinates.size() << std::endl;
            //}
            finished = true;
            return;
        }
    }

    // Calculate next a (NOTE: Inverse of Thiele procedure)
    F new_a = new_f;
    for (auto& coord : coordinates) {
        if (new_x == coord.first) {
            std::cerr << "new_x = " << new_x << std::endl;
            std::cerr << "coord.first = " << coord.first << std::endl;
            throw std::runtime_error("Coincident input points in reconstruction.");
        }
        new_a = (new_a - coord.second) / (new_x - coord.first);
    }

    coordinates.push_back({new_x, new_a});

    if ((int)(coordinates.size()) == max_rank + 1) { finished = true; }
}

template <typename F> F NewtonFunction<F>::operator()(F x) {
    if (finished) { return newton_function_evaluate(coordinates, x); }
    else {
        throw std::runtime_error("Error in NewtonFunction - attempted to evaluate when reconstruction not finished.");
    }
}

template <typename F> DensePolynomial<F> NewtonFunction<F>::to_canonical() const {
    DensePolynomial<F> poly({coordinates.back().second}, var_name);

    for (int i = coordinates.size() - 2; i >= 0; i--) {
        auto& coord = coordinates.at(i);
        poly = (poly.multiply_with_linear(-coord.first));
        poly.coefficients.at(0) += coord.second;
    }

    return poly;
}

template <typename F> std::optional<F> VectorNewtonFunction<F>::next_point() { return std::optional<F>(x_gen()); }

template <typename F> std::vector<F> VectorNewtonFunction<F>::guess_points(size_t n) const {
    // Simply copy the point generator and run it forward. Easy in this case
    auto copy = x_gen;
    std::vector<F> result;
    for (size_t i = 0; i < n; i++) { result.push_back(copy()); }
    return result;
}

template <typename F> void VectorNewtonFunction<F>::add_point(const std::vector<F>& f_val, F x_val) {
    // When adding the first point, we don't yet know the length
    // of the vector, so we have to set up here.
    if (components.size() == 0) {
        // Note that x_gen is copied in for everyone. Guaranteeing alignment, which is not checked.
        components.resize(f_val.size(), NewtonFunction<F>(x_gen, -1, var_name));
    }

    // Feed each component with a new point
    for (size_t i = 0; i < f_val.size(); i++) { components.at(i).add_point(f_val.at(i), x_val); }

    // We are finished if all of the components are finished.
    bool all_done = true;
    for (auto& component : components) {
        if (!component.finished) { all_done = false; }
    }

    if (all_done) { finished = true; }
}

template <typename F> VectorNewtonFunction<F>::VectorNewtonFunction(std::function<F()> _x_gen, std::string _var_name) : x_gen(_x_gen), var_name(_var_name) {}

template <typename F>
VectorNewtonFunction<F>::VectorNewtonFunction(std::function<std::vector<F>(F)> g, std::function<F()> _x_gen, std::string _var_name, bool run_in_parallel)
    : VectorNewtonFunction(_x_gen, _var_name) {
    auto f = g;
    if (run_in_parallel) { f = block_computer(g, x_gen); }

    while (!finished) {
        try {
            auto x_val = next_point().value();
            auto f_val = f(x_val);
            add_point(f_val, x_val);
        }
        catch (const DivisionByZeroException& div_by_zero) {
            continue;
        }
        catch (const NonQuadraticResidueException& non_quad_residue) {
            continue;
        }
    }
}

template <typename F>
VectorNewtonFunction<F>::VectorNewtonFunction(std::vector<std::function<std::vector<F>(F)>>&& g, std::function<F()> _x_gen, std::string _var_name,
                                              bool run_in_parallel)
    : VectorNewtonFunction(_x_gen, _var_name) {

    typename std::remove_reference<decltype(g.front())>::type f;

    if (run_in_parallel) { f = block_computer(std::move(g), x_gen); }
    else { f = std::move(g.front()); }

    while (!finished) {
        auto x_val = next_point().value();
        auto f_val = f(x_val);
        add_point(f_val, x_val);
    }
}

template <typename F> std::vector<DensePolynomial<F>> VectorNewtonFunction<F>::to_canonical() {
    std::vector<DensePolynomial<F>> result;
    for (auto& f : components) {
        if (!f.finished) { throw std::runtime_error("Error in VectorNewtonFunction::to_canonical: attempted to canonicalize incomplete reconstruction."); }

        result.push_back(f.to_canonical());
    }

    return result;
}

} // namespace Caravel
