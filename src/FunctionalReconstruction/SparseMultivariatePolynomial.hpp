#include "Core/typedefs.h"

#include "Core/MathInterface.h"
#include "Core/AlgebraicStructures.h"

namespace Caravel{

template <typename F>
SparseMultivariatePolynomial<F>::SparseMultivariatePolynomial(const MathList& mlist){
    assert_head(mlist, "Polynomial");
    for (auto& termString : mlist){
        MathList term(termString, "Term");

        MathList monomialList(term[0], "Monomial");

        Monomial monomial;
        for (auto& var : monomialList){
            size_t degree = std::stoi(var);
            monomial.push_back(degree);
        }

        Constant coefficient(term[1]);

        (*this) += PolynomialTerm<F>({monomial, coefficient.get_num<F>()});
    }
}

template<typename F>
void SparseMultivariatePolynomial<F>::homogeneous_ax(F a, HomogeneousPolynomial<F>& x){
    for (auto& var : x){
    var.second *= a;
    }
}

template<typename F>
void SparseMultivariatePolynomial<F>::multiply_with_linear(size_t z_i, F z_0){
      // Multiply with linear going from leading to lowest degree.

      auto leading_degree = multiply_by_variable(polynomials_by_degree.back(), z_i);
      polynomials_by_degree.push_back(leading_degree);

      for (int i = polynomials_by_degree.size() - 2; i > 0; i--){
          homogeneous_ax(z_0, polynomials_by_degree.at(i));
          auto z_times_lower = multiply_by_variable(polynomials_by_degree.at(i-1), z_i);
          homogeneous_sum(polynomials_by_degree.at(i), z_times_lower);
      }

      homogeneous_ax(z_0, polynomials_by_degree.at(0));
    }

template<typename F>
void SparseMultivariatePolynomial<F>::homogeneous_sum(HomogeneousPolynomial<F>& x, const HomogeneousPolynomial<F>& y){
	  // Temporarily deprecate as doesn't work with empty polynomials
	  //#ifdef BH_DEBUG
	  //auto& x_candidate = x.begin()->first;
	  //auto x_num_vars = x_candidate.size();
	  //auto& y_candidate = y.begin()->first;
	  //auto y_num_vars = y_candidate.size();
  	  //if (x_num_vars != y_num_vars){
	  //  std::cerr << "ERROR: Trying to add HomogeneousPolynomials with differing number of variables." << std::endl;
	  //  exit(1);
	  //}

	  //auto x_degree = std::accumulate(x_candidate.begin(), x_candidate.end(), 0);
	  //auto y_degree = std::accumulate(y_candidate.begin(), y_candidate.end(), 0);
	  //if (x_degree != y_degree){
	  //  std::cerr << "ERROR: Trying to add homogeneousPolynomials with different orders" << std::endl;
	  //  exit(1);
	  //}
	  //#endif //BH_DEBUG

	  // add coincident elements
	  for (auto& monomial : x){
	    auto y_val = y.find(monomial.first);
	    if (y_val != y.end()){
	      monomial.second += y_val->second;
	    }
	  }

	  // add missing elements.
	  x.insert(y.begin(), y.end());
}

template<typename F>
HomogeneousPolynomial<F> SparseMultivariatePolynomial<F>::multiply_by_variable(const HomogeneousPolynomial<F>& p, size_t z_i){
    HomogeneousPolynomial<F> result;
    for (auto& monomial : p){
	auto new_power = monomial.first;
	new_power.at(z_i-1)++;
	result[new_power] = monomial.second;
    }
    return result;
}

template<typename F>
template<size_t N>
void SparseMultivariatePolynomial<F>::shift(std::array<F,N> z){
    auto binomial = [](int n, int k){
	F num(1), den(1);
	for (int i = 1; i <= k; i++){
	    num *= F(n+1-i);
	    den *= F(i);
	}

	return num/den;
    };

    for (size_t i = 0; i < N; i++){
	for (auto& homopoly : polynomials_by_degree){
	    for (auto& term : homopoly){
		auto& term_coeff = term.second;
		auto term_degree = int(term.first.at(i));
		auto shift_var = -z[i];
		for (int j = 0; j < term_degree; j++){
		    auto new_monomial = term.first;
		    new_monomial.at(i) = size_t(j);
		    auto new_coefficient = term_coeff*pow(shift_var, term_degree - j)*binomial(term_degree, j);
		    (*this) += make_pair(new_monomial, new_coefficient);
		}
	    }
	}
    }
}

template<class F>
std::ostream& operator <<(std::ostream& out, const SparseMultivariatePolynomial<F>& p){

    

    auto riffle = [](const std::vector<std::string>& list, std::string delimiter){
	std::string result = "";
	bool first = true;
	for (auto& el : list){
	    if (!first){
		result += delimiter;
	    }
	    first = false;
	    result += el;
	}
	return result;
    };

    auto term_string = [riffle, &p](std::pair<std::vector<size_t>, F> term){
        std::vector<std::string> elements;
	auto& coefficient = term.second;

	if (!(coefficient == F(1))){
	  std::stringstream buffer;
	  buffer << coefficient;
	  elements.push_back(buffer.str());
	}

	auto& monomial = term.first;
	for (size_t k = 0; k < monomial.size(); k++){
	    if (monomial.at(k) != 0){
	      std::stringstream buffer;

              if (p.variable_names.size() == 0){buffer << "x"<<k;}
              else{buffer << p.variable_names.at(k);}

	      auto& power = monomial.at(k);
	      if (power != 1){buffer<<"^"<<power;}
	      elements.push_back(buffer.str());
	    }
	}

	if (elements.size() == 0){return std::string("1");}
	else{return riffle(elements, "*");}
    };

    auto homogeneous_string = [riffle, term_string](std::unordered_map<std::vector<size_t>, F> polynomial){
        std::vector<std::string> terms;
	for (auto& term : polynomial){
	  if (term.second == F(0)){continue;}
	  terms.push_back(term_string(term));
	}

	return riffle(terms, " + ");
    };

    std::vector<std::string> polys;
    for (auto& homogeneous_polynomial : p.polynomials_by_degree){
      for (auto& term : homogeneous_polynomial){
	if (!(term.second == F(0))){
	    polys.push_back(homogeneous_string(homogeneous_polynomial));
	    break;
	}
      }
    }

    if (polys.size() > 0){
        out << riffle(polys, " + ");
    }
    else{
        out << "0";
    }

  return out;
}

template<typename A, typename B>
SparseMultivariatePolynomial<A> lift(const std::function<A(A,B)>& f, SparseMultivariatePolynomial<A> pa,
				     const SparseMultivariatePolynomial<B>& pb){

    auto& polys_a = pa.polynomials_by_degree;
    auto& polys_b = pb.polynomials_by_degree;
    auto max = std::min(polys_a.size(), polys_b.size());
    for (size_t i = 0; i < max; i++){

        auto& x = polys_a.at(i);
        auto& y = polys_b.at(i);

	// Homogeneous lift
	for (auto& monomial : y){
	    auto x_val = x.find(monomial.first);
	    if (x_val != x.end()){
	      // if it's there, apply
	      x_val->second = f(x_val->second, monomial.second);
	    }
	    else{
	      // If it is not there, this means the coefficient is zero, so compose with 0.
	      x[monomial.first] = f(A(0), monomial.second);
	    }
	}
    }

    // Only runs if other is bigger
    for (size_t i = polys_a.size(); i < polys_b.size(); i++){
	auto& poly_b = polys_b.at(i);
	using HomogeneousPolynomialA = std::unordered_map<std::vector<size_t>, A>;
	HomogeneousPolynomialA poly_a;

	for (auto& monomial_b : poly_b){
	  poly_a[monomial_b.first] = A(monomial_b.second);
	}
      
	polys_a.push_back(poly_a);
    }

    return pa;
}

template<typename A, typename B>
SparseMultivariatePolynomial<B> fmap(const std::function<B(A)>& f, SparseMultivariatePolynomial<A> pa){
    using HomogeneousPolynomialB = std::unordered_map<std::vector<size_t>, B>;

    std::vector<HomogeneousPolynomialB> result_polys;

    for (auto& xa : pa.polynomials_by_degree){
        HomogeneousPolynomialB xb;
        for (auto& monomial : xa){
            xb[monomial.first] = f(monomial.second);
	      }
        result_polys.push_back(xb);
    }

    SparseMultivariatePolynomial<B> res(result_polys);
    res.variable_names = pa.variable_names;
    return res;
}

  template<typename F>
  bool operator ==(const SparseMultivariatePolynomial<F>& pa, const SparseMultivariatePolynomial<F>& pb){

    // Inefficient but simple implementation
    auto diff = pb; diff *= F(-1); diff += pa;

    for (auto& homo_poly : diff.polynomials_by_degree){
        for (auto& term : homo_poly){
          if (term.second != F(0)){return false;}
        }
    }

    return true;

  }

    template<typename F>
    void SparseMultivariatePolynomial<F>::add_variable(size_t degree, std::optional<size_t> pos){
      using std::unordered_map; using std::vector;
      // First degree are naturally empty/zero
      vector<HomogeneousPolynomial<F>> new_polynomials_by_degree(degree);

      for (auto& polynomial : polynomials_by_degree){
        HomogeneousPolynomial<F> new_coordinates;

        for (auto& coordinate : polynomial){
          auto new_monomial = coordinate.first;
          if (!pos){
             new_monomial.push_back(degree);
          }
          else{
            new_monomial.insert(new_monomial.begin()+pos.value(), degree);
          }
          new_coordinates[new_monomial] = coordinate.second;
        }

        new_polynomials_by_degree.push_back(new_coordinates);
      }

      polynomials_by_degree = new_polynomials_by_degree;
    }

    template<typename F>
    void SparseMultivariatePolynomial<F>::operator +=(const SparseMultivariatePolynomial& other){
      auto& other_polys = other.polynomials_by_degree;
      auto max = std::min(other_polys.size(), polynomials_by_degree.size());
      for (size_t i = 0; i < max; i++){
        homogeneous_sum(polynomials_by_degree.at(i), other_polys.at(i));

      }

      // Only runs if other is bigger
      for (size_t i = polynomials_by_degree.size(); i < other_polys.size(); i++){
        polynomials_by_degree.push_back(other_polys.at(i));
      }

    }

    template<typename F>
    void SparseMultivariatePolynomial<F>::operator +=(const PolynomialTerm<F>& term){
        auto polynomial_degree = std::accumulate(term.first.begin(), term.first.end(), 0);

        if (polynomial_degree >= int(polynomials_by_degree.size())){
            polynomials_by_degree.resize(polynomial_degree + 1);
        }

        auto& homo_poly = polynomials_by_degree.at(polynomial_degree);

        auto current_val = homo_poly.find(term.first);
        if (current_val != homo_poly.end()){
          current_val->second += term.second;
        }
        else{
          homo_poly[term.first] = term.second;
        }
    }

    template<typename F>
    F SparseMultivariatePolynomial<F>::operator()(const std::vector<F>& variables) const{
        F result(0);

        for (auto& homo_poly : polynomials_by_degree){
            for(auto& term : homo_poly){
                auto& monomial = term . first;
                F monomial_val(term . second);

                for (size_t i = 0; i < monomial.size(); i++){
                    monomial_val *= prod_pow(variables.at(i), monomial.at(i));
                }

                result += monomial_val;
            }
        }

        return result;
        
    }


    template<typename F>
    SparseMultivariatePolynomial<F> SparseMultivariatePolynomial<F>::homogenize(std::vector<std::string> var_names, size_t new_var_pos){

        SparseMultivariatePolynomial<F> homo_poly;
        homo_poly.variable_names = var_names;

        auto deg = polynomials_by_degree.size();
        for (size_t i = 0; i < deg; i++){
           HomogeneousPolynomial<F> empty;
           std::vector<HomogeneousPolynomial<F>> comp(i, empty);
           comp.push_back(polynomials_by_degree.at(i));
           SparseMultivariatePolynomial<F> p(comp);
           p.add_variable(deg-i-1, new_var_pos);
           homo_poly += p;
        }

        return homo_poly;
    }
}
