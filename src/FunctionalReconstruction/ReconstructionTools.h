#ifndef _RECONSTRUCTION_TOOLS_H_
#define _RECONSTRUCTION_TOOLS_H_

#include <vector>
#include <string>
#include <utility>
#include <memory>
#include <unordered_map>
#include <fstream>
#include <stdexcept>

#include "AmplitudeCoefficients/IntegrandHierarchy.h"

#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/FBCoefficient.h"
#include "IntegralLibrary/FunctionBasisExpansion.h"
#include "IntegralLibrary/MapToBasis.h"

#include "FunctionalReconstruction/ThieleFunction.h"
#include "FunctionalReconstruction/DenseRational.h"
#include "FunctionalReconstruction/DensePolynomial.h"

namespace Caravel {

// Forward declaration
template <typename T, size_t L>
class ReconstructionInputProvider;

/**
 * This class is a copy of an evaluator that provides the reconstruction routines
 * with a vector that contains all the powers of all the basis function coefficients
 * in the dimensional regulator.
 *
 * DO NOT COPY. SAME REASON AS FOR ColourExpandedAmplitude.
 * Tried to remove copy constructor but then I cannot copy my functions into vectors.
 *
 * Such an evaluators takes a phase space point, compute the complete amplitude in a basis of special functions
 * and stores the coefficients of each basis function / special function as a Series in
 * the dimensional regulator \f$ \epsilon \f$. However, this representation is not
 * favourable to perform rational/functional reconstruction since the reconstruction
 * facilities takes iterable input. Therefore the for all the coefficients the different
 * powers in epsilon are mapped to a vector which is then returned to functional
 * reconstruction facilities. This mapping is created at construction time and saved
 * such that in the end, each coefficient can again be associated to the corresponding
 * basis function and power in \f$ \epsilon \f$.
 */
template <typename T, size_t L>
class ReconstructionInputEvaluator {

    friend class ReconstructionInputProvider<T, L>;

public:
    typedef Integrals::StdBasisFunction<C, C> FType;      // The basis function type.
    typedef Integrals::FBCoefficient<Series<T>, T> CType; // The basis coefficient type.
    // Default constructor
    ReconstructionInputEvaluator();

    // Want to disable but have to keep it.
    //
    ReconstructionInputEvaluator(const ReconstructionInputEvaluator<T, L>&);

    /**
     * Default move constructor.
     * @param other The ReconstructionInputProvider to be moved into *this.
     * @return      A ReconstructionInputProvider with the content of other.
     */
    ReconstructionInputEvaluator(ReconstructionInputEvaluator<T, L>&& other);

    /**
     * Default move assignment operator.
     * @param other The instance to be moved into *this.
     * @return      A ReconstructionInputEvaluator whith content of *this.
     */
    ReconstructionInputEvaluator<T, L>& operator=(ReconstructionInputEvaluator<T, L>&& other);

    /// Evaluate the amplitude for a given momentum configuration.
    ///
    /// What is being returned is a vector that contains for each basis function
    /// all the powers in \f[ \epsilon \f] of its coefficients.
    std::vector<T> operator()(const momentumD_configuration<T, 4>& mom_conf) const;

    /**
     * Check whether a function of name `name` belongs to the basis.
     * 
     * @param  name The name of the function which is queried.
     * @return      true if the function is contained, else false.
     */
    bool contains(const std::string& name) const;

private:
    std::function<Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<C,C>>(const momD_conf<T,4>&)> function;

    /// The multiplicity of the process under consideration.
    size_t multiplicity;

    /// Map the coefficient of a given power in epsilon of a basis function coefficient to an index.
    std::unordered_map<std::pair<std::string, int>, unsigned long> function_to_position;

    /// Map an index to a coefficient of a given power in epsilon of a basis function coefficient.
    std::unordered_map<unsigned long, std::pair<std::string, int>> position_to_function;

    Integrals::MapToBasis<T, FType> basis_map;

    /// Store the number of coefficients that appear in the amplitude.
    unsigned long number_of_coefficients;

    /// Contains the names of the functions whose names shall be reconstructed.
    std::vector<std::string> function_names;

    /**
     * Map the coefficients appearing in the NumericFunctionBasisExpansion into a vector that is
     * being returned to the reconstruction facilities.
     *
     * The order in the vector that is being returned is defined internally but the resulting reconstructed coefficients
     * can be mapped back to a FunctionBasisExpansion.
     *
     * @param nfbe The numeric function basis expansion that provides the coefficients of the basis functions.
     * @return     Contains the coefficients in an order that is defined internally.
     */
    std::vector<T>
    map_coefficients_to_vector(const Integrals::NumericFunctionBasisExpansion<Series<T>, std::string>& nfbe) const;
};

/**
 * Class that provides the input to the rational and functional reconstruction.
 *
 * It is given ColourExpandedAmplitude and a phase space point. The phase space point
 * is used to do the warmup of the amplitude (if it has not yet been performed) and
 * therefore it is important that this phase space point is a completely general and
 * random one. <br>
 *
 * Just as for ColourExpandedAmplitude, once this class is instantiated, it can provide
 * any amount of evaluators that can be used in multiple threads. These evaluators take
 * a phase space point, compute the complete amplitude in a basis of special functions
 * and store the coefficients of each basis function / special function as a Series in
 * the dimensional regulator \f$ \epsilon \f$. However, this representation is not
 * favourable to perform rational/functional reconstruction since the reconstruction
 * facilities takes iterable input. Therefore the for all the coefficients the different
 * powers in epsilon are mapped to a vector which is then returned to functional
 * reconstruction facilities. This mapping is created at construction time and saved
 * such that in the end, each coefficient can again be associated to the corresponding
 * basis function and power in \f$ \epsilon \f$.
 */
template <typename T, size_t L>
class ReconstructionInputProvider {

public:

    typedef Integrals::StdBasisFunction<C, C> FType;      // The basis function type.
    typedef Integrals::FBCoefficient<Series<T>, T> CType; // The basis coefficient type.

    /**
     * Construct a ReconstructionInputProvider by handing over a unique pointer to a ColourExpandedAmplitude.
     * @param amp       A unique_ptr to a ColourExpandedAmplitude.
     * @param bmap      A mapper that maps the special functions appearing in the integral to the basis functions.
     * @param moms      A momentum configuration that is used to do the warmup of the amplitude. This phase space point
     *                  should not be a special case but as general as possible in order for the warmup to make sense.
     * @param functions A vector containing the names of the functions whose coefficients shall be reconstructed. Per default,
     *                  it is an empty vector meaning that all coefficients are reconstructed.
     */
    ReconstructionInputProvider(std::function<Integrals::NumericFunctionBasisExpansion<Series<T>, FType>(const momD_conf<T,4>&)> function,
                                Integrals::MapToBasis<T, FType>&& bmap,
                                const momentumD_configuration<T, 4>& moms,
                                const std::vector<std::string>& functions = {});

    /**
     * Construct a ReconstructionInputProvider by handing over a unique pointer to a ColourExpandedAmplitude.
     * @param amp           A unique_ptr to a ColourExpandedAmplitude.
     * @param bmap          A mapper that maps the special functions appearing in the integral to the basis functions.
     * @param mom_configs   Several momentum configurations. The first one is used to do the warmup of the amplitude. 
     *                      This phase space point should not be a special case but as general as possible in order for 
     *                      the warmup to make sense. The remaining points are used to evaluate the amplitude several 
     *                      times and identify functions that always vanish and should therefore be removed for efficiency
     *                      and memory usage.
     * @param functions     A vector containing the names of the functions whose coefficients shall be reconstructed. Per default,
     *                      it is an empty vector meaning that all coefficients are reconstructed.
     */
    ReconstructionInputProvider(std::function<Integrals::NumericFunctionBasisExpansion<Series<T>, FType>(const momD_conf<T,4>&)> function,
                                Integrals::MapToBasis<T, FType>&& bmap,
                                const std::vector<momentumD_configuration<T, 4>>& mom_configs,
                                const std::vector<std::string>& functions = {});


    // Do not allow copying and default construction.
    ReconstructionInputProvider() = delete;
    ReconstructionInputProvider(const ReconstructionInputProvider<T, L>&) = delete;

    /**
     * Provide a copy of an input provider that can be used in a thread.
     * @param functions  A vector with the names of the functions that shall be reconstructed.
     *                   The default is an empty vector, in which case the coefficients of all
     *                   the functions will be reconstructed.
     * @return A ReconstructionInputEvaluator.
     */
    ReconstructionInputEvaluator<T, L>
      get_new_reconstruction_input_evaluator(std::function<Integrals::NumericFunctionBasisExpansion<Series<T>, FType>(const momD_conf<T,4>&)> thread_safe_function_copy) const;

    /**
     * Return the number of coefficients.
     * @return The number of coefficients.
     */
    unsigned long get_n_coefficients() const;

    /**
     * @param coeffs A set of coefficients arranged as the output of
     * "map_coefficients_to_vector".
     * @return Expression with the coefficients in place.
     */
    template<typename Coeff>
    Integrals::NumericFunctionBasisExpansion<Series<Coeff>, std::string>
    to_basis(const std::vector<Coeff>& coeffs) const;

    /**
     * Obtain the position of the coefficient of a given basis function
     * `id` multiplying \f$ \epsilon \f$ to the power `power`.
     * @param  id    The id of the basis function the position is requested for.
     * @param  power The power in \f$ \epsilon \f$ the position is requested for.
     * @return       The position of the requested coefficient in the output vector.
     */
    unsigned long get_position_of(const std::string& id, const int& power) const;

    /**
     * Obtain the function and the power in the laurent expansion a coefficient in the output belongs to.
     * @param index The index of the coefficient in the ouptut vector.
     * @return      A pair containing the power in the Laurent expansion and the string representation of the
     *              basis function the coefficient is associated to.
     */ 
    std::pair<std::string, int> get_function_and_power(const std::size_t& index) const;

    /**
     * Print the map of indices to coefficients to a stream.
     * @param stream The stream the information is going to be printed to.
     */
    void print_map_index_to_function(std::ofstream& stream) const;

    /**
     * Check whether a function of name `name` belongs to the basis.
     * 
     * @param  name The name of the function which is queried.
     * @return      true if the function is contained, else false.
     */
    bool contains(const std::string& name) const;

private:
    std::function<Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<C,C>>(const momD_conf<T,4>&)> function;

    /// Indicates whether the lookup tables for the coefficient have already been
    /// constructed.
    bool constructed_lookup_tables = false;

    /// Store the multiplicity.
    size_t multiplicity;

    /// Map the coefficient of a given power in epsilon of a basis function coefficient to an index.
    std::unordered_map<std::pair<std::string, int>, unsigned long> function_to_position;

    /// Map an index to a coefficient of a given power in epsilon of a basis function coefficient.
    std::unordered_map<unsigned long, std::pair<std::string, int>> position_to_function;

    Integrals::MapToBasis<T, FType> basis_map;

    /// Store the number of coefficients that appear in the amplitude.
    unsigned long number_of_coefficients;

    /// Contains the names of the functions whose names shall be reconstructed.
    std::vector<std::string> function_names;

    /**
     * Map the coefficients appearing in the NumericFunctionBasisExpansion into a vector that is
     * being returned to the reconstruction facilities.
     *
     * The order in the vector that is being returned is defined internally but the resulting reconstructed coefficients
     * can be mapped back to a FunctionBasisExpansion.
     *
     * @param nfbe The numeric function basis expansion that provides the coefficients of the basis functions.
     * @return     Contains the coefficients in an order that is defined internally.
     */
    std::vector<T>
    map_coefficients_to_vector(const Integrals::NumericFunctionBasisExpansion<Series<T>, std::string>& nfbe);

    /**
     * Construct a lookup table for the order of the coefficient in the output vector on the first phase space point
     * evaluation.
     *
     * It will create a map whose keys are pairs of basis functions and power in epsilon of the corresponding
     * coefficient and the value will be an index in the output vector.
     * @param nfbe      The NumericFunctionBasisExpansion that is obtained by evaluating the amplitude on the first
     * phase space point.
     */
    void construct_lookup_tables(const Integrals::NumericFunctionBasisExpansion<Series<T>, std::string>& nfbe);

    /// Evaluate the amplitude for a given momentum configuration.
    ///
    /// This method is private since it is only used once internally to evaluate the amplitude on the
    /// phase space point provided at construction time which is used to create to maps.
    ///
    /// What is being returned is a vector that contains for each basis function
    /// all the powers in \f[ \epsilon \f] of its coefficients.
    std::vector<T> operator()(const momentumD_configuration<T, 4>& mom_conf);
};

// ----------------------------------------------------------------------------
// -------------------------- Output Function ---------------------------------
// ----------------------------------------------------------------------------

/**
 * Output the functionally reconstructed amplitude to a file.
 *
 * The output is formatted in such a way that further parsing in Mathematica is simplified.
 *
 * @param nfbe     The functionally reconstructed amplitude.
 * @param filename The name of the file the output shall be saved to.
 */
template <typename T>
void output_to_file(
    const Integrals::NumericFunctionBasisExpansion<Series<Ratio<DensePolynomial<T>>>, std::string>& nfbe,
    const std::string& filename);

/**
 * Output the rationally reconstructed result to a file.
 *
 * The output is formatted in such a way that further parsing in Mathematica is simplified.

 * @param nfbe     The rationally reconstructed amplitude.
 * @param filename The name of the file the output shall be saved to.
 */
  void output_to_file(const Integrals::NumericFunctionBasisExpansion<Series<BigRat>, std::string>& nfbe,
                    const std::string& filename);

} // namespace Caravel

// Implementations
#include "ReconstructionTools.hpp"

#endif // _RECONSTRUCTION_TOOLS_H_
