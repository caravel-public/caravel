#pragma once
#include <functional>
#include <vector>
#include "Core/RationalReconstruction.h"
#include "FunctionalReconstruction/DenseRational.h"
#include "FunctionalReconstruction/SparseMultivariatePolynomial.h"
namespace Caravel{
namespace Reconstruction{

template<typename F>
/**
 * Takes a vector valued function over a finite field and an argument
 * count. Evaluates the function on two points, related be a rescaling
 * to work out the degree of homogeneity. If the function is not
 * homogeneous, the results will be wrong.
 */
std::vector<int64_t> determine_dimension(const std::function<std::vector<F>(std::vector<F>)>& f, size_t num_args){
    std::vector<F> random_input, twice_random_input;
    for (size_t i = 0; i < num_args; i++){
        F rand_val = rand();
        random_input.push_back(rand_val);
        twice_random_input.push_back(F(2)*rand_val);
    }

    auto fx = f(random_input);
    auto f2x = f(twice_random_input);


    std::vector<int64_t> dimensions;
    for (size_t i = 0; i < f2x.size(); i++){
        if (f2x.at(i) == F(0)){
            dimensions.push_back(0);
            continue;
        }

        auto ratio_ff = f2x.at(i)/fx.at(i);
        auto ratio_rat = Caravel::RationalReconstruction::rational_guess(ratio_ff);
        double ratio_float(ratio_rat);

        double dimension(std::log(ratio_float)/std::log(2));

        dimensions.push_back(std::round(dimension));
    }

    return dimensions;
}

/**
 * Takes a scalar valued function over a finite field and an argument
 * count. Evaluates the function on two points, related be a rescaling
 * to work out the degree of homogeneity. If the function is not
 * homogeneous, the results will be wrong.
 */
template<typename F>
int64_t determine_dimension(const std::function<F(std::vector<F>)>& f, size_t num_args){
    // Wrap around the vector valued version of the function.
    std::function<std::vector<F>(std::vector<F>)> wrapper = [&f](std::vector<F> x){
       std::vector<F> eval;
       eval.push_back(f(x));
       return eval;
    };
    return determine_dimension(wrapper, num_args).at(0);
}

/**
 * Takes a univariate rational function and introduces a new variable so
 * that the result is a homogeneous rational function of the given
 * homogeneity degree. The last variable name is used as the new
 * variable.
 */

template<typename F>
Ratio<SparseMultivariatePolynomial<F>>
restore_dimension(DenseRational<F>& rat, int64_t homo_degree, std::vector<std::string> var_names, size_t new_var_pos){

  auto homo_num = SparseMultivariatePolynomial<F>(rat.numerator).homogenize(var_names, new_var_pos);
  auto homo_den = SparseMultivariatePolynomial<F>(rat.denominator).homogenize(var_names, new_var_pos);
  Ratio<SparseMultivariatePolynomial<F>> homo_rat(homo_num, homo_den);

  auto dim_mismatch = homo_degree - determine_dimension(std::function<F(std::vector<F>)>(homo_rat), var_names.size());

  std::vector<size_t> monomial {0};
  monomial.insert(monomial.begin() + new_var_pos, dim_mismatch < 0 ? (-dim_mismatch) : dim_mismatch);
  if (dim_mismatch < 0){homo_rat.denominator *= monomial;}
  if (dim_mismatch > 0){homo_rat.numerator *= monomial;}

  return homo_rat;
}

/**
 * Takes a multivariate rational function and introduces a new variable so
 * that the result is a homogeneous rational function of the given
 * homogeneity degree. The last variable name is used as the new
 * variable.
 */

template<typename F>
Ratio<SparseMultivariatePolynomial<F>>
restore_dimension(Ratio<SparseMultivariatePolynomial<F>>& rat, int64_t homo_degree, std::vector<std::string> var_names, size_t new_var_pos){

  auto homo_num = rat.numerator.homogenize(var_names, new_var_pos);
  auto homo_den = rat.denominator.homogenize(var_names, new_var_pos);
  Ratio<SparseMultivariatePolynomial<F>> homo_rat(homo_num, homo_den);

  auto dim_mismatch = homo_degree - determine_dimension(std::function<F(std::vector<F>)>(homo_rat), var_names.size());
  
  std::vector<size_t> monomial(var_names.size() - 1, 0);
  monomial.insert(monomial.begin()+new_var_pos, dim_mismatch < 0 ? (-dim_mismatch) : dim_mismatch);
  if (dim_mismatch < 0){homo_rat.denominator *= monomial;}
  if (dim_mismatch > 0){homo_rat.numerator *= monomial;}

  return homo_rat;
}

}
}
