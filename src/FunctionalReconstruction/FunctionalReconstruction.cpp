#include "FunctionalReconstruction/FunctionalReconstruction.h"

namespace Caravel{
  #ifdef USE_FINITE_FIELDS
  namespace Reconstruction{
  template class PeraroFunction<F32,4>;
  template class PeraroFunction<F32,3>;
  template class PeraroFunction<F32,2>;
  //template class PeraroFunction<F32,1>;

  template class VectorPeraroFunction<F32,4>;
  template class VectorPeraroFunction<F32,3>;
  template class VectorPeraroFunction<F32,2>;
  //template class VectorPeraroFunction<F32,1>;
  }

  template class DensePolynomial<F32>;
  #endif
}
