#ifndef MPI_PARALLEL_RECONSTRUCTION_H_
#define MPI_PARALLEL_RECONSTRUCTION_H_

#include "FunctionalReconstruction/type_traits.h"
#include <optional>
#include <functional>
#include <unordered_map>
#include <mpi.h>
#include <vector>
#include <cstdint>
#include "Core/type_traits_extra.h"

namespace Caravel{

using std::uint64_t;

/**
 * Utility to transform from X to uint64_t. Collection of instances instead of template
 */
template <typename T> std::vector<uint64_t> transform_X_to_int(const T& in){
	return std::vector<uint64_t>({uint64_t(in)});
}
template <typename T,size_t D> std::vector<uint64_t> transform_X_to_int(const std::array<T,D>& in){
	std::vector<uint64_t> toret(D);
	std::transform(in.begin(),in.end(),toret.begin(), [](const T& i){ return uint64_t(i);});
	return toret;
}
/**
 * Utility to transform from uint64_t type to X. Collection of instances instead of template
 */
template <typename T> std::enable_if_t<is_exact<T>::value,T> transform_int_to_X(const std::vector<uint64_t>& in){
	return T(in[0]);
}
template <typename A> std::enable_if_t<!is_exact<A>::value,A> transform_int_to_X(const std::vector<uint64_t>& in){
	A toret;
	std::transform(in.begin(),in.end(),toret.begin(),[](const uint64_t& i){ return typename A::value_type(i);});
	return toret;
}
/**
 * Utility to transform a Y (type of result) into an MPI-transferable vector of uint64_t. Collection of instances instead of a template
 */
template <typename Y> std::vector<uint64_t> transform_Y_to_int(const Y& in){
	std::vector<uint64_t> result(in.size());
	std::transform(in.begin(),in.end(),result.begin(),[](const typename Y::value_type& y){return uint64_t(y);});
	return result;
}
/**
 * Utility to transform an MPI-transferable vector of uint64_t into Y. Collection of instances instead of a template
 */
template <typename Y> Y transform_int_to_Y(const std::vector<uint64_t>& in){
	Y result(in.size());
	std::transform(in.begin(),in.end(),result.begin(),[](const uint64_t& i){return typename Y::value_type(i);});
	return result;
}

  /**
   * Function which takes a reconstruction algorithm, implementing the
   * correct interface, and the function to be reconstructed and
   * performs the evaluation in parallel.
   *
   * Modifies algorithm in place.
   *
   * To avoid extra calls to MPI functions, thread_number n_mpi_threads are passed as arguments
   */
  template<typename Algorithm, typename Y, typename X, typename = std::enable_if<Caravel::Reconstruction::is_reconstruction_algorithm<Algorithm>::value>>
    void MPI_parallel_reconstruct(Algorithm& algorithm, const std::function<Y(X)>& f,int thread_number,int n_mpi_threads){

	if (n_mpi_threads < 2){
		std::cerr << "Error: MPI_parallel_reconstruct needs 2 or more threads to run. " << std::endl;
		exit(42);
	}

	// counts how many parallel steps have been taken (how many requests to the full pool of workers made)
	int steps(0);

	// the master thread
	if(thread_number==0){
		size_t n_workers(n_mpi_threads-1);
		std::optional<X> missed_point;
		size_t total_points(0);
		size_t total_points_used(0);
                size_t total_points_from_unused(0);
                size_t total_points_recomputed(0);

                // let's store the functions computed that aren't used
                std::unordered_map<X,Y> unused;

		while(!algorithm.finished){
			// one new step
			steps++;
			//std::cout<<"Parallel step # "<<steps<<std::endl;
			// track # of points used in this step
			size_t ltotal_points_used(0);
                        size_t ltotal_points_from_unused(0);
                        size_t ltotal_points_recomputed(0);

			// Compute in batches, with a factor for improving efficiency
                        size_t factor(10);
			auto doubled_potential_points = algorithm.guess_points(unused.size()+factor*n_workers);

                        std::vector<X> potential_points;
			for(size_t yy = 0; yy < doubled_potential_points.size() ; yy++){
                            if( std::find(potential_points.begin(), potential_points.end(), doubled_potential_points[yy]) == potential_points.end() )
                                if(unused.find(doubled_potential_points[yy])==unused.end())
                                    potential_points.push_back(doubled_potential_points[yy]);
                            if( potential_points.size() >= n_workers )
                                break;
                        }
			
			// Put any missed point evaluation on the end
			if (missed_point){
                                auto itpot = std::find(potential_points.begin(),potential_points.end(),missed_point.value());
                                auto itunu = unused.find(missed_point.value());
                                if(itpot==potential_points.end() && itunu==unused.end()){
					// Make space if necessary
					if (potential_points.size() == n_workers){potential_points.pop_back();}
					potential_points.push_back(missed_point.value());
				}
			}
			size_t sampled(potential_points.size());
			total_points+=sampled;

			// Distribute the points among the workers and collect them
			for(int worker_i=1;worker_i<=int(potential_points.size());worker_i++){
				std::vector<uint64_t> in_x_gen_int(transform_X_to_int(potential_points[worker_i-1]));
				int size_send(in_x_gen_int.size());
							      // type "0" communication (initiate step), single int, send to worker_i
				MPI_Send(&size_send,1,MPI_INT,worker_i,0,MPI_COMM_WORLD);
										     // type "steps" communication, order int's, always from master
				MPI_Send(in_x_gen_int.data(),size_send,MPI_LONG_LONG,worker_i,steps,MPI_COMM_WORLD);
			}
			// and now collect results
			std::vector<Y> potential_fs(potential_points.size());
			for(int worker_i=1;worker_i<=int(potential_points.size());worker_i++){
				int size_result(0);
								// type "100000000" communication, single int, from worker_i
				MPI_Recv(&size_result,1,MPI_INT,worker_i,100000000,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
				std::vector<uint64_t> local_result_int;
				local_result_int.resize(size_t(size_result));
											   // type "100000001" communication
				MPI_Recv(local_result_int.data(),size_result,MPI_LONG_LONG,worker_i,100000001,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
				
				potential_fs[worker_i-1]=transform_int_to_Y<Y>(local_result_int);
			}

                        // we add all received data to 'unused'
                        for(size_t ii=0;ii<potential_points.size();ii++){
                            if(unused.find(potential_points[ii])==unused.end()){
                                unused[potential_points[ii]] = potential_fs[ii];
                            }
                            else{
                                total_points_recomputed++;
                                ltotal_points_recomputed++;
                            }
                        }

			// Process all received data

			// Now add the missed point
			if (missed_point){
                                auto val = missed_point.value();
				missed_point = std::optional<X>();
                                // find location
                                auto itpot = std::find(potential_points.begin(),potential_points.end(),val);
                                auto itunu = unused.find(val);
                                if(itpot!=potential_points.end()){
					auto pos = itpot - potential_points.begin();
					algorithm.add_point(potential_fs.at(pos), val);
                                        // cleanup 'unused'
                                        if(unused.find(*itpot)!=unused.end())
                                            unused.erase(*itpot);
                                }
                                else if(itunu!=unused.end()){
				    algorithm.add_point(itunu->second, val);
                                    unused.erase(val);
                                    ltotal_points_from_unused++;
                                    total_points_from_unused++;
                                }
                                else{
                                    std::cerr<<"WARNING: missed_point result not found!"<<std::endl;
                                }
				total_points_used++;
				ltotal_points_used++;
				if (algorithm.finished){break;}
			}

            		size_t pos = 0;
			while (true){
				auto maybe_new_xs = algorithm.next_point();
				// All remaining points were in the cache!
				if (!maybe_new_xs){break;}
				auto new_xs = maybe_new_xs.value();
				auto it = std::find(potential_points.begin(), potential_points.end(), new_xs);
				if (it == potential_points.end()){
                                        // do we have it from a previously unused evaluation?
                                        if(unused.find(new_xs)!=unused.end()){
                                            algorithm.add_point(unused.at(new_xs),new_xs);
                                            // cleanup 'unused'
                                            unused.erase(new_xs);
					    total_points_used++;
					    ltotal_points_used++;
                                            ltotal_points_from_unused++;
                                            total_points_from_unused++;
					    if (algorithm.finished){break;}
                                        }
                                        else{
  					    missed_point = std::optional<X>(new_xs);
					    break;
                                        }
				}
				else{
					pos = it - potential_points.begin();
					algorithm.add_point(potential_fs.at(pos), new_xs);
                                        // cleanup 'unused'
                                        if(unused.find(*it)!=unused.end())
                                            unused.erase(*it);
					total_points_used++;
					ltotal_points_used++;
					if (algorithm.finished){break;}
				}
			}
			std::cout<<"Used PSPs: "<<ltotal_points_used<<"/"<<sampled<<" (from unused: "<<ltotal_points_from_unused;
                        if(ltotal_points_recomputed>0)
			    std::cout<<", recomputed: "<<ltotal_points_recomputed;
                        std::cout<<", unused cache size: "<<unused.size()<<") step # "<<steps<<std::endl;

		}

		// shutdown all workers
		for(int worker_i=1;worker_i<=int(n_workers);worker_i++){
			int sleep(0);
						  // type "0" communication (initiate step), single int, send to worker_i
			MPI_Send(&sleep,1,MPI_INT,worker_i,0,MPI_COMM_WORLD);
		}
		std::cout<<"Master ends after sampling "<<total_points<<" points in "<<steps<<" steps, and using "<<total_points_used<<" of them (total recomputation: "<<total_points_recomputed<<", used from a previous computation: "<<total_points_from_unused<<", unused: "<<unused.size()<<")"<<std::endl;
	}
	// the workers simply receive, compute, send, and ends if told
	else{
		while(true){
			// one new step
			steps++;
			// choose path (<=0: ends execution, i>0 size of passed INT_LONG_LONG container)
			int order(-1);
					   // type "0" communication (initiate step), single int, always from master
			MPI_Recv(&order,1,MPI_INT,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			if(order<=0)
				// break the while loop and end
				break;
			// container for incoming information
			std::vector<uint64_t> in_x_gen_int;
			in_x_gen_int.resize(size_t(order));
			// receive			      	  // type "steps" communication, order int's, always from master
			MPI_Recv(in_x_gen_int.data(),order,MPI_LONG_LONG,0,steps,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			// convert
			X in_x_gen(transform_int_to_X<X>(in_x_gen_int));
			// compute
			Y result;
                        size_t maxevals(3);
                        for(size_t cc=0;cc<maxevals;cc++){
			    try { result = f(in_x_gen) ; }
			    catch(...) { 
                                std::cout<<"FAILED evaluation! Thread # "<<thread_number<<std::endl; 
                                std::cout<<"input: "<<in_x_gen<<" --- ATTEMPT # "<<cc+1<<std::endl;
                                if(cc+1==maxevals){
                                    std::cout<<"Exit 97"<<std::endl;
                                    std::exit(97);  
                                }
                                else
                                    continue;
                            }
                            break;
                        }
			// and return
			std::vector<uint64_t> local_result_int(transform_Y_to_int(result));
			int size_result(local_result_int.size());
						 // type "100000000" communication, single int, send to master
			MPI_Send(&size_result,1,MPI_INT,0,100000000,MPI_COMM_WORLD);
			// send to master				    // type "100000001" communication
			MPI_Send(local_result_int.data(),size_result,MPI_LONG_LONG,0,100000001,MPI_COMM_WORLD);
		}
	}


  }

}

#endif 	// ends MPI_PARALLEL_RECONSTRUCTION_H_
