/**
 *
 * This file contains the letters for the pentagon alphabet,
 * expressed in terms of twistor variables.
 *
 */

#ifndef _PENTAGON_ALPHABET_H_
#define _PENTAGON_ALPHABET_H_

#include <functional>
#include <string>
#include <vector>

#include "../Alphabet.h"
#include "Core/typedefs.h"

namespace Caravel {

/**
 * The planar pentagon alphabet in twistor space.
 */
class PentagonAlphabetPlanar : public Alphabet {
    using F = Caravel::F32;

  public:
    /// Fills the vectors with the information of the letters
    PentagonAlphabetPlanar();

};

/**
 * The planar pentagon alphabet in invariant space.
 */
class PentagonAlphabetPlanarInvSpace : public Alphabet {
    using F = Caravel::F32;

  public:
    /// Fills the vectors with the information of the letters
    PentagonAlphabetPlanarInvSpace();

};

} // namespace Caravel

#endif // _PENTAGON_ALPHABET_H_
