/**
 *
 * This file contains the letters for the non-planar pentagon alphabet,
 * expressed both in terms of twistor variables and mandelstams.
 *
 */

#ifndef _PENTAGON_ALPHABET_NONPLANAR_H_
#define _PENTAGON_ALPHABET_NONPLANAR_H_

#include <functional>
#include <string>
#include <vector>

#include "../Alphabet.h"
#include "Core/typedefs.h"

namespace Caravel {

/**
 * The planar pentagon alphabet in twistor space.
 */
class PentagonAlphabetNonPlanar : public Alphabet {
    using F = Caravel::F32;

  public:
    /// Fills the vectors with the information of the letters
    PentagonAlphabetNonPlanar();

};

/**
 * The planar pentagon alphabet in invariant space.
 */
class PentagonAlphabetNonPlanarInvSpace : public Alphabet {
    using F = Caravel::F32;

  public:
    /// Fills the vectors with the information of the letters
    PentagonAlphabetNonPlanarInvSpace();

};

} // namespace Caravel

#endif // _PENTAGON_ALPHABET_NONPLANAR_H_
