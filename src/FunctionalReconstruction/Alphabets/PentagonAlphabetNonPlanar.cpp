#include "PentagonAlphabetNonPlanar.h"

namespace Caravel {

namespace {

F prodpow(const F& x, std::size_t n) {
    F result(1);
    for (size_t i = 0; i < n; i++) { result *= x; }
    return result;
}

} // anonymous namespace

// ------------------------------------------------------------------------------------------------
//                      Non-planar Pentagon Alphabet in Twistor Space
// ------------------------------------------------------------------------------------------------

PentagonAlphabetNonPlanar::PentagonAlphabetNonPlanar() {

    _name = "PentagonNonPlanar";

    // Add the letter strings
    letter_strings = {
        "x0",
        "x1",
        "x2",
        "x3",
        "1+x0",
        "1+x1",
        "x0-x2",
        "1+x2",
        "x2-x3",
        "-1+x3",
        "1+x1+x0*x1",
        "x0*x1-x2-x1*x2",
        "1+x2-x3",
        "x0-x2+x3",
        "-x0+x2+x0*x3",
        "1+x0*x1-x1*x2+x1*x3",
        "x0*x1-x2-x1*x2+x1*x3",
        "x0*x1-x1*x2+x3+x1*x3",
        "-x0*x1+x2+x1*x2+x0*x1*x3",
        "x0*x1+x0^2*x1-x2-x1*x2-x0*x1*x2",
        "x0*x1-x2-x1*x2+x3+x1*x3",
        "-x0-x0*x1+x2+x1*x2+x0*x1*x3",
        "-x0*x1+x2+x0*x2+x1*x2+x0*x1*x3",
        "-x0*x1+x2+x1*x2+x0*x3+x0*x1*x3",
        "-x0^2*x1+x0*x2+2*x0*x1*x2-x2^2-x1*x2^2+x0^2*x1*x3+x2*x3+x1*x2*x3",
        /* non-planar*/
        "x0*(1+x1+x0*x1)-(1+x0)*(1+x1)*x2",
        "(1+x1+x0*x1)*(x0-x2)-x0*x3",
        "(1 + x0)*(x0*x1 - (1 + x1)*x2) + x0*x3",
        "-1 + x0*x1 - (1 + x1)*x2 + x3 + x1*x3",
        "x2 + x1*x2 - x0*(1 + x1 + x2) + x0*(1 + x1)*x3",
    };

    letters = {
        [](const std::vector<F>& x) { return x[0]; },
        [](const std::vector<F>& x) { return x[1]; },
        [](const std::vector<F>& x) { return x[2]; },
        [](const std::vector<F>& x) { return x[3]; },
        [](const std::vector<F>& x) { return F(1) + x[0]; },
        [](const std::vector<F>& x) { return F(1) + x[1]; },
        [](const std::vector<F>& x) { return x[0] - x[2]; },
        [](const std::vector<F>& x) { return F(1) + x[2]; },
        [](const std::vector<F>& x) { return x[2] - x[3]; },
        [](const std::vector<F>& x) { return -F(1) + x[3]; },
        [](const std::vector<F>& x) { return F(1) + x[1] + x[0] * x[1]; },
        [](const std::vector<F>& x) { return x[0] * x[1] - x[2] - x[1] * x[2]; },
        [](const std::vector<F>& x) { return F(1) + x[2] - x[3]; },
        [](const std::vector<F>& x) { return x[0] - x[2] + x[3]; },
        [](const std::vector<F>& x) { return -x[0] + x[2] + x[0] * x[3]; },
        [](const std::vector<F>& x) { return F(1) + x[0] * x[1] - x[1] * x[2] + x[1] * x[3]; },
        [](const std::vector<F>& x) { return x[0] * x[1] - x[2] - x[1] * x[2] + x[1] * x[3]; },
        [](const std::vector<F>& x) { return x[0] * x[1] - x[1] * x[2] + x[3] + x[1] * x[3]; },
        [](const std::vector<F>& x) { return -x[0] * x[1] + x[2] + x[1] * x[2] + x[0] * x[1] * x[3]; },
        [](const std::vector<F>& x) { return x[0] * x[1] + prodpow(x[0], 2) * x[1] - x[2] - x[1] * x[2] - x[0] * x[1] * x[2]; },
        [](const std::vector<F>& x) { return x[0] * x[1] - x[2] - x[1] * x[2] + x[3] + x[1] * x[3]; },
        [](const std::vector<F>& x) { return -x[0] - x[0] * x[1] + x[2] + x[1] * x[2] + x[0] * x[1] * x[3]; },
        [](const std::vector<F>& x) { return -x[0] * x[1] + x[2] + x[0] * x[2] + x[1] * x[2] + x[0] * x[1] * x[3]; },
        [](const std::vector<F>& x) { return -x[0] * x[1] + x[2] + x[1] * x[2] + x[0] * x[3] + x[0] * x[1] * x[3]; },
        [](const std::vector<F>& x) {
            return -prodpow(x[0], 2) * x[1] + x[0] * x[2] + F(2) * x[0] * x[1] * x[2] - prodpow(x[2], 2) - x[1] * prodpow(x[2], 2) +
                   prodpow(x[0], 2) * x[1] * x[3] + x[2] * x[3] + x[1] * x[2] * x[3];
        },
        /* non-planar*/
        [](const std::vector<F>& x) { return x[0] * (F(1) + x[1] + x[0] * x[1]) - (F(1) + x[0]) * (F(1) + x[1]) * x[2]; },
        [](const std::vector<F>& x) { return (F(1) + x[1] + x[0] * x[1]) * (x[0] - x[2]) - x[0] * x[3]; },
        [](const std::vector<F>& x) { return (F(1) + x[0]) * (x[0] * x[1] - (F(1) + x[1]) * x[2]) + x[0] * x[3]; },
        [](const std::vector<F>& x) { return F(-1) + x[0] * x[1] - (F(1) + x[1]) * x[2] + x[3] + x[1] * x[3]; },
        [](const std::vector<F>& x) { return x[2] + x[1] * x[2] - x[0] * (F(1) + x[1] + x[2]) + x[0] * (F(1) + x[1]) * x[3]; },
    };
}

// ------------------------------------------------------------------------------------------------
//                      Non-planar Pentagon Alphabet in Invariant Space
// ------------------------------------------------------------------------------------------------

PentagonAlphabetNonPlanarInvSpace::PentagonAlphabetNonPlanarInvSpace() {

    _name = "PentagonNonPlanarInvSpace";

    // Add the letter strings
    letter_strings = {"s23", "s34", "s45", "s51", "s34+s45", "s45+s51", "1+s51", "1+s23", "s23+s34", "1-s45", "s23-s51", "-1+s34", "-s23+s45", "-s34+s51",
                      "-1-s23+s45", "-s23-s34+s51", "1-s34-s45", "s23-s45-s51", "-1+s34-s51",
                      "-4*s34*(-s45-s23*s45+s45^2)*s51+(-s23+s23*s34-s34*s45+s51-s45*s51)^2",
                      /* non-planar */
                      "s34+s45-s12-s23", "s45+s51-s23-s34", "s51+s12-s34-s45", "s12+s23-s45-s51", "s23+s34-s51-s12"};

    letters = {
        [](const std::vector<F>& x) { return x[2]; },
        [](const std::vector<F>& x) { return ((F(1) + x[1]) * x[2]) / x[0] + x[1] * (-F(1) + x[3]); },
        [](const std::vector<F>& x) { return x[3]; },
        [](const std::vector<F>& x) { return x[1] * (x[0] - x[2] + x[3]); },
        [](const std::vector<F>& x) { return ((F(1) + x[1]) * x[2]) / x[0] + x[1] * (-F(1) + x[3]) + x[3]; },
        [](const std::vector<F>& x) { return x[3] + x[1] * (x[0] - x[2] + x[3]); },
        [](const std::vector<F>& x) { return F(1) + x[1] * (x[0] - x[2] + x[3]); },
        [](const std::vector<F>& x) { return F(1) + x[2]; },
        [](const std::vector<F>& x) { return x[2] + ((F(1) + x[1]) * x[2]) / x[0] + x[1] * (-F(1) + x[3]); },
        [](const std::vector<F>& x) { return F(1) - x[3]; },
        [](const std::vector<F>& x) { return x[2] - x[1] * (x[0] - x[2] + x[3]); },
        [](const std::vector<F>& x) { return -F(1) + ((F(1) + x[1]) * x[2]) / x[0] + x[1] * (-F(1) + x[3]); },
        [](const std::vector<F>& x) { return -x[2] + x[3]; },
        [](const std::vector<F>& x) { return -(((F(1) + x[1]) * x[2]) / x[0]) - x[1] * (-F(1) + x[3]) + x[1] * (x[0] - x[2] + x[3]); },
        [](const std::vector<F>& x) { return -F(1) - x[2] + x[3]; },
        [](const std::vector<F>& x) { return -x[2] - ((F(1) + x[1]) * x[2]) / x[0] - x[1] * (-F(1) + x[3]) + x[1] * (x[0] - x[2] + x[3]); },
        [](const std::vector<F>& x) { return F(1) - ((F(1) + x[1]) * x[2]) / x[0] - x[1] * (-F(1) + x[3]) - x[3]; },
        [](const std::vector<F>& x) { return x[2] - x[3] - x[1] * (x[0] - x[2] + x[3]); },
        [](const std::vector<F>& x) { return -F(1) + ((F(1) + x[1]) * x[2]) / x[0] + x[1] * (-F(1) + x[3]) - x[1] * (x[0] - x[2] + x[3]); },
        [](const std::vector<F>& x) {
            return -F(4) * x[1] * (((F(1) + x[1]) * x[2]) / x[0] + x[1] * (-F(1) + x[3])) * (x[0] - x[2] + x[3]) * (-x[3] - x[2] * x[3] + prodpow(x[3], 2)) +
                   prodpow(-x[2] + x[2] * (((F(1) + x[1]) * x[2]) / x[0] + x[1] * (-F(1) + x[3])) -
                               (((F(1) + x[1]) * x[2]) / x[0] + x[1] * (-F(1) + x[3])) * x[3] + x[1] * (x[0] - x[2] + x[3]) -
                               x[1] * x[3] * (x[0] - x[2] + x[3]),
                           2);
        },
        /* non-planar */
        [](const std::vector<F>& x) { return F(-1) - x[2] + ((F(1) + x[1]) * x[2]) / x[0] + x[1] * (F(-1) + x[3]) + x[3]; },
        [](const std::vector<F>& x) { return ((F(1) + x[0]) * (x[0] * x[1] - (F(1) + x[1]) * x[2])) / x[0] + x[3]; },
        [](const std::vector<F>& x) { return ((F(1) + x[1] + x[0] * x[1]) * (x[0] - x[2])) / x[0] - x[3]; },
        [](const std::vector<F>& x) { return F(1) - x[0] * x[1] + x[2] + x[1] * x[2] - (F(1) + x[1]) * x[3]; },
        [](const std::vector<F>& x) { return -x[0] * x[1] + (F(1) + x[1]) * (F(-1) + x[2]) + ((F(1) + x[1]) * x[2]) / x[0]; },
    };
}

} // namespace Caravel