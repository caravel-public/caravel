// File assumes the presence of exact kinematics setup. Otherwise you really shouldn't be using this.

#ifndef PERARO_FUNCTION_H_INC
#define PERARO_FUNCTION_H_INC

#include <vector>
#include <functional>
#include "FunctionalReconstruction/MultivariateNewtonFunction.h"
#include "FunctionalReconstruction/ThieleFunction.h"
#include "FunctionalReconstruction/SparseMultivariatePolynomial.h"
#include "FunctionalReconstruction/InvertingRationalReconstructor.h"
#include "FunctionalReconstruction/InvertingPolynomialReconstructor.h"
#include <optional>

namespace Caravel{
namespace Reconstruction{

    /**
     * Simple class to store the degree information. Intended either to be
     * passed to Peraro function, or constructed within it.
     */
	struct DegreeInformation{
	   size_t numerator_total{0};
	   size_t denominator_total{0};
           std::vector<size_t> numerator_univariate{};
           std::vector<size_t> denominator_univariate{};
	};


    /**
     * Forward declaration for partial template specialization
     */

    template<typename F, size_t N> class PeraroFunction;
  
    /**
    * Implementation of Peraro's rational reconstruction
    * algorithm. 
    */
    template<typename F, size_t N>
    class PeraroFunction{
        public:
        /**
         * Option to work out the individual degrees through univariate
         * slicing. These slices might not actually exist, and this
         * option provides the ability to turn it off in this case.
         * Defaults to true.
         */
        bool compute_individual_degrees{true};

        private:
        std::unordered_map<std::array<F,N-1>, DenseRational<F>> slice_cache;
        std::array<F,N> next_expected;

        void slice_add_point(F f_val, F s_val);
        std::optional<F> slice_next_point();
        bool slice_finished() const;
        DenseRational<F> slice_canonical();

        std::pair<bool, size_t> reconstruction_position{true, 0};
	MultivariateNewtonFunction<F,N-1>& get_active_component();
	const MultivariateNewtonFunction<F,N-1>& get_active_component_const() const;
	std::optional<std::pair<bool, size_t>> next_component() const;
	
        bool done_prep{false};

        std::array<std::function<F()>,N> x_gens;
	std::array<F,N-1> current_slice;

	PeraroFunction<F,N> clone_no_data() const;
      
	DegreeInformation degrees;

	ThieleFunction<F> new_preparation_reconstructor();
        ThieleFunction<F> preparation_reconstructor;

	InvertingPolynomialReconstructor<F> new_known_denominator_reconstructor() const;
	InvertingPolynomialReconstructor<F> known_denominator_reconstructor;

	InvertingRationalReconstructor<F> new_known_degree_reconstructor() const;
	InvertingRationalReconstructor<F> known_degree_reconstructor;

        /**
         * Adds a slice to the reconstruction
         */
        void add_slice(const std::array<F, N-1>& ts, const DenseRational<F>& slice);
        /**
         * Takes a "projectivised point" and returns it to x space
         */

        std::array<F,N> to_x_space(const std::array<F,N>& t_space);




    public:

        /**
         * Helper function to figure out what position the given slice
         * is at.
         */
        size_t slice_position() const;

        /**
         * When done preparing, the value of this must be N-1, else
         * the total ordering will get confused.
         */
        size_t projective_slice{0};

        enum class Stage {PREPARATION, KNOWN_DEGREE, KNOWN_DENOMINATOR};
	Stage stage{Stage::PREPARATION};

        std::array<size_t, N> get_current_position() const;

        /**
         * Variable names used when to_canonical is called.
         */
        std::vector<std::string> variable_names;

	bool verbose{false};


        // TODO: Need VectorPeraro to be a friend to make this private.
	bool simulation{false};
        std::string name = "";

        /**
         * Option for remapping the ansatz function in terms of the
         * generated point. 
         */
        std::optional<std::function<F(std::array<F,N>)>> point_transform;

        /**
         * Algorithm requires that the constant term of the
         * denominator is non-zero. This is achieved by shifting the
         * arguments appropriately.
         */
        std::array<F,N> find_shift(const std::function<F(std::array<F,N>)>& f) const;
	std::optional<std::array<F,N>> shift;
	bool finished{false};
	void add_point(F f_val, std::array<F,N> x_vals);

        /**
         * Returns the next point upon which the function under
         * reconstruction should be evaluated on.
         */
	std::optional<std::array<F,N>> next_point();

	/**
	 * Guesses the next n points which the algorithm will ask
	 * for. Useful for precomputation when parallelizing. Only a
	 * guess because the algorithm may realize that a section is
	 * finished after any individual point, and so not need some
	 * of the points.
	 */
	std::vector<std::array<F,N>> guess_points(size_t n) const;
	std::vector<MultivariateNewtonFunction<F,N-1>>  numerator_coordinates;
	std::vector<MultivariateNewtonFunction<F,N-1>>  denominator_coordinates;

        /**
         * Returns a prepared copy of the function which has been
         * analyzed by the algorithm. NOTE: captures the input
         * function. If no shift was provided in the constructor, then
         * one will be found here.
         */

        std::function<F(std::array<F,N>)> prepare_function(const std::function<F(std::array<F,N>)>& f);

        /**
         * Constructor for point feeding idiom for Peraro Function.
         */
	PeraroFunction(std::array<std::function<F()>,N> x_gens, bool compute_individual_degrees = true);

        /**
         * Returns the canonical form of the reconstructed rational
         * function. Will throw an exception if called on an unfinished
         * reconstruction.
         */
        Ratio<SparseMultivariatePolynomial<F>> to_canonical();

        /**
         * Option whether or not to use the parameter rescaling of
         * Peraro. This is quite dangerous, as without it the algorithm
         * can only reconstruct polynomial functions.
         */
        bool rescale{true};

    };

    /**
     * Specialization of Peraro algorithm to univariate (== Thiele) case
     */
    template<typename F>
      class PeraroFunction<F,1>{

    public:
        ThieleFunction<F> univariate_f;

	/**
	 * @param f A function F -> F
	 * @param x_gens A (1d) array of (copiable) generators for each argument.
	 */
	PeraroFunction<F,1>(std::function<F(std::array<F,1>)> f, std::array<std::function<F()>,1> x_gens);

        Ratio<SparseMultivariatePolynomial<F>> to_canonical();

    };

    /**
     * Extension of Peraro's algorithm to handle vector valued
     * multivariate rational functions.
     */
    template<typename F, size_t N>
    class VectorPeraroFunction{

        /**
         * When any component is paused, we are waiting for each reconstruction to ask for this point
         */
        std::vector<std::optional<std::array<F,N>>> proposals;

        /**
         * To check if things are becoming desynced, we need to be
         * able to compare with the previous requested point.
         */
        std::optional<std::array<F,N>> last_point {};

        std::array<std::function<F()>,N> x_gens;

        /**
         * Records the different x_i which are generated by x_gens, in
         * the order they are generated. Necessary to decide which of
         * two slices come first in the reconstruction
         */
        std::array<std::vector<F>, N> canonical_orders;

	/**
	 * Total ordering of the points. Necessary to know if someone "got ahead".
	 */
        bool earlier(std::array<F,N> a, std::array<F,N> b);

        bool simulation {false};

	VectorPeraroFunction<F,N> clone_no_data() const;

      public:

      /**
       * Setup function to declare the size of the vector.
       */
        void populate_components(size_t dimension);

        bool verbose {false};

        /**
         * Option for remapping the ansatz function in terms of the
         * generated point. 
         */
        std::optional<std::function<F(std::array<F,N>)>> point_transform;

        bool compute_individual_degrees{true};

        /**
         * Origin chosen by algorithm. Will automatically generate one
         * if non provided. Must be provided before call to
         * prepare_function.
         */
	std::optional<std::array<F,N>> shift;

        std::unordered_map<std::array<F,N>, std::vector<F>> function_cache;
        std::vector<PeraroFunction<F,N>> components;

        /**
         * Variable names used when to_canonical is called.
         */
        std::vector<std::string> variable_names;

        bool finished{false};

	/**
	 * Guesses the next n points which the algorithm will ask
	 * for. Useful for precomputation when parallelizing. Only a
	 * guess because the algorithm may realize that a section is
	 * finished after any individual point, and so not need some
	 * of the points.
	 */
	std::vector<std::array<F,N>> guess_points(size_t n) const;

        /**
         * Evaluates the function enough times (in serial) to find a good origin, which is stored.
         */
        std::function<std::vector<F>(std::array<F,N>)> prepare_function(const std::function<std::vector<F>(std::array<F,N>)>& f);

        /**
         * Evaluates the function enough times (in serial) to find a
         * good origin, which is stored.  Takes a list of
         * thread-safe input functions, returning the versions to be
         * passed to the reconstructor.
         */
        std::vector<std::function<std::vector<F>(std::array<F,N>)>> prepare_function(const std::vector<std::function<std::vector<F>(std::array<F,N>)>>& fs);


        /**
         * Add the requested point to the reconstruction algorithm.
         */
        void add_point(const std::vector<F>& f_val, const std::array<F,N>& x_val);

        /**
         * Returns which point the algorithm wants next (if it does at all).
         */
        std::optional<std::array<F,N>> next_point();


        /**
         * Constructor for point feeding interface. Must run analyze
         * function before starting to use next_point/add_point.
         */
	VectorPeraroFunction(std::array<std::function<F()>,N> x_gens, bool _compute_individual_degrees = true);

        /**
         * Converts the result of the reconstruction to canonical form.
         */
        std::vector<Ratio<SparseMultivariatePolynomial<F>>> to_canonical();


    };
}
}

#include "FunctionalReconstruction/PeraroFunction.hpp"

#endif //PERARO_FUNCTION_H_INC
