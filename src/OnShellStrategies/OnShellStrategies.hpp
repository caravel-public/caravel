
namespace Caravel {

template <typename T> using SingleTransverse = std::vector<T>;
template <typename T> using TransversePoint = std::vector<SingleTransverse<T>>;


template <typename T>
OnShellPoint<T, 6> with_mu_components(const momentumD<T, 6>& l1, const momentumD<T, 6>& l2, const T& mu11, const T& mu22, const T& mu12,
                                      typename std::enable_if_t<is_exact<T>::value>* = nullptr) {

    EpsilonBasis<T>::squares[0] = mu11;
    EpsilonBasis<T>::squares[1] = mu12 * mu12 / mu11 - mu22;

    if (is_complex<T>::value) { EpsilonBasis<T>::squares[0] *= T(get_metric_signature<4, T>()); }

    auto l14 = T(1);
    auto l15 = T(0);
    auto l24 = T(mu12 / mu11);
    auto l25 = T(1);

    momentumD<T, 6> os_l1(l1.pi(0), l1.pi(1), l1.pi(2), l1.pi(3), l14, l15);
    momentumD<T, 6> os_l2(l2.pi(0), l2.pi(1), l2.pi(2), l2.pi(3), l24, l25);

    OnShellPoint<T, 6> os_point{os_l1, os_l2};
    return os_point;
}

template <typename T>
OnShellPoint<T, 6> with_mu_components(const momentumD<T, 6>& l1, const momentumD<T, 6>& l2, const T& mu11, const T& mu22, const T& mu12,
                                      typename std::enable_if_t<!is_exact<T>::value>* = nullptr) {
    using std::sqrt;
    T basis_square_five = mu11;
    T basis_square_six = mu12 * mu12 / mu11 - mu22;

    if (is_complex<T>::value) { basis_square_five *= T(get_metric_signature<4, T>()); }

    auto l14 = sqrt(basis_square_five);
    auto l15 = T(0);
    auto l24 = T(mu12 / mu11) * sqrt(basis_square_five);
    auto l25 = sqrt(basis_square_six);

    momentumD<T, 6> os_l1(l1.pi(0), l1.pi(1), l1.pi(2), l1.pi(3), l14, l15);
    momentumD<T, 6> os_l2(l2.pi(0), l2.pi(1), l2.pi(2), l2.pi(3), l24, l25);

    OnShellPoint<T, 6> os_point{os_l1, os_l2};
    return os_point;
}

template <typename T> OnShellPoint<T, 5> with_mu_components(const momentumD<T, 5>& l, const T& mu11, typename std::enable_if_t<is_exact<T>::value>* = nullptr) {

    EpsilonBasis<T>::squares[0] = mu11;
    EpsilonBasis<T>::squares[1] = T(1);

    if (is_complex<T>::value) { EpsilonBasis<T>::squares[0] *= T(get_metric_signature<4, T>()); }

    auto l4 = T(1);

    momentumD<T, 5> os_l(l.pi(0), l.pi(1), l.pi(2), l.pi(3), l4);

    OnShellPoint<T, 5> os_point{os_l};
    return os_point;
}

template <typename T>
OnShellPoint<T, 5> with_mu_components(const momentumD<T, 5>& l, const T& mu11, typename std::enable_if_t<!is_exact<T>::value>* = nullptr) {
    using std::sqrt;
    T basis_square_five = mu11;

    if (is_complex<T>::value) { basis_square_five *= T(get_metric_signature<4, T>()); }

    auto l4 = sqrt(basis_square_five);

    momentumD<T, 5> os_l(l.pi(0), l.pi(1), l.pi(2), l.pi(3), l4);

    OnShellPoint<T, 5> os_point{os_l};
    return os_point;
}

/**
 * Computes the scalar products belonging to a given loop as
 * determined by the on shell coordinates. Takes into account
 * situation where externals are the full set.
 */
template <typename F, size_t D>
std::pair<std::vector<F>, std::vector<F>> build_strand_scalar_products(const momentumD_configuration<F, D>& mom_conf,
                                                                       const std::vector<std::vector<int>>& externals, size_t num_point,
                                                                       const std::vector<F>& on_shell_coords) {
    using std::sqrt;

    // NOTE: Function built to be transparent, not efficient. But probably unnecessary to be efficient.

    // Build independent left scalar products.
    std::vector<F> os_scalar_products;
    bool has_dependent_sp = (externals.size() == num_point);
    size_t num_independent_os_sps = externals.size() - has_dependent_sp;
    momentumD<F, D> q;
    for (size_t i = 0; i < num_independent_os_sps; i++) {
        auto mom = (q + mom_conf.Sum(externals.at(i)));
        auto sp = (mom * mom - q * q) / F(2);
        q = mom;
        os_scalar_products.push_back(sp);
    }

    // Build independent ISPS
    std::vector<F> isps;
    auto num_transverse = 4 - (num_point - 1);
    auto num_isps = 4 - num_transverse - num_independent_os_sps;
    for (size_t i = 0; i < num_isps; i++) { isps.push_back(on_shell_coords.at(i)); }

    // Build transverse coordinates
    std::vector<F> transverse_coords;
    for (size_t i = 0; i < num_transverse; i++) { transverse_coords.push_back(on_shell_coords.at(num_isps + i)); }

    // Arrange result as:
    // {l.pj, l.ni}
    // Note that we need to compute the dependent scalar product.

    std::vector<F> scattering_sps;
    scattering_sps.insert(scattering_sps.end(), os_scalar_products.begin(), os_scalar_products.end());
    scattering_sps.insert(scattering_sps.end(), isps.begin(), isps.end());
    auto dependent_sp = -std::accumulate(scattering_sps.begin(), scattering_sps.end(), F(0));
    scattering_sps.push_back(dependent_sp);

    return std::make_pair(scattering_sps, transverse_coords);
}

template <typename F> OnShellMomentumParameterization<F, 5> surd_massive_parameterization(const lGraph::xGraph& g, const lGraph::lGraphKin::GraphKin<F, 5>& gK) {

    // TODO: extension of this function to cover massive propagators is simple but I leave them to later work
    if (g.get_n_loops() != 1) {
        std::cerr << "ERROR: use only for now surd_massive_parameterization<.,5>(xGraph,GraphKin) for 1-loop diagrams!" << std::endl;
        g.show();
        std::exit(121112);
    }

    // here we collect the number of transverse dimensions per loop
    auto trans_dims = g.transverse_space_dimensions();
    // and here the dimension spanned by attached legs to each strand with a loop
    std::vector<size_t> phys_dims;
    for (auto d : trans_dims) {
        if (d > 4) {
            std::cerr << "ERROR: logical error in dimensions in surd_massive_parameterization<.,5>(xGraph)! " << trans_dims << std::endl;
            g.show();
            std::exit(121114);
        }
        phys_dims.push_back(4 - d);
    }

    // get all loop momenta
    auto loop_mom = g.get_internal_momenta();

    // local copy of xGraph with loop-mom conventions of this function
    auto lg = g;
    //lg.show();
    lg.reset_momentum_routing();
    lg.trace_momentum_routing();
    //lg.show();
    auto lOSmap = OnShellStrategies::OnShellPointMap<F, 5>(lg, g);

    // physical momenta attached to given loop momentum
    std::vector<std::vector<std::vector<size_t>>> external_momenta;

    // masses of a given strand
    std::vector<std::vector<size_t>> masses;

    // Traverse all the beads (contained in the strands of the connections)
    const auto& theconnections = g.get_base_graph().get_connection_map();

    // TODO: it makes sense to transform this into a method of xGraph (and replace similar calls around the code)
    for (const auto& mc : loop_mom) {
        const auto& thestrand = (theconnections.at(mc.get_connection()))[mc.get_strand()];
        const auto& thebeads = thestrand.get_beads();
        std::vector<std::vector<size_t>> lmommapper;
        for (auto& b : thebeads) lmommapper.push_back(b.get_momentum_indices());
        // then add to container
        external_momenta.push_back(lmommapper);
        masses.push_back( thestrand.get_link_mass_indices() );
    }

    return [&gK, external_momenta, masses, phys_dims, lOSmap](const std::vector<std::vector<F>>& os_coords) {
        const auto& momconf = gK.get_mom_conf();
        const auto& dual_bases = gK.get_strand_by_strand_common_dual();
        const auto& common_transverse = gK.get_common_transverse();
        const auto& mass = gK.get_masses();

        // the loop momentum
        momentumD<F, 5> ll;

        const size_t& phys = phys_dims[0];
        const auto& externals = external_momenta[0];
        const auto& duals = dual_bases[0];
        const auto& alphas = os_coords[0];
        const auto& midx = masses[0];

        // fixed scalar products
        momD<F, 5> q;
        auto m = mass[midx[0]];
        for (size_t jj = 0; jj < phys; jj++) {
            auto qp = q + momconf.Sum(externals[jj]);
	    auto mp = mass[midx[jj+1]];
            auto rl = (qp * qp - q * q + m * m - mp * mp) / F(2);
            ll += rl * duals[jj];
            q = qp;
            m = mp;
        }

        // local transverse
        size_t coord_counter(0);
        for (size_t jj = phys; jj < duals.size(); jj++) {
            ll += alphas[coord_counter] * duals[jj];
            coord_counter++;
        }
        // common transverse (dropping extra-dimensional piece)
        for (size_t jj = 0; jj + 1 < common_transverse.size(); jj++) {
            ll += alphas[coord_counter] * common_transverse[jj];
            coord_counter++;
        }

	// normalize on last propagator
        auto mull = -( ll* ll - mass[midx[0]]*mass[midx[0]]);
        auto point = with_mu_components(ll, mull);

        return lOSmap(point, momconf);
    };
}

template <typename F> OnShellMomentumParameterization<F, 5> surd_parameterization(const lGraph::xGraph& g, const lGraph::lGraphKin::GraphKin<F, 5>& gK) {

    // TODO: extension of this function to cover massive propagators is simple but I leave them to later work
    if (g.get_n_loops() != 1) {
        std::cerr << "ERROR: use only for now surd_parameterization<.,5>(xGraph,GraphKin) for 1-loop diagrams!" << std::endl;
        g.show();
        std::exit(121112);
    }
    if (g.has_massive_link())
	return surd_massive_parameterization(g,gK);

    // here we collect the number of transverse dimensions per loop
    auto trans_dims = g.transverse_space_dimensions();
    // and here the dimension spanned by attached legs to each strand with a loop
    std::vector<size_t> phys_dims;
    for (auto d : trans_dims) {
        if (d > 4) {
            std::cerr << "ERROR: logical error in dimensions in surd_parameterization<.,5>(xGraph)! " << trans_dims << std::endl;
            g.show();
            std::exit(121114);
        }
        phys_dims.push_back(4 - d);
    }

    // get all loop momenta
    auto loop_mom = g.get_internal_momenta();

    // local copy of xGraph with loop-mom conventions of this function
    auto lg = g;
    lg.reset_momentum_routing();
    lg.trace_momentum_routing();
    // lg.show();
    auto lOSmap = OnShellStrategies::OnShellPointMap<F, 5>(lg, g);

    // physical momenta attached to given loop momentum
    std::vector<std::vector<std::vector<size_t>>> external_momenta;

    // Traverse all the beads (contained in the strands of the connections)
    const auto& theconnections = g.get_base_graph().get_connection_map();

    // TODO: it makes sense to transform this into a method of xGraph (and replace similar calls around the code)
    for (const auto& mc : loop_mom) {
        const auto& thestrand = (theconnections.at(mc.get_connection()))[mc.get_strand()];
        const auto& thebeads = thestrand.get_beads();
        std::vector<std::vector<size_t>> lmommapper;
        for (auto& b : thebeads) lmommapper.push_back(b.get_momentum_indices());
        // then add to container
        external_momenta.push_back(lmommapper);
    }

    return [&gK, external_momenta, phys_dims, lOSmap](const std::vector<std::vector<F>>& os_coords) {
        const auto& momconf = gK.get_mom_conf();
        const auto& dual_bases = gK.get_strand_by_strand_common_dual();
        const auto& common_transverse = gK.get_common_transverse();

        // the loop momentum
        momentumD<F, 5> ll;

        const size_t& phys = phys_dims[0];
        const auto& externals = external_momenta[0];
        const auto& duals = dual_bases[0];
        const auto& alphas = os_coords[0];

        // fixed scalar products
        momD<F, 5> q;
        for (size_t jj = 0; jj < phys; jj++) {
            auto qp = q + momconf.Sum(externals[jj]);
            auto rl = (qp * qp - q * q) / F(2);
            ll += rl * duals[jj];
            q = qp;
        }
        // local transverse
        size_t coord_counter(0);
        for (size_t jj = phys; jj < duals.size(); jj++) {
            ll += alphas[coord_counter] * duals[jj];
            coord_counter++;
        }
        // common transverse (dropping extra-dimensional piece)
        for (size_t jj = 0; jj + 1 < common_transverse.size(); jj++) {
            ll += alphas[coord_counter] * common_transverse[jj];
            coord_counter++;
        }

        auto mull = -(ll * ll);

        auto point = with_mu_components(ll, mull);

        return lOSmap(point, momconf);
    };
}

// auxilary function to help surd_parameterization(xGraph) in momentum-conservation conditions where loops join
inline void to_apply_mom_conservation(const lGraph::xGraph& g, std::vector<size_t>& node_momentum) {

    // pick the node
    int node = g.attached_node(0);

    if (node < 0) {
        std::cerr << "ERROR: to_apply_mom_conservation(.) found an arrangement of an xGraph in which a loop momentum isn't attached to a node!" << std::endl;
        g.show();
        std::exit(78781);
    }
    // std::cout<<"NODE picked: "<<node<<std::endl;

    // get the momenta in the corresponding node
    node_momentum = g.get_base_graph().get_nodes()[node].get_momentum_indices();
}

template <typename F> OnShellMomentumParameterization<F, 6> surd_parameterization(const lGraph::xGraph& g, const lGraph::lGraphKin::GraphKin<F, 6>& gK) {

    // TODO: extensions of this function to cover 1-loop graphs, and even massive propagators, are simple but I leave them to later work
    //       Also, and extention to 3 or more loops is possible, but that requires a bit more work
    if (g.get_n_loops() != 2) {
        std::cerr << "ERROR: use only for now surd_parameterization<.,6>(xGraph,GraphKin) for 2-loop diagrams!" << std::endl;
        g.show();
        std::exit(111112);
    }
    if (!g.is_planar()) {
        return nonplanar_surd_parameterization(g, gK);
    }

    // copy of xGraph with loop-mom conventions of this function
    auto& lg = g.get_special_routing();
    // lg.show();
    auto lOSmap = OnShellStrategies::OnShellPointMap<F, 6>(lg, g);

    // here we collect the number of transverse dimensions per loop
    auto trans_dims = lg.transverse_space_dimensions();
    // and here the dimension spanned by attached legs to each strand with a loop
    std::vector<size_t> phys_dims;
    for (auto d : trans_dims) {
        if (d > 4) {
            std::cerr << "ERROR: logical error in dimensions in surd_parameterization<.,6>(xGraph)! " << trans_dims << std::endl;
            g.show();
            lg.show();
            std::exit(111114);
        }
        phys_dims.push_back(4 - d);
    }

    // get all loop momenta
    auto loop_mom = lg.get_internal_momenta();

    // physical momenta attached to given loop momentum
    std::vector<std::vector<std::vector<size_t>>> external_momenta;

    // masses of a given strand
    std::vector<std::vector<size_t>> masses;

    // Traverse all the beads (contained in the strands of the connections)
    const auto& theconnections = lg.get_base_graph().get_connection_map();

    // TODO: it makes sense to transform this into a method of xGraph (and replace similar calls around the code)
    for (const auto& mc : loop_mom) {
        const auto& thestrand = (theconnections.at(mc.get_connection()))[mc.get_strand()];
        const auto& thebeads = thestrand.get_beads();
        std::vector<std::vector<size_t>> lmommapper;
        for (auto& b : thebeads) lmommapper.push_back(b.get_momentum_indices());
        // then add to container
        external_momenta.push_back(lmommapper);
        masses.push_back( thestrand.get_link_mass_indices() );
    }
    bool foundit(false);
    // include in 'masses' the mass of the "central" strand
    for (const auto& [pair, conn] : theconnections) {
        for (const auto& strand : conn.get_edges()) {
            if (!strand.has_link_with_mom_label()) {
                masses.push_back(strand.get_link_mass_indices());
                foundit = true;
                break;
            }
        }
        if (foundit) break;
    }

    // FIXME: very two-loopish
    // we store the momentum attached to the node where momentum conservation is imposed
    std::vector<size_t> node_momentum;
    to_apply_mom_conservation(lg, node_momentum);

    bool is_factorized = lg.get_base_graph().is_factorizable();
    size_t n_loops = size_t(lg.get_n_loops());

    return [&gK, external_momenta, masses, phys_dims, n_loops, is_factorized, node_momentum, lOSmap](const std::vector<std::vector<F>>& os_coords) {
        const auto& momconf = gK.get_mom_conf();
        const auto& dual_bases = gK.get_strand_by_strand_common_dual();
        const auto& common_transverse = gK.get_common_transverse();
        const auto& mass = gK.get_masses();

        // the loop momenta
        std::vector<momentumD<F, 6>> ll(n_loops);
        std::vector<F> mull(n_loops);

        // assemble the momenta
        for (size_t ii = 0; ii < n_loops; ii++) {
            // few references for readability
            momentumD<F, 6>& l = ll[ii];
            const size_t& phys = phys_dims[ii];
            const auto& externals = external_momenta[ii];
            const auto& duals = dual_bases[ii];
            const auto& alphas = os_coords[ii];
            const auto& midx = masses[ii];
            /////////////////////////////////////////////////////////////////////////

#if 0
std::cout<<"node_momentum: "<<node_momentum<<std::endl;
std::cout<<"momconf: "<<momconf<<std::endl;
std::cout<<"Duals: "<<duals<<std::endl;
std::cout<<"Common trans: "<<common_transverse<<std::endl;
std::cout<<ii<<", phys: "<<phys<<", alphas: "<<alphas<<std::endl;

std::cout<<"Check external trans/dual: "<<externals<<std::endl;
for(const auto& ve:externals){
    auto e = momconf.Sum(ve);
    for(const auto& d:duals)
        std::cout<<"	Delta: "<<d*e<<std::endl;
    for(const auto& t:common_transverse)
        std::cout<<"	Zero: "<<e*t<<std::endl;
}

std::cout<<"Check DUAL trans:"<<std::endl;
for(const auto& d:duals)
    for(const auto& t:common_transverse)
        std::cout<<"	Zero: "<<d*t<<std::endl;
#endif

            // fixed scalar products
            momD<F, 6> q;
            auto m = mass[midx[0]];
            for (size_t jj = 0; jj < phys; jj++) {
                auto qp = q + momconf.Sum(externals[jj]);
	        auto mp = mass[midx[jj+1]];
                auto rl = (qp * qp - q * q + m * m - mp * mp) / F(2);
                l += rl * duals[jj];
                q = qp;
                m = mp;
            }
            // local transverse
            size_t coord_counter(0);
            for (size_t jj = phys; jj < duals.size(); jj++) {
                l += alphas[coord_counter] * duals[jj];
                coord_counter++;
            }
            // common transverse (dropping extra-dimensional pieces)
            for (size_t jj = 0; jj + 2 < common_transverse.size(); jj++) {
                l += alphas[coord_counter] * common_transverse[jj];
                coord_counter++;
            }

            mull[ii] = -(l * l - mass[midx[0]]*mass[midx[0]]);
        }

        F mu12(0);
        if (is_factorized) {
            mu12 = os_coords.at(1).back(); // mu12 is the "last" right coordinate.
        }
        else {
            // Fix the ep dim components.
            // Walk through left rung
            momentumD<F, 6> central_mom(-ll[0]);
            const auto& midx = masses[2];

            // Flow through bottom vertex
            central_mom -= ll[1];
            central_mom -= momconf.Sum(node_momentum);
            auto centralrho = central_mom * central_mom - mass[midx[0]]*mass[midx[0]];

            mu12 = (centralrho + mull[0] + mull[1]) / F(2);
        }

        // NOTICE the -l2 for matching 'with_mu_components' conventions
        auto toret = with_mu_components(ll[0], -ll[1], mull[0], mull[1], mu12);
        // come back to standard
        toret[1] = -toret[1];
        return lOSmap(toret, momconf);
    };
}

template <typename F>
OnShellMomentumParameterization<F, 6> nonplanar_surd_parameterization(const lGraph::xGraph& g, const lGraph::lGraphKin::GraphKin<F, 6>& gK) {

    // TODO: extensions of this function to cover 1-loop graphs, and even massive propagators, are simple but I leave them to later work
    //       Also, and extention to 3 or more loops is possible, but that requires a bit more work
    if (g.get_n_loops() != 2) {
        std::cerr << "ERROR: use only for now nonplanar_surd_parameterization(xGraph) for 2-loop diagrams!" << std::endl;
        g.show();
        std::exit(111112);
    }

    // copy of xGraph with loop-mom conventions of this function
    auto& lg = g.get_special_routing();
    // lg.show();
    auto lOSmap = OnShellStrategies::OnShellPointMap<F, 6>(lg, g);

    // here we collect the number of transverse dimensions per loop
    auto trans_dims = lg.transverse_space_dimensions();
    // and here the dimension spanned by attached legs to each strand with a loop
    std::vector<size_t> phys_dims;
    for (auto d : trans_dims) {
        if (d > 4) {
            std::cerr << "ERROR: logical error in dimensions in nonplanar_surd_parameterization<.,6>(xGraph)! " << trans_dims << std::endl;
            g.show();
            lg.show();
            std::exit(111114);
        }
        phys_dims.push_back(4 - d);
    }

    // get all loop momenta
    auto loop_mom = lg.get_internal_momenta();

    // physical momenta attached to given loop momentum
    std::vector<std::vector<std::vector<size_t>>> external_momenta;

    // masses of a given strand
    std::vector<std::vector<size_t>> masses;

    // Traverse all the beads (contained in the strands of the connections)
    const auto& theconnections = lg.get_base_graph().get_connection_map();

    // TODO: it makes sense to transform this into a method of xGraph (and replace similar calls around the code)
    for (const auto& mc : loop_mom) {
        const auto& thestrand = (theconnections.at(mc.get_connection()))[mc.get_strand()];
        const auto& thebeads = thestrand.get_beads();
        std::vector<std::vector<size_t>> lmommapper;
        for (auto& b : thebeads) lmommapper.push_back(b.get_momentum_indices());
        // then add to container
        external_momenta.push_back(lmommapper);
        masses.push_back( thestrand.get_link_mass_indices() );
    }

    // FIXME: very two-loopish
    // we store the momentum attached to the node where momentum conservation is imposed
    std::vector<size_t> node_momentum;
    to_apply_mom_conservation(lg, node_momentum);

    bool is_factorized = lg.get_base_graph().is_factorizable();
    if (is_factorized) {
        std::cerr << "ERROR: nonplanar_surd_parameterization() should not receive a factorized xGraph!" << std::endl;
        g.show();
        exit(1);
    }
    size_t n_loops = size_t(lg.get_n_loops());

    auto the_connection = lg.get_base_graph().get_connection(0, 1);
    lGraph::Strand central_strand;

    for (size_t i = 0; i < the_connection.size(); ++i) {
        if (!the_connection[i].has_link_with_mom_label()) central_strand = the_connection[i];
    }
    if (central_strand.size() > 1) {
        std::cout << "for now nonplanar_surd_parameterization() only works when there is at most one central leg." << std::endl;
        exit(1);
    }
    if (central_strand.size() == 0) {
        std::cout << "Found no central leg in nonplanar_surd_parameterization()." << std::endl;
        exit(1);
    }

    auto central_bead = central_strand.get_beads()[0];
    std::vector<size_t> central_external_indices = central_bead.get_momentum_indices();
    std::vector<size_t> central_masses = central_strand.get_link_mass_indices();

    bool has_bottom = (node_momentum.size() > 0);

    if (central_external_indices.size() == 0) {
        std::cout << "Found no indices in central leg in nonplanar_surd_parameterization()." << std::endl;
        exit(1);
    }

    return
        [&gK, external_momenta, masses, phys_dims, n_loops, node_momentum, has_bottom, central_external_indices, central_masses, lOSmap](const std::vector<std::vector<F>>& os_coords) {
            const auto& momconf = gK.get_mom_conf();
            const auto& dual_bases = gK.get_strand_by_strand_common_dual();
            const auto& common_transverse = gK.get_common_transverse();
            const auto& mass = gK.get_masses();

            // the loop momenta
            std::vector<momentumD<F, 6>> ll(n_loops);
            std::vector<F> mull(n_loops);

            auto p_central = momconf.Sum(central_external_indices);
            momentumD<F, 6> p_bottom;
            if (has_bottom) { p_bottom += momconf.Sum(node_momentum); }

            // assemble the first momentum
            momD<F, 6> q1;
            auto midx1 = masses[0];
            auto m1 = mass[midx1[0]];
            for (size_t jj = 0; jj < phys_dims[0]; jj++) {
                auto qp = q1 + momconf.Sum(external_momenta[0][jj]);
                auto mp = mass[midx1[jj + 1]];
                auto rl = (qp * qp - q1 * q1 - mp * mp + m1 * m1) / F(2);
                ll[0] += rl * dual_bases[0][jj];
                q1 = qp;
                m1 = mp;
            }
            // local transverse
            size_t coord_counter(0);
            for (size_t jj = phys_dims[0]; jj < dual_bases[0].size(); jj++) {
                ll[0] += os_coords[0][coord_counter] * dual_bases[0][jj];
                coord_counter++;
            }
            // common transverse (dropping extra-dimensional pieces)
            for (size_t jj = 0; jj + 2 < common_transverse.size(); jj++) {
                ll[0] += os_coords[0][coord_counter] * common_transverse[jj];
                coord_counter++;
            }

            mull[0] = -(ll[0] * ll[0] - mass[midx1[0]] * mass[midx1[0]]);

            // assemble the second momentum

            /*
             * We get around solving the additional constraint imposed by the additional central propagator by instead imposing it on one of the ISP momenta of
             * the second strand We can write \f[ 0 = (-l_1-l_2-p_b-p_c)^2 =(-l_1-l_2-p_b)^2-2p_c\cdot (-l_1-l_2-p_b)+p_c^2 \f] The first term vanishes due to
             * the on-shell codition. Furthermore we assume have fixed l_1 and l_2 is fixed up to a contribution \f$ \lambda_i v_i\f$ with \f$ v_i\cdot p_c\neq
             * 0\f$. Then \f[ 0 =-2p_c\cdot (-l_1-l_2^{\mathrm{rest.}}-p_b)+p_c^2 +2\lambda_i p_c\cdot v_i \f] Therefore \f[ \lambda_i =\frac{1}{ p_c\cdot
             * v_i}\left(-l_1-l_2^{\mathrm{rest.}}-p_b)-\frac{1}{2}*p_c^2)\right) \f]
             */

            // we pick the ISP corresponding to the first momentum on the l_1 strand to be constrained.

            // find a dual which is not orthogonal to the central momentum
            bool found_special_index = false;
            size_t special_index;
            for (size_t jj = phys_dims[1]; jj < dual_bases[1].size(); ++jj) {
                if (dual_bases[1][jj] * p_central == F(0))
                    continue;
                else {
                    special_index = jj;
                    found_special_index = true;
                    break;
                }
            }
            if (!found_special_index) {
                std::cerr << "ERROR: Did not find a suitable dual for the construction of the second loop momentum. " << std::endl;
                exit(1);
            }

            momD<F, 6> q2;
            auto midx2 = masses[1];
            auto m2 = mass[midx2[0]];
            for (size_t jj = 0; jj < phys_dims[1]; jj++) {
                auto qp = q2 + momconf.Sum(external_momenta[1][jj]);
                auto mp = mass[midx2[jj + 1]];
                auto rl = (qp * qp - q2 * q2 - mp * mp + m2 * m2) / F(2);
                ll[1] += rl * dual_bases[1][jj];
                q2 = qp;
		m2 = mp;
            }
            // local transverse
            coord_counter = 0;
            for (size_t jj = phys_dims[1]; jj < dual_bases[1].size(); jj++) {
                if (jj == special_index) continue;
                ll[1] += os_coords[1][coord_counter] * dual_bases[1][jj];
                coord_counter++;
            }
            // common transverse (dropping extra-dimensional pieces)
            for (size_t jj = 0; jj + 2 < common_transverse.size(); jj++) {
                ll[1] += os_coords[1][coord_counter] * common_transverse[jj];
                coord_counter++;
            }

            // FIXME: This should not be computed every time
            F msq = mass[central_masses[0]]*mass[central_masses[0]] - mass[central_masses[1]]*mass[central_masses[1]]  ;
            F last_coord = F(1) / (dual_bases[1][special_index] * p_central) * (p_central * (-ll[0] - ll[1] - p_bottom) - F(1) / F(2) * (p_central * p_central + msq));
            ll[1] += last_coord * dual_bases[1][special_index];

            mull[1] = -(ll[1] * ll[1] - mass[midx2[0]] * mass[midx2[0]]);

            F mu12(0);

            // Fix the ep dim components.
            // Walk through left rung
            momentumD<F, 6> central_mom(-ll[0]);

            // Flow through bottom vertex
            central_mom -= ll[1];
            central_mom -= p_bottom;
            auto centralrho = central_mom * central_mom;

            mu12 = (centralrho + mull[0] + mull[1] - mass[central_masses[0]] * mass[central_masses[0]]) / F(2);

            // NOTICE the -l2 for matching 'with_mu_components' conventions
            auto toret = with_mu_components(ll[0], -ll[1], mull[0], mull[1], mu12);
            // come back to standard
            toret[1] = -toret[1];

            return lOSmap(toret, momconf);
        };
}


/**
 * Produces a function which randomly generates on shell
 * momenta.
 */
template <typename T, size_t D> auto random_natural_point_generation(const lGraph::xGraph& graph, const lGraph::lGraphKin::GraphKin<T, D>& kinematics) {


    assert(graph.get_n_loops() == 2 );

    auto number_of_alpha_coordinates = graph.n_OS_variables_with_special_routing();

    return [&kinematics,
           number_of_alpha_coordinates,
           parameterization = surd_parameterization(graph,kinematics)
    ](auto gen) -> auto {

        auto random_source = [gen,scale = natural_scale(kinematics)]() { return random_scaled_number(scale, *gen); };

        return [&number_of_alpha_coordinates, parameterization, random_source](T x = T(0)) {
            std::vector<std::vector<T>> os_coords(number_of_alpha_coordinates.size());
            os_coords.shrink_to_fit();

            for(auto&& [i, obj] : enumerate(os_coords)){
                obj.resize(number_of_alpha_coordinates[i]);
                std::generate(obj.begin(),obj.end(),[random_source, x](){ return random_source() + x * random_source(); });
            }

            return parameterization(os_coords);
        };
    };
}


} // namespace Caravel
