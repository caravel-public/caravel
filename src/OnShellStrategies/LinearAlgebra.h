#ifndef LINEAR_ALGEBRA_H_INC
#define LINEAR_ALGEBRA_H_INC

#include "Core/momD.h"
#include <vector>

/**
 * File for organization of general linear algebra technology that has
 * become scattered around the code.
 */

namespace Caravel{
namespace OnShellStrategies{
  /**
   * Computes a minor of a matrix. Ask Evgenij for details.
   */
  template<typename T>
  std::vector<T> Minor(std::vector<T> a, unsigned k, unsigned l);

  /**
   * Computes the determinant of a square matrix "a". Ask Evgenij for details.
   */
  template<typename T>
  T det(std::vector<T> a);

  /**
   * Computes the inverse of a square matrix "a". Ask Evgenij for details.
   */
  template<typename T>
  std::vector<T> inverse(std::vector<T> a);

  /**
   * Computes the generalized gram matrix of two sets of vectors
   */
  template<typename F, size_t D>
    std::vector<F> generalized_gram_matrix(const std::vector<momD<F,D>>& vs, const std::vector<momD<F,D>>& ws);

  /**
   * Computes the generalized gram determinant of two sets of vectors
   */
  template<typename F, size_t D>
    F generalized_gram(const std::vector<momD<F,D>>& vs, const std::vector<momD<F,D>>& ws);

  /**
   * Computes the gram determinant of a set of vectors
   */
  template<typename F, size_t D>
    F gram(const std::vector<momD<F,D>>& vs);
}
}

#include "LinearAlgebra.hpp" // implementation

#endif
