#include "osm.h"
#include "Core/enumerate.h"
#include <exception>

namespace Caravel {
namespace OnShellStrategies {

using Gtype = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
std::unordered_map<std::tuple<Gtype, size_t, size_t>, std::vector<size_t>> StandardInsertions::insertion_map = {};

void StandardInsertions::define(const lGraph::xGraph& xg, size_t loop, size_t index, const std::vector<size_t>& momenta) {
    auto id = std::make_tuple(xg.get_base_graph(), loop, index);
    static std::mutex access;
    std::lock_guard<std::mutex> lock(access);
    auto it = insertion_map.find(id);
    if (it != insertion_map.end()) {
        // sanity check
        assert(it->second == momenta);
        return;
    }
    insertion_map[id] = momenta;
}

const std::vector<size_t>& StandardInsertions::insertion_momenta(const lGraph::xGraph& xg, size_t loop, size_t index) {
    auto id = std::make_tuple(xg.get_base_graph(), loop, index);
    auto it = insertion_map.find(id);
    if (it == insertion_map.end()) {
        std::cerr << "ERROR: did not find insertion with StandardInsertions::insertion_momenta(.)" << std::endl;
        xg.get_special_routing().show(std::cerr);
        std::cerr << "loop: " << loop << std::endl;
        std::cerr << "index: " << index << std::endl;
        std::exit(1);
    }
    else
        return it->second;
}

info_OS_map TransformMomRoutingOSMap(const lGraph::MomentumRouting& mr) {
    auto mci = mr.get_internal();
    info_OS_map toret;
    for (auto plm : mci.plus_momenta) {
        toret.loop_sign.push_back(1);
        // 0 based
        toret.loop_indices.push_back(plm - 1);
    }
    for (auto mlm : mci.minus_momenta) {
        toret.loop_sign.push_back(-1);
        // 0 based
        toret.loop_indices.push_back(mlm - 1);
    }
    auto mce = mr.get_external();
    for (auto plm : mce.plus_momenta) {
        toret.external_sign.push_back(1);
        // 1 based
        toret.external_indices.push_back(plm);
    }
    for (auto mlm : mce.minus_momenta) {
        toret.external_sign.push_back(-1);
        // 1 based
        toret.external_indices.push_back(mlm);
    }
    return toret;
}

lGraph::MomentumRouting TransformOSMapMomRouting(const info_OS_map& im) {
    std::vector<int> vi;
    for (size_t ii = 0; ii < im.loop_sign.size(); ii++) vi.push_back(im.loop_sign[ii] * int(im.loop_indices[ii] + 1));
    std::vector<int> ve;
    for (size_t ii = 0; ii < im.external_sign.size(); ii++) ve.push_back(im.external_sign[ii] * int(im.external_indices[ii]));
    return lGraph::MomentumRouting(vi, ve);
}

bool operator==(const info_OS_map& im1, const info_OS_map& im2) {
    return im1.loop_sign == im2.loop_sign && im1.loop_indices == im2.loop_indices && im1.external_sign == im2.external_sign &&
           im1.external_indices == im2.external_indices;
}

void print_vector_info_OS_map(const std::vector<info_OS_map>& absorb, std::ostream& o) {
    for (size_t ii = 0; ii < absorb.size(); ii++) {
        o << "l_to_" << ii + 1 << " = 	";
        for (size_t kk = 0; kk < absorb[ii].loop_indices.size(); kk++) {
            if (absorb[ii].loop_sign[kk] > 0)
                o << " (+l_from_" << absorb[ii].loop_indices[kk] + 1 << ") ";
            else
                o << " (-l_from_" << absorb[ii].loop_indices[kk] + 1 << ") ";
            if (kk + 1 != absorb[ii].loop_indices.size()) o << "+";
        }
        if (absorb[ii].external_indices.size() > 0) o << " + ";
        for (size_t kk = 0; kk < absorb[ii].external_indices.size(); kk++) {
            if (absorb[ii].external_sign[kk] > 0)
                o << " (+" << absorb[ii].external_indices[kk] << ") ";
            else
                o << " (-" << absorb[ii].external_indices[kk] << ") ";
            if (kk + 1 != absorb[ii].external_indices.size()) o << "+";
        }
        o << std::endl;
    }
    o << std::endl;
}

void compact_print_vector_info_OS_map(const std::vector<info_OS_map>& absorb, std::ostream& o, const std::string& to, const std::string& from) {
    for (size_t ii = 0; ii < absorb.size(); ii++) {
        o << "l"+to << ii + 1 << " = ";
        for (size_t kk = 0; kk < absorb[ii].loop_indices.size(); kk++) {
            if (absorb[ii].loop_sign[kk] > 0)
                o << " (+l"+from << absorb[ii].loop_indices[kk] + 1 << ") ";
            else
                o << " (-l"+from << absorb[ii].loop_indices[kk] + 1 << ") ";
            if (kk + 1 != absorb[ii].loop_indices.size()) o << "+";
        }
        if (absorb[ii].external_indices.size() > 0) o << "+";
        for (size_t kk = 0; kk < absorb[ii].external_indices.size(); kk++) {
            if (absorb[ii].external_sign[kk] > 0)
                o << " (+k" << absorb[ii].external_indices[kk] << ") ";
            else
                o << " (-k" << absorb[ii].external_indices[kk] << ") ";
            if (kk + 1 != absorb[ii].external_indices.size()) o << "+";
        }
        if(ii+1 != absorb.size())
            o << "; ";
    }
}

std::vector<info_OS_map> abstract_OnShellPointMap(const lGraph::xGraph& from,const lGraph::xGraph& to) {
    // base_graphs should match
    if(!(from.get_base_graph()==to.get_base_graph())){
        std::cerr<<"OnShellPointMap received different xGraphs! Here they are:"<<std::endl;
        from.show(std::cerr);
        to.show(std::cerr);
        throw std::logic_error("ERROR: should receive identical xGraphs!");
    }
    // check that momentum routing has been produced for both xGraphs
    if((!from.momemtum_traced())||(!to.momemtum_traced())){
        std::cerr<<"OnShellPointMap received xGraphs without tracings! Here they are:"<<std::endl;
        from.show(std::cerr);
        to.show(std::cerr);
        throw std::logic_error("ERROR: should receive traced xGraphs!");
    }

    // Access all the needed momenta
    auto needed = to.get_internal_momenta();
    // get the corresponding momenta in from
    auto maps = from.map_momenta_in_links(needed);

    std::vector<info_OS_map> absorb;
    for(const auto& m:maps){
        absorb.push_back(TransformMomRoutingOSMap(m));

        const auto& newmap = absorb.back();

        if (newmap.external_indices.size() != newmap.external_sign.size() or newmap.loop_indices.size() != newmap.loop_sign.size() or
            int(*std::max_element(newmap.loop_indices.begin(), newmap.loop_indices.end())) + 1 > from.get_n_loops() or
            (newmap.external_indices.size() > 0 and (*std::max_element(newmap.external_indices.begin(), newmap.external_indices.end())) + 1) >
                from.get_n_external_momenta()) {

            _WARNING_R("Internal inconsistency while constructing info_OS_map!");
            _WARNING("from:\n",from);
            _WARNING("to:\n",to);
            _WARNING(math_string(newmap));

            throw std::runtime_error("Internal inconsistency while constructing info_OS_map!");
        }
    }

    DEBUG(
        std::cout<<"OnShellPointMap result:"<<std::endl;
        std::cout<<"From graph: "<<from<<std::endl;
        std::cout<<"To graph: "<<to<<std::endl;
        print_vector_info_OS_map(absorb);
    );

    return absorb;
}

std::vector<info_OS_map> invert_abstract_OnShellPointMap(size_t n, const std::vector<lGraph::MomentumRouting>& in2copy) {
    using namespace lGraph;
    std::vector<MomentumRouting> mrout;
    auto in = in2copy;
    for(size_t ll = 1; ll <= in.size(); ll++)
        mrout.push_back(MomentumRouting({int(ll)}, {}));
    // simple Gaussian elimination procedure
    std::set<size_t> eqs;
    for(size_t i = 0; i < in.size(); i++) eqs.insert(i);
    std::vector<size_t> pivot;
    for(size_t ll = 1; ll <= in.size(); ll++) {
        int minimal(-1);
        size_t teq(0);
        for(auto eq: eqs) {
            if(in[eq].has_loop(ll)) {
                size_t nls = in[eq].get_internal().plus_momenta.size() + in[eq].get_internal().minus_momenta.size();
                if(minimal<0 || int(nls) < minimal) {
                    minimal = int(nls);
                    teq = eq;
                }
            }
        }
        if(minimal<0) {
            std::cerr<<"ERROR: invert_abstract_OnShellPointMap("<<n<<", "<<in2copy<<") could not produce pivot!"<<std::endl;
            std::exit(1);
        }
        pivot.push_back(teq);
        if (in[teq].has_loop_negative(ll)) {
            mrout[ll - 1] = -mrout[ll - 1];
            in[teq] = -in[teq];
        }
        eqs.erase(teq);
    }
    if (pivot.size() != in.size()) {
        std::cerr << "ERROR: invert_abstract_OnShellPointMap(" << n << ", " << in2copy << ") produced incomplete pivot! " << pivot << std::endl;
        std::exit(1);
    }
    
    for(size_t ll = 2; ll <= in.size(); ll++) {
        for(size_t jj = 1; jj < ll; jj++) {
            if(in[pivot[ll-1]].has_loop_positive(jj)) {
                mrout[ll-1] = mrout[ll-1] - mrout[jj-1];
                mrout[ll-1].external_mom_conservation(n);
                in[pivot[ll-1]] = in[pivot[ll-1]] - in[pivot[jj-1]];
                in[pivot[ll-1]].external_mom_conservation(n);
            }
            else if(in[pivot[ll-1]].has_loop_negative(jj)) {
                mrout[ll-1] = mrout[ll-1] + mrout[jj-1];
                mrout[ll-1].external_mom_conservation(n);
                in[pivot[ll-1]] = in[pivot[ll-1]] + in[pivot[jj-1]];
                in[pivot[ll-1]].external_mom_conservation(n);
            }
        }
    }
    for(size_t ll = in.size(); ll > 0; ll--) {
        if(in[pivot[ll-1]].has_loop_positive(ll)){
            in[pivot[ll-1]] = in[pivot[ll-1]] - MomentumRouting({int(ll)}, {});
            in[pivot[ll-1]].external_mom_conservation(n);
        }
        else if(in[pivot[ll-1]].has_loop_negative(ll)){
            in[pivot[ll-1]] = -(in[pivot[ll-1]] + MomentumRouting({int(ll)}, {}));
            in[pivot[ll-1]].external_mom_conservation(n);
            mrout[ll-1] = -mrout[ll-1];
        }
        else {
            std::cerr<<"ERROR: invert_abstract_OnShellPointMap("<<n<<", "<<in2copy<<") failed!"<<std::endl;
            std::exit(1);
        }
        // now substitute all previous
        for (size_t jj = in.size(); jj > ll; jj--) {
            if (in[pivot[ll - 1]].has_loop_positive(jj)) {
                in[pivot[ll - 1]] = in[pivot[ll - 1]] - MomentumRouting({int(jj)}, {}) + mrout[jj - 1];
                in[pivot[ll - 1]].external_mom_conservation(n);
            }
            else if (in[pivot[ll - 1]].has_loop_negative(jj)) {
                in[pivot[ll - 1]] = in[pivot[ll - 1]] + MomentumRouting({int(jj)}, {}) - mrout[jj - 1];
                in[pivot[ll - 1]].external_mom_conservation(n);
            }
        }
        mrout[ll-1] = mrout[ll-1] - in[pivot[ll-1]];
    }
    std::vector<MomentumRouting> mroutf;
    // undo pivot
    for(size_t ii=0; ii<mrout.size(); ii++) {
        auto external = mrout[ii].get_external();
        MomCollection internal;
        internal.prefix = mrout[ii].get_internal().prefix;
        for(auto i: mrout[ii].get_internal().plus_momenta) internal.plus_momenta.insert(pivot[i-1]+1);
        for(auto i: mrout[ii].get_internal().minus_momenta) internal.minus_momenta.insert(pivot[i-1]+1);
        mroutf.push_back(MomentumRouting(internal, external));
    }


    std::vector<info_OS_map> out;
    for(const auto& m:mroutf)    out.push_back(TransformMomRoutingOSMap(m));
    return out;
}

std::string math_string(const info_OS_map& in){
    using std::to_string;
    std::string out;

    out += "(";

    std::vector<std::string> toadd;

    for(auto [lind, lsign] : enumerate(in.loop_sign)) {
        std::string ss;
        if (lsign == -1) {
            ss = "-";
        }
        else if (lsign == 1) {}
        else {
            throw std::runtime_error("info_OS_map encountered internal inconsistency!");
        }

        ss+="l["+to_string(in.loop_indices[lind]+1)+"]";

        toadd.push_back(ss);
    }

    for(auto [pind, psign] : enumerate(in.external_sign)) {
        std::string ss;
        if (psign == -1) {
            ss = "-";
        }
        else if (psign == 1) {}
        else {
            throw std::runtime_error("info_OS_map encountered internal inconsistency!");
        }
        ss+="p["+to_string(in.external_indices[pind])+"]";

        toadd.push_back(ss);
    }

    return math_list_with_head("Plus", toadd);
}


_I_OSM_TYPE(C,R)

_I_OSM_TYPE(C,C)
#ifdef HIGH_PRECISION
_I_OSM_TYPE(CHP,CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_I_OSM_TYPE(CVHP,CVHP)
#endif

#ifdef INSTANTIATE_GMP
_I_OSM_TYPE(CGMP,CGMP)
#endif

} // namespace OnShellStrategies
} // namespace Caravel
