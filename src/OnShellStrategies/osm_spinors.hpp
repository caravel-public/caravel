#ifndef OSM_SPINORS_H
#define OSM_SPINORS_H

#include<complex>
#include<utility>
#include<iostream>
#include <vector>
#include "Core/typedefs.h"
#include "Core/momD.h"
#include "Core/type_traits_extra.h"
#if INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

#define _MAX(X,Y) (((X) > (Y)) ? (X) : (Y))

/*
//Scalar operators declarations
#define _I_H_SCALOP_TWOTYPE(_OP_,TYPE1,TYPE2)		\
  TYPE1 operator _OP_(const TYPE1&,const TYPE2&);	\
  TYPE1 operator _OP_(const TYPE2&,const TYPE1&);	\

#define _I_H_SCALOP_OP(_OP_,TYPE)		\
  _I_H_SCALOP_TWOTYPE(_OP_,TYPE,double)		\
  //  _I_H_SCALOP_TWOTYPE(_OP_,TYPE,int)	

#define _I_H_SCALOP_CPX(_OP_,TYPE)		\
  _I_H_SCALOP_OP(_OP_,std::complex<TYPE>)	\

#define _I_H_SCALOP(TYPE)			\
  _I_H_SCALOP_CPX(+,TYPE)			\
  _I_H_SCALOP_CPX(-,TYPE)			\
  _I_H_SCALOP_CPX(*,TYPE)			\
  //  _I_H_SCALOP_CPX(/,TYPE)			

_I_H_SCALOP(Caravel::R)
#ifdef HIGH_PRECISION
_I_H_SCALOP(Caravel::RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_I_H_SCALOP(Caravel::RVHP)
#endif

#ifdef INSTANTIATE_GMP
_I_H_SCALOP(RGMP)
#endif
*/
//End of scalar operators declarations

//Scalar operators definitions

#define _I_SCALOP_OP_T2(_OP_,TYPE1,TYPE2,TYPER)			\
  inline TYPER operator _OP_(const TYPE1&a,			\
			     const TYPE2&b){			\
    return (TYPER)a _OP_ (TYPER)b;				\
  }								\
  inline TYPER operator _OP_(const TYPE2&b,			\
			     const TYPE1&a){			\
    return (TYPER)b _OP_ (TYPER)a;				\
  }							

#define _I_SCALOP_T2(TYPE1,TYPE2,TYPER)			\
  _I_SCALOP_OP_T2(+,TYPE1,TYPE2,TYPER)			\
  _I_SCALOP_OP_T2(-,TYPE1,TYPE2,TYPER)			\
  _I_SCALOP_OP_T2(*,TYPE1,TYPE2,TYPER)			\
  _I_SCALOP_OP_T2(/,TYPE1,TYPE2,TYPER)				

#ifdef HIGH_PRECISION
_I_SCALOP_T2(int,Caravel::RHP,Caravel::RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_I_SCALOP_T2(int,Caravel::RVHP,Caravel::RVHP)
#endif
_I_SCALOP_T2(Caravel::R,Caravel::C,Caravel::C)
#ifdef HIGH_PRECISION
_I_SCALOP_T2(Caravel::RHP,Caravel::CHP,Caravel::CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_I_SCALOP_T2(Caravel::RVHP,Caravel::CVHP,Caravel::CVHP)
#endif
/*
#ifdef INSTANTIATE_GMP
_I_SCALOP_T2(int,
	     RGMP,
	     RGMP)
#endif
*/
/*
#ifdef INSTANTIATE_GMP
_I_SCALOP_T2(int,
	     CGMP,
	     CGMP)
#endif
*/

//TODO: Int operators
_I_SCALOP_T2(int,
	     std::complex<Caravel::R>,
	     std::complex<Caravel::R>)
#ifdef HIGH_PRECISION
_I_SCALOP_T2(int,
	     std::complex<Caravel::RHP>,
	     std::complex<Caravel::RHP>)
#endif
#ifdef VERY_HIGH_PRECISION
_I_SCALOP_T2(int,
	     std::complex<Caravel::RVHP>,
	     std::complex<Caravel::RVHP>)
#endif
#ifdef HIGH_PRECISION
_I_SCALOP_T2(std::complex<Caravel::R>,
	     std::complex<Caravel::RHP>,
	     std::complex<Caravel::RHP>)
#endif
#ifdef VERY_HIGH_PRECISION
 _I_SCALOP_T2(std::complex<Caravel::R>,
 	     std::complex<Caravel::RVHP>,
 	     std::complex<Caravel::RVHP>)
#endif
#if defined(HIGH_PRECISION) && defined(VERY_HIGH_PRECISION)
 _I_SCALOP_T2(std::complex<Caravel::RHP>,
 	     std::complex<Caravel::RVHP>,
 	     std::complex<Caravel::RVHP>)
#endif
/*
#ifdef INSTANTIATE_GMP
_I_SCALOP_T2(std::complex<double>,
	     CGMP,
	     CGMP)
#endif
*/
// _I_SCALOP(Caravel::R)
//#ifdef HIGH_PRECISION
// _I_SCALOP(Caravel::RHP)
//#endif
//#ifdef VERY_HIGH_PRECISION
// _I_SCALOP(Caravel::RVHP)
//#endif

// #ifdef INSTANTIATE_GMP
//  _I_SCALOP(RGMP)
// #endif

//End of scalar operators definitions.

template<class T1,class T2>
using sim_t=typename std::remove_const<decltype(std::declval<T1>()*std::declval<T2>())>::type;

//template<class T1,class T2>
//using c2_t=c_t<sim_t<T1,T2> >;
//template<class T1,class T2,class T3>
//using c3_t=sim_t<c2_t<T1,T2>,T3>;


template<class T>
std::complex<T> chop(std::complex<T>);
template<class T>
T chop(T);

template <class T,unsigned D>
struct sp_t {
  T v[D];
  sp_t();
  sp_t(T*);
  template<class T2>
  sp_t(T2*);
  template<class T2,unsigned D2>
  sp_t(const sp_t<T2,D2>&);
  T& operator[](unsigned);
  const T& operator[](unsigned)const;
  const sp_t<T,D> operator-()const;
  template<class T2,unsigned D2>
  sp_t<T,D> operator+=(const sp_t<T2,D2>& p);
  template<class T2,unsigned D2>
  sp_t<T,D> operator-=(const sp_t<T2,D2>& p);
  template <class T2>
  sp_t<T,D>operator*=(const T2& c);
  template <class T2>
  sp_t<T,D>operator/=(const T2& c);
  T norm();
};

#define osm_eps 1e-10

template<class T>
inline
std::complex<T> chop(std::complex<T> c){

  std::complex<T> res=c;
  using std::abs;
  
  if(abs(res)<osm_eps)
    res=0;
  else if(abs(res-1)<osm_eps)
    res=1;
  else if(abs(res-std::complex<T>(0,1))<osm_eps)
    res=std::complex<T>(0,1);
  else if(abs(res.imag())<osm_eps)
    res=res.real();
  else if(abs(res.real())<osm_eps)
    res=std::complex<T>(0,res.imag());

  return res;
}

template<class T>
inline
T chop(T c){
    using std::abs;
  T res=c;
  if(abs(res)<osm_eps)
    res=T(0);
  else if(abs(res-T(1))<osm_eps)
    res=T(1);
  return res;
}

template<class T,unsigned D>
inline T& sp_t<T,D>::operator[](unsigned i){
  return v[i];
}
template<class T,unsigned D>
inline const T& sp_t<T,D>::operator[](unsigned i) const{
  return v[i];
}
template<class T,unsigned D>
inline sp_t<T,D>::sp_t(){
  for(unsigned i=0;i<D;++i)
    v[i]=(T)0;
}
template<class T,unsigned D>
inline sp_t<T,D>::sp_t(T*inV){
  for(unsigned i=0;i<D;++i)
    v[i]=inV[i];
}
template<class T,unsigned D>
template<class T2>
inline sp_t<T,D>::sp_t(T2*inV){
  for(unsigned i=0;i<D;++i)
    v[i]=(T)inV[i];
}
template<class T,unsigned D> template<class T2,unsigned D2>
inline sp_t<T,D>::sp_t(const sp_t<T2,D2>&inK){
  // cout<<"generating sp_t from"<<endl
  //     <<inK<<endl
  //     <<"D="<<D<<"D2="<<D2<<endl;
  for(unsigned i=0;i<(D<D2?D:D2);++i)
    v[i]=(T)inK[i];
  for(unsigned i=D2;i<D;++i)
    v[i]=T(0);
  // cout<<"generated"<<endl
  //     <<*this<<endl;
}
template<class T,unsigned D>
inline sp_t<T,D>const sp_t<T,D>::operator-()const{
  sp_t<T,D>res(*this);
  for(unsigned i=0;i<D;++i)
    res[i]*=-1;
  return res;
}
template <class T,unsigned D>
template <class T2,unsigned D2>
inline sp_t<T,D> sp_t<T,D>::operator+=(const sp_t<T2,D2>& p){
  for(unsigned i=0;i<D2;i++)
    v[i]+=p[i];
  return *this;
}

template <class T,unsigned D>
template <class T2,unsigned D2>
inline sp_t<T,D> sp_t<T,D>::operator-=(const sp_t<T2,D2>& p){
  *this+=-p;
  return *this;
}

template <class T,unsigned D>
template <class T2>
inline sp_t<T,D> sp_t<T,D>::operator*=(const T2& c){
  for(unsigned i=0;i<D;++i)
    v[i]*=c;
  return *this;
}

template <class T,unsigned D>
template <class T2>
inline sp_t<T,D> sp_t<T,D>::operator/=(const T2& c){
  for(unsigned i=0;i<D;++i)
    v[i]*=(T)(T2(1)/c);
  return *this;
}

template <class T1,class T2,unsigned D1>
inline sp_t<sim_t<T1,T2>,D1> operator*(const sp_t<T1,D1>&p,const T2&c){
  sp_t<sim_t<T1,T2>,D1> res(p);
  for(unsigned i=0;i<D1;++i)
    res[i]*=c;
  return res;
}

template <class T1,class T2,unsigned D1>
inline sp_t<sim_t<T1,T2>,D1> operator*(const T2&c,const sp_t<T1,D1>&p){
  return p*c;
}

template <class T1,class T2,unsigned D1>
inline sp_t<sim_t<T1,T2>,D1> operator/(const sp_t<T1,D1>&p,const T2&c){
  return p*(sim_t<T1,T2>)(T2(1)/c);
}

template<class T,unsigned D>
inline T sp_t<T,D>::norm(){
  return *this**this;
}

template <class T1,class T2,
	  unsigned D1,unsigned D2>
inline sim_t<T1,T2>
operator*(const sp_t<T1,D1>& p,const sp_t<T2,D2>& k){
  sim_t<T1,T2> res = sim_t<T1,T2>(Caravel::get_metric_signature<T1>(0))*(p[0]*k[0]);

  for(unsigned i=1;i<(D1<D2?D1:D2);++i){
    res += sim_t<T1,T2>(Caravel::get_metric_signature<T1>(i))*(p[i]*k[i]);
  }

  return res;
}

template <class T1, class T2, unsigned D1, unsigned D2>
sp_t<sim_t<T1, T2>, _MAX(D1, D2)> inline operator+(const sp_t<T1,D1>&p,const sp_t<T2,D2>&k){
  sp_t<sim_t<T1,T2>, _MAX(D1, D2)> res(p);
  for(unsigned i=0;i<D2;++i){
    res[i]+=k[i];
  }
  return res;
}

template <class T1,class T2,
	  unsigned D1,unsigned D2>
sp_t<sim_t<T1,T2>,_MAX(D1,D2)>
inline operator-(const sp_t<T1,D1>&p,const sp_t<T2,D2>&k){
  return p+-k;
}

template <class T> using c_t=std::complex<T>;

template<class T,unsigned D>
inline std::enable_if_t<!Caravel::is_complex<T>::value && !Caravel::is_exact<T>::value,std::ostream&> operator<<(std::ostream& os,const sp_t<T,D>&p){
  using std::abs;
  os<<"{";
  int outCount=0;
  for(unsigned i=0;i<D;++i) {
    if(abs(real(c_t<T>(p[i])))>osm_eps) {
      os<<real(c_t<T>(p[i]));
      outCount++;
      if(imag(c_t<T>(p[i]))>osm_eps)
	os<<"+";
    }
    if(imag(c_t<T>(p[i]))<-osm_eps)
      os<<"-";
    if(abs(imag(c_t<T>(p[i])))>osm_eps) {
      os<<"I*"<<abs(imag(c_t<T>(p[i])));
      outCount++;
    }
    if(abs(p[i])<osm_eps)
      os<<"0";
    if(p[i]!=p[i])
      os<<"NaN";
    
    os<<(i==D-1?"}":",");
    if(i<D-1&&outCount*os.precision()>30) {
      outCount=0;
      os<<"\n";
    }
  }
  return os;
}

template<class T,unsigned D>
inline std::enable_if_t<!Caravel::is_complex<T>::value && Caravel::is_exact<T>::value,std::ostream&> operator<<(std::ostream& os,const sp_t<T,D>&p){
  os<<"{";
  int outCount=0;
  for(unsigned i=0;i<D;++i) {
    os<<p[i];
    
    os<<(i==D-1?"}":",");
    if(i<D-1&&outCount*os.precision()>30) {
      outCount=0;
      os<<"\n";
    }
  }
  return os;
}


template<class T,unsigned D>
inline std::enable_if_t<Caravel::is_complex<T>::value,std::ostream&> operator<<(std::ostream& os,const sp_t<T,D>&p){
  using std::abs;
  os<<"{";
  int outCount=0;
  for(unsigned i=0;i<D;++i) {
    if(abs(real(p[i]))>osm_eps) {
      os<<real(p[i]);
      outCount++;
      if(imag(p[i])>osm_eps)
	os<<"+";
    }
    if(imag(p[i])<-osm_eps)
      os<<"-";
    if(abs(imag(p[i]))>osm_eps) {
      os<<"I*"<<abs(imag(p[i]));
      outCount++;
    }
    if(abs(p[i])<osm_eps)
      os<<"0";
    if(p[i]!=p[i])
      os<<"NaN";
    
    os<<(i==D-1?"}":",");
    if(i<D-1&&outCount*os.precision()>30) {
      outCount=0;
      os<<"\n";
    }
  }
  return os;
}

/*
#define _I_SP_PM_BINARY(TYPE1,TYPE2,DIM1,DIM2)			\
  template sp_t<sim_t<TYPE1,TYPE2>,_MAX(DIM1,DIM2)>		\
  operator+(const sp_t<TYPE1,DIM1>&,				\
	    const sp_t<TYPE2,DIM2>&);				\
  								\
  template sp_t<sim_t<TYPE1,TYPE2>,_MAX(DIM1,DIM2)>		\
  operator-(const sp_t<TYPE1,DIM1>&,				\
  	    const sp_t<TYPE2,DIM2>&);				\
								\
  template sim_t<TYPE1,TYPE2>					\
  operator*(const sp_t<TYPE1,DIM1>&,				\
	    const sp_t<TYPE2,DIM2>&);				\

#define _I_SP_PM_UNARY(TYPE1,TYPE2,DIM1,DIM2)				\
  template sp_t<TYPE1,DIM1>						\
  sp_t<TYPE1,DIM1>::operator+=(const sp_t<TYPE2,DIM2>& p);		\
  template sp_t<TYPE1,DIM1>						\
  sp_t<TYPE1,DIM1>::operator-=(const sp_t<TYPE2,DIM2>& p);		\
  template								\
  sp_t<TYPE1,DIM1>::sp_t<TYPE2,DIM2>(const sp_t<TYPE2,DIM2>& p);	\

#define _I_SP_PM_SCAL(TYPE1,TYPE2,DIM1)			\
  template sp_t<TYPE1,DIM1>				\
  sp_t<TYPE1,DIM1>::operator*=(const TYPE2& c);		\
  template sp_t<TYPE1,DIM1>				\
  sp_t<TYPE1,DIM1>::operator/=(const TYPE2& c);		\

#define _I_SP_PM_BINARY_SCAL(TYPE1,TYPE2,DIM1)		\
  template sp_t<sim_t<TYPE1,TYPE2>,DIM1>		\
  operator*(const sp_t<TYPE1,DIM1>&,const TYPE2&);	\
  template sp_t<sim_t<TYPE1,TYPE2>,DIM1>		\
  operator*(const TYPE2& c,const sp_t<TYPE1,DIM1>&);	\
  template sp_t<sim_t<TYPE1,TYPE2>,DIM1>		\
  operator/(const sp_t<TYPE1,DIM1>&,const TYPE2&);	\

#define _I_SP_PM_OS(TYPE)				\
  template std::ostream&				\
  operator<<(std::ostream& os,const sp_t<TYPE,4>&p);	\
  template std::ostream&				\
  operator<<(std::ostream& os,const sp_t<TYPE,5>&p);	\
  template std::ostream&				\
  operator<<(std::ostream& os,const sp_t<TYPE,6>&p);	\

#define _I_SP_CHOP(TYPE)				\
  template TYPE chop(TYPE);				\

#define _I_SP_PM_COM_TYPE(TYPE1,TYPE2,DIM1,DIM2)	\
  _I_SP_PM_BINARY(TYPE1,TYPE2,DIM1,DIM2)		\
  _I_SP_PM_BINARY(TYPE2,TYPE1,DIM1,DIM2)		\

#define _I_SP_PM_COM_DIM(TYPE1,TYPE2,DIM1,DIM2)	\
  _I_SP_PM_BINARY(TYPE1,TYPE2,DIM1,DIM2)	\
  _I_SP_PM_BINARY(TYPE1,TYPE2,DIM2,DIM1)	\

#define _I_SP_PM_COM(TYPE1,TYPE2,DIM1,DIM2)	\
  _I_SP_PM_COM_TYPE(TYPE1,TYPE2,DIM1,DIM2)	\
  _I_SP_PM_COM_TYPE(TYPE1,TYPE2,DIM2,DIM1)	\

#define _I_SP_PM_INSDIM_SAME(_F,TYPE1,TYPE2)	\
  _F(TYPE1,TYPE2,4,4)				\
  _F(TYPE1,TYPE2,5,5)				\
  _F(TYPE1,TYPE2,6,6)				\

#define _I_SP_PM_INSDIM_DIFF(_F,TYPE1,TYPE2)	\
  _F(TYPE1,TYPE2,4,5)				\
  _F(TYPE1,TYPE2,4,6)				\
  _F(TYPE1,TYPE2,5,6)				\

#define _I_SP_PM_UNARY_DIM(TYPE1,TYPE2)		\
  _I_SP_PM_UNARY(TYPE1,TYPE2,4,4)		\
  _I_SP_PM_UNARY(TYPE1,TYPE2,5,4)		\
  _I_SP_PM_UNARY(TYPE1,TYPE2,5,5)		\
  _I_SP_PM_UNARY(TYPE1,TYPE2,6,4)		\
  _I_SP_PM_UNARY(TYPE1,TYPE2,6,5)		\
  _I_SP_PM_UNARY(TYPE1,TYPE2,6,6)		\

#define _I_SP_PM_SCAL_DIM(TYPE1,TYPE2)		\
  _I_SP_PM_SCAL(TYPE1,TYPE2,4)			\
  _I_SP_PM_SCAL(TYPE1,TYPE2,5)			\
  _I_SP_PM_SCAL(TYPE1,TYPE2,6)			\

#define _I_SP_PM_BINARY_SCAL_DIM(TYPE1,TYPE2)	\
  _I_SP_PM_BINARY_SCAL(TYPE1,TYPE2,4)		\
  _I_SP_PM_BINARY_SCAL(TYPE1,TYPE2,5)		\
  _I_SP_PM_BINARY_SCAL(TYPE1,TYPE2,6)		\

#define _I_SP_PM_ONETYPE(TYPE)				\
  template struct sp_t<TYPE,4>;				\
  template struct sp_t<TYPE,5>;				\
  template struct sp_t<TYPE,6>;				\
  _I_SP_PM_INSDIM_SAME(_I_SP_PM_BINARY,TYPE,TYPE)	\
  _I_SP_PM_INSDIM_DIFF(_I_SP_PM_COM_DIM,TYPE,TYPE)	\
  _I_SP_PM_OS(TYPE)					\
  _I_SP_CHOP(TYPE)					\
  _I_SP_PM_SCAL_DIM(TYPE,TYPE)				\
  _I_SP_PM_UNARY_DIM(TYPE,TYPE)				\
  _I_SP_PM_BINARY_SCAL_DIM(TYPE,TYPE)			\
  
#define _I_SP_PM_TWOTYPE(TYPE1,TYPE2)			\
  _I_SP_PM_INSDIM_SAME(_I_SP_PM_COM_TYPE,TYPE1,TYPE2)	\
  _I_SP_PM_INSDIM_DIFF(_I_SP_PM_COM,TYPE1,TYPE2)	\
  _I_SP_PM_BINARY_SCAL_DIM(TYPE1,TYPE2)			\
  _I_SP_PM_UNARY_DIM(TYPE2,TYPE1)			\

#define _I_SP_PM(TYPE)				\
  _I_SP_PM_ONETYPE(TYPE)			\
  _I_SP_PM_ONETYPE(std::complex<TYPE>)		\
  _I_SP_PM_TWOTYPE(TYPE,std::complex<TYPE>)	\

_I_SP_PM(Caravel::R)
#ifdef HIGH_PRECISION
_I_SP_PM(Caravel::RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_I_SP_PM(Caravel::RVHP)
#endif

#ifdef INSTANTIATE_GMP
_I_SP_PM(RGMP)
#endif
*/

#endif //  OSM_SPINORS_H
