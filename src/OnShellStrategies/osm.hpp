
#define _DEBUG_OSM 0 

namespace Caravel {
namespace OnShellStrategies {

template <typename T, size_t D>
std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)> common_transverse_dual_evaluator(const lGraph::xGraph& graph_in) {
    using namespace lGraph;

    auto& graph = graph_in.get_special_routing();

    // collect here all momenta (represented as vector<size_t>) of all legs in the graph
    std::vector<std::vector<size_t>> mommapper;

    // Traverse all the beads (contained in the strands of the connections)
    const auto& theconnections = graph.get_base_graph().get_connection_map();

    // get all loop momenta
    auto loop_mom = graph.get_internal_momenta();

    // we choose to add first momenta from strands with momentum assignments (sorted as in get_internal_momenta(), that is l1 , ... , ln)
    for(auto& mc:loop_mom){
        const Strand& thestrand = (theconnections.at(mc.link.connection))[mc.link.strand];
        const auto& thebeads = thestrand.get_beads();
        std::vector<std::vector<size_t>> lmommapper;
        for(auto& b:thebeads)    lmommapper.push_back(b.get_momentum_indices());
        // then add to global container
        for(auto& m:lmommapper)    mommapper.push_back(m);
    }
    
    // traverse the nodes of the graph
    const auto& thenodes = graph.get_base_graph().get_nodes();

    for(auto& node:thenodes){
        if(node.get_momentum_indices().size()>0)
            mommapper.push_back(node.get_momentum_indices());
    }

    // traverse all other momenta
    for(auto& conn:theconnections){
        const auto& thestrands = conn.second.get_edges();
        for(auto& strand:thestrands){
            // only strands without mom assigned (as they were already added)
            if(!strand.has_link_with_mom_label()){
                const auto& thebeads = strand.get_beads();
                for(auto& b:thebeads)    mommapper.push_back(b.get_momentum_indices());
            }
        }
    }

    // due to momentum conservation we drop the last momentum
    mommapper.pop_back();
    // no more than four entries
    while(mommapper.size()>4)
        mommapper.pop_back();
    size_t mommapper_size = mommapper.size();

    // WARNING: check if we have a 1Loop bubble with a single leg which is massless
    if (graph.is_bubble_with_single_massless_leg()) {
        assert(mommapper.size() == 1);
        std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)> lambda = [mommapper](const momD_conf<T, D>& mc) {
            // compute momenta
            std::vector<momD<T, D>> momenta(2);
            momenta[0] = mc.Sum(mommapper[0]);
            momenta[1] = momenta[0].get_parity_conjugated();
            // give a normalization
            momenta[1] /= (momenta[1] * momenta[0]);

            std::vector<momD<T, D>> transverse;
            std::vector<momD<T, D>> dual;
            std::tie(transverse, dual) = vNV_basis<T, D>(momenta);
            return std::make_pair(transverse, dual);
        };
        return lambda;
    }

    std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)> lambda = [graph, mommapper,
                                                                                                                 mommapper_size](const momD_conf<T, D>& mc) {
        // compute momenta
        std::vector<momD<T, D>> momenta(mommapper_size);
        for (size_t ii = 0; ii < mommapper_size; ii++) momenta[ii] = mc.Sum(mommapper[ii]);

        std::vector<momD<T, D>> transverse;
        std::vector<momD<T, D>> dual;
        std::tie(transverse, dual) = vNV_basis<T, D>(momenta);

        return std::make_pair(transverse, dual);
    };
    return lambda;
}

template <typename T>
bool is_contained(const std::vector<T>& v,const std::vector<std::vector<T>>& c){
    for(const auto& i:c)
        if(v==i)
            return true;
    return false;
}

template <typename T, size_t D>
std::function<std::pair<std::vector<momD<T, D>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)> common_transverse_strand_by_strand_dual_evaluator(const lGraph::xGraph& in_graph) {
    using namespace lGraph;

    auto graph = in_graph.get_special_routing();

    // collect here, for each Strand with assigned loop momentum, the corresponding external momentum
    std::vector<std::vector<std::vector<size_t>>> mommapper_strand_by_strand;

    // Traverse all the beads (contained in the strands of the connections)
    const auto& theconnections = graph.get_base_graph().get_connection_map();

    // get all loop momenta
    auto loop_mom = graph.get_internal_momenta();

    // we choose to add first momenta from strands with momentum assignments (sorted as in get_internal_momenta(), that is l1 , ... , ln)
    for(auto& mc:loop_mom){
        const Strand& thestrand = (theconnections.at(mc.link.connection))[mc.link.strand];
        const auto& thebeads = thestrand.get_beads();
        std::vector<std::vector<size_t>> lmommapper;
        for(auto& b:thebeads)    lmommapper.push_back(b.get_momentum_indices());
        mommapper_strand_by_strand.push_back(lmommapper);
    }
    
    // we add momenta from different strands into each other
    std::vector<std::vector<std::vector<size_t>>> aux_mommapper_strand_by_strand = mommapper_strand_by_strand;
    for(size_t ii=0;ii<mommapper_strand_by_strand.size();ii++){
        for(size_t jj=0;jj<mommapper_strand_by_strand.size();jj++){
            if(ii!=jj)
                mommapper_strand_by_strand[ii].insert(mommapper_strand_by_strand[ii].end(),aux_mommapper_strand_by_strand[jj].begin(),aux_mommapper_strand_by_strand[jj].end());
        }
    }

    // traverse the nodes of the graph
    const auto& thenodes = graph.get_base_graph().get_nodes();

    for(auto& node:thenodes){
        if(node.get_momentum_indices().size()>0){
            auto lmommapper = node.get_momentum_indices();
            for(size_t ii=0;ii<mommapper_strand_by_strand.size();ii++){
                mommapper_strand_by_strand[ii].push_back(lmommapper);
            }
        }
    }

    // we add momenta in other strands
    for (auto& conn : theconnections) {
        const auto& thestrands = conn.second.get_edges();
        for (auto& strand : thestrands) {
            // only strands without mom assigned (as they were already added)
            if (!strand.has_link_with_mom_label()) {
                const auto& thebeads = strand.get_beads();
                for (auto& b : thebeads) {
                    auto lmommapper = b.get_momentum_indices();
                    for (size_t ii = 0; ii < mommapper_strand_by_strand.size(); ii++) { mommapper_strand_by_strand[ii].push_back(lmommapper); }
                }
            }
        }
    }

    // apply mom conservation
    size_t n_momenta = graph.get_n_topology_legs();
    if(n_momenta>4)    n_momenta=4;
    else    n_momenta--;
    for (size_t ii = 0; ii < mommapper_strand_by_strand.size(); ii++) {
        while(mommapper_strand_by_strand[ii].size()>n_momenta)
            mommapper_strand_by_strand[ii].pop_back();
    }

    size_t n_loops(loop_mom.size());

    // WARNING: check if we have a 1Loop bubble single leg which is massless
    if (graph.is_bubble_with_single_massless_leg()) {
        assert(mommapper_strand_by_strand.size() == 1);
        assert(mommapper_strand_by_strand[0].size() == 1);
        assert(n_loops == 1);
        std::function<std::pair<std::vector<momD<T, D>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)> lambda =
            [mommapper_strand_by_strand](const momD_conf<T, D>& mc) {
                std::vector<momD<T, D>> transverse;
                std::vector<std::vector<momD<T, D>>> dual(1);

                // compute momenta
                std::vector<momD<T, D>> momenta(2);
                momenta[0] = mc.Sum(mommapper_strand_by_strand[0][0]);
                momenta[1] = momenta[0].get_parity_conjugated();
                // give a normalization
                momenta[1] /= (momenta[1] * momenta[0]);

                std::tie(transverse, dual[0]) = vNV_basis<T, D>(momenta);

                return std::make_pair(transverse, dual);
            };
        return lambda;
    }

    std::function<std::pair<std::vector<momD<T, D>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)> lambda =
        [n_momenta, mommapper_strand_by_strand, n_loops](const momD_conf<T, D>& mc) {
            std::vector<momD<T, D>> transverse;
            std::vector<std::vector<momD<T, D>>> dual(n_loops);

            for (size_t ii = 0; ii < n_loops; ii++) {
                // compute momenta
                std::vector<momD<T, D>> momenta(n_momenta);
                for (size_t jj = 0; jj < n_momenta; jj++) momenta[jj] = mc.Sum(mommapper_strand_by_strand[ii][jj]);

                std::tie(transverse, dual[ii]) = vNV_basis<T, D>(momenta);
            }

            return std::make_pair(transverse, dual);
        };
    return lambda;
}


template <typename T, size_t D>
std::function<std::pair<std::vector<std::vector<momD<T, D>>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)>
transverse_dual_evaluator(const lGraph::xGraph& graph_in) {
    auto& graph = graph_in.get_special_routing();

    // get all loop momenta
    auto loop_mom = graph.get_internal_momenta();

    using EachLoopMomentaLambda = std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)>;
    std::vector<EachLoopMomentaLambda> lambdas;
    
    for(const auto& e:loop_mom)    lambdas.push_back(transverse_dual_from_Strand<T,D>(e,graph));
    size_t n_loops(lambdas.size());

    std::function<std::pair<std::vector<std::vector<momD<T, D>>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)> lambda = [n_loops,lambdas](const momD_conf<T, D>& mc) {
        std::vector<std::vector<momD<T, D>>> trn(n_loops);
        std::vector<std::vector<momD<T, D>>> trd(n_loops);

        for(size_t ii=0;ii<n_loops;ii++)
            std::tie(trn[ii],trd[ii]) = lambdas[ii](mc);

        return std::make_pair(trn,trd);
    };
    return lambda;
}

template <typename T, size_t D>
std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)>
transverse_dual_from_Strand(const lGraph::MomChoice& mchoice,const lGraph::xGraph& graph_in) {
    using namespace lGraph;
    auto& graph = graph_in.get_special_routing();
    const Strand& thestrand = ((graph.get_base_graph().get_connection_map()).at(mchoice.link.connection))[mchoice.link.strand];

    auto beads = thestrand.get_beads();

    std::vector<std::vector<size_t>> mommapper;

    for(auto& b:beads)    mommapper.push_back(b.get_momentum_indices());

    // check if momenta attached to Strand is independent and act accordingly
    size_t n_momenta = graph.get_n_topology_legs();
    if(mommapper.size()==n_momenta)
        // We match the total number of momenta, so by mom conservation we drop last 
        mommapper.pop_back();
    size_t mommapper_size = mommapper.size();

    // WARNING: check if we have a single leg which is massless
    if (graph.strand_has_single_massless_leg(thestrand)) {
        // SPECIAL 1-LOOP CASE
        if( graph.is_bubble_with_single_massless_leg() ){
            std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)> lambda = [mommapper](const momD_conf<T, D>& mc) {
                // compute momenta
                std::vector<momD<T, D>> momenta(2);
                momenta[0] = mc.Sum(mommapper[0]);
                momenta[1] = momenta[0].get_parity_conjugated();
                // give a normalization
                momenta[1] /= (momenta[1] * momenta[0]);

                std::vector<momD<T, D>> transverse;
                std::vector<momD<T, D>> dual;
                std::tie(transverse, dual) = vNV_basis<T, D>(momenta);

                return std::make_pair(transverse, dual);
            };

            return lambda;
        }

        // GENERAL CASE
        std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)> lambda = [mommapper](const momD_conf<T, D>& mc) {
            // compute momenta
            std::vector<momD<T, D>> momenta(2);
            momenta[0] = mc.Sum(mommapper[0]);
            momenta[1] = momenta[0].get_parity_conjugated();
            // give a normalization
            momenta[1] /= (T(2) * momenta[1][0] * momenta[1][0]);

            std::vector<momD<T, D>> transverse;
            std::vector<momD<T, D>> dual;
            std::tie(transverse, dual) = vNV_basis<T, D>(momenta);

            // add the massless momenta as transverse
            transverse.insert(transverse.begin(), momenta[0]);

            return std::make_pair(transverse, dual);
        };

        return lambda;
    }

    std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)> lambda = [mommapper,mommapper_size](const momD_conf<T, D>& mc) {
        // compute momenta
        std::vector<momD<T,D>> momenta(mommapper_size);
        for(size_t ii=0;ii<mommapper_size;ii++)
            momenta[ii]=mc.Sum(mommapper[ii]);

        std::vector<momD<T,D>> transverse;
        std::vector<momD<T,D>> dual;
        std::tie(transverse,dual) = vNV_basis<T,D>(momenta);

        return std::make_pair(transverse,dual);
    };
    return lambda;
}

template <typename T>
std::enable_if_t<is_complex<T>::value,T> ASSIGN_TRANSVERSE_ENTRY() { return T(0,1); }

template <typename T>
std::enable_if_t<!is_complex<T>::value,T> ASSIGN_TRANSVERSE_ENTRY() { return T(1); }

template <typename T,size_t D>
std::vector<momD<T,D>> full_transverse_space(){
    std::vector<momD<T,D>> toret;
    for(size_t ii=0;ii<D;ii++){
        std::array<T, D> m;
        m.fill(T(0));
        if(ii==0)
            m[ii]=T(1);
        else
            m[ii]=ASSIGN_TRANSVERSE_ENTRY<T>();
        toret.push_back(momD<T,D>(m));
    }
    return toret;
}

// Builds a basis of independent vectors which fully overlap onto the transverse space
template <typename T, size_t D> std::vector<momD<T, D>> generate_transverse_space_overlap(size_t n) {
    std::vector<momD<T, D>> toret(D - n);

    for (size_t j = D - 1; j >= n; --j) {
        std::array<T, D> components;
#if 0
    // FFC: I rewrite the following command in a step-by-step process below
    //for(i=0;i<D;++i){
    //  components[i] = (!i&&!j ? Vvectors_const<T>() : T(int(i==j?1:(j<4&&i==1?(i+1):0)))) + offset; // offset always zero!
    //}
    // result, for example for a 1-loop bubble (in 5 D), should be the vectors (meant to span transverse space):
    //		v1:	(0,0,0,0,1)
    //		v2:	(0,2,0,1,0)
    //		v3:	(0,2,1,0,0)
    //		v4:	(2,1,0,0,0)
    // (this choice is irrespective of metric used!)
#endif
        // zero fill
        components.fill(T(0));
        // for case with 0-dimensional physical space we fill the j==0 zero as a pure ligh-like vector
        if (j == 0) components[0] = ASSIGN_TRANSVERSE_ENTRY<T>();
        for (size_t i = 0; i < D; ++i) {
            // fill with ones diagonal in i={0,D-1} and j={n,D-1}
            if (i == j) components[i] = T(1);
            // fill first space component (when i!=j and j<4) with 2
            else if (i == 1 && j < 4)
                components[i] = T(2);
        }
        // slight overlap of last toret vector (j==n) with time component when transverse space has dimension larger than 2
        if (toret.size() > 2 && j == n) { components[0] = T(2); }

        toret.at(D - 1 - j) = momD<T, D>(components);
    }
    return toret;
}

template <class T> std::enable_if_t<!is_exact<T>::value,bool> TEST_ZERO(const T& in){
	return abs(in)<DeltaZero<decltype(abs(in))>();
}

template <class T> std::enable_if_t<is_exact<T>::value,bool> TEST_ZERO(const T& in){
	return in==T(0);
}

template <class T, size_t D> void Alternative_NonnormalizingGramSchmidt(std::vector<momD<T, D>>& in) {
    momD<T, D> spTmp;
    std::vector<momD<T, D>> spv;
    // reverse order
    for (size_t ii = in.size(); ii > 0; --ii) spv.push_back(in[ii - 1]);

    typename std::vector<momD<T, D>>::iterator spI, spJ;

    bool TEST(false);
    for (spI = spv.begin() + 1; spI != spv.end(); spI++) {
        spTmp = *spI;
        for (spJ = spv.begin(); spJ != spI; spJ++) {
            // only subtract if projection isn't zero
            if (!TEST_ZERO(spTmp * *spJ)) { *spI -= (spTmp * *spJ) / ((*spJ)*(*spJ)) * *spJ; }
        }
        if (TEST_ZERO((*spJ)*(*spJ))) { 
           std::cerr<<"Alternative_NonnormalizingGramSchmidt fails!"<<std::endl;
           exit(1); 
        }
    }

    if (TEST)
        // try a different orthonormalization ordering
        return Alternative_NonnormalizingGramSchmidt(in);

    // map inverse to match input
    size_t jj(0);
    for (size_t ii = in.size(); ii > 0; --ii) {
        in[jj] = spv[ii - 1];
        ++jj;
    }
}

template <class T, size_t D> void NonnormalizingGramSchmidt(std::vector<momD<T, D>>& in) {
    momD<T, D> spTmp;
    std::vector<momD<T, D>> spv(in);

    typename std::vector<momD<T, D>>::iterator spI, spJ;

    bool TEST(false);
    for (spI = spv.begin() + 1; spI != spv.end(); spI++) {
        spTmp = *spI;
        for (spJ = spv.begin(); spJ != spI; spJ++) {
            // only subtract if projection isn't zero
            if (!TEST_ZERO(spTmp * *spJ)) { *spI -= (spTmp * *spJ) / ((*spJ)*(*spJ)) * *spJ; }
        }
        if (TEST_ZERO(((*spJ)*(*spJ)))) {
            TEST = true;
            break;
        }
    }

    if (TEST)
        // try a different orthonormalization ordering
        return Alternative_NonnormalizingGramSchmidt(in);

    in = spv;
}

template <class T, size_t D> void NormalizeList(std::vector<momD<T, D>>& in) {
    for (auto& m : in) { m /= sqrt(m * m); }
}

template <class T, size_t D> std::enable_if_t<!is_exact<T>::value> GramSchmidt(std::vector<momD<T, D>>& in) {
    NonnormalizingGramSchmidt(in);
    NormalizeList(in);
}

template <class T, size_t D> std::enable_if_t<is_exact<T>::value> GramSchmidt(std::vector<momD<T, D>>& in) { 
    NonnormalizingGramSchmidt(in); 
}

template <typename T,size_t D>
std::pair<std::vector<momD<T,D>>,std::vector<momD<T,D>>> vNV_basis(const std::vector<momD<T,D>>& in){
    // FIXME: in.size()==4 should take into account mom conservation!
    size_t n = D>4&&in.size()>4?5:in.size();

    // as many duals as in the entry (with a maximum of 5 (1-loop like))
    std::vector<momD<T,D>> dual(n);
    // transverse space dimension is D-n
    std::vector<momD<T,D>> transverse(D-n);

    // zero dimensional physical space:
    if(n==0){
        static const std::vector<momD<T,D>> full_trans = full_transverse_space<T,D>();
        transverse=full_trans;
        return std::make_pair(transverse,dual);
    }

    // linearized version of the Gram matrix
    std::vector<T> Gram(n*n);
    for(size_t ii=0;ii<n;ii++){
        for(size_t jj=0;jj<=ii;jj++){
            Gram[ii*n+jj]=in[ii]*in[jj];
            // symmetric entry
            Gram[jj*n+ii]=Gram[ii*n+jj];
        }
    }
    std::vector<T> InvGram = inverse(Gram);

    // compute dual vectors
    for(size_t ii=0;ii<n;ii++){
        for(size_t jj=0;jj<n;jj++){
            dual[ii]+=InvGram[ii*n+jj]*in[jj];
        }
    }

    // This is a basis of independent vectors that fully overlap the transverse space
    std::vector<momD<T,D>> tmoms = generate_transverse_space_overlap<T,D>(n);

    for(size_t ii=0;ii<D-n;ii++){
        momD<T,D> normal(tmoms.at(ii));
        for(size_t kk=0;kk<in.size();kk++){
            normal-=in[kk]*(dual[kk]*tmoms[ii]);
        }
        transverse[ii]=normal;
    }

    // Orthogonalize transverse space (it will be orthonormal if calculation is not exact)
    if(D-n>0){
        GramSchmidt<T,D>(transverse);
    }
    // let's put extra-dimensional entries to the back (instead of to the front)
    std::reverse(transverse.begin(),transverse.end());

    return std::make_pair(transverse,dual);
}

template <typename T, size_t D>
std::function<std::vector<std::vector<momentumD<T, D>>>(const momD_conf<T, D>&,const std::vector<std::vector<momD<T,D>>>&)> TwoLoopISP_momenta_evaluator(const lGraph::xGraph& in_graph) {
    using namespace lGraph;

    auto graph = in_graph.get_special_routing();

    // Check that this is only used for two-loop graphs
    if(graph.get_n_loops()!=2){
        // WARNING: careful in particular if extended to what is done below for non-planar graphs
        std::cerr<<"ERROR: calling TwoLoopISP_momenta_evaluator(.) for graph with "<<graph.get_n_loops()<<" loops!"<<std::endl;
        std::exit(94271);
    }

    // handle first factorized xGraphs
    if (graph.get_base_graph().is_factorizable()) {
        // For factorized graphs this is loop by loop transverse
        std::function<std::vector<std::vector<momentumD<T, D>>>(const momD_conf<T, D>&, const std::vector<std::vector<momD<T, D>>>&)> lambda =
            [](const momD_conf<T, D>& momDconf, const std::vector<std::vector<momD<T, D>>>& transverse) {
                std::vector<std::vector<momentumD<T, D>>> toret;
                for (size_t loop = 0; loop < transverse.size(); loop++) {
                    toret.push_back(std::vector<momentumD<T, D>>());
                    // we drop the last two entries because they are (assumed to be) extra dimensional
                    for (size_t ii = 0; ii + 2 < transverse[loop].size(); ii++) toret.back().push_back(transverse[loop][ii]);
                }
                return toret;
            };
        return lambda;
    }

    // collect here, for each Strand with assigned loop momentum, the corresponding external momentum
    std::vector<std::vector<std::vector<size_t>>> aux_mommapper_strand_by_strand;

    // Traverse all the beads (contained in the strands of the connections)
    const auto& theconnections = graph.get_base_graph().get_connection_map();

    // get all loop momenta
    auto loop_mom = graph.get_internal_momenta();

    // we choose to add first momenta from strands with momentum assignments (sorted as in get_internal_momenta(), that is l1 , ... , ln)
    for(auto& mc:loop_mom){
        const Strand& thestrand = (theconnections.at(mc.link.connection))[mc.link.strand];
        const auto& thebeads = thestrand.get_beads();
        std::vector<std::vector<size_t>> lmommapper;
        for(auto& b:thebeads)    lmommapper.push_back(b.get_momentum_indices());
        aux_mommapper_strand_by_strand.push_back(lmommapper);
    }
    
    // collect here, for each Strand with assigned loop momentum, the corresponding external momentum of other strands with momentum
    std::vector<std::vector<std::vector<size_t>>> mommapper_strand_by_strand(aux_mommapper_strand_by_strand.size());

    // we add momenta from different strands into each other
    for(size_t ii=0;ii<aux_mommapper_strand_by_strand.size();ii++){
        for(size_t jj=0;jj<mommapper_strand_by_strand.size();jj++){
            if(ii!=jj)
                mommapper_strand_by_strand[ii].insert(mommapper_strand_by_strand[ii].end(),aux_mommapper_strand_by_strand[jj].begin(),aux_mommapper_strand_by_strand[jj].end());
        }
    }

    // traverse the nodes of the graph
    const auto& thenodes = graph.get_base_graph().get_nodes();

    for(auto& node:thenodes){
        if(node.get_momentum_indices().size()>0){
            auto lmommapper = node.get_momentum_indices();
            for(size_t ii=0;ii<mommapper_strand_by_strand.size();ii++){
                mommapper_strand_by_strand[ii].push_back(lmommapper);
            }
        }
    }

    // we add momenta in other strands
    //size_t shared_beads(0);
    for (auto& conn : theconnections) {
        const auto& thestrands = conn.second.get_edges();
        for (auto& strand : thestrands) {
            // only strands without mom assigned (as they were already added)
            if (!strand.has_link_with_mom_label()) {
                const auto& thebeads = strand.get_beads();
                for (auto& b : thebeads) {
                    //shared_beads++;
                    auto lmommapper = b.get_momentum_indices();
                    for (size_t ii = 0; ii < mommapper_strand_by_strand.size(); ii++) { mommapper_strand_by_strand[ii].push_back(lmommapper); }
                }
            }
        }
    }

    // total number of momenta attached to this graph
    size_t n_momenta = graph.get_n_topology_legs();
    // the dimension of the total transverse space
    size_t common_trans_dim(0);
    if (n_momenta < 5) common_trans_dim = 5 - n_momenta;

    // obtain dimensions of needed ISP vectors
    std::vector<size_t> needed_dim_per_strand = graph.n_OS_variables_with_special_routing();
    for(size_t ii=0;ii<needed_dim_per_strand.size();ii++)   needed_dim_per_strand[ii]-= common_trans_dim;

    // store info of variables
    for (size_t loop = 1; loop <= mommapper_strand_by_strand.size(); loop++) {
        for (size_t insertionid = 1; insertionid <= needed_dim_per_strand[loop - 1]; insertionid++) {
            StandardInsertions::define(graph, loop, insertionid, mommapper_strand_by_strand[loop - 1][insertionid - 1]);
        }
    }

    // Only build a basis completing the common space
    std::function<std::vector<std::vector<momentumD<T, D>>>(const momD_conf<T, D>&, const std::vector<std::vector<momD<T, D>>>&)> lambda =
        [needed_dim_per_strand, mommapper_strand_by_strand](const momD_conf<T, D>& momDconf,
                                                                             const std::vector<std::vector<momD<T, D>>>& transverse) {
            std::vector<std::vector<momentumD<T, D>>> toret;

            for(size_t kk=0;kk<mommapper_strand_by_strand.size();kk++){
                toret.push_back(std::vector<momentumD<T, D>>());
                for (size_t ii = 0; ii < needed_dim_per_strand[kk]; ii++)
                    toret.back().push_back(momDconf.Sum(mommapper_strand_by_strand[kk][ii]));
            }

            return toret;
        };
    return lambda;
}


template <typename T,size_t D>
std::function<OnShellPoint<T,D>(const OnShellPoint<T,D>&,const momD_conf<T,D>&)> OnShellPointMap(const lGraph::xGraph& from,const lGraph::xGraph& to){
    auto absorb = abstract_OnShellPointMap(from, to);

    std::function<OnShellPoint<T,D>(const OnShellPoint<T,D>&,const momD_conf<T,D>&)> lambda = [absorb](const OnShellPoint<T,D>& os,const momD_conf<T,D>& mc){
        std::vector<momD<T,D>> tr;
        for(const auto& e:absorb){
            momD<T,D> ta;
            for(size_t ii=0;ii<e.loop_sign.size();ii++)
                ta+=T(e.loop_sign[ii])*os[e.loop_indices[ii]];
            for(size_t ii=0;ii<e.external_sign.size();ii++)
                ta+=T(e.external_sign[ii])*mc.p(e.external_indices[ii]);
            tr.push_back(ta);
        }
        return tr;
    };
    return lambda;
}


template <class T> std::enable_if_t<!is_exact<T>::value,bool> TEST_IS_ZERO(const T& in){
	return abs(in)<DeltaZero<decltype(abs(in))>();
}

template <class T> std::enable_if_t<is_exact<T>::value,bool> TEST_IS_ZERO(const T& in){
	return in==T(0);
}

template <class T,unsigned D>
void Alternative_NonnormalizingGramSchmidt(std::vector<sp_t<T,D> > &ext_spv){
  sp_t<T,D> spTmp;
  std::vector<sp_t<T,D> > spv;
  // reverse order
  for(size_t ii=ext_spv.size();ii>0;--ii)
	spv.push_back(ext_spv[ii-1]);

  typename std::vector<sp_t<T,D> >::iterator spI, spJ;
  
  bool TEST(false);
  for(spI=spv.begin()+1; spI!=spv.end(); spI++){
    spTmp=*spI;
#if _DEBUG_OSM
std::cout<<"(A) Incoming: "<<spTmp<<std::endl;
int count(0);
#endif
    for(spJ=spv.begin(); spJ!=spI; spJ++){
      // only subtract if projection isn't zero
      if(!TEST_IS_ZERO(spTmp**spJ)){
        *spI-=(spTmp**spJ)/(*spJ).norm()**spJ;
      }
#if _DEBUG_OSM
++count;
std::cout<<"(A) Step "<<count<<" obtained: "<<*spI<<" squared: "<<*spI**spI<<" norm: "<<(*spJ).norm()<<std::endl;
#endif
    }
    if(TEST_IS_ZERO((*spJ).norm())){
#if _DEBUG_OSM
	std::cout<<"In Alternative_NonnormalizingGramSchmidt we detected a zero-norm vector used for normalization! Maybe some other ordering would help!"<<std::endl;
	std::cout<<"ERROR: exit 1"<<std::endl;
#endif
	exit(1);
    }
      
  }

  if(TEST)
	// try a different orthonormalization ordering
	return Alternative_NonnormalizingGramSchmidt(ext_spv);


  // map inverse to match input
  size_t jj(0);
  for(size_t ii=ext_spv.size();ii>0;--ii){
	ext_spv[jj]=spv[ii-1];
	++jj;
  }

}

template <class T,unsigned D>
void NonnormalizingGramSchmidt(std::vector<sp_t<T,D> > &ext_spv){
  sp_t<T,D> spTmp;
  std::vector<sp_t<T,D> > spv(ext_spv);

  typename std::vector<sp_t<T,D> >::iterator spI, spJ;
  
  bool TEST(false);
  for(spI=spv.begin()+1; spI!=spv.end(); spI++){
    spTmp=*spI;
#if _DEBUG_OSM
std::cout<<"Incoming: "<<spTmp<<std::endl;
int count(0);
#endif
    for(spJ=spv.begin(); spJ!=spI; spJ++){
      // only subtract if projection isn't zero
      if(!TEST_IS_ZERO(spTmp**spJ)){
        *spI-=(spTmp**spJ)/(*spJ).norm()**spJ;
      }
#if _DEBUG_OSM
++count;
std::cout<<"Step "<<count<<" obtained: "<<*spI<<" squared: "<<*spI**spI<<" norm: "<<(*spJ).norm()<<std::endl;
#endif
    }
    if(TEST_IS_ZERO((*spJ).norm())){
#if _DEBUG_OSM
	std::cout<<"In NonnormalizingGramSchmidt we detected a zero-norm vector used for normalization! Will try opposite ordering with Alternative_NonnormalizingGramSchmidt"<<std::endl;
#endif
	TEST=true;
	break;
    }
      
  }

  if(TEST)
	// try a different orthonormalization ordering
	return Alternative_NonnormalizingGramSchmidt(ext_spv);

  ext_spv=spv;
}

template <class T,unsigned D>
void NormalizeList(std::vector<sp_t<T,D>> &spv){
  for(auto& spI : spv){
    spI/=sqrt((spI).norm());
  }
}

template <class T,unsigned D>
std::enable_if_t<!is_exact<T>::value> GramSchmidt(std::vector<sp_t<T,D> > &spv){
#if _DEBUG_OSM
std::cout<<"spv 1: "<<spv<<std::endl;
#endif
  NonnormalizingGramSchmidt(spv);
#if _DEBUG_OSM
std::cout<<"spv 2: "<<spv<<std::endl;
#endif
  NormalizeList(spv);
#if _DEBUG_OSM
std::cout<<"spv 3: "<<spv<<std::endl;
#endif
}
template <class T,unsigned D>
std::enable_if_t<is_exact<T>::value> GramSchmidt(std::vector<sp_t<T,D> > &spv){
  NonnormalizingGramSchmidt(spv);
}


template<typename T>
constexpr std::enable_if_t<is_complex<T>::value,T> Vvectors_const(){
  return T(0,1);
}

template<typename T>
constexpr std::enable_if_t<!is_complex<T>::value,T> Vvectors_const(){
  return T(1);
}

template <class T,unsigned D,class Text,unsigned Dext>
void Vvectors(const std::vector<sp_t<Text,Dext> >& klist, 
	      std::vector<sp_t<Text,Dext> > &dualv,
	      // dimension Dext
	      std::vector<sp_t<T,D> > &Nvec, // dimension D-n
	      T offset){

  unsigned i,j, n=Dext>4&&klist.size()>4?5:klist.size();

  sp_t<Text,Dext> buf;
#if _DEBUG_OSM
std::cout<<"IN: ";
for(auto& kk:klist)
	std::cout<<kk<<" ";
std::cout<<std::endl;
#endif

  dualv=std::vector<sp_t<Text,Dext> >(n,sp_t<Text,Dext>());
  Nvec=std::vector<sp_t<T,D > >(D-n,sp_t<T,D >());

  if(!n) {
#if _DEBUG_OSM
std::cout<<"OSM case with zero dimensional physical space"<<std::endl;
#endif
    for(i=0;i<D;++i)
      for(j=0;j<D;++j){
        Nvec[i][j]=i==j&&!i
	  ?T(1)
	  :(i==j?Vvectors_const<T>():T(0));
      }
    return;
  }

  // setting s
  //cout<<"s:"<<endl;
  std::vector<Text> s(n*n);
  for(i=0;i<n;++i) {
    for(j=0;j<n;++j) {
      s[i*n+j]=klist[i]*klist[j];
      //cout<<chop(s[i*n+j])<<"\t";
    }
    //cout<<endl;
  }
  
  std::vector<Text> invs=inverse(s);
#if _DEBUG_OSM
std::cout<<"Gram matrix: "; for(auto& ss:s) std::cout<<ss<<" "; std::cout<<std::endl;
std::cout<<"Inverse:     "; for(auto& ss:invs) std::cout<<ss<<" "; std::cout<<std::endl;
#endif

  // setting dualv
  //cout<<"inverse:"<<endl;
  for(i=0;i<n;++i) {
    for(j=0;j<n;++j) {
      dualv[i]+=invs[i*n+j]*klist[j];
      //cout<<chop(invs[i*n+j])<<"\t";
    }
    //cout<<endl;
  }
#if _DEBUG_OSM
for(i=0;i<n;++i) {
  std::cout<<"dual_"<<i<<": "<<dualv[i]<<" squared: "<<dualv[i]*dualv[i]<<std::endl;
}
#endif
  
  // Build tmoms - a basis of independent vectors which fully overlap onto the transverse space.
  std::vector<sp_t<T,D>> tmoms(D-n);
  for(j=D-1;j>=n;--j){
    std::array<T,D> components;
#if 0
    // FFC: I rewrite the following command in a step-by-step process below
    //for(i=0;i<D;++i){
    //  components[i] = (!i&&!j ? Vvectors_const<T>() : T(int(i==j?1:(j<4&&i==1?(i+1):0)))) + offset;
    //}
    // result, for example for a 1-loop bubble (in 5 D), should be the vectors (meant to span transverse space):
    //		v1:	(0,0,0,0,1)
    //		v2:	(0,2,0,1,0)
    //		v3:	(0,2,1,0,0)
    //		v4:	(2,1,0,0,0)
    // (this choice is irrespective of metric used!)
#endif
    // zero fill
    components.fill(T(0));
    // for case with 0-dimensional physical space we fill the j==0 zero as a pure ligh-like vector
    if(j==0)
      components[0]=Vvectors_const<T>();
    for(i=0;i<D;++i){
      // fill with ones diagonal in i={0,D-1} and j={n,D-1}
      if(i==j)
        components[i]=T(1); 
      // fill first space component (when i!=j and j<4) with 2
      else if(i==1&&j<4)
        components[i]=T(2); 
      // offset term
      components[i]+=offset;
    }
    // slight overlap of last tmom vector (j==n) with time component when transverse space has dimension larger than 2
    if(tmoms.size()>2&&j==n){
      components[0]=T(2);
    }
#if _DEBUG_OSM
std::cout<<"j= "<<j<<" components[D]: "<<components<<std::endl;
#endif

    tmoms.at(D-1-j) = sp_t<T,D>(&components[0]);
  }
	

  
  /*
  auto sp_t_to_mom = [](const sp_t<Text,Dext>& evgenijP){
    std::array<T,D> components;
    for (size_t i = 0; i < Dext && i < D; i++){
      components[i] = T(evgenijP[i]);
    }

    for (size_t i = Dext; i < D; i++){
      components[i] = 0;
    }

    momentumD<T,D> result (&components[0]);
    return result;
  };

  auto mom_to_sp_t = [](const momentumD<T,D>& mom){
    std::array<T,D> components;

    for (size_t i = 0; i < D; i++){
      components[i] = mom.pi(i);
    }

    sp_t<T,D> result (&components[0]);

    return result;
  };
  */

  for (size_t i = 0; i < D-n; i++){
    sp_t<T,D> Nmom(tmoms.at(i));
    for (size_t k = 0; k < klist.size(); k++){
       Nmom -= klist.at(k)*( dualv[k]*tmoms.at(i) );
    }

#if _DEBUG_OSM
std::cout<<"i: "<<i<<" Nmom: "<<Nmom<<" squared: "<<Nmom*Nmom<<std::endl;
#endif
    Nvec.at(i) = Nmom;
  }

#if _DEBUG_OSM
std::cout<<"N-projections: ";
for (size_t ii = 0; ii < D-n; ii++){
  for (size_t jj = ii+1; jj < D-n; jj++){
	std::cout<<ii<<","<<jj<<" "<<Nvec[ii]*Nvec[jj]<<" ";
  }
}
std::cout<<std::endl;
#endif

  // Orthogonalize transverse space.
  if(D-n){
    GramSchmidt<T,D>(Nvec);
  }

}

template <class T,unsigned D,class Text, unsigned Dext>
sp_t<T,D> LoopMomenta(std::vector<T> masses, 
		      std::vector<sp_t<Text,Dext> > k, 
		      std::vector<T>  props,
		      std::vector<T> vars,
		      bool masslessbubble) {
  if(!k.size())
    return sp_t<T,D>();
  
  unsigned i,j, n=k.size();
 
  // setting v and nvec
  std::vector<sp_t<Text,Dext> > v;
  std::vector<sp_t<T,D> > nvec;

  if(masslessbubble) {
    std::vector<sp_t<Text,Dext> >kp(2,k.front());
    for(i=1;i<Dext;++i)
      kp.back()[i]*=-1;
    
    Vvectors<T,D,Text,Dext>(kp,v,nvec,T(0,0));

    v=std::vector<sp_t<Text,Dext> >(v.begin()+1,v.end());
    nvec.push_back(kp.front());
  }
  else
    Vvectors<T,D,Text,Dext>(k,v,nvec,T(0,0));
#if _DEBUG_OSM
std::cout<<"After calling Vvectors: "<<std::endl;
std::cout<<"v: "<<v<<std::endl;
std::cout<<"n: "<<nvec<<std::endl;
#endif
  
  // setting rho and m2
  std::vector<T> m2(n+2), rho(n+2);
  for(i=1;i<n+2;++i){
    m2[i]=masses[i-1]*masses[i-1];
    rho[i]=props[i-1];  
  }
  m2[0]=m2[n+1];
  rho[0]=rho[n+1];
  
  // setting q2
  std::vector<sp_t<T,D> >
    q(n+2,sp_t<T,Dext>());
  std::vector<T> q2(n+2);
  
  //cout<<"calculate q[i]..."<<endl;
  for(i=1;i<n+2;++i) {
    if(i<n+1)
      for(j=0;j<i;++j) {
	q[i]=q[i]-k[j];
      }
    q2[i]=q[i].norm();
  }
  q2[0]=q2[n+1];
  
  // setting VR
  sp_t<T,D> VR;
  for(i=0;i<n;++i)
    VR+=v[i]*(rho[i+1]+m2[i+1]-q2[i+1]
  	      -(rho[i]+m2[i]-q2[i]))/T(-2);
  
  // set valpha
  T norm=-(VR.norm()-m2[0]-rho[0]);

  n=v.size();

  std::vector<T> valpha(D-n);
  
  if(D-n==1)
    valpha.front()=T(0,1)*sqrt(-norm);
  
  else if(D-n>1){
    //t is chosen to be the last relevant parameter variable.
    T t=vars.at(D-n-2);

    //We set all but the first two alphas to the respective
    //parameter variables.
    for(i=0;i<D-n-2;++i){
      valpha[i+2]=vars[i];

      //In case of massless bubble the last coordinate
      //corresponds to the external momentum, which squares to
      //zero, so that it shall not be subtracted from the norm.
      if(i<D-n-3||!masslessbubble)
	norm-=vars[i]*vars[i];
    }
    valpha[0]=(norm/t+t)/T(2);
    valpha[1]=(norm/t-t)/T(0,-2);
  }	
  
  // set Lloop
  sp_t<T,D> Lloop;
  for(i=0;i<D-n;++i)
    Lloop+=valpha[i]*nvec[i];
  Lloop+=VR;

  return Lloop;
}

template <class T,unsigned D>
sp_t<T,D> LoopMomenta(std::vector<sp_t<T,D> > k, 
		      std::vector<T> vars,
		      bool masslessbubble) {

  return LoopMomenta<T,D>(std::vector<T>(k.size()+1),
			  k,
			  std::vector<T>(k.size()+1),
			  vars,
			  masslessbubble);  
}

template <class T,unsigned D>
sp_t<T,D> LoopMomenta(std::vector<T>  masses, 
		      std::vector<sp_t<T,D> > k, 
		      std::vector<T> props,
		      std::vector<T> vars) {
  return LoopMomenta(masses,k,props,vars,false);  
}

template <class T,unsigned D>
sp_t<T,D> LoopMomenta(std::vector<sp_t<T,D> > k, 
		      std::vector<T> vars){
  return LoopMomenta<T,D>(k,vars,false);
}

#define _I_OSM_DIM_GEN(KEY,TYPE1,TYPE2,DIM1,DIM2)			\
  KEY template sp_t<TYPE1,DIM1>						\
  LoopMomenta<TYPE1,DIM1,TYPE2,DIM2> (std::vector<TYPE1>masses,		\
				      std::vector<sp_t<TYPE2,DIM2> >k,	\
				      std::vector<TYPE1>props,		\
				      std::vector<TYPE1>vars,		\
				      bool masslessbubble);		\
  KEY template void							\
  Vvectors<TYPE1,DIM1,TYPE2,DIM2> (const std::vector<sp_t<TYPE2,DIM2> >& klist,	\
				   std::vector<sp_t<TYPE2,DIM2> >&dualv,	\
				   std::vector<sp_t<TYPE1,DIM1> >&Nvec,      \
				   TYPE1 offset);

#define  _I_OSM_TYPE_GEN(KEY,TYPE1,TYPE2)	\
  _I_OSM_DIM_GEN(KEY,TYPE1,TYPE2,4,4)		\
  _I_OSM_DIM_GEN(KEY,TYPE1,TYPE2,5,4)		\
  _I_OSM_DIM_GEN(KEY,TYPE1,TYPE2,6,4)		\
  _I_OSM_DIM_GEN(KEY,TYPE1,TYPE2,5,5)		\
  _I_OSM_DIM_GEN(KEY,TYPE1,TYPE2,6,5)		\
  _I_OSM_DIM_GEN(KEY,TYPE1,TYPE2,6,6)			

#define  _I_OSM_TYPE(TYPE1,TYPE2) _I_OSM_TYPE_GEN(,TYPE1,TYPE2)
#define  _I_OSM_TYPE_EXTERN(TYPE1,TYPE2) _I_OSM_TYPE_GEN(extern,TYPE1,TYPE2)

_I_OSM_TYPE_EXTERN(C,R)

_I_OSM_TYPE_EXTERN(C,C)
#ifdef HIGH_PRECISION
_I_OSM_TYPE_EXTERN(CHP,CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_I_OSM_TYPE_EXTERN(CVHP,CVHP)
#endif

#ifdef INSTANTIATE_GMP
_I_OSM_TYPE_EXTERN(CGMP,CGMP)
#endif

} // namespace OnShellStrategies
} // namespace Caravel

