/** 
 * @file osm.h
 *
 * Functionalities for on-shell loop-momenta generation
*/

#pragma once

#include <algorithm>
#include <functional>
#include <mutex>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Core/typedefs.h"
#include "Core/Utilities.h"
#include "osm_spinors.hpp"
#include "Core/momD_conf.h"
#include "misc/DeltaZero.h"
#include "Graph/Graph.h"
#include "Core/type_traits_extra.h"

#include "LinearAlgebra.h"
#include "Core/Debug.h"

#include "Core/MathOutput.h"

#define MAX_PROP 1e10

namespace Caravel {
namespace OnShellStrategies {

class StandardInsertions {
    using Gtype = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
    static std::unordered_map<std::tuple<Gtype, size_t, size_t>, std::vector<size_t>> insertion_map;

  public:
    StandardInsertions() = delete;
    /**
     * Defines for xg.get_special_routing() the ith alpha variable for loop corresponding to loop * momenta
     *
     * Gives an error if redefinition is attempted
     */
    static void define(const lGraph::xGraph& xg, size_t loop, size_t index, const std::vector<size_t>& momenta);
    /**
     * Returns momenta corresponding to standard insertion
     */
    static const std::vector<size_t>& insertion_momenta(const lGraph::xGraph& xg, size_t loop, size_t index);
};

/**
 * Given an xGraph this function returns a functor that can be evaluated over phase space
 * to get a set of transverse and dual (Van-Neerven Vermaseren) momenta to the whole xGraph,
 * making the calculation basically 1-loop like
 *
 * There is a choice made in the order in which global momenta attached to xGraph is handled:
 *
 *     first: all strands with loop momentum assignment are traverse (l1, l2, ... , ln), from top to bottom
 *
 *     second: all momenta attached to nodes (according to cannonical order in the xGraph)
 *
 *     third: all other strands (according to cannonical order in the xGraph)
 *
 * Notice that this has an impact in the common-dual momenta produced
 *
 * @return std::function with argument momD_conf and return type:
 *               std::pair: first: vector<momD> representing the corresponding set of common-transverse momenta; 
                            second: vector<momD> representing the corresponding set of common-dual momenta
 */
template <typename T, size_t D>
std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)> common_transverse_dual_evaluator(const lGraph::xGraph&);

/**
 * Given an xGraph this function returns a functor that can be evaluated over phase space
 * to get a set of transverse vectors to the whole xGraph, together with a set of bases of
 * common dual vectors, one for each Strand on the xGraph that has assigned a loop momentum.
 * Notice that this can easily later on be extented to cover non-planar two-loop cases or
 * other multi-loop graphs.
 *
 * Ordering of the strand-by-strand dual bases is according to:
 *
 * 1) Momenta attached to strand with a loop-momentum assignement. Always from 'top-to-bottom' (no flip due to direction)
 *
 * 2) We add momenta from other strands with momentum assignment as collected in '1)'. We do it sequentially (l1, l2, ...)
 *
 * 3) Momenta attached to vertices (in corresponding order)
 *
 * 4) Momenta attached to strands without loop-momentum assignment (in the order of the canonicalization of xGraph)
 *
 * @return std::function with argument momD_conf and return type:
 *               std::pair: first: vector<momD> representing the corresponding set of common-transverse momenta; 
                            second: vector<vector<momD>> representing the bases of common-dual momenta adapted 
                                    to each Strand with loop-momentum assigment
 */
template <typename T, size_t D>
std::function<std::pair<std::vector<momD<T, D>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)> common_transverse_strand_by_strand_dual_evaluator(const lGraph::xGraph&);

/**
 * Given an xGraph this function returns a functor that can be evaluated over phase space
 * to get a set of transverse and dual (Van-Neerven Vermaseren) momenta to each Strand with
 * an internal momentum assignment according to xGraph::get_internal_momenta(). Momenta
 * traversed in given order of the strand.
 *
 * The outputted transverse/dual momenta follows the same ordering as the internal momenta
 * in the passed xGraph's.
 *
 * @return std::function with argument momD_conf and return type:
 *               std::pair: first: vector<vector<momD>> representing for each loop mom the corresponding set of transverse momenta; 
                            second: vector<vector<momD>> representing for each loop mom the corresponding set of dual momenta
 */
template <typename T, size_t D>
std::function<std::pair<std::vector<std::vector<momD<T, D>>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)> transverse_dual_evaluator(const lGraph::xGraph&);

/**
 * This function returns a functor that produces a van Neerven-Vermaseren basis over phase space (returning a (transverse,dual) pair),
 * according to the external momenta attached to the Strand that is pick by the MomChoice argument passed. Momenta picked from top to bottom.
 *
 * WARNING: Notice the special handling of strands with single massless momentum!
 */
template <typename T, size_t D>
std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)> transverse_dual_from_Strand(const lGraph::MomChoice&,const lGraph::xGraph&);

/**
 * Templated function to produce a van Neerven-Vermaseren basis 
 * for a given set of momenta. Returns a pair that contains:
 *
 * first: vector of momD<T,D> representing the set of transverse momenta
 *
 * second: vector of momD<T,D> representing the set of dual vectors (to the passed ones)
*/
template <typename T,size_t D>
std::pair<std::vector<momD<T,D>>,std::vector<momD<T,D>>> vNV_basis(const std::vector<momD<T,D>>&);

/**
 * Auxiliary function to allow construction of ISP momenta in two-loop graphs
 *
 * Factorized 2-loop graphs are just filled with corresponding transverse momenta.
 *
 * Other two-loop graphs are filled by momenta from the complementary loop, using the loop-momentum assignment corresponding
 * to xGraph::get_special_routing(), according to:
 *
 * 1) The number of independent variables computed through the usage of xGraph::n_OS_variables_with_special_routing()
 *
 * 2) Traversing the momenta attached to strands of other momenta according to xGraph::get_special_routing() assignments (top to bottom, in increasing l1, l2, ... order)
 *
 * 3) Traversing the momenta attached to nodes (as defined canonically)
 *
 * 4) Traversing the momenta attached to the leftover strands (as defined canonically)
 */
template <typename T, size_t D>
std::function<std::vector<std::vector<momentumD<T, D>>>(const momD_conf<T, D>&,const std::vector<std::vector<momD<T,D>>>&)> TwoLoopISP_momenta_evaluator(const lGraph::xGraph& graph);

/**
 * Data structure to represent a map between a set of loop momenta {l_from_i} and a 
 * loop momentum l_to_j
 */
struct info_OS_map {
    std::vector<int> loop_sign; /**< each entry represents the sign of the corresponding loop momentum in 'loop_indices' */
    std::vector<size_t> loop_indices; /**< the indices of the loop momenta in the 'from' xGraphs that contribute to the given loop momentum in 'to' */
    std::vector<int> external_sign; /**< each entry represents the sign of the corresponding external momentum in 'external_indices' */
    std::vector<size_t> external_indices; /**< the indices of the external momenta in the 'from' xGraphs that contribute to the given loop momentum in 'to' */
};

std::string math_string(const info_OS_map&);

bool operator==(const info_OS_map&, const info_OS_map&);

info_OS_map TransformMomRoutingOSMap(const lGraph::MomentumRouting& mr);
lGraph::MomentumRouting TransformOSMapMomRouting(const info_OS_map& im);

/**
 * This function returns abstract information about the loop-momenta map between two topologically-related
 * xGraphs. 
 *
 * The returned vector contains the data to map each loop momentum in the xGraphs in a struct 'info_OS_map'
 * (one entry for each loop momentum).
 */
std::vector<info_OS_map> abstract_OnShellPointMap(const lGraph::xGraph& from,const lGraph::xGraph& to);

void print_vector_info_OS_map(const std::vector<info_OS_map>&, std::ostream& o = std::cout);
void compact_print_vector_info_OS_map(const std::vector<info_OS_map>&, std::ostream& o = std::cout, const std::string& = "to", const std::string& = "from" );

/**
 * Given an abstract OnShellPointMap l_to_j(l_from_i) it returns the inverse
 * l_from_i(l_to_j). The unsigned integer is the number of external momenta
 * to apply momentum conservation
 */
std::vector<info_OS_map> invert_abstract_OnShellPointMap(size_t n, const std::vector<lGraph::MomentumRouting>&);

/**
 * This function returns an std::function that can map loop momenta associated to the xGraph 'from' into the corresponding one of xGraph 'to'
 */
template <typename T,size_t D>
std::function<OnShellPoint<T,D>(const OnShellPoint<T,D>&,const momD_conf<T,D>&)> OnShellPointMap(const lGraph::xGraph& from,const lGraph::xGraph& to);

/**
   Function that returns the loop momentum of a one-loop
   diagram provided the external kinematics
   @param masses a vector of masses of the external
   particles
   @param k vector of sp_t external
   momenta
   @param props vector of the desired values the
   propagators shall have
   @param vars vector of variables parametrizing the
   transverse space. They are read as
   \f$
   \alpha_3,\alpha_4,\dots,t
   \f$
   and the first two coefficients determined by
   \f[
   \alpha_1=\frac{1}{2}\Bigl(t-
   \frac{V^2+\alpha_3^2+\alpha_4^2+\cdots}{t}\Bigr)
   ,\qquad
   \alpha_2=\frac{1}{2\mathrm{i}}\Bigl(t+
   \frac{V^2+\alpha_3^2+\alpha_4^2+\cdots}{t}\Bigr)
   \f]
   where \f$V^2\f$ is the norm of the physical part of the loop
   momentum
   @param masslessbubble whether the diagram is a massless
   bubble, because the parametrization is then different
   @return loop momentum with D entries of type T 
 */
template <class T,unsigned D,class T2,unsigned D2>
sp_t<T,D> LoopMomenta(std::vector<T> masses, 
		      std::vector<sp_t<T2,D2> > k, 
		      std::vector<T> props,
		      std::vector<T> vars,
		      bool masslessbubble);
/**
   Function that returns duals to provided momenta and
   orthonormal vectors transverse to these
   @param klist list of momenta
   @param dualv pointer where to write the duals
   @param Nvec pointer where to write the transverse momenta
   @param offset moving the transverse space in the case of a
   massless triangle
*/
template <class T,unsigned D,class Text,unsigned Dext>
void Vvectors(const std::vector<sp_t<Text,Dext> >& klist, 
	      std::vector<sp_t<Text,Dext> > &dualv,
	      std::vector<sp_t<T,D> > &Nvec, // dimension D-n
	      T offset);

} // namespace OnShellStrategies
} // namespace Caravel

#include "osm.hpp"

