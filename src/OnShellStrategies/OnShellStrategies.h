#ifndef ON_SHELL_STRATEGIES_H_INC
#define ON_SHELL_STRATEGIES_H_INC

#include <functional>
#include <vector>
#include <cmath>
#include "AmplitudeCoefficients/NumericalTools.h"
#include "Graph/Graph.h"
#include "Graph/GraphKin.h"
#include "Core/type_traits_extra.h"

namespace Caravel{

    /**
     * Interface for strategies which generate on-shell momentum. In full
     * generality there are a number of degrees of freedom for each loop
     * momenta. Each set is pacakaged into a vector, each of which are
     * elements of another vector. The strategy then takes these to
     * generate an on-shell momenta for a given topology.
     */

    template<typename T>
    using OnShellCoordinates = std::vector<std::vector<T>>;

    template<typename T, size_t D>
    using OnShellMomentumParameterization = std::function<OnShellPoint<T,D>(const OnShellCoordinates<T>&)>;

    /**
     * Out of an xGraph and a corresponding GraphKin (assumed to be related, though no check is performed),
     * we construct an on-shell momenta generator for two-loop diagrams. Two loops is assumed, as momenta is
     * fixed to '6' in the specialization of OnShellMomentumParameterization. Extensions to other number of
     * loops should be possible.
     *
     * Strategy follows section II.C.2 of arXiv::1712.03946.
     *
     * Momentum routing, for defining conventions, is taken from the passed xGraph.
     *
     * FIXME: it is slightly awkward to pass the 'GraphKin' as an argument, just to copy the reference. It might
     *        be better to pass it as an argument of the returned functor ('OnShellMomentumParameterization<F,6>')
     */
    template<typename F>
    OnShellMomentumParameterization<F,6> surd_parameterization(const lGraph::xGraph&,const lGraph::lGraphKin::GraphKin<F,6>&);

    template<typename F>
    OnShellMomentumParameterization<F,6> surd_massive_parameterization(const lGraph::xGraph&,const lGraph::lGraphKin::GraphKin<F,6>&);

    template<typename F>
    OnShellMomentumParameterization<F,6> nonplanar_surd_parameterization(const lGraph::xGraph&,const lGraph::lGraphKin::GraphKin<F,6>&);

}

#include "OnShellStrategies.hpp" // Template code

#endif 
