#include <vector>
#include <exception>
#include "misc/MiscMath.h"

namespace Caravel{
namespace OnShellStrategies{
    template <class T>
    std::vector<T> Minor(std::vector<T> a, unsigned k, unsigned l) {
	unsigned n=sqrt(a.size()), iM=0;
	std::vector<T> m((n-1)*(n-1));
	for(unsigned iA=0;iA<n;++iA)
	    if(iA!=k){
	    unsigned jM=0;
	    for(unsigned jA=0;jA<n;++jA)
		if(jA!=l)
		m[jM+++(n-1)*iM]=a[n*iA+jA];
	    iM++;
	    }
	return m;
    }

    template <class T>
    T det(std::vector<T> a) {
	unsigned n=sqrt(a.size());
	if(n==1) return a[0];
	unsigned i;
	T d=T(0),s=T(1);
	for(i=0;i<n;++i){
	    d+=s*a[i]*det<T>(Minor<T>(a,0,i));
	    s*=T(-1);
	}
	return d;
    }

    template <class T>
    std::vector<T> inverse(std::vector<T> a) {
	unsigned i,j, n=sqrt(a.size());
	if(n==1)
	    return std::vector<T>(1,(T(1))/a[0]);
	std::vector<T> invs(a.size());
	T d=det<T>(a);
	for(i=0;i<n;++i)
	    for(j=0;j<n;++j)
	    invs[i*n+j]=(T)prod_pow(-1,i+j)*det<T>(Minor<T>(a,j,i))/(T)d;
	return invs;
    }

  template<typename F, size_t D>
  std::vector<F> generalized_gram_matrix(const std::vector<momD<F,D>>& vs, const std::vector<momD<F,D>>& ws){
    auto size = vs.size();
    if (size != ws.size()){
      throw std::runtime_error("Mismatch in vector sizes in generalized_gram_matrix.");
    }
    
    std::vector<F> gram_matrix(size*size);
    for (size_t i = 0; i < size; i++){
      for (size_t j = 0; j < size; j++){
	gram_matrix.at(i + j*size) = vs.at(i)*ws.at(j);
      }
    }

    return gram_matrix;
  }
  
  template<typename F, size_t D>
  F generalized_gram(const std::vector<momD<F,D>>& vs, const std::vector<momD<F,D>>& ws){
    return det(generalized_gram_matrix(vs, ws));
  }
    
  template<typename F, size_t D>
  F gram(const std::vector<momD<F,D>>& vs){
    return generalized_gram(vs, vs);
  }

}
}
