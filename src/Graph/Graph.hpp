namespace Caravel {
namespace lGraph {

using Caravel::operator<<;

template <typename EdgeType1, typename EdgeType2> bool operator<(const Connection<EdgeType1>& l1, const Connection<EdgeType2>& l2) {
    if constexpr (!std::is_same<EdgeType1, EdgeType2>::value) {
        std::cout << "trying to compare links of inequal types -- returned false" << '\n';
        return false;
    }

    // Check full info
    return std::lexicographical_compare(l1.get_edges().begin(), l1.get_edges().end(), l2.get_edges().begin(), l2.get_edges().end());
}

template <typename EdgeType1, typename EdgeType2> bool operator==(const Connection<EdgeType1>& l1, const Connection<EdgeType2>& l2) {
    return !(l1 < l2) && !(l2 < l1);
}

template <typename EdgeType1, typename EdgeType2> bool operator>(const Connection<EdgeType1>& l1, const Connection<EdgeType2>& l2) { return l2 < l1; }

template <typename EdgeType> Connection<EdgeType> concatenate_connection(Connection<EdgeType>& c1, Connection<EdgeType>& c2) {
    Connection<EdgeType> toret(c1);
    toret.append(c2);
    toret.canonicalize();
    return toret;
}

template <typename T> std::vector<std::vector<size_t>> to_adjacency(std::vector<std::vector<T>>& matr) {
    size_t n = matr.size();
    if (n == 0) {
        std::vector<std::vector<size_t>> toret;
        return toret;
    }
    else {
        size_t m = matr.at(0).size();
        if (n != m) {
            std::cout << "ERROR: matrix representing vacuum graph has to be square! " << std::endl;
            std::cout << "recieved " << n << " " << m << std::endl;
            exit(1);
        }
        std::vector<std::vector<size_t>> toret(n, std::vector<size_t>(n, 0));
        for (size_t ii = 0; ii < n; ii++) {
            for (size_t jj = 0; jj < n; jj++) { toret[ii][jj] = matr[ii][jj].size(); }
        }
        return toret;
    }
}

template <typename EdgeType> std::enable_if_t<std::is_class<EdgeType>::value> COUNT_SUM_OF_EDGES_SIZES(size_t& n,const std::vector<EdgeType>& edges){
    n=0;
    for(const auto& s:edges)    n+=s.size();
}

template <typename EdgeType> std::enable_if_t<!std::is_class<EdgeType>::value> COUNT_SUM_OF_EDGES_SIZES(size_t& n,const std::vector<EdgeType>& edges){
    // do nothing for non-class types
    n=0;
}

template <typename EdgeType> Connection<EdgeType>::Connection(const std::vector<EdgeType>& edge_list) : edges(edge_list) {
    // initialize as trivial graph
    n_edges = edges.size();
    COUNT_SUM_OF_EDGES_SIZES(n_beads,edges);
    canonicalize();
}

template <typename EdgeType> Connection<EdgeType>::Connection(const EdgeType& edge) {
    std::vector<EdgeType> edge_list({edge});
    edges = edge_list;
    n_edges = edges.size();
    COUNT_SUM_OF_EDGES_SIZES(n_beads,edges);
    canonicalize();
}

template <typename EdgeType> void Connection<EdgeType>::canonicalize() { std::sort(edges.begin(), edges.end()); }

template <typename EdgeType> void Connection<EdgeType>::add_edge(const EdgeType& edge) {
    edges.push_back(edge);
    n_edges++;
    canonicalize();
}

template <typename EdgeType> void Connection<EdgeType>::append(const Connection<EdgeType>& c2) {
    for (size_t i = 0; i < c2.size(); i++) { this->add_edge(c2[i]); }
    n_edges += c2.size();
    canonicalize();
}

template <typename EdgeType> void Connection<EdgeType>::erase(size_t position) {
    edges.erase(edges.begin() + position);
    n_edges--;
}

template <typename Edge> std::enable_if_t<std::is_class<Edge>::value> REVERSE_EDGES(std::vector<std::vector<Edge>>& edges){
    for (auto& e : edges) { std::reverse(e.begin(),e.end()); }
}

template <typename EdgeType> std::enable_if_t<std::is_class<EdgeType>::value> REVERSE_EDGES(std::vector<EdgeType>& edges){
    for (auto& e : edges) { e.reverse(); }
}

template <typename EdgeType> std::enable_if_t<!std::is_class<EdgeType>::value> REVERSE_EDGES(std::vector<EdgeType>& edges){
    // do nothing for non-class types
}

template <typename EdgeType> void Connection<EdgeType>::reverse() {
    REVERSE_EDGES(edges);
    canonicalize();
}

template <typename EdgeType> Connection<EdgeType> Connection<EdgeType>::filter() const {
    std::vector<EdgeType> filtered_edges;
    for (auto& e : edges) { filtered_edges.push_back(e.filter()); }
    Connection<EdgeType> filtered(filtered_edges);
    return filtered;
}

template <typename EdgeType> Connection<EdgeType> Connection<EdgeType>::get_untraced_copy() const {
    std::vector<EdgeType> new_edges;
    new_edges.reserve(edges.size());
    for(auto& e : edges) {
        new_edges.push_back(e.get_untraced_copy());
    }
    return new_edges;
}


namespace detail {
  template <typename... Ts> void fill_beads_and_links(std::vector<Bead>& bs, std::vector<Link>& ls, Link l){
    ls.push_back(std::move(l));
  }
  template <typename... Ts> void fill_beads_and_links(std::vector<Bead>& bs, std::vector<Link>& ls, LoopMomentum l){
    ls.emplace_back(l);
  }
  template <typename... Ts> void fill_beads_and_links(std::vector<Bead>& bs, std::vector<Link>& ls, Link l, Bead b, Ts... args){
    ls.push_back(std::move(l));
    bs.push_back(std::move(b));
    fill_beads_and_links(bs,ls,args...);
  }
  template <typename... Ts> void fill_beads_and_links(std::vector<Bead>& bs, std::vector<Link>& ls, LoopMomentum l, Bead b, Ts... args){
    ls.emplace_back(l);
    bs.push_back(std::move(b));
    fill_beads_and_links(bs,ls,args...);
  }
  template <typename... Ts> void fill_beads_and_links(std::vector<Bead>& bs, std::vector<Link>& ls, Link l, Leg b, Ts... args){
    ls.push_back(std::move(l));
    bs.emplace_back(b);
    fill_beads_and_links(bs,ls,args...);
  }
  template <typename... Ts> void fill_beads_and_links(std::vector<Bead>& bs, std::vector<Link>& ls, LoopMomentum l, Leg b, Ts... args){
    ls.emplace_back(l);
    bs.emplace_back(b);
    fill_beads_and_links(bs,ls,args...);
  }
}

template <typename... Ts> Strand::Strand(Link l,Ts... args){
  detail::fill_beads_and_links(beads,links,l,args...);
  n_beads = beads.size();
  n_links = links.size();
  if (n_links != n_beads + 1) {
      std::cerr << "ERROR: number of links in Strand has to be number of beads +1 " << std::endl;
      std::cerr << "recieved"
                << " n_links = " << n_links << ", n_beads = " << n_beads << std::endl;
      std::exit(1);
  }
  construct_graphic();
}


template <typename EdgeType>
std::enable_if_t<!std::is_same<EdgeType, Strand>::value> EXTRA_INFO_STRANDS(std::ostream& out, const std::vector<EdgeType>& edges) {}

template <typename EdgeType>
std::enable_if_t<std::is_same<EdgeType, Strand>::value> EXTRA_INFO_STRANDS(std::ostream& out, const std::vector<EdgeType>& strands) {
    bool prints(false);
    for (auto& strand : strands) {
        auto momroutings = strand.get_routings();
        if (momroutings.size() != 0) {
            prints = true;
            break;
        }
    }
    if (!prints)
        return;
    else {
        out << std::endl;
        size_t index(0);
        for (auto& strand : strands) {
            auto momroutings = strand.get_routings();
            auto masses = strand.get_link_mass_indices();
            out << "MomRouting Strand " << index << ": { [" << momroutings[0] << "," << masses[0] << "]";
            for (size_t ii = 1; ii < momroutings.size(); ii++) out << " , [" << momroutings[ii] << "," << masses[ii] << "]";
            out << " }" << std::endl;

            if(strand.has_link_with_mom_label()) {
                auto link = strand.get_links()[strand.mom_label_link()];
                auto loopmom = link.get_loop_momentum();
                if(!loopmom.get_shift().is_empty())
                    out << "Loop-mom with shift = "<<loopmom<<std::endl;
            }
            index++;
        }
    }
}

template <typename EdgeType> std::enable_if_t<!std::is_same<EdgeType, Strand>::value,std::vector<std::string>> GET_GRAPHIC_FROM_EDGE(const EdgeType& e){
    std::vector<std::string> toret;
    toret.push_back(" |   ");
    for(size_t ii=0;ii<e.size();ii++){
        std::ostringstream stream;
        stream<<e[ii];
        toret.push_back(" |-- "+stream.str());
        if(ii+1<e.size())
            toret.push_back(" |   ");
    }
    toret.push_back(" |   ");
    size_t max(0);
    for(const auto& s:toret)  if(max<s.size())  max=s.size();
    for(auto& s:toret)  for(size_t ii=s.size();ii<max;ii++) s+=" ";
    return toret;
}

template <typename EdgeType> std::enable_if_t<std::is_same<EdgeType, Strand>::value,std::vector<std::string>> GET_GRAPHIC_FROM_EDGE(const EdgeType& e){
    return e.get_graphic();
}

template <typename EdgeType> void Connection<EdgeType>::show(size_t from, size_t to, std::ostream& out) const {
    // to separate different strands
    static const std::string separate("  ");

    size_t fromsize(3);
    if (from > 9) fromsize++;
    size_t tosize(3);
    if (to > 9) tosize++;

    size_t max_height(0);
    std::vector<size_t> height_graphic_strands;
    std::vector<size_t> width_graphic_strands;
    std::vector<size_t> width_initial_blank_strands;
    for (size_t kk = 0; kk < size(); kk++) {
        std::vector<std::string> graphic = GET_GRAPHIC_FROM_EDGE(edges[kk]);
        height_graphic_strands.push_back(graphic.size());
        if (graphic.size() > max_height) max_height = graphic.size();

        if (graphic[0][4] == '|' || graphic[0][4] == '/')
            width_initial_blank_strands.push_back(3);
        else
            width_initial_blank_strands.push_back(0);

        if (graphic.size() == 0)
            width_graphic_strands.push_back(0);
        else
            width_graphic_strands.push_back(graphic[0].length());
    }
    // first line
    for (size_t kk = 0; kk < size(); kk++) {
        // include from info
        for (size_t ii = 0; ii < width_initial_blank_strands[kk]; ii++) out << " ";
        out << "(" << from << ")";
        for (size_t ii = fromsize + width_initial_blank_strands[kk]; ii < width_graphic_strands[kk]; ii++) out << " ";
        out << separate;
    }
    out << "\n";
    // content
    for (size_t ll = 0; ll < max_height + 1; ll++) {
        for (size_t kk = 0; kk < size(); kk++) {
            std::vector<std::string> graphic = GET_GRAPHIC_FROM_EDGE(edges[kk]);
            // we already printed all kkth edge
            if (ll > height_graphic_strands[kk]) {
                for (size_t ii = 0; ii < width_graphic_strands[kk]; ii++) out << " ";
            }
            // the bottom line
            else if (ll == height_graphic_strands[kk]) {
                for (size_t ii = 0; ii < width_initial_blank_strands[kk]; ii++) out << " ";
                out << "(" << to << ")";
                for (size_t ii = tosize + width_initial_blank_strands[kk]; ii < width_graphic_strands[kk]; ii++) out << " ";
            }
            // normal content
            else
                out << graphic[ll];
            out << separate;
        }
        out << "\n";
    }
    EXTRA_INFO_STRANDS(out, edges);
}

template <typename EdgeType> size_t Connection<EdgeType>::get_n_legs() const {
    size_t tr(0);
    for (const auto& e : edges){ tr += e.size(); }
    return tr;
}

template <typename EdgeType> typename std::vector<EdgeType>::iterator Connection<EdgeType>::begin() { return edges.begin(); }

template <typename EdgeType> typename std::vector<EdgeType>::iterator Connection<EdgeType>::end() { return edges.end(); }

template <typename EdgeType> typename std::vector<EdgeType>::const_iterator Connection<EdgeType>::begin() const { return edges.begin(); }

template <typename EdgeType> typename std::vector<EdgeType>::const_iterator Connection<EdgeType>::end() const { return edges.end(); }

template <typename NodeType1, typename LinkType1, typename NodeType2, typename LinkType2>
bool isEqualGraph(Graph<NodeType1, LinkType1>& gr1, Graph<NodeType2, LinkType2>& gr2) {
    if (!std::is_same<NodeType1, NodeType2>::value || !std::is_same<LinkType1, LinkType2>::value) {
        std::cout << "trying to compare graphs of inequal types -- returned false" << '\n';
        return false;
    }
    // first check that topological information is equal
    if (gr1.get_n_edges() != gr2.get_n_edges()) { return false; }
    if (gr1.get_n_nodes() != gr2.get_n_nodes()) { return false; }
    if (gr1.get_n_loops() != gr2.get_n_loops()) { return false; }
    if (gr1.get_adjacency() != gr2.get_adjacency()) { return false; }

    // check elements
    if (gr1.get_nodes() != gr2.get_nodes()) { return false; }

    if (gr1.get_connection_map() != gr2.get_connection_map()) { return false; }

    return true;
}

template <typename NodeType, typename LinkType> Graph<NodeType, LinkType>::Graph() : n_connections(0), n_edges(0), n_loops(0) {}

template <typename NodeType, typename LinkType> Graph<NodeType, LinkType>::Graph(const std::vector<std::vector<size_t>>& matr) : adjacency(matr) {
    n_connections = get_n_connections_from_adjacency();
    n_edges = get_n_edges_from_connections();

    // use Euler identity to calculate loop number. This may not work as expected here because we store some loops inside connections
    n_loops = int(n_edges) - int(get_n_nodes()) + 1;
    // std::cout << "constructing Graph from adjacency matrix" << std::endl;
    // FIXME: why is this not canonicalized?
    canonicalize();
}

template <typename NodeType, typename LinkType>
Graph<NodeType, LinkType>::Graph(const std::vector<NodeType>& vert_list, const LinkType& conn) : Graph{vert_list, {{{0, vert_list.size()-1}, conn}}} {
    if (get_n_nodes() > 2) {
        _WARNING_R("Using this constructor with ", get_n_nodes(), " doesn't make sense!");
        std::exit(1);
    }
}

template <typename NodeType, typename LinkType>
Graph<NodeType, LinkType>::Graph(const std::vector<NodeType>& vert_list, const std::map<std::pair<size_t, size_t>, LinkType>& con_map)
    : nodes(vert_list), connection_map(con_map) {
    // sanity check: no (i,j) key with i>j appears
    for(size_t ii=1;ii<get_n_nodes();ii++){
        for(size_t jj=0;jj<ii;jj++){
            std::pair<size_t,size_t> key = {ii,jj};
            if(connection_map.find(key)!=connection_map.end()){
                std::cerr<<"ERROR: Graph connection_map should only contain upper-level connections, but found key: "<<key<<std::endl;
                std::exit(8799223);
            }
        }
    }
    adjacency = std::vector<std::vector<size_t>>(get_n_nodes(), std::vector<size_t>(get_n_nodes(), 0));
    for (size_t ii = 0; ii < get_n_nodes(); ii++) {
        for (size_t jj = ii; jj < get_n_nodes(); jj++) {
            auto pair = std::make_pair(ii, jj);
            adjacency[ii][jj] = connection_map[pair].size();
            if (ii != jj) { // mirror the matrix along the diagonal
                adjacency[jj][ii] = connection_map[pair].size();
            }
        }
    }
    n_connections = get_n_connections_from_adjacency();
    n_edges = get_n_edges_from_connections();

    // use Euler identity to calculate loop number. This may not work as expected here because we store some loops inside connections
    n_loops = int(n_edges) - int(get_n_nodes()) + 1;
    // std::cout << "constructign Graph from vertList and linklist" << std::endl;
   canonicalize();
}

template <typename NodeType, typename LinkType> Graph<NodeType, LinkType>::Graph(const LinkType& link, const NodeType& vert) {
    std::vector<std::vector<size_t>> vac_matrix({{1}});
    std::vector<NodeType> vert_vec({vert});
    auto pair = std::make_pair(0, 0);
    connection_map.insert(std::make_pair(pair, link));
    adjacency = vac_matrix;
    nodes = vert_vec;
    n_loops = 1;
    // std::cout << "constructing Graph from single link" << std::endl;
    // std::cout << link << std::endl;
    // std::cout << n_loops << std::endl;
    // std::cout << n_vertices << std::endl;
    canonicalize();
}

template <typename NodeType, typename LinkType> void Graph<NodeType, LinkType>::update() {
    for (auto& it : connection_map) {
        auto ii = it.first.first;
        auto jj = it.first.second;
        adjacency[ii][jj] = it.second.size();
        if (ii != jj) { adjacency[jj][ii] = it.second.size(); }
    }
    n_connections = get_n_connections_from_adjacency();
    n_edges = get_n_edges_from_connections();
    n_loops = int(n_edges) - int(get_n_nodes()) + 1;
    canonicalize();
}
#if 0
template <typename NodeType, typename LinkType> Graph<NodeType, LinkType> get_pinched(size_t i, size_t j, size_t k, size_t l, Graph<NodeType, LinkType>& gr) {
    auto new_links = gr.get_connections();
    auto new_nodes = gr.get_nodes();
    auto new_adjacency = gr.get_adjacency();

    auto size = gr.get_n_vertices();

    auto link = new_links[i][j];
    auto strand = new_links[i][j][k];
    auto vertex1 = new_nodes[i];
    auto vertex2 = new_nodes[j];

    if (link.size() == 0) {
        std::cout << "ERROR: can not pinch empty connection!" << std::endl;
        return gr;
    }

    if (k > link.size() - 1) {
        std::cout << "ERROR: requested link " << k << " does not exist!" << std::endl;
        return gr;
    }

    if (new_links[i][j][k].is_plain()) { // the Runglike object we want to pinch is plain (i.e. there are no vertices on it). Here, the vacuum topology changes.

        // join adjacent the  vertices i and j
        new_nodes[i] = merge_graph_nodes(vertex1, vertex2);
        new_nodes.erase(new_nodes.begin() + j);

        // erase the propagator and move all connections i->j to i->i
        new_links[i][j].erase(k);
        new_links[i][i] = new_links[i][j];

        // all connections j->k and k->j are now connections i->k and k->i
        for (size_t ii = 0; ii < size; ii++) {
            if (ii != i) { new_links[ii][i].append(new_links[ii][j]); }
            new_links[ii].erase(new_links[ii].begin() + j);
        }
        for (size_t ii = 0; ii < size; ii++) {
            if (ii != i) { new_links[i][ii].append(new_links[j][ii]); }
        }
        new_links.erase(new_links.begin() + j);
    }
    else {            // the Runglike object containes at least one vertex.
        if (l == 0) { // pinch the first propagator
            auto first_vertex = strand.get_vertex(0);
            // join the first vertex with the vertex i
            new_nodes[i] = merge_graph_nodes(vertex1, first_vertex);
            new_links[i][j][k].drop_first();
        }
        else {
            if (l == strand.get_n_props() - 1) { // pinch last propagator
                auto last_vertex = strand.get_vertex(l - 1);
                // join the last vertex with the vertex j
                new_nodes[j] = merge_graph_nodes(vertex2, last_vertex);
                new_links[i][j][k].drop_last();
            }
            else {
                // pinch an 'internal' propagator l
                new_links[i][j][k].pinch(l);
            }
        }
    }
    Graph<Node, Connection<Strand>> new_graph(new_adjacency, new_nodes, new_links);

    // update graph. Vacuum topology may have changed
    new_graph.update();
    return new_graph;
}

template <typename NodeType, typename LinkType> std::vector<Graph<NodeType, LinkType>> get_all_daughters(Graph<NodeType, LinkType>& gr) {
    std::vector<Graph<NodeType, LinkType>> daughter_list;
    auto adj_matrix = gr.get_adjacency();
    auto links = gr.get_connections();
    for (size_t i = 0; i < gr.get_n_vertices(); i++) {
        for (size_t j = i; j < gr.get_n_vertices(); j++) {
            for (size_t k = 0; k < adj_matrix[i][j]; k++) {
                for (size_t l = 0; l < links[i][j][k].get_n_props(); l++) { daughter_list.push_back(get_pinched(i, j, k, l, gr)); }
            }
        }
    }
    return daughter_list;
}

#endif

template <typename NodeType, typename LinkType> void Graph<NodeType, LinkType>::print_adjacency() const {
    for (auto& v : adjacency) {
        for (auto& j : v) { std::cout << j << '\t'; }
        std::cout << std::endl;
    }
}

// TODO this should be moved to the update method
template <typename NodeType, typename LinkType> size_t Graph<NodeType, LinkType>::get_n_connections_from_adjacency() const {
    size_t toret(0);
    for (size_t ii = 0; ii < adjacency.size(); ii++) {
        for (size_t jj = 0; jj <= ii; jj++) { toret += adjacency[ii][jj]; }
    }
    return toret;
}

// TODO this should be moved to the update method
template <typename NodeType, typename LinkType> size_t Graph<NodeType, LinkType>::get_n_edges_from_connections() const {
    size_t toret(0);

    for (auto& it : connection_map) { toret += it.second.size(); }
    return toret;
}

// TODO this should be moved to the update method
template <typename NodeType, typename LinkType> size_t Graph<NodeType, LinkType>::get_n_legs() const {
    size_t toret(0);
    for (auto n : nodes) if(n.size()>0) ++toret;
    for (auto c : connection_map) toret += c.second.get_n_legs();
    return toret;
}

// permute rows and columns of a matrix
template <typename T> std::vector<std::vector<T>> permute_indices(std::vector<std::vector<T>>& matr, const std::vector<size_t>& perm) {
    if (matr.size() != perm.size()) {
        std::cout << "ERROR: matrix size " << matr.size() << " is incompatible with permutation " << perm;
        exit(1);
    }
    std::vector<std::vector<T>> toret(matr.size(), std::vector<T>(matr.size()));
    for (size_t ii = 0; ii < matr.size(); ii++) {
        for (size_t jj = 0; jj < matr.size(); jj++) { toret[perm[ii]][perm[jj]] = matr[ii][jj]; }
    }
    return toret;
}

template <typename T>
std::map<std::pair<size_t, size_t>, T> get_permuted_map(const std::map<std::pair<size_t, size_t>, T>& map, const std::vector<size_t>& perm) {
    std::map<std::pair<size_t, size_t>, T> toret;
    std::map<size_t, size_t> permutation_map;

    for (auto const& x : map) {
        auto ii = x.first.first;
        auto jj = x.first.second;
        auto new_ii = perm[ii];
        auto new_jj = perm[jj];
        if (new_ii <= new_jj) { toret[std::make_pair(new_ii, new_jj)] = x.second; }
        else {
            auto value = x.second;
            value.reverse();
            toret[std::make_pair(new_jj, new_ii)] = value;
        }
    }
    return toret;
}

template <typename T> std::vector<T> get_permutation(const std::vector<T>& vec, const std::vector<size_t>& perm) {
    std::vector<T> toret(vec.size());
    if(perm.size()!= vec.size()){
        _WARNING_R("ERROR: Called get_pertmutation(vec,perm) with permuation of size ", perm.size(), " for vector of size ", vec.size());
        std::cout << vec <<std::endl;
        exit(1);
    }
    for (size_t i = 0; i < vec.size(); i++) { toret[perm[i]] = vec[i]; }
    return toret;
}

template <typename Edge> std::enable_if_t<std::is_class<Edge>::value> REVERSE_EDGE(std::vector<Edge>& e){
    std::reverse(e.begin(),e.end());
}

template <typename EdgeType> std::enable_if_t<std::is_class<EdgeType>::value> REVERSE_EDGE(EdgeType& edge){
    edge.reverse();
}

template <typename EdgeType> std::enable_if_t<!std::is_class<EdgeType>::value> REVERSE_EDGE(EdgeType& edge){
    // do nothing for non-class types
}

template <typename NodeType, typename LinkType> void Graph<NodeType, LinkType>::canonicalize() {

    std::vector<NodeType> minNodes(nodes);
    std::map<std::pair<size_t, size_t>, LinkType> minMap(connection_map);

    std::vector<size_t> indices(nodes.size());
    std::iota(std::begin(indices), std::end(indices), 0);
    std::vector<size_t> minperm(indices);
    DEBUG_MESSAGE("starting to compare");
    do {
        auto perm_map = get_permuted_map<LinkType>(connection_map, indices);
        auto perm_nodes = get_permutation<NodeType>(nodes, indices);

        DEBUG_MESSAGE("comparing");
        DEBUG_MESSAGE("permutation ", indices);
        // find the labeling such that the connection map is lexicographically minimized
        if (std::lexicographical_compare(perm_map.begin(), perm_map.end(), minMap.begin(), minMap.end())) {
            DEBUG_MESSAGE("permutation is smaller!");
            minMap = perm_map;
            minNodes = perm_nodes;
            minperm = indices;
        }
        // smaller minMap, continues
        else if (std::lexicographical_compare(minMap.begin(), minMap.end(), perm_map.begin(), perm_map.end())) {
            continue;
        }
        // if connections are identical consider the nodes
        else {
            if (std::lexicographical_compare(perm_nodes.begin(), perm_nodes.end(), minNodes.begin(), minNodes.end())) {
                DEBUG_MESSAGE("permutation is smaller!");
                minMap = perm_map;
                minNodes = perm_nodes;
                minperm = indices;
            }
        }

    } while (std::next_permutation(indices.begin(), indices.end()));
    DEBUG_MESSAGE("picking permutation ", minperm);

    connection_map = minMap;
    nodes = minNodes;

    // flip self-loops if they are considered smaller
    for (size_t ii = 0; ii < get_n_nodes(); ii++) {
        auto key = std::make_pair(ii, ii);
        auto to_comp = connection_map[key];
        // as these are edges from a node to itself, we canonicalize each edge
        for(size_t ee = 0; ee < to_comp.get_edges().size(); ++ee) {
            auto testedge = to_comp.get_edges()[ee];
            REVERSE_EDGE(testedge);
            if(testedge < to_comp.get_edges()[ee]) to_comp[ee] = testedge;
        }
        to_comp.canonicalize();
        if (to_comp < connection_map[key]) { connection_map[key] = to_comp; }
    }

    adjacency = permute_indices<size_t>(adjacency, minperm);
}
template <typename NodeType, typename LinkType> const LinkType& Graph<NodeType, LinkType>::get_connection(size_t i, size_t j) const {
    std::pair<size_t, size_t> key;
    if (i <= j) {
        key.first = i;
        key.second = j;
    }
    else {
        key.first = j;
        key.second = i;
    }
    // TODO: should we return reversed connection?
    return connection_map.at(key);
}

template <typename T> std::vector<T> vectorize_matrix(const std::vector<std::vector<T>>& matr) {
    std::vector<T> lin;
    for (auto& row : matr) { lin.insert(lin.end(), row.begin(), row.end()); }
    return lin;
}

template <typename NodeType, typename LinkType> inline bool operator<(const Graph<NodeType, LinkType>& graph1, const Graph<NodeType, LinkType>& graph2) {

    if (std::lexicographical_compare(graph1.get_connection_map().begin(), graph1.get_connection_map().end(), graph2.get_connection_map().begin(),
                                     graph2.get_connection_map().end())) {
        return true;
    }
    if (std::lexicographical_compare(graph2.get_connection_map().begin(), graph2.get_connection_map().end(), graph1.get_connection_map().begin(),
                                     graph1.get_connection_map().end())) {
        return false;
    }
    // lexicographical comparison of the nodes
    return std::lexicographical_compare(graph1.get_nodes().begin(), graph1.get_nodes().end(), graph2.get_nodes().begin(), graph2.get_nodes().end());
}

template <typename NodeType, typename LinkType> inline bool operator==(const Graph<NodeType, LinkType>& graph1, const Graph<NodeType, LinkType>& graph2) {
    return !(graph1 < graph2) && !(graph2 < graph1);
}

template <typename NodeType, typename LinkType> inline bool operator>(const Graph<NodeType, LinkType>& graph1, const Graph<NodeType, LinkType>& graph2) {
    return graph2 < graph1;
}

template <typename NodeType, typename LinkType> Graph<NodeType, LinkType> Graph<NodeType, LinkType>::filter() const {
    std::vector<NodeType> filtered_nodes;

    // filter the nodes
    for (auto n : nodes) { filtered_nodes.push_back(n.filter()); }

    // filter the map
    std::map<std::pair<size_t, size_t>, LinkType> filtered_map;

    for (auto const& x : connection_map) { filtered_map[x.first] = x.second.filter(); }
    Graph<NodeType, LinkType> filtered(filtered_nodes, filtered_map);
    return filtered;
}

template <typename NodeType, typename LinkType> Graph<NodeType, LinkType> Graph<NodeType, LinkType>::remove_loop_momenta_info() const {
    auto connection_map_copy = connection_map;

    for (auto& it: connection_map_copy) {
        it.second = it.second.get_untraced_copy();
    }

    return {nodes, connection_map_copy};
}

template <typename NodeType, typename LinkType> std::string Graph<NodeType, LinkType>::get_short_name() const {
    std::string toret("");
    if (n_loops == 0) { toret += "Tree " + std::to_string(get_n_legs()) + "-point diagram"; }
    if (n_loops == 1) {
        toret += "One-loop " + std::to_string(get_n_legs()) + "-point diagram: ";
        // std::cout <<gr.get_base_graph().get_connection(0,0)[0]<<std::endl;
        toret += ngon_name((get_connection(0, 0))[0].size() + 1);
    }
    if (n_loops == 2) {
        toret += "Two-loop " + std::to_string(get_n_legs()) + "-point diagram: ";
        if (is_factorizable()) {
            toret += "Factorized ";
            if(nodes[0].size()>0)
                toret += "Generic ";
            size_t n1 = (get_connection(0, 0))[0].size() + 1;
            size_t n2 = (get_connection(0, 0))[1].size() + 1;
            if (n1 < n2) { toret += ngon_name(n2) + ngon_name(n1); }
            else {
                toret += ngon_name(n1) + ngon_name(n2);
            }
        }
        else {
            std::vector<size_t> strand_sizes;
            if(nodes[0].size()>0 && nodes[1].size()>0)
                toret += "Generic ";
            else if(nodes[0].size()>0 || nodes[1].size()>0)
                toret += "Semisimple ";
            // Adapt to 2-loop naming convention (i.e. strand with most legs to the left, strand with the fewest legs in the centre)
            strand_sizes.push_back(get_connection(0, 1)[0].size());
            strand_sizes.push_back(get_connection(0, 1)[1].size());
            strand_sizes.push_back(get_connection(0, 1)[2].size());

            std::sort(strand_sizes.begin(), strand_sizes.end());

            size_t n1 = strand_sizes[2] + strand_sizes[0] + 2;
            size_t n2 = strand_sizes[1] + strand_sizes[0] + 2;

            if (strand_sizes[0]) toret += "Non-planar ";
            toret += ngon_name(n1) + ngon_name(n2);
        }
    }
    if (n_loops > 2) { toret += std::to_string(n_loops) + "-loop " + std::to_string(get_n_legs()) + "-point diagram"; }
    return toret;
}

template <typename NodeType, typename LinkType> std::string Graph<NodeType, LinkType>::get_diag_name() const {
    std::string toret("");
    if (n_loops == 0) { toret += "Tree " + std::to_string(get_n_legs()) + "-point diagram"; }
    if (n_loops == 1) { toret += ngon_name((get_connection(0, 0))[0].size() + 1); }
    if (n_loops == 2) {
        if (is_factorizable()) {
            size_t n1 = (get_connection(0, 0))[0].size() + 1;
            size_t n2 = (get_connection(0, 0))[1].size() + 1;
            if (n1 < n2) { toret += ngon_name(n2) + ngon_name(n1); }
            else {
                toret += ngon_name(n1) + ngon_name(n2);
            }
        }
        else {
            std::vector<size_t> strand_sizes;
            // Adapt to 2-loop naming convention (i.e. strand with most legs to the left, strand with the fewest legs in the centre)
            strand_sizes.push_back(get_connection(0, 1)[0].size());
            strand_sizes.push_back(get_connection(0, 1)[1].size());
            strand_sizes.push_back(get_connection(0, 1)[2].size());
            std::sort(strand_sizes.begin(), strand_sizes.end());

            size_t n1 = strand_sizes[2] + strand_sizes[0] + 2;
            size_t n2 = strand_sizes[1] + strand_sizes[0] + 2;
            toret += ngon_name(n1) + ngon_name(n2);
        }
    }
    // TODO: names for generic diagrams can be computed from one loop subdiagrams
    return toret;
}

template <typename NodeType, typename LinkType> std::string Graph<NodeType, LinkType>::get_topo_string() const {
    std::string toret("");
    if (n_loops != 2) { toret += n_loops + "topology"; }
    else {
        if (is_factorizable()) {
            toret += "Factorized";
            size_t n1 = (get_connection(0, 0))[0].size() + 1;
            size_t n2 = (get_connection(0, 0))[1].size() + 1;
            if (n2 < n1) { toret += ngon_name(n2) + ngon_name(n1); }
            else {
                toret += ngon_name(n1) + ngon_name(n2);
            }
            toret += "_" + std::to_string(n1);
        }
        else {
            std::vector<size_t> strand_sizes;
            // Adapt to 2-loop naming convention (i.e. strand with most legs to the left, strand with the fewest legs in the centre)
            strand_sizes.push_back(get_connection(0, 1)[0].size());
            strand_sizes.push_back(get_connection(0, 1)[1].size());
            strand_sizes.push_back(get_connection(0, 1)[2].size());

            std::sort(strand_sizes.begin(), strand_sizes.end());

            size_t n1 = strand_sizes[2] + strand_sizes[0] + 2;
            size_t n2 = strand_sizes[1] + strand_sizes[0] + 2;

            toret += ngon_name(n2) + ngon_name(n1);

            toret += "_" + std::to_string(strand_sizes[1]) + std::to_string(strand_sizes[0]) + std::to_string(strand_sizes[2]);

            std::string endstring("");
            for (auto& nod : nodes) {
                if (nod.size() == 0) {
                    toret += "n";
                    endstring += "_";
                }
                else {
                    toret += "y";
                    if (nod.size() == 1 && nod.is_on_shell()) { endstring += "z"; }
                    else {
                        endstring += "m";
                    }
                }
            }
            toret += "_";
            // left strand
            for (auto& bead : get_connection(0, 1)[1].get_beads()) {
                if (bead.size() == 1 && bead.is_on_shell()) { toret += "z"; }
                else {
                    toret += "m";
                }
            }
            toret += "_";
            // centre strand
            for (auto& bead : get_connection(0, 1)[0].get_beads()) {
                if (bead.size() == 1 && bead.is_on_shell()) { toret += "z"; }
                else {
                    toret += "m";
                }
            }
            toret += "_";
            // right strand
            for (auto& bead : get_connection(0, 1)[2].get_beads()) {
                if (bead.size() == 1 && bead.is_on_shell()) { toret += "z"; }
                else {
                    toret += "m";
                }
            }
            toret += "_";
            toret += endstring;
        }
    }

    /*if (n_loops == 2) {
        toret += "Two-loop " + std::to_string(get_n_legs()) + "-point diagram: ";
        if (n_nodes == 1) { // TODO: this check may be incorporated int a is_factorized method
            toret += "Factorized ";
            size_t n1 = (get_connection(0, 0))[0].size() + 1;
            size_t n2 = (get_connection(0, 0))[1].size() + 1;
            if (n1 < n2) { toret += ngon_name(n2) + ngon_name(n1); }
            else {
                toret += ngon_name(n1) + ngon_name(n2);
            }
        }
        else {
            std::vector<size_t> strand_sizes;
            // Adapt to 2-loop naming convention (i.e. strand with most legs to the left, strand with the fewest legs in the centre)
            strand_sizes.push_back(get_connection(0, 1)[0].size());
            strand_sizes.push_back(get_connection(0, 1)[1].size());
            strand_sizes.push_back(get_connection(0, 1)[2].size());

            std::sort(strand_sizes.begin(), strand_sizes.end());

            size_t n1 = strand_sizes[2] + strand_sizes[0] + 2;
            size_t n2 = strand_sizes[1] + strand_sizes[0] + 2;

            if(strand_sizes[0] ) toret += "Non-planar ";
            toret += ngon_name(n1) + ngon_name(n2);
        }
    }
    */
    return toret;
}

template <typename NodeType, typename LinkType> void Graph<NodeType, LinkType>::show_adjacency(std::ostream& out) const {
    out << "Adjacency: ";
    if (adjacency.size() == 0)
        out << "(.)\n";
    else if (adjacency.size() == 1)
        out << "(" << adjacency[0][0] << ")\n";
    else {
        for (size_t ii = 0; ii < adjacency.size(); ii++) {
            if (ii == 0)
                out << " / ";
            else if (ii + 1 < adjacency.size())
                out << "            | ";
            else
                out << "            \\ ";
            for (size_t jj = 0; jj < adjacency[ii].size(); jj++) { out << adjacency[ii][jj] << " "; }
            if (ii == 0)
                out << "\\\n";
            else if (ii + 1 < adjacency.size())
                out << "|\n";
            else
                out << "/\n";
        }
    }
}

template <typename NodeType, typename LinkType> void Graph<NodeType, LinkType>::show(std::ostream& out) const {
    static const std::string initend("==========================================\n");
    static const std::string division("------------------------------------------\n");
    out << initend;
    // # of loops (and short name)
    out << get_short_name() << std::endl;
    out << initend;
    // now show adjacency matrix
    show_adjacency(out);
    out << division;
    // Now show nodes
    out << "Nodes: ";
    for (size_t ii = 0; ii < nodes.size(); ii++) {
        if (ii > 0) out << "       ";
        out << "(" << ii << "): " << nodes[ii] << std::endl;
    }
    out << division;
    // Now show connections
    for (auto it : connection_map) {
        if (it.second.size() == 0) { continue; /* out << "No Connection FROM: (" << ii << ") TO: (" << jj << ")\n"; */ }
        else {
            size_t ii = it.first.first;
            size_t jj = it.first.second;
            if (ii != jj)
                out << "Connection FROM: (" << ii << ") " << nodes[ii] << " TO: (" << jj << ") " << nodes[jj] << "\n\n";
            else
                out << "Self connection FROM/TO: (" << ii << ") " << nodes[ii] << "\n\n";
            // loop over connections
            it.second.show(ii, jj, out);
        }
        out << division;
    }

    out << initend;
}

const std::vector<std::set<size_t>>& find_all_bubbles_in_graph(size_t node,const std::vector<std::vector<size_t>>& adj);

// The used a transparent algorithm but with bad scaling properties, but this is not meant to deal with more than 5 loop vacuum diagrams
template <typename NodeType, typename LinkType> 
const std::vector<std::set<size_t>>& Graph<NodeType, LinkType>::get_vacuum_bubbles(size_t node) const {
    return find_all_bubbles_in_graph(node,adjacency);
}

template <typename NodeType, typename LinkType> bool Graph<NodeType, LinkType>::has_flower_node() const {
    if (adjacency.size() == 0) {
        std::cerr << "ERROR: adjacency matrix should have at least one entry!" << std::endl;
        std::exit(528);
    }
    if (adjacency.size() == 1) {
        if (adjacency[0][0] > 0)
            return false;
        else
            return true;
    }
    for (size_t ii = 0; ii < adjacency.size(); ii++) {
        std::vector<size_t> row = adjacency[ii];
        // drop petals
        // size_t petals=row[ii];
        row.erase(row.begin() + ii);
        std::sort(row.begin(), row.end());
        if (adjacency.size() == 2) {
            if (row[row.size() - 1] == 1) return true;
        }
        else {
            if (row[row.size() - 1] == 1 && row[row.size() - 2] == 0) return true;
        }
    }
    return false;
}

// has a redundant node: one with only two connections (which should not happen for two or more loops)
template <typename NodeType, typename LinkType> bool Graph<NodeType, LinkType>::has_redundant_node() const {
    if (adjacency.size() == 0) {
        std::cerr << "ERROR: adjacency matrix should have at least one entry!" << std::endl;
        std::exit(528);
    }
    if (adjacency.size() == 1) return false;
    for (size_t ii = 0; ii < adjacency.size(); ii++) {
        size_t sum(0);
        for (auto e : adjacency[ii]) sum += e;
        if (sum == 2) return true;
    }
    return false;
}

// has a redundant node: one with only two connections (which should not happen for two or more loops)
template <typename NodeType, typename LinkType> bool Graph<NodeType, LinkType>::is_connected() const {
    if (adjacency.size() == 0) {
        std::cerr << "ERROR: adjacency matrix should have at least one entry!" << std::endl;
        std::exit(528);
    }
    std::vector<std::vector<bool>> connection_matrix(adjacency.size(), std::vector<bool>(adjacency.size()));
    for (size_t ii = 0; ii < adjacency.size(); ii++) {
        for (size_t jj = ii; jj < adjacency.size(); jj++) {
            if (adjacency[ii][jj]) {
                connection_matrix[ii][jj] = true;
                connection_matrix[jj][ii] = true;
            }
        }
    }

    return is_connected_matrix(connection_matrix);
}

template <typename NodeType, typename LinkType> bool Graph<NodeType, LinkType>::is_factorizable() const {
    if (n_loops < 2) { return false; }
    else if (n_loops == 2) {
        return (get_n_nodes() == 1);
    }
    else {
        if (get_n_nodes() == 1) return true;
        std::vector<std::vector<bool>> connection_matrix(adjacency.size(), std::vector<bool>(adjacency.size()));
        for (size_t ii = 0; ii < adjacency.size(); ii++) {
            for (size_t jj = ii; jj < adjacency.size(); jj++) {
                if (adjacency[ii][jj] > 0) {
                    if (ii == jj) { return true; /*self loop */ }
                    connection_matrix[ii][jj] = true;
                    connection_matrix[jj][ii] = true;
                }
            }
        }
        std::vector<bool> visited_nodes(adjacency.size());
        // check if the graph still connected when we remove a node
        for (size_t ii = 0; ii < get_n_nodes(); ii++) {
            std::fill(visited_nodes.begin(), visited_nodes.end(), false);
            visited_nodes[ii] = true;
            // if we remove 0 we start at 1
            if (ii == 0) { DFS(connection_matrix, visited_nodes, 1); }
            else {
                DFS(connection_matrix, visited_nodes, 0);
            }

            for (auto n : visited_nodes) {
                if (!n) { return true; }
            }
        }
        return false;
    }
}

template <typename EdgeType> inline std::ostream& operator<<(std::ostream& s, const Connection<EdgeType>& is) {
    s << "{";
    for (size_t i = 0; i < is.size(); i++) {
        s << is[i];
        if (i != is.size() - 1) { s << ", "; }
    }
    if (is.size() == 0) s << "EmptyConnection[]";
    s << "}";
    return s;
}

// includes all (machine-readable) information for reconstructing the Graph
template <typename NodeType, typename LinkType> inline std::ostream& operator<<(std::ostream& outstr, const Graph<NodeType, LinkType>& graph) {
    // auto nodes = graph.get_nodes();
    // outstr << "{" << graph.get_n_loops() << ", " << graph.get_nodes() << ", " << graph.get_connection_map() << "}";
    graph.show(outstr);
    return outstr;
}

// template<typename T,size_t D>std::function<std::vector<std::vector<momD<T, D>>>(const momD_conf<T, D>)>> xGraph::get_momentum_collector() const{
//     std::vector<std::vector<size_t>> indices;
//     if(n_loops!=2){
//         std::cout << "xGraph::get_momentum_collector only implemented for 2 loops"<<std::endl;
//     }
//     if(n_nodes ==1){
//         strand_l = base_graph.get_connection(0,0)[0];
//         strand_r = base_graph.get_connection(0,0)[1];

//         for(auto& b:strand_l.get_beads()){
//             indices.push_back(b.get_momentum_indices());
//         }
//         for(auto& b:strand_r.get_beads()){
//             indices.push_back(b.get_momentum_indices());
//         }

//     }

// }

} // namespace lGraph
} // namespace Caravel

namespace std {
template <typename EdgeType> size_t hash<Caravel::lGraph::Connection<EdgeType>>::operator()(const Caravel::lGraph::Connection<EdgeType>& con) const {
    hash<std::vector<EdgeType>> hash_fkt;
    size_t result = hash_fkt(con.get_edges());
    return result;
}
template <typename NodeType, typename LinkType>
size_t hash<Caravel::lGraph::Graph<NodeType, LinkType>>::operator()(const Caravel::lGraph::Graph<NodeType, LinkType>& graph) const {
    hash<std::vector<NodeType>> hash_fkt;
    size_t result = hash_fkt(graph.get_nodes());
    auto con_map = graph.get_connection_map();
    for (auto& it : graph.get_connection_map()) { Caravel::hash_combine(result, it.first); }
    return result;
}
} // namespace std
