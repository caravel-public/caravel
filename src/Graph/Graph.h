/**
 * @file Graph.h
 *
 * @date 8.2.2019
 *
 * @brief Header file for handling of graphs
 *
 * The class Graph represents an abstract graph structure.
 *
 */
#pragma once

#include <algorithm>
#include <numeric>
#include <ostream>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <type_traits>
#include <unordered_map>
#include <tuple>

#include "Graph/Utilities.h"
#include "Core/Debug.h"
#include "Core/Utilities.h"
#include "Core/MathInterface.h"
//#include "Core/momD_conf.h"

namespace Caravel {
namespace lGraph {

class Node;
class Strand;
class xGraph;

/**
 * Class to represents a generic link between two nodes in a graph.
 *
 * We require existence of EdgeType::get_graphic()
 */
template <typename EdgeType> class Connection {
    template <typename EType> friend std::ostream& operator<<(std::ostream&, const Connection<EType>&);
    friend xGraph;
    size_t n_edges{0};           /**< Number of edges in *this */
    size_t n_beads{0};           /**< Sum of the size of all entries in 'edges' */
    // FIXME: replace by a set such that by construction every instance is canonicalized (and then Connection::canonicalize() could be removed)
    std::vector<EdgeType> edges; /**< Container of the edges */

    typename std::vector<EdgeType>::iterator begin();
    typename std::vector<EdgeType>::iterator end();

  public:
    Connection() = default;
    /**
     * Constructor without giving from/to-information
     * @param std::vector<EdgeType>& representing a list of edges
     */
    Connection(const std::vector<EdgeType>&);
    /**
     * Constructor from a single edge (one-loop like)
     * @param EdgeType& representing the edge
     */
    Connection(const EdgeType&);
    /**
     * Get canonical ordering of edges according to order relation < on Edgetype
     */
    void canonicalize();
    /**
     * Returns the number of Edges
     */
    size_t size() const { return edges.size(); }
    /**
     * Method to append the edge
     * @param EdgeType& representing the edge to append
     */
    void add_edge(const EdgeType&);
    /**
     * Method to append annother connection
     * @param Connection<EdgeType>& representing the connection to append
     */
    void append(const Connection<EdgeType>&);
    /**
     * Method to remove an edge
     * @param size_t representing the position of the Edge
     */
    void erase(size_t);
    /**
     * Method to reverse the connection
     */
    void reverse();
    /**
     * Returns a copy with reduced information
     */
    Connection<EdgeType> filter() const;
    /**
     * Returns a copy with no momentum routing.
     */
    Connection<EdgeType> get_untraced_copy() const;
    /**
     * Methods that return the sum of the sizes of all edges contained in *this
     */
    size_t sum_edges_sizes() const { return n_beads; }
    /**
     * Methods that return topological info
     */
    const std::vector<EdgeType>& get_edges() const { return edges; }
    /**
     * Operator to access elements of Connection c by c[i]
     * @param size_t representing the position of the element to access
     */
    const EdgeType& operator[](size_t i) const { return edges[i]; }
    EdgeType& operator[](size_t i) { return edges[i]; }

    /**
     * Method that sums the size of each edge contained (assuming there is a method EdgeType::size())
     */
    size_t get_n_legs() const;

    /**
     * Will print to the passed ostream (std::cout by default) a 2-D representation of this
     *
     * @param size_t representing the 'from' of this connection
     * @param size_t representing the 'to' of this connection
     * @oaram an ostream to deliver the output (defaulted to std::cout)
     */
    void show(size_t, size_t, std::ostream& = std::cout) const;

    // make the container iterable

    typename std::vector<EdgeType>::const_iterator begin() const;
    typename std::vector<EdgeType>::const_iterator end() const;

    /**
     * Connections are compared by:
     * TODO: we also should consider the from/to information for the comparison (for now we make sure to only compare connections of the same type)
     * 1.) length (where fewer edges are considered smaller)
     * 2.) the edges (lexicographic comparison)
     */
    template <typename EdgeType1, typename EdgeType2> friend bool operator==(const Connection<EdgeType1>&, const Connection<EdgeType2>&);
    template <typename EdgeType1, typename EdgeType2> friend bool operator<(const Connection<EdgeType1>&, const Connection<EdgeType2>&);
    template <typename EdgeType1, typename EdgeType2> friend bool operator>(const Connection<EdgeType1>&, const Connection<EdgeType2>&);
    template <typename EdgeType1, typename EdgeType2> friend bool topo_less_connection(const Connection<EdgeType1>&, const Connection<EdgeType2>&);
};

/**
 * Class to represents a generic multi-loop graph
 */
// TODO: rename LinkType to ConnectionType
template <typename NodeType, typename LinkType> class Graph {
    template <typename NType, typename LType> friend std::ostream& operator<<(std::ostream&, const Graph<NType, LType>&);
    friend xGraph;
    size_t n_connections;                                         /**< Number of links between nodes */
    size_t n_edges;                                               /**< Number of edges taking into account multiplicity of edges in each connection */
    int n_loops;                                                  /**< Number of loops in *this */
    std::vector<std::vector<size_t>> adjacency;                   /**< The adjacency matrix */
    std::vector<NodeType> nodes;                                  /**< Information on nodes */
    std::map<std::pair<size_t, size_t>, LinkType> connection_map; /**< Keeps track of the connections. Maps pair (i,j) to connection from vertex i to vertex j (only containing cases with i<=j) */
  public:
    /**
     * Default Graph representing a point graph
     */
    Graph();
    /**
     * Constructor of graph without any decoration
     *
     * @param std::vector<std::vector<size_t>>& representing a vacuum graph
     */
    explicit Graph(const std::vector<std::vector<size_t>>&);
    /**
     * A simplified constructor valid only for a subclass of graphs
     * with one or two nodes and exactly one connection (e.g. all graphs with up to two loops)
     *
     * @param representing the node decorations
     * @param representing the connection for nodes 0->0 or 0->1
     */
    Graph(const std::vector<NodeType>&, const LinkType&);
    /**
     * The same as the above constructor, but accepts MathList of the form
     * CaravelGraph[Nodes[Node[_Leg...]..],Connection[Strand[Link[___],Bead[Leg[__]..],Link[___],...]..]]
     * as the input.
     *
     * Only makes sense for a particular type of Graph specified with enable_ifs.
     */
    template <typename NT = NodeType, typename LT = LinkType>
        Graph(MathList, typename std::enable_if_t<std::is_same<NT, Node>::value>* = nullptr,
                typename std::enable_if_t < std::is_same<LT,Connection<Strand>>::value>* = nullptr);
    /**
     * Constructor form node list and connection map
     */
    Graph(const std::vector<NodeType>&, const std::map<std::pair<size_t, size_t>, LinkType>&);
    /**
     * Superfluous Constructor of graph from a single node and a given link
     *
     * @param the link to be used to construct a single connection
     * @param the corresponding node
     */
    explicit Graph(const LinkType&, const NodeType&);
    /**
     * Method that prints the vacuum graph to std::cout
     */
    void print_adjacency() const;
    /**
     * Method that updates the information contained in the graph
     */
    void update();
    /**
     * Method that returns the vacuum graph
     */
    const std::vector<std::vector<size_t>>& get_adjacency() const { return adjacency; }
    /**
     * Method that returns the corresponding connection
     */
    const LinkType& get_connection(size_t i, size_t j) const;
    
    /**
     * Method that brings the graph to ist canonical form
     */
    void canonicalize();
    /**
     * Returns a copy with reduced information
     */
    Graph<NodeType, LinkType> filter() const;
    /**
     * Returns a copy with all information about loop momenta removed.
     */
    Graph remove_loop_momenta_info() const ;
    /**
     * Methods that return topological info
     */
    size_t get_n_connections_from_adjacency() const;
    size_t get_n_edges_from_connections() const;
    size_t get_n_edges() const { return n_edges; }
    /**
     * Size of the 'nodes' container
     */
    size_t get_n_nodes() const { return nodes.size(); }
    int get_n_loops() const { return n_loops; }
    /**
     * Returns the total number of nodes with node.size()>0 (NodeType::size() supposed to exist) plus
     * the sum for each LinkType (in 'connection_map') of get_n_legs() (assumes existence of LinkType::get_n_legs())
     *
     * TODO: Normally a generic Graph does not have 'legs'. Maybe remove? And push functionality to an xGraph method,
     *       this would remove need for existence of NodeType::size() and LinkType::get_n_legs()
     */
    size_t get_n_legs() const;
    /**
     * Method that returns connection map
     */
    const std::map<std::pair<size_t, size_t>, LinkType>& get_connection_map() const { return connection_map; }
    std::map<std::pair<size_t, size_t>, LinkType>& get_connection_map() { return connection_map; }
    /**
     * Method that returns nodes
     */
    const std::vector<NodeType>& get_nodes() const { return nodes; };
    const NodeType& operator[](size_t i) const { return nodes[i]; }
    NodeType& operator[](size_t i) { return nodes[i]; }
    /**
     * Given a node this method returns all bubbles of its vacuum diagram attached to it
     *
     * @param unsigned integer to specify node of interest 
     * @return a vector of size_t sets. Each element represents the set of indices associated to the nodes that form a bubble
     */
    const std::vector<std::set<size_t>>& get_vacuum_bubbles(size_t) const;
    /*
    * Some checks of graph properties 
    */
    bool has_flower_node() const;
    bool has_redundant_node() const;
    bool is_connected() const; 
    bool is_factorizable() const; 
    
    
    void set_node_visited(size_t i,bool b) {nodes[i].set_visited(b);};
    std::string get_diag_name() const;
    std::string get_short_name() const;
    std::string get_topo_string() const;
    void show_adjacency(std::ostream& = std::cout) const;
    void show(std::ostream& = std::cout) const;
};

template <typename NType, typename LType> bool operator==(const Graph<NType, LType>&, const Graph<NType, LType>&);
template <typename NType, typename LType> bool operator!=(const Graph<NType, LType>& g1, const Graph<NType, LType>& g2){return !(g1==g2);}
template <typename NType, typename LType> bool operator<(const Graph<NType, LType>&, const Graph<NType, LType>&);
template <typename NType, typename LType> bool operator>(const Graph<NType, LType>&, const Graph<NType, LType>&);

/**
 *  Class to represent edges a Strand ("Links between Beads")
 *  We may choose to add further structure (particle type, direction etc.)
 *
 * Links are compared by:
 *
 * 1.) mass_index
 */
class Link {
    size_t mass_index{0};        /**< The mass index associated to *this */
    LoopMomentum mom;            /**< To track if *this holds a loop momentum */
  public:
    Link() = default;
    /**
     * Concstruct a Link with
     * @param mass index
     */
    explicit Link(size_t massi) : mass_index(massi) {}
    /**
     * Concstruct a Link with
     * @param LoopMomentum corresponding to this link
     * @param mass index
     */
    Link(LoopMomentum ll, size_t set_mi = 0);

    bool is_massless_propagator() const { return (mass_index == 0); }
    size_t get_mass_index() const { return mass_index; }
    bool tracks_momentum() const { return mom.active; }
    int get_momentum_label() const { return mom.index; }
    const LoopMomentum& get_loop_momentum() const { return mom; }
    void set_momentum_label(int label, const MomentumRouting& shift = {}) { mom.index=label; mom.active=true; mom.set_shift(shift); }
    void drop_momentum_label() { mom = LoopMomentum(); }
    /**
     * Returns a copy with reduced information
     */
    Link filter() const;
    /**
     * Basically reverse if necessary the 'mom' sign
     */
    void reverse();
    /**
     * If momentum.active, then erases shift
     */
    void drop_shift() {if(mom.active) mom.set_shift({});}
    /**
     * If parent xGraph has 'fields_defined' contains field/particle info
     * through *this
     */
    Field ptype{"", ""};
};

std::ostream& operator<<(std::ostream&, const Link&);
// Main comparison operator
bool operator<(const Link&, const Link&);
// The following operators are derived from operator<
bool operator==(const Link&, const Link&);
bool operator>(const Link&, const Link&);

/**
 * Class to represent the external legs attached to Graph (through either Beads or Nodes).
 * For now relatively basic structure, including info if on-shell, mass index in that case, and momentum index.
 * We may choose to add further structure (particle type etc.)
 *
 * Legs are compared by:
 *
 * 1.) on-shellness (on-shell vertices are considered smaller)
 *
 * 2.) The mass index (meaningless if off shell)
 *
 * 3.) the momentum label
 */
class Leg {
    friend std::ostream& operator<<(std::ostream&, const Leg&);

    bool on_shell{false};         /**< bool to see if there is a single particle attached to vertex */
    size_t mass_index{0};     /**< If on_shell is true (otherwise just zero) keeps a size_t associated with the corresponding mass (as defined by
                              ParticleMass::get_mass_index()) */
    size_t momentum_index; /**< The momentum index of the leg */
  public:
    Leg();
    /**
     * Main constructor with explicit information to construct members
     *
     * @param bool for on_shell
     * @param size_t for mass index
     * @param size_t for momentum index
     */
    Leg(bool, size_t, size_t);
    /**
     * Off-shell momentum with index
     *
     * @param size_t momentum index
     */
    Leg(size_t mom) : Leg{false,0,mom} {}
    /**
     * On-shell momentum with index.
     *
     * @param size_t momentum index
     * @param size_t mass index (0 = zero mass)
     */
    Leg(size_t mom, size_t mass) : Leg{true,mass,mom} {}

    bool is_on_shell() const { return on_shell; }
    size_t get_mass_index() const {
        if (on_shell)
            return mass_index;
        else {
            throw std::runtime_error("Called for Leg::get_mass_index() when is not on-shell!");
        }
    }
    size_t get_momentum_index() const { return momentum_index; }
    /**
     * If parent xGraph has 'fields_defined' contains field/particle + state info
     * in *this
     */
    std::pair<std::string, std::string> ptype{"", ""};
    bool has_field_defined() const { return ptype.first != "" || ptype.second != ""; }
};

std::ostream& operator<<(std::ostream&, const Leg&);
// Main comparison operator
bool operator<(const Leg&, const Leg&);
// The following operators are derived from operator<
bool operator==(const Leg&, const Leg&);
bool operator>(const Leg&, const Leg&);

/**
 * Data structure to hold information on external legs attached to a Graph node. It
 * has an intimate relation to the Bead class, but we treat Bead separately as a Bead
 * always is in between two Links, while a Node can have multiple Connections.
 *
 * Node are compared by:
 *
 * 1.) on-shellness (on-shell vertices are considered smaller)
 *
 * 2.) The mass index (meaningless if off shell)
 *
 * 3.) std::lexicographical_compare of momentum indices (notice that this includes ordering depending on lengths of the containers) (notice also that we make
 * the comparison with the simple 'momentum_indices' container, instead of with 'legs' for performance)
 */
class Node {
    using couplings_t = std::map<std::string,int>;
    bool on_shell{false};                        /**< whether the Node has attached an on-shell leg (false if 'legs' is empty) */
    size_t mass_index{0};                    /**< If on_shell is true (otherwise just zero) keeps a size_t associated with the corresponding mass (as defined by
                                             ParticleMass::get_mass_index()) */
    std::vector<Leg> legs;                /**< Keeps a sorted set of Legs */
    std::vector<size_t> momentum_indices; /**< Keeps a sorted set of momentum indices from the legs */
    int permutation_tracking_label{-1};   /**< This is decoration, used by xGraph, to track permutations of xGraph.base_graph and xGraph.topology. Default value means unassigned */
  public:
    Node();
    explicit Node(const Leg&);
    explicit Node(const std::vector<Leg>&);

    size_t size() const { return legs.size(); }
    bool is_on_shell() const { return on_shell; }
    size_t get_mass_index() const {
        if (on_shell)
            return mass_index;
        else
            throw std::runtime_error("Called for Node::get_mass_index() when is not on-shell!");
    }
    const std::vector<Leg>& get_legs() const { return legs; }
    std::vector<Leg>& get_legs() { return legs; }
    const std::vector<size_t>& get_momentum_indices() const { return momentum_indices; }
    void set_on_shell_state(bool b) { on_shell = b; }
    /**
     * Returns a copy with reduced information
     */
    Node filter() const;
    void assign_permutation_tracking_label(int i) { permutation_tracking_label=i; }
    int get_permutation_tracking_label() const { return permutation_tracking_label; }
    /**
     * If parent xGraph has 'fields_defined' contains coupling info of *this
     */
    couplings_t coups{};
    bool has_fields_defined() const;
};

std::ostream& operator<<(std::ostream&, const Node&);
// Main comparison operator
bool operator<(const Node&, const Node&);
// The following operators are derived from operator<
bool operator==(const Node&, const Node&);
bool operator>(const Node&, const Node&);

/**
 * Class to hold information on external legs attached to a Strand vertex (the "Bead").
 * It has an intimate relation to the Node class, but we treat them separately as a
 * Bead always is in between two Links, while a Node can have multiple Connections.
 *
 * Beads are compared by:
 *
 * 1.) on-shellness (on-shell vertices are considered smaller)
 *
 * 2.) The mass index (meaningless if off shell)
 *
 * 3.) std::lexicographical_compare of momentum indices (notice that this includes ordering depending on lengths of the containers) (notice also that we make
 * the comparison with the simple 'momentum_indices container', instead of with 'legs' for performance)
 */
class Bead {
    using couplings_t = std::map<std::string,int>;
    friend std::ostream& operator<<(std::ostream&, const Bead&);
    bool on_shell{false};                        /**< whether the Bead has attached an on-shell leg (false if 'legs' is empty) */
    size_t mass_index{0};                    /**< If on_shell is true (otherwise just zero) keeps a size_t associated with the corresponding mass, as defined by
                                             ParticleMass::get_mass_index() */
    std::vector<Leg> legs;                /**< Keeps a sorted set of Legs */
    std::vector<size_t> momentum_indices; /**< Keeps a sorted set of momentum indices from the legs */

  public:
    Bead();
    explicit Bead(const Leg&);
    explicit Bead(const std::vector<Leg>&);

    size_t size() const { return legs.size(); }
    const std::vector<Leg>& get_legs() const { return legs; }
    std::vector<Leg>& get_legs() { return legs; }
    const std::vector<size_t>& get_momentum_indices() const { return momentum_indices; }

    void append(const Bead&);

    size_t get_mass_index() const {
        if (on_shell)
            return mass_index;
        else
            throw std::runtime_error("Called for Bead::get_mass_index() when is not on-shell!");
    }
    bool is_on_shell() const { return on_shell; }
    void set_on_shell_state(bool b) { on_shell = b; }

    bool is_massless() const;
    /**
     * Returns a copy with reduced information
     */
    Bead filter() const;
    /**
     * If parent xGraph has 'fields_defined' contains coupling info of *this
     */
    couplings_t coups{};
    bool has_fields_defined() const;
};

// Main comparison operator
bool operator<(const Bead&, const Bead&);
// The following operators are derived from operator<
bool operator==(const Bead&, const Bead&);
bool operator>(const Bead&, const Bead&);

/**
 * Main class to represent an ordered sequence of Links and Beads. They will be used for reprenting rungs in xGraph.
 * The Strand by itself has no knowledge of from where comes or go. This information is kept by its owner
 * (like 'Connection').
 *
 * Strands are compared by:
 *
 * 1.) std::lexicographical_compare of links
 *
 * 2.) std::lexicographical_compare of beads
 */
class Strand {
    friend std::ostream& operator<<(std::ostream&, const Strand&);
    friend class xGraph;
    size_t n_beads;          /**< Number of beads in *this */
    size_t n_links;          /**< Number of propagators in this in *this */
    std::vector<Bead> beads; /**< Keeps information related to each bead. An entry is assumed to the right of corresponding entry in propagator_mass_indices */
    std::vector<Link> links; /**< Keeps track of the mass indices in propagators (as defined by ParticleMass::get_mass_index(). Always zero
                                                    if massless. An entry is assumed to the left of corresponding entry in beads */
    std::vector<std::string> graphic; /**< A 2-D array of characters to graphically represent the strand on the standard output (forced to be rectangular) */
    bool track_momenta{false};
    std::vector<MomentumRouting> momenta; /**< Collection of MomentumRouting specifying flowing momenta through the links (we keep this info attached to Strand
                                             and not to individual Link's */
    int permutation_tracking_label{-1};   /**< This is decoration, used by xGraph, to track permutations of xGraph.base_graph and xGraph.topology. Default value means unassigned */
    bool has_been_reversed{false};    /**< Defined false at construction, and negated everytime the method reverse() is called */
  public:
    Strand();
    /**
     * Main constructor from a list of consecutive vertices and links.
     * Should start and end with Link, and ([number of Beads] +1) should equal [number of Links]
     */
    template <typename... Ts> explicit Strand(Link,Ts...);
    /**
     * Main constructor from a list of vertices and link. We check that there are n Beads and n+1 Links
     *
     * @param std::vector<Bead> representing a vector of vertices on the strand
     * @param std::vector<Link> represents a vector of links
     */
    Strand(const std::vector<Bead>&, const std::vector<Link>&);
    /**
     * Main constructor from a list of vertices, assuming all propagatos as massless
     *
     * @param std::vector<Bead> representing a vector of vertices on the strand
     */
    explicit Strand(const std::vector<Bead>&);
    /**
     * Returns the vector of mass labels
     */
    std::vector<size_t> get_link_mass_indices() const;
    /**
     * Returns the vector of beads
     */
    const std::vector<Bead>& get_beads() const { return beads; }
    std::vector<Bead>& get_beads() { return beads; }
    /**
     * Returns the i'th index
     * @param int representing the ith bead in the strand (0-based)
     */
    const Bead& get_bead(size_t i) const { return beads[i]; }
    const Bead& operator[](size_t i) const { return beads[i]; }
    /**
     * Returns the vector of links
     */
    const std::vector<Link>& get_links() const { return links; }
    std::vector<Link>& get_links() { return links; }
    /**
     * Returns the i'th link
     * @param int representing the ith link in the strand (0-based)
     */
    const Link& get_link(size_t i) const { return links[i]; }
    /**
     * Returns if link is massless (index 1-based)
     *
     * @param int representing the ith link in the strand (1-based)
     * @return bool and a 'false' together with an error message if out of bounds
     */
    bool is_massless_link(int) const;
    /**
     * Returns if a given bead of the strand is massless, that is built up of a single massless particle (index 1-based)
     *
     * @param int representing the ith bead in the strand (1-based)
     * @return bool and a 'false' together with an error message if out of bounds
     */
    bool is_massless_bead(int) const;
    /**
     * Method to check if a given bead is on-shell and has a mass that matches the mass in the given link
     *
     * @param int representing the bead to be checked (1-based)
     * @param int representing the link to be checked (1-based)
     */
    bool check_on_shell_leg(int, int) const;
    /**
     * Method to get number of attached momenta to a given bead
     *
     * @param int representing the bead considered (1-based)
     */
    size_t bead_size(int) const;
    /**
     * This method checks that the corresponding bead is on-shell (otherwise returns 0 with error message) and the returns the corresponding mass index
     *
     * @param int representing the bead considered (1-based)
     */
    size_t get_bead_mass_index(int) const;
    /**
     * Returns the mass index of the link requested (1-based, and bounds checked)
     *
     * @param int representing the link (1-based)
     */
    size_t get_link_mass_index(int) const;
    /**
     * Method to return the size of *this (that is, the number of beads)
     */
    size_t size() const { return n_beads; }
    /**
     * Method to return how many links are contained in *this
     */
    size_t get_n_links() const { return n_links; }
    /**
     * This method the corresponding vector of mass indices corresponding to the requested bead (1-based, bounds checked)
     *
     * @param int representing the bead considered (1-based)
     */
    const std::vector<size_t>& bead_momentum_indices(int) const;

    /**
     * Method to pinch a link in the strand. 
     *
     * @param size_t represents the position of the link (0-based)
     * @return tuple consisting of { vector of left-out Legs, dropped Link, boolean (true if returned Strand is valid), Strand }
     */
    std::tuple<std::vector<Leg>, Link, bool, Strand> pinch(size_t) const;
    /**
     * Method to drop the first link and bead
     */
    void drop_first();
    /**
     * Method to drop last link and bead
     */
    void drop_last();
    /**
     * Method to check if the strand consists of a single link
     */
    bool is_plain() { return (n_beads == 0); }
    /**
     * Reverse the string of links and beads
     */
    void reverse();
    /**
     * Returns a copy with reduced information
     */
    Strand filter() const;
    void show() const;
    const std::vector<std::string>& get_graphic() const { return graphic; }
    size_t get_n_legs() const;
    /**
     * We traverse the links and find if there is a unique link with momentum label
     */
    bool has_link_with_mom_label() const;
    /**
     * Will return the (first) link found that has a momentum label (no has_link_with_mom_label() will be performed!)
     */
    size_t mom_label_link() const;
    /**
     * If !track_momenta, this method checks existence of Link with mom label and proceeds to fill 'momenta'
     *
     * @param number of external legs to apply momentum conservation
     */
    void trace_momentum_routing(size_t);
    /**
     * This method determines if a given loop momentum flows through the strand
     * 
     * @param loop momentum index
     */
    bool has_loop_momentum(size_t) const;
    /**
     * If !track_momenta && !has_link_with_mom_label() (otherwise dies), it assigns the passed momentum label to the specified link
     *
     * @param unsigned integer specifying the link to assign
     * @param integer with momentum label
     * @param momentum shift (meaning that the assigned momentum is l_i + shift)
     */
    void set_momentum_label(size_t, int, const MomentumRouting& shift = {});

    /**
     * If !track_momenta, this method checks non existence of Link with mom label, and proceed to fill 'momenta' starting with the passed MomentumRouting
     *
     * @param number of external legs to apply momentum conservation
     * @param momentum routing of initial link
     */
    void trace_momentum_routing(size_t,const MomentumRouting&);
    const std::vector<MomentumRouting>& get_routings() const { return momenta; }
    void drop_routings()  { momenta.clear(); track_momenta=false; }

    /**
     * Returns a copy with no momentum routing.
     */
    Strand get_untraced_copy() const;
    bool momemtum_traced() const { return track_momenta; }
    /**
     * This method will reset 'track_momenta' and 'momenta', and, for bool = true, if for any Link we have a momentum assigned, 
     * it'll be moved n%N links from the top link in positive flowing, where n is the passed unsigned integer (defaulted to
     * zero) and N is the # of links in *this
     */
    void reset_momentum_routing(bool = true, size_t = 0);

    void assign_permutation_tracking_label(int i) { permutation_tracking_label=i; construct_graphic(); }
    int get_permutation_tracking_label() const { return permutation_tracking_label; }
    bool get_has_been_reversed() const { return has_been_reversed; }

    /**
     * This method will erase and fill the 'graphic' container
     */
    void construct_graphic();
    bool has_fields_defined() const;
};

// Main comparison operator
bool operator<(const Strand&, const Strand&);
// The following operators are derived from operator<
bool operator==(const Strand&, const Strand&);
bool operator>(const Strand&, const Strand&);

/**
 * A function to get number of edges (counting links in Strands) from an
 * instance of Graph<Node, Connection<Strand>>
 */
size_t get_n_of_links(const Graph<Node, Connection<Strand>>& g);

/**
 * Class representing a graph decorated by external legs and propagators
 */
class xGraph {
    friend std::ostream& operator<<(std::ostream&, const xGraph&);

    size_t n_vertices;                          /**< Number of vertices in *this */
    size_t n_nodes;                             /**< Number of nodes in *this */
    size_t n_edges;                             /**< Number of edges in *this */
    size_t n_legs;                              /**< Number of legs in base_graph */
    size_t n_topology_legs;                     /**< Number of legs in topology */
    size_t n_external_momenta;                  /**< Number of independent external momenta flowing into *this */
    int n_loops;                                /**< Number of loops in *this */
    Graph<Node, Connection<Strand>> base_graph; /**< The abstract graph associated to *this */
    Graph<Node, Connection<Strand>> topology;   /**< base_graph with all labels dropped */
    std::vector<Leg> legs;			/**< We keep a copy of all legs attached to *this */
    MomentumDecoration loop_momenta;            /**< This is a decoration for xGraph, which can hold information on particular selection of loop momenta */
    void fill_info();                           /**< Updates info for n_vertices, n_legs, and n_loops */
    /**
     * This private method takes existing base_graph and if it is a 1-loop diagram, makes sure to restructure
     * base_graph into "canonical 1-loop form", meaning that we pick the Node+Beads configuration that has
     * minimum lexicographical order
     *
     * @param graph being standardized
     */
    void standard_one_loop_xGraph(Graph<Node, Connection<Strand>>&);
    /**
     * Private method to construct topology out of a filtered base_graph.
     *
     * First it will assign permutation labels to base_graph.
     *
     * Then, it will take care of properly passing information on labels to tag permutations of Nodes and 
     * Strands in the topology canonicalization procedure (basically through the Graph::filter() method)
     */
    void generate_topology_from_base_graph();
    /**
     * Auxiliary method to xGraph::get_relabeled_copy(.). Given a MomChoice in the 'other' xGraph (assumed
     * to be topologically related as checked by get_relabeled_copy(.)) it returns the corresponding 
     * MomChoice in *this
     */
    MomChoice get_corresponding_MomChoice(const MomChoice& l,const xGraph& other) const;

    /**
     * Stores on-shell index map for topology canonicalization: map: {given index} ---> {canonical index}
     */
    std::map<size_t, size_t> canonical_onshell_indices;

    /**
     * Allows for 'base_graph' to track Field information
     */
    bool var_fields_defined{false};
  public:
    typedef decltype(base_graph) base_graph_type;
    /**
     * Default Graph representing a point graph
     */
    xGraph();
    /**
     * Constructor of graph without any decoration
     * @param Graph representing the base_graph
     */
    xGraph(const Graph<Node, Connection<Strand>>&);
    /**
     * A constructor from MathList.
     * Simply forwards to the constructor of the underlying base graph and uses the constructor from Graph<Node, Connection<Strand>>
     */
    template <typename T, typename = std::enable_if_t<std::is_same<MathList, std::decay_t<T>>::value>>
    xGraph(T&& input) : xGraph{decltype(base_graph){std::forward<T>(input)}} {}

    const Graph<Node, Connection<Strand>>& get_base_graph() const { return base_graph; }

    const Graph<Node, Connection<Strand>>& get_topology() const { return topology; }

    size_t get_n_external_momenta() const { return n_external_momenta; }
    bool is_planar() const ;
    /**
    * Returns the dimension of the scattering plain
    */
    size_t get_span_dim() const;
    size_t get_n_vertices() const { return n_vertices; }
    size_t get_n_nodes() const { return n_nodes; }
    size_t get_n_edges() const { return n_edges; }
    size_t get_n_legs() const { return n_legs; }
    size_t get_n_topology_legs() const { return n_topology_legs; }
    int get_n_loops() const { return n_loops; }
    const std::vector<Leg>& get_legs() const { return legs; }

    /*
    * Returns a pair {Node,Strand} which correspond to a one-loopish subdiagram
    * where the loop momentum with a given index flows
    * @parm represents the loop momentum index
    */
    std::pair<Node,Strand> get_one_loop_subdiagram(size_t) const;
    /*
    * Returns the copy of *this with a different momentum labeling
    * @param xGraph represents a topologically equivalent xGraph which defines the relabeling when the two graphs are put on top of each other.
    */
    xGraph get_relabeled_copy(const xGraph&) const;
    /**
     * Returns an underlying base graph with all loop momentum information removed.
     */
    decltype(base_graph) remove_loop_momenta_info() const {return base_graph.remove_loop_momenta_info();}
    /**
     * Will print to the passed ostream (std::cout by default) a 2-D representation of the 'base_graph'
     */
    void show(std::ostream& = std::cout) const;
    /**
     * Will print to the passed ostream (std::cout by default) a 2-D representation of the 'topology'
     */
    void show_topology(std::ostream& = std::cout) const;
    /**
     * This method will check if there are any Links in the Connections' Strands with momentum labels and based on that (after consistency checks) 
     * constructs full momentum routings for *this and the whole base_graph
     */
    void trace_momentum_routing();
    bool momemtum_traced() const { return loop_momenta.active; }
    const std::vector<MomChoice>& get_internal_momenta() const { return loop_momenta.internal_momentum_choice; }
    /**
     * In the input list we get information of a request for momenta flowing in corresponding links. For each one we return (if momemtum_traced())
     * the corresponding MomentumRouting (with corresponding sign)
     */
    std::vector<MomentumRouting> map_momenta_in_links(const std::vector<MomChoice>&) const;
    /**
     * In the input list we get information of a request for momenta flowing in corresponding links. For each one we return (if momemtum_traced())
     * the corresponding MomentumRouting (with corresponding sign)
     */
    //template<typename T,size_t D>std::function<std::vector<std::vector<momD<T, D>> >(const momD_conf<T, D>&)> get_momentum_collector() const;
    /**
     * Returns the dimension of the transverse space to the whole xGraph, that is, as if it was a 1-loop diagram
     */
    size_t dimension_common_transverse() const;
    /**
     * Returns the number of the OS variables of the xGraph when using the standard loop-momentum routing according to get_special_routing()
     *
     * Non-planar 2-loop graphs: variables are removed from second loop momentum.
     *
     * Factorized 2-loop graphs: one variable added to second loop momentum (related to mu12)
     */
    std::vector<size_t> n_OS_variables_with_special_routing() const;
    /**
     * Checks momemtum_traced() then computes for each momentum the dimension of its transverse space
     */
    std::vector<size_t> transverse_space_dimensions() const;
    /**
     * Checks if any of the links of the xGraph is massive
     */
    bool has_massive_link() const;
    /**
     * This method will return the node from which the given loop momentum goes out, making a series of consistency checks first
     *
     * @param size_t the momentum label (it has to be between 0 and (non-inclusive) n_loops)
     * @return int which will be negative in case a loop momentum is NOT attached to any node, or the node attached
     */
    int attached_node(size_t) const;
    /**
     * This method will reset 'loop_momenta', and even more, if bool = true, for any Strand with momentum assigned, 
     * it'll be moved n%N links from the top link in positive flowing, where n is the passed unsigned integer (defaulted to
     * zero) and N is the # of links in the corresponding strand
     */
    void reset_momentum_routing(bool = true, size_t = 0);

    /**
     * This method helps standardizing choices of loop-momentum assignments (for example for on-shell loop-momentum 
     * parametrization, dual/transverse space calculations in GraphKin, integrand parametrization, etc)
     *
     * 1-loop xGraphs: set with reset_momentum_routing(true, 0)
     *
     * 2-loop xGraphs: if 3 strands present, strand without loop-momentum assignment will be set to one with the 
     * least links (both loop momenta will be in zeroth link and positive flow). Loop-1 and loop-2 will be assigned with
     * decreasing number of beads in corresponding strands. 2-strand instances are set with reset_momentum_routing(true, 0)
     */
    const xGraph& get_special_routing() const;

    /**
     * This method checks that all vectors of momentum indices in base_graph have either empty (only possible for 
     * Nodes in *this) or contain a single integer (like { 4 })
     */
    bool has_only_simple_mom_insertions() const;
    /**
     * Will traverse all nodes in the base_graph and check if a massless (scaleless) bubble appears attached to it.
     *
     * There is an intrinsic assumption that sij (for any {i,j} pair, with i!=j) is non zero
     */
    bool has_scaleless_bubble() const;
    /* Checks if the graph is a one-loop graph with a single massless leg */
    bool is_bubble_with_single_massless_leg() const;
    /* Checks if any strand of the graph has a single massless leg (takes momentum conservation into account) */
    bool has_single_massless_leg() const;
    /* Checks if the strand has a single massless leg (takes momentum conservation into account) */
    bool strand_has_single_massless_leg(const Strand& strand) const;
    /**
     * Utility to produce a standard ordering for the Link's in *this
     *
     * @return vector of links, each specified as a tuple {connection, strand, link}
     */
    std::vector<AccessLink> list_links() const;
    /**
     * Creates an xGraph *this but without the Link passed
     *
     * @param specifies link to be pinched
     * @param boolean to control if shifts are to be dropped before pinching (default true)
     */
    xGraph pinch(const AccessLink&, bool = true) const;
    /**
     * Returns a full list of all propagators in *this
     */
    std::vector<Propagator> get_propagators() const;
    std::string get_propagators_string() const;
    Propagator get_propagator(const AccessLink&) const;
    
    /**
     * Checks if requested link has momentum assigned (otherwise throws) and returns shift
     */
    const MomentumRouting& get_shift(const AccessLink&) const;
    /**
     * For every loop momentum assigned, drops shift
     */
    void drop_shifts();

    /**
     * Returns momentum routing of pointed link
     */
    const MomentumRouting& get_momentum_routing(const AccessLink&) const;

    std::string get_MGraphString() const;

    /**
     * Returns a (sorted) vector of on-shell indices in base_graph (corresponding
     * to mass indices for links and mass indices for on-shell (external) legs)
     */
    std::vector<size_t> onshell_indices() const;

    /**
     * Gives access to 'topology' canonicalization procedure (including mass-index canonicalization)
     *
     * @param Graph<Node, Connection<Strand>> defaulted to base_graph
     * @param vector of onshell indices (as an xgraph instance would return with onshell_indices() call)
     */
    static Graph<Node, Connection<Strand>> canonical_topology(const Graph<Node, Connection<Strand>>&, const std::vector<size_t>& indices, std::map<size_t, size_t>&);

    /**
     * Gives access to all permutations of Nodes and Strands in Connections that give equivalent representations
     * of the 'base_graph'.
     *
     * The identity is excluded.
     *
     * FIXME: For now always returns empty vector except for 1-loop bubbles
     */
    std::vector<xGraphPermute> get_automorphisms() const;

    const auto& get_canonical_onshell_indices() const { return canonical_onshell_indices; }

    xGraph& operator=(const Graph<Node, Connection<Strand>>&);
    xGraph& operator= (const xGraph&) = default;
    xGraph& operator= (xGraph&&) = default;
    xGraph(const xGraph&) = default;
    xGraph(xGraph&&) = default;

    bool fields_defined() const { return var_fields_defined; }
    /**
     * Method to map field content of an 'in' xGraph into *this
     */
    void map_fields(const xGraph& in);
    /**
     * A utility to access field information
     */
    void show_fields() const;
  private:
    xGraph get_2loop_special_routing() const;
    std::vector<size_t> n_OS_vars_helper() const;
};

/**
 * Generate a Mathematica-formatted string.
 * For now only works for 1- and 2-loop graphs, but extension just amounts to choosing a format.
 * TODO: extend to multiloop
 */
std::string math_string(const lGraph::Leg&);
std::string math_string(const lGraph::Link&);
std::string math_string(const lGraph::Bead&);
std::string math_string(const lGraph::Node&);
std::string math_string(const lGraph::Strand&);
std::string math_string(const lGraph::Connection<lGraph::Strand>&);
std::string math_string(const lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>&);
std::string math_string(const lGraph::xGraph&);

/**
 * Takes a graph and replaces all mass indices (external Legs on-shell indices, and internal
 * Link mass index) according to map
 */
Graph<Node, Connection<Strand>> replace_mass_indices(const Graph<Node, Connection<Strand>>& xg, const std::map<size_t, size_t>& map);

} // namespace lGraph
} // namespace Caravel

namespace std {
template <> struct hash<Caravel::lGraph::Leg> { size_t operator()(const Caravel::lGraph::Leg&) const; };
template <> struct hash<Caravel::lGraph::Node> { size_t operator()(const Caravel::lGraph::Node&) const; };
template <> struct hash<Caravel::lGraph::Bead> { size_t operator()(const Caravel::lGraph::Bead&) const; };
template <> struct hash<Caravel::lGraph::Link> { size_t operator()(const Caravel::lGraph::Link&) const; };
template <> struct hash<Caravel::lGraph::Strand> { size_t operator()(const Caravel::lGraph::Strand&) const; };
template <typename EdgeType> struct hash<Caravel::lGraph::Connection<EdgeType>> { size_t operator()(const Caravel::lGraph::Connection<EdgeType>&) const; };
template <typename NodeType, typename LinkType> struct hash<Caravel::lGraph::Graph<NodeType, LinkType>> {
    size_t operator()(const Caravel::lGraph::Graph<NodeType, LinkType>&) const;
};
} // namespace std

#include "Graph.hpp"

