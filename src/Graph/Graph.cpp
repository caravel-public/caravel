#include "Graph/Graph.h"

#include "Core/enumerate.h"
#include "Core/MathOutput.h"

#include <cassert>

namespace Caravel {
namespace lGraph {

Leg::Leg() : on_shell(false), mass_index(0), momentum_index(0) {}

Leg::Leg(bool b, size_t m, size_t i) : on_shell(b), mass_index(m), momentum_index(i) {}

std::ostream& operator<<(std::ostream& o, const Leg& l) {
    if (l.is_on_shell())
        o << "{true," << l.get_mass_index() << "," << l.get_momentum_index() << "}";
    else
        o << "{false,-1," << l.get_momentum_index() << "}";
    return o;
}

bool operator<(const Leg& leg1, const Leg& leg2) {
    // if the labels match size is the same check use lexicographic comparison of the labels
    if (leg1.is_on_shell() && !leg2.is_on_shell()) { return true; }
    if (!leg1.is_on_shell() && leg2.is_on_shell()) { return false; }
    // at this point we know both is_on_shell() are equal
    if (leg1.is_on_shell()) {
        // if is on shell, check mass labels
        const size_t m1(leg1.get_mass_index());
        const size_t m2(leg2.get_mass_index());
        if (m1 < m2) return true;
        if (m2 < m1) return false;
    }
    // finally compare momentum indices
    return leg1.get_momentum_index() < leg2.get_momentum_index();
}

bool operator==(const Leg& leg1, const Leg& leg2) { return !(leg1 < leg2) && !(leg2 < leg1); }

bool operator>(const Leg& leg1, const Leg& leg2) { return leg2 < leg1; }

Node::Node() : on_shell(false), mass_index(0) {}

Node::Node(const Leg& l) : on_shell(l.is_on_shell()), legs({l}), momentum_indices({l.get_momentum_index()}) {
    if (on_shell) { mass_index = l.get_mass_index(); }
}

Node::Node(const std::vector<Leg>& vl) : legs(vl) {
    // sort legs
    std::sort(legs.begin(), legs.end());

    momentum_indices.clear();
    for (auto& l : legs) momentum_indices.push_back(l.get_momentum_index());
    std::sort(momentum_indices.begin(), momentum_indices.end());

    on_shell = false;
    mass_index = 0;
    if (legs.size() == 1) {
        on_shell = vl[0].is_on_shell();
        if (on_shell) { mass_index = vl[0].get_mass_index(); }
    }
}

Node Node::filter() const {
    if (size() == 0) { // empty node -- do nothing
        return *this;
    }
    Node toret;
    size_t mom_ind = 1;
    if(on_shell){mom_ind = 0;}
    Leg filtered_leg(on_shell, mass_index, mom_ind);
    // Modify accordingly members of toret
    toret.on_shell = on_shell;
    toret.legs = {filtered_leg};
    toret.momentum_indices = {mom_ind};
    toret.mass_index = mass_index;
    toret.permutation_tracking_label = permutation_tracking_label;
    return toret;
}

bool Node::has_fields_defined() const {
    if (coups.size() > 0) return true;
    for (auto l : legs)
        if (l.has_field_defined()) return true;
    return false;
}

std::ostream& operator<<(std::ostream& s, const Node& nn) {
    s << "{";
    if (nn.is_on_shell())
        s << "true, " << nn.get_mass_index() << ", ";
    else
        s << "false, -1, ";
    s << "{";
    const std::vector<Leg>& llegs(nn.get_legs());
    for (size_t ii = 0; ii < llegs.size(); ii++) {
        s << llegs[ii].get_momentum_index();
        if (ii + 1 < llegs.size()) s << ", ";
    }
    s << "}";
    s << "}";
    if(nn.get_permutation_tracking_label()>-1)
        s<<"<"<<nn.get_permutation_tracking_label()<<">";
    return s;
}

bool operator<(const Node& c1, const Node& c2) {
    // on-shell Nodes are smaller
    if (c1.is_on_shell() && !c2.is_on_shell()) { return true; }
    if (!c1.is_on_shell() && c2.is_on_shell()) { return false; }
    // at this point we know both is_on_shell() are equal
    if (c1.is_on_shell()) {
        // if is on shell, check mass labels
        const size_t m1(c1.get_mass_index());
        const size_t m2(c2.get_mass_index());
        if (m1 < m2) return true;
        if (m2 < m1) return false;
    }
    // lexicographical comparison of momentum indices
    return std::lexicographical_compare(c1.get_momentum_indices().begin(), c1.get_momentum_indices().end(), c2.get_momentum_indices().begin(),
                                        c2.get_momentum_indices().end());
}

bool operator==(const Node& c1, const Node& c2) { return !(c1 < c2) && !(c2 < c1); }

bool operator>(const Node& c1, const Node& c2) { return c2 < c1; }

std::ostream& operator<<(std::ostream& s, const Link& ll) {
    s << ll.get_mass_index();
    return s;
}

Bead::Bead() : on_shell(false), mass_index(0) {}

Bead::Bead(const Leg& l) : on_shell(l.is_on_shell()), legs({l}), momentum_indices({l.get_momentum_index()}) {
    if (on_shell) { mass_index = l.get_mass_index(); }
}

Bead::Bead(const std::vector<Leg>& vl) : legs(vl) {
    // sort legs
    std::sort(legs.begin(), legs.end());

    momentum_indices.clear();
    for (auto& l : legs) momentum_indices.push_back(l.get_momentum_index());
    std::sort(momentum_indices.begin(), momentum_indices.end());

    if (legs.size() == 1) {
        on_shell = vl[0].is_on_shell();
        if (on_shell) { mass_index = vl[0].get_mass_index(); }
    }
}

// TODO: this may be misnamed
bool Bead::is_massless() const {
    if (is_on_shell()) {
        // massless index is always 0
        return (get_mass_index() == 0);
    }
    else
        return false;
}

Bead Bead::filter() const {
    if (size() == 0) { // empty bead -- do nothing
        return *this;
    }
    Bead toret;
    size_t mom_ind = 1;
    if(on_shell){mom_ind = 0;}
    Leg filtered_leg(on_shell, mass_index, mom_ind);
    // Modify accordingly members of toret
    toret.on_shell = on_shell;
    toret.legs = {filtered_leg};
    toret.momentum_indices = {mom_ind};
    toret.mass_index = mass_index;
    return toret;
}

bool Bead::has_fields_defined() const {
    if (coups.size() > 0) return true;
    for (auto l : legs)
        if (l.has_field_defined()) return true;
    return false;
}

std::ostream& operator<<(std::ostream& s, const Bead& is) {
    s << "{";
    if (is.is_on_shell())
        s << "true, " << is.get_mass_index() << ", ";
    else
        s << "false, -1, ";
    s << "{";
    const std::vector<Leg>& llegs(is.get_legs());
    for (size_t ii = 0; ii < llegs.size(); ii++) {
        s << llegs[ii].get_momentum_index();
        if (ii + 1 < llegs.size()) s << ", ";
    }
    s << "}";
    s << "}";
    return s;
}

bool operator<(const Bead& b1, const Bead& b2) {
    // on-shell Bead is smaller
    if (b1.is_on_shell() && !b2.is_on_shell()) { return true; }
    if (!b1.is_on_shell() && b2.is_on_shell()) { return false; }
    // at this point we know both is_on_shell() are equal
    if (b1.is_on_shell()) {
        // if is on shell, check mass labels
        const size_t m1(b1.get_mass_index());
        const size_t m2(b2.get_mass_index());
        if (m1 < m2) return true;
        if (m2 < m1) return false;
    }
    // lexicographical comparison of momentum indices
    return std::lexicographical_compare(b1.get_momentum_indices().begin(), b1.get_momentum_indices().end(), b2.get_momentum_indices().begin(),
                                        b2.get_momentum_indices().end());
}

bool operator==(const Bead& b1, const Bead& b2) { return !(b1 < b2) && !(b2 < b1); }

bool operator>(const Bead& b1, const Bead& b2) { return b2 < b1; }

Node merge_graph_nodes(const Node& v1, const Node& v2) {
    std::vector<Leg> lls(v1.get_legs());
    // just insert, no need to sort, as Node sorts content
    lls.insert(lls.end(), v2.get_legs().begin(), v2.get_legs().end());

    return Node(lls);
}

Link::Link(LoopMomentum ll, size_t set_mi) : mass_index{set_mi}, mom{ll} {
    if (mom.active&&mom.index == 0) {
        _WARNING_R("ERROR: Inconsistent call of constructor: Link(", set_mi, ",", (mom.active ? "true" : "false"), ",", mom.index, ")");
        std::exit(1);
    }
}

Link Link::filter() const { return Link(mass_index); }

void Link::reverse() {
    mom.reverse();
    ptype.reverse();
}

bool operator<(const Link& link1, const Link& link2) { return link1.get_mass_index() < link2.get_mass_index(); }

bool operator==(const Link& link1, const Link& link2) { return !(link1 < link2) && !(link2 < link1); }

bool operator>(const Link& link1, const Link& link2) { return link2 < link1; }

Strand::Strand() : n_beads(0), n_links(1) { links.emplace_back(); construct_graphic(); }

Strand::Strand(const std::vector<Bead>& bds, const std::vector<Link>& lnks) : n_beads(bds.size()), n_links(lnks.size()), beads(bds), links(lnks) {
    if (n_links != n_beads + 1) {
        std::cerr << "ERROR: number of links in Strand has to be number of beads +1 " << std::endl;
        std::cerr << "recieved"
                  << " n_links = " << n_links << ", n_beads = " << n_beads << std::endl;
        std::exit(1);
    }
    construct_graphic();
}

Strand::Strand(const std::vector<Bead>& bds) : n_beads(bds.size()), n_links(bds.size() + 1), beads(bds) {
    links = std::vector<Link>(n_beads + 1, Link{});
    construct_graphic();
}

bool Strand::is_massless_link(int i) const {
    if (i < 1 || i > int(n_links)) {
        std::cerr << "ERROR: Out of bounds call to Strand::is_massless_link(i=" << i << ") for strand with " << n_links << " links -- Returned false"
                  << std::endl;
        return false;
    }
    // index should be 0 for all massless particles
    return links[i - 1].get_mass_index() == 0;
}

// TODO: Is this method properly named? Massless != on-shell
bool Strand::is_massless_bead(int i) const {
    if (i < 1 || i > int(n_beads)) {
        std::cerr << "ERROR: Out of bounds call to Strand::is_massless_bead(i=" << i << ") for strand with " << n_beads << " beads -- Returned false"
                  << std::endl;
        return false;
    }
    const Bead& localB(beads[i - 1]);
    return localB.is_massless();
}

bool Strand::check_on_shell_leg(int v, int p) const {
    if (v < 1 || v > int(n_beads) || p < 1 || p > int(n_links)) {
        std::cerr << "ERROR: Out of bounds call to Strand::check_on_shell_leg(v=" << v << ",p=" << p << ") for strand with " << n_beads
                  << " beads -- Returned false" << std::endl;
        return false;
    }
    const Bead& localB(beads[v - 1]);
    // check on-shellness
    if (!localB.is_on_shell()) return false;
    // check mass indices match
    if (localB.get_mass_index() == links[p - 1].get_mass_index())
        return true;
    else
        return false;
}

size_t Strand::bead_size(int c) const {
    if (c < 1 || c > int(n_beads)) {
        std::cerr << "ERROR: Out of bounds call to Strand::bead_size(c=" << c << ") for strand with " << n_beads << " beads -- Returned 0" << std::endl;
        return 0;
    }
    const Bead& localB(beads[c - 1]);
    return localB.size();
}

size_t Strand::get_bead_mass_index(int c) const {
    if (c < 1 || c > int(n_beads)) {
        std::cerr << "ERROR: Out of bounds call to Strand::get_bead_mass_index(c=" << c << ") for strand with " << n_beads << " vertices -- Returned 0"
                  << std::endl;
        return 0;
    }
    const Bead& localB(beads[c - 1]);
    if (!localB.is_on_shell()) {
        std::cerr << "ERROR: called get_bead_mass_index(.) for an off-shell vertex! Returned 0" << std::endl;
        return 0;
    }
    return localB.get_mass_index();
}

std::vector<size_t> Strand::get_link_mass_indices() const {
    std::vector<size_t> toret;
    for (auto lnk : links) { toret.push_back(lnk.get_mass_index()); }
    return toret;
}

size_t Strand::get_link_mass_index(int i) const {
    if (i < 1 || i > int(n_links)) {
        std::cerr << "ERROR: Out of bounds call to Strand::is_massless_propagator(i=" << i << ") for strand with " << n_links << " links -- Returned false"
                  << std::endl;
        return false;
    }
    return links[i - 1].get_mass_index();
}

const std::vector<size_t>& Strand::bead_momentum_indices(int i) const {
    if (i < 1 || i > int(n_beads)) {
        std::cout << "Out of bounds call to IntegralGraph::vertex_momentum_indices(i=" << i << ") for integral with " << n_beads
                  << " vertices -- Returned first vec" << std::endl;
        return beads[0].get_momentum_indices();
    }
    const Bead& localB(beads[i - 1]);
    // std::cout << localB.get_momentum_indices() << std::endl;
    return localB.get_momentum_indices();
}

namespace _misc {

using couplings_t = std::map<std::string,int>;
couplings_t add_couplings(couplings_t ca, const couplings_t& cb) {
    // we add cb to ca
    for (auto c : cb) {
        auto key = c.first;
        if (ca.find(key) == ca.end())
            ca[key] = c.second;
        else
            ca[key] += c.second;
    }
    return ca;
}
}

std::tuple<std::vector<Leg>, Link, bool, Strand> Strand::pinch(size_t i) const {
    if(i > links.size()){
        std::cerr<<"ERROR: Strand::pinch("<<i<<") called for a strand with "<<links.size()<<" links!"<<std::endl;
        std::exit(1);
    }
    std::vector<Leg> toret_legs;
    bool valid(true);
    Link trlink = links[i];

    // special case
    if ( n_beads == 0 )
        return { toret_legs, links[0], false, Strand() };

    if(i > 0 && i + 1< links.size()){
        std::vector<Leg> toconst = beads[i-1].get_legs();
        toconst.insert(toconst.end(), beads[i].get_legs().begin(), beads[i].get_legs().end());
        auto cbcoups = _misc::add_couplings(beads[i-1].coups, beads[i].coups);
        std::vector<Bead> vbtc;
        for(size_t jj=0; jj<i-1; jj++)  vbtc.push_back(beads[jj]);
        vbtc.push_back(Bead(toconst));
        vbtc.back().coups = cbcoups;
        for(size_t jj=i+1; jj < beads.size(); jj++)  vbtc.push_back(beads[jj]);
        std::vector<Link> vltc;
        vltc.insert(vltc.end(), links.begin(), links.begin()+i);
        vltc.insert(vltc.end(), links.begin()+i+1, links.end());
        return { toret_legs, trlink, valid, Strand(vbtc, vltc) };
    }
    else{
        size_t bdrop;
        if(i == 0){
            toret_legs = beads[0].get_legs();
            bdrop = 0;
        }
        else{
            toret_legs = beads.back().get_legs();
            bdrop = beads.size() - 1;
        }
        std::vector<Bead> vbtc;
        for(size_t jj=0; jj<beads.size(); jj++){
            if(jj != bdrop)
                vbtc.push_back(beads[jj]);
        }
        std::vector<Link> vltc;
        for(size_t jj=0; jj<links.size(); jj++){
            if(jj != i)
                vltc.push_back(links[jj]);
        }
        return { toret_legs, trlink, valid, Strand(vbtc, vltc) };
    }
}

void Strand::reverse() {
    has_been_reversed=!has_been_reversed;
    std::reverse(beads.begin(), beads.end());
    std::reverse(links.begin(), links.end());
    // reverse links
    for (auto& l : links) l.reverse();
    construct_graphic();
}

Strand Strand::filter() const {
    if (links.size() == 0) { // empty strand -- do nothing
        Strand toret;
        toret.permutation_tracking_label = permutation_tracking_label;
        toret.construct_graphic();
        return Strand();
    }
    else {
        std::vector<Bead> filtered_beads;
        for (auto& b : beads) { filtered_beads.push_back(b.filter()); }
        std::vector<Link> filtered_links;
        for (auto& l : links) { filtered_links.push_back(l.filter()); }
        Strand toret(filtered_beads, filtered_links);
        toret.permutation_tracking_label = permutation_tracking_label;
        toret.construct_graphic();
        return toret;
    }
}

bool Strand::has_loop_momentum(size_t index) const {
    if (!track_momenta) {
        std::cout << "WARNING: Requesting Strand::has_loop_momentum() when momenta are not tracked in strand -- returned false." << std::endl;
        return false;
    }
    if (n_links == 0) { return false; }
    // if everything is consistend it should be enough to check the first link
    std::set<size_t> internals = momenta[0].get_internal().plus_momenta;
    internals.insert(momenta[0].get_internal().minus_momenta.begin(), momenta[0].get_internal().minus_momenta.end());
    return internals.find(index) != internals.end();
}

void Strand::set_momentum_label(size_t link, int mom, const MomentumRouting& shift) {
    if (track_momenta) {
        std::cerr<<"ERROR: called Strand::set_momentum_label(.) when momentum was traced already!"<<std::endl;
        std::exit(767671);
    }
    if (has_link_with_mom_label()) {
        std::cerr<<"ERROR: called Strand::set_momentum_label(.) when there is a momentum label already!"<<std::endl;
        std::exit(767672);
    }
    if(link>=links.size()){
        std::cerr<<"ERROR: called Strand::set_momentum_label(.) for link: "<<link<<" (0-based) when there is only "<<links.size()<<std::endl;
        std::exit(767673);
    }
    links[link].set_momentum_label(mom, shift);
}

size_t Strand::get_n_legs() const {
    size_t tr(0);
    for (auto& b : beads) tr += b.size();
    return tr;
}

void Strand::show() const {
    DEBUG(
        std::cout<<"traced: "<<(track_momenta?"true":"false")<<std::endl;
        std::cout<<"momenta: "<<momenta<<std::endl;
    );
    for (auto& l : graphic) std::cout << l << std::endl;
}

void Strand::construct_graphic() {
    graphic.clear();
    // empty Strand
    if (links.size() == 0) { graphic.push_back("Empty_Strand"); }
    static const std::string m(" |");
    // not so nice, but well
    static const std::string M(" /");
    static const std::string l("--- ");
    static const std::string L("=== ");
    static const std::string Ls("==< ");
    static const std::string V(" +");
    for (size_t ii = 0; ii < links.size(); ii++) {
        bool massless_link(links[ii].is_massless_propagator());
        // initial and final links treated specially
        if (ii != 0 && (ii + 1) != links.size()) {
            if (massless_link)
                graphic.push_back(m);
            else
                graphic.push_back(M);
        }
        if (massless_link)
            graphic.push_back(m);
        else
            graphic.push_back(M + "(" + std::to_string(links[ii].get_mass_index()) + ")");
        if (ii != 0 && (ii + 1) != links.size()) {
            if (massless_link)
                graphic.push_back(m);
            else
                graphic.push_back(M);
        }
        if (ii < beads.size()) {
            graphic.push_back(V);
            if (!beads[ii].is_on_shell())
                graphic.back() += Ls;
            else if (beads[ii].get_mass_index() == 0)
                graphic.back() += l;
            else
                graphic.back() += L;
            // now add Bead info
            std::ostringstream stream;
            stream << beads[ii];
            graphic.back() += stream.str();
        }
    }
    // show a permutation index at the end if it exists
    if(get_permutation_tracking_label()>-1){
        graphic.push_back(" "+std::to_string(get_permutation_tracking_label()));
    }

    // just to allow a bit of space in this peculiar case
    if(get_permutation_tracking_label()>-1){
        if (links.size() == 1){ graphic.front() += "  "; graphic.back() += "  "; }
    }
    else{
        if (links.size() == 1){ graphic.back() += "  "; }
    }
    // standardize size
    size_t max_size(0);
    for (auto& l : graphic) {
        if (l.length() > max_size) max_size = l.length();
    }
    for (auto& l : graphic) {
        size_t missing(max_size - l.length());
        for (size_t ii = 0; ii < missing; ii++) l += " ";
    }

    // check if there is a link with momentum label and add it
    bool exist(false);
    int label(-42);
    size_t position(0);
    for (auto& l : links) {
        if (l.tracks_momentum()) {
            exist = true;
            label = l.get_momentum_label();
            break;
        }
        position++;
    }
    if (exist) {
        if (position > 0 && (position + 1) < links.size())
            position = 4 * position - 1;
        else if (position + 1 == links.size() && position != 0)
            position = 4 * position - 2;
        for (size_t ii = 0; ii + 1 < 4 * (links.size() - 1) || ii == 0; ii++) {
            if (ii == position) {
                if (label > 0)
                    graphic[ii] = "  " + std::to_string(label) + graphic[ii];
                else
                    graphic[ii] = " " + std::to_string(label) + graphic[ii];
            }
            else
                graphic[ii] = "   " + graphic[ii];
        }
        if(get_permutation_tracking_label()>-1)
            graphic.back() = "   " + graphic.back();
    }
}

bool Strand::has_fields_defined() const {
    for (auto b : beads)
        if (b.has_fields_defined()) return true;
    for (auto l : links) {
        if (l.ptype.particle != "" || l.ptype.antiparticle != "") return true;
    }
    return false;
}

bool Strand::has_link_with_mom_label() const {
    size_t counter(0);
    for (const auto& l : links)
        if (l.tracks_momentum()) counter++;
    if (counter == 0)
        return false;
    else if (counter == 1)
        return true;
    else
        throw std::logic_error("ERROR: in Strand::has_link_with_mom_label() found a Strand with " + std::to_string(counter) + " internal momentum labels!");
}

size_t Strand::mom_label_link() const {
    for (size_t ii = 0; ii < links.size(); ii++)
        if (links[ii].tracks_momentum()) return ii;
    throw std::logic_error("ERROR: in Strand::mom_label_link() found no link with internal momentum labels!");
}

std::vector<int> vsizet2vint(const std::vector<size_t>& in) {
    std::vector<int> r;
    for (auto i : in) r.push_back(int(i));
    return r;
}

void Strand::trace_momentum_routing(size_t nlegs) {
    if (track_momenta) {
        std::cout << "WARNING: trying Strand::trace_momentum_routing() when already done! Did nothing" << std::endl;
        return;
    }
    if (!has_link_with_mom_label()) { throw std::logic_error("ERROR: call to Strand::trace_momentum_routing() when no link has momentum label!"); }
    size_t master = mom_label_link();
    int mlabel = links[master].get_momentum_label();
    // allocate space for momenta
    momenta = std::vector<MomentumRouting>(links.size());
    // fill master link
    momenta[master] = MomentumRouting({mlabel}, {});
    // fill "later" links
    for (size_t ii = master + 1; ii < links.size(); ii++) momenta[ii] = momenta[ii - 1] - MomentumRouting(vsizet2vint(beads[ii - 1].get_momentum_indices()));
    // fill "earlier" links
    for (size_t ii = master; ii > 0; ii--) momenta[ii - 1] = momenta[ii] + MomentumRouting(vsizet2vint(beads[ii - 1].get_momentum_indices()));
    // momentum conservation
    for (size_t ii = 0; ii < links.size(); ii++) momenta[ii].external_mom_conservation(nlegs);
    track_momenta = true;
}

void Strand::trace_momentum_routing(size_t nlegs, const MomentumRouting& initial) {
    if (track_momenta) {
        std::cout << "WARNING: trying Strand::trace_momentum_routing() when already done! Did nothing" << std::endl;
        return;
    }
    if (has_link_with_mom_label()) {
        throw std::logic_error("ERROR: call to Strand::trace_momentum_routing(size_t,MomentumRouting) when a link has a momentum label!");
    }
    momenta = std::vector<MomentumRouting>(links.size());
    momenta[0] = initial;
    // fill "later" links
    for (size_t ii = 1; ii < links.size(); ii++) momenta[ii] = momenta[ii - 1] - MomentumRouting(vsizet2vint(beads[ii - 1].get_momentum_indices()));
    // momentum conservation
    for (size_t ii = 0; ii < links.size(); ii++) momenta[ii].external_mom_conservation(nlegs);
    track_momenta = true;
}

void Strand::reset_momentum_routing(bool b, size_t n) {
    // make sure n < get_n_links()
    n = n % get_n_links();
    track_momenta = false;
    momenta.clear();
    // now check if any Link has an assignment and move it accordingly
    if (has_link_with_mom_label() && b) {
        auto thelink = mom_label_link();
        auto thelabel = links[thelink].get_momentum_label();
        // reset
        links[thelink] = Link(links[thelink].get_mass_index());
        // and redefine corresponding link!
        links[n] = Link(LoopMomentum{std::abs(thelabel)},links[n].get_mass_index());
    }
    construct_graphic();
}

Strand Strand::get_untraced_copy() const {
    if (links.size() == 0) { // empty strand -- do nothing
        return Strand();
    }
    else {
        std::vector<Link> untraced_links;
        for (auto& l : links) { untraced_links.push_back(Link(l.get_mass_index())); }
        return Strand(beads, untraced_links);
    }
}

bool operator<(const Strand& strand1, const Strand& strand2) {

    // lexicographical comparison of the links
    if (std::lexicographical_compare(strand1.get_links().begin(), strand1.get_links().end(), strand2.get_links().begin(), strand2.get_links().end())) {
        return true;
    }
    if (std::lexicographical_compare(strand2.get_links().begin(), strand2.get_links().end(), strand1.get_links().begin(), strand1.get_links().end())) {
        return false;
    }

    // lexicographical comparison of the beads
    return std::lexicographical_compare(strand1.get_beads().begin(), strand1.get_beads().end(), strand2.get_beads().begin(), strand2.get_beads().end());
}

bool operator==(const Strand& strand1, const Strand& strand2) { return !(strand1 < strand2) && !(strand2 < strand1); }

bool operator>(const Strand& strand1, const Strand& strand2) { return strand2 < strand1; }

xGraph::xGraph() {
    base_graph = Graph<Node, Connection<Strand>>();
    // as this is an 'empty' xGraph, we don't call here the heavier method generate_topology_from_base_graph();
    topology = base_graph.filter();
    fill_info();
}

xGraph::xGraph(const Graph<Node, Connection<Strand>>& graph) {
    base_graph = graph;
    fill_info(); //< has to be performed first, other methods depend on properly assigned info
    // make the onshell indices (trivial) map
    for (auto& i: onshell_indices()) canonical_onshell_indices[i] = i;
    standard_one_loop_xGraph(base_graph);
    generate_topology_from_base_graph(); 
    standard_one_loop_xGraph(topology);
    trace_momentum_routing();
    // fill legs
    for(auto& n:base_graph.get_nodes())
        legs.insert(legs.end(),n.get_legs().begin(),n.get_legs().end());
    for(auto& conn:base_graph.get_connection_map())
        for(auto& strand:conn.second.get_edges())
            for(auto& b:strand.get_beads())
                legs.insert(legs.end(),b.get_legs().begin(),b.get_legs().end());
}

size_t _compute_number_of_legs(const Graph<Node, Connection<Strand>>& g) {
    size_t nlegs(0);
    for(auto& n: g.get_nodes()) nlegs += n.size();
    for(auto& [pair, conn]: g.get_connection_map()) {
        for(auto& strand:conn.get_edges()) {
            nlegs += strand.get_n_legs();
        }
    }
    return nlegs;
}

void xGraph::fill_info() {
    n_vertices = base_graph.get_n_nodes();
    n_nodes = base_graph.get_n_nodes();
    n_external_momenta = 0;
    n_edges = 0;
    for (auto& it : base_graph.get_connection_map()) {
        auto con = it.second;
        for (size_t ii = 0; ii < con.size(); ii++) {
            n_vertices += con[ii].size();
            n_edges += con[ii].get_n_links();
            n_external_momenta += con[ii].size();
        }
    }
    n_loops = base_graph.get_n_loops();
    n_legs = _compute_number_of_legs(base_graph);
    n_topology_legs = base_graph.get_n_legs();
    for (auto& v : base_graph.get_nodes()) {
        if (v.size() > 0) { n_external_momenta++; }
    }
    // check field information
    for (auto n : base_graph.get_nodes())
        if (n.has_fields_defined()) {
            var_fields_defined = true;
            break;
        }
    if(!var_fields_defined) {
        for (auto con : base_graph.get_connection_map()){
            for(auto strand: con.second.get_edges()) {
                if(strand.has_fields_defined()) {
                    var_fields_defined = true;
                    break;
                }
            }
            if(var_fields_defined) break;
        }
    }
}

void xGraph::generate_topology_from_base_graph() {
    // Before producing topology, assign permutation tracking labels for nodes and strand in base_graph. This way the labels between base_graph 
    // and topology will be aligned (w.r.t. Nodes and Strands in Connections)
    for (size_t ii = 0; ii < base_graph.get_nodes().size(); ii++) {
        base_graph[ii].assign_permutation_tracking_label(int(ii));
    }
    for (auto& con : base_graph.get_connection_map())
        for (size_t ii = 0; ii < con.second.size(); ii++) con.second[ii].assign_permutation_tracking_label(int(ii));
    topology = canonical_topology(base_graph, onshell_indices(), canonical_onshell_indices);
}

void xGraph::standard_one_loop_xGraph(Graph<Node, Connection<Strand>>& modify) {
    // this only applies to 1-loop graphs
    if (!(modify.get_n_loops() == 1)) return;
    // collect the Node and all Beads in Strand into a single container
    const Strand& strand = modify.get_connection(0, 0)[0];
    const std::vector<Bead>& lbeads = strand.get_beads();
    const std::vector<Link>& llinks = strand.get_links();

    // construct a Bead build of the legs of Node
    Bead node(modify.get_nodes()[0].get_legs());

    // construct a container of all Beads and Links
    std::vector<std::pair<Bead, Link>> one_loop = {{node, llinks[0]}};
    for (size_t ii = 0; ii < lbeads.size(); ii++) one_loop.push_back({lbeads[ii], llinks[ii + 1]});

    size_t the_rotation(0);
    auto minimum = one_loop;
    // find the minimum
    for (size_t ii = 1; ii < one_loop.size(); ii++) {
        // rotate 1-loop
        std::rotate(one_loop.begin(), one_loop.begin() + 1, one_loop.end());
        if (std::lexicographical_compare(one_loop.begin(), one_loop.end(), minimum.begin(), minimum.end())) {
            minimum = one_loop;
            the_rotation = ii;
        }
    }

    // nothing to do
    if (the_rotation == 0) return;

    // restructure 1-loop graph
    std::vector<Link> tcl = strand.get_links();
    std::rotate(tcl.begin(), tcl.begin() + the_rotation, tcl.end());
    std::vector<Bead> tcb = lbeads;
    // insert in the end the old Node
    tcb.push_back(Bead(modify.get_nodes()[0].get_legs()));
    tcb.back().coups = modify.get_nodes()[0].coups;
    // rotate to bring minimum to the back
    std::rotate(tcb.begin(), tcb.begin() + the_rotation, tcb.end());
    // drop minimum from container
    Node nm(tcb.back().get_legs());
    nm.coups = tcb.back().coups;
    nm.assign_permutation_tracking_label(modify.get_nodes()[0].get_permutation_tracking_label());
    tcb.pop_back();
    Strand thestrand(tcb,tcl);
    thestrand.assign_permutation_tracking_label(strand.get_permutation_tracking_label());

    modify = Graph<Node, Connection<Strand>>{{nm}, thestrand};
}

std::ostream& operator<<(std::ostream& o, const xGraph& g) {
    o << g.get_base_graph();
    return o;
}

xGraph xGraph::get_relabeled_copy(const xGraph& other) const {

    if (!(topology == other.get_topology())) {
        _WARNING_R("ERROR: Trying to relable \n",*this,"in terms of topologically non-equivalent graph\n",other, "in xGraph::get_relabeled_copy(.).");
        std::exit(1);
    }
    if(!other.momemtum_traced()){
        std::cerr<<"ERROR: calling xGraph::get_relabeled_copy(other) when 'other' isn't traced! We print it next and exit!"<<std::endl;
        other.show();
        std::exit(4711265);
    }

    std::map<std::pair<size_t, size_t>, Connection<Strand>> new_connection_map;
    for (auto& it : base_graph.connection_map) {
        auto con = it.second;
        std::vector<Strand> new_strands;
        for (size_t ii = 0; ii < con.size(); ii++) {
            // take the beads and mass labels from *this
            std::vector<Link> new_links;
            std::vector<Link> links = con[ii].get_links();
            for (size_t jj = 0; jj < con[ii].get_n_links(); jj++) {
                // make sure no momentum info is passed into the links
                new_links.push_back(Link(links[jj].get_mass_index()));
            }

            Strand lstrand(Strand(con[ii].get_beads(), new_links));
            lstrand.assign_permutation_tracking_label(con[ii].get_permutation_tracking_label());
            new_strands.push_back(lstrand);
        }
        new_connection_map[it.first] = Connection<Strand>(new_strands);
    }
    // Now loop over momenta assigned in other
    const auto& momenta_other = other.get_internal_momenta();
    // traverse momenta_other and assign corresponding link momentum label in links in new_connection_map
    for(const auto& ll:momenta_other){
        // this is the MomChoice correspoding to 'll' in 'this'
        auto momchoice = get_corresponding_MomChoice(ll,other);
        DEBUG(
            std::cout<<"TRACK get_corresponding_MomChoice(.): input: "<<ll<<" output: "<<momchoice<<std::endl;
        );
        auto& strand = new_connection_map.at(momchoice.link.connection)[momchoice.link.strand];
        auto& link = strand.get_links()[momchoice.link.locate];

        // assign momentum from 'other'
        if(momchoice.direction)
            link.set_momentum_label(int(momchoice.n));
        else
            link.set_momentum_label(-int(momchoice.n));
        strand.construct_graphic();
    }

    // take the nodes from *this to construct a new base graph
    Graph<Node, Connection<Strand>> graph(base_graph.get_nodes(), new_connection_map);
    return {graph};
}

MomChoice xGraph::get_corresponding_MomChoice(const MomChoice& mc,const xGraph& other) const {
    // permutation labels for the nodes of the connection that contains mc in other
    std::pair<int,int> clabels = { other.get_base_graph().get_nodes()[mc.link.connection.first].get_permutation_tracking_label(),
                                            other.get_base_graph().get_nodes()[mc.link.connection.second].get_permutation_tracking_label() };
    // sanity check
    if (clabels.first < 0 || clabels.second < 0) {
        std::cerr << "ERROR: called get_corresponding_MomChoice(.) without assigning tracking labels!" << std::endl;
        other.show();
        std::exit(1212);
    }
    const auto& otherstrand_bg = other.get_base_graph().get_connection_map().at(mc.link.connection)[mc.link.strand];
    int slabel = otherstrand_bg.get_permutation_tracking_label();
    // sanity check
    if (slabel < 0) {
        std::cerr << "ERROR: called get_corresponding_MomChoice(.) without assigning tracking labels for a strand!" << std::endl;
        other.show();
        std::exit(1213);
    }
    // now find the corresponding 'clabels' and 'slabel' in *this
    std::pair<int,int> clabels_this = { -1 , -1 };
    std::pair<size_t,size_t> thisnodes_topology = { 0 , 0 };
    for (size_t ii = 0; ii < other.get_topology().get_nodes().size(); ii++) {
        const auto& onode = other.get_topology().get_nodes()[ii];
        if (clabels.first == onode.get_permutation_tracking_label()) {
            clabels_this.first = get_topology().get_nodes()[ii].get_permutation_tracking_label();
            thisnodes_topology.first = ii;
        }
        if (clabels.second == onode.get_permutation_tracking_label()) {
            clabels_this.second = get_topology().get_nodes()[ii].get_permutation_tracking_label();
            thisnodes_topology.second = ii;
        }
    }
    if (clabels_this.first < 0 || clabels_this.second < 0) {
        std::cerr << "ERROR: Failed to find corresponding nodes with labels " << clabels << " in 'other', in the 'this' xGraph! Call in xGraph::get_corresponding_MomChoice(.)" << std::endl;
        show();
        other.show();
        std::exit(7716239);
    }
    
    // now find the corresponding connection in 'this' xGraph (we also tag if a "reverse" is needed)
    std::pair<size_t,size_t> this_connection;
    bool reverse (false);
    for (size_t ii = 0; ii < get_base_graph().get_nodes().size(); ii++) {
        const auto& fnode = get_base_graph().get_nodes()[ii];
        if (clabels_this.first == fnode.get_permutation_tracking_label()) {
            this_connection.first = ii;
        }
        if (clabels_this.second == fnode.get_permutation_tracking_label()) {
            this_connection.second = ii;
        }
    }
    // in case the connection isn't present
    if(get_base_graph().get_connection_map().find(this_connection)==get_base_graph().get_connection_map().end()){
        std::pair<size_t,size_t> this_connection_reverse = { this_connection.second , this_connection.first };
        // it should then be the reverse!
        if(get_base_graph().get_connection_map().find(this_connection_reverse)==get_base_graph().get_connection_map().end()){
            std::cerr << "ERROR: Did not find connection " << this_connection << " (or reversed) in the 'this' xGraph!" << std::endl;
            show();
            std::exit(7716240);
        }
        this_connection = this_connection_reverse;
        reverse = true;
    }
    // special care for 'reverse' in self-connections
    if(this_connection.first==this_connection.second){
        // check if the corresponding strand in this->topology has been reversed
        // we pick first strand in the connection, just because the reverse info is the same to all of the strands contained in the connection
        bool this_reversed = topology.get_connection_map().at(this_connection)[0].get_has_been_reversed();
        // and the same for 'other'
        bool other_reversed = other.get_topology().get_connection_map().at(mc.link.connection)[0].get_has_been_reversed();
        if (this_reversed != other_reversed) reverse = true;
    }

    // find out which strand in the connection we need to target
    int slabel_this(-1);
    size_t thestrand(0);
    if(get_topology().get_connection_map().find(thisnodes_topology)==get_topology().get_connection_map().end()){

        std::pair<size_t,size_t> thisnodes_topology_reverse = { thisnodes_topology.second , thisnodes_topology.first };
        // it should then be the reverse!
        if(get_topology().get_connection_map().find(thisnodes_topology_reverse)==get_topology().get_connection_map().end()){
            std::cerr << "ERROR: Did not find connection " << thisnodes_topology << " (or reversed) in the 'this' xGraph!" << std::endl;
            show();
            std::exit(7716240);
        }
        thisnodes_topology = thisnodes_topology_reverse;
    }

    for(size_t ii=0;ii<other.get_topology().get_connection_map().at(thisnodes_topology).get_edges().size();ii++){
        const auto& topostrand = other.get_topology().get_connection_map().at(thisnodes_topology).get_edges()[ii];
        if(topostrand.get_permutation_tracking_label() == slabel){
            const auto& topostrand_this = get_topology().get_connection_map().at(thisnodes_topology).get_edges()[ii];
            slabel_this = topostrand_this.get_permutation_tracking_label();
            break;
        }
    }
    if (slabel_this < 0) {
        std::cerr << "ERROR: Failed to find corresponding strand with label " << slabel << " in 'other', in the 'this' xGraph! xGraph::get_corresponding_MomChoice(.)" << std::endl;
        show();
        other.show();
        std::exit(7716242);
    }
    bool found(false);
    for(size_t ii=0;ii<get_base_graph().get_connection_map().at(this_connection).get_edges().size();ii++){
        const auto& lstrand = get_base_graph().get_connection_map().at(thisnodes_topology).get_edges()[ii];
        if(lstrand.get_permutation_tracking_label() == slabel_this){
            thestrand=ii;
            found=true;
            break;
        }
    }
    if (!found) {
        std::cerr << "ERROR: Failed to find strand with label " << slabel_this << " in base_graph of the 'this' xGraph!" << std::endl;
        show();
        show_topology();
        std::exit(7716242);
    }

    // to return the information
    size_t link = (reverse ? otherstrand_bg.get_beads().size()-mc.link.locate : mc.link.locate );
    bool direction;
    if(( mc.direction&&reverse )||( (!mc.direction)&&(!reverse) ))
        direction=false;
    else
        direction=true;

    return { mc.n , {this_connection, thestrand, link}, direction };
}

void xGraph::show(std::ostream& out) const { base_graph.show(out); }

void xGraph::show_topology(std::ostream& out) const { topology.show(out); }

void xGraph::reset_momentum_routing(bool b, size_t n) {
    loop_momenta = MomentumDecoration();
    // reset base_graph
    std::map<std::pair<size_t, size_t>, Connection<Strand>>& theconnections = base_graph.get_connection_map();
    for (auto& con : theconnections) {
        for (size_t ii = 0; ii < con.second.size(); ii++) con.second[ii].reset_momentum_routing(b, n);
    }
    // reset topology
    std::map<std::pair<size_t, size_t>, Connection<Strand>>& topconnections = topology.get_connection_map();
    for (auto& con : topconnections) {
        for (size_t ii = 0; ii < con.second.size(); ii++) con.second[ii].reset_momentum_routing(b, n);
    }
}

bool xGraph::has_only_simple_mom_insertions() const {
    // first check Nodes
    for(auto& node:base_graph.get_nodes())
        // it can be either 0 or 1
        if(node.size()>1)  return false;
    // now traverse all Beads
    for(const auto& con:base_graph.get_connection_map()){
        for(const auto& strand:con.second.get_edges()){
            for(const auto& bead:strand.get_beads()){
                // it has to be equal to 1
                if(bead.size()!=1)
                    return false;
            }
        }
    }
    return true;
}

Leg get_missing_leg(const std::vector<Leg>& subset, const std::vector<Leg>& all) {
    // sanity check
    if(!(subset.size()+1==all.size())){
        std::cerr<<"ERROR: get_missing_leg(.) with wrong size input "<<subset.size()<<" & "<<all.size()<<std::endl;
        std::exit(1);
    }
    Leg toret;
    size_t check(0);
    for (auto& l : all) {
        if (std::find(subset.begin(), subset.end(), l) == subset.end()) {
            toret=l;
            check++;
        }
    }
    
    if(check!=1){
        std::cerr << "ERROR: get_missing_leg(.) failed! check: "<<check<<" "<<subset<<" "<<all << std::endl;
    }
    return toret;
}

bool bubble_check(const std::vector<size_t>& relevantnodes, const std::vector<std::pair<size_t, size_t>>& relevantconns,
                  const xGraph& xgraph) {
    const auto& graph = xgraph.get_base_graph();
    size_t nlegs(xgraph.get_n_legs());

    std::vector<Leg> all_massless;
    std::vector<Leg> all_nlegs_minus_one_massless;

    // now all nodes (except node)
    for (size_t ii:relevantnodes) {
        auto& thenode = graph.get_nodes()[ii];
        if (thenode.size() > 1){
            // check if all attached
            if(!(thenode.size()==nlegs)){
                // check if it adds to massless leg
                if (thenode.size() + 1 == nlegs) {
                    auto theleg = get_missing_leg(thenode.get_legs(), xgraph.get_legs());
                    if (theleg.is_on_shell()) {
                        if (theleg.get_mass_index() != 0) return false;
                        else all_nlegs_minus_one_massless.push_back(theleg);
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            // else continues, just to check if propagators are massless
        }
        else if (thenode.size() == 1) {
            if (thenode.get_legs()[0].is_on_shell()) {
                if (thenode.get_legs()[0].get_mass_index() == 0) {
                    all_massless.push_back(thenode.get_legs()[0]);
                }
                else {
                    return false;
                }
            }
            else
                return false;
        }
        // else just continues
    }
    // now all strands
    for (auto connpair : relevantconns) {
        auto conn = graph.get_connection_map().at(connpair);
        for (auto strand : conn.get_edges()) {
            // first: check if momenta attached implies a scale (and return false)
            if (strand.size() > 2)
                // more than thre attached legs
                return false;
            else if (strand.size() == 2) {
                // two legs attached, test if (not) massless + (nlegs-1) combination
                bool oneAll = strand.is_massless_bead(1) && (strand[1].size() + 1 == nlegs);
                bool Allone = strand.is_massless_bead(2) && (strand[0].size() + 1 == nlegs);
                if((!oneAll)&&(!Allone))
                    return false;
                if(oneAll){
                    all_massless.push_back(strand[0].get_legs()[0]);
                    all_nlegs_minus_one_massless.push_back(strand[0].get_legs()[0]);
                }
                if(Allone){
                    all_massless.push_back(strand[1].get_legs()[0]);
                    all_nlegs_minus_one_massless.push_back(strand[1].get_legs()[0]);
                }
                // else continues, checking if massive props
            }
            else if (strand.size() == 1) {
                if (strand.is_massless_bead(1) && strand.is_massless_link(1) && strand.is_massless_link(2)) {
                    all_massless.push_back(strand[0].get_legs()[0]);
                }
                // check the particular case where the attached leg with momentum conservation adds to zero (or massless momentum)
                else if (strand.is_massless_link(1) && strand.is_massless_link(2)) {
                    // doesn't add to zero
                    if (strand[0].size() != nlegs) {
                        if (strand[0].size() + 1 == nlegs) {
                            auto theleg = get_missing_leg(strand[0].get_legs(), xgraph.get_legs());
                            if (theleg.is_on_shell()) {
                                if (theleg.get_mass_index() != 0) return false;
                                else all_nlegs_minus_one_massless.push_back(theleg);
                            }
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    // else continues, checking if massive props
                }
            }
            else {
                if (!strand.is_massless_link(1)) return false;
            }
            // second: check if any massive prop
            for(auto& l:strand.get_links())
                if(!l.is_massless_propagator())
                    return false;
        }
    }

    if(all_nlegs_minus_one_massless.size()==0){
        if(all_massless.size()<2)
            return true;
        else
            return false;
    }
    if(all_nlegs_minus_one_massless.size()==1)
        return true;
    else{
        std::cerr<<"ERROR: should never reach end in bubble_check(.)!"<<std::endl;
        std::exit(1);
        return false;
    }
}

bool node_with_scaleless_bubble(size_t node,const xGraph& xgraph){
    const auto& graph = xgraph.get_base_graph();
    const auto& bubsets = graph.get_vacuum_bubbles(node);
    for(auto bubset:bubsets){
        // treat two special cases separately:
        // tadpole
        if(bubset.size()==1){
            auto conn = graph.get_connection_map().at({node,node});
            for(auto strand:conn.get_edges()){
                if(strand.size()>1)
                    // more than two attached legs, continue to next strand
                    continue;
                else if(strand.size()==1){
                    if(strand.is_massless_bead(1)&&strand.is_massless_link(1)&&strand.is_massless_link(2))
                        return true;
                    // check the particular case where the attached leg with momentum conservation adds to zero (or massless momentum)
                    else if(strand.is_massless_link(1)&&strand.is_massless_link(2)){
                        size_t nlegs(xgraph.get_n_legs());
                        // adds to zero
                        if(strand[0].size()==nlegs)
                            return true;
                        else if (strand[0].size() + 1 == nlegs) {
                            auto theleg = get_missing_leg(strand[0].get_legs(), xgraph.get_legs());
                            if (theleg.is_on_shell()) {
                                if (theleg.get_mass_index() == 0) return true;
                                // continue to next strand
                                continue;
                            }
                            // continue to next strand
                            continue;
                        }
                        else
                            // continue to next strand
                            continue;
                    }
                    else
                        // continue to next strand
                        continue;
                }
                else{
                    if(strand.is_massless_link(1))
                        return true;
                    else
                        // continue to next strand
                        continue;
                }
            }
        }
        // whole graph
        else if(bubset.size()==graph.get_n_nodes()){
            std::vector<size_t> relevantnodes;
            for(size_t ii=0;ii<graph.get_nodes().size();ii++)
                if(ii!=node)
                    relevantnodes.push_back(ii);
            std::vector<std::pair<size_t,size_t>> relevantconns;
            for(auto conn:graph.get_connection_map())
                relevantconns.push_back(conn.first);
            if(bubble_check(relevantnodes,relevantconns,xgraph))
                return true;
            else
                // continue to next bubset
                continue;
        }
        // generic bubble
        else{
            std::vector<size_t> relevantnodes;
            for(size_t ii:bubset)
                if(ii!=node)
                    relevantnodes.push_back(ii);
            std::vector<std::pair<size_t,size_t>> relevantconns;
            for(auto conn:graph.get_connection_map()){
                // the pair should connect two nodes included in bubset (while not being a self connection {node, node})
                if(conn.first!=std::pair<size_t,size_t>{node,node}){
                    if( std::find(bubset.begin(),bubset.end(),conn.first.first)!=bubset.end()
                      &&  std::find(bubset.begin(),bubset.end(),conn.first.second)!=bubset.end())
                        relevantconns.push_back(conn.first);
                }
            }
            if(bubble_check(relevantnodes,relevantconns,xgraph))
                return true;
            else
                // continue to next bubset
                continue;
        }
    }
    return false;
}

bool xGraph::has_scaleless_bubble() const {
    if(legs.size()<3){
        std::cout<<"WARNING: xGraph::has_scaleless_bubble() assumes all sij's different from zero! Legs: "<<legs<<std::endl;
    }
    for(size_t node=0;node<n_nodes;node++){
        if(node_with_scaleless_bubble(node,*this))
            return true;
    }
    return false;
}

bool xGraph::is_bubble_with_single_massless_leg() const {
  if( n_loops != 1 ) return false;
  return has_single_massless_leg();
}

bool xGraph::strand_has_single_massless_leg(const Strand& strand) const {
    size_t nlegs = get_n_legs();
    std::vector<Bead> beads = strand.get_beads();
    if (beads.size() != 1) return false;

    auto lnlegs = beads[0].get_legs().size();
    if (lnlegs == 1) {
        if (beads[0].is_on_shell() && beads[0].get_mass_index() == 0) return true;
    }
    else if (lnlegs + 1 == nlegs) {
        auto theleg = get_missing_leg(beads[0].get_legs(), get_legs());
        if (theleg.is_on_shell() && theleg.get_mass_index() == 0) return true;
    }
    return false;
}

bool xGraph::has_single_massless_leg() const {
    auto conMap = (*this).get_base_graph().get_connection_map();

    for (auto s : conMap) {
        auto strands = s.second;
        for (unsigned n = 0; n < strands.size(); n++) {
            if (strand_has_single_massless_leg(strands[n]) ) return true;
        }
    }

    return false;
}

std::vector<AccessLink> xGraph::list_links() const {
    std::vector<AccessLink> toret;

    // traverse base_graph
    for(auto& conn:base_graph.get_connection_map()){
        size_t ii(0);
        for(auto& strand:conn.second.get_edges()){
            for(size_t jj = 0; jj < strand.get_links().size(); jj++)
                toret.push_back( { conn.first, ii, jj } );
            ++ii;
        }
    }

    return toret;
}

namespace _misc {

size_t update_index(size_t old, const std::pair<size_t, size_t>& old_nodes) {
    assert( old_nodes.first < old_nodes.second );
    if ( old < old_nodes.second ) return old;
    else if ( old == old_nodes.second) return old_nodes.first;
    else return old - 1;
}

void fill_in_map(const std::pair<size_t, size_t>& old_nodes, const std::pair<size_t, size_t>& this_connection, size_t thestrand, const Connection<Strand>& conn, 
                 std::map<std::pair<size_t, size_t>, Connection<Strand>>& tofill){
    std::pair<size_t, size_t> new_connection = { update_index(this_connection.first, old_nodes), update_index(this_connection.second, old_nodes) };
    auto replace_conn = conn;
    // operate in replace_conn
    if( old_nodes == this_connection ) {
        auto newedges = conn.get_edges();
        newedges.erase(newedges.begin() + thestrand, newedges.begin() + thestrand + 1);
        replace_conn = Connection<Strand>( newedges );
    }
    tofill[new_connection] = replace_conn;
}

AccessLink get_new_assignmom_for_merged_node_case(size_t ln, const std::map<std::pair<size_t, size_t>, Connection<Strand>>& cmtc, const xGraph& xg, const AccessLink& drop) {
    AccessLink toret;
    bool done(false);
    // find 'first' Link with corresponding momentum assignment
    for (auto& cc : cmtc) {
        // loop over all strands in cc.second
        for (size_t kk = 0; kk < cc.second.get_edges().size(); kk++) {
            auto& tstrand = cc.second.get_edges()[kk];
            // do not pick strands with assined momentum
            if (!tstrand.has_link_with_mom_label()) {
                // check first link in which ln appears
                for (size_t ww = 0; ww < tstrand.get_routings().size(); ww++) {
                    auto& rout = tstrand.get_routings()[ww];
                    auto& momenta = rout.get_internal();
                    // check negative momenta
                    if (std::find(momenta.minus_momenta.begin(), momenta.minus_momenta.end(), ln) != momenta.minus_momenta.end() ||
                        // check positive momenta
                        std::find(momenta.plus_momenta.begin(), momenta.plus_momenta.end(), ln) != momenta.plus_momenta.end()) {
                        toret.connection = cc.first;
                        toret.strand = kk;
                        toret.locate = ww;
                        done = true;
                        DEBUG(std::cout << "Will assign momentum " << ln << " in connection " << toret.connection << " strand: " << toret.strand
                                        << " new_link: " << toret.locate << " which has routing: "<< rout << std::endl;);
                        break;
                    }
                }
            }
            if (done) break;
        }
        if (done) break;
    }
    if (!done) {
        std::cerr << "ERROR: logical error, did not find a place to assign momentum " << ln << " while pinching " << drop
                  << " in the following xgraph (node merge case)" << std::endl;
        xg.show(std::cerr);
        std::exit(1);
    }
    return toret;
}

bool positive_flow(const MomentumRouting& q0,const size_t& l) {
    auto& positive = q0.get_internal().plus_momenta;
    return std::find(positive.begin(), positive.end(), l) != positive.end();
}

bool negative_flow(const MomentumRouting& q0,const size_t& l) {
    auto& negative = q0.get_internal().minus_momenta;
    return std::find(negative.begin(), negative.end(), l) != negative.end();
}

void assign_q0new(MomentumRouting& q0new, const size_t& ln, const MomentumRouting& q0old, const xGraph& xg, const AccessLink& drop) {
    if (positive_flow(q0new, ln)) {
        q0new = q0new - MomentumRouting({int(ln)}, {}) + q0old;
    }
    else if (negative_flow(q0new, ln)) {
        q0new = q0new + MomentumRouting({int(ln)}, {}) - q0old;
    }
    else {
        std::cerr << "ERROR assign_q0new(.): when assigning new mom, did not find momentum: " << ln << " in " << q0new << " drop: " << drop
                  << std::endl;
        xg.show(std::cerr);
        std::exit(1);
    }
}

AccessLink get_new_assigned_mom(const xGraph& xg, const AccessLink& drop, size_t ln, size_t count_beads) {
    AccessLink new_assignmom;
    auto cmtc = xg.get_base_graph().get_connection_map();
    // merges nodes
    if (count_beads == 0) new_assignmom = _misc::get_new_assignmom_for_merged_node_case(ln, cmtc, xg, drop);
    // extreme pinch
    else if (drop.locate == 0 || drop.locate == count_beads) {
        new_assignmom.connection = drop.connection;
        new_assignmom.strand = drop.strand;

        if (drop.locate == 0)
            new_assignmom.locate = 1;
        else
            new_assignmom.locate = 0;
    }
    // normal pinch
    else {
        new_assignmom.connection = drop.connection;
        new_assignmom.strand = drop.strand;
        new_assignmom.locate = 0; // always at first location
    }
    return new_assignmom;
}

}

#define _PINCH_ERASE_SHIFT_IN_DAUGHTER

xGraph xGraph::pinch(const AccessLink& drop, bool erase_shifts) const {
    // sanity check (in debug mode)
    DEBUG(
        auto listlinks = list_links();
        if ( std::find(listlinks.begin(), listlinks.end(), drop) == listlinks.end() ) {
            std::cerr<<"ERROR: link "<<drop<<" not present in the following xGraph"<<std::endl;
            show(std::cerr);
            std::exit(1);
        }
    );

    auto newnodes = base_graph.get_nodes();
    auto newconnections = base_graph.get_connection_map();
    auto momenta_assigned = get_internal_momenta();

    size_t count_strands = newconnections.at( drop.connection ).get_edges().size();
    auto& thestrand = newconnections.at( drop.connection ).get_edges()[ drop.strand ];
    size_t count_beads = thestrand.size();

    // check if 'drop' removes a Link with assigned momentum
    for (auto& ma : momenta_assigned)
        if (ma.link == drop) {
            // dropping propagator in a 'flower' Node
            if (count_beads == 0) {
                size_t old_node1 = drop.connection.first;
                size_t old_node2 = drop.connection.second;
                // dropping propagator in a 'flower' Node
                if (old_node1 == old_node2) {
                    DEBUG(std::cout << "CAREFUL xGraph::pinch(.): flower pinch" << std::endl;);
                    return xGraph();
                }
            }
            AccessLink replace = _misc::get_new_assigned_mom(*this, drop, ma.n, count_beads);
            auto this_mom_routing = get_momentum_routing(replace);
            auto q0new = this_mom_routing;
#ifndef _PINCH_ERASE_SHIFT_IN_DAUGHTER
            auto q0old = get_shift(ma.link);
#else
            // drop old info
            MomentumRouting q0old;
#endif
            _misc::assign_q0new(q0new, ma.n, q0old, *this, drop);
            bool direction;
            auto internal = this_mom_routing.get_internal();
            if (std::find(internal.plus_momenta.begin(), internal.plus_momenta.end(), ma.n) != internal.plus_momenta.end())
                direction = true;
            else if (std::find(internal.minus_momenta.begin(), internal.minus_momenta.end(), ma.n) != internal.minus_momenta.end())
                direction = false;
            else {
                std::cerr << "ERROR: asked to drop " << drop << " but corresponding momentum routing " << this_mom_routing << " to replace it has no momentum "
                          << ma.n << std::endl;
                show(std::cerr);
                std::exit(1);
            }
            MomChoice newMomChoice = {ma.n, replace, direction};
            // produce a relabelled xGraph equivalent to *this and pinch!
            auto newbasegraph = get_base_graph();
            for(auto& conn: newbasegraph.get_connection_map())
                for(size_t ss=0; ss < conn.second.get_edges().size(); ++ss)
                    conn.second[ss].reset_momentum_routing(false);

            for (auto& mb : momenta_assigned) {
                if (mb.link == drop) {
                    // erase old assignment
                    auto& old_strand = newbasegraph.get_connection_map().at(mb.link.connection)[mb.link.strand];
                    auto& the_links = old_strand.get_links();
                    for(auto& l: the_links) l.drop_momentum_label();
                    auto& thestrand = newbasegraph.get_connection_map().at(newMomChoice.link.connection)[newMomChoice.link.strand];
                    if (newMomChoice.direction)
                        thestrand.set_momentum_label(newMomChoice.link.locate, int(newMomChoice.n), q0new);
                    else
                        thestrand.set_momentum_label(newMomChoice.link.locate, -int(newMomChoice.n), -q0new);
                }
                // erase old shifts
                else {
                    auto& old_strand = newbasegraph.get_connection_map().at(mb.link.connection)[mb.link.strand];
                    auto& the_link = old_strand.get_links()[mb.link.locate];
                    the_link.drop_shift();
                }
            }

            xGraph topinch(newbasegraph);
            return topinch.pinch(drop, false);
        }

    // in general, removal of Link straightforward, but we need to treat special cases
    if ( count_strands == 1 && get_n_loops() != 1) {
        std::cerr<<"ERROR xGraph::pinch(.): investigate special case!"<<std::endl;
        std::exit(1);
        // dropping tadpole, irrelevant
        return xGraph();
    }
    else {
        // merges nodes
        if ( count_beads == 0 ){
            size_t old_node1 = drop.connection.first;
            size_t old_node2 = drop.connection.second;
            // dropping propagator in a 'flower' Node
            if( old_node1 == old_node2 ) {
                std::cerr << "ERROR: xGraph::pinch(.) flower pinch" << std::endl;
                std::exit(1);
            }
            std::vector<Leg> tc_node = newnodes[old_node1].get_legs();
            tc_node.insert(tc_node.end(), newnodes[old_node2].get_legs().begin(), newnodes[old_node2].get_legs().end());
            newnodes.clear();
            for(size_t ii = 0; ii < base_graph.get_nodes().size(); ii++){
                if( ii == old_node1 ) {
                    newnodes.push_back( Node(tc_node) );
                    newnodes.back().coups = _misc::add_couplings(base_graph.get_nodes()[old_node1].coups, base_graph.get_nodes()[old_node2].coups);
                }
                else if( ii == old_node2 )
                    continue;
                else
                    newnodes.push_back( base_graph.get_nodes()[ii] );
            }
            // produce connection map
            std::map<std::pair<size_t, size_t>, Connection<Strand>> cmtc;
            for ( auto& in : newconnections ){
                if(in.second.get_edges().size() == 0)    continue;
                _misc::fill_in_map({ old_node1, old_node2 }, in.first, drop.strand, in.second, cmtc);
            }

            // clear momentum routings
            for(auto& cc : cmtc){
                for(size_t kk = 0; kk < cc.second.get_edges().size(); kk++)
                    cc.second[kk].drop_routings();
            }

            Graph<Node, Connection<Strand>> graph( newnodes, cmtc );
            xGraph toreturn {graph};
            if(erase_shifts)
                toreturn.drop_shifts();
            return toreturn;
        }
        // extreme pinch
        else if ( drop.locate == 0 || drop.locate == count_beads ){
            size_t nodetoincrease;
            if ( drop.locate == 0 ) nodetoincrease = drop.connection.first;
            else nodetoincrease = drop.connection.second;
            auto pinchstrand = thestrand.pinch( drop.locate );
            auto newstrand = std::get<Strand>( pinchstrand );

            auto leftlegs = std::get<std::vector<Leg>>( pinchstrand ); 
            leftlegs.insert(leftlegs.end(), newnodes[nodetoincrease].get_legs().begin(), newnodes[nodetoincrease].get_legs().end());
            auto ncoups = newnodes[nodetoincrease].coups;
            newnodes[nodetoincrease] = Node(leftlegs);

            auto bcoups = thestrand.get_bead(0).coups;
            if(drop.locate == count_beads)
                bcoups = thestrand.get_beads().back().coups;
            newnodes[nodetoincrease].coups = _misc::add_couplings(ncoups, bcoups);

            auto ledges = newconnections.at( drop.connection ).get_edges();
            ledges[drop.strand] = newstrand;
            // produce connection map
            std::map<std::pair<size_t, size_t>, Connection<Strand>> cmtc;
            for(auto& lconn : newconnections){
                 if( lconn.first == drop.connection)
                     cmtc[lconn.first] = Connection<Strand>(ledges);
                 else
                     cmtc[lconn.first] = lconn.second;
            }

            // clear momentum routings
            for(auto& cc : cmtc){
                for(size_t kk = 0; kk < cc.second.get_edges().size(); kk++)
                    cc.second[kk].drop_routings();
            }
            
            Graph<Node, Connection<Strand>> graph( newnodes, cmtc );
            xGraph toreturn {graph};
            if(erase_shifts)
                toreturn.drop_shifts();
            return toreturn;
        }
        // normal pinch
        else {
            auto pinchstrand = thestrand.pinch( drop.locate );
            auto newstrand = std::get<Strand>( pinchstrand );

            auto ledges = newconnections.at( drop.connection ).get_edges();
            ledges[drop.strand] = newstrand;
            // produce connection map
            std::map<std::pair<size_t, size_t>, Connection<Strand>> cmtc;
            for(auto& lconn : newconnections){
                 if( lconn.first == drop.connection)
                     cmtc[lconn.first] = Connection<Strand>(ledges);
                 else
                     cmtc[lconn.first] = lconn.second;
            }
 
            // clear momentum routings
            for(auto& cc : cmtc){
                for(size_t kk = 0; kk < cc.second.get_edges().size(); kk++)
                    cc.second[kk].drop_routings();
            }
            
            Graph<Node, Connection<Strand>> graph( newnodes, cmtc );
            xGraph toreturn {graph};
            if(erase_shifts)
                toreturn.drop_shifts();
            return toreturn;
        }
    }

    std::cerr<<"ERROR xGraph::pinch(.): should not reach end"<<std::endl;
    std::exit(1);
    return xGraph();
}

std::vector<Propagator> xGraph::get_propagators() const {
    std::vector<Propagator> toret;

    for(auto& conn: base_graph.get_connection_map()){
        for(auto& strand: conn.second.get_edges()){
            auto& routings = strand.get_routings();
            auto& links = strand.get_links();
            assert(routings.size() == links.size());
            for(size_t ii=0; ii<routings.size(); ii++)
                toret.push_back( { routings[ii], links[ii].get_mass_index() } );
        }
    }

    return toret;
}

std::string xGraph::get_propagators_string() const {
    std::string tr = "Propagators[";
    auto ps = get_propagators();
    for (size_t ii = 0; ii < ps.size(); ii++) {
        auto& p = ps[ii];
        tr += p.get_prop_string();
        if (ii + 1 < ps.size()) tr += ",";
    }
    tr += "]";
    return tr;
}

Propagator xGraph::get_propagator(const AccessLink& l) const {
    
    auto& strand = ( base_graph.get_connection_map().at( l.connection ) )[l.strand];
    auto routing = strand.get_routings()[l.locate];
    auto mass = strand.get_links()[l.locate].get_mass_index();

    return Propagator{ routing, mass };
}

const MomentumRouting& xGraph::get_shift(const AccessLink& l) const {
    
    auto& strand = ( base_graph.get_connection_map().at( l.connection ) )[l.strand];
    auto& link = strand.get_links()[l.locate];

    if (!link.tracks_momentum()){
        std::cerr<<"ERROR: called xGraph::get_shift("<< l <<") when link has no assigned momentum! Corresponding xGraph:"<<std::endl;
        show(std::cerr);
        std::exit(1);
    }

    return link.get_loop_momentum().get_shift();
}

void xGraph::drop_shifts() {
    for(auto& mchoice: loop_momenta.internal_momentum_choice) {
        auto& strand = ( base_graph.get_connection_map().at( mchoice.link.connection ) )[mchoice.link.strand];
        auto& link = strand.get_links()[mchoice.link.locate];    
        link.drop_shift();
    }
}

const MomentumRouting& xGraph::get_momentum_routing(const AccessLink& l) const {
    
    auto& strand = ( base_graph.get_connection_map().at( l.connection ) )[l.strand];

    if (!strand.momemtum_traced()){
        std::cerr<<"ERROR: called xGraph::get_momentum_routing("<< l <<") when strand doesn't traced momentum! Corresponding xGraph:"<<std::endl;
        show(std::cerr);
        std::exit(1);
    }

    return strand.get_routings()[l.locate];
}

std::string xGraph::get_MGraphString() const {

    // a bunch of streams to be filled with mathematica strings
    std::stringstream s;
    std::stringstream edge_stream;
    std::stringstream mom_mass_stream;
    // external data is to be put last in all strings
    std::stringstream edge_stream_external;
    std::stringstream mom_mass_stream_external;
    
    auto nodes = base_graph.get_nodes();

    // some bools to print commata at correct positions
    bool first_internal = true;
    bool first_external = true;
    bool first_mom_mass = true;

    edge_stream <<"{";
    mom_mass_stream <<"{";

    /****************************************************     
    vertex numbering:
    -n_ext,...,-1 corresponds to external momenta n_ext,...,1 .
    0,...,n_nodes-1 are internal nodes. 
    n_nodes,...,n_vertices-1 are internal vertices where an external momentum enters.
    ***************************************************/

    size_t vcnt = n_nodes;

    for (auto& it: base_graph.get_connection_map()){
        auto from = it.first.first;
        auto to = it.first.second;
        auto con = it.second;
        for (auto& strand: con.get_edges()){
            auto& routings = strand.get_routings();            
            auto& links = strand.get_links();
            assert(routings.size() == links.size());
            if(!first_internal){
                edge_stream <<",";
            }
            first_internal=false;
            edge_stream << "DirectedEdge["<< std::to_string(from);
            for(size_t ii=0; ii<strand.size(); ii++){
                vcnt++;
                edge_stream << ","<< std::to_string(vcnt) << "]";
                for(auto& leg: strand.get_bead(ii).get_legs()){
                    if(!first_external){
                        edge_stream_external <<",";
                    }
                    first_external=false;
                    edge_stream_external << "DirectedEdge[-" << std::to_string(leg.get_momentum_index()) << "," <<   std::to_string(vcnt) << "]";
                    mom_mass_stream_external << ",{-k" << leg.get_momentum_index();
                    if(!leg.is_on_shell()){ // for off shell legs we put sqrt(p^2) as 'mass'
                        mom_mass_stream_external <<",Sqrt[("<< leg.get_momentum_index() <<")^2]}";
                    }
                    else{
                        if(leg.get_mass_index() == 0){
                            mom_mass_stream_external << ",0}";
                        }
                        else{
                            mom_mass_stream_external << ",m"<<leg.get_mass_index() <<"}";
                        }  
                    }
         
                }
                edge_stream <<",DirectedEdge["<< std::to_string(vcnt);
            }
            edge_stream <<","<< std::to_string(to) << "]";
            for(size_t ii=0; ii<routings.size(); ii++){
                if(!first_mom_mass){
                    mom_mass_stream <<",";
                }
                first_mom_mass=false;
                mom_mass_stream << "{"<< routings[ii];
                if(links[ii].is_massless_propagator()){
                    mom_mass_stream << ",0}";
                }
                else{
                    mom_mass_stream << ",m"<<links[ii].get_mass_index() <<"}";
                }
                
            }
        }
    }

    // add node data
    for (size_t ii=0; ii< base_graph.get_n_nodes(); ii++){
        auto the_node = base_graph.get_nodes().at(ii);
        for(auto& leg: the_node.get_legs()){
            if(!first_external){
                edge_stream_external <<",";
            }
            first_external=false;
            edge_stream_external << "DirectedEdge[-" << std::to_string(leg.get_momentum_index()) << "," <<   std::to_string(ii) << "]";
            mom_mass_stream_external << ",{-k" << leg.get_momentum_index();
            if(!leg.is_on_shell()){ // for off shell legs we put sqrt(p^2) as 'mass'
                mom_mass_stream_external <<",Sqrt[("<< leg.get_momentum_index() <<")^2]}";
            }
            else{
                if(leg.get_mass_index() == 0){
                    mom_mass_stream_external << ",0}";
                }
                else{
                    mom_mass_stream_external << ",m"<<leg.get_mass_index() <<"}";
                }  
            }

        }
    }
    

    // append the external data
    edge_stream << ","<< edge_stream_external.str();
    mom_mass_stream << mom_mass_stream_external.str();

    edge_stream <<"}";
    mom_mass_stream <<"}";
    
    // put everything in a Mathematica association
    s << "Association[";
    s << "\"edges\" -> " << edge_stream.str() <<",";
    s << "\"momentumAndMass\" -> " << mom_mass_stream.str() ;
    s <<"]"; 

    return s.str();
}

std::vector<size_t> xGraph::onshell_indices() const {
    std::set<size_t> lindices;
    // all onshell indices in legs
    for (auto& l : legs)
        if (l.is_on_shell()) lindices.insert(l.get_mass_index());
    // all masses in links
    for (auto& [pair, conn] : base_graph.get_connection_map())
        for (auto& strand : conn.get_edges())
            for (auto& link : strand.get_links()) lindices.insert(link.get_mass_index());
    std::vector<size_t> toret;
    toret.insert(toret.end(), lindices.begin(), lindices.end());
    return toret;
}

namespace _misc {

Strand _get_strand_mass_permute(const Strand& strand, const std::map<size_t, size_t>& m) {
    std::vector<Bead> tcbeads;
    for(auto& bead: strand.get_beads()) {
        if (bead.is_on_shell()) {
            auto llegs = bead.get_legs();
            for (auto& l : llegs) {
                if (l.is_on_shell()) {
                    if (l.get_mass_index() == 0)
                        // no map
                        l = Leg(l.is_on_shell(), 0, l.get_momentum_index());
                    else
                        // if on-shell leg, map mass index according to 'm'
                        l = Leg(l.is_on_shell(), m.at(l.get_mass_index()), l.get_momentum_index());
                }
            }
            tcbeads.push_back(Bead{llegs});
        }
        else
            tcbeads.push_back(bead);
    }
    std::vector<Link> tclinks;
    for(auto& link: strand.get_links()) {
        if(link.is_massless_propagator())
            tclinks.push_back(link);
        else
            tclinks.push_back(Link(link.get_loop_momentum(), m.at(link.get_mass_index())));
    }
    Strand toret{tcbeads, tclinks};
    toret.assign_permutation_tracking_label(strand.get_permutation_tracking_label());
    return toret;
}

Connection<Strand> _get_connection_mass_permute(const Connection<Strand>& conn, const std::map<size_t, size_t>& m) {
    std::vector<Strand> toconst;
    for(auto& strand: conn.get_edges()) {
        toconst.push_back(_get_strand_mass_permute(strand, m));
    }
    return Connection<Strand>{toconst};
}

Graph<Node, Connection<Strand>> _get_base_graph_mass_permute(const Graph<Node, Connection<Strand>>& bg, const std::map<size_t, size_t>& m) {
    std::vector<Node> new_nodes;
    for(auto& node: bg.get_nodes()) {
        if (node.is_on_shell()) {
            auto llegs = node.get_legs();
            for (auto& l : llegs) {
                if (l.is_on_shell()) {
                    if (l.get_mass_index() == 0)
                        // no map
                        l = Leg(l.is_on_shell(), 0, l.get_momentum_index());
                    else
                        // if on-shell leg, map mass index according to 'm'
                        l = Leg(l.is_on_shell(), m.at(l.get_mass_index()), l.get_momentum_index());
                }
            }
            new_nodes.push_back(Node{llegs});
        }
        else
            new_nodes.push_back(node);
        new_nodes.back().assign_permutation_tracking_label(node.get_permutation_tracking_label());
    }
    std::map<std::pair<size_t, size_t>, Connection<Strand>> new_connection_map;
    for(auto& [pair, conn]: bg.get_connection_map()) {
        new_connection_map[pair] = _get_connection_mass_permute(conn, m);
    }
    return Graph<Node, Connection<Strand>>(new_nodes, new_connection_map);
}

}

Graph<Node, Connection<Strand>> xGraph::canonical_topology(const Graph<Node, Connection<Strand>>& bg, const std::vector<size_t>& aindices, std::map<size_t, size_t>& cimap) {
    auto indices = aindices;
    assert(indices.size() > 0);

    std::sort(indices.begin(), indices.end());
    auto start = indices.begin();
    // do not permute zero index
    if(indices[0] == 0) ++start; 
    auto toret = bg.filter();
    bool first(true);
    do {
        // masses in topology will go either from 0 to n-1 or from 1 to n (0 is always special, not permuted!)
        std::map<size_t, size_t> canonical_indices;  // map: {given index} ---> {canonical index}
        size_t value(0);
        if(indices[0] > 0)
            value++;
        for(size_t ii = 0; ii < indices.size(); ++ii) canonical_indices[indices[ii]] = value++;
        auto lbg = _misc::_get_base_graph_mass_permute(bg, canonical_indices);
        auto tocompare = lbg.filter();
        if(first) {
            toret = tocompare;
            for(auto& [i, imap]: canonical_indices) cimap.at(i) = imap;
            first = false;
        }
        if (tocompare < toret) {
            toret = tocompare;
            for(auto& [i, imap]: canonical_indices) cimap.at(i) = imap;
        }
    } while(std::next_permutation(start, indices.end()));
    return toret;
}

std::vector<xGraphPermute> xGraph::get_automorphisms() const {
    using xGtype = Graph<Node, Connection<Strand>>;

    static thread_local std::unordered_map<xGtype, std::vector<xGraphPermute>> cache_automorphisms;
    if(cache_automorphisms.find(topology) != cache_automorphisms.end())
        return cache_automorphisms.at(topology);

    if (get_n_loops() != 1) std::cerr << "WARNING: incomplete xGraph::get_automorphisms()!" << std::endl;

    std::vector<xGraphPermute> toret;
    static const xGraph bubble = xGtype("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]]]]");
    if(bubble.get_topology() == topology) {
        toret.push_back( {{0, 0}, {0}} );
    }

    cache_automorphisms[topology] = toret;
    return toret;
}

void map_legs(const std::vector<Leg>& inlegs, std::vector<Leg>& outlegs) {
    for (const auto& il : inlegs) {
        // we id by momentum index
        size_t lil = il.get_momentum_index();
        auto match_mi = [lil](const Leg& ol) { return ol.get_momentum_index() == lil; };
        auto it = std::find_if(outlegs.begin(), outlegs.end(), match_mi);
        if (it == outlegs.end()) {
            std::cerr << "ERROR: map_legs(.) did not find match! " << il << " " << outlegs << std::endl;
            std::exit(1);
        }
        it->ptype = il.ptype;
    }
}

namespace _misc {

void print_legs(const std::vector<Leg>& il) {
    for (size_t ii = 0; ii < il.size(); ii++) {
        auto& l = il[ii];
        std::cout << "{" << l.get_momentum_index() << ", " << l.get_mass_index() << ", " << l.ptype << "}";
        if (ii + 1 < il.size()) std::cout << ", ";
    }
}

}

void xGraph::map_fields(const xGraph& in) {
    // sanity check
    if (!(in.base_graph == base_graph)) {
        std::cerr << "ERROR: map_fields(.) got inequivalent xGraphs!" << std::endl;
        show(std::cerr);
        in.show(std::cerr);
        std::exit(1);
    }
    if (!in.fields_defined()) return;
    var_fields_defined = true;
    // traverse all base_graph
    const auto& innodes = in.base_graph.get_nodes();
    for (size_t ii = 0; ii < innodes.size(); ii++) { 
        base_graph[ii].coups = innodes[ii].coups;
        map_legs(innodes[ii].get_legs(), base_graph[ii].get_legs());
    }
    const auto& incmap = in.base_graph.get_connection_map();
    auto& thiscmap = base_graph.get_connection_map();
    for (const auto& tincon : incmap) {
        const auto& incon = tincon.second;
        auto& thiscon = thiscmap.at(tincon.first);
        for (size_t ee = 0; ee < incon.get_edges().size(); ee++) {
            const auto& instrand = incon[ee];
            auto& thisstrand = thiscon[ee];
            // now take care of beads
            const auto& inbeads = instrand.get_beads();
            auto& thisbeads = thisstrand.get_beads();
            for (size_t bb = 0; bb < inbeads.size(); bb++) { 
                thisbeads[bb].coups = inbeads[bb].coups;
                map_legs(inbeads[bb].get_legs(), thisbeads[bb].get_legs());
            }
            // now take care of links
            const auto& inlinks = instrand.get_links();
            auto& thislinks = thisstrand.get_links();
            for (size_t ll = 0; ll < inlinks.size(); ll++) thislinks[ll].ptype = inlinks[ll].ptype;
        }
    }
    
}

void xGraph::show_fields() const {
    if (!fields_defined()) {
        std::cout << "No fields defined for xGraph " << std::endl;
        return;
    }
    // first show base_graph
    show(std::cout);
    std::cout << "============" << std::endl;
    std::cout << "Information from nodes: " << std::endl;
    std::cout << "============" << std::endl;
    for (size_t ii = 0; ii < base_graph.get_nodes().size(); ii++) {
        auto& nn = base_graph.get_nodes()[ii];
        std::cout << ii << ": " << nn.coups;
        if (nn.get_legs().size() > 0) { 
            std::cout << "; ";
            _misc::print_legs(nn.get_legs());
        }
        std::cout << std::endl;
    }
    std::cout << "============" << std::endl;
    std::cout << "Information from connections: " << std::endl;
    std::cout << "============" << std::endl;
    for (auto& conn : base_graph.get_connection_map()) {
        std::cout << "connection: " << conn.first << std::endl;
        auto thestrands = conn.second.get_edges();
        for (size_t ss = 0; ss < thestrands.size(); ss++) {
            auto strand = thestrands[ss];
            for (size_t bb = 0; bb < strand.get_beads().size(); bb++) {
                auto link = strand.get_links()[bb];
                auto bead = strand.get_beads()[bb];
                std::cout << "propagator " << bb << ": " << link.ptype.particle << std::endl;
                std::cout << bb << ": " << bead.coups;
                if (bead.get_legs().size() > 0) {
                    std::cout << "; ";
                    _misc::print_legs(bead.get_legs());
                }
                std::cout << std::endl;
            }
            auto link = strand.get_links().back();
            std::cout << "propagator " << strand.get_beads().size() << ": " << link.ptype.particle << std::endl;
        }
        std::cout << "============" << std::endl;
    }
}

void xGraph::trace_momentum_routing() {
    if (loop_momenta.active) {
        std::cout << "WARNING: called xGraph::trace_momentum_routing() when tracing was ready! Did nothing!" << std::endl;
        return;
    }

    // check that external momenta indices are 1..n
    {
        std::vector<size_t> all_mom_indices;
        for (auto& n : base_graph.get_nodes()) {
            auto inds = n.get_momentum_indices();
            all_mom_indices.insert(all_mom_indices.end(), inds.begin(), inds.end());
        }
        for (auto& cons : base_graph.get_connection_map()) {
            for (auto& str : cons.second) {
                for (auto& b : str.get_beads()) {
                    auto inds = b.get_momentum_indices();
                    all_mom_indices.insert(all_mom_indices.end(), inds.begin(), inds.end());
                }
            }
        }
        std::sort(all_mom_indices.begin(),all_mom_indices.end());
        if (!all_mom_indices.empty()) {
            for (auto&& [i, obj] : enumerate(all_mom_indices)) {
                if (i + 1 != obj) {
                    _WARNING_R("External momentum index assignments in\n", this->base_graph, "are not consecutive integers starting from 1!");
                    _WARNING("\tgot indices: ",all_mom_indices);
                    std::exit(1);
                }
            }
        }
    }

    // traverse connections in look for loop momenta
    std::vector<size_t> collect_abs_labels;
    const std::map<std::pair<size_t, size_t>, Connection<Strand>>& connections = base_graph.get_connection_map();
    for (const auto& c : connections) {
        // traverse strands
        const auto& strands = c.second.get_edges();
        for (size_t ii = 0; ii < strands.size(); ii++) {
            if (strands[ii].has_link_with_mom_label()) {
                size_t thelink = strands[ii].mom_label_link();
                int label = strands[ii].get_link(thelink).get_momentum_label();
                collect_abs_labels.push_back(size_t(std::abs(label)));
                loop_momenta.add_momentum_information(label, c.first, ii, thelink);
            }
        }
    }
    if (size_t(n_loops) != loop_momenta.size()) {
        throw std::logic_error("ERROR: xGraph::trace_momentum_routing(.) found only " + std::to_string(loop_momenta.size()) + " momenta for a " +
                               std::to_string(n_loops) + "-loop diagram!");
    }
    bool correclabels(true);
    for (size_t ii = 1; ii <= size_t(n_loops); ii++) {
        if (std::find(collect_abs_labels.begin(), collect_abs_labels.end(), ii) == collect_abs_labels.end()) {
            correclabels = false;
            break;
        }
    }
    if (!correclabels) {
        std::cerr << "ERROR: wrong momenum labels: " << collect_abs_labels << ", it should be {1, ... ," << n_loops << "}" << std::endl;
        throw std::logic_error("ERROR: xGraph::trace_momentum_routing(.) found bad momentum labels!");
    }
    // now that we have all local information of loop momenta in xGraph we order all Strand's containing them to trace their momenta
    std::map<std::pair<size_t, size_t>, Connection<Strand>>& cmap = base_graph.get_connection_map();
    for (const auto& mchoice : loop_momenta.internal_momentum_choice) {
        // ask the strand to trace its momenta
        cmap[mchoice.get_connection()][mchoice.get_strand()].trace_momentum_routing(n_legs);
    }
    // what remains is momentum conservation on base_graph's nodes, we traverse them in order, allowing a max amount of rounds, otherwise fails (might be quite
    // inneficient!)
    std::vector<size_t> nodes_to_process;
    for (size_t ii = 0; ii < base_graph.get_n_nodes(); ii++) nodes_to_process.push_back(ii);
    size_t max_rounds(6 * base_graph.get_n_nodes());
    size_t rounds_counter(0);
    size_t lnode(0);
    while (rounds_counter < max_rounds) {
        rounds_counter++;
        // all connections that include nodes_to_process[lnode]
        std::set<std::pair<size_t, size_t>> lconnections;
        for (size_t ii = 0; ii < base_graph.get_n_nodes(); ii++) {
            if (connections.find(std::make_pair(nodes_to_process[lnode], ii)) != connections.end()) {
                if (connections.at(std::make_pair(nodes_to_process[lnode], ii)).size() > 0) lconnections.insert(std::make_pair(nodes_to_process[lnode], ii));
            }
            if (connections.find(std::make_pair(ii, nodes_to_process[lnode])) != connections.end()) {
                if (connections.at(std::make_pair(ii, nodes_to_process[lnode])).size() > 0) lconnections.insert(std::make_pair(ii, nodes_to_process[lnode]));
            }
        }
        size_t missing(0);
        // to track, if there is a single missing strand, the needed connection
        std::pair<size_t, size_t> lc;
        // to track, if there is a single missing strand, the given strand in the connection
        size_t ls;
        for (auto& ccheck : lconnections) {
            for (size_t ii = 0; ii < connections.at(ccheck).size(); ii++) {
                auto thestrand = connections.at(ccheck)[ii];
                if (!thestrand.momemtum_traced()) {
                    // this strand does not have momentum traced
                    missing++;
                    lc = ccheck;
                    ls = ii;
                }
            }
        }
        if (missing == 0) {
            // maybe is good to make a more comprehensive check of this case? for now assume things are consistent, in any case if not, trouble will appear
            // elsewhere in the graph
            nodes_to_process.erase(nodes_to_process.begin() + lnode);
            if (nodes_to_process.size() == 0)
                break; // while loop
            else {
                lnode = lnode % nodes_to_process.size();
                continue;
            }
        }
        else if (missing > 1) {
            // continue with next node
            lnode++;
            lnode = lnode % nodes_to_process.size();
            continue;
        }
        // missing == 1
        else {
            // gather the MomentumRouting that starts the link in the missing strand
            MomentumRouting mr;
            for (auto& ccheck : lconnections) {
                for (size_t ii = 0; ii < connections.at(ccheck).size(); ii++) {
                    auto thestrand = connections.at(ccheck)[ii];
                    if (thestrand.momemtum_traced()) {
                        if (ccheck.first == nodes_to_process[lnode])
                            // we need the first entry
                            mr = mr - thestrand.get_routings()[0];
                        else
                            // we need the back entry
                            mr = mr + thestrand.get_routings().back();
                    }
                }
            }
            // absorb momenta in the legs of the node
            mr = mr - MomentumRouting(vsizet2vint(base_graph.get_nodes()[nodes_to_process[lnode]].get_momentum_indices()));
            // in this case we need to inver flow
            if (lc.second == nodes_to_process[lnode]) mr = -mr;
            // and trace the missing strand
            auto& themissingstrand = cmap[lc][ls];
            themissingstrand.trace_momentum_routing(n_legs, mr);
            // remove node
            nodes_to_process.erase(nodes_to_process.begin() + lnode);
            if (nodes_to_process.size() == 0)
                break; // while loop
            else {
                lnode = lnode % nodes_to_process.size();
                continue;
            }
        }
    }
    // not all nodes processed!
    if (nodes_to_process.size() > 0) {
        //std::cerr << "ERROR: application of momentum conservation in the following base_graph failed after " << rounds_counter << "(" << max_rounds
        //          << ") attempts!" << std::endl;
        //base_graph.show();
        throw std::logic_error("ERROR: momentum conservation failure in xGraph::trace_momentum_routing(.)");
    }
    // all done!
    loop_momenta.active = true;
}

std::vector<MomentumRouting> xGraph::map_momenta_in_links(const std::vector<MomChoice>& in) const {
    if (!momemtum_traced()) {
        std::cerr << "ERROR: to call xGraph::map_momenta_in_links(.) you need to trace_momentum_routing()!" << std::endl;
        show();
        throw std::logic_error("ERROR: wrong call to map_momenta_in_links(.)");
    }
    std::vector<MomentumRouting> toret;
    for (const auto& mchoice : in) {
        const auto& connection = base_graph.get_connection_map().at(mchoice.get_connection());
        const auto& strand = connection[mchoice.get_strand()];
        MomentumRouting lmr = strand.get_routings()[mchoice.link.locate];
        if (!mchoice.direction)
            // in this case, invert!
            lmr = -lmr;
        toret.push_back(lmr);
    }
    return toret;
}

size_t xGraph::dimension_common_transverse() const {
    size_t dim(0);
    if (topology.get_n_legs() < 5) dim = 5 - topology.get_n_legs();
    return dim;
}

std::vector<size_t> xGraph::transverse_space_dimensions() const {
    if (!momemtum_traced()) {
        std::cerr << "ERROR: called xGraph::transverse_space_dimensions() for an xGraph without tracing!" << std::endl;
        show();
        std::exit(8871635);
    }
    size_t n_points = topology.get_n_legs();

    std::vector<size_t> toret;

    for (const auto& mc : loop_momenta.internal_momentum_choice) {
        const auto& theconnection = base_graph.get_connection_map().at(mc.get_connection());
        const auto& thestrand = theconnection[mc.get_strand()];
        size_t ln_legs = thestrand.get_beads().size();

        size_t dim(0);
        if (ln_legs < 4) dim = 4 - ln_legs;
        // if all legs in this strand, apply mom conservation
        if (ln_legs == n_points && ln_legs < 5) dim++;
        // FIXME: shall we check what happens when ln_legs == 1 and is massless?
        toret.push_back(dim);
    }

    return toret;
}

std::vector<size_t> xGraph::n_OS_variables_with_special_routing() const {
    if (!momemtum_traced()) {
        std::cerr << "ERROR: called xGraph::n_OS_variables_with_special_routing() for an xGraph without tracing!" << std::endl;
        show();
        std::exit(1);
    }
    return get_special_routing().n_OS_vars_helper();
}

std::vector<size_t> xGraph::n_OS_vars_helper() const {
    if (n_loops > 2) {
        std::cerr << "ERROR: called xGraph::n_OS_vars_helper() for a " << n_loops << "-loops xGraph!" << std::endl;
        show();
        std::exit(1);
    }
    size_t n_points = topology.get_n_legs();

    std::vector<size_t> toret;

    for (const auto& mc : loop_momenta.internal_momentum_choice) {
        const auto& theconnection = base_graph.get_connection_map().at(mc.get_connection());
        const auto& thestrand = theconnection[mc.get_strand()];
        size_t ln_legs = thestrand.get_beads().size();

        size_t dim(0);
        if (ln_legs < 4) dim = 4 - ln_legs;
        // if all legs in this strand, apply mom conservation
        if (ln_legs == n_points && ln_legs < 5) dim++;
        // FIXME: shall we check what happens when ln_legs == 1 and is massless?
        toret.push_back(dim);
    }
    if (!is_planar()) {
        size_t count_nonplanar_beads(0);
        for (auto& [pair, conn] : base_graph.get_connection_map())
            for (auto& strand : conn.get_edges())
                if (!strand.has_link_with_mom_label()) count_nonplanar_beads = strand.size();
        // For non-planar graphs there are additional on-shell conditions
        // in the central rung. We enforce this condition onto l2 therefore
        // the number of free variables in the l2 strand is reduced
        assert(count_nonplanar_beads <= toret.back());
        toret.back() = toret.back() - count_nonplanar_beads;
    }
    // we add a variable to l2 for factorizable cases
    else if (n_loops == 2 && base_graph.is_factorizable())
        ++toret.back();

    return toret;
}

bool xGraph::has_massive_link() const {
    for (const auto& con : base_graph.get_connection_map())
        for (const auto& strand : con.second.get_edges())
            for (const auto& link : strand.get_links())
                if (!link.is_massless_propagator()) return true;
    return false;
}


int xGraph::attached_node(size_t loop) const {
    if (!loop_momenta.active) {
        std::cerr << "ERROR: call to xGraph::attached_node(" << loop << ") when there is no trace_momentum_routing()!" << std::endl;
        show();
        std::exit(5454441);
    }
    if (loop >= loop_momenta.internal_momentum_choice.size()) {
        std::cerr << "ERROR: call to xGraph::attached_node(" << loop << ") when there is only " << loop_momenta.internal_momentum_choice.size()
                  << " registered momenta!" << std::endl;
        show();
        std::exit(5454442);
    }
    const auto& mc = loop_momenta.internal_momentum_choice[loop];
    // graph components
    const auto& theconnection = base_graph.get_connection_map().at(mc.get_connection());
    const auto& thestrand = theconnection[mc.get_strand()];
    size_t n_beads = thestrand.get_beads().size();

    // check that we are attached to a node
    if (mc.link.locate != 0 && mc.link.locate != n_beads)
        // we return a negative value, which is a fail
        return -1;

    if (mc.link.locate == 0) { return int(mc.get_connection().first); }
    else
        return int(mc.get_connection().second);
}

/*
 * Conversion from old IBP input info to new graph structure
 */

namespace detail{
    Leg get_Leg(std::string&& s){
        MathList n(std::move(s), "Leg");
        switch (n.size()) {
            case 1: return Leg{std::stoul(n[0])};
            case 2: return Leg{std::stoul(n[0]), std::stoul(n[1])};
            case 3: { 
                auto toret = Leg{std::stoul(n[0]), std::stoul(n[1])};
                MathList pt(n[2], "PType");
                toret.ptype = {pt[0], pt[1]}; 
                return toret;
            }
        }
        _WARNING_R("Do not understand ", n);
        std::exit(1);
    }

    using couplings_t = std::map<std::string,int>;
    couplings_t get_Couplings(std::string&& s) {
        MathList cl(std::move(s), "Couplings");
        couplings_t toret;
        for (auto c : cl) {
            MathList cm(c, "Coupling");
            // should have coupling name and power
            assert(cm.tail.size() == 2);
            auto it = toret.find(cm[0]);
            if (it != toret.end()) {
                std::cerr << "ERROR: got repeated coupling in: " << cl << std::endl;
                std::exit(1);
            }
            toret[cm[0]] = std::stoul(cm[1]);
        }
        return toret;
    }

    std::vector<Node> get_vector_of_nodes(std::string&& input){
        auto get_Node = [](std::string&& s) -> Node {
            MathList n(s);
            couplings_t coups;
            if (n.head == "Leg")
                return Node{get_Leg(std::move(s))};
            else if (n.head == "Node") {
                std::vector<Leg> legs;
                for (auto&& it : n) { 
                    MathList ll(it);
                    if (ll.head == "Leg")
                        legs.push_back(get_Leg(std::move(it))); 
                    else if (ll.head == "Couplings")
                        coups = get_Couplings(std::move(it));
                    else {
                        std::cerr << "ERROR: should get Leg or Couplings, got " << ll.head << std::endl;
                        std::exit(1);
                    }
                }
                auto toret = Node{legs};
                toret.coups = coups;
                return toret;
            }
            _WARNING_R("Do not understand ", n);
            std::exit(1);
        };
        std::vector<Node> nodes;
        for (auto&& it : MathList(std::move(input), "Nodes")) { nodes.push_back(get_Node(std::move(it))); }

        return nodes;
    }

    std::vector<Strand> get_vector_of_strands(std::string&& input) {

        auto get_Bead = [](std::string&& s) -> Bead {
            MathList n(s);
            couplings_t coups;
            if (n.head == "Leg")
                return Bead{get_Leg(std::move(s))};
            else if (n.head == "Bead") {
                std::vector<Leg> legs;
                for (auto&& it : n) { 
                    MathList ll(it);
                    if (ll.head == "Leg")
                        legs.push_back(get_Leg(std::move(it))); 
                    else if (ll.head == "Couplings")
                        coups = get_Couplings(std::move(it));
                    else {
                        std::cerr << "ERROR: should get Leg or Couplings, got " << ll.head << std::endl;
                        std::exit(1);
                    }
                }
                auto toret = Bead{legs};
                toret.coups = coups;
                return toret;
            }
            _WARNING_R("Do not understand ", n);
            std::exit(1);
        };

        auto get_Link = [](std::string&& s) -> Link {
            MathList n(s);
            if(n.head== "LoopMomentum"){
                return LoopMomentum{std::stoi(n[0])};
            }

            assert_head(n,"Link");
            Field lfield{"", ""};
            bool hfield(false);

            if (n.size() == 0) return {};
            else if (n.size()>1) {
                try {
                    MathList field(n[n.size()-1], "Field");
                    assert(field.size()==2);
                    lfield.particle = field[0];
                    lfield.antiparticle = field[1];
                    hfield = true;
                }
                catch (...) {}
            }

            try {
                size_t mind = std::stoul(n[0]);
                auto toret = Link{mind};
                toret.ptype = lfield;
                return toret;
            }
            catch (std::invalid_argument&) {
                MathList lm(n[0], "LoopMomentum");
                size_t count(2);
                if (hfield) count = 3;
                if (n.size() == count) {
                    auto toret = Link{LoopMomentum{std::stoi(lm[0])}, std::stoul(n[1])};
                    toret.ptype = lfield;
                    return toret;
                }
                else {
                    auto toret = Link{LoopMomentum{std::stoi(lm[0])}};
                    toret.ptype = lfield;
                    return toret;
                }
            }

            _WARNING_R("Do not understand ", n);
            std::exit(1);
        };

        auto get_Strand = [get_Link,get_Bead](std::string&& s) -> Strand {
            MathList n(s, "Strand");
            std::vector<Link> links;
            std::vector<Bead> beads;

            if (n.size() == 0) return {};

            bool link_or_bead = true;

            for (auto&& it : n) {
                if (link_or_bead)
                    links.push_back(get_Link(std::move(it)));
                else
                    beads.push_back(get_Bead(std::move(it)));
                link_or_bead = !link_or_bead;
            }

            return {beads, links};
        };

        std::vector<Strand> strands;

        for (auto&& it : MathList(std::move(input), "Connection")) { strands.push_back(get_Strand(std::move(it))); }

        return strands;
    }
}

template <typename NodeType, typename LinkType>
template <typename NT, typename LT>
Graph<NodeType, LinkType>::Graph(MathList input, typename std::enable_if_t<std::is_same<NT, Node>::value>*,
                                 typename std::enable_if_t<std::is_same<LT, Connection<Strand>>::value>*)
    : Graph{detail::get_vector_of_nodes(input[0]), {detail::get_vector_of_strands(input[1])}} {}

// instantiate explicitely the above construct with the only type it is supposed to work
template Graph<Node, Connection<Strand>>::Graph(MathList, void*, void*);

std::ostream& operator<<(std::ostream& outstr, const Strand& strand) {
    outstr << "Strand[";
    if(strand.get_permutation_tracking_label()>-1)
        outstr<<strand.get_permutation_tracking_label()<<",";
    outstr << strand.get_beads();
    outstr << ", ";
    outstr << strand.get_link_mass_indices();
    outstr << "]";
    return outstr;
}

size_t xGraph::get_span_dim() const {
    size_t dim = 0;
    for (auto& it : base_graph.get_connection_map()) {
        auto con = it.second;
        for (size_t ii = 0; ii < con.size(); ii++) { dim += con[ii].size(); }
    }
    for (auto& n : base_graph.get_nodes()) {
        if (n.size() != 0) { dim++; }
    }
    if (dim > 0) {
        //
        dim--;
    }
    if (dim > 4) { return 4; }
    return dim;
}

std::vector<std::vector<bool>> get_link_pinched(std::vector<std::vector<bool>> matr, size_t i, size_t j) {
    std::cout << "pinching link " << i << ":" << j << std::endl;
    auto toret = matr;
    std::cout << "matr" << std::endl;
    for (auto const& r : matr) {
        for (auto const& c : r) { std::cout << c; }
        std::cout << std::endl;
    }

    for (size_t ii = 0; ii < matr.size(); ii++) {
        toret[ii][i] = (toret[ii][i] || toret[ii][j]);
        toret[i][ii] = (toret[i][ii] || toret[j][ii]);
    }
    toret.erase(toret.begin() + j);
    for (auto& v : toret) { v.erase(v.begin() + i); }
    for (auto const& r : toret) {
        for (auto const& c : r) { std::cout << c; }
        std::cout << std::endl;
    }
    return toret;
}

std::vector<std::vector<bool>> get_link_dropped(std::vector<std::vector<bool>>& matr, size_t i, size_t j) {
    std::cout << "dropping link " << i << ":" << j << std::endl;
    auto toret = matr;
    toret[i][j] = false;
    toret[j][i] = false;
    return toret;
}

std::vector<std::vector<bool>> get_vertex_dropped(std::vector<std::vector<bool>>& matr, size_t i) {
    std::cout << "dropping vertex " << i << std::endl;
    auto toret = matr;
    toret.erase(toret.begin() + i);
    for (auto& v : toret) { v.erase(v.begin() + i); }

    for (auto const& r : toret) {
        for (auto const& c : r) { std::cout << c; }
        std::cout << std::endl;
    };
    return toret;
}

bool is_K33_related(std::vector<std::vector<bool>>& matr) {
    std::cout << "checking isomorpy to K33" << std::endl;
    if (matr.size() != 6) { std::cerr << "ERROR: matrix size must be 6" << std::endl; }
    for (size_t ii = 0; ii < 6; ii++) {
        for (size_t jj = 0; jj < 6; jj++) {
            if (ii < 3 && jj < 3 && matr[ii][jj]) return false;
            if (ii > 3 && jj > 3 && matr[ii][jj]) return false;
            if (ii < 3 && jj > 3 && (!matr[ii][jj] || !matr[jj][ii])) return false;
        }
    }
    std::cout << "found K33" << std::endl;
    return true;
}

bool is_K5_related(std::vector<std::vector<bool>>& matr) {
    std::cout << "checking isomorpy to K5" << std::endl;
    if (matr.size() != 5) { std::cerr << "ERROR: matrix size must be 6" << std::endl; }
    for (size_t ii = 0; ii < 5; ii++) {
        for (size_t jj = ii + 1; jj < 5; jj++) {
            if (!matr[ii][jj] || !matr[jj][ii]) return false;
        }
    }
    for (size_t ii = 0; ii < 5; ii++) {
        if (matr[ii][ii]) return false;
    }
    std::cout << "found K5" << std::endl;
    return true;
}

bool is_planar_adjacency(std::vector<std::vector<bool>> matr) {
    std::cout << "here" << std::endl;
    auto n = matr.size();
    std::cout << "here" << std::endl;
    _PRINT(n);
    if (n < 5) { return true; }
    if (n == 5) {
        if (is_K5_related(matr)) { return false; }
    }
    if (n == 6) {
        if (is_K33_related(matr)) { return false; }
    }
    // for all vertices
    for (size_t ii = 0; ii < matr.size(); ii++) {
        for (size_t jj = ii + 1; jj < matr.size(); jj++) {
            if (matr[ii][jj]) {
                // if (!is_planar_adjacency(get_link_pinched(matr, ii, jj))) { return false; }
                auto dropped = get_link_dropped(matr, ii, jj);
                if (!is_planar_adjacency(get_link_dropped(matr, ii, jj))) { return false; }
            }
        }
    }
    for (size_t ii = 0; ii < matr.size(); ii++) {
        if (!is_planar_adjacency(get_vertex_dropped(matr, ii))) { return false; }
    }
    return true;
}

bool xGraph::is_planar() const {
    // Eulers criterion is neccesary condition e <= 3*v-6 for a graph to be planar
    //  if (n_edges > (3 * n_vertices - 6) && n_vertices > 2) { return false; }

    if(n_loops<2) return true;
    if(n_loops>2) {
        std::cout << "xGraph::is_planar() has only a two-loop logic for now. Requested "<<n_loops << " loops. Return false"<<std::endl;
        return false;
    }
    if(base_graph.is_factorizable()) return true;

    auto the_connection = base_graph.get_connection(0,1);
    for(auto& strand:the_connection){
        if(strand.is_plain()) return true; 
    }
    return false;

    //FIXME implement the proper logic to determine non-planarity at higher loops

    std::vector<std::vector<bool>> matrix(n_vertices + 1, (std::vector<bool>(n_vertices + 1)));
    for (size_t ii = 0; ii < n_nodes; ii++) {
        for (size_t jj = 0; jj < n_nodes; jj++) { matrix[ii][jj] = (base_graph.get_adjacency()[ii][jj] > 0); }
    }
    size_t index = n_nodes;
    for (auto& it : base_graph.get_connection_map()) {
        auto ii = it.first.first;
        auto jj = it.first.second;
        auto con = it.second;
        bool connected = false;
        for (size_t kk = 0; kk < con.size(); kk++) {
            if (con[kk].size() == 0) { connected = true; }
            else {
                for (size_t ll = 0; ll < con[kk].size(); ll++) {
                    if (ll == 0) {
                        matrix[ii][index] = true;
                        matrix[index][ii] = true;
                    }
                    if (ll == con[kk].size() - 1) {
                        matrix[jj][index] = true;
                        matrix[index][jj] = true;
                    }
                    else {
                        matrix[index + 1][index] = true;
                        matrix[index][index + 1] = true;
                    }
                    matrix[n_vertices][index] = true;
                    matrix[index][n_vertices] = true;
                    index++;
                }
            }
        }
        if (!connected) {
            matrix[ii][jj] = false;
            matrix[jj][ii] = false;
        }
    }

    for (auto const& r : matrix) {
        for (auto const& c : r) { std::cout << c; }
        std::cout << std::endl;
    };

    // std::cout << matrix << std::endl;

    return is_planar_adjacency(matrix);
}

std::pair<Node,Strand> xGraph::get_one_loop_subdiagram(size_t loop_index) const {

    if (loop_index > size_t(n_loops) || loop_index == 0) {
        std::cerr << "ERROR: Out of bounds call of xGraph::get_one_loop_subdiagram(i = " << loop_index << ") in a " << n_loops << " diagram." << std::endl;
        std::exit(1);
    }
    std::vector<std::pair<size_t, size_t>> edge_indices;
    std::vector<Strand> strands;
    // find the links through which the loop momentum flows
    for (auto& it : base_graph.get_connection_map()) {
        auto con = it.second;
        for (size_t ii = 0; ii < con.size(); ii++) {
            if (con[ii].has_loop_momentum(loop_index)) {
                edge_indices.push_back(it.first);
                strands.push_back(con[ii]);
            }
        }
    }
    if (edge_indices.size() == 0) {
        std::cerr << "ERROR: Did not find any loop associated to index " << loop_index << " ." << std::endl;
        std::exit(1);
    }
    // generate a linear chain
    for (size_t ii = 0; ii < edge_indices.size(); ii++) {
        size_t to_match = edge_indices[ii].second;
        for (size_t jj = ii + 1; jj < edge_indices.size(); jj++) {
            if (edge_indices[jj].first == to_match) {
                iter_swap(edge_indices.begin() + ii + 1, edge_indices.begin() + jj);
                iter_swap(strands.begin() + ii + 1, strands.begin() + jj);
                break;
            }
            if (edge_indices[jj].second == to_match) {
                auto swap = edge_indices[jj].first;
                edge_indices[jj].first = edge_indices[jj].second;
                edge_indices[jj].second = swap;
                iter_swap(edge_indices.begin() + ii + 1, edge_indices.begin() + jj);
                strands[jj].reverse();
                iter_swap(strands.begin() + ii + 1, strands.begin() + jj);
                break;
            }
        }
    }
    // check that the resulting object is cyclic, i.e. the last strand leads back to the first node
    if (edge_indices.front().first != edge_indices.back().second && edge_indices.front().second != edge_indices.back().first &&
        edge_indices.front().first != edge_indices.back().first && edge_indices.front().second != edge_indices.back().second) {
        std::cerr << "ERROR: Did find inconsistent loop " << edge_indices;
        std::exit(1);
    }
    std::vector<Bead> beads;
    std::vector<Link> links;

    // glue the strands together
    for (size_t ii = 0; ii < edge_indices.size(); ii++) {
        links.insert(links.end(), strands[ii].get_links().begin(), strands[ii].get_links().end());
        beads.insert(beads.end(), strands[ii].get_beads().begin(), strands[ii].get_beads().end());
        // Add the node. The last node is singled out because we have to pass a node for a one-loop graph
        if (ii != edge_indices.size() - 1) { beads.push_back(Bead(base_graph.get_nodes()[edge_indices[ii].second].get_legs())); }
    }

    auto node  =  Node{base_graph.get_nodes()[edge_indices.back().second].get_legs()};

    return {node,{beads, links}};
}

xGraph& xGraph::operator=(const Graph<Node, Connection<Strand>>& gr) {
    *this = xGraph(gr);
    return *this;
}

const xGraph& xGraph::get_special_routing() const {
    if (get_n_loops() > 2) {
        std::cerr << "ERROR: xGraph::get_special_routing() only implemented up-to two loops" << std::endl;
        std::exit(1);
    }

    using xGtype = Graph<Node, Connection<Strand>>;

    static thread_local std::unordered_map<xGtype, xGraph> cache_special_routings;
    auto it = cache_special_routings.find(get_base_graph());
    if(it != cache_special_routings.end())
        return it->second;

    // 1-loop and 2-loop-factorized xgraphs
    if (get_n_loops() == 1 || (get_n_loops() == 2 && get_base_graph().is_factorizable())) {
        auto copy = *this;
        copy.reset_momentum_routing(true, 0);
        copy.trace_momentum_routing();
        cache_special_routings[get_base_graph()] = copy;
    }
    else if (get_n_loops() == 2) {
        // ELSE: only 2 loops non-factorizable
        cache_special_routings[get_base_graph()] = get_2loop_special_routing();
    }
    return cache_special_routings.at(get_base_graph());
}

xGraph xGraph::get_2loop_special_routing() const {
    auto& nodes = get_base_graph().get_nodes();
    std::map<std::pair<size_t, size_t>, Connection<Strand>> newmap = get_base_graph().get_connection_map();
    auto& conn = newmap.at({0, 1});
    // get higher number of beads first
    auto compp = [](std::pair<size_t, size_t> a, std::pair<size_t, size_t> b) {
        if (a.first > b.first)
            return true;
        else
            return a.second < b.second;
    };
    std::set<std::pair<size_t, size_t>, decltype(compp)> count_strand(compp);
    for (size_t sindex = 0; sindex < conn.get_edges().size(); sindex++) {
        auto& strand = conn[sindex];
        strand.reset_momentum_routing(false);
        for (auto& l : strand.get_links()) l.drop_momentum_label();
        strand.drop_routings();
        count_strand.insert({strand.size(), sindex});
    }
    size_t count(0);
    for(auto& entry: count_strand) {
        if(count++>1) break;
        conn[entry.second].set_momentum_label(0, count);
    }
    return xGraph(Graph<Node, Connection<Strand>>(nodes, newmap));
}

///// Math output


void recursive_filling_all_subsets(const size_t& step,const size_t& previous,const size_t& nsubsets,const size_t& size,const std::vector<size_t>& passed,std::vector<std::vector<size_t>>& container){
    if(step+1==nsubsets){
        // recursion finished
        container.push_back(passed);
        return;
    }
    else{
        for(size_t ii=previous+1;ii<size-nsubsets+1+step+1;ii++){
            std::vector<size_t> local = passed;
            local.push_back(ii);
            recursive_filling_all_subsets(step+1,ii,nsubsets,size,local,container);
        }
    }
}

const std::vector<std::vector<size_t>>& get_all_subsets(size_t nsubsets,size_t size){
    static thread_local std::unordered_map<std::pair<size_t,size_t>,std::vector<std::vector<size_t>>> cache_all_subsets;
    if(cache_all_subsets.find({nsubsets,size})!=cache_all_subsets.end())
        return cache_all_subsets.at({nsubsets,size});
    // sanity check
    if(nsubsets>size){
        std::cerr<<"ERROR: in a container of size "<<size<<" we can't find subsets of size "<<nsubsets<<std::endl;
        std::exit(64517);
    }
    std::vector<std::vector<size_t>> toret;

    for(size_t ii=0;ii<size-nsubsets+1;ii++){
        std::vector<size_t> local = {ii};
        recursive_filling_all_subsets(0,ii,nsubsets,size,local,toret);
    }

    // store
    cache_all_subsets[{nsubsets,size}]=toret;
    return cache_all_subsets.at({nsubsets,size});
}

std::vector<std::vector<size_t>> permute_adjacency(const std::vector<size_t>& permute,const std::vector<std::vector<size_t>>& adjacency){
    std::vector<std::vector<size_t>> toret(adjacency.size());
    for(size_t ii=0;ii<adjacency.size();ii++)
        toret[ii]=std::vector<size_t>(adjacency.size());
    
    for(size_t ii=0;ii<adjacency.size();ii++)
        for(size_t jj=0;jj<adjacency.size();jj++)
            toret[ii][jj]=adjacency[permute[ii]][permute[jj]];

    return toret;
}

bool is_this_a_bubble(size_t node,const std::vector<size_t>& subset,const std::vector<std::vector<size_t>>& adjacency){
    // notice, this permutation is actually not required, but it is included for easier test
    std::vector<size_t> permute = subset;
    permute.push_back(node);
    for(size_t ii=0;ii<adjacency.size();ii++){
        if(std::find(permute.begin(),permute.end(),ii)==permute.end())
            permute.push_back(ii);
    }
    auto padjacency = permute_adjacency(permute,adjacency);
#if 0
for(const auto& v:adjacency)
//std::cout<<"	"<<v<<std::endl;
//std::cout<<"PERMUTE: "<<permute<<std::endl;
for(const auto& v:padjacency)
//std::cout<<"	"<<v<<std::endl;
//std::cout<<" "<<std::endl;
//std::cout<<"ii: "<<0<<" "<<subset.size()<<std::endl;
//std::cout<<"jj: "<<subset.size()+1<<" "<<adjacency.size()<<std::endl;
#endif
    // check for up-left zero submatrix 
    for(size_t ii=0;ii<subset.size();ii++)
        for(size_t jj=subset.size()+1;jj<adjacency.size();jj++)
            if(padjacency[ii][jj]!=0)
                return false;
    return true;
}

const std::vector<std::set<size_t>>& find_all_bubbles_in_graph(size_t node,const std::vector<std::vector<size_t>>& adjacency){
    using AdjacencyMatrix = std::vector<std::vector<size_t>>;
    if(node>adjacency.size()){
        std::cerr<<"ERROR: find_all_bubbles_in_graph("<<node<<") called for a graph with "<<adjacency.size()<<" nodes!"<<std::endl;
        std::exit(42);
    }
    // we cache information in this map, according to the { node , adjacency matrix } of this call
    static thread_local std::unordered_map<std::pair<size_t,AdjacencyMatrix>,std::vector<std::set<size_t>>> bubble_container;
    if(bubble_container.find({node,adjacency})!=bubble_container.end())
        return bubble_container.at({node,adjacency});

    std::vector<std::set<size_t>> toret;
    // deal first with self connections
    if(adjacency[node][node]>0)
        toret.push_back({node});

    std::vector<size_t> othern;
    for(size_t ii=0;ii<adjacency.size();ii++)
        if(ii!=node)
            othern.push_back(ii);    

    while(othern.size()>0){
        bool failed(true);
        for(size_t nnodes=1;nnodes<=othern.size();nnodes++){
            const auto& partitions = get_all_subsets(nnodes,othern.size());
            // substitute for our nodes
            std::vector<std::vector<size_t>> subsets;
            for(auto p:partitions){
                subsets.push_back(std::vector<size_t>());
                for(auto kk:p)  subsets.back().push_back(othern[kk]);
            }
            bool extracted(false);
            // now check all subsets
            for(const auto& subset:subsets){
                if(is_this_a_bubble(node,subset,adjacency)){
                    extracted=true;
                    failed=false;
                    // remove nodes from othern
                    toret.push_back({node});
                    for(auto i:subset){
                        toret.back().insert(i);
                        auto loc = std::find(othern.begin(),othern.end(),i);
                        if(loc!=othern.end())
                            othern.erase(loc);
                    }
                }
            }
            if(extracted)
                break;
        }
        if(failed)
            break;
    }

    // store
    bubble_container[{node,adjacency}]=toret;

    return bubble_container.at({node,adjacency});
}

std::string math_string(const lGraph::Leg& l) {
    if (l.is_on_shell())
        return math_list_with_head("Leg", l.get_momentum_index(), l.get_mass_index());
    else
        return math_list_with_head("Leg", l.get_momentum_index());
}
std::string math_string(const lGraph::Link& l) {
    if (l.tracks_momentum())
        return math_list_with_head("Link", math_list_with_head("LoopMomentum",l.get_momentum_label()), l.get_mass_index());
    else
        return math_list_with_head("Link", l.get_mass_index());
}

std::string math_string(const lGraph::Bead& b) {
    return math_list_with_head("Bead",b.get_legs());
}
std::string math_string(const lGraph::Node& b) {
    return math_list_with_head("Node",b.get_legs());
}
std::string math_string(const lGraph::Strand& str) {
    using ::Caravel::math_string;
    auto& beads = str.get_beads();
    auto& links = str.get_links();

    std::vector<std::string> flattened;

    for(auto&& [i, obj] : enumerate(beads)){
        flattened.push_back(math_string(links.at(i)));
        flattened.push_back(math_string(obj));
    }
    flattened.push_back(math_string(links.back()));

    return math_list_with_head("Strand",flattened);
}

std::string math_string(const lGraph::Connection<lGraph::Strand>& b) {
    using ::Caravel::math_string;
    return math_list_with_head("Connection",b);
}

std::string math_string(const lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>& base_graph) {
    using ::Caravel::math_string;
    assert(base_graph.get_n_loops() <= 2);
    if (base_graph.get_n_loops() == 1) {
        auto con = base_graph.get_connection(0, 0);
        return math_list_with_head("CaravelGraph", math_list_with_head("Nodes", base_graph.get_nodes()), con);
    }
    else {
        auto con = base_graph.is_factorizable() ? base_graph.get_connection(0, 0) : base_graph.get_connection(0, 1);
        return math_list_with_head("CaravelGraph", math_list_with_head("Nodes", base_graph.get_nodes()), con);
    }
}


std::string math_string(const lGraph::xGraph& gr){
    return math_string(gr.get_base_graph());
}

size_t get_n_of_links(const Graph<Node, Connection<Strand>>& g) {
    size_t tret(0);
    for (auto& [ij, con] : g.get_connection_map()) {
        for (size_t ii = 0; ii < con.size(); ii++) {
            tret += con[ii].get_n_links();
        }
    }
    return tret;
}

Graph<Node, Connection<Strand>> replace_mass_indices(const Graph<Node, Connection<Strand>>& xg, const std::map<size_t, size_t>& map) {
    std::vector<Node> nnodes = xg.get_nodes();
    std::map<std::pair<size_t, size_t>, Connection<Strand>> nconns = xg.get_connection_map();

    for (auto& n : nnodes) {
        std::vector<Leg> nls;
        for (auto& ll : n.get_legs()) {
            if (ll.is_on_shell()) {
                nls.emplace_back(true, map.at(ll.get_mass_index()), ll.get_momentum_index());
                nls.back().ptype = ll.ptype;
            }
            else
                nls.push_back(ll);
        }
        auto lcoups = n.coups;
        n = Node(nls);
        n.coups = lcoups;
    }
    for (auto& [pair, conn] : nconns)
        for (size_t ii = 0; ii < conn.get_edges().size(); ii++) {
            auto& strand = conn[ii];
            for (auto& b : strand.get_beads()) {
                std::vector<Leg> nls;
                for (auto& ll : b.get_legs()) {
                    if (ll.is_on_shell()) {
                        nls.emplace_back(true, map.at(ll.get_mass_index()), ll.get_momentum_index());
                        nls.back().ptype = ll.ptype;
                    }
                    else
                        nls.push_back(ll);
                }
                auto lcoups = b.coups;
                b = Bead(nls);
                b.coups = lcoups;
            }
            for (auto& link : strand.get_links()) {
                auto lptype = link.ptype;
                link = Link(link.get_loop_momentum(), map.at(link.get_mass_index()));
                link.ptype = lptype;
            }
            strand.drop_routings();
        }
    return Graph<Node, Connection<Strand>>(nnodes, nconns);
}

} // namespace lGraph
} // namespace Caravel

namespace std {
size_t hash<Caravel::lGraph::Leg>::operator()(const Caravel::lGraph::Leg& l) const {
    hash<bool> hash_fkt;
    size_t result = hash_fkt(l.is_on_shell());
    if (l.is_on_shell()) { Caravel::hash_combine(result, l.get_mass_index()); }
    Caravel::hash_combine(result, l.get_momentum_index());
    return result;
}
size_t hash<Caravel::lGraph::Node>::operator()(const Caravel::lGraph::Node& node) const {
    hash<bool> hash_fkt;
    size_t result = hash_fkt(node.is_on_shell());
    if (node.is_on_shell()) { Caravel::hash_combine(result, node.get_mass_index()); }
    Caravel::hash_combine(result, node.get_legs());

    return result;
}
size_t hash<Caravel::lGraph::Bead>::operator()(const Caravel::lGraph::Bead& bead) const {
    hash<bool> hash_fkt;
    size_t result = hash_fkt(bead.is_on_shell());
    if (bead.is_on_shell()) { Caravel::hash_combine(result, bead.get_mass_index()); }
    Caravel::hash_combine(result, bead.get_legs());

    return result;
}
size_t hash<Caravel::lGraph::Link>::operator()(const Caravel::lGraph::Link& link) const {
    hash<size_t> hash_fkt;
    return hash_fkt(link.get_mass_index());
}
size_t hash<Caravel::lGraph::Strand>::operator()(const Caravel::lGraph::Strand& str) const {
    hash<std::vector<Caravel::lGraph::Bead>> hash_fkt;
    size_t result = hash_fkt(str.get_beads());
    Caravel::hash_combine(result, str.get_links());
    return result;
}
} // namespace std
