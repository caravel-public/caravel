/**
 * @file IntegralGraph.h
 *
 * @date 10.7.2017
 *
 * @brief Header file for handler of integrals
 *
 * The class IntegralGraph represents in abstract an integral structure. It should be the main
 * tool to communicate between coefficient-providers and integral-providers.
 *
 * The class IntegralContainer allows to track information of sets of integrals.
 *
 */
#ifndef INTEGRAL_CONTAINER_H_
#define INTEGRAL_CONTAINER_H_

#include "Graph.h"
#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <utility>

#include "FunctionSpace/IntegralInsertion.h"
#include "Core/Series.h"
#include "Core/cutTopology.h"
#include "Core/momD_conf.h"

#include "Core/MathInterface.h"

#define _DEBUG_INTEGRALS 1

namespace Caravel {
namespace lGraph {

/**
 * Class to represent a single integral. So far 1-loop integrals represented by single cut and 2-loop integrals by associated S-tree.
 * In the future 2-loop integrals might be represented differently.
 */
class IntegralGraph {
    friend class IntegralContainer;
    friend std::ostream& operator<<(std::ostream&, const IntegralGraph&);
    IntegralInsertion insertion_enum; /**< Tracks information of integral insertion */
    xGraph base_graph;
    std::string insertion{}; /**< Name of the insertion (optional) */
    std::string topology_name{};  /**< Name of the corresponding topology (optional) */

  public:
    /**
     * Default IntegralGraph constructor from automatic generation (by compiler)
     */
    IntegralGraph();
    /**
     * Main constructor for integral providers. The input represents the
     * @param propagator stucture
     * @param master insertion name
     */
    IntegralGraph(const xGraph&, const std::string&);
    /**
     * Main constructor for integral providers. The input represents the
     * @param propagator stucture
     * @param master insertion name
     * @param topology name
     */
    IntegralGraph(const xGraph&, const std::string&, const std::string&);
    /**
     * Same as above packaged in a pair
     */
    IntegralGraph(const std::pair<xGraph, std::string>& in) : IntegralGraph{in.first, in.second} {}
    /**
     * Constructor from given graph and insertion
     *
     * @param IntegralInsertion representing the numerator of the integral
     * @param Graph represents the graph corresponding to the topology
     */
    IntegralGraph(const IntegralInsertion&, const xGraph&);
    /**
     * Constructor from MathList
     * format:
     * IntegralGraph["insertion name",CaravelGraph[...],"topology name", MapToRepresentative[]]
     */
    IntegralGraph(const MathList&);
    /**
     * Method to find if the passed IntegralGraph is associated with *this (that is, it represents the same integral).
     * So far, only thorough checks are performed at 1-loop for 4-, 3-, 2- and 1-pt integrals. At 2-loop only exact match
     * of S-tree is made, without possible relations via flips or symmetries
     */
    bool is_associated_to(const IntegralGraph&) const;
    /**
     * Returns if propagator is massless (index 1-based)
     *
     * @param int representing the ith propagator in the integral (1-based)
     * @return bool and a 'false' together with an error message if out of bounds
     */
    bool is_massless_propagator(int) const;
    /**
     * Returns if a given vertex of the integral is massless, that is built up of a single massless particle (index 1-based)
     *
     * @param int representing the ith vertex in the integral (1-based)
     * @return bool and a 'false' together with an error message if out of bounds
     */
    bool is_massless_vertex(int) const;
    /**
     * Method to check if a given vertex is on-shell and has a mass that matches the mass in the given propagator
     *
     * @param int representing the vertex to be checked (1-based)
     * @param int representing the propagator to be checked (1-based)
     */
    bool check_on_shell_leg(int, int) const;
    /**
     * Method to get number of attached momenta to a given vertex
     *
     * @param int representing the vertex considered (1-based)
     */
    size_t vertex_size(int) const;
    /**
     * This method checks that the corresponding vertex is on-shell (otherwise returns 0 with error message) and the returns the corresponding mass index
     *
     * @param int representing the vertex considered (1-based)
     */
    const xGraph& get_graph() const { return base_graph; };

    /**
     * This method checks that the corresponding vertex is on-shell (otherwise returns 0 with error message) and the returns the corresponding mass index
     *
     * @param int representing the vertex considered (1-based)
     */
    size_t get_on_shell_vertex_mass_index(int) const;
    /**
     * Returns the mass index of the propagator requested (1-based, and bounds checked)
     *
     * @param int representing the propagator (1-based)
     */
    size_t get_propagator_mass_index(int) const;
    /**
     * Method to return how many vertices are contained in *this
     */
    size_t number_of_vertices() const;
    /**
     * This method the corresponding vector of mass indices corresponding to the requested vertex (1-based, bounds checked)
     *
     * @param int representing the vertex considered (1-based)
     */
    const std::vector<size_t> vertex_momentum_indices(int) const;
    /**
     * Modifies the insertion associated to *this
     */
    void print_info(std::ostream& outstr = std::cout) const;
    /**
     * Modifies the insertion associated to *this
     */
    void set_integral_insertion(const IntegralInsertion&);
    int get_n_loops() const;
    IntegralInsertion get_insertion() const;
    /// Get the string representation of the insertion.
    std::string get_insertion_string() const;

    std::string get_topology_name() const;

    /// Returns a human readable string representing the integral in the form TopologyName[insertion, propagatorList]
    std::string get_readable_graph() const;

    std::vector<std::vector<size_t>> to_representative_kinematics_map{}; /**< How to map this integral to a representative integral in this topology (optional) */
};


/**
 * Compare two IntegralGraph's.
 * 
 * Two IntegralGraphs are considered equal if both the insertion string representation 
 * and the xGraph::get_topology agree.
 * 
 * @param g1 The first IntegralGraph in the comparison.
 * @param g2 The second IntegralGraph in the comparison.
 * @return `true` if the IntegralGraph contain the same information else `false`.
 */
bool operator==(const IntegralGraph& g1, const IntegralGraph& g2);

/**
 * Class to hold information associated with scalar integrals. It works as an interface between libForest and libIntegrals, and it can
 * be used by Forest and Builder
 */
class IntegralContainer {
    std::vector<IntegralGraph> integrals; /**< Vector of integrals needed */
    std::unordered_map<std::pair<int, IntegralInsertion>, size_t>
        map; /**< Maps the different cuts/insertion processed into the vector integrals. size_t is 1-based (zero assigned to no integral) */
    std::vector<std::vector<int>> joined_cuts; /**< Keeps track of all cuts that have been associated to each entry in integrals */
  public:
    /**
     * Default IntegralContainerNew constructor from automatic generation (by compiler)
     */
    IntegralContainer() = default;
    /**
     * Method to handle a new cutTopology in *this. If necessary, a new integral is registered. If not it is associated with an existing one
     *
     * @param IntegralInsertion to specify the numerator of the integral
     * @param xGraph to be processed
     * @param int representing the associated index for the cutTopology in corresponding Forest
     */
    void process_graph(const IntegralInsertion&, const xGraph&,int);
    /**
     * Method to count the number of (independent) integrals registered considering all loops
     */
    size_t size() const;
    /**
     * Method to count the number of (independent) integrals registered at a given number of loops
     *
     * @param int representing the number of loops to count
     */
    size_t size(int) const;
    /**
     * Get access to the map
     *
     * @param int representing cut
     * @param the numerator insertion
     */
    size_t get_integral_index(int, const IntegralInsertion&) const;
    /**
     * Get access to the corresponding IntegralGraph
     *
     * @param int representing cut
     * @param the numerator insertion
     */
    IntegralGraph get_integral_graph(int, const IntegralInsertion&) const;
    /**
     * Gives full access to all IntegralGraph's, exactly what's in the member integrals
     */
    const std::vector<IntegralGraph>& get_all_integral_graphs() const;
};

std::string math_string(const IntegralGraph&);

struct IntegralInfo {
    typename xGraph::base_graph_type graph;
    std::string insertion_name{};
    std::string topology_name{};
    std::vector<std::vector<size_t>> map_to_representative;

    IntegralInfo(const lGraph::IntegralGraph& igraph)
        : graph(igraph.get_graph().remove_loop_momenta_info()), insertion_name(igraph.get_insertion_string()), topology_name(igraph.get_topology_name()),
          map_to_representative(igraph.to_representative_kinematics_map) {}
    IntegralInfo(const MathList& mlist);
};

bool operator==(const IntegralInfo& ii1, const IntegralInfo& ii2);
std::string math_string(const IntegralInfo&);
std::ostream& operator<<(std::ostream&, const IntegralInfo&);

} // namespace lGraph
} // namespace Caravel


namespace std {
  template <> struct hash<Caravel::lGraph::IntegralGraph> { size_t operator()(const Caravel::lGraph::IntegralGraph&) const; };
  template <> struct hash<Caravel::lGraph::IntegralInfo> { size_t operator()(const Caravel::lGraph::IntegralInfo&) const; };
}

#endif // INTEGRAL_CONTAINER_H_
