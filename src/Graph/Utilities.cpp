#include "Graph/Utilities.h"

#include "Core/MathOutput.h"

namespace Caravel {
namespace lGraph {

using Caravel::operator<<;

MomCollection::MomCollection(const std::string& n, const std::vector<int>& in) : prefix(n) {
#ifdef DEBUG_ON
    // no zero present
    if (std::find(in.begin(), in.end(), 0) != in.end()) {
        std::cerr << "ERROR: no '0' index should be passed to MomentumRouting! " << in << std::endl;
        std::exit(77162);
    }
#endif
    for (auto i : in) {
        if (i < 0) {
            // no doubled momentum shall appear at construction time
            if (minus_momenta.find(size_t(std::abs(i))) != minus_momenta.end()) {
                std::cerr << "ERROR: " << prefix << i << " momenta was passed more than once! " << in << std::endl;
                std::exit(77163);
            }
            minus_momenta.insert(size_t(std::abs(i)));
        }
        else {
            // no doubled momentum shall appear at construction time
            if (plus_momenta.find(size_t(i)) != plus_momenta.end()) {
                std::cerr << "ERROR: " << prefix << i << " momenta was passed more than once! " << in << std::endl;
                std::exit(77164);
            }
            plus_momenta.insert(size_t(i));
        }
    }
}

void MomCollection::mom_conservation(size_t n) {
#ifdef DEBUG_ON
    // sanity check (no momenta should be larger than n)
    if (plus_momenta.size() > 0) {
        if (*plus_momenta.rbegin() > n) {
            std::cerr << "ERROR: momentum conservation called for " << n << " particles but has larger entry! *this = " << *this << std::endl;
            std::exit(5424);
        }
    }
    if (minus_momenta.size() > 0) {
        if (*minus_momenta.rbegin() > n) {
            std::cerr << "ERROR: momentum conservation called for " << n << " particles but has larger entry! *this = " << *this << std::endl;
            std::exit(5424);
        }
    }
#endif
    // No zero should ever be stored. I assume no i - i contained in *this
    if (plus_momenta.size() == n) plus_momenta = std::set<size_t>();
    if (minus_momenta.size() == n) minus_momenta = std::set<size_t>();
}

void momcollection_canonicalize(size_t n, std::set<size_t>& plus, std::set<size_t>& minus) {
    size_t max = std::max(plus.size(), minus.size());
    // no action required
    if (n % 2 == 0) {
        if (max < n / 2) return;
    }
    else {
        if (max <= n / 2) return;
    }
    
    std::set<size_t> alternate;
    if(plus.size() > 0) {
        for(size_t ii = 1; ii <= n; ii++)
            if(std::find(plus.begin(), plus.end(), ii) == plus.end())
                alternate.insert(ii);
        if(*alternate.begin() < *plus.begin() || alternate.size() < plus.size()){
            plus = std::set<size_t>();
            minus = alternate;
            return;
        }
    }
    else {
        for(size_t ii = 1; ii <= n; ii++)
            if(std::find(minus.begin(), minus.end(), ii) == minus.end())
                alternate.insert(ii);
        if(*alternate.begin() < *minus.begin() || alternate.size() < minus.size()){
            minus = std::set<size_t>();
            plus = alternate;
            return;
        }
    }
}

void MomCollection::canonical_external(size_t n) {
    if (plus_momenta.size() > 0 && minus_momenta.size() > 0) {
        std::cerr << "ERROR: MomCollection::canonical_external(n=" << n << ") called when negative and positive entries present! " << *this << std::endl;
        std::exit(1);
        //std::cerr << "Did nothing..." << std::endl;
        //return;
    }
    if (plus_momenta.size() > n || minus_momenta.size() > n) {
        std::cerr << "ERROR: logical error MomCollection::canonical_external(.) called with n=" << n << " too small! " << *this << std::endl;
        std::exit(1);
    }
    momcollection_canonicalize(n, plus_momenta, minus_momenta);
}

MomCollection MomCollection::operator-() const {
    MomCollection toret;
    toret.prefix = prefix;
    toret.plus_momenta = minus_momenta;
    toret.minus_momenta = plus_momenta;
    for(auto& e: momenta_cache) toret.momenta_cache[e.first] = -e.second;
    return toret;
}

MomCollection operator+(const MomCollection& m1, const MomCollection& m2) {
#ifdef DEBUG_ON
    // prefixes should match
    if (m1.prefix != m2.prefix) {
        std::cerr << "ERROR: operator+(MomCollection,MomCollection) with different prefixes! " << m1.prefix << " and " << m2.prefix << std::endl;
        std::exit(41976);
    }
#endif
    MomCollection toret(m1);
    for (auto pl : m2.plus_momenta) {
        if (toret.plus_momenta.find(pl) == toret.plus_momenta.end())
            toret.plus_momenta.insert(pl);
        else {
            auto it = toret.momenta_cache.find(pl);
            if(it != toret.momenta_cache.end())
                ++(it->second);
            else
                toret.momenta_cache[pl] = 1;
        }
    }
    for (auto ml : m2.minus_momenta) {
        if (toret.minus_momenta.find(ml) == toret.minus_momenta.end())
            toret.minus_momenta.insert(ml);
        else {
            auto it = toret.momenta_cache.find(ml);
            if(it != toret.momenta_cache.end())
                --(it->second);
            else
                toret.momenta_cache[ml] = -1;
        }
    }
    for (auto& [pl, multiplicity] : m2.momenta_cache) {
        auto it = toret.momenta_cache.find(pl);
        if (it != toret.momenta_cache.end())
            it->second += multiplicity;
        else
            toret.momenta_cache[pl] = multiplicity;
    }
    toret.cancel_momentum_entries();
    return toret;
}

void set_add_with_mom_conservation(size_t n, std::set<size_t>& s1, const std::set<size_t>& s2) {
    std::set<size_t> dropped;
    for (auto i : s2) {
        auto r = s1.insert(i);
        if (!r.second) {
            auto rr = dropped.insert(i);
            if (!rr.second) {
                std::cerr << "ERROR: set_add_with_mom_conservation(.) too many equal indices: " << s1 << " " << s2 << std::endl;
                std::exit(1);
            }
        }
    }
    if (dropped.size() > 0) {
        if (dropped.size() == n) { 
            // (just do a sanity check
            if(*dropped.begin() != 1 || *dropped.rbegin() != n) {
                std::cerr<<"ERROR: set_add_with_mom_conservation(.) with n = "<<n<<" passed bad momenta: "<<s1<<" "<<s2<<std::endl;
                std::exit(1);
            }
        }
        else {
            // check if 's1' has 1+...+n
            if(s1.size() == n && *s1.begin() == 1 && *s1.rbegin() == n) {
                s1 = dropped;
                return;
            }
            std::cerr<<"ERROR: set_add_with_mom_conservation(.) passed overlapping momenta: "<<s1<<" "<<s2<<std::endl;
            std::exit(1);
        }
    }
}

MomCollection add_with_mom_conservation(size_t n, const MomCollection& m1, const MomCollection& m2) {
#ifdef DEBUG_ON
    // prefixes should match
    if (m1.prefix != m2.prefix) {
        std::cerr << "ERROR: add_with_mom_conservation(.) with different prefixes! " << m1.prefix << " and " << m2.prefix << std::endl;
        std::exit(1);
    }
#endif
    std::set<size_t> plus = m1.plus_momenta;
    set_add_with_mom_conservation(n, plus, m2.plus_momenta);
    std::set<size_t> minus = m1.minus_momenta;
    set_add_with_mom_conservation(n, minus, m2.minus_momenta);
    std::vector<int> tr;
    for(auto i: plus) tr.push_back(int(i));
    for(auto i: minus) tr.push_back(-int(i));
    auto tret = MomCollection{m1.prefix, tr};
    tret.cancel_momentum_entries();
    return tret;
}


MomCollection operator-(const MomCollection& m1, const MomCollection& m2) {
#ifdef DEBUG_ON
    // prefixes should match
    if (m1.prefix != m2.prefix) {
        std::cerr << "ERROR: operator-(MomCollection,MomCollection) with different prefixes! " << m1.prefix << " and " << m2.prefix << std::endl;
        std::exit(41976);
    }
#endif
    MomCollection toret(m1);
    for (auto pl : m2.minus_momenta) {
        if (toret.plus_momenta.find(pl) == toret.plus_momenta.end())
            toret.plus_momenta.insert(pl);
        else {
            auto it = toret.momenta_cache.find(pl);
            if(it != toret.momenta_cache.end())
                --(it->second);
            else
                toret.momenta_cache[pl] = -1;
        }
    }
    for (auto ml : m2.plus_momenta) {
        if (toret.minus_momenta.find(ml) == toret.minus_momenta.end())
            toret.minus_momenta.insert(ml);
        else {
            auto it = toret.momenta_cache.find(ml);
            if(it != toret.momenta_cache.end())
                ++(it->second);
            else
                toret.momenta_cache[ml] = 1;
        }
    }
    for (auto& [pl, multiplicity] : m2.momenta_cache) {
        auto it = toret.momenta_cache.find(pl);
        if (it != toret.momenta_cache.end())
            it->second -= multiplicity;
        else
            toret.momenta_cache[pl] = -multiplicity;
    }
    toret.cancel_momentum_entries();
    return toret;
}

bool operator==(const MomCollection& m1, const MomCollection& m2) {
    return m1.prefix == m2.prefix && m1.plus_momenta == m2.plus_momenta && m1.minus_momenta == m2.minus_momenta;
}

void MomCollection::cancel_momentum_entries() {
    std::vector<size_t> toerase;
    for (auto pl : plus_momenta) {
        if (minus_momenta.find(pl) != minus_momenta.end()) { toerase.push_back(pl); }
    }
    for (auto te : toerase) {
        plus_momenta.erase(te);
        minus_momenta.erase(te);
    }
}

void MomCollection::clear_cache() {
    // first remove zeros
    for (auto it = momenta_cache.begin(); it != momenta_cache.end();) {
        if (it->second == 0)
            it = momenta_cache.erase(it);
        else
            ++it;
    }
    cancel_momentum_entries();
    for (auto& [m, multiplicity] : momenta_cache) {
        if (multiplicity < -1 || multiplicity > 1) {
            std::cerr << "ERROR: MomCollection::clear_cache() can't handle the cache: " << momenta_cache << std::endl;
            std::exit(1);
        }
        else if (multiplicity == -1) {
            if (minus_momenta.find(m) == minus_momenta.end())
                minus_momenta.insert(m);
            else {
                std::cerr << "ERROR: MomCollection::clear_cache() found (negative) repeated momenta: " << m << std::endl;
                std::exit(1);
            }
        }
        else {
            if (plus_momenta.find(m) == plus_momenta.end())
                plus_momenta.insert(m);
            else {
                std::cerr << "ERROR: MomCollection::clear_cache() found (positive) repeated momenta: " << m << std::endl;
                std::exit(1);
            }
        }
    }
    momenta_cache.clear();
}

std::ostream& operator<<(std::ostream& o, const MomCollection& mc) {
    for (auto pl : mc.plus_momenta) { o << "+" << mc.prefix << pl; }
    for (auto ml : mc.minus_momenta) { o << "-" << mc.prefix << ml; }
    return o;
}

MomentumRouting MomentumRouting::operator-() const {
    MomentumRouting toret;
    toret.internal = -internal;
    toret.external = -external;
    return toret;
}

bool MomentumRouting::is_empty() const {
    return internal.is_empty() && external.is_empty();
}

bool MomentumRouting::has_loop_positive(size_t loop) const {
    if (loop == 0) {
        std::cerr << "ERROR: called MomentumRouting::has_loop positive(.) (which is 1-based) with zero argument!" << std::endl;
        std::exit(1);
    }
    return std::find(internal.plus_momenta.begin(), internal.plus_momenta.end(), loop) != internal.plus_momenta.end();
}

bool MomentumRouting::has_loop_negative(size_t loop) const {
    if (loop == 0) {
        std::cerr << "ERROR: called MomentumRouting::has_loop_negative(.) (which is 1-based) with zero argument!" << std::endl;
        std::exit(1);
    }
    return std::find(internal.minus_momenta.begin(), internal.minus_momenta.end(), loop) != internal.minus_momenta.end();
}

bool MomentumRouting::has_loop(size_t loop) const {
    if (loop == 0) {
        std::cerr << "ERROR: called MomentumRouting::has_loop(.) (which is 1-based) with zero argument!" << std::endl;
        std::exit(1);
    }
    return has_loop_positive(loop) || has_loop_negative(loop);
}

MomentumRouting ComposeSingleMomRouting(size_t n, const MomentumRouting& t, const std::vector<MomentumRouting>& lP_from_lD) {
    // we operate on MomCollection first, and then construct with them MomentumRouting 
    // (that way we avoid "intermediate" issues with momentum conservation like in 
    // ((l1-l2-1-4)+(l2+3))+(-3) where before adding (-3) external_mom_conservation(.)
    // would complain!
    MomCollection itoret = MomCollection("l");
    MomCollection etoret = t.get_external();
    auto& internal = t.get_internal();
    for(auto i: internal.plus_momenta) {
        itoret = itoret + lP_from_lD[i-1].get_internal();
        etoret = etoret + lP_from_lD[i-1].get_external();
        itoret.cancel_momentum_entries();
        etoret.cancel_momentum_entries();
    }
    for(auto i: internal.minus_momenta) {
        itoret = itoret - lP_from_lD[i-1].get_internal();
        etoret = etoret - lP_from_lD[i-1].get_external();
        itoret.cancel_momentum_entries();
        etoret.cancel_momentum_entries();
    }
    itoret.clear_cache();
    etoret.clear_cache();
    MomentumRouting toret(itoret, etoret);
    toret.external_mom_conservation(n);
    return toret;
}

std::vector<MomentumRouting> ComposeMomentumRoutings(size_t n, const std::vector<MomentumRouting>& lG_from_lP, const std::vector<MomentumRouting>& lP_from_lD) {
    assert(lG_from_lP.size() == lP_from_lD.size());
    std::vector<MomentumRouting> lG_from_lD;
    for(size_t ii = 0; ii<lP_from_lD.size(); ii++)  lG_from_lD.push_back(ComposeSingleMomRouting(n, lG_from_lP[ii], lP_from_lD));
    return lG_from_lD;
}


MomentumRouting add_with_external_mom_conservation(size_t n, const MomentumRouting& m1, const MomentumRouting& m2) {
    MomentumRouting toret;
    toret.internal = m1.internal + m2.internal;
    toret.external = add_with_mom_conservation(n, m1.external, m2.external);
    toret.external_mom_conservation(n);
    return toret;
}

MomentumRouting operator+(const MomentumRouting& m1, const MomentumRouting& m2) {
    MomentumRouting toret;
    toret.internal = m1.internal + m2.internal;
    toret.external = m1.external + m2.external;
    return toret;
}

MomentumRouting operator-(const MomentumRouting& m1, const MomentumRouting& m2) {
    MomentumRouting toret;
    toret.internal = m1.internal - m2.internal;
    toret.external = m1.external - m2.external;
    return toret;
}

bool operator==(const MomentumRouting& m1, const MomentumRouting& m2) {
    return m1.internal == m2.internal && m1.external == m2.external;
}

std::ostream& operator<<(std::ostream& o, const MomentumRouting& mr) {
    o << mr.internal << mr.external;
    return o;
}

std::ostream& operator<<(std::ostream& o, const LoopMomentum& lm) {
    if(lm.index>0) o<<"l"<<lm.index;
    if(lm.index<0) o<<"-l"<<-lm.index;
    if(lm.active && !lm.get_shift().is_empty()) o<<"[shift:"<<lm.get_shift()<<"]";
    return o;
}


std::string ngon_name(size_t n) {
    switch (n) {
        case 0: return "0-gon";
        case 1: return "Tadpole";
        case 2: return "Bubble";
        case 3: return "Triangle";
        case 4: return "Box";
        case 5: return "Pentagon";
        case 6: return "Hexagon";
        case 7: return "Heptagon";
        case 8: return "Octagon";
        case 9: return "Nonagon";
        default: return std::to_string(n) + "-gon";
    }
}

void DFS(std::vector<std::vector<bool>>& adjacency, std::vector<bool>& visited_nodes, size_t start_node) {
    for (size_t ii = 0; ii < adjacency.size(); ii++) {
        // check if vertex is connected
        if (adjacency[ii][start_node]) {
            // if it was not already visited continue recursively
            if (!visited_nodes[ii]) {
                visited_nodes[ii] = true;
                DFS(adjacency, visited_nodes, ii);
            }
        }
    }
}

bool is_connected_matrix(std::vector<std::vector<bool>>& adjacency) {
    if (adjacency.size() == 0) {
        std::cerr << "ERROR: adjacency matrix should have at least one entry!" << std::endl;
        std::exit(528);
    }
    std::vector<bool> visited_nodes(adjacency.size());
    DFS(adjacency, visited_nodes, 0);
    for (auto n : visited_nodes) {
        if (!n) { return false; }
    }
    return true;
}

std::ostream& operator<<(std::ostream& o, const MomChoice& mom) {
    o<<"l"<<mom.n<<":"<<mom.link<<":"<<(mom.direction?"(down)":"(up)");
    return o;
}

std::ostream& operator<<(std::ostream& o, const Propagator& p){
    o<<"("<<p.mom<<")^2";
    if(p.mass != 0)
        o<<"-m"<<p.mass<<"^2";
    return o;
}

std::string Propagator::get_prop_string() const {
    std::string tr = "Prop[";
    std::stringstream r;
    r << mom;
    if (mass != 0) r << ",m" + std::to_string(mass);
    tr += r.str();
    tr += "]";
    return tr;
}

bool operator==(const Propagator& p1, const Propagator& p2) {
    return (p1.mass == p2.mass) && (p1.mom == p2.mom || p1.mom == -p2.mom);
}

bool operator==(const AccessLink& m1,const AccessLink& m2){
    return m1.connection == m2.connection && 
           m1.strand == m2.strand &&
           m1.locate == m2.locate;
}

bool operator!=(const AccessLink& m1,const AccessLink& m2){
    return !(m1 == m2);
}

std::ostream& operator<<(std::ostream& o, const AccessLink& link) {
    o<<"<conn="<<link.connection.first<<","<<link.connection.second<<">:{strand="<<link.strand<<"|link="<<link.locate<<"}";
    return o;
}

bool operator==(const MomChoice& m1,const MomChoice& m2){
    return m1.n == m2.n && 
           m1.link == m2.link &&
           m1.direction == m2.direction;
}

bool operator!=(const MomChoice& m1,const MomChoice& m2){
    return !(m1 == m2);
}

void MomentumDecoration::add_momentum_information(const int& label, const std::pair<size_t, size_t>& con, const size_t& sd, const size_t& lk) {
    // search to see if abs(label) is already accounted for
    if (std::find_if(internal_momentum_choice.begin(), internal_momentum_choice.end(), [label](const MomChoice& m) { return m.n == static_cast<size_t>(std::abs(label)); }) !=
        internal_momentum_choice.end()) {
        throw std::logic_error("ERROR: MomentumDecoration::add_momentum_information(.) tried to add label " + std::to_string(label) +
                               " when it was already present!");
    }
    internal_momentum_choice.push_back({size_t(std::abs(label)), {con, sd, lk}, (label > 0 ? true : false)});
    std::sort(internal_momentum_choice.begin(), internal_momentum_choice.end(), [](const MomChoice& m1, const MomChoice& m2) { return m1.n < m2.n; });
}

} // namespace lGraph
} // namespace Caravel

