/**
 * @file Utilities.h
 *
 * @date 15.6.2020
 *
 * @brief Collect lGraph utilities
 *
 * Includes structs/classes like MomentumRouting, MomChoice, Propagator, etc
 *
 */
#pragma once

#include <algorithm>
#include <cassert>
#include <map>
#include <numeric>
#include <ostream>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <type_traits>
#include <unordered_map>

#include "Core/Debug.h"
#include "Core/Utilities.h"

namespace Caravel {
namespace lGraph {

class MomentumRouting;
struct MomCollection;

MomCollection operator+(const MomCollection&, const MomCollection&);
MomCollection operator-(const MomCollection&, const MomCollection&);
bool operator==(const MomCollection&, const MomCollection&);

MomCollection add_with_mom_conservation(size_t n, const MomCollection&, const MomCollection&);

std::ostream& operator<<(std::ostream&, const MomCollection&);

/**
 * Struct to track particle types on Link's
 * (eventually this can be extended to hold more information as needed)
 */
struct Field {
    std::string particle; /**< Meant to define particle in Link, via ParticleType(std::string) */
    std::string antiparticle; /**< Meant to define anti-particle in Link, via ParticleType(std::string) */
    Field() = default;
    void reverse() {
        auto ap = antiparticle;
        antiparticle = particle;
        particle = ap;
    }
};

/**
 * Helpful struct for collecting sets of momenta (can only be accessed through MomentumRouting)
 */
struct MomCollection {
    std::string prefix{""};
    std::set<size_t> plus_momenta;
    std::set<size_t> minus_momenta;
    void mom_conservation(size_t);
    void cancel_momentum_entries();
    MomCollection() = default;
    MomCollection(const std::string& name, const std::vector<int>& = {});
    MomCollection operator-() const;
    bool is_empty() const { return plus_momenta.size() == 0 && minus_momenta.size() == 0; }
    std::map<size_t, int> momenta_cache; /**< For situations when temporarily one needs to track doubled momenta. Such cases should always end by running 'clear_cache()' */
    void clear_cache();
  private:
    friend class MomentumRouting;
    /**
     * Only 'MomentumRouting' can access. Uses momentum conservation, to fix a standard (minimal)
     * way to write sums of momenta
     *
     * @param the (unsigned) integer passed to 'MomentumRouting::external_mom_conservation'
     */
    void canonical_external(size_t);
   
};

MomentumRouting operator+(const MomentumRouting&, const MomentumRouting&);
MomentumRouting operator-(const MomentumRouting&, const MomentumRouting&);
bool operator==(const MomentumRouting&,const MomentumRouting&);

MomentumRouting add_with_external_mom_conservation(size_t, const MomentumRouting&, const MomentumRouting&);

std::ostream& operator<<(std::ostream&, const MomentumRouting&);

/**
 * A class to track abstract information about internal momenta in an xGraph (contained in its owned Strand's).
 * It can be used to build functions that handle kinematical information.
 */
class MomentumRouting {
    friend std::ostream& operator<<(std::ostream&, const MomentumRouting&);
    friend MomentumRouting operator+(const MomentumRouting&, const MomentumRouting&);
    friend MomentumRouting operator-(const MomentumRouting&, const MomentumRouting&);
    friend bool operator==(const MomentumRouting&, const MomentumRouting&);
    friend MomentumRouting add_with_external_mom_conservation(size_t, const MomentumRouting&, const MomentumRouting&);
    MomCollection internal; /**< The internal loop momenta */
    MomCollection external; /**< The momenta of external legs */
  public:
    MomentumRouting() : internal(MomCollection("l")), external(MomCollection("k")) {}
    explicit MomentumRouting(const std::vector<int>& e) : internal(MomCollection("l")), external(MomCollection("k", e)) {}
    MomentumRouting(const std::vector<int>& i, const std::vector<int>& e) : internal(MomCollection("l", i)), external(MomCollection("k", e)) {}
    MomentumRouting(const MomCollection& i,const MomCollection& e): internal(i), external(e) {}
    /**
     * Checks momentum conservation for external momenta (in n-point kinematics: 1+...+n = 0)
     */
    void external_mom_conservation(size_t n) { external.mom_conservation(n); canonical_external(n); }
    MomentumRouting operator-() const;
    const MomCollection& get_internal() const { return internal; }
    const MomCollection& get_external() const { return external; }
    bool is_empty() const;
    /**
     * check if loop-momentum passed (1-based) is present
     */
    bool has_loop(size_t loop) const;
    bool has_loop_positive(size_t loop) const;
    bool has_loop_negative(size_t loop) const;
  private:
    /**
     * Auxiliary tool to method external_mom_conservation(.). It takes care of making
     * a canonical choice of external momenta when momentum conservation is checked
     *
     * @param the (unsigned) integer passed to 'external_mom_conservation'
     */
    void canonical_external(size_t n) { external.canonical_external(n); }
};

/**
 * Allows to compose a momentum routing representing transformations
 * lG(lP_j) and lP_j(lD_k) to get lG(lD_k). The unsigned integer is used
 * for imposing (external) momentum conservation
 */
MomentumRouting ComposeSingleMomRouting(size_t n, const MomentumRouting& t, const std::vector<MomentumRouting>& lP_from_lD);

/**
 * Allows to compose sets of momentum routing representing transformations
 * lG_i(lP_j) and lP_j(lD_k) to get lG_i(lD_k). The unsigned integer is used
 * for imposing (external) momentum conservation
 */
std::vector<MomentumRouting> ComposeMomentumRoutings(size_t n, const std::vector<MomentumRouting>& lG_from_lP, const std::vector<MomentumRouting>& lP_from_lD);

/**
 * A data structure that lets track a momentum assignment in a Link instance.
 * Activates strong typing for loop momentum labels and prevent implicit casting
 */
struct LoopMomentum {
    bool active{false}; /**< To track if this link holds a tag for a momentum flowing through */
    int index{0};       /**< If active, then we hold the label of the momentum "l_{index}", with the sign meaning the direction */

  private:
    MomentumRouting shift; /**< Let's shift a loop momentum by another vector "l_{index}" ---> "l_{index}" + shift (with a sanity check that shift does not
                              contain "l_{index}") */

  public:
    LoopMomentum() = default;
    explicit LoopMomentum(int m, bool b = false, const MomentumRouting& s = {}) : active(b), index(m) {
        if (!b && m != 0) active = true;
        if (active) set_shift(s);
    }
    void set_shift(const MomentumRouting& s) {
        // sanity checks
        if (!active || index == 0) {
            std::cerr << "ERROR: LoopMomentum::set_shift(.) called for inactive instance! " << index << std::endl;
            std::exit(1);
        }
        auto s1 = s.get_internal().plus_momenta.find(index);
        auto s2 = s.get_internal().minus_momenta.find(index);
        if (s1 != s.get_internal().plus_momenta.end() || s2 != s.get_internal().minus_momenta.end()) {
            std::cerr << "ERROR: LoopMomentum::set_shift(.) called with argument " << s << " which contains corresponding momentum l" << index << std::endl;
            std::exit(1);
        }
        shift = s;
    }
    void reverse() {
        if (!active) return;
        index = -index; // shift gets intrinsically reverted by switch in loop momentum direction
    }
    const MomentumRouting& get_shift() const { return shift; }
};

std::ostream& operator<<(std::ostream&, const LoopMomentum&);

/**
 * Axuliary struct to specify a Link on an xGraph
 */
struct AccessLink {
    std::pair<size_t, size_t> connection; /**< The {'from','to'} Connection that holds the Strand in which l_{n} is stored */
    size_t strand{0};                     /**< Picks the given Strand in the connection */
    size_t locate{0};                     /**< The link in the strand that holds the momentum */
};

bool operator==(const AccessLink&,const AccessLink&);
bool operator!=(const AccessLink&,const AccessLink&);

std::ostream& operator<<(std::ostream&, const AccessLink&);

/**
 * This auxiliary struct to MomentumDecoration keeps all necessary information for
 * locating a (choice of) loop momentum in xGraph
 */
struct MomChoice {
    size_t n{0};                          /**< l_{n} (the absolute value of the) label of the loop momentum */
    AccessLink link;                      /**< specfies corresponding link */
    bool direction{true};                 /**< Whether it flows in the direction of the connection {'from','to'} "true", or in reverse "false" */

    std::pair<size_t, size_t> get_connection() const {return link.connection;};
    size_t get_strand() const {return link.strand;};
    size_t get_link_location() const {return link.locate;};
};

bool operator==(const MomChoice&,const MomChoice&);
bool operator!=(const MomChoice&,const MomChoice&);

std::ostream& operator<<(std::ostream&, const MomChoice&);

/**
 * This auxiliary class to xGraph helps holding decoration of instances tracking
 * information of selected MomentumRouting's through the xGraph's owned Strand's
 */
class MomentumDecoration {
    friend class xGraph;
    bool active{false};
    int n_loops{0};                                  /**< We keep here a copy of this info */
    std::vector<MomChoice> internal_momentum_choice; /**< If 'active' we store here n_loop choices of internal (loop) momentum */
    MomentumDecoration() = default;
    /**
     * The information required to relabel location of a loop momentum (to be found in 'internal_momentum_choice')
     *
     * @param momentum label
     * @param {from,to} for connection
     * @param strand
     * @param link
     */
    void add_momentum_information(const int&, const std::pair<size_t, size_t>&, const size_t&, const size_t&);
    size_t size() const { return internal_momentum_choice.size(); }
};

/**
 * Compact structure to label all propagators (with momentum routing) in an xGraph
 */
struct Propagator {
    MomentumRouting mom;
    size_t mass;
    std::string get_prop_string() const;
};

std::ostream& operator<<(std::ostream&, const Propagator&);
bool operator==(const Propagator&, const Propagator&);

/**
 * Information to make a Node and Strand-in-Connection map for an xGraph
 *
 * TODO: more than an 'xGraph' struct, this applies to any instance of the
 *       Graph<Node, Connection<Strand>> class
 */
struct xGraphPermute {
    std::pair<size_t, size_t> connection;
    std::vector<size_t> strand_permute_or_flip; /**< If self-connection, this means which strands should be flipped */
};

std::string ngon_name(size_t n);

template <typename T> std::vector<std::vector<T>> permute_indices(std::vector<std::vector<T>>&, const std::vector<size_t>&);

template <typename T>
std::map<std::pair<size_t, size_t>, T> get_permuted_map(const std::map<std::pair<size_t, size_t>, T>&, const std::vector<size_t>&);

template <typename T> std::vector<T> get_permutation(const std::vector<T>&, const std::vector<size_t>&);

void DFS(std::vector<std::vector<bool>>&, std::vector<bool>&, size_t );

bool is_connected_matrix(std::vector<std::vector<bool>>&);


/**
 * Produces all subsets of {1, ..., size} with nsubsets entries
 */
const std::vector<std::vector<size_t>>& get_all_subsets(size_t nsubsets,size_t size);

} // namespace lGraph
} // namespace Caravel

