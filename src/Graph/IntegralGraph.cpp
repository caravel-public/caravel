/**
 * @file IntegralGraph.cpp
 *
 * @date 11.7.2017
 *
 * @brief Implementation of classes associated with IntegralGraph and IntegralContainer
 *
 */

#include <algorithm>
#include <iostream>
#include <utility>

#include "IntegralGraph.h"

#include "Core/Debug.h"
#include "Core/Particle.h"

#include "Core/MathOutput.h"
#include "Graph/Graph.h"

#include "Core/CaravelInput.h"

#define _DEBUG_INTCONTAINER 0

namespace Caravel {
namespace lGraph {

using subprocess = std::vector<Particle*>;
using edgeType = std::vector<std::vector<Particle*>>;
using vertexType = std::vector<Particle*>;

Node get_info_vertex(const ColorHandledProcess& chp, const std::vector<size_t>& drop) {
    // get both colored and coloreless particles
    const std::vector<Particle*>& lcolorless(chp.get_colorless());
    const std::vector<Particle*>& lcolored(chp.get_colored());
    std::vector<Particle*> all;
    all.insert(all.end(), lcolorless.begin(), lcolorless.end());
    all.insert(all.end(), lcolored.begin(), lcolored.end());

    size_t nparts(all.size());
    // take into account loop particles to drop
    if (drop.size() > 0) {
        if (!(nparts >= drop.size())) {
            std::cout << "ERROR in Node get_info_vertex(.): asks to remove " << drop.size() << " when there is only " << nparts << " particles in vertex!!"
                      << std::endl;
        }
    }

    std::vector<Leg> lmi;
    for (size_t ii = 0; ii < nparts; ii++) {
        if (std::find(drop.begin(), drop.end(), ii) == drop.end()) { lmi.push_back(Leg(true, all[ii]->get_mass_index(), all[ii]->mom_index())); }
    }

    return Node(lmi);
}
// Default constructor, we keep "-1" in n_loops for IntegralGraph's that are default constructed
IntegralGraph::IntegralGraph() : insertion_enum(IntegralInsertion::undefined), insertion("UnknownIntegral") {}

IntegralGraph::IntegralGraph(const xGraph& gr, const std::string& in) : base_graph(gr), insertion(in) {
    try {
        auto iet = wise_enum::from_string<IntegralInsertion>(in.c_str());
        insertion_enum = iet.value();
    }
    catch (...) {
        insertion_enum = IntegralInsertion::undefined;
    }
}

IntegralGraph::IntegralGraph(const xGraph& gr, const std::string& in, const std::string& topname) : IntegralGraph(gr, in) { topology_name = topname; }

// generic constructor
IntegralGraph::IntegralGraph(const IntegralInsertion& lii, const xGraph& gr) : insertion_enum(lii), base_graph(gr), insertion(wise_enum::to_string(insertion_enum)) {}

IntegralGraph::IntegralGraph(const MathList& mlist) {
    assert_head(mlist,"IntegralGraph");
    insertion = mlist[0];

    base_graph = xGraph{MathList(mlist[1])};

    switch (mlist.size()) {
        case 4: {
            MathList mm(mlist[3], "MapToRepresentative");
            to_representative_kinematics_map = InputParser::parse_MathList<std::vector<std::vector<size_t>>>(mm);
        }
        [[fallthrough]];
        case 3: topology_name = mlist[2]; break;
    };
}

std::string math_string(const lGraph::IntegralGraph& is) {
    using ::Caravel::math_string;
    std::string s{};
    s += "IntegralGraph[" + is.get_insertion_string() + ", ";
    s += math_string(is.get_graph());

    if (!is.get_topology_name().empty()) { s += ", \"" + is.get_topology_name() + "\""; }

    if (!is.to_representative_kinematics_map.empty()) { s += ", " + math_list_with_head("MapToRepresentative",is.to_representative_kinematics_map); }
    s += ", " + is.get_graph().get_propagators_string();
    s += "]";
    return s;
}

std::ostream& operator<<(std::ostream& s, const IntegralGraph& is) { return s << math_string(is); }

bool IntegralGraph::is_associated_to(const IntegralGraph& iIS) const {
    // check integral insertions
    if (this->get_insertion() != iIS.get_insertion()) return false;
    auto gr1 = (this->get_graph()).get_base_graph();
    auto gr2 = iIS.get_graph().get_base_graph();
    if (!isEqualGraph(gr1, gr2)) return false;
    return true;
}

bool IntegralGraph::is_massless_propagator(int i) const {
    if (this->get_n_loops() != 1) {
        std::cout << "All calls to IntegralGraph::is_massless_propagator(.) so far only coded for 1-loop integrals!! -- Returned false" << std::endl;
        return false;
    }
    // redirect to the single strand
    auto strand = (this->get_graph().get_base_graph()).get_connection(0, 0)[0];
    return strand.is_massless_link(i);
}

bool IntegralGraph::is_massless_vertex(int i) const {
    if (this->get_n_loops() != 1) {
        std::cout << "All calls to IntegralGraph::is_massless_vertex(.) so far only coded for 1-loop integrals!! -- Returned false" << std::endl;
        return false;
    }
    if (i == 1) {
        return (this->get_graph().get_base_graph().get_nodes()[0]).is_on_shell();
    }
    // redirect to the single strand
    auto strand = this->get_graph().get_base_graph().get_connection(0, 0)[0];
    return strand.is_massless_bead(i - 1);
}

bool IntegralGraph::check_on_shell_leg(int v, int p) const {
    if (this->get_n_loops() != 1) {
        std::cout << "All calls to IntegralGraph::check_on_shell_leg(.) so far only coded for 1-loop integrals!! -- Returned false" << std::endl;
        return false;
    }
    // redirect to the single strand
    auto strand = this->get_graph().get_base_graph().get_connection(0, 0)[0];

    if (v == 1) {
        auto node = this->get_graph().get_base_graph().get_nodes();
        Bead bnode(node[0].get_legs());
        if (!bnode.is_on_shell()) return false;
        std::vector<size_t> link_mass_idx = strand.get_link_mass_indices();
        if (bnode.get_mass_index() == link_mass_idx[p - 1])
            return true;
        else
            return false;
    }
    else {
        return strand.check_on_shell_leg(v - 1, p);
    }
}

size_t IntegralGraph::vertex_size(int i) const {
    if (this->get_n_loops() != 1) {
        std::cerr << "ERROR: All calls to IntegralGraph::vertex_size(.) so far only coded for 1-loop integrals! Exit 1" << std::endl;
        std::exit(1);
        return 0;
    }
    if(i==0){
        std::cerr << "ERROR: Wrong call IntegralGraph::vertex_size(0), the index is 1-based!" << std::endl;
        std::exit(1);
        return 0;
    }
    if(i==1)
        return this->get_graph().get_base_graph().get_nodes()[0].size();
    // redirect to the single strand
    auto strand = this->get_graph().get_base_graph().get_connection(0, 0)[0];
    return strand.bead_size(i-1);
}

size_t IntegralGraph::get_on_shell_vertex_mass_index(int i) const {
    if (this->get_n_loops() != 1) {
        std::cout << "All calls to IntegralGraph::get_on_shell_vertex_mass_index(.) so far only coded for 1-loop integrals!! -- Returned 0" << std::endl;
        return 0;
    }
    // the first bead is not part of the strand and has to be treated separately
    if (i == 1) {
        return (this->get_graph().get_base_graph().get_nodes()[0]).get_mass_index();
    }
    if (i == (int)(this->number_of_vertices() + 1)) {
        return (this->get_graph().get_base_graph().get_nodes()[0]).get_mass_index();
    }
    // redirect to the single strand
    auto strand = this->get_graph().get_base_graph().get_connection(0, 0)[0];
    return strand.get_bead_mass_index(i - 1);
}

size_t IntegralGraph::get_propagator_mass_index(int i) const {
    if (this->get_n_loops() != 1) {
        std::cout << "All calls to IntegralGraph::get_propagator_mass_index(.) so far only coded for 1-loop integrals!! -- Returned 0" << std::endl;
        return 0;
    }
    // redirect to the single strand
    auto strand = this->get_graph().get_base_graph().get_connection(0, 0)[0];
    return strand.get_link_mass_index(i);
}

size_t IntegralGraph::number_of_vertices() const {
    // # vertices is 1 for a 1-loop xGraph, and we add the numbers of beads in its connection
    // xGraph     base_graph         0-0 connection  Strand
    return get_graph().get_n_vertices();
}

const std::vector<size_t> IntegralGraph::vertex_momentum_indices(int i) const {

    if (this->get_n_loops() != 1) {
        std::cout << "All calls to IntegralGraph::vertex_momentum_indices(.) so far only coded for 1-loop integrals!! -- Returned first vec" << std::endl;
        // return strand.vertex_momentum_indices(0);
    }
    // the first bead is not part of the strand and has to be treated separately
    if (i == 1) {
        return (this->get_graph().get_base_graph().get_nodes()[0]).get_momentum_indices();
    }
    if (i == (int)(this->number_of_vertices() + 1)) {
        return (this->get_graph().get_base_graph().get_nodes()[0]).get_momentum_indices();
    }
    // redirect to the single strand
    auto strand = this->get_graph().get_base_graph().get_connection(0, 0)[0];
    return strand.bead_momentum_indices(i - 1);
}

void IntegralGraph::print_info(std::ostream& outstr) const {
    outstr << "IntegralGraphInfo:" << std::endl;
    outstr << this << std::endl;
}

void IntegralGraph::set_integral_insertion(const IntegralInsertion& in) { insertion_enum = in; insertion=wise_enum::to_string(insertion_enum); }

IntegralInsertion IntegralGraph::get_insertion() const { return insertion_enum; }

std::string IntegralGraph::get_insertion_string() const { return insertion; }

std::string IntegralGraph::get_topology_name() const { return topology_name; }

int IntegralGraph::get_n_loops() const { return base_graph.get_n_loops(); }

void IntegralContainer::process_graph(const IntegralInsertion& insertion, const xGraph& xG,int cut) {
    IntegralGraph checkint(insertion, xG);

    // check if integral is to be considered (for example, a pentagon won't be stored)
    if (checkint.get_n_loops() == -1) {
        DEBUG_MESSAGE("No integral considered for cut: " ,xG , " -- IntegralContainer::process_cutTopology(.) did nothing!");
        // sanity check before adding to map
        if (map.find(std::make_pair(cut, insertion)) == map.end()) {
            // no integral
            map[std::make_pair(cut, insertion)] = 0;
        }
        else {
            std::cout << "ERROR!! Found no integral associated with: " << xG << " but its index: " << cut
                      << " was already present in the integral map!! -- did nothing" << std::endl;
        }
        return;
    }

    // check if checkint matches any integral already stored
    bool matchfound(false);
    size_t tomap(0);
    for (size_t ii = 0; ii < integrals.size(); ii++) {
        const IntegralGraph& local(integrals[ii]);
        if (checkint.is_associated_to(local)) {
#if _DEBUG_INTCONTAINER
            std::cout << "Match for: " << local << std::endl;
#endif
            tomap = ii;
            matchfound = true;
            break;
        }
    }
    // new integral
    if (!matchfound) {
#if _DEBUG_INTCONTAINER
        std::cout << "New integral found: " << checkint << std::endl;
#endif
        // store it
        integrals.push_back(checkint);
        // sanity check before adding to map
        if (map.find(std::make_pair(cut, insertion)) == map.end()) { map[std::make_pair(cut, insertion)] = integrals.size(); }
        else {
            std::cout << "ERROR!! Found new integral associated with: " << xG << " but its index: " << cut
                      << " was already present in the integral map!! -- did nothing" << std::endl;
        }
        // create new entry in joined_cuts
        joined_cuts.push_back(std::vector<int>({cut}));
    }
    // existing integral
    else {
        // sanity check before adding to map
        if (map.find(std::make_pair(cut, insertion)) == map.end()) { map[std::make_pair(cut, insertion)] = tomap + 1; }
        else {
            std::cout << "ERROR!! Found old integral associated with: " << xG << " but its index: " << cut
                      << " was already present in the integral map!! -- did nothing" << std::endl;
        }
        // add entry in joined_cuts
        joined_cuts[tomap].push_back(cut);
    }
}

size_t IntegralContainer::size() const { return integrals.size(); }

size_t IntegralContainer::size(int nls) const {
    if (nls <= 0) return 0;
    size_t toret(0);
    for (auto& integral : integrals) {
        if (integral.get_n_loops() == nls) toret++;
    }
    return toret;
}

size_t IntegralContainer::get_integral_index(int cut, const IntegralInsertion& insertion) const { return map.at(std::make_pair(cut, insertion)); }

IntegralGraph IntegralContainer::get_integral_graph(int cut, const IntegralInsertion& insertion) const {
    size_t li(map.at(std::make_pair(cut, insertion)));
    return integrals[li - 1];
}

const std::vector<IntegralGraph>& IntegralContainer::get_all_integral_graphs() const { return integrals; }

bool operator==(const IntegralGraph& g1, const IntegralGraph& g2) {
    return ( g1.get_graph().get_topology() == g2.get_graph().get_topology() ) && ( g1.get_insertion_string() == g2.get_insertion_string() );
}


std::string IntegralGraph::get_readable_graph() const {
    std::stringstream ss;

    auto& graph = get_graph();
    ss << graph.get_base_graph().get_diag_name() << "[" << get_insertion_string() << ", " << graph.get_propagators() << "]";

    return ss.str();
}


bool operator==(const IntegralInfo& ii1, const IntegralInfo& ii2) {
    return ii1.graph == ii2.graph && ii1.insertion_name == ii2.insertion_name && ii1.topology_name == ii2.topology_name && ii1.map_to_representative==ii2.map_to_representative;
}

IntegralInfo::IntegralInfo(const MathList& mlist) {
    assert_head(mlist, "IntegralGraph");

    if(mlist.size() != 4) {
        throw std::invalid_argument("Incomplete IntegralGraph input "+math_string(mlist));
    }

    insertion_name = mlist[0];

    graph = decltype(graph){MathList(mlist[1])};

    topology_name = mlist[2];

    if (topology_name == "Null") {
        _WARNING("Warning: ", mlist, " does not provide topology name.");
        topology_name.clear();
    }

    MathList mm(mlist[3], "MapToRepresentative");
    map_to_representative = InputParser::parse_MathList<std::vector<std::vector<size_t>>>(mm);

    if (map_to_representative == decltype(map_to_representative){}) {
        _WARNING("Warning: ", mlist, " does not name provide MapToRepresentative");
    }
}

std::string math_string(const IntegralInfo& is) {
    using ::Caravel::math_string;

    std::string s{};
    s += "IntegralGraph[\"" + is.insertion_name + "\", ";
    s += math_string(is.graph);

    s += ", \"" + is.topology_name + "\""; 

    s += ", " + math_list_with_head("MapToRepresentative",is.map_to_representative); 
    s += "]";
    return s;
}


std::ostream& operator<< (std::ostream& ostr, const IntegralInfo& ii2) {
    return ostr << math_string(ii2);
}



} // namespace lGraph

} // namespace Caravel


namespace std {

size_t hash<Caravel::lGraph::IntegralGraph>::operator()(const Caravel::lGraph::IntegralGraph& l) const {
    hash<std::pair<Caravel::lGraph::Graph<Caravel::lGraph::Node, Caravel::lGraph::Connection<Caravel::lGraph::Strand>>, std::string>> hash_function;
    return hash_function(std::make_pair(l.get_graph().get_topology(), l.get_insertion_string()));
}

size_t hash<Caravel::lGraph::IntegralInfo>::operator()(const Caravel::lGraph::IntegralInfo& l) const {
    hash<std::tuple<Caravel::lGraph::Graph<Caravel::lGraph::Node, Caravel::lGraph::Connection<Caravel::lGraph::Strand>>, std::string,std::string,std::vector<std::vector<size_t>>>> hash_function;
    return hash_function(std::make_tuple(l.graph, l.insertion_name, l.topology_name,l.map_to_representative));
}


}
