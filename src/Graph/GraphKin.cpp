#include "Graph/GraphKin.h"

namespace Caravel {
namespace lGraph {
namespace lGraphKin {

namespace _misc {
void map_onshell_indices_for_1loop_rotation(std::vector<std::pair<Bead, Link>>& one_loop, const std::map<size_t, size_t>& imap) {
    std::vector<std::pair<Bead, Link>> toret;
    for (auto e : one_loop) {
        Bead b;
        if (e.first.is_on_shell()) {
            b = Bead(Leg(true, imap.at(e.first.get_mass_index()), e.first.get_legs()[0].get_momentum_index()));
        }
        else { b = e.first; }
        Link l = Link(imap.at(e.second.get_mass_index()));
        toret.push_back(std::make_pair(b, l));
    }
    one_loop = toret;
}
}

int get_1loop_rotation(const xGraph& xg){
    const auto& g = xg.get_base_graph();

    // collect the Node and all Beads in Strand into a single container
    const Strand& strand = g.get_connection(0, 0)[0];
    const std::vector<Bead>& lbeads = strand.get_beads();
    const std::vector<Link>& llinks = strand.get_links();

    // construct a Bead build of the legs of Node
    Bead node(g.get_nodes()[0].get_legs());

    // construct a container of all Beads and Links
    std::vector<std::pair<Bead, Link>> one_loop = {{node, llinks[0]}};
    for (size_t ii = 0; ii < lbeads.size(); ii++) one_loop.push_back({lbeads[ii], llinks[ii + 1]});

    // and filter
    for(auto& e:one_loop)    e = {e.first.filter(), e.second.filter()};
    // and map on-shell indices
    _misc::map_onshell_indices_for_1loop_rotation(one_loop, xg.get_canonical_onshell_indices());
    
    // now the proper for topology
    const auto& t = xg.get_topology();

    // collect the Node and all Beads in Strand into a single container
    const Strand& tstrand = t.get_connection(0, 0)[0];
    const std::vector<Bead>& tlbeads = tstrand.get_beads();
    const std::vector<Link>& tllinks = tstrand.get_links();

    // construct a Bead build of the legs of Node
    Bead tnode(t.get_nodes()[0].get_legs());

    // construct a container of all Beads and Links
    std::vector<std::pair<Bead, Link>> one_loop_topo = {{tnode, tllinks[0]}};
    for (size_t ii = 0; ii < tlbeads.size(); ii++) one_loop_topo.push_back({tlbeads[ii], tllinks[ii + 1]});

    for(int ii=0;ii<=int(tlbeads.size());ii++){
        if(one_loop==one_loop_topo) return ii;
        std::rotate(one_loop.begin(), one_loop.begin() + 1, one_loop.end());
    }

    std::cerr<<"ERROR: get_1loop_rotation(.) did not find rotation!"<<std::endl;
    g.show(std::cerr);
    t.show(std::cerr);
    std::exit(1);
    return -1;
}

void one_loop_add_momentum_to_map(std::unordered_map<size_t, std::vector<size_t>>& map_indices, const xGraph& from, const xGraph& to) {
    // find relative rotations
    int rot_from = get_1loop_rotation(from);
    int rot_to = get_1loop_rotation(to);
    int nn = int(to.get_n_legs());

    std::vector<std::vector<size_t>> from_indices;
    from_indices.push_back( from.get_base_graph().get_nodes()[0].get_momentum_indices() );
    const auto& from_strand = from.get_base_graph().get_connection(0, 0)[0];
    for(int jj=0;jj+1<nn;jj++)
        from_indices.push_back( from_strand.get_beads()[jj].get_momentum_indices() );

    std::vector<size_t> to_indices;
    to_indices.push_back( to.get_base_graph().get_nodes()[0].get_momentum_indices()[0] );
    const auto& to_strand = to.get_base_graph().get_connection(0, 0)[0];
    for(int jj=0;jj+1<nn;jj++)
        to_indices.push_back( to_strand.get_beads()[jj].get_momentum_indices()[0] );

    for(int ii=0;ii<nn;ii++){
        map_indices[to_indices[(ii+rot_to)%nn]] = from_indices[(ii+rot_from)%nn];
    }
}

void add_node_momentum_to_map(std::unordered_map<size_t, std::vector<size_t>>& map_indices, const Node& tonode, const xGraph& from, const xGraph& to) {
    int llabel(tonode.get_permutation_tracking_label());
    bool onshell(tonode.is_on_shell());
    // sanity check
    if (llabel < 0) {
        std::cerr << "ERROR: called add_node_momentum_to_map(.) without assigning tracking labels!" << std::endl;
        to.show();
        std::exit(7716235);
    }
    // the index associated with the leg attached to node
    size_t pi(tonode.get_momentum_indices()[0]);
    int llabel_from(-1);
    // now find the corresponding 'llabel' in 'from' xGraph
    for (size_t ii = 0; ii < to.get_topology().get_nodes().size(); ii++) {
        const auto& tnode = to.get_topology().get_nodes()[ii];
        if (llabel == tnode.get_permutation_tracking_label()) {
            llabel_from = from.get_topology().get_nodes()[ii].get_permutation_tracking_label();
            break;
        }
    }
    if (llabel_from < 0) {
        std::cerr << "ERROR: Failed to find corresponding node with label " << llabel << " in 'to', in the 'from' xGraph! Call in add_node_momentum_to_map(.)" << std::endl;
        from.show();
        to.show();
        std::exit(7716236);
    }
    // now retrieve the vector<size_t> associated to 'pi'
    for (const auto& fromnode : from.get_base_graph().get_nodes()) {
        if (fromnode.get_permutation_tracking_label() == llabel_from) { 
            // sanity check
            if(onshell!=fromnode.is_on_shell()){
                std::cerr << "ERROR: Mismatch in momentum mapper! Corresponding momentum in 'to' graph: "<< pi << std::endl;
                from.show();
                to.show();
                std::exit(7716237);
            }
            // strengthen even more the check
            else if(onshell){
                // check that onshell labels match
                if(tonode.get_mass_index()!=fromnode.get_mass_index()){
                    std::cerr << "ERROR: Mismatch in on-shell mass index in momentum mapper! Corresponding momentum in 'to' graph: "<< pi << std::endl;
                    from.show();
                    to.show();
                    std::exit(7716238);
                }
            }
            map_indices[pi] = fromnode.get_momentum_indices(); 
        }
    }
}

void add_beads_momenta_to_map(std::unordered_map<size_t, std::vector<size_t>>& map_indices, bool reverse, const Strand& fromstrand,
                               const Strand& tostrand, const xGraph& from, const xGraph& to) {
    size_t n_beads = tostrand.get_beads().size();
    // traverse Beads
    for(size_t ii=0; ii<n_beads; ii++){
        const auto& tobead = tostrand.get_beads()[ii];
        const auto& frombead = (reverse ? fromstrand.get_beads()[n_beads - ii - 1] : fromstrand.get_beads()[ii]);
        size_t pi(tobead.get_momentum_indices()[0]);
        // sanity check
        if (tobead.is_on_shell() != frombead.is_on_shell()) {
            std::cerr << "ERROR: Mismatch in momentum mapper (add_beads_momenta_to_map(.))! Corresponding momentum in 'to' graph: " << pi << std::endl;
            from.show();
            to.show();
            std::exit(7716241);
        }
        // strengthen even more the check
        else if (tobead.is_on_shell()) {
            // check that onshell labels match
            if (tobead.get_mass_index() != frombead.get_mass_index()) {
                std::cerr << "ERROR: Mismatch in on-shell mass index in momentum mapper (add_beads_momenta_to_map(.)! Corresponding momentum in 'to' graph: " << pi << std::endl;
                from.show();
                to.show();
                std::exit(7716242);
            }
        }
        map_indices[pi] = frombead.get_momentum_indices();
    }
}

void add_strand_momenta_to_map(std::unordered_map<size_t, std::vector<size_t>>& map_indices, const std::pair<size_t, size_t> tofromnodes,
                               const Strand& tostrand, const xGraph& from, const xGraph& to) {
    std::pair<int,int> clabels = { to.get_base_graph().get_nodes()[tofromnodes.first].get_permutation_tracking_label(),
                                            to.get_base_graph().get_nodes()[tofromnodes.second].get_permutation_tracking_label() };
    // sanity check
    if (clabels.first < 0 || clabels.second < 0) {
        std::cerr << "ERROR: called add_strand_momenta_to_map(.) without assigning tracking labels!" << std::endl;
        to.show();
        std::exit(7716237);
    }
    int slabel = tostrand.get_permutation_tracking_label();
    // sanity check
    if (slabel < 0) {
        std::cerr << "ERROR: called add_strand_momenta_to_map(.) without assigning tracking labels for a strand!" << std::endl;
        to.show();
        std::exit(7716238);
    }
    // now find the corresponding 'clabels' in 'from' xGraph
    std::pair<int,int> clabels_from = { -1 , -1 };
    std::pair<size_t,size_t> tofromnodes_topology = { 0 , 0 };
    for (size_t ii = 0; ii < to.get_topology().get_nodes().size(); ii++) {
        const auto& tnode = to.get_topology().get_nodes()[ii];
        if (clabels.first == tnode.get_permutation_tracking_label()) {
            clabels_from.first = from.get_topology().get_nodes()[ii].get_permutation_tracking_label();
            tofromnodes_topology.first = ii;
        }
        if (clabels.second == tnode.get_permutation_tracking_label()) {
            clabels_from.second = from.get_topology().get_nodes()[ii].get_permutation_tracking_label();
            tofromnodes_topology.second = ii;
        }
    }
    if (clabels_from.first < 0 || clabels_from.second < 0) {
        std::cerr << "ERROR: Failed to find corresponding nodes with labels " << clabels << " in 'to', in the 'from' xGraph! Call in add_strand_momenta_to_map(.)" << std::endl;
        from.show();
        to.show();
        std::exit(7716239);
    }
    if(to.get_topology().get_connection_map().find(tofromnodes_topology)==to.get_topology().get_connection_map().end()){
        // should be the reversed
        std::pair<size_t,size_t> tofromnodes_topology_reverse = { tofromnodes_topology.second , tofromnodes_topology.first };
        if(to.get_topology().get_connection_map().find(tofromnodes_topology_reverse)==to.get_topology().get_connection_map().end()){
            std::cerr<<"ERROR: failed to find connection in 'to' xGraph in add_strand_momenta_to_map(.)"<<std::endl;
            from.show();
            to.show();
            std::exit(7716244);
        }
        tofromnodes_topology=tofromnodes_topology_reverse;
    }
    // now find the corresponding connection in 'from' xGraph (we also tag if a "reverse" is needed)
    std::pair<size_t,size_t> from_connection;
    bool reverse (false);
    for (size_t ii = 0; ii < from.get_base_graph().get_nodes().size(); ii++) {
        const auto& fnode = from.get_base_graph().get_nodes()[ii];
        if (clabels_from.first == fnode.get_permutation_tracking_label()) {
            from_connection.first = ii;
        }
        if (clabels_from.second == fnode.get_permutation_tracking_label()) {
            from_connection.second = ii;
        }
    }
    // in case the connection isn't present
    if(from.get_base_graph().get_connection_map().find(from_connection)==from.get_base_graph().get_connection_map().end()){
        std::pair<size_t,size_t> from_connection_reverse = { from_connection.second , from_connection.first };
        // it should then be the reverse!
        if(from.get_base_graph().get_connection_map().find(from_connection_reverse)==from.get_base_graph().get_connection_map().end()){
            std::cerr << "ERROR: Did not find connection " << from_connection << " (or reversed) in the 'from' xGraph!" << std::endl;
            from.show();
            std::exit(7716240);
        }
        from_connection = from_connection_reverse;
        reverse = true;
    }
    // find out which strand in the connection we need to target
    int slabel_from(-1);
    size_t thestrand(0);
    for(size_t ii=0;ii<to.get_topology().get_connection_map().at(tofromnodes_topology).get_edges().size();ii++){
        const auto& topostrand = to.get_topology().get_connection_map().at(tofromnodes_topology).get_edges()[ii];
        if(topostrand.get_permutation_tracking_label() == slabel){
            const auto& topostrand_from = from.get_topology().get_connection_map().at(tofromnodes_topology).get_edges()[ii];
            slabel_from = topostrand_from.get_permutation_tracking_label();
            break;
        }
    }
    if (slabel_from < 0) {
        std::cerr << "ERROR: Failed to find corresponding strand with label " << slabel << " in 'to', in the 'from' xGraph! Call in add_strand_momenta_to_map(.)" << std::endl;
        from.show();
        to.show();
        std::exit(7716242);
    }
    bool found(false);
    for(size_t ii=0;ii<from.get_base_graph().get_connection_map().at(from_connection).get_edges().size();ii++){
        const auto& lstrand = to.get_base_graph().get_connection_map().at(tofromnodes_topology).get_edges()[ii];
        if(lstrand.get_permutation_tracking_label() == slabel_from){
            thestrand=ii;
            found=true;
            break;
        }
    }
    if (!found) {
        std::cerr << "ERROR: Failed to find strand with label " << slabel_from << " in base_graph of the 'from' xGraph!" << std::endl;
        from.show();
        from.show_topology();
        std::exit(7716243);
    }
    // now traverse the Beads and add to map
    const auto& corresponding_strand = from.get_base_graph().get_connection_map().at(from_connection)[thestrand];

    // when from_connection.first == from_connection.second (that is, we have strands in a self-connection), special care required for 'reverse'
    if (from_connection.first == from_connection.second) {
        // we pick first strand in the connection, just because the reverse info is the same to all of the strands contained in the connection
        const auto& tostrand_topology = to.get_topology().get_connection_map().at(tofromnodes_topology)[0];
        const auto& fromstrand_topology = from.get_topology().get_connection_map().at(tofromnodes_topology)[0];
        bool to_reversed = tostrand_topology.get_has_been_reversed();
        bool from_reversed = fromstrand_topology.get_has_been_reversed();
        // in this case, we need a reverse!
        if (to_reversed != from_reversed) reverse = true;
    }

    add_beads_momenta_to_map(map_indices,reverse,corresponding_strand,tostrand,from,to);
}

std::vector<std::vector<size_t>> abstract_external_momentum_mapper(const xGraph& from, const xGraph& to) {
    // For now we require 'to' to have only simple momentum insertions (see documentation on the hearder files!)
    if (!to.has_only_simple_mom_insertions()) {
        std::cerr << "ERROR: abstract_external_momentum_mapper(.) requires (for now) 'to' xGraph (printed next) to have only simple momentum insertions! Exit 55128" << std::endl;
        to.show();
        std::exit(55128);
    }

    // sanity check
    if (!(from.get_topology() == to.get_topology())) {
        std::cerr << "ERROR: abstract_external_momentum_mapper(.) called for two graphs not topologically equivalent! Topologies printed next, then exit 55127"
                  << std::endl;
        from.show_topology();
        to.show_topology();
        // // check if they are subleading poles related
        // if(same_structures(from,to)){
        //     if(from.is_factorizable()||to.is_factorizable()){
        //         std::exit(55127);
        //     }
        //     // only semisimple diagrams with a massless bubble can be subleading pole related
        //     if(from.get_base_graph().get_nodes()[0].size()>0&&from.get_base_graph().get_nodes()[1].size()==0&&from.get_base_graph().get_connection(0,1)[0].is_plain()&&from.get_base_graph().get_connection(0,1)[1].is_plain()){              
        //         std::map<std::pair<size_t,size_t>,Connection<Strand>> new_map;
        //         std::vector<Node> new_nodes;
        //         std::vector<Link> new_links;
        //         std::vector<Bead> new_beads;
        //         for(auto& )

        //     }
        //     else if (from.get_base_graph().get_nodes()[1].size()>0&&from.get_base_graph().get_nodes()[0].size()==0&&from.get_base_graph().get_connection(0,1)[0].is_plain()&&from.get_base_graph().get_connection(0,1)[1].is_plain()) {
        //         std::map<std::pair<size_t,size_t>,Connection<Strand>> new_map;
        //         std::vector<Node> new_nodes;
        //     }
        //     else{
        //         std::exit(55127);
        //     }
        // }
        // else{
            std::exit(55127);
        //}
    }

    std::unordered_map<size_t,std::vector<size_t>> map_indices;

    // 1-loop mapping is particular due to forced node
    if (from.get_n_loops() == 1) {
        one_loop_add_momentum_to_map(map_indices, from, to);
    }
    else {
        // traverse all nodes
        for (const auto& node : to.get_base_graph().get_nodes())
            // only nodes with attached leg matter
            if (node.size() > 0) add_node_momentum_to_map(map_indices, node, from, to);

        // traverse all strands
        for (const auto& con : to.get_base_graph().get_connection_map())
            for (const auto& strand : con.second.get_edges()) add_strand_momenta_to_map(map_indices, con.first, strand, from, to);
    }

    std::vector<std::vector<size_t>> momenta_from_indices;
    for(size_t ii=0;ii<to.get_n_legs();ii++){
        if(map_indices.find(ii+1)==map_indices.end()){
            std::cerr<<"ERROR: did not find momentum "<<ii+1<<" in the 'to' xGraph!"<<std::endl;
            to.show();
            std::exit(55129);
        }
        momenta_from_indices.push_back(map_indices.at(ii+1));
    }

    DEBUG(
        std::cout<<"FROM: "<<from<<std::endl;
        std::cout<<"TO: "<<to<<std::endl;
        for(size_t ii=0;ii<momenta_from_indices.size();ii++)
            std::cout<<"k_to_"<<ii+1<<" = k_from_"<<momenta_from_indices[ii]<<std::endl;
    );

    return momenta_from_indices;
}


} // namespace GraphKin
} // namespace lGraph
} // namespace Caravel

