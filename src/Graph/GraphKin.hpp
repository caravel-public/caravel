#include <exception>
#include "Core/Debug.h"
#include "Core/Particle.h"

namespace Caravel {
namespace lGraph {
namespace lGraphKin {
using Caravel::operator<<;

template <typename T, size_t D>
GraphKin<T, D>::GraphKin(const xGraph& graph)
    : compute_common_transverse{OnShellStrategies::common_transverse_dual_evaluator<T, D>(graph)}, 
      compute_strand_by_strand_common_transverse{OnShellStrategies::common_transverse_strand_by_strand_dual_evaluator<T, D>(graph)},
      compute_transverse_dual{OnShellStrategies::transverse_dual_evaluator<T, D>(graph)} {
    if (graph.get_n_loops() == 2)
        getISPmomenta = OnShellStrategies::TwoLoopISP_momenta_evaluator<T, D>(graph);
    else {
        // do nothing
        std::function<std::vector<std::vector<momentumD<T, D>>>(const momD_conf<T, D>&, const std::vector<std::vector<momD<T, D>>>&)> lambda =
            [](const momD_conf<T, D>&, const std::vector<std::vector<momD<T, D>>>&) { return std::vector < std::vector<momentumD<T, D>>>(); };
        getISPmomenta = lambda;
    }
}

template <typename T, size_t D> GraphKin<T, D>::GraphKin(const xGraph& graph, const momD_conf<T, D>& momc) : GraphKin{graph} {
    change_point(momc);
}

template <typename T, size_t D> void GraphKin<T,D>::change_point(const momD_conf<T, D>& momc){
    momconf = &momc;
    std::tie(transverse, dual) = compute_transverse_dual(momc);
    std::tie(common_transverse, strand_by_strand_common_dual) = compute_strand_by_strand_common_transverse(momc);
    std::tie(common_transverse, common_dual) = compute_common_transverse(momc);
    non_common_momenta = getISPmomenta(momc, transverse);
    if (compute_ibp_vector_coefficients) ibp_vector_coefficients = compute_ibp_vector_coefficients(momc);
    if (compute_surface_terms_coefficients) surface_term_coefficients = compute_surface_terms_coefficients(momc, ibp_vector_coefficients);
    mass = ParticleMass::get_vector_of_real_masses<T>();
}

template <typename T, size_t D> const momD_conf<T, D>& GraphKin<T, D>::get_mom_conf() const {
    if (momconf == nullptr) { throw std::runtime_error("[GraphKin] Attempted to access a non-existent momentum configuration."); }
    return *momconf;
}

// bool same_structures(const xGraph& g1, const xGraph& g2){
//     if(!(g1.get_n_legs() = g2.get_n_legs())) return false;
//     if(!(g1.get_n_vertices() = g2.get_n_vertices())) return false;
//     if(!(g1.get_n_edges() = g2.get_n_edges())) return false;
//     if(!(g1.is_planar() = g2.is_planar())) return false;
//     return true;
// }

template <typename T, size_t D>
std::function<std::vector<momD<T, D>>(const momD_conf<T, D>&)> external_momentum_mapper(const xGraph& from, const xGraph& to) {
    auto momenta_from_indices = abstract_external_momentum_mapper(from, to);
    size_t n_mom(momenta_from_indices.size());

    DEBUG(
        std::cout<<"external_momentum_mapper result: "<<std::endl;
        std::cout<<"FROM: "<<from<<std::endl;
        std::cout<<"TO: "<<to<<std::endl;
        for(size_t ii=0;ii<momenta_from_indices.size();ii++){
            std::cout<<"k_to_"<<ii+1<<" = ";
            for(size_t jj=0;jj<momenta_from_indices[ii].size();jj++){
                std::cout<<" k_from_"<<momenta_from_indices[ii][jj];
                if(jj+1!=momenta_from_indices[ii].size()) std::cout<<" +";
            }
            std::cout<<std::endl;
        }
        std::cout<<std::endl;
    );

    std::function<std::vector<momD<T, D>>(const momD_conf<T, D>&)> lambda = [n_mom,momenta_from_indices](const momD_conf<T, D>& mc) {
        std::vector<momD<T, D>> toret(n_mom);

        for (size_t ii=0;ii<n_mom;ii++) toret[ii] = mc.Sum(momenta_from_indices[ii]);

        return toret;
    };

    return lambda;
}

template <typename T, size_t D>
std::function<momD<T, D>(const std::vector<momD<T, D>>&, const momD_conf<T, D>&)> MomentumRoutingEvaluator(const MomentumRouting& mr){
    return [mr](const std::vector<momD<T, D>>& os, const momD_conf<T, D>& mconf){
            momD<T, D> toret;
            const auto& internal = mr.get_internal();
            const auto& external = mr.get_external();
            for(const auto& i:internal.plus_momenta) toret+=os[i-1];
            for(const auto& i:internal.minus_momenta) toret-=os[i-1];
            for(const auto& i:external.plus_momenta) toret+=mconf[i];
            for(const auto& i:external.minus_momenta) toret-=mconf[i];
            return toret;
        };
}

namespace _detail {
template <typename T> bool is_zero(T x) {
    if constexpr (is_floating_point_v<T>) {
        using TR = remove_complex_t<T>;
        using std::fabs;
        if (fabs(x) > DeltaZeroSqr<TR>())
            return false;
        else
            return true;
    }
    else if constexpr (is_exact_v<T>) { return x == T(0); }
    else if constexpr (is_iterable_v<T>) {
        for (const auto& it : x) {
            if (!::Caravel::lGraph::lGraphKin::_detail::is_zero(it)) return false;
        }
        return true;
    }
    throw std::runtime_error("Not implemented");
}
} // namespace _detail

template <typename T, size_t D> std::function<size_t(const OnShellPoint<T, D>&, const momD_conf<T, D>&)> LoopOnShellChecker(const xGraph& xg) {
    auto props = xg.get_propagators();
    using mrEval = std::function<momD<T, D>(const std::vector<momD<T, D>>&, const momD_conf<T, D>&)>;
    std::vector<mrEval> momenta;
    for (auto& p : props) momenta.push_back(MomentumRoutingEvaluator<T, D>(p.mom));
    return [props, momenta](const OnShellPoint<T, D>& os, const momD_conf<T, D>& mconf) {
        size_t errors(0);
        auto masses = ParticleMass::get_vector_of_real_masses<T>();
        for (size_t i = 0; i < props.size(); i++) {
            auto p = momenta[i](os, mconf);
            auto rho = p * p;
            rho -= masses[props[i].mass] * masses[props[i].mass];
            if (!_detail::is_zero(rho)) {
                std::cerr << "WARNING: failed on-shell condition for propagator " << props[i] << " (value: " << rho << ")" << std::endl;
                errors++;
            }
        }
        return errors;
    };
}

} // namespace GraphKin
} // namespace lGraph
} // namespace Caravel
