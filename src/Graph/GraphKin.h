/**
 * @file GraphKin.h
 *
 * @date 8.3.2019
 *
 * @brief Here we collect some few functionalities from Graph to be applied to kinematic evaluations in Caravel
 */
#ifndef GRAPHKIN_H_
#define GRAPHKIN_H_

#include "Graph.h"
#include "OnShellStrategies/osm.h"
#include "Core/momD_conf.h"
#include "Core/Debug.h"
#include "Core/type_traits_extra.h"
#include "misc/DeltaZero.h"

namespace Caravel {
namespace lGraph {
namespace lGraphKin {

template <typename T, size_t D> class GraphKin {
    std::vector<std::vector<momD<T, D>>>
        transverse; /**< For each strand with a loop momentum assignment, stores a vNV transverse set of vectors (ordered from l1, ... ,ln) */
    std::vector<std::vector<momD<T, D>>>
        dual; /**< For each strand with a loop momentum assignment, stores a vNV dual set of vectors (ordered from l1, ... ,ln) */
    std::vector<momD<T, D>> common_transverse; /**< Stores a set of momenta transverse to all the xGraph */
    std::vector<momD<T, D>> common_dual;       /**< Stores a set of dual momenta with an ordering defined by the functor member 'compute_common_transverse' */
    std::vector<std::vector<momD<T, D>>> strand_by_strand_common_dual; /**< For each strand with a loop mom assignment, stores a vNV dual basis to the strand's
                                                                          attached external momenta completed with other momenta (to make it common) according
                                                                          to the functor member 'compute_strand_by_strand_common_transverse' */
    std::vector<std::vector<momD<T, D>>> non_common_momenta; /**< These momenta help to complete a set of independent momenta to the externals attached to a
                                                                strand with loop momentum assigned, for computing ISP's */

    std::vector<std::vector<T>> surface_term_coefficients; /**< Stores coefficients which are solution of the linear system for power counting counting reduction */
    std::vector<std::vector<T>> ibp_vector_coefficients; /**< Stores coefficients which are solution of the linear system for IBP vectors */
    std::vector<T> mass; /**< Stores values of masses included at run time */

    std::function<std::pair<std::vector<momD<T, D>>, std::vector<momD<T, D>>>(const momD_conf<T, D>&)>
        compute_common_transverse; /**< Used to set 'common_transverse' and 'common_dual' */
    std::function<std::pair<std::vector<momD<T, D>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)>
        compute_strand_by_strand_common_transverse; /**< Used to set 'strand_by_strand_common_dual' */
    std::function<std::pair<std::vector<std::vector<momD<T, D>>>, std::vector<std::vector<momD<T, D>>>>(const momD_conf<T, D>&)>
        compute_transverse_dual; /**< Used to set 'transverse' and 'dual' members */
    std::function<std::vector<std::vector<momentumD<T, D>>>(const momD_conf<T, D>&, const std::vector<std::vector<momD<T, D>>>&)>
        getISPmomenta; /**< Used to set 'non_common_momenta' */
    // Non-owning pointer to the momconf which was used the last time to change_point
    // It is the responsibility of the user that the pointer stored here does not outlive the source
    // TODO: is it really necessary to keep this reference?
    const momD_conf<T, D>* momconf{nullptr};

  public:
    GraphKin(const xGraph&);
    GraphKin() = default;
    /**
     * Construct and also change point immidiately after that.
     */
    GraphKin(const xGraph&, const momD_conf<T, D>&);
    const auto& get_transverse() const { return transverse; }
    const auto& get_common_transverse() const { return common_transverse; }
    const auto& get_dual() const { return dual; }
    const auto& get_common_dual() const { return common_dual; }
    const auto& get_strand_by_strand_common_dual() const { return strand_by_strand_common_dual; }
    const auto& get_non_common_momenta() const { return non_common_momenta; }
    const auto& get_surface_term_coefficients() const { return surface_term_coefficients; }
    const auto& get_ibp_vector_coefficients() const { return ibp_vector_coefficients; }
    const auto& get_masses() const { return mass; }
    const momD_conf<T, D>& get_mom_conf() const;
    /**
     * A function which will update surface term coefficients (in place)
     */
    std::function<decltype(surface_term_coefficients)(const momD_conf<T, D>&, const std::vector<std::vector<T>>&)> compute_surface_terms_coefficients;
    std::function<decltype(surface_term_coefficients)(const momD_conf<T, D>&)> compute_ibp_vector_coefficients;

    void change_point(const momD_conf<T, D>&);
};

/**
 * Function to produce an abstract map of indices between the external momenta of two topologically-related 
 * xGraphs.
 *
 * We force the 'to' xGraph to have only simple momentum insertions. This means that only vectors of 
 * momentum indices like { i } (or {} for a given Node) can be included in the xGraph 'to'. This is 
 * a feature that can be removed later on, but then the return type has to change.
 *
 * The returned array of indices gives the external momenta of 'to' in terms of the external momenta
 * of 'from'
 *
 * This is a key function for the function external_momentum_mapper(.)
 */
std::vector<std::vector<size_t>> abstract_external_momentum_mapper(const xGraph& from, const xGraph& to);

/**
 * Function to produce a map from two topologically-related xGraphs between their external momenta.
 *
 * We force the 'to' xGraph to have only simple momentum insertions. This means that only vectors of 
 * momentum indices like { i } (or {} for a given Node) can be included in the xGraph 'to'. This is 
 * a feature that can be removed later on, but then the return type of the std::function has to change
 * (it can not only be a simple vector of momDs).
 *
 * The returned std::function can be evaluated on a momD_conf<T,D> and it will return a vector of
 * momD<T,D>, where the entry 'i' is related to the 'i+1' momentum in the xGraph 'to'
 */
template <typename T, size_t D>
std::function<std::vector<momD<T, D>>(const momD_conf<T, D>&)> external_momentum_mapper(const xGraph& from, const xGraph& to);

/**
 * It allows kinematical evaluations of a momentum routing from a momD_conf and a vector of loop momenta
 */
template <typename T, size_t D>
std::function<momD<T, D>(const std::vector<momD<T, D>>&, const momD_conf<T, D>&)> MomentumRoutingEvaluator(const MomentumRouting& mr);

/**
 * Given an xGraph, generates a function for checking on-shell conditions for loop momenta
 *
 * Arguments for lambda are an OnShellPoint<T, D> (aka std::vector<momD<T, D>>) and a momD_conf<T, D>,
 * respectively the loop momenta and the external momenta.
 *
 * Return is a size_t counting the number of failed on-shell propagator tests
 */
template <typename T, size_t D> std::function<size_t(const OnShellPoint<T, D>&, const momD_conf<T, D>&)> LoopOnShellChecker(const xGraph&);

} // namespace lGraphKin
} // namespace lGraph
} // namespace Caravel

#include "GraphKin.hpp"

#endif // GRAPHKIN_H_
