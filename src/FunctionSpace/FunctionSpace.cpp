// Handles explicit instantiations for FunctionSpace

#include "FunctionSpace.h"
#include <algorithm>
#include <initializer_list>

#include "Core/typedefs.h"

namespace Caravel {
namespace FunctionSpace {

LoopMonomial::LoopMonomial(const std::vector<size_t>& _contributions) {
    for (size_t i = 0; i < _contributions.size(); i++) {
        if (_contributions.at(i) != 0) { contributions.push_back({i, _contributions.at(i)}); }
    }
}

LoopMonomial::LoopMonomial(std::initializer_list<size_t> _contributions) {
    auto new_contributions = std::vector<size_t>(_contributions);
    for (size_t i = 0; i < new_contributions.size(); i++) {
        if (new_contributions.at(i) != 0) { contributions.push_back({i, new_contributions.at(i)}); }
    }
}

std::vector<std::pair<size_t, size_t>>::iterator LoopMonomial::at_it(size_t n) {
    return std::find_if(contributions.begin(), contributions.end(), [n](std::pair<size_t, size_t> x) { return x.first == n; });
}

size_t LoopMonomial::at(size_t n) const {
    auto result = std::find_if(contributions.begin(), contributions.end(), [n](std::pair<size_t, size_t> x) { return x.first == n; });

    if (result == contributions.end()) { return 0; }
    else {
        return result->second;
    }
}

void LoopMonomial::set(size_t direction, size_t power) {
    auto contribution = at_it(direction);
    if (contribution == contributions.end()) { contributions.push_back({direction, power}); }
    else {
        contribution->second = power;
    }
}

void LoopMonomial::decrease_by(size_t direction, size_t n) {
    auto contribution = at_it(direction);
    if (contribution == contributions.end()) {
        std::cerr << "ERROR: Trying to decrease a non-existant power." << std::endl;
        exit(1);
    }
    else {
        contribution->second -= n;
    }
}

size_t LoopMonomial::power_counting() const {
    size_t result = 0;
    for (auto& term : contributions) { result += term.second; }
    return result;
}

size_t TensorInsertion::power_counting() const {
    size_t result = 0;
    for(auto& t : first) result += t.power_counting();
    for(auto& t : second) result += 2*t;
    return result;
}

std::vector<std::pair<size_t, size_t>>::iterator LoopMonomial::begin() { return contributions.begin(); }
std::vector<std::pair<size_t, size_t>>::iterator LoopMonomial::end() { return contributions.end(); }

std::vector<std::pair<size_t, size_t>>::const_iterator LoopMonomial::begin() const { return contributions.begin(); }
std::vector<std::pair<size_t, size_t>>::const_iterator LoopMonomial::end() const { return contributions.end(); }

std::ostream& operator<<(std::ostream& out, const LoopMonomial& monomial) {
    using Caravel::operator<<;
    out << monomial.contributions;
    return out;
}

std::ostream& operator<<(std::ostream& o, const TensorInsertion& ti) {
    using Caravel::operator<<;
    o << ti.first << ", " << ti.second ;
    if(ti.insertion != IntegralInsertion::undefined)  o<< ", " << ti.insertion;
    return o;
}

// TODO: This function only needs topological information, not a full xGraph
std::vector<TensorInsertion> tensor_insertions(const lGraph::xGraph& gr_in) {
    using std::pair;
    using std::vector;

    auto& gr = gr_in.get_special_routing();

    const bool gravity_power_counting = (settings::general::power_counting_of_theory == settings::general::power_counting::Gravity);

    if (gr.get_n_loops() != 2) {
        std::cerr << "Error: recieved an " << gr.get_n_loops() << "-loop xGraph. Expected 2 loops." << std::endl;
        exit(1);
    }

    auto trans_dims = gr.n_OS_variables_with_special_routing();

    // make sure not to over count variables for the second-loop mom as n_OS_variables_with_special_routing() gives one extra variable (the mu12)
    bool is_factor = (gr.get_base_graph().is_factorizable());
    if (is_factor) --trans_dims.back();

    const size_t num_var1 = trans_dims[0];
    const size_t num_var2 = trans_dims[1];

    // if gravity powercounting is used, there has to be an additional shift in the powercounting
    size_t offset(0);
    if(gravity_power_counting){
        offset = get_gravity_power_couting_offset(gr);
    }    

    const auto lppv = loop_powers_per_vertex(settings::general::power_counting_of_theory);
    if (lppv > 2) {
        _MESSAGE_R("Power counting ", lppv, " has not yet been implemented!");
        std::exit(1);
    }

    // we count powers using the get_one_loop_subdiagram(.) method to align with the corresponding call in topology_coordinates_evaluator_2L(.)
    size_t max_la_power = (gr.get_one_loop_subdiagram(1).second.size()+1) * lppv+offset;
    size_t max_lb_power = (gr.get_one_loop_subdiagram(2).second.size()+1) * lppv+offset;

    //TODO: why +is_factor?
    size_t max_total_power = (gr.get_n_vertices()+is_factor) * lppv+offset;

    vector<size_t> possible_la_powers(max_la_power + 1);
    std::iota(std::begin(possible_la_powers), std::end(possible_la_powers), 0);
    vector<size_t> possible_lb_powers(max_lb_power + 1);
    std::iota(std::begin(possible_lb_powers), std::end(possible_lb_powers), 0);

    // Cauchy product out for la and lb over the monomials
    std::function<vector<size_t>(size_t, vector<size_t>)> appender = [](size_t new_el, vector<size_t> list) {
        list.push_back(new_el);
        return list;
    };

    vector<vector<size_t>> all_possible_la_monomials;
    if (num_var1 > 0) {
        all_possible_la_monomials.push_back({});
        for (size_t i = 0; i < num_var1; i++) {
            all_possible_la_monomials = detail::cauchy_product(possible_la_powers, all_possible_la_monomials, appender);
        }
    }
    if (all_possible_la_monomials.size() == 0) {
        vector<size_t> scalar(num_var1, 0);
        all_possible_la_monomials.push_back(scalar);
    }

    vector<vector<size_t>> all_possible_lb_monomials;
    if (num_var2 > 0) {
        all_possible_lb_monomials.push_back({});
        for (size_t i = 0; i < num_var2; i++) {
            all_possible_lb_monomials = detail::cauchy_product(possible_lb_powers, all_possible_lb_monomials, appender);
        }
    }
    if (all_possible_lb_monomials.size() == 0) {
        vector<size_t> scalar(num_var2, 0);
        all_possible_lb_monomials.push_back(scalar);
    }

    // Pair them up
    std::function<vector<LoopMonomial>(vector<size_t>,vector<size_t>)> loop_monomial_pairer = [](vector<size_t> l1, vector<size_t> l2) {
        vector<LoopMonomial> two_loop_insertion{LoopMonomial(l1), LoopMonomial(l2)};
        return two_loop_insertion;
    };

    vector<vector<LoopMonomial>> all_4d_monomials = detail::cauchy_product(all_possible_la_monomials, all_possible_lb_monomials, loop_monomial_pairer);

    // Build the mu monomials. Simple because there is only more than one if we we have a factorized graph
    size_t max_muab_power = std::min(max_la_power, max_lb_power);
    if (!is_factor) { max_muab_power = 0; }
    vector<RegulatingMonomial> possible_muab_insertions;
    for (size_t i = 0; i < max_muab_power + 1; i++) { possible_muab_insertions.push_back({0, 0, i}); }

    // Pair everything together
    std::function<TensorInsertion(RegulatingMonomial,vector<LoopMonomial>)> tensor_insertion_pairer = [](RegulatingMonomial mus, vector<LoopMonomial> alphas) {
        return TensorInsertion({alphas, mus, IntegralInsertion::undefined});
    };

    auto tensor_insertions = detail::cauchy_product(possible_muab_insertions, all_4d_monomials, tensor_insertion_pairer);

    // Select those with renormalizable power counting
    auto loop_power = [](const TensorInsertion& tensor, size_t loop_i) {
        size_t result = 0;
        auto& mu_part = tensor.second;
        auto& fourD_part = tensor.first;

        result += fourD_part.at(loop_i).power_counting();

        // FIXME: TWO LOOP HACK
        result += mu_part.at(2);
        result += mu_part.at(loop_i) * 2;

        return result;
    };

    auto total_power = [=](const TensorInsertion& monomial) {
        size_t power = 0;

        for (size_t loop_i = 0; loop_i < monomial.first.size(); loop_i++) { power += loop_power(monomial, loop_i); }
        return power;
    };

    DEBUG_MESSAGE("max_la_power", max_la_power);
    DEBUG_MESSAGE("max_lb_power", max_lb_power);

    vector<TensorInsertion> renormalizable_tensor_insertions;
    for (auto& monomial : tensor_insertions) {
        if (loop_power(monomial, 0) > max_la_power) { continue; }
        if (loop_power(monomial, 1) > max_lb_power) { continue; }
        if (total_power(monomial) > max_total_power) { continue; }
        renormalizable_tensor_insertions.push_back(monomial);
    }
    return renormalizable_tensor_insertions;
}

size_t get_gravity_power_couting_offset(const lGraph::xGraph& graph) {
    // computed by assuming only 3-pt vertices
    size_t props_in_maximal = graph.get_n_legs() + 3;
    size_t n_propagators = graph.get_n_edges();
    assert(props_in_maximal >= n_propagators);
    return props_in_maximal - n_propagators;
}

CustomBasis parse_relabel(const std::string& in){
    using namespace lGraph;
    MathList mrelabel(in, "GraphRelabel");
    MathList loops(mrelabel[0], "Loops");
    MathList ext_permutation(mrelabel[1], "ExternalPermutation");

    CustomBasis toret;

    for(auto& l: loops){
        MathList loop(l, "Loop");
        MathList internal(loop[0], "Internal");
        MathList external(loop[1], "External");
        std::vector<int> i;
        for(auto& index: internal)    i.push_back(std::stoi(index));
        std::vector<int> e;
        for(auto& index: external)    e.push_back(std::stoi(index));
        toret.new_internal.push_back( MomentumRouting(i, e) );
    }

    for(auto& index: ext_permutation){
        auto ii = std::stoul(index);
        if(ii==0){
            std::cerr<<"ERROR: permutation labels in ExternalPermutation[] MathList are 1-based! Found a 0"<<std::endl;
            std::exit(1);
        }
        toret.new_external.push_back(ii-1);
    }

    return toret;
}

// Format assumed: one line the graph, next line the replacement for corresponding candidate, and repeats
void read_custom_basis_from_stream(std::ifstream& custom, std::unordered_map<TopoIDType, CustomBasis>& map){
    std::string thegraph;
    while (std::getline(custom, thegraph)) {
        if(thegraph.at(0)=='#')
            continue;
        std::string therelabeling;
        if(std::getline(custom, therelabeling)){
            TopoIDType graph(thegraph.c_str());
            CustomBasis relabel = parse_relabel(therelabeling);
            map[graph]=relabel;
        }
        else{
            std::cerr<<"ERROR: reading custom basis got no ExternalPermutation[] for the graph: "<<thegraph<<std::endl;
            std::exit(1);
        }
    }
}



_INSTANTIATE_FUNCTION_SPACE(,C)
#ifdef USE_FINITE_FIELDS
_INSTANTIATE_FUNCTION_SPACE(,F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_FUNCTION_SPACE(,BigRat)
#endif
#endif
#ifdef HIGH_PRECISION
_INSTANTIATE_FUNCTION_SPACE(,CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_FUNCTION_SPACE(,CVHP)
#endif

} // namespace FunctionSpace
} // namespace Caravel
