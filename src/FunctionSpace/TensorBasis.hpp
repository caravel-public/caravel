#include "Core/type_traits_extra.h"
#include <numeric>

namespace Caravel {
namespace FunctionSpace {

using Caravel::operator<<;
namespace detail {
template <typename A, typename B, typename C> std::vector<C> cauchy_product(const std::vector<A>& listA,const std::vector<B>& listB,const std::function<C(A, B)>& product) {
    std::vector<C> result;

    for (auto& elA : listA) {
        for (auto& elB : listB) { result.push_back(product(elA, elB)); }
    }

    return result;
}
// TODO: This function only needs topological information, not a full xGraph
template <typename T> std::vector<BasisFunction<T>> tensor_insertion_functions(const lGraph::xGraph& graph) {
    auto topo_tensors = tensor_insertions(graph);

    std::vector<BasisFunction<T>> tensor_functions;
    for (auto& tensor : topo_tensors) { tensor_functions.push_back(tensor_insertion_evaluator<T>(tensor)); }

    return tensor_functions;
}
} // namespace detail

template <typename T>
inline std::enable_if_t<is_floating_point<T>::value, void> transverse_normalize_tensor(T&, const TensorInsertion&, const TopologyCoordinates<T>&) {}

// TODO: This can/should be much faste
template <typename T>
inline std::enable_if_t<!is_floating_point<T>::value, void> transverse_normalize_tensor(T& result, const TensorInsertion& t,
                                                                                        const TopologyCoordinates<T>& coords) {
    auto& norms = coords.common_transverse_norms;
    auto& monomials = t.first;
    // here only norms associated to common-transverse vectors are considered (those are the ones set by set_transverse_norms(.) in coords.common_transverse_norms
    for (size_t i = 0; i < norms.size(); i++) {
        size_t norm_count = 0;
        for (auto& monomial : monomials) { norm_count += monomial.at(i); }
        // Note, if norm_count is odd, this is unnecessary. Maybe that speeds things up.
        result *= pow(norms.at(i), norm_count / 2);
    }
}

// Tensor is arranged as common then uncommon.
template <typename T> T evaluate_tensor_insertion(const TensorInsertion& t, const TopologyCoordinates<T>& coords) {

    T result(1);

    // 4-D Coordinates
    auto& fourD_part = t.first;
    auto num_loops = fourD_part.size();

    for (size_t i = 0; i < num_loops; i++) {
        auto& tensor = fourD_part[i].contributions;
        auto& fourD_coords = coords.fourD_coords[i];

        auto tensor_size = tensor.size();
        for (size_t j = 0; j < tensor_size; j++) {
            auto& tensor_contribution = tensor[j];
            result *= fourD_coords[tensor_contribution.first][tensor_contribution.second];
        }
    }

    transverse_normalize_tensor(result, t, coords);

    auto& t_mu = t.second;
    auto& coords_mu = coords.mu_coords;

    // Regulating/Mu coordinates
    for (size_t i = 0; i < coords_mu.size(); i++) { result *= prod_pow(coords_mu[i], t_mu[i]); }

    return result;
}

template <typename T> BasisFunction<T> tensor_insertion_evaluator(const TensorInsertion& t) {
    auto bf = BasisFunction<T>([t](const TopologyCoordinates<T>& coords) { return evaluate_tensor_insertion(t, coords); });
    bf.set_integral_insertion(t.insertion);
    return bf;
}

} // namespace FunctionSpace
} // namespace Caravel
