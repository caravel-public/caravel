/**
 * @file IntegralInsertion.h
 *
 * @date 31.1.2019
 *
 * @brief Header file for the enum IntegralInsertion which labels different (master) integral insertions
 *
*/
#ifndef INTEGRAL_INSERTION_H_
#define INTEGRAL_INSERTION_H_

#include "wise_enum.h"

namespace Caravel {

/**
 * \enum IntegralInsertion
 *
 * \brief An enumeration of possible integral insertions. Later on it might be abstracted differently
 */
WISE_ENUM_CLASS(IntegralInsertion,
    (scalar , 0),	/**< A trivial insertion, just 1 */
    (mu2 , 1),	/**< A 1-loop integral insertion of mu^2 */
    (mu4 , 2),	/**< A 1-loop integral insertion of (mu^2)^2 */
    (mu6 , 3),	/**< A 1-loop integral insertion of (mu^2)^3 */
    (mu8 , 4),	/**< A 1-loop integral insertion of (mu^2)^4 */
    (mu10 , 5),	/**< A 1-loop integral insertion of (mu^2)^5 */
    (surface , -1),	/**< All surface-terms are labeled with this */
    (undefined , 999)	/**< Just to identify cases without defined integral insertions */
)

}

#endif	// INTEGRAL_INSERTION_H_
