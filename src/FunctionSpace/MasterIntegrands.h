/**
 * A collection of easy to evaluate and hopefully well tested master
 * integrands with nice properties. Conventions used are those of
 * "candidate diagrams".
 */

#ifndef MASTER_INTEGRANDS_H_INC
#define MASTER_INTEGRANDS_H_INC

#include <utility>
#include <iostream>
#include <cstdlib>

#include "Core/momD.h"
#include "Core/type_traits_extra.h"
#include "misc/DeltaZero.h"

namespace Caravel{
namespace FunctionSpace{
namespace MasterIntegrands{

template <typename T, size_t D> inline auto mu(const momD<T, D>& m1, const momD<T, D>& m2) { return compute_Dsm4_prod(m1, m2); }
template <typename T> inline auto sq(const T& m) { return m * m; }

template<typename T, size_t D>
T levi_expr(const momentumD<T,D>& l2, const momentumD<T,D>& mom1, const momentumD<T,D>& mom2,const momentumD<T,D>& mom3 ){
	  return
	   l2.pi(3)*mom1.pi(2)*mom2.pi(1)*mom3.pi(0) - l2.pi(2)*mom1.pi(3)*mom2.pi(1)*mom3.pi(0) -
	   l2.pi(3)*mom1.pi(1)*mom2.pi(2)*mom3.pi(0) + l2.pi(1)*mom1.pi(3)*mom2.pi(2)*mom3.pi(0) +
	   l2.pi(2)*mom1.pi(1)*mom2.pi(3)*mom3.pi(0) - l2.pi(1)*mom1.pi(2)*mom2.pi(3)*mom3.pi(0) -
	   l2.pi(3)*mom1.pi(2)*mom2.pi(0)*mom3.pi(1) + l2.pi(2)*mom1.pi(3)*mom2.pi(0)*mom3.pi(1) +
	   l2.pi(3)*mom1.pi(0)*mom2.pi(2)*mom3.pi(1) - l2.pi(0)*mom1.pi(3)*mom2.pi(2)*mom3.pi(1) -
	   l2.pi(2)*mom1.pi(0)*mom2.pi(3)*mom3.pi(1) + l2.pi(0)*mom1.pi(2)*mom2.pi(3)*mom3.pi(1) +
	   l2.pi(3)*mom1.pi(1)*mom2.pi(0)*mom3.pi(2) - l2.pi(1)*mom1.pi(3)*mom2.pi(0)*mom3.pi(2) -
	   l2.pi(3)*mom1.pi(0)*mom2.pi(1)*mom3.pi(2) + l2.pi(0)*mom1.pi(3)*mom2.pi(1)*mom3.pi(2) +
	   l2.pi(1)*mom1.pi(0)*mom2.pi(3)*mom3.pi(2) - l2.pi(0)*mom1.pi(1)*mom2.pi(3)*mom3.pi(2) -
	   l2.pi(2)*mom1.pi(1)*mom2.pi(0)*mom3.pi(3) + l2.pi(1)*mom1.pi(2)*mom2.pi(0)*mom3.pi(3) +
	   l2.pi(2)*mom1.pi(0)*mom2.pi(1)*mom3.pi(3) - l2.pi(0)*mom1.pi(2)*mom2.pi(1)*mom3.pi(3) -
	   l2.pi(1)*mom1.pi(0)*mom2.pi(2)*mom3.pi(3) + l2.pi(0)*mom1.pi(1)*mom2.pi(2)*mom3.pi(3);
}

template<typename T, size_t D>
std::enable_if_t<is_complex<T>::value, T> levi(const momentumD<T,D>& l2, const momentumD<T,D>& mom1, const momentumD<T,D>& mom2,const momentumD<T,D>& mom3 ){
    return T(0,-1)*levi_expr(l2,mom1,mom2,mom3);
}

template<typename T, size_t D>
std::enable_if_t<!is_complex<T>::value, T> levi(const momentumD<T,D>& l2, const momentumD<T,D>& mom1, const momentumD<T,D>& mom2,const momentumD<T,D>& mom3 ){
    return levi_expr(l2,mom1,mom2,mom3);
}

template<typename T>
std::enable_if_t<!is_complex_v<T> && !is_finite_field_v<T>, T> sign_helper(const T& val){
    if(val < T(0))
        return T(-1);
    else
        return T(1);
}

template <typename T> std::enable_if_t<is_complex_v<T>, T> sign_helper(const T& val) {
    using RT = remove_complex_t<T>;
    if constexpr (is_exact_v<RT>) {
        if(val.real() == RT(0)) return sign_helper(val.imag());
        else if (val.imag() == RT(0)) return sign_helper(val.real());
        else {
            _WARNING_R("sign_helper should receive only real or imaginary values!");
            std::exit(1);
        }
    }
    else {
        auto norm = val.real() + val.imag();
        if ((val.real() / norm) < DeltaZero<RT>())
            return sign_helper(val.imag());
        else if ((val.imag() / norm) < DeltaZero<RT>())
            return sign_helper(val.real());
        else {
            _WARNING_R("sign_helper should receive only real or imaginary values!");
            std::exit(1);
        }
    }
}

template<typename T, size_t D>
T sign_tr5(const momentumD<T,D>& l2, const momentumD<T,D>& mom1, const momentumD<T,D>& mom2, const momentumD<T,D>& mom3){
    if constexpr (!is_finite_field_v<T>) {
        return sign_helper(levi(l2,mom1,mom2,mom3));
    }
    else{
        // in this case the sign doesn't really make sense and we should throw in principle,
        // however at the moment that would throw too frequently since it is called from the integral input variables
        // TODO: this shouldn't be called at all 
        return 0;
    }
}

template<typename T, size_t D>
T trplus(const momD<T,D>& p1, const momD<T,D>& p2, const momD<T,D>& p3, const momD<T,D>& p4){
	auto tr = T(4)*(p1*p2)*(p3*p4) - T(4)*(p1*p3)*(p2*p4) + T(4)*(p1*p4)*(p2*p3);

	auto tr5 = T(4) * levi(p1, p2, p3, p4);

	return (tr + tr5)/T(2);
}

template<typename T, size_t D>
T trminus(const momD<T,D>& p1, const momD<T,D>& p2, const momD<T,D>& p3, const momD<T,D>& p4){
	auto tr = T(4)*(p1*p2)*(p3*p4) - T(4)*(p1*p3)*(p2*p4) + T(4)*(p1*p4)*(p2*p3);

	auto tr5 = T(4) * levi(p1, p2, p3, p4);

	return (tr - tr5)/T(2);
}

} // namespace MasterIntegrands
} // namespace FunctionSpace
} // namespace Caravel

#endif
