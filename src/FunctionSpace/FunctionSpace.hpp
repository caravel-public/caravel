#include "Core/type_traits_extra.h"

#include "Core/Timing.h"
#include "Core/type_traits_extra.h"

namespace Caravel {
namespace FunctionSpace {

template <typename T, size_t D>
std::vector<BasisFunction<T>> build_topology_master_surface(const lGraph::xGraph& gr, const GraphKin<T, D>& kinematics, size_t& n_masters, size_t& dimension) {
    auto master_surface = master_surface_functions<T>(gr);

    n_masters = master_surface.first.size();
    dimension = n_masters + master_surface.second.size();

    std::vector<BasisFunction<T>> result;
    for (auto& master : master_surface.first) { result.push_back(master); }

    for (auto& surface : master_surface.second) { result.push_back(surface); }

    return result;
}

template <typename T> std::vector<BasisFunction<T>> truncate_basis(std::vector<BasisFunction<T>> basis, BasisTruncation truncation) {
    std::sort(std::begin(truncation), std::end(truncation), [](size_t i, size_t j) { return i > j; });
    for (auto& to_remove : truncation) { basis.erase(basis.begin() + to_remove); }
    return basis;
}

namespace detail {
// Normalize the common transverse squares by the basis vector squares. Only a useful strategy for
// factorizable and one-loop
template <typename T, size_t D> inline std::enable_if_t<!is_exact_v<T>> transverse_normalize(CoordPowers<T>&, const momentumD<T, D>&) {}

template <typename T, size_t D> inline std::enable_if_t<is_exact_v<T>> transverse_normalize(CoordPowers<T>& coords, const momentumD<T, D>& vec) {
    using std::pow;
    auto inv_norm = T(1) / (vec * vec);
    for (size_t i = 0; i < coords.size(); i += 2) { coords[i] *= pow(inv_norm, i / 2); }
}

// Store the common transverse norms, only useful if non-factorizable
template <typename T> inline std::enable_if_t<!is_exact_v<T>> set_transverse_norms(TopologyCoordinates<T>&, const std::vector<T>&) {}

template <typename T>
inline std::enable_if_t<is_exact_v<T>> set_transverse_norms(TopologyCoordinates<T>& coords, const std::vector<T>& inverse_common_transverse_norms) {
    coords.common_transverse_norms = inverse_common_transverse_norms;
}

} // namespace detail

template <typename T, size_t D>
auto topology_coordinates_evaluator_2L(const lGraph::xGraph& graph_in, const GraphKin<T, D>& kinematics) {
    using std::vector;

    // standardize routing
    auto& graph = graph_in.get_special_routing();
    auto convert_loop_mom = OnShellStrategies::OnShellPointMap<T, D>(graph_in, graph);

    const bool gravity_power_counting = (settings::general::power_counting_of_theory == settings::general::power_counting::Gravity);
    const bool is_factorizable = graph.get_topology().is_factorizable();
    const size_t num_common = is_factorizable ? 0 : (4-graph.get_span_dim());

    // if gravity powercounting is used, there has to be an additional shift in the powercounting
    size_t offset(0);
    if(gravity_power_counting) {
        offset = get_gravity_power_couting_offset(graph);
    }


    std::array<size_t,2> max_powers;
    {
        const auto lppv = loop_powers_per_vertex(settings::general::power_counting_of_theory);
        if (lppv > 2) {
            _MESSAGE_R("Power counting ", lppv, " has not yet been implemented!");
            std::exit(1);
        }

        auto n_vertices_in_subloop = [&graph](size_t i){
            return graph.get_one_loop_subdiagram(i).second.size()+1;
        };

        max_powers = {n_vertices_in_subloop(1) * lppv+offset, n_vertices_in_subloop(2) * lppv+offset};
    }

    return [&kinematics, is_factorizable, num_common, max_powers = std::move(max_powers), convert_loop_mom = std::move(convert_loop_mom)](const OnShellPoint<T, D>& p_in) {
        auto& transverse_vecs = kinematics.get_transverse();
        auto& common_transverse_vecs = kinematics.get_common_transverse();
        auto& momconf = kinematics.get_mom_conf();

        auto p = convert_loop_mom(p_in, momconf);

        constexpr size_t L = 2;

        // Begin the building of the 4d coordinates.
        vector<LoopCoordinates<T>> topo_fourD_coords(L);

        // vector of x^0, x^1, ..., x^n
        auto all_powers_up_to = [](T&& x, size_t n) {
            vector<T> powers(n + 1);

            powers[0] = T(1);
            for (size_t i = 1; i <= n; i++) { powers[i] = powers[i - 1] * x; }

            return powers;
        };

        auto& non_common_momenta =  kinematics.get_non_common_momenta();

        for (size_t loop_i = 0; loop_i < L; loop_i++) {
            size_t max_power = max_powers[loop_i];

            auto& li_transverse = transverse_vecs.at(loop_i);
            size_t transverse_size = li_transverse.size() - 2;
            LoopCoordinates<T> li_coords(transverse_size);

            // Common transverse
            for (size_t i_common = 0; i_common < num_common; i_common++) {
                auto& common_vec = common_transverse_vecs.at(i_common);
                auto coord = common_vec * p[loop_i];
                li_coords[i_common] = std::move(all_powers_up_to(std::move(coord), max_power));
            }

            auto& li_non_common = non_common_momenta.at(loop_i);

            for (size_t i_uncommon = 0; i_uncommon < li_non_common.size(); i_uncommon++) {
                auto& uncommon = li_non_common.at(i_uncommon);
                auto coord = uncommon * p.at(loop_i);
                li_coords[i_uncommon + num_common] = std::move(all_powers_up_to(std::move(coord), max_power));

                if (is_factorizable) { detail::transverse_normalize(li_coords[i_uncommon], uncommon); }
            }

            topo_fourD_coords[loop_i] = std::move(li_coords);
        }

        auto mua2 = compute_Dsm4_prod(p.at(0), p.at(0));
        auto mub2 = compute_Dsm4_prod(p.at(1), p.at(1));
        auto muab = compute_Dsm4_prod(p.at(0), p.at(1));

        TopologyCoordinates<T> result;
        result.fourD_coords = std::move(topo_fourD_coords);
        result.mu_coords = RegulatingCoordinates<T>{mua2, mub2, muab};

        if (!is_factorizable) {
            std::vector<T> inverse_common_transverse_norms;
            // we keep only inverse norms for transverse vectors in the physical region
            // FIXME: these norms (which are constant over on-shell PS) can be cached into GraphKin to reduce a little computation
            for (size_t i_common = 0; i_common < num_common; i_common++) {
                auto& vec = common_transverse_vecs.at(i_common);
                inverse_common_transverse_norms.push_back(T(1) / (vec * vec));
            }
            detail::set_transverse_norms(result, inverse_common_transverse_norms);
        }

        return result;
    };
}

/**
 * This routine computes the topology coordinates for the special case of the massive one-loop bubble with
 * an on-shell massless external leg
 */
template <typename T, size_t D>
auto topology_coordinates_1L_special_bubble(const lGraph::xGraph graph, const GraphKin<T, D>& kinematics) {
    using namespace lGraph;

    // The bubble always has a single node, where the attached legs are either a single on-shell momentum
    // or they add up via momentum conservation to a massless momentum 
    const auto& node = (graph.get_base_graph().get_nodes())[0]; 
    std::vector<size_t> mommapper = node.get_momentum_indices();

    const auto lppv = loop_powers_per_vertex(settings::general::power_counting_of_theory);
    if (lppv > 2) {
        _MESSAGE_R("Power counting ", lppv, " has not yet been implemented!");
        std::exit(1);
    }

    const size_t max_power = graph.get_n_vertices() * lppv;

    return [&kinematics, mommapper, max_power](const OnShellPoint<T, D>& p) {
        const auto& common_transverse_vecs = kinematics.get_common_transverse();

        LoopCoordinates<T> li_common;
        auto& l = p.at(0);
        const momD_conf<T,D> mc = kinematics.get_mom_conf();
        const momD<T,D> pi = -mc.Sum(mommapper);
        // auxilliary momentum n, such that (n*p) = 1
        momD<T,D> n = pi.get_parity_conjugated();
        n /= (n * pi); // Has to match normalization in osm.hpp

        // vector of x^0, x^1, ..., x^n
        auto all_powers_up_to = [](T x, size_t n) {
            std::vector<T> powers(n + 1);

            powers[0] = T(1);
            for (size_t i = 1; i <= n; i++) { powers[i] = powers[i - 1] * x; }

            return powers;
        };

        // (t1,t2) form the 2 transverse space to p and n, while t3 spans the (d-4) transverse space
	const momD<T,D> t1 = common_transverse_vecs.at(0);
	const momD<T,D> t2 = common_transverse_vecs.at(1);
	const momD<T,D> t3 = common_transverse_vecs.at(2);

        li_common.push_back( all_powers_up_to( t1*l, max_power ) );
        li_common.push_back( all_powers_up_to( t2*l, max_power ) );
        li_common.push_back( all_powers_up_to( n*l, max_power ) );

        auto mu2 = compute_Dsm4_prod(p.at(0), p.at(0));

        RegulatingCoordinates<T> mu_part{mu2};

        TopologyCoordinates<T> result;
        result.fourD_coords = {li_common};
        result.mu_coords = mu_part;

        std::vector<T> inverse_common_transverse_norms;
        // we keep only inverse norms for transverse vectors in the physical region
        // FIXME: these norms (which are constant over on-shell PS) can be cached into GraphKin to reduce a little computation
        for (unsigned n = 0; n < 2; n++) {
            auto& vec = common_transverse_vecs.at(n);
            inverse_common_transverse_norms.push_back(T(1) / (vec * vec));
        }

        detail::set_transverse_norms(result, inverse_common_transverse_norms);

        return result;
    };
}

template <typename T, size_t D>
auto topology_coordinates_evaluator_1L(const lGraph::xGraph graph, const GraphKin<T, D>& kinematics) {
    // Topology coordinates for one loop are the dot products of loop
    // momenta with common transverse vectors.

    const size_t num_common = 4 - graph.get_span_dim();

    const auto lppv = loop_powers_per_vertex(settings::general::power_counting_of_theory);
    if (lppv > 2) {
        _MESSAGE_R("Power counting ", lppv, " has not yet been implemented!");
        std::exit(1);
    }

    const size_t max_power = graph.get_n_vertices() * lppv;

    return [&kinematics,num_common, max_power](const OnShellPoint<T, D>& p) {
        const auto& common_transverse_vecs = kinematics.get_common_transverse();
        LoopCoordinates<T> li_common;
        auto& l = p.at(0);

        // vector of x^0, x^1, ..., x^n
        auto all_powers_up_to = [](T x, size_t n) {
            std::vector<T> powers(n + 1);

            powers[0] = T(1);
            for (size_t i = 1; i <= n; i++) { powers[i] = powers[i - 1] * x; }

            return powers;
        };

        for (size_t i_common = 0; i_common < num_common; i_common++) {
            auto& common_vec = common_transverse_vecs.at(i_common);
            auto coord = common_vec * l;
            li_common.push_back(all_powers_up_to(coord, max_power));
        }
	

        auto mu2 = compute_Dsm4_prod(p.at(0), p.at(0));

        RegulatingCoordinates<T> mu_part{mu2};

        TopologyCoordinates<T> result;
        result.fourD_coords = {li_common};
        result.mu_coords = mu_part;

        std::vector<T> inverse_common_transverse_norms;

        // we keep only inverse norms for transverse vectors in the physical region
        // FIXME: these norms (which are constant over on-shell PS) can be cached into GraphKin to reduce a little computation
        for (size_t i_common = 0; i_common < num_common; i_common++) {
            auto& vec = common_transverse_vecs.at(i_common);
            inverse_common_transverse_norms.push_back(T(1) / (vec * vec));
        }

        detail::set_transverse_norms(result, inverse_common_transverse_norms);

        return result;
    };
}

template <typename T>
FunctionBasis<T, 4> build_topology_master_surface_basis(const lGraph::xGraph& graph, const GraphKin<T, 4>& kinematics, BasisTruncation truncation) {
    _WARNING_R("build_topology_master_surface_basis called with D=4!");
    std::exit(1);
}

void read_custom_basis_from_stream(std::ifstream&, std::unordered_map<TopoIDType, CustomBasis>&);

namespace _misc {
template <typename T, size_t D>
std::function<TopologyCoordinates<T>(const OnShellPoint<T, D>&)>
_helper_construct_coord_evaluator(const GraphKin<T, D>& kinematics, MasterCoordinatesFP<T, D>&& Special_coordinates_evaluator,
                                  std::function<TopologyCoordinates<T>(const OnShellPoint<T, D>&)>&& evaluate_topology_coordinates,
                                  std::function<OnShellPoint<T, D>(const OnShellPoint<T, D>&, const momD_conf<T, D>&)>&& conventions_map_loop_surface,
                                  std::function<std::vector<momD<T, D>>(const momD_conf<T, D>&)>&& conventions_map_ext_surface,
                                  std::function<OnShellPoint<T, D>(const OnShellPoint<T, D>&, const momD_conf<T, D>&)>&& conventions_map_loop_master,
                                  std::function<std::vector<momD<T, D>>(const momD_conf<T, D>&)>&& conventions_map_ext_master,
                                  IBPCoordinatesFP<T, D>&& IBP_coordinates_evaluator) {
    if (conventions_map_loop_surface) {
        if (Special_coordinates_evaluator && conventions_map_loop_master) {
            // surface map and independent master map and master coordinates
            return
                [&kinematics, Special_coordinates_evaluator = std::move(Special_coordinates_evaluator),
                 evaluate_topology_coordinates = std::move(evaluate_topology_coordinates),
                 conventions_map_loop_surface = std::move(conventions_map_loop_surface), conventions_map_ext_surface = std::move(conventions_map_ext_surface),
                 conventions_map_loop_master = std::move(conventions_map_loop_master), conventions_map_ext_master = std::move(conventions_map_ext_master),
                 IBP_coordinates_evaluator = std::move(IBP_coordinates_evaluator)](const OnShellPoint<T, D>& os_point) {
                    auto topology_coordinates = evaluate_topology_coordinates(os_point);
                    topology_coordinates.b_coeffs = &(kinematics.get_surface_term_coefficients());

                    const auto& mc = kinematics.get_mom_conf();

                    auto loop_momenta = conventions_map_loop_surface(os_point, mc);
                    auto external_corners = conventions_map_ext_surface(mc);

                    auto ibp_coordinates = IBP_coordinates_evaluator(loop_momenta, external_corners, kinematics.get_ibp_vector_coefficients());

                    topology_coordinates.propagators = std::move(ibp_coordinates.propagators);
                    topology_coordinates.invariants = std::move(ibp_coordinates.invariants);
                    topology_coordinates.vector_components = std::move(ibp_coordinates.vector_components);

                    loop_momenta = conventions_map_loop_master(os_point, mc);
                    external_corners = conventions_map_ext_master(mc);

                    topology_coordinates.special = Special_coordinates_evaluator(loop_momenta, external_corners);

                    return topology_coordinates;
                };
        }
        else if (Special_coordinates_evaluator) {
            // surface map (same for master map) and master coordinates
            return
                [&kinematics, Special_coordinates_evaluator = std::move(Special_coordinates_evaluator),
                 evaluate_topology_coordinates = std::move(evaluate_topology_coordinates),
                 conventions_map_loop_surface = std::move(conventions_map_loop_surface), conventions_map_ext_surface = std::move(conventions_map_ext_surface),
                 IBP_coordinates_evaluator = std::move(IBP_coordinates_evaluator)](const OnShellPoint<T, D>& os_point) {
                    auto topology_coordinates = evaluate_topology_coordinates(os_point);
                    topology_coordinates.b_coeffs = &(kinematics.get_surface_term_coefficients());

                    const auto& mc = kinematics.get_mom_conf();

                    auto loop_momenta = conventions_map_loop_surface(os_point, mc);
                    auto external_corners = conventions_map_ext_surface(mc);

                    auto ibp_coordinates = IBP_coordinates_evaluator(loop_momenta, external_corners, kinematics.get_ibp_vector_coefficients());

                    topology_coordinates.propagators = std::move(ibp_coordinates.propagators);
                    topology_coordinates.invariants = std::move(ibp_coordinates.invariants);
                    topology_coordinates.vector_components = std::move(ibp_coordinates.vector_components);

                    topology_coordinates.special = Special_coordinates_evaluator(loop_momenta, external_corners);

                    return topology_coordinates;
                };
        }
        // surface map 
        return [&kinematics, evaluate_topology_coordinates = std::move(evaluate_topology_coordinates),
                conventions_map_loop_surface = std::move(conventions_map_loop_surface), conventions_map_ext_surface = std::move(conventions_map_ext_surface),
                IBP_coordinates_evaluator = std::move(IBP_coordinates_evaluator)](const OnShellPoint<T, D>& os_point) {
            auto topology_coordinates = evaluate_topology_coordinates(os_point);
            topology_coordinates.b_coeffs = &(kinematics.get_surface_term_coefficients());

            const auto& mc = kinematics.get_mom_conf();

            auto loop_momenta = conventions_map_loop_surface(os_point, mc);
            auto external_corners = conventions_map_ext_surface(mc);

            auto ibp_coordinates = IBP_coordinates_evaluator(loop_momenta, external_corners, kinematics.get_ibp_vector_coefficients());

            topology_coordinates.propagators = std::move(ibp_coordinates.propagators);
            topology_coordinates.invariants = std::move(ibp_coordinates.invariants);
            topology_coordinates.vector_components = std::move(ibp_coordinates.vector_components);

            return topology_coordinates;
        };
    }
    else if (Special_coordinates_evaluator && conventions_map_loop_master) {
        // no surface map, master map and master coordinates
        return [&kinematics, Special_coordinates_evaluator = std::move(Special_coordinates_evaluator),
                evaluate_topology_coordinates = std::move(evaluate_topology_coordinates), conventions_map_loop_master = std::move(conventions_map_loop_master),
                conventions_map_ext_master = std::move(conventions_map_ext_master)](const OnShellPoint<T, D>& os_point) {
            auto topology_coordinates = evaluate_topology_coordinates(os_point);
            topology_coordinates.b_coeffs = &(kinematics.get_surface_term_coefficients());

            const auto& mc = kinematics.get_mom_conf();

            auto loop_momenta = conventions_map_loop_master(os_point, mc);
            auto external_corners = conventions_map_ext_master(mc);
            topology_coordinates.special = Special_coordinates_evaluator(loop_momenta, external_corners);

            return topology_coordinates;
        };
    }
    // simple topology coordinate evaluation
    return [&kinematics, evaluate_topology_coordinates = std::move(evaluate_topology_coordinates)](const OnShellPoint<T, D>& os_point) {
        auto topology_coordinates = evaluate_topology_coordinates(os_point);
        topology_coordinates.b_coeffs = &(kinematics.get_surface_term_coefficients());

        return topology_coordinates;
    };
}

} // namespace _misc

template <typename T, size_t D>
FunctionBasis<T, D> build_topology_master_surface_basis(const lGraph::xGraph& graph, const GraphKin<T, D>& kinematics, BasisTruncation truncation) { 
    size_t n_masters(0);
    size_t dimension(0);
    auto full_basis = build_topology_master_surface(graph,kinematics, n_masters, dimension);
    auto basis = truncate_basis(full_basis, truncation);

    switch (graph.get_n_loops()) {
        case 1:
            // no need of master/surface containers
            if (settings::general::one_loop_master_basis != settings::general::OneLoopMasterBasis::pure) {
                if (!graph.is_bubble_with_single_massless_leg()) {
                    return FunctionBasis<T, D>([evaluate_topology_coordinates = topology_coordinates_evaluator_1L(graph, kinematics)](
                                                   const OnShellPoint<T, D>& os_point) { return evaluate_topology_coordinates(os_point); },
                                               basis, n_masters, dimension);
                }
                else {
                    // Special parametrisation for massive bubble with massless external leg
                    return FunctionBasis<T, D>([evaluate_topology_coordinates = topology_coordinates_1L_special_bubble(graph, kinematics)](
                                                   const OnShellPoint<T, D>& os_point) { return evaluate_topology_coordinates(os_point); },
                                               basis, n_masters, dimension);
                }
            }
            else
                break;
        case 2: break;
        default: _WARNING_R("Not implemented!"); std::exit(1);
    }

    auto topo = graph.get_topology();

    using lGraph::xGraph;

    // The following block allows customization of integrand basis:
    //		Meant for application of symmetries to given diagrams.
    //		User needs to understand used basis and provided candidates
    static thread_local bool read_custom(true);
    // Map between:
    //	1: A base_graph (in an xGraph)
    //	2: The corresponding mapping of external and internal momenta
    static thread_local std::unordered_map<TopoIDType, CustomBasis> custom_integrand_basis;
    // TODO: make more flexible later on (use settings, etc)
    std::string filename_custom = "integrand_basis_customize.dat";
    if(read_custom){
        read_custom=false;
        std::ifstream custom(filename_custom);
        if(custom.is_open()){
            // load information in map
            read_custom_basis_from_stream(custom,custom_integrand_basis);
        }
    }

    std::function<TopologyCoordinates<T>(const OnShellPoint<T, D>&)> evaluate_topology_coordinates;
    if (graph.get_n_loops() == 1) 
        evaluate_topology_coordinates = topology_coordinates_evaluator_1L(graph, kinematics);
    else if (graph.get_n_loops() == 2)
        evaluate_topology_coordinates = topology_coordinates_evaluator_2L(graph, kinematics);
    else {
        _WARNING_R("No coordinate evaluation implemented!");
        std::exit(1);
    }

    std::function<OnShellPoint<T, D>(const OnShellPoint<T, D>&, const momD_conf<T, D>&)> conventions_map_loop_surface = nullptr;
    std::function<std::vector<momD<T, D>>(const momD_conf<T, D>&)> conventions_map_ext_surface = nullptr;
    IBPCoordinatesFP<T,D> IBP_coordinates_evaluator = nullptr;

    auto s = surface_term_data_holder.find(topo);
    if (s != surface_term_data_holder.end()) {
        const SurfaceTermData& surfacedata = s->second;

        // get an xGraph for which the function evaluating surface terms is implemented
        // we then map all momenta to this representative
        const xGraph& IBPcandidate_xGraph = std::get<xGraph>(surfacedata);

        xGraph copy_relabeled = graph.get_relabeled_copy(IBPcandidate_xGraph);

        conventions_map_loop_surface = OnShellStrategies::OnShellPointMap<T, D>(graph, copy_relabeled);
        conventions_map_ext_surface = lGraph::lGraphKin::external_momentum_mapper<T, D>(copy_relabeled, IBPcandidate_xGraph);
        IBP_coordinates_evaluator = std::get<IBPCoordinatesFP<T,D>>(surfacedata);
    }

    MasterCoordinatesFP<T,D> Special_coordinates_evaluator = nullptr;
    std::function<OnShellPoint<T,D>(const OnShellPoint<T,D>&,const momD_conf<T,D>&)> conventions_map_loop_master = nullptr;
    std::function<std::vector<momD<T, D>>(const momD_conf<T, D>&)> conventions_map_ext_master = nullptr;

    // if we have masters in this topology, constuct the same information for them
    {
        auto f = master_term_data_holder.find(topo);
        if (f != master_term_data_holder.end()) {
            Special_coordinates_evaluator = std::get<MasterCoordinatesFP<T,D>>(f->second);

            xGraph candidate_xGraph = std::get<xGraph>(f->second);
            if(candidate_xGraph.get_base_graph() != xGraph{}.get_base_graph()){

                auto replacement = custom_integrand_basis.find(graph.get_base_graph());
                if(replacement != custom_integrand_basis.end()){
                    std::cout<<"From information loaded from the custom file "<<filename_custom<<" we remap external momenta from the master candidate: "<<std::endl;
                    candidate_xGraph.show();
                    std::cout<<"to:	loop-momentum mappings: ";
                    for(size_t nn=0;nn<(replacement->second).new_internal.size();nn++)
                        std::cout<<"new_l"<<nn+1<<" = "<<(replacement->second).new_internal[nn]<<"; ";
                    std::cout<<std::endl;
                    std::cout<<"	external permutations: ";
                    for(size_t nn=0;nn<(replacement->second).new_external.size();nn++)
                        std::cout<<"new_k_"<<nn+1<<" = k_"<<(replacement->second).new_external[nn]+1<<"; ";
                    std::cout<<std::endl;
                    std::cout<<"for the graph: "<<std::endl;
                    graph.show();
                }

                const auto& candidate_momenta = candidate_xGraph.get_internal_momenta();
                bool load(false);
                if (s == surface_term_data_holder.end())
                    load = true;
                else {
                    const xGraph& IBPcandidate_xGraph = std::get<xGraph>(s->second);
                    const auto& IBP_momenta = IBPcandidate_xGraph.get_internal_momenta();
                    if (candidate_xGraph.get_base_graph() != IBPcandidate_xGraph.get_base_graph() || candidate_momenta != IBP_momenta) load = true;
                }

                if(load){

                    xGraph copy_relabeled_insertion = graph.get_relabeled_copy(candidate_xGraph);
                    conventions_map_loop_master = OnShellStrategies::OnShellPointMap<T, D>(graph, copy_relabeled_insertion);
                    conventions_map_ext_master = lGraph::lGraphKin::external_momentum_mapper<T, D>(copy_relabeled_insertion, candidate_xGraph);

                    // in case custom choice
                    if(replacement != custom_integrand_basis.end()){
                        const auto& remap = replacement->second;
                        using loopmap = std::function<momD<T, D>(const std::vector<momD<T, D>>&, const momD_conf<T, D>&)>;
                        std::vector<loopmap> loopremap;
                        for(const auto& mr: remap.new_internal)    loopremap.push_back( lGraph::lGraphKin::MomentumRoutingEvaluator<T, D>(mr) );
                        conventions_map_loop_master = [loopremap=std::move(loopremap), first=conventions_map_loop_master](const OnShellPoint<T,D>& os,const momD_conf<T,D>& mc){
                                auto mom = first(os, mc);
                                decltype(mom) toret(mom.size());
                                for(size_t ii=0;ii<mom.size();ii++)    toret[ii] = loopremap[ii](mom, mc);
                                return toret;
                            };
                        conventions_map_ext_master = [permute=remap.new_external, first=conventions_map_ext_master](const momD_conf<T, D>& mc){
                                auto externals = first(mc);
                                decltype(externals) toret(externals.size());
                                for(size_t ii=0;ii<externals.size();ii++)    toret[ii] = externals[permute[ii]];
                                return toret;
                            };
                    }
                }
            }
        }
    }

    return FunctionBasis<T, D>(_misc::_helper_construct_coord_evaluator(kinematics, std::move(Special_coordinates_evaluator),
                                                                        std::move(evaluate_topology_coordinates), std::move(conventions_map_loop_surface),
                                                                        std::move(conventions_map_ext_surface), std::move(conventions_map_loop_master),
                                                                        std::move(conventions_map_ext_master), std::move(IBP_coordinates_evaluator)),
                               basis, n_masters, dimension);
}

template <typename T, size_t D>
FunctionBasis<T, D> build_topology_tensor_basis(const lGraph::xGraph& graph, const GraphKin<T, D>& kinematics, BasisTruncation truncation) {
    //! pass xgraph here
    auto full_basis = detail::tensor_insertion_functions<T>(graph);
    // all entries in the tensor-insertion basis are considered masters
    size_t dimension(full_basis.size());

    auto basis = truncate_basis(full_basis, truncation);

    switch (graph.get_n_loops()) {
        case 1:
            return FunctionBasis<T, D>([evaluate_topology_coordinates = topology_coordinates_evaluator_1L(graph, kinematics)](
                                           const OnShellPoint<T, D>& os_point) { return evaluate_topology_coordinates(os_point); },
                                       basis, dimension, dimension);
        case 2:
            return FunctionBasis<T, D>([evaluate_topology_coordinates = topology_coordinates_evaluator_2L(graph, kinematics)](
                                           const OnShellPoint<T, D>& os_point) { return evaluate_topology_coordinates(os_point); },
                                       basis, dimension, dimension);
        default: _WARNING_R("Not implemented!"); std::exit(1);
    }
}

template <typename T, size_t D>
FunctionBasis<T, D> build_topology_scattering_plane_tensor_basis(const lGraph::xGraph& graph, const GraphKin<T, D>& kinematics, BasisTruncation truncation) {
    //! pass xgraph here
    auto master_surface_basis = detail::scattering_plane_tensor_insertion_functions<T>(graph);
    auto full_basis = master_surface_basis.first;
    for(auto s: master_surface_basis.second) full_basis.push_back(s);

    // all entries in the tensor-insertion basis are considered masters
    size_t n_masters = master_surface_basis.first.size();
    size_t dimension = n_masters + master_surface_basis.second.size();

    auto basis = truncate_basis(full_basis, truncation);

    std::function<TopologyCoordinates<T>(const OnShellPoint<T, D>&)> evaluate_topology_coordinates;
    if (graph.get_n_loops() == 1) 
        evaluate_topology_coordinates = topology_coordinates_evaluator_1L(graph, kinematics);
    else if (graph.get_n_loops() == 2)
        evaluate_topology_coordinates = topology_coordinates_evaluator_2L(graph, kinematics);
    else {
        _WARNING_R("No coordinate evaluation implemented!");
        std::exit(1);
    }

    return FunctionBasis<T, D>([evaluate_topology_coordinates = std::move(evaluate_topology_coordinates)](
                                           const OnShellPoint<T, D>& os_point) { return evaluate_topology_coordinates(os_point); },
                                       basis, n_masters, dimension);
}

template <typename T, size_t D>
FunctionBasis<T, D> build_topology_function_basis(const lGraph::xGraph& gr, const GraphKin<T,D>& kinematics, BasisTruncation truncation) {
    using namespace settings::general;
    if (numerator_decomposition == IntegralBasis::master_surface) { 
      return build_topology_master_surface_basis(gr,kinematics, truncation); 
    }
    else if (numerator_decomposition == IntegralBasis::tensors) {
        return build_topology_tensor_basis(gr,kinematics, truncation);
    }
    else if (numerator_decomposition == IntegralBasis::scattering_plane_tensors) {
        return build_topology_scattering_plane_tensor_basis(gr,kinematics, truncation);
    }
    else {
        std::cerr << "ERROR: Unknown function basis: " << wise_enum::to_string(numerator_decomposition.value) << std::endl;
        exit(1);
    }
}

template <typename T, size_t D> using HierarchyFunctionBases = std::vector<std::vector<FunctionBasis<T, D>>>;

using Pij = std::vector<int>;

template <typename T, size_t D> std::vector<T> FunctionBasis<T, D>::operator()(const OnShellPoint<T, D>& p, T d) const {
    // Pre-allocate for speed
    std::vector<T> result(basis.size());
    auto coords = coord_evaluator(p);
    for (size_t i = 0; i < basis.size(); i++) {
        auto& f = basis.at(i);
        result[i] = f(coords, d);
    }

    return result;
}

template <typename T, size_t D> std::vector<std::vector<T>> FunctionBasis<T, D>::operator()(const OnShellPoint<T, D>& p, const std::vector<T>& d_values) const {
    // Pre-allocate for speed
    std::vector<std::vector<T>> result(basis.size());
    auto coords = coord_evaluator(p);

    for (size_t i = 0; i < basis.size(); i++) {
        auto& f = basis.at(i);
        result[i] = f(coords, d_values);
    }

    return result;
}

template <typename T> template <typename Func> BasisFunction<T>::BasisFunction(Func&& f) {
    static_assert(std::negation_v<std::is_same<std::remove_cv_t<std::remove_reference_t<Func>>, BasisFunction<T>>>, "Not a copy/move constructor!");

    if constexpr (std::is_invocable_v<Func, TopologyCoordinates<T>, T>) {
        /**
         * Construct the basis function from a simple d dependent
         * function. If operator () is called with a vector of d-values
         * then this constructor will lead to inefficiencies.
         */
        if constexpr (std::is_same_v<std::result_of_t<Func(TopologyCoordinates<T>, T)>, T>) {
            action = [_action = std::forward<Func>(f)](const TopologyCoordinates<T>& coords, const std::vector<T>& d_values) {
                std::vector<T> results(d_values.size());
                for (size_t i = 0; i < d_values.size(); i++) { results.at(i) = _action(coords, d_values.at(i)); }
                return results;
            };
        }
        else {
            static_assert(_delayed_static_false_v<T>);
        }
    }
    else if constexpr (std::is_invocable_v<Func, TopologyCoordinates<T>>) {
        /**
         * Construct the basis function from a d independent function.
         * If operator () is called with a vector of d-values then this
         * constructor will lead to the most efficient evaluation.
         */
        if constexpr (std::is_same_v<std::result_of_t<Func(TopologyCoordinates<T>)>, T>) {
            action = [_action = std::forward<Func>(f)](const TopologyCoordinates<T>& coords, const std::vector<T>& d_values) {
                std::vector<T> results(d_values.size(), _action(coords));
                return results;
            };
        }
        /**
         * Construct the basis function from a function which returns the
         * coefficients of a linear polynomial in d stored as a std::pair.
         * If operator () is called with a vector of d-values then this constructor will
         * lead to the most efficient evaluation (in case of linear dependence on d).
         */
        else if constexpr (std::is_same_v<std::result_of_t<Func(TopologyCoordinates<T>)>, std::pair<T, T>>) {
            action = [_action = std::forward<Func>(f)](const TopologyCoordinates<T>& coords, const std::vector<T>& d_values) {
                auto coeffs = _action(coords);

                std::vector<T> results(d_values.size());
                for (size_t i = 0; i < d_values.size(); i++) { results.at(i) = coeffs.first + d_values.at(i) * coeffs.second; }

                return results;
            };
        }
        /**
         * Construct the basis function from a function which returns the
         * coefficients of a polynomial in d stored in an array.
         * If operator () is called with a vector of d-values then this constructor will
         * lead to the most efficient evaluation.
         */
        else if constexpr (is_iterable_v<std::result_of_t<Func(TopologyCoordinates<T>)>>) {
            action = [_action = std::forward<Func>(f)](const TopologyCoordinates<T>& coords, const std::vector<T>& d_values) {
                auto coeffs = _action(coords);

                const size_t Ndvals = d_values.size();

                std::vector<T> results(Ndvals);
                results.shrink_to_fit();

                const size_t ND = coeffs.size();

                for (size_t i = 0; i < Ndvals; ++i) {
                    results[i] = coeffs[0];
                    auto di = d_values[i];
                    for (size_t j = 1; j < ND; ++j) { results[i] += coeffs[j] * prod_pow(di, j); }
                }

                return results;
            };
        }
        else {
            static_assert(_delayed_static_false_v<T>);
        }
    }
    else if constexpr (std::is_invocable_v<Func, TopologyCoordinates<T>, std::vector<T>>) {
        /**
         * Construct the basis function from a function with factorized d-dependence.
         * The functions knows about it itself and evaluates efficiently internally.
         * The signature is the same as in action, so it is stored directly.
         */
        if constexpr (std::is_same_v<std::result_of_t<Func(TopologyCoordinates<T>, std::vector<T>)>, std::vector<T>>) { action = std::forward<Func>(f); }
        else {
            static_assert(_delayed_static_false_v<T>);
        }
    }
    else {
        static_assert(_delayed_static_false_v<T>);
    }
}

template <typename T> template <typename Func,typename> BasisFunction<T>& BasisFunction<T>::operator=(Func&& f){
    *this = BasisFunction<T>{std::forward<Func>(f)};
    return *this;
}

template <typename T> T BasisFunction<T>::operator()(const TopologyCoordinates<T>& coords, T d) const { return action(coords, {d}).at(0); }

template <typename T> std::vector<T> BasisFunction<T>::operator()(const TopologyCoordinates<T>& coords, const std::vector<T>& d_values) const {
    return action(coords, d_values);
}

template <typename TF, typename T, size_t D>
std::vector<TF> evaluate_function_in_basis(const std::vector<std::reference_wrapper<const std::vector<TF>>>& coefficients, const OnShellPoint<T, D>& os_point,
                                           const std::vector<T> d_values, const FunctionBasis<T, D>& basis, size_t container_dimension) {

#ifdef TIMING_ON
    timing::surface_terms_timing.start();
#endif
    auto basis_vals = basis(os_point, d_values);
#ifdef TIMING_ON
    timing::surface_terms_timing.stop();
#endif

    std::vector<TF> results(d_values.size(), TF(static_cast<int>(container_dimension)));
    for (size_t d_i = 0; d_i < d_values.size(); d_i++) {
        const std::vector<TF>& coeffs_d_i = coefficients[d_i];
        auto& result = results[d_i];
        for (size_t i = 0; i < basis_vals.size(); i++) { result += coeffs_d_i[i] * (basis_vals[i][d_i]); }
    }

#ifdef DEBUG_ON
    for (auto& result : results) {
        if (result != result) {
            using Caravel::operator<<;
            std::cerr << "Error in evaluate_function_in_basis, result is NaN." << std::endl;
            std::cerr << "input value (p): " << std::endl;
            std::cerr << os_point << std::endl;
            std::cerr << "Basis function vals: " << std::endl;
            std::cerr << basis_vals << std::endl;
            exit(1);
        }
    }
#endif

    return results;
}

#define _INSTANTIATE_FUNCTION_SPACE(W,T)\
    W template class FunctionBasis<T,5>;\
    W template class FunctionBasis<T,6>;\
    W template FunctionBasis<T, 5> build_topology_tensor_basis<T,5>(const lGraph::xGraph&, const GraphKin<T, 5>&, BasisTruncation truncation);\
    W template FunctionBasis<T, 6> build_topology_tensor_basis<T,6>(const lGraph::xGraph&, const GraphKin<T, 6>&, BasisTruncation truncation);\
    W template FunctionBasis<T, 5> build_topology_scattering_plane_tensor_basis<T,5>(const lGraph::xGraph&, const GraphKin<T, 5>&, BasisTruncation truncation);\
    W template FunctionBasis<T, 6> build_topology_scattering_plane_tensor_basis<T,6>(const lGraph::xGraph&, const GraphKin<T, 6>&, BasisTruncation truncation);\
    W template FunctionBasis<T, 5> build_topology_master_surface_basis(const lGraph::xGraph&, const GraphKin<T, 5>&, BasisTruncation truncation);\
    W template FunctionBasis<T, 6> build_topology_master_surface_basis(const lGraph::xGraph&, const GraphKin<T, 6>&, BasisTruncation truncation);

_INSTANTIATE_FUNCTION_SPACE(extern,C)
#ifdef USE_FINITE_FIELDS
_INSTANTIATE_FUNCTION_SPACE(extern,F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_FUNCTION_SPACE(extern,BigRat)
#endif
#endif
#ifdef HIGH_PRECISION
_INSTANTIATE_FUNCTION_SPACE(extern,CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_FUNCTION_SPACE(extern,CVHP)
#endif

} // namespace FunctionSpace
} // namespace Caravel
