/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#include "FunctionSpace/FunctionSpace.h"

#include "FunctionSpace/IBPs/FactorizableFunctions.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Factorizable{

void load()
{
using gType = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2], Link[], Leg[3, 0], Link[]], Strand[LoopMomentum[-2], Leg[4], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[], Leg[3], Link[]], Strand[LoopMomentum[-2], Leg[4], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[], Leg[3, 0], Link[]], Strand[LoopMomentum[-2], Leg[4], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[4, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[], Leg[3, 0], Link[]], Strand[LoopMomentum[-2], Leg[5], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[-2], Leg[2], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[2]]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[-2], Leg[3], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[-2], Leg[3], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]], Strand[LoopMomentum[-2], Leg[5], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[5, 0], Link[], Leg[4, 0], Link[], Leg[3, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[5, 0], Link[], Leg[4], Link[], Leg[3, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[5, 0], Link[], Leg[4, 0], Link[], Leg[3], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[5, 0], Link[], Leg[4, 0], Link[], Leg[3, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[6, 0], Link[], Leg[5, 0], Link[], Leg[4, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[], Leg[2], Link[]], Strand[LoopMomentum[-2], Leg[3], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[3], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[3], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[1], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4], Link[], Leg[3, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4], Link[], Leg[3], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[5, 0], Link[], Leg[4], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[5, 0], Link[], Leg[4, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[5, 0], Link[], Leg[4, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = factorizable_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {factorizable_surface_function<C>()};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = factorizable_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {factorizable_surface_function<CHP>()};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = factorizable_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {factorizable_surface_function<CVHP>()};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = factorizable_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {factorizable_surface_function<F32>()};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 1], Link[1], Leg[2, 1], Link[]], Strand[LoopMomentum[-2], Leg[4, 2], Link[2], Leg[3, 2], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::vector<std::string>>(mdata) = {"scalar"};
std::get<IBPCoordinatesFP<C,6>>(data) = empty_IBP_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = {};
std::get<std::vector<BasisFunction<C>>>(mdata) = {scalar_master<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = empty_IBP_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = {};
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {scalar_master<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = empty_IBP_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {scalar_master<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = empty_IBP_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = {};
std::get<std::vector<BasisFunction<F32>>>(mdata) = {scalar_master<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
}

}}}}

