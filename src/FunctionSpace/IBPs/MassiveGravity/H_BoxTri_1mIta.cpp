/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#include "FunctionSpace/IBPhelper.h"

#include "H_BoxTri_1mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace MassiveGravity{

template std::array<SurfaceTermsFunctionPointer<C>,0> H_BoxTri_1mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,0> H_BoxTri_1mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,0> H_BoxTri_1mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,0> H_BoxTri_1mSurfaceIta<F32>();

#endif

}}}}

