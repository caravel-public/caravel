/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#pragma once

#include "BowTieVectors.hpp"

#include "BowTieInfo.hpp"

#include "FanVectors.hpp"

#include "FanInfo.hpp"

#include "g11Vectors.hpp"

#include "g11Info.hpp"

#include "g12Vectors.hpp"

#include "g12Info.hpp"

#include "HVectors.hpp"

#include "HInfo.hpp"

#include "IIIVectors.hpp"

#include "IIIInfo.hpp"

#include "IXVectors.hpp"

#include "IXInfo.hpp"

#include "IX_BoxTriVectors.hpp"

#include "IX_BoxTriInfo.hpp"

#include "IYVectors.hpp"

#include "IYInfo.hpp"

#include "IY_BoxTriVectors.hpp"

#include "IY_BoxTriInfo.hpp"

#include "NVectors.hpp"

#include "NInfo.hpp"

#include "xIYVectors.hpp"

#include "xIYInfo.hpp"

#include "xIY_BoxBoxVectors.hpp"

#include "xIY_BoxBoxInfo.hpp"

