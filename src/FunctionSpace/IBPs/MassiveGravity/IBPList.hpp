/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#pragma once

#include "BowTieIta.hpp"

#include "FanIta.hpp"

#include "g11Ita.hpp"

#include "g12Ita.hpp"

#include "HIta.hpp"

#include "IIIIta.hpp"

#include "IXIta.hpp"

#include "IX_BoxTriIta.hpp"

#include "IYIta.hpp"

#include "IY_BoxTriIta.hpp"

#include "NIta.hpp"

#include "xIYIta.hpp"

#include "xIY_BoxBoxIta.hpp"

