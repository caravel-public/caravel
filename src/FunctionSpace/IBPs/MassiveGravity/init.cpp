#include "FunctionSpace/FunctionSpace.h"

#include "IBPInfoList.hpp"

#include "IBPList.hpp"

#include "Masters.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace MassiveGravity{

void load()
{
using gType = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[Link[0], Bead[Leg[1, 1]], Link[1], Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0]], Strand[Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2], Bead[Leg[3, 2]], Link[0]]], BowTie]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BowTieCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BowTieSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BowTieCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BowTieSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BowTieCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BowTieSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BowTieCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BowTieSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1]], Node[]], Connection[Strand[Link[0]], Strand[Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]], Strand[Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]]], Fan]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 39;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = FanCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(FanSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = FanCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(FanSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = FanCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(FanSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = FanCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(FanSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[4, 1]], Node[]], Connection[Strand[Link[0]], Strand[Link[1], Bead[Leg[1, 1]], Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]], Strand[Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]]], g11]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 192;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = g11Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(g11SurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = g11Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(g11SurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = g11Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(g11SurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = g11Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(g11SurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1, 1]], Node[]], Connection[Strand[Link[0]], Strand[Link[1], Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0]], Strand[Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2], Bead[Leg[3, 2]], Link[0]]], g12]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 192;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = g12Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(g12SurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = g12Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(g12SurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = g12Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(g12SurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = g12Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(g12SurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0]], Strand[Link[0], Bead[Leg[1, 1]], Link[1], Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0]], Strand[Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2], Bead[Leg[3, 2]], Link[0]]], H]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 71;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = HCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(HSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = HCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(HSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = HCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(HSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = HCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(HSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0]], Strand[Link[1], Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]], Strand[Link[1], Bead[Leg[1, 1]], Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]]], III]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 71;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = IIICoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(IIISurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = IIICoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(IIISurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = IIICoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(IIISurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = IIICoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(IIISurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[2], 0], Bead[Leg[4, 1]], Link[1]], Strand[Link[2], Bead[Leg[3, 2]], Link[0]], Strand[Link[2], Bead[Leg[2, 2]], Link[LoopMomentum[1], 0], Bead[Leg[1, 1]], Link[1]]], IX]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 78;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = IXCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(IXSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = IXCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(IXSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = IXCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(IXSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = IXCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(IXSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[1, 1]]], Connection[Strand[Link[LoopMomentum[2], 0], Bead[Leg[4, 1]], Link[1]], Strand[Link[2], Bead[Leg[3, 2]], Link[0]], Strand[Link[2], Bead[Leg[2, 2]], Link[LoopMomentum[1], 0]]], IX_BoxTri]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 251;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = IX_BoxTriCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(IX_BoxTriSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = IX_BoxTriCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(IX_BoxTriSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = IX_BoxTriCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(IX_BoxTriSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = IX_BoxTriCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(IX_BoxTriSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0]], Strand[Link[0], Bead[Leg[3, 2]], Link[2], Bead[Leg[2, 2]], Link[LoopMomentum[1], 0], Bead[Leg[1, 1]], Link[1]], Strand[Link[LoopMomentum[2], 0], Bead[Leg[4, 1]], Link[1]]], IY]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 28;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = IYCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(IYSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = IYCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(IYSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = IYCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(IYSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = IYCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(IYSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0]], Strand[Link[0], Bead[Leg[1]], Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]], Strand[Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]]], IY_BoxTri]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 7;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = IY_BoxTriCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(IY_BoxTriSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = IY_BoxTriCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(IY_BoxTriSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = IY_BoxTriCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(IY_BoxTriSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = IY_BoxTriCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(IY_BoxTriSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[4, 1]], Node[Leg[2, 2]]], Connection[Strand[Link[0]], Strand[Link[1], Bead[Leg[1, 1]], Link[LoopMomentum[-1], 0]], Strand[Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]]], N]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 39;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = NCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(NSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = NCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(NSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = NCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(NSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = NCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(NSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0], Bead[Leg[3, 2]], Link[2]], Strand[Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]], Strand[Link[0], Bead[Leg[1, 1]], Link[1], Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0]]], xIY]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 45;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = xIYCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(xIYSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = xIYCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(xIYSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = xIYCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(xIYSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = xIYCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(xIYSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0], Bead[Leg[3, 2]], Link[2]], Strand[Link[LoopMomentum[-1]], Bead[Leg[2, 2]], Link[2]], Strand[Link[0], Bead[Leg[1]], Link[LoopMomentum[-2], 0]]], xIY_BoxBox]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 21;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = xIY_BoxBoxCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(xIY_BoxBoxSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = xIY_BoxBoxCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(xIY_BoxBoxSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = xIY_BoxBoxCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(xIY_BoxBoxSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = xIY_BoxBoxCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(xIY_BoxBoxSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
surface_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(data);
master_term_data_holder[std::get<lGraph::xGraph>(data).get_topology()] = std::move(mdata);
}
}

}}}}

