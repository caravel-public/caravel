/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#include "FunctionSpace/IBPhelper.h"

#include "IX_BoxTriIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace MassiveGravity{

template std::array<SurfaceTermsFunctionPointer<C>,0> IX_BoxTriSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,0> IX_BoxTriSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,0> IX_BoxTriSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,0> IX_BoxTriSurfaceIta<F32>();

#endif

}}}}

