/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#include "FunctionSpace/IBPhelper.h"

#include "Core/momD.h"

#include "BowTieVectors.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace MassiveGravity{

template std::vector<std::vector<C>> BowTie_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

template std::vector<std::vector<CHP>> BowTie_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

template std::vector<std::vector<CVHP>> BowTie_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

template std::vector<std::vector<F32>> BowTie_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

