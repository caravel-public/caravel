/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace MassiveGravity{

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,0> IY_BoxTriSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,0> IY_BoxTriSurfaceItaArray;
return IY_BoxTriSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,0> IY_BoxTriSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,0> IY_BoxTriSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,0> IY_BoxTriSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,0> IY_BoxTriSurfaceIta<F32>();

#endif

}}}}

