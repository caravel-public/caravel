/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace MassiveGravity{

template<typename T> std::vector<std::vector<T>> xIY_vectors(const T* , const T* , const T* );

template<typename T, size_t D> IBPCoordinates<T> xIYCoordinates(const OnShellPoint<T,D>& l, const std::vector<momentumD<T,D>>&  P, const std::vector<std::vector<T>>& ccoeffs)
{
auto invariant1 = P[0] * P[1];
auto invariant2 = P[1] * P[2];
auto invariant3 = P[0] * P[0];
auto invariant4 = P[1] * P[1];
auto invdimension = T(1) / invariant1;
std::vector<T> invariants = {invdimension * invariant1, invdimension * invariant2, invdimension * invariant3, invdimension * invariant4, invariant1};
auto D1mom = l[0] + -P[0];
auto D1 = invdimension * (D1mom * D1mom);
auto D2mom = l[0] + P[1];
auto D2 = invdimension * (D2mom * D2mom);
auto D3mom = l[1] + P[0] + P[1] + P[2];
auto D3 = invdimension * (D3mom * D3mom);
auto D4mom = l[0] + l[1] + P[1];
auto D4 = invdimension * (D4mom * D4mom);
auto D5mom = l[0];
auto D5 = invdimension * (D5mom * D5mom);
auto D6mom = l[1];
auto D6 = invdimension * (D6mom * D6mom);
auto D7mom = l[0] + l[1] + P[1] + P[2];
auto D7 = invdimension * (D7mom * D7mom);
auto D8mom = l[0] + P[1] + P[2];
auto D8 = invdimension * (D8mom * D8mom);
auto D9mom = l[1] + P[1] + P[2];
auto D9 = invdimension * (D9mom * D9mom);
auto alpha1 = (-D1 + D3) / T(2);
auto alpha2 = (-D6 + D2) / T(2);
std::vector<T> props = {alpha1, alpha2, D3, D4, D5, D6, D7, D8, D9};
auto vector_components = xIY_vectors(&props[0], &invariants[0],ccoeffs);
return IBPCoordinates<T>{props, invariants, vector_components};
}

}}}}

