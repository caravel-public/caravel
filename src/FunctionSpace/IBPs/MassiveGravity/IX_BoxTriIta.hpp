/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace MassiveGravity{

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,0> IX_BoxTriSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,0> IX_BoxTriSurfaceItaArray;
return IX_BoxTriSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,0> IX_BoxTriSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,0> IX_BoxTriSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,0> IX_BoxTriSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,0> IX_BoxTriSurfaceIta<F32>();

#endif

}}}}

