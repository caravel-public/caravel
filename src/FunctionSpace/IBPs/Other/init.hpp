#include "FunctionSpace/MasterIntegrands.h"

namespace Caravel {
namespace FunctionSpace {
namespace SurfaceTerms {
namespace Other {

using namespace MasterIntegrands;

template <typename T, size_t D>
IBPCoordinates<T> DummyIBPCoord(const OnShellPoint<T, D>& l, const std::vector<momentumD<T, D>>& P, const std::vector<std::vector<T>>& ccoeffs) {
    std::vector<T> propagators;
    std::vector<T> invariants;
    std::vector<std::vector<T>> vector_components;
    return IBPCoordinates<T>{propagators, invariants, vector_components};
}

template <typename T, size_t D> std::vector<T> Pentagon1M_coordinates(const OnShellPoint<T, D>& l, const momD_conf<T, D>& mc) {
    // tr5 and mu^2
    return {T(4) * levi(mc[1], mc[2], mc[3], mc[4]), mu(l[0], l[0])};
}

template <typename T> T Pentagon1M(const TopologyCoordinates<T>& cs) {
    const auto& abb = cs.special;
    // insertion: tr5 * mu^2
    return abb[0] * abb[1];
}

template <typename T, size_t D> std::vector<T> Box1M_coordinates(const OnShellPoint<T, D>& l, const momD_conf<T, D>& mc) {
    // s23 and s34
    return {mc.s(2, 3), mc.s(3, 4)};
}

template <typename T> T Box1M(const TopologyCoordinates<T>& cs) {
    const auto& abb = cs.special;
    // insertion: s23 * s34
    return abb[0] * abb[1];
}

template <typename T, size_t D> std::vector<T> Box2MEasy_coordinates(const OnShellPoint<T, D>& l, const momD_conf<T, D>& mc) {
    // s14, s34
    return {mc.s(1, 4), mc.s(3, 4)};
}

template <typename T> T Box2MEasy(const TopologyCoordinates<T>& cs) {
    const auto& abb = cs.special;
    // insertion: s14 * s34
    return abb[0] * abb[1];
}

template <typename T, size_t D> std::vector<T> Box2MHard_coordinates(const OnShellPoint<T, D>& l, const momD_conf<T, D>& mc) {
    // s12, s14, p1^2, p3^2
    return {mc.s(1, 2), mc.s(1, 4), sq(mc[1]), sq(mc[3])};
}

template <typename T> T Box2MHard(const TopologyCoordinates<T>& cs) {
    const auto& abb = cs.special;
    // insertion: s12 * s14 - p1^2 *p3^2
    return abb[0] * abb[1] - abb[2] * abb[3];
}

template <typename T, size_t D>
IBPCoordinates<T> Triangle1Mor2M_coordinates(const OnShellPoint<T, D>& l, const std::vector<momentumD<T, D>>& P, const std::vector<std::vector<T>>& ccoeffs) {
    auto p1dotp2 = P[0] * P[1];
    auto rho2mom = l[0] + -P[0];
    auto rho2 = (rho2mom * rho2mom);
    auto rho3mom = rho2mom + -P[1];
    auto rho3 = (rho3mom * rho3mom);
    std::vector<T> props = {rho2, rho3};
    std::vector<T> invariants = {p1dotp2};
    std::vector<std::vector<T>> vector_components;
    return IBPCoordinates<T>{props, invariants, vector_components};
}


template <typename T> T Triangle1Mor2M(const TopologyCoordinates<T>& cs, const T& d_val) {
    const auto& props = cs.propagators;
    const auto& invs = cs.invariants;
    // insertion: (D-4) * p1.p2 + (D-3) * (rho2-rho3)
    return (d_val - T(4)) * invs[0] + (d_val - T(3)) * (props[0] - props[1]);
}

template <typename T, size_t D>
IBPCoordinates<T> Triangle2M_massive_coordinates(const OnShellPoint<T, D>& l, const std::vector<momentumD<T, D>>& P,
                                                 const std::vector<std::vector<T>>& ccoeffs) {
    auto s = (P[0] + P[1]) * (P[0] + P[1]);
    auto msq = P[1] * P[1];
    auto rho1 = l[0] * l[0];
    auto rho2mom = l[0] - P[0];
    auto rho2 = rho2mom * rho2mom;
    std::vector<T> props = {rho1, rho2};
    std::vector<T> invariants = {msq, s};
    std::vector<std::vector<T>> vector_components;
    return IBPCoordinates<T>{props, invariants, vector_components};
}

template <typename T> T Triangle2M_massive(const TopologyCoordinates<T>& cs, const T& d_val) {
    const auto& props = cs.propagators;
    const auto& invs = cs.invariants;
    // insertion: 2(D-4) m^2(m^2-s)^2 + 4(D-3) s*m^2 rho2 - (d-2)(m^2+s) rho1*rho2
    return T(2) * (d_val - T(4)) * invs[0] * (invs[0] - invs[1]) * (invs[0] - invs[1]) + T(4) * (d_val - T(3)) * invs[1] * invs[0] * props[1] -
           (d_val - T(2)) * (invs[0] + invs[1]) * props[0] * props[1];
}

template <typename T, size_t D> std::vector<T> Triangle3M_coordinates(const OnShellPoint<T, D>& l, const momD_conf<T, D>& mc) {
    // do nothing
    return {};
}

template <typename T> T Triangle3M(const TopologyCoordinates<T>& cs) {
    // insertion: 1
    return T(1);
}

template <typename T, size_t D> std::vector<T> Bubble_coordinates(const OnShellPoint<T, D>& l, const momD_conf<T, D>& mc) {
    return {};
}

template <typename T> T Bubble(const TopologyCoordinates<T>& cs, const T& d_val) {
    const auto& ep = (T(4) - d_val) / T(2);
    // insertion: (1 - 2*ep)/ep
    return (T(1) - T(2) * ep) / ep;
}

inline void load() {
    /**
     * Here some surface terms can be put by hand. See imported surface term sets for how this looks like
     */

    // using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
    using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[], Leg[5, 0], "
                    "Link[]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 1;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<std::string>(mdata) = "Pentagon1M";
        std::get<std::vector<std::string>>(mdata) = {"pure"};
        std::get<IBPCoordinatesFP<C, 5>>(data) = DummyIBPCoord<C, 5>;
        std::get<MasterCoordinatesFP<C, 5>>(mdata) = Pentagon1M_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {Pentagon1M<C>};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = DummyIBPCoord<CHP, 5>;
        std::get<MasterCoordinatesFP<CHP, 5>>(mdata) = Pentagon1M_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Pentagon1M<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = DummyIBPCoord<CVHP, 5>;
        std::get<MasterCoordinatesFP<CVHP, 5>>(mdata) = Pentagon1M_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Pentagon1M<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = DummyIBPCoord<F32, 5>;
        std::get<MasterCoordinatesFP<F32, 5>>(mdata) = Pentagon1M_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {Pentagon1M<F32>};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 1;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<std::string>(mdata) = "Box1M";
        std::get<std::vector<std::string>>(mdata) = {"pure"};
        std::get<IBPCoordinatesFP<C, 5>>(data) = DummyIBPCoord<C, 5>;
        std::get<MasterCoordinatesFP<C, 5>>(mdata) = Box1M_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {Box1M<C>};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = DummyIBPCoord<CHP, 5>;
        std::get<MasterCoordinatesFP<CHP, 5>>(mdata) = Box1M_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Box1M<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = DummyIBPCoord<CVHP, 5>;
        std::get<MasterCoordinatesFP<CVHP, 5>>(mdata) = Box1M_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Box1M<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = DummyIBPCoord<F32, 5>;
        std::get<MasterCoordinatesFP<F32, 5>>(mdata) = Box1M_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {Box1M<F32>};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 1;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<std::string>(mdata) = "Box2MEasy";
        std::get<std::vector<std::string>>(mdata) = {"pure"};
        std::get<IBPCoordinatesFP<C, 5>>(data) = DummyIBPCoord<C, 5>;
        std::get<MasterCoordinatesFP<C, 5>>(mdata) = Box2MEasy_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {Box2MEasy<C>};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = DummyIBPCoord<CHP, 5>;
        std::get<MasterCoordinatesFP<CHP, 5>>(mdata) = Box2MEasy_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Box2MEasy<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = DummyIBPCoord<CVHP, 5>;
        std::get<MasterCoordinatesFP<CVHP, 5>>(mdata) = Box2MEasy_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Box2MEasy<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = DummyIBPCoord<F32, 5>;
        std::get<MasterCoordinatesFP<F32, 5>>(mdata) = Box2MEasy_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {Box2MEasy<F32>};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3], Link[], Leg[4, 0], Link[]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 1;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<std::string>(mdata) = "Box2MHard";
        std::get<std::vector<std::string>>(mdata) = {"pure"};
        std::get<IBPCoordinatesFP<C, 5>>(data) = DummyIBPCoord<C, 5>;
        std::get<MasterCoordinatesFP<C, 5>>(mdata) = Box2MHard_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {Box2MHard<C>};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = DummyIBPCoord<CHP, 5>;
        std::get<MasterCoordinatesFP<CHP, 5>>(mdata) = Box2MHard_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Box2MHard<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = DummyIBPCoord<CVHP, 5>;
        std::get<MasterCoordinatesFP<CVHP, 5>>(mdata) = Box2MHard_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Box2MHard<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = DummyIBPCoord<F32, 5>;
        std::get<MasterCoordinatesFP<F32, 5>>(mdata) = Box2MHard_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {Box2MHard<F32>};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[], Leg[2, 0], Link[], Leg[3], LoopMomentum[1]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 0;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<IBPCoordinatesFP<C, 5>>(data) = Triangle1Mor2M_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {Triangle1Mor2M<C>};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = Triangle1Mor2M_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {Triangle1Mor2M<CHP>};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = Triangle1Mor2M_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {Triangle1Mor2M<CVHP>};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = Triangle1Mor2M_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {Triangle1Mor2M<F32>};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[], Leg[2, 0], Link[], Leg[3], LoopMomentum[1]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 0;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<IBPCoordinatesFP<C, 5>>(data) = Triangle1Mor2M_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {Triangle1Mor2M<C>};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = Triangle1Mor2M_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {Triangle1Mor2M<CHP>};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = Triangle1Mor2M_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {Triangle1Mor2M<CVHP>};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = Triangle1Mor2M_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {Triangle1Mor2M<F32>};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[0], Leg[2,1], Link[1], Leg[3], LoopMomentum[1]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 0;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<IBPCoordinatesFP<C, 5>>(data) = Triangle2M_massive_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {Triangle2M_massive<C>};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = Triangle2M_massive_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {Triangle2M_massive<CHP>};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = Triangle2M_massive_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {Triangle2M_massive<CVHP>};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = Triangle2M_massive_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {Triangle2M_massive<F32>};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[], Leg[3], Link[]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 1;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<std::string>(mdata) = "Triangle3M";
        std::get<std::vector<std::string>>(mdata) = {"pure"};
        std::get<IBPCoordinatesFP<C, 5>>(data) = DummyIBPCoord<C, 5>;
        std::get<MasterCoordinatesFP<C, 5>>(mdata) = Triangle3M_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {Triangle3M<C>};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = DummyIBPCoord<CHP, 5>;
        std::get<MasterCoordinatesFP<CHP, 5>>(mdata) = Triangle3M_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Triangle3M<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = DummyIBPCoord<CVHP, 5>;
        std::get<MasterCoordinatesFP<CVHP, 5>>(mdata) = Triangle3M_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Triangle3M<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = DummyIBPCoord<F32, 5>;
        std::get<MasterCoordinatesFP<F32, 5>>(mdata) = Triangle3M_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {Triangle3M<F32>};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
    {
        gType graph("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[]]]]");
        SurfaceTermData data;
        MasterTermData mdata;
        std::get<size_t>(data) = 1;
        std::get<lGraph::xGraph>(data) = graph;
        std::get<lGraph::xGraph>(mdata) = graph;
        std::get<std::string>(mdata) = "Bubble";
        std::get<std::vector<std::string>>(mdata) = {"pure"};
        std::get<IBPCoordinatesFP<C, 5>>(data) = DummyIBPCoord<C, 5>;
        std::get<MasterCoordinatesFP<C, 5>>(mdata) = Bubble_coordinates<C, 5>;
        std::get<std::vector<BasisFunction<C>>>(data) = {};
        std::get<std::vector<BasisFunction<C>>>(mdata) = {Bubble<C>};
#ifdef HIGH_PRECISION
        std::get<IBPCoordinatesFP<CHP, 5>>(data) = DummyIBPCoord<CHP, 5>;
        std::get<MasterCoordinatesFP<CHP, 5>>(mdata) = Bubble_coordinates<CHP, 5>;
        std::get<std::vector<BasisFunction<CHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Bubble<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
        std::get<IBPCoordinatesFP<CVHP, 5>>(data) = DummyIBPCoord<CVHP, 5>;
        std::get<MasterCoordinatesFP<CVHP, 5>>(mdata) = Bubble_coordinates<CVHP, 5>;
        std::get<std::vector<BasisFunction<CVHP>>>(data) = {};
        std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Bubble<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
        std::get<IBPCoordinatesFP<F32, 5>>(data) = DummyIBPCoord<F32, 5>;
        std::get<MasterCoordinatesFP<F32, 5>>(mdata) = Bubble_coordinates<F32, 5>;
        std::get<std::vector<BasisFunction<F32>>>(data) = {};
        std::get<std::vector<BasisFunction<F32>>>(mdata) = {Bubble<F32>};
#endif
        auto TopoKey = std::get<lGraph::xGraph>(data).get_topology();
        surface_term_data_holder[TopoKey] = std::move(data);
        master_term_data_holder[TopoKey] = std::move(mdata);
    }
}

} // namespace Other
} // namespace SurfaceTerms
} // namespace FunctionSpace
} // namespace Caravel
