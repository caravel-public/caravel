/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][5];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][5];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002ny___zz__mSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][5];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[3][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,28> BubbleBox_002ny___zz__mSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,28> BubbleBox_002ny___zz__mSurfaceItaArray;
BubbleBox_002ny___zz__mSurfaceItaArray[0] = {BubbleBox_002ny___zz__mSurfaceIta1, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[1] = {BubbleBox_002ny___zz__mSurfaceIta2, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[2] = {BubbleBox_002ny___zz__mSurfaceIta3, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[3] = {BubbleBox_002ny___zz__mSurfaceIta4, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[4] = {BubbleBox_002ny___zz__mSurfaceIta5, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[5] = {BubbleBox_002ny___zz__mSurfaceIta6, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[6] = {BubbleBox_002ny___zz__mSurfaceIta7, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[7] = {BubbleBox_002ny___zz__mSurfaceIta8, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[8] = {BubbleBox_002ny___zz__mSurfaceIta9, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[9] = {BubbleBox_002ny___zz__mSurfaceIta10, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[10] = {BubbleBox_002ny___zz__mSurfaceIta11, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[11] = {BubbleBox_002ny___zz__mSurfaceIta12, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[12] = {BubbleBox_002ny___zz__mSurfaceIta13, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[13] = {BubbleBox_002ny___zz__mSurfaceIta14, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[14] = {BubbleBox_002ny___zz__mSurfaceIta15, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[15] = {BubbleBox_002ny___zz__mSurfaceIta16, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[16] = {BubbleBox_002ny___zz__mSurfaceIta17, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[17] = {BubbleBox_002ny___zz__mSurfaceIta18, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[18] = {BubbleBox_002ny___zz__mSurfaceIta19, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[19] = {BubbleBox_002ny___zz__mSurfaceIta20, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[20] = {BubbleBox_002ny___zz__mSurfaceIta21, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[21] = {BubbleBox_002ny___zz__mSurfaceIta22, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[22] = {BubbleBox_002ny___zz__mSurfaceIta23, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[23] = {BubbleBox_002ny___zz__mSurfaceIta24, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[24] = {BubbleBox_002ny___zz__mSurfaceIta25, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[25] = {BubbleBox_002ny___zz__mSurfaceIta26, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[26] = {BubbleBox_002ny___zz__mSurfaceIta27, -1};
BubbleBox_002ny___zz__mSurfaceItaArray[27] = {BubbleBox_002ny___zz__mSurfaceIta28, -1};
return BubbleBox_002ny___zz__mSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,28> BubbleBox_002ny___zz__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,28> BubbleBox_002ny___zz__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,28> BubbleBox_002ny___zz__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,28> BubbleBox_002ny___zz__mSurfaceIta<F32>();

#endif

}}}}

