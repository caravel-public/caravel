#include "FunctionSpace/IBPhelper.h"

#include "BubbleBox_002ny___zm__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,28> BubbleBox_002ny___zm__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,28> BubbleBox_002ny___zm__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,28> BubbleBox_002ny___zm__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,28> BubbleBox_002ny___zm__zSurfaceIta<F32>();

#endif

}}}}

