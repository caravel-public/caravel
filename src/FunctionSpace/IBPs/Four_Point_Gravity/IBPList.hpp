#pragma once

#include "BoxBox_111nn_z_z_m__Ita.hpp"

#include "BoxBox_111ny_z_z_z__zIta.hpp"

#include "BoxBox_202nn_zz__zz__Ita.hpp"

#include "BoxPentagon_112nn_z_z_zz__Ita.hpp"

#include "BubbleBox_002nn___mm__Ita.hpp"

#include "BubbleBox_002ny___mz__zIta.hpp"

#include "BubbleBox_002ny___zm__zIta.hpp"

#include "BubbleBox_002ny___zz__mIta.hpp"

#include "BubbleBox_002yy___zz_z_zIta.hpp"

#include "BubbleBubble_000yy____m_mIta.hpp"

#include "BubbleHexagon_004nn___zzzz__Ita.hpp"

#include "BubblePentagon_003nn___mzz__Ita.hpp"

#include "BubblePentagon_003nn___zmz__Ita.hpp"

#include "BubblePentagon_003ny___zzz__zIta.hpp"

#include "BubbleTriangle_001ny___m__mIta.hpp"

#include "BubbleTriangle_001yy___m_z_zIta.hpp"

#include "BubbleTriangle_001yy___z_m_zIta.hpp"

#include "TriangleBox_102nn_m__zz__Ita.hpp"

#include "TriangleBox_102nn_z__mz__Ita.hpp"

#include "TriangleBox_102ny_z__zz__zIta.hpp"

#include "TrianglePentagon_103nn_z__zzz__Ita.hpp"

#include "TriangleTriangle_101nn_m__m__Ita.hpp"

#include "TriangleTriangle_101ny_z__m__zIta.hpp"

#include "TriangleTriangle_101ny_z__z__mIta.hpp"

#include "TriangleTriangle_101yy_z__z_z_zIta.hpp"

