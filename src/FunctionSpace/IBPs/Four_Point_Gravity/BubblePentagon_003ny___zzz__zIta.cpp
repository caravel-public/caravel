#include "FunctionSpace/IBPhelper.h"

#include "BubblePentagon_003ny___zzz__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,56> BubblePentagon_003ny___zzz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,56> BubblePentagon_003ny___zzz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,56> BubblePentagon_003ny___zzz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,56> BubblePentagon_003ny___zzz__zSurfaceIta<F32>();

#endif

}}}}

