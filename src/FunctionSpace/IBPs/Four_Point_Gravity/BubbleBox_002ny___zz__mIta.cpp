#include "FunctionSpace/IBPhelper.h"

#include "BubbleBox_002ny___zz__mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,28> BubbleBox_002ny___zz__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,28> BubbleBox_002ny___zz__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,28> BubbleBox_002ny___zz__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,28> BubbleBox_002ny___zz__mSurfaceIta<F32>();

#endif

}}}}

