#include "FunctionSpace/IBPhelper.h"

#include "BubbleTriangle_001yy___m_z_zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,191> BubbleTriangle_001yy___m_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,191> BubbleTriangle_001yy___m_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,191> BubbleTriangle_001yy___m_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,191> BubbleTriangle_001yy___m_z_zSurfaceIta<F32>();

#endif

}}}}

