#include "FunctionSpace/IBPhelper.h"

#include "Core/momD.h"

#include "BoxBox_202nn_zz__zz__Vectors.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::vector<std::vector<C>> BoxBox_202nn_zz__zz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

template std::vector<std::vector<CHP>> BoxBox_202nn_zz__zz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

template std::vector<std::vector<CVHP>> BoxBox_202nn_zz__zz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

template std::vector<std::vector<F32>> BoxBox_202nn_zz__zz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

