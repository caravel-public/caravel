#ifndef TriangleTriangle_101nn_m__m__Vectors_H_INC

#define TriangleTriangle_101nn_m__m__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> T TriangleTriangle_101nn_m__m__vec1_component6(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = z[3] + -z[2];
return z[1] * T(-3) + T(1) + z[4] * T(2) + -z[0];
}

template<typename T> T TriangleTriangle_101nn_m__m__vec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
return z[0] + z[1] + -z[2];
}

template<typename T> std::vector<std::vector<T>> TriangleTriangle_101nn_m__m___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(1, std::vector<T>(7, T(0)));
ibp_vectors[0][5] = TriangleTriangle_101nn_m__m__vec1_component6(invariants, propagators);
ibp_vectors[0].back() = TriangleTriangle_101nn_m__m__vec1_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleTriangle_101nn_m__m___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleTriangle_101nn_m__m___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleTriangle_101nn_m__m___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleTriangle_101nn_m__m___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

