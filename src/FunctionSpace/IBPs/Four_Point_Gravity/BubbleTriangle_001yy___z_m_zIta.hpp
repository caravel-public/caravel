/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][5];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta77(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta78(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta79(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta80(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta81(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta82(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta83(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta84(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta85(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta86(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta87(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta88(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta89(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta90(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta91(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta92(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta93(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(3);
z[10] = z[1] * prod_pow(z[2], 2) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta94(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta95(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta96(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta97(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta98(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta99(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta100(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta101(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta102(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta103(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta104(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta105(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta106(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta107(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta108(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta109(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta110(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta111(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta112(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta113(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta114(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta115(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta116(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta117(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(4);
z[8] = prod_pow(z[0], 3) * z[2] * z[8];
z[9] = prod_pow(z[0], 4) * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta118(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta119(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta120(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta121(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[0], 2) * z[2] * z[8];
z[9] = prod_pow(z[0], 3) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta122(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 3) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(3);
z[7] = prod_pow(z[0], 2) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta123(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta124(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = z[7] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(3);
z[10] = z[0] * prod_pow(z[2], 2) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[1] * z[9];
z[8] = prod_pow(z[0], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta125(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta126(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta127(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta128(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta129(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta130(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta131(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta132(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta133(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta134(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta135(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta136(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta137(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 3) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(3);
z[7] = prod_pow(z[0], 2) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta138(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta139(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta140(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta141(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][5];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta142(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta143(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta144(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta145(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta146(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta147(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta148(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta149(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta150(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta151(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta152(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta153(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta154(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta155(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta156(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta157(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta158(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta159(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta160(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta161(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta162(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta163(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta164(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta165(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta166(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta167(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][5];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta168(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta169(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta170(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta171(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta172(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta173(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + (z[3] * T(5)) / T(2);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta174(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta175(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta176(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta177(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + (z[3] * T(5)) / T(2);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta178(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta179(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta180(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + (z[3] * T(5)) / T(2);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta181(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + (z[3] * T(5)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][5];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta182(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta183(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][5];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta184(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][5];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta185(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta186(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta187(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta188(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta189(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta190(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta191(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][5];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_zSurfaceIta192(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][5];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][5];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,192> BubbleTriangle_001yy___z_m_zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,192> BubbleTriangle_001yy___z_m_zSurfaceItaArray;
BubbleTriangle_001yy___z_m_zSurfaceItaArray[0] = {BubbleTriangle_001yy___z_m_zSurfaceIta1, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[1] = {BubbleTriangle_001yy___z_m_zSurfaceIta2, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[2] = {BubbleTriangle_001yy___z_m_zSurfaceIta3, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[3] = {BubbleTriangle_001yy___z_m_zSurfaceIta4, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[4] = {BubbleTriangle_001yy___z_m_zSurfaceIta5, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[5] = {BubbleTriangle_001yy___z_m_zSurfaceIta6, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[6] = {BubbleTriangle_001yy___z_m_zSurfaceIta7, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[7] = {BubbleTriangle_001yy___z_m_zSurfaceIta8, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[8] = {BubbleTriangle_001yy___z_m_zSurfaceIta9, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[9] = {BubbleTriangle_001yy___z_m_zSurfaceIta10, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[10] = {BubbleTriangle_001yy___z_m_zSurfaceIta11, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[11] = {BubbleTriangle_001yy___z_m_zSurfaceIta12, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[12] = {BubbleTriangle_001yy___z_m_zSurfaceIta13, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[13] = {BubbleTriangle_001yy___z_m_zSurfaceIta14, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[14] = {BubbleTriangle_001yy___z_m_zSurfaceIta15, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[15] = {BubbleTriangle_001yy___z_m_zSurfaceIta16, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[16] = {BubbleTriangle_001yy___z_m_zSurfaceIta17, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[17] = {BubbleTriangle_001yy___z_m_zSurfaceIta18, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[18] = {BubbleTriangle_001yy___z_m_zSurfaceIta19, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[19] = {BubbleTriangle_001yy___z_m_zSurfaceIta20, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[20] = {BubbleTriangle_001yy___z_m_zSurfaceIta21, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[21] = {BubbleTriangle_001yy___z_m_zSurfaceIta22, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[22] = {BubbleTriangle_001yy___z_m_zSurfaceIta23, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[23] = {BubbleTriangle_001yy___z_m_zSurfaceIta24, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[24] = {BubbleTriangle_001yy___z_m_zSurfaceIta25, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[25] = {BubbleTriangle_001yy___z_m_zSurfaceIta26, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[26] = {BubbleTriangle_001yy___z_m_zSurfaceIta27, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[27] = {BubbleTriangle_001yy___z_m_zSurfaceIta28, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[28] = {BubbleTriangle_001yy___z_m_zSurfaceIta29, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[29] = {BubbleTriangle_001yy___z_m_zSurfaceIta30, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[30] = {BubbleTriangle_001yy___z_m_zSurfaceIta31, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[31] = {BubbleTriangle_001yy___z_m_zSurfaceIta32, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[32] = {BubbleTriangle_001yy___z_m_zSurfaceIta33, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[33] = {BubbleTriangle_001yy___z_m_zSurfaceIta34, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[34] = {BubbleTriangle_001yy___z_m_zSurfaceIta35, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[35] = {BubbleTriangle_001yy___z_m_zSurfaceIta36, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[36] = {BubbleTriangle_001yy___z_m_zSurfaceIta37, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[37] = {BubbleTriangle_001yy___z_m_zSurfaceIta38, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[38] = {BubbleTriangle_001yy___z_m_zSurfaceIta39, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[39] = {BubbleTriangle_001yy___z_m_zSurfaceIta40, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[40] = {BubbleTriangle_001yy___z_m_zSurfaceIta41, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[41] = {BubbleTriangle_001yy___z_m_zSurfaceIta42, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[42] = {BubbleTriangle_001yy___z_m_zSurfaceIta43, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[43] = {BubbleTriangle_001yy___z_m_zSurfaceIta44, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[44] = {BubbleTriangle_001yy___z_m_zSurfaceIta45, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[45] = {BubbleTriangle_001yy___z_m_zSurfaceIta46, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[46] = {BubbleTriangle_001yy___z_m_zSurfaceIta47, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[47] = {BubbleTriangle_001yy___z_m_zSurfaceIta48, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[48] = {BubbleTriangle_001yy___z_m_zSurfaceIta49, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[49] = {BubbleTriangle_001yy___z_m_zSurfaceIta50, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[50] = {BubbleTriangle_001yy___z_m_zSurfaceIta51, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[51] = {BubbleTriangle_001yy___z_m_zSurfaceIta52, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[52] = {BubbleTriangle_001yy___z_m_zSurfaceIta53, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[53] = {BubbleTriangle_001yy___z_m_zSurfaceIta54, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[54] = {BubbleTriangle_001yy___z_m_zSurfaceIta55, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[55] = {BubbleTriangle_001yy___z_m_zSurfaceIta56, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[56] = {BubbleTriangle_001yy___z_m_zSurfaceIta57, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[57] = {BubbleTriangle_001yy___z_m_zSurfaceIta58, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[58] = {BubbleTriangle_001yy___z_m_zSurfaceIta59, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[59] = {BubbleTriangle_001yy___z_m_zSurfaceIta60, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[60] = {BubbleTriangle_001yy___z_m_zSurfaceIta61, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[61] = {BubbleTriangle_001yy___z_m_zSurfaceIta62, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[62] = {BubbleTriangle_001yy___z_m_zSurfaceIta63, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[63] = {BubbleTriangle_001yy___z_m_zSurfaceIta64, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[64] = {BubbleTriangle_001yy___z_m_zSurfaceIta65, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[65] = {BubbleTriangle_001yy___z_m_zSurfaceIta66, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[66] = {BubbleTriangle_001yy___z_m_zSurfaceIta67, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[67] = {BubbleTriangle_001yy___z_m_zSurfaceIta68, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[68] = {BubbleTriangle_001yy___z_m_zSurfaceIta69, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[69] = {BubbleTriangle_001yy___z_m_zSurfaceIta70, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[70] = {BubbleTriangle_001yy___z_m_zSurfaceIta71, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[71] = {BubbleTriangle_001yy___z_m_zSurfaceIta72, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[72] = {BubbleTriangle_001yy___z_m_zSurfaceIta73, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[73] = {BubbleTriangle_001yy___z_m_zSurfaceIta74, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[74] = {BubbleTriangle_001yy___z_m_zSurfaceIta75, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[75] = {BubbleTriangle_001yy___z_m_zSurfaceIta76, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[76] = {BubbleTriangle_001yy___z_m_zSurfaceIta77, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[77] = {BubbleTriangle_001yy___z_m_zSurfaceIta78, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[78] = {BubbleTriangle_001yy___z_m_zSurfaceIta79, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[79] = {BubbleTriangle_001yy___z_m_zSurfaceIta80, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[80] = {BubbleTriangle_001yy___z_m_zSurfaceIta81, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[81] = {BubbleTriangle_001yy___z_m_zSurfaceIta82, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[82] = {BubbleTriangle_001yy___z_m_zSurfaceIta83, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[83] = {BubbleTriangle_001yy___z_m_zSurfaceIta84, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[84] = {BubbleTriangle_001yy___z_m_zSurfaceIta85, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[85] = {BubbleTriangle_001yy___z_m_zSurfaceIta86, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[86] = {BubbleTriangle_001yy___z_m_zSurfaceIta87, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[87] = {BubbleTriangle_001yy___z_m_zSurfaceIta88, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[88] = {BubbleTriangle_001yy___z_m_zSurfaceIta89, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[89] = {BubbleTriangle_001yy___z_m_zSurfaceIta90, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[90] = {BubbleTriangle_001yy___z_m_zSurfaceIta91, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[91] = {BubbleTriangle_001yy___z_m_zSurfaceIta92, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[92] = {BubbleTriangle_001yy___z_m_zSurfaceIta93, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[93] = {BubbleTriangle_001yy___z_m_zSurfaceIta94, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[94] = {BubbleTriangle_001yy___z_m_zSurfaceIta95, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[95] = {BubbleTriangle_001yy___z_m_zSurfaceIta96, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[96] = {BubbleTriangle_001yy___z_m_zSurfaceIta97, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[97] = {BubbleTriangle_001yy___z_m_zSurfaceIta98, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[98] = {BubbleTriangle_001yy___z_m_zSurfaceIta99, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[99] = {BubbleTriangle_001yy___z_m_zSurfaceIta100, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[100] = {BubbleTriangle_001yy___z_m_zSurfaceIta101, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[101] = {BubbleTriangle_001yy___z_m_zSurfaceIta102, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[102] = {BubbleTriangle_001yy___z_m_zSurfaceIta103, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[103] = {BubbleTriangle_001yy___z_m_zSurfaceIta104, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[104] = {BubbleTriangle_001yy___z_m_zSurfaceIta105, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[105] = {BubbleTriangle_001yy___z_m_zSurfaceIta106, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[106] = {BubbleTriangle_001yy___z_m_zSurfaceIta107, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[107] = {BubbleTriangle_001yy___z_m_zSurfaceIta108, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[108] = {BubbleTriangle_001yy___z_m_zSurfaceIta109, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[109] = {BubbleTriangle_001yy___z_m_zSurfaceIta110, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[110] = {BubbleTriangle_001yy___z_m_zSurfaceIta111, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[111] = {BubbleTriangle_001yy___z_m_zSurfaceIta112, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[112] = {BubbleTriangle_001yy___z_m_zSurfaceIta113, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[113] = {BubbleTriangle_001yy___z_m_zSurfaceIta114, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[114] = {BubbleTriangle_001yy___z_m_zSurfaceIta115, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[115] = {BubbleTriangle_001yy___z_m_zSurfaceIta116, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[116] = {BubbleTriangle_001yy___z_m_zSurfaceIta117, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[117] = {BubbleTriangle_001yy___z_m_zSurfaceIta118, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[118] = {BubbleTriangle_001yy___z_m_zSurfaceIta119, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[119] = {BubbleTriangle_001yy___z_m_zSurfaceIta120, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[120] = {BubbleTriangle_001yy___z_m_zSurfaceIta121, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[121] = {BubbleTriangle_001yy___z_m_zSurfaceIta122, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[122] = {BubbleTriangle_001yy___z_m_zSurfaceIta123, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[123] = {BubbleTriangle_001yy___z_m_zSurfaceIta124, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[124] = {BubbleTriangle_001yy___z_m_zSurfaceIta125, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[125] = {BubbleTriangle_001yy___z_m_zSurfaceIta126, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[126] = {BubbleTriangle_001yy___z_m_zSurfaceIta127, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[127] = {BubbleTriangle_001yy___z_m_zSurfaceIta128, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[128] = {BubbleTriangle_001yy___z_m_zSurfaceIta129, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[129] = {BubbleTriangle_001yy___z_m_zSurfaceIta130, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[130] = {BubbleTriangle_001yy___z_m_zSurfaceIta131, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[131] = {BubbleTriangle_001yy___z_m_zSurfaceIta132, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[132] = {BubbleTriangle_001yy___z_m_zSurfaceIta133, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[133] = {BubbleTriangle_001yy___z_m_zSurfaceIta134, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[134] = {BubbleTriangle_001yy___z_m_zSurfaceIta135, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[135] = {BubbleTriangle_001yy___z_m_zSurfaceIta136, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[136] = {BubbleTriangle_001yy___z_m_zSurfaceIta137, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[137] = {BubbleTriangle_001yy___z_m_zSurfaceIta138, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[138] = {BubbleTriangle_001yy___z_m_zSurfaceIta139, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[139] = {BubbleTriangle_001yy___z_m_zSurfaceIta140, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[140] = {BubbleTriangle_001yy___z_m_zSurfaceIta141, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[141] = {BubbleTriangle_001yy___z_m_zSurfaceIta142, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[142] = {BubbleTriangle_001yy___z_m_zSurfaceIta143, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[143] = {BubbleTriangle_001yy___z_m_zSurfaceIta144, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[144] = {BubbleTriangle_001yy___z_m_zSurfaceIta145, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[145] = {BubbleTriangle_001yy___z_m_zSurfaceIta146, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[146] = {BubbleTriangle_001yy___z_m_zSurfaceIta147, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[147] = {BubbleTriangle_001yy___z_m_zSurfaceIta148, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[148] = {BubbleTriangle_001yy___z_m_zSurfaceIta149, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[149] = {BubbleTriangle_001yy___z_m_zSurfaceIta150, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[150] = {BubbleTriangle_001yy___z_m_zSurfaceIta151, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[151] = {BubbleTriangle_001yy___z_m_zSurfaceIta152, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[152] = {BubbleTriangle_001yy___z_m_zSurfaceIta153, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[153] = {BubbleTriangle_001yy___z_m_zSurfaceIta154, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[154] = {BubbleTriangle_001yy___z_m_zSurfaceIta155, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[155] = {BubbleTriangle_001yy___z_m_zSurfaceIta156, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[156] = {BubbleTriangle_001yy___z_m_zSurfaceIta157, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[157] = {BubbleTriangle_001yy___z_m_zSurfaceIta158, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[158] = {BubbleTriangle_001yy___z_m_zSurfaceIta159, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[159] = {BubbleTriangle_001yy___z_m_zSurfaceIta160, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[160] = {BubbleTriangle_001yy___z_m_zSurfaceIta161, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[161] = {BubbleTriangle_001yy___z_m_zSurfaceIta162, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[162] = {BubbleTriangle_001yy___z_m_zSurfaceIta163, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[163] = {BubbleTriangle_001yy___z_m_zSurfaceIta164, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[164] = {BubbleTriangle_001yy___z_m_zSurfaceIta165, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[165] = {BubbleTriangle_001yy___z_m_zSurfaceIta166, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[166] = {BubbleTriangle_001yy___z_m_zSurfaceIta167, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[167] = {BubbleTriangle_001yy___z_m_zSurfaceIta168, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[168] = {BubbleTriangle_001yy___z_m_zSurfaceIta169, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[169] = {BubbleTriangle_001yy___z_m_zSurfaceIta170, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[170] = {BubbleTriangle_001yy___z_m_zSurfaceIta171, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[171] = {BubbleTriangle_001yy___z_m_zSurfaceIta172, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[172] = {BubbleTriangle_001yy___z_m_zSurfaceIta173, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[173] = {BubbleTriangle_001yy___z_m_zSurfaceIta174, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[174] = {BubbleTriangle_001yy___z_m_zSurfaceIta175, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[175] = {BubbleTriangle_001yy___z_m_zSurfaceIta176, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[176] = {BubbleTriangle_001yy___z_m_zSurfaceIta177, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[177] = {BubbleTriangle_001yy___z_m_zSurfaceIta178, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[178] = {BubbleTriangle_001yy___z_m_zSurfaceIta179, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[179] = {BubbleTriangle_001yy___z_m_zSurfaceIta180, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[180] = {BubbleTriangle_001yy___z_m_zSurfaceIta181, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[181] = {BubbleTriangle_001yy___z_m_zSurfaceIta182, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[182] = {BubbleTriangle_001yy___z_m_zSurfaceIta183, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[183] = {BubbleTriangle_001yy___z_m_zSurfaceIta184, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[184] = {BubbleTriangle_001yy___z_m_zSurfaceIta185, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[185] = {BubbleTriangle_001yy___z_m_zSurfaceIta186, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[186] = {BubbleTriangle_001yy___z_m_zSurfaceIta187, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[187] = {BubbleTriangle_001yy___z_m_zSurfaceIta188, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[188] = {BubbleTriangle_001yy___z_m_zSurfaceIta189, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[189] = {BubbleTriangle_001yy___z_m_zSurfaceIta190, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[190] = {BubbleTriangle_001yy___z_m_zSurfaceIta191, -1};
BubbleTriangle_001yy___z_m_zSurfaceItaArray[191] = {BubbleTriangle_001yy___z_m_zSurfaceIta192, -1};
return BubbleTriangle_001yy___z_m_zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,192> BubbleTriangle_001yy___z_m_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,192> BubbleTriangle_001yy___z_m_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,192> BubbleTriangle_001yy___z_m_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,192> BubbleTriangle_001yy___z_m_zSurfaceIta<F32>();

#endif

}}}}

