/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][7];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[0][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
zd[0] = z[10] + z[11];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta77(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta78(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta79(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta80(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta81(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta82(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta83(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta84(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta85(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[1] * z[3];
z[10] = z[10] * z[11];
z[12] = z[3] * z[8];
z[13] = z[1] * z[6];
z[10] = z[10] + z[12] + z[13];
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[0] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta86(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta87(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta88(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta89(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta90(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta91(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta92(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta93(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta94(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta95(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[2] * z[10];
z[10] = z[7] + z[10];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[2];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta96(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta97(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta98(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[1] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta99(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[8] + z[10];
z[11] = z[0] * z[2];
z[10] = z[10] * z[11];
z[12] = z[2] * z[9];
z[13] = z[0] * z[7];
z[12] = z[12] + z[13];
z[12] = z[1] * z[12];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta100(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta101(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta102(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta103(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta104(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta105(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta106(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta107(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta108(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta109(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta110(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta111(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta112(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta113(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta114(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta115(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta116(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta117(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta118(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta119(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta120(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta121(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta122(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta123(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta124(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta125(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta126(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta127(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta128(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta129(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta130(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta131(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta132(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[8];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta133(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta134(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta135(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta136(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta137(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta138(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[2] * z[8] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[13];
z[10] = z[2] * z[10];
z[11] = z[3] * z[7] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta139(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta140(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[1] * z[7] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[13];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta141(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[2];
z[10] = z[10] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[2] * z[13];
z[13] = z[7] + z[13];
z[13] = z[1] * z[13];
z[14] = z[2] * z[8];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[3];
zd[1] = -((z[1] * z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta142(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta143(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta144(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta145(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta146(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta147(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta148(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[9];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[0] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[2] * z[8] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta149(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta150(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[2], 2);
z[12] = z[3] * z[8] * z[10] * z[11];
z[10] = z[7] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[13] = z[2] * z[13];
z[10] = z[10] + z[13];
z[10] = z[2] * z[3] * z[10];
z[11] = z[0] * z[6] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta151(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[8] * T(2);
z[10] = z[0] * z[1] * z[10];
z[11] = prod_pow(z[1], 2);
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[11];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[1] * z[3];
zd[1] = -((z[0] * z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta152(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta153(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[11] = z[0] * z[3];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta154(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 2);
z[12] = z[1] * z[7] * z[10] * z[11];
z[10] = z[6] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[13] = z[3] * z[13];
z[10] = z[10] + z[13];
z[10] = z[1] * z[3] * z[10];
z[11] = z[0] * z[8] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[2] * z[3];
zd[1] = -((z[0] * z[1] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta155(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[9];
z[11] = z[5] + z[4] * T(3);
z[11] = z[2] * z[11];
z[11] = z[7] + z[11];
z[11] = z[0] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[11] = z[0] * z[2];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta156(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta157(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta158(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta159(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta160(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta161(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta162(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta163(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta164(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta165(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta166(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta167(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta168(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta169(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta170(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta171(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta172(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta173(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta174(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta175(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta176(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta177(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta178(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta179(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta180(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta181(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta182(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta183(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta184(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta185(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta186(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta187(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta188(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[2] * z[11];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[1] * z[2];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[3] * z[10];
z[11] = prod_pow(z[3], 2) * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta189(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta190(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta191(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta192(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta193(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta194(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[1];
z[11] = prod_pow(z[11], 2);
z[10] = z[10] * z[11];
z[12] = prod_pow(z[0], 2) * z[1] * z[8];
z[13] = z[0] * prod_pow(z[1], 2) * z[9];
z[12] = z[12] + z[13];
z[10] = z[10] + z[12] * T(2);
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[1] * z[3];
zd[1] = -((z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta195(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta196(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[3];
z[11] = prod_pow(z[11], 2);
z[10] = z[10] * z[11];
z[12] = prod_pow(z[0], 2) * z[3] * z[6];
z[13] = z[0] * prod_pow(z[3], 2) * z[9];
z[12] = z[12] + z[13];
z[10] = z[10] + z[12] * T(2);
z[10] = z[1] * z[10];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[2] * z[3];
zd[1] = -((z[1] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta197(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2);
z[11] = z[8] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[0] * z[12];
z[12] = z[12] + z[9] * T(2);
z[12] = z[0] * z[1] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[1] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[6] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta198(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta199(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta200(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta201(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta202(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta203(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta204(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[11] = z[7] * z[11];
z[13] = z[5] * z[12];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta205(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta206(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 2);
z[12] = z[10] * z[11];
z[13] = z[7] * z[12];
z[14] = z[5] + z[4] * T(3);
z[14] = z[0] * z[14];
z[14] = z[9] + z[14];
z[11] = z[11] * z[14];
z[10] = z[3] * z[6] * z[10];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[10] = z[10] + z[13];
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[2], 2) * z[8] * z[12];
z[10] = z[10] + z[11];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[1] * z[2] * z[3];
zd[1] = -((z[0] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta207(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[2];
z[11] = prod_pow(z[1], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[2] * z[9];
z[14] = z[5] + z[4] * T(3);
z[14] = z[2] * z[14];
z[14] = z[7] + z[14];
z[14] = z[0] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[1] * z[8] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta208(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta209(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[3] * z[10];
z[11] = z[0] * prod_pow(z[3], 2);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta210(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[1];
z[11] = prod_pow(z[2], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[1] * z[13];
z[13] = z[8] + z[13];
z[13] = z[0] * z[13];
z[14] = z[1] * z[9];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[2] * z[7] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta211(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[8] + z[10];
z[11] = z[0] * z[2];
z[10] = z[10] * z[11];
z[12] = z[2] * z[9];
z[13] = z[0] * z[7];
z[12] = z[12] + z[13];
z[12] = z[1] * z[12];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta212(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta213(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta214(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta215(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta216(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta217(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta218(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta219(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta220(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta221(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta222(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta223(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta224(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta225(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta226(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta227(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta228(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta229(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta230(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta231(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta232(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta233(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta234(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta235(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta236(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta237(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta238(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta239(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta240(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta241(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta242(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta243(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta244(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * T(3);
z[11] = prod_pow(z[3], 3);
z[12] = z[2] * z[9] * z[10] * z[11];
z[10] = z[6] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[1] * z[13];
z[13] = z[8] + z[13];
z[13] = z[3] * z[13];
z[10] = z[10] + z[13];
z[10] = z[2] * prod_pow(z[3], 2) * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[10] = z[10] + z[12];
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[3];
zd[1] = -((z[1] * z[2] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta245(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta246(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta247(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta248(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta249(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta250(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[8] * T(2);
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[1], 2);
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[11];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[3] * z[10];
z[11] = prod_pow(z[0], 2) * z[6] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta251(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta252(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2) * z[2] * z[7];
z[11] = z[0] * prod_pow(z[2], 2) * z[9];
z[10] = z[10] + z[11];
z[11] = z[0] * z[2];
z[11] = prod_pow(z[11], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[1] * z[10];
z[12] = z[1] * T(3);
z[12] = z[11] * z[12];
z[13] = z[4] * z[12];
z[11] = z[8] * z[11];
z[10] = z[10] + z[11] + z[13];
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta253(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2);
z[11] = z[8] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[0] * z[12];
z[12] = z[12] + z[9] * T(2);
z[12] = z[0] * z[1] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[1] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[6] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta254(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta255(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta256(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta257(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta258(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta259(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta260(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[13] = z[5] * z[12];
z[11] = z[7] * z[11];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[1] * z[3];
zd[1] = -((z[0] * z[2] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta261(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta262(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[9] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[1] * z[12];
z[12] = z[12] + z[8] * T(2);
z[12] = z[0] * z[1] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12] * T(2);
z[11] = z[2] * z[3] * z[11];
z[10] = prod_pow(z[2], 2) * z[6] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta263(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[5] + z[4] * T(3);
z[11] = z[10] * z[11];
z[12] = z[1] * z[8];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[11];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[6] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta264(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta265(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 3);
z[11] = z[1] * z[10];
z[12] = z[4] * z[11];
z[13] = z[1] * prod_pow(z[3], 2) * z[6];
z[12] = z[12] + z[13];
z[10] = z[8] * z[10];
z[13] = z[5] * z[11];
z[10] = z[10] + z[13] + z[12] * T(3);
z[10] = z[0] * z[10];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[2] * z[3];
zd[1] = -((z[0] * z[1] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta266(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[1];
z[11] = prod_pow(z[2], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[0] * z[8];
z[14] = z[5] + z[4] * T(3);
z[14] = z[0] * z[14];
z[14] = z[9] + z[14];
z[14] = z[1] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[2] * z[7] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta267(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[8];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[1];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta268(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta269(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta270(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta271(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta272(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta273(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta274(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta275(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta276(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta277(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta278(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta279(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta280(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta281(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta282(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta283(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta284(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta285(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta286(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta287(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta288(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta289(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta290(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta291(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta292(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta293(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta294(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta295(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta296(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta297(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta298(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta299(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta300(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2) * z[9];
z[11] = prod_pow(z[0], 3);
z[12] = z[4] * z[11];
z[10] = z[10] + z[12];
z[12] = z[1] * T(3);
z[10] = z[10] * z[12];
z[12] = z[1] * z[5];
z[12] = z[8] + z[12];
z[12] = z[11] * z[12];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[11];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta301(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta302(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta303(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta304(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta305(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta306(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2) * z[8];
z[11] = z[0] * z[1];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = prod_pow(z[11], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[2] * z[10];
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[11] = z[7] * z[11];
z[10] = z[10] + z[11] + z[13] * T(3);
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta307(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta308(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * T(2);
z[11] = z[7] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[1] * z[12];
z[12] = z[8] + z[12];
z[12] = z[2] * z[12];
z[11] = z[11] + z[12];
z[12] = prod_pow(z[0], 2);
z[11] = z[2] * z[11] * z[12];
z[13] = prod_pow(z[2], 2);
z[10] = z[0] * z[9] * z[10] * z[13];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[12] * z[13];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta309(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[7];
z[11] = z[1] * z[2];
z[12] = z[4] * z[11];
z[10] = z[10] + z[12] * T(3);
z[12] = prod_pow(z[0], 2);
z[10] = z[10] * z[12];
z[13] = z[1] * z[9];
z[14] = z[1] * z[5];
z[14] = z[8] + z[14];
z[14] = z[0] * z[14];
z[13] = z[14] + z[13] * T(2);
z[13] = z[0] * z[2] * z[13];
z[10] = z[10] + z[13];
z[10] = z[3] * z[10];
z[11] = z[6] * z[11] * z[12];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta310(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta311(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta312(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta313(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta314(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta315(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta316(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[11] = z[7] * z[11];
z[13] = z[5] * z[12];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta317(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 2) * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta318(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 4);
z[10] = z[10] * z[11];
z[12] = z[7] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[11] = z[11] * z[13];
z[13] = z[0] * prod_pow(z[3], 3) * z[6];
z[11] = z[11] + z[13] * T(4);
z[11] = z[2] * z[11];
z[11] = z[11] + z[12];
z[11] = z[1] * z[2] * z[11];
z[10] = prod_pow(z[2], 2) * z[8] * z[10];
z[10] = z[10] + z[11];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta319(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[5] + z[4] * T(3);
z[11] = z[10] * z[11];
z[12] = z[1] * z[8];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[11];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[6] * z[10];
z[10] = z[11] + z[10] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta320(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta321(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[2], 3);
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[10] * z[11];
z[12] = z[1] * prod_pow(z[2], 2) * z[7];
z[11] = z[11] + z[12] * T(3);
z[11] = z[0] * z[11];
z[10] = z[1] * z[10];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[0] * z[6] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta322(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[1];
z[11] = prod_pow(z[2], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[0] * z[8];
z[14] = z[5] + z[4] * T(3);
z[14] = z[0] * z[14];
z[14] = z[9] + z[14];
z[14] = z[1] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[2] * z[7] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta323(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[8];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[1];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(6);
zd[0] = prod_pow(z[3], 5) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 6) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta324(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta325(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta326(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 2) * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta327(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta328(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta329(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta330(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta331(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta332(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta333(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta334(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta335(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta336(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta337(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta338(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta339(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta340(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta341(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 2) * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta342(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta343(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta344(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta345(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta346(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta347(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta348(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta349(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta350(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(9);
zd[0] = prod_pow(z[0], 8) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 9) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta351(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][7];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[1][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta352(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta353(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta354(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta355(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta356(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta357(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta358(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta359(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta360(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta361(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta362(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta363(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta364(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta365(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta366(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta367(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta368(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta369(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta370(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta371(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta372(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta373(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta374(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta375(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta376(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta377(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta378(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta379(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta380(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta381(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta382(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta383(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta384(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta385(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta386(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta387(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta388(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta389(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta390(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta391(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta392(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta393(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta394(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta395(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta396(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta397(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta398(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta399(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta400(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta401(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta402(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta403(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta404(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta405(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta406(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta407(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta408(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta409(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta410(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta411(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[3] * z[8];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta412(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta413(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta414(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[2] * z[8] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[13];
z[10] = z[2] * z[10];
z[11] = z[3] * z[7] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta415(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[1] * z[7] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[13];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta416(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta417(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta418(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[3] * z[9];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[0] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[2] * z[8] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta419(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[2], 2);
z[12] = z[3] * z[8] * z[10] * z[11];
z[10] = z[7] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[13] = z[2] * z[13];
z[10] = z[10] + z[13];
z[10] = z[2] * z[3] * z[10];
z[11] = z[0] * z[6] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta420(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[11] = z[0] * z[3];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta421(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta422(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta423(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta424(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta425(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta426(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta427(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta428(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta429(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta430(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta431(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta432(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[1] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[2] * z[11];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[1] * z[2];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[3] * z[10];
z[11] = prod_pow(z[3], 2) * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta433(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta434(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta435(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[1];
z[11] = prod_pow(z[11], 2);
z[10] = z[10] * z[11];
z[12] = prod_pow(z[0], 2) * z[1] * z[8];
z[13] = z[0] * prod_pow(z[1], 2) * z[9];
z[12] = z[12] + z[13];
z[10] = z[10] + z[12] * T(2);
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[1] * z[3];
zd[1] = -((z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta436(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[3];
z[11] = prod_pow(z[11], 2);
z[10] = z[10] * z[11];
z[12] = prod_pow(z[0], 2) * z[3] * z[6];
z[13] = z[0] * prod_pow(z[3], 2) * z[9];
z[12] = z[12] + z[13];
z[10] = z[10] + z[12] * T(2);
z[10] = z[1] * z[10];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[2] * z[3];
zd[1] = -((z[1] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta437(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta438(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta439(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[11] = z[7] * z[11];
z[13] = z[5] * z[12];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta440(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 2);
z[12] = z[10] * z[11];
z[13] = z[7] * z[12];
z[14] = z[5] + z[4] * T(3);
z[14] = z[0] * z[14];
z[14] = z[9] + z[14];
z[11] = z[11] * z[14];
z[10] = z[3] * z[6] * z[10];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[10] = z[10] + z[13];
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[2], 2) * z[8] * z[12];
z[10] = z[10] + z[11];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[1] * z[2] * z[3];
zd[1] = -((z[0] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta441(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[3] * z[10];
z[11] = z[0] * prod_pow(z[3], 2);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta442(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta443(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta444(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta445(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta446(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta447(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta448(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta449(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta450(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta451(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta452(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta453(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[1] * T(3);
z[11] = prod_pow(z[3], 3);
z[12] = z[2] * z[9] * z[10] * z[11];
z[10] = z[6] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[1] * z[13];
z[13] = z[8] + z[13];
z[13] = z[3] * z[13];
z[10] = z[10] + z[13];
z[10] = z[2] * prod_pow(z[3], 2) * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[10] = z[10] + z[12];
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[3];
zd[1] = -((z[1] * z[2] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta454(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta455(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta456(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[8] * T(2);
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[1], 2);
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[11];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[3] * z[10];
z[11] = prod_pow(z[0], 2) * z[6] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta457(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[0], 2) * z[2] * z[7];
z[11] = z[0] * prod_pow(z[2], 2) * z[9];
z[10] = z[10] + z[11];
z[11] = z[0] * z[2];
z[11] = prod_pow(z[11], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[1] * z[10];
z[12] = z[1] * T(3);
z[12] = z[11] * z[12];
z[13] = z[4] * z[12];
z[11] = z[8] * z[11];
z[10] = z[10] + z[11] + z[13];
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta458(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta459(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta460(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[13] = z[5] * z[12];
z[11] = z[7] * z[11];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[1] * z[3];
zd[1] = -((z[0] * z[2] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta461(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[9] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[1] * z[12];
z[12] = z[12] + z[8] * T(2);
z[12] = z[0] * z[1] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12] * T(2);
z[11] = z[2] * z[3] * z[11];
z[10] = prod_pow(z[2], 2) * z[6] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta462(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[3], 3);
z[11] = z[1] * z[10];
z[12] = z[4] * z[11];
z[13] = z[1] * prod_pow(z[3], 2) * z[6];
z[12] = z[12] + z[13];
z[10] = z[8] * z[10];
z[13] = z[5] * z[11];
z[10] = z[10] + z[13] + z[12] * T(3);
z[10] = z[0] * z[10];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[2] * z[3];
zd[1] = -((z[0] * z[1] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta463(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta464(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta465(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta466(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta467(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta468(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta469(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta470(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta471(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta472(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta473(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta474(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[0], 2) * z[9];
z[11] = prod_pow(z[0], 3);
z[12] = z[4] * z[11];
z[10] = z[10] + z[12];
z[12] = z[1] * T(3);
z[10] = z[10] * z[12];
z[12] = z[1] * z[5];
z[12] = z[8] + z[12];
z[12] = z[11] * z[12];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[11];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta475(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta476(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta477(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[0], 2) * z[8];
z[11] = z[0] * z[1];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = prod_pow(z[11], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[2] * z[10];
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[11] = z[7] * z[11];
z[10] = z[10] + z[11] + z[13] * T(3);
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta478(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[1] * T(2);
z[11] = z[7] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[1] * z[12];
z[12] = z[8] + z[12];
z[12] = z[2] * z[12];
z[11] = z[11] + z[12];
z[12] = prod_pow(z[0], 2);
z[11] = z[2] * z[11] * z[12];
z[13] = prod_pow(z[2], 2);
z[10] = z[0] * z[9] * z[10] * z[13];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[12] * z[13];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta479(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta480(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta481(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[11] = z[7] * z[11];
z[13] = z[5] * z[12];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta482(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 4);
z[10] = z[10] * z[11];
z[12] = z[7] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[11] = z[11] * z[13];
z[13] = z[0] * prod_pow(z[3], 3) * z[6];
z[11] = z[11] + z[13] * T(4);
z[11] = z[2] * z[11];
z[11] = z[11] + z[12];
z[11] = z[1] * z[2] * z[11];
z[10] = prod_pow(z[2], 2) * z[8] * z[10];
z[10] = z[10] + z[11];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta483(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[2], 3);
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[10] * z[11];
z[12] = z[1] * prod_pow(z[2], 2) * z[7];
z[11] = z[11] + z[12] * T(3);
z[11] = z[0] * z[11];
z[10] = z[1] * z[10];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[0] * z[6] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta484(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta485(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta486(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta487(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta488(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta489(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta490(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta491(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][7];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[2][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta492(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta493(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta494(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta495(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta496(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta497(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta498(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta499(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta500(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta501(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta502(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta503(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta504(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta505(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta506(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta507(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta508(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta509(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta510(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta511(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta512(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta513(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta514(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta515(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta516(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta517(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta518(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta519(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta520(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta521(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta522(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta523(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta524(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta525(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta526(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta527(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta528(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta529(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta530(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta531(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta532(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta533(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta534(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][7];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta535(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta536(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][7];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[4][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta537(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta538(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta539(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta540(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta541(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta542(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][7];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta543(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][7];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][7];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta544(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][7];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][7];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___zz_z_zSurfaceIta545(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][7];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][7];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,545> BubbleBox_002yy___zz_z_zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,545> BubbleBox_002yy___zz_z_zSurfaceItaArray;
BubbleBox_002yy___zz_z_zSurfaceItaArray[0] = {BubbleBox_002yy___zz_z_zSurfaceIta1, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[1] = {BubbleBox_002yy___zz_z_zSurfaceIta2, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[2] = {BubbleBox_002yy___zz_z_zSurfaceIta3, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[3] = {BubbleBox_002yy___zz_z_zSurfaceIta4, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[4] = {BubbleBox_002yy___zz_z_zSurfaceIta5, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[5] = {BubbleBox_002yy___zz_z_zSurfaceIta6, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[6] = {BubbleBox_002yy___zz_z_zSurfaceIta7, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[7] = {BubbleBox_002yy___zz_z_zSurfaceIta8, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[8] = {BubbleBox_002yy___zz_z_zSurfaceIta9, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[9] = {BubbleBox_002yy___zz_z_zSurfaceIta10, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[10] = {BubbleBox_002yy___zz_z_zSurfaceIta11, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[11] = {BubbleBox_002yy___zz_z_zSurfaceIta12, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[12] = {BubbleBox_002yy___zz_z_zSurfaceIta13, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[13] = {BubbleBox_002yy___zz_z_zSurfaceIta14, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[14] = {BubbleBox_002yy___zz_z_zSurfaceIta15, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[15] = {BubbleBox_002yy___zz_z_zSurfaceIta16, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[16] = {BubbleBox_002yy___zz_z_zSurfaceIta17, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[17] = {BubbleBox_002yy___zz_z_zSurfaceIta18, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[18] = {BubbleBox_002yy___zz_z_zSurfaceIta19, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[19] = {BubbleBox_002yy___zz_z_zSurfaceIta20, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[20] = {BubbleBox_002yy___zz_z_zSurfaceIta21, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[21] = {BubbleBox_002yy___zz_z_zSurfaceIta22, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[22] = {BubbleBox_002yy___zz_z_zSurfaceIta23, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[23] = {BubbleBox_002yy___zz_z_zSurfaceIta24, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[24] = {BubbleBox_002yy___zz_z_zSurfaceIta25, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[25] = {BubbleBox_002yy___zz_z_zSurfaceIta26, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[26] = {BubbleBox_002yy___zz_z_zSurfaceIta27, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[27] = {BubbleBox_002yy___zz_z_zSurfaceIta28, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[28] = {BubbleBox_002yy___zz_z_zSurfaceIta29, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[29] = {BubbleBox_002yy___zz_z_zSurfaceIta30, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[30] = {BubbleBox_002yy___zz_z_zSurfaceIta31, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[31] = {BubbleBox_002yy___zz_z_zSurfaceIta32, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[32] = {BubbleBox_002yy___zz_z_zSurfaceIta33, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[33] = {BubbleBox_002yy___zz_z_zSurfaceIta34, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[34] = {BubbleBox_002yy___zz_z_zSurfaceIta35, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[35] = {BubbleBox_002yy___zz_z_zSurfaceIta36, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[36] = {BubbleBox_002yy___zz_z_zSurfaceIta37, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[37] = {BubbleBox_002yy___zz_z_zSurfaceIta38, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[38] = {BubbleBox_002yy___zz_z_zSurfaceIta39, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[39] = {BubbleBox_002yy___zz_z_zSurfaceIta40, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[40] = {BubbleBox_002yy___zz_z_zSurfaceIta41, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[41] = {BubbleBox_002yy___zz_z_zSurfaceIta42, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[42] = {BubbleBox_002yy___zz_z_zSurfaceIta43, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[43] = {BubbleBox_002yy___zz_z_zSurfaceIta44, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[44] = {BubbleBox_002yy___zz_z_zSurfaceIta45, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[45] = {BubbleBox_002yy___zz_z_zSurfaceIta46, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[46] = {BubbleBox_002yy___zz_z_zSurfaceIta47, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[47] = {BubbleBox_002yy___zz_z_zSurfaceIta48, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[48] = {BubbleBox_002yy___zz_z_zSurfaceIta49, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[49] = {BubbleBox_002yy___zz_z_zSurfaceIta50, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[50] = {BubbleBox_002yy___zz_z_zSurfaceIta51, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[51] = {BubbleBox_002yy___zz_z_zSurfaceIta52, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[52] = {BubbleBox_002yy___zz_z_zSurfaceIta53, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[53] = {BubbleBox_002yy___zz_z_zSurfaceIta54, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[54] = {BubbleBox_002yy___zz_z_zSurfaceIta55, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[55] = {BubbleBox_002yy___zz_z_zSurfaceIta56, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[56] = {BubbleBox_002yy___zz_z_zSurfaceIta57, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[57] = {BubbleBox_002yy___zz_z_zSurfaceIta58, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[58] = {BubbleBox_002yy___zz_z_zSurfaceIta59, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[59] = {BubbleBox_002yy___zz_z_zSurfaceIta60, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[60] = {BubbleBox_002yy___zz_z_zSurfaceIta61, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[61] = {BubbleBox_002yy___zz_z_zSurfaceIta62, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[62] = {BubbleBox_002yy___zz_z_zSurfaceIta63, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[63] = {BubbleBox_002yy___zz_z_zSurfaceIta64, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[64] = {BubbleBox_002yy___zz_z_zSurfaceIta65, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[65] = {BubbleBox_002yy___zz_z_zSurfaceIta66, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[66] = {BubbleBox_002yy___zz_z_zSurfaceIta67, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[67] = {BubbleBox_002yy___zz_z_zSurfaceIta68, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[68] = {BubbleBox_002yy___zz_z_zSurfaceIta69, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[69] = {BubbleBox_002yy___zz_z_zSurfaceIta70, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[70] = {BubbleBox_002yy___zz_z_zSurfaceIta71, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[71] = {BubbleBox_002yy___zz_z_zSurfaceIta72, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[72] = {BubbleBox_002yy___zz_z_zSurfaceIta73, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[73] = {BubbleBox_002yy___zz_z_zSurfaceIta74, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[74] = {BubbleBox_002yy___zz_z_zSurfaceIta75, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[75] = {BubbleBox_002yy___zz_z_zSurfaceIta76, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[76] = {BubbleBox_002yy___zz_z_zSurfaceIta77, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[77] = {BubbleBox_002yy___zz_z_zSurfaceIta78, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[78] = {BubbleBox_002yy___zz_z_zSurfaceIta79, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[79] = {BubbleBox_002yy___zz_z_zSurfaceIta80, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[80] = {BubbleBox_002yy___zz_z_zSurfaceIta81, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[81] = {BubbleBox_002yy___zz_z_zSurfaceIta82, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[82] = {BubbleBox_002yy___zz_z_zSurfaceIta83, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[83] = {BubbleBox_002yy___zz_z_zSurfaceIta84, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[84] = {BubbleBox_002yy___zz_z_zSurfaceIta85, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[85] = {BubbleBox_002yy___zz_z_zSurfaceIta86, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[86] = {BubbleBox_002yy___zz_z_zSurfaceIta87, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[87] = {BubbleBox_002yy___zz_z_zSurfaceIta88, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[88] = {BubbleBox_002yy___zz_z_zSurfaceIta89, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[89] = {BubbleBox_002yy___zz_z_zSurfaceIta90, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[90] = {BubbleBox_002yy___zz_z_zSurfaceIta91, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[91] = {BubbleBox_002yy___zz_z_zSurfaceIta92, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[92] = {BubbleBox_002yy___zz_z_zSurfaceIta93, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[93] = {BubbleBox_002yy___zz_z_zSurfaceIta94, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[94] = {BubbleBox_002yy___zz_z_zSurfaceIta95, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[95] = {BubbleBox_002yy___zz_z_zSurfaceIta96, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[96] = {BubbleBox_002yy___zz_z_zSurfaceIta97, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[97] = {BubbleBox_002yy___zz_z_zSurfaceIta98, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[98] = {BubbleBox_002yy___zz_z_zSurfaceIta99, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[99] = {BubbleBox_002yy___zz_z_zSurfaceIta100, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[100] = {BubbleBox_002yy___zz_z_zSurfaceIta101, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[101] = {BubbleBox_002yy___zz_z_zSurfaceIta102, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[102] = {BubbleBox_002yy___zz_z_zSurfaceIta103, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[103] = {BubbleBox_002yy___zz_z_zSurfaceIta104, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[104] = {BubbleBox_002yy___zz_z_zSurfaceIta105, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[105] = {BubbleBox_002yy___zz_z_zSurfaceIta106, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[106] = {BubbleBox_002yy___zz_z_zSurfaceIta107, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[107] = {BubbleBox_002yy___zz_z_zSurfaceIta108, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[108] = {BubbleBox_002yy___zz_z_zSurfaceIta109, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[109] = {BubbleBox_002yy___zz_z_zSurfaceIta110, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[110] = {BubbleBox_002yy___zz_z_zSurfaceIta111, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[111] = {BubbleBox_002yy___zz_z_zSurfaceIta112, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[112] = {BubbleBox_002yy___zz_z_zSurfaceIta113, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[113] = {BubbleBox_002yy___zz_z_zSurfaceIta114, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[114] = {BubbleBox_002yy___zz_z_zSurfaceIta115, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[115] = {BubbleBox_002yy___zz_z_zSurfaceIta116, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[116] = {BubbleBox_002yy___zz_z_zSurfaceIta117, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[117] = {BubbleBox_002yy___zz_z_zSurfaceIta118, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[118] = {BubbleBox_002yy___zz_z_zSurfaceIta119, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[119] = {BubbleBox_002yy___zz_z_zSurfaceIta120, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[120] = {BubbleBox_002yy___zz_z_zSurfaceIta121, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[121] = {BubbleBox_002yy___zz_z_zSurfaceIta122, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[122] = {BubbleBox_002yy___zz_z_zSurfaceIta123, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[123] = {BubbleBox_002yy___zz_z_zSurfaceIta124, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[124] = {BubbleBox_002yy___zz_z_zSurfaceIta125, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[125] = {BubbleBox_002yy___zz_z_zSurfaceIta126, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[126] = {BubbleBox_002yy___zz_z_zSurfaceIta127, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[127] = {BubbleBox_002yy___zz_z_zSurfaceIta128, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[128] = {BubbleBox_002yy___zz_z_zSurfaceIta129, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[129] = {BubbleBox_002yy___zz_z_zSurfaceIta130, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[130] = {BubbleBox_002yy___zz_z_zSurfaceIta131, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[131] = {BubbleBox_002yy___zz_z_zSurfaceIta132, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[132] = {BubbleBox_002yy___zz_z_zSurfaceIta133, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[133] = {BubbleBox_002yy___zz_z_zSurfaceIta134, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[134] = {BubbleBox_002yy___zz_z_zSurfaceIta135, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[135] = {BubbleBox_002yy___zz_z_zSurfaceIta136, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[136] = {BubbleBox_002yy___zz_z_zSurfaceIta137, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[137] = {BubbleBox_002yy___zz_z_zSurfaceIta138, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[138] = {BubbleBox_002yy___zz_z_zSurfaceIta139, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[139] = {BubbleBox_002yy___zz_z_zSurfaceIta140, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[140] = {BubbleBox_002yy___zz_z_zSurfaceIta141, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[141] = {BubbleBox_002yy___zz_z_zSurfaceIta142, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[142] = {BubbleBox_002yy___zz_z_zSurfaceIta143, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[143] = {BubbleBox_002yy___zz_z_zSurfaceIta144, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[144] = {BubbleBox_002yy___zz_z_zSurfaceIta145, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[145] = {BubbleBox_002yy___zz_z_zSurfaceIta146, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[146] = {BubbleBox_002yy___zz_z_zSurfaceIta147, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[147] = {BubbleBox_002yy___zz_z_zSurfaceIta148, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[148] = {BubbleBox_002yy___zz_z_zSurfaceIta149, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[149] = {BubbleBox_002yy___zz_z_zSurfaceIta150, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[150] = {BubbleBox_002yy___zz_z_zSurfaceIta151, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[151] = {BubbleBox_002yy___zz_z_zSurfaceIta152, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[152] = {BubbleBox_002yy___zz_z_zSurfaceIta153, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[153] = {BubbleBox_002yy___zz_z_zSurfaceIta154, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[154] = {BubbleBox_002yy___zz_z_zSurfaceIta155, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[155] = {BubbleBox_002yy___zz_z_zSurfaceIta156, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[156] = {BubbleBox_002yy___zz_z_zSurfaceIta157, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[157] = {BubbleBox_002yy___zz_z_zSurfaceIta158, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[158] = {BubbleBox_002yy___zz_z_zSurfaceIta159, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[159] = {BubbleBox_002yy___zz_z_zSurfaceIta160, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[160] = {BubbleBox_002yy___zz_z_zSurfaceIta161, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[161] = {BubbleBox_002yy___zz_z_zSurfaceIta162, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[162] = {BubbleBox_002yy___zz_z_zSurfaceIta163, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[163] = {BubbleBox_002yy___zz_z_zSurfaceIta164, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[164] = {BubbleBox_002yy___zz_z_zSurfaceIta165, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[165] = {BubbleBox_002yy___zz_z_zSurfaceIta166, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[166] = {BubbleBox_002yy___zz_z_zSurfaceIta167, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[167] = {BubbleBox_002yy___zz_z_zSurfaceIta168, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[168] = {BubbleBox_002yy___zz_z_zSurfaceIta169, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[169] = {BubbleBox_002yy___zz_z_zSurfaceIta170, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[170] = {BubbleBox_002yy___zz_z_zSurfaceIta171, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[171] = {BubbleBox_002yy___zz_z_zSurfaceIta172, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[172] = {BubbleBox_002yy___zz_z_zSurfaceIta173, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[173] = {BubbleBox_002yy___zz_z_zSurfaceIta174, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[174] = {BubbleBox_002yy___zz_z_zSurfaceIta175, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[175] = {BubbleBox_002yy___zz_z_zSurfaceIta176, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[176] = {BubbleBox_002yy___zz_z_zSurfaceIta177, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[177] = {BubbleBox_002yy___zz_z_zSurfaceIta178, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[178] = {BubbleBox_002yy___zz_z_zSurfaceIta179, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[179] = {BubbleBox_002yy___zz_z_zSurfaceIta180, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[180] = {BubbleBox_002yy___zz_z_zSurfaceIta181, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[181] = {BubbleBox_002yy___zz_z_zSurfaceIta182, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[182] = {BubbleBox_002yy___zz_z_zSurfaceIta183, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[183] = {BubbleBox_002yy___zz_z_zSurfaceIta184, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[184] = {BubbleBox_002yy___zz_z_zSurfaceIta185, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[185] = {BubbleBox_002yy___zz_z_zSurfaceIta186, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[186] = {BubbleBox_002yy___zz_z_zSurfaceIta187, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[187] = {BubbleBox_002yy___zz_z_zSurfaceIta188, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[188] = {BubbleBox_002yy___zz_z_zSurfaceIta189, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[189] = {BubbleBox_002yy___zz_z_zSurfaceIta190, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[190] = {BubbleBox_002yy___zz_z_zSurfaceIta191, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[191] = {BubbleBox_002yy___zz_z_zSurfaceIta192, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[192] = {BubbleBox_002yy___zz_z_zSurfaceIta193, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[193] = {BubbleBox_002yy___zz_z_zSurfaceIta194, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[194] = {BubbleBox_002yy___zz_z_zSurfaceIta195, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[195] = {BubbleBox_002yy___zz_z_zSurfaceIta196, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[196] = {BubbleBox_002yy___zz_z_zSurfaceIta197, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[197] = {BubbleBox_002yy___zz_z_zSurfaceIta198, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[198] = {BubbleBox_002yy___zz_z_zSurfaceIta199, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[199] = {BubbleBox_002yy___zz_z_zSurfaceIta200, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[200] = {BubbleBox_002yy___zz_z_zSurfaceIta201, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[201] = {BubbleBox_002yy___zz_z_zSurfaceIta202, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[202] = {BubbleBox_002yy___zz_z_zSurfaceIta203, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[203] = {BubbleBox_002yy___zz_z_zSurfaceIta204, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[204] = {BubbleBox_002yy___zz_z_zSurfaceIta205, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[205] = {BubbleBox_002yy___zz_z_zSurfaceIta206, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[206] = {BubbleBox_002yy___zz_z_zSurfaceIta207, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[207] = {BubbleBox_002yy___zz_z_zSurfaceIta208, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[208] = {BubbleBox_002yy___zz_z_zSurfaceIta209, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[209] = {BubbleBox_002yy___zz_z_zSurfaceIta210, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[210] = {BubbleBox_002yy___zz_z_zSurfaceIta211, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[211] = {BubbleBox_002yy___zz_z_zSurfaceIta212, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[212] = {BubbleBox_002yy___zz_z_zSurfaceIta213, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[213] = {BubbleBox_002yy___zz_z_zSurfaceIta214, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[214] = {BubbleBox_002yy___zz_z_zSurfaceIta215, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[215] = {BubbleBox_002yy___zz_z_zSurfaceIta216, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[216] = {BubbleBox_002yy___zz_z_zSurfaceIta217, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[217] = {BubbleBox_002yy___zz_z_zSurfaceIta218, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[218] = {BubbleBox_002yy___zz_z_zSurfaceIta219, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[219] = {BubbleBox_002yy___zz_z_zSurfaceIta220, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[220] = {BubbleBox_002yy___zz_z_zSurfaceIta221, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[221] = {BubbleBox_002yy___zz_z_zSurfaceIta222, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[222] = {BubbleBox_002yy___zz_z_zSurfaceIta223, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[223] = {BubbleBox_002yy___zz_z_zSurfaceIta224, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[224] = {BubbleBox_002yy___zz_z_zSurfaceIta225, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[225] = {BubbleBox_002yy___zz_z_zSurfaceIta226, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[226] = {BubbleBox_002yy___zz_z_zSurfaceIta227, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[227] = {BubbleBox_002yy___zz_z_zSurfaceIta228, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[228] = {BubbleBox_002yy___zz_z_zSurfaceIta229, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[229] = {BubbleBox_002yy___zz_z_zSurfaceIta230, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[230] = {BubbleBox_002yy___zz_z_zSurfaceIta231, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[231] = {BubbleBox_002yy___zz_z_zSurfaceIta232, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[232] = {BubbleBox_002yy___zz_z_zSurfaceIta233, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[233] = {BubbleBox_002yy___zz_z_zSurfaceIta234, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[234] = {BubbleBox_002yy___zz_z_zSurfaceIta235, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[235] = {BubbleBox_002yy___zz_z_zSurfaceIta236, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[236] = {BubbleBox_002yy___zz_z_zSurfaceIta237, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[237] = {BubbleBox_002yy___zz_z_zSurfaceIta238, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[238] = {BubbleBox_002yy___zz_z_zSurfaceIta239, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[239] = {BubbleBox_002yy___zz_z_zSurfaceIta240, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[240] = {BubbleBox_002yy___zz_z_zSurfaceIta241, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[241] = {BubbleBox_002yy___zz_z_zSurfaceIta242, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[242] = {BubbleBox_002yy___zz_z_zSurfaceIta243, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[243] = {BubbleBox_002yy___zz_z_zSurfaceIta244, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[244] = {BubbleBox_002yy___zz_z_zSurfaceIta245, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[245] = {BubbleBox_002yy___zz_z_zSurfaceIta246, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[246] = {BubbleBox_002yy___zz_z_zSurfaceIta247, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[247] = {BubbleBox_002yy___zz_z_zSurfaceIta248, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[248] = {BubbleBox_002yy___zz_z_zSurfaceIta249, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[249] = {BubbleBox_002yy___zz_z_zSurfaceIta250, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[250] = {BubbleBox_002yy___zz_z_zSurfaceIta251, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[251] = {BubbleBox_002yy___zz_z_zSurfaceIta252, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[252] = {BubbleBox_002yy___zz_z_zSurfaceIta253, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[253] = {BubbleBox_002yy___zz_z_zSurfaceIta254, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[254] = {BubbleBox_002yy___zz_z_zSurfaceIta255, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[255] = {BubbleBox_002yy___zz_z_zSurfaceIta256, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[256] = {BubbleBox_002yy___zz_z_zSurfaceIta257, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[257] = {BubbleBox_002yy___zz_z_zSurfaceIta258, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[258] = {BubbleBox_002yy___zz_z_zSurfaceIta259, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[259] = {BubbleBox_002yy___zz_z_zSurfaceIta260, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[260] = {BubbleBox_002yy___zz_z_zSurfaceIta261, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[261] = {BubbleBox_002yy___zz_z_zSurfaceIta262, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[262] = {BubbleBox_002yy___zz_z_zSurfaceIta263, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[263] = {BubbleBox_002yy___zz_z_zSurfaceIta264, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[264] = {BubbleBox_002yy___zz_z_zSurfaceIta265, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[265] = {BubbleBox_002yy___zz_z_zSurfaceIta266, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[266] = {BubbleBox_002yy___zz_z_zSurfaceIta267, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[267] = {BubbleBox_002yy___zz_z_zSurfaceIta268, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[268] = {BubbleBox_002yy___zz_z_zSurfaceIta269, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[269] = {BubbleBox_002yy___zz_z_zSurfaceIta270, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[270] = {BubbleBox_002yy___zz_z_zSurfaceIta271, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[271] = {BubbleBox_002yy___zz_z_zSurfaceIta272, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[272] = {BubbleBox_002yy___zz_z_zSurfaceIta273, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[273] = {BubbleBox_002yy___zz_z_zSurfaceIta274, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[274] = {BubbleBox_002yy___zz_z_zSurfaceIta275, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[275] = {BubbleBox_002yy___zz_z_zSurfaceIta276, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[276] = {BubbleBox_002yy___zz_z_zSurfaceIta277, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[277] = {BubbleBox_002yy___zz_z_zSurfaceIta278, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[278] = {BubbleBox_002yy___zz_z_zSurfaceIta279, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[279] = {BubbleBox_002yy___zz_z_zSurfaceIta280, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[280] = {BubbleBox_002yy___zz_z_zSurfaceIta281, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[281] = {BubbleBox_002yy___zz_z_zSurfaceIta282, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[282] = {BubbleBox_002yy___zz_z_zSurfaceIta283, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[283] = {BubbleBox_002yy___zz_z_zSurfaceIta284, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[284] = {BubbleBox_002yy___zz_z_zSurfaceIta285, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[285] = {BubbleBox_002yy___zz_z_zSurfaceIta286, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[286] = {BubbleBox_002yy___zz_z_zSurfaceIta287, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[287] = {BubbleBox_002yy___zz_z_zSurfaceIta288, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[288] = {BubbleBox_002yy___zz_z_zSurfaceIta289, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[289] = {BubbleBox_002yy___zz_z_zSurfaceIta290, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[290] = {BubbleBox_002yy___zz_z_zSurfaceIta291, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[291] = {BubbleBox_002yy___zz_z_zSurfaceIta292, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[292] = {BubbleBox_002yy___zz_z_zSurfaceIta293, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[293] = {BubbleBox_002yy___zz_z_zSurfaceIta294, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[294] = {BubbleBox_002yy___zz_z_zSurfaceIta295, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[295] = {BubbleBox_002yy___zz_z_zSurfaceIta296, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[296] = {BubbleBox_002yy___zz_z_zSurfaceIta297, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[297] = {BubbleBox_002yy___zz_z_zSurfaceIta298, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[298] = {BubbleBox_002yy___zz_z_zSurfaceIta299, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[299] = {BubbleBox_002yy___zz_z_zSurfaceIta300, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[300] = {BubbleBox_002yy___zz_z_zSurfaceIta301, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[301] = {BubbleBox_002yy___zz_z_zSurfaceIta302, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[302] = {BubbleBox_002yy___zz_z_zSurfaceIta303, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[303] = {BubbleBox_002yy___zz_z_zSurfaceIta304, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[304] = {BubbleBox_002yy___zz_z_zSurfaceIta305, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[305] = {BubbleBox_002yy___zz_z_zSurfaceIta306, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[306] = {BubbleBox_002yy___zz_z_zSurfaceIta307, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[307] = {BubbleBox_002yy___zz_z_zSurfaceIta308, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[308] = {BubbleBox_002yy___zz_z_zSurfaceIta309, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[309] = {BubbleBox_002yy___zz_z_zSurfaceIta310, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[310] = {BubbleBox_002yy___zz_z_zSurfaceIta311, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[311] = {BubbleBox_002yy___zz_z_zSurfaceIta312, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[312] = {BubbleBox_002yy___zz_z_zSurfaceIta313, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[313] = {BubbleBox_002yy___zz_z_zSurfaceIta314, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[314] = {BubbleBox_002yy___zz_z_zSurfaceIta315, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[315] = {BubbleBox_002yy___zz_z_zSurfaceIta316, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[316] = {BubbleBox_002yy___zz_z_zSurfaceIta317, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[317] = {BubbleBox_002yy___zz_z_zSurfaceIta318, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[318] = {BubbleBox_002yy___zz_z_zSurfaceIta319, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[319] = {BubbleBox_002yy___zz_z_zSurfaceIta320, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[320] = {BubbleBox_002yy___zz_z_zSurfaceIta321, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[321] = {BubbleBox_002yy___zz_z_zSurfaceIta322, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[322] = {BubbleBox_002yy___zz_z_zSurfaceIta323, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[323] = {BubbleBox_002yy___zz_z_zSurfaceIta324, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[324] = {BubbleBox_002yy___zz_z_zSurfaceIta325, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[325] = {BubbleBox_002yy___zz_z_zSurfaceIta326, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[326] = {BubbleBox_002yy___zz_z_zSurfaceIta327, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[327] = {BubbleBox_002yy___zz_z_zSurfaceIta328, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[328] = {BubbleBox_002yy___zz_z_zSurfaceIta329, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[329] = {BubbleBox_002yy___zz_z_zSurfaceIta330, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[330] = {BubbleBox_002yy___zz_z_zSurfaceIta331, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[331] = {BubbleBox_002yy___zz_z_zSurfaceIta332, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[332] = {BubbleBox_002yy___zz_z_zSurfaceIta333, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[333] = {BubbleBox_002yy___zz_z_zSurfaceIta334, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[334] = {BubbleBox_002yy___zz_z_zSurfaceIta335, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[335] = {BubbleBox_002yy___zz_z_zSurfaceIta336, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[336] = {BubbleBox_002yy___zz_z_zSurfaceIta337, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[337] = {BubbleBox_002yy___zz_z_zSurfaceIta338, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[338] = {BubbleBox_002yy___zz_z_zSurfaceIta339, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[339] = {BubbleBox_002yy___zz_z_zSurfaceIta340, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[340] = {BubbleBox_002yy___zz_z_zSurfaceIta341, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[341] = {BubbleBox_002yy___zz_z_zSurfaceIta342, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[342] = {BubbleBox_002yy___zz_z_zSurfaceIta343, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[343] = {BubbleBox_002yy___zz_z_zSurfaceIta344, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[344] = {BubbleBox_002yy___zz_z_zSurfaceIta345, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[345] = {BubbleBox_002yy___zz_z_zSurfaceIta346, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[346] = {BubbleBox_002yy___zz_z_zSurfaceIta347, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[347] = {BubbleBox_002yy___zz_z_zSurfaceIta348, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[348] = {BubbleBox_002yy___zz_z_zSurfaceIta349, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[349] = {BubbleBox_002yy___zz_z_zSurfaceIta350, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[350] = {BubbleBox_002yy___zz_z_zSurfaceIta351, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[351] = {BubbleBox_002yy___zz_z_zSurfaceIta352, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[352] = {BubbleBox_002yy___zz_z_zSurfaceIta353, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[353] = {BubbleBox_002yy___zz_z_zSurfaceIta354, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[354] = {BubbleBox_002yy___zz_z_zSurfaceIta355, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[355] = {BubbleBox_002yy___zz_z_zSurfaceIta356, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[356] = {BubbleBox_002yy___zz_z_zSurfaceIta357, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[357] = {BubbleBox_002yy___zz_z_zSurfaceIta358, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[358] = {BubbleBox_002yy___zz_z_zSurfaceIta359, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[359] = {BubbleBox_002yy___zz_z_zSurfaceIta360, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[360] = {BubbleBox_002yy___zz_z_zSurfaceIta361, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[361] = {BubbleBox_002yy___zz_z_zSurfaceIta362, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[362] = {BubbleBox_002yy___zz_z_zSurfaceIta363, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[363] = {BubbleBox_002yy___zz_z_zSurfaceIta364, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[364] = {BubbleBox_002yy___zz_z_zSurfaceIta365, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[365] = {BubbleBox_002yy___zz_z_zSurfaceIta366, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[366] = {BubbleBox_002yy___zz_z_zSurfaceIta367, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[367] = {BubbleBox_002yy___zz_z_zSurfaceIta368, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[368] = {BubbleBox_002yy___zz_z_zSurfaceIta369, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[369] = {BubbleBox_002yy___zz_z_zSurfaceIta370, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[370] = {BubbleBox_002yy___zz_z_zSurfaceIta371, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[371] = {BubbleBox_002yy___zz_z_zSurfaceIta372, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[372] = {BubbleBox_002yy___zz_z_zSurfaceIta373, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[373] = {BubbleBox_002yy___zz_z_zSurfaceIta374, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[374] = {BubbleBox_002yy___zz_z_zSurfaceIta375, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[375] = {BubbleBox_002yy___zz_z_zSurfaceIta376, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[376] = {BubbleBox_002yy___zz_z_zSurfaceIta377, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[377] = {BubbleBox_002yy___zz_z_zSurfaceIta378, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[378] = {BubbleBox_002yy___zz_z_zSurfaceIta379, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[379] = {BubbleBox_002yy___zz_z_zSurfaceIta380, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[380] = {BubbleBox_002yy___zz_z_zSurfaceIta381, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[381] = {BubbleBox_002yy___zz_z_zSurfaceIta382, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[382] = {BubbleBox_002yy___zz_z_zSurfaceIta383, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[383] = {BubbleBox_002yy___zz_z_zSurfaceIta384, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[384] = {BubbleBox_002yy___zz_z_zSurfaceIta385, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[385] = {BubbleBox_002yy___zz_z_zSurfaceIta386, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[386] = {BubbleBox_002yy___zz_z_zSurfaceIta387, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[387] = {BubbleBox_002yy___zz_z_zSurfaceIta388, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[388] = {BubbleBox_002yy___zz_z_zSurfaceIta389, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[389] = {BubbleBox_002yy___zz_z_zSurfaceIta390, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[390] = {BubbleBox_002yy___zz_z_zSurfaceIta391, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[391] = {BubbleBox_002yy___zz_z_zSurfaceIta392, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[392] = {BubbleBox_002yy___zz_z_zSurfaceIta393, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[393] = {BubbleBox_002yy___zz_z_zSurfaceIta394, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[394] = {BubbleBox_002yy___zz_z_zSurfaceIta395, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[395] = {BubbleBox_002yy___zz_z_zSurfaceIta396, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[396] = {BubbleBox_002yy___zz_z_zSurfaceIta397, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[397] = {BubbleBox_002yy___zz_z_zSurfaceIta398, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[398] = {BubbleBox_002yy___zz_z_zSurfaceIta399, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[399] = {BubbleBox_002yy___zz_z_zSurfaceIta400, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[400] = {BubbleBox_002yy___zz_z_zSurfaceIta401, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[401] = {BubbleBox_002yy___zz_z_zSurfaceIta402, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[402] = {BubbleBox_002yy___zz_z_zSurfaceIta403, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[403] = {BubbleBox_002yy___zz_z_zSurfaceIta404, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[404] = {BubbleBox_002yy___zz_z_zSurfaceIta405, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[405] = {BubbleBox_002yy___zz_z_zSurfaceIta406, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[406] = {BubbleBox_002yy___zz_z_zSurfaceIta407, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[407] = {BubbleBox_002yy___zz_z_zSurfaceIta408, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[408] = {BubbleBox_002yy___zz_z_zSurfaceIta409, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[409] = {BubbleBox_002yy___zz_z_zSurfaceIta410, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[410] = {BubbleBox_002yy___zz_z_zSurfaceIta411, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[411] = {BubbleBox_002yy___zz_z_zSurfaceIta412, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[412] = {BubbleBox_002yy___zz_z_zSurfaceIta413, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[413] = {BubbleBox_002yy___zz_z_zSurfaceIta414, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[414] = {BubbleBox_002yy___zz_z_zSurfaceIta415, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[415] = {BubbleBox_002yy___zz_z_zSurfaceIta416, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[416] = {BubbleBox_002yy___zz_z_zSurfaceIta417, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[417] = {BubbleBox_002yy___zz_z_zSurfaceIta418, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[418] = {BubbleBox_002yy___zz_z_zSurfaceIta419, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[419] = {BubbleBox_002yy___zz_z_zSurfaceIta420, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[420] = {BubbleBox_002yy___zz_z_zSurfaceIta421, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[421] = {BubbleBox_002yy___zz_z_zSurfaceIta422, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[422] = {BubbleBox_002yy___zz_z_zSurfaceIta423, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[423] = {BubbleBox_002yy___zz_z_zSurfaceIta424, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[424] = {BubbleBox_002yy___zz_z_zSurfaceIta425, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[425] = {BubbleBox_002yy___zz_z_zSurfaceIta426, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[426] = {BubbleBox_002yy___zz_z_zSurfaceIta427, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[427] = {BubbleBox_002yy___zz_z_zSurfaceIta428, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[428] = {BubbleBox_002yy___zz_z_zSurfaceIta429, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[429] = {BubbleBox_002yy___zz_z_zSurfaceIta430, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[430] = {BubbleBox_002yy___zz_z_zSurfaceIta431, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[431] = {BubbleBox_002yy___zz_z_zSurfaceIta432, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[432] = {BubbleBox_002yy___zz_z_zSurfaceIta433, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[433] = {BubbleBox_002yy___zz_z_zSurfaceIta434, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[434] = {BubbleBox_002yy___zz_z_zSurfaceIta435, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[435] = {BubbleBox_002yy___zz_z_zSurfaceIta436, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[436] = {BubbleBox_002yy___zz_z_zSurfaceIta437, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[437] = {BubbleBox_002yy___zz_z_zSurfaceIta438, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[438] = {BubbleBox_002yy___zz_z_zSurfaceIta439, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[439] = {BubbleBox_002yy___zz_z_zSurfaceIta440, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[440] = {BubbleBox_002yy___zz_z_zSurfaceIta441, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[441] = {BubbleBox_002yy___zz_z_zSurfaceIta442, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[442] = {BubbleBox_002yy___zz_z_zSurfaceIta443, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[443] = {BubbleBox_002yy___zz_z_zSurfaceIta444, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[444] = {BubbleBox_002yy___zz_z_zSurfaceIta445, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[445] = {BubbleBox_002yy___zz_z_zSurfaceIta446, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[446] = {BubbleBox_002yy___zz_z_zSurfaceIta447, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[447] = {BubbleBox_002yy___zz_z_zSurfaceIta448, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[448] = {BubbleBox_002yy___zz_z_zSurfaceIta449, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[449] = {BubbleBox_002yy___zz_z_zSurfaceIta450, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[450] = {BubbleBox_002yy___zz_z_zSurfaceIta451, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[451] = {BubbleBox_002yy___zz_z_zSurfaceIta452, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[452] = {BubbleBox_002yy___zz_z_zSurfaceIta453, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[453] = {BubbleBox_002yy___zz_z_zSurfaceIta454, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[454] = {BubbleBox_002yy___zz_z_zSurfaceIta455, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[455] = {BubbleBox_002yy___zz_z_zSurfaceIta456, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[456] = {BubbleBox_002yy___zz_z_zSurfaceIta457, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[457] = {BubbleBox_002yy___zz_z_zSurfaceIta458, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[458] = {BubbleBox_002yy___zz_z_zSurfaceIta459, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[459] = {BubbleBox_002yy___zz_z_zSurfaceIta460, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[460] = {BubbleBox_002yy___zz_z_zSurfaceIta461, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[461] = {BubbleBox_002yy___zz_z_zSurfaceIta462, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[462] = {BubbleBox_002yy___zz_z_zSurfaceIta463, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[463] = {BubbleBox_002yy___zz_z_zSurfaceIta464, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[464] = {BubbleBox_002yy___zz_z_zSurfaceIta465, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[465] = {BubbleBox_002yy___zz_z_zSurfaceIta466, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[466] = {BubbleBox_002yy___zz_z_zSurfaceIta467, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[467] = {BubbleBox_002yy___zz_z_zSurfaceIta468, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[468] = {BubbleBox_002yy___zz_z_zSurfaceIta469, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[469] = {BubbleBox_002yy___zz_z_zSurfaceIta470, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[470] = {BubbleBox_002yy___zz_z_zSurfaceIta471, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[471] = {BubbleBox_002yy___zz_z_zSurfaceIta472, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[472] = {BubbleBox_002yy___zz_z_zSurfaceIta473, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[473] = {BubbleBox_002yy___zz_z_zSurfaceIta474, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[474] = {BubbleBox_002yy___zz_z_zSurfaceIta475, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[475] = {BubbleBox_002yy___zz_z_zSurfaceIta476, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[476] = {BubbleBox_002yy___zz_z_zSurfaceIta477, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[477] = {BubbleBox_002yy___zz_z_zSurfaceIta478, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[478] = {BubbleBox_002yy___zz_z_zSurfaceIta479, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[479] = {BubbleBox_002yy___zz_z_zSurfaceIta480, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[480] = {BubbleBox_002yy___zz_z_zSurfaceIta481, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[481] = {BubbleBox_002yy___zz_z_zSurfaceIta482, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[482] = {BubbleBox_002yy___zz_z_zSurfaceIta483, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[483] = {BubbleBox_002yy___zz_z_zSurfaceIta484, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[484] = {BubbleBox_002yy___zz_z_zSurfaceIta485, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[485] = {BubbleBox_002yy___zz_z_zSurfaceIta486, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[486] = {BubbleBox_002yy___zz_z_zSurfaceIta487, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[487] = {BubbleBox_002yy___zz_z_zSurfaceIta488, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[488] = {BubbleBox_002yy___zz_z_zSurfaceIta489, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[489] = {BubbleBox_002yy___zz_z_zSurfaceIta490, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[490] = {BubbleBox_002yy___zz_z_zSurfaceIta491, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[491] = {BubbleBox_002yy___zz_z_zSurfaceIta492, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[492] = {BubbleBox_002yy___zz_z_zSurfaceIta493, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[493] = {BubbleBox_002yy___zz_z_zSurfaceIta494, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[494] = {BubbleBox_002yy___zz_z_zSurfaceIta495, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[495] = {BubbleBox_002yy___zz_z_zSurfaceIta496, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[496] = {BubbleBox_002yy___zz_z_zSurfaceIta497, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[497] = {BubbleBox_002yy___zz_z_zSurfaceIta498, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[498] = {BubbleBox_002yy___zz_z_zSurfaceIta499, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[499] = {BubbleBox_002yy___zz_z_zSurfaceIta500, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[500] = {BubbleBox_002yy___zz_z_zSurfaceIta501, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[501] = {BubbleBox_002yy___zz_z_zSurfaceIta502, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[502] = {BubbleBox_002yy___zz_z_zSurfaceIta503, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[503] = {BubbleBox_002yy___zz_z_zSurfaceIta504, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[504] = {BubbleBox_002yy___zz_z_zSurfaceIta505, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[505] = {BubbleBox_002yy___zz_z_zSurfaceIta506, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[506] = {BubbleBox_002yy___zz_z_zSurfaceIta507, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[507] = {BubbleBox_002yy___zz_z_zSurfaceIta508, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[508] = {BubbleBox_002yy___zz_z_zSurfaceIta509, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[509] = {BubbleBox_002yy___zz_z_zSurfaceIta510, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[510] = {BubbleBox_002yy___zz_z_zSurfaceIta511, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[511] = {BubbleBox_002yy___zz_z_zSurfaceIta512, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[512] = {BubbleBox_002yy___zz_z_zSurfaceIta513, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[513] = {BubbleBox_002yy___zz_z_zSurfaceIta514, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[514] = {BubbleBox_002yy___zz_z_zSurfaceIta515, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[515] = {BubbleBox_002yy___zz_z_zSurfaceIta516, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[516] = {BubbleBox_002yy___zz_z_zSurfaceIta517, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[517] = {BubbleBox_002yy___zz_z_zSurfaceIta518, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[518] = {BubbleBox_002yy___zz_z_zSurfaceIta519, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[519] = {BubbleBox_002yy___zz_z_zSurfaceIta520, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[520] = {BubbleBox_002yy___zz_z_zSurfaceIta521, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[521] = {BubbleBox_002yy___zz_z_zSurfaceIta522, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[522] = {BubbleBox_002yy___zz_z_zSurfaceIta523, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[523] = {BubbleBox_002yy___zz_z_zSurfaceIta524, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[524] = {BubbleBox_002yy___zz_z_zSurfaceIta525, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[525] = {BubbleBox_002yy___zz_z_zSurfaceIta526, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[526] = {BubbleBox_002yy___zz_z_zSurfaceIta527, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[527] = {BubbleBox_002yy___zz_z_zSurfaceIta528, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[528] = {BubbleBox_002yy___zz_z_zSurfaceIta529, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[529] = {BubbleBox_002yy___zz_z_zSurfaceIta530, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[530] = {BubbleBox_002yy___zz_z_zSurfaceIta531, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[531] = {BubbleBox_002yy___zz_z_zSurfaceIta532, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[532] = {BubbleBox_002yy___zz_z_zSurfaceIta533, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[533] = {BubbleBox_002yy___zz_z_zSurfaceIta534, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[534] = {BubbleBox_002yy___zz_z_zSurfaceIta535, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[535] = {BubbleBox_002yy___zz_z_zSurfaceIta536, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[536] = {BubbleBox_002yy___zz_z_zSurfaceIta537, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[537] = {BubbleBox_002yy___zz_z_zSurfaceIta538, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[538] = {BubbleBox_002yy___zz_z_zSurfaceIta539, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[539] = {BubbleBox_002yy___zz_z_zSurfaceIta540, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[540] = {BubbleBox_002yy___zz_z_zSurfaceIta541, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[541] = {BubbleBox_002yy___zz_z_zSurfaceIta542, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[542] = {BubbleBox_002yy___zz_z_zSurfaceIta543, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[543] = {BubbleBox_002yy___zz_z_zSurfaceIta544, -1};
BubbleBox_002yy___zz_z_zSurfaceItaArray[544] = {BubbleBox_002yy___zz_z_zSurfaceIta545, -1};
return BubbleBox_002yy___zz_z_zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,545> BubbleBox_002yy___zz_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,545> BubbleBox_002yy___zz_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,545> BubbleBox_002yy___zz_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,545> BubbleBox_002yy___zz_z_zSurfaceIta<F32>();

#endif

}}}}

