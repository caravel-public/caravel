#include "FunctionSpace/IBPhelper.h"

#include "BoxBox_111nn_z_z_m__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,9> BoxBox_111nn_z_z_m__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,9> BoxBox_111nn_z_z_m__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,9> BoxBox_111nn_z_z_m__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,9> BoxBox_111nn_z_z_m__SurfaceIta<F32>();

#endif

}}}}

