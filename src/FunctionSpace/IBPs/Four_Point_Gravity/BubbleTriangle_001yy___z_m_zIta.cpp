#include "FunctionSpace/IBPhelper.h"

#include "BubbleTriangle_001yy___z_m_zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,192> BubbleTriangle_001yy___z_m_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,192> BubbleTriangle_001yy___z_m_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,192> BubbleTriangle_001yy___z_m_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,192> BubbleTriangle_001yy___z_m_zSurfaceIta<F32>();

#endif

}}}}

