#pragma once

#include "BoxBox_111nn_z_z_m__Vectors.hpp"

#include "BoxBox_111nn_z_z_m__Info.hpp"

#include "BoxBox_111ny_z_z_z__zVectors.hpp"

#include "BoxBox_111ny_z_z_z__zInfo.hpp"

#include "BoxBox_202nn_zz__zz__Vectors.hpp"

#include "BoxBox_202nn_zz__zz__Info.hpp"

#include "BoxPentagon_112nn_z_z_zz__Vectors.hpp"

#include "BoxPentagon_112nn_z_z_zz__Info.hpp"

#include "BubbleBox_002nn___mm__Vectors.hpp"

#include "BubbleBox_002nn___mm__Info.hpp"

#include "BubbleBox_002ny___mz__zVectors.hpp"

#include "BubbleBox_002ny___mz__zInfo.hpp"

#include "BubbleBox_002ny___zm__zVectors.hpp"

#include "BubbleBox_002ny___zm__zInfo.hpp"

#include "BubbleBox_002ny___zz__mVectors.hpp"

#include "BubbleBox_002ny___zz__mInfo.hpp"

#include "BubbleBox_002yy___zz_z_zVectors.hpp"

#include "BubbleBox_002yy___zz_z_zInfo.hpp"

#include "BubbleBubble_000yy____m_mVectors.hpp"

#include "BubbleBubble_000yy____m_mInfo.hpp"

#include "BubbleHexagon_004nn___zzzz__Vectors.hpp"

#include "BubbleHexagon_004nn___zzzz__Info.hpp"

#include "BubblePentagon_003nn___mzz__Vectors.hpp"

#include "BubblePentagon_003nn___mzz__Info.hpp"

#include "BubblePentagon_003nn___zmz__Vectors.hpp"

#include "BubblePentagon_003nn___zmz__Info.hpp"

#include "BubblePentagon_003ny___zzz__zVectors.hpp"

#include "BubblePentagon_003ny___zzz__zInfo.hpp"

#include "BubbleTriangle_001ny___m__mVectors.hpp"

#include "BubbleTriangle_001ny___m__mInfo.hpp"

#include "BubbleTriangle_001yy___m_z_zVectors.hpp"

#include "BubbleTriangle_001yy___m_z_zInfo.hpp"

#include "BubbleTriangle_001yy___z_m_zVectors.hpp"

#include "BubbleTriangle_001yy___z_m_zInfo.hpp"

#include "TriangleBox_102nn_m__zz__Vectors.hpp"

#include "TriangleBox_102nn_m__zz__Info.hpp"

#include "TriangleBox_102nn_z__mz__Vectors.hpp"

#include "TriangleBox_102nn_z__mz__Info.hpp"

#include "TriangleBox_102ny_z__zz__zVectors.hpp"

#include "TriangleBox_102ny_z__zz__zInfo.hpp"

#include "TrianglePentagon_103nn_z__zzz__Vectors.hpp"

#include "TrianglePentagon_103nn_z__zzz__Info.hpp"

#include "TriangleTriangle_101nn_m__m__Vectors.hpp"

#include "TriangleTriangle_101nn_m__m__Info.hpp"

#include "TriangleTriangle_101ny_z__m__zVectors.hpp"

#include "TriangleTriangle_101ny_z__m__zInfo.hpp"

#include "TriangleTriangle_101ny_z__z__mVectors.hpp"

#include "TriangleTriangle_101ny_z__z__mInfo.hpp"

#include "TriangleTriangle_101yy_z__z_z_zVectors.hpp"

#include "TriangleTriangle_101yy_z__z_z_zInfo.hpp"

