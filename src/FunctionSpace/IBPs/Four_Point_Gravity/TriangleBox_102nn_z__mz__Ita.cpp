#include "FunctionSpace/IBPhelper.h"

#include "TriangleBox_102nn_z__mz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,8> TriangleBox_102nn_z__mz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,8> TriangleBox_102nn_z__mz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,8> TriangleBox_102nn_z__mz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,8> TriangleBox_102nn_z__mz__SurfaceIta<F32>();

#endif

}}}}

