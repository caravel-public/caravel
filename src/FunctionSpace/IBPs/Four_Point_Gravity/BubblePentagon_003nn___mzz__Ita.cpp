#include "FunctionSpace/IBPhelper.h"

#include "BubblePentagon_003nn___mzz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,21> BubblePentagon_003nn___mzz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,21> BubblePentagon_003nn___mzz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,21> BubblePentagon_003nn___mzz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,21> BubblePentagon_003nn___mzz__SurfaceIta<F32>();

#endif

}}}}

