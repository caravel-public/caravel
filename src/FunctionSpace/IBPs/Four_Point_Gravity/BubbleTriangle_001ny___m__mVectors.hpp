#ifndef BubbleTriangle_001ny___m__mVectors_H_INC

#define BubbleTriangle_001ny___m__mVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> T BubbleTriangle_001ny___m__mvec1_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T BubbleTriangle_001ny___m__mvec1_component4(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[2];
z[2] = -z[0] + -z[1];
return z[2] * T(2);
}

template<typename T> T BubbleTriangle_001ny___m__mvec1_divergence(const T* invariants, const T* D)
{
T z[1];
z[0] = D[2];
return z[0];
}

template<typename T> T BubbleTriangle_001ny___m__mvec2_component1(const T* invariants, const T* D)
{
T z[2];
z[0] = D[0];
z[1] = z[0] * T(-2) + T(-1);
return z[0] * z[1];
}

template<typename T> T BubbleTriangle_001ny___m__mvec2_component4(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[2];
z[2] = z[1] + T(1) + z[0] * T(4);
return z[2] * T(2);
}

template<typename T> T BubbleTriangle_001ny___m__mvec2_divergence(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[2];
return z[0] * T(-4) + T(-1) + -z[1];
}

template<typename T> std::vector<std::vector<T>> BubbleTriangle_001ny___m__m_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(2, std::vector<T>(5, T(0)));
ibp_vectors[0][3] = BubbleTriangle_001ny___m__mvec1_component4(invariants, propagators);
ibp_vectors[0].back() = BubbleTriangle_001ny___m__mvec1_divergence(invariants, propagators);
ibp_vectors[1][0] = BubbleTriangle_001ny___m__mvec2_component1(invariants, propagators);
ibp_vectors[1][3] = BubbleTriangle_001ny___m__mvec2_component4(invariants, propagators);
ibp_vectors[1].back() = BubbleTriangle_001ny___m__mvec2_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BubbleTriangle_001ny___m__m_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BubbleTriangle_001ny___m__m_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BubbleTriangle_001ny___m__m_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BubbleTriangle_001ny___m__m_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

