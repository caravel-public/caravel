#include "FunctionSpace/IBPhelper.h"

#include "BubbleHexagon_004nn___zzzz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,35> BubbleHexagon_004nn___zzzz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,35> BubbleHexagon_004nn___zzzz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,35> BubbleHexagon_004nn___zzzz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,35> BubbleHexagon_004nn___zzzz__SurfaceIta<F32>();

#endif

}}}}

