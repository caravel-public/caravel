#include "FunctionSpace/IBPhelper.h"

#include "BoxPentagon_112nn_z_z_zz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta<F32>();

#endif

}}}}

