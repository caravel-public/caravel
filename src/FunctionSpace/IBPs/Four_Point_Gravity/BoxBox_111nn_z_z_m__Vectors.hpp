#ifndef BoxBox_111nn_z_z_m__Vectors_H_INC

#define BoxBox_111nn_z_z_m__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> T BoxBox_111nn_z_z_m__vec3_component1(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = z[2] + T(-2) + z[0] * T(-2) + -z[1];
z[3] = z[0] * z[3];
return z[2] + z[3];
}

template<typename T> T BoxBox_111nn_z_z_m__vec3_component8(const T* invariants, const T* D)
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[5];
z[4] = D[6];
z[5] = z[1] + T(1) + z[0] * T(2) + -z[2];
z[5] = z[4] + z[5] * T(2) + -z[3];
return z[5] * T(2);
}

template<typename T> T BoxBox_111nn_z_z_m__vec3_divergence(const T* invariants, const T* D)
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[5];
z[4] = D[6];
z[5] = z[2] + z[0] * T(-2) + T(-1) + -z[1];
return z[3] + z[5] * T(2) + -z[4];
}

template<typename T> std::vector<std::vector<T>> BoxBox_111nn_z_z_m___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(3, std::vector<T>(9, T(0)));
ibp_vectors[2][0] = BoxBox_111nn_z_z_m__vec3_component1(invariants, propagators);
ibp_vectors[2][7] = BoxBox_111nn_z_z_m__vec3_component8(invariants, propagators);
ibp_vectors[2].back() = BoxBox_111nn_z_z_m__vec3_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BoxBox_111nn_z_z_m___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BoxBox_111nn_z_z_m___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BoxBox_111nn_z_z_m___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BoxBox_111nn_z_z_m___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

