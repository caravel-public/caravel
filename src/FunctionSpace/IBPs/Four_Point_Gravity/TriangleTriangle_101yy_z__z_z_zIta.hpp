/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][8];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[0][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
zd[0] = z[10] + z[11];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta77(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta78(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta79(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta80(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta81(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta82(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta83(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta84(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta85(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[1] * z[3];
z[10] = z[10] * z[11];
z[12] = z[3] * z[8];
z[13] = z[1] * z[6];
z[10] = z[10] + z[12] + z[13];
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[0] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta86(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta87(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta88(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta89(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta90(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta91(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta92(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta93(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta94(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta95(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[2] * z[10];
z[10] = z[7] + z[10];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[2];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta96(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta97(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta98(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[1] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta99(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[8] + z[10];
z[11] = z[0] * z[2];
z[10] = z[10] * z[11];
z[12] = z[2] * z[9];
z[13] = z[0] * z[7];
z[12] = z[12] + z[13];
z[12] = z[1] * z[12];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta100(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta101(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta102(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta103(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta104(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta105(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta106(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta107(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta108(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta109(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta110(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta111(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta112(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta113(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta114(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta115(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta116(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta117(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta118(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta119(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta120(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta121(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta122(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta123(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta124(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta125(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta126(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta127(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta128(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta129(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta130(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta131(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta132(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta133(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta134(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta135(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta136(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta137(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta138(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta139(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta140(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta141(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[8];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta142(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta143(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta144(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta145(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta146(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta147(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta148(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta149(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta150(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta151(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[2] * z[8] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[13];
z[10] = z[2] * z[10];
z[11] = z[3] * z[7] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta152(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta153(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta154(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[1] * z[7] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[13];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta155(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[2];
z[10] = z[10] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[2] * z[13];
z[13] = z[7] + z[13];
z[13] = z[1] * z[13];
z[14] = z[2] * z[8];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[3];
zd[1] = -((z[1] * z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta156(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta157(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta158(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta159(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta160(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta161(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta162(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta163(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta164(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta165(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta166(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[9];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[0] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[2] * z[8] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta167(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta168(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta169(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[2], 2);
z[12] = z[3] * z[8] * z[10] * z[11];
z[10] = z[7] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[13] = z[2] * z[13];
z[10] = z[10] + z[13];
z[10] = z[2] * z[3] * z[10];
z[11] = z[0] * z[6] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta170(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[8] * T(2);
z[10] = z[0] * z[1] * z[10];
z[11] = prod_pow(z[1], 2);
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[11];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[3];
zd[1] = -((z[0] * z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta171(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta172(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta173(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[11] = z[0] * z[3];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta174(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 2);
z[12] = z[1] * z[7] * z[10] * z[11];
z[10] = z[6] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[13] = z[3] * z[13];
z[10] = z[10] + z[13];
z[10] = z[1] * z[3] * z[10];
z[11] = z[0] * z[8] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[10] = z[10] + z[12];
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[2] * z[3];
zd[1] = -((z[0] * z[1] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta175(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[9];
z[11] = z[5] + z[4] * T(3);
z[11] = z[2] * z[11];
z[11] = z[7] + z[11];
z[11] = z[0] * z[11];
z[10] = z[10] + z[11];
z[10] = z[1] * z[10];
z[11] = z[0] * z[2];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta176(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta177(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta178(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta179(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta180(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta181(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta182(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta183(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta184(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta185(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta186(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta187(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta188(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta189(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta190(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta191(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta192(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta193(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta194(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta195(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta196(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta197(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta198(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta199(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta200(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta201(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta202(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta203(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta204(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta205(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta206(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta207(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta208(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta209(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta210(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta211(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta212(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta213(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta214(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta215(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta216(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta217(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta218(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta219(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta220(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta221(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta222(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta223(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta224(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta225(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[1] * z[3];
z[10] = z[10] * z[11];
z[12] = z[3] * z[8];
z[13] = z[1] * z[6];
z[10] = z[10] + z[12] + z[13];
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta226(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta227(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta228(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta229(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta230(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta231(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta232(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta233(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta234(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta235(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[6];
z[11] = z[2] * z[3];
z[12] = z[5] * z[11];
z[10] = z[10] + z[12];
z[12] = prod_pow(z[1], 2);
z[10] = z[10] * z[12];
z[13] = z[2] * z[8];
z[14] = z[2] * T(3);
z[14] = z[4] * z[14];
z[14] = z[7] + z[14];
z[14] = z[1] * z[14];
z[13] = z[14] + z[13] * T(2);
z[13] = z[1] * z[3] * z[13];
z[10] = z[10] + z[13];
z[10] = z[0] * z[10];
z[11] = z[9] * z[11] * z[12];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta236(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta237(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta238(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[2] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[2], 2) * z[3];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[1] * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta239(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[2] * z[11];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[1] * z[2];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[3] * z[10];
z[11] = prod_pow(z[3], 2) * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta240(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta241(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta242(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta243(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta244(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta245(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta246(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta247(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta248(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta249(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta250(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[2] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[2] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[1] * z[10];
z[11] = prod_pow(z[0], 2) * z[8] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta251(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta252(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta253(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[11] = z[1] * z[2];
z[12] = prod_pow(z[11], 2);
z[10] = z[10] * z[12];
z[13] = prod_pow(z[1], 2) * z[7];
z[11] = z[8] * z[11];
z[11] = z[11] + z[13];
z[13] = z[3] * T(2);
z[11] = z[2] * z[11] * z[13];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[9] * z[12] * z[13];
z[10] = z[10] + z[11];
zd[0] = z[0] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta254(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[1];
z[11] = prod_pow(z[11], 2);
z[10] = z[10] * z[11];
z[12] = prod_pow(z[0], 2) * z[1] * z[8];
z[13] = z[0] * prod_pow(z[1], 2) * z[9];
z[12] = z[12] + z[13];
z[10] = z[10] + z[12] * T(2);
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1] * z[3];
zd[1] = -((z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta255(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta256(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta257(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2);
z[11] = z[8] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[0] * z[12];
z[12] = z[12] + z[9] * T(2);
z[12] = z[0] * z[1] * z[12];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[1] * z[10];
z[12] = z[6] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[3] * z[7] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta258(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[3];
z[11] = prod_pow(z[11], 2);
z[10] = z[10] * z[11];
z[12] = prod_pow(z[0], 2) * z[3] * z[6];
z[13] = z[0] * prod_pow(z[3], 2) * z[9];
z[12] = z[12] + z[13];
z[10] = z[10] + z[12] * T(2);
z[10] = z[1] * z[10];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2] * z[3];
zd[1] = -((z[1] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta259(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2);
z[11] = z[8] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[0] * z[12];
z[12] = z[12] + z[9] * T(2);
z[12] = z[0] * z[1] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[1] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[6] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta260(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta261(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta262(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta263(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta264(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta265(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta266(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta267(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta268(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta269(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta270(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta271(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[2] * z[10];
z[10] = z[7] + z[10];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[2];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta272(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta273(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta274(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 3);
z[11] = z[9] * z[10];
z[12] = z[1] * z[4];
z[12] = z[8] + z[12];
z[12] = z[0] * prod_pow(z[1], 2) * z[12];
z[10] = z[0] * z[10];
z[13] = z[5] * z[10];
z[11] = z[11] + z[13] + z[12] * T(3);
z[11] = z[3] * z[11];
z[12] = z[6] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[3] * z[7] * z[10];
z[10] = z[11] + z[10] * T(2);
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta275(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[11] = z[7] * z[11];
z[13] = z[5] * z[12];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta276(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta277(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta278(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[2], 3);
z[11] = z[3] * z[10];
z[12] = z[4] * z[11];
z[13] = prod_pow(z[2], 2) * z[3] * z[7];
z[12] = z[12] + z[13];
z[13] = z[5] * z[11];
z[10] = z[6] * z[10];
z[10] = z[10] + z[13] + z[12] * T(3);
z[10] = z[0] * z[10];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[0] * z[8] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta279(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 2);
z[12] = z[10] * z[11];
z[13] = z[7] * z[12];
z[14] = z[5] + z[4] * T(3);
z[14] = z[0] * z[14];
z[14] = z[9] + z[14];
z[11] = z[11] * z[14];
z[10] = z[3] * z[6] * z[10];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[10] = z[10] + z[13];
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[2], 2) * z[8] * z[12];
z[10] = z[10] + z[11];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2] * z[3];
zd[1] = -((z[0] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta280(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[2];
z[11] = prod_pow(z[1], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[2] * z[9];
z[14] = z[5] + z[4] * T(3);
z[14] = z[2] * z[14];
z[14] = z[7] + z[14];
z[14] = z[0] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[1] * z[8] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta281(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta282(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta283(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[1] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta284(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[3] * z[10];
z[11] = z[0] * prod_pow(z[3], 2);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta285(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[1];
z[11] = prod_pow(z[2], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[1] * z[13];
z[13] = z[8] + z[13];
z[13] = z[0] * z[13];
z[14] = z[1] * z[9];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[2] * z[7] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta286(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[8] + z[10];
z[11] = z[0] * z[2];
z[10] = z[10] * z[11];
z[12] = z[2] * z[9];
z[13] = z[0] * z[7];
z[12] = z[12] + z[13];
z[12] = z[1] * z[12];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta287(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta288(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta289(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta290(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta291(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta292(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta293(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta294(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta295(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta296(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta297(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta298(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta299(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta300(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta301(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta302(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta303(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta304(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta305(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta306(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta307(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta308(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta309(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta310(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta311(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta312(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta313(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta314(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta315(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta316(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta317(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta318(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta319(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta320(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta321(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta322(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta323(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta324(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta325(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta326(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta327(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta328(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta329(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta330(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta331(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta332(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta333(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta334(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta335(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta336(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta337(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta338(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta339(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta340(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta341(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[2] * z[11];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[1] * z[2];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[3] * z[9] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[0], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta342(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta343(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta344(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta345(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta346(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta347(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta348(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta349(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta350(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[3];
z[11] = prod_pow(z[1], 2);
z[12] = z[9] * z[10] * z[11];
z[13] = z[2] * z[6];
z[14] = z[5] + z[4] * T(3);
z[14] = z[2] * z[14];
z[14] = z[7] + z[14];
z[14] = z[3] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[1] * z[8] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[0] * z[10];
z[10] = z[10] + z[12] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta351(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta352(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta353(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[7];
z[11] = prod_pow(z[2], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[1] * z[10];
z[12] = z[1] * z[11];
z[13] = z[4] * z[12];
z[11] = z[8] * z[11];
z[10] = z[10] + z[11] + z[13] * T(3);
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[3] * z[9] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta354(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[2] * z[11];
z[11] = z[7] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[3] * z[10];
z[11] = z[2] * prod_pow(z[3], 2);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[1] * z[9] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta355(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta356(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta357(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta358(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta359(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta360(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta361(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 3) * z[2] * z[6];
z[9] = z[9] + z[10] * T(4);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta362(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 3) * z[2] * z[6];
z[9] = z[9] + z[10] * T(4);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta363(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta364(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 3);
z[11] = z[6] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[0] * z[12];
z[12] = z[12] + z[9] * T(3);
z[12] = prod_pow(z[0], 2) * z[3] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[3] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[2] * z[8] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta365(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta366(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta367(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = prod_pow(z[1], 2) * z[2] * z[10];
z[11] = z[1] * prod_pow(z[2], 2) * z[8];
z[10] = z[10] + z[11] * T(2);
z[10] = z[3] * z[10];
z[11] = z[1] * z[2];
z[11] = prod_pow(z[11], 2);
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[3] * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta368(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 2);
z[11] = z[5] + z[4] * T(3);
z[11] = z[10] * z[11];
z[12] = z[3] * z[6];
z[11] = z[11] + z[12] * T(2);
z[11] = z[2] * z[11];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[2] * z[10];
z[12] = z[8] * z[10];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[1] * z[11];
z[10] = prod_pow(z[1], 2) * z[9] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[3];
zd[1] = -((prod_pow(z[0], 3) * z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta369(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta370(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta371(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[5];
z[10] = z[8] + z[10];
z[11] = prod_pow(z[2], 3);
z[10] = z[10] * z[11];
z[12] = z[4] * z[11];
z[13] = prod_pow(z[2], 2) * z[7];
z[12] = z[12] + z[13];
z[13] = z[1] * T(3);
z[12] = z[12] * z[13];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[12] = z[1] * z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[3] * z[9] * z[11] * z[13];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta372(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[2], 2) * z[6];
z[11] = z[2] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = prod_pow(z[11], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[1] * z[10];
z[12] = z[1] * T(3);
z[12] = z[11] * z[12];
z[13] = z[4] * z[12];
z[11] = z[8] * z[11];
z[10] = z[10] + z[11] + z[13];
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[2] * z[3];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta373(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * T(3);
z[11] = prod_pow(z[3], 3);
z[12] = z[2] * z[9] * z[10] * z[11];
z[10] = z[6] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[1] * z[13];
z[13] = z[8] + z[13];
z[13] = z[3] * z[13];
z[10] = z[10] + z[13];
z[10] = z[2] * prod_pow(z[3], 2) * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[10] = z[10] + z[12];
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[3];
zd[1] = -((z[1] * z[2] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta374(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta375(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta376(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 3) * z[2] * z[6];
z[9] = z[9] + z[10] * T(4);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta377(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta378(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta379(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta380(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta381(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta382(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta383(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta384(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[3];
z[11] = prod_pow(z[0], 2);
z[12] = z[8] * z[10] * z[11];
z[13] = z[3] * z[7];
z[14] = z[5] + z[4] * T(3);
z[14] = z[3] * z[14];
z[14] = z[6] + z[14];
z[14] = z[2] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[0] * z[9] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[10];
z[10] = z[10] + z[12] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta385(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta386(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta387(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[0], 2);
z[12] = z[7] * z[10] * z[11];
z[13] = z[3] * T(3);
z[14] = z[4] * z[13];
z[15] = z[3] * z[5];
z[14] = z[6] + z[14] + z[15];
z[14] = z[11] * z[14];
z[10] = z[0] * z[9] * z[10];
z[10] = z[10] + z[14];
z[10] = z[2] * z[10];
z[10] = z[10] + z[12];
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[2], 2) * z[8] * z[11] * z[13];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta388(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2);
z[11] = z[5] * z[10];
z[12] = z[0] * z[9];
z[11] = z[11] + z[12] * T(2);
z[11] = z[2] * z[11];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[12] = z[2] * z[6] * z[10];
z[11] = z[11] + z[12] * T(2);
z[11] = z[3] * z[11];
z[12] = z[2] * T(3);
z[10] = prod_pow(z[3], 2) * z[10] * z[12];
z[12] = z[4] * z[10];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[8] * z[10];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[3];
zd[1] = -((prod_pow(z[1], 3) * z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta389(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta390(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta391(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[1], 2);
z[12] = z[9] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[11] * z[13];
z[10] = z[1] * z[8] * z[10];
z[10] = z[10] + z[13];
z[10] = z[0] * z[10];
z[10] = z[10] + z[12];
z[10] = z[0] * z[2] * z[10];
z[11] = prod_pow(z[0], 2) * z[3] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta392(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[1];
z[12] = prod_pow(z[11], 2);
z[13] = prod_pow(z[2], 2) * z[12];
z[10] = z[10] * z[13];
z[14] = prod_pow(z[0], 2) * z[8];
z[11] = z[9] * z[11];
z[11] = z[11] + z[14];
z[11] = z[1] * z[2] * z[11];
z[12] = z[7] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[10] + z[11] * T(2);
z[10] = z[3] * z[10];
z[11] = z[6] * z[13];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1] * z[2] * z[3];
zd[1] = -((z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta393(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[8] * T(2);
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[1], 2);
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[11];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[3] * z[10];
z[11] = prod_pow(z[0], 2) * z[6] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta394(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta395(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta396(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[3] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[1] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[2] * z[10];
z[11] = prod_pow(z[0], 2) * z[7] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta397(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2) * z[3] * z[6];
z[11] = z[0] * prod_pow(z[3], 2) * z[9];
z[10] = z[10] + z[11];
z[11] = z[0] * z[3];
z[11] = prod_pow(z[11], 2);
z[12] = z[11] * T(3);
z[13] = z[4] * z[12];
z[14] = z[5] * z[11];
z[10] = z[13] + z[14] + z[10] * T(2);
z[10] = z[1] * z[10];
z[11] = z[8] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[12];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[3];
zd[1] = -((z[1] * prod_pow(z[2], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta398(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2) * z[2] * z[7];
z[11] = z[0] * prod_pow(z[2], 2) * z[9];
z[10] = z[10] + z[11];
z[11] = z[0] * z[2];
z[11] = prod_pow(z[11], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[1] * z[10];
z[12] = z[1] * T(3);
z[12] = z[11] * z[12];
z[13] = z[4] * z[12];
z[11] = z[8] * z[11];
z[10] = z[10] + z[11] + z[13];
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta399(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2);
z[11] = z[8] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[0] * z[12];
z[12] = z[12] + z[9] * T(2);
z[12] = z[0] * z[1] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[1] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[6] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta400(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta401(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta402(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta403(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta404(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta405(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta406(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta407(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta408(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta409(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta410(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta411(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[0] * z[10];
z[10] = z[9] + z[10];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[2] * z[8] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[1], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta412(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta413(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta414(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[2] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[2] * z[3] * z[10];
z[11] = z[0] * prod_pow(z[2], 2);
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta415(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 2);
z[11] = z[7] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[3] * z[12];
z[12] = z[12] + z[6] * T(2);
z[12] = z[2] * z[3] * z[12];
z[11] = z[11] + z[12];
z[11] = z[0] * z[11];
z[10] = z[2] * z[10];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[0] * z[8] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta416(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta417(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta418(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[9];
z[11] = z[0] * z[3];
z[12] = z[5] * z[11];
z[10] = z[10] + z[12];
z[12] = prod_pow(z[1], 3);
z[10] = z[10] * z[12];
z[13] = prod_pow(z[1], 2) * z[8];
z[14] = z[4] * z[12];
z[13] = z[13] + z[14];
z[14] = z[3] * T(3);
z[13] = z[13] * z[14];
z[14] = z[6] * z[12];
z[13] = z[13] + z[14];
z[13] = z[0] * z[13];
z[10] = z[10] + z[13];
z[10] = z[2] * z[10];
z[11] = z[7] * z[11] * z[12];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta419(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[3];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[11] + z[6] * T(2);
z[11] = z[10] * z[11];
z[12] = prod_pow(z[3], 2) * z[7];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[2] * z[11];
z[10] = prod_pow(z[10], 2);
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[0] * z[8] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[2] * z[3];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta420(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[13] = z[5] * z[12];
z[11] = z[7] * z[11];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[3];
zd[1] = -((z[0] * z[2] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta421(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta422(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta423(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[9] * z[10];
z[12] = z[1] * z[4];
z[12] = z[8] * T(2) + z[12] * T(3);
z[12] = z[0] * z[1] * z[12];
z[10] = z[0] * z[10];
z[13] = z[5] * z[10];
z[11] = z[11] + z[12] + z[13];
z[11] = z[3] * z[11];
z[12] = z[6] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[3] * z[7] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta424(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[3];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[11] + z[6] * T(2);
z[11] = z[10] * z[11];
z[12] = prod_pow(z[3], 2) * z[8];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[1] * z[11];
z[10] = prod_pow(z[10], 2);
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[7] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[3];
zd[1] = -((z[0] * prod_pow(z[2], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta425(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[9] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[1] * z[12];
z[12] = z[12] + z[8] * T(2);
z[12] = z[0] * z[1] * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12] * T(2);
z[11] = z[2] * z[3] * z[11];
z[10] = prod_pow(z[2], 2) * z[6] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta426(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[5] + z[4] * T(3);
z[11] = z[10] * z[11];
z[12] = z[1] * z[8];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[11];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[6] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta427(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta428(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta429(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[3] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[1] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[2], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta430(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[3] * z[10];
z[11] = z[0] * prod_pow(z[3], 2);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta431(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 3);
z[11] = z[1] * z[10];
z[12] = z[4] * z[11];
z[13] = z[1] * prod_pow(z[3], 2) * z[6];
z[12] = z[12] + z[13];
z[10] = z[8] * z[10];
z[13] = z[5] * z[11];
z[10] = z[10] + z[13] + z[12] * T(3);
z[10] = z[0] * z[10];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[2] * z[3];
zd[1] = -((z[0] * z[1] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta432(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[1];
z[11] = prod_pow(z[2], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[0] * z[8];
z[14] = z[5] + z[4] * T(3);
z[14] = z[0] * z[14];
z[14] = z[9] + z[14];
z[14] = z[1] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[2] * z[7] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta433(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[8];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[1];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta434(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta435(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta436(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta437(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta438(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta439(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta440(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta441(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta442(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta443(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta444(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta445(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta446(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta447(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta448(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta449(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta450(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta451(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta452(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta453(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta454(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta455(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta456(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta457(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta458(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 4);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 3) * z[2] * z[6];
z[9] = z[9] + z[10] * T(4);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta459(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta460(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta461(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta462(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta463(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta464(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta465(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta466(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta467(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta468(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta469(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta470(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta471(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta472(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta473(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta474(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta475(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta476(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta477(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta478(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta479(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[0], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta480(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta481(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta482(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[2] * z[11];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[1] * z[2];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[3] * z[9] * z[11];
z[10] = z[10] + z[11] * T(6);
zd[0] = prod_pow(z[0], 5) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta483(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta484(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta485(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta486(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta487(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta488(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta489(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[7] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[1] * z[12];
z[12] = z[12] + z[8] * T(2);
z[12] = z[1] * z[2] * z[12];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[10];
z[12] = z[6] * z[10];
z[11] = z[11] + z[12];
z[11] = z[0] * z[11];
z[10] = z[3] * z[9] * z[10];
z[10] = z[11] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta490(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta491(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta492(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[7];
z[11] = prod_pow(z[2], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[1] * z[10];
z[12] = z[1] * z[11];
z[13] = z[4] * z[12];
z[11] = z[8] * z[11];
z[10] = z[10] + z[11] + z[13] * T(3);
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[3] * z[9] * z[12];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[0], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta493(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[2] * z[11];
z[11] = z[7] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[3] * z[10];
z[11] = z[2] * prod_pow(z[3], 2);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[1] * z[9] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[0], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta494(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta495(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta496(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta497(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta498(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta499(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta500(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta501(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[13] = z[5] * z[12];
z[11] = z[7] * z[11];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[3] * z[9] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta502(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta503(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta504(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[2];
z[10] = z[3] * prod_pow(z[10], 2);
z[11] = z[5] + z[4] * T(3);
z[11] = z[10] * z[11];
z[12] = prod_pow(z[1], 2);
z[13] = z[6] * z[12];
z[14] = z[3] * T(2);
z[15] = z[1] * z[8] * z[14];
z[13] = z[13] + z[15];
z[13] = prod_pow(z[2], 2) * z[13];
z[12] = z[2] * z[7] * z[12] * z[14];
z[11] = z[11] + z[12] + z[13];
z[11] = z[0] * z[11];
z[10] = z[9] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta505(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 2);
z[11] = z[5] + z[4] * T(3);
z[11] = z[10] * z[11];
z[12] = z[3] * z[6];
z[11] = z[11] + z[12] * T(2);
z[11] = z[2] * z[11];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[2] * z[10];
z[12] = z[8] * z[10];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[1] * z[11];
z[10] = prod_pow(z[1], 2) * z[9] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[3];
zd[1] = -((prod_pow(z[0], 4) * z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta506(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta507(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta508(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[5];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[2], 2) * z[10];
z[11] = prod_pow(z[2], 3);
z[12] = z[4] * z[11];
z[10] = z[10] + z[12] * T(3);
z[10] = z[3] * z[10];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[3] * z[11];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[1] * z[9] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta509(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[2] * z[3];
z[12] = prod_pow(z[11], 2);
z[13] = z[1] * z[12];
z[10] = z[10] * z[13];
z[14] = prod_pow(z[2], 2) * z[6];
z[11] = z[7] * z[11];
z[11] = z[11] + z[14];
z[11] = z[1] * z[3] * z[11];
z[12] = z[8] * z[12];
z[10] = z[10] + z[12] + z[11] * T(2);
z[10] = z[0] * z[10];
z[11] = z[9] * z[13];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[2] * z[3];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta510(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 3);
z[11] = z[8] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[3] * z[12];
z[12] = z[12] + z[6] * T(3);
z[12] = z[1] * prod_pow(z[3], 2) * z[12];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[1] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[0] * z[11];
z[10] = z[2] * z[9] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta511(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta512(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta513(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(4);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta514(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta515(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta516(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta517(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta518(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta519(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * z[3];
z[11] = prod_pow(z[0], 3);
z[12] = z[8] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[2] * z[13];
z[14] = z[3] * z[7];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = prod_pow(z[0], 2) * z[9] * z[10];
z[10] = z[11] + z[10] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[12] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta520(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta521(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[5] * z[9];
z[8] = z[8] + z[10];
z[8] = prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3) * z[9];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta522(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 3);
z[11] = z[3] * z[10];
z[12] = prod_pow(z[2], 2);
z[13] = z[8] * z[11] * z[12];
z[14] = z[4] * z[10];
z[15] = prod_pow(z[0], 2) * z[9];
z[14] = z[14] + z[15];
z[15] = z[5] * z[10];
z[14] = z[15] + z[14] * T(3);
z[14] = z[3] * z[14];
z[10] = z[6] * z[10];
z[10] = z[10] + z[14];
z[10] = z[10] * z[12];
z[11] = z[2] * z[7] * z[11];
z[10] = z[10] + z[11] * T(2);
z[10] = z[1] * z[10];
z[10] = z[10] + z[13] * T(3);
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta523(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 3) * z[8];
z[11] = z[0] * z[4];
z[11] = z[9] + z[11];
z[11] = prod_pow(z[0], 2) * z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = prod_pow(z[1], 2) * z[10];
z[11] = z[0] * z[1];
z[11] = prod_pow(z[11], 3);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(3);
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[3] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2) * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta524(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[5] * z[9];
z[8] = z[8] + z[10];
z[8] = prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3) * z[9];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta525(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta526(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[2];
z[10] = prod_pow(z[10], 3);
z[11] = z[4] * z[10];
z[12] = prod_pow(z[0], 3) * prod_pow(z[2], 2) * z[7];
z[13] = prod_pow(z[0], 2) * prod_pow(z[2], 3) * z[9];
z[11] = z[11] + z[12] + z[13];
z[12] = z[5] * z[10];
z[11] = z[12] + z[11] * T(3);
z[11] = z[3] * z[11];
z[12] = z[6] * z[10];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[3] * z[8] * z[10];
z[10] = z[11] + z[10] * T(2);
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta527(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[5] * z[10];
z[12] = z[10] * T(3);
z[13] = z[4] * z[12];
z[11] = z[11] + z[13];
z[13] = z[2] * z[3];
z[13] = prod_pow(z[13], 2);
z[11] = z[11] * z[13];
z[14] = prod_pow(z[2], 2) * z[3] * z[6];
z[15] = z[2] * prod_pow(z[3], 2) * z[7];
z[14] = z[14] + z[15];
z[10] = z[10] * z[14];
z[14] = z[1] * z[8] * z[13];
z[10] = z[10] + z[14];
z[10] = z[11] + z[10] * T(2);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12] * z[13];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2] * z[3];
zd[1] = -((prod_pow(z[0], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta528(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[8] * T(2);
z[10] = z[1] * z[2] * z[10];
z[11] = prod_pow(z[1], 2);
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[12] = prod_pow(z[0], 3);
z[10] = z[10] * z[12];
z[13] = z[2] * T(3);
z[11] = z[11] * z[13];
z[13] = prod_pow(z[0], 2) * z[9] * z[11];
z[10] = z[10] + z[13];
z[10] = z[3] * z[10];
z[11] = z[6] * z[11] * z[12];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[3];
zd[1] = -((prod_pow(z[1], 2) * z[2] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta529(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta530(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta531(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[9];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[0] * z[11];
z[10] = z[11] + z[10] * T(3);
z[10] = prod_pow(z[0], 2) * z[3] * z[10];
z[11] = prod_pow(z[0], 3) * z[1];
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[3] * z[7] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta532(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * T(3);
z[11] = prod_pow(z[3], 2);
z[12] = prod_pow(z[2], 3);
z[13] = z[9] * z[10] * z[11] * z[12];
z[14] = z[1] * z[5];
z[14] = z[8] + z[14];
z[14] = z[12] * z[14];
z[15] = z[2] * z[4];
z[15] = z[7] + z[15];
z[10] = prod_pow(z[2], 2) * z[10] * z[15];
z[10] = z[10] + z[14];
z[10] = z[10] * z[11];
z[11] = z[1] * z[3] * z[6] * z[12];
z[10] = z[10] + z[11] * T(2);
z[10] = z[0] * z[10];
z[10] = z[10] + z[13];
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * prod_pow(z[3], 2) * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta533(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 3);
z[11] = z[1] * z[10];
z[12] = z[4] * z[11];
z[13] = z[1] * prod_pow(z[3], 2) * z[6];
z[12] = z[12] + z[13];
z[13] = z[5] * z[11];
z[10] = z[8] * z[10];
z[10] = z[10] + z[13] + z[12] * T(3);
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[2] * z[10];
z[11] = prod_pow(z[2], 2) * z[9] * z[11];
z[10] = z[10] + z[11] * T(3);
zd[0] = prod_pow(z[0], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[3];
zd[1] = -((z[1] * prod_pow(z[2], 2) * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta534(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2) * z[9];
z[11] = prod_pow(z[0], 3);
z[12] = z[4] * z[11];
z[10] = z[10] + z[12];
z[12] = z[1] * T(3);
z[10] = z[10] * z[12];
z[12] = z[1] * z[5];
z[12] = z[8] + z[12];
z[12] = z[11] * z[12];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[11];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta535(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta536(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta537(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[0], 2) * z[1] * z[8];
z[9] = prod_pow(z[0], 3) * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta538(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta539(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[5] * z[9];
z[8] = z[8] + z[10];
z[8] = prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3) * z[9];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta540(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta541(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta542(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta543(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta544(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[9];
z[11] = z[5] + z[4] * T(3);
z[11] = z[3] * z[11];
z[11] = z[6] + z[11];
z[11] = z[0] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[0] * z[2] * z[10];
z[11] = prod_pow(z[0], 2) * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[2] * z[8] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[1], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta545(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta546(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta547(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * T(2);
z[11] = prod_pow(z[2], 2);
z[12] = z[9] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[6] + z[13];
z[13] = z[11] * z[13];
z[10] = z[2] * z[7] * z[10];
z[10] = z[10] + z[13];
z[10] = z[0] * z[10];
z[10] = z[10] + z[12];
z[10] = z[0] * z[1] * z[10];
z[11] = prod_pow(z[0], 2) * z[3] * z[8] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta548(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[3];
z[11] = prod_pow(z[11], 2);
z[12] = z[2] * z[11];
z[10] = z[10] * z[12];
z[13] = z[0] * prod_pow(z[3], 2) * z[9];
z[14] = prod_pow(z[0], 2) * z[3] * z[6];
z[13] = z[13] + z[14];
z[13] = z[2] * z[13];
z[11] = z[7] * z[11];
z[10] = z[10] + z[11] + z[13] * T(2);
z[10] = z[1] * z[10];
z[11] = z[8] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[3];
zd[1] = -((prod_pow(z[1], 4) * z[2] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta549(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta550(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta551(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[3] * z[5];
z[10] = z[6] + z[10];
z[11] = prod_pow(z[2], 3);
z[10] = z[10] * z[11];
z[12] = prod_pow(z[2], 2) * z[7];
z[13] = z[4] * z[11];
z[12] = z[12] + z[13];
z[13] = z[3] * T(3);
z[12] = z[12] * z[13];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[12] = z[3] * z[9] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[1] * z[10];
z[11] = prod_pow(z[0], 2) * z[8] * z[11] * z[13];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta552(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2);
z[11] = z[5] * z[10];
z[10] = z[10] * T(3);
z[12] = z[4] * z[10];
z[11] = z[11] + z[12];
z[12] = z[2] * z[3];
z[12] = prod_pow(z[12], 2);
z[11] = z[11] * z[12];
z[13] = z[2] * prod_pow(z[3], 2) * z[7];
z[14] = prod_pow(z[2], 2) * z[3] * z[6];
z[13] = z[13] + z[14];
z[13] = z[0] * z[13];
z[14] = z[9] * z[12];
z[13] = z[13] + z[14];
z[13] = z[0] * z[13];
z[11] = z[11] + z[13] * T(2);
z[11] = z[1] * z[11];
z[10] = z[8] * z[10] * z[12];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2] * z[3];
zd[1] = -((prod_pow(z[1], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta553(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[2] * T(3);
z[11] = prod_pow(z[1], 3);
z[12] = prod_pow(z[0], 2);
z[13] = z[6] * z[10] * z[11] * z[12];
z[10] = z[8] * z[10];
z[14] = z[5] + z[4] * T(3);
z[14] = z[2] * z[14];
z[14] = z[7] + z[14];
z[14] = z[1] * z[14];
z[10] = z[10] + z[14];
z[10] = prod_pow(z[1], 2) * z[10] * z[12];
z[11] = z[0] * z[2] * z[9] * z[11];
z[10] = z[10] + z[11] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[13];
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[3];
zd[1] = -((prod_pow(z[0], 2) * z[2] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta554(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta555(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta556(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2);
z[11] = z[5] + z[4] * T(3);
z[11] = z[10] * z[11];
z[12] = z[0] * z[9];
z[11] = z[11] + z[12] * T(2);
z[11] = z[1] * z[11];
z[12] = z[8] * z[10];
z[11] = z[11] + z[12] * T(2);
z[11] = z[1] * z[3] * z[11];
z[10] = prod_pow(z[1], 2) * z[10];
z[12] = z[6] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[3] * z[7] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta557(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2) * z[1] * z[8];
z[11] = z[0] * prod_pow(z[1], 2) * z[9];
z[10] = z[10] + z[11];
z[11] = z[0] * z[1];
z[11] = prod_pow(z[11], 2);
z[12] = z[11] * T(3);
z[13] = z[4] * z[12];
z[14] = z[5] * z[11];
z[10] = z[13] + z[14] + z[10] * T(2);
z[13] = prod_pow(z[3], 2);
z[10] = z[10] * z[13];
z[11] = z[3] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
z[10] = z[2] * z[10];
z[11] = z[7] * z[12] * z[13];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1] * z[3];
zd[1] = -((prod_pow(z[2], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta558(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[2], 2);
z[11] = z[5] * z[10];
z[10] = z[10] * T(3);
z[12] = z[4] * z[10];
z[11] = z[11] + z[12];
z[12] = z[0] * z[1];
z[12] = prod_pow(z[12], 2);
z[11] = z[11] * z[12];
z[13] = prod_pow(z[0], 2) * z[1] * z[8];
z[14] = z[0] * prod_pow(z[1], 2) * z[9];
z[13] = z[13] + z[14];
z[13] = z[2] * z[13];
z[14] = z[7] * z[12];
z[13] = z[13] + z[14];
z[13] = z[2] * z[13];
z[11] = z[11] + z[13] * T(2);
z[11] = z[3] * z[11];
z[10] = z[6] * z[10] * z[12];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1] * z[2];
zd[1] = -((prod_pow(z[3], 3) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta559(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[0], 2) * z[8];
z[11] = z[0] * z[1];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = prod_pow(z[11], 2);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(2);
z[10] = z[2] * z[10];
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[11] = z[7] * z[11];
z[10] = z[10] + z[11] + z[13] * T(3);
z[10] = z[3] * z[10];
z[11] = z[6] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta560(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta561(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta562(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[3] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[1] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12] * T(2);
z[10] = z[0] * z[2] * z[10];
z[11] = prod_pow(z[0], 2) * z[7] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[2], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta563(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[11] = z[0] * z[3];
z[11] = prod_pow(z[11], 2);
z[10] = z[10] * z[11];
z[12] = prod_pow(z[0], 2) * z[3] * z[6];
z[13] = z[0] * prod_pow(z[3], 2) * z[9];
z[12] = z[12] + z[13];
z[10] = z[10] + z[12] * T(2);
z[10] = z[1] * z[10];
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[3];
zd[1] = -((z[1] * prod_pow(z[2], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta564(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * T(3);
z[11] = prod_pow(z[0], 2);
z[12] = prod_pow(z[2], 3);
z[13] = z[6] * z[10] * z[11] * z[12];
z[14] = z[5] + z[4] * T(3);
z[14] = z[1] * z[14];
z[14] = z[8] + z[14];
z[14] = z[12] * z[14];
z[10] = prod_pow(z[2], 2) * z[7] * z[10];
z[10] = z[10] + z[14];
z[10] = z[10] * z[11];
z[11] = z[0] * z[1] * z[9] * z[12];
z[10] = z[10] + z[11] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[13];
zd[0] = prod_pow(z[3], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[2] * z[3];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta565(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * T(2);
z[11] = z[7] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[1] * z[12];
z[12] = z[8] + z[12];
z[12] = z[2] * z[12];
z[11] = z[11] + z[12];
z[12] = prod_pow(z[0], 2);
z[11] = z[2] * z[11] * z[12];
z[13] = prod_pow(z[2], 2);
z[10] = z[0] * z[9] * z[10] * z[13];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[1] * z[6] * z[12] * z[13];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[0] * z[2];
zd[1] = -((z[1] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta566(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[7];
z[11] = z[1] * z[2];
z[12] = z[4] * z[11];
z[10] = z[10] + z[12] * T(3);
z[12] = prod_pow(z[0], 2);
z[10] = z[10] * z[12];
z[13] = z[1] * z[9];
z[14] = z[1] * z[5];
z[14] = z[8] + z[14];
z[14] = z[0] * z[14];
z[13] = z[14] + z[13] * T(2);
z[13] = z[0] * z[2] * z[13];
z[10] = z[10] + z[13];
z[10] = z[3] * z[10];
z[11] = z[6] * z[11] * z[12];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta567(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta568(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta569(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta570(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta571(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta572(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta573(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta574(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta575(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta576(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta577(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + z[4] * T(3);
z[10] = z[0] * z[10];
z[10] = z[9] + z[10];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[2] * z[8] * z[11];
z[10] = z[10] + z[11] * T(6);
zd[0] = prod_pow(z[1], 5) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta578(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta579(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta580(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[7];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[2] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[2] * z[3] * z[10];
z[11] = z[0] * prod_pow(z[2], 2);
z[12] = z[6] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[1], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta581(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 2);
z[11] = z[7] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[3] * z[12];
z[12] = z[12] + z[6] * T(2);
z[12] = z[2] * z[3] * z[12];
z[11] = z[11] + z[12];
z[11] = z[0] * z[11];
z[10] = z[2] * z[10];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[0] * z[8] * z[10];
z[10] = z[11] + z[10] * T(5);
zd[0] = prod_pow(z[1], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta582(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta583(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta584(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[2], 3);
z[11] = z[3] * z[10];
z[12] = z[4] * z[11];
z[13] = prod_pow(z[2], 2) * z[3] * z[7];
z[12] = z[12] + z[13];
z[13] = z[5] * z[11];
z[10] = z[6] * z[10];
z[10] = z[10] + z[13] + z[12] * T(3);
z[10] = z[0] * z[10];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[0] * z[8] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta585(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 2);
z[12] = z[7] * z[10] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[13] = z[11] * z[13];
z[10] = z[3] * z[6] * z[10];
z[10] = z[10] + z[13];
z[10] = z[2] * z[10];
z[10] = z[10] + z[12];
z[10] = z[1] * z[2] * z[10];
z[11] = z[0] * prod_pow(z[2], 2) * z[8] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[2] * z[3];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta586(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 2) * z[6];
z[11] = prod_pow(z[3], 3);
z[12] = z[4] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[12] = z[7] * z[11];
z[11] = z[2] * z[11];
z[13] = z[5] * z[11];
z[10] = z[12] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[1] * z[10];
z[11] = z[0] * z[8] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[1], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta587(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta588(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 2) * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta589(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 3);
z[11] = z[9] * z[10];
z[12] = z[1] * z[4];
z[12] = z[8] + z[12];
z[12] = z[0] * prod_pow(z[1], 2) * z[12];
z[10] = z[0] * z[10];
z[13] = z[5] * z[10];
z[11] = z[11] + z[13] + z[12] * T(3);
z[11] = z[3] * z[11];
z[12] = z[6] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[3] * z[7] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta590(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(3);
z[11] = prod_pow(z[3], 2);
z[10] = z[10] * z[11];
z[12] = z[7] * z[10];
z[11] = z[9] * z[11];
z[13] = z[5] + z[4] * T(3);
z[13] = z[3] * z[13];
z[13] = z[13] + z[6] * T(2);
z[13] = z[0] * z[3] * z[13];
z[11] = z[11] + z[13];
z[11] = z[2] * z[11];
z[11] = z[11] + z[12];
z[11] = z[1] * prod_pow(z[2], 2) * z[11];
z[10] = prod_pow(z[2], 3) * z[8] * z[10];
z[10] = z[10] + z[11];
zd[0] = prod_pow(z[1], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * prod_pow(z[3], 2) * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta591(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 3);
z[11] = z[4] * z[10];
z[12] = prod_pow(z[3], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[1] * z[11];
z[10] = z[8] * z[10];
z[10] = z[10] + z[11];
z[10] = prod_pow(z[1], 2) * z[10];
z[11] = z[1] * z[3];
z[11] = prod_pow(z[11], 3);
z[12] = z[5] * z[11];
z[10] = z[12] + z[10] * T(3);
z[10] = z[0] * z[10];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(2);
zd[0] = z[2] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[3];
zd[1] = -((z[0] * prod_pow(z[2], 2) * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta592(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2) * z[2] * z[8];
z[11] = prod_pow(z[1], 3);
z[12] = z[2] * z[11];
z[13] = z[4] * z[12];
z[10] = z[10] + z[13];
z[11] = z[7] * z[11];
z[13] = z[5] * z[12];
z[10] = z[11] + z[13] + z[10] * T(3);
z[10] = z[0] * z[10];
z[11] = z[9] * z[12];
z[10] = z[10] + z[11];
z[10] = z[3] * z[10];
z[11] = z[0] * z[6] * z[12];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta593(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 2) * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta594(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta595(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[6] * z[10];
z[12] = z[5] + z[4] * T(3);
z[12] = z[1] * z[12];
z[12] = z[12] + z[8] * T(2);
z[12] = z[1] * z[3] * z[12];
z[11] = z[11] + z[12];
z[11] = z[0] * z[11];
z[10] = z[3] * z[10];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[7] * z[10];
z[10] = z[11] + z[10] * T(5);
zd[0] = prod_pow(z[2], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta596(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[0] * z[10];
z[12] = prod_pow(z[3], 2);
z[13] = z[7] * z[11] * z[12];
z[14] = z[5] + z[4] * T(3);
z[14] = z[1] * z[14];
z[14] = z[14] + z[8] * T(2);
z[14] = z[0] * z[1] * z[14];
z[10] = z[9] * z[10];
z[10] = z[10] + z[14];
z[10] = z[10] * z[12];
z[11] = z[3] * z[6] * z[11];
z[10] = z[10] + z[11] * T(2);
z[10] = z[2] * z[10];
z[10] = z[10] + z[13] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[3];
zd[1] = -((z[0] * prod_pow(z[2], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta597(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[3], 3);
z[11] = z[4] * z[10];
z[12] = prod_pow(z[3], 2) * z[6];
z[11] = z[11] + z[12];
z[12] = z[5] * z[10];
z[11] = z[12] + z[11] * T(3);
z[11] = z[1] * z[11];
z[12] = z[8] * z[10];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[1] * z[11];
z[10] = prod_pow(z[1], 2) * z[10];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[7] * z[10];
z[10] = z[11] + z[10] * T(3);
zd[0] = prod_pow(z[2], 2) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[2] * z[3];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[4] * prod_pow(z[5], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta598(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * T(2);
z[11] = prod_pow(z[3], 4);
z[10] = z[10] * z[11];
z[12] = z[7] * z[10];
z[13] = z[5] + z[4] * T(3);
z[13] = z[0] * z[13];
z[13] = z[9] + z[13];
z[11] = z[11] * z[13];
z[13] = z[0] * prod_pow(z[3], 3) * z[6];
z[11] = z[11] + z[13] * T(4);
z[11] = z[2] * z[11];
z[11] = z[11] + z[12];
z[11] = z[1] * z[2] * z[11];
z[10] = prod_pow(z[2], 2) * z[8] * z[10];
z[10] = z[10] + z[11];
zd[0] = z[1] * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = z[1] * z[2];
zd[1] = -((z[0] * prod_pow(z[3], 4) * z[4] * prod_pow(z[5], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta599(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[1], 2);
z[11] = z[5] + z[4] * T(3);
z[11] = z[10] * z[11];
z[12] = z[1] * z[8];
z[11] = z[11] + z[12] * T(2);
z[11] = z[0] * z[11];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[2] * z[11];
z[10] = z[0] * z[10];
z[12] = z[7] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[2] * z[6] * z[10];
z[10] = z[11] + z[10] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta600(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta601(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta602(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[1] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[3] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[1] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(6);
zd[0] = prod_pow(z[2], 5) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta603(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[3] * z[10];
z[11] = z[0] * prod_pow(z[3], 2);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[2], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta604(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(3);
z[10] = z[1] * prod_pow(z[3], 2) * z[10];
z[11] = z[0] * prod_pow(z[3], 3);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta605(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = prod_pow(z[2], 3);
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[10] * z[11];
z[12] = z[1] * prod_pow(z[2], 2) * z[7];
z[11] = z[11] + z[12] * T(3);
z[11] = z[0] * z[11];
z[10] = z[1] * z[10];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[0] * z[6] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta606(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[1];
z[11] = prod_pow(z[2], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[0] * z[8];
z[14] = z[5] + z[4] * T(3);
z[14] = z[0] * z[14];
z[14] = z[9] + z[14];
z[14] = z[1] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[2] * z[7] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta607(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[0] * z[8];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[1];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(6);
zd[0] = prod_pow(z[3], 5) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 6) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta608(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta609(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta610(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[0], 6) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta611(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta612(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta613(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta614(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta615(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta616(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta617(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta618(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta619(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta620(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta621(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta622(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * T(4);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta623(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta624(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta625(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta626(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta627(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta628(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[0], 2) * z[1] * z[8];
z[9] = prod_pow(z[0], 3) * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta629(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta630(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[5] * z[9];
z[8] = z[8] + z[10];
z[8] = prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3) * z[9];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta631(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta632(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta633(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta634(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta635(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta636(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta637(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta638(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta639(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta640(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta641(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta642(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][8];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[1][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta643(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta644(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta645(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta646(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta647(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta648(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta649(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta650(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta651(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta652(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta653(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta654(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta655(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta656(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta657(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta658(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta659(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta660(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta661(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta662(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta663(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta664(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta665(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta666(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta667(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta668(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta669(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta670(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta671(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta672(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta673(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta674(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta675(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta676(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta677(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta678(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta679(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta680(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta681(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta682(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta683(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta684(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta685(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta686(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta687(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta688(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta689(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta690(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta691(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta692(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta693(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta694(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta695(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta696(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta697(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta698(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta699(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta700(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta701(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta702(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta703(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta704(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta705(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta706(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta707(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta708(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta709(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta710(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta711(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta712(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta713(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta714(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta715(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta716(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta717(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta718(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta719(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta720(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta721(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta722(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta723(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta724(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta725(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta726(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta727(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta728(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta729(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta730(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta731(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta732(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta733(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta734(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta735(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta736(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta737(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta738(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta739(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta740(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta741(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta742(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta743(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta744(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta745(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta746(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta747(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta748(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta749(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta750(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta751(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta752(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta753(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta754(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta755(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta756(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta757(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta758(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta759(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta760(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta761(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta762(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta763(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta764(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta765(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta766(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta767(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta768(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta769(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta770(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta771(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta772(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta773(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta774(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta775(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta776(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta777(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta778(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta779(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta780(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta781(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta782(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta783(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta784(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta785(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta786(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta787(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta788(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta789(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta790(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta791(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta792(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta793(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta794(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta795(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta796(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta797(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta798(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta799(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta800(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta801(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta802(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta803(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta804(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta805(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta806(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta807(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta808(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta809(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta810(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta811(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta812(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta813(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta814(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta815(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta816(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta817(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta818(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta819(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 4);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 3) * z[2] * z[6];
z[9] = z[9] + z[10] * T(4);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta820(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta821(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta822(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta823(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta824(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta825(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta826(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta827(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta828(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta829(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta830(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta831(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta832(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta833(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta834(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta835(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta836(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta837(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta838(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta839(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta840(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta841(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta842(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta843(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta844(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta845(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta846(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta847(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta848(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta849(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta850(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 2) * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta851(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta852(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta853(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[1] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[3] * z[11];
z[10] = z[10] + z[11];
z[10] = z[0] * z[10];
z[11] = z[1] * z[3];
z[12] = z[9] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[0] * z[7] * z[11];
z[10] = z[10] + z[11] * T(6);
zd[0] = prod_pow(z[2], 5) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta854(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(2);
z[10] = z[1] * z[3] * z[10];
z[11] = z[0] * prod_pow(z[3], 2);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(5);
zd[0] = prod_pow(z[2], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * prod_pow(z[3], 2) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta855(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * z[6];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[3] * z[11];
z[10] = z[11] + z[10] * T(3);
z[10] = z[1] * prod_pow(z[3], 2) * z[10];
z[11] = z[0] * prod_pow(z[3], 3);
z[12] = z[8] * z[11];
z[10] = z[10] + z[12];
z[10] = z[2] * z[10];
z[11] = z[1] * z[7] * z[11];
z[10] = z[10] + z[11] * T(4);
zd[0] = prod_pow(z[2], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * prod_pow(z[3], 3) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta856(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = prod_pow(z[2], 3);
z[11] = z[5] + z[4] * T(3);
z[11] = z[1] * z[11];
z[11] = z[8] + z[11];
z[11] = z[10] * z[11];
z[12] = z[1] * prod_pow(z[2], 2) * z[7];
z[11] = z[11] + z[12] * T(3);
z[11] = z[0] * z[11];
z[10] = z[1] * z[10];
z[12] = z[9] * z[10];
z[11] = z[11] + z[12];
z[11] = z[3] * z[11];
z[10] = z[0] * z[6] * z[10];
z[10] = z[11] + z[10] * T(4);
zd[0] = prod_pow(z[3], 3) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * prod_pow(z[3], 4) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta857(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * z[1];
z[11] = prod_pow(z[2], 2);
z[12] = z[6] * z[10] * z[11];
z[13] = z[0] * z[8];
z[14] = z[5] + z[4] * T(3);
z[14] = z[0] * z[14];
z[14] = z[9] + z[14];
z[14] = z[1] * z[14];
z[13] = z[13] + z[14];
z[11] = z[11] * z[13];
z[10] = z[2] * z[7] * z[10];
z[10] = z[11] + z[10] * T(2);
z[10] = z[3] * z[10];
z[10] = z[10] + z[12] * T(5);
zd[0] = prod_pow(z[3], 4) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * prod_pow(z[3], 5) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta858(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
z[5] = ibp_vectors[1].back();
z[6] = ibp_vectors[1][3];
z[7] = ibp_vectors[1][2];
z[8] = ibp_vectors[1][1];
z[9] = ibp_vectors[1][0];
z[10] = z[0] * z[8];
z[11] = z[5] + z[4] * T(3);
z[11] = z[0] * z[11];
z[11] = z[9] + z[11];
z[11] = z[1] * z[11];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[0] * z[1];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[3] * z[10];
z[11] = z[2] * z[6] * z[11];
z[10] = z[10] + z[11] * T(6);
zd[0] = prod_pow(z[3], 5) * z[10];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2] * prod_pow(z[3], 6) * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta859(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta860(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta861(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[0], 6) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta862(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta863(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta864(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta865(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta866(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta867(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta868(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta869(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta870(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta871(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta872(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta873(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * T(4);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta874(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta875(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta876(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta877(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta878(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta879(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[0], 2) * z[1] * z[8];
z[9] = prod_pow(z[0], 3) * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta880(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta881(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[5] * z[9];
z[8] = z[8] + z[10];
z[8] = prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3) * z[9];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta882(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta883(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta884(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta885(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta886(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta887(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta888(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta889(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta890(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta891(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta892(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta893(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta894(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta895(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta896(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta897(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta898(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta899(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta900(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][8];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta901(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta902(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][8];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[0], 6) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta903(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta904(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta905(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta906(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta907(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta908(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta909(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][8];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[4][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta910(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta911(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta912(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta913(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta914(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta915(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta916(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta917(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__z_z_zSurfaceIta918(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 8) * z[1] * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,918> TriangleTriangle_101yy_z__z_z_zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,918> TriangleTriangle_101yy_z__z_z_zSurfaceItaArray;
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[0] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta1, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[1] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta2, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[2] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta3, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[3] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta4, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[4] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta5, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[5] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta6, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[6] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta7, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[7] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta8, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[8] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta9, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[9] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta10, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[10] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta11, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[11] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta12, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[12] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta13, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[13] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta14, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[14] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta15, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[15] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta16, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[16] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta17, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[17] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta18, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[18] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta19, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[19] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta20, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[20] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta21, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[21] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta22, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[22] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta23, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[23] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta24, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[24] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta25, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[25] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta26, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[26] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta27, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[27] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta28, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[28] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta29, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[29] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta30, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[30] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta31, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[31] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta32, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[32] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta33, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[33] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta34, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[34] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta35, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[35] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta36, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[36] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta37, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[37] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta38, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[38] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta39, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[39] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta40, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[40] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta41, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[41] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta42, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[42] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta43, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[43] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta44, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[44] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta45, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[45] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta46, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[46] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta47, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[47] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta48, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[48] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta49, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[49] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta50, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[50] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta51, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[51] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta52, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[52] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta53, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[53] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta54, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[54] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta55, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[55] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta56, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[56] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta57, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[57] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta58, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[58] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta59, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[59] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta60, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[60] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta61, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[61] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta62, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[62] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta63, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[63] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta64, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[64] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta65, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[65] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta66, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[66] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta67, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[67] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta68, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[68] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta69, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[69] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta70, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[70] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta71, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[71] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta72, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[72] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta73, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[73] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta74, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[74] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta75, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[75] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta76, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[76] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta77, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[77] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta78, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[78] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta79, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[79] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta80, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[80] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta81, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[81] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta82, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[82] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta83, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[83] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta84, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[84] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta85, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[85] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta86, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[86] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta87, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[87] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta88, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[88] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta89, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[89] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta90, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[90] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta91, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[91] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta92, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[92] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta93, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[93] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta94, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[94] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta95, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[95] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta96, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[96] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta97, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[97] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta98, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[98] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta99, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[99] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta100, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[100] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta101, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[101] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta102, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[102] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta103, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[103] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta104, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[104] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta105, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[105] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta106, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[106] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta107, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[107] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta108, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[108] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta109, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[109] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta110, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[110] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta111, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[111] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta112, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[112] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta113, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[113] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta114, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[114] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta115, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[115] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta116, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[116] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta117, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[117] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta118, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[118] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta119, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[119] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta120, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[120] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta121, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[121] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta122, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[122] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta123, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[123] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta124, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[124] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta125, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[125] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta126, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[126] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta127, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[127] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta128, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[128] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta129, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[129] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta130, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[130] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta131, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[131] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta132, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[132] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta133, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[133] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta134, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[134] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta135, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[135] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta136, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[136] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta137, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[137] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta138, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[138] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta139, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[139] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta140, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[140] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta141, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[141] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta142, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[142] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta143, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[143] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta144, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[144] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta145, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[145] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta146, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[146] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta147, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[147] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta148, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[148] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta149, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[149] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta150, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[150] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta151, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[151] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta152, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[152] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta153, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[153] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta154, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[154] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta155, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[155] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta156, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[156] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta157, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[157] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta158, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[158] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta159, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[159] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta160, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[160] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta161, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[161] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta162, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[162] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta163, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[163] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta164, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[164] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta165, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[165] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta166, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[166] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta167, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[167] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta168, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[168] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta169, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[169] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta170, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[170] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta171, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[171] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta172, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[172] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta173, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[173] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta174, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[174] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta175, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[175] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta176, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[176] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta177, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[177] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta178, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[178] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta179, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[179] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta180, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[180] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta181, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[181] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta182, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[182] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta183, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[183] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta184, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[184] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta185, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[185] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta186, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[186] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta187, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[187] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta188, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[188] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta189, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[189] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta190, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[190] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta191, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[191] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta192, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[192] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta193, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[193] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta194, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[194] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta195, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[195] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta196, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[196] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta197, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[197] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta198, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[198] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta199, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[199] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta200, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[200] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta201, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[201] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta202, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[202] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta203, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[203] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta204, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[204] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta205, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[205] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta206, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[206] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta207, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[207] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta208, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[208] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta209, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[209] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta210, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[210] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta211, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[211] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta212, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[212] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta213, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[213] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta214, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[214] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta215, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[215] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta216, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[216] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta217, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[217] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta218, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[218] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta219, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[219] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta220, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[220] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta221, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[221] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta222, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[222] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta223, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[223] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta224, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[224] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta225, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[225] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta226, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[226] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta227, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[227] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta228, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[228] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta229, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[229] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta230, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[230] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta231, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[231] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta232, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[232] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta233, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[233] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta234, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[234] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta235, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[235] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta236, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[236] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta237, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[237] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta238, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[238] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta239, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[239] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta240, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[240] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta241, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[241] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta242, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[242] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta243, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[243] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta244, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[244] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta245, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[245] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta246, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[246] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta247, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[247] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta248, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[248] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta249, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[249] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta250, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[250] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta251, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[251] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta252, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[252] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta253, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[253] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta254, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[254] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta255, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[255] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta256, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[256] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta257, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[257] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta258, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[258] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta259, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[259] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta260, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[260] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta261, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[261] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta262, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[262] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta263, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[263] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta264, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[264] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta265, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[265] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta266, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[266] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta267, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[267] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta268, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[268] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta269, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[269] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta270, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[270] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta271, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[271] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta272, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[272] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta273, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[273] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta274, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[274] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta275, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[275] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta276, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[276] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta277, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[277] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta278, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[278] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta279, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[279] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta280, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[280] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta281, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[281] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta282, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[282] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta283, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[283] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta284, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[284] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta285, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[285] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta286, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[286] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta287, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[287] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta288, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[288] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta289, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[289] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta290, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[290] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta291, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[291] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta292, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[292] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta293, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[293] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta294, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[294] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta295, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[295] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta296, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[296] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta297, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[297] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta298, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[298] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta299, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[299] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta300, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[300] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta301, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[301] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta302, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[302] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta303, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[303] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta304, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[304] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta305, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[305] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta306, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[306] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta307, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[307] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta308, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[308] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta309, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[309] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta310, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[310] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta311, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[311] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta312, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[312] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta313, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[313] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta314, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[314] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta315, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[315] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta316, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[316] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta317, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[317] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta318, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[318] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta319, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[319] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta320, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[320] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta321, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[321] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta322, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[322] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta323, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[323] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta324, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[324] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta325, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[325] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta326, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[326] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta327, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[327] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta328, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[328] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta329, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[329] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta330, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[330] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta331, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[331] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta332, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[332] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta333, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[333] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta334, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[334] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta335, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[335] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta336, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[336] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta337, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[337] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta338, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[338] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta339, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[339] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta340, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[340] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta341, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[341] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta342, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[342] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta343, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[343] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta344, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[344] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta345, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[345] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta346, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[346] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta347, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[347] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta348, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[348] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta349, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[349] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta350, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[350] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta351, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[351] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta352, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[352] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta353, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[353] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta354, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[354] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta355, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[355] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta356, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[356] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta357, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[357] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta358, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[358] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta359, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[359] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta360, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[360] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta361, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[361] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta362, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[362] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta363, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[363] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta364, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[364] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta365, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[365] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta366, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[366] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta367, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[367] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta368, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[368] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta369, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[369] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta370, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[370] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta371, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[371] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta372, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[372] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta373, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[373] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta374, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[374] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta375, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[375] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta376, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[376] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta377, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[377] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta378, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[378] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta379, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[379] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta380, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[380] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta381, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[381] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta382, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[382] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta383, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[383] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta384, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[384] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta385, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[385] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta386, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[386] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta387, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[387] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta388, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[388] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta389, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[389] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta390, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[390] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta391, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[391] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta392, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[392] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta393, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[393] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta394, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[394] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta395, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[395] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta396, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[396] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta397, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[397] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta398, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[398] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta399, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[399] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta400, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[400] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta401, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[401] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta402, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[402] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta403, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[403] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta404, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[404] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta405, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[405] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta406, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[406] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta407, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[407] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta408, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[408] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta409, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[409] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta410, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[410] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta411, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[411] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta412, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[412] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta413, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[413] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta414, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[414] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta415, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[415] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta416, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[416] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta417, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[417] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta418, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[418] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta419, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[419] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta420, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[420] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta421, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[421] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta422, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[422] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta423, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[423] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta424, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[424] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta425, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[425] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta426, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[426] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta427, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[427] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta428, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[428] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta429, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[429] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta430, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[430] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta431, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[431] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta432, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[432] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta433, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[433] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta434, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[434] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta435, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[435] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta436, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[436] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta437, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[437] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta438, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[438] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta439, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[439] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta440, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[440] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta441, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[441] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta442, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[442] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta443, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[443] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta444, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[444] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta445, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[445] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta446, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[446] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta447, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[447] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta448, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[448] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta449, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[449] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta450, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[450] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta451, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[451] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta452, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[452] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta453, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[453] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta454, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[454] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta455, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[455] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta456, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[456] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta457, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[457] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta458, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[458] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta459, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[459] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta460, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[460] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta461, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[461] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta462, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[462] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta463, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[463] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta464, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[464] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta465, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[465] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta466, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[466] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta467, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[467] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta468, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[468] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta469, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[469] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta470, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[470] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta471, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[471] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta472, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[472] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta473, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[473] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta474, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[474] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta475, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[475] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta476, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[476] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta477, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[477] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta478, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[478] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta479, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[479] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta480, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[480] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta481, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[481] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta482, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[482] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta483, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[483] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta484, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[484] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta485, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[485] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta486, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[486] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta487, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[487] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta488, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[488] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta489, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[489] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta490, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[490] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta491, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[491] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta492, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[492] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta493, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[493] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta494, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[494] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta495, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[495] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta496, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[496] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta497, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[497] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta498, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[498] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta499, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[499] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta500, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[500] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta501, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[501] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta502, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[502] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta503, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[503] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta504, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[504] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta505, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[505] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta506, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[506] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta507, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[507] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta508, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[508] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta509, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[509] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta510, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[510] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta511, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[511] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta512, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[512] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta513, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[513] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta514, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[514] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta515, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[515] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta516, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[516] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta517, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[517] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta518, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[518] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta519, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[519] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta520, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[520] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta521, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[521] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta522, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[522] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta523, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[523] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta524, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[524] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta525, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[525] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta526, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[526] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta527, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[527] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta528, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[528] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta529, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[529] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta530, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[530] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta531, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[531] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta532, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[532] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta533, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[533] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta534, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[534] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta535, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[535] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta536, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[536] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta537, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[537] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta538, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[538] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta539, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[539] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta540, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[540] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta541, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[541] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta542, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[542] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta543, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[543] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta544, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[544] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta545, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[545] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta546, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[546] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta547, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[547] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta548, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[548] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta549, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[549] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta550, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[550] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta551, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[551] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta552, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[552] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta553, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[553] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta554, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[554] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta555, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[555] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta556, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[556] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta557, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[557] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta558, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[558] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta559, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[559] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta560, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[560] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta561, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[561] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta562, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[562] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta563, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[563] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta564, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[564] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta565, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[565] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta566, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[566] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta567, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[567] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta568, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[568] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta569, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[569] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta570, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[570] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta571, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[571] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta572, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[572] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta573, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[573] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta574, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[574] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta575, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[575] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta576, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[576] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta577, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[577] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta578, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[578] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta579, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[579] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta580, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[580] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta581, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[581] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta582, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[582] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta583, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[583] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta584, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[584] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta585, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[585] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta586, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[586] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta587, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[587] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta588, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[588] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta589, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[589] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta590, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[590] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta591, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[591] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta592, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[592] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta593, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[593] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta594, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[594] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta595, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[595] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta596, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[596] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta597, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[597] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta598, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[598] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta599, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[599] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta600, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[600] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta601, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[601] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta602, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[602] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta603, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[603] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta604, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[604] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta605, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[605] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta606, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[606] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta607, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[607] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta608, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[608] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta609, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[609] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta610, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[610] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta611, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[611] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta612, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[612] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta613, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[613] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta614, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[614] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta615, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[615] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta616, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[616] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta617, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[617] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta618, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[618] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta619, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[619] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta620, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[620] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta621, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[621] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta622, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[622] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta623, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[623] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta624, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[624] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta625, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[625] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta626, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[626] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta627, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[627] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta628, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[628] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta629, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[629] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta630, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[630] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta631, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[631] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta632, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[632] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta633, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[633] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta634, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[634] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta635, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[635] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta636, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[636] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta637, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[637] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta638, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[638] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta639, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[639] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta640, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[640] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta641, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[641] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta642, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[642] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta643, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[643] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta644, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[644] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta645, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[645] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta646, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[646] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta647, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[647] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta648, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[648] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta649, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[649] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta650, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[650] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta651, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[651] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta652, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[652] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta653, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[653] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta654, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[654] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta655, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[655] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta656, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[656] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta657, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[657] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta658, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[658] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta659, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[659] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta660, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[660] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta661, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[661] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta662, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[662] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta663, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[663] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta664, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[664] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta665, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[665] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta666, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[666] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta667, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[667] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta668, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[668] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta669, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[669] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta670, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[670] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta671, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[671] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta672, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[672] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta673, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[673] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta674, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[674] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta675, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[675] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta676, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[676] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta677, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[677] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta678, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[678] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta679, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[679] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta680, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[680] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta681, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[681] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta682, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[682] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta683, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[683] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta684, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[684] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta685, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[685] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta686, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[686] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta687, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[687] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta688, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[688] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta689, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[689] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta690, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[690] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta691, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[691] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta692, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[692] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta693, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[693] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta694, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[694] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta695, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[695] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta696, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[696] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta697, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[697] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta698, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[698] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta699, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[699] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta700, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[700] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta701, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[701] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta702, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[702] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta703, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[703] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta704, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[704] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta705, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[705] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta706, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[706] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta707, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[707] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta708, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[708] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta709, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[709] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta710, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[710] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta711, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[711] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta712, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[712] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta713, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[713] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta714, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[714] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta715, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[715] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta716, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[716] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta717, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[717] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta718, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[718] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta719, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[719] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta720, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[720] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta721, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[721] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta722, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[722] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta723, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[723] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta724, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[724] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta725, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[725] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta726, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[726] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta727, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[727] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta728, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[728] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta729, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[729] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta730, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[730] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta731, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[731] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta732, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[732] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta733, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[733] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta734, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[734] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta735, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[735] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta736, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[736] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta737, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[737] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta738, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[738] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta739, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[739] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta740, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[740] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta741, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[741] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta742, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[742] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta743, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[743] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta744, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[744] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta745, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[745] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta746, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[746] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta747, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[747] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta748, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[748] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta749, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[749] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta750, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[750] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta751, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[751] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta752, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[752] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta753, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[753] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta754, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[754] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta755, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[755] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta756, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[756] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta757, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[757] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta758, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[758] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta759, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[759] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta760, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[760] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta761, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[761] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta762, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[762] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta763, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[763] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta764, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[764] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta765, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[765] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta766, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[766] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta767, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[767] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta768, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[768] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta769, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[769] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta770, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[770] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta771, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[771] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta772, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[772] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta773, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[773] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta774, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[774] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta775, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[775] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta776, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[776] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta777, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[777] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta778, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[778] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta779, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[779] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta780, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[780] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta781, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[781] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta782, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[782] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta783, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[783] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta784, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[784] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta785, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[785] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta786, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[786] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta787, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[787] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta788, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[788] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta789, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[789] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta790, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[790] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta791, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[791] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta792, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[792] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta793, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[793] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta794, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[794] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta795, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[795] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta796, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[796] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta797, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[797] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta798, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[798] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta799, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[799] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta800, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[800] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta801, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[801] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta802, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[802] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta803, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[803] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta804, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[804] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta805, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[805] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta806, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[806] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta807, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[807] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta808, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[808] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta809, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[809] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta810, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[810] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta811, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[811] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta812, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[812] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta813, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[813] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta814, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[814] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta815, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[815] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta816, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[816] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta817, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[817] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta818, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[818] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta819, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[819] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta820, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[820] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta821, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[821] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta822, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[822] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta823, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[823] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta824, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[824] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta825, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[825] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta826, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[826] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta827, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[827] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta828, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[828] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta829, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[829] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta830, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[830] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta831, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[831] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta832, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[832] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta833, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[833] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta834, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[834] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta835, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[835] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta836, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[836] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta837, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[837] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta838, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[838] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta839, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[839] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta840, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[840] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta841, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[841] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta842, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[842] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta843, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[843] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta844, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[844] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta845, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[845] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta846, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[846] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta847, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[847] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta848, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[848] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta849, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[849] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta850, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[850] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta851, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[851] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta852, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[852] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta853, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[853] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta854, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[854] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta855, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[855] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta856, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[856] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta857, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[857] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta858, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[858] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta859, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[859] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta860, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[860] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta861, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[861] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta862, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[862] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta863, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[863] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta864, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[864] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta865, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[865] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta866, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[866] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta867, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[867] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta868, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[868] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta869, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[869] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta870, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[870] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta871, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[871] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta872, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[872] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta873, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[873] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta874, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[874] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta875, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[875] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta876, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[876] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta877, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[877] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta878, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[878] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta879, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[879] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta880, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[880] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta881, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[881] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta882, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[882] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta883, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[883] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta884, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[884] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta885, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[885] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta886, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[886] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta887, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[887] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta888, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[888] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta889, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[889] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta890, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[890] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta891, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[891] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta892, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[892] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta893, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[893] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta894, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[894] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta895, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[895] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta896, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[896] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta897, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[897] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta898, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[898] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta899, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[899] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta900, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[900] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta901, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[901] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta902, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[902] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta903, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[903] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta904, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[904] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta905, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[905] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta906, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[906] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta907, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[907] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta908, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[908] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta909, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[909] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta910, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[910] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta911, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[911] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta912, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[912] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta913, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[913] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta914, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[914] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta915, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[915] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta916, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[916] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta917, -1};
TriangleTriangle_101yy_z__z_z_zSurfaceItaArray[917] = {TriangleTriangle_101yy_z__z_z_zSurfaceIta918, -1};
return TriangleTriangle_101yy_z__z_z_zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,918> TriangleTriangle_101yy_z__z_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,918> TriangleTriangle_101yy_z__z_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,918> TriangleTriangle_101yy_z__z_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,918> TriangleTriangle_101yy_z__z_z_zSurfaceIta<F32>();

#endif

}}}}

