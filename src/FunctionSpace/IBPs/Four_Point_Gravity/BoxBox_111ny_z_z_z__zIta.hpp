/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][9];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[1][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta77(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta78(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta79(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta80(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta81(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta82(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta83(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta84(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta85(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta86(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta87(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta88(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta89(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta90(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta91(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta92(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta93(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta94(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta95(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta96(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta97(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta98(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta99(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta100(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta101(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta102(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta103(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta104(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta105(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta106(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta107(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta108(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta109(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta110(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta111(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta112(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta113(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta114(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta115(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta116(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta117(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta118(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta119(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta120(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta121(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta122(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta123(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta124(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta125(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta126(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta127(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta128(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta129(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta130(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta131(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta132(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta133(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta134(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta135(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta136(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta137(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 3) * z[2] * z[6];
z[9] = z[9] + z[10] * T(4);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta138(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta139(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta140(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta141(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta142(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta143(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta144(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta145(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta146(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta147(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta148(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta149(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta150(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta151(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta152(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta153(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta154(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta155(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta156(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta157(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta158(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta159(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta160(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta161(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta162(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta163(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta164(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta165(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta166(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta167(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[0], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta168(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta169(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta170(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta171(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta172(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta173(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta174(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta175(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta176(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * T(4);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta177(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta178(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta179(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta180(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta181(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[0], 2) * z[1] * z[8];
z[9] = prod_pow(z[0], 3) * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta182(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta183(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[5] * z[9];
z[8] = z[8] + z[10];
z[8] = prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3) * z[9];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta184(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta185(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta186(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta187(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta188(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta189(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta190(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta191(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta192(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta193(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta194(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[1], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta195(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 6) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta196(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta197(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta198(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 2) * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta199(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta200(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta201(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta202(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta203(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta204(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta205(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta206(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta207(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta208(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta209(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta210(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta211(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[0], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta212(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta213(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 6) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta214(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta215(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta216(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta217(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta218(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = z[1] * prod_pow(z[2], 4) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta219(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 5) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(5);
z[7] = z[0] * prod_pow(z[1], 4) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 5)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta220(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(4);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta221(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta222(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta223(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(4);
z[8] = prod_pow(z[0], 3) * z[2] * z[8];
z[9] = prod_pow(z[0], 4) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta224(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta225(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[2] * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta226(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 3) * prod_pow(z[2], 2) * z[5];
z[9] = prod_pow(z[0], 2) * prod_pow(z[2], 3) * z[7];
z[10] = z[0] * z[2];
z[10] = prod_pow(z[10], 3);
z[11] = z[3] * z[10];
z[8] = z[8] + z[9] + z[11];
z[9] = z[4] * z[10];
z[8] = z[9] + z[8] * T(3);
z[8] = z[1] * z[8];
z[9] = z[6] * z[10];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta227(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[1];
z[8] = prod_pow(z[8], 3);
z[9] = z[3] * z[8];
z[10] = prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[6];
z[11] = prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[7];
z[9] = z[9] + z[10] + z[11];
z[10] = z[4] * z[8];
z[9] = z[10] + z[9] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta228(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2) * z[7];
z[9] = prod_pow(z[0], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[1] * z[8];
z[10] = z[6] * z[9];
z[8] = z[8] + z[10] * T(2);
z[8] = z[1] * z[2] * z[8];
z[9] = prod_pow(z[1], 2) * z[5] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta229(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[0], 2) * z[2] * z[8];
z[9] = prod_pow(z[0], 3) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta230(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta231(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 6) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta232(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 3);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(3);
z[10] = z[0] * prod_pow(z[2], 2) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[1] * z[9];
z[8] = prod_pow(z[0], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta233(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[0] * prod_pow(z[2], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[1] * z[9];
z[8] = prod_pow(z[0], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta234(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta235(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 6) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta236(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta237(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta238(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[1], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta239(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 6) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta240(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta241(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 3) * z[6];
z[9] = z[9] + z[10] * T(4);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta242(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[1], 2) * z[2] * z[8];
z[9] = z[0] * prod_pow(z[1], 3) * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta243(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta244(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(8);
zd[0] = prod_pow(z[2], 7) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 8) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta245(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta246(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta247(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta248(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 5) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(5);
z[7] = z[0] * prod_pow(z[1], 4) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 5)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta249(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta250(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta251(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta252(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][9];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[2][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta253(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta254(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta255(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta256(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta257(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta258(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta259(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta260(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta261(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta262(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta263(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta264(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta265(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta266(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta267(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta268(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta269(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta270(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta271(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta272(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta273(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta274(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta275(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta276(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta277(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta278(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta279(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta280(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta281(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta282(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta283(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta284(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta285(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta286(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta287(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta288(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta289(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta290(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta291(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta292(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta293(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta294(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta295(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta296(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta297(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 8) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta298(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[0], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta299(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta300(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[2] * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta301(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[2] * T(4);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta302(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[0], 2) * z[1] * z[8];
z[9] = prod_pow(z[0], 3) * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta303(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta304(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[1], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta305(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 8) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta306(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 8) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta307(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[0], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta308(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(6);
zd[0] = prod_pow(z[0], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 6) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta309(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta310(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(4);
z[9] = prod_pow(z[1], 4);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 3) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta311(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[2] * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta312(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(6);
zd[0] = prod_pow(z[1], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 6) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta313(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[1], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta314(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][9];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][9];
zd[1] = -((prod_pow(z[0], 8) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta315(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][9];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[3][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta316(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta317(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta318(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta319(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta320(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta321(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta322(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta323(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta324(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta325(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][9];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[4][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111ny_z_z_z__zSurfaceIta326(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][9];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][9];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,326> BoxBox_111ny_z_z_z__zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,326> BoxBox_111ny_z_z_z__zSurfaceItaArray;
BoxBox_111ny_z_z_z__zSurfaceItaArray[0] = {BoxBox_111ny_z_z_z__zSurfaceIta1, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[1] = {BoxBox_111ny_z_z_z__zSurfaceIta2, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[2] = {BoxBox_111ny_z_z_z__zSurfaceIta3, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[3] = {BoxBox_111ny_z_z_z__zSurfaceIta4, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[4] = {BoxBox_111ny_z_z_z__zSurfaceIta5, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[5] = {BoxBox_111ny_z_z_z__zSurfaceIta6, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[6] = {BoxBox_111ny_z_z_z__zSurfaceIta7, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[7] = {BoxBox_111ny_z_z_z__zSurfaceIta8, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[8] = {BoxBox_111ny_z_z_z__zSurfaceIta9, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[9] = {BoxBox_111ny_z_z_z__zSurfaceIta10, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[10] = {BoxBox_111ny_z_z_z__zSurfaceIta11, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[11] = {BoxBox_111ny_z_z_z__zSurfaceIta12, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[12] = {BoxBox_111ny_z_z_z__zSurfaceIta13, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[13] = {BoxBox_111ny_z_z_z__zSurfaceIta14, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[14] = {BoxBox_111ny_z_z_z__zSurfaceIta15, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[15] = {BoxBox_111ny_z_z_z__zSurfaceIta16, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[16] = {BoxBox_111ny_z_z_z__zSurfaceIta17, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[17] = {BoxBox_111ny_z_z_z__zSurfaceIta18, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[18] = {BoxBox_111ny_z_z_z__zSurfaceIta19, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[19] = {BoxBox_111ny_z_z_z__zSurfaceIta20, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[20] = {BoxBox_111ny_z_z_z__zSurfaceIta21, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[21] = {BoxBox_111ny_z_z_z__zSurfaceIta22, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[22] = {BoxBox_111ny_z_z_z__zSurfaceIta23, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[23] = {BoxBox_111ny_z_z_z__zSurfaceIta24, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[24] = {BoxBox_111ny_z_z_z__zSurfaceIta25, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[25] = {BoxBox_111ny_z_z_z__zSurfaceIta26, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[26] = {BoxBox_111ny_z_z_z__zSurfaceIta27, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[27] = {BoxBox_111ny_z_z_z__zSurfaceIta28, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[28] = {BoxBox_111ny_z_z_z__zSurfaceIta29, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[29] = {BoxBox_111ny_z_z_z__zSurfaceIta30, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[30] = {BoxBox_111ny_z_z_z__zSurfaceIta31, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[31] = {BoxBox_111ny_z_z_z__zSurfaceIta32, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[32] = {BoxBox_111ny_z_z_z__zSurfaceIta33, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[33] = {BoxBox_111ny_z_z_z__zSurfaceIta34, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[34] = {BoxBox_111ny_z_z_z__zSurfaceIta35, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[35] = {BoxBox_111ny_z_z_z__zSurfaceIta36, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[36] = {BoxBox_111ny_z_z_z__zSurfaceIta37, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[37] = {BoxBox_111ny_z_z_z__zSurfaceIta38, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[38] = {BoxBox_111ny_z_z_z__zSurfaceIta39, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[39] = {BoxBox_111ny_z_z_z__zSurfaceIta40, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[40] = {BoxBox_111ny_z_z_z__zSurfaceIta41, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[41] = {BoxBox_111ny_z_z_z__zSurfaceIta42, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[42] = {BoxBox_111ny_z_z_z__zSurfaceIta43, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[43] = {BoxBox_111ny_z_z_z__zSurfaceIta44, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[44] = {BoxBox_111ny_z_z_z__zSurfaceIta45, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[45] = {BoxBox_111ny_z_z_z__zSurfaceIta46, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[46] = {BoxBox_111ny_z_z_z__zSurfaceIta47, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[47] = {BoxBox_111ny_z_z_z__zSurfaceIta48, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[48] = {BoxBox_111ny_z_z_z__zSurfaceIta49, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[49] = {BoxBox_111ny_z_z_z__zSurfaceIta50, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[50] = {BoxBox_111ny_z_z_z__zSurfaceIta51, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[51] = {BoxBox_111ny_z_z_z__zSurfaceIta52, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[52] = {BoxBox_111ny_z_z_z__zSurfaceIta53, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[53] = {BoxBox_111ny_z_z_z__zSurfaceIta54, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[54] = {BoxBox_111ny_z_z_z__zSurfaceIta55, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[55] = {BoxBox_111ny_z_z_z__zSurfaceIta56, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[56] = {BoxBox_111ny_z_z_z__zSurfaceIta57, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[57] = {BoxBox_111ny_z_z_z__zSurfaceIta58, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[58] = {BoxBox_111ny_z_z_z__zSurfaceIta59, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[59] = {BoxBox_111ny_z_z_z__zSurfaceIta60, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[60] = {BoxBox_111ny_z_z_z__zSurfaceIta61, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[61] = {BoxBox_111ny_z_z_z__zSurfaceIta62, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[62] = {BoxBox_111ny_z_z_z__zSurfaceIta63, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[63] = {BoxBox_111ny_z_z_z__zSurfaceIta64, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[64] = {BoxBox_111ny_z_z_z__zSurfaceIta65, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[65] = {BoxBox_111ny_z_z_z__zSurfaceIta66, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[66] = {BoxBox_111ny_z_z_z__zSurfaceIta67, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[67] = {BoxBox_111ny_z_z_z__zSurfaceIta68, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[68] = {BoxBox_111ny_z_z_z__zSurfaceIta69, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[69] = {BoxBox_111ny_z_z_z__zSurfaceIta70, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[70] = {BoxBox_111ny_z_z_z__zSurfaceIta71, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[71] = {BoxBox_111ny_z_z_z__zSurfaceIta72, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[72] = {BoxBox_111ny_z_z_z__zSurfaceIta73, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[73] = {BoxBox_111ny_z_z_z__zSurfaceIta74, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[74] = {BoxBox_111ny_z_z_z__zSurfaceIta75, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[75] = {BoxBox_111ny_z_z_z__zSurfaceIta76, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[76] = {BoxBox_111ny_z_z_z__zSurfaceIta77, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[77] = {BoxBox_111ny_z_z_z__zSurfaceIta78, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[78] = {BoxBox_111ny_z_z_z__zSurfaceIta79, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[79] = {BoxBox_111ny_z_z_z__zSurfaceIta80, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[80] = {BoxBox_111ny_z_z_z__zSurfaceIta81, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[81] = {BoxBox_111ny_z_z_z__zSurfaceIta82, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[82] = {BoxBox_111ny_z_z_z__zSurfaceIta83, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[83] = {BoxBox_111ny_z_z_z__zSurfaceIta84, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[84] = {BoxBox_111ny_z_z_z__zSurfaceIta85, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[85] = {BoxBox_111ny_z_z_z__zSurfaceIta86, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[86] = {BoxBox_111ny_z_z_z__zSurfaceIta87, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[87] = {BoxBox_111ny_z_z_z__zSurfaceIta88, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[88] = {BoxBox_111ny_z_z_z__zSurfaceIta89, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[89] = {BoxBox_111ny_z_z_z__zSurfaceIta90, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[90] = {BoxBox_111ny_z_z_z__zSurfaceIta91, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[91] = {BoxBox_111ny_z_z_z__zSurfaceIta92, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[92] = {BoxBox_111ny_z_z_z__zSurfaceIta93, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[93] = {BoxBox_111ny_z_z_z__zSurfaceIta94, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[94] = {BoxBox_111ny_z_z_z__zSurfaceIta95, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[95] = {BoxBox_111ny_z_z_z__zSurfaceIta96, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[96] = {BoxBox_111ny_z_z_z__zSurfaceIta97, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[97] = {BoxBox_111ny_z_z_z__zSurfaceIta98, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[98] = {BoxBox_111ny_z_z_z__zSurfaceIta99, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[99] = {BoxBox_111ny_z_z_z__zSurfaceIta100, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[100] = {BoxBox_111ny_z_z_z__zSurfaceIta101, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[101] = {BoxBox_111ny_z_z_z__zSurfaceIta102, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[102] = {BoxBox_111ny_z_z_z__zSurfaceIta103, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[103] = {BoxBox_111ny_z_z_z__zSurfaceIta104, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[104] = {BoxBox_111ny_z_z_z__zSurfaceIta105, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[105] = {BoxBox_111ny_z_z_z__zSurfaceIta106, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[106] = {BoxBox_111ny_z_z_z__zSurfaceIta107, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[107] = {BoxBox_111ny_z_z_z__zSurfaceIta108, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[108] = {BoxBox_111ny_z_z_z__zSurfaceIta109, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[109] = {BoxBox_111ny_z_z_z__zSurfaceIta110, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[110] = {BoxBox_111ny_z_z_z__zSurfaceIta111, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[111] = {BoxBox_111ny_z_z_z__zSurfaceIta112, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[112] = {BoxBox_111ny_z_z_z__zSurfaceIta113, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[113] = {BoxBox_111ny_z_z_z__zSurfaceIta114, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[114] = {BoxBox_111ny_z_z_z__zSurfaceIta115, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[115] = {BoxBox_111ny_z_z_z__zSurfaceIta116, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[116] = {BoxBox_111ny_z_z_z__zSurfaceIta117, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[117] = {BoxBox_111ny_z_z_z__zSurfaceIta118, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[118] = {BoxBox_111ny_z_z_z__zSurfaceIta119, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[119] = {BoxBox_111ny_z_z_z__zSurfaceIta120, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[120] = {BoxBox_111ny_z_z_z__zSurfaceIta121, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[121] = {BoxBox_111ny_z_z_z__zSurfaceIta122, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[122] = {BoxBox_111ny_z_z_z__zSurfaceIta123, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[123] = {BoxBox_111ny_z_z_z__zSurfaceIta124, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[124] = {BoxBox_111ny_z_z_z__zSurfaceIta125, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[125] = {BoxBox_111ny_z_z_z__zSurfaceIta126, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[126] = {BoxBox_111ny_z_z_z__zSurfaceIta127, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[127] = {BoxBox_111ny_z_z_z__zSurfaceIta128, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[128] = {BoxBox_111ny_z_z_z__zSurfaceIta129, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[129] = {BoxBox_111ny_z_z_z__zSurfaceIta130, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[130] = {BoxBox_111ny_z_z_z__zSurfaceIta131, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[131] = {BoxBox_111ny_z_z_z__zSurfaceIta132, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[132] = {BoxBox_111ny_z_z_z__zSurfaceIta133, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[133] = {BoxBox_111ny_z_z_z__zSurfaceIta134, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[134] = {BoxBox_111ny_z_z_z__zSurfaceIta135, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[135] = {BoxBox_111ny_z_z_z__zSurfaceIta136, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[136] = {BoxBox_111ny_z_z_z__zSurfaceIta137, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[137] = {BoxBox_111ny_z_z_z__zSurfaceIta138, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[138] = {BoxBox_111ny_z_z_z__zSurfaceIta139, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[139] = {BoxBox_111ny_z_z_z__zSurfaceIta140, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[140] = {BoxBox_111ny_z_z_z__zSurfaceIta141, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[141] = {BoxBox_111ny_z_z_z__zSurfaceIta142, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[142] = {BoxBox_111ny_z_z_z__zSurfaceIta143, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[143] = {BoxBox_111ny_z_z_z__zSurfaceIta144, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[144] = {BoxBox_111ny_z_z_z__zSurfaceIta145, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[145] = {BoxBox_111ny_z_z_z__zSurfaceIta146, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[146] = {BoxBox_111ny_z_z_z__zSurfaceIta147, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[147] = {BoxBox_111ny_z_z_z__zSurfaceIta148, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[148] = {BoxBox_111ny_z_z_z__zSurfaceIta149, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[149] = {BoxBox_111ny_z_z_z__zSurfaceIta150, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[150] = {BoxBox_111ny_z_z_z__zSurfaceIta151, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[151] = {BoxBox_111ny_z_z_z__zSurfaceIta152, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[152] = {BoxBox_111ny_z_z_z__zSurfaceIta153, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[153] = {BoxBox_111ny_z_z_z__zSurfaceIta154, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[154] = {BoxBox_111ny_z_z_z__zSurfaceIta155, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[155] = {BoxBox_111ny_z_z_z__zSurfaceIta156, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[156] = {BoxBox_111ny_z_z_z__zSurfaceIta157, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[157] = {BoxBox_111ny_z_z_z__zSurfaceIta158, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[158] = {BoxBox_111ny_z_z_z__zSurfaceIta159, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[159] = {BoxBox_111ny_z_z_z__zSurfaceIta160, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[160] = {BoxBox_111ny_z_z_z__zSurfaceIta161, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[161] = {BoxBox_111ny_z_z_z__zSurfaceIta162, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[162] = {BoxBox_111ny_z_z_z__zSurfaceIta163, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[163] = {BoxBox_111ny_z_z_z__zSurfaceIta164, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[164] = {BoxBox_111ny_z_z_z__zSurfaceIta165, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[165] = {BoxBox_111ny_z_z_z__zSurfaceIta166, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[166] = {BoxBox_111ny_z_z_z__zSurfaceIta167, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[167] = {BoxBox_111ny_z_z_z__zSurfaceIta168, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[168] = {BoxBox_111ny_z_z_z__zSurfaceIta169, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[169] = {BoxBox_111ny_z_z_z__zSurfaceIta170, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[170] = {BoxBox_111ny_z_z_z__zSurfaceIta171, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[171] = {BoxBox_111ny_z_z_z__zSurfaceIta172, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[172] = {BoxBox_111ny_z_z_z__zSurfaceIta173, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[173] = {BoxBox_111ny_z_z_z__zSurfaceIta174, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[174] = {BoxBox_111ny_z_z_z__zSurfaceIta175, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[175] = {BoxBox_111ny_z_z_z__zSurfaceIta176, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[176] = {BoxBox_111ny_z_z_z__zSurfaceIta177, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[177] = {BoxBox_111ny_z_z_z__zSurfaceIta178, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[178] = {BoxBox_111ny_z_z_z__zSurfaceIta179, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[179] = {BoxBox_111ny_z_z_z__zSurfaceIta180, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[180] = {BoxBox_111ny_z_z_z__zSurfaceIta181, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[181] = {BoxBox_111ny_z_z_z__zSurfaceIta182, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[182] = {BoxBox_111ny_z_z_z__zSurfaceIta183, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[183] = {BoxBox_111ny_z_z_z__zSurfaceIta184, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[184] = {BoxBox_111ny_z_z_z__zSurfaceIta185, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[185] = {BoxBox_111ny_z_z_z__zSurfaceIta186, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[186] = {BoxBox_111ny_z_z_z__zSurfaceIta187, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[187] = {BoxBox_111ny_z_z_z__zSurfaceIta188, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[188] = {BoxBox_111ny_z_z_z__zSurfaceIta189, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[189] = {BoxBox_111ny_z_z_z__zSurfaceIta190, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[190] = {BoxBox_111ny_z_z_z__zSurfaceIta191, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[191] = {BoxBox_111ny_z_z_z__zSurfaceIta192, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[192] = {BoxBox_111ny_z_z_z__zSurfaceIta193, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[193] = {BoxBox_111ny_z_z_z__zSurfaceIta194, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[194] = {BoxBox_111ny_z_z_z__zSurfaceIta195, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[195] = {BoxBox_111ny_z_z_z__zSurfaceIta196, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[196] = {BoxBox_111ny_z_z_z__zSurfaceIta197, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[197] = {BoxBox_111ny_z_z_z__zSurfaceIta198, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[198] = {BoxBox_111ny_z_z_z__zSurfaceIta199, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[199] = {BoxBox_111ny_z_z_z__zSurfaceIta200, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[200] = {BoxBox_111ny_z_z_z__zSurfaceIta201, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[201] = {BoxBox_111ny_z_z_z__zSurfaceIta202, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[202] = {BoxBox_111ny_z_z_z__zSurfaceIta203, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[203] = {BoxBox_111ny_z_z_z__zSurfaceIta204, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[204] = {BoxBox_111ny_z_z_z__zSurfaceIta205, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[205] = {BoxBox_111ny_z_z_z__zSurfaceIta206, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[206] = {BoxBox_111ny_z_z_z__zSurfaceIta207, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[207] = {BoxBox_111ny_z_z_z__zSurfaceIta208, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[208] = {BoxBox_111ny_z_z_z__zSurfaceIta209, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[209] = {BoxBox_111ny_z_z_z__zSurfaceIta210, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[210] = {BoxBox_111ny_z_z_z__zSurfaceIta211, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[211] = {BoxBox_111ny_z_z_z__zSurfaceIta212, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[212] = {BoxBox_111ny_z_z_z__zSurfaceIta213, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[213] = {BoxBox_111ny_z_z_z__zSurfaceIta214, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[214] = {BoxBox_111ny_z_z_z__zSurfaceIta215, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[215] = {BoxBox_111ny_z_z_z__zSurfaceIta216, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[216] = {BoxBox_111ny_z_z_z__zSurfaceIta217, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[217] = {BoxBox_111ny_z_z_z__zSurfaceIta218, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[218] = {BoxBox_111ny_z_z_z__zSurfaceIta219, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[219] = {BoxBox_111ny_z_z_z__zSurfaceIta220, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[220] = {BoxBox_111ny_z_z_z__zSurfaceIta221, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[221] = {BoxBox_111ny_z_z_z__zSurfaceIta222, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[222] = {BoxBox_111ny_z_z_z__zSurfaceIta223, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[223] = {BoxBox_111ny_z_z_z__zSurfaceIta224, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[224] = {BoxBox_111ny_z_z_z__zSurfaceIta225, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[225] = {BoxBox_111ny_z_z_z__zSurfaceIta226, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[226] = {BoxBox_111ny_z_z_z__zSurfaceIta227, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[227] = {BoxBox_111ny_z_z_z__zSurfaceIta228, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[228] = {BoxBox_111ny_z_z_z__zSurfaceIta229, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[229] = {BoxBox_111ny_z_z_z__zSurfaceIta230, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[230] = {BoxBox_111ny_z_z_z__zSurfaceIta231, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[231] = {BoxBox_111ny_z_z_z__zSurfaceIta232, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[232] = {BoxBox_111ny_z_z_z__zSurfaceIta233, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[233] = {BoxBox_111ny_z_z_z__zSurfaceIta234, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[234] = {BoxBox_111ny_z_z_z__zSurfaceIta235, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[235] = {BoxBox_111ny_z_z_z__zSurfaceIta236, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[236] = {BoxBox_111ny_z_z_z__zSurfaceIta237, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[237] = {BoxBox_111ny_z_z_z__zSurfaceIta238, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[238] = {BoxBox_111ny_z_z_z__zSurfaceIta239, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[239] = {BoxBox_111ny_z_z_z__zSurfaceIta240, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[240] = {BoxBox_111ny_z_z_z__zSurfaceIta241, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[241] = {BoxBox_111ny_z_z_z__zSurfaceIta242, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[242] = {BoxBox_111ny_z_z_z__zSurfaceIta243, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[243] = {BoxBox_111ny_z_z_z__zSurfaceIta244, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[244] = {BoxBox_111ny_z_z_z__zSurfaceIta245, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[245] = {BoxBox_111ny_z_z_z__zSurfaceIta246, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[246] = {BoxBox_111ny_z_z_z__zSurfaceIta247, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[247] = {BoxBox_111ny_z_z_z__zSurfaceIta248, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[248] = {BoxBox_111ny_z_z_z__zSurfaceIta249, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[249] = {BoxBox_111ny_z_z_z__zSurfaceIta250, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[250] = {BoxBox_111ny_z_z_z__zSurfaceIta251, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[251] = {BoxBox_111ny_z_z_z__zSurfaceIta252, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[252] = {BoxBox_111ny_z_z_z__zSurfaceIta253, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[253] = {BoxBox_111ny_z_z_z__zSurfaceIta254, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[254] = {BoxBox_111ny_z_z_z__zSurfaceIta255, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[255] = {BoxBox_111ny_z_z_z__zSurfaceIta256, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[256] = {BoxBox_111ny_z_z_z__zSurfaceIta257, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[257] = {BoxBox_111ny_z_z_z__zSurfaceIta258, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[258] = {BoxBox_111ny_z_z_z__zSurfaceIta259, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[259] = {BoxBox_111ny_z_z_z__zSurfaceIta260, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[260] = {BoxBox_111ny_z_z_z__zSurfaceIta261, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[261] = {BoxBox_111ny_z_z_z__zSurfaceIta262, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[262] = {BoxBox_111ny_z_z_z__zSurfaceIta263, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[263] = {BoxBox_111ny_z_z_z__zSurfaceIta264, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[264] = {BoxBox_111ny_z_z_z__zSurfaceIta265, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[265] = {BoxBox_111ny_z_z_z__zSurfaceIta266, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[266] = {BoxBox_111ny_z_z_z__zSurfaceIta267, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[267] = {BoxBox_111ny_z_z_z__zSurfaceIta268, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[268] = {BoxBox_111ny_z_z_z__zSurfaceIta269, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[269] = {BoxBox_111ny_z_z_z__zSurfaceIta270, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[270] = {BoxBox_111ny_z_z_z__zSurfaceIta271, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[271] = {BoxBox_111ny_z_z_z__zSurfaceIta272, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[272] = {BoxBox_111ny_z_z_z__zSurfaceIta273, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[273] = {BoxBox_111ny_z_z_z__zSurfaceIta274, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[274] = {BoxBox_111ny_z_z_z__zSurfaceIta275, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[275] = {BoxBox_111ny_z_z_z__zSurfaceIta276, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[276] = {BoxBox_111ny_z_z_z__zSurfaceIta277, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[277] = {BoxBox_111ny_z_z_z__zSurfaceIta278, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[278] = {BoxBox_111ny_z_z_z__zSurfaceIta279, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[279] = {BoxBox_111ny_z_z_z__zSurfaceIta280, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[280] = {BoxBox_111ny_z_z_z__zSurfaceIta281, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[281] = {BoxBox_111ny_z_z_z__zSurfaceIta282, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[282] = {BoxBox_111ny_z_z_z__zSurfaceIta283, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[283] = {BoxBox_111ny_z_z_z__zSurfaceIta284, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[284] = {BoxBox_111ny_z_z_z__zSurfaceIta285, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[285] = {BoxBox_111ny_z_z_z__zSurfaceIta286, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[286] = {BoxBox_111ny_z_z_z__zSurfaceIta287, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[287] = {BoxBox_111ny_z_z_z__zSurfaceIta288, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[288] = {BoxBox_111ny_z_z_z__zSurfaceIta289, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[289] = {BoxBox_111ny_z_z_z__zSurfaceIta290, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[290] = {BoxBox_111ny_z_z_z__zSurfaceIta291, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[291] = {BoxBox_111ny_z_z_z__zSurfaceIta292, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[292] = {BoxBox_111ny_z_z_z__zSurfaceIta293, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[293] = {BoxBox_111ny_z_z_z__zSurfaceIta294, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[294] = {BoxBox_111ny_z_z_z__zSurfaceIta295, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[295] = {BoxBox_111ny_z_z_z__zSurfaceIta296, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[296] = {BoxBox_111ny_z_z_z__zSurfaceIta297, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[297] = {BoxBox_111ny_z_z_z__zSurfaceIta298, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[298] = {BoxBox_111ny_z_z_z__zSurfaceIta299, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[299] = {BoxBox_111ny_z_z_z__zSurfaceIta300, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[300] = {BoxBox_111ny_z_z_z__zSurfaceIta301, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[301] = {BoxBox_111ny_z_z_z__zSurfaceIta302, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[302] = {BoxBox_111ny_z_z_z__zSurfaceIta303, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[303] = {BoxBox_111ny_z_z_z__zSurfaceIta304, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[304] = {BoxBox_111ny_z_z_z__zSurfaceIta305, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[305] = {BoxBox_111ny_z_z_z__zSurfaceIta306, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[306] = {BoxBox_111ny_z_z_z__zSurfaceIta307, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[307] = {BoxBox_111ny_z_z_z__zSurfaceIta308, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[308] = {BoxBox_111ny_z_z_z__zSurfaceIta309, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[309] = {BoxBox_111ny_z_z_z__zSurfaceIta310, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[310] = {BoxBox_111ny_z_z_z__zSurfaceIta311, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[311] = {BoxBox_111ny_z_z_z__zSurfaceIta312, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[312] = {BoxBox_111ny_z_z_z__zSurfaceIta313, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[313] = {BoxBox_111ny_z_z_z__zSurfaceIta314, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[314] = {BoxBox_111ny_z_z_z__zSurfaceIta315, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[315] = {BoxBox_111ny_z_z_z__zSurfaceIta316, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[316] = {BoxBox_111ny_z_z_z__zSurfaceIta317, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[317] = {BoxBox_111ny_z_z_z__zSurfaceIta318, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[318] = {BoxBox_111ny_z_z_z__zSurfaceIta319, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[319] = {BoxBox_111ny_z_z_z__zSurfaceIta320, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[320] = {BoxBox_111ny_z_z_z__zSurfaceIta321, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[321] = {BoxBox_111ny_z_z_z__zSurfaceIta322, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[322] = {BoxBox_111ny_z_z_z__zSurfaceIta323, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[323] = {BoxBox_111ny_z_z_z__zSurfaceIta324, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[324] = {BoxBox_111ny_z_z_z__zSurfaceIta325, -1};
BoxBox_111ny_z_z_z__zSurfaceItaArray[325] = {BoxBox_111ny_z_z_z__zSurfaceIta326, -1};
return BoxBox_111ny_z_z_z__zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,326> BoxBox_111ny_z_z_z__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,326> BoxBox_111ny_z_z_z__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,326> BoxBox_111ny_z_z_z__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,326> BoxBox_111ny_z_z_z__zSurfaceIta<F32>();

#endif

}}}}

