#include "FunctionSpace/IBPhelper.h"

#include "BoxBox_111ny_z_z_z__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,326> BoxBox_111ny_z_z_z__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,326> BoxBox_111ny_z_z_z__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,326> BoxBox_111ny_z_z_z__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,326> BoxBox_111ny_z_z_z__zSurfaceIta<F32>();

#endif

}}}}

