/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][7];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_111nn_z_z_m__SurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][7];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,9> BoxBox_111nn_z_z_m__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,9> BoxBox_111nn_z_z_m__SurfaceItaArray;
BoxBox_111nn_z_z_m__SurfaceItaArray[0] = {BoxBox_111nn_z_z_m__SurfaceIta1, -1};
BoxBox_111nn_z_z_m__SurfaceItaArray[1] = {BoxBox_111nn_z_z_m__SurfaceIta2, -1};
BoxBox_111nn_z_z_m__SurfaceItaArray[2] = {BoxBox_111nn_z_z_m__SurfaceIta3, -1};
BoxBox_111nn_z_z_m__SurfaceItaArray[3] = {BoxBox_111nn_z_z_m__SurfaceIta4, -1};
BoxBox_111nn_z_z_m__SurfaceItaArray[4] = {BoxBox_111nn_z_z_m__SurfaceIta5, -1};
BoxBox_111nn_z_z_m__SurfaceItaArray[5] = {BoxBox_111nn_z_z_m__SurfaceIta6, -1};
BoxBox_111nn_z_z_m__SurfaceItaArray[6] = {BoxBox_111nn_z_z_m__SurfaceIta7, -1};
BoxBox_111nn_z_z_m__SurfaceItaArray[7] = {BoxBox_111nn_z_z_m__SurfaceIta8, -1};
BoxBox_111nn_z_z_m__SurfaceItaArray[8] = {BoxBox_111nn_z_z_m__SurfaceIta9, -1};
return BoxBox_111nn_z_z_m__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,9> BoxBox_111nn_z_z_m__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,9> BoxBox_111nn_z_z_m__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,9> BoxBox_111nn_z_z_m__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,9> BoxBox_111nn_z_z_m__SurfaceIta<F32>();

#endif

}}}}

