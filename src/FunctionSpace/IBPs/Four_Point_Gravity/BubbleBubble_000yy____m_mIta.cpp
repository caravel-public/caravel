#include "FunctionSpace/IBPhelper.h"

#include "BubbleBubble_000yy____m_mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,44> BubbleBubble_000yy____m_mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,44> BubbleBubble_000yy____m_mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,44> BubbleBubble_000yy____m_mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,44> BubbleBubble_000yy____m_mSurfaceIta<F32>();

#endif

}}}}

