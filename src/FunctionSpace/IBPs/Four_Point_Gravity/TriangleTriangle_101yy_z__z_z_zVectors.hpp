#ifndef TriangleTriangle_101yy_z__z_z_zVectors_H_INC

#define TriangleTriangle_101yy_z__z_z_zVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec1_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec1_component2(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec1_component3(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[1];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = D[4];
z[7] = D[5];
z[8] = z[3] + T(1);
z[9] = z[6] + z[2] * T(2);
z[8] = z[8] * z[9];
z[9] = z[6] + -z[7];
z[10] = z[3] * T(-4) + T(-2) + -z[9];
z[10] = z[4] * z[10];
z[9] = z[9] + z[3] * T(2);
z[9] = z[5] * z[9];
z[11] = z[5] + T(-1) + -z[0] + -z[4];
z[11] = z[1] * z[11];
return z[9] + z[10] + z[8] * T(2) + z[11] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec1_component4(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = D[1];
z[3] = D[6];
z[4] = D[7];
z[5] = D[4];
z[6] = z[4] + -z[3];
z[7] = T(2) + -z[6];
z[7] = z[2] * z[7];
z[6] = z[6] + T(-1) + -z[0];
z[6] = z[1] * z[6];
z[6] = z[5] + z[7] + z[6] * T(2) + -z[3];
return z[6] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec1_component9(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[6];
z[2] = D[7];
z[3] = z[0] + z[1] + T(1) + -z[2];
return z[3] * T(8);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[6];
z[2] = D[7];
z[3] = z[1] + -z[2];
z[3] = z[3] * T(-3) + T(-2) + z[0] * T(-2);
return z[3] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec2_component1(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = D[5];
z[4] = invariants[1];
z[5] = D[3];
z[6] = D[6];
z[7] = D[7];
z[8] = T(-1) + -z[4];
z[9] = z[5] * T(2) + -z[6];
z[8] = z[8] * z[9];
z[9] = z[7] + -z[6];
z[10] = z[9] + T(-4) + z[4] * T(-2);
z[10] = z[2] * z[10];
z[9] = T(2) + -z[9];
z[9] = z[3] * z[9];
z[11] = z[3] + -z[1] + -z[2] + -z[4];
z[11] = z[0] * z[11];
return z[9] + z[10] + z[8] * T(2) + z[11] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec2_component2(const T* invariants, const T* D)
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = D[5];
z[4] = invariants[1];
z[5] = D[3];
z[6] = D[6];
z[7] = z[3] + -z[2];
z[8] = -(z[5] * z[7]);
z[9] = z[2] + z[5] * T(2) + -z[6];
z[9] = z[4] * z[9];
z[7] = z[7] + -z[1] + -z[4];
z[7] = z[0] * z[7];
z[7] = z[8] + z[9] + z[7] * T(2);
return z[7] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec2_component3(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec2_component4(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec2_component9(const T* invariants, const T* D)
{
T z[5];
z[0] = D[2];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = z[0] + z[1] + z[3] + -z[2];
return z[4] * T(8);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec2_divergence(const T* invariants, const T* D)
{
T z[6];
z[0] = D[2];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = z[1] + -z[2];
z[5] = -z[0] + -z[3];
z[4] = z[4] * T(-3) + z[5] * T(2);
return z[4] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec3_component1(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = T(-1) + -z[0] + -z[1];
z[3] = z[0] * z[3];
return z[2] + z[3] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec3_component2(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = T(-1) + -z[0] + -z[1];
z[3] = z[1] * z[3];
return z[2] + z[3] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec3_component3(const T* invariants, const T* D)
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[1];
z[5] = -z[0] + -z[4];
z[6] = z[1] * T(2) + -z[2];
z[7] = z[3] + z[6];
z[5] = z[5] * z[7];
return z[5] + -z[6];
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec3_component4(const T* invariants, const T* D)
{
T z[4];
z[0] = D[3];
z[1] = D[6];
z[2] = D[7];
z[3] = z[1] + T(-2) + z[0] * T(-2) + -z[2];
z[3] = z[0] * z[3];
return z[1] + z[3];
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec3_component9(const T* invariants, const T* D)
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[6];
z[4] = D[7];
z[5] = z[0] + z[1] + z[2] + T(1);
z[5] = z[4] + z[5] * T(2) + -z[3];
return z[5] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec3_divergence(const T* invariants, const T* D)
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[6];
z[4] = D[7];
z[5] = T(-1) + -z[0] + -z[1] + -z[2];
z[5] = z[3] + z[5] * T(2) + -z[4];
return z[5] * T(3);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec5_component1(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[6];
z[2] = invariants[1];
z[3] = D[7];
z[4] = D[1];
z[5] = D[4];
z[6] = D[5];
z[7] = z[5] + -z[6];
z[8] = z[3] + -z[1];
z[7] = z[7] * z[8];
z[9] = z[2] * z[8];
z[8] = z[8] + z[9];
z[8] = z[4] * z[8];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
return z[7] + z[8] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec5_component2(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[3];
z[2] = invariants[1];
z[3] = D[1];
z[4] = D[2];
z[5] = D[4];
z[6] = D[6];
z[7] = z[0] + T(1);
z[7] = z[1] * z[7];
z[7] = z[7] * T(2) + -z[5] + -z[6];
z[7] = z[2] * z[7];
z[8] = z[1] + T(-1);
z[8] = z[2] * z[8];
z[8] = z[1] + z[8] + -z[4];
z[8] = z[3] * z[8];
z[7] = z[7] + z[8] * T(2);
return z[7] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec5_component3(const T* invariants, const T* D)
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = invariants[1];
z[3] = D[6];
z[4] = D[7];
z[5] = z[1] * T(2);
z[6] = z[4] + z[5] + -z[3];
z[7] = z[6] + T(-2);
z[7] = z[0] * z[7];
z[6] = z[2] * z[6];
z[5] = z[5] + z[6] + z[7];
z[5] = z[2] * z[5];
z[6] = z[1] + -z[0];
z[6] = z[0] * z[6];
z[5] = z[5] + z[6] * T(2);
return z[5] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec5_component4(const T* invariants, const T* D)
{
T z[7];
z[0] = D[2];
z[1] = D[3];
z[2] = invariants[1];
z[3] = D[6];
z[4] = D[7];
z[5] = z[1] * T(2);
z[6] = z[1] + -z[0];
z[6] = z[5] * z[6];
z[5] = z[4] + z[5] + -z[3];
z[5] = z[1] * z[5];
z[5] = z[5] + z[3] * T(-2);
z[5] = z[2] * z[5];
z[5] = z[5] + z[6];
return z[5] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec5_component9(const T* invariants, const T* D)
{
T z[7];
z[0] = D[2];
z[1] = D[3];
z[2] = invariants[1];
z[3] = D[6];
z[4] = D[7];
z[5] = z[0] + -z[1];
z[6] = z[4] + -z[3];
z[6] = z[1] * T(-6) + z[6] * T(-3) + T(4);
z[6] = z[2] * z[6];
z[5] = z[6] + z[5] * T(6);
return z[5] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_z_zvec5_divergence(const T* invariants, const T* D)
{
T z[7];
z[0] = D[2];
z[1] = D[3];
z[2] = invariants[1];
z[3] = D[6];
z[4] = D[7];
z[5] = z[1] + -z[0];
z[6] = z[4] + T(-1) + z[1] * T(2) + -z[3];
z[6] = z[2] * z[6];
z[5] = z[6] + z[5] * T(2);
return z[5] * T(8);
}

template<typename T> std::vector<std::vector<T>> TriangleTriangle_101yy_z__z_z_z_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(5, std::vector<T>(10, T(0)));
ibp_vectors[0][2] = TriangleTriangle_101yy_z__z_z_zvec1_component3(invariants, propagators);
ibp_vectors[0][3] = TriangleTriangle_101yy_z__z_z_zvec1_component4(invariants, propagators);
ibp_vectors[0][8] = TriangleTriangle_101yy_z__z_z_zvec1_component9(invariants, propagators);
ibp_vectors[0].back() = TriangleTriangle_101yy_z__z_z_zvec1_divergence(invariants, propagators);
ibp_vectors[1][0] = TriangleTriangle_101yy_z__z_z_zvec2_component1(invariants, propagators);
ibp_vectors[1][1] = TriangleTriangle_101yy_z__z_z_zvec2_component2(invariants, propagators);
ibp_vectors[1][8] = TriangleTriangle_101yy_z__z_z_zvec2_component9(invariants, propagators);
ibp_vectors[1].back() = TriangleTriangle_101yy_z__z_z_zvec2_divergence(invariants, propagators);
ibp_vectors[2][0] = TriangleTriangle_101yy_z__z_z_zvec3_component1(invariants, propagators);
ibp_vectors[2][1] = TriangleTriangle_101yy_z__z_z_zvec3_component2(invariants, propagators);
ibp_vectors[2][2] = TriangleTriangle_101yy_z__z_z_zvec3_component3(invariants, propagators);
ibp_vectors[2][3] = TriangleTriangle_101yy_z__z_z_zvec3_component4(invariants, propagators);
ibp_vectors[2][8] = TriangleTriangle_101yy_z__z_z_zvec3_component9(invariants, propagators);
ibp_vectors[2].back() = TriangleTriangle_101yy_z__z_z_zvec3_divergence(invariants, propagators);
ibp_vectors[4][0] = TriangleTriangle_101yy_z__z_z_zvec5_component1(invariants, propagators);
ibp_vectors[4][1] = TriangleTriangle_101yy_z__z_z_zvec5_component2(invariants, propagators);
ibp_vectors[4][2] = TriangleTriangle_101yy_z__z_z_zvec5_component3(invariants, propagators);
ibp_vectors[4][3] = TriangleTriangle_101yy_z__z_z_zvec5_component4(invariants, propagators);
ibp_vectors[4][8] = TriangleTriangle_101yy_z__z_z_zvec5_component9(invariants, propagators);
ibp_vectors[4].back() = TriangleTriangle_101yy_z__z_z_zvec5_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleTriangle_101yy_z__z_z_z_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleTriangle_101yy_z__z_z_z_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleTriangle_101yy_z__z_z_z_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleTriangle_101yy_z__z_z_z_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

