#include "FunctionSpace/FunctionSpace.h"

#include "IBPInfoList.hpp"

#include "IBPList.hpp"

#include "Masters.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

void load()
{
using gType = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[2], Link[]], Strand[Link[], Leg[3, 0], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BoxBox_111nn_z_z_m__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BoxBox_111nn_z_z_m__SurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxBox_111nn_z_z_m___Master_1<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BoxBox_111nn_z_z_m__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BoxBox_111nn_z_z_m__SurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxBox_111nn_z_z_m___Master_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BoxBox_111nn_z_z_m__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BoxBox_111nn_z_z_m__SurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxBox_111nn_z_z_m___Master_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BoxBox_111nn_z_z_m__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BoxBox_111nn_z_z_m__SurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxBox_111nn_z_z_m___Master_1<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[3, 0], Link[]], Strand[Link[], Leg[4, 0], Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BoxBox_111ny_z_z_z__zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BoxBox_111ny_z_z_z__zSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BoxBox_111ny_z_z_z__zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BoxBox_111ny_z_z_z__zSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BoxBox_111ny_z_z_z__zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BoxBox_111ny_z_z_z__zSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BoxBox_111ny_z_z_z__zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BoxBox_111ny_z_z_z__zSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 2;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BoxBox_202nn_zz__zz__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BoxBox_202nn_zz__zz__SurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxBox_202nn_zz__zz___Master_1<C>, BoxBox_202nn_zz__zz___Master_2<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BoxBox_202nn_zz__zz__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BoxBox_202nn_zz__zz__SurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxBox_202nn_zz__zz___Master_1<CHP>, BoxBox_202nn_zz__zz___Master_2<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BoxBox_202nn_zz__zz__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BoxBox_202nn_zz__zz__SurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxBox_202nn_zz__zz___Master_1<CVHP>, BoxBox_202nn_zz__zz___Master_2<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BoxBox_202nn_zz__zz__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BoxBox_202nn_zz__zz__SurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxBox_202nn_zz__zz___Master_1<F32>, BoxBox_202nn_zz__zz___Master_2<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[], Leg[4, 0], Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 2;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BoxPentagon_112nn_z_z_zz__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BoxPentagon_112nn_z_z_zz__SurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxPentagon_112nn_z_z_zz___Master_1<C>, BoxPentagon_112nn_z_z_zz___Master_2<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BoxPentagon_112nn_z_z_zz__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BoxPentagon_112nn_z_z_zz__SurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxPentagon_112nn_z_z_zz___Master_1<CHP>, BoxPentagon_112nn_z_z_zz___Master_2<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BoxPentagon_112nn_z_z_zz__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BoxPentagon_112nn_z_z_zz__SurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxPentagon_112nn_z_z_zz___Master_1<CVHP>, BoxPentagon_112nn_z_z_zz___Master_2<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BoxPentagon_112nn_z_z_zz__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BoxPentagon_112nn_z_z_zz__SurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxPentagon_112nn_z_z_zz___Master_1<F32>, BoxPentagon_112nn_z_z_zz___Master_2<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[2], Link[], Leg[1], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleBox_002nn___mm__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleBox_002nn___mm__SurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleBox_002nn___mm__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleBox_002nn___mm__SurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleBox_002nn___mm__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleBox_002nn___mm__SurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleBox_002nn___mm__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleBox_002nn___mm__SurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[3, 0], Link[], Leg[2], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleBox_002ny___mz__zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleBox_002ny___mz__zSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleBox_002ny___mz__zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleBox_002ny___mz__zSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleBox_002ny___mz__zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleBox_002ny___mz__zSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleBox_002ny___mz__zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleBox_002ny___mz__zSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[3], Link[], Leg[2, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleBox_002ny___zm__zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleBox_002ny___zm__zSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleBox_002ny___zm__zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleBox_002ny___zm__zSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleBox_002ny___zm__zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleBox_002ny___zm__zSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleBox_002ny___zm__zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleBox_002ny___zm__zSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[1]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleBox_002ny___zz__mCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleBox_002ny___zz__mSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleBox_002ny___zz__mCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleBox_002ny___zz__mSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleBox_002ny___zz__mCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleBox_002ny___zz__mSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleBox_002ny___zz__mCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleBox_002ny___zz__mSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[4, 0]], Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleBox_002yy___zz_z_zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleBox_002yy___zz_z_zSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {BubbleBox_002yy___zz_z_z_Master_1<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleBox_002yy___zz_z_zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleBox_002yy___zz_z_zSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BubbleBox_002yy___zz_z_z_Master_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleBox_002yy___zz_z_zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleBox_002yy___zz_z_zSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BubbleBox_002yy___zz_z_z_Master_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleBox_002yy___zz_z_zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleBox_002yy___zz_z_zSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BubbleBox_002yy___zz_z_z_Master_1<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[2]], Node[Leg[1]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2]], Strand[Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleBubble_000yy____m_mCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleBubble_000yy____m_mSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {BubbleBubble_000yy____m_m_Master_1<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleBubble_000yy____m_mCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleBubble_000yy____m_mSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BubbleBubble_000yy____m_m_Master_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleBubble_000yy____m_mCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleBubble_000yy____m_mSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BubbleBubble_000yy____m_m_Master_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleBubble_000yy____m_mCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleBubble_000yy____m_mSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BubbleBubble_000yy____m_m_Master_1<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3, 0], Link[], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleHexagon_004nn___zzzz__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleHexagon_004nn___zzzz__SurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleHexagon_004nn___zzzz__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleHexagon_004nn___zzzz__SurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleHexagon_004nn___zzzz__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleHexagon_004nn___zzzz__SurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleHexagon_004nn___zzzz__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleHexagon_004nn___zzzz__SurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[3, 0], Link[], Leg[2, 0], Link[], Leg[1], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubblePentagon_003nn___mzz__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubblePentagon_003nn___mzz__SurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubblePentagon_003nn___mzz__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubblePentagon_003nn___mzz__SurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubblePentagon_003nn___mzz__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubblePentagon_003nn___mzz__SurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubblePentagon_003nn___mzz__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubblePentagon_003nn___mzz__SurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[3, 0], Link[], Leg[2], Link[], Leg[1, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubblePentagon_003nn___zmz__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubblePentagon_003nn___zmz__SurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubblePentagon_003nn___zmz__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubblePentagon_003nn___zmz__SurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubblePentagon_003nn___zmz__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubblePentagon_003nn___zmz__SurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubblePentagon_003nn___zmz__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubblePentagon_003nn___zmz__SurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubblePentagon_003ny___zzz__zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubblePentagon_003ny___zzz__zSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubblePentagon_003ny___zzz__zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubblePentagon_003ny___zzz__zSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubblePentagon_003ny___zzz__zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubblePentagon_003ny___zzz__zSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubblePentagon_003ny___zzz__zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubblePentagon_003ny___zzz__zSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[1]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[2], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleTriangle_001ny___m__mCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleTriangle_001ny___m__mSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleTriangle_001ny___m__mCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleTriangle_001ny___m__mSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleTriangle_001ny___m__mCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleTriangle_001ny___m__mSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleTriangle_001ny___m__mCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleTriangle_001ny___m__mSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]], Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[2], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleTriangle_001yy___m_z_zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleTriangle_001yy___m_z_zSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {BubbleTriangle_001yy___m_z_z_Master_1<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleTriangle_001yy___m_z_zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleTriangle_001yy___m_z_zSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BubbleTriangle_001yy___m_z_z_Master_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleTriangle_001yy___m_z_zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleTriangle_001yy___m_z_zSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BubbleTriangle_001yy___m_z_z_Master_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleTriangle_001yy___m_z_zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleTriangle_001yy___m_z_zSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BubbleTriangle_001yy___m_z_z_Master_1<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3]], Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1]], Strand[LoopMomentum[-2], Leg[2, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = BubbleTriangle_001yy___z_m_zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(BubbleTriangle_001yy___z_m_zSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = BubbleTriangle_001yy___z_m_zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(BubbleTriangle_001yy___z_m_zSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = BubbleTriangle_001yy___z_m_zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(BubbleTriangle_001yy___z_m_zSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = BubbleTriangle_001yy___z_m_zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(BubbleTriangle_001yy___z_m_zSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[-2], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = TriangleBox_102nn_m__zz__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(TriangleBox_102nn_m__zz__SurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = TriangleBox_102nn_m__zz__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(TriangleBox_102nn_m__zz__SurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = TriangleBox_102nn_m__zz__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(TriangleBox_102nn_m__zz__SurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = TriangleBox_102nn_m__zz__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(TriangleBox_102nn_m__zz__SurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[3, 0], Link[], Leg[2], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = TriangleBox_102nn_z__mz__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(TriangleBox_102nn_z__mz__SurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = TriangleBox_102nn_z__mz__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(TriangleBox_102nn_z__mz__SurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = TriangleBox_102nn_z__mz__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(TriangleBox_102nn_z__mz__SurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = TriangleBox_102nn_z__mz__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(TriangleBox_102nn_z__mz__SurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = TriangleBox_102ny_z__zz__zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(TriangleBox_102ny_z__zz__zSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = TriangleBox_102ny_z__zz__zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(TriangleBox_102ny_z__zz__zSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = TriangleBox_102ny_z__zz__zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(TriangleBox_102ny_z__zz__zSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = TriangleBox_102ny_z__zz__zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(TriangleBox_102ny_z__zz__zSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = TrianglePentagon_103nn_z__zzz__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(TrianglePentagon_103nn_z__zzz__SurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = TrianglePentagon_103nn_z__zzz__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(TrianglePentagon_103nn_z__zzz__SurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = TrianglePentagon_103nn_z__zzz__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(TrianglePentagon_103nn_z__zzz__SurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = TrianglePentagon_103nn_z__zzz__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(TrianglePentagon_103nn_z__zzz__SurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[-2], Leg[2], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = TriangleTriangle_101nn_m__m__Coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(TriangleTriangle_101nn_m__m__SurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = TriangleTriangle_101nn_m__m__Coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(TriangleTriangle_101nn_m__m__SurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = TriangleTriangle_101nn_m__m__Coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(TriangleTriangle_101nn_m__m__SurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = TriangleTriangle_101nn_m__m__Coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(TriangleTriangle_101nn_m__m__SurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[3], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = TriangleTriangle_101ny_z__m__zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(TriangleTriangle_101ny_z__m__zSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = TriangleTriangle_101ny_z__m__zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(TriangleTriangle_101ny_z__m__zSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = TriangleTriangle_101ny_z__m__zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(TriangleTriangle_101ny_z__m__zSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = TriangleTriangle_101ny_z__m__zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(TriangleTriangle_101ny_z__m__zSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[2]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[3, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
std::get<size_t>(data) = 0;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = TriangleTriangle_101ny_z__z__mCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(TriangleTriangle_101ny_z__z__mSurfaceIta<C>());
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = TriangleTriangle_101ny_z__z__mCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(TriangleTriangle_101ny_z__z__mSurfaceIta<CHP>());
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = TriangleTriangle_101ny_z__z__mCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(TriangleTriangle_101ny_z__z__mSurfaceIta<CVHP>());
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = TriangleTriangle_101ny_z__z__mCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(TriangleTriangle_101ny_z__z__mSurfaceIta<F32>());
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[4, 0]], Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[LoopMomentum[-2], Leg[3, 0], Link[]], Strand[Link[]]]]");
SurfaceTermData data;
MasterTermData mdata;
std::get<size_t>(data) = 1;
std::get<lGraph::xGraph>(data) = graph;
std::get<IBPCoordinatesFP<C,6>>(data) = TriangleTriangle_101yy_z__z_z_zCoordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(data) = IBP_array_to_vector(TriangleTriangle_101yy_z__z_z_zSurfaceIta<C>());
std::get<std::vector<BasisFunction<C>>>(mdata) = {TriangleTriangle_101yy_z__z_z_z_Master_1<C>};
#ifdef HIGH_PRECISION
std::get<IBPCoordinatesFP<CHP,6>>(data) = TriangleTriangle_101yy_z__z_z_zCoordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(data) = IBP_array_to_vector(TriangleTriangle_101yy_z__z_z_zSurfaceIta<CHP>());
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {TriangleTriangle_101yy_z__z_z_z_Master_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<IBPCoordinatesFP<CVHP,6>>(data) = TriangleTriangle_101yy_z__z_z_zCoordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(data) = IBP_array_to_vector(TriangleTriangle_101yy_z__z_z_zSurfaceIta<CVHP>());
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {TriangleTriangle_101yy_z__z_z_z_Master_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<IBPCoordinatesFP<F32,6>>(data) = TriangleTriangle_101yy_z__z_z_zCoordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(data) = IBP_array_to_vector(TriangleTriangle_101yy_z__z_z_zSurfaceIta<F32>());
std::get<std::vector<BasisFunction<F32>>>(mdata) = {TriangleTriangle_101yy_z__z_z_z_Master_1<F32>};
#endif
surface_term_data_holder[graph.filter()] = std::move(data);
master_term_data_holder[graph.filter()] = std::move(mdata);
}
}

}}}}

