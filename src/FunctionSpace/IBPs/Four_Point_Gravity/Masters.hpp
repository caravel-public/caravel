namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template <typename T> T BoxBox_111nn_z_z_m___Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BoxBox_202nn_zz__zz___Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BoxBox_202nn_zz__zz___Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[0] + D[2]) / T(2);
}

template <typename T> T BoxPentagon_112nn_z_z_zz___Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BoxPentagon_112nn_z_z_zz___Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[0] + D[2]) / T(2);
}

template <typename T> T BubbleBox_002yy___zz_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BubbleBubble_000yy____m_m_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BubbleTriangle_001yy___m_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T TriangleTriangle_101yy_z__z_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

}}}}
