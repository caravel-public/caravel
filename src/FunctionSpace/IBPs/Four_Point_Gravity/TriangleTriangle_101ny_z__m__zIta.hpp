/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][6];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][6];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 3) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(3);
z[7] = prod_pow(z[0], 2) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 3) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(3);
z[7] = prod_pow(z[0], 2) * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][6];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][6];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][6];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][6];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][6];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][6];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][6];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][6];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,60> TriangleTriangle_101ny_z__m__zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,60> TriangleTriangle_101ny_z__m__zSurfaceItaArray;
TriangleTriangle_101ny_z__m__zSurfaceItaArray[0] = {TriangleTriangle_101ny_z__m__zSurfaceIta1, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[1] = {TriangleTriangle_101ny_z__m__zSurfaceIta2, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[2] = {TriangleTriangle_101ny_z__m__zSurfaceIta3, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[3] = {TriangleTriangle_101ny_z__m__zSurfaceIta4, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[4] = {TriangleTriangle_101ny_z__m__zSurfaceIta5, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[5] = {TriangleTriangle_101ny_z__m__zSurfaceIta6, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[6] = {TriangleTriangle_101ny_z__m__zSurfaceIta7, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[7] = {TriangleTriangle_101ny_z__m__zSurfaceIta8, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[8] = {TriangleTriangle_101ny_z__m__zSurfaceIta9, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[9] = {TriangleTriangle_101ny_z__m__zSurfaceIta10, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[10] = {TriangleTriangle_101ny_z__m__zSurfaceIta11, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[11] = {TriangleTriangle_101ny_z__m__zSurfaceIta12, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[12] = {TriangleTriangle_101ny_z__m__zSurfaceIta13, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[13] = {TriangleTriangle_101ny_z__m__zSurfaceIta14, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[14] = {TriangleTriangle_101ny_z__m__zSurfaceIta15, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[15] = {TriangleTriangle_101ny_z__m__zSurfaceIta16, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[16] = {TriangleTriangle_101ny_z__m__zSurfaceIta17, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[17] = {TriangleTriangle_101ny_z__m__zSurfaceIta18, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[18] = {TriangleTriangle_101ny_z__m__zSurfaceIta19, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[19] = {TriangleTriangle_101ny_z__m__zSurfaceIta20, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[20] = {TriangleTriangle_101ny_z__m__zSurfaceIta21, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[21] = {TriangleTriangle_101ny_z__m__zSurfaceIta22, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[22] = {TriangleTriangle_101ny_z__m__zSurfaceIta23, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[23] = {TriangleTriangle_101ny_z__m__zSurfaceIta24, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[24] = {TriangleTriangle_101ny_z__m__zSurfaceIta25, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[25] = {TriangleTriangle_101ny_z__m__zSurfaceIta26, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[26] = {TriangleTriangle_101ny_z__m__zSurfaceIta27, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[27] = {TriangleTriangle_101ny_z__m__zSurfaceIta28, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[28] = {TriangleTriangle_101ny_z__m__zSurfaceIta29, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[29] = {TriangleTriangle_101ny_z__m__zSurfaceIta30, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[30] = {TriangleTriangle_101ny_z__m__zSurfaceIta31, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[31] = {TriangleTriangle_101ny_z__m__zSurfaceIta32, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[32] = {TriangleTriangle_101ny_z__m__zSurfaceIta33, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[33] = {TriangleTriangle_101ny_z__m__zSurfaceIta34, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[34] = {TriangleTriangle_101ny_z__m__zSurfaceIta35, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[35] = {TriangleTriangle_101ny_z__m__zSurfaceIta36, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[36] = {TriangleTriangle_101ny_z__m__zSurfaceIta37, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[37] = {TriangleTriangle_101ny_z__m__zSurfaceIta38, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[38] = {TriangleTriangle_101ny_z__m__zSurfaceIta39, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[39] = {TriangleTriangle_101ny_z__m__zSurfaceIta40, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[40] = {TriangleTriangle_101ny_z__m__zSurfaceIta41, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[41] = {TriangleTriangle_101ny_z__m__zSurfaceIta42, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[42] = {TriangleTriangle_101ny_z__m__zSurfaceIta43, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[43] = {TriangleTriangle_101ny_z__m__zSurfaceIta44, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[44] = {TriangleTriangle_101ny_z__m__zSurfaceIta45, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[45] = {TriangleTriangle_101ny_z__m__zSurfaceIta46, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[46] = {TriangleTriangle_101ny_z__m__zSurfaceIta47, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[47] = {TriangleTriangle_101ny_z__m__zSurfaceIta48, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[48] = {TriangleTriangle_101ny_z__m__zSurfaceIta49, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[49] = {TriangleTriangle_101ny_z__m__zSurfaceIta50, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[50] = {TriangleTriangle_101ny_z__m__zSurfaceIta51, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[51] = {TriangleTriangle_101ny_z__m__zSurfaceIta52, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[52] = {TriangleTriangle_101ny_z__m__zSurfaceIta53, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[53] = {TriangleTriangle_101ny_z__m__zSurfaceIta54, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[54] = {TriangleTriangle_101ny_z__m__zSurfaceIta55, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[55] = {TriangleTriangle_101ny_z__m__zSurfaceIta56, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[56] = {TriangleTriangle_101ny_z__m__zSurfaceIta57, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[57] = {TriangleTriangle_101ny_z__m__zSurfaceIta58, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[58] = {TriangleTriangle_101ny_z__m__zSurfaceIta59, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[59] = {TriangleTriangle_101ny_z__m__zSurfaceIta60, -1};
return TriangleTriangle_101ny_z__m__zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,60> TriangleTriangle_101ny_z__m__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,60> TriangleTriangle_101ny_z__m__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,60> TriangleTriangle_101ny_z__m__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,60> TriangleTriangle_101ny_z__m__zSurfaceIta<F32>();

#endif

}}}}

