#ifndef TriangleTriangle_101ny_z__m__zVectors_H_INC

#define TriangleTriangle_101ny_z__m__zVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> T TriangleTriangle_101ny_z__m__zvec1_component1(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[2];
z[2] = T(-1) + -z[0];
z[2] = z[0] * z[2];
z[2] = z[1] + z[2];
return z[2] * T(2);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec1_component2(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = D[5];
z[3] = z[1] + -z[2];
z[3] = z[0] * z[3];
return z[1] + z[3];
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec1_component7(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = z[2] + T(2) + z[0] * T(2) + -z[1];
return z[3] * T(2);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = z[1] + z[0] * T(-2) + T(-1) + -z[2];
return z[3] * T(2);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec2_component1(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[5];
z[6] = T(2) + -z[5];
z[6] = z[3] * z[6];
z[7] = z[5] + T(-4);
z[7] = z[2] * z[7];
z[8] = z[2] + -z[3];
z[9] = -z[1] + -z[8];
z[9] = z[0] * z[9];
z[8] = T(2) + -z[8];
z[8] = z[4] * z[8];
return z[6] + z[7] + z[8] + z[9] * T(4);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec2_component2(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec2_component7(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = z[0] + z[1] + -z[2];
return z[3] * T(8);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec2_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = z[2] + -z[1];
z[3] = z[3] * T(2) + -z[0];
return z[3] * T(4);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec3_component1(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = T(-1) + -z[0];
z[3] = z[2] * z[3];
z[4] = z[0] + T(2);
z[4] = z[1] * z[4];
return z[3] + z[4];
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec3_component2(const T* invariants, const T* D)
{
T z[2];
z[0] = D[1];
z[1] = T(-1) + -z[0];
return z[0] * z[1] * T(2);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec3_component7(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = z[2] + T(2) + z[0] * T(2) + -z[1];
return z[3] * T(2);
}

template<typename T> T TriangleTriangle_101ny_z__m__zvec3_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = z[1] + z[0] * T(-2) + T(-1) + -z[2];
return z[3] * T(2);
}

template<typename T> std::vector<std::vector<T>> TriangleTriangle_101ny_z__m__z_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(3, std::vector<T>(8, T(0)));
ibp_vectors[0][0] = TriangleTriangle_101ny_z__m__zvec1_component1(invariants, propagators);
ibp_vectors[0][1] = TriangleTriangle_101ny_z__m__zvec1_component2(invariants, propagators);
ibp_vectors[0][6] = TriangleTriangle_101ny_z__m__zvec1_component7(invariants, propagators);
ibp_vectors[0].back() = TriangleTriangle_101ny_z__m__zvec1_divergence(invariants, propagators);
ibp_vectors[1][0] = TriangleTriangle_101ny_z__m__zvec2_component1(invariants, propagators);
ibp_vectors[1][6] = TriangleTriangle_101ny_z__m__zvec2_component7(invariants, propagators);
ibp_vectors[1].back() = TriangleTriangle_101ny_z__m__zvec2_divergence(invariants, propagators);
ibp_vectors[2][0] = TriangleTriangle_101ny_z__m__zvec3_component1(invariants, propagators);
ibp_vectors[2][1] = TriangleTriangle_101ny_z__m__zvec3_component2(invariants, propagators);
ibp_vectors[2][6] = TriangleTriangle_101ny_z__m__zvec3_component7(invariants, propagators);
ibp_vectors[2].back() = TriangleTriangle_101ny_z__m__zvec3_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleTriangle_101ny_z__m__z_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleTriangle_101ny_z__m__z_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleTriangle_101ny_z__m__z_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleTriangle_101ny_z__m__z_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

