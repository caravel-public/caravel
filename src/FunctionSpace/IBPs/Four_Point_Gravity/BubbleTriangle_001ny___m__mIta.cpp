#include "FunctionSpace/IBPhelper.h"

#include "BubbleTriangle_001ny___m__mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,8> BubbleTriangle_001ny___m__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,8> BubbleTriangle_001ny___m__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,8> BubbleTriangle_001ny___m__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,8> BubbleTriangle_001ny___m__mSurfaceIta<F32>();

#endif

}}}}

