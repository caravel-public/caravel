/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][5];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][5];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003nn___zmz__SurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][5];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[3][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,21> BubblePentagon_003nn___zmz__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,21> BubblePentagon_003nn___zmz__SurfaceItaArray;
BubblePentagon_003nn___zmz__SurfaceItaArray[0] = {BubblePentagon_003nn___zmz__SurfaceIta1, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[1] = {BubblePentagon_003nn___zmz__SurfaceIta2, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[2] = {BubblePentagon_003nn___zmz__SurfaceIta3, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[3] = {BubblePentagon_003nn___zmz__SurfaceIta4, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[4] = {BubblePentagon_003nn___zmz__SurfaceIta5, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[5] = {BubblePentagon_003nn___zmz__SurfaceIta6, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[6] = {BubblePentagon_003nn___zmz__SurfaceIta7, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[7] = {BubblePentagon_003nn___zmz__SurfaceIta8, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[8] = {BubblePentagon_003nn___zmz__SurfaceIta9, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[9] = {BubblePentagon_003nn___zmz__SurfaceIta10, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[10] = {BubblePentagon_003nn___zmz__SurfaceIta11, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[11] = {BubblePentagon_003nn___zmz__SurfaceIta12, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[12] = {BubblePentagon_003nn___zmz__SurfaceIta13, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[13] = {BubblePentagon_003nn___zmz__SurfaceIta14, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[14] = {BubblePentagon_003nn___zmz__SurfaceIta15, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[15] = {BubblePentagon_003nn___zmz__SurfaceIta16, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[16] = {BubblePentagon_003nn___zmz__SurfaceIta17, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[17] = {BubblePentagon_003nn___zmz__SurfaceIta18, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[18] = {BubblePentagon_003nn___zmz__SurfaceIta19, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[19] = {BubblePentagon_003nn___zmz__SurfaceIta20, -1};
BubblePentagon_003nn___zmz__SurfaceItaArray[20] = {BubblePentagon_003nn___zmz__SurfaceIta21, -1};
return BubblePentagon_003nn___zmz__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,21> BubblePentagon_003nn___zmz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,21> BubblePentagon_003nn___zmz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,21> BubblePentagon_003nn___zmz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,21> BubblePentagon_003nn___zmz__SurfaceIta<F32>();

#endif

}}}}

