/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][2];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][2];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[5];
z[7] = prod_pow(z[0], 2);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(2);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[5];
z[7] = prod_pow(z[0], 2);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(2);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[5];
z[7] = prod_pow(z[0], 2);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(2);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][2];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][2];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][2];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][2];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBubble_000yy____m_mSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][2];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,44> BubbleBubble_000yy____m_mSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,44> BubbleBubble_000yy____m_mSurfaceItaArray;
BubbleBubble_000yy____m_mSurfaceItaArray[0] = {BubbleBubble_000yy____m_mSurfaceIta1, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[1] = {BubbleBubble_000yy____m_mSurfaceIta2, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[2] = {BubbleBubble_000yy____m_mSurfaceIta3, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[3] = {BubbleBubble_000yy____m_mSurfaceIta4, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[4] = {BubbleBubble_000yy____m_mSurfaceIta5, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[5] = {BubbleBubble_000yy____m_mSurfaceIta6, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[6] = {BubbleBubble_000yy____m_mSurfaceIta7, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[7] = {BubbleBubble_000yy____m_mSurfaceIta8, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[8] = {BubbleBubble_000yy____m_mSurfaceIta9, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[9] = {BubbleBubble_000yy____m_mSurfaceIta10, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[10] = {BubbleBubble_000yy____m_mSurfaceIta11, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[11] = {BubbleBubble_000yy____m_mSurfaceIta12, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[12] = {BubbleBubble_000yy____m_mSurfaceIta13, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[13] = {BubbleBubble_000yy____m_mSurfaceIta14, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[14] = {BubbleBubble_000yy____m_mSurfaceIta15, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[15] = {BubbleBubble_000yy____m_mSurfaceIta16, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[16] = {BubbleBubble_000yy____m_mSurfaceIta17, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[17] = {BubbleBubble_000yy____m_mSurfaceIta18, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[18] = {BubbleBubble_000yy____m_mSurfaceIta19, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[19] = {BubbleBubble_000yy____m_mSurfaceIta20, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[20] = {BubbleBubble_000yy____m_mSurfaceIta21, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[21] = {BubbleBubble_000yy____m_mSurfaceIta22, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[22] = {BubbleBubble_000yy____m_mSurfaceIta23, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[23] = {BubbleBubble_000yy____m_mSurfaceIta24, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[24] = {BubbleBubble_000yy____m_mSurfaceIta25, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[25] = {BubbleBubble_000yy____m_mSurfaceIta26, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[26] = {BubbleBubble_000yy____m_mSurfaceIta27, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[27] = {BubbleBubble_000yy____m_mSurfaceIta28, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[28] = {BubbleBubble_000yy____m_mSurfaceIta29, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[29] = {BubbleBubble_000yy____m_mSurfaceIta30, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[30] = {BubbleBubble_000yy____m_mSurfaceIta31, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[31] = {BubbleBubble_000yy____m_mSurfaceIta32, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[32] = {BubbleBubble_000yy____m_mSurfaceIta33, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[33] = {BubbleBubble_000yy____m_mSurfaceIta34, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[34] = {BubbleBubble_000yy____m_mSurfaceIta35, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[35] = {BubbleBubble_000yy____m_mSurfaceIta36, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[36] = {BubbleBubble_000yy____m_mSurfaceIta37, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[37] = {BubbleBubble_000yy____m_mSurfaceIta38, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[38] = {BubbleBubble_000yy____m_mSurfaceIta39, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[39] = {BubbleBubble_000yy____m_mSurfaceIta40, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[40] = {BubbleBubble_000yy____m_mSurfaceIta41, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[41] = {BubbleBubble_000yy____m_mSurfaceIta42, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[42] = {BubbleBubble_000yy____m_mSurfaceIta43, -1};
BubbleBubble_000yy____m_mSurfaceItaArray[43] = {BubbleBubble_000yy____m_mSurfaceIta44, -1};
return BubbleBubble_000yy____m_mSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,44> BubbleBubble_000yy____m_mSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,44> BubbleBubble_000yy____m_mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,44> BubbleBubble_000yy____m_mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,44> BubbleBubble_000yy____m_mSurfaceIta<F32>();

#endif

}}}}

