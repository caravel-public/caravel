#include "FunctionSpace/IBPhelper.h"

#include "TriangleTriangle_101nn_m__m__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,1> TriangleTriangle_101nn_m__m__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,1> TriangleTriangle_101nn_m__m__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,1> TriangleTriangle_101nn_m__m__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,1> TriangleTriangle_101nn_m__m__SurfaceIta<F32>();

#endif

}}}}

