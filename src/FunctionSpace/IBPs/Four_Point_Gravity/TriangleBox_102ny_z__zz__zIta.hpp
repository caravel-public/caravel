/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][8];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[0][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[0] * z[8];
z[8] = z[7] + z[8];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[1] * z[8];
z[9] = prod_pow(z[0], 2) * z[2] * z[6];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * z[1] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta77(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta78(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta79(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta80(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta81(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta82(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta83(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta84(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta85(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta86(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta87(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta88(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta89(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta90(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta91(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta92(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta93(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[4];
z[8] = z[6] + z[8];
z[9] = prod_pow(z[2], 3);
z[8] = z[8] * z[9];
z[10] = z[1] * T(3);
z[11] = z[2] * z[3];
z[11] = z[5] + z[11];
z[11] = prod_pow(z[2], 2) * z[10] * z[11];
z[8] = z[8] + z[11];
z[8] = z[0] * z[8];
z[9] = z[7] * z[9] * z[10];
z[8] = z[8] + z[9];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta94(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta95(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta96(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[8] * z[9];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[2] * z[9];
z[10] = z[5] * z[8];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = prod_pow(z[2], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta97(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 3) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta98(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta99(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta100(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta101(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta102(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta103(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta104(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta105(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta106(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta107(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta108(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta109(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta110(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta111(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta112(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta113(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta114(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta115(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta116(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta117(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta118(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta119(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta120(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta121(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta122(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta123(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta124(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = prod_pow(z[2], 3);
z[8] = z[8] * z[10];
z[11] = prod_pow(z[2], 2) * z[5] * z[9];
z[8] = z[8] + z[11];
z[9] = z[9] * z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta125(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta126(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta127(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta128(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta129(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta130(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[1], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta131(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta132(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta133(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta134(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta135(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta136(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta137(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta138(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta139(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta140(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta141(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta142(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta143(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta144(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta145(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[1] * prod_pow(z[2], 3) * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta146(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta147(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[5] * z[9];
z[8] = z[8] + z[10];
z[8] = prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3) * z[9];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta148(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta149(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 3);
z[9] = z[6] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(3);
z[10] = prod_pow(z[0], 2) * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[1] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta150(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta151(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta152(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta153(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[9] + z[7] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[0], 2) * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[1] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 5) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta154(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta155(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta156(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta157(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta158(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 3);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 2) * z[6];
z[9] = z[9] + z[10] * T(3);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta159(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta160(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta161(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta162(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta163(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta164(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta165(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta166(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta167(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta168(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta169(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = z[1] * prod_pow(z[2], 4) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta170(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 5) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(5);
z[7] = z[0] * prod_pow(z[1], 4) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 5)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta171(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta172(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(4);
z[8] = prod_pow(z[0], 3) * z[2] * z[8];
z[9] = prod_pow(z[0], 4) * z[1] * z[5];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta173(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta174(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[8] = prod_pow(z[8], 3);
z[9] = z[3] * z[8];
z[10] = prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[6];
z[11] = prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[7];
z[9] = z[9] + z[10] + z[11];
z[10] = z[4] * z[8];
z[9] = z[10] + z[9] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta175(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2) * z[7];
z[9] = prod_pow(z[0], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[1] * z[8];
z[10] = z[6] * z[9];
z[8] = z[8] + z[10] * T(2);
z[8] = z[1] * z[2] * z[8];
z[9] = prod_pow(z[1], 2) * z[5] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta176(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[0], 2) * z[2] * z[8];
z[9] = prod_pow(z[0], 3) * z[1] * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta177(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta178(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[0] * prod_pow(z[2], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[1] * z[9];
z[8] = prod_pow(z[0], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta179(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[4] * z[8];
z[10] = z[0] * z[7];
z[9] = z[9] + z[10] * T(2);
z[10] = prod_pow(z[1], 3);
z[9] = z[9] * z[10];
z[11] = z[3] * z[10];
z[12] = prod_pow(z[1], 2) * z[6];
z[11] = z[11] + z[12];
z[11] = z[8] * z[11];
z[9] = z[9] + z[11] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8] * z[10];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta180(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[1];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[9] + z[6] * T(2);
z[9] = z[8] * z[9];
z[10] = prod_pow(z[1], 2) * z[7];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = z[5] * prod_pow(z[8], 2);
z[8] = z[9] + z[8] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 6) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta181(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[1] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[0] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[0], 2) * z[1] * z[5];
z[8] = z[8] + z[9] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta182(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta183(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta184(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[8] * z[9];
z[10] = z[0] * prod_pow(z[1], 3) * z[6];
z[9] = z[9] + z[10] * T(4);
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(5);
zd[0] = prod_pow(z[2], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * prod_pow(z[2], 5) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta185(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[9] + z[8] * T(3);
z[8] = prod_pow(z[1], 2) * z[2] * z[8];
z[9] = z[0] * prod_pow(z[1], 3) * z[5];
z[8] = z[8] + z[9] * T(6);
zd[0] = prod_pow(z[2], 5) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * prod_pow(z[2], 6) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta186(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(7);
zd[0] = prod_pow(z[2], 6) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * prod_pow(z[2], 7) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta187(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(8);
zd[0] = prod_pow(z[2], 7) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 8) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta188(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta189(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 5) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(5);
z[7] = z[0] * prod_pow(z[1], 4) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 5)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta190(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta191(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta192(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta193(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][8];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[1][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta194(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta195(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta196(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta197(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta198(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta199(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta200(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta201(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta202(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta203(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta204(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta205(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta206(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta207(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta208(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta209(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta210(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta211(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta212(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta213(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta214(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta215(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta216(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta217(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta218(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta219(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta220(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta221(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta222(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta223(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[8] * z[9];
z[10] = z[1] * z[2] * z[6];
z[9] = z[9] + z[10] * T(2);
z[9] = z[0] * z[9];
z[8] = z[2] * z[7] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta224(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[5] + z[11];
z[9] = z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((z[2] * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta225(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[0], 2);
z[9] = z[5] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[0] * z[10];
z[10] = z[10] + z[7] * T(2);
z[10] = z[0] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[1] * z[9];
z[8] = z[2] * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta226(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[7];
z[9] = z[4] + z[3] * T(3);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta227(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta228(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta229(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = z[1] * prod_pow(z[2], 2) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta230(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta231(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[8] = z[8] * T(3);
z[9] = prod_pow(z[1], 3);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = prod_pow(z[1], 2) * z[6] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[8] = z[8] + z[10];
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 2) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta232(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 2);
z[9] = prod_pow(z[0], 2);
z[10] = z[6] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(2);
z[9] = z[2] * z[9] * z[11];
z[8] = z[0] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[1] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 4) * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta233(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(2);
z[8] = z[0] * z[2] * z[8];
z[9] = prod_pow(z[2], 2);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta234(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta235(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta236(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[2] * z[3];
z[8] = z[5] + z[8];
z[8] = z[1] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[6] * z[9];
z[9] = z[1] * z[9];
z[11] = z[4] * z[9];
z[8] = z[10] + z[11] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta237(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 3);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(3);
z[9] = prod_pow(z[2], 2) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta238(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 2) * z[6];
z[9] = prod_pow(z[1], 3);
z[10] = z[3] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[5] * z[9];
z[8] = z[8] + z[10];
z[8] = prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3) * z[9];
z[10] = z[4] * z[9];
z[8] = z[10] + z[8] * T(3);
z[8] = z[0] * z[8];
z[9] = z[7] * z[9];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[0], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1] * z[2];
zd[1] = -((z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta239(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[1], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(4);
z[10] = z[0] * prod_pow(z[1], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[2] * z[9];
z[8] = prod_pow(z[0], 2) * z[5] * z[8];
z[8] = z[9] + z[8] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta240(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(3);
z[8] = z[0] * prod_pow(z[2], 2) * z[8];
z[9] = prod_pow(z[2], 3);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta241(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta242(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta243(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[1] * z[5];
z[9] = z[4] + z[3] * T(3);
z[9] = z[1] * z[9];
z[9] = z[6] + z[9];
z[9] = z[2] * z[9];
z[8] = z[9] + z[8] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = z[1] * prod_pow(z[2], 4) * z[7];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[0], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 5) * z[1] * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta244(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = prod_pow(z[1], 2);
z[10] = z[7] * z[8] * z[9];
z[11] = z[4] + z[3] * T(3);
z[11] = z[2] * z[11];
z[11] = z[11] + z[5] * T(4);
z[9] = prod_pow(z[2], 3) * z[9] * z[11];
z[8] = z[1] * z[6] * z[8];
z[8] = z[9] + z[8] * T(2);
z[8] = z[0] * z[8];
z[8] = z[8] + z[10] * T(4);
zd[0] = prod_pow(z[0], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[2];
zd[1] = -((prod_pow(z[1], 2) * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta245(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[1];
z[8] = prod_pow(z[8], 3);
z[9] = z[3] * z[8];
z[10] = prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[6];
z[11] = prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[7];
z[9] = z[9] + z[10] + z[11];
z[10] = z[4] * z[8];
z[9] = z[10] + z[9] * T(3);
z[9] = z[2] * z[9];
z[8] = z[5] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[2], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = z[0] * z[1];
zd[1] = -((prod_pow(z[2], 4) * z[3] * prod_pow(z[4], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta246(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = prod_pow(z[2], 4);
z[9] = z[7] * z[8];
z[10] = z[4] + z[3] * T(3);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(4);
z[10] = z[0] * prod_pow(z[2], 3) * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[0] * z[1] * z[9];
z[8] = prod_pow(z[0], 2) * z[6] * z[8];
z[8] = z[9] + z[8] * T(4);
zd[0] = prod_pow(z[1], 3) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = z[1] * z[2];
zd[1] = -((prod_pow(z[0], 2) * z[3] * prod_pow(z[4], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta247(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[8] + z[5] * T(4);
z[8] = z[0] * prod_pow(z[2], 3) * z[8];
z[9] = prod_pow(z[2], 4);
z[10] = z[7] * z[9];
z[8] = z[8] + z[10];
z[8] = z[1] * z[8];
z[9] = z[0] * z[6] * z[9];
z[8] = z[8] + z[9] * T(5);
zd[0] = prod_pow(z[1], 4) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * prod_pow(z[1], 5) * prod_pow(z[2], 4) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta248(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta249(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][8];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[2][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta250(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta251(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta252(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta253(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta254(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta255(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta256(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta257(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta258(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta259(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__zSurfaceIta260(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][8];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[3][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,260> TriangleBox_102ny_z__zz__zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,260> TriangleBox_102ny_z__zz__zSurfaceItaArray;
TriangleBox_102ny_z__zz__zSurfaceItaArray[0] = {TriangleBox_102ny_z__zz__zSurfaceIta1, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[1] = {TriangleBox_102ny_z__zz__zSurfaceIta2, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[2] = {TriangleBox_102ny_z__zz__zSurfaceIta3, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[3] = {TriangleBox_102ny_z__zz__zSurfaceIta4, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[4] = {TriangleBox_102ny_z__zz__zSurfaceIta5, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[5] = {TriangleBox_102ny_z__zz__zSurfaceIta6, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[6] = {TriangleBox_102ny_z__zz__zSurfaceIta7, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[7] = {TriangleBox_102ny_z__zz__zSurfaceIta8, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[8] = {TriangleBox_102ny_z__zz__zSurfaceIta9, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[9] = {TriangleBox_102ny_z__zz__zSurfaceIta10, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[10] = {TriangleBox_102ny_z__zz__zSurfaceIta11, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[11] = {TriangleBox_102ny_z__zz__zSurfaceIta12, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[12] = {TriangleBox_102ny_z__zz__zSurfaceIta13, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[13] = {TriangleBox_102ny_z__zz__zSurfaceIta14, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[14] = {TriangleBox_102ny_z__zz__zSurfaceIta15, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[15] = {TriangleBox_102ny_z__zz__zSurfaceIta16, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[16] = {TriangleBox_102ny_z__zz__zSurfaceIta17, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[17] = {TriangleBox_102ny_z__zz__zSurfaceIta18, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[18] = {TriangleBox_102ny_z__zz__zSurfaceIta19, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[19] = {TriangleBox_102ny_z__zz__zSurfaceIta20, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[20] = {TriangleBox_102ny_z__zz__zSurfaceIta21, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[21] = {TriangleBox_102ny_z__zz__zSurfaceIta22, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[22] = {TriangleBox_102ny_z__zz__zSurfaceIta23, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[23] = {TriangleBox_102ny_z__zz__zSurfaceIta24, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[24] = {TriangleBox_102ny_z__zz__zSurfaceIta25, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[25] = {TriangleBox_102ny_z__zz__zSurfaceIta26, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[26] = {TriangleBox_102ny_z__zz__zSurfaceIta27, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[27] = {TriangleBox_102ny_z__zz__zSurfaceIta28, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[28] = {TriangleBox_102ny_z__zz__zSurfaceIta29, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[29] = {TriangleBox_102ny_z__zz__zSurfaceIta30, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[30] = {TriangleBox_102ny_z__zz__zSurfaceIta31, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[31] = {TriangleBox_102ny_z__zz__zSurfaceIta32, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[32] = {TriangleBox_102ny_z__zz__zSurfaceIta33, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[33] = {TriangleBox_102ny_z__zz__zSurfaceIta34, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[34] = {TriangleBox_102ny_z__zz__zSurfaceIta35, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[35] = {TriangleBox_102ny_z__zz__zSurfaceIta36, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[36] = {TriangleBox_102ny_z__zz__zSurfaceIta37, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[37] = {TriangleBox_102ny_z__zz__zSurfaceIta38, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[38] = {TriangleBox_102ny_z__zz__zSurfaceIta39, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[39] = {TriangleBox_102ny_z__zz__zSurfaceIta40, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[40] = {TriangleBox_102ny_z__zz__zSurfaceIta41, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[41] = {TriangleBox_102ny_z__zz__zSurfaceIta42, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[42] = {TriangleBox_102ny_z__zz__zSurfaceIta43, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[43] = {TriangleBox_102ny_z__zz__zSurfaceIta44, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[44] = {TriangleBox_102ny_z__zz__zSurfaceIta45, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[45] = {TriangleBox_102ny_z__zz__zSurfaceIta46, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[46] = {TriangleBox_102ny_z__zz__zSurfaceIta47, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[47] = {TriangleBox_102ny_z__zz__zSurfaceIta48, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[48] = {TriangleBox_102ny_z__zz__zSurfaceIta49, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[49] = {TriangleBox_102ny_z__zz__zSurfaceIta50, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[50] = {TriangleBox_102ny_z__zz__zSurfaceIta51, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[51] = {TriangleBox_102ny_z__zz__zSurfaceIta52, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[52] = {TriangleBox_102ny_z__zz__zSurfaceIta53, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[53] = {TriangleBox_102ny_z__zz__zSurfaceIta54, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[54] = {TriangleBox_102ny_z__zz__zSurfaceIta55, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[55] = {TriangleBox_102ny_z__zz__zSurfaceIta56, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[56] = {TriangleBox_102ny_z__zz__zSurfaceIta57, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[57] = {TriangleBox_102ny_z__zz__zSurfaceIta58, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[58] = {TriangleBox_102ny_z__zz__zSurfaceIta59, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[59] = {TriangleBox_102ny_z__zz__zSurfaceIta60, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[60] = {TriangleBox_102ny_z__zz__zSurfaceIta61, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[61] = {TriangleBox_102ny_z__zz__zSurfaceIta62, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[62] = {TriangleBox_102ny_z__zz__zSurfaceIta63, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[63] = {TriangleBox_102ny_z__zz__zSurfaceIta64, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[64] = {TriangleBox_102ny_z__zz__zSurfaceIta65, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[65] = {TriangleBox_102ny_z__zz__zSurfaceIta66, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[66] = {TriangleBox_102ny_z__zz__zSurfaceIta67, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[67] = {TriangleBox_102ny_z__zz__zSurfaceIta68, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[68] = {TriangleBox_102ny_z__zz__zSurfaceIta69, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[69] = {TriangleBox_102ny_z__zz__zSurfaceIta70, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[70] = {TriangleBox_102ny_z__zz__zSurfaceIta71, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[71] = {TriangleBox_102ny_z__zz__zSurfaceIta72, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[72] = {TriangleBox_102ny_z__zz__zSurfaceIta73, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[73] = {TriangleBox_102ny_z__zz__zSurfaceIta74, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[74] = {TriangleBox_102ny_z__zz__zSurfaceIta75, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[75] = {TriangleBox_102ny_z__zz__zSurfaceIta76, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[76] = {TriangleBox_102ny_z__zz__zSurfaceIta77, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[77] = {TriangleBox_102ny_z__zz__zSurfaceIta78, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[78] = {TriangleBox_102ny_z__zz__zSurfaceIta79, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[79] = {TriangleBox_102ny_z__zz__zSurfaceIta80, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[80] = {TriangleBox_102ny_z__zz__zSurfaceIta81, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[81] = {TriangleBox_102ny_z__zz__zSurfaceIta82, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[82] = {TriangleBox_102ny_z__zz__zSurfaceIta83, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[83] = {TriangleBox_102ny_z__zz__zSurfaceIta84, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[84] = {TriangleBox_102ny_z__zz__zSurfaceIta85, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[85] = {TriangleBox_102ny_z__zz__zSurfaceIta86, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[86] = {TriangleBox_102ny_z__zz__zSurfaceIta87, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[87] = {TriangleBox_102ny_z__zz__zSurfaceIta88, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[88] = {TriangleBox_102ny_z__zz__zSurfaceIta89, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[89] = {TriangleBox_102ny_z__zz__zSurfaceIta90, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[90] = {TriangleBox_102ny_z__zz__zSurfaceIta91, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[91] = {TriangleBox_102ny_z__zz__zSurfaceIta92, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[92] = {TriangleBox_102ny_z__zz__zSurfaceIta93, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[93] = {TriangleBox_102ny_z__zz__zSurfaceIta94, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[94] = {TriangleBox_102ny_z__zz__zSurfaceIta95, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[95] = {TriangleBox_102ny_z__zz__zSurfaceIta96, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[96] = {TriangleBox_102ny_z__zz__zSurfaceIta97, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[97] = {TriangleBox_102ny_z__zz__zSurfaceIta98, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[98] = {TriangleBox_102ny_z__zz__zSurfaceIta99, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[99] = {TriangleBox_102ny_z__zz__zSurfaceIta100, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[100] = {TriangleBox_102ny_z__zz__zSurfaceIta101, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[101] = {TriangleBox_102ny_z__zz__zSurfaceIta102, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[102] = {TriangleBox_102ny_z__zz__zSurfaceIta103, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[103] = {TriangleBox_102ny_z__zz__zSurfaceIta104, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[104] = {TriangleBox_102ny_z__zz__zSurfaceIta105, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[105] = {TriangleBox_102ny_z__zz__zSurfaceIta106, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[106] = {TriangleBox_102ny_z__zz__zSurfaceIta107, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[107] = {TriangleBox_102ny_z__zz__zSurfaceIta108, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[108] = {TriangleBox_102ny_z__zz__zSurfaceIta109, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[109] = {TriangleBox_102ny_z__zz__zSurfaceIta110, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[110] = {TriangleBox_102ny_z__zz__zSurfaceIta111, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[111] = {TriangleBox_102ny_z__zz__zSurfaceIta112, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[112] = {TriangleBox_102ny_z__zz__zSurfaceIta113, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[113] = {TriangleBox_102ny_z__zz__zSurfaceIta114, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[114] = {TriangleBox_102ny_z__zz__zSurfaceIta115, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[115] = {TriangleBox_102ny_z__zz__zSurfaceIta116, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[116] = {TriangleBox_102ny_z__zz__zSurfaceIta117, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[117] = {TriangleBox_102ny_z__zz__zSurfaceIta118, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[118] = {TriangleBox_102ny_z__zz__zSurfaceIta119, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[119] = {TriangleBox_102ny_z__zz__zSurfaceIta120, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[120] = {TriangleBox_102ny_z__zz__zSurfaceIta121, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[121] = {TriangleBox_102ny_z__zz__zSurfaceIta122, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[122] = {TriangleBox_102ny_z__zz__zSurfaceIta123, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[123] = {TriangleBox_102ny_z__zz__zSurfaceIta124, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[124] = {TriangleBox_102ny_z__zz__zSurfaceIta125, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[125] = {TriangleBox_102ny_z__zz__zSurfaceIta126, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[126] = {TriangleBox_102ny_z__zz__zSurfaceIta127, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[127] = {TriangleBox_102ny_z__zz__zSurfaceIta128, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[128] = {TriangleBox_102ny_z__zz__zSurfaceIta129, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[129] = {TriangleBox_102ny_z__zz__zSurfaceIta130, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[130] = {TriangleBox_102ny_z__zz__zSurfaceIta131, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[131] = {TriangleBox_102ny_z__zz__zSurfaceIta132, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[132] = {TriangleBox_102ny_z__zz__zSurfaceIta133, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[133] = {TriangleBox_102ny_z__zz__zSurfaceIta134, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[134] = {TriangleBox_102ny_z__zz__zSurfaceIta135, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[135] = {TriangleBox_102ny_z__zz__zSurfaceIta136, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[136] = {TriangleBox_102ny_z__zz__zSurfaceIta137, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[137] = {TriangleBox_102ny_z__zz__zSurfaceIta138, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[138] = {TriangleBox_102ny_z__zz__zSurfaceIta139, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[139] = {TriangleBox_102ny_z__zz__zSurfaceIta140, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[140] = {TriangleBox_102ny_z__zz__zSurfaceIta141, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[141] = {TriangleBox_102ny_z__zz__zSurfaceIta142, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[142] = {TriangleBox_102ny_z__zz__zSurfaceIta143, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[143] = {TriangleBox_102ny_z__zz__zSurfaceIta144, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[144] = {TriangleBox_102ny_z__zz__zSurfaceIta145, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[145] = {TriangleBox_102ny_z__zz__zSurfaceIta146, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[146] = {TriangleBox_102ny_z__zz__zSurfaceIta147, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[147] = {TriangleBox_102ny_z__zz__zSurfaceIta148, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[148] = {TriangleBox_102ny_z__zz__zSurfaceIta149, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[149] = {TriangleBox_102ny_z__zz__zSurfaceIta150, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[150] = {TriangleBox_102ny_z__zz__zSurfaceIta151, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[151] = {TriangleBox_102ny_z__zz__zSurfaceIta152, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[152] = {TriangleBox_102ny_z__zz__zSurfaceIta153, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[153] = {TriangleBox_102ny_z__zz__zSurfaceIta154, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[154] = {TriangleBox_102ny_z__zz__zSurfaceIta155, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[155] = {TriangleBox_102ny_z__zz__zSurfaceIta156, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[156] = {TriangleBox_102ny_z__zz__zSurfaceIta157, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[157] = {TriangleBox_102ny_z__zz__zSurfaceIta158, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[158] = {TriangleBox_102ny_z__zz__zSurfaceIta159, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[159] = {TriangleBox_102ny_z__zz__zSurfaceIta160, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[160] = {TriangleBox_102ny_z__zz__zSurfaceIta161, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[161] = {TriangleBox_102ny_z__zz__zSurfaceIta162, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[162] = {TriangleBox_102ny_z__zz__zSurfaceIta163, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[163] = {TriangleBox_102ny_z__zz__zSurfaceIta164, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[164] = {TriangleBox_102ny_z__zz__zSurfaceIta165, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[165] = {TriangleBox_102ny_z__zz__zSurfaceIta166, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[166] = {TriangleBox_102ny_z__zz__zSurfaceIta167, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[167] = {TriangleBox_102ny_z__zz__zSurfaceIta168, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[168] = {TriangleBox_102ny_z__zz__zSurfaceIta169, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[169] = {TriangleBox_102ny_z__zz__zSurfaceIta170, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[170] = {TriangleBox_102ny_z__zz__zSurfaceIta171, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[171] = {TriangleBox_102ny_z__zz__zSurfaceIta172, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[172] = {TriangleBox_102ny_z__zz__zSurfaceIta173, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[173] = {TriangleBox_102ny_z__zz__zSurfaceIta174, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[174] = {TriangleBox_102ny_z__zz__zSurfaceIta175, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[175] = {TriangleBox_102ny_z__zz__zSurfaceIta176, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[176] = {TriangleBox_102ny_z__zz__zSurfaceIta177, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[177] = {TriangleBox_102ny_z__zz__zSurfaceIta178, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[178] = {TriangleBox_102ny_z__zz__zSurfaceIta179, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[179] = {TriangleBox_102ny_z__zz__zSurfaceIta180, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[180] = {TriangleBox_102ny_z__zz__zSurfaceIta181, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[181] = {TriangleBox_102ny_z__zz__zSurfaceIta182, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[182] = {TriangleBox_102ny_z__zz__zSurfaceIta183, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[183] = {TriangleBox_102ny_z__zz__zSurfaceIta184, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[184] = {TriangleBox_102ny_z__zz__zSurfaceIta185, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[185] = {TriangleBox_102ny_z__zz__zSurfaceIta186, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[186] = {TriangleBox_102ny_z__zz__zSurfaceIta187, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[187] = {TriangleBox_102ny_z__zz__zSurfaceIta188, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[188] = {TriangleBox_102ny_z__zz__zSurfaceIta189, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[189] = {TriangleBox_102ny_z__zz__zSurfaceIta190, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[190] = {TriangleBox_102ny_z__zz__zSurfaceIta191, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[191] = {TriangleBox_102ny_z__zz__zSurfaceIta192, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[192] = {TriangleBox_102ny_z__zz__zSurfaceIta193, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[193] = {TriangleBox_102ny_z__zz__zSurfaceIta194, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[194] = {TriangleBox_102ny_z__zz__zSurfaceIta195, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[195] = {TriangleBox_102ny_z__zz__zSurfaceIta196, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[196] = {TriangleBox_102ny_z__zz__zSurfaceIta197, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[197] = {TriangleBox_102ny_z__zz__zSurfaceIta198, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[198] = {TriangleBox_102ny_z__zz__zSurfaceIta199, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[199] = {TriangleBox_102ny_z__zz__zSurfaceIta200, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[200] = {TriangleBox_102ny_z__zz__zSurfaceIta201, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[201] = {TriangleBox_102ny_z__zz__zSurfaceIta202, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[202] = {TriangleBox_102ny_z__zz__zSurfaceIta203, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[203] = {TriangleBox_102ny_z__zz__zSurfaceIta204, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[204] = {TriangleBox_102ny_z__zz__zSurfaceIta205, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[205] = {TriangleBox_102ny_z__zz__zSurfaceIta206, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[206] = {TriangleBox_102ny_z__zz__zSurfaceIta207, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[207] = {TriangleBox_102ny_z__zz__zSurfaceIta208, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[208] = {TriangleBox_102ny_z__zz__zSurfaceIta209, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[209] = {TriangleBox_102ny_z__zz__zSurfaceIta210, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[210] = {TriangleBox_102ny_z__zz__zSurfaceIta211, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[211] = {TriangleBox_102ny_z__zz__zSurfaceIta212, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[212] = {TriangleBox_102ny_z__zz__zSurfaceIta213, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[213] = {TriangleBox_102ny_z__zz__zSurfaceIta214, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[214] = {TriangleBox_102ny_z__zz__zSurfaceIta215, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[215] = {TriangleBox_102ny_z__zz__zSurfaceIta216, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[216] = {TriangleBox_102ny_z__zz__zSurfaceIta217, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[217] = {TriangleBox_102ny_z__zz__zSurfaceIta218, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[218] = {TriangleBox_102ny_z__zz__zSurfaceIta219, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[219] = {TriangleBox_102ny_z__zz__zSurfaceIta220, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[220] = {TriangleBox_102ny_z__zz__zSurfaceIta221, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[221] = {TriangleBox_102ny_z__zz__zSurfaceIta222, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[222] = {TriangleBox_102ny_z__zz__zSurfaceIta223, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[223] = {TriangleBox_102ny_z__zz__zSurfaceIta224, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[224] = {TriangleBox_102ny_z__zz__zSurfaceIta225, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[225] = {TriangleBox_102ny_z__zz__zSurfaceIta226, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[226] = {TriangleBox_102ny_z__zz__zSurfaceIta227, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[227] = {TriangleBox_102ny_z__zz__zSurfaceIta228, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[228] = {TriangleBox_102ny_z__zz__zSurfaceIta229, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[229] = {TriangleBox_102ny_z__zz__zSurfaceIta230, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[230] = {TriangleBox_102ny_z__zz__zSurfaceIta231, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[231] = {TriangleBox_102ny_z__zz__zSurfaceIta232, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[232] = {TriangleBox_102ny_z__zz__zSurfaceIta233, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[233] = {TriangleBox_102ny_z__zz__zSurfaceIta234, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[234] = {TriangleBox_102ny_z__zz__zSurfaceIta235, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[235] = {TriangleBox_102ny_z__zz__zSurfaceIta236, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[236] = {TriangleBox_102ny_z__zz__zSurfaceIta237, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[237] = {TriangleBox_102ny_z__zz__zSurfaceIta238, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[238] = {TriangleBox_102ny_z__zz__zSurfaceIta239, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[239] = {TriangleBox_102ny_z__zz__zSurfaceIta240, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[240] = {TriangleBox_102ny_z__zz__zSurfaceIta241, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[241] = {TriangleBox_102ny_z__zz__zSurfaceIta242, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[242] = {TriangleBox_102ny_z__zz__zSurfaceIta243, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[243] = {TriangleBox_102ny_z__zz__zSurfaceIta244, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[244] = {TriangleBox_102ny_z__zz__zSurfaceIta245, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[245] = {TriangleBox_102ny_z__zz__zSurfaceIta246, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[246] = {TriangleBox_102ny_z__zz__zSurfaceIta247, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[247] = {TriangleBox_102ny_z__zz__zSurfaceIta248, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[248] = {TriangleBox_102ny_z__zz__zSurfaceIta249, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[249] = {TriangleBox_102ny_z__zz__zSurfaceIta250, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[250] = {TriangleBox_102ny_z__zz__zSurfaceIta251, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[251] = {TriangleBox_102ny_z__zz__zSurfaceIta252, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[252] = {TriangleBox_102ny_z__zz__zSurfaceIta253, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[253] = {TriangleBox_102ny_z__zz__zSurfaceIta254, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[254] = {TriangleBox_102ny_z__zz__zSurfaceIta255, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[255] = {TriangleBox_102ny_z__zz__zSurfaceIta256, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[256] = {TriangleBox_102ny_z__zz__zSurfaceIta257, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[257] = {TriangleBox_102ny_z__zz__zSurfaceIta258, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[258] = {TriangleBox_102ny_z__zz__zSurfaceIta259, -1};
TriangleBox_102ny_z__zz__zSurfaceItaArray[259] = {TriangleBox_102ny_z__zz__zSurfaceIta260, -1};
return TriangleBox_102ny_z__zz__zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,260> TriangleBox_102ny_z__zz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,260> TriangleBox_102ny_z__zz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,260> TriangleBox_102ny_z__zz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,260> TriangleBox_102ny_z__zz__zSurfaceIta<F32>();

#endif

}}}}

