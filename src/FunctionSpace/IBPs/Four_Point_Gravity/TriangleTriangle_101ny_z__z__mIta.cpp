#include "FunctionSpace/IBPhelper.h"

#include "TriangleTriangle_101ny_z__z__mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,60> TriangleTriangle_101ny_z__z__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,60> TriangleTriangle_101ny_z__z__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,60> TriangleTriangle_101ny_z__z__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,60> TriangleTriangle_101ny_z__z__mSurfaceIta<F32>();

#endif

}}}}

