#ifndef BubbleBox_002nn___mm__Vectors_H_INC

#define BubbleBox_002nn___mm__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> T BubbleBox_002nn___mm__vec1_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T BubbleBox_002nn___mm__vec1_component4(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[2];
z[2] = -z[0] + -z[1];
return z[2] * T(2);
}

template<typename T> T BubbleBox_002nn___mm__vec1_divergence(const T* invariants, const T* D)
{
T z[2];
z[0] = D[0];
return z[0] * T(-2);
}

template<typename T> T BubbleBox_002nn___mm__vec2_component1(const T* invariants, const T* D)
{
T z[2];
z[0] = D[0];
z[1] = z[0] * T(-2) + T(-1);
return z[0] * z[1];
}

template<typename T> T BubbleBox_002nn___mm__vec2_component4(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[2];
z[2] = z[1] + T(1) + z[0] * T(4);
return z[2] * T(2);
}

template<typename T> T BubbleBox_002nn___mm__vec2_divergence(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> std::vector<std::vector<T>> BubbleBox_002nn___mm___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(2, std::vector<T>(5, T(0)));
ibp_vectors[0][3] = BubbleBox_002nn___mm__vec1_component4(invariants, propagators);
ibp_vectors[0].back() = BubbleBox_002nn___mm__vec1_divergence(invariants, propagators);
ibp_vectors[1][0] = BubbleBox_002nn___mm__vec2_component1(invariants, propagators);
ibp_vectors[1][3] = BubbleBox_002nn___mm__vec2_component4(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BubbleBox_002nn___mm___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BubbleBox_002nn___mm___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BubbleBox_002nn___mm___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BubbleBox_002nn___mm___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

