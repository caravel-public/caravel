#include "FunctionSpace/IBPhelper.h"

#include "TriangleBox_102ny_z__zz__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,260> TriangleBox_102ny_z__zz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,260> TriangleBox_102ny_z__zz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,260> TriangleBox_102ny_z__zz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,260> TriangleBox_102ny_z__zz__zSurfaceIta<F32>();

#endif

}}}}

