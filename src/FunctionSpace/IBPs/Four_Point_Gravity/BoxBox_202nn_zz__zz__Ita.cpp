#include "FunctionSpace/IBPhelper.h"

#include "BoxBox_202nn_zz__zz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template std::array<SurfaceTermsFunctionPointer<C>,69> BoxBox_202nn_zz__zz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,69> BoxBox_202nn_zz__zz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,69> BoxBox_202nn_zz__zz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,69> BoxBox_202nn_zz__zz__SurfaceIta<F32>();

#endif

}}}}

