/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> TriangleTriangle_101nn_m__m__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][5];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,1> TriangleTriangle_101nn_m__m__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,1> TriangleTriangle_101nn_m__m__SurfaceItaArray;
TriangleTriangle_101nn_m__m__SurfaceItaArray[0] = {TriangleTriangle_101nn_m__m__SurfaceIta1, -1};
return TriangleTriangle_101nn_m__m__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,1> TriangleTriangle_101nn_m__m__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,1> TriangleTriangle_101nn_m__m__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,1> TriangleTriangle_101nn_m__m__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,1> TriangleTriangle_101nn_m__m__SurfaceIta<F32>();

#endif

}}}}

