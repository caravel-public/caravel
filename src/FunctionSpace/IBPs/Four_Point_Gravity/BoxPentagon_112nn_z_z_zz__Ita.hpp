/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][9];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[1][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 3)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[0], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 4)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(9);
zd[0] = prod_pow(z[0], 8) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 9) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 5) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[1], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 5) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(9);
zd[0] = prod_pow(z[0], 8) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 9) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(10);
zd[0] = prod_pow(z[0], 9) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 10) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(9);
zd[0] = prod_pow(z[0], 8) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 9) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[0], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 8) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 3) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(3);
z[7] = z[0] * prod_pow(z[1], 2) * z[7];
z[6] = z[7] + z[6] * T(7);
zd[0] = prod_pow(z[0], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 7) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 4) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(4);
z[7] = z[0] * prod_pow(z[1], 3) * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[0], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 6) * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 5) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(5);
z[7] = z[0] * prod_pow(z[1], 4) * z[7];
z[6] = z[7] + z[6] * T(5);
zd[0] = prod_pow(z[0], 4) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 5)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 4) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(4);
z[7] = prod_pow(z[0], 3) * z[1] * z[7];
z[6] = z[7] + z[6] * T(6);
zd[0] = prod_pow(z[1], 5) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 4) * prod_pow(z[1], 6) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[5];
z[7] = prod_pow(z[0], 3);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[8] = z[3] * z[7];
z[6] = z[8] + z[6] * T(3);
z[6] = z[1] * z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + z[7] * T(7);
zd[0] = prod_pow(z[1], 6) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 7) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(8);
zd[0] = prod_pow(z[1], 7) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 8) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(9);
zd[0] = prod_pow(z[1], 8) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 9) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(10);
zd[0] = prod_pow(z[0], 9) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 10) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(5);
zd[0] = prod_pow(z[0], 4) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 5) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(6);
zd[0] = prod_pow(z[0], 5) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 6) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(7);
zd[0] = prod_pow(z[0], 6) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 7) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(8);
zd[0] = prod_pow(z[0], 7) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 8) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(9);
zd[0] = prod_pow(z[0], 8) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 9) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][9];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[4][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,76> BoxPentagon_112nn_z_z_zz__SurfaceItaArray;
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[0] = {BoxPentagon_112nn_z_z_zz__SurfaceIta1, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[1] = {BoxPentagon_112nn_z_z_zz__SurfaceIta2, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[2] = {BoxPentagon_112nn_z_z_zz__SurfaceIta3, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[3] = {BoxPentagon_112nn_z_z_zz__SurfaceIta4, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[4] = {BoxPentagon_112nn_z_z_zz__SurfaceIta5, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[5] = {BoxPentagon_112nn_z_z_zz__SurfaceIta6, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[6] = {BoxPentagon_112nn_z_z_zz__SurfaceIta7, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[7] = {BoxPentagon_112nn_z_z_zz__SurfaceIta8, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[8] = {BoxPentagon_112nn_z_z_zz__SurfaceIta9, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[9] = {BoxPentagon_112nn_z_z_zz__SurfaceIta10, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[10] = {BoxPentagon_112nn_z_z_zz__SurfaceIta11, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[11] = {BoxPentagon_112nn_z_z_zz__SurfaceIta12, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[12] = {BoxPentagon_112nn_z_z_zz__SurfaceIta13, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[13] = {BoxPentagon_112nn_z_z_zz__SurfaceIta14, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[14] = {BoxPentagon_112nn_z_z_zz__SurfaceIta15, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[15] = {BoxPentagon_112nn_z_z_zz__SurfaceIta16, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[16] = {BoxPentagon_112nn_z_z_zz__SurfaceIta17, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[17] = {BoxPentagon_112nn_z_z_zz__SurfaceIta18, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[18] = {BoxPentagon_112nn_z_z_zz__SurfaceIta19, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[19] = {BoxPentagon_112nn_z_z_zz__SurfaceIta20, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[20] = {BoxPentagon_112nn_z_z_zz__SurfaceIta21, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[21] = {BoxPentagon_112nn_z_z_zz__SurfaceIta22, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[22] = {BoxPentagon_112nn_z_z_zz__SurfaceIta23, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[23] = {BoxPentagon_112nn_z_z_zz__SurfaceIta24, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[24] = {BoxPentagon_112nn_z_z_zz__SurfaceIta25, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[25] = {BoxPentagon_112nn_z_z_zz__SurfaceIta26, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[26] = {BoxPentagon_112nn_z_z_zz__SurfaceIta27, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[27] = {BoxPentagon_112nn_z_z_zz__SurfaceIta28, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[28] = {BoxPentagon_112nn_z_z_zz__SurfaceIta29, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[29] = {BoxPentagon_112nn_z_z_zz__SurfaceIta30, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[30] = {BoxPentagon_112nn_z_z_zz__SurfaceIta31, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[31] = {BoxPentagon_112nn_z_z_zz__SurfaceIta32, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[32] = {BoxPentagon_112nn_z_z_zz__SurfaceIta33, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[33] = {BoxPentagon_112nn_z_z_zz__SurfaceIta34, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[34] = {BoxPentagon_112nn_z_z_zz__SurfaceIta35, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[35] = {BoxPentagon_112nn_z_z_zz__SurfaceIta36, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[36] = {BoxPentagon_112nn_z_z_zz__SurfaceIta37, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[37] = {BoxPentagon_112nn_z_z_zz__SurfaceIta38, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[38] = {BoxPentagon_112nn_z_z_zz__SurfaceIta39, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[39] = {BoxPentagon_112nn_z_z_zz__SurfaceIta40, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[40] = {BoxPentagon_112nn_z_z_zz__SurfaceIta41, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[41] = {BoxPentagon_112nn_z_z_zz__SurfaceIta42, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[42] = {BoxPentagon_112nn_z_z_zz__SurfaceIta43, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[43] = {BoxPentagon_112nn_z_z_zz__SurfaceIta44, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[44] = {BoxPentagon_112nn_z_z_zz__SurfaceIta45, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[45] = {BoxPentagon_112nn_z_z_zz__SurfaceIta46, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[46] = {BoxPentagon_112nn_z_z_zz__SurfaceIta47, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[47] = {BoxPentagon_112nn_z_z_zz__SurfaceIta48, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[48] = {BoxPentagon_112nn_z_z_zz__SurfaceIta49, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[49] = {BoxPentagon_112nn_z_z_zz__SurfaceIta50, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[50] = {BoxPentagon_112nn_z_z_zz__SurfaceIta51, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[51] = {BoxPentagon_112nn_z_z_zz__SurfaceIta52, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[52] = {BoxPentagon_112nn_z_z_zz__SurfaceIta53, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[53] = {BoxPentagon_112nn_z_z_zz__SurfaceIta54, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[54] = {BoxPentagon_112nn_z_z_zz__SurfaceIta55, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[55] = {BoxPentagon_112nn_z_z_zz__SurfaceIta56, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[56] = {BoxPentagon_112nn_z_z_zz__SurfaceIta57, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[57] = {BoxPentagon_112nn_z_z_zz__SurfaceIta58, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[58] = {BoxPentagon_112nn_z_z_zz__SurfaceIta59, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[59] = {BoxPentagon_112nn_z_z_zz__SurfaceIta60, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[60] = {BoxPentagon_112nn_z_z_zz__SurfaceIta61, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[61] = {BoxPentagon_112nn_z_z_zz__SurfaceIta62, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[62] = {BoxPentagon_112nn_z_z_zz__SurfaceIta63, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[63] = {BoxPentagon_112nn_z_z_zz__SurfaceIta64, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[64] = {BoxPentagon_112nn_z_z_zz__SurfaceIta65, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[65] = {BoxPentagon_112nn_z_z_zz__SurfaceIta66, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[66] = {BoxPentagon_112nn_z_z_zz__SurfaceIta67, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[67] = {BoxPentagon_112nn_z_z_zz__SurfaceIta68, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[68] = {BoxPentagon_112nn_z_z_zz__SurfaceIta69, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[69] = {BoxPentagon_112nn_z_z_zz__SurfaceIta70, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[70] = {BoxPentagon_112nn_z_z_zz__SurfaceIta71, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[71] = {BoxPentagon_112nn_z_z_zz__SurfaceIta72, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[72] = {BoxPentagon_112nn_z_z_zz__SurfaceIta73, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[73] = {BoxPentagon_112nn_z_z_zz__SurfaceIta74, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[74] = {BoxPentagon_112nn_z_z_zz__SurfaceIta75, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[75] = {BoxPentagon_112nn_z_z_zz__SurfaceIta76, -1};
return BoxPentagon_112nn_z_z_zz__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,76> BoxPentagon_112nn_z_z_zz__SurfaceIta<F32>();

#endif

}}}}

