namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> std::vector<std::vector<T>> BubbleBox_002nn___mm___vectors(const T* , const T* , const T* );

template<typename T, size_t D> IBPCoordinates<T> BubbleBox_002nn___mm__Coordinates(const OnShellPoint<T,D>& l, const std::vector<momentumD<T,D>>&  P, const std::vector<std::vector<T>>& ccoeffs)
{
auto invariant1 = P[1] * P[1];
auto invdimension = T(1) / invariant1;
std::vector<T> invariants = {invdimension * invariant1, invariant1};
auto D1mom = l[0] + -P[1];
auto D1 = invdimension * (D1mom * D1mom);
auto D2mom = l[0];
auto D2 = invdimension * (D2mom * D2mom);
auto D3mom = l[1];
auto D3 = invdimension * (D3mom * D3mom);
auto D4mom = l[1] + P[1];
auto D4 = invdimension * (D4mom * D4mom);
auto D5mom = l[0] + -l[1];
auto D5 = invdimension * (D5mom * D5mom);
auto alpha1 = (-D1 + invariants[0] + D2) / T(2);
std::vector<T> props = {alpha1, D2, D3, D4, D5};
auto vector_components = BubbleBox_002nn___mm___vectors(&props[0], &invariants[0],ccoeffs);
return IBPCoordinates<T>{props, invariants, vector_components};
}

}}}}

