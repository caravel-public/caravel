#ifndef TrianglePentagon_103nn_z__zzz__Vectors_H_INC

#define TrianglePentagon_103nn_z__zzz__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Four_Point_Gravity{

template<typename T> T TrianglePentagon_103nn_z__zzz__vec1_component1(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = T(-1) + -z[0] + -z[1];
z[3] = z[0] * z[3];
return z[2] + z[3] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec1_component2(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = T(-1) + -z[0] + -z[1];
z[3] = z[1] * z[3];
return z[2] + z[3] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec1_component9(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = D[6];
z[4] = z[0] + z[1] + T(1);
z[4] = z[3] + z[4] * T(2) + -z[2];
return z[4] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec1_divergence(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = D[6];
z[4] = z[0] + z[1];
return z[2] + z[4] * T(-6) + T(-4) + -z[3];
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec2_component1(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = invariants[1];
z[5] = T(1) + -z[4];
z[5] = z[2] * z[5];
z[6] = T(-1) + -z[1] + -z[4];
z[6] = z[2] + z[6] * T(2) + -z[3];
z[6] = z[0] * z[6];
return z[5] + z[6];
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec2_component2(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[2];
z[4] = D[3];
z[5] = z[1] + T(1);
z[5] = z[0] * z[5];
z[6] = z[2] + T(1);
z[7] = -(z[2] * z[6]);
z[5] = z[5] + z[7];
z[6] = -(z[4] * z[6]);
z[7] = z[1] + z[2] + T(2);
z[7] = z[3] * z[7];
return z[6] + z[7] + z[5] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec2_component9(const T* invariants, const T* D)
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = z[2] + z[5] + z[6] + T(2) + z[0] * T(2) + -z[1] + -z[3] + -z[4];
return z[7] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec2_divergence(const T* invariants, const T* D)
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = invariants[1];
z[8] = z[1] + -z[2];
return z[3] + z[4] + z[0] * T(-6) + T(-4) + z[7] * T(-2) + z[8] * T(3) + -z[5] + -z[6];
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec4_component1(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[1];
z[4] = D[2];
z[5] = D[4];
z[6] = D[5];
z[7] = D[3];
z[8] = z[3] * T(-2) + T(-1) + -z[4];
z[8] = z[5] * z[8];
z[9] = z[3] + z[4];
z[10] = z[6] * z[9];
z[8] = z[8] + z[10];
z[8] = z[2] * z[8];
z[10] = z[0] * T(2);
z[11] = T(-2) + -z[1] + -z[2];
z[11] = z[10] * z[11];
z[9] = T(1) + -z[5] + -z[9];
z[9] = z[6] + z[9] * T(2);
z[9] = z[2] * z[9];
z[9] = z[6] + z[9] + z[11] + -z[1] + -z[5];
z[9] = z[9] * z[10];
z[10] = z[4] + -z[7];
z[10] = z[5] * z[10];
return z[9] + z[10] + z[8] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec4_component2(const T* invariants, const T* D)
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[7];
z[3] = invariants[1];
z[4] = D[5];
z[5] = D[6];
z[6] = D[2];
z[7] = D[4];
z[8] = D[3];
z[9] = -z[1] + -z[6];
z[10] = z[1] * T(2);
z[11] = z[4] + z[10] + T(2);
z[9] = z[9] * z[11];
z[11] = z[4] + T(4);
z[12] = -z[10] + -z[11];
z[12] = z[0] * z[12];
z[13] = z[0] + z[1];
z[13] = z[6] + z[13] * T(2);
z[13] = z[5] * z[13];
z[9] = z[9] + z[12] + z[13] + -z[7];
z[9] = z[3] * z[9];
z[12] = T(-2) + -z[2];
z[10] = z[10] * z[12];
z[10] = z[10] + -z[11];
z[10] = z[0] * z[10];
z[11] = z[2] + -z[7];
z[11] = z[1] * z[11];
z[12] = z[2] * z[6];
z[9] = z[8] + z[9] + z[10] + z[11] + z[12] + -z[7];
z[10] = z[6] + z[0] * T(4) + -z[8];
z[10] = z[5] * z[10];
return z[10] + z[9] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec4_component9(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[1];
z[4] = D[2];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = z[1] + T(2);
z[8] = z[0] * z[8];
z[9] = z[0] + z[3] + z[4] + z[5] + -z[7];
z[9] = z[2] * z[9];
z[8] = z[8] + z[9];
z[9] = z[6] + -z[5];
z[9] = z[1] * z[9];
z[8] = z[5] + z[6] + z[9] + z[8] * T(2);
return z[8] * T(4);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec4_divergence(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[1];
z[4] = D[2];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = z[5] + z[0] * T(-3) + -z[6];
z[8] = z[1] * z[8];
z[8] = z[8] + z[0] * T(-6) + -z[5];
z[9] = -z[0] + -z[3] + -z[4];
z[9] = z[7] + z[9] * T(2) + -z[5];
z[9] = z[2] * z[9];
z[8] = z[8] * T(2) + z[9] * T(3);
return z[8] * T(2);
}

template<typename T> std::vector<std::vector<T>> TrianglePentagon_103nn_z__zzz___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(4, std::vector<T>(10, T(0)));
ibp_vectors[0][0] = TrianglePentagon_103nn_z__zzz__vec1_component1(invariants, propagators);
ibp_vectors[0][1] = TrianglePentagon_103nn_z__zzz__vec1_component2(invariants, propagators);
ibp_vectors[0][8] = TrianglePentagon_103nn_z__zzz__vec1_component9(invariants, propagators);
ibp_vectors[0].back() = TrianglePentagon_103nn_z__zzz__vec1_divergence(invariants, propagators);
ibp_vectors[1][0] = TrianglePentagon_103nn_z__zzz__vec2_component1(invariants, propagators);
ibp_vectors[1][1] = TrianglePentagon_103nn_z__zzz__vec2_component2(invariants, propagators);
ibp_vectors[1][8] = TrianglePentagon_103nn_z__zzz__vec2_component9(invariants, propagators);
ibp_vectors[1].back() = TrianglePentagon_103nn_z__zzz__vec2_divergence(invariants, propagators);
ibp_vectors[3][0] = TrianglePentagon_103nn_z__zzz__vec4_component1(invariants, propagators);
ibp_vectors[3][1] = TrianglePentagon_103nn_z__zzz__vec4_component2(invariants, propagators);
ibp_vectors[3][8] = TrianglePentagon_103nn_z__zzz__vec4_component9(invariants, propagators);
ibp_vectors[3].back() = TrianglePentagon_103nn_z__zzz__vec4_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TrianglePentagon_103nn_z__zzz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TrianglePentagon_103nn_z__zzz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TrianglePentagon_103nn_z__zzz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TrianglePentagon_103nn_z__zzz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

