#include "FunctionSpace/IBPhelper.h"

#include "BoxBox_202ny_zz__zz__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,157> BoxBox_202ny_zz__zz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,157> BoxBox_202ny_zz__zz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,157> BoxBox_202ny_zz__zz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,157> BoxBox_202ny_zz__zz__zSurfaceIta<F32>();

#endif

}}}}

