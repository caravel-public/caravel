#ifndef BubbleHexagon_004ny___zzzz__zVectors_H_INC

#define BubbleHexagon_004ny___zzzz__zVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BubbleHexagon_004ny___zzzz__zvec1_component1(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[5];
z[2] = D[6];
z[3] = z[0] * T(2);
z[4] = z[2] + -z[1] + -z[3];
return z[3] * z[4];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec1_component2(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[4];
z[5] = D[5];
z[6] = -(z[4] * z[5]);
z[7] = z[2] + -z[5];
z[7] = z[1] * z[7];
z[8] = z[3] + z[1] * T(-4) + z[4] * T(-2) + -z[2];
z[8] = z[0] * z[8];
return z[6] + z[7] + z[8];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec1_component3(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[7];
z[3] = D[8];
z[4] = invariants[4];
z[5] = D[5];
z[6] = D[6];
z[7] = invariants[3];
z[8] = z[4] + T(-1);
z[9] = z[7] + z[8];
z[9] = z[5] * z[9];
z[8] = z[3] + z[8] * T(2) + -z[2];
z[8] = z[0] * z[8];
z[10] = z[6] + z[0] * T(-4) + -z[5];
z[10] = z[1] * z[10];
return z[8] + z[9] + z[10];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec1_component4(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[3];
z[2] = D[8];
z[3] = D[9];
z[4] = D[5];
z[5] = D[6];
z[6] = invariants[1];
z[7] = invariants[3];
z[8] = z[1] * z[5];
z[9] = z[3] + z[1] * T(-4) + T(2) + -z[2];
z[9] = z[0] * z[9];
z[10] = z[6] + T(1) + -z[1] + -z[7];
z[10] = z[4] * z[10];
return z[8] + z[9] + z[10];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec1_component10(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[5];
z[2] = D[6];
z[3] = z[1] + z[0] * T(4) + -z[2];
return z[3] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[5];
z[2] = D[6];
z[3] = z[2] + z[0] * T(-4) + -z[1];
return z[3] * T(5);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec2_component1(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[3];
z[2] = D[8];
z[3] = D[9];
z[4] = D[5];
z[5] = D[6];
z[6] = invariants[1];
z[7] = invariants[3];
z[8] = z[1] * z[5];
z[9] = z[3] + z[1] * T(-4) + T(2) + -z[2];
z[9] = z[0] * z[9];
z[10] = z[6] + T(1) + -z[1] + -z[7];
z[10] = z[4] * z[10];
return z[8] + z[9] + z[10];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec2_component2(const T* invariants, const T* D)
{
T z[14];
z[0] = D[1];
z[1] = D[3];
z[2] = D[8];
z[3] = D[9];
z[4] = D[6];
z[5] = D[7];
z[6] = invariants[4];
z[7] = D[5];
z[8] = invariants[1];
z[9] = invariants[2];
z[10] = invariants[3];
z[11] = z[9] + z[10] + -z[8];
z[11] = z[7] * z[11];
z[12] = z[5] + z[6] * T(-2) + -z[4];
z[12] = z[1] * z[12];
z[13] = z[3] + z[1] * T(-4) + T(2) + -z[2];
z[13] = z[0] * z[13];
return z[11] + z[12] + z[13];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec2_component3(const T* invariants, const T* D)
{
T z[11];
z[0] = D[2];
z[1] = D[3];
z[2] = D[8];
z[3] = D[9];
z[4] = D[7];
z[5] = invariants[4];
z[6] = D[5];
z[7] = invariants[2];
z[8] = -(z[6] * z[7]);
z[9] = z[3] + T(2) + -z[2];
z[9] = z[0] * z[9];
z[10] = z[5] + T(-1);
z[10] = z[2] + z[0] * T(-4) + z[10] * T(2) + -z[4];
z[10] = z[1] * z[10];
return z[8] + z[9] + z[10];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec2_component4(const T* invariants, const T* D)
{
T z[5];
z[0] = D[3];
z[1] = D[8];
z[2] = D[9];
z[3] = z[0] * T(2);
z[4] = z[2] + T(2) + -z[1] + -z[3];
return z[3] * z[4];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec2_component10(const T* invariants, const T* D)
{
T z[4];
z[0] = D[3];
z[1] = D[8];
z[2] = D[9];
z[3] = z[1] + T(-2) + z[0] * T(4) + -z[2];
return z[3] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec2_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[3];
z[1] = D[8];
z[2] = D[9];
z[3] = z[2] + z[0] * T(-4) + T(2) + -z[1];
return z[3] * T(5);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec3_component1(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[4];
z[5] = D[5];
z[6] = -(z[4] * z[5]);
z[7] = z[2] + -z[5];
z[7] = z[1] * z[7];
z[8] = z[3] + z[1] * T(-4) + z[4] * T(-2) + -z[2];
z[8] = z[0] * z[8];
return z[6] + z[7] + z[8];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec3_component2(const T* invariants, const T* D)
{
T z[6];
z[0] = D[1];
z[1] = D[6];
z[2] = D[7];
z[3] = invariants[4];
z[4] = z[0] * T(2);
z[5] = z[2] + z[3] * T(-2) + -z[1] + -z[4];
return z[4] * z[5];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec3_component3(const T* invariants, const T* D)
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[7];
z[3] = D[8];
z[4] = invariants[4];
z[5] = D[6];
z[6] = D[5];
z[7] = invariants[3];
z[8] = z[4] * T(2) + -z[2];
z[9] = -z[5] + -z[8];
z[9] = z[1] * z[9];
z[10] = -(z[6] * z[7]);
z[8] = z[3] + z[8] + z[1] * T(-4) + T(-2);
z[8] = z[0] * z[8];
return z[8] + z[9] + z[10];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec3_component4(const T* invariants, const T* D)
{
T z[14];
z[0] = D[1];
z[1] = D[3];
z[2] = D[8];
z[3] = D[9];
z[4] = D[6];
z[5] = D[7];
z[6] = invariants[4];
z[7] = D[5];
z[8] = invariants[1];
z[9] = invariants[2];
z[10] = invariants[3];
z[11] = z[9] + z[10] + -z[8];
z[11] = z[7] * z[11];
z[12] = z[5] + z[6] * T(-2) + -z[4];
z[12] = z[1] * z[12];
z[13] = z[3] + z[1] * T(-4) + T(2) + -z[2];
z[13] = z[0] * z[13];
return z[11] + z[12] + z[13];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec3_component10(const T* invariants, const T* D)
{
T z[5];
z[0] = D[1];
z[1] = D[6];
z[2] = D[7];
z[3] = invariants[4];
z[4] = z[1] + z[3] * T(2) + z[0] * T(4) + -z[2];
return z[4] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec3_divergence(const T* invariants, const T* D)
{
T z[5];
z[0] = D[1];
z[1] = D[6];
z[2] = D[7];
z[3] = invariants[4];
z[4] = z[2] + z[0] * T(-4) + z[3] * T(-2) + -z[1];
return z[4] * T(5);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec4_component1(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[7];
z[3] = D[8];
z[4] = invariants[4];
z[5] = D[5];
z[6] = D[6];
z[7] = invariants[3];
z[8] = z[4] + T(-1);
z[9] = z[7] + z[8];
z[9] = z[5] * z[9];
z[8] = z[3] + z[8] * T(2) + -z[2];
z[8] = z[0] * z[8];
z[10] = z[6] + z[0] * T(-4) + -z[5];
z[10] = z[1] * z[10];
return z[8] + z[9] + z[10];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec4_component2(const T* invariants, const T* D)
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = D[7];
z[3] = D[8];
z[4] = invariants[4];
z[5] = D[6];
z[6] = D[5];
z[7] = invariants[3];
z[8] = z[4] * T(2) + -z[2];
z[9] = -z[5] + -z[8];
z[9] = z[1] * z[9];
z[10] = -(z[6] * z[7]);
z[8] = z[3] + z[8] + z[1] * T(-4) + T(-2);
z[8] = z[0] * z[8];
return z[8] + z[9] + z[10];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec4_component3(const T* invariants, const T* D)
{
T z[6];
z[0] = D[2];
z[1] = D[7];
z[2] = D[8];
z[3] = invariants[4];
z[4] = z[0] * T(2);
z[5] = z[2] + T(-2) + z[3] * T(2) + -z[1] + -z[4];
return z[4] * z[5];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec4_component4(const T* invariants, const T* D)
{
T z[11];
z[0] = D[2];
z[1] = D[3];
z[2] = D[8];
z[3] = D[9];
z[4] = D[7];
z[5] = invariants[4];
z[6] = D[5];
z[7] = invariants[2];
z[8] = -(z[6] * z[7]);
z[9] = z[3] + T(2) + -z[2];
z[9] = z[0] * z[9];
z[10] = z[5] + T(-1);
z[10] = z[2] + z[0] * T(-4) + z[10] * T(2) + -z[4];
z[10] = z[1] * z[10];
return z[8] + z[9] + z[10];
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec4_component10(const T* invariants, const T* D)
{
T z[5];
z[0] = D[2];
z[1] = D[7];
z[2] = D[8];
z[3] = invariants[4];
z[4] = T(1) + z[0] * T(2) + -z[3];
z[4] = z[1] + z[4] * T(2) + -z[2];
return z[4] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec4_divergence(const T* invariants, const T* D)
{
T z[5];
z[0] = D[2];
z[1] = D[7];
z[2] = D[8];
z[3] = invariants[4];
z[4] = z[3] + z[0] * T(-2) + T(-1);
z[4] = z[2] + z[4] * T(2) + -z[1];
return z[4] * T(5);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec17_component1(const T* invariants, const T* D)
{
T z[22];
z[0] = D[0];
z[1] = D[5];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = invariants[3];
z[5] = D[6];
z[6] = D[7];
z[7] = D[1];
z[8] = D[8];
z[9] = invariants[4];
z[10] = D[2];
z[11] = D[3];
z[12] = D[9];
z[13] = z[2] + T(2) + -z[9];
z[14] = z[5] + -z[6];
z[15] = z[8] + z[14] + -z[13];
z[15] = z[7] * z[15];
z[16] = z[3] + -z[2];
z[17] = z[14] + z[16];
z[18] = z[0] * T(2);
z[17] = z[17] * z[18];
z[19] = z[12] + z[11] * T(-2);
z[19] = z[9] * z[19];
z[20] = z[6] + z[9] * T(-2);
z[20] = z[2] * z[20];
z[21] = z[9] + -z[3];
z[21] = z[5] * z[21];
z[15] = z[17] + z[19] + z[20] + z[21] + z[15] * T(2);
z[15] = z[0] * z[15];
z[16] = z[4] + z[16];
z[17] = z[0] * z[16];
z[19] = z[9] * z[11];
z[20] = -(z[4] * z[11]);
z[16] = T(-1) + -z[16];
z[16] = z[7] * z[16];
z[16] = z[16] + z[20] + -z[17] + -z[19];
z[16] = z[0] * z[16];
z[13] = z[4] * T(2) + -z[13];
z[13] = z[7] * z[13];
z[17] = z[9] + -z[17];
z[17] = z[10] * z[17];
z[13] = z[13] + z[16] + z[17] + -z[19];
z[13] = z[1] * z[13];
z[14] = z[9] + z[14];
z[14] = z[10] * z[14] * z[18];
z[13] = z[13] + z[14] + z[15];
return z[13] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec17_component2(const T* invariants, const T* D)
{
T z[20];
z[0] = D[0];
z[1] = D[1];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = invariants[3];
z[6] = D[6];
z[7] = D[7];
z[8] = invariants[4];
z[9] = D[8];
z[10] = D[2];
z[11] = D[3];
z[12] = D[9];
z[13] = z[4] + -z[3];
z[14] = z[5] + z[13];
z[15] = z[0] + z[10];
z[15] = z[14] * z[15];
z[16] = z[5] * z[11];
z[15] = z[15] + z[16];
z[14] = z[14] + T(1);
z[16] = -z[11] + -z[14];
z[16] = z[8] * z[16];
z[16] = z[16] + z[5] * T(-2) + -z[13] + -z[15];
z[16] = z[2] * z[16];
z[17] = z[6] + -z[7];
z[13] = z[13] + z[17];
z[13] = z[0] * z[13];
z[17] = z[8] + z[9] + z[17] + T(-2) + -z[3];
z[14] = -(z[2] * z[14]);
z[14] = z[14] + z[17] * T(2);
z[14] = z[1] * z[14];
z[17] = z[10] * T(2);
z[18] = z[3] + -z[17];
z[18] = z[7] * z[18];
z[17] = z[17] + -z[4];
z[17] = z[6] * z[17];
z[19] = z[10] + -z[3] + -z[11];
z[19] = z[6] + z[12] + z[19] * T(2);
z[19] = z[8] * z[19];
z[13] = z[14] + z[16] + z[17] + z[18] + z[19] + z[13] * T(2);
z[13] = z[1] * z[13];
z[14] = -(z[8] * z[11]);
z[14] = z[14] + -z[15];
z[14] = z[2] * z[8] * z[14];
z[13] = z[13] + z[14];
return z[13] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec17_component3(const T* invariants, const T* D)
{
T z[25];
z[0] = D[0];
z[1] = D[2];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = invariants[3];
z[6] = D[6];
z[7] = D[7];
z[8] = invariants[4];
z[9] = D[1];
z[10] = D[8];
z[11] = D[3];
z[12] = D[9];
z[13] = z[5] + z[8];
z[14] = z[11] * z[13];
z[15] = z[13] + T(-1);
z[15] = z[4] * z[15];
z[16] = z[4] + -z[3];
z[17] = z[5] + z[16];
z[18] = T(-1) + -z[17];
z[18] = z[9] * z[18];
z[15] = z[3] + z[5] + z[15] + z[18] + -z[14];
z[15] = z[2] * z[15];
z[17] = z[2] * z[17];
z[18] = z[7] + -z[6];
z[19] = z[18] + -z[8];
z[20] = z[19] * T(-2) + -z[17];
z[20] = z[1] * z[20];
z[21] = z[6] + z[12];
z[21] = z[8] * z[21];
z[22] = z[8] * T(2);
z[23] = z[7] + -z[22];
z[23] = z[3] * z[23];
z[24] = -(z[4] * z[6]);
z[22] = -(z[11] * z[22]);
z[19] = z[10] + T(-2) + -z[3] + -z[19];
z[19] = z[9] * z[19];
z[15] = z[15] + z[20] + z[21] + z[22] + z[23] + z[24] + z[19] * T(2);
z[15] = z[1] * z[15];
z[19] = z[8] + T(-1);
z[14] = z[14] * z[19];
z[19] = z[4] + T(1);
z[19] = z[13] * z[19];
z[19] = z[19] + T(-1);
z[19] = z[9] * z[19];
z[14] = z[14] + z[19];
z[14] = z[2] * z[14];
z[16] = z[16] + -z[18];
z[16] = z[16] * T(2) + -z[17];
z[16] = z[1] * z[16];
z[13] = z[4] * z[13];
z[13] = z[5] + z[13];
z[13] = z[2] * z[13];
z[13] = z[13] + z[16];
z[13] = z[0] * z[13];
z[13] = z[13] + z[14] + z[15];
return z[13] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec17_component4(const T* invariants, const T* D)
{
T z[23];
z[0] = D[0];
z[1] = D[3];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = invariants[3];
z[6] = D[6];
z[7] = D[7];
z[8] = D[1];
z[9] = D[8];
z[10] = invariants[4];
z[11] = D[2];
z[12] = D[9];
z[13] = z[4] + -z[3];
z[14] = z[5] + z[13];
z[15] = z[0] + z[11];
z[15] = z[14] * z[15];
z[16] = z[4] + T(1);
z[16] = z[5] * z[16];
z[17] = z[5] + T(-1);
z[18] = z[3] + -z[17];
z[18] = z[10] * z[18];
z[14] = T(-1) + -z[14];
z[14] = z[8] * z[14];
z[14] = z[14] + z[16] + z[18] + -z[15];
z[14] = z[2] * z[14];
z[16] = z[6] + -z[7];
z[18] = z[13] + z[16];
z[18] = z[0] * z[18];
z[19] = z[9] + z[10] + z[16] + T(-2) + -z[3];
z[19] = z[8] * z[19];
z[18] = z[18] + z[19];
z[19] = z[11] * T(2);
z[16] = z[16] * z[19];
z[20] = z[3] * z[7];
z[21] = -(z[4] * z[6]);
z[19] = z[6] + z[12] + z[19] + z[3] * T(-2);
z[19] = z[10] * z[19];
z[22] = -z[5] + -z[10];
z[22] = z[2] * z[22];
z[22] = z[22] + z[10] * T(-2);
z[22] = z[1] * z[22];
z[14] = z[14] + z[16] + z[19] + z[20] + z[21] + z[22] + z[18] * T(2);
z[14] = z[1] * z[14];
z[13] = -z[13] + -z[17];
z[13] = z[8] * z[13];
z[13] = z[13] + -z[15];
z[13] = z[2] * z[13];
z[13] = z[13] + z[14];
return z[13] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec17_component10(const T* invariants, const T* D)
{
T z[24];
z[0] = D[0];
z[1] = D[5];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = invariants[3];
z[5] = D[6];
z[6] = D[7];
z[7] = D[1];
z[8] = D[8];
z[9] = invariants[4];
z[10] = D[2];
z[11] = D[3];
z[12] = D[9];
z[13] = z[5] + -z[6];
z[14] = z[3] + -z[2];
z[15] = -z[13] + -z[14];
z[16] = z[0] * T(2);
z[15] = z[15] * z[16];
z[17] = z[9] + z[13];
z[18] = z[2] + T(2) + -z[8] + -z[17];
z[19] = z[4] + z[14];
z[20] = z[19] + T(1);
z[20] = z[1] * z[20];
z[18] = z[20] + z[18] * T(2);
z[18] = z[7] * z[18];
z[20] = z[1] * z[19];
z[17] = z[20] + z[17] * T(-2);
z[17] = z[10] * z[17];
z[20] = z[2] + z[11];
z[20] = z[20] * T(2) + -z[12];
z[20] = z[9] * z[20];
z[21] = -(z[2] * z[6]);
z[22] = z[9] + -z[3];
z[23] = -(z[5] * z[22]);
z[15] = z[15] + z[17] + z[18] + z[20] + z[21] + z[23];
z[17] = z[11] + -z[2];
z[17] = z[9] * z[17];
z[18] = z[11] + z[22];
z[18] = z[4] * z[18];
z[17] = z[14] + z[17] + z[18];
z[16] = z[16] + -z[1];
z[16] = z[16] * z[19];
z[14] = z[14] + -z[9];
z[14] = z[8] * z[14];
z[18] = z[4] + z[9];
z[18] = z[12] * z[18];
z[13] = z[13] + z[14] + z[16] + z[18] + z[17] * T(2);
z[13] = z[1] * z[13];
z[13] = z[13] + z[15] * T(2);
return z[13] * T(2);
}

template<typename T> T BubbleHexagon_004ny___zzzz__zvec17_divergence(const T* invariants, const T* D)
{
T z[25];
z[0] = D[0];
z[1] = D[5];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = invariants[3];
z[5] = D[6];
z[6] = D[7];
z[7] = D[1];
z[8] = D[8];
z[9] = invariants[4];
z[10] = D[2];
z[11] = D[3];
z[12] = D[9];
z[13] = z[0] * T(5);
z[14] = z[10] * T(5);
z[15] = z[13] + z[14] + -z[1];
z[16] = z[2] + -z[3];
z[17] = z[16] + -z[4];
z[15] = z[15] * z[17];
z[18] = z[9] + -z[3];
z[19] = z[11] * T(5);
z[20] = z[18] * T(-3) + -z[12] + -z[19];
z[20] = z[4] * z[20];
z[21] = z[9] * z[12];
z[22] = z[5] + -z[6];
z[23] = z[2] * T(3) + -z[19];
z[23] = z[9] * z[23];
z[24] = z[9] + z[16];
z[24] = z[8] * z[24];
z[15] = z[15] + z[20] + z[23] + z[24] + z[16] * T(3) + -z[21] + -z[22];
z[15] = z[1] * z[15];
z[20] = z[2] * z[6];
z[18] = z[5] * z[18];
z[18] = z[18] + z[20] + z[21];
z[16] = z[22] + -z[16];
z[13] = z[13] * z[16];
z[16] = z[9] + z[22];
z[14] = z[14] * z[16];
z[19] = z[2] * T(-4) + -z[19];
z[19] = z[9] * z[19];
z[13] = z[13] + z[14] + z[19] + z[18] * T(2);
z[14] = z[8] + z[16] + T(-2) + -z[2];
z[16] = z[17] + T(-1);
z[16] = z[1] * z[16];
z[14] = z[16] + z[14] * T(2);
z[14] = z[7] * z[14];
z[13] = z[15] + z[13] * T(2) + z[14] * T(5);
return z[13] * T(2);
}

template<typename T> std::vector<std::vector<T>> BubbleHexagon_004ny___zzzz__z_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(17, std::vector<T>(11, T(0)));
ibp_vectors[0][0] = BubbleHexagon_004ny___zzzz__zvec1_component1(invariants, propagators);
ibp_vectors[0][1] = BubbleHexagon_004ny___zzzz__zvec1_component2(invariants, propagators);
ibp_vectors[0][2] = BubbleHexagon_004ny___zzzz__zvec1_component3(invariants, propagators);
ibp_vectors[0][3] = BubbleHexagon_004ny___zzzz__zvec1_component4(invariants, propagators);
ibp_vectors[0][9] = BubbleHexagon_004ny___zzzz__zvec1_component10(invariants, propagators);
ibp_vectors[0].back() = BubbleHexagon_004ny___zzzz__zvec1_divergence(invariants, propagators);
ibp_vectors[1][0] = BubbleHexagon_004ny___zzzz__zvec2_component1(invariants, propagators);
ibp_vectors[1][1] = BubbleHexagon_004ny___zzzz__zvec2_component2(invariants, propagators);
ibp_vectors[1][2] = BubbleHexagon_004ny___zzzz__zvec2_component3(invariants, propagators);
ibp_vectors[1][3] = BubbleHexagon_004ny___zzzz__zvec2_component4(invariants, propagators);
ibp_vectors[1][9] = BubbleHexagon_004ny___zzzz__zvec2_component10(invariants, propagators);
ibp_vectors[1].back() = BubbleHexagon_004ny___zzzz__zvec2_divergence(invariants, propagators);
ibp_vectors[2][0] = BubbleHexagon_004ny___zzzz__zvec3_component1(invariants, propagators);
ibp_vectors[2][1] = BubbleHexagon_004ny___zzzz__zvec3_component2(invariants, propagators);
ibp_vectors[2][2] = BubbleHexagon_004ny___zzzz__zvec3_component3(invariants, propagators);
ibp_vectors[2][3] = BubbleHexagon_004ny___zzzz__zvec3_component4(invariants, propagators);
ibp_vectors[2][9] = BubbleHexagon_004ny___zzzz__zvec3_component10(invariants, propagators);
ibp_vectors[2].back() = BubbleHexagon_004ny___zzzz__zvec3_divergence(invariants, propagators);
ibp_vectors[3][0] = BubbleHexagon_004ny___zzzz__zvec4_component1(invariants, propagators);
ibp_vectors[3][1] = BubbleHexagon_004ny___zzzz__zvec4_component2(invariants, propagators);
ibp_vectors[3][2] = BubbleHexagon_004ny___zzzz__zvec4_component3(invariants, propagators);
ibp_vectors[3][3] = BubbleHexagon_004ny___zzzz__zvec4_component4(invariants, propagators);
ibp_vectors[3][9] = BubbleHexagon_004ny___zzzz__zvec4_component10(invariants, propagators);
ibp_vectors[3].back() = BubbleHexagon_004ny___zzzz__zvec4_divergence(invariants, propagators);
ibp_vectors[16][0] = BubbleHexagon_004ny___zzzz__zvec17_component1(invariants, propagators);
ibp_vectors[16][1] = BubbleHexagon_004ny___zzzz__zvec17_component2(invariants, propagators);
ibp_vectors[16][2] = BubbleHexagon_004ny___zzzz__zvec17_component3(invariants, propagators);
ibp_vectors[16][3] = BubbleHexagon_004ny___zzzz__zvec17_component4(invariants, propagators);
ibp_vectors[16][9] = BubbleHexagon_004ny___zzzz__zvec17_component10(invariants, propagators);
ibp_vectors[16].back() = BubbleHexagon_004ny___zzzz__zvec17_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BubbleHexagon_004ny___zzzz__z_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BubbleHexagon_004ny___zzzz__z_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BubbleHexagon_004ny___zzzz__z_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BubbleHexagon_004ny___zzzz__z_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

