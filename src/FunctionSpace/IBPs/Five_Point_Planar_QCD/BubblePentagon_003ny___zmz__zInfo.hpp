namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<std::vector<T>> BubblePentagon_003ny___zmz__z_vectors(const T* , const T* , const T* );

template<typename T, size_t D> IBPCoordinates<T> BubblePentagon_003ny___zmz__zCoordinates(const OnShellPoint<T,D>& l, const std::vector<momentumD<T,D>>&  P, const std::vector<std::vector<T>>& ccoeffs)
{
auto invariant1 = P[0] * P[1];
auto invariant2 = P[1] * P[2];
auto invariant3 = P[2] * P[2];
auto invdimension = T(1) / invariant1;
std::vector<T> invariants = {invdimension * invariant1, invdimension * invariant2, invdimension * invariant3, invariant1};
auto D1mom = l[0] + -P[3];
auto D1 = invdimension * (D1mom * D1mom);
auto D2mom = l[0] + -P[2] + -P[3];
auto D2 = invdimension * (D2mom * D2mom);
auto D3mom = l[0] + -P[1] + -P[2] + -P[3];
auto D3 = invdimension * (D3mom * D3mom);
auto D4mom = l[0];
auto D4 = invdimension * (D4mom * D4mom);
auto D5mom = l[1];
auto D5 = invdimension * (D5mom * D5mom);
auto D6mom = l[1] + P[3];
auto D6 = invdimension * (D6mom * D6mom);
auto D7mom = l[1] + P[2] + P[3];
auto D7 = invdimension * (D7mom * D7mom);
auto D8mom = l[1] + P[1] + P[2] + P[3];
auto D8 = invdimension * (D8mom * D8mom);
auto D9mom = l[0] + -l[1];
auto D9 = invdimension * (D9mom * D9mom);
auto alpha1 = (-D1 + D4) / T(2);
auto alpha2 = (-D2 + T(2) * invariants[0] + D1) / T(2);
auto alpha3 = (-D3 + T(-2) * invariants[0] + D2) / T(2);
std::vector<T> props = {alpha1, alpha2, alpha3, D4, D5, D6, D7, D8, D9};
auto vector_components = BubblePentagon_003ny___zmz__z_vectors(&props[0], &invariants[0],ccoeffs);
return IBPCoordinates<T>{props, invariants, vector_components};
}

}}}}

