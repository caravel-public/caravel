/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][10];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][10];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][10];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][10];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][10];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][10];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][10];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][10];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][10];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][10];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][10];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][10];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][10];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][10];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[3][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][10];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[3][10];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[3][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[3][10];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[3][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][10];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta77(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][10];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][1];
z[5] = ibp_vectors[3][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta78(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][10];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta79(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[3][10];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[3][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta80(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][10];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta81(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[3][10];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[3][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta82(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[3][10];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][3];
z[5] = ibp_vectors[3][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[3][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta83(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[3][10];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][3];
z[5] = ibp_vectors[3][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[3][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta84(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[3][10];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][3];
z[5] = ibp_vectors[3][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[3][10];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta85(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][10];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[4][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta86(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][10];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta87(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][10];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta88(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][10];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta89(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][10];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta90(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][10];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta91(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[8][10];
z[1] = ibp_vectors[8].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[8][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta92(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[8][10];
z[2] = ibp_vectors[8].back();
z[3] = ibp_vectors[8][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[8][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta93(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[8][10];
z[2] = ibp_vectors[8].back();
z[3] = ibp_vectors[8][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[8][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TrianglePentagon_103ny_z__zzz__zSurfaceIta94(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[9][10];
z[1] = ibp_vectors[9].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[9][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,94> TrianglePentagon_103ny_z__zzz__zSurfaceItaArray;
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[0] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta1, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[1] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta2, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[2] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta3, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[3] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta4, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[4] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta5, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[5] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta6, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[6] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta7, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[7] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta8, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[8] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta9, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[9] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta10, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[10] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta11, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[11] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta12, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[12] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta13, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[13] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta14, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[14] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta15, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[15] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta16, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[16] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta17, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[17] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta18, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[18] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta19, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[19] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta20, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[20] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta21, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[21] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta22, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[22] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta23, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[23] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta24, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[24] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta25, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[25] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta26, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[26] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta27, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[27] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta28, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[28] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta29, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[29] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta30, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[30] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta31, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[31] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta32, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[32] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta33, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[33] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta34, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[34] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta35, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[35] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta36, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[36] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta37, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[37] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta38, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[38] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta39, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[39] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta40, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[40] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta41, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[41] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta42, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[42] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta43, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[43] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta44, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[44] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta45, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[45] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta46, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[46] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta47, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[47] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta48, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[48] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta49, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[49] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta50, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[50] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta51, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[51] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta52, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[52] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta53, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[53] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta54, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[54] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta55, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[55] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta56, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[56] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta57, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[57] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta58, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[58] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta59, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[59] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta60, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[60] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta61, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[61] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta62, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[62] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta63, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[63] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta64, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[64] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta65, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[65] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta66, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[66] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta67, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[67] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta68, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[68] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta69, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[69] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta70, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[70] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta71, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[71] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta72, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[72] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta73, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[73] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta74, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[74] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta75, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[75] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta76, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[76] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta77, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[77] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta78, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[78] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta79, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[79] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta80, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[80] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta81, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[81] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta82, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[82] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta83, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[83] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta84, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[84] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta85, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[85] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta86, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[86] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta87, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[87] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta88, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[88] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta89, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[89] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta90, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[90] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta91, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[91] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta92, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[92] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta93, -1};
TrianglePentagon_103ny_z__zzz__zSurfaceItaArray[93] = {TrianglePentagon_103ny_z__zzz__zSurfaceIta94, -1};
return TrianglePentagon_103ny_z__zzz__zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta<F32>();

#endif

}}}}

