#include "FunctionSpace/IBPhelper.h"

#include "Core/momD.h"

#include "BubbleHexagon_004nn___zmzz__Vectors.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::vector<std::vector<C>> BubbleHexagon_004nn___zmzz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

template std::vector<std::vector<CHP>> BubbleHexagon_004nn___zmzz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

template std::vector<std::vector<CVHP>> BubbleHexagon_004nn___zmzz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

template std::vector<std::vector<F32>> BubbleHexagon_004nn___zmzz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

