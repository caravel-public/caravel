#include "FunctionSpace/IBPhelper.h"

#include "BubbleBox_002nn___mm__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,3> BubbleBox_002nn___mm__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,3> BubbleBox_002nn___mm__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,3> BubbleBox_002nn___mm__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,3> BubbleBox_002nn___mm__SurfaceIta<F32>();

#endif

}}}}

