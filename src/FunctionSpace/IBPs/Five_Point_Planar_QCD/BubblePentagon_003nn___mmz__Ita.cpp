#include "FunctionSpace/IBPhelper.h"

#include "BubblePentagon_003nn___mmz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,6> BubblePentagon_003nn___mmz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,6> BubblePentagon_003nn___mmz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,6> BubblePentagon_003nn___mmz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,6> BubblePentagon_003nn___mmz__SurfaceIta<F32>();

#endif

}}}}

