/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][11];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][11];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[2], 2);
z[9] = z[6] * z[8];
z[10] = z[4] + (z[3] * T(7)) / T(2);
z[10] = z[2] * z[10];
z[10] = z[10] + z[5] * T(2);
z[10] = z[1] * z[2] * z[10];
z[9] = z[9] + z[10];
z[9] = z[0] * z[9];
z[8] = z[1] * z[7] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = z[0] * z[2];
zd[1] = -((z[1] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + (z[3] * T(7)) / T(2);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][11];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[3][11];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][1];
z[5] = ibp_vectors[3][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][1];
z[5] = ibp_vectors[3][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][1];
z[5] = ibp_vectors[3][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
z[4] = ibp_vectors[3].back();
z[5] = ibp_vectors[3][2];
z[6] = ibp_vectors[3][1];
z[7] = ibp_vectors[3][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
z[4] = ibp_vectors[3].back();
z[5] = ibp_vectors[3][2];
z[6] = ibp_vectors[3][1];
z[7] = ibp_vectors[3][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
z[4] = ibp_vectors[3].back();
z[5] = ibp_vectors[3][2];
z[6] = ibp_vectors[3][1];
z[7] = ibp_vectors[3][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
z[4] = ibp_vectors[3].back();
z[5] = ibp_vectors[3][2];
z[6] = ibp_vectors[3][1];
z[7] = ibp_vectors[3][0];
z[8] = prod_pow(z[1], 2);
z[9] = z[7] * z[8];
z[10] = z[4] + (z[3] * T(7)) / T(2);
z[10] = z[1] * z[10];
z[10] = z[10] + z[6] * T(2);
z[10] = z[0] * z[1] * z[10];
z[9] = z[9] + z[10];
z[9] = z[2] * z[9];
z[8] = z[0] * z[5] * z[8];
z[8] = z[9] + z[8] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
z[4] = z[1] * z[2];
zd[1] = -((z[0] * z[3] * prod_pow(z[4], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
z[4] = ibp_vectors[3].back();
z[5] = ibp_vectors[3][2];
z[6] = ibp_vectors[3][1];
z[7] = ibp_vectors[3][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(3);
zd[0] = prod_pow(z[2], 2) * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 3) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][11];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[4][11];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][1];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(4);
zd[0] = prod_pow(z[1], 3) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * prod_pow(z[1], 4) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][0];
z[6] = prod_pow(z[0], 2) * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[7] + z[5] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_203nn_zz__zzz__SurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 3) * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,73> BoxPentagon_203nn_zz__zzz__SurfaceItaArray;
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[0] = {BoxPentagon_203nn_zz__zzz__SurfaceIta1, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[1] = {BoxPentagon_203nn_zz__zzz__SurfaceIta2, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[2] = {BoxPentagon_203nn_zz__zzz__SurfaceIta3, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[3] = {BoxPentagon_203nn_zz__zzz__SurfaceIta4, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[4] = {BoxPentagon_203nn_zz__zzz__SurfaceIta5, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[5] = {BoxPentagon_203nn_zz__zzz__SurfaceIta6, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[6] = {BoxPentagon_203nn_zz__zzz__SurfaceIta7, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[7] = {BoxPentagon_203nn_zz__zzz__SurfaceIta8, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[8] = {BoxPentagon_203nn_zz__zzz__SurfaceIta9, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[9] = {BoxPentagon_203nn_zz__zzz__SurfaceIta10, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[10] = {BoxPentagon_203nn_zz__zzz__SurfaceIta11, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[11] = {BoxPentagon_203nn_zz__zzz__SurfaceIta12, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[12] = {BoxPentagon_203nn_zz__zzz__SurfaceIta13, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[13] = {BoxPentagon_203nn_zz__zzz__SurfaceIta14, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[14] = {BoxPentagon_203nn_zz__zzz__SurfaceIta15, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[15] = {BoxPentagon_203nn_zz__zzz__SurfaceIta16, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[16] = {BoxPentagon_203nn_zz__zzz__SurfaceIta17, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[17] = {BoxPentagon_203nn_zz__zzz__SurfaceIta18, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[18] = {BoxPentagon_203nn_zz__zzz__SurfaceIta19, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[19] = {BoxPentagon_203nn_zz__zzz__SurfaceIta20, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[20] = {BoxPentagon_203nn_zz__zzz__SurfaceIta21, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[21] = {BoxPentagon_203nn_zz__zzz__SurfaceIta22, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[22] = {BoxPentagon_203nn_zz__zzz__SurfaceIta23, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[23] = {BoxPentagon_203nn_zz__zzz__SurfaceIta24, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[24] = {BoxPentagon_203nn_zz__zzz__SurfaceIta25, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[25] = {BoxPentagon_203nn_zz__zzz__SurfaceIta26, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[26] = {BoxPentagon_203nn_zz__zzz__SurfaceIta27, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[27] = {BoxPentagon_203nn_zz__zzz__SurfaceIta28, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[28] = {BoxPentagon_203nn_zz__zzz__SurfaceIta29, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[29] = {BoxPentagon_203nn_zz__zzz__SurfaceIta30, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[30] = {BoxPentagon_203nn_zz__zzz__SurfaceIta31, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[31] = {BoxPentagon_203nn_zz__zzz__SurfaceIta32, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[32] = {BoxPentagon_203nn_zz__zzz__SurfaceIta33, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[33] = {BoxPentagon_203nn_zz__zzz__SurfaceIta34, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[34] = {BoxPentagon_203nn_zz__zzz__SurfaceIta35, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[35] = {BoxPentagon_203nn_zz__zzz__SurfaceIta36, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[36] = {BoxPentagon_203nn_zz__zzz__SurfaceIta37, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[37] = {BoxPentagon_203nn_zz__zzz__SurfaceIta38, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[38] = {BoxPentagon_203nn_zz__zzz__SurfaceIta39, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[39] = {BoxPentagon_203nn_zz__zzz__SurfaceIta40, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[40] = {BoxPentagon_203nn_zz__zzz__SurfaceIta41, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[41] = {BoxPentagon_203nn_zz__zzz__SurfaceIta42, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[42] = {BoxPentagon_203nn_zz__zzz__SurfaceIta43, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[43] = {BoxPentagon_203nn_zz__zzz__SurfaceIta44, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[44] = {BoxPentagon_203nn_zz__zzz__SurfaceIta45, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[45] = {BoxPentagon_203nn_zz__zzz__SurfaceIta46, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[46] = {BoxPentagon_203nn_zz__zzz__SurfaceIta47, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[47] = {BoxPentagon_203nn_zz__zzz__SurfaceIta48, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[48] = {BoxPentagon_203nn_zz__zzz__SurfaceIta49, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[49] = {BoxPentagon_203nn_zz__zzz__SurfaceIta50, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[50] = {BoxPentagon_203nn_zz__zzz__SurfaceIta51, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[51] = {BoxPentagon_203nn_zz__zzz__SurfaceIta52, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[52] = {BoxPentagon_203nn_zz__zzz__SurfaceIta53, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[53] = {BoxPentagon_203nn_zz__zzz__SurfaceIta54, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[54] = {BoxPentagon_203nn_zz__zzz__SurfaceIta55, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[55] = {BoxPentagon_203nn_zz__zzz__SurfaceIta56, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[56] = {BoxPentagon_203nn_zz__zzz__SurfaceIta57, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[57] = {BoxPentagon_203nn_zz__zzz__SurfaceIta58, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[58] = {BoxPentagon_203nn_zz__zzz__SurfaceIta59, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[59] = {BoxPentagon_203nn_zz__zzz__SurfaceIta60, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[60] = {BoxPentagon_203nn_zz__zzz__SurfaceIta61, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[61] = {BoxPentagon_203nn_zz__zzz__SurfaceIta62, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[62] = {BoxPentagon_203nn_zz__zzz__SurfaceIta63, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[63] = {BoxPentagon_203nn_zz__zzz__SurfaceIta64, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[64] = {BoxPentagon_203nn_zz__zzz__SurfaceIta65, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[65] = {BoxPentagon_203nn_zz__zzz__SurfaceIta66, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[66] = {BoxPentagon_203nn_zz__zzz__SurfaceIta67, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[67] = {BoxPentagon_203nn_zz__zzz__SurfaceIta68, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[68] = {BoxPentagon_203nn_zz__zzz__SurfaceIta69, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[69] = {BoxPentagon_203nn_zz__zzz__SurfaceIta70, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[70] = {BoxPentagon_203nn_zz__zzz__SurfaceIta71, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[71] = {BoxPentagon_203nn_zz__zzz__SurfaceIta72, -1};
BoxPentagon_203nn_zz__zzz__SurfaceItaArray[72] = {BoxPentagon_203nn_zz__zzz__SurfaceIta73, -1};
return BoxPentagon_203nn_zz__zzz__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta<F32>();

#endif

}}}}

