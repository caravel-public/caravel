#ifndef TriangleTriangle_101yy_z__z_m_zVectors_H_INC

#define TriangleTriangle_101yy_z__z_m_zVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec1_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec1_component2(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec1_component3(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = D[2];
z[2] = D[1];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = D[6];
z[6] = D[7];
z[7] = D[4];
z[8] = D[5];
z[9] = z[2] + T(1) + -z[5];
z[10] = z[4] * T(2);
z[9] = z[6] + z[9] * T(2) + -z[10];
z[11] = z[4] * z[9];
z[9] = z[7] + z[9];
z[9] = z[3] * z[9];
z[12] = z[6] + -z[5];
z[9] = z[9] + z[11] + -z[12];
z[10] = z[10] + z[12];
z[10] = z[7] * z[10];
z[11] = z[12] + -z[0] + -z[4];
z[11] = z[1] * z[11];
z[12] = -(z[8] * z[12]);
return z[10] + z[12] + z[9] * T(2) + z[11] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec1_component4(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[3];
z[2] = D[1];
z[3] = D[6];
z[4] = D[7];
z[5] = invariants[2];
z[6] = D[4];
z[7] = z[4] + -z[3];
z[8] = z[7] + -z[0] + -z[5];
z[8] = z[1] * z[8];
z[8] = z[7] + -z[8];
z[9] = z[5] * T(2);
z[7] = z[9] + -z[7];
z[7] = z[2] * z[7];
z[9] = z[6] + z[3] * T(-3) + T(2) + z[4] * T(2) + -z[9];
z[9] = z[5] * z[9];
z[7] = z[7] + z[9] + z[8] * T(-2);
return z[7] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec1_component9(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[6];
z[2] = D[7];
z[3] = invariants[2];
z[4] = z[0] + z[1] + z[3] + -z[2];
return z[4] * T(8);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec1_divergence(const T* invariants, const T* D)
{
T z[6];
z[0] = D[0];
z[1] = D[6];
z[2] = D[7];
z[3] = invariants[2];
z[4] = z[1] + -z[2];
z[5] = -z[0] + -z[3];
z[4] = z[4] * T(-3) + z[5] * T(2);
return z[4] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec2_component1(const T* invariants, const T* D)
{
T z[14];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = D[5];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[3];
z[7] = D[6];
z[8] = D[7];
z[9] = z[3] + -z[2];
z[10] = z[7] + -z[8];
z[10] = z[9] * z[10];
z[11] = z[6] + T(-1);
z[12] = z[5] * T(2);
z[11] = z[12] + z[11] * T(2) + -z[7];
z[13] = z[3] + z[2] * T(-2) + -z[11];
z[12] = z[12] * z[13];
z[11] = -z[2] + -z[11];
z[11] = z[4] * z[11];
z[9] = z[9] + T(1) + -z[1] + -z[4] + -z[5];
z[9] = z[0] * z[9];
return z[10] + z[12] + z[11] * T(2) + z[9] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec2_component2(const T* invariants, const T* D)
{
T z[13];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = D[5];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[3];
z[7] = D[6];
z[8] = z[4] + -z[3];
z[9] = z[5] + T(-1);
z[8] = z[8] * z[9];
z[10] = z[5] + T(-2);
z[10] = z[5] * z[10];
z[11] = z[4] + z[9];
z[12] = z[3] + -z[1] + -z[2] + -z[11];
z[12] = z[0] * z[12];
z[8] = z[8] + z[10] + z[12] + T(1);
z[10] = z[11] * T(2) + -z[3];
z[10] = z[6] * z[10];
z[9] = z[4] + z[6] + z[9] * T(3);
z[9] = z[2] * z[9];
z[11] = -(z[7] * z[11]);
z[8] = z[9] + z[10] + z[11] + z[8] * T(2);
return z[8] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec2_component3(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec2_component4(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec2_component9(const T* invariants, const T* D)
{
T z[6];
z[0] = D[2];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = z[0] + z[1] + z[3] + z[4] + T(-1) + -z[2];
return z[5] * T(8);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec2_divergence(const T* invariants, const T* D)
{
T z[7];
z[0] = D[2];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = z[1] + -z[2];
z[6] = T(1) + -z[0] + -z[3] + -z[4];
z[5] = z[5] * T(-3) + z[6] * T(2);
return z[5] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec5_component1(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[4];
z[5] = D[5];
z[6] = z[0] + z[3];
z[7] = T(1) + -z[6];
z[7] = z[0] * z[7];
z[8] = -(z[2] * z[6]);
z[7] = z[7] + z[8];
z[7] = z[2] * z[7];
z[8] = z[0] + z[2];
z[6] = -(z[1] * z[6] * z[8]);
z[9] = prod_pow(z[0], 2);
z[6] = z[6] + z[7] + z[9];
z[7] = z[5] + -z[4];
z[7] = z[2] * z[7] * z[8];
return z[7] + z[6] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec5_component2(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[4];
z[5] = D[5];
z[6] = z[1] + T(1);
z[7] = z[3] + T(-2) + -z[1];
z[7] = z[3] * z[7];
z[7] = z[6] + z[7];
z[7] = z[0] * z[7];
z[6] = z[6] + -z[3];
z[8] = z[1] * z[6];
z[9] = -(z[0] * z[6]);
z[9] = z[9] + -z[8];
z[9] = z[2] * z[9];
z[8] = -(z[3] * z[8]);
z[7] = z[7] + z[8] + z[9];
z[6] = z[3] * z[6];
z[8] = z[5] * z[6];
z[6] = z[2] * T(2) + -z[6];
z[6] = z[4] * z[6];
return z[6] + z[8] + z[7] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec5_component3(const T* invariants, const T* D)
{
T z[12];
z[0] = D[2];
z[1] = invariants[2];
z[2] = D[3];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = z[5] + -z[4];
z[7] = z[6] + z[2] * T(2);
z[8] = z[3] * z[7];
z[9] = z[2] + z[6];
z[10] = z[9] + -z[8];
z[7] = z[7] + z[0] * T(-2);
z[11] = T(-2) + z[3] * T(2) + -z[7];
z[11] = z[0] * z[11];
z[7] = -(z[1] * z[7]);
z[7] = z[7] + z[11] + z[10] * T(2);
z[7] = z[1] * z[7];
z[9] = z[9] * T(2) + -z[8];
z[9] = z[3] * z[9];
z[8] = z[6] + -z[8];
z[8] = z[0] * z[8];
return z[7] + z[8] + z[9] + -z[6];
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec5_component4(const T* invariants, const T* D)
{
T z[11];
z[0] = D[2];
z[1] = D[3];
z[2] = invariants[2];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = z[4] + -z[5];
z[7] = z[1] + T(-1);
z[8] = z[2] + z[7];
z[8] = z[0] * z[8];
z[8] = z[6] + -z[8];
z[9] = z[1] * T(2) + -z[6];
z[10] = z[9] + T(-2);
z[10] = z[1] * z[10];
z[9] = z[2] * z[9];
z[9] = z[9] + z[10];
z[8] = z[8] * T(-2) + -z[9];
z[8] = z[2] * z[8];
z[6] = -(z[6] * z[7]);
z[7] = z[4] + z[5] + -z[9];
z[7] = z[3] * z[7];
return z[6] + z[7] + z[8];
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec5_component9(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[0] + z[3] + z[5];
z[11] = z[9] + -z[8];
z[10] = z[11] + z[10] * T(2);
z[12] = z[6] + z[10] + z[4] * T(-2) + -z[7];
z[12] = z[2] * z[12];
z[10] = z[1] * z[10];
z[10] = z[10] + z[12] + z[0] * T(-2) + -z[11];
return z[10] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec5_divergence(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[0] + z[3] + z[5];
z[11] = z[9] + -z[8];
z[10] = z[11] + z[10] * T(2);
z[12] = z[7] + z[4] * T(2) + -z[6] + -z[10];
z[12] = z[2] * z[12];
z[10] = -(z[1] * z[10]);
z[10] = z[10] + z[11] + z[12] + z[0] * T(2);
return z[10] * T(3);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec11_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec11_component2(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec11_component3(const T* invariants, const T* D)
{
T z[14];
z[0] = D[2];
z[1] = invariants[2];
z[2] = D[3];
z[3] = invariants[1];
z[4] = D[4];
z[5] = D[5];
z[6] = D[6];
z[7] = D[7];
z[8] = z[6] + -z[7];
z[9] = z[2] * T(2) + -z[8];
z[10] = z[3] * z[9];
z[11] = z[8] + -z[2];
z[12] = z[10] + z[11];
z[9] = z[9] + z[0] * T(-2);
z[13] = z[1] * z[9];
z[9] = z[9] + z[3] * T(-2) + T(2);
z[9] = z[0] * z[9];
z[9] = z[9] + z[13] + z[12] * T(2);
z[9] = z[1] * z[9];
z[11] = z[10] + z[11] * T(2);
z[11] = z[3] * z[11];
z[10] = z[8] + z[10];
z[12] = z[0] * z[10];
z[8] = z[9] + z[11] + z[12] + -z[8];
z[9] = z[10] + z[13];
z[10] = z[4] + -z[5];
z[9] = z[9] * z[10];
return z[9] + z[8] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec11_component4(const T* invariants, const T* D)
{
T z[19];
z[0] = D[0];
z[1] = D[2];
z[2] = invariants[2];
z[3] = D[3];
z[4] = invariants[1];
z[5] = D[1];
z[6] = D[6];
z[7] = D[7];
z[8] = D[4];
z[9] = D[5];
z[10] = z[3] * T(2);
z[11] = z[7] + -z[6];
z[12] = z[10] + z[11];
z[13] = z[12] + T(-2);
z[14] = z[5] * T(2);
z[15] = z[13] + -z[14];
z[15] = z[3] * z[15];
z[16] = z[1] * T(2);
z[17] = z[3] + T(-1);
z[18] = z[5] + -z[17];
z[18] = z[16] * z[18];
z[16] = z[12] + -z[16];
z[16] = z[2] * z[16];
z[15] = z[15] + z[16] + z[18] + z[11] * T(-2);
z[15] = z[2] * z[15];
z[12] = z[12] + T(-4) + -z[14];
z[12] = z[3] * z[12];
z[13] = z[2] * z[13];
z[10] = -(z[0] * z[10]);
z[10] = z[8] + z[10] + z[12] + z[13] + z[14] + T(2) + -z[7];
z[10] = z[4] * z[10];
z[12] = -(z[11] * z[17]);
z[10] = z[10] + z[12] + z[15];
z[12] = z[8] + -z[9];
z[11] = z[11] * z[12];
z[12] = z[1] + -z[3];
z[12] = z[0] * z[12];
z[11] = z[11] + z[12] * T(4);
z[12] = z[2] + T(-1);
z[11] = z[11] * z[12];
return z[11] + z[10] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec11_component9(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[0] + z[8] + -z[9];
z[11] = z[3] + z[10] + z[5] * T(-2);
z[12] = z[11] + T(2);
z[12] = z[1] * z[12];
z[10] = z[12] + -z[10];
z[11] = z[11] + z[4] * T(2);
z[11] = z[6] + z[11] * T(2) + -z[7];
z[11] = z[2] * z[11];
z[10] = z[11] + z[10] * T(2);
return z[10] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__z_m_zvec11_divergence(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[9] + -z[8];
z[10] = z[0] * T(-2) + z[10] * T(3);
z[11] = z[10] + z[3] * T(-2) + z[5] * T(6);
z[12] = z[7] + z[11] + z[4] * T(-6) + -z[6];
z[12] = z[2] * z[12];
z[11] = z[11] + T(-4);
z[11] = z[1] * z[11];
z[10] = z[11] + z[12] + -z[10];
return z[10] * T(2);
}

template<typename T> std::vector<std::vector<T>> TriangleTriangle_101yy_z__z_m_z_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(11, std::vector<T>(10, T(0)));
ibp_vectors[0][2] = TriangleTriangle_101yy_z__z_m_zvec1_component3(invariants, propagators);
ibp_vectors[0][3] = TriangleTriangle_101yy_z__z_m_zvec1_component4(invariants, propagators);
ibp_vectors[0][8] = TriangleTriangle_101yy_z__z_m_zvec1_component9(invariants, propagators);
ibp_vectors[0].back() = TriangleTriangle_101yy_z__z_m_zvec1_divergence(invariants, propagators);
ibp_vectors[1][0] = TriangleTriangle_101yy_z__z_m_zvec2_component1(invariants, propagators);
ibp_vectors[1][1] = TriangleTriangle_101yy_z__z_m_zvec2_component2(invariants, propagators);
ibp_vectors[1][8] = TriangleTriangle_101yy_z__z_m_zvec2_component9(invariants, propagators);
ibp_vectors[1].back() = TriangleTriangle_101yy_z__z_m_zvec2_divergence(invariants, propagators);
ibp_vectors[4][0] = TriangleTriangle_101yy_z__z_m_zvec5_component1(invariants, propagators);
ibp_vectors[4][1] = TriangleTriangle_101yy_z__z_m_zvec5_component2(invariants, propagators);
ibp_vectors[4][2] = TriangleTriangle_101yy_z__z_m_zvec5_component3(invariants, propagators);
ibp_vectors[4][3] = TriangleTriangle_101yy_z__z_m_zvec5_component4(invariants, propagators);
ibp_vectors[4][8] = TriangleTriangle_101yy_z__z_m_zvec5_component9(invariants, propagators);
ibp_vectors[4].back() = TriangleTriangle_101yy_z__z_m_zvec5_divergence(invariants, propagators);
ibp_vectors[10][2] = TriangleTriangle_101yy_z__z_m_zvec11_component3(invariants, propagators);
ibp_vectors[10][3] = TriangleTriangle_101yy_z__z_m_zvec11_component4(invariants, propagators);
ibp_vectors[10][8] = TriangleTriangle_101yy_z__z_m_zvec11_component9(invariants, propagators);
ibp_vectors[10].back() = TriangleTriangle_101yy_z__z_m_zvec11_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleTriangle_101yy_z__z_m_z_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleTriangle_101yy_z__z_m_z_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleTriangle_101yy_z__z_m_z_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleTriangle_101yy_z__z_m_z_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

