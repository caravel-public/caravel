/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][8];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[0][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][8];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][2];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + z[3] * T(3);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[0][8];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][8];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][2];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[1][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][8];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[2][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][8];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][8];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[4][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102ny_z__zz__mSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[5][8];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[5][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,39> TriangleBox_102ny_z__zz__mSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,39> TriangleBox_102ny_z__zz__mSurfaceItaArray;
TriangleBox_102ny_z__zz__mSurfaceItaArray[0] = {TriangleBox_102ny_z__zz__mSurfaceIta1, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[1] = {TriangleBox_102ny_z__zz__mSurfaceIta2, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[2] = {TriangleBox_102ny_z__zz__mSurfaceIta3, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[3] = {TriangleBox_102ny_z__zz__mSurfaceIta4, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[4] = {TriangleBox_102ny_z__zz__mSurfaceIta5, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[5] = {TriangleBox_102ny_z__zz__mSurfaceIta6, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[6] = {TriangleBox_102ny_z__zz__mSurfaceIta7, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[7] = {TriangleBox_102ny_z__zz__mSurfaceIta8, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[8] = {TriangleBox_102ny_z__zz__mSurfaceIta9, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[9] = {TriangleBox_102ny_z__zz__mSurfaceIta10, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[10] = {TriangleBox_102ny_z__zz__mSurfaceIta11, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[11] = {TriangleBox_102ny_z__zz__mSurfaceIta12, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[12] = {TriangleBox_102ny_z__zz__mSurfaceIta13, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[13] = {TriangleBox_102ny_z__zz__mSurfaceIta14, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[14] = {TriangleBox_102ny_z__zz__mSurfaceIta15, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[15] = {TriangleBox_102ny_z__zz__mSurfaceIta16, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[16] = {TriangleBox_102ny_z__zz__mSurfaceIta17, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[17] = {TriangleBox_102ny_z__zz__mSurfaceIta18, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[18] = {TriangleBox_102ny_z__zz__mSurfaceIta19, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[19] = {TriangleBox_102ny_z__zz__mSurfaceIta20, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[20] = {TriangleBox_102ny_z__zz__mSurfaceIta21, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[21] = {TriangleBox_102ny_z__zz__mSurfaceIta22, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[22] = {TriangleBox_102ny_z__zz__mSurfaceIta23, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[23] = {TriangleBox_102ny_z__zz__mSurfaceIta24, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[24] = {TriangleBox_102ny_z__zz__mSurfaceIta25, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[25] = {TriangleBox_102ny_z__zz__mSurfaceIta26, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[26] = {TriangleBox_102ny_z__zz__mSurfaceIta27, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[27] = {TriangleBox_102ny_z__zz__mSurfaceIta28, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[28] = {TriangleBox_102ny_z__zz__mSurfaceIta29, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[29] = {TriangleBox_102ny_z__zz__mSurfaceIta30, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[30] = {TriangleBox_102ny_z__zz__mSurfaceIta31, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[31] = {TriangleBox_102ny_z__zz__mSurfaceIta32, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[32] = {TriangleBox_102ny_z__zz__mSurfaceIta33, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[33] = {TriangleBox_102ny_z__zz__mSurfaceIta34, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[34] = {TriangleBox_102ny_z__zz__mSurfaceIta35, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[35] = {TriangleBox_102ny_z__zz__mSurfaceIta36, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[36] = {TriangleBox_102ny_z__zz__mSurfaceIta37, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[37] = {TriangleBox_102ny_z__zz__mSurfaceIta38, -1};
TriangleBox_102ny_z__zz__mSurfaceItaArray[38] = {TriangleBox_102ny_z__zz__mSurfaceIta39, -1};
return TriangleBox_102ny_z__zz__mSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,39> TriangleBox_102ny_z__zz__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,39> TriangleBox_102ny_z__zz__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,39> TriangleBox_102ny_z__zz__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,39> TriangleBox_102ny_z__zz__mSurfaceIta<F32>();

#endif

}}}}

