#include "FunctionSpace/IBPhelper.h"

#include "BubbleTriangle_001yy___z_m_mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,16> BubbleTriangle_001yy___z_m_mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,16> BubbleTriangle_001yy___z_m_mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,16> BubbleTriangle_001yy___z_m_mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,16> BubbleTriangle_001yy___z_m_mSurfaceIta<F32>();

#endif

}}}}

