/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubbleBox_002nn___mm__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][3];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][3];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002nn___mm__SurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][3];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][3];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002nn___mm__SurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][3];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][3];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,3> BubbleBox_002nn___mm__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,3> BubbleBox_002nn___mm__SurfaceItaArray;
BubbleBox_002nn___mm__SurfaceItaArray[0] = {BubbleBox_002nn___mm__SurfaceIta1, -1};
BubbleBox_002nn___mm__SurfaceItaArray[1] = {BubbleBox_002nn___mm__SurfaceIta2, -1};
BubbleBox_002nn___mm__SurfaceItaArray[2] = {BubbleBox_002nn___mm__SurfaceIta3, -1};
return BubbleBox_002nn___mm__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,3> BubbleBox_002nn___mm__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,3> BubbleBox_002nn___mm__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,3> BubbleBox_002nn___mm__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,3> BubbleBox_002nn___mm__SurfaceIta<F32>();

#endif

}}}}

