#ifndef TriangleTriangle_101yy_z__m_z_zVectors_H_INC

#define TriangleTriangle_101yy_z__m_z_zVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec5_component1(const T* invariants, const T* D)
{
T z[14];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = D[5];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[3];
z[7] = D[6];
z[8] = D[7];
z[9] = z[4] * T(-2) + -z[5];
z[10] = z[6] * T(2) + -z[7];
z[11] = z[2] + z[10] + z[0] * T(2);
z[9] = z[9] * z[11];
z[11] = z[7] + -z[8];
z[12] = z[11] + T(2);
z[12] = z[3] * z[12];
z[11] = T(-4) + -z[11];
z[11] = z[2] * z[11];
z[13] = z[3] + -z[1] + -z[2];
z[13] = z[0] * z[13];
return z[9] + z[11] + z[12] + z[10] * T(-2) + z[13] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec5_component2(const T* invariants, const T* D)
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = D[5];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[3];
z[7] = D[6];
z[8] = z[0] * T(2);
z[9] = z[3] + -z[2];
z[10] = z[9] + -z[1];
z[10] = z[8] * z[10];
z[9] = -(z[6] * z[9]);
z[8] = z[7] + z[8] + z[6] * T(-2) + -z[2];
z[11] = -(z[4] * z[8]);
z[9] = z[9] + z[10] + z[11];
z[8] = -(z[5] * z[8]);
return z[8] + z[9] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec5_component3(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec5_component4(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec5_component9(const T* invariants, const T* D)
{
T z[6];
z[0] = D[2];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = z[0] + z[1] + z[3] + -z[2];
z[5] = z[4] + z[5] * T(2);
return z[5] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec5_divergence(const T* invariants, const T* D)
{
T z[7];
z[0] = D[2];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = z[2] + -z[1];
z[6] = -z[0] + -z[3];
z[5] = z[6] * T(2) + z[5] * T(3) + -z[4];
return z[5] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec20_component1(const T* invariants, const T* D)
{
T z[17];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[5] * T(2);
z[11] = z[10] + -z[8];
z[12] = z[0] * T(2);
z[13] = z[11] + z[12];
z[14] = -(z[3] * z[13]);
z[15] = -(z[2] * z[12]);
z[14] = z[14] + z[15] + -z[11];
z[13] = -(z[4] * z[13]);
z[13] = z[13] + z[14] * T(2);
z[13] = z[1] * z[13];
z[12] = z[12] + -z[9];
z[14] = -z[8] + -z[10] + -z[12];
z[14] = z[3] * z[14];
z[15] = z[2] + -z[10];
z[16] = -(z[0] * z[10]);
z[14] = z[8] + z[14] + z[16] + z[15] * T(2);
z[15] = -z[2] + -z[3] + -z[8];
z[12] = z[15] * T(2) + -z[4] + -z[12];
z[12] = z[4] * z[12];
z[12] = z[12] + z[14] * T(2);
z[12] = z[6] * z[12];
z[14] = z[0] + T(1);
z[10] = z[10] * z[14];
z[10] = z[10] + -z[8];
z[11] = -(z[4] * z[11]);
z[10] = z[11] + z[10] * T(2);
z[10] = z[7] * z[10];
return z[10] + z[12] + z[13] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec20_component2(const T* invariants, const T* D)
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = z[0] + z[5];
z[9] = z[4] * T(2);
z[8] = z[8] * z[9];
z[9] = z[5] + z[7] + z[0] * T(2);
z[9] = z[0] * z[9];
z[8] = z[9] + -z[8];
z[9] = -(z[2] * z[8]);
z[10] = prod_pow(z[0], 2) * z[1];
z[11] = z[6] + -z[5];
z[11] = z[0] * z[4] * z[11];
z[9] = z[9] + z[11] + z[10] * T(-2);
z[8] = -(z[3] * z[8]);
z[8] = z[8] + z[9] * T(2);
return z[8] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec20_component3(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec20_component4(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec20_component9(const T* invariants, const T* D)
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[5];
z[7] = z[3] + z[2] * T(2);
z[8] = z[7] + z[1] * T(2);
z[8] = z[0] * z[8];
z[9] = z[5] + -z[6];
z[9] = z[4] * z[9];
z[8] = z[8] + z[9];
z[7] = z[5] * z[7];
z[7] = z[7] + z[8] * T(2);
return z[7] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec20_divergence(const T* invariants, const T* D)
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = z[2] * T(-2) + -z[3];
z[9] = z[7] + z[4] * T(-2) + z[5] * T(3) + z[0] * T(6);
z[8] = z[8] * z[9];
z[9] = z[6] + -z[5];
z[9] = z[4] * z[9];
z[10] = z[0] * z[1];
z[9] = z[9] + z[10] * T(-2);
z[8] = z[8] + z[9] * T(6);
return z[8] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec27_component1(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = invariants[2];
z[4] = z[0] + T(1);
z[5] = z[0] * T(4);
z[6] = -(z[4] * z[5]);
z[7] = z[3] + T(2);
z[8] = z[2] * z[7];
z[6] = z[6] + z[8];
z[6] = z[2] * z[6];
z[4] = -z[1] + -z[4];
z[4] = z[4] * z[5];
z[5] = z[7] + z[0] * T(-2);
z[5] = z[2] * z[5];
z[4] = z[4] + z[5];
z[4] = z[1] * z[4];
z[4] = z[6] + z[4] * T(2);
return z[4] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec27_component2(const T* invariants, const T* D)
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = invariants[2];
z[4] = z[1] * T(2);
z[5] = z[0] * T(-2) + -z[3] + -z[4];
z[4] = z[4] * z[5];
z[5] = T(2) + -z[3];
z[5] = z[2] * z[5];
z[4] = z[4] + z[5];
z[4] = z[2] * z[4];
z[5] = T(-1) + -z[0] + -z[1];
z[5] = prod_pow(z[1], 2) * z[5];
z[4] = z[4] + z[5] * T(8);
return z[4] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec27_component3(const T* invariants, const T* D)
{
T z[17];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[6];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[1];
z[7] = D[7];
z[8] = D[4];
z[9] = D[5];
z[10] = z[7] + -z[3];
z[11] = z[2] * T(2);
z[12] = z[10] + z[11];
z[12] = z[8] * z[12];
z[13] = T(-1) + -z[0];
z[13] = z[11] * z[13];
z[10] = z[2] * T(-4) + -z[10];
z[10] = z[6] * z[10];
z[10] = z[3] + z[10] + z[13] + -z[12];
z[13] = z[11] + -z[3];
z[14] = z[5] * z[13];
z[10] = z[14] + z[10] * T(2);
z[10] = z[1] * z[10];
z[15] = z[7] + z[11];
z[15] = z[6] * z[15];
z[16] = z[0] * z[3];
z[15] = z[15] + z[16];
z[15] = z[15] * T(2) + -z[14];
z[12] = -z[12] + -z[15];
z[12] = z[4] * z[12];
z[13] = z[6] * z[13];
z[11] = z[3] * T(3) + -z[11];
z[11] = z[8] * z[11];
z[10] = z[10] + z[11] + z[12] + z[13] * T(-2);
z[11] = -(z[7] * z[8]);
z[11] = z[11] + -z[15];
z[11] = z[5] * z[11];
z[12] = z[3] * T(-2) + -z[14];
z[12] = z[9] * z[12];
return z[11] + z[12] + z[10] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec27_component4(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[3];
z[2] = D[1];
z[3] = D[6];
z[4] = D[7];
z[5] = D[4];
z[6] = invariants[2];
z[7] = z[0] + z[5];
z[8] = z[6] + T(-2);
z[7] = z[8] + z[2] * T(-4) + z[7] * T(-2);
z[7] = z[1] * z[7];
z[9] = z[3] + -z[4];
z[10] = z[9] + T(-2);
z[10] = z[2] * z[10];
z[7] = z[7] + z[10];
z[9] = z[8] + z[9] * T(2);
z[9] = z[5] * z[9];
z[8] = z[3] * z[8];
z[7] = z[9] + z[7] * T(2) + -z[8];
z[7] = z[1] * z[7];
z[8] = -(z[5] * z[8]);
z[7] = z[7] + z[8];
return z[7] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec27_component9(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = D[6];
z[5] = D[7];
z[6] = invariants[2];
z[7] = z[3] + z[1] * T(2);
z[8] = z[1] + T(1);
z[9] = z[0] + z[2] + z[8];
z[7] = z[7] * z[9];
z[9] = z[0] + T(1);
z[9] = z[2] * z[9];
z[7] = z[7] + z[9];
z[9] = z[1] + z[3];
z[9] = z[5] * z[9];
z[8] = -z[3] + -z[8];
z[8] = z[4] * z[8];
z[7] = z[8] + z[9] + z[7] * T(2);
z[8] = z[4] + z[2] * T(-2);
z[8] = z[6] * z[8];
z[7] = z[8] + z[7] * T(2);
return z[7] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec27_divergence(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = D[6];
z[5] = D[7];
z[6] = invariants[2];
z[7] = z[4] * T(3);
z[8] = z[5] * T(3) + -z[7];
z[9] = T(7) + z[0] * T(8);
z[10] = z[1] * T(-16) + z[9] * T(-2) + -z[8];
z[10] = z[1] * z[10];
z[8] = z[1] * T(-8) + -z[8] + -z[9];
z[8] = z[3] * z[8];
z[9] = z[2] * T(6);
z[11] = z[1] * T(-2) + T(-1) + -z[0] + -z[3];
z[11] = z[9] * z[11];
z[8] = z[7] + z[8] + z[10] + z[11];
z[7] = z[9] + -z[3] + -z[7];
z[7] = z[6] * z[7];
z[7] = z[7] + z[8] * T(2);
return z[7] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec37_component1(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[4];
z[5] = D[5];
z[6] = z[0] + T(1);
z[7] = z[0] * z[6];
z[8] = z[0] + T(-1);
z[9] = z[3] * z[8];
z[7] = z[7] + z[9];
z[8] = -(z[4] * z[8]);
z[9] = z[5] * z[6];
z[8] = z[8] + z[9] + z[7] * T(-2);
z[8] = z[2] * z[8];
z[9] = z[4] + z[3] * T(2) + -z[5];
z[6] = z[6] * z[9];
z[7] = z[4] + -z[7];
z[7] = z[1] * z[7];
z[6] = z[8] + z[6] * T(2) + z[7] * T(4);
return z[6] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec37_component2(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[4];
z[5] = D[5];
z[6] = z[5] + -z[4];
z[7] = z[3] + T(-2);
z[6] = z[6] * z[7];
z[8] = T(-2) + -z[0];
z[9] = z[2] * T(2);
z[10] = z[3] + z[9];
z[8] = z[8] * z[10];
z[7] = -z[7] + -z[9];
z[9] = z[1] * T(2);
z[7] = z[7] * z[9];
z[6] = z[6] + z[7] + z[8] * T(2);
return z[6] * z[9];
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec37_component3(const T* invariants, const T* D)
{
T z[11];
z[0] = D[2];
z[1] = invariants[2];
z[2] = D[3];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = z[1] + z[3];
z[7] = z[4] + z[2] * T(-2) + -z[5];
z[8] = z[1] + z[7];
z[6] = z[6] * z[8];
z[9] = z[4] + -z[2];
z[7] = z[7] + T(-2) + z[1] * T(2);
z[10] = z[0] * z[7];
z[6] = z[6] + z[10] + z[9] * T(2);
z[6] = z[3] * z[6];
z[8] = z[1] * z[8];
z[8] = z[8] + z[9] * T(4);
z[8] = z[1] * z[8];
z[9] = z[0] * T(2);
z[10] = z[1] + T(-2);
z[10] = z[9] * z[10];
z[7] = z[1] * z[7];
z[7] = z[7] + z[10] + z[2] * T(4);
z[7] = z[7] * z[9];
return z[7] + z[8] + z[6] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec37_component4(const T* invariants, const T* D)
{
T z[10];
z[0] = D[2];
z[1] = D[3];
z[2] = invariants[2];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = z[1] * T(2);
z[7] = z[4] + T(-4) + -z[5] + -z[6];
z[7] = z[1] * z[7];
z[8] = z[1] * z[2];
z[7] = z[7] + z[8];
z[9] = z[2] + z[3] * T(2);
z[7] = z[7] * z[9];
z[6] = z[8] + -z[6];
z[6] = z[0] * z[6];
z[8] = prod_pow(z[1], 2);
z[6] = z[7] + z[6] * T(2) + z[8] * T(4);
return z[6] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec37_component9(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[1] * T(-2) + -z[2];
z[11] = z[0] + z[3] + z[5] + T(2);
z[11] = z[2] + z[8] + z[11] * T(-2) + -z[9];
z[10] = z[10] * z[11];
z[11] = z[6] + z[4] * T(-2) + -z[7];
z[12] = z[2] + T(-2);
z[11] = z[11] * z[12];
z[12] = -z[3] + -z[5];
z[10] = z[10] + z[11] + z[12] * T(4);
return z[10] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec37_divergence(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[2] + z[1] * T(2);
z[11] = z[0] + z[3] + z[5] + T(2);
z[11] = z[2] + z[8] + z[11] * T(-2) + -z[9];
z[10] = z[10] * z[11];
z[11] = z[7] + z[4] * T(2) + -z[6];
z[12] = z[2] + T(-2);
z[11] = z[11] * z[12];
z[12] = z[3] + z[5];
z[10] = z[10] + z[11] + z[12] * T(4);
return z[10] * T(6);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec39_component1(const T* invariants, const T* D)
{
T z[17];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = invariants[2];
z[4] = D[5];
z[5] = D[6];
z[6] = invariants[1];
z[7] = D[7];
z[8] = D[2];
z[9] = D[3];
z[10] = z[9] * T(2);
z[11] = z[0] * T(4);
z[12] = z[5] + -z[7];
z[13] = z[10] + -z[11] + -z[12];
z[13] = z[2] * z[13];
z[14] = z[12] + T(4);
z[15] = z[1] * z[14];
z[12] = z[0] * z[12];
z[12] = z[12] + z[15];
z[15] = z[1] * T(2);
z[16] = z[2] + z[10] + -z[5] + -z[15];
z[16] = z[3] * z[16];
z[11] = z[11] + z[14];
z[10] = z[11] + -z[10];
z[10] = z[4] * z[10];
z[14] = z[1] * z[8];
z[10] = z[10] + z[13] + z[12] * T(2) + z[14] * T(4) + -z[16];
z[10] = z[3] * z[10];
z[12] = z[12] + z[2] * T(2);
z[12] = z[12] * T(2) + -z[16];
z[12] = z[6] * z[12];
z[13] = z[2] + z[15] + -z[4];
z[11] = z[11] * z[13];
z[11] = z[11] + z[12];
return z[10] + z[11] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec39_component2(const T* invariants, const T* D)
{
T z[16];
z[0] = D[0];
z[1] = D[3];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[1];
z[5] = D[2];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = z[5] + z[6] + -z[7];
z[10] = z[4] * T(2);
z[11] = z[1] + T(1);
z[12] = -(z[2] * z[11]);
z[12] = z[9] + z[10] + z[12] + -z[1];
z[12] = z[10] * z[12];
z[13] = z[0] + T(1);
z[13] = z[1] * z[13];
z[14] = z[8] + -z[6];
z[13] = z[13] * T(2) + -z[14];
z[15] = -(z[2] * z[13]);
z[12] = z[12] + z[15];
z[9] = -z[2] + -z[9] + -z[11];
z[9] = z[9] * z[10];
z[11] = z[1] * T(2) + -z[14];
z[14] = z[2] * z[11];
z[9] = z[9] + z[14] + -z[13];
z[10] = z[11] + -z[10];
z[10] = z[3] * z[10];
z[9] = z[10] + z[9] * T(2);
z[9] = z[3] * z[9];
return z[9] + z[12] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec39_component3(const T* invariants, const T* D)
{
T z[11];
z[0] = D[2];
z[1] = invariants[2];
z[2] = D[3];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = z[1] + z[3];
z[7] = z[4] + z[2] * T(-2) + -z[5];
z[8] = z[1] + z[7];
z[6] = z[6] * z[8];
z[9] = z[4] + -z[2];
z[7] = z[7] + T(-2) + z[1] * T(2);
z[10] = z[0] * z[7];
z[6] = z[6] + z[10] + z[9] * T(2);
z[6] = z[3] * z[6];
z[8] = z[1] * z[8];
z[8] = z[8] + z[9] * T(4);
z[8] = z[1] * z[8];
z[9] = z[0] * T(2);
z[10] = z[1] + T(-2);
z[10] = z[9] * z[10];
z[7] = z[1] * z[7];
z[7] = z[7] + z[10] + z[2] * T(4);
z[7] = z[7] * z[9];
return z[7] + z[8] + z[6] * T(4);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec39_component4(const T* invariants, const T* D)
{
T z[10];
z[0] = D[2];
z[1] = D[3];
z[2] = invariants[2];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = z[1] * T(2);
z[7] = z[4] + T(-4) + -z[5] + -z[6];
z[7] = z[1] * z[7];
z[8] = z[1] * z[2];
z[7] = z[7] + z[8];
z[9] = z[2] + z[3] * T(2);
z[7] = z[7] * z[9];
z[6] = z[8] + -z[6];
z[6] = z[0] * z[6];
z[8] = prod_pow(z[1], 2);
z[6] = z[7] + z[6] * T(2) + z[8] * T(4);
return z[6] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec39_component9(const T* invariants, const T* D)
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[2];
z[3] = D[3];
z[4] = invariants[1];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = z[4] * T(-2) + -z[2];
z[10] = z[7] + -z[8];
z[10] = z[2] + z[3] * T(-6) + T(-4) + z[10] * T(3);
z[9] = z[9] * z[10];
z[10] = z[5] + -z[6];
z[10] = z[1] * T(-2) + z[10] * T(4);
z[11] = z[2] + T(-2);
z[10] = z[10] * z[11];
z[9] = z[9] + z[10] + z[0] * T(-16) + z[3] * T(-4);
return z[9] * T(2);
}

template<typename T> T TriangleTriangle_101yy_z__m_z_zvec39_divergence(const T* invariants, const T* D)
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[2];
z[3] = D[3];
z[4] = invariants[1];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = z[5] + -z[6];
z[9] = z[9] * T(-3);
z[10] = z[2] + T(-2);
z[9] = z[9] * z[10];
z[10] = z[7] + T(-2) + z[3] * T(-2) + -z[8];
z[11] = z[4] * z[10];
z[11] = z[3] + z[11] + z[0] * T(3) + -z[1];
z[10] = z[1] + z[4] + z[10];
z[10] = z[2] + z[10] * T(2);
z[10] = z[2] * z[10];
z[9] = z[9] + z[10] + z[11] * T(4);
return z[9] * T(4);
}

template<typename T> std::vector<std::vector<T>> TriangleTriangle_101yy_z__m_z_z_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(39, std::vector<T>(10, T(0)));
ibp_vectors[4][0] = TriangleTriangle_101yy_z__m_z_zvec5_component1(invariants, propagators);
ibp_vectors[4][1] = TriangleTriangle_101yy_z__m_z_zvec5_component2(invariants, propagators);
ibp_vectors[4][8] = TriangleTriangle_101yy_z__m_z_zvec5_component9(invariants, propagators);
ibp_vectors[4].back() = TriangleTriangle_101yy_z__m_z_zvec5_divergence(invariants, propagators);
ibp_vectors[19][0] = TriangleTriangle_101yy_z__m_z_zvec20_component1(invariants, propagators);
ibp_vectors[19][1] = TriangleTriangle_101yy_z__m_z_zvec20_component2(invariants, propagators);
ibp_vectors[19][8] = TriangleTriangle_101yy_z__m_z_zvec20_component9(invariants, propagators);
ibp_vectors[19].back() = TriangleTriangle_101yy_z__m_z_zvec20_divergence(invariants, propagators);
ibp_vectors[26][0] = TriangleTriangle_101yy_z__m_z_zvec27_component1(invariants, propagators);
ibp_vectors[26][1] = TriangleTriangle_101yy_z__m_z_zvec27_component2(invariants, propagators);
ibp_vectors[26][2] = TriangleTriangle_101yy_z__m_z_zvec27_component3(invariants, propagators);
ibp_vectors[26][3] = TriangleTriangle_101yy_z__m_z_zvec27_component4(invariants, propagators);
ibp_vectors[26][8] = TriangleTriangle_101yy_z__m_z_zvec27_component9(invariants, propagators);
ibp_vectors[26].back() = TriangleTriangle_101yy_z__m_z_zvec27_divergence(invariants, propagators);
ibp_vectors[36][0] = TriangleTriangle_101yy_z__m_z_zvec37_component1(invariants, propagators);
ibp_vectors[36][1] = TriangleTriangle_101yy_z__m_z_zvec37_component2(invariants, propagators);
ibp_vectors[36][2] = TriangleTriangle_101yy_z__m_z_zvec37_component3(invariants, propagators);
ibp_vectors[36][3] = TriangleTriangle_101yy_z__m_z_zvec37_component4(invariants, propagators);
ibp_vectors[36][8] = TriangleTriangle_101yy_z__m_z_zvec37_component9(invariants, propagators);
ibp_vectors[36].back() = TriangleTriangle_101yy_z__m_z_zvec37_divergence(invariants, propagators);
ibp_vectors[38][0] = TriangleTriangle_101yy_z__m_z_zvec39_component1(invariants, propagators);
ibp_vectors[38][1] = TriangleTriangle_101yy_z__m_z_zvec39_component2(invariants, propagators);
ibp_vectors[38][2] = TriangleTriangle_101yy_z__m_z_zvec39_component3(invariants, propagators);
ibp_vectors[38][3] = TriangleTriangle_101yy_z__m_z_zvec39_component4(invariants, propagators);
ibp_vectors[38][8] = TriangleTriangle_101yy_z__m_z_zvec39_component9(invariants, propagators);
ibp_vectors[38].back() = TriangleTriangle_101yy_z__m_z_zvec39_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleTriangle_101yy_z__m_z_z_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleTriangle_101yy_z__m_z_z_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleTriangle_101yy_z__m_z_z_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleTriangle_101yy_z__m_z_z_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

