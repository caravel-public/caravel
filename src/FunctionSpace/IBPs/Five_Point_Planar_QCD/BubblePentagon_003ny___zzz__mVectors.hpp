#ifndef BubblePentagon_003ny___zzz__mVectors_H_INC

#define BubblePentagon_003ny___zzz__mVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BubblePentagon_003ny___zzz__mvec1_component1(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = z[0] * T(2);
z[4] = z[2] + -z[1] + -z[3];
return z[3] * z[4];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec1_component2(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[5];
z[3] = D[6];
z[4] = invariants[1];
z[5] = D[4];
z[6] = -(z[4] * z[5]);
z[7] = z[2] + -z[5];
z[7] = z[1] * z[7];
z[8] = z[3] + z[1] * T(-4) + z[4] * T(-2) + -z[2];
z[8] = z[0] * z[8];
return z[6] + z[7] + z[8];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec1_component3(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[4];
z[7] = D[5];
z[8] = z[4] + z[5];
z[9] = z[6] + z[0] * T(2);
z[8] = z[8] * z[9];
z[9] = z[7] + -z[6];
z[9] = z[1] * z[9];
z[10] = z[3] + z[1] * T(-4) + T(-2) + -z[2];
z[10] = z[0] * z[10];
return z[8] + z[9] + z[10];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec1_component8(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = z[1] + z[0] * T(4) + -z[2];
return z[3] * T(2);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = z[2] + z[0] * T(-4) + -z[1];
return z[3] * T(4);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec2_component1(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[5];
z[3] = D[6];
z[4] = invariants[1];
z[5] = D[4];
z[6] = -(z[4] * z[5]);
z[7] = z[2] + -z[5];
z[7] = z[1] * z[7];
z[8] = z[3] + z[1] * T(-4) + z[4] * T(-2) + -z[2];
z[8] = z[0] * z[8];
return z[6] + z[7] + z[8];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec2_component2(const T* invariants, const T* D)
{
T z[6];
z[0] = D[1];
z[1] = D[5];
z[2] = D[6];
z[3] = invariants[1];
z[4] = z[0] * T(2);
z[5] = z[2] + z[3] * T(-2) + -z[1] + -z[4];
return z[4] * z[5];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec2_component3(const T* invariants, const T* D)
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[5];
z[7] = D[4];
z[8] = z[4] + z[5] + T(-1);
z[8] = z[3] + z[8] * T(2) + -z[2];
z[8] = z[0] * z[8];
z[9] = z[2] + z[0] * T(-4) + z[4] * T(-2) + -z[6];
z[9] = z[1] * z[9];
return z[8] + z[9] + -z[7];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec2_component8(const T* invariants, const T* D)
{
T z[5];
z[0] = D[1];
z[1] = D[5];
z[2] = D[6];
z[3] = invariants[1];
z[4] = z[1] + z[3] * T(2) + z[0] * T(4) + -z[2];
return z[4] * T(2);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec2_divergence(const T* invariants, const T* D)
{
T z[5];
z[0] = D[1];
z[1] = D[5];
z[2] = D[6];
z[3] = invariants[1];
z[4] = z[2] + z[0] * T(-4) + z[3] * T(-2) + -z[1];
return z[4] * T(4);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec3_component1(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[4];
z[7] = D[5];
z[8] = z[4] + z[5];
z[9] = z[6] + z[0] * T(2);
z[8] = z[8] * z[9];
z[9] = z[7] + -z[6];
z[9] = z[1] * z[9];
z[10] = z[3] + z[1] * T(-4) + T(-2) + -z[2];
z[10] = z[0] * z[10];
return z[8] + z[9] + z[10];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec3_component2(const T* invariants, const T* D)
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[5];
z[7] = D[4];
z[8] = z[4] + z[5] + T(-1);
z[8] = z[3] + z[8] * T(2) + -z[2];
z[8] = z[0] * z[8];
z[9] = z[2] + z[0] * T(-4) + z[4] * T(-2) + -z[6];
z[9] = z[1] * z[9];
return z[8] + z[9] + -z[7];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec3_component3(const T* invariants, const T* D)
{
T z[7];
z[0] = D[2];
z[1] = D[6];
z[2] = D[7];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = z[0] * T(2);
z[6] = z[3] + z[4] + T(-1);
z[6] = z[2] + z[6] * T(2) + -z[1] + -z[5];
return z[5] * z[6];
}

template<typename T> T BubblePentagon_003ny___zzz__mvec3_component8(const T* invariants, const T* D)
{
T z[6];
z[0] = D[2];
z[1] = D[6];
z[2] = D[7];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = T(1) + z[0] * T(2) + -z[3] + -z[4];
z[5] = z[1] + z[5] * T(2) + -z[2];
return z[5] * T(2);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec3_divergence(const T* invariants, const T* D)
{
T z[6];
z[0] = D[2];
z[1] = D[6];
z[2] = D[7];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = z[3] + z[4] + z[0] * T(-2) + T(-1);
z[5] = z[2] + z[5] * T(2) + -z[1];
return z[5] * T(4);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec6_component1(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[2];
z[5] = T(-2) + -z[1] + -z[4];
z[5] = z[2] * z[5];
z[6] = -(z[1] * z[3]);
z[5] = z[5] + z[6] + -z[0];
return z[0] * z[5] * T(2);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec6_component2(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[2];
z[5] = z[2] + z[3];
z[6] = -z[4] + -z[5];
z[6] = z[2] * z[6];
z[5] = -(z[1] * z[5]);
z[5] = z[5] + z[6] + -z[0];
z[5] = z[1] * z[5];
z[6] = -(z[2] * z[4]);
z[6] = z[0] + z[6];
z[6] = z[2] * z[6];
z[5] = z[5] + z[6];
return z[5] * T(2);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec6_component3(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[1];
z[5] = z[1] + T(1);
z[6] = z[5] + -z[3];
z[7] = -(z[3] * z[6]);
z[5] = z[2] + z[3] * T(2) + -z[5];
z[5] = z[2] * z[5];
z[5] = z[5] + z[7];
z[5] = z[4] * z[5];
z[7] = -(z[0] * z[6]);
z[6] = z[2] + -z[6];
z[6] = z[1] * z[6];
z[6] = z[0] + z[6];
z[6] = z[2] * z[6];
z[5] = z[5] + z[6] + z[7];
return z[5] * T(2);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec6_component8(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[2];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = z[1] * T(2) + -z[6];
z[10] = z[8] + z[9] + T(2) + z[4] * T(2);
z[10] = z[2] * z[10];
z[9] = z[7] + z[9];
z[9] = z[3] * z[9];
z[9] = z[6] + z[9] + z[10] + z[0] * T(2) + -z[5];
return z[9] * T(2);
}

template<typename T> T BubblePentagon_003ny___zzz__mvec6_divergence(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[2];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = z[1] * T(8) + -z[6];
z[10] = -z[7] + -z[9];
z[10] = z[3] * z[10];
z[9] = z[4] * T(-8) + T(-6) + -z[8] + -z[9];
z[9] = z[2] * z[9];
return z[5] + z[9] + z[10] + z[0] * T(-8) + -z[6];
}

template<typename T> std::vector<std::vector<T>> BubblePentagon_003ny___zzz__m_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(6, std::vector<T>(9, T(0)));
ibp_vectors[0][0] = BubblePentagon_003ny___zzz__mvec1_component1(invariants, propagators);
ibp_vectors[0][1] = BubblePentagon_003ny___zzz__mvec1_component2(invariants, propagators);
ibp_vectors[0][2] = BubblePentagon_003ny___zzz__mvec1_component3(invariants, propagators);
ibp_vectors[0][7] = BubblePentagon_003ny___zzz__mvec1_component8(invariants, propagators);
ibp_vectors[0].back() = BubblePentagon_003ny___zzz__mvec1_divergence(invariants, propagators);
ibp_vectors[1][0] = BubblePentagon_003ny___zzz__mvec2_component1(invariants, propagators);
ibp_vectors[1][1] = BubblePentagon_003ny___zzz__mvec2_component2(invariants, propagators);
ibp_vectors[1][2] = BubblePentagon_003ny___zzz__mvec2_component3(invariants, propagators);
ibp_vectors[1][7] = BubblePentagon_003ny___zzz__mvec2_component8(invariants, propagators);
ibp_vectors[1].back() = BubblePentagon_003ny___zzz__mvec2_divergence(invariants, propagators);
ibp_vectors[2][0] = BubblePentagon_003ny___zzz__mvec3_component1(invariants, propagators);
ibp_vectors[2][1] = BubblePentagon_003ny___zzz__mvec3_component2(invariants, propagators);
ibp_vectors[2][2] = BubblePentagon_003ny___zzz__mvec3_component3(invariants, propagators);
ibp_vectors[2][7] = BubblePentagon_003ny___zzz__mvec3_component8(invariants, propagators);
ibp_vectors[2].back() = BubblePentagon_003ny___zzz__mvec3_divergence(invariants, propagators);
ibp_vectors[5][0] = BubblePentagon_003ny___zzz__mvec6_component1(invariants, propagators);
ibp_vectors[5][1] = BubblePentagon_003ny___zzz__mvec6_component2(invariants, propagators);
ibp_vectors[5][2] = BubblePentagon_003ny___zzz__mvec6_component3(invariants, propagators);
ibp_vectors[5][7] = BubblePentagon_003ny___zzz__mvec6_component8(invariants, propagators);
ibp_vectors[5].back() = BubblePentagon_003ny___zzz__mvec6_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BubblePentagon_003ny___zzz__m_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BubblePentagon_003ny___zzz__m_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BubblePentagon_003ny___zzz__m_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BubblePentagon_003ny___zzz__m_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

