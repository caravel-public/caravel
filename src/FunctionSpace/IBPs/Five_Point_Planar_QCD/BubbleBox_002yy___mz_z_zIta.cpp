#include "FunctionSpace/IBPhelper.h"

#include "BubbleBox_002yy___mz_z_zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,34> BubbleBox_002yy___mz_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,34> BubbleBox_002yy___mz_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,34> BubbleBox_002yy___mz_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,34> BubbleBox_002yy___mz_z_zSurfaceIta<F32>();

#endif

}}}}

