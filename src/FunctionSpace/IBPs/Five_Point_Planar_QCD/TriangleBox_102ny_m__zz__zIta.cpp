#include "FunctionSpace/IBPhelper.h"

#include "TriangleBox_102ny_m__zz__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,38> TriangleBox_102ny_m__zz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,38> TriangleBox_102ny_m__zz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,38> TriangleBox_102ny_m__zz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,38> TriangleBox_102ny_m__zz__zSurfaceIta<F32>();

#endif

}}}}

