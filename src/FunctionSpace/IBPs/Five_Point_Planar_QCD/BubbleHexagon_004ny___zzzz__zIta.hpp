/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][9];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][9];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][9];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][9];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[3][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHexagon_004ny___zzzz__zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[16][9];
z[1] = ibp_vectors[16].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[16][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,15> BubbleHexagon_004ny___zzzz__zSurfaceItaArray;
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[0] = {BubbleHexagon_004ny___zzzz__zSurfaceIta1, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[1] = {BubbleHexagon_004ny___zzzz__zSurfaceIta2, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[2] = {BubbleHexagon_004ny___zzzz__zSurfaceIta3, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[3] = {BubbleHexagon_004ny___zzzz__zSurfaceIta4, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[4] = {BubbleHexagon_004ny___zzzz__zSurfaceIta5, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[5] = {BubbleHexagon_004ny___zzzz__zSurfaceIta6, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[6] = {BubbleHexagon_004ny___zzzz__zSurfaceIta7, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[7] = {BubbleHexagon_004ny___zzzz__zSurfaceIta8, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[8] = {BubbleHexagon_004ny___zzzz__zSurfaceIta9, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[9] = {BubbleHexagon_004ny___zzzz__zSurfaceIta10, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[10] = {BubbleHexagon_004ny___zzzz__zSurfaceIta11, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[11] = {BubbleHexagon_004ny___zzzz__zSurfaceIta12, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[12] = {BubbleHexagon_004ny___zzzz__zSurfaceIta13, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[13] = {BubbleHexagon_004ny___zzzz__zSurfaceIta14, -1};
BubbleHexagon_004ny___zzzz__zSurfaceItaArray[14] = {BubbleHexagon_004ny___zzzz__zSurfaceIta15, -1};
return BubbleHexagon_004ny___zzzz__zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta<F32>();

#endif

}}}}

