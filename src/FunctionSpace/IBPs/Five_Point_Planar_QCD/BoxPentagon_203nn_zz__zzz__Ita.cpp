#include "FunctionSpace/IBPhelper.h"

#include "BoxPentagon_203nn_zz__zzz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,73> BoxPentagon_203nn_zz__zzz__SurfaceIta<F32>();

#endif

}}}}

