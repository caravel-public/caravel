#ifndef TriangleHexagon_104nn_z__zzzz__Vectors_H_INC

#define TriangleHexagon_104nn_z__zzzz__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec1_component1(const T* invariants, const T* D)
{
T z[17];
z[0] = D[0];
z[1] = invariants[2];
z[2] = invariants[4];
z[3] = D[2];
z[4] = invariants[1];
z[5] = D[5];
z[6] = invariants[3];
z[7] = D[6];
z[8] = D[9];
z[9] = D[3];
z[10] = D[4];
z[11] = z[10] + -z[9];
z[12] = z[5] + -z[7];
z[13] = z[2] + T(-1);
z[14] = z[6] + z[13];
z[11] = z[11] * z[12] * z[14];
z[15] = z[7] * T(-2) + z[0] * T(4);
z[16] = z[13] + -z[1];
z[15] = z[15] * z[16];
z[13] = z[13] + z[1] * T(-2) + -z[6];
z[13] = z[5] * z[13];
z[16] = z[8] * z[14];
z[13] = z[13] + z[15] + z[16];
z[15] = z[0] * T(2);
z[13] = z[13] * z[15];
z[16] = z[5] + -z[9];
z[14] = z[14] * z[16];
z[12] = -z[12] + -z[15];
z[12] = z[3] * z[12];
z[12] = z[14] + z[12] * T(2);
z[12] = z[4] * z[12];
return z[11] + z[13] + z[12] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec1_component2(const T* invariants, const T* D)
{
T z[24];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[2];
z[3] = invariants[4];
z[4] = D[5];
z[5] = invariants[3];
z[6] = D[6];
z[7] = D[7];
z[8] = invariants[1];
z[9] = D[8];
z[10] = D[9];
z[11] = D[2];
z[12] = D[3];
z[13] = D[4];
z[14] = z[3] * T(2);
z[15] = z[0] + z[1] + -z[11];
z[15] = z[14] * z[15];
z[16] = z[4] + z[7] + z[6] * T(-2);
z[16] = z[11] * z[16];
z[17] = z[12] + -z[4];
z[17] = z[5] * z[17];
z[18] = z[9] + T(-2);
z[19] = z[18] + -z[7];
z[20] = z[0] * z[19];
z[19] = z[19] + z[11] * T(-4);
z[19] = z[1] * z[19];
z[15] = z[15] + z[16] + z[17] + z[19] + z[20];
z[15] = z[8] * z[15];
z[16] = z[7] * T(2) + -z[6];
z[17] = z[16] + -z[18];
z[17] = z[0] * z[17];
z[18] = z[0] * T(4);
z[19] = z[4] + -z[6];
z[20] = z[18] + z[19];
z[21] = -(z[1] * z[20]);
z[22] = -(z[11] * z[19]);
z[18] = z[4] + z[18];
z[23] = z[12] + -z[18];
z[23] = z[3] * z[23];
z[17] = z[17] + z[21] + z[22] + z[23];
z[17] = z[2] * z[17];
z[15] = z[15] + z[17];
z[17] = z[1] * T(2);
z[17] = z[17] * z[20];
z[20] = z[7] + -z[9];
z[21] = z[13] + -z[12];
z[20] = z[20] * z[21];
z[22] = z[0] * T(2);
z[16] = z[9] + T(-4) + -z[16];
z[16] = z[16] * z[22];
z[18] = z[18] + -z[13];
z[14] = z[14] * z[18];
z[18] = z[10] + -z[6];
z[18] = z[11] * z[18];
z[18] = z[13] + z[18] + -z[4];
z[14] = z[14] + z[16] + z[17] + z[20] + z[18] * T(2);
z[14] = z[3] * z[14];
z[16] = -(z[5] * z[19] * z[21]);
z[18] = z[4] + -z[10];
z[18] = z[5] * z[18];
z[18] = z[7] + z[18] + -z[6];
z[18] = z[18] * z[22];
return z[14] + z[16] + z[18] + z[15] * T(2) + -z[17];
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec1_component3(const T* invariants, const T* D)
{
T z[19];
z[0] = D[0];
z[1] = D[2];
z[2] = invariants[2];
z[3] = invariants[4];
z[4] = D[7];
z[5] = D[8];
z[6] = invariants[1];
z[7] = D[5];
z[8] = invariants[3];
z[9] = D[9];
z[10] = D[3];
z[11] = D[4];
z[12] = z[3] + T(-2);
z[12] = z[3] * z[12];
z[12] = z[12] + T(1);
z[13] = z[3] + T(-1);
z[14] = -(z[5] * z[13]);
z[15] = z[13] * T(2);
z[16] = z[5] + z[15];
z[16] = z[2] * z[16];
z[17] = z[13] + -z[2];
z[18] = z[4] * z[17];
z[14] = z[14] + z[16] + z[18] + z[12] * T(-2);
z[16] = z[0] * T(2);
z[14] = z[14] * z[16];
z[16] = z[8] * z[13];
z[12] = z[12] + z[16];
z[13] = z[8] + z[13];
z[16] = z[2] * z[13];
z[16] = z[16] + -z[12];
z[16] = z[7] * z[16];
z[18] = z[7] + -z[9];
z[18] = z[13] * z[18];
z[17] = z[0] * z[17];
z[17] = z[18] + z[17] * T(4);
z[17] = z[1] * z[17];
z[14] = z[14] + z[16] + z[17];
z[16] = z[4] + -z[5];
z[17] = -(z[13] * z[16]);
z[12] = z[17] + z[12] * T(2);
z[12] = z[11] * z[12];
z[17] = z[16] + z[2] * T(-2);
z[13] = z[10] * z[13] * z[17];
z[15] = z[15] + z[1] * T(-2) + -z[16];
z[15] = z[1] * z[6] * z[15];
return z[12] + z[13] + z[14] * T(2) + z[15] * T(4);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec1_component11(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = invariants[2];
z[2] = invariants[4];
z[3] = D[2];
z[4] = invariants[1];
z[5] = D[5];
z[6] = D[6];
z[7] = D[7];
z[8] = D[8];
z[9] = z[6] + z[0] * T(-4) + -z[5];
z[10] = z[2] + T(-1);
z[11] = z[10] + -z[1];
z[9] = z[9] * z[11];
z[10] = z[7] + z[10] * T(-2) + z[3] * T(4) + -z[8];
z[10] = z[4] * z[10];
z[9] = z[9] + z[10];
return z[9] * T(4);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec1_divergence(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[2];
z[2] = invariants[4];
z[3] = D[2];
z[4] = invariants[1];
z[5] = D[5];
z[6] = D[6];
z[7] = D[7];
z[8] = D[8];
z[9] = z[2] + T(-1);
z[10] = z[9] + -z[1];
z[11] = z[0] * z[10];
z[12] = z[3] * z[4];
z[11] = z[11] + -z[12];
z[12] = z[5] + -z[6];
z[10] = z[10] * z[12];
z[9] = z[8] + z[9] * T(2) + -z[7];
z[9] = z[4] * z[9];
z[9] = z[9] + z[10];
z[9] = z[9] * T(3) + z[11] * T(16);
return z[9] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec2_component1(const T* invariants, const T* D)
{
T z[23];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[2];
z[3] = invariants[4];
z[4] = D[2];
z[5] = invariants[1];
z[6] = D[6];
z[7] = D[7];
z[8] = D[8];
z[9] = D[5];
z[10] = invariants[3];
z[11] = D[9];
z[12] = D[3];
z[13] = D[4];
z[14] = z[0] + z[1];
z[15] = z[8] + T(-2) + -z[7];
z[14] = -(z[14] * z[15]);
z[15] = z[9] * z[10];
z[16] = z[15] + -z[9];
z[17] = z[0] * T(4);
z[18] = z[7] + z[9] + z[17] + z[6] * T(-2);
z[18] = z[4] * z[18];
z[19] = z[1] + z[4];
z[20] = z[0] + z[19];
z[20] = z[20] * T(-2) + -z[9];
z[20] = z[3] * z[20];
z[21] = z[10] + T(-1);
z[22] = z[3] + z[21];
z[22] = z[12] * z[22];
z[14] = z[14] + z[18] + z[20] + z[22] + -z[16];
z[14] = z[5] * z[14];
z[17] = z[17] + -z[6];
z[18] = -z[9] + -z[17];
z[18] = z[18] * z[19];
z[20] = z[6] + -z[8];
z[22] = T(-2) + -z[20];
z[22] = z[0] * z[22];
z[16] = z[16] + z[18] + z[22];
z[16] = z[2] * z[16];
z[17] = -z[11] + -z[17];
z[18] = -(z[17] * z[19]);
z[19] = z[0] * z[20];
z[15] = z[18] + z[19] + -z[15];
z[15] = z[3] * z[15];
z[18] = z[11] + -z[9];
z[18] = z[10] * z[18];
z[17] = z[17] + z[18];
z[17] = z[1] * z[17];
z[18] = z[7] + -z[6];
z[19] = z[0] * z[18];
z[14] = z[14] + z[15] + z[16] + z[17] + z[19];
z[15] = z[18] + z[2] * T(-2);
z[15] = z[15] * z[21];
z[16] = -(z[3] * z[20]);
z[15] = z[15] + z[16];
z[15] = z[12] * z[15];
z[16] = -(z[18] * z[21]);
z[17] = z[20] + z[10] * T(2);
z[17] = z[3] * z[17];
z[16] = z[16] + z[17];
z[16] = z[13] * z[16];
return z[15] + z[16] + z[14] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec2_component2(const T* invariants, const T* D)
{
T z[18];
z[0] = D[1];
z[1] = invariants[2];
z[2] = invariants[4];
z[3] = D[2];
z[4] = invariants[1];
z[5] = D[5];
z[6] = invariants[3];
z[7] = D[6];
z[8] = D[7];
z[9] = D[9];
z[10] = D[3];
z[11] = D[4];
z[12] = -(z[0] * z[9]);
z[13] = z[2] + -z[1];
z[14] = z[4] + z[13];
z[15] = z[0] + z[14];
z[15] = z[5] * z[15];
z[12] = z[12] + z[15];
z[15] = z[8] + z[2] * T(-2);
z[15] = z[11] * z[15];
z[16] = z[4] + -z[1];
z[16] = z[16] * T(-2) + -z[8];
z[16] = z[10] * z[16];
z[17] = z[10] + -z[11];
z[17] = z[7] * z[17];
z[12] = z[15] + z[16] + z[17] + z[12] * T(2);
z[12] = z[6] * z[12];
z[13] = z[13] + T(-1);
z[15] = z[0] * z[13];
z[14] = z[3] * z[14];
z[15] = z[14] + z[15];
z[16] = z[7] + -z[8];
z[16] = z[15] * z[16];
z[13] = z[2] * z[13];
z[13] = z[13] + z[15];
z[13] = z[0] * z[13];
z[14] = z[2] * z[14];
z[13] = z[13] + z[14];
z[13] = z[16] + z[13] * T(2);
return z[12] + z[13] * T(4);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec2_component3(const T* invariants, const T* D)
{
T z[24];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[2];
z[3] = invariants[4];
z[4] = D[7];
z[5] = D[8];
z[6] = invariants[1];
z[7] = D[5];
z[8] = invariants[3];
z[9] = D[9];
z[10] = D[3];
z[11] = D[4];
z[12] = z[5] + -z[4];
z[13] = z[12] + T(-2);
z[14] = z[3] * T(2);
z[15] = z[13] + z[14];
z[16] = z[2] * T(2);
z[17] = z[16] + -z[15];
z[17] = z[3] * z[17];
z[18] = -(z[6] * z[15]);
z[19] = z[1] * T(2);
z[20] = z[3] + -z[2];
z[21] = z[6] + z[20];
z[21] = z[19] * z[21];
z[22] = z[2] * z[13];
z[20] = z[20] + T(-1);
z[23] = z[0] * z[20];
z[17] = z[17] + z[18] + z[21] + z[22] + z[23] * T(2);
z[17] = z[1] * z[17];
z[14] = z[16] + T(4) + -z[12] + -z[14];
z[14] = z[3] * z[14];
z[18] = z[2] + T(1);
z[13] = z[13] * z[18];
z[13] = z[13] + z[14];
z[13] = z[0] * z[13];
z[13] = z[13] + z[17];
z[14] = -(z[11] * z[15]);
z[12] = z[12] + z[16];
z[12] = z[10] * z[12];
z[15] = z[9] + -z[7];
z[15] = z[15] * z[19];
z[16] = z[7] * z[20];
z[12] = z[12] + z[14] + z[15] + z[16] * T(2);
z[12] = z[8] * z[12];
return z[12] + z[13] * T(4);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec2_component11(const T* invariants, const T* D)
{
T z[12];
z[0] = D[1];
z[1] = invariants[2];
z[2] = invariants[4];
z[3] = D[2];
z[4] = invariants[1];
z[5] = D[6];
z[6] = D[7];
z[7] = D[8];
z[8] = z[7] + z[3] * T(-4);
z[9] = z[2] + -z[1];
z[10] = z[4] + z[9];
z[8] = z[8] * z[10];
z[10] = z[2] + T(-1);
z[10] = z[4] * z[10];
z[9] = z[9] + T(-1);
z[11] = z[0] * z[9];
z[10] = z[1] + z[10] + z[11] * T(-2);
z[11] = T(-1) + -z[4];
z[11] = z[6] * z[11];
z[9] = -(z[5] * z[9]);
z[8] = z[8] + z[9] + z[11] + z[10] * T(2);
return z[8] * T(4);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec2_divergence(const T* invariants, const T* D)
{
T z[14];
z[0] = D[1];
z[1] = invariants[2];
z[2] = invariants[4];
z[3] = D[2];
z[4] = invariants[1];
z[5] = D[6];
z[6] = D[7];
z[7] = D[8];
z[8] = T(1) + -z[2];
z[8] = z[4] * z[8];
z[8] = z[8] + -z[1];
z[9] = z[2] + -z[1];
z[10] = z[4] + z[9];
z[11] = -(z[7] * z[10]);
z[9] = z[9] + T(-1);
z[12] = z[5] * z[9];
z[13] = z[4] + T(1);
z[13] = z[6] * z[13];
z[8] = z[11] + z[12] + z[13] + z[8] * T(2);
z[10] = z[3] * z[10];
z[9] = z[0] * z[9];
z[9] = z[9] + z[10];
z[8] = z[8] * T(3) + z[9] * T(16);
return z[8] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec6_component1(const T* invariants, const T* D)
{
T z[24];
z[0] = D[0];
z[1] = D[5];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = invariants[4];
z[5] = D[6];
z[6] = D[7];
z[7] = D[1];
z[8] = D[9];
z[9] = D[2];
z[10] = D[3];
z[11] = invariants[3];
z[12] = D[4];
z[13] = z[2] + -z[3];
z[14] = z[13] * T(2);
z[15] = -(z[2] * z[4]);
z[15] = z[15] + -z[14];
z[15] = z[1] * z[15];
z[16] = z[1] * z[11];
z[17] = z[4] + -z[3];
z[18] = z[16] * z[17];
z[19] = z[2] * T(2);
z[20] = z[19] + -z[8];
z[21] = z[4] * z[20];
z[22] = z[19] + -z[3] + -z[4];
z[22] = z[5] * z[22];
z[23] = -(z[2] * z[6]);
z[15] = z[15] + z[18] + z[21] + z[22] + z[23];
z[15] = z[10] * z[15];
z[13] = z[4] + z[13];
z[18] = z[1] * z[13];
z[22] = z[6] + -z[5];
z[23] = z[18] + -z[14] + -z[22];
z[23] = z[0] * z[23];
z[13] = z[13] + T(-1);
z[13] = z[1] * z[13];
z[13] = z[13] + -z[20];
z[13] = z[7] * z[13];
z[18] = z[9] * z[18];
z[13] = z[13] + z[18] + z[23] + -z[21];
z[18] = z[1] * z[4];
z[16] = z[16] + z[18];
z[14] = z[16] + z[22] + -z[14];
z[14] = z[10] * z[14];
z[16] = -(z[12] * z[16]);
z[17] = z[7] + z[17];
z[17] = z[12] + z[17] * T(2);
z[17] = z[5] * z[17];
z[18] = z[19] + -z[12];
z[18] = z[6] * z[18];
z[13] = z[14] + z[16] + z[17] + z[18] + z[13] * T(2);
z[13] = z[0] * z[13];
return z[13] + z[15];
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec6_component2(const T* invariants, const T* D)
{
T z[21];
z[0] = D[0];
z[1] = D[1];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = invariants[4];
z[6] = D[6];
z[7] = D[7];
z[8] = D[9];
z[9] = D[2];
z[10] = D[3];
z[11] = invariants[3];
z[12] = D[4];
z[13] = z[1] + z[5];
z[14] = z[6] + z[8] + z[3] * T(-2);
z[14] = z[13] * z[14];
z[15] = z[3] * z[7];
z[16] = -(z[4] * z[6]);
z[14] = z[14] + z[15] + z[16];
z[14] = z[1] * z[14];
z[15] = z[3] + -z[4];
z[16] = z[5] + z[15];
z[17] = z[16] + T(-1);
z[17] = z[1] * z[13] * z[17];
z[18] = z[13] * z[16];
z[19] = z[9] * z[18];
z[17] = z[17] + z[19];
z[17] = z[2] * z[17];
z[19] = z[6] + -z[7];
z[15] = z[15] * T(2);
z[20] = z[19] + -z[15];
z[20] = z[1] * z[20];
z[18] = z[2] * z[18];
z[18] = z[18] + z[20];
z[18] = z[0] * z[18];
z[14] = z[14] + z[17] + z[18];
z[17] = z[5] + z[11];
z[13] = z[2] * z[13] * z[17];
z[16] = -(z[16] * z[19]);
z[15] = -z[15] + -z[19];
z[15] = z[1] * z[15];
z[15] = z[13] + z[15] + z[16] * T(2);
z[15] = z[10] * z[15];
z[16] = z[1] * z[19];
z[13] = z[16] + -z[13];
z[13] = z[12] * z[13];
return z[13] + z[15] + z[14] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec6_component3(const T* invariants, const T* D)
{
T z[29];
z[0] = D[0];
z[1] = D[2];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = invariants[4];
z[6] = D[6];
z[7] = D[7];
z[8] = invariants[3];
z[9] = D[1];
z[10] = D[9];
z[11] = D[3];
z[12] = D[4];
z[13] = D[8];
z[14] = z[3] + -z[4];
z[15] = z[5] + z[8];
z[16] = z[1] * z[15];
z[17] = z[4] + T(2);
z[18] = z[8] * z[17];
z[19] = z[3] + z[8] * T(-2) + T(1) + -z[5];
z[19] = z[5] * z[19];
z[18] = z[16] + z[18] + z[19] + -z[14];
z[18] = z[2] * z[18];
z[19] = z[4] * T(2);
z[20] = z[3] * T(2);
z[21] = z[19] + -z[20];
z[22] = z[7] + -z[6];
z[23] = z[21] + z[22];
z[23] = z[1] * z[23];
z[19] = z[19] + -z[7] + -z[10];
z[19] = z[8] * z[19];
z[24] = -(z[6] * z[17]);
z[25] = z[4] + T(1);
z[26] = z[25] * T(2) + -z[3];
z[26] = z[7] * z[26];
z[27] = z[5] * z[22];
z[28] = z[13] * z[14];
z[18] = z[18] + z[19] + z[23] + z[24] + z[26] + z[28] + z[27] * T(-2);
z[18] = z[11] * z[18];
z[19] = z[15] + -z[25];
z[19] = z[5] * z[19];
z[23] = z[3] + z[5];
z[24] = z[23] + -z[4];
z[24] = z[1] * z[24];
z[19] = z[24] + -z[19];
z[24] = z[4] * z[8];
z[14] = z[14] + z[19] + z[24];
z[14] = z[2] * z[14];
z[20] = z[20] + -z[6] + -z[10];
z[24] = -(z[5] * z[20]);
z[26] = -(z[4] * z[6]);
z[27] = z[3] * z[7];
z[14] = z[14] + z[24] + z[26] + z[27];
z[14] = z[1] * z[14];
z[17] = z[17] + -z[15];
z[17] = z[5] * z[17];
z[23] = z[23] + -z[25];
z[23] = z[1] * z[23];
z[24] = z[8] * z[25];
z[17] = z[17] + z[23] + z[24] + -z[25];
z[17] = z[2] * z[17];
z[20] = -(z[1] * z[20]);
z[17] = z[17] + z[20];
z[17] = z[9] * z[17];
z[20] = z[21] + -z[22];
z[20] = z[1] * z[20];
z[19] = z[19] + z[24];
z[19] = z[2] * z[19];
z[19] = z[19] + z[20];
z[19] = z[0] * z[19];
z[14] = z[14] + z[17] + z[19];
z[15] = z[15] + T(-1);
z[15] = z[5] * z[15];
z[15] = z[15] + -z[16];
z[15] = z[2] * z[15];
z[16] = -(z[1] * z[22]);
z[15] = z[15] + z[16];
z[15] = z[12] * z[15];
return z[15] + z[18] + z[14] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec6_component11(const T* invariants, const T* D)
{
T z[25];
z[0] = D[0];
z[1] = D[5];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = invariants[4];
z[5] = D[6];
z[6] = D[7];
z[7] = D[1];
z[8] = D[9];
z[9] = D[2];
z[10] = D[3];
z[11] = invariants[3];
z[12] = D[4];
z[13] = D[8];
z[14] = z[13] + z[9] * T(2);
z[15] = z[3] + -z[2];
z[16] = z[15] + -z[4];
z[14] = z[14] * z[16];
z[17] = z[2] * T(2);
z[18] = z[17] + -z[8];
z[19] = z[10] + -z[12];
z[20] = -z[18] + -z[19];
z[20] = z[4] * z[20];
z[21] = z[4] + -z[3];
z[21] = z[19] + z[21] * T(-2);
z[22] = z[8] + -z[21];
z[22] = z[11] * z[22];
z[23] = z[5] + z[15] * T(2);
z[24] = -z[11] + -z[15];
z[24] = z[1] * z[24];
z[14] = z[14] + z[20] + z[22] + z[23] + z[24];
z[14] = z[1] * z[14];
z[20] = z[16] + T(1);
z[20] = z[1] * z[20];
z[20] = z[18] + z[20] + -z[5];
z[20] = z[7] * z[20];
z[16] = z[1] * z[16];
z[16] = z[6] + z[16] + -z[23];
z[16] = z[0] * z[16];
z[15] = -(z[10] * z[15]);
z[18] = z[4] * z[18];
z[15] = z[15] + z[16] + z[18] + z[20];
z[16] = z[5] * z[21];
z[17] = -z[1] + -z[17] + -z[19];
z[17] = z[6] * z[17];
z[14] = z[14] + z[16] + z[17] + z[15] * T(2);
return z[14] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec6_divergence(const T* invariants, const T* D)
{
T z[24];
z[0] = D[0];
z[1] = D[5];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = invariants[4];
z[5] = D[6];
z[6] = D[7];
z[7] = D[1];
z[8] = D[9];
z[9] = D[2];
z[10] = D[3];
z[11] = invariants[3];
z[12] = D[4];
z[13] = D[8];
z[14] = z[2] + -z[3];
z[15] = z[13] + T(2);
z[15] = z[14] * z[15];
z[16] = z[10] * T(2);
z[17] = z[16] + -z[8];
z[18] = z[2] * T(2);
z[19] = z[13] + z[17] + z[18];
z[19] = z[4] * z[19];
z[20] = z[3] * T(2);
z[17] = z[17] + z[20] + z[4] * T(-2);
z[17] = z[11] * z[17];
z[21] = z[6] + -z[5];
z[22] = z[4] + z[14];
z[23] = z[9] * z[22];
z[14] = z[11] + -z[14];
z[14] = z[1] * z[14];
z[14] = z[14] + z[15] + z[17] + z[19] + z[21] + z[23] * T(4);
z[14] = z[1] * z[14];
z[15] = z[20] + -z[18];
z[17] = z[1] * z[22];
z[17] = z[15] + z[17] + -z[21];
z[17] = z[0] * z[17];
z[18] = z[5] + z[8] + -z[18];
z[19] = z[22] + T(-1);
z[19] = z[1] * z[19];
z[19] = z[18] + z[19];
z[19] = z[7] * z[19];
z[17] = z[17] + z[19];
z[19] = -(z[3] * z[5]);
z[20] = z[2] * z[6];
z[18] = z[4] * z[18];
z[18] = z[18] + z[19] + z[20];
z[15] = z[15] + z[21];
z[15] = z[15] * z[16];
z[16] = -z[4] + -z[11];
z[16] = z[1] * z[16];
z[16] = z[16] + -z[21];
z[16] = z[12] * z[16];
z[14] = z[14] + z[15] + z[16] * T(2) + z[18] * T(3) + z[17] * T(4);
return z[14] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec7_component1(const T* invariants, const T* D)
{
T z[23];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[8];
z[4] = D[9];
z[5] = invariants[2];
z[6] = invariants[4];
z[7] = D[1];
z[8] = D[2];
z[9] = D[3];
z[10] = invariants[3];
z[11] = D[4];
z[12] = D[5];
z[13] = D[6];
z[14] = z[6] * T(2) + -z[1];
z[15] = T(2) + -z[3] + -z[14];
z[16] = z[2] * z[15];
z[17] = z[4] * z[6];
z[18] = z[17] + -z[4];
z[19] = z[4] * z[10];
z[20] = -(z[8] * z[14]);
z[18] = z[20] + z[18] * T(-2) + -z[16] + -z[19];
z[18] = z[2] * z[18];
z[16] = z[16] + z[17];
z[20] = T(-1) + -z[5];
z[20] = z[4] * z[20];
z[20] = z[16] + z[20];
z[20] = z[0] * z[20];
z[21] = -(z[10] * z[17]);
z[19] = z[19] + -z[4];
z[22] = z[5] * z[19];
z[18] = z[18] + z[20] + z[21] + z[22];
z[18] = z[0] * z[18];
z[20] = z[2] * z[14];
z[17] = z[20] + -z[17];
z[17] = z[8] * z[17];
z[16] = z[16] + z[19];
z[15] = z[0] * z[15];
z[15] = z[15] + -z[16];
z[15] = z[7] * z[15];
z[15] = z[15] + z[17];
z[15] = z[2] * z[15];
z[15] = z[15] + z[18];
z[17] = -(z[2] * z[16]);
z[18] = z[2] * z[5];
z[19] = z[13] + -z[12];
z[19] = z[18] * z[19];
z[18] = z[16] + z[18] * T(-2);
z[18] = z[0] * z[18];
z[17] = z[17] + z[18] + z[19];
z[17] = z[9] * z[17];
z[18] = z[3] * z[6];
z[14] = -(z[10] * z[14]);
z[14] = z[14] + z[18] + -z[1];
z[14] = z[2] * z[14];
z[16] = -(z[0] * z[16]);
z[14] = z[14] + z[16];
z[14] = z[11] * z[14];
return z[14] + z[17] + z[15] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec7_component2(const T* invariants, const T* D)
{
T z[24];
z[0] = D[0];
z[1] = D[1];
z[2] = D[7];
z[3] = invariants[1];
z[4] = D[8];
z[5] = D[9];
z[6] = invariants[2];
z[7] = invariants[4];
z[8] = invariants[3];
z[9] = D[2];
z[10] = D[3];
z[11] = D[4];
z[12] = D[6];
z[13] = z[7] * T(2) + -z[2];
z[14] = z[4] + T(-2);
z[15] = z[13] + z[14];
z[16] = z[0] + z[1];
z[16] = z[15] * z[16];
z[13] = z[9] * z[13];
z[16] = z[13] + z[16];
z[16] = z[3] * z[16];
z[17] = z[1] + z[7];
z[18] = z[15] * z[17];
z[19] = z[5] * z[8];
z[19] = z[19] + -z[18];
z[19] = z[1] * z[19];
z[13] = -(z[13] * z[17]);
z[20] = z[7] + T(-1);
z[21] = z[8] + z[20];
z[22] = z[5] * z[21];
z[18] = z[22] + -z[18];
z[18] = z[0] * z[18];
z[13] = z[13] + z[18] + z[19] + -z[16];
z[13] = z[3] * z[13];
z[18] = z[0] * z[5];
z[19] = -z[1] + -z[21];
z[19] = z[18] * z[19];
z[16] = z[16] + z[19];
z[16] = z[6] * z[16];
z[19] = z[7] * z[21];
z[23] = z[1] * z[20];
z[23] = z[19] + z[23];
z[18] = z[18] * z[23];
z[13] = z[13] + z[16] + z[18];
z[16] = z[8] * T(2);
z[18] = z[16] + z[20];
z[18] = z[7] * z[18];
z[20] = z[1] * z[21];
z[18] = z[18] + z[20];
z[18] = z[5] * z[18];
z[17] = -z[3] + -z[17];
z[17] = z[15] * z[17];
z[17] = z[17] + z[22];
z[17] = z[3] * z[17];
z[21] = z[8] + T(-1);
z[21] = -(z[5] * z[21]);
z[14] = z[14] + z[1] * T(-2) + -z[12];
z[14] = z[3] * z[14];
z[14] = z[14] + z[21];
z[14] = z[6] * z[14];
z[14] = z[14] + z[17] + z[18];
z[14] = z[10] * z[14];
z[16] = z[15] + z[16];
z[16] = z[7] * z[16];
z[15] = z[1] * z[15];
z[17] = -(z[2] * z[8]);
z[15] = z[15] + z[16] + z[17];
z[15] = z[3] * z[15];
z[16] = -z[19] + -z[20];
z[16] = z[5] * z[16];
z[15] = z[15] + z[16];
z[15] = z[11] * z[15];
return z[14] + z[15] + z[13] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec7_component3(const T* invariants, const T* D)
{
T z[19];
z[0] = D[0];
z[1] = D[2];
z[2] = D[7];
z[3] = invariants[1];
z[4] = D[8];
z[5] = D[9];
z[6] = invariants[2];
z[7] = invariants[4];
z[8] = D[1];
z[9] = D[3];
z[10] = invariants[3];
z[11] = D[4];
z[12] = z[4] + -z[2];
z[13] = z[7] + T(-1);
z[14] = z[13] + -z[1];
z[15] = z[12] * z[14];
z[16] = z[1] * z[13];
z[17] = z[7] + T(-2);
z[17] = z[7] * z[17];
z[16] = z[16] + T(-1) + -z[17];
z[15] = z[16] * T(2) + -z[15];
z[12] = z[12] + z[13] * T(2);
z[12] = z[6] * z[12];
z[12] = z[12] + z[15];
z[13] = -z[0] + -z[8];
z[12] = z[12] * z[13];
z[13] = z[1] * T(2);
z[17] = z[7] * z[13];
z[18] = z[1] * z[2];
z[17] = z[17] + -z[18];
z[18] = z[14] + -z[6];
z[17] = z[17] * z[18];
z[12] = z[12] + z[17];
z[12] = z[3] * z[12];
z[17] = z[6] * z[14];
z[17] = z[16] + z[17];
z[17] = z[0] * z[5] * z[17];
z[12] = z[12] + z[17];
z[13] = -(z[6] * z[13]);
z[13] = z[13] + -z[15];
z[13] = z[3] * z[13];
z[14] = z[10] * z[14];
z[14] = z[16] + -z[14];
z[14] = z[5] * z[14];
z[13] = z[13] + z[14];
z[13] = z[9] * z[13];
z[15] = z[3] * z[15];
z[14] = z[15] + -z[14];
z[14] = z[11] * z[14];
return z[13] + z[14] + z[12] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec7_component11(const T* invariants, const T* D)
{
T z[21];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[8];
z[4] = D[9];
z[5] = invariants[2];
z[6] = invariants[4];
z[7] = D[1];
z[8] = D[2];
z[9] = D[3];
z[10] = invariants[3];
z[11] = D[4];
z[12] = D[5];
z[13] = D[6];
z[14] = z[6] * z[10];
z[15] = z[10] + T(-1);
z[15] = -(z[5] * z[15]);
z[16] = z[6] + T(-1);
z[17] = z[16] + -z[5];
z[18] = -(z[0] * z[17]);
z[14] = z[14] + z[15] + z[18];
z[15] = z[4] + z[11] + -z[9];
z[18] = z[10] + z[16];
z[15] = z[15] * z[18];
z[17] = -(z[13] * z[17]);
z[18] = -z[5] + -z[10];
z[18] = z[12] * z[18];
z[14] = z[15] + z[17] + z[18] + z[14] * T(2);
z[14] = z[4] * z[14];
z[15] = z[16] * T(2);
z[16] = z[1] + -z[3];
z[17] = z[15] + -z[16];
z[18] = z[0] * z[17];
z[15] = z[3] + z[15];
z[15] = z[7] * z[15];
z[19] = z[6] * z[8];
z[20] = -z[7] + -z[8];
z[20] = z[1] * z[20];
z[15] = z[15] + z[18] + z[20] + z[19] * T(2);
z[18] = z[17] + z[5] * T(2);
z[18] = z[9] * z[18];
z[17] = -(z[11] * z[17]);
z[16] = z[4] * z[16];
z[15] = z[16] + z[17] + z[18] + z[15] * T(2);
z[15] = z[2] * z[15];
z[14] = z[14] + z[15];
return z[14] * T(2);
}

template<typename T> T TriangleHexagon_104nn_z__zzzz__vec7_divergence(const T* invariants, const T* D)
{
T z[26];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[8];
z[4] = D[9];
z[5] = invariants[2];
z[6] = invariants[4];
z[7] = D[1];
z[8] = D[2];
z[9] = D[3];
z[10] = invariants[3];
z[11] = D[4];
z[12] = D[5];
z[13] = D[6];
z[14] = z[11] * T(2);
z[15] = z[9] * T(2);
z[16] = z[15] + -z[4] + -z[14];
z[17] = z[6] + T(-1);
z[18] = z[10] + z[17];
z[16] = z[16] * z[18];
z[18] = z[0] * T(4);
z[19] = z[17] + -z[5];
z[19] = z[18] * z[19];
z[20] = z[1] + -z[3];
z[21] = -z[17] + -z[20];
z[21] = z[2] * z[21];
z[22] = z[13] * z[17];
z[23] = z[6] * T(2);
z[24] = z[12] + -z[23];
z[24] = z[10] * z[24];
z[25] = z[12] + T(-2) + z[10] * T(2) + -z[13];
z[25] = z[5] * z[25];
z[16] = z[16] + z[19] + z[21] + z[22] + z[24] + z[25];
z[16] = z[4] * z[16];
z[14] = z[14] + z[7] * T(-4) + -z[18];
z[17] = z[17] * T(2) + -z[20];
z[14] = z[14] * z[17];
z[17] = z[5] * T(-2) + -z[17];
z[15] = z[15] * z[17];
z[17] = -(z[3] * z[6]);
z[18] = z[3] + T(-2);
z[18] = z[5] * z[18];
z[19] = z[1] + -z[23];
z[19] = z[8] * z[19];
z[14] = z[1] + z[14] + z[15] + z[17] + z[18] + z[19] * T(4);
z[14] = z[2] * z[14];
z[14] = z[14] + z[16];
return z[14] * T(2);
}

template<typename T> std::vector<std::vector<T>> TriangleHexagon_104nn_z__zzzz___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(7, std::vector<T>(12, T(0)));
ibp_vectors[0][0] = TriangleHexagon_104nn_z__zzzz__vec1_component1(invariants, propagators);
ibp_vectors[0][1] = TriangleHexagon_104nn_z__zzzz__vec1_component2(invariants, propagators);
ibp_vectors[0][2] = TriangleHexagon_104nn_z__zzzz__vec1_component3(invariants, propagators);
ibp_vectors[0][10] = TriangleHexagon_104nn_z__zzzz__vec1_component11(invariants, propagators);
ibp_vectors[0].back() = TriangleHexagon_104nn_z__zzzz__vec1_divergence(invariants, propagators);
ibp_vectors[1][0] = TriangleHexagon_104nn_z__zzzz__vec2_component1(invariants, propagators);
ibp_vectors[1][1] = TriangleHexagon_104nn_z__zzzz__vec2_component2(invariants, propagators);
ibp_vectors[1][2] = TriangleHexagon_104nn_z__zzzz__vec2_component3(invariants, propagators);
ibp_vectors[1][10] = TriangleHexagon_104nn_z__zzzz__vec2_component11(invariants, propagators);
ibp_vectors[1].back() = TriangleHexagon_104nn_z__zzzz__vec2_divergence(invariants, propagators);
ibp_vectors[5][0] = TriangleHexagon_104nn_z__zzzz__vec6_component1(invariants, propagators);
ibp_vectors[5][1] = TriangleHexagon_104nn_z__zzzz__vec6_component2(invariants, propagators);
ibp_vectors[5][2] = TriangleHexagon_104nn_z__zzzz__vec6_component3(invariants, propagators);
ibp_vectors[5][10] = TriangleHexagon_104nn_z__zzzz__vec6_component11(invariants, propagators);
ibp_vectors[5].back() = TriangleHexagon_104nn_z__zzzz__vec6_divergence(invariants, propagators);
ibp_vectors[6][0] = TriangleHexagon_104nn_z__zzzz__vec7_component1(invariants, propagators);
ibp_vectors[6][1] = TriangleHexagon_104nn_z__zzzz__vec7_component2(invariants, propagators);
ibp_vectors[6][2] = TriangleHexagon_104nn_z__zzzz__vec7_component3(invariants, propagators);
ibp_vectors[6][10] = TriangleHexagon_104nn_z__zzzz__vec7_component11(invariants, propagators);
ibp_vectors[6].back() = TriangleHexagon_104nn_z__zzzz__vec7_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleHexagon_104nn_z__zzzz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleHexagon_104nn_z__zzzz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleHexagon_104nn_z__zzzz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleHexagon_104nn_z__zzzz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

