#include "FunctionSpace/IBPhelper.h"

#include "BubbleBox_002ny___zm__mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,6> BubbleBox_002ny___zm__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,6> BubbleBox_002ny___zm__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,6> BubbleBox_002ny___zm__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,6> BubbleBox_002ny___zm__mSurfaceIta<F32>();

#endif

}}}}

