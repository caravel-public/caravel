/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][8];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[4][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][1];
z[5] = ibp_vectors[4][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[4][8];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[4][8];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][2];
z[6] = ibp_vectors[4][1];
z[7] = ibp_vectors[4][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[4][8];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][3];
z[6] = ibp_vectors[4][1];
z[7] = ibp_vectors[4][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][8];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][3];
z[6] = ibp_vectors[4][2];
z[7] = ibp_vectors[4][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][8];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][3];
z[6] = ibp_vectors[4][2];
z[7] = ibp_vectors[4][1];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[19][8];
z[2] = ibp_vectors[19].back();
z[3] = ibp_vectors[19][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[19][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[19][8];
z[3] = ibp_vectors[19].back();
z[4] = ibp_vectors[19][1];
z[5] = ibp_vectors[19][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[19][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[19][8];
z[2] = ibp_vectors[19].back();
z[3] = ibp_vectors[19][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[19][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[26][8];
z[1] = ibp_vectors[26].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[26][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[26][8];
z[2] = ibp_vectors[26].back();
z[3] = ibp_vectors[26][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[26][8];
z[2] = ibp_vectors[26].back();
z[3] = ibp_vectors[26][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[26][8];
z[2] = ibp_vectors[26].back();
z[3] = ibp_vectors[26][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[26][8];
z[2] = ibp_vectors[26].back();
z[3] = ibp_vectors[26][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[26][8];
z[3] = ibp_vectors[26].back();
z[4] = ibp_vectors[26][2];
z[5] = ibp_vectors[26][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[26][8];
z[3] = ibp_vectors[26].back();
z[4] = ibp_vectors[26][3];
z[5] = ibp_vectors[26][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[26][8];
z[3] = ibp_vectors[26].back();
z[4] = ibp_vectors[26][2];
z[5] = ibp_vectors[26][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[26][8];
z[3] = ibp_vectors[26].back();
z[4] = ibp_vectors[26][3];
z[5] = ibp_vectors[26][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[26][8];
z[3] = ibp_vectors[26].back();
z[4] = ibp_vectors[26][3];
z[5] = ibp_vectors[26][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[26][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[26][8];
z[2] = ibp_vectors[26].back();
z[3] = ibp_vectors[26][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[26][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[36][8];
z[1] = ibp_vectors[36].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[36][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[36][8];
z[2] = ibp_vectors[36].back();
z[3] = ibp_vectors[36][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[36][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[36][8];
z[2] = ibp_vectors[36].back();
z[3] = ibp_vectors[36][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[36][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[36][8];
z[2] = ibp_vectors[36].back();
z[3] = ibp_vectors[36][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[36][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[36][8];
z[3] = ibp_vectors[36].back();
z[4] = ibp_vectors[36][1];
z[5] = ibp_vectors[36][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[36][8];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[36][8];
z[3] = ibp_vectors[36].back();
z[4] = ibp_vectors[36][2];
z[5] = ibp_vectors[36][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[36][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[36][8];
z[3] = ibp_vectors[36].back();
z[4] = ibp_vectors[36][3];
z[5] = ibp_vectors[36][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[36][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[36][8];
z[4] = ibp_vectors[36].back();
z[5] = ibp_vectors[36][3];
z[6] = ibp_vectors[36][1];
z[7] = ibp_vectors[36][0];
z[8] = z[4] + z[3] * T(3);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[36][8];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[36][8];
z[3] = ibp_vectors[36].back();
z[4] = ibp_vectors[36][3];
z[5] = ibp_vectors[36][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[36][8];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[36][8];
z[3] = ibp_vectors[36].back();
z[4] = ibp_vectors[36][3];
z[5] = ibp_vectors[36][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[36][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[38][8];
z[1] = ibp_vectors[38].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[38][8];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[38][8];
z[2] = ibp_vectors[38].back();
z[3] = ibp_vectors[38][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[38][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[38][8];
z[2] = ibp_vectors[38].back();
z[3] = ibp_vectors[38][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[38][8];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[38][8];
z[2] = ibp_vectors[38].back();
z[3] = ibp_vectors[38][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[38][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[38][8];
z[2] = ibp_vectors[38].back();
z[3] = ibp_vectors[38][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[38][8];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[38][8];
z[3] = ibp_vectors[38].back();
z[4] = ibp_vectors[38][3];
z[5] = ibp_vectors[38][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[38][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101yy_z__m_z_zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[38][8];
z[3] = ibp_vectors[38].back();
z[4] = ibp_vectors[38][3];
z[5] = ibp_vectors[38][1];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[38][8];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,58> TriangleTriangle_101yy_z__m_z_zSurfaceItaArray;
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[0] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta1, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[1] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta2, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[2] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta3, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[3] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta4, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[4] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta5, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[5] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta6, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[6] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta7, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[7] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta8, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[8] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta9, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[9] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta10, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[10] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta11, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[11] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta12, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[12] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta13, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[13] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta14, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[14] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta15, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[15] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta16, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[16] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta17, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[17] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta18, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[18] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta19, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[19] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta20, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[20] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta21, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[21] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta22, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[22] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta23, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[23] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta24, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[24] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta25, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[25] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta26, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[26] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta27, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[27] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta28, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[28] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta29, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[29] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta30, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[30] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta31, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[31] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta32, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[32] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta33, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[33] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta34, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[34] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta35, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[35] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta36, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[36] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta37, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[37] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta38, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[38] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta39, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[39] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta40, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[40] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta41, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[41] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta42, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[42] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta43, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[43] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta44, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[44] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta45, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[45] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta46, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[46] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta47, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[47] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta48, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[48] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta49, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[49] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta50, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[50] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta51, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[51] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta52, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[52] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta53, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[53] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta54, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[54] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta55, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[55] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta56, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[56] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta57, -1};
TriangleTriangle_101yy_z__m_z_zSurfaceItaArray[57] = {TriangleTriangle_101yy_z__m_z_zSurfaceIta58, -1};
return TriangleTriangle_101yy_z__m_z_zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta<F32>();

#endif

}}}}

