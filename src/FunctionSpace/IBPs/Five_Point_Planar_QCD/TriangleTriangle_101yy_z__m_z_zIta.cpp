#include "FunctionSpace/IBPhelper.h"

#include "TriangleTriangle_101yy_z__m_z_zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,58> TriangleTriangle_101yy_z__m_z_zSurfaceIta<F32>();

#endif

}}}}

