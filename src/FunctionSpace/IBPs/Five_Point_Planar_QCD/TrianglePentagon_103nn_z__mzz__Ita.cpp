#include "FunctionSpace/IBPhelper.h"

#include "TrianglePentagon_103nn_z__mzz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,10> TrianglePentagon_103nn_z__mzz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,10> TrianglePentagon_103nn_z__mzz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,10> TrianglePentagon_103nn_z__mzz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,10> TrianglePentagon_103nn_z__mzz__SurfaceIta<F32>();

#endif

}}}}

