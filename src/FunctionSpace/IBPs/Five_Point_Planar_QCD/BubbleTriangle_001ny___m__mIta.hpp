/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubbleTriangle_001ny___m__mSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][3];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][3];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001ny___m__mSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][3];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][3];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001ny___m__mSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][3];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][3];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,3> BubbleTriangle_001ny___m__mSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,3> BubbleTriangle_001ny___m__mSurfaceItaArray;
BubbleTriangle_001ny___m__mSurfaceItaArray[0] = {BubbleTriangle_001ny___m__mSurfaceIta1, -1};
BubbleTriangle_001ny___m__mSurfaceItaArray[1] = {BubbleTriangle_001ny___m__mSurfaceIta2, -1};
BubbleTriangle_001ny___m__mSurfaceItaArray[2] = {BubbleTriangle_001ny___m__mSurfaceIta3, -1};
return BubbleTriangle_001ny___m__mSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,3> BubbleTriangle_001ny___m__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,3> BubbleTriangle_001ny___m__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,3> BubbleTriangle_001ny___m__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,3> BubbleTriangle_001ny___m__mSurfaceIta<F32>();

#endif

}}}}

