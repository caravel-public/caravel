#include "FunctionSpace/IBPhelper.h"

#include "BubbleTriangle_001yy___z_m_zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,16> BubbleTriangle_001yy___z_m_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,16> BubbleTriangle_001yy___z_m_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,16> BubbleTriangle_001yy___z_m_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,16> BubbleTriangle_001yy___z_m_zSurfaceIta<F32>();

#endif

}}}}

