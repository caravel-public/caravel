#include "FunctionSpace/IBPhelper.h"

#include "BubbleHexagon_004nn___mzzz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,10> BubbleHexagon_004nn___mzzz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,10> BubbleHexagon_004nn___mzzz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,10> BubbleHexagon_004nn___mzzz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,10> BubbleHexagon_004nn___mzzz__SurfaceIta<F32>();

#endif

}}}}

