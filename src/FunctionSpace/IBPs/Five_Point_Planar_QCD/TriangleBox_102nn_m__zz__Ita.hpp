/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> TriangleBox_102nn_m__zz__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][7];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102nn_m__zz__SurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102nn_m__zz__SurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102nn_m__zz__SurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][7];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,4> TriangleBox_102nn_m__zz__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,4> TriangleBox_102nn_m__zz__SurfaceItaArray;
TriangleBox_102nn_m__zz__SurfaceItaArray[0] = {TriangleBox_102nn_m__zz__SurfaceIta1, -1};
TriangleBox_102nn_m__zz__SurfaceItaArray[1] = {TriangleBox_102nn_m__zz__SurfaceIta2, -1};
TriangleBox_102nn_m__zz__SurfaceItaArray[2] = {TriangleBox_102nn_m__zz__SurfaceIta3, -1};
TriangleBox_102nn_m__zz__SurfaceItaArray[3] = {TriangleBox_102nn_m__zz__SurfaceIta4, -1};
return TriangleBox_102nn_m__zz__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,4> TriangleBox_102nn_m__zz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,4> TriangleBox_102nn_m__zz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,4> TriangleBox_102nn_m__zz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,4> TriangleBox_102nn_m__zz__SurfaceIta<F32>();

#endif

}}}}

