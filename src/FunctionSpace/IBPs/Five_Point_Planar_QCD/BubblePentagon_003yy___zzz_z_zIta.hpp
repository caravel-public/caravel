/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][9];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][3];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[0][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[0][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(4);
zd[0] = prod_pow(z[0], 3) * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[0][9];
zd[1] = -((prod_pow(z[0], 4) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][9];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][3];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][9];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[3][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][3];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][4];
z[5] = ibp_vectors[3][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[3][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][9];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[4][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][9];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[4][9];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[4][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][4];
z[5] = ibp_vectors[4][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][4];
z[5] = ibp_vectors[4][3];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][4];
z[5] = ibp_vectors[4][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][4];
z[5] = ibp_vectors[4][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][4];
z[5] = ibp_vectors[4][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][4];
z[5] = ibp_vectors[4][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[4][9];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[18][9];
z[1] = ibp_vectors[18].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[18][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[18][9];
z[2] = ibp_vectors[18].back();
z[3] = ibp_vectors[18][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[18][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[18][9];
z[3] = ibp_vectors[18].back();
z[4] = ibp_vectors[18][4];
z[5] = ibp_vectors[18][3];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[18][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003yy___zzz_z_zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[18][9];
z[3] = ibp_vectors[18].back();
z[4] = ibp_vectors[18][4];
z[5] = ibp_vectors[18][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[18][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,64> BubblePentagon_003yy___zzz_z_zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,64> BubblePentagon_003yy___zzz_z_zSurfaceItaArray;
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[0] = {BubblePentagon_003yy___zzz_z_zSurfaceIta1, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[1] = {BubblePentagon_003yy___zzz_z_zSurfaceIta2, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[2] = {BubblePentagon_003yy___zzz_z_zSurfaceIta3, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[3] = {BubblePentagon_003yy___zzz_z_zSurfaceIta4, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[4] = {BubblePentagon_003yy___zzz_z_zSurfaceIta5, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[5] = {BubblePentagon_003yy___zzz_z_zSurfaceIta6, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[6] = {BubblePentagon_003yy___zzz_z_zSurfaceIta7, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[7] = {BubblePentagon_003yy___zzz_z_zSurfaceIta8, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[8] = {BubblePentagon_003yy___zzz_z_zSurfaceIta9, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[9] = {BubblePentagon_003yy___zzz_z_zSurfaceIta10, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[10] = {BubblePentagon_003yy___zzz_z_zSurfaceIta11, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[11] = {BubblePentagon_003yy___zzz_z_zSurfaceIta12, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[12] = {BubblePentagon_003yy___zzz_z_zSurfaceIta13, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[13] = {BubblePentagon_003yy___zzz_z_zSurfaceIta14, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[14] = {BubblePentagon_003yy___zzz_z_zSurfaceIta15, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[15] = {BubblePentagon_003yy___zzz_z_zSurfaceIta16, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[16] = {BubblePentagon_003yy___zzz_z_zSurfaceIta17, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[17] = {BubblePentagon_003yy___zzz_z_zSurfaceIta18, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[18] = {BubblePentagon_003yy___zzz_z_zSurfaceIta19, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[19] = {BubblePentagon_003yy___zzz_z_zSurfaceIta20, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[20] = {BubblePentagon_003yy___zzz_z_zSurfaceIta21, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[21] = {BubblePentagon_003yy___zzz_z_zSurfaceIta22, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[22] = {BubblePentagon_003yy___zzz_z_zSurfaceIta23, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[23] = {BubblePentagon_003yy___zzz_z_zSurfaceIta24, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[24] = {BubblePentagon_003yy___zzz_z_zSurfaceIta25, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[25] = {BubblePentagon_003yy___zzz_z_zSurfaceIta26, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[26] = {BubblePentagon_003yy___zzz_z_zSurfaceIta27, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[27] = {BubblePentagon_003yy___zzz_z_zSurfaceIta28, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[28] = {BubblePentagon_003yy___zzz_z_zSurfaceIta29, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[29] = {BubblePentagon_003yy___zzz_z_zSurfaceIta30, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[30] = {BubblePentagon_003yy___zzz_z_zSurfaceIta31, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[31] = {BubblePentagon_003yy___zzz_z_zSurfaceIta32, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[32] = {BubblePentagon_003yy___zzz_z_zSurfaceIta33, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[33] = {BubblePentagon_003yy___zzz_z_zSurfaceIta34, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[34] = {BubblePentagon_003yy___zzz_z_zSurfaceIta35, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[35] = {BubblePentagon_003yy___zzz_z_zSurfaceIta36, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[36] = {BubblePentagon_003yy___zzz_z_zSurfaceIta37, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[37] = {BubblePentagon_003yy___zzz_z_zSurfaceIta38, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[38] = {BubblePentagon_003yy___zzz_z_zSurfaceIta39, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[39] = {BubblePentagon_003yy___zzz_z_zSurfaceIta40, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[40] = {BubblePentagon_003yy___zzz_z_zSurfaceIta41, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[41] = {BubblePentagon_003yy___zzz_z_zSurfaceIta42, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[42] = {BubblePentagon_003yy___zzz_z_zSurfaceIta43, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[43] = {BubblePentagon_003yy___zzz_z_zSurfaceIta44, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[44] = {BubblePentagon_003yy___zzz_z_zSurfaceIta45, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[45] = {BubblePentagon_003yy___zzz_z_zSurfaceIta46, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[46] = {BubblePentagon_003yy___zzz_z_zSurfaceIta47, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[47] = {BubblePentagon_003yy___zzz_z_zSurfaceIta48, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[48] = {BubblePentagon_003yy___zzz_z_zSurfaceIta49, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[49] = {BubblePentagon_003yy___zzz_z_zSurfaceIta50, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[50] = {BubblePentagon_003yy___zzz_z_zSurfaceIta51, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[51] = {BubblePentagon_003yy___zzz_z_zSurfaceIta52, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[52] = {BubblePentagon_003yy___zzz_z_zSurfaceIta53, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[53] = {BubblePentagon_003yy___zzz_z_zSurfaceIta54, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[54] = {BubblePentagon_003yy___zzz_z_zSurfaceIta55, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[55] = {BubblePentagon_003yy___zzz_z_zSurfaceIta56, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[56] = {BubblePentagon_003yy___zzz_z_zSurfaceIta57, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[57] = {BubblePentagon_003yy___zzz_z_zSurfaceIta58, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[58] = {BubblePentagon_003yy___zzz_z_zSurfaceIta59, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[59] = {BubblePentagon_003yy___zzz_z_zSurfaceIta60, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[60] = {BubblePentagon_003yy___zzz_z_zSurfaceIta61, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[61] = {BubblePentagon_003yy___zzz_z_zSurfaceIta62, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[62] = {BubblePentagon_003yy___zzz_z_zSurfaceIta63, -1};
BubblePentagon_003yy___zzz_z_zSurfaceItaArray[63] = {BubblePentagon_003yy___zzz_z_zSurfaceIta64, -1};
return BubblePentagon_003yy___zzz_z_zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,64> BubblePentagon_003yy___zzz_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,64> BubblePentagon_003yy___zzz_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,64> BubblePentagon_003yy___zzz_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,64> BubblePentagon_003yy___zzz_z_zSurfaceIta<F32>();

#endif

}}}}

