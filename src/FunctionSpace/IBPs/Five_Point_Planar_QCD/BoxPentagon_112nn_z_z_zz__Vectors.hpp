/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#ifndef BoxPentagon_112nn_z_z_zz__Vectors_H_INC

#define BoxPentagon_112nn_z_z_zz__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BoxPentagon_112nn_z_z_zz__vec2_component1(const T* invariants, const T* D)
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = D[8];
z[10] = z[0] * T(2);
z[11] = z[2] * z[10];
z[12] = z[8] + -z[5];
z[13] = z[7] + z[8] + -z[9];
z[14] = z[6] + T(2) + -z[13];
z[14] = z[0] * z[14];
z[14] = z[11] + z[12] + z[14];
z[14] = z[2] * z[14];
z[15] = z[6] + -z[5];
z[11] = z[11] + z[15];
z[11] = z[1] * z[11];
z[11] = z[11] + z[14];
z[13] = z[4] + z[6] * T(2) + -z[5] + -z[13];
z[13] = z[10] * z[13];
z[10] = z[15] + T(2) + z[2] * T(2) + -z[10];
z[10] = z[3] * z[10];
z[14] = -(z[4] * z[15]);
return z[10] + z[13] + z[14] + z[12] * T(2) + z[11] * T(4);
}

template<typename T> T BoxPentagon_112nn_z_z_zz__vec2_component2(const T* invariants, const T* D)
{
T z[17];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[5];
z[4] = D[6];
z[5] = D[2];
z[6] = D[3];
z[7] = D[4];
z[8] = D[7];
z[9] = D[8];
z[10] = z[6] + -z[5];
z[11] = z[0] * T(2) + -z[10];
z[12] = z[2] * T(2);
z[13] = z[11] * z[12];
z[14] = z[7] + -z[8];
z[15] = z[4] + z[9] + z[10] + z[14] + z[3] * T(-2) + -z[13];
z[15] = z[1] * z[15];
z[15] = z[14] + z[15] + -z[5];
z[16] = z[4] + -z[12];
z[11] = z[11] * z[16];
z[16] = z[0] * T(4);
z[11] = z[6] + z[11] + z[14] + z[5] * T(-3) + -z[16];
z[11] = z[11] * z[12];
z[10] = z[16] + -z[10];
z[12] = -z[10] + -z[13];
z[12] = z[3] * z[12];
z[10] = z[4] * z[10];
return z[10] + z[11] + z[12] + z[15] * T(2);
}

template<typename T> T BoxPentagon_112nn_z_z_zz__vec2_component10(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = D[8];
z[10] = z[0] + T(-1) + -z[1] + -z[2];
z[11] = z[3] + z[8] + -z[4] + -z[9];
z[10] = z[7] + z[11] + z[10] * T(2) + -z[6];
z[10] = z[1] * z[10];
z[10] = z[5] + z[10] + z[11] + -z[7];
return z[10] * T(4);
}

template<typename T> T BoxPentagon_112nn_z_z_zz__vec2_divergence(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = D[8];
z[10] = z[0] + -z[2];
z[11] = z[3] + z[8] + -z[4] + -z[9];
z[12] = z[1] * T(2);
z[10] = z[6] + z[12] + z[10] * T(-2) + T(2) + -z[7] + -z[11];
z[10] = z[10] * z[12];
z[10] = z[7] + z[10] + z[11] * T(-2) + -z[5];
return z[10] * T(2);
}

template<typename T> T BoxPentagon_112nn_z_z_zz__vec4_component1(const T* invariants, const T* D)
{
T z[32];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = D[8];
z[8] = D[2];
z[9] = D[3];
z[10] = z[0] * T(3);
z[11] = z[8] + T(1);
z[12] = z[10] + -z[11];
z[12] = z[6] * z[12];
z[13] = z[4] * z[9];
z[14] = z[0] * T(4);
z[15] = z[13] + -z[14];
z[15] = z[2] * z[15];
z[16] = z[14] + T(4);
z[17] = z[9] + -z[16];
z[17] = z[0] * z[17];
z[18] = -(z[7] * z[8]);
z[12] = z[12] + z[15] + z[17] + z[18];
z[15] = z[0] * T(2);
z[17] = z[15] + -z[8];
z[18] = z[9] * T(3);
z[19] = z[17] + z[18];
z[19] = z[4] * z[19];
z[20] = z[15] + T(-4);
z[21] = z[9] + -z[8];
z[22] = z[20] + -z[21];
z[23] = z[6] * z[22];
z[24] = z[15] + z[21];
z[25] = z[7] * z[24];
z[26] = T(1) + -z[8] + -z[15];
z[26] = z[0] * z[26];
z[26] = z[9] + z[26];
z[23] = z[19] + z[23] + z[25] + z[26] * T(4);
z[23] = z[4] * z[23];
z[22] = -(z[4] * z[22]);
z[25] = z[8] + z[0] * T(-8);
z[22] = z[22] + z[25] * T(2);
z[25] = z[1] * T(2);
z[22] = z[22] * z[25];
z[26] = z[5] * z[14];
z[12] = z[22] + z[23] + z[26] + z[12] * T(4);
z[12] = z[2] * z[12];
z[22] = z[21] + T(2) + -z[14];
z[22] = z[0] * z[22];
z[23] = z[15] + T(-3) + -z[9];
z[23] = z[6] * z[23];
z[26] = -(z[7] * z[9]);
z[22] = z[21] + z[22] + z[23] + z[26];
z[19] = z[19] + z[22] * T(2);
z[19] = z[4] * z[19];
z[22] = z[0] * z[7];
z[23] = z[0] + T(-1);
z[26] = z[6] * z[23];
z[27] = z[22] + z[26];
z[23] = z[9] + -z[23];
z[23] = z[15] * z[23];
z[28] = z[0] * z[5];
z[13] = z[23] + z[28] + -z[13] + -z[27];
z[23] = z[5] * T(2);
z[13] = z[13] * z[23];
z[28] = z[8] + z[9];
z[29] = -z[16] + -z[28];
z[29] = z[0] * z[29];
z[30] = T(-3) + z[0] * T(5);
z[30] = z[6] * z[30];
z[22] = z[8] + z[22] + z[29] + z[30];
z[29] = z[8] + T(-2);
z[30] = z[15] + z[29];
z[18] = z[4] + z[7] + z[18] + -z[6] + -z[30];
z[18] = z[4] * z[18];
z[31] = z[4] + -z[14];
z[31] = z[25] * z[31];
z[18] = z[18] + z[31] + z[22] * T(2);
z[18] = z[18] * z[25];
z[22] = z[11] + z[15];
z[22] = z[0] * z[22];
z[11] = z[22] + -z[11] + -z[26];
z[11] = z[6] * z[11];
z[22] = z[0] * z[30];
z[22] = z[22] + -z[8];
z[22] = z[7] * z[22];
z[14] = -z[9] + -z[14];
z[14] = z[0] * z[14];
z[11] = z[11] + z[14] + z[22];
z[11] = z[12] + z[13] + z[18] + z[19] + z[11] * T(4);
z[11] = z[2] * z[11];
z[12] = z[24] + T(2);
z[12] = z[0] * z[12];
z[13] = z[24] + T(-2);
z[13] = z[2] * z[13];
z[13] = z[13] + -z[8];
z[13] = z[3] * z[13];
z[14] = -(z[6] * z[15]);
z[18] = z[0] + z[8];
z[18] = z[4] * z[18];
z[12] = z[8] + z[12] + z[13] + z[14] + z[18];
z[13] = z[0] * T(6);
z[14] = z[13] + -z[21];
z[14] = z[0] * z[14];
z[18] = z[7] * z[30];
z[17] = -(z[6] * z[17]);
z[19] = z[8] + z[9] * T(-2) + T(3) + -z[0];
z[19] = z[4] * z[19];
z[14] = z[8] + z[14] + z[17] + z[18] + z[19] + T(2);
z[16] = z[4] + z[16] + -z[28];
z[16] = z[16] * z[25];
z[17] = -z[20] + -z[28];
z[17] = z[4] * z[17];
z[18] = z[0] + T(1) + -z[21];
z[17] = z[17] + z[18] * T(4);
z[17] = z[2] * z[17];
z[18] = T(-2) + z[8] * T(3) + -z[9] + -z[13];
z[18] = z[5] * z[18];
z[14] = z[16] + z[17] + z[18] + z[14] * T(2);
z[14] = z[2] * z[14];
z[13] = z[9] + z[29] + -z[13];
z[13] = z[5] * z[13];
z[10] = z[10] + T(1) + -z[6] + -z[7];
z[10] = z[4] + z[10] * T(2) + z[1] * T(4) + -z[23];
z[10] = z[10] * z[25];
z[10] = z[10] + z[13] + z[14] + z[12] * T(2);
z[10] = z[3] * z[10];
z[12] = prod_pow(z[0], 2);
z[12] = z[12] * T(-2) + -z[27];
z[13] = -z[15] + -z[28];
z[13] = z[4] * z[13];
z[14] = -z[4] + -z[15];
z[14] = z[14] * z[25];
z[15] = z[5] * z[15];
z[12] = z[13] + z[14] + z[15] + z[12] * T(2);
z[12] = z[5] * z[12];
return z[10] + z[11] + z[12];
}

template<typename T> T BoxPentagon_112nn_z_z_zz__vec4_component2(const T* invariants, const T* D)
{
T z[33];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = D[8];
z[8] = D[2];
z[9] = D[3];
z[10] = z[9] + -z[8];
z[11] = z[0] * T(2);
z[12] = z[10] + -z[11];
z[13] = -(z[7] * z[12]);
z[14] = z[12] + T(10);
z[14] = z[6] * z[14];
z[15] = z[1] * T(2);
z[16] = z[1] * T(4);
z[17] = z[9] + T(-8) + z[0] * T(-6) + z[8] * T(-3) + z[6] * T(2) + -z[16];
z[17] = z[15] * z[17];
z[12] = z[12] + T(-10) + -z[16];
z[12] = z[3] * z[12];
z[18] = z[8] + z[9];
z[19] = -z[11] + -z[18];
z[19] = z[4] * z[19];
z[20] = -z[9] + -z[11];
z[21] = z[9] * T(2);
z[22] = z[5] * z[21];
z[12] = z[12] + z[13] + z[14] + z[17] + z[19] + z[22] + z[20] * T(6);
z[12] = z[4] * z[12];
z[13] = z[0] * T(4);
z[14] = z[13] + T(4);
z[17] = z[1] * T(8);
z[19] = z[21] + -z[8];
z[20] = z[6] * T(6) + -z[14] + -z[17] + -z[19];
z[20] = z[1] * z[20];
z[22] = z[8] * T(2) + -z[9];
z[23] = z[8] + z[11];
z[24] = z[7] * z[23];
z[25] = z[23] + -z[6];
z[26] = z[25] + T(2);
z[26] = z[6] * z[26];
z[20] = z[20] + z[22] + z[24] + z[26];
z[26] = z[10] + z[11];
z[27] = z[26] + T(-4);
z[28] = z[7] * T(2);
z[29] = z[3] * T(2);
z[30] = z[5] + z[27] + z[28] + z[29] + -z[15];
z[30] = z[3] * z[30];
z[31] = z[15] + -z[6];
z[32] = z[19] + z[31];
z[32] = z[5] * z[32];
z[20] = z[30] + z[32] + z[20] * T(2);
z[30] = -z[3] + -z[23] + -z[31];
z[30] = z[4] * z[30];
z[22] = z[6] + z[22] + z[30] + -z[3] + -z[16];
z[22] = z[2] * z[22];
z[12] = z[12] + z[20] * T(2) + z[22] * T(4);
z[12] = z[2] * z[12];
z[14] = z[7] + z[6] * T(5) + -z[14] + -z[16] + -z[18];
z[14] = z[1] * z[14];
z[16] = z[25] + T(6);
z[16] = z[6] * z[16];
z[14] = z[14] + z[16] + z[24] + -z[13] + -z[18];
z[14] = z[1] * z[14];
z[16] = z[8] + T(1);
z[20] = z[11] + z[16];
z[22] = z[20] + -z[6];
z[22] = z[6] * z[22];
z[14] = z[14] + z[22] + z[24];
z[22] = z[27] + z[7] * T(4);
z[22] = z[1] * z[22];
z[24] = z[1] + T(1);
z[24] = z[24] * z[29];
z[25] = z[0] + T(1) + -z[10];
z[25] = z[25] * T(2) + -z[1];
z[25] = z[5] * z[25];
z[16] = z[9] + z[22] + z[24] + z[25] + z[28] + z[16] * T(-2);
z[16] = z[16] * z[29];
z[22] = z[0] + z[1];
z[24] = z[6] + z[22] * T(-3) + T(-1) + -z[9];
z[24] = z[15] * z[24];
z[25] = z[0] + z[9];
z[25] = z[7] * z[25];
z[27] = z[9] + T(3) + -z[0];
z[27] = z[6] * z[27];
z[24] = z[24] + z[25] + z[27] + -z[26];
z[25] = z[9] * T(3);
z[26] = T(-2) + -z[8];
z[13] = z[25] + z[26] * T(3) + -z[13] + -z[17];
z[13] = z[3] * z[13];
z[17] = z[15] + z[23] + z[25];
z[17] = z[5] * z[17];
z[26] = z[11] + z[15];
z[25] = z[8] + -z[25] + -z[26];
z[25] = z[4] * z[25];
z[13] = z[13] + z[17] + z[25] + z[24] * T(2);
z[13] = z[4] * z[13];
z[17] = z[23] + -z[21];
z[17] = z[7] * z[17];
z[24] = z[23] + T(-2);
z[21] = z[24] + -z[21];
z[21] = z[6] * z[21];
z[25] = z[6] + z[7];
z[24] = z[9] * T(5) + -z[24] + -z[25];
z[24] = z[1] * z[24];
z[17] = z[17] + z[19] + z[21] + z[24];
z[15] = z[18] + -z[15];
z[15] = z[5] * z[15];
z[17] = z[17] * T(2) + -z[15];
z[17] = z[5] * z[17];
z[12] = z[12] + z[13] + z[16] + z[17] + z[14] * T(4);
z[12] = z[2] * z[12];
z[13] = z[25] + -z[26];
z[13] = z[1] * z[13];
z[14] = z[7] * z[11];
z[11] = z[11] + T(-1);
z[11] = z[6] * z[11];
z[11] = z[11] + z[13] + z[14];
z[11] = z[11] * T(2) + -z[15];
z[11] = z[5] * z[11];
z[10] = z[10] + z[28] + T(-2) + -z[26];
z[10] = z[1] * z[10];
z[13] = z[1] + z[20];
z[13] = z[5] * z[13];
z[10] = z[10] + z[13] + -z[23];
z[10] = z[10] * z[29];
z[13] = z[18] + z[26];
z[13] = z[5] * z[13];
z[14] = -z[8] + -z[22];
z[14] = z[14] * z[29];
z[13] = z[13] + z[14];
z[13] = z[4] * z[13];
return z[10] + z[11] + z[12] + z[13];
}

template<typename T> T BoxPentagon_112nn_z_z_zz__vec4_component10(const T* invariants, const T* D)
{
T z[26];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = D[8];
z[8] = D[2];
z[9] = D[3];
z[10] = z[4] * T(3);
z[11] = z[6] * T(4);
z[12] = z[3] * T(3);
z[13] = z[5] + T(-5);
z[13] = z[11] + z[12] + z[1] * T(-18) + z[13] * T(2) + -z[10];
z[13] = z[6] * z[13];
z[14] = z[2] * T(2);
z[15] = z[4] + T(2);
z[15] = z[14] * z[15];
z[16] = z[1] * T(6);
z[17] = z[16] + T(2) + -z[5] + -z[7];
z[18] = z[3] + -z[5];
z[19] = z[7] + z[18] + T(2) + z[1] * T(4);
z[19] = z[4] * z[19];
z[20] = T(-5) + z[4] * T(-2);
z[20] = z[6] * z[20];
z[15] = z[15] + z[19] + z[20] + z[17] * T(2);
z[14] = z[14] * z[15];
z[15] = z[1] * T(8);
z[17] = T(3) + -z[3] + -z[5];
z[17] = z[15] + z[17] * T(2) + -z[7];
z[19] = z[1] * T(2);
z[17] = z[17] * z[19];
z[20] = z[5] * T(2);
z[21] = z[20] + -z[12];
z[22] = z[21] * T(-2) + z[1] * T(10) + -z[7];
z[22] = z[4] * z[22];
z[23] = -(z[3] * z[18]);
z[23] = z[23] + -z[5];
z[24] = z[12] + z[7] * T(2);
z[25] = -(z[7] * z[24]);
z[13] = z[13] + z[14] + z[17] + z[22] + z[25] + z[23] * T(4);
z[13] = z[2] * z[13];
z[11] = z[11] + z[7] * T(4);
z[10] = z[10] + T(4);
z[14] = z[4] * T(5);
z[17] = z[14] + T(2);
z[17] = z[2] * z[17];
z[12] = z[10] + z[16] + z[17] + -z[11] + -z[12];
z[12] = z[2] * z[12];
z[12] = z[5] + z[12];
z[12] = z[8] * z[12];
z[16] = z[6] + T(1) + -z[5];
z[10] = -(z[2] * z[10]);
z[10] = z[10] + z[24] + z[16] * T(2) + -z[14] + -z[19];
z[10] = z[2] * z[10];
z[10] = z[5] + z[10] + z[3] * T(-2);
z[10] = z[9] * z[10];
z[16] = z[2] + T(1);
z[14] = z[14] + T(6);
z[14] = z[14] * z[16];
z[11] = z[5] + z[14] + z[15] + z[3] * T(-5) + -z[11];
z[11] = z[2] * z[11];
z[11] = z[11] + -z[18];
z[11] = z[0] * z[11];
z[14] = -(z[18] * z[19]);
z[15] = -(z[6] * z[21]);
z[16] = z[3] + -z[20];
z[16] = z[7] * z[16];
z[10] = z[10] + z[12] + z[13] + z[14] + z[15] + z[16] + z[11] * T(2);
return z[10] * T(4);
}

template<typename T> T BoxPentagon_112nn_z_z_zz__vec4_divergence(const T* invariants, const T* D)
{
T z[20];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = D[8];
z[8] = D[2];
z[9] = D[3];
z[10] = prod_pow(z[7], 2);
z[11] = z[7] * T(5);
z[12] = z[11] + T(-6);
z[12] = z[0] * z[12];
z[13] = T(-1) + -z[7];
z[13] = z[9] * z[13];
z[10] = z[10] + z[12] + z[13];
z[12] = z[8] + -z[9];
z[13] = z[7] + z[12];
z[14] = z[6] * T(3);
z[15] = z[3] * T(4);
z[13] = z[15] + T(-1) + z[13] * T(3) + z[1] * T(4) + z[0] * T(10) + -z[14];
z[13] = z[3] * z[13];
z[16] = z[0] + z[1];
z[17] = z[11] + z[16] * T(-16) + T(-12) + -z[9];
z[17] = z[1] * z[17];
z[11] = z[11] + z[1] * T(-5) + T(-3);
z[11] = z[8] * z[11];
z[18] = z[6] * T(4);
z[19] = z[8] * T(4) + z[0] * T(8) + T(11) + z[1] * T(19) + -z[9] + -z[18];
z[19] = z[6] * z[19];
z[10] = z[11] + z[13] + z[17] + z[19] + z[10] * T(2);
z[11] = z[3] + -z[1];
z[13] = z[9] * T(7);
z[17] = T(2) + -z[0];
z[11] = z[13] + z[11] * T(-8) + z[17] * T(4) + z[4] * T(9) + -z[8] + -z[18];
z[11] = z[5] * z[11];
z[17] = z[5] + z[2] * T(-2);
z[18] = z[4] * T(5) + T(8);
z[17] = z[17] * z[18];
z[18] = z[7] + z[0] * T(-3) + T(-2);
z[12] = z[1] * T(-24) + z[18] * T(4) + z[6] * T(11) + -z[3] + -z[12];
z[18] = z[0] * T(-4) + T(-2) + -z[7];
z[15] = z[1] * T(-18) + z[8] * T(-10) + z[18] * T(5) + z[9] * T(6) + z[6] * T(9) + -z[4] + -z[15];
z[15] = z[4] * z[15];
z[12] = z[15] + z[17] + z[12] * T(2);
z[12] = z[2] * z[12];
z[13] = z[13] + z[16] * T(-22) + z[3] * T(-13) + z[8] * T(-5) + z[6] * T(8);
z[13] = z[4] * z[13];
z[10] = z[11] + z[12] + z[13] + z[10] * T(2);
z[10] = z[2] * z[10];
z[11] = z[0] + z[9];
z[12] = z[1] * T(2);
z[11] = z[12] + z[11] * T(2) + -z[7] + -z[14];
z[11] = z[3] * z[11];
z[13] = z[6] + z[7] + -z[0];
z[12] = z[13] * T(2) + -z[8] + -z[9] + -z[12];
z[12] = z[5] * z[12];
z[11] = z[11] + z[12];
z[10] = z[10] + z[11] * T(2);
return z[10] * T(2);
}

template<typename T> std::vector<std::vector<T>> BoxPentagon_112nn_z_z_zz___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(4, std::vector<T>(11, T(0)));
ibp_vectors[1][0] = BoxPentagon_112nn_z_z_zz__vec2_component1(invariants, propagators);
ibp_vectors[1][1] = BoxPentagon_112nn_z_z_zz__vec2_component2(invariants, propagators);
ibp_vectors[1][9] = BoxPentagon_112nn_z_z_zz__vec2_component10(invariants, propagators);
ibp_vectors[1].back() = BoxPentagon_112nn_z_z_zz__vec2_divergence(invariants, propagators);
ibp_vectors[3][0] = BoxPentagon_112nn_z_z_zz__vec4_component1(invariants, propagators);
ibp_vectors[3][1] = BoxPentagon_112nn_z_z_zz__vec4_component2(invariants, propagators);
ibp_vectors[3][9] = BoxPentagon_112nn_z_z_zz__vec4_component10(invariants, propagators);
ibp_vectors[3].back() = BoxPentagon_112nn_z_z_zz__vec4_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BoxPentagon_112nn_z_z_zz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BoxPentagon_112nn_z_z_zz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BoxPentagon_112nn_z_z_zz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BoxPentagon_112nn_z_z_zz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

