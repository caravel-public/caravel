#ifndef TriangleBox_102nn_z__mm__Vectors_H_INC

#define TriangleBox_102nn_z__mm__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T TriangleBox_102nn_z__mm__vec1_component1(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[3];
z[2] = D[5];
z[3] = D[1];
z[4] = D[4];
z[5] = invariants[1];
z[6] = D[2];
z[7] = z[2] + -z[1];
z[7] = z[0] * z[7];
z[7] = z[1] + z[7];
z[8] = z[4] + z[5] + -z[1];
z[9] = T(2) + -z[8];
z[9] = z[3] * z[9];
z[8] = z[6] * z[8];
return z[8] + z[9] + z[7] * T(2);
}

template<typename T> T TriangleBox_102nn_z__mm__vec1_component7(const T* invariants, const T* D)
{
T z[3];
z[0] = D[3];
z[1] = D[5];
z[2] = z[1] + -z[0];
z[2] = z[2] * T(-3) + T(4);
return z[2] * T(4);
}

template<typename T> T TriangleBox_102nn_z__mm__vec1_divergence(const T* invariants, const T* D)
{
T z[3];
z[0] = D[3];
z[1] = D[5];
z[2] = z[1] + -z[0];
return z[2] * T(6);
}

template<typename T> std::vector<std::vector<T>> TriangleBox_102nn_z__mm___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(1, std::vector<T>(8, T(0)));
ibp_vectors[0][0] = TriangleBox_102nn_z__mm__vec1_component1(invariants, propagators);
ibp_vectors[0][6] = TriangleBox_102nn_z__mm__vec1_component7(invariants, propagators);
ibp_vectors[0].back() = TriangleBox_102nn_z__mm__vec1_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleBox_102nn_z__mm___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleBox_102nn_z__mm___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleBox_102nn_z__mm___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleBox_102nn_z__mm___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

