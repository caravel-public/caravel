#include "FunctionSpace/IBPhelper.h"

#include "TriangleBox_102ny_z__mz__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,39> TriangleBox_102ny_z__mz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,39> TriangleBox_102ny_z__mz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,39> TriangleBox_102ny_z__mz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,39> TriangleBox_102ny_z__mz__zSurfaceIta<F32>();

#endif

}}}}

