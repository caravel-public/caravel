#pragma once

#include "BoxBox_202nn_zz__mz__Ita.hpp"

#include "BoxBox_202nn_zz__zz__Ita.hpp"

#include "BoxBox_202ny_zz__zz__zIta.hpp"

#include "BoxPentagon_203nn_zz__zzz__Ita.hpp"

#include "BubbleBox_002nn___mm__Ita.hpp"

#include "BubbleBox_002ny___mm__zIta.hpp"

#include "BubbleBox_002ny___mz__mIta.hpp"

#include "BubbleBox_002ny___mz__zIta.hpp"

#include "BubbleBox_002ny___zm__mIta.hpp"

#include "BubbleBox_002ny___zm__zIta.hpp"

#include "BubbleBox_002ny___zz__mIta.hpp"

#include "BubbleBox_002yy___mz_z_zIta.hpp"

#include "BubbleBox_002yy___zz_m_zIta.hpp"

#include "BubbleBox_002yy___zz_z_zIta.hpp"

#include "BubbleBubble_000yy____m_mIta.hpp"

#include "BubbleHeptagon_005nn___zzzzz__Ita.hpp"

#include "BubbleHexagon_004nn___mzzz__Ita.hpp"

#include "BubbleHexagon_004nn___zmzz__Ita.hpp"

#include "BubbleHexagon_004nn___zzzz__Ita.hpp"

#include "BubbleHexagon_004ny___zzzz__zIta.hpp"

#include "BubblePentagon_003nn___mmz__Ita.hpp"

#include "BubblePentagon_003nn___mzm__Ita.hpp"

#include "BubblePentagon_003nn___mzz__Ita.hpp"

#include "BubblePentagon_003nn___zmz__Ita.hpp"

#include "BubblePentagon_003ny___mzz__zIta.hpp"

#include "BubblePentagon_003ny___zmz__zIta.hpp"

#include "BubblePentagon_003ny___zzm__zIta.hpp"

#include "BubblePentagon_003ny___zzz__mIta.hpp"

#include "BubblePentagon_003ny___zzz__zIta.hpp"

#include "BubblePentagon_003yy___zzz_z_zIta.hpp"

#include "BubbleTriangle_001ny___m__mIta.hpp"

#include "BubbleTriangle_001yy___m_m_zIta.hpp"

#include "BubbleTriangle_001yy___m_z_zIta.hpp"

#include "BubbleTriangle_001yy___z_m_mIta.hpp"

#include "BubbleTriangle_001yy___z_m_zIta.hpp"

#include "TriangleBox_102nn_m__mz__Ita.hpp"

#include "TriangleBox_102nn_m__zz__Ita.hpp"

#include "TriangleBox_102nn_z__mm__Ita.hpp"

#include "TriangleBox_102nn_z__mz__Ita.hpp"

#include "TriangleBox_102ny_m__zz__zIta.hpp"

#include "TriangleBox_102ny_z__mz__zIta.hpp"

#include "TriangleBox_102ny_z__zm__zIta.hpp"

#include "TriangleBox_102ny_z__zz__mIta.hpp"

#include "TriangleBox_102ny_z__zz__zIta.hpp"

#include "TriangleBox_102yy_z__zz_z_zIta.hpp"

#include "TriangleHexagon_104nn_z__zzzz__Ita.hpp"

#include "TrianglePentagon_103nn_m__zzz__Ita.hpp"

#include "TrianglePentagon_103nn_z__mzz__Ita.hpp"

#include "TrianglePentagon_103nn_z__zmz__Ita.hpp"

#include "TrianglePentagon_103nn_z__zzz__Ita.hpp"

#include "TrianglePentagon_103ny_z__zzz__zIta.hpp"

#include "TriangleTriangle_101nn_m__m__Ita.hpp"

#include "TriangleTriangle_101ny_m__m__zIta.hpp"

#include "TriangleTriangle_101ny_z__m__mIta.hpp"

#include "TriangleTriangle_101ny_z__m__zIta.hpp"

#include "TriangleTriangle_101ny_z__z__mIta.hpp"

#include "TriangleTriangle_101yy_z__m_z_zIta.hpp"

#include "TriangleTriangle_101yy_z__z_m_zIta.hpp"

#include "TriangleTriangle_101yy_z__z_z_zIta.hpp"

/* Non-planar */

#include "BoxBox_111nn_z_z_m__Ita.hpp"

#include "BoxBox_111ny_z_z_z__zIta.hpp"

#include "BoxPentagon_112nn_z_z_zz__Ita.hpp"
