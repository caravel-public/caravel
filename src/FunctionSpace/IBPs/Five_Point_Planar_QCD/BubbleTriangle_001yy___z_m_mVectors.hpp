#ifndef BubbleTriangle_001yy___z_m_mVectors_H_INC

#define BubbleTriangle_001yy___z_m_mVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BubbleTriangle_001yy___z_m_mvec1_component1(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = invariants[1];
z[4] = D[4];
z[5] = D[5];
z[6] = z[2] * T(2);
z[7] = z[3] * T(2);
z[8] = z[1] * T(2);
z[9] = z[6] + z[7] + T(3) + -z[4] + -z[8];
z[9] = z[3] * z[9];
z[8] = z[8] + T(-1);
z[7] = z[2] + z[7] + -z[8];
z[7] = z[0] * z[7];
z[10] = T(-1) + -z[1];
z[10] = z[4] * z[10];
z[11] = z[1] * z[5];
return z[6] + z[9] + z[10] + z[11] + z[7] * T(2) + -z[8];
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec1_component2(const T* invariants, const T* D)
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = D[4];
z[4] = z[1] * T(2) + -z[3];
z[5] = z[0] * T(4);
z[6] = z[2] * T(2);
z[7] = z[5] + T(-2) + -z[4] + -z[6];
z[6] = z[6] * z[7];
z[7] = z[1] + T(1) + -z[0];
z[5] = z[5] * z[7];
return z[5] + z[6] + T(-1) + -z[4];
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec1_component3(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec1_component6(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = z[0] + -z[2];
z[3] = T(-1) + z[3] * T(2) + -z[1];
return z[3] * T(4);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = z[0] + -z[2];
z[3] = z[1] + z[3] * T(-2) + T(1);
return z[3] * T(6);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec2_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec2_component2(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec2_component3(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[1];
z[3] = D[4];
z[4] = D[5];
z[5] = invariants[1];
z[6] = z[4] + T(-1);
z[7] = z[6] + -z[0] + -z[3] + -z[5];
z[7] = z[1] * z[7];
z[7] = z[7] + -z[3];
z[8] = z[3] + T(2) + -z[4];
z[8] = z[2] * z[8];
z[9] = z[5] + -z[2] + -z[4];
z[9] = T(-3) + z[3] * T(-3) + z[9] * T(-2);
z[9] = z[5] * z[9];
return z[6] + z[8] + z[9] + z[7] * T(2);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec2_component6(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = z[0] + z[1] + z[3] + T(1) + -z[2];
return z[4] * T(4);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec2_divergence(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = z[1] + -z[2];
z[4] = z[4] * T(-2) + T(-1) + -z[0] + -z[3];
return z[4] * T(2);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec3_component1(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = invariants[1];
z[2] = T(-1) + -z[0] + -z[1];
return z[0] * z[2] * T(4);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec3_component2(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = z[1] + -z[2];
z[3] = z[3] * T(-2) + T(1);
return z[0] * z[3] * T(2);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec3_component3(const T* invariants, const T* D)
{
T z[6];
z[0] = D[2];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = z[0] * T(2);
z[5] = z[4] + T(3) + z[3] * T(4);
z[5] = z[1] * z[5];
z[4] = z[3] * T(-2) + T(-1) + -z[4];
z[4] = z[2] * z[4];
return z[4] + z[5];
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec3_component6(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = z[2] + z[0] * T(2) + -z[1];
return z[3] * T(4);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec3_divergence(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = z[1] + z[0] * T(-3) + T(-1) + -z[2] + -z[3];
return z[4] * T(4);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec5_component1(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[4];
z[4] = invariants[1];
z[5] = z[4] * T(2);
z[6] = z[1] * T(2);
z[7] = z[5] + T(3) + -z[6];
z[7] = z[4] * z[7];
z[6] = z[6] + T(-1);
z[5] = z[5] + -z[6];
z[5] = z[2] * z[5];
z[8] = z[1] + -z[4];
z[8] = z[3] * z[8];
z[5] = z[5] + z[7] + z[8] + -z[6];
return z[0] * z[5] * T(2);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec5_component2(const T* invariants, const T* D)
{
T z[7];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = invariants[1];
z[4] = z[3] * T(2);
z[5] = z[4] + -z[2];
z[6] = z[5] + T(3);
z[6] = z[3] * z[6];
z[4] = z[4] + T(1);
z[4] = z[1] * z[4];
z[5] = T(-2) + z[1] * T(-2) + -z[5];
z[5] = z[0] * z[5];
z[4] = z[4] + z[5] + z[6] + T(1);
return z[0] * z[4] * T(2);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec5_component3(const T* invariants, const T* D)
{
T z[7];
z[0] = D[2];
z[1] = D[4];
z[2] = invariants[1];
z[3] = z[2] * T(2);
z[4] = -z[0] + -z[1];
z[4] = z[3] * z[4];
z[5] = z[1] + T(-2);
z[5] = z[1] * z[5];
z[6] = z[0] * T(-4) + T(-3) + -z[1];
z[6] = z[0] * z[6];
z[4] = z[4] + z[5] + z[6];
z[3] = z[3] * z[4];
z[4] = z[0] * T(2);
z[5] = z[1] + T(-3) + -z[4];
z[5] = z[0] * z[5];
z[5] = z[5] + T(-1);
z[4] = z[4] * z[5];
z[5] = z[1] + T(-1);
z[5] = z[1] * z[5];
return z[3] + z[4] + z[5];
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec5_component6(const T* invariants, const T* D)
{
T z[6];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = invariants[1];
z[4] = -z[0] + -z[1];
z[5] = z[1] + z[3];
z[5] = z[2] + T(-2) + z[5] * T(-2);
z[4] = z[4] * z[5];
z[4] = z[4] + -z[2];
return z[4] * T(4);
}

template<typename T> T BubbleTriangle_001yy___z_m_mvec5_divergence(const T* invariants, const T* D)
{
T z[7];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = invariants[1];
z[4] = z[2] + T(-2);
z[4] = z[0] * z[4];
z[5] = z[0] * T(6);
z[6] = z[3] * T(2) + T(3) + -z[2] + -z[5];
z[6] = z[3] * z[6];
z[5] = z[1] * T(-6) + T(-4) + z[3] * T(-4) + z[2] * T(3) + -z[5];
z[5] = z[1] * z[5];
z[4] = z[5] + z[6] + T(1) + z[2] * T(2) + z[4] * T(3);
return z[4] * T(2);
}

template<typename T> std::vector<std::vector<T>> BubbleTriangle_001yy___z_m_m_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(5, std::vector<T>(7, T(0)));
ibp_vectors[0][0] = BubbleTriangle_001yy___z_m_mvec1_component1(invariants, propagators);
ibp_vectors[0][1] = BubbleTriangle_001yy___z_m_mvec1_component2(invariants, propagators);
ibp_vectors[0][5] = BubbleTriangle_001yy___z_m_mvec1_component6(invariants, propagators);
ibp_vectors[0].back() = BubbleTriangle_001yy___z_m_mvec1_divergence(invariants, propagators);
ibp_vectors[1][2] = BubbleTriangle_001yy___z_m_mvec2_component3(invariants, propagators);
ibp_vectors[1][5] = BubbleTriangle_001yy___z_m_mvec2_component6(invariants, propagators);
ibp_vectors[1].back() = BubbleTriangle_001yy___z_m_mvec2_divergence(invariants, propagators);
ibp_vectors[2][0] = BubbleTriangle_001yy___z_m_mvec3_component1(invariants, propagators);
ibp_vectors[2][1] = BubbleTriangle_001yy___z_m_mvec3_component2(invariants, propagators);
ibp_vectors[2][2] = BubbleTriangle_001yy___z_m_mvec3_component3(invariants, propagators);
ibp_vectors[2][5] = BubbleTriangle_001yy___z_m_mvec3_component6(invariants, propagators);
ibp_vectors[2].back() = BubbleTriangle_001yy___z_m_mvec3_divergence(invariants, propagators);
ibp_vectors[4][0] = BubbleTriangle_001yy___z_m_mvec5_component1(invariants, propagators);
ibp_vectors[4][1] = BubbleTriangle_001yy___z_m_mvec5_component2(invariants, propagators);
ibp_vectors[4][2] = BubbleTriangle_001yy___z_m_mvec5_component3(invariants, propagators);
ibp_vectors[4][5] = BubbleTriangle_001yy___z_m_mvec5_component6(invariants, propagators);
ibp_vectors[4].back() = BubbleTriangle_001yy___z_m_mvec5_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BubbleTriangle_001yy___z_m_m_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BubbleTriangle_001yy___z_m_m_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BubbleTriangle_001yy___z_m_m_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BubbleTriangle_001yy___z_m_m_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

