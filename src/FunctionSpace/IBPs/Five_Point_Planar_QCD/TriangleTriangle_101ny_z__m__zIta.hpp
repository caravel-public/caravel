/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][6];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][6];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][6];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][6];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][6];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][6];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][6];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(5)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][6];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleTriangle_101ny_z__m__zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][6];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][6];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,13> TriangleTriangle_101ny_z__m__zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,13> TriangleTriangle_101ny_z__m__zSurfaceItaArray;
TriangleTriangle_101ny_z__m__zSurfaceItaArray[0] = {TriangleTriangle_101ny_z__m__zSurfaceIta1, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[1] = {TriangleTriangle_101ny_z__m__zSurfaceIta2, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[2] = {TriangleTriangle_101ny_z__m__zSurfaceIta3, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[3] = {TriangleTriangle_101ny_z__m__zSurfaceIta4, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[4] = {TriangleTriangle_101ny_z__m__zSurfaceIta5, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[5] = {TriangleTriangle_101ny_z__m__zSurfaceIta6, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[6] = {TriangleTriangle_101ny_z__m__zSurfaceIta7, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[7] = {TriangleTriangle_101ny_z__m__zSurfaceIta8, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[8] = {TriangleTriangle_101ny_z__m__zSurfaceIta9, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[9] = {TriangleTriangle_101ny_z__m__zSurfaceIta10, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[10] = {TriangleTriangle_101ny_z__m__zSurfaceIta11, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[11] = {TriangleTriangle_101ny_z__m__zSurfaceIta12, -1};
TriangleTriangle_101ny_z__m__zSurfaceItaArray[12] = {TriangleTriangle_101ny_z__m__zSurfaceIta13, -1};
return TriangleTriangle_101ny_z__m__zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,13> TriangleTriangle_101ny_z__m__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,13> TriangleTriangle_101ny_z__m__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,13> TriangleTriangle_101ny_z__m__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,13> TriangleTriangle_101ny_z__m__zSurfaceIta<F32>();

#endif

}}}}

