#ifndef BubbleBubble_000yy____m_mVectors_H_INC

#define BubbleBubble_000yy____m_mVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BubbleBubble_000yy____m_mvec1_component1(const T* invariants, const T* D)
{
T z[2];
z[0] = D[0];
z[1] = z[0] * T(-2) + T(-1);
return z[0] * z[1];
}

template<typename T> T BubbleBubble_000yy____m_mvec1_component2(const T* invariants, const T* D)
{
T z[2];
z[0] = D[1];
z[1] = z[0] * T(-2) + T(1);
return z[0] * z[1];
}

template<typename T> T BubbleBubble_000yy____m_mvec1_component3(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[1];
z[2] = z[0] + z[1];
return z[2] * T(4);
}

template<typename T> T BubbleBubble_000yy____m_mvec1_divergence(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[1];
z[2] = -z[0] + -z[1];
return z[2] * T(4);
}

template<typename T> T BubbleBubble_000yy____m_mvec2_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T BubbleBubble_000yy____m_mvec2_component2(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[1];
z[2] = z[1] + T(-1) + -z[0];
z[2] = z[1] * z[2];
return T(1) + z[0] * T(2) + z[2] * T(4);
}

template<typename T> T BubbleBubble_000yy____m_mvec2_component3(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[1];
z[2] = z[0] + z[1] * T(-2) + T(1);
return z[2] * T(4);
}

template<typename T> T BubbleBubble_000yy____m_mvec2_divergence(const T* invariants, const T* D)
{
T z[3];
z[0] = D[0];
z[1] = D[1];
z[2] = T(-1) + z[1] * T(2) + -z[0];
return z[2] * T(4);
}

template<typename T> std::vector<std::vector<T>> BubbleBubble_000yy____m_m_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(2, std::vector<T>(4, T(0)));
ibp_vectors[0][0] = BubbleBubble_000yy____m_mvec1_component1(invariants, propagators);
ibp_vectors[0][1] = BubbleBubble_000yy____m_mvec1_component2(invariants, propagators);
ibp_vectors[0][2] = BubbleBubble_000yy____m_mvec1_component3(invariants, propagators);
ibp_vectors[0].back() = BubbleBubble_000yy____m_mvec1_divergence(invariants, propagators);
ibp_vectors[1][1] = BubbleBubble_000yy____m_mvec2_component2(invariants, propagators);
ibp_vectors[1][2] = BubbleBubble_000yy____m_mvec2_component3(invariants, propagators);
ibp_vectors[1].back() = BubbleBubble_000yy____m_mvec2_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BubbleBubble_000yy____m_m_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BubbleBubble_000yy____m_m_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BubbleBubble_000yy____m_m_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BubbleBubble_000yy____m_m_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

