#ifndef TrianglePentagon_103nn_z__zzz__Vectors_H_INC

#define TrianglePentagon_103nn_z__zzz__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T TrianglePentagon_103nn_z__zzz__vec1_component1(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[7];
z[5] = invariants[1];
z[6] = D[5];
z[7] = z[2] + -z[1];
z[8] = z[5] * T(2) + -z[7];
z[9] = z[0] * T(2);
z[10] = z[4] + z[8] + -z[3] + -z[9];
z[9] = z[9] * z[10];
z[8] = z[3] * z[8];
z[7] = z[6] * z[7];
return z[7] + z[8] + z[9];
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec1_component2(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[7];
z[7] = D[5];
z[8] = D[6];
z[9] = z[5] + z[0] * T(2);
z[10] = z[3] + -z[9];
z[11] = z[6] + z[10] + -z[4];
z[11] = z[1] * z[11];
z[9] = -(z[2] * z[9]);
z[9] = z[9] + z[10] + z[11];
z[10] = z[8] + -z[7];
z[11] = z[4] + -z[3];
z[10] = z[10] * z[11];
return z[10] + z[9] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec1_component9(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[7];
z[6] = z[0] + -z[5];
z[6] = z[2] + z[3] + z[4] + z[6] * T(2) + -z[1];
return z[6] * T(4);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec1_divergence(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[7];
z[6] = invariants[1];
z[7] = z[3] + -z[6];
z[8] = z[1] + z[5] + z[0] * T(-2) + -z[2];
z[7] = z[7] * T(-2) + z[8] * T(3) + -z[4];
return z[7] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec2_component1(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[7];
z[6] = invariants[1];
z[7] = D[5];
z[8] = z[2] + -z[3];
z[9] = z[8] + z[6] * T(2);
z[10] = z[4] + T(-2) + z[1] * T(-2) + -z[5] + -z[9];
z[10] = z[0] * z[10];
z[10] = z[2] + z[10];
z[9] = -(z[4] * z[9]);
z[8] = z[7] * z[8];
return z[8] + z[9] + z[10] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec2_component2(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[7];
z[7] = D[5];
z[8] = D[6];
z[9] = z[8] + -z[7];
z[10] = z[3] + -z[4];
z[9] = z[9] * z[10];
z[11] = z[1] + T(1);
z[12] = z[5] + z[0] * T(2);
z[11] = z[11] * z[12];
z[10] = z[5] + T(-2) + z[2] * T(-2) + -z[6] + -z[10];
z[10] = z[2] * z[10];
z[10] = z[10] + z[11];
return z[9] + z[10] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec2_component9(const T* invariants, const T* D)
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = z[0] + z[6] + T(1) + -z[3];
z[7] = z[1] + z[5] + z[7] * T(2) + -z[2] + -z[4];
return z[7] * T(4);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec2_divergence(const T* invariants, const T* D)
{
T z[9];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = invariants[1];
z[8] = z[2] + z[3] + -z[1] + -z[6];
z[8] = z[4] + z[0] * T(-6) + T(-4) + z[7] * T(-2) + z[8] * T(3) + -z[5];
return z[8] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec3_component1(const T* invariants, const T* D)
{
T z[15];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[7];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[6];
z[8] = D[5];
z[9] = z[0] + z[2];
z[10] = z[0] * T(2);
z[11] = z[10] + -z[8];
z[12] = -(z[9] * z[11]);
z[13] = z[4] * z[11];
z[9] = T(-1) + -z[9];
z[9] = z[9] * T(2) + -z[4];
z[9] = z[6] * z[9];
z[9] = z[9] + z[12] + -z[13];
z[12] = z[4] + z[6];
z[12] = z[7] * z[12];
z[9] = z[12] + z[9] * T(2);
z[9] = z[1] * z[9];
z[12] = z[5] + -z[10];
z[11] = z[11] * z[12];
z[12] = z[2] * z[10];
z[12] = z[12] + -z[4];
z[14] = z[0] * z[7];
z[12] = z[14] + z[12] * T(2);
z[12] = z[3] * z[12];
z[9] = z[9] + z[11] + z[12] + -z[13];
z[11] = z[4] + -z[5];
z[10] = z[11] + -z[10];
z[10] = z[6] * z[10];
z[11] = -(z[8] * z[11]);
z[10] = z[10] + z[11];
z[10] = z[7] * z[10];
return z[10] + z[9] * T(2);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec3_component2(const T* invariants, const T* D)
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[5];
z[4] = D[6];
z[5] = D[7];
z[6] = D[2];
z[7] = D[3];
z[8] = D[4];
z[9] = z[3] + T(2);
z[10] = z[1] * T(2);
z[11] = z[9] + z[10];
z[12] = -(z[10] * z[11]);
z[13] = z[11] * T(2);
z[14] = z[4] + -z[13];
z[14] = z[6] * z[14];
z[10] = z[10] + -z[8];
z[10] = z[4] * z[10];
z[10] = z[10] + z[12] + z[14];
z[10] = z[2] * z[10];
z[12] = z[1] + T(1);
z[12] = z[1] * z[5] * z[12];
z[14] = z[5] + -z[8];
z[14] = z[1] * z[14];
z[14] = z[14] + -z[8];
z[14] = z[4] * z[14];
z[10] = z[10] + z[14] + z[12] * T(4);
z[12] = z[4] + T(-4) + -z[3];
z[12] = z[4] * z[12];
z[12] = z[12] + z[13];
z[12] = z[7] * z[12];
z[9] = z[9] + -z[4];
z[9] = z[4] * z[9];
z[9] = z[9] + -z[13];
z[9] = z[6] * z[9];
z[11] = z[11] + -z[4];
z[13] = T(-1) + -z[2];
z[11] = z[0] * z[11] * z[13];
return z[9] + z[12] + z[10] * T(2) + z[11] * T(4);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec3_component9(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[7];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[6];
z[8] = D[5];
z[9] = z[0] + z[2] + z[4] + z[6];
z[9] = z[1] * z[9];
z[9] = z[0] + z[9];
z[10] = z[8] + z[2] * T(-2);
z[10] = z[3] * z[10];
z[9] = z[4] + z[10] + z[9] * T(2) + -z[5];
z[10] = z[3] * T(-5) + z[6] * T(3);
z[10] = z[7] * z[10];
z[9] = z[10] + z[9] * T(2);
return z[9] * T(4);
}

template<typename T> T TrianglePentagon_103nn_z__zzz__vec3_divergence(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[7];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[6];
z[8] = D[5];
z[9] = z[2] * T(6);
z[10] = z[0] * T(6);
z[11] = z[7] + z[4] * T(-6) + z[6] * T(-3) + T(-2) + -z[9] + -z[10];
z[11] = z[1] * z[11];
z[12] = -(z[6] * z[7]);
z[12] = z[5] + z[12] + -z[4];
z[9] = z[9] + z[8] * T(-2) + T(1) + z[7] * T(5);
z[9] = z[3] * z[9];
z[9] = z[8] + z[9] + z[11] + z[12] * T(3) + -z[10];
return z[9] * T(4);
}

template<typename T> std::vector<std::vector<T>> TrianglePentagon_103nn_z__zzz___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(3, std::vector<T>(10, T(0)));
ibp_vectors[0][0] = TrianglePentagon_103nn_z__zzz__vec1_component1(invariants, propagators);
ibp_vectors[0][1] = TrianglePentagon_103nn_z__zzz__vec1_component2(invariants, propagators);
ibp_vectors[0][8] = TrianglePentagon_103nn_z__zzz__vec1_component9(invariants, propagators);
ibp_vectors[0].back() = TrianglePentagon_103nn_z__zzz__vec1_divergence(invariants, propagators);
ibp_vectors[1][0] = TrianglePentagon_103nn_z__zzz__vec2_component1(invariants, propagators);
ibp_vectors[1][1] = TrianglePentagon_103nn_z__zzz__vec2_component2(invariants, propagators);
ibp_vectors[1][8] = TrianglePentagon_103nn_z__zzz__vec2_component9(invariants, propagators);
ibp_vectors[1].back() = TrianglePentagon_103nn_z__zzz__vec2_divergence(invariants, propagators);
ibp_vectors[2][0] = TrianglePentagon_103nn_z__zzz__vec3_component1(invariants, propagators);
ibp_vectors[2][1] = TrianglePentagon_103nn_z__zzz__vec3_component2(invariants, propagators);
ibp_vectors[2][8] = TrianglePentagon_103nn_z__zzz__vec3_component9(invariants, propagators);
ibp_vectors[2].back() = TrianglePentagon_103nn_z__zzz__vec3_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TrianglePentagon_103nn_z__zzz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TrianglePentagon_103nn_z__zzz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TrianglePentagon_103nn_z__zzz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TrianglePentagon_103nn_z__zzz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

