/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][7];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[0][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][7];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[1][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][7];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[2][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubblePentagon_003ny___zzz__mSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[5][7];
z[1] = ibp_vectors[5].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[5][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,10> BubblePentagon_003ny___zzz__mSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,10> BubblePentagon_003ny___zzz__mSurfaceItaArray;
BubblePentagon_003ny___zzz__mSurfaceItaArray[0] = {BubblePentagon_003ny___zzz__mSurfaceIta1, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[1] = {BubblePentagon_003ny___zzz__mSurfaceIta2, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[2] = {BubblePentagon_003ny___zzz__mSurfaceIta3, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[3] = {BubblePentagon_003ny___zzz__mSurfaceIta4, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[4] = {BubblePentagon_003ny___zzz__mSurfaceIta5, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[5] = {BubblePentagon_003ny___zzz__mSurfaceIta6, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[6] = {BubblePentagon_003ny___zzz__mSurfaceIta7, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[7] = {BubblePentagon_003ny___zzz__mSurfaceIta8, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[8] = {BubblePentagon_003ny___zzz__mSurfaceIta9, -1};
BubblePentagon_003ny___zzz__mSurfaceItaArray[9] = {BubblePentagon_003ny___zzz__mSurfaceIta10, -1};
return BubblePentagon_003ny___zzz__mSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,10> BubblePentagon_003ny___zzz__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,10> BubblePentagon_003ny___zzz__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,10> BubblePentagon_003ny___zzz__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,10> BubblePentagon_003ny___zzz__mSurfaceIta<F32>();

#endif

}}}}

