/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][11];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][11];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][1];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][2];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[2][11];
z[5] = ibp_vectors[2].back();
z[6] = ibp_vectors[2][3];
z[7] = ibp_vectors[2][2];
z[8] = ibp_vectors[2][1];
z[9] = ibp_vectors[2][0];
z[10] = z[5] + (z[4] * T(7)) / T(2);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
zd[0] = z[10] + z[11];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][1];
z[7] = ibp_vectors[2][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][2];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[2][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
z[4] = ibp_vectors[2].back();
z[5] = ibp_vectors[2][3];
z[6] = ibp_vectors[2][2];
z[7] = ibp_vectors[2][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[2][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][11];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[3][11];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][1];
z[5] = ibp_vectors[3][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[3][11];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
z[4] = ibp_vectors[3].back();
z[5] = ibp_vectors[3][2];
z[6] = ibp_vectors[3][1];
z[7] = ibp_vectors[3][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
z[4] = ibp_vectors[3].back();
z[5] = ibp_vectors[3][2];
z[6] = ibp_vectors[3][1];
z[7] = ibp_vectors[3][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[3][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = ibp_vectors[3].back();
z[4] = ibp_vectors[3][2];
z[5] = ibp_vectors[3][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[3][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][11];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[4][11];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][1];
z[5] = ibp_vectors[4][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta77(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta78(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta79(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta80(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[4][11];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta81(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta82(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta83(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[4][11];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][2];
z[6] = ibp_vectors[4][1];
z[7] = ibp_vectors[4][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta84(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][3];
z[6] = ibp_vectors[4][1];
z[7] = ibp_vectors[4][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta85(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta86(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][3];
z[6] = ibp_vectors[4][2];
z[7] = ibp_vectors[4][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta87(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta88(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta89(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta90(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta91(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta92(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][3];
z[6] = ibp_vectors[4][2];
z[7] = ibp_vectors[4][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta93(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta94(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[4][11];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][2];
z[6] = ibp_vectors[4][1];
z[7] = ibp_vectors[4][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta95(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[4][11];
z[5] = ibp_vectors[4].back();
z[6] = ibp_vectors[4][3];
z[7] = ibp_vectors[4][2];
z[8] = ibp_vectors[4][1];
z[9] = ibp_vectors[4][0];
z[10] = z[5] + (z[4] * T(7)) / T(2);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
zd[0] = z[10] + z[11];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta96(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][3];
z[6] = ibp_vectors[4][1];
z[7] = ibp_vectors[4][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta97(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][2];
z[5] = ibp_vectors[4][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[4][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta98(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
z[4] = ibp_vectors[4].back();
z[5] = ibp_vectors[4][3];
z[6] = ibp_vectors[4][2];
z[7] = ibp_vectors[4][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[4][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta99(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = ibp_vectors[4].back();
z[4] = ibp_vectors[4][3];
z[5] = ibp_vectors[4][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[4][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta100(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[5][11];
z[1] = ibp_vectors[5].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[5][11];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta101(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta102(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta103(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta104(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta105(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta106(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][1];
z[5] = ibp_vectors[5][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta107(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta108(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta109(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta110(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta111(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta112(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta113(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta114(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta115(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta116(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][1];
z[5] = ibp_vectors[5][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta117(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta118(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta119(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][1];
z[5] = ibp_vectors[5][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta120(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[5][11];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][2];
z[6] = ibp_vectors[5][1];
z[7] = ibp_vectors[5][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta121(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[5][11];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][3];
z[6] = ibp_vectors[5][1];
z[7] = ibp_vectors[5][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta122(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta123(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta124(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[5][11];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta125(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta126(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta127(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta128(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta129(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta130(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[5][11];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][2];
z[6] = ibp_vectors[5][1];
z[7] = ibp_vectors[5][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta131(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta132(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta133(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[5][11];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][3];
z[6] = ibp_vectors[5][1];
z[7] = ibp_vectors[5][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[5][11];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta134(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta135(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta136(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta137(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[6][11];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[6][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta138(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[6][11];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[6][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta139(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[6][11];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][2];
z[5] = ibp_vectors[6][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[6][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta140(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[6][11];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][3];
z[5] = ibp_vectors[6][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[6][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta141(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[6][11];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][2];
z[5] = ibp_vectors[6][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[6][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta142(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[6][11];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][3];
z[5] = ibp_vectors[6][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[6][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta143(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[7][11];
z[2] = ibp_vectors[7].back();
z[3] = ibp_vectors[7][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[7][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta144(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[7][11];
z[2] = ibp_vectors[7].back();
z[3] = ibp_vectors[7][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[7][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta145(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[7][11];
z[3] = ibp_vectors[7].back();
z[4] = ibp_vectors[7][2];
z[5] = ibp_vectors[7][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[7][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta146(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[7][11];
z[3] = ibp_vectors[7].back();
z[4] = ibp_vectors[7][2];
z[5] = ibp_vectors[7][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[7][11];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta147(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[7][11];
z[3] = ibp_vectors[7].back();
z[4] = ibp_vectors[7][3];
z[5] = ibp_vectors[7][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[7][11];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta148(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[7][11];
z[3] = ibp_vectors[7].back();
z[4] = ibp_vectors[7][2];
z[5] = ibp_vectors[7][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[7][11];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta149(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[7][11];
z[3] = ibp_vectors[7].back();
z[4] = ibp_vectors[7][3];
z[5] = ibp_vectors[7][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[7][11];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta150(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[10][11];
z[2] = ibp_vectors[10].back();
z[3] = ibp_vectors[10][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[10][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta151(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[10][11];
z[2] = ibp_vectors[10].back();
z[3] = ibp_vectors[10][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[10][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta152(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[11][11];
z[2] = ibp_vectors[11].back();
z[3] = ibp_vectors[11][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[11][11];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta153(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[11][11];
z[3] = ibp_vectors[11].back();
z[4] = ibp_vectors[11][3];
z[5] = ibp_vectors[11][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[11][11];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta154(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[11][11];
z[2] = ibp_vectors[11].back();
z[3] = ibp_vectors[11][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[11][11];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta155(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[11][11];
z[3] = ibp_vectors[11].back();
z[4] = ibp_vectors[11][3];
z[5] = ibp_vectors[11][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[11][11];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta156(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[11][11];
z[3] = ibp_vectors[11].back();
z[4] = ibp_vectors[11][2];
z[5] = ibp_vectors[11][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[11][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxBox_202ny_zz__zz__zSurfaceIta157(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[12][11];
z[3] = ibp_vectors[12].back();
z[4] = ibp_vectors[12][2];
z[5] = ibp_vectors[12][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[0], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[12][11];
zd[1] = -((prod_pow(z[0], 3) * z[1] * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,157> BoxBox_202ny_zz__zz__zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,157> BoxBox_202ny_zz__zz__zSurfaceItaArray;
BoxBox_202ny_zz__zz__zSurfaceItaArray[0] = {BoxBox_202ny_zz__zz__zSurfaceIta1, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[1] = {BoxBox_202ny_zz__zz__zSurfaceIta2, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[2] = {BoxBox_202ny_zz__zz__zSurfaceIta3, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[3] = {BoxBox_202ny_zz__zz__zSurfaceIta4, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[4] = {BoxBox_202ny_zz__zz__zSurfaceIta5, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[5] = {BoxBox_202ny_zz__zz__zSurfaceIta6, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[6] = {BoxBox_202ny_zz__zz__zSurfaceIta7, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[7] = {BoxBox_202ny_zz__zz__zSurfaceIta8, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[8] = {BoxBox_202ny_zz__zz__zSurfaceIta9, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[9] = {BoxBox_202ny_zz__zz__zSurfaceIta10, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[10] = {BoxBox_202ny_zz__zz__zSurfaceIta11, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[11] = {BoxBox_202ny_zz__zz__zSurfaceIta12, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[12] = {BoxBox_202ny_zz__zz__zSurfaceIta13, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[13] = {BoxBox_202ny_zz__zz__zSurfaceIta14, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[14] = {BoxBox_202ny_zz__zz__zSurfaceIta15, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[15] = {BoxBox_202ny_zz__zz__zSurfaceIta16, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[16] = {BoxBox_202ny_zz__zz__zSurfaceIta17, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[17] = {BoxBox_202ny_zz__zz__zSurfaceIta18, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[18] = {BoxBox_202ny_zz__zz__zSurfaceIta19, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[19] = {BoxBox_202ny_zz__zz__zSurfaceIta20, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[20] = {BoxBox_202ny_zz__zz__zSurfaceIta21, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[21] = {BoxBox_202ny_zz__zz__zSurfaceIta22, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[22] = {BoxBox_202ny_zz__zz__zSurfaceIta23, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[23] = {BoxBox_202ny_zz__zz__zSurfaceIta24, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[24] = {BoxBox_202ny_zz__zz__zSurfaceIta25, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[25] = {BoxBox_202ny_zz__zz__zSurfaceIta26, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[26] = {BoxBox_202ny_zz__zz__zSurfaceIta27, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[27] = {BoxBox_202ny_zz__zz__zSurfaceIta28, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[28] = {BoxBox_202ny_zz__zz__zSurfaceIta29, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[29] = {BoxBox_202ny_zz__zz__zSurfaceIta30, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[30] = {BoxBox_202ny_zz__zz__zSurfaceIta31, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[31] = {BoxBox_202ny_zz__zz__zSurfaceIta32, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[32] = {BoxBox_202ny_zz__zz__zSurfaceIta33, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[33] = {BoxBox_202ny_zz__zz__zSurfaceIta34, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[34] = {BoxBox_202ny_zz__zz__zSurfaceIta35, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[35] = {BoxBox_202ny_zz__zz__zSurfaceIta36, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[36] = {BoxBox_202ny_zz__zz__zSurfaceIta37, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[37] = {BoxBox_202ny_zz__zz__zSurfaceIta38, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[38] = {BoxBox_202ny_zz__zz__zSurfaceIta39, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[39] = {BoxBox_202ny_zz__zz__zSurfaceIta40, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[40] = {BoxBox_202ny_zz__zz__zSurfaceIta41, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[41] = {BoxBox_202ny_zz__zz__zSurfaceIta42, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[42] = {BoxBox_202ny_zz__zz__zSurfaceIta43, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[43] = {BoxBox_202ny_zz__zz__zSurfaceIta44, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[44] = {BoxBox_202ny_zz__zz__zSurfaceIta45, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[45] = {BoxBox_202ny_zz__zz__zSurfaceIta46, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[46] = {BoxBox_202ny_zz__zz__zSurfaceIta47, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[47] = {BoxBox_202ny_zz__zz__zSurfaceIta48, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[48] = {BoxBox_202ny_zz__zz__zSurfaceIta49, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[49] = {BoxBox_202ny_zz__zz__zSurfaceIta50, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[50] = {BoxBox_202ny_zz__zz__zSurfaceIta51, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[51] = {BoxBox_202ny_zz__zz__zSurfaceIta52, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[52] = {BoxBox_202ny_zz__zz__zSurfaceIta53, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[53] = {BoxBox_202ny_zz__zz__zSurfaceIta54, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[54] = {BoxBox_202ny_zz__zz__zSurfaceIta55, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[55] = {BoxBox_202ny_zz__zz__zSurfaceIta56, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[56] = {BoxBox_202ny_zz__zz__zSurfaceIta57, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[57] = {BoxBox_202ny_zz__zz__zSurfaceIta58, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[58] = {BoxBox_202ny_zz__zz__zSurfaceIta59, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[59] = {BoxBox_202ny_zz__zz__zSurfaceIta60, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[60] = {BoxBox_202ny_zz__zz__zSurfaceIta61, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[61] = {BoxBox_202ny_zz__zz__zSurfaceIta62, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[62] = {BoxBox_202ny_zz__zz__zSurfaceIta63, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[63] = {BoxBox_202ny_zz__zz__zSurfaceIta64, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[64] = {BoxBox_202ny_zz__zz__zSurfaceIta65, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[65] = {BoxBox_202ny_zz__zz__zSurfaceIta66, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[66] = {BoxBox_202ny_zz__zz__zSurfaceIta67, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[67] = {BoxBox_202ny_zz__zz__zSurfaceIta68, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[68] = {BoxBox_202ny_zz__zz__zSurfaceIta69, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[69] = {BoxBox_202ny_zz__zz__zSurfaceIta70, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[70] = {BoxBox_202ny_zz__zz__zSurfaceIta71, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[71] = {BoxBox_202ny_zz__zz__zSurfaceIta72, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[72] = {BoxBox_202ny_zz__zz__zSurfaceIta73, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[73] = {BoxBox_202ny_zz__zz__zSurfaceIta74, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[74] = {BoxBox_202ny_zz__zz__zSurfaceIta75, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[75] = {BoxBox_202ny_zz__zz__zSurfaceIta76, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[76] = {BoxBox_202ny_zz__zz__zSurfaceIta77, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[77] = {BoxBox_202ny_zz__zz__zSurfaceIta78, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[78] = {BoxBox_202ny_zz__zz__zSurfaceIta79, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[79] = {BoxBox_202ny_zz__zz__zSurfaceIta80, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[80] = {BoxBox_202ny_zz__zz__zSurfaceIta81, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[81] = {BoxBox_202ny_zz__zz__zSurfaceIta82, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[82] = {BoxBox_202ny_zz__zz__zSurfaceIta83, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[83] = {BoxBox_202ny_zz__zz__zSurfaceIta84, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[84] = {BoxBox_202ny_zz__zz__zSurfaceIta85, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[85] = {BoxBox_202ny_zz__zz__zSurfaceIta86, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[86] = {BoxBox_202ny_zz__zz__zSurfaceIta87, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[87] = {BoxBox_202ny_zz__zz__zSurfaceIta88, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[88] = {BoxBox_202ny_zz__zz__zSurfaceIta89, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[89] = {BoxBox_202ny_zz__zz__zSurfaceIta90, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[90] = {BoxBox_202ny_zz__zz__zSurfaceIta91, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[91] = {BoxBox_202ny_zz__zz__zSurfaceIta92, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[92] = {BoxBox_202ny_zz__zz__zSurfaceIta93, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[93] = {BoxBox_202ny_zz__zz__zSurfaceIta94, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[94] = {BoxBox_202ny_zz__zz__zSurfaceIta95, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[95] = {BoxBox_202ny_zz__zz__zSurfaceIta96, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[96] = {BoxBox_202ny_zz__zz__zSurfaceIta97, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[97] = {BoxBox_202ny_zz__zz__zSurfaceIta98, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[98] = {BoxBox_202ny_zz__zz__zSurfaceIta99, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[99] = {BoxBox_202ny_zz__zz__zSurfaceIta100, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[100] = {BoxBox_202ny_zz__zz__zSurfaceIta101, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[101] = {BoxBox_202ny_zz__zz__zSurfaceIta102, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[102] = {BoxBox_202ny_zz__zz__zSurfaceIta103, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[103] = {BoxBox_202ny_zz__zz__zSurfaceIta104, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[104] = {BoxBox_202ny_zz__zz__zSurfaceIta105, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[105] = {BoxBox_202ny_zz__zz__zSurfaceIta106, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[106] = {BoxBox_202ny_zz__zz__zSurfaceIta107, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[107] = {BoxBox_202ny_zz__zz__zSurfaceIta108, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[108] = {BoxBox_202ny_zz__zz__zSurfaceIta109, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[109] = {BoxBox_202ny_zz__zz__zSurfaceIta110, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[110] = {BoxBox_202ny_zz__zz__zSurfaceIta111, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[111] = {BoxBox_202ny_zz__zz__zSurfaceIta112, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[112] = {BoxBox_202ny_zz__zz__zSurfaceIta113, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[113] = {BoxBox_202ny_zz__zz__zSurfaceIta114, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[114] = {BoxBox_202ny_zz__zz__zSurfaceIta115, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[115] = {BoxBox_202ny_zz__zz__zSurfaceIta116, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[116] = {BoxBox_202ny_zz__zz__zSurfaceIta117, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[117] = {BoxBox_202ny_zz__zz__zSurfaceIta118, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[118] = {BoxBox_202ny_zz__zz__zSurfaceIta119, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[119] = {BoxBox_202ny_zz__zz__zSurfaceIta120, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[120] = {BoxBox_202ny_zz__zz__zSurfaceIta121, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[121] = {BoxBox_202ny_zz__zz__zSurfaceIta122, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[122] = {BoxBox_202ny_zz__zz__zSurfaceIta123, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[123] = {BoxBox_202ny_zz__zz__zSurfaceIta124, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[124] = {BoxBox_202ny_zz__zz__zSurfaceIta125, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[125] = {BoxBox_202ny_zz__zz__zSurfaceIta126, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[126] = {BoxBox_202ny_zz__zz__zSurfaceIta127, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[127] = {BoxBox_202ny_zz__zz__zSurfaceIta128, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[128] = {BoxBox_202ny_zz__zz__zSurfaceIta129, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[129] = {BoxBox_202ny_zz__zz__zSurfaceIta130, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[130] = {BoxBox_202ny_zz__zz__zSurfaceIta131, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[131] = {BoxBox_202ny_zz__zz__zSurfaceIta132, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[132] = {BoxBox_202ny_zz__zz__zSurfaceIta133, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[133] = {BoxBox_202ny_zz__zz__zSurfaceIta134, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[134] = {BoxBox_202ny_zz__zz__zSurfaceIta135, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[135] = {BoxBox_202ny_zz__zz__zSurfaceIta136, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[136] = {BoxBox_202ny_zz__zz__zSurfaceIta137, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[137] = {BoxBox_202ny_zz__zz__zSurfaceIta138, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[138] = {BoxBox_202ny_zz__zz__zSurfaceIta139, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[139] = {BoxBox_202ny_zz__zz__zSurfaceIta140, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[140] = {BoxBox_202ny_zz__zz__zSurfaceIta141, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[141] = {BoxBox_202ny_zz__zz__zSurfaceIta142, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[142] = {BoxBox_202ny_zz__zz__zSurfaceIta143, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[143] = {BoxBox_202ny_zz__zz__zSurfaceIta144, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[144] = {BoxBox_202ny_zz__zz__zSurfaceIta145, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[145] = {BoxBox_202ny_zz__zz__zSurfaceIta146, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[146] = {BoxBox_202ny_zz__zz__zSurfaceIta147, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[147] = {BoxBox_202ny_zz__zz__zSurfaceIta148, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[148] = {BoxBox_202ny_zz__zz__zSurfaceIta149, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[149] = {BoxBox_202ny_zz__zz__zSurfaceIta150, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[150] = {BoxBox_202ny_zz__zz__zSurfaceIta151, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[151] = {BoxBox_202ny_zz__zz__zSurfaceIta152, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[152] = {BoxBox_202ny_zz__zz__zSurfaceIta153, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[153] = {BoxBox_202ny_zz__zz__zSurfaceIta154, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[154] = {BoxBox_202ny_zz__zz__zSurfaceIta155, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[155] = {BoxBox_202ny_zz__zz__zSurfaceIta156, -1};
BoxBox_202ny_zz__zz__zSurfaceItaArray[156] = {BoxBox_202ny_zz__zz__zSurfaceIta157, -1};
return BoxBox_202ny_zz__zz__zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,157> BoxBox_202ny_zz__zz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,157> BoxBox_202ny_zz__zz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,157> BoxBox_202ny_zz__zz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,157> BoxBox_202ny_zz__zz__zSurfaceIta<F32>();

#endif

}}}}

