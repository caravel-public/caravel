#ifndef TriangleTriangle_101ny_z__z__mVectors_H_INC

#define TriangleTriangle_101ny_z__z__mVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T TriangleTriangle_101ny_z__z__mvec1_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec1_component2(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = D[5];
z[4] = D[2];
z[5] = D[3];
z[6] = z[5] + -z[4];
z[7] = -(z[3] * z[6]);
z[6] = z[6] + T(1);
z[6] = z[2] * z[6];
z[8] = z[3] + -z[0] + -z[2];
z[8] = z[1] * z[8];
return z[6] + z[7] + z[8] * T(4) + -z[4];
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec1_component7(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = z[0] + z[1] + -z[2];
return z[3] * T(8);
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = z[2] + -z[1];
z[3] = z[3] * T(2) + -z[0];
return z[3] * T(4);
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec2_component1(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[5];
z[6] = z[5] + -z[4];
z[7] = -(z[3] * z[6]);
z[6] = z[6] + T(1);
z[6] = z[2] * z[6];
z[8] = z[3] + -z[1] + -z[2];
z[8] = z[0] * z[8];
return z[6] + z[7] + z[8] * T(4) + -z[4];
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec2_component2(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec2_component7(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = z[0] + z[1] + -z[2];
return z[3] * T(8);
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec2_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = z[2] + -z[1];
z[3] = z[3] * T(2) + -z[0];
return z[3] * T(4);
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec3_component1(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = z[1] + -z[2];
z[3] = z[0] * z[3];
return z[3] * T(2) + -z[1];
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec3_component2(const T* invariants, const T* D)
{
T z[3];
z[0] = D[1];
z[1] = z[0] * T(2);
z[2] = T(1) + -z[1];
return z[1] * z[2];
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec3_component7(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = z[2] + T(-1) + z[0] * T(2) + -z[1];
return z[3] * T(4);
}

template<typename T> T TriangleTriangle_101ny_z__z__mvec3_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = z[1] + -z[2];
z[3] = z[0] * T(-4) + T(1) + z[3] * T(2);
return z[3] * T(2);
}

template<typename T> std::vector<std::vector<T>> TriangleTriangle_101ny_z__z__m_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(3, std::vector<T>(8, T(0)));
ibp_vectors[0][1] = TriangleTriangle_101ny_z__z__mvec1_component2(invariants, propagators);
ibp_vectors[0][6] = TriangleTriangle_101ny_z__z__mvec1_component7(invariants, propagators);
ibp_vectors[0].back() = TriangleTriangle_101ny_z__z__mvec1_divergence(invariants, propagators);
ibp_vectors[1][0] = TriangleTriangle_101ny_z__z__mvec2_component1(invariants, propagators);
ibp_vectors[1][6] = TriangleTriangle_101ny_z__z__mvec2_component7(invariants, propagators);
ibp_vectors[1].back() = TriangleTriangle_101ny_z__z__mvec2_divergence(invariants, propagators);
ibp_vectors[2][0] = TriangleTriangle_101ny_z__z__mvec3_component1(invariants, propagators);
ibp_vectors[2][1] = TriangleTriangle_101ny_z__z__mvec3_component2(invariants, propagators);
ibp_vectors[2][6] = TriangleTriangle_101ny_z__z__mvec3_component7(invariants, propagators);
ibp_vectors[2].back() = TriangleTriangle_101ny_z__z__mvec3_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleTriangle_101ny_z__z__m_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleTriangle_101ny_z__z__m_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleTriangle_101ny_z__z__m_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleTriangle_101ny_z__z__m_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

