#ifndef BubblePentagon_003nn___mzz__Vectors_H_INC

#define BubblePentagon_003nn___mzz__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BubblePentagon_003nn___mzz__vec1_component1(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = z[2] + z[0] * T(-2) + -z[1];
return z[0] * z[3] * T(4);
}

template<typename T> T BubblePentagon_003nn___mzz__vec1_component2(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = D[5];
z[4] = D[3];
z[5] = z[2] + -z[4];
z[5] = z[1] * z[5];
z[6] = z[3] + z[1] * T(-4) + T(-1) + -z[2];
z[6] = z[0] * z[6];
z[5] = z[5] + z[6];
return z[5] * T(2) + -z[4];
}

template<typename T> T BubblePentagon_003nn___mzz__vec1_component6(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = z[1] + z[0] * T(4) + -z[2];
return z[3] * T(4);
}

template<typename T> T BubblePentagon_003nn___mzz__vec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = z[2] + z[0] * T(-4) + -z[1];
return z[3] * T(6);
}

template<typename T> T BubblePentagon_003nn___mzz__vec2_component1(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = D[5];
z[4] = D[3];
z[5] = z[2] + -z[4];
z[5] = z[1] * z[5];
z[6] = z[3] + z[1] * T(-4) + T(-1) + -z[2];
z[6] = z[0] * z[6];
z[5] = z[5] + z[6];
return z[5] * T(2) + -z[4];
}

template<typename T> T BubblePentagon_003nn___mzz__vec2_component2(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = D[5];
z[3] = z[2] + z[0] * T(-2) + T(-1) + -z[1];
return z[0] * z[3] * T(4);
}

template<typename T> T BubblePentagon_003nn___mzz__vec2_component6(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = D[5];
z[3] = z[1] + T(1) + z[0] * T(4) + -z[2];
return z[3] * T(4);
}

template<typename T> T BubblePentagon_003nn___mzz__vec2_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = D[5];
z[3] = z[2] + z[0] * T(-4) + T(-1) + -z[1];
return z[3] * T(6);
}

template<typename T> T BubblePentagon_003nn___mzz__vec4_component1(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = D[5];
z[4] = D[1];
z[5] = z[2] + z[3] + z[1] * T(-2) + T(1);
z[5] = z[0] * z[5];
z[6] = z[2] + -z[1];
z[6] = z[4] * z[6];
z[5] = z[5] + z[6];
return z[5] * T(2) + -z[1];
}

template<typename T> T BubblePentagon_003nn___mzz__vec4_component2(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = D[1];
z[4] = D[3];
z[5] = z[2] + T(-1) + -z[1];
z[5] = z[0] * z[5];
z[6] = z[2] * T(2) + -z[1] + -z[4];
z[6] = z[3] * z[6];
z[5] = z[5] + z[6];
return z[5] * T(2) + -z[4];
}

template<typename T> T BubblePentagon_003nn___mzz__vec4_component6(const T* invariants, const T* D)
{
T z[3];
z[0] = D[3];
z[1] = D[5];
z[2] = z[1] + -z[0];
z[2] = z[2] * T(-3) + T(-1);
return z[2] * T(4);
}

template<typename T> T BubblePentagon_003nn___mzz__vec4_divergence(const T* invariants, const T* D)
{
T z[3];
z[0] = D[3];
z[1] = D[5];
z[2] = z[1] + -z[0];
z[2] = T(-1) + z[2] * T(3);
return z[2] * T(2);
}

template<typename T> std::vector<std::vector<T>> BubblePentagon_003nn___mzz___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(4, std::vector<T>(7, T(0)));
ibp_vectors[0][0] = BubblePentagon_003nn___mzz__vec1_component1(invariants, propagators);
ibp_vectors[0][1] = BubblePentagon_003nn___mzz__vec1_component2(invariants, propagators);
ibp_vectors[0][5] = BubblePentagon_003nn___mzz__vec1_component6(invariants, propagators);
ibp_vectors[0].back() = BubblePentagon_003nn___mzz__vec1_divergence(invariants, propagators);
ibp_vectors[1][0] = BubblePentagon_003nn___mzz__vec2_component1(invariants, propagators);
ibp_vectors[1][1] = BubblePentagon_003nn___mzz__vec2_component2(invariants, propagators);
ibp_vectors[1][5] = BubblePentagon_003nn___mzz__vec2_component6(invariants, propagators);
ibp_vectors[1].back() = BubblePentagon_003nn___mzz__vec2_divergence(invariants, propagators);
ibp_vectors[3][0] = BubblePentagon_003nn___mzz__vec4_component1(invariants, propagators);
ibp_vectors[3][1] = BubblePentagon_003nn___mzz__vec4_component2(invariants, propagators);
ibp_vectors[3][5] = BubblePentagon_003nn___mzz__vec4_component6(invariants, propagators);
ibp_vectors[3].back() = BubblePentagon_003nn___mzz__vec4_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BubblePentagon_003nn___mzz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BubblePentagon_003nn___mzz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BubblePentagon_003nn___mzz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BubblePentagon_003nn___mzz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

