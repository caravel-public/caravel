/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][7];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[0][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][7];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][7];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[1][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][7];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[2][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][1];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
z[3] = ibp_vectors[2].back();
z[4] = ibp_vectors[2][3];
z[5] = ibp_vectors[2][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[2][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[5][7];
z[1] = ibp_vectors[5].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[5][7];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[5][7];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][2];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[5][7];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[5][7];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][2];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[5][7];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleBox_002yy___mz_z_zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[5][7];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][2];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[5][7];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,34> BubbleBox_002yy___mz_z_zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,34> BubbleBox_002yy___mz_z_zSurfaceItaArray;
BubbleBox_002yy___mz_z_zSurfaceItaArray[0] = {BubbleBox_002yy___mz_z_zSurfaceIta1, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[1] = {BubbleBox_002yy___mz_z_zSurfaceIta2, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[2] = {BubbleBox_002yy___mz_z_zSurfaceIta3, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[3] = {BubbleBox_002yy___mz_z_zSurfaceIta4, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[4] = {BubbleBox_002yy___mz_z_zSurfaceIta5, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[5] = {BubbleBox_002yy___mz_z_zSurfaceIta6, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[6] = {BubbleBox_002yy___mz_z_zSurfaceIta7, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[7] = {BubbleBox_002yy___mz_z_zSurfaceIta8, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[8] = {BubbleBox_002yy___mz_z_zSurfaceIta9, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[9] = {BubbleBox_002yy___mz_z_zSurfaceIta10, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[10] = {BubbleBox_002yy___mz_z_zSurfaceIta11, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[11] = {BubbleBox_002yy___mz_z_zSurfaceIta12, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[12] = {BubbleBox_002yy___mz_z_zSurfaceIta13, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[13] = {BubbleBox_002yy___mz_z_zSurfaceIta14, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[14] = {BubbleBox_002yy___mz_z_zSurfaceIta15, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[15] = {BubbleBox_002yy___mz_z_zSurfaceIta16, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[16] = {BubbleBox_002yy___mz_z_zSurfaceIta17, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[17] = {BubbleBox_002yy___mz_z_zSurfaceIta18, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[18] = {BubbleBox_002yy___mz_z_zSurfaceIta19, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[19] = {BubbleBox_002yy___mz_z_zSurfaceIta20, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[20] = {BubbleBox_002yy___mz_z_zSurfaceIta21, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[21] = {BubbleBox_002yy___mz_z_zSurfaceIta22, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[22] = {BubbleBox_002yy___mz_z_zSurfaceIta23, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[23] = {BubbleBox_002yy___mz_z_zSurfaceIta24, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[24] = {BubbleBox_002yy___mz_z_zSurfaceIta25, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[25] = {BubbleBox_002yy___mz_z_zSurfaceIta26, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[26] = {BubbleBox_002yy___mz_z_zSurfaceIta27, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[27] = {BubbleBox_002yy___mz_z_zSurfaceIta28, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[28] = {BubbleBox_002yy___mz_z_zSurfaceIta29, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[29] = {BubbleBox_002yy___mz_z_zSurfaceIta30, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[30] = {BubbleBox_002yy___mz_z_zSurfaceIta31, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[31] = {BubbleBox_002yy___mz_z_zSurfaceIta32, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[32] = {BubbleBox_002yy___mz_z_zSurfaceIta33, -1};
BubbleBox_002yy___mz_z_zSurfaceItaArray[33] = {BubbleBox_002yy___mz_z_zSurfaceIta34, -1};
return BubbleBox_002yy___mz_z_zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,34> BubbleBox_002yy___mz_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,34> BubbleBox_002yy___mz_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,34> BubbleBox_002yy___mz_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,34> BubbleBox_002yy___mz_z_zSurfaceIta<F32>();

#endif

}}}}

