#include "FunctionSpace/IBPhelper.h"

#include "TriangleBox_102nn_m__mz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,4> TriangleBox_102nn_m__mz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,4> TriangleBox_102nn_m__mz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,4> TriangleBox_102nn_m__mz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,4> TriangleBox_102nn_m__mz__SurfaceIta<F32>();

#endif

}}}}

