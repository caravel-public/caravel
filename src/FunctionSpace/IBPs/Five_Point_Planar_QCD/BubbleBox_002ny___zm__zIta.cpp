#include "FunctionSpace/IBPhelper.h"

#include "BubbleBox_002ny___zm__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,6> BubbleBox_002ny___zm__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,6> BubbleBox_002ny___zm__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,6> BubbleBox_002ny___zm__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,6> BubbleBox_002ny___zm__zSurfaceIta<F32>();

#endif

}}}}

