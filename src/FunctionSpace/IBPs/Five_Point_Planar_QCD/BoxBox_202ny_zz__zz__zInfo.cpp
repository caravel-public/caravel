#include "FunctionSpace/IBPhelper.h"

#include "Core/momD.h"

#include "BoxBox_202ny_zz__zz__zVectors.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::vector<std::vector<C>> BoxBox_202ny_zz__zz__z_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

template std::vector<std::vector<CHP>> BoxBox_202ny_zz__zz__z_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

template std::vector<std::vector<CVHP>> BoxBox_202ny_zz__zz__z_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

template std::vector<std::vector<F32>> BoxBox_202ny_zz__zz__z_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

