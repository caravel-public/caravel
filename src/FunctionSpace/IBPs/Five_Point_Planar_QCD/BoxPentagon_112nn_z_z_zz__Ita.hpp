/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][9];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[1][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + z[2] * T(3);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + z[2] * T(3);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[0] * z[4];
z[7] = z[3] + z[2] * T(3);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][9];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][9];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + z[0] * T(3);
}
{
T z[2];
z[0] = ibp_vectors[3][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BoxPentagon_112nn_z_z_zz__SurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][0];
z[4] = z[2] + z[1] * T(3);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[3][9];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,13> BoxPentagon_112nn_z_z_zz__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,13> BoxPentagon_112nn_z_z_zz__SurfaceItaArray;
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[0] = {BoxPentagon_112nn_z_z_zz__SurfaceIta1, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[1] = {BoxPentagon_112nn_z_z_zz__SurfaceIta2, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[2] = {BoxPentagon_112nn_z_z_zz__SurfaceIta3, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[3] = {BoxPentagon_112nn_z_z_zz__SurfaceIta4, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[4] = {BoxPentagon_112nn_z_z_zz__SurfaceIta5, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[5] = {BoxPentagon_112nn_z_z_zz__SurfaceIta6, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[6] = {BoxPentagon_112nn_z_z_zz__SurfaceIta7, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[7] = {BoxPentagon_112nn_z_z_zz__SurfaceIta8, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[8] = {BoxPentagon_112nn_z_z_zz__SurfaceIta9, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[9] = {BoxPentagon_112nn_z_z_zz__SurfaceIta10, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[10] = {BoxPentagon_112nn_z_z_zz__SurfaceIta11, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[11] = {BoxPentagon_112nn_z_z_zz__SurfaceIta12, -1};
BoxPentagon_112nn_z_z_zz__SurfaceItaArray[12] = {BoxPentagon_112nn_z_z_zz__SurfaceIta13, -1};
return BoxPentagon_112nn_z_z_zz__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,13> BoxPentagon_112nn_z_z_zz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,13> BoxPentagon_112nn_z_z_zz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,13> BoxPentagon_112nn_z_z_zz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,13> BoxPentagon_112nn_z_z_zz__SurfaceIta<F32>();

#endif

}}}}

