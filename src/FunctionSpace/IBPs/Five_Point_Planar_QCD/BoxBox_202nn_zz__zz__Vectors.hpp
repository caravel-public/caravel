#ifndef BoxBox_202nn_zz__zz__Vectors_H_INC

#define BoxBox_202nn_zz__zz__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BoxBox_202nn_zz__zz__vec2_component1(const T* invariants, const T* D)
{
T z[7];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = invariants[1];
z[4] = D[2];
z[5] = z[3] + T(1);
z[5] = z[4] * z[5];
z[6] = z[0] + -z[3];
z[6] = z[1] + z[6] * T(-2) + -z[2];
z[6] = z[0] * z[6];
return z[5] + z[6];
}

template<typename T> T BoxBox_202nn_zz__zz__vec2_component2(const T* invariants, const T* D)
{
T z[7];
z[0] = D[1];
z[1] = D[5];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[1];
z[5] = T(1) + -z[4];
z[5] = z[1] * z[5];
z[6] = z[0] + z[1] + -z[4];
z[6] = z[6] * T(2) + -z[2] + -z[3];
z[6] = z[0] * z[6];
return z[5] + z[6];
}

template<typename T> T BoxBox_202nn_zz__zz__vec2_component10(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = z[0] + -z[1] + -z[4];
z[7] = z[3] + z[5] + z[6] + z[7] * T(2) + -z[2];
return z[7] * T(2);
}

template<typename T> T BoxBox_202nn_zz__zz__vec2_divergence(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = D[7];
z[7] = z[1] + z[4] + -z[0];
z[7] = z[2] + z[7] * T(2) + -z[3] + -z[5] + -z[6];
return z[7] * T(2);
}

template<typename T> T BoxBox_202nn_zz__zz__vec3_component1(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = invariants[1];
z[5] = z[0] * T(2);
z[6] = z[1] + T(-2);
z[7] = z[6] + z[3] * T(3);
z[8] = z[7] + z[2] * T(-2);
z[8] = z[5] * z[8];
z[6] = z[6] + -z[3];
z[9] = z[3] * z[6];
z[10] = z[3] + T(-2) + -z[1];
z[10] = z[2] * z[10];
z[8] = z[8] + z[9] + z[10];
z[8] = z[0] * z[8];
z[5] = -(z[5] * z[7]);
z[6] = z[1] * z[6];
z[5] = z[5] + -z[6];
z[5] = z[4] * z[5];
return z[5] + z[8] + -z[6];
}

template<typename T> T BoxBox_202nn_zz__zz__vec3_component2(const T* invariants, const T* D)
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[5];
z[3] = D[7];
z[4] = D[2];
z[5] = D[4];
z[6] = D[6];
z[7] = invariants[1];
z[8] = D[3];
z[9] = D[8];
z[10] = z[4] + z[0] * T(2);
z[10] = z[10] * T(2);
z[11] = z[9] * T(2);
z[12] = z[0] * T(4) + -z[5];
z[13] = T(-2) + -z[4] + -z[12];
z[13] = z[1] * z[13];
z[13] = z[8] + z[11] + z[13] + -z[10];
z[13] = z[1] * z[13];
z[14] = z[4] + T(-2);
z[15] = z[5] + z[14];
z[11] = z[11] + -z[15];
z[11] = z[1] * z[11];
z[11] = z[11] + -z[8];
z[11] = z[7] * z[11];
z[11] = z[11] + z[13];
z[10] = -(z[1] * z[10]);
z[13] = z[5] + z[8] + -z[14];
z[13] = z[7] * z[13];
z[10] = z[8] + z[10] + z[13] + z[4] * T(-3) + T(2) + -z[12];
z[10] = z[2] * z[10];
z[12] = z[12] + z[14];
z[12] = z[1] * z[12];
z[13] = z[8] + z[1] * T(-4);
z[13] = z[7] * z[13];
z[12] = z[12] + z[13];
z[12] = z[3] * z[12];
z[13] = z[1] * z[15];
z[13] = z[8] + z[13];
z[13] = z[6] * z[13];
return z[10] + z[12] + z[13] + z[11] * T(2);
}

template<typename T> T BoxBox_202nn_zz__zz__vec3_component10(const T* invariants, const T* D)
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[5];
z[6] = D[7];
z[7] = D[6];
z[8] = invariants[1];
z[9] = D[8];
z[10] = z[2] + z[4] * T(3);
z[11] = z[1] * T(2);
z[12] = z[3] + z[11] + -z[6];
z[13] = z[12] + T(2);
z[14] = z[5] + z[13];
z[14] = z[14] * T(2) + -z[10];
z[14] = z[0] * z[14];
z[15] = z[6] + T(-1) + -z[9];
z[10] = z[10] + z[15] * T(2);
z[10] = z[8] * z[10];
z[10] = z[6] + z[10] + z[11] + z[14] + z[9] * T(-2);
z[11] = z[2] + T(-2);
z[12] = z[4] + -z[11] + -z[12];
z[12] = z[4] * z[12];
z[13] = z[13] + z[5] * T(2);
z[13] = z[2] * z[13];
z[11] = -z[4] + -z[11];
z[11] = z[7] * z[11];
z[10] = z[11] + z[12] + z[13] + z[10] * T(2);
return z[10] * T(2);
}

template<typename T> T BoxBox_202nn_zz__zz__vec3_divergence(const T* invariants, const T* D)
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[5];
z[6] = D[7];
z[7] = D[6];
z[8] = invariants[1];
z[9] = D[8];
z[10] = z[3] * T(3) + z[1] * T(4);
z[11] = z[6] + T(-2) + -z[5];
z[11] = z[11] * T(2) + -z[10];
z[11] = z[0] * z[11];
z[12] = z[4] * T(3);
z[13] = z[6] + T(-1);
z[14] = z[9] + -z[13];
z[14] = z[14] * T(2) + -z[2] + -z[12];
z[14] = z[8] * z[14];
z[15] = z[1] + -z[9];
z[11] = z[11] + z[14] + z[15] * T(-2) + -z[6] + -z[7];
z[10] = z[10] + -z[12];
z[12] = z[7] + T(-1) + -z[6];
z[12] = z[10] + z[12] * T(2) + z[0] * T(18);
z[12] = z[4] * z[12];
z[13] = z[7] + z[13] + z[5] * T(-2);
z[10] = z[13] * T(2) + z[0] * T(6) + -z[10];
z[10] = z[2] * z[10];
return z[10] + z[12] + z[11] * T(4);
}

template<typename T> std::vector<std::vector<T>> BoxBox_202nn_zz__zz___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(3, std::vector<T>(11, T(0)));
ibp_vectors[1][0] = BoxBox_202nn_zz__zz__vec2_component1(invariants, propagators);
ibp_vectors[1][1] = BoxBox_202nn_zz__zz__vec2_component2(invariants, propagators);
ibp_vectors[1][9] = BoxBox_202nn_zz__zz__vec2_component10(invariants, propagators);
ibp_vectors[1].back() = BoxBox_202nn_zz__zz__vec2_divergence(invariants, propagators);
ibp_vectors[2][0] = BoxBox_202nn_zz__zz__vec3_component1(invariants, propagators);
ibp_vectors[2][1] = BoxBox_202nn_zz__zz__vec3_component2(invariants, propagators);
ibp_vectors[2][9] = BoxBox_202nn_zz__zz__vec3_component10(invariants, propagators);
ibp_vectors[2].back() = BoxBox_202nn_zz__zz__vec3_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BoxBox_202nn_zz__zz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BoxBox_202nn_zz__zz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BoxBox_202nn_zz__zz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BoxBox_202nn_zz__zz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

