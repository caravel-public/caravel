namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template <typename T> T BoxBox_202nn_zz__mz___Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BoxBox_202nn_zz__mz___Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[0] + D[2]) / T(2);
}

template <typename T> T BoxBox_202nn_zz__zz___Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BoxBox_202nn_zz__zz___Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[0] + D[2]) / T(2);
}

template <typename T> T BoxBox_202ny_zz__zz__z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BoxBox_202ny_zz__zz__z_Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[0] + D[4]) / T(2);
}

template <typename T> T BoxBox_202ny_zz__zz__z_Master_3(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
const auto& invariants = cs.invariants;
return (-D[1] + T(2) * invariants[4] + D[0]) / T(2);
}

template <typename T> T BoxPentagon_203nn_zz__zzz___Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BoxPentagon_203nn_zz__zzz___Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[0] + D[3]) / T(2);
}

template <typename T> T BoxPentagon_203nn_zz__zzz___Master_3(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[6] + D[2]) / T(2);
}

template <typename T> T BubbleBox_002yy___mz_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BubbleBox_002yy___zz_m_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BubbleBox_002yy___zz_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BubbleBubble_000yy____m_m_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BubblePentagon_003yy___zzz_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BubblePentagon_003yy___zzz_z_z_Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[3] + D[5]) / T(2);
}

template <typename T> T BubbleTriangle_001yy___m_m_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BubbleTriangle_001yy___m_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T TriangleBox_102ny_m__zz__z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T TriangleBox_102yy_z__zz_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T TriangleBox_102yy_z__zz_z_z_Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[0] + D[5]) / T(2);
}

template <typename T> T TriangleTriangle_101ny_m__m__z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T TriangleTriangle_101yy_z__m_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T TriangleTriangle_101yy_z__m_z_z_Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
const auto& invariants = cs.invariants;
return (-D[0] + invariants[2] + D[4]) / T(2);
}

template <typename T> T TriangleTriangle_101yy_z__z_m_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T TriangleTriangle_101yy_z__z_z_z_Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

/* Non-planar */

template <typename T> T BoxBox_111nn_z_z_m___Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}
 
template <typename T> T BoxPentagon_112nn_z_z_zz___Master_1(const TopologyCoordinates<T>& cs)
{
return T(1);
}

template <typename T> T BoxPentagon_112nn_z_z_zz___Master_2(const TopologyCoordinates<T>& cs)
{
const auto& D = cs.propagators;
return (-D[0] + D[2]) / T(2);
}


}}}}
