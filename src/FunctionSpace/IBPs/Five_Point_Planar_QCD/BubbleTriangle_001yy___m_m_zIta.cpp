#include "FunctionSpace/IBPhelper.h"

#include "BubbleTriangle_001yy___m_m_zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,15> BubbleTriangle_001yy___m_m_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,15> BubbleTriangle_001yy___m_m_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,15> BubbleTriangle_001yy___m_m_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,15> BubbleTriangle_001yy___m_m_zSurfaceIta<F32>();

#endif

}}}}

