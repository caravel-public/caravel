#include "FunctionSpace/IBPhelper.h"

#include "BubbleBubble_000yy____m_mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,5> BubbleBubble_000yy____m_mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,5> BubbleBubble_000yy____m_mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,5> BubbleBubble_000yy____m_mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,5> BubbleBubble_000yy____m_mSurfaceIta<F32>();

#endif

}}}}

