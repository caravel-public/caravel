/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#ifndef BoxBox_111ny_z_z_z__zVectors_H_INC

#define BoxBox_111ny_z_z_z__zVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BoxBox_111ny_z_z_z__zvec2_component1(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = invariants[1];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = z[6] + -z[8];
z[9] = z[5] * z[9];
z[10] = z[2] + T(-1) + -z[1] + -z[5];
z[10] = z[3] + z[10] * T(2) + -z[4];
z[10] = z[0] * z[10];
z[9] = z[9] + z[10];
z[10] = z[7] + -z[6];
z[11] = T(2) + -z[10];
z[11] = z[3] * z[11];
z[10] = z[4] * z[10];
return z[10] + z[11] + z[9] * T(2);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec2_component2(const T* invariants, const T* D)
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = D[3];
z[4] = D[7];
z[5] = D[8];
z[6] = D[4];
z[7] = D[5];
z[8] = z[3] + -z[6];
z[9] = z[5] + -z[4];
z[8] = z[8] * z[9];
z[9] = z[2] + T(1);
z[10] = z[7] + -z[4];
z[10] = -(z[9] * z[10]);
z[9] = z[1] + -z[0] + -z[9];
z[9] = z[0] * z[9];
return z[8] + z[10] * T(2) + z[9] * T(4);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec2_component3(const T* invariants, const T* D)
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = D[7];
z[3] = D[8];
z[4] = invariants[1];
z[5] = D[5];
z[6] = z[4] + T(1);
z[7] = z[1] + -z[0] + -z[6];
z[7] = z[3] + z[7] * T(2) + -z[2];
z[7] = z[1] * z[7];
z[6] = z[5] * z[6];
z[6] = z[6] + z[7];
return z[6] * T(2);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec2_component10(const T* invariants, const T* D)
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[7];
z[5] = D[8];
z[6] = invariants[1];
z[7] = z[0] + z[6] + T(1) + -z[1];
z[7] = z[3] + z[4] + z[7] * T(4) + -z[2] + -z[5];
return z[7] * T(4);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec2_divergence(const T* invariants, const T* D)
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[7];
z[5] = D[8];
z[6] = invariants[1];
z[7] = z[1] + -z[0];
z[7] = z[2] + z[5] + T(-3) + z[6] * T(-3) + z[7] * T(4) + -z[3] + -z[4];
return z[7] * T(4);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec3_component1(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = invariants[1];
z[5] = D[5];
z[6] = D[6];
z[7] = D[7];
z[8] = z[2] + -z[3];
z[9] = z[0] * T(2);
z[10] = z[4] + -z[1];
z[10] = z[10] * T(2) + -z[8] + -z[9];
z[9] = z[9] * z[10];
z[10] = z[6] + -z[5];
z[8] = z[8] * z[10];
z[10] = z[7] + -z[5];
z[10] = z[4] * z[10];
return z[8] + z[9] + z[10] * T(2);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec3_component2(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = invariants[1];
z[4] = D[3];
z[5] = D[7];
z[6] = D[8];
z[7] = D[4];
z[8] = D[5];
z[9] = T(-1) + -z[3];
z[10] = z[5] + -z[8];
z[9] = z[9] * z[10];
z[10] = z[5] + -z[6];
z[11] = -(z[7] * z[10]);
z[10] = z[10] + T(2);
z[10] = z[4] * z[10];
z[12] = z[3] + -z[0] + -z[2];
z[12] = z[1] * z[12];
return z[10] + z[11] + z[9] * T(2) + z[12] * T(4);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec3_component3(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[5];
z[3] = D[6];
z[4] = invariants[1];
z[5] = D[3];
z[6] = D[4];
z[7] = D[7];
z[8] = z[2] + -z[3];
z[9] = z[1] * T(2);
z[10] = z[4] + -z[0];
z[10] = z[10] * T(2) + -z[8] + -z[9];
z[9] = z[9] * z[10];
z[10] = z[6] + -z[5];
z[8] = z[8] * z[10];
z[10] = z[7] + -z[5];
z[10] = z[4] * z[10];
return z[8] + z[9] + z[10] * T(2);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec3_component10(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = invariants[1];
z[7] = z[0] + z[1] + -z[6];
z[7] = z[2] + z[4] + z[7] * T(4) + -z[3] + -z[5];
return z[7] * T(4);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec3_divergence(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = invariants[1];
z[7] = -z[0] + -z[1];
z[7] = z[3] + z[5] + z[6] * T(3) + z[7] * T(4) + -z[2] + -z[4];
return z[7] * T(4);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec4_component1(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec4_component2(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec4_component3(const T* invariants, const T* D)
{
T z[20];
z[0] = D[0];
z[1] = D[2];
z[2] = D[7];
z[3] = D[8];
z[4] = invariants[1];
z[5] = D[5];
z[6] = D[1];
z[7] = D[6];
z[8] = D[3];
z[9] = D[4];
z[10] = z[1] * T(2);
z[11] = z[8] + z[10] + -z[9];
z[12] = z[6] * z[11];
z[13] = z[2] + z[5] + -z[8];
z[13] = z[4] * z[13];
z[13] = z[13] + -z[8] + -z[12];
z[14] = z[6] + z[9] + T(2);
z[15] = z[1] * T(4);
z[16] = z[8] + z[15];
z[14] = z[14] * T(2) + -z[16];
z[14] = z[2] * z[14];
z[17] = T(4) + -z[16];
z[17] = z[5] * z[17];
z[18] = z[7] * z[16];
z[19] = z[16] + z[9] * T(-2);
z[19] = z[3] * z[19];
z[13] = z[14] + z[17] + z[18] + z[19] + z[13] * T(4);
z[13] = z[4] * z[13];
z[11] = -(z[11] * z[15]);
z[14] = z[12] + z[16];
z[14] = z[7] * z[14];
z[16] = T(-1) + -z[4];
z[15] = z[5] + z[15];
z[15] = z[15] * z[16];
z[16] = z[2] + -z[3];
z[16] = z[1] * z[16];
z[15] = z[15] + z[16];
z[15] = z[0] * z[15];
z[12] = z[9] + z[1] * T(-6) + z[8] * T(-2) + -z[12];
z[12] = z[5] * z[12];
z[10] = z[2] * z[10];
return z[10] + z[11] + z[12] + z[13] + z[14] + z[15] * T(2);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec4_component10(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = D[7];
z[2] = D[8];
z[3] = invariants[1];
z[4] = D[1];
z[5] = D[5];
z[6] = D[6];
z[7] = D[2];
z[8] = D[3];
z[9] = D[4];
z[10] = z[6] + -z[5];
z[11] = z[2] + -z[1];
z[12] = z[0] + z[4] + -z[10] + -z[11];
z[12] = z[3] * z[12];
z[12] = z[12] + -z[6];
z[10] = -(z[4] * z[10]);
z[11] = z[11] + T(2);
z[11] = z[0] * z[11];
z[10] = z[8] + z[10] + z[11] + z[12] * T(2) + z[5] * T(3) + z[7] * T(4) + -z[1] + -z[9];
return z[10] * T(4);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec4_divergence(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = D[7];
z[2] = D[8];
z[3] = invariants[1];
z[4] = D[1];
z[5] = D[5];
z[6] = D[6];
z[7] = D[2];
z[8] = D[3];
z[9] = D[4];
z[10] = z[6] + -z[5];
z[11] = z[2] + -z[1];
z[12] = z[10] + z[11] + -z[0] + -z[4];
z[12] = z[3] * z[12];
z[12] = z[6] + z[12];
z[10] = z[4] * z[10];
z[11] = T(-2) + -z[11];
z[11] = z[0] * z[11];
z[10] = z[1] + z[9] + z[10] + z[11] + z[7] * T(-4) + z[5] * T(-3) + z[12] * T(2) + -z[8];
return z[10] * T(4);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec8_component1(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[5];
z[2] = invariants[1];
z[3] = D[6];
z[4] = D[1];
z[5] = D[3];
z[6] = D[4];
z[7] = z[6] + -z[5];
z[8] = z[1] + -z[3];
z[7] = z[7] * z[8];
z[9] = z[2] * z[8];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[4] * z[9];
z[8] = z[8] + z[9];
return z[7] + z[8] * T(2);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec8_component2(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = D[7];
z[4] = D[8];
z[5] = D[2];
z[6] = D[3];
z[7] = D[5];
z[8] = -z[0] + -z[1];
z[9] = z[1] * T(2);
z[10] = z[3] + z[9] + -z[4];
z[8] = z[8] * z[10];
z[11] = z[3] + -z[6] + -z[7];
z[8] = z[8] + -z[11];
z[8] = z[2] * z[8];
z[9] = -(z[5] * z[9]);
z[10] = -(z[0] * z[10]);
z[8] = z[8] + z[9] + z[10] + -z[11];
return z[8] * T(2);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec8_component3(const T* invariants, const T* D)
{
T z[13];
z[0] = D[1];
z[1] = D[3];
z[2] = invariants[1];
z[3] = D[4];
z[4] = D[2];
z[5] = D[5];
z[6] = D[6];
z[7] = D[7];
z[8] = D[8];
z[9] = z[5] + -z[6];
z[10] = z[1] + -z[3];
z[9] = z[9] * z[10];
z[11] = z[7] + -z[8];
z[11] = z[10] * z[11];
z[10] = z[10] * T(2);
z[12] = z[0] * z[10];
z[11] = z[11] + z[12] + -z[9];
z[11] = z[2] * z[11];
z[10] = z[4] * z[10];
return z[10] + z[11] + -z[9];
}

template<typename T> T BoxBox_111ny_z_z_z__zvec8_component10(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = D[8];
z[10] = z[6] + z[0] * T(-2) + -z[7];
z[11] = z[8] + z[2] * T(4) + -z[9] + -z[10];
z[11] = z[1] * z[11];
z[10] = z[5] + z[11] + z[3] * T(2) + -z[4] + -z[10];
return z[10] * T(2);
}

template<typename T> T BoxBox_111ny_z_z_z__zvec8_divergence(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = invariants[1];
z[2] = D[1];
z[3] = D[2];
z[4] = D[3];
z[5] = D[4];
z[6] = D[5];
z[7] = D[6];
z[8] = D[7];
z[9] = D[8];
z[10] = z[6] + z[0] * T(-2) + -z[7];
z[11] = z[9] + z[10] + z[2] * T(-4) + -z[8];
z[11] = z[1] * z[11];
z[10] = z[4] + z[10] + z[11] + z[3] * T(-2) + -z[5];
return z[10] * T(2);
}

template<typename T> std::vector<std::vector<T>> BoxBox_111ny_z_z_z__z_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(8, std::vector<T>(11, T(0)));
ibp_vectors[1][0] = BoxBox_111ny_z_z_z__zvec2_component1(invariants, propagators);
ibp_vectors[1][1] = BoxBox_111ny_z_z_z__zvec2_component2(invariants, propagators);
ibp_vectors[1][2] = BoxBox_111ny_z_z_z__zvec2_component3(invariants, propagators);
ibp_vectors[1][9] = BoxBox_111ny_z_z_z__zvec2_component10(invariants, propagators);
ibp_vectors[1].back() = BoxBox_111ny_z_z_z__zvec2_divergence(invariants, propagators);
ibp_vectors[2][0] = BoxBox_111ny_z_z_z__zvec3_component1(invariants, propagators);
ibp_vectors[2][1] = BoxBox_111ny_z_z_z__zvec3_component2(invariants, propagators);
ibp_vectors[2][2] = BoxBox_111ny_z_z_z__zvec3_component3(invariants, propagators);
ibp_vectors[2][9] = BoxBox_111ny_z_z_z__zvec3_component10(invariants, propagators);
ibp_vectors[2].back() = BoxBox_111ny_z_z_z__zvec3_divergence(invariants, propagators);
ibp_vectors[3][2] = BoxBox_111ny_z_z_z__zvec4_component3(invariants, propagators);
ibp_vectors[3][9] = BoxBox_111ny_z_z_z__zvec4_component10(invariants, propagators);
ibp_vectors[3].back() = BoxBox_111ny_z_z_z__zvec4_divergence(invariants, propagators);
ibp_vectors[7][0] = BoxBox_111ny_z_z_z__zvec8_component1(invariants, propagators);
ibp_vectors[7][1] = BoxBox_111ny_z_z_z__zvec8_component2(invariants, propagators);
ibp_vectors[7][2] = BoxBox_111ny_z_z_z__zvec8_component3(invariants, propagators);
ibp_vectors[7][9] = BoxBox_111ny_z_z_z__zvec8_component10(invariants, propagators);
ibp_vectors[7].back() = BoxBox_111ny_z_z_z__zvec8_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BoxBox_111ny_z_z_z__z_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BoxBox_111ny_z_z_z__z_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BoxBox_111ny_z_z_z__z_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BoxBox_111ny_z_z_z__z_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

