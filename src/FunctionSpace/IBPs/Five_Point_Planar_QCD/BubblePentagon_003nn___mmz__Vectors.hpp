#ifndef BubblePentagon_003nn___mmz__Vectors_H_INC

#define BubblePentagon_003nn___mmz__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BubblePentagon_003nn___mmz__vec1_component1(const T* invariants, const T* D)
{
T z[5];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = z[0] * T(2);
z[4] = z[2] + -z[1] + -z[3];
return z[3] * z[4];
}

template<typename T> T BubblePentagon_003nn___mmz__vec1_component2(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = D[5];
z[4] = invariants[1];
z[5] = D[3];
z[6] = z[2] + -z[5];
z[6] = z[1] * z[6];
z[7] = z[3] + z[1] * T(-4) + -z[2] + -z[4];
z[7] = z[0] * z[7];
return z[5] + z[6] + z[7];
}

template<typename T> T BubblePentagon_003nn___mmz__vec1_component6(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = z[1] + z[0] * T(4) + -z[2];
return z[3] * T(2);
}

template<typename T> T BubblePentagon_003nn___mmz__vec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = z[2] + z[0] * T(-4) + -z[1];
return z[3] * T(3);
}

template<typename T> T BubblePentagon_003nn___mmz__vec2_component1(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = D[5];
z[4] = invariants[1];
z[5] = D[3];
z[6] = z[2] + -z[5];
z[6] = z[1] * z[6];
z[7] = z[3] + z[1] * T(-4) + -z[2] + -z[4];
z[7] = z[0] * z[7];
return z[5] + z[6] + z[7];
}

template<typename T> T BubblePentagon_003nn___mmz__vec2_component2(const T* invariants, const T* D)
{
T z[7];
z[0] = D[1];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = D[3];
z[5] = z[0] * T(2);
z[6] = z[2] + -z[1] + -z[3] + -z[5];
z[5] = z[5] * z[6];
z[6] = T(-2) + -z[3];
z[6] = z[4] * z[6];
return z[5] + z[6];
}

template<typename T> T BubblePentagon_003nn___mmz__vec2_component6(const T* invariants, const T* D)
{
T z[5];
z[0] = D[1];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = z[1] + z[3] + z[0] * T(4) + -z[2];
return z[4] * T(2);
}

template<typename T> T BubblePentagon_003nn___mmz__vec2_divergence(const T* invariants, const T* D)
{
T z[5];
z[0] = D[1];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = z[2] + z[0] * T(-4) + -z[1] + -z[3];
return z[4] * T(3);
}

template<typename T> T BubblePentagon_003nn___mmz__vec3_component1(const T* invariants, const T* D)
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = z[2] + T(-2) + -z[1];
return z[0] * z[3] * T(2);
}

template<typename T> T BubblePentagon_003nn___mmz__vec3_component2(const T* invariants, const T* D)
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = D[5];
z[3] = invariants[1];
z[4] = D[1];
z[5] = D[3];
z[6] = z[1] + -z[5];
z[6] = z[4] * z[6];
z[7] = z[2] + z[3] + T(4) + -z[1];
z[7] = z[0] * z[7];
return z[5] + z[6] + z[7];
}

template<typename T> T BubblePentagon_003nn___mmz__vec3_component6(const T* invariants, const T* D)
{
T z[3];
z[0] = D[3];
z[1] = D[4];
z[2] = z[1] + -z[0];
z[2] = z[2] * T(-3) + T(4);
return z[2] * T(2);
}

template<typename T> T BubblePentagon_003nn___mmz__vec3_divergence(const T* invariants, const T* D)
{
T z[3];
z[0] = D[3];
z[1] = D[4];
z[2] = z[1] + -z[0];
return z[2] * T(3);
}

template<typename T> std::vector<std::vector<T>> BubblePentagon_003nn___mmz___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(3, std::vector<T>(7, T(0)));
ibp_vectors[0][0] = BubblePentagon_003nn___mmz__vec1_component1(invariants, propagators);
ibp_vectors[0][1] = BubblePentagon_003nn___mmz__vec1_component2(invariants, propagators);
ibp_vectors[0][5] = BubblePentagon_003nn___mmz__vec1_component6(invariants, propagators);
ibp_vectors[0].back() = BubblePentagon_003nn___mmz__vec1_divergence(invariants, propagators);
ibp_vectors[1][0] = BubblePentagon_003nn___mmz__vec2_component1(invariants, propagators);
ibp_vectors[1][1] = BubblePentagon_003nn___mmz__vec2_component2(invariants, propagators);
ibp_vectors[1][5] = BubblePentagon_003nn___mmz__vec2_component6(invariants, propagators);
ibp_vectors[1].back() = BubblePentagon_003nn___mmz__vec2_divergence(invariants, propagators);
ibp_vectors[2][0] = BubblePentagon_003nn___mmz__vec3_component1(invariants, propagators);
ibp_vectors[2][1] = BubblePentagon_003nn___mmz__vec3_component2(invariants, propagators);
ibp_vectors[2][5] = BubblePentagon_003nn___mmz__vec3_component6(invariants, propagators);
ibp_vectors[2].back() = BubblePentagon_003nn___mmz__vec3_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BubblePentagon_003nn___mmz___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BubblePentagon_003nn___mmz___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BubblePentagon_003nn___mmz___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BubblePentagon_003nn___mmz___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

