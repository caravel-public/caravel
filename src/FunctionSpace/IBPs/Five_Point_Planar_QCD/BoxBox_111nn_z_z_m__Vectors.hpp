/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms

  */

#ifndef BoxBox_111nn_z_z_m__Vectors_H_INC

#define BoxBox_111nn_z_z_m__Vectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T BoxBox_111nn_z_z_m__vec2_component1(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[5];
z[6] = D[6];
z[7] = z[2] + -z[1];
z[8] = z[4] + z[5] + z[7] + T(-4) + z[0] * T(-4) + -z[3] + -z[6];
z[8] = z[0] * z[8];
z[8] = z[2] + z[5] + z[8];
z[9] = z[4] * z[7];
z[7] = T(-2) + -z[7];
z[7] = z[3] * z[7];
return z[7] + z[9] + z[8] * T(2);
}

template<typename T> T BoxBox_111nn_z_z_m__vec2_component8(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[5];
z[6] = D[6];
z[7] = z[2] + z[5] + -z[1] + -z[6];
z[8] = z[3] + T(2) + -z[4];
z[7] = z[7] * T(-3) + z[8] * T(2) + z[0] * T(8);
return z[7] * T(4);
}

template<typename T> T BoxBox_111nn_z_z_m__vec2_divergence(const T* invariants, const T* D)
{
T z[9];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[5];
z[6] = D[6];
z[7] = z[2] + z[5] + -z[1] + -z[6];
z[8] = z[4] + T(-2) + -z[3];
z[7] = z[0] * T(-8) + z[8] * T(2) + z[7] * T(3);
return z[7] * T(2);
}

template<typename T> std::vector<std::vector<T>> BoxBox_111nn_z_z_m___vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(2, std::vector<T>(9, T(0)));
ibp_vectors[1][0] = BoxBox_111nn_z_z_m__vec2_component1(invariants, propagators);
ibp_vectors[1][7] = BoxBox_111nn_z_z_m__vec2_component8(invariants, propagators);
ibp_vectors[1].back() = BoxBox_111nn_z_z_m__vec2_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> BoxBox_111nn_z_z_m___vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> BoxBox_111nn_z_z_m___vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> BoxBox_111nn_z_z_m___vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> BoxBox_111nn_z_z_m___vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

