/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][9];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][9];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][9];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[1][9];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[1][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][9];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[2][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[2][9];
z[2] = ibp_vectors[2].back();
z[3] = ibp_vectors[2][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[2][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[3][9];
z[1] = ibp_vectors[3].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[3][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[3][9];
z[2] = ibp_vectors[3].back();
z[3] = ibp_vectors[3][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[3][9];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleHeptagon_005nn___zzzzz__SurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[16][9];
z[1] = ibp_vectors[16].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[16][9];
zd[1] = -(z[0] / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,15> BubbleHeptagon_005nn___zzzzz__SurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,15> BubbleHeptagon_005nn___zzzzz__SurfaceItaArray;
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[0] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta1, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[1] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta2, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[2] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta3, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[3] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta4, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[4] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta5, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[5] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta6, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[6] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta7, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[7] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta8, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[8] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta9, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[9] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta10, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[10] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta11, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[11] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta12, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[12] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta13, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[13] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta14, -1};
BubbleHeptagon_005nn___zzzzz__SurfaceItaArray[14] = {BubbleHeptagon_005nn___zzzzz__SurfaceIta15, -1};
return BubbleHeptagon_005nn___zzzzz__SurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,15> BubbleHeptagon_005nn___zzzzz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,15> BubbleHeptagon_005nn___zzzzz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,15> BubbleHeptagon_005nn___zzzzz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,15> BubbleHeptagon_005nn___zzzzz__SurfaceIta<F32>();

#endif

}}}}

