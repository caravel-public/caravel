/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][10];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][1];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta17(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta18(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta19(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta20(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][3];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta21(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta22(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta23(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta24(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta25(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta26(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta27(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta28(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta29(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta30(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta31(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta32(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta33(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta34(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta35(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta36(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta37(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta38(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta39(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta40(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta41(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta42(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta43(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][3];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[3];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta44(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][3];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta45(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][3];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[3];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta46(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[0][10];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta47(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta48(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta49(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta50(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta51(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = ibp_vectors[0][10];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][4];
z[7] = ibp_vectors[0][3];
z[8] = ibp_vectors[0][1];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + (z[4] * T(7)) / T(2);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
zd[0] = z[10] + z[11];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta52(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][1];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta53(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta54(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = ibp_vectors[0][10];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][4];
z[7] = ibp_vectors[0][3];
z[8] = ibp_vectors[0][2];
z[9] = ibp_vectors[0][0];
z[10] = z[5] + (z[4] * T(7)) / T(2);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
zd[0] = z[10] + z[11];
}
{
T z[6];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta55(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta56(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta57(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][0];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta58(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta59(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta60(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta61(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta62(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta63(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][3];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta64(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = ibp_vectors[0][10];
z[5] = ibp_vectors[0].back();
z[6] = ibp_vectors[0][4];
z[7] = ibp_vectors[0][3];
z[8] = ibp_vectors[0][2];
z[9] = ibp_vectors[0][1];
z[10] = z[5] + (z[4] * T(7)) / T(2);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
zd[0] = z[10] + z[11];
}
{
T z[6];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta65(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][2];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta66(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta67(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][1];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta68(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta69(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta70(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta71(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta72(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta73(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][3];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta74(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = z[2] * z[7];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[2] * z[9];
z[9] = z[5] + z[9];
z[9] = z[0] * z[9];
z[8] = z[8] + z[9];
z[8] = z[1] * z[8];
z[9] = z[0] * z[2] * z[6];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[1] * z[8];
}
{
T z[5];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta75(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
z[4] = ibp_vectors[0].back();
z[5] = ibp_vectors[0][4];
z[6] = ibp_vectors[0][3];
z[7] = ibp_vectors[0][2];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[0][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta76(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][4];
z[5] = ibp_vectors[0][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[0][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta77(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][10];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta78(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][1];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta79(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta80(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][10];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta81(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta82(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][10];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta83(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta84(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta85(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta86(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][4];
z[6] = ibp_vectors[1][1];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta87(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta88(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][4];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta89(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta90(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta91(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][3];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta92(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[1][10];
z[4] = ibp_vectors[1].back();
z[5] = ibp_vectors[1][4];
z[6] = ibp_vectors[1][2];
z[7] = ibp_vectors[1][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[1][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta93(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][3];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta94(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[1][10];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][4];
z[5] = ibp_vectors[1][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[1][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta95(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[5][10];
z[1] = ibp_vectors[5].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[5][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta96(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta97(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta98(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta99(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta100(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta101(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][1];
z[5] = ibp_vectors[5][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[1];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta102(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta103(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta104(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][1];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta105(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][2];
z[5] = ibp_vectors[5][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta106(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][1];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta107(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta108(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta109(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta110(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta111(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta112(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][3];
z[6] = ibp_vectors[5][1];
z[7] = ibp_vectors[5][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta113(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][4];
z[6] = ibp_vectors[5][1];
z[7] = ibp_vectors[5][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta114(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][4];
z[6] = ibp_vectors[5][2];
z[7] = ibp_vectors[5][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta115(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta116(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta117(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][1];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta118(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][4];
z[6] = ibp_vectors[5][2];
z[7] = ibp_vectors[5][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta119(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta120(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta121(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta122(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[5][10];
z[2] = ibp_vectors[5].back();
z[3] = ibp_vectors[5][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta123(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[5][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta124(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][4];
z[6] = ibp_vectors[5][3];
z[7] = ibp_vectors[5][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta125(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta126(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][3];
z[6] = ibp_vectors[5][1];
z[7] = ibp_vectors[5][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta127(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[13];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = ibp_vectors[5][10];
z[5] = ibp_vectors[5].back();
z[6] = ibp_vectors[5][4];
z[7] = ibp_vectors[5][3];
z[8] = ibp_vectors[5][1];
z[9] = ibp_vectors[5][0];
z[10] = z[5] + (z[4] * T(7)) / T(2);
z[10] = z[3] * z[10];
z[10] = z[6] + z[10];
z[10] = z[1] * z[10];
z[11] = z[3] * z[8];
z[10] = z[10] + z[11];
z[10] = z[2] * z[10];
z[11] = z[1] * z[3];
z[12] = z[7] * z[11];
z[10] = z[10] + z[12];
z[10] = z[0] * z[10];
z[11] = z[2] * z[9] * z[11];
zd[0] = z[10] + z[11];
}
{
T z[6];
z[0] = D[0];
z[1] = D[1];
z[2] = D[3];
z[3] = D[4];
z[4] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3] * z[4]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta128(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][4];
z[6] = ibp_vectors[5][1];
z[7] = ibp_vectors[5][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[1];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta129(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][4];
z[6] = ibp_vectors[5][2];
z[7] = ibp_vectors[5][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta130(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta131(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][3];
z[5] = ibp_vectors[5][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[3];
z[2] = ibp_vectors[5][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta132(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][4];
z[6] = ibp_vectors[5][3];
z[7] = ibp_vectors[5][1];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta133(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][1];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta134(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
z[4] = ibp_vectors[5].back();
z[5] = ibp_vectors[5][4];
z[6] = ibp_vectors[5][2];
z[7] = ibp_vectors[5][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[5][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta135(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][1];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[1];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta136(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta137(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
z[3] = ibp_vectors[5].back();
z[4] = ibp_vectors[5][4];
z[5] = ibp_vectors[5][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[5][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta138(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[6][10];
z[1] = ibp_vectors[6].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[6][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta139(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[6][10];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta140(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[6][10];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta141(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[6][10];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta142(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[6][10];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[6][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta143(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][2];
z[5] = ibp_vectors[6][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta144(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta145(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[6][10];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[6][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta146(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][2];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta147(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[6][10];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[6][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta148(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][3];
z[5] = ibp_vectors[6][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[6][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta149(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta150(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[6][10];
z[4] = ibp_vectors[6].back();
z[5] = ibp_vectors[6][4];
z[6] = ibp_vectors[6][2];
z[7] = ibp_vectors[6][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta151(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta152(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta153(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[1] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
zd[1] = -((z[0] * prod_pow(z[1], 2) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta154(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[4];
z[1] = ibp_vectors[6][10];
z[2] = ibp_vectors[6].back();
z[3] = ibp_vectors[6][4];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(3);
zd[0] = prod_pow(z[0], 2) * z[4];
}
{
T z[3];
z[0] = D[4];
z[1] = ibp_vectors[6][10];
zd[1] = -((prod_pow(z[0], 3) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta155(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][3];
z[5] = ibp_vectors[6][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[3];
z[2] = ibp_vectors[6][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta156(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[6][10];
z[4] = ibp_vectors[6].back();
z[5] = ibp_vectors[6][4];
z[6] = ibp_vectors[6][3];
z[7] = ibp_vectors[6][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[0] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[3];
z[2] = D[4];
z[3] = ibp_vectors[6][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta157(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][0];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta158(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[6][10];
z[4] = ibp_vectors[6].back();
z[5] = ibp_vectors[6][4];
z[6] = ibp_vectors[6][2];
z[7] = ibp_vectors[6][0];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta159(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][0];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta160(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[6][10];
z[4] = ibp_vectors[6].back();
z[5] = ibp_vectors[6][4];
z[6] = ibp_vectors[6][2];
z[7] = ibp_vectors[6][1];
z[8] = z[0] * z[6];
z[9] = z[4] + (z[3] * T(7)) / T(2);
z[9] = z[0] * z[9];
z[9] = z[7] + z[9];
z[9] = z[1] * z[9];
z[8] = z[8] + z[9];
z[8] = z[2] * z[8];
z[9] = z[0] * z[1] * z[5];
z[8] = z[8] + z[9] * T(2);
zd[0] = z[2] * z[8];
}
{
T z[5];
z[0] = D[1];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[6][10];
zd[1] = -((z[0] * z[1] * prod_pow(z[2], 2) * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta161(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][2];
z[6] = prod_pow(z[1], 2) * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[7] + z[4] * T(2);
z[7] = z[0] * z[1] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = z[0] * z[1];
zd[1] = -((z[2] * prod_pow(z[3], 2)) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta162(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
z[3] = ibp_vectors[6].back();
z[4] = ibp_vectors[6][4];
z[5] = ibp_vectors[6][2];
z[6] = z[0] * z[4];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[0] * z[7];
z[7] = z[5] + z[7];
z[7] = z[1] * z[7];
z[6] = z[7] + z[6] * T(3);
zd[0] = prod_pow(z[1], 2) * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[6][10];
zd[1] = -((z[0] * prod_pow(z[1], 3) * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta163(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[9][10];
z[1] = ibp_vectors[9].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[9][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta164(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[9][10];
z[2] = ibp_vectors[9].back();
z[3] = ibp_vectors[9][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[9][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta165(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[9][10];
z[2] = ibp_vectors[9].back();
z[3] = ibp_vectors[9][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[9][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta166(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[9][10];
z[2] = ibp_vectors[9].back();
z[3] = ibp_vectors[9][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[9][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta167(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[9][10];
z[3] = ibp_vectors[9].back();
z[4] = ibp_vectors[9][2];
z[5] = ibp_vectors[9][0];
z[6] = z[3] + (z[2] * T(7)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[9][10];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta168(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[9][10];
z[2] = ibp_vectors[9].back();
z[3] = ibp_vectors[9][2];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[9][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta169(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[9][10];
z[3] = ibp_vectors[9].back();
z[4] = ibp_vectors[9][4];
z[5] = ibp_vectors[9][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[9][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta170(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[10];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[9][10];
z[4] = ibp_vectors[9].back();
z[5] = ibp_vectors[9][4];
z[6] = ibp_vectors[9][2];
z[7] = ibp_vectors[9][0];
z[8] = z[4] + (z[3] * T(7)) / T(2);
z[8] = z[2] * z[8];
z[8] = z[5] + z[8];
z[8] = z[1] * z[8];
z[9] = z[2] * z[6];
z[8] = z[8] + z[9];
z[8] = z[0] * z[8];
z[9] = z[1] * z[2] * z[7];
zd[0] = z[8] + z[9];
}
{
T z[5];
z[0] = D[0];
z[1] = D[2];
z[2] = D[4];
z[3] = ibp_vectors[9][10];
zd[1] = -((z[0] * z[1] * z[2] * z[3]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta171(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[9][10];
z[3] = ibp_vectors[9].back();
z[4] = ibp_vectors[9][4];
z[5] = ibp_vectors[9][2];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[2];
z[1] = D[4];
z[2] = ibp_vectors[9][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta172(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[12][10];
z[1] = ibp_vectors[12].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[12][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta173(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[12][10];
z[2] = ibp_vectors[12].back();
z[3] = ibp_vectors[12][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[12][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta174(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[13][10];
z[2] = ibp_vectors[13].back();
z[3] = ibp_vectors[13][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[13][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta175(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[14][10];
z[1] = ibp_vectors[14].back();
zd[0] = z[1] + (z[0] * T(7)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[14][10];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta176(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[14][10];
z[2] = ibp_vectors[14].back();
z[3] = ibp_vectors[14][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[14][10];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta177(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[14][10];
z[2] = ibp_vectors[14].back();
z[3] = ibp_vectors[14][0];
z[4] = z[2] + (z[1] * T(7)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[14][10];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> TriangleBox_102yy_z__zz_z_zSurfaceIta178(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[14][10];
z[3] = ibp_vectors[14].back();
z[4] = ibp_vectors[14][4];
z[5] = ibp_vectors[14][0];
z[6] = z[1] * z[5];
z[7] = z[3] + (z[2] * T(7)) / T(2);
z[7] = z[1] * z[7];
z[7] = z[4] + z[7];
z[7] = z[0] * z[7];
z[6] = z[7] + z[6] * T(2);
zd[0] = z[0] * z[6];
}
{
T z[4];
z[0] = D[0];
z[1] = D[4];
z[2] = ibp_vectors[14][10];
zd[1] = -((prod_pow(z[0], 2) * z[1] * z[2]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,178> TriangleBox_102yy_z__zz_z_zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,178> TriangleBox_102yy_z__zz_z_zSurfaceItaArray;
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[0] = {TriangleBox_102yy_z__zz_z_zSurfaceIta1, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[1] = {TriangleBox_102yy_z__zz_z_zSurfaceIta2, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[2] = {TriangleBox_102yy_z__zz_z_zSurfaceIta3, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[3] = {TriangleBox_102yy_z__zz_z_zSurfaceIta4, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[4] = {TriangleBox_102yy_z__zz_z_zSurfaceIta5, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[5] = {TriangleBox_102yy_z__zz_z_zSurfaceIta6, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[6] = {TriangleBox_102yy_z__zz_z_zSurfaceIta7, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[7] = {TriangleBox_102yy_z__zz_z_zSurfaceIta8, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[8] = {TriangleBox_102yy_z__zz_z_zSurfaceIta9, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[9] = {TriangleBox_102yy_z__zz_z_zSurfaceIta10, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[10] = {TriangleBox_102yy_z__zz_z_zSurfaceIta11, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[11] = {TriangleBox_102yy_z__zz_z_zSurfaceIta12, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[12] = {TriangleBox_102yy_z__zz_z_zSurfaceIta13, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[13] = {TriangleBox_102yy_z__zz_z_zSurfaceIta14, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[14] = {TriangleBox_102yy_z__zz_z_zSurfaceIta15, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[15] = {TriangleBox_102yy_z__zz_z_zSurfaceIta16, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[16] = {TriangleBox_102yy_z__zz_z_zSurfaceIta17, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[17] = {TriangleBox_102yy_z__zz_z_zSurfaceIta18, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[18] = {TriangleBox_102yy_z__zz_z_zSurfaceIta19, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[19] = {TriangleBox_102yy_z__zz_z_zSurfaceIta20, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[20] = {TriangleBox_102yy_z__zz_z_zSurfaceIta21, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[21] = {TriangleBox_102yy_z__zz_z_zSurfaceIta22, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[22] = {TriangleBox_102yy_z__zz_z_zSurfaceIta23, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[23] = {TriangleBox_102yy_z__zz_z_zSurfaceIta24, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[24] = {TriangleBox_102yy_z__zz_z_zSurfaceIta25, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[25] = {TriangleBox_102yy_z__zz_z_zSurfaceIta26, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[26] = {TriangleBox_102yy_z__zz_z_zSurfaceIta27, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[27] = {TriangleBox_102yy_z__zz_z_zSurfaceIta28, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[28] = {TriangleBox_102yy_z__zz_z_zSurfaceIta29, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[29] = {TriangleBox_102yy_z__zz_z_zSurfaceIta30, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[30] = {TriangleBox_102yy_z__zz_z_zSurfaceIta31, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[31] = {TriangleBox_102yy_z__zz_z_zSurfaceIta32, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[32] = {TriangleBox_102yy_z__zz_z_zSurfaceIta33, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[33] = {TriangleBox_102yy_z__zz_z_zSurfaceIta34, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[34] = {TriangleBox_102yy_z__zz_z_zSurfaceIta35, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[35] = {TriangleBox_102yy_z__zz_z_zSurfaceIta36, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[36] = {TriangleBox_102yy_z__zz_z_zSurfaceIta37, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[37] = {TriangleBox_102yy_z__zz_z_zSurfaceIta38, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[38] = {TriangleBox_102yy_z__zz_z_zSurfaceIta39, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[39] = {TriangleBox_102yy_z__zz_z_zSurfaceIta40, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[40] = {TriangleBox_102yy_z__zz_z_zSurfaceIta41, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[41] = {TriangleBox_102yy_z__zz_z_zSurfaceIta42, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[42] = {TriangleBox_102yy_z__zz_z_zSurfaceIta43, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[43] = {TriangleBox_102yy_z__zz_z_zSurfaceIta44, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[44] = {TriangleBox_102yy_z__zz_z_zSurfaceIta45, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[45] = {TriangleBox_102yy_z__zz_z_zSurfaceIta46, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[46] = {TriangleBox_102yy_z__zz_z_zSurfaceIta47, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[47] = {TriangleBox_102yy_z__zz_z_zSurfaceIta48, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[48] = {TriangleBox_102yy_z__zz_z_zSurfaceIta49, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[49] = {TriangleBox_102yy_z__zz_z_zSurfaceIta50, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[50] = {TriangleBox_102yy_z__zz_z_zSurfaceIta51, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[51] = {TriangleBox_102yy_z__zz_z_zSurfaceIta52, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[52] = {TriangleBox_102yy_z__zz_z_zSurfaceIta53, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[53] = {TriangleBox_102yy_z__zz_z_zSurfaceIta54, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[54] = {TriangleBox_102yy_z__zz_z_zSurfaceIta55, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[55] = {TriangleBox_102yy_z__zz_z_zSurfaceIta56, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[56] = {TriangleBox_102yy_z__zz_z_zSurfaceIta57, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[57] = {TriangleBox_102yy_z__zz_z_zSurfaceIta58, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[58] = {TriangleBox_102yy_z__zz_z_zSurfaceIta59, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[59] = {TriangleBox_102yy_z__zz_z_zSurfaceIta60, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[60] = {TriangleBox_102yy_z__zz_z_zSurfaceIta61, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[61] = {TriangleBox_102yy_z__zz_z_zSurfaceIta62, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[62] = {TriangleBox_102yy_z__zz_z_zSurfaceIta63, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[63] = {TriangleBox_102yy_z__zz_z_zSurfaceIta64, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[64] = {TriangleBox_102yy_z__zz_z_zSurfaceIta65, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[65] = {TriangleBox_102yy_z__zz_z_zSurfaceIta66, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[66] = {TriangleBox_102yy_z__zz_z_zSurfaceIta67, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[67] = {TriangleBox_102yy_z__zz_z_zSurfaceIta68, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[68] = {TriangleBox_102yy_z__zz_z_zSurfaceIta69, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[69] = {TriangleBox_102yy_z__zz_z_zSurfaceIta70, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[70] = {TriangleBox_102yy_z__zz_z_zSurfaceIta71, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[71] = {TriangleBox_102yy_z__zz_z_zSurfaceIta72, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[72] = {TriangleBox_102yy_z__zz_z_zSurfaceIta73, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[73] = {TriangleBox_102yy_z__zz_z_zSurfaceIta74, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[74] = {TriangleBox_102yy_z__zz_z_zSurfaceIta75, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[75] = {TriangleBox_102yy_z__zz_z_zSurfaceIta76, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[76] = {TriangleBox_102yy_z__zz_z_zSurfaceIta77, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[77] = {TriangleBox_102yy_z__zz_z_zSurfaceIta78, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[78] = {TriangleBox_102yy_z__zz_z_zSurfaceIta79, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[79] = {TriangleBox_102yy_z__zz_z_zSurfaceIta80, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[80] = {TriangleBox_102yy_z__zz_z_zSurfaceIta81, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[81] = {TriangleBox_102yy_z__zz_z_zSurfaceIta82, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[82] = {TriangleBox_102yy_z__zz_z_zSurfaceIta83, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[83] = {TriangleBox_102yy_z__zz_z_zSurfaceIta84, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[84] = {TriangleBox_102yy_z__zz_z_zSurfaceIta85, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[85] = {TriangleBox_102yy_z__zz_z_zSurfaceIta86, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[86] = {TriangleBox_102yy_z__zz_z_zSurfaceIta87, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[87] = {TriangleBox_102yy_z__zz_z_zSurfaceIta88, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[88] = {TriangleBox_102yy_z__zz_z_zSurfaceIta89, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[89] = {TriangleBox_102yy_z__zz_z_zSurfaceIta90, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[90] = {TriangleBox_102yy_z__zz_z_zSurfaceIta91, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[91] = {TriangleBox_102yy_z__zz_z_zSurfaceIta92, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[92] = {TriangleBox_102yy_z__zz_z_zSurfaceIta93, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[93] = {TriangleBox_102yy_z__zz_z_zSurfaceIta94, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[94] = {TriangleBox_102yy_z__zz_z_zSurfaceIta95, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[95] = {TriangleBox_102yy_z__zz_z_zSurfaceIta96, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[96] = {TriangleBox_102yy_z__zz_z_zSurfaceIta97, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[97] = {TriangleBox_102yy_z__zz_z_zSurfaceIta98, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[98] = {TriangleBox_102yy_z__zz_z_zSurfaceIta99, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[99] = {TriangleBox_102yy_z__zz_z_zSurfaceIta100, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[100] = {TriangleBox_102yy_z__zz_z_zSurfaceIta101, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[101] = {TriangleBox_102yy_z__zz_z_zSurfaceIta102, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[102] = {TriangleBox_102yy_z__zz_z_zSurfaceIta103, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[103] = {TriangleBox_102yy_z__zz_z_zSurfaceIta104, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[104] = {TriangleBox_102yy_z__zz_z_zSurfaceIta105, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[105] = {TriangleBox_102yy_z__zz_z_zSurfaceIta106, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[106] = {TriangleBox_102yy_z__zz_z_zSurfaceIta107, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[107] = {TriangleBox_102yy_z__zz_z_zSurfaceIta108, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[108] = {TriangleBox_102yy_z__zz_z_zSurfaceIta109, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[109] = {TriangleBox_102yy_z__zz_z_zSurfaceIta110, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[110] = {TriangleBox_102yy_z__zz_z_zSurfaceIta111, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[111] = {TriangleBox_102yy_z__zz_z_zSurfaceIta112, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[112] = {TriangleBox_102yy_z__zz_z_zSurfaceIta113, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[113] = {TriangleBox_102yy_z__zz_z_zSurfaceIta114, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[114] = {TriangleBox_102yy_z__zz_z_zSurfaceIta115, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[115] = {TriangleBox_102yy_z__zz_z_zSurfaceIta116, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[116] = {TriangleBox_102yy_z__zz_z_zSurfaceIta117, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[117] = {TriangleBox_102yy_z__zz_z_zSurfaceIta118, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[118] = {TriangleBox_102yy_z__zz_z_zSurfaceIta119, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[119] = {TriangleBox_102yy_z__zz_z_zSurfaceIta120, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[120] = {TriangleBox_102yy_z__zz_z_zSurfaceIta121, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[121] = {TriangleBox_102yy_z__zz_z_zSurfaceIta122, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[122] = {TriangleBox_102yy_z__zz_z_zSurfaceIta123, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[123] = {TriangleBox_102yy_z__zz_z_zSurfaceIta124, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[124] = {TriangleBox_102yy_z__zz_z_zSurfaceIta125, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[125] = {TriangleBox_102yy_z__zz_z_zSurfaceIta126, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[126] = {TriangleBox_102yy_z__zz_z_zSurfaceIta127, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[127] = {TriangleBox_102yy_z__zz_z_zSurfaceIta128, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[128] = {TriangleBox_102yy_z__zz_z_zSurfaceIta129, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[129] = {TriangleBox_102yy_z__zz_z_zSurfaceIta130, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[130] = {TriangleBox_102yy_z__zz_z_zSurfaceIta131, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[131] = {TriangleBox_102yy_z__zz_z_zSurfaceIta132, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[132] = {TriangleBox_102yy_z__zz_z_zSurfaceIta133, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[133] = {TriangleBox_102yy_z__zz_z_zSurfaceIta134, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[134] = {TriangleBox_102yy_z__zz_z_zSurfaceIta135, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[135] = {TriangleBox_102yy_z__zz_z_zSurfaceIta136, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[136] = {TriangleBox_102yy_z__zz_z_zSurfaceIta137, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[137] = {TriangleBox_102yy_z__zz_z_zSurfaceIta138, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[138] = {TriangleBox_102yy_z__zz_z_zSurfaceIta139, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[139] = {TriangleBox_102yy_z__zz_z_zSurfaceIta140, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[140] = {TriangleBox_102yy_z__zz_z_zSurfaceIta141, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[141] = {TriangleBox_102yy_z__zz_z_zSurfaceIta142, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[142] = {TriangleBox_102yy_z__zz_z_zSurfaceIta143, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[143] = {TriangleBox_102yy_z__zz_z_zSurfaceIta144, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[144] = {TriangleBox_102yy_z__zz_z_zSurfaceIta145, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[145] = {TriangleBox_102yy_z__zz_z_zSurfaceIta146, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[146] = {TriangleBox_102yy_z__zz_z_zSurfaceIta147, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[147] = {TriangleBox_102yy_z__zz_z_zSurfaceIta148, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[148] = {TriangleBox_102yy_z__zz_z_zSurfaceIta149, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[149] = {TriangleBox_102yy_z__zz_z_zSurfaceIta150, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[150] = {TriangleBox_102yy_z__zz_z_zSurfaceIta151, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[151] = {TriangleBox_102yy_z__zz_z_zSurfaceIta152, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[152] = {TriangleBox_102yy_z__zz_z_zSurfaceIta153, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[153] = {TriangleBox_102yy_z__zz_z_zSurfaceIta154, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[154] = {TriangleBox_102yy_z__zz_z_zSurfaceIta155, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[155] = {TriangleBox_102yy_z__zz_z_zSurfaceIta156, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[156] = {TriangleBox_102yy_z__zz_z_zSurfaceIta157, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[157] = {TriangleBox_102yy_z__zz_z_zSurfaceIta158, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[158] = {TriangleBox_102yy_z__zz_z_zSurfaceIta159, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[159] = {TriangleBox_102yy_z__zz_z_zSurfaceIta160, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[160] = {TriangleBox_102yy_z__zz_z_zSurfaceIta161, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[161] = {TriangleBox_102yy_z__zz_z_zSurfaceIta162, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[162] = {TriangleBox_102yy_z__zz_z_zSurfaceIta163, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[163] = {TriangleBox_102yy_z__zz_z_zSurfaceIta164, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[164] = {TriangleBox_102yy_z__zz_z_zSurfaceIta165, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[165] = {TriangleBox_102yy_z__zz_z_zSurfaceIta166, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[166] = {TriangleBox_102yy_z__zz_z_zSurfaceIta167, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[167] = {TriangleBox_102yy_z__zz_z_zSurfaceIta168, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[168] = {TriangleBox_102yy_z__zz_z_zSurfaceIta169, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[169] = {TriangleBox_102yy_z__zz_z_zSurfaceIta170, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[170] = {TriangleBox_102yy_z__zz_z_zSurfaceIta171, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[171] = {TriangleBox_102yy_z__zz_z_zSurfaceIta172, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[172] = {TriangleBox_102yy_z__zz_z_zSurfaceIta173, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[173] = {TriangleBox_102yy_z__zz_z_zSurfaceIta174, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[174] = {TriangleBox_102yy_z__zz_z_zSurfaceIta175, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[175] = {TriangleBox_102yy_z__zz_z_zSurfaceIta176, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[176] = {TriangleBox_102yy_z__zz_z_zSurfaceIta177, -1};
TriangleBox_102yy_z__zz_z_zSurfaceItaArray[177] = {TriangleBox_102yy_z__zz_z_zSurfaceIta178, -1};
return TriangleBox_102yy_z__zz_z_zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,178> TriangleBox_102yy_z__zz_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,178> TriangleBox_102yy_z__zz_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,178> TriangleBox_102yy_z__zz_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,178> TriangleBox_102yy_z__zz_z_zSurfaceIta<F32>();

#endif

}}}}

