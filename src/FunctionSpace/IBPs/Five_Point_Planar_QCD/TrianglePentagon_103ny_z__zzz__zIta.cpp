#include "FunctionSpace/IBPhelper.h"

#include "TrianglePentagon_103ny_z__zzz__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,94> TrianglePentagon_103ny_z__zzz__zSurfaceIta<F32>();

#endif

}}}}

