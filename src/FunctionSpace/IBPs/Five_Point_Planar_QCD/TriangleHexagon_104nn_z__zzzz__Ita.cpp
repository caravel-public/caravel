#include "FunctionSpace/IBPhelper.h"

#include "TriangleHexagon_104nn_z__zzzz__Ita.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,20> TriangleHexagon_104nn_z__zzzz__SurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,20> TriangleHexagon_104nn_z__zzzz__SurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,20> TriangleHexagon_104nn_z__zzzz__SurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,20> TriangleHexagon_104nn_z__zzzz__SurfaceIta<F32>();

#endif

}}}}

