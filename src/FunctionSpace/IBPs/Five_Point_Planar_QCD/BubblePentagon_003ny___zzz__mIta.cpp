#include "FunctionSpace/IBPhelper.h"

#include "BubblePentagon_003ny___zzz__mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,10> BubblePentagon_003ny___zzz__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,10> BubblePentagon_003ny___zzz__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,10> BubblePentagon_003ny___zzz__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,10> BubblePentagon_003ny___zzz__mSurfaceIta<F32>();

#endif

}}}}

