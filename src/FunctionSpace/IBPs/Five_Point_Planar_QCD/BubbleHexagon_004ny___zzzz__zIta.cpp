#include "FunctionSpace/IBPhelper.h"

#include "BubbleHexagon_004ny___zzzz__zIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,15> BubbleHexagon_004ny___zzzz__zSurfaceIta<F32>();

#endif

}}}}

