#include "FunctionSpace/IBPhelper.h"

#include "TriangleTriangle_101ny_z__m__mIta.hpp"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template std::array<SurfaceTermsFunctionPointer<C>,13> TriangleTriangle_101ny_z__m__mSurfaceIta<C>();

#ifdef HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CHP>,13> TriangleTriangle_101ny_z__m__mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

template std::array<SurfaceTermsFunctionPointer<CVHP>,13> TriangleTriangle_101ny_z__m__mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

template std::array<SurfaceTermsFunctionPointer<F32>,13> TriangleTriangle_101ny_z__m__mSurfaceIta<F32>();

#endif

}}}}

