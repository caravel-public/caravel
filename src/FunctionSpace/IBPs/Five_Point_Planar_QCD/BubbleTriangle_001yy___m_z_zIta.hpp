/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][5];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][5];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[4][5];
z[1] = ibp_vectors[4].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[4][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][5];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___m_z_zSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][5];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,15> BubbleTriangle_001yy___m_z_zSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,15> BubbleTriangle_001yy___m_z_zSurfaceItaArray;
BubbleTriangle_001yy___m_z_zSurfaceItaArray[0] = {BubbleTriangle_001yy___m_z_zSurfaceIta1, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[1] = {BubbleTriangle_001yy___m_z_zSurfaceIta2, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[2] = {BubbleTriangle_001yy___m_z_zSurfaceIta3, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[3] = {BubbleTriangle_001yy___m_z_zSurfaceIta4, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[4] = {BubbleTriangle_001yy___m_z_zSurfaceIta5, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[5] = {BubbleTriangle_001yy___m_z_zSurfaceIta6, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[6] = {BubbleTriangle_001yy___m_z_zSurfaceIta7, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[7] = {BubbleTriangle_001yy___m_z_zSurfaceIta8, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[8] = {BubbleTriangle_001yy___m_z_zSurfaceIta9, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[9] = {BubbleTriangle_001yy___m_z_zSurfaceIta10, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[10] = {BubbleTriangle_001yy___m_z_zSurfaceIta11, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[11] = {BubbleTriangle_001yy___m_z_zSurfaceIta12, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[12] = {BubbleTriangle_001yy___m_z_zSurfaceIta13, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[13] = {BubbleTriangle_001yy___m_z_zSurfaceIta14, -1};
BubbleTriangle_001yy___m_z_zSurfaceItaArray[14] = {BubbleTriangle_001yy___m_z_zSurfaceIta15, -1};
return BubbleTriangle_001yy___m_z_zSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,15> BubbleTriangle_001yy___m_z_zSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,15> BubbleTriangle_001yy___m_z_zSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,15> BubbleTriangle_001yy___m_z_zSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,15> BubbleTriangle_001yy___m_z_zSurfaceIta<F32>();

#endif

}}}}

