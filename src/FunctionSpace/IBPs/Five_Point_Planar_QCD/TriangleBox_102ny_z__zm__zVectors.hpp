#ifndef TriangleBox_102ny_z__zm__zVectors_H_INC

#define TriangleBox_102ny_z__zm__zVectors_H_INC

#include "misc/MiscMath.h"

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> T TriangleBox_102ny_z__zm__zvec1_component1(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = D[6];
z[6] = invariants[1];
z[7] = invariants[2];
z[8] = z[3] + -z[2];
z[9] = z[7] + T(-1);
z[10] = -z[6] + -z[9];
z[10] = z[8] + z[10] * T(2);
z[10] = z[4] * z[10];
z[9] = z[9] * T(2);
z[11] = z[6] + z[9];
z[11] = z[5] + z[11] * T(2);
z[11] = z[2] * z[11];
z[9] = -z[5] + -z[9];
z[9] = z[3] * z[9];
z[8] = z[8] + -z[1];
z[8] = z[0] * z[8];
return z[9] + z[10] + z[11] + z[8] * T(4);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec1_component2(const T* invariants, const T* D)
{
T z[13];
z[0] = D[1];
z[1] = D[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[6];
z[5] = D[7];
z[6] = invariants[1];
z[7] = invariants[2];
z[8] = D[5];
z[9] = z[3] + z[8] + z[2] * T(-2);
z[9] = z[7] * z[9];
z[10] = z[8] + -z[2];
z[10] = z[6] * z[10];
z[11] = z[2] + -z[3];
z[12] = -z[1] + -z[11];
z[12] = z[0] * z[12];
z[9] = z[9] + z[10] + z[12] * T(2);
z[10] = z[5] + -z[4];
z[10] = z[10] * z[11];
return z[10] + z[9] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec1_component3(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec1_component9(const T* invariants, const T* D)
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = z[0] + z[1] + -z[2];
return z[3] * T(8);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec1_divergence(const T* invariants, const T* D)
{
T z[4];
z[0] = D[2];
z[1] = D[3];
z[2] = D[4];
z[3] = z[2] + -z[1];
z[3] = z[0] * T(-2) + z[3] * T(3);
return z[3] * T(4);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec2_component1(const T* invariants, const T* D)
{
T z[16];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = D[3];
z[6] = D[6];
z[7] = D[7];
z[8] = D[4];
z[9] = D[5];
z[10] = z[4] + T(-1);
z[11] = z[10] + -z[0];
z[12] = -(z[4] * z[11]);
z[13] = z[0] * T(2) + -z[10];
z[13] = z[3] * z[13];
z[12] = z[12] + z[13];
z[13] = z[0] * z[7];
z[14] = -z[0] + -z[3] + -z[4];
z[14] = z[6] * z[14];
z[15] = z[3] * z[9];
z[12] = z[13] + z[14] + z[15] + z[12] * T(2);
z[12] = z[8] * z[12];
z[13] = z[7] + z[3] * T(2) + -z[6];
z[11] = z[3] + z[11];
z[11] = z[11] * z[13];
z[13] = z[4] * T(2);
z[14] = z[2] * z[13];
z[11] = z[11] + z[14];
z[11] = z[5] * z[11];
z[14] = z[1] + z[4];
z[10] = -(z[10] * z[14]);
z[13] = T(1) + -z[1] + -z[3] + -z[13];
z[13] = z[3] * z[13];
z[10] = z[10] + z[13];
z[13] = z[4] * z[5];
z[10] = z[13] + z[10] * T(2);
z[10] = z[9] * z[10];
z[13] = -z[3] + -z[14];
z[13] = z[0] * z[2] * z[13];
return z[10] + z[11] + z[12] + z[13] * T(4);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec2_component2(const T* invariants, const T* D)
{
T z[16];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[3];
z[5] = D[6];
z[6] = D[7];
z[7] = D[4];
z[8] = D[5];
z[9] = prod_pow(z[2], 2);
z[10] = z[2] * T(2);
z[11] = z[3] + z[10];
z[11] = z[3] * z[11];
z[12] = z[2] + z[3];
z[13] = z[0] * z[12];
z[9] = z[9] + z[11] + z[13];
z[9] = z[8] * z[9];
z[11] = z[3] * T(2);
z[13] = z[6] + -z[5];
z[14] = z[10] + z[11] + -z[13];
z[14] = z[3] * z[14];
z[11] = z[11] + z[13] + z[2] * T(4);
z[11] = z[0] * z[11];
z[15] = -(z[2] * z[13]);
z[11] = z[11] + z[14] + z[15];
z[11] = z[7] * z[11];
z[10] = z[10] + z[13];
z[10] = -(z[4] * z[10]);
z[13] = z[0] * z[1];
z[10] = z[10] + z[13] * T(-4);
z[12] = z[0] + z[12];
z[10] = z[10] * z[12];
return z[10] + z[11] + z[9] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec2_component3(const T* invariants, const T* D)
{
return T(0);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec2_component9(const T* invariants, const T* D)
{
T z[11];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[3];
z[5] = D[6];
z[6] = D[7];
z[7] = D[4];
z[8] = z[0] + z[2] + z[3];
z[8] = z[1] * z[8];
z[9] = z[6] + -z[5];
z[10] = z[2] * T(-4) + z[3] * T(-2) + -z[9];
z[10] = z[7] * z[10];
z[9] = z[9] + z[2] * T(2);
z[9] = z[4] * z[9];
z[8] = z[9] + z[10] + z[8] * T(4);
return z[8] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec2_divergence(const T* invariants, const T* D)
{
T z[12];
z[0] = D[1];
z[1] = D[2];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[3];
z[5] = D[6];
z[6] = D[7];
z[7] = D[4];
z[8] = D[5];
z[9] = z[6] + -z[5];
z[10] = z[2] * T(2);
z[11] = -z[9] + -z[10];
z[11] = z[4] * z[11];
z[10] = z[3] + z[10];
z[9] = z[9] + z[10] * T(2);
z[9] = z[7] * z[9];
z[9] = z[9] + z[11];
z[10] = -z[2] + -z[3];
z[11] = z[1] * T(4) + -z[8];
z[10] = z[10] * z[11];
z[11] = z[0] * z[1];
z[10] = z[10] + z[11] * T(-6);
return z[10] * T(2) + z[9] * T(3);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec3_component1(const T* invariants, const T* D)
{
T z[16];
z[0] = D[0];
z[1] = D[2];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = D[3];
z[6] = D[6];
z[7] = D[4];
z[8] = z[4] + T(-1);
z[9] = z[5] * z[8];
z[10] = z[8] + -z[0];
z[11] = z[0] * z[10];
z[12] = z[3] + z[8];
z[13] = z[3] * z[12];
z[11] = z[11] + z[13] + -z[9];
z[11] = z[2] * z[11];
z[13] = z[3] * T(2);
z[8] = z[8] + z[0] * T(-2);
z[8] = z[8] * z[13];
z[14] = z[0] + z[3];
z[14] = z[6] * z[14];
z[15] = -(z[2] * z[3]);
z[8] = z[8] + z[14] + z[15];
z[8] = z[7] * z[8];
z[13] = z[6] + -z[13];
z[12] = z[12] + -z[0];
z[12] = z[5] * z[12] * z[13];
z[10] = z[3] + z[10];
z[10] = z[0] * z[10];
z[9] = z[10] + -z[9];
z[9] = z[1] * z[9];
return z[8] + z[12] + z[11] * T(2) + z[9] * T(4);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec3_component2(const T* invariants, const T* D)
{
T z[15];
z[0] = D[0];
z[1] = D[1];
z[2] = D[2];
z[3] = D[5];
z[4] = invariants[1];
z[5] = invariants[2];
z[6] = D[3];
z[7] = D[6];
z[8] = D[4];
z[9] = D[7];
z[10] = -z[6] + -z[8];
z[10] = z[4] * z[10];
z[11] = -(z[5] * z[6]);
z[12] = z[6] + -z[8];
z[12] = -(z[1] * z[12]);
z[10] = z[10] + z[11] + z[12];
z[10] = z[7] * z[10];
z[11] = z[6] + -z[3];
z[12] = z[4] * T(2);
z[13] = z[11] * z[12];
z[14] = z[8] * z[9];
z[13] = z[13] + z[14];
z[13] = z[4] * z[13];
z[11] = z[11] + -z[8];
z[11] = z[11] * z[12];
z[12] = z[3] + z[2] * T(2);
z[14] = z[6] * z[12];
z[11] = z[11] + z[14];
z[11] = z[5] * z[11];
z[14] = z[5] + T(-1) + -z[0];
z[12] = z[12] * z[14];
z[14] = z[2] + -z[8];
z[14] = z[6] + z[14] * T(2);
z[14] = z[4] * z[14];
z[12] = z[12] + z[14];
z[12] = z[1] * z[12];
return z[10] + z[11] + z[13] + z[12] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec3_component3(const T* invariants, const T* D)
{
T z[7];
z[0] = D[2];
z[1] = D[5];
z[2] = D[6];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = z[3] + z[4] + T(-1) + -z[0];
z[6] = z[5] * T(-2) + -z[2];
z[6] = z[0] * z[6];
z[5] = -(z[1] * z[5]);
z[5] = z[5] + z[6];
return z[1] * z[5];
}

template<typename T> T TriangleBox_102ny_z__zm__zvec3_component9(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[2];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = D[3];
z[6] = D[6];
z[7] = D[4];
z[8] = z[3] * z[7];
z[9] = z[4] + T(-1) + -z[0];
z[10] = -z[3] + -z[9];
z[10] = z[1] * z[10];
z[8] = z[8] + z[10];
z[9] = -z[1] + -z[9];
z[9] = z[9] * T(2) + -z[2];
z[9] = z[2] * z[9];
z[10] = z[2] + -z[7];
z[10] = z[6] * z[10];
z[11] = z[6] + z[3] * T(-2);
z[11] = z[5] * z[11];
z[8] = z[9] + z[10] + z[11] + z[8] * T(4);
return z[8] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec3_divergence(const T* invariants, const T* D)
{
T z[11];
z[0] = D[0];
z[1] = D[2];
z[2] = D[5];
z[3] = invariants[1];
z[4] = invariants[2];
z[5] = D[3];
z[6] = D[6];
z[7] = D[4];
z[8] = z[7] * T(-6) + z[5] * T(3) + -z[2];
z[8] = z[3] * z[8];
z[9] = z[4] * T(-2) + T(2) + z[0] * T(3);
z[9] = z[2] * T(-3) + z[9] * T(2);
z[10] = z[3] * T(4) + -z[9];
z[10] = z[1] * z[10];
z[8] = z[8] + z[10];
z[9] = -(z[2] * z[9]);
z[10] = z[7] + -z[2] + -z[5];
z[10] = z[6] * z[10];
return z[9] + z[8] * T(2) + z[10] * T(3);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec4_component1(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[3];
z[5] = D[4];
z[6] = z[0] + T(1);
z[7] = z[2] + T(-2) + -z[0];
z[7] = z[2] * z[7];
z[7] = z[6] + z[7];
z[7] = z[3] * z[7];
z[6] = z[6] + -z[2];
z[8] = z[0] * z[6];
z[9] = -(z[3] * z[6]);
z[9] = z[9] + -z[8];
z[9] = z[1] * z[9];
z[8] = -(z[2] * z[8]);
z[7] = z[7] + z[8] + z[9];
z[6] = z[2] * z[6];
z[8] = z[5] * z[6];
z[6] = z[1] * T(2) + -z[6];
z[6] = z[4] * z[6];
return z[6] + z[8] + z[7] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec4_component2(const T* invariants, const T* D)
{
T z[10];
z[0] = D[0];
z[1] = D[1];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[3];
z[5] = D[4];
z[6] = z[1] * z[3];
z[7] = prod_pow(z[3], 2);
z[6] = z[6] + z[7];
z[7] = z[5] + -z[4];
z[6] = z[6] * z[7];
z[7] = -z[1] + -z[3];
z[8] = z[2] + z[3];
z[9] = z[8] + T(-1);
z[7] = z[7] * z[9];
z[8] = z[0] * z[8];
z[7] = z[7] + -z[8];
z[7] = z[1] * z[7];
z[8] = -(z[3] * z[8]);
z[7] = z[7] + z[8];
return z[6] + z[7] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec4_component3(const T* invariants, const T* D)
{
T z[8];
z[0] = D[2];
z[1] = invariants[2];
z[2] = D[5];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = z[5] + -z[2];
z[7] = z[0] * T(2) + -z[6];
z[7] = z[1] * z[7];
z[6] = T(-2) + -z[6];
z[6] = z[3] * z[6];
z[6] = z[5] + z[6] + z[7] + -z[4];
return z[0] * z[6];
}

template<typename T> T TriangleBox_102ny_z__zm__zvec4_component9(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[9] + -z[7];
z[11] = z[0] + z[3];
z[12] = z[11] + -z[4];
z[12] = z[5] + z[10] + z[12] * T(2) + -z[6];
z[12] = z[2] * z[12];
z[11] = z[11] + T(1);
z[10] = z[10] + z[11] * T(2);
z[10] = z[1] * z[10];
z[10] = z[8] + z[10] + z[12] + z[3] * T(-2) + -z[9];
return z[10] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec4_divergence(const T* invariants, const T* D)
{
T z[13];
z[0] = D[0];
z[1] = invariants[1];
z[2] = invariants[2];
z[3] = D[1];
z[4] = D[2];
z[5] = D[3];
z[6] = D[4];
z[7] = D[5];
z[8] = D[6];
z[9] = D[7];
z[10] = z[9] + z[3] * T(3);
z[11] = z[10] + z[0] * T(3) + -z[7];
z[12] = T(-2) + -z[11];
z[12] = z[1] * z[12];
z[10] = z[10] + z[12] + -z[8];
z[12] = z[5] + -z[6];
z[11] = z[12] * T(-3) + z[11] * T(-2) + z[4] * T(4);
z[11] = z[2] * z[11];
return z[11] + z[10] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec5_component1(const T* invariants, const T* D)
{
T z[14];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[1];
z[4] = D[5];
z[5] = D[4];
z[6] = invariants[2];
z[7] = D[3];
z[8] = D[6];
z[9] = z[0] + T(1);
z[10] = z[0] * z[9];
z[10] = z[10] + -z[7];
z[11] = z[7] + -z[0];
z[11] = z[6] * z[11];
z[12] = -(z[0] * z[2]);
z[11] = z[10] + z[11] + z[12];
z[11] = z[1] * z[11];
z[12] = z[0] * z[3];
z[10] = z[10] + z[12];
z[12] = z[6] * z[7];
z[10] = z[12] + z[10] * T(2);
z[10] = z[2] * z[10];
z[9] = z[9] + -z[6];
z[12] = z[2] + -z[9];
z[13] = z[3] + z[6];
z[12] = z[4] * z[12] * z[13];
z[10] = z[10] + z[11] + z[12];
z[9] = z[8] + z[9] * T(-2);
z[9] = z[5] * z[6] * z[9];
return z[9] + z[10] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec5_component2(const T* invariants, const T* D)
{
T z[14];
z[0] = D[0];
z[1] = D[1];
z[2] = D[7];
z[3] = invariants[1];
z[4] = D[5];
z[5] = D[4];
z[6] = invariants[2];
z[7] = D[3];
z[8] = D[6];
z[9] = z[3] * T(2);
z[10] = -z[4] + -z[7];
z[10] = z[9] * z[10];
z[11] = -(z[2] * z[7]);
z[12] = z[2] + -z[8];
z[12] = z[5] * z[12];
z[13] = -z[4] + -z[5];
z[13] = z[6] * z[13];
z[10] = z[10] + z[11] + z[12] + z[13] * T(2);
z[10] = z[6] * z[10];
z[11] = T(2) + -z[2] + -z[4];
z[11] = z[3] * z[11];
z[12] = z[2] + z[9];
z[12] = z[0] * z[12];
z[13] = z[4] * T(-2) + -z[2] + -z[5];
z[13] = z[6] * z[13];
z[9] = z[9] + -z[4];
z[9] = z[1] * z[9];
z[9] = z[2] + z[9] + z[11] + z[12] + z[13];
z[9] = z[1] * z[9];
return z[10] + z[9] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec5_component3(const T* invariants, const T* D)
{
T z[11];
z[0] = D[2];
z[1] = D[5];
z[2] = D[6];
z[3] = D[7];
z[4] = invariants[2];
z[5] = invariants[1];
z[6] = z[1] * z[4];
z[7] = z[1] + z[3];
z[8] = z[6] * z[7];
z[9] = z[1] * z[3];
z[6] = z[6] + -z[9];
z[10] = z[2] * z[7];
z[6] = z[10] + z[6] * T(2);
z[6] = z[0] * z[6];
z[7] = z[7] + T(-2);
z[7] = z[1] * z[5] * z[7];
return z[6] + z[7] + z[8] + -z[9];
}

template<typename T> T TriangleBox_102ny_z__zm__zvec5_component9(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[1];
z[4] = D[5];
z[5] = D[4];
z[6] = invariants[2];
z[7] = D[6];
z[8] = z[2] + z[6];
z[9] = z[0] + T(1);
z[10] = z[4] + z[8] + -z[9];
z[10] = z[1] * z[10];
z[11] = z[5] * z[6];
z[9] = -z[3] + -z[9];
z[9] = z[2] * z[9];
z[8] = z[3] + z[8];
z[8] = z[4] * z[8];
z[8] = z[8] + z[10] + z[11] + z[9] * T(2);
z[9] = -z[1] + -z[4];
z[9] = z[7] * z[9];
z[8] = z[9] + z[8] * T(2);
return z[8] * T(2);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec5_divergence(const T* invariants, const T* D)
{
T z[12];
z[0] = D[0];
z[1] = D[7];
z[2] = invariants[1];
z[3] = D[1];
z[4] = D[5];
z[5] = D[4];
z[6] = invariants[2];
z[7] = D[6];
z[8] = -z[3] + -z[6];
z[8] = z[4] * z[8];
z[9] = -(z[5] * z[6]);
z[8] = z[8] + z[9];
z[9] = T(2) + z[0] * T(3);
z[10] = z[1] + z[4];
z[11] = z[9] + z[3] * T(3) + -z[10];
z[11] = z[2] * z[11];
z[9] = z[9] + z[4] * T(-3) + z[6] * T(-2);
z[9] = z[1] * z[9];
z[8] = z[9] + z[11] * T(2) + z[8] * T(3);
z[9] = z[7] * z[10];
return z[8] * T(2) + z[9] * T(3);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec6_component1(const T* invariants, const T* D)
{
T z[17];
z[0] = D[0];
z[1] = D[2];
z[2] = invariants[2];
z[3] = D[3];
z[4] = D[4];
z[5] = D[5];
z[6] = invariants[1];
z[7] = D[6];
z[8] = D[1];
z[9] = D[7];
z[10] = z[7] + -z[5];
z[11] = z[2] + z[6];
z[11] = z[10] * z[11];
z[12] = z[3] + -z[4];
z[13] = -(z[2] * z[12]);
z[14] = z[1] * z[2];
z[11] = z[11] + -z[13] + -z[14];
z[11] = z[0] * z[11];
z[15] = z[2] + T(-1);
z[16] = z[6] + z[15];
z[10] = z[10] * z[16];
z[14] = z[1] + -z[14];
z[10] = z[10] + z[14] * T(2);
z[10] = z[8] * z[10];
z[10] = z[10] + z[11];
z[11] = z[2] + z[9];
z[11] = z[4] * z[11] * z[15];
z[14] = z[2] * T(-2) + -z[9];
z[14] = z[14] * z[15];
z[16] = T(-2) + -z[2];
z[16] = z[6] * z[16];
z[14] = z[14] + z[16];
z[14] = z[3] * z[14];
z[11] = z[11] + z[14];
z[14] = z[2] * z[15];
z[15] = z[2] + T(-2);
z[16] = z[6] * z[15];
z[14] = z[14] + z[16];
z[13] = z[14] * T(2) + -z[13];
z[13] = z[5] * z[13];
z[12] = z[7] * z[12] * z[15];
return z[12] + z[13] + z[11] * T(2) + z[10] * T(4);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec6_component2(const T* invariants, const T* D)
{
T z[15];
z[0] = D[0];
z[1] = D[6];
z[2] = invariants[1];
z[3] = invariants[2];
z[4] = D[7];
z[5] = D[1];
z[6] = D[2];
z[7] = D[3];
z[8] = D[4];
z[9] = D[5];
z[10] = z[7] + -z[8];
z[11] = z[4] + -z[1];
z[12] = z[6] + z[10] + z[11];
z[12] = z[3] * z[12];
z[13] = z[2] * z[11];
z[12] = z[12] + z[13] + -z[11];
z[12] = z[5] * z[12];
z[14] = z[3] * z[11];
z[13] = z[13] + z[14];
z[13] = z[0] * z[13];
z[12] = z[12] + z[13];
z[13] = z[7] + -z[9];
z[13] = z[2] * z[13];
z[14] = z[7] * T(2) + -z[8] + -z[9];
z[14] = z[3] * z[14];
z[13] = z[13] + z[14];
z[10] = z[10] * z[11];
z[10] = z[10] + z[13] * T(2);
z[10] = z[3] * z[10];
return z[10] + z[12] * T(4);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec6_component3(const T* invariants, const T* D)
{
T z[8];
z[0] = D[2];
z[1] = invariants[2];
z[2] = D[5];
z[3] = invariants[1];
z[4] = D[6];
z[5] = D[7];
z[6] = z[5] + -z[2];
z[7] = z[6] + z[0] * T(-2);
z[7] = z[1] * z[7];
z[6] = z[6] + T(2);
z[6] = z[3] * z[6];
z[6] = z[4] + z[6] + z[7] + -z[5];
return z[0] * z[6] * T(4);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec6_component9(const T* invariants, const T* D)
{
T z[11];
z[0] = D[2];
z[1] = invariants[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = invariants[1];
z[6] = D[6];
z[7] = D[7];
z[8] = z[4] + -z[7];
z[8] = z[8] * T(-3);
z[9] = z[3] + -z[2];
z[9] = z[9] * T(2) + z[0] * T(4) + -z[8];
z[9] = z[1] * z[9];
z[10] = z[7] + -z[6];
z[8] = T(-2) + -z[8];
z[8] = z[5] * z[8];
z[8] = z[8] + z[9] + z[10] * T(3);
return z[8] * T(4);
}

template<typename T> T TriangleBox_102ny_z__zm__zvec6_divergence(const T* invariants, const T* D)
{
T z[11];
z[0] = D[2];
z[1] = invariants[2];
z[2] = D[3];
z[3] = D[4];
z[4] = D[5];
z[5] = invariants[1];
z[6] = D[6];
z[7] = D[7];
z[8] = z[7] + -z[4];
z[9] = z[2] + z[8] + -z[3];
z[9] = z[1] * z[9];
z[9] = z[6] + z[9] + -z[7];
z[8] = T(2) + z[8] * T(3);
z[8] = z[5] * z[8];
z[10] = z[0] * z[1];
z[8] = z[8] + z[10] * T(-4) + z[9] * T(3);
return z[8] * T(4);
}

template<typename T> std::vector<std::vector<T>> TriangleBox_102ny_z__zm__z_vectors(const T* propagators, const T* invariants, const std::vector<std::vector<T>>& ccoeffs)
{
std::vector<std::vector<T>> ibp_vectors(6, std::vector<T>(10, T(0)));
ibp_vectors[0][0] = TriangleBox_102ny_z__zm__zvec1_component1(invariants, propagators);
ibp_vectors[0][1] = TriangleBox_102ny_z__zm__zvec1_component2(invariants, propagators);
ibp_vectors[0][8] = TriangleBox_102ny_z__zm__zvec1_component9(invariants, propagators);
ibp_vectors[0].back() = TriangleBox_102ny_z__zm__zvec1_divergence(invariants, propagators);
ibp_vectors[1][0] = TriangleBox_102ny_z__zm__zvec2_component1(invariants, propagators);
ibp_vectors[1][1] = TriangleBox_102ny_z__zm__zvec2_component2(invariants, propagators);
ibp_vectors[1][8] = TriangleBox_102ny_z__zm__zvec2_component9(invariants, propagators);
ibp_vectors[1].back() = TriangleBox_102ny_z__zm__zvec2_divergence(invariants, propagators);
ibp_vectors[2][0] = TriangleBox_102ny_z__zm__zvec3_component1(invariants, propagators);
ibp_vectors[2][1] = TriangleBox_102ny_z__zm__zvec3_component2(invariants, propagators);
ibp_vectors[2][2] = TriangleBox_102ny_z__zm__zvec3_component3(invariants, propagators);
ibp_vectors[2][8] = TriangleBox_102ny_z__zm__zvec3_component9(invariants, propagators);
ibp_vectors[2].back() = TriangleBox_102ny_z__zm__zvec3_divergence(invariants, propagators);
ibp_vectors[3][0] = TriangleBox_102ny_z__zm__zvec4_component1(invariants, propagators);
ibp_vectors[3][1] = TriangleBox_102ny_z__zm__zvec4_component2(invariants, propagators);
ibp_vectors[3][2] = TriangleBox_102ny_z__zm__zvec4_component3(invariants, propagators);
ibp_vectors[3][8] = TriangleBox_102ny_z__zm__zvec4_component9(invariants, propagators);
ibp_vectors[3].back() = TriangleBox_102ny_z__zm__zvec4_divergence(invariants, propagators);
ibp_vectors[4][0] = TriangleBox_102ny_z__zm__zvec5_component1(invariants, propagators);
ibp_vectors[4][1] = TriangleBox_102ny_z__zm__zvec5_component2(invariants, propagators);
ibp_vectors[4][2] = TriangleBox_102ny_z__zm__zvec5_component3(invariants, propagators);
ibp_vectors[4][8] = TriangleBox_102ny_z__zm__zvec5_component9(invariants, propagators);
ibp_vectors[4].back() = TriangleBox_102ny_z__zm__zvec5_divergence(invariants, propagators);
ibp_vectors[5][0] = TriangleBox_102ny_z__zm__zvec6_component1(invariants, propagators);
ibp_vectors[5][1] = TriangleBox_102ny_z__zm__zvec6_component2(invariants, propagators);
ibp_vectors[5][2] = TriangleBox_102ny_z__zm__zvec6_component3(invariants, propagators);
ibp_vectors[5][8] = TriangleBox_102ny_z__zm__zvec6_component9(invariants, propagators);
ibp_vectors[5].back() = TriangleBox_102ny_z__zm__zvec6_divergence(invariants, propagators);
return ibp_vectors;
}

extern template std::vector<std::vector<C>> TriangleBox_102ny_z__zm__z_vectors<C>(const C*, const C*, const std::vector<std::vector<C>>&);

#ifdef HIGH_PRECISION

extern template std::vector<std::vector<CHP>> TriangleBox_102ny_z__zm__z_vectors<CHP>(const CHP*, const CHP*, const std::vector<std::vector<CHP>>&);

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::vector<std::vector<CVHP>> TriangleBox_102ny_z__zm__z_vectors<CVHP>(const CVHP*, const CVHP*, const std::vector<std::vector<CVHP>>&);

#endif

#ifdef USE_FINITE_FIELDS

extern template std::vector<std::vector<F32>> TriangleBox_102ny_z__zm__z_vectors<F32>(const F32*, const F32*, const std::vector<std::vector<F32>>&);

#endif

}}}}

#endif

