/*  Ita Surface terms.  */

namespace Caravel{
namespace FunctionSpace{
namespace SurfaceTerms{
namespace Five_Point_Planar_QCD{

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta1(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[0][5];
z[1] = ibp_vectors[0].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[0][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta2(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta3(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta4(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta5(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta6(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
z[3] = ibp_vectors[0].back();
z[4] = ibp_vectors[0][2];
z[5] = ibp_vectors[0][1];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[0][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta7(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
z[2] = ibp_vectors[0].back();
z[3] = ibp_vectors[0][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[0][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta8(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[1][5];
z[1] = ibp_vectors[1].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[1][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta9(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][0];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[0];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta10(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta11(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta12(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][0];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[0];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta13(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[8];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
z[3] = ibp_vectors[1].back();
z[4] = ibp_vectors[1][2];
z[5] = ibp_vectors[1][1];
z[6] = z[3] + (z[2] * T(5)) / T(2);
z[6] = z[1] * z[6];
z[6] = z[4] + z[6];
z[6] = z[0] * z[6];
z[7] = z[1] * z[5];
zd[0] = z[6] + z[7];
}
{
T z[4];
z[0] = D[1];
z[1] = D[2];
z[2] = ibp_vectors[1][5];
zd[1] = -((z[0] * z[1] * z[2]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta14(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[2];
z[1] = ibp_vectors[1][5];
z[2] = ibp_vectors[1].back();
z[3] = ibp_vectors[1][2];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
z[4] = z[4] + z[3] * T(2);
zd[0] = z[0] * z[4];
}
{
T z[3];
z[0] = D[2];
z[1] = ibp_vectors[1][5];
zd[1] = -((prod_pow(z[0], 2) * z[1]) / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta15(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[3];
z[0] = ibp_vectors[2][5];
z[1] = ibp_vectors[2].back();
zd[0] = z[1] + (z[0] * T(5)) / T(2);
}
{
T z[2];
z[0] = ibp_vectors[2][5];
zd[1] = -(z[0] / T(2));
}
return zd;
}

template<typename T> std::vector<T> BubbleTriangle_001yy___z_m_mSurfaceIta16(const T* invariants, const T* D, const std::vector<std::vector<T>>& ibp_vectors, const T* b)
{
std::vector<T> zd(2);
{
T z[5];
z[0] = D[1];
z[1] = ibp_vectors[4][5];
z[2] = ibp_vectors[4].back();
z[3] = ibp_vectors[4][1];
z[4] = z[2] + (z[1] * T(5)) / T(2);
z[4] = z[0] * z[4];
zd[0] = z[3] + z[4];
}
{
T z[3];
z[0] = D[1];
z[1] = ibp_vectors[4][5];
zd[1] = -((z[0] * z[1]) / T(2));
}
return zd;
}

/*  Build collection of function pointers for exporting.  */

template<typename T> std::array<SurfaceTermsFunctionPointer<T>,16> BubbleTriangle_001yy___z_m_mSurfaceIta()
{
std::array<SurfaceTermsFunctionPointer<T>,16> BubbleTriangle_001yy___z_m_mSurfaceItaArray;
BubbleTriangle_001yy___z_m_mSurfaceItaArray[0] = {BubbleTriangle_001yy___z_m_mSurfaceIta1, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[1] = {BubbleTriangle_001yy___z_m_mSurfaceIta2, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[2] = {BubbleTriangle_001yy___z_m_mSurfaceIta3, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[3] = {BubbleTriangle_001yy___z_m_mSurfaceIta4, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[4] = {BubbleTriangle_001yy___z_m_mSurfaceIta5, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[5] = {BubbleTriangle_001yy___z_m_mSurfaceIta6, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[6] = {BubbleTriangle_001yy___z_m_mSurfaceIta7, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[7] = {BubbleTriangle_001yy___z_m_mSurfaceIta8, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[8] = {BubbleTriangle_001yy___z_m_mSurfaceIta9, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[9] = {BubbleTriangle_001yy___z_m_mSurfaceIta10, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[10] = {BubbleTriangle_001yy___z_m_mSurfaceIta11, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[11] = {BubbleTriangle_001yy___z_m_mSurfaceIta12, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[12] = {BubbleTriangle_001yy___z_m_mSurfaceIta13, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[13] = {BubbleTriangle_001yy___z_m_mSurfaceIta14, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[14] = {BubbleTriangle_001yy___z_m_mSurfaceIta15, -1};
BubbleTriangle_001yy___z_m_mSurfaceItaArray[15] = {BubbleTriangle_001yy___z_m_mSurfaceIta16, -1};
return BubbleTriangle_001yy___z_m_mSurfaceItaArray;
}

extern template std::array<SurfaceTermsFunctionPointer<C>,16> BubbleTriangle_001yy___z_m_mSurfaceIta<C>();

#ifdef HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CHP>,16> BubbleTriangle_001yy___z_m_mSurfaceIta<CHP>();

#endif

#ifdef VERY_HIGH_PRECISION

extern template std::array<SurfaceTermsFunctionPointer<CVHP>,16> BubbleTriangle_001yy___z_m_mSurfaceIta<CVHP>();

#endif

#ifdef USE_FINITE_FIELDS

extern template std::array<SurfaceTermsFunctionPointer<F32>,16> BubbleTriangle_001yy___z_m_mSurfaceIta<F32>();

#endif

}}}}

