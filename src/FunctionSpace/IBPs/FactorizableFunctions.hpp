namespace Caravel {
namespace FunctionSpace {
namespace SurfaceTerms {
namespace Factorizable {

template <typename T, size_t D> IBPCoordinates<T> factorizable_IBP_coordinates(const OnShellPoint<T, D>& l, const std::vector<momentumD<T, D>>& P, const std::vector<std::vector<T>>&) {
    return {{l[0] * P[1]}, {P[1] * P[0]}, {}};
}

template <typename T, size_t D> IBPCoordinates<T> empty_IBP_coordinates(const OnShellPoint<T, D>&, const std::vector<momentumD<T, D>>&, const std::vector<std::vector<T>>&) { return {}; }

template <typename T> BasisFunction<T> factorizable_surface_function() {
    return {[](const TopologyCoordinates<T>& coords) {
        auto& p1p2 = coords.invariants.at(0);
        auto& l1p2 = coords.propagators.at(0);
        auto constant = T(-2) * p1p2 + T(6) * l1p2;
        auto linear = p1p2 - T(2) * l1p2;
        return std::make_pair(constant, linear);
    }};
}

template <typename T> T scalar_master(const TopologyCoordinates<T>& coords) { return static_cast<T>(1); }
} // namespace Factorizable
} // namespace SurfaceTerms
} // namespace FunctionSpace
} // namespace Caravel
