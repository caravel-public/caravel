#include "FunctionSpace.h"

namespace Caravel {
namespace FunctionSpace {
decltype(master_term_data_holder) master_term_data_holder;
decltype(surface_term_data_holder) surface_term_data_holder;
} // namespace FunctionSpace
} // namespace Caravel

