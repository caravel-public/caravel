#include "FunctionSpace/FunctionSpace.h"
#include "Core/settings.h"

#include "FunctionSpace/MasterIntegrands.h"

namespace Caravel {
namespace FunctionSpace {
namespace MasterIntegrands {
namespace PureMasterBasis {

using namespace MasterIntegrands;

template <typename T, size_t D>
std::vector<T> BoxBox_Pure_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(-mc[1] + l[1])};
}

template <typename T> T BoxBox_Pure_scalar(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T BoxBox_Pure_tensor(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0];
}

inline void load() {
  if (verbosity_settings.report_integral_basis){
      std::cout<<"Loading 'PureMasterBasis' master integrands"<<std::endl;
  }
  using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
  {
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], "
                "Link[], Leg[2, 0], Link[]], Strand[LoopMomentum[-2], Leg[4, 0], Link[], Leg[3, "
                "0], Link[]], Strand[Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar", "tensor"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = BoxBox_Pure_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxBox_Pure_scalar<C>, BoxBox_Pure_tensor<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = BoxBox_Pure_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxBox_Pure_scalar<CHP>,
                                                        BoxBox_Pure_tensor<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = BoxBox_Pure_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxBox_Pure_scalar<CVHP>,
                                                         BoxBox_Pure_tensor<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = BoxBox_Pure_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxBox_Pure_scalar<F32>,
                                                        BoxBox_Pure_tensor<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
}

} // namespace PureMasterBasis
} // namespace MasterIntegrands
} // namespace FunctionSpace
} // namespace Caravel
