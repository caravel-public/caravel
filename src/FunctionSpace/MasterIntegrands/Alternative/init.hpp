#include "FunctionSpace/FunctionSpace.h"
#include "Core/settings.h"

#include "FunctionSpace/MasterIntegrands.h"

namespace Caravel {
namespace FunctionSpace {
namespace MasterIntegrands {
namespace Alternative {

using namespace MasterIntegrands;

template <typename T, size_t D>
std::vector<T> Bowtie_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {};
}

template <typename T> T Bowtie_scalar(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T, size_t D>
std::vector<T> BubTri_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {};
}

template <typename T> T BubTri_scalar(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T, size_t D>
std::vector<T> Sunrise_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {};
}

template <typename T> T Sunrise_scalar(const TopologyCoordinates<T> &cs) { return T(1); }


inline void load() {

  if (verbosity_settings.report_integral_basis){
      std::cout<<"Loading 'Alternative' master integrands"<<std::endl;
  }
  using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
  {
    gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], "
                "Strand[LoopMomentum[2], Leg[2], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = Bowtie_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {Bowtie_scalar<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = Bowtie_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Bowtie_scalar<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = Bowtie_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Bowtie_scalar<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = Bowtie_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {Bowtie_scalar<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1], "
        "Leg[3], Link[]], Strand[Link[]], Strand[LoopMomentum[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = BubTri_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {BubTri_scalar<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = BubTri_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BubTri_scalar<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = BubTri_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BubTri_scalar<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = BubTri_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {BubTri_scalar<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2]]], "
                "Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = Sunrise_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {Sunrise_scalar<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = Sunrise_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Sunrise_scalar<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = Sunrise_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Sunrise_scalar<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = Sunrise_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {Sunrise_scalar<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
}

} // namespace Alternative
} // namespace MasterIntegrands
} // namespace FunctionSpace
} // namespace Caravel
