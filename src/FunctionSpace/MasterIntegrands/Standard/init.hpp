#include "FunctionSpace/FunctionSpace.h"
#include "Core/settings.h"

#include "FunctionSpace/MasterIntegrands.h"

namespace Caravel {
namespace FunctionSpace {
namespace MasterIntegrands {
namespace Standard {

using namespace MasterIntegrands;

template <typename T, size_t D>
std::vector<T> XBox_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {mc.s(1, 2), mc.s(1, 3), mc.s(1, 4), sq(l[0] + mc[3]), sq(l[0] + mc[4])};
}

template <typename T> T XBox_Tensor1(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * abb[2] * abb[3];
}

template <typename T> T XBox_Tensor2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * abb[1] * abb[4];
}

template <typename T, size_t D>
std::vector<T> np_oneMass_BoxBox_coordinates(const OnShellPoint<T, D> &l,
                                             const momD_conf<T, D> &mc) {
  return {};
}

template <typename T> T np_oneMass_BoxBox_scalar(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T, size_t D>
std::vector<T> BubbleBox_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {};
}

template <typename T> T BubbleBox_scalar(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T, size_t D>
std::vector<T> DoubleBox_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(l[1] + mc[2])};
}

template <typename T> T DoubleBox_scalar(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T DoubleBox_tensor(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0];
}

template <typename T, size_t D>
std::vector<T> DoubleTri_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {};
}

template <typename T> T DoubleTri_scalar(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T, size_t D>
std::vector<T> BoxBox4pt1M_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {mc.s(1, 2), mc.s(2, 3), (-mc[1] + -mc[4] + l[1]) * mc[2], sq(mc[4])};
}

template <typename T> T BoxBox4pt1M_std0(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 2);
}

template <typename T> T BoxBox4pt1M_std1(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return T(-1) * abb[1] * abb[2] * (abb[0] + T(-1) * abb[3]) * (abb[1] + T(-1) * abb[3]);
}

template <typename T, size_t D>
std::vector<T> FactBoxBubble_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {mc.s(1, 2), mc.s(2, 3)};
}

template <typename T>
std::vector<T> FactBoxBubble_std0(const TopologyCoordinates<T> &cs,
                                  const std::vector<T> &d_values) {
  const auto &abb = cs.special;
  T expr = abb[0] * abb[1];
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(-3) + d; };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T, size_t D>
std::vector<T> FactBubbleBubble_coordinates(const OnShellPoint<T, D> &l,
                                            const momD_conf<T, D> &mc) {
  return {};
}

template <typename T>
std::vector<T> FactBubbleBubble_std0(const TopologyCoordinates<T> &cs,
                                     const std::vector<T> &d_values) {
  T expr = T(1);
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(9) + (T(-6) + d) * d; };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T, size_t D>
std::vector<T> FactSSBubbleBubble_coordinates(const OnShellPoint<T, D> &l,
                                              const momD_conf<T, D> &mc) {
  return {};
}

template <typename T>
std::vector<T> FactSSBubbleBubble_std0(const TopologyCoordinates<T> &cs,
                                       const std::vector<T> &d_values) {
  T expr = T(1);
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(9) + (T(-6) + d) * d; };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T, size_t D>
std::vector<T> GenBoxBubble1Ma_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {mc.s(1, 2)};
}

template <typename T>
std::vector<T> GenBoxBubble1Ma_std0(const TopologyCoordinates<T> &cs,
                                    const std::vector<T> &d_values) {
  const auto &abb = cs.special;
  T expr = abb[0];
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(-3) + d; };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T, size_t D>
std::vector<T> GenBoxBubble1Mb_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {mc.s(1, 2), sq(mc[4])};
}

template <typename T>
std::vector<T> GenBoxBubble1Mb_std0(const TopologyCoordinates<T> &cs,
                                    const std::vector<T> &d_values) {
  const auto &abb = cs.special;
  T expr = abb[0] + T(-1) * abb[1];
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(-3) + d; };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T, size_t D>
std::vector<T> GenTrgBubble2M_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {};
}

template <typename T>
std::vector<T> GenTrgBubble2M_std0(const TopologyCoordinates<T> &cs,
                                   const std::vector<T> &d_values) {
  T expr = T(1);
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(30) + d * (T(-19) + T(3) * d); };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T, size_t D>
std::vector<T> GenTrgBubble_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {};
}

template <typename T>
std::vector<T> GenTrgBubble_std0(const TopologyCoordinates<T> &cs, const std::vector<T> &d_values) {
  T expr = T(1);
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(30) + d * (T(-19) + T(3) * d); };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T, size_t D>
std::vector<T> GenTrgTrg1Ma_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {mc.s(1, 2), mc.s(2, 3),        (-mc[4] + l[1]) * mc[3], sq(l[0]),
          sq(l[1]),   sq(-mc[2] + l[0]), sq(-mc[4] + l[1]),       sq(mc[4])};
}

template <typename T> T GenTrgTrg1Ma_scalar(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] + abb[1];
}

template <typename T> T GenTrgTrg1Ma_dot(const TopologyCoordinates<T> &cs, const T &d) {
  const auto &abb = cs.special;
  return abb[0] * abb[1] *
         (T(-2) * (T(-10) + T(3) * d) * (T(1) / prod_pow(abb[0], 2)) *
              (T(1) / prod_pow(abb[1], 2)) * prod_pow(abb[0] + abb[1], 2) * abb[2] +
          (T(1) / prod_pow(d + T(-4), 2)) * (T(-10) + T(3) * d) * (T(-8) + T(3) * d) *
              (T(1) / prod_pow(abb[0], 2)) * (T(1) / prod_pow(abb[1], 2)) *
              ((T(-10) + T(3) * d) * abb[0] + (T(-4) + d) * abb[1]) * abb[4] * abb[5] +
          (T(1) / prod_pow(d + T(-4), 2)) * (T(-10) + T(3) * d) * (T(-8) + T(3) * d) *
              (T(1) / prod_pow(abb[0], 3)) * (T(1) / abb[1]) *
              ((T(-2) + d) * abb[0] + T(-1) * (T(-4) + d) * abb[1]) * abb[3] * abb[6] +
          T(-2) * (T(-7) + T(2) * d) * (T(1) / abb[0]) * (T(1) / abb[1]) *
              (abb[0] + abb[1] + T(-2) * abb[7]) +
          (T(1) / (d + T(-4))) * (T(-3) + d) * (T(-10) + T(3) * d) * (T(1) / prod_pow(abb[0], 2)) *
              (T(1) / prod_pow(abb[1], 2)) * abb[3] *
              (abb[0] * (T(2) * abb[0] + abb[1]) + T(-2) * (abb[0] + T(-1) * abb[1]) * abb[7]) +
          T(-1) * (T(1) / (d + T(-4))) * (T(-3) + d) * (T(-10) + T(3) * d) *
              (T(1) / prod_pow(abb[0], 2)) * (T(1) / prod_pow(abb[1], 2)) * abb[5] *
              (abb[0] * (T(2) * abb[0] + T(3) * abb[1]) +
               T(-2) * (abb[0] + T(-1) * abb[1]) * abb[7]));
}

template <typename T, size_t D>
std::vector<T> GenTrgTrg1Mb_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {mc.s(1, 2), mc.s(2, 3), sq(mc[4])};
}

template <typename T> T GenTrgTrg1Mb_std0(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] + abb[1] + T(-1) * abb[2];
}

template <typename T, size_t D>
std::vector<T> MasslessGenBoxTriang_coordinates(const OnShellPoint<T, D> &l,
                                                const momD_conf<T, D> &mc) {
  return {levi(mc[1], mc[2], mc[3], mc[4]),
          mc.s(1, 2),
          mc.s(1, 5),
          mc.s(2, 3),
          mc.s(3, 4),
          mc.s(4, 5),
          (l[1] + mc[2]) * mc[1],
          sq(l[0]),
          sq(l[1]),
          sq(-mc[1] + l[0]),
          sq(-mc[3] + l[1]),
          sq(-mc.Sum(3, 4) + l[1]),
          sq(-mc.Sum(3, 4, 5) + l[1])};
}

template <typename T> T MasslessGenBoxTriang_div(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4] * (T(-1) * abb[3] + abb[5]) + T(-2) * abb[4] * abb[6] +
         T(-1) * (abb[2] + T(-1) * abb[3] + abb[5]) * abb[8] +
         (abb[1] + abb[3] + T(-1) * abb[5]) * abb[11];
}

template <typename T> std::array<T, 2> MasslessGenBoxTriang_fin(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return {
      (T(1) / abb[0]) *
          ((T(1) / T(4)) * abb[4] *
               (abb[2] *
                    (abb[1] * abb[3] + (abb[1] + abb[3]) * abb[5] + T(-1) * prod_pow(abb[5], 2)) +
                (T(-1) * abb[3] + abb[5]) *
                    (abb[1] * abb[3] + abb[4] * (T(-1) * abb[3] + abb[5]))) +
           (T(3) / T(8)) * abb[4] *
               (abb[3] * (abb[1] + T(-1) * abb[4]) +
                (T(-2) * abb[1] + T(-2) * abb[3] + abb[4]) * abb[5] + T(2) * prod_pow(abb[5], 2) +
                abb[2] * (T(-1) * abb[1] + abb[5])) *
               abb[7] +
           (T(-1) / T(4)) * (abb[2] + T(-1) * abb[3] + abb[5]) *
               (abb[1] * abb[3] + abb[2] * (T(-1) * abb[1] + abb[5]) +
                abb[4] * (T(-1) * abb[3] + abb[5])) *
               abb[8] +
           (T(-3) / T(8)) * abb[4] *
               (abb[3] * (abb[1] + T(-1) * abb[4]) +
                (T(-2) * abb[1] + T(-2) * abb[3] + abb[4]) * abb[5] + T(2) * prod_pow(abb[5], 2) +
                abb[2] * (T(-1) * abb[1] + abb[5])) *
               abb[9] +
           (T(1) / T(4)) *
               (prod_pow(abb[2], 2) * (T(-1) * abb[1] + abb[5]) +
                abb[2] * (T(-1) * prod_pow(abb[1], 2) + abb[3] * abb[4] +
                          abb[1] * (abb[3] + T(2) * abb[4]) + (abb[1] + T(-1) * abb[4]) * abb[5]) +
                abb[1] * (abb[1] * abb[3] + abb[4] * (T(-1) * abb[3] + abb[5]))) *
               abb[10] +
           (T(1) / T(4)) * (T(-1) * abb[1] + T(-1) * abb[3] + abb[5]) *
               (abb[1] * abb[3] + abb[4] * (abb[3] + T(-1) * abb[5]) +
                abb[2] * (T(-1) * abb[1] + abb[5])) *
               abb[11] +
           (T(1) / T(2)) * abb[4] * (T(-1) * abb[1] + T(-1) * abb[3] + abb[5]) *
               (abb[2] + T(-1) * abb[3] + abb[5]) * abb[12]),
      (T(1) / abb[0]) *
          ((T(-1) / T(16)) * abb[4] *
               (abb[2] *
                    (abb[1] * abb[3] + (abb[1] + abb[3]) * abb[5] + T(-1) * prod_pow(abb[5], 2)) +
                (T(-1) * abb[3] + abb[5]) *
                    (abb[1] * abb[3] + abb[4] * (T(-1) * abb[3] + abb[5]))) +
           (T(-1) / T(8)) * abb[4] *
               (abb[3] * (abb[1] + T(-1) * abb[4]) +
                (T(-2) * abb[1] + T(-2) * abb[3] + abb[4]) * abb[5] + T(2) * prod_pow(abb[5], 2) +
                abb[2] * (T(-1) * abb[1] + abb[5])) *
               abb[7] +
           (T(1) / T(16)) * (abb[2] + T(-1) * abb[3] + abb[5]) *
               (abb[1] * abb[3] + abb[2] * (T(-1) * abb[1] + abb[5]) +
                abb[4] * (T(-1) * abb[3] + abb[5])) *
               abb[8] +
           (T(1) / T(8)) * abb[4] *
               (abb[3] * (abb[1] + T(-1) * abb[4]) +
                (T(-2) * abb[1] + T(-2) * abb[3] + abb[4]) * abb[5] + T(2) * prod_pow(abb[5], 2) +
                abb[2] * (T(-1) * abb[1] + abb[5])) *
               abb[9] +
           (T(-1) / T(16)) *
               (prod_pow(abb[2], 2) * (T(-1) * abb[1] + abb[5]) +
                abb[2] * (T(-1) * prod_pow(abb[1], 2) + abb[3] * abb[4] +
                          abb[1] * (abb[3] + T(2) * abb[4]) + (abb[1] + T(-1) * abb[4]) * abb[5]) +
                abb[1] * (abb[1] * abb[3] + abb[4] * (T(-1) * abb[3] + abb[5]))) *
               abb[10] +
           (T(-1) / T(16)) * (T(-1) * abb[1] + T(-1) * abb[3] + abb[5]) *
               (abb[1] * abb[3] + abb[4] * (abb[3] + T(-1) * abb[5]) +
                abb[2] * (T(-1) * abb[1] + abb[5])) *
               abb[11] +
           (T(-1) / T(8)) * abb[4] * (T(-1) * abb[1] + T(-1) * abb[3] + abb[5]) *
               (abb[2] + T(-1) * abb[3] + abb[5]) * abb[12])};
}

template <typename T, size_t D>
std::vector<T> MasslessPentaBox_coordinates(const OnShellPoint<T, D> &l,
                                            const momD_conf<T, D> &mc) {
  return {levi(mc[1], mc[2], mc[3], mc[4]),
          mu(l[0], l[1]),
          mc.s(1, 2),
          sq(l[1] + mc[2]),
          trplus(mc[1], -mc[2] + l[0], l[0], l[1]),
          trplus(mc[3], -mc[3] + l[1], -mc[3] + -mc[4] + l[1], mc[5])};
}

template <typename T> T MasslessPentaBox_double_wiggly(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4] * abb[5];
}

template <typename T> T MasslessPentaBox_mu12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * abb[1] * abb[2];
}

template <typename T> T MasslessPentaBox_ls_mu12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * abb[1] * abb[3];
}

template <typename T, size_t D>
std::vector<T> MasslessPentaBubble_coordinates(const OnShellPoint<T, D> &l,
                                               const momD_conf<T, D> &mc) {
  return {levi(mc[1], mc[2], mc[3], mc[4]),
          mc.s(1, 2),
          mc.s(1, 5),
          mc.s(2, 3),
          mc.s(3, 4),
          mc.s(4, 5),
          sq(l[1]),
          sq(l[1] + mc[1]),
          sq(-mc[2] + l[1]),
          sq(l[1] + mc[1] + mc[5]),
          sq(l[1] + mc[1] + mc[4] + mc[5]),
          trminus(mc[2], -mc[2] + l[1], -mc[2] + -mc[3] + l[1], mc[4]),
          trplus(mc[2], -mc[2] + l[1], -mc[2] + -mc[3] + l[1], mc[4])};
}

template <typename T>
std::vector<T> MasslessPentaBubble_div(const TopologyCoordinates<T> &cs,
                                       const std::vector<T> &d_values) {
  const auto &abb = cs.special;
  T expr = (T(1) / T(2)) * (abb[11] + abb[12]);
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(-3) + d; };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T>
std::vector<T> MasslessPentaBubble_fin(const TopologyCoordinates<T> &cs,
                                       const std::vector<T> &d_values) {
  const auto &abb = cs.special;
  T expr =
      (T(1) / T(4)) * (T(1) / abb[0]) *
      (T(-1) * abb[1] * abb[2] * abb[3] * abb[4] + abb[1] * prod_pow(abb[3], 2) * abb[4] +
       T(-1) * prod_pow(abb[3], 2) * prod_pow(abb[4], 2) +
       T(-1) * abb[2] * abb[3] * abb[4] * abb[5] + abb[3] * prod_pow(abb[4], 2) * abb[5] +
       abb[1] * abb[2] * abb[4] * abb[6] + T(-1) * abb[1] * abb[3] * abb[4] * abb[6] +
       abb[3] * prod_pow(abb[4], 2) * abb[6] + T(-1) * abb[2] * abb[4] * abb[5] * abb[6] +
       T(2) * abb[3] * abb[4] * abb[5] * abb[6] + prod_pow(abb[4], 2) * abb[5] * abb[6] +
       T(2) * abb[2] * abb[3] * abb[4] * abb[7] + T(-2) * prod_pow(abb[3], 2) * abb[4] * abb[7] +
       T(-2) * abb[3] * prod_pow(abb[4], 2) * abb[7] +
       T(-1) * abb[1] * prod_pow(abb[2], 2) * abb[8] + T(2) * abb[1] * abb[2] * abb[3] * abb[8] +
       T(-1) * abb[1] * prod_pow(abb[3], 2) * abb[8] + T(-1) * abb[2] * abb[3] * abb[4] * abb[8] +
       prod_pow(abb[3], 2) * abb[4] * abb[8] + prod_pow(abb[2], 2) * abb[5] * abb[8] +
       T(-1) * abb[2] * abb[3] * abb[5] * abb[8] + T(-1) * abb[2] * abb[4] * abb[5] * abb[8] +
       T(-1) * abb[3] * abb[4] * abb[5] * abb[8] + T(-1) * abb[1] * abb[2] * abb[3] * abb[9] +
       abb[1] * prod_pow(abb[3], 2) * abb[9] + T(2) * abb[1] * abb[3] * abb[4] * abb[9] +
       prod_pow(abb[3], 2) * abb[4] * abb[9] + abb[2] * abb[3] * abb[5] * abb[9] +
       T(-1) * abb[3] * abb[4] * abb[5] * abb[9] + abb[1] * prod_pow(abb[2], 2) * abb[10] +
       T(-1) * abb[1] * abb[2] * abb[3] * abb[10] + T(-1) * abb[1] * abb[2] * abb[4] * abb[10] +
       T(-1) * abb[1] * abb[3] * abb[4] * abb[10] + T(-1) * abb[2] * abb[3] * abb[4] * abb[10] +
       abb[3] * prod_pow(abb[4], 2) * abb[10] + T(-1) * prod_pow(abb[2], 2) * abb[5] * abb[10] +
       T(2) * abb[2] * abb[4] * abb[5] * abb[10] + T(-1) * prod_pow(abb[4], 2) * abb[5] * abb[10]);
  std::vector<T> result(d_values.size(), expr);
  auto di = [](const T &d) { return T(-3) + d; };
  for (size_t i = 0; i < d_values.size(); i++) {
    result[i] *= di(d_values[i]);
  }
  return result;
}

template <typename T, size_t D>
std::vector<T> MasslessSSBoxBox_coordinates(const OnShellPoint<T, D> &l,
                                            const momD_conf<T, D> &mc) {
  return {levi(mc[1], mc[2], mc[3], mc[4]),
          mu(l[0], l[1]),
          mc.s(1, 2),
          mc.s(1, 5),
          mc.s(2, 3),
          mc.s(3, 4),
          mc.s(4, 5),
          sq(l[0]),
          sq(l[1]),
          sq(-mc[2] + l[0]),
          sq(-mc[1] + -mc[2] + l[0]),
          sq(l[0] + mc[3]),
          sq(l[1] + mc[3]),
          sq(l[0] + l[1] + mc[3]),
          sq(l[1] + mc[2] + mc[3]),
          sq(-mc[4] + l[1]),
          sq(l[0] + mc[3] + mc[4]),
          sq(-mc[4] + -mc[5] + l[1])};
}

template <typename T> T MasslessSSBoxBox_pure_finite_1(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[8] * ((T(-1) * abb[3] + abb[5]) * abb[10] + abb[2] * (abb[3] + T(-1) * abb[16])) +
         abb[6] * (abb[10] * (abb[3] + T(-1) * abb[5] + abb[12] + T(-1) * abb[14]) +
                   abb[2] * (T(-1) * abb[3] + T(-1) * abb[13] + abb[14] + abb[16])) +
         ((T(-1) * abb[3] + abb[4] + abb[5]) * abb[10] +
          abb[2] * (abb[3] + T(-1) * abb[4] + abb[11] + T(-1) * abb[16])) *
             abb[17] +
         abb[7] * (T(-1) * abb[3] * abb[8] + abb[6] * (abb[3] + T(-1) * abb[14]) +
                   (T(-1) * abb[3] + abb[4]) * abb[17]);
}

template <typename T> T MasslessSSBoxBox_pure_finite_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(1) / (abb[4] * abb[5] + abb[2] * (abb[3] + -abb[4]) + abb[6] * (abb[3] + -abb[5]))) *
         (T(-8) * prod_pow(abb[0], 2) * abb[1] +
          (abb[3] + T(-1) * abb[4] + T(-1) * abb[5]) *
              (abb[2] * abb[6] * abb[9] * abb[15] +
               T(-1) * abb[10] *
                   (abb[3] * abb[6] * abb[12] + T(-1) * abb[5] * abb[6] * abb[14] +
                    abb[4] * abb[5] * abb[17]) +
               abb[2] * (abb[3] * abb[6] * abb[13] + T(-1) * abb[6] * abb[14] * abb[16] +
                         (T(-1) * abb[3] * abb[11] + abb[4] * abb[16]) * abb[17])));
}

template <typename T> T MasslessSSBoxBox_pure_mu12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * abb[1];
}

template <typename T, size_t D>
std::vector<T> SSBoxTrg_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {mc.s(1, 2), mc.s(2, 3), sq(mc[4])};
}

template <typename T> T SSBoxTrg_std0(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * (abb[1] + T(-1) * abb[2]);
}

template <typename T, size_t D>
std::vector<T> SSTrgTrg_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(mc[1]), sq(mc[3])};
}

template <typename T> T SSTrgTrg_std0(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] + T(-1) * abb[1];
}

template <typename T, size_t D>
std::vector<T> Sunrise_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(mc[1])};
}

template <typename T> T Sunrise_std0(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return T(1) / abb[0];
}


inline void load() {
  if (verbosity_settings.report_integral_basis){
      std::cout<<"Loading 'Standard' master integrands"<<std::endl;
  }
  using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
{
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[], Leg[1, 0], Link[], "
                "Leg[2, 0], LoopMomentum[-1]], Strand[Link[], Leg[3, 0], LoopMomentum[-2]], "
                "Strand[Link[], Leg[4, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"Tensor1", "Tensor2"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = XBox_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {XBox_Tensor1<C>, XBox_Tensor2<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = XBox_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {XBox_Tensor1<CHP>, XBox_Tensor2<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = XBox_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {XBox_Tensor1<CVHP>, XBox_Tensor2<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = XBox_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {XBox_Tensor1<F32>, XBox_Tensor2<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[], Leg[1], Link[]], "
                "Strand[Link[], Leg[3, 0], LoopMomentum[-2]], Strand[Link[], Leg[2, 0], "
                "LoopMomentum[-1]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = np_oneMass_BoxBox_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {np_oneMass_BoxBox_scalar<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = np_oneMass_BoxBox_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {np_oneMass_BoxBox_scalar<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = np_oneMass_BoxBox_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {np_oneMass_BoxBox_scalar<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = np_oneMass_BoxBox_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {np_oneMass_BoxBox_scalar<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[Leg[4, 0]]], Connection[Strand[LoopMomentum[1], "
        "Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = BubbleBox_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {BubbleBox_scalar<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = BubbleBox_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BubbleBox_scalar<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = BubbleBox_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BubbleBox_scalar<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = BubbleBox_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {BubbleBox_scalar<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], "
                "Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], "
                "Link[], Leg[4, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar", "dbTensor"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = DoubleBox_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {DoubleBox_scalar<C>, DoubleBox_tensor<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = DoubleBox_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {DoubleBox_scalar<CHP>,
                                                        DoubleBox_tensor<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = DoubleBox_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {DoubleBox_scalar<CVHP>,
                                                         DoubleBox_tensor<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = DoubleBox_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {DoubleBox_scalar<F32>,
                                                        DoubleBox_tensor<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[2, 0]], Node[Leg[4, 0]]], Connection[Strand[LoopMomentum[1], "
        "Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = DoubleTri_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {DoubleTri_scalar<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = DoubleTri_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {DoubleTri_scalar<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = DoubleTri_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {DoubleTri_scalar<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = DoubleTri_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {DoubleTri_scalar<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[3, 0], "
                "Link[], Leg[2, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], "
                "Link[], Leg[1, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0", "std1"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = BoxBox4pt1M_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxBox4pt1M_std0<C>, BoxBox4pt1M_std1<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = BoxBox4pt1M_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxBox4pt1M_std0<CHP>,
                                                        BoxBox4pt1M_std1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = BoxBox4pt1M_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxBox4pt1M_std0<CVHP>,
                                                         BoxBox4pt1M_std1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = BoxBox4pt1M_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxBox4pt1M_std0<F32>,
                                                        BoxBox4pt1M_std1<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], "
                "Leg[2, 0], Link[], Leg[3, 0], Link[]], Strand[LoopMomentum[2], Leg[4], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = FactBoxBubble_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {FactBoxBubble_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = FactBoxBubble_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {FactBoxBubble_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = FactBoxBubble_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {FactBoxBubble_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = FactBoxBubble_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {FactBoxBubble_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], "
                "Strand[LoopMomentum[2], Leg[2], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = FactBubbleBubble_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {FactBubbleBubble_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = FactBubbleBubble_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {FactBubbleBubble_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = FactBubbleBubble_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {FactBubbleBubble_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = FactBubbleBubble_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {FactBubbleBubble_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[1], "
                "Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = FactSSBubbleBubble_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {FactSSBubbleBubble_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = FactSSBubbleBubble_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {FactSSBubbleBubble_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = FactSSBubbleBubble_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {FactSSBubbleBubble_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = FactSSBubbleBubble_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {FactSSBubbleBubble_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[Leg[4]]], Connection[Strand[LoopMomentum[1]], "
        "Strand[Link[]], Strand[LoopMomentum[2], Leg[2, 0], Link[], Leg[1, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = GenBoxBubble1Ma_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {GenBoxBubble1Ma_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = GenBoxBubble1Ma_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {GenBoxBubble1Ma_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = GenBoxBubble1Ma_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {GenBoxBubble1Ma_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = GenBoxBubble1Ma_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {GenBoxBubble1Ma_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1]], "
        "Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[], Leg[3, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = GenBoxBubble1Mb_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {GenBoxBubble1Mb_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = GenBoxBubble1Mb_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {GenBoxBubble1Mb_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = GenBoxBubble1Mb_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {GenBoxBubble1Mb_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = GenBoxBubble1Mb_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {GenBoxBubble1Mb_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3]]], Connection[Strand[LoopMomentum[1]], "
        "Strand[Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = GenTrgBubble2M_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {GenTrgBubble2M_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = GenTrgBubble2M_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {GenTrgBubble2M_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = GenTrgBubble2M_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {GenTrgBubble2M_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = GenTrgBubble2M_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {GenTrgBubble2M_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1]], "
        "Strand[Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = GenTrgBubble_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {GenTrgBubble_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = GenTrgBubble_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {GenTrgBubble_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = GenTrgBubble_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {GenTrgBubble_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = GenTrgBubble_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {GenTrgBubble_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], "
        "Leg[2, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"scalar", "dot"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = GenTrgTrg1Ma_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {GenTrgTrg1Ma_scalar<C>, GenTrgTrg1Ma_dot<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = GenTrgTrg1Ma_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {GenTrgTrg1Ma_scalar<CHP>,
                                                        GenTrgTrg1Ma_dot<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = GenTrgTrg1Ma_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {GenTrgTrg1Ma_scalar<CVHP>,
                                                         GenTrgTrg1Ma_dot<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = GenTrgTrg1Ma_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {GenTrgTrg1Ma_scalar<F32>,
                                                        GenTrgTrg1Ma_dot<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[2, 0]], Node[Leg[4]]], Connection[Strand[LoopMomentum[1], "
        "Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = GenTrgTrg1Mb_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {GenTrgTrg1Mb_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = GenTrgTrg1Mb_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {GenTrgTrg1Mb_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = GenTrgTrg1Mb_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {GenTrgTrg1Mb_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = GenTrgTrg1Mb_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {GenTrgTrg1Mb_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[2, 0]], Node[Leg[5, 0]]], "
                "Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[Link[]], "
                "Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"div", "fin"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = MasslessGenBoxTriang_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {MasslessGenBoxTriang_div<C>,
                                                      MasslessGenBoxTriang_fin<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = MasslessGenBoxTriang_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {MasslessGenBoxTriang_div<CHP>,
                                                        MasslessGenBoxTriang_fin<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = MasslessGenBoxTriang_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {MasslessGenBoxTriang_div<CVHP>,
                                                         MasslessGenBoxTriang_fin<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = MasslessGenBoxTriang_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {MasslessGenBoxTriang_div<F32>,
                                                        MasslessGenBoxTriang_fin<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], "
                "Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], "
                "Link[], Leg[4, 0], Link[], Leg[5, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"double_wiggly", "mu12", "ls_mu12"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = MasslessPentaBox_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        MasslessPentaBox_double_wiggly<C>, MasslessPentaBox_mu12<C>, MasslessPentaBox_ls_mu12<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = MasslessPentaBox_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {MasslessPentaBox_double_wiggly<CHP>,
                                                        MasslessPentaBox_mu12<CHP>,
                                                        MasslessPentaBox_ls_mu12<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = MasslessPentaBox_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {MasslessPentaBox_double_wiggly<CVHP>,
                                                         MasslessPentaBox_mu12<CVHP>,
                                                         MasslessPentaBox_ls_mu12<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = MasslessPentaBox_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {MasslessPentaBox_double_wiggly<F32>,
                                                        MasslessPentaBox_mu12<F32>,
                                                        MasslessPentaBox_ls_mu12<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[5, 0]]], "
                "Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2], "
                "Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"div", "fin"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = MasslessPentaBubble_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {MasslessPentaBubble_div<C>,
                                                      MasslessPentaBubble_fin<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = MasslessPentaBubble_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {MasslessPentaBubble_div<CHP>,
                                                        MasslessPentaBubble_fin<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = MasslessPentaBubble_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {MasslessPentaBubble_div<CVHP>,
                                                         MasslessPentaBubble_fin<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = MasslessPentaBubble_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {MasslessPentaBubble_div<F32>,
                                                        MasslessPentaBubble_fin<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]], Node[]], Connection[Strand[LoopMomentum[1], "
                "Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], "
                "Leg[4, 0], Link[], Leg[5, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"pure_finite_1", "pure_finite_2", "pure_mu12"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = MasslessSSBoxBox_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {MasslessSSBoxBox_pure_finite_1<C>,
                                                      MasslessSSBoxBox_pure_finite_2<C>,
                                                      MasslessSSBoxBox_pure_mu12<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = MasslessSSBoxBox_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {MasslessSSBoxBox_pure_finite_1<CHP>,
                                                        MasslessSSBoxBox_pure_finite_2<CHP>,
                                                        MasslessSSBoxBox_pure_mu12<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = MasslessSSBoxBox_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {MasslessSSBoxBox_pure_finite_1<CVHP>,
                                                         MasslessSSBoxBox_pure_finite_2<CVHP>,
                                                         MasslessSSBoxBox_pure_mu12<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = MasslessSSBoxBox_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {MasslessSSBoxBox_pure_finite_1<F32>,
                                                        MasslessSSBoxBox_pure_finite_2<F32>,
                                                        MasslessSSBoxBox_pure_mu12<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[]], Connection[Strand[LoopMomentum[1], Leg[4], "
        "Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[2, 0], Link[], Leg[1, 0], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = SSBoxTrg_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {SSBoxTrg_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = SSBoxTrg_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {SSBoxTrg_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = SSBoxTrg_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {SSBoxTrg_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = SSBoxTrg_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {SSBoxTrg_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[2, 0]], Node[]], Connection[Strand[LoopMomentum[1], "
                "Leg[1], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3], Link[]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = SSTrgTrg_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {SSTrgTrg_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = SSTrgTrg_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {SSTrgTrg_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = SSTrgTrg_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {SSTrgTrg_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = SSTrgTrg_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {SSTrgTrg_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2]]], "
                "Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"std0"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = Sunrise_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {Sunrise_std0<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = Sunrise_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Sunrise_std0<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = Sunrise_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Sunrise_std0<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = Sunrise_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {Sunrise_std0<F32>};
#endif
    master_term_data_holder[graph.filter()] = std::move(mdata);
  }
}

} // namespace Standard
} // namespace MasterIntegrands
} // namespace FunctionSpace
} // namespace Caravel
