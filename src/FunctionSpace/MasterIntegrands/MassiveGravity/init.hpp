/*
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms/CaravelGraph/Integrands.m

  */

#include "FunctionSpace/FunctionSpace.h"

#include "FunctionSpace/MasterIntegrands.h"

namespace Caravel {
namespace FunctionSpace {
namespace MasterIntegrands {
namespace MassiveGravity {

using namespace MasterIntegrands;

template <typename T, size_t D>
std::vector<T> g11_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(mc[1]), sq(-mc[4] + l[1]), sq(-mc[1] + -mc[4] + l[0]), sq(-mc[1] + -mc[4] + l[1])};
}

template <typename T> T g11_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T g11_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[3];
}

template <typename T> T g11_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2];
}

template <typename T> T g11_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return T(-1) * abb[0] + abb[1];
}

template <typename T> T g11_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * abb[3];
}

template <typename T> T g11_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2);
}

template <typename T> T g11_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * abb[3];
}

template <typename T> T g11_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * abb[2];
}

template <typename T> T g11_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2);
}

template <typename T> T g11_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T g11_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3);
}

template <typename T> T g11_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * abb[2] * abb[3];
}

template <typename T> T g11_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 2);
}

template <typename T> T g11_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * abb[3];
}

template <typename T> T g11_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * abb[2];
}

template <typename T> T g11_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3);
}

template <typename T> T g11_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T g11_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4);
}

template <typename T> T g11_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T g11_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 3);
}

template <typename T> T g11_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * abb[2] * abb[3];
}

template <typename T> T g11_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 2);
}

template <typename T> T g11_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * abb[3];
}

template <typename T> T g11_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * abb[2];
}

template <typename T> T g11_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4);
}

template <typename T> T g11_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_40(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T g11_ten_41(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5);
}

template <typename T> T g11_ten_42(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_43(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_44(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_45(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T g11_ten_46(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 4);
}

template <typename T> T g11_ten_47(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_48(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_49(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T g11_ten_50(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 3);
}

template <typename T> T g11_ten_51(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_52(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * abb[2] * abb[3];
}

template <typename T> T g11_ten_53(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 2);
}

template <typename T> T g11_ten_54(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * abb[3];
}

template <typename T> T g11_ten_55(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * abb[2];
}

template <typename T> T g11_ten_56(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5);
}

template <typename T> T g11_ten_57(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 6);
}

template <typename T> T g11_ten_58(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_59(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_60(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_61(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_62(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T g11_ten_63(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6);
}

template <typename T> T g11_ten_64(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_65(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_66(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_67(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_68(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T g11_ten_69(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 5);
}

template <typename T> T g11_ten_70(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_71(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_72(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_73(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T g11_ten_74(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 4);
}

template <typename T> T g11_ten_75(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_76(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_77(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T g11_ten_78(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 3);
}

template <typename T> T g11_ten_79(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_80(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * abb[2] * abb[3];
}

template <typename T> T g11_ten_81(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 2);
}

template <typename T> T g11_ten_82(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * abb[3];
}

template <typename T> T g11_ten_83(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * abb[2];
}

template <typename T> T g11_ten_84(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 6);
}

template <typename T> T g11_ten_85(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 6);
}

template <typename T> T g11_ten_86(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_87(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_88(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_89(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_90(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6) * abb[3];
}

template <typename T> T g11_ten_91(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7);
}

template <typename T> T g11_ten_92(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * abb[2] * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_93(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_94(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_95(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_96(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T g11_ten_97(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 6);
}

template <typename T> T g11_ten_98(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_99(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_100(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_101(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T g11_ten_102(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 5);
}

template <typename T> T g11_ten_103(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_104(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_105(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T g11_ten_106(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 4);
}

template <typename T> T g11_ten_107(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_108(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T g11_ten_109(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 3);
}

template <typename T> T g11_ten_110(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * abb[2] * abb[3];
}

template <typename T> T g11_ten_111(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * prod_pow(abb[2], 2);
}

template <typename T> T g11_ten_112(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 6) * abb[2];
}

template <typename T> T g11_ten_113(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 6);
}

template <typename T> T g11_ten_114(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_115(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_116(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_117(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_118(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7) * abb[3];
}

template <typename T> T g11_ten_119(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 8);
}

template <typename T> T g11_ten_120(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_121(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_122(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 4) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_123(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 5) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_124(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 6) * abb[3];
}

template <typename T> T g11_ten_125(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 7);
}

template <typename T> T g11_ten_126(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 2) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_127(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_128(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_129(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T g11_ten_130(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 6);
}

template <typename T> T g11_ten_131(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_132(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_133(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T g11_ten_134(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 5);
}

template <typename T> T g11_ten_135(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_136(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T g11_ten_137(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 4);
}

template <typename T> T g11_ten_138(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T g11_ten_139(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * prod_pow(abb[2], 3);
}

template <typename T> T g11_ten_140(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 6) * prod_pow(abb[2], 2);
}

template <typename T> T g11_ten_141(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 6);
}

template <typename T> T g11_ten_142(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_143(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_144(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_145(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_146(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 8) * abb[3];
}

template <typename T> T g11_ten_147(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_148(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 4) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_149(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 5) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_150(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 6) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_151(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 7) * abb[3];
}

template <typename T> T g11_ten_152(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 8);
}

template <typename T> T g11_ten_153(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 3) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_154(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 4) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_155(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 5) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_156(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 6) * abb[3];
}

template <typename T> T g11_ten_157(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 7);
}

template <typename T> T g11_ten_158(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_159(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_160(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T g11_ten_161(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 6);
}

template <typename T> T g11_ten_162(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_163(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T g11_ten_164(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 5);
}

template <typename T> T g11_ten_165(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T g11_ten_166(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * prod_pow(abb[2], 4);
}

template <typename T> T g11_ten_167(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 6) * prod_pow(abb[2], 3);
}

template <typename T> T g11_ten_168(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 6);
}

template <typename T> T g11_ten_169(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_170(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_171(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_172(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 8) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_173(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 4) * prod_pow(abb[3], 5);
}

template <typename T> T g11_ten_174(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 5) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_175(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 6) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_176(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 7) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_177(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 8) * abb[3];
}

template <typename T> T g11_ten_178(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 4) * prod_pow(abb[3], 4);
}

template <typename T> T g11_ten_179(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 5) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_180(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 6) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_181(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 7) * abb[3];
}

template <typename T> T g11_ten_182(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 8);
}

template <typename T> T g11_ten_183(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 4) * prod_pow(abb[3], 3);
}

template <typename T> T g11_ten_184(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 5) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_185(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 6) * abb[3];
}

template <typename T> T g11_ten_186(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 7);
}

template <typename T> T g11_ten_187(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T g11_ten_188(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T g11_ten_189(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 6);
}

template <typename T> T g11_ten_190(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T g11_ten_191(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * prod_pow(abb[2], 5);
}

template <typename T> T g11_ten_192(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 6) * prod_pow(abb[2], 4);
}

template <typename T, size_t D>
std::vector<T> g12_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(-mc[1] + l[0]), sq(mc[1]), sq(mc[2]), sq(l[1] + mc[3]), sq(-mc[1] + -mc[4] + l[1])};
}

template <typename T> T g12_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T g12_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4];
}

template <typename T> T g12_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return T(-1) * abb[2] + abb[3];
}

template <typename T> T g12_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] + T(-1) * abb[1];
}

template <typename T> T g12_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T g12_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[4];
}

template <typename T> T g12_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T g12_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2);
}

template <typename T> T g12_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 2) * abb[4];
}

template <typename T> T g12_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T g12_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T g12_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[4];
}

template <typename T> T g12_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T g12_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3);
}

template <typename T> T g12_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 3) * abb[4];
}

template <typename T> T g12_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T g12_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 2) * abb[4];
}

template <typename T> T g12_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T g12_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T g12_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[4];
}

template <typename T> T g12_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T g12_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4);
}

template <typename T> T g12_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_40(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 4) * abb[4];
}

template <typename T> T g12_ten_41(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T g12_ten_42(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_43(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_44(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_45(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 3) * abb[4];
}

template <typename T> T g12_ten_46(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T g12_ten_47(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_48(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_49(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 2) * abb[4];
}

template <typename T> T g12_ten_50(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T g12_ten_51(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_52(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_53(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T g12_ten_54(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[4];
}

template <typename T> T g12_ten_55(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T g12_ten_56(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5);
}

template <typename T> T g12_ten_57(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 6);
}

template <typename T> T g12_ten_58(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_59(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_60(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_61(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 4) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_62(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 5) * abb[4];
}

template <typename T> T g12_ten_63(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T g12_ten_64(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_65(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_66(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_67(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_68(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 4) * abb[4];
}

template <typename T> T g12_ten_69(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T g12_ten_70(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_71(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_72(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_73(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 3) * abb[4];
}

template <typename T> T g12_ten_74(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T g12_ten_75(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_76(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_77(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 2) * abb[4];
}

template <typename T> T g12_ten_78(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T g12_ten_79(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_80(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_81(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T g12_ten_82(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * abb[4];
}

template <typename T> T g12_ten_83(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T g12_ten_84(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6);
}

template <typename T> T g12_ten_85(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[4], 6);
}

template <typename T> T g12_ten_86(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_87(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_88(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_89(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 4) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_90(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 5) * abb[4];
}

template <typename T> T g12_ten_91(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T g12_ten_92(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_93(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_94(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_95(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_96(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 4) * abb[4];
}

template <typename T> T g12_ten_97(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T g12_ten_98(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_99(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_100(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_101(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 3) * abb[4];
}

template <typename T> T g12_ten_102(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T g12_ten_103(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_104(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_105(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 2) * abb[4];
}

template <typename T> T g12_ten_106(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T g12_ten_107(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_108(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_109(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T g12_ten_110(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * abb[4];
}

template <typename T> T g12_ten_111(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T g12_ten_112(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7);
}

template <typename T> T g12_ten_113(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[4], 6);
}

template <typename T> T g12_ten_114(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_115(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_116(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_117(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 4) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_118(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 5) * abb[4];
}

template <typename T> T g12_ten_119(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T g12_ten_120(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_121(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_122(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_123(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_124(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 4) * abb[4];
}

template <typename T> T g12_ten_125(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T g12_ten_126(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_127(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_128(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_129(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 3) * abb[4];
}

template <typename T> T g12_ten_130(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T g12_ten_131(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_132(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_133(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 2) * abb[4];
}

template <typename T> T g12_ten_134(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T g12_ten_135(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_136(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_137(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T g12_ten_138(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * abb[4];
}

template <typename T> T g12_ten_139(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T g12_ten_140(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8);
}

template <typename T> T g12_ten_141(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[4], 6);
}

template <typename T> T g12_ten_142(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_143(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_144(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_145(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 4) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_146(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 5) * abb[4];
}

template <typename T> T g12_ten_147(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T g12_ten_148(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_149(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_150(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_151(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_152(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 4) * abb[4];
}

template <typename T> T g12_ten_153(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T g12_ten_154(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_155(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_156(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_157(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 3) * abb[4];
}

template <typename T> T g12_ten_158(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T g12_ten_159(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_160(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_161(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 2) * abb[4];
}

template <typename T> T g12_ten_162(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T g12_ten_163(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_164(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_165(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T g12_ten_166(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * abb[4];
}

template <typename T> T g12_ten_167(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T g12_ten_168(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[4], 6);
}

template <typename T> T g12_ten_169(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_170(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_171(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_172(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 4) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_173(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 5) * abb[4];
}

template <typename T> T g12_ten_174(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T g12_ten_175(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[4], 5);
}

template <typename T> T g12_ten_176(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_177(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_178(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 3) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_179(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 4) * abb[4];
}

template <typename T> T g12_ten_180(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T g12_ten_181(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[4], 4);
}

template <typename T> T g12_ten_182(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_183(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 2) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_184(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 3) * abb[4];
}

template <typename T> T g12_ten_185(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T g12_ten_186(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[4], 3);
}

template <typename T> T g12_ten_187(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * (T(-1) * abb[2] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_188(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3] + -abb[2], 2) * abb[4];
}

template <typename T> T g12_ten_189(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T g12_ten_190(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * prod_pow(abb[4], 2);
}

template <typename T> T g12_ten_191(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * (T(-1) * abb[2] + abb[3]) * abb[4];
}

template <typename T> T g12_ten_192(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T, size_t D>
std::vector<T> H_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(-mc[1] + l[0]), sq(mc[1]), sq(mc[2]), sq(l[1] + mc[3])};
}

template <typename T> T H_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T H_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return T(-1) * abb[2] + abb[3];
}

template <typename T> T H_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] + T(-1) * abb[1];
}

template <typename T> T H_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T H_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2);
}

template <typename T> T H_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T H_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3);
}

template <typename T> T H_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T H_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T H_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4);
}

template <typename T> T H_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T H_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T H_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T H_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5);
}

template <typename T> T H_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T H_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T H_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T H_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T H_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6);
}

template <typename T> T H_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 7);
}

template <typename T> T H_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T H_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T H_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T H_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T H_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7);
}

template <typename T> T H_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[2], 8);
}

template <typename T> T H_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 7);
}

template <typename T> T H_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T H_ten_40(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T H_ten_41(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T H_ten_42(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_43(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_44(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T H_ten_45(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8);
}

template <typename T> T H_ten_46(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3] + -abb[2], 8);
}

template <typename T> T H_ten_47(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 7);
}

template <typename T> T H_ten_48(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T H_ten_49(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T H_ten_50(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T H_ten_51(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_52(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_53(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * (T(-1) * abb[2] + abb[3]);
}

template <typename T> T H_ten_54(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3] + -abb[2], 8);
}

template <typename T> T H_ten_55(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 7);
}

template <typename T> T H_ten_56(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T H_ten_57(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T H_ten_58(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T H_ten_59(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_60(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * prod_pow(abb[3] + -abb[2], 2);
}

template <typename T> T H_ten_61(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3] + -abb[2], 8);
}

template <typename T> T H_ten_62(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 7);
}

template <typename T> T H_ten_63(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T H_ten_64(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T H_ten_65(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T> T H_ten_66(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * prod_pow(abb[3] + -abb[2], 3);
}

template <typename T> T H_ten_67(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3] + -abb[2], 8);
}

template <typename T> T H_ten_68(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3] + -abb[2], 7);
}

template <typename T> T H_ten_69(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3] + -abb[2], 6);
}

template <typename T> T H_ten_70(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3] + -abb[2], 5);
}

template <typename T> T H_ten_71(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * prod_pow(abb[3] + -abb[2], 4);
}

template <typename T, size_t D>
std::vector<T> III_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(-mc[1] + -mc[4] + l[0]), sq(-mc[1] + -mc[4] + l[1])};
}

template <typename T> T III_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T III_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[1];
}

template <typename T> T III_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0];
}

template <typename T> T III_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 2);
}

template <typename T> T III_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * abb[1];
}

template <typename T> T III_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2);
}

template <typename T> T III_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 3);
}

template <typename T> T III_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 2);
}

template <typename T> T III_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * abb[1];
}

template <typename T> T III_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3);
}

template <typename T> T III_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 4);
}

template <typename T> T III_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 3);
}

template <typename T> T III_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 2);
}

template <typename T> T III_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * abb[1];
}

template <typename T> T III_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4);
}

template <typename T> T III_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 5);
}

template <typename T> T III_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 4);
}

template <typename T> T III_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 3);
}

template <typename T> T III_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 2);
}

template <typename T> T III_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * abb[1];
}

template <typename T> T III_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5);
}

template <typename T> T III_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 6);
}

template <typename T> T III_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 5);
}

template <typename T> T III_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 4);
}

template <typename T> T III_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 3);
}

template <typename T> T III_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 2);
}

template <typename T> T III_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * abb[1];
}

template <typename T> T III_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6);
}

template <typename T> T III_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 7);
}

template <typename T> T III_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 6);
}

template <typename T> T III_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 5);
}

template <typename T> T III_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 4);
}

template <typename T> T III_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 3);
}

template <typename T> T III_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 2);
}

template <typename T> T III_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * abb[1];
}

template <typename T> T III_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7);
}

template <typename T> T III_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 8);
}

template <typename T> T III_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 7);
}

template <typename T> T III_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 6);
}

template <typename T> T III_ten_40(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 5);
}

template <typename T> T III_ten_41(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 4);
}

template <typename T> T III_ten_42(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 3);
}

template <typename T> T III_ten_43(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 2);
}

template <typename T> T III_ten_44(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * abb[1];
}

template <typename T> T III_ten_45(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8);
}

template <typename T> T III_ten_46(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 8);
}

template <typename T> T III_ten_47(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 7);
}

template <typename T> T III_ten_48(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 6);
}

template <typename T> T III_ten_49(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 5);
}

template <typename T> T III_ten_50(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 4);
}

template <typename T> T III_ten_51(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 3);
}

template <typename T> T III_ten_52(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * prod_pow(abb[1], 2);
}

template <typename T> T III_ten_53(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8) * abb[1];
}

template <typename T> T III_ten_54(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 8);
}

template <typename T> T III_ten_55(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 7);
}

template <typename T> T III_ten_56(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 6);
}

template <typename T> T III_ten_57(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 5);
}

template <typename T> T III_ten_58(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 4);
}

template <typename T> T III_ten_59(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * prod_pow(abb[1], 3);
}

template <typename T> T III_ten_60(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8) * prod_pow(abb[1], 2);
}

template <typename T> T III_ten_61(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 8);
}

template <typename T> T III_ten_62(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 7);
}

template <typename T> T III_ten_63(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 6);
}

template <typename T> T III_ten_64(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 5);
}

template <typename T> T III_ten_65(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * prod_pow(abb[1], 4);
}

template <typename T> T III_ten_66(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8) * prod_pow(abb[1], 3);
}

template <typename T> T III_ten_67(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 8);
}

template <typename T> T III_ten_68(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 7);
}

template <typename T> T III_ten_69(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 6);
}

template <typename T> T III_ten_70(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * prod_pow(abb[1], 5);
}

template <typename T> T III_ten_71(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8) * prod_pow(abb[1], 4);
}

template <typename T, size_t D>
std::vector<T> IX_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(-mc[1] + -mc[4] + l[0]), sq(-mc[1] + -mc[4] + l[1])};
}

template <typename T> T IX_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T IX_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[1];
}

template <typename T> T IX_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0];
}

template <typename T> T IX_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * abb[1];
}

template <typename T> T IX_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2);
}

template <typename T> T IX_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * abb[1];
}

template <typename T> T IX_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3);
}

template <typename T> T IX_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * abb[1];
}

template <typename T> T IX_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4);
}

template <typename T> T IX_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 5);
}

template <typename T> T IX_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * abb[1];
}

template <typename T> T IX_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5);
}

template <typename T> T IX_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 6);
}

template <typename T> T IX_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 5);
}

template <typename T> T IX_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * abb[1];
}

template <typename T> T IX_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6);
}

template <typename T> T IX_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 7);
}

template <typename T> T IX_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 6);
}

template <typename T> T IX_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 5);
}

template <typename T> T IX_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * abb[1];
}

template <typename T> T IX_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7);
}

template <typename T> T IX_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 8);
}

template <typename T> T IX_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 7);
}

template <typename T> T IX_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 6);
}

template <typename T> T IX_ten_40(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 5);
}

template <typename T> T IX_ten_41(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_42(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_43(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_44(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * abb[1];
}

template <typename T> T IX_ten_45(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8);
}

template <typename T> T IX_ten_46(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 8);
}

template <typename T> T IX_ten_47(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 7);
}

template <typename T> T IX_ten_48(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 6);
}

template <typename T> T IX_ten_49(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 5);
}

template <typename T> T IX_ten_50(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_51(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_52(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_53(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8) * abb[1];
}

template <typename T> T IX_ten_54(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 9);
}

template <typename T> T IX_ten_55(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 8);
}

template <typename T> T IX_ten_56(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 7);
}

template <typename T> T IX_ten_57(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 6);
}

template <typename T> T IX_ten_58(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 5);
}

template <typename T> T IX_ten_59(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_60(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_61(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8) * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_62(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 9) * abb[1];
}

template <typename T> T IX_ten_63(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 10);
}

template <typename T> T IX_ten_64(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 8);
}

template <typename T> T IX_ten_65(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 7);
}

template <typename T> T IX_ten_66(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 6);
}

template <typename T> T IX_ten_67(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 5);
}

template <typename T> T IX_ten_68(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_69(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8) * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_70(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 9) * prod_pow(abb[1], 2);
}

template <typename T> T IX_ten_71(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 10) * abb[1];
}

template <typename T> T IX_ten_72(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 8);
}

template <typename T> T IX_ten_73(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 7);
}

template <typename T> T IX_ten_74(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 6);
}

template <typename T> T IX_ten_75(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7) * prod_pow(abb[1], 5);
}

template <typename T> T IX_ten_76(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8) * prod_pow(abb[1], 4);
}

template <typename T> T IX_ten_77(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 9) * prod_pow(abb[1], 3);
}

template <typename T> T IX_ten_78(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 10) * prod_pow(abb[1], 2);
}

template <typename T, size_t D>
std::vector<T> IX_BoxTri_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(-mc[1] + l[0]), sq(mc[1]), sq(-mc[1] + -mc[4] + l[0]), sq(-mc[1] + -mc[4] + l[1])};
}

template <typename T> T IX_BoxTri_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T IX_BoxTri_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[3];
}

template <typename T> T IX_BoxTri_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2];
}

template <typename T> T IX_BoxTri_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] + T(-1) * abb[1];
}

template <typename T> T IX_BoxTri_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * abb[3];
}

template <typename T> T IX_BoxTri_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2);
}

template <typename T> T IX_BoxTri_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[3];
}

template <typename T> T IX_BoxTri_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2];
}

template <typename T> T IX_BoxTri_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2);
}

template <typename T> T IX_BoxTri_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T IX_BoxTri_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3);
}

template <typename T> T IX_BoxTri_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2] * abb[3];
}

template <typename T> T IX_BoxTri_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2);
}

template <typename T> T IX_BoxTri_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[3];
}

template <typename T> T IX_BoxTri_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2];
}

template <typename T> T IX_BoxTri_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3);
}

template <typename T> T IX_BoxTri_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T IX_BoxTri_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4);
}

template <typename T> T IX_BoxTri_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T IX_BoxTri_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 3);
}

template <typename T> T IX_BoxTri_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2] * abb[3];
}

template <typename T> T IX_BoxTri_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 2);
}

template <typename T> T IX_BoxTri_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[3];
}

template <typename T> T IX_BoxTri_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[2];
}

template <typename T> T IX_BoxTri_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4);
}

template <typename T> T IX_BoxTri_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_40(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T IX_BoxTri_ten_41(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5);
}

template <typename T> T IX_BoxTri_ten_42(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_43(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_44(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_45(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T IX_BoxTri_ten_46(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 4);
}

template <typename T> T IX_BoxTri_ten_47(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_48(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_49(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T IX_BoxTri_ten_50(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 3);
}

template <typename T> T IX_BoxTri_ten_51(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_52(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[2] * abb[3];
}

template <typename T> T IX_BoxTri_ten_53(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 2);
}

template <typename T> T IX_BoxTri_ten_54(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[3];
}

template <typename T> T IX_BoxTri_ten_55(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[2];
}

template <typename T> T IX_BoxTri_ten_56(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5);
}

template <typename T> T IX_BoxTri_ten_57(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_58(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_59(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_60(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_61(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_62(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T IX_BoxTri_ten_63(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6);
}

template <typename T> T IX_BoxTri_ten_64(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_65(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_66(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_67(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_68(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T IX_BoxTri_ten_69(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 5);
}

template <typename T> T IX_BoxTri_ten_70(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_71(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_72(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_73(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T IX_BoxTri_ten_74(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 4);
}

template <typename T> T IX_BoxTri_ten_75(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_76(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_77(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T IX_BoxTri_ten_78(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 3);
}

template <typename T> T IX_BoxTri_ten_79(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_80(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[2] * abb[3];
}

template <typename T> T IX_BoxTri_ten_81(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 2);
}

template <typename T> T IX_BoxTri_ten_82(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * abb[3];
}

template <typename T> T IX_BoxTri_ten_83(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * abb[2];
}

template <typename T> T IX_BoxTri_ten_84(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6);
}

template <typename T> T IX_BoxTri_ten_85(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_86(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_87(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_88(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_89(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_90(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_91(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6) * abb[3];
}

template <typename T> T IX_BoxTri_ten_92(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7);
}

template <typename T> T IX_BoxTri_ten_93(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_94(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2] * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_95(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_96(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_97(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_98(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T IX_BoxTri_ten_99(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 6);
}

template <typename T> T IX_BoxTri_ten_100(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_101(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_102(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_103(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_104(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T IX_BoxTri_ten_105(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 5);
}

template <typename T> T IX_BoxTri_ten_106(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_107(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_108(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_109(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T IX_BoxTri_ten_110(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 4);
}

template <typename T> T IX_BoxTri_ten_111(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_112(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_113(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T IX_BoxTri_ten_114(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 3);
}

template <typename T> T IX_BoxTri_ten_115(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_116(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * abb[2] * abb[3];
}

template <typename T> T IX_BoxTri_ten_117(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 2);
}

template <typename T> T IX_BoxTri_ten_118(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * abb[3];
}

template <typename T> T IX_BoxTri_ten_119(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * abb[2];
}

template <typename T> T IX_BoxTri_ten_120(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7);
}

template <typename T> T IX_BoxTri_ten_121(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3], 8);
}

template <typename T> T IX_BoxTri_ten_122(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_123(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_124(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_125(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_126(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_127(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_128(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7) * abb[3];
}

template <typename T> T IX_BoxTri_ten_129(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 8);
}

template <typename T> T IX_BoxTri_ten_130(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_131(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2] * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_132(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_133(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_134(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 4) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_135(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 5) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_136(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 6) * abb[3];
}

template <typename T> T IX_BoxTri_ten_137(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 7);
}

template <typename T> T IX_BoxTri_ten_138(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_139(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2] * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_140(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 2) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_141(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_142(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_143(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T IX_BoxTri_ten_144(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 6);
}

template <typename T> T IX_BoxTri_ten_145(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_146(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_147(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_148(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_149(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T IX_BoxTri_ten_150(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 5);
}

template <typename T> T IX_BoxTri_ten_151(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_152(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_153(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_154(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T IX_BoxTri_ten_155(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 4);
}

template <typename T> T IX_BoxTri_ten_156(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_157(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_158(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T IX_BoxTri_ten_159(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 3);
}

template <typename T> T IX_BoxTri_ten_160(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_161(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * abb[2] * abb[3];
}

template <typename T> T IX_BoxTri_ten_162(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[2], 2);
}

template <typename T> T IX_BoxTri_ten_163(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * abb[3];
}

template <typename T> T IX_BoxTri_ten_164(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * abb[2];
}

template <typename T> T IX_BoxTri_ten_165(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8);
}

template <typename T> T IX_BoxTri_ten_166(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2] * prod_pow(abb[3], 8);
}

template <typename T> T IX_BoxTri_ten_167(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_168(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_169(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_170(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_171(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_172(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_173(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 8) * abb[3];
}

template <typename T> T IX_BoxTri_ten_174(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[3], 8);
}

template <typename T> T IX_BoxTri_ten_175(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2] * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_176(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_177(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_178(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 4) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_179(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 5) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_180(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 6) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_181(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 7) * abb[3];
}

template <typename T> T IX_BoxTri_ten_182(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_183(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2] * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_184(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 2) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_185(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 3) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_186(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 4) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_187(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 5) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_188(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 6) * abb[3];
}

template <typename T> T IX_BoxTri_ten_189(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_190(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[2] * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_191(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 2) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_192(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_193(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_194(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 5) * abb[3];
}

template <typename T> T IX_BoxTri_ten_195(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_196(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_197(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_198(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_199(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 4) * abb[3];
}

template <typename T> T IX_BoxTri_ten_200(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_201(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_202(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_203(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 3) * abb[3];
}

template <typename T> T IX_BoxTri_ten_204(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_205(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_206(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[2], 2) * abb[3];
}

template <typename T> T IX_BoxTri_ten_207(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_208(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * abb[2] * abb[3];
}

template <typename T> T IX_BoxTri_ten_209(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * abb[3];
}

template <typename T> T IX_BoxTri_ten_210(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2) * prod_pow(abb[3], 8);
}

template <typename T> T IX_BoxTri_ten_211(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3) * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_212(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_213(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_214(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_215(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_216(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 8) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_217(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2] * prod_pow(abb[3], 8);
}

template <typename T> T IX_BoxTri_ten_218(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2) * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_219(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 3) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_220(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 4) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_221(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 5) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_222(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 6) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_223(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 7) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_224(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[3], 8);
}

template <typename T> T IX_BoxTri_ten_225(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2] * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_226(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 2) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_227(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 3) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_228(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 4) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_229(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 5) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_230(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 6) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_231(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[3], 7);
}

template <typename T> T IX_BoxTri_ten_232(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[2] * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_233(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 2) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_234(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 3) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_235(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 4) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_236(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 5) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_237(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[3], 6);
}

template <typename T> T IX_BoxTri_ten_238(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[2] * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_239(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 2) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_240(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 3) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_241(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 4) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_242(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[3], 5);
}

template <typename T> T IX_BoxTri_ten_243(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * abb[2] * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_244(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 2) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_245(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 3) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_246(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[3], 4);
}

template <typename T> T IX_BoxTri_ten_247(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * abb[2] * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_248(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[2], 2) * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_249(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * prod_pow(abb[3], 3);
}

template <typename T> T IX_BoxTri_ten_250(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * abb[2] * prod_pow(abb[3], 2);
}

template <typename T> T IX_BoxTri_ten_251(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8) * prod_pow(abb[3], 2);
}

template <typename T, size_t D>
std::vector<T> IY_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(mc[2]), sq(l[1] + mc[3]), sq(-mc[1] + -mc[4] + l[1])};
}

template <typename T> T IY_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T IY_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2];
}

template <typename T> T IY_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return T(-1) * abb[0] + abb[1];
}

template <typename T> T IY_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2);
}

template <typename T> T IY_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * abb[2];
}

template <typename T> T IY_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2);
}

template <typename T> T IY_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3);
}

template <typename T> T IY_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 2);
}

template <typename T> T IY_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * abb[2];
}

template <typename T> T IY_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3);
}

template <typename T> T IY_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4);
}

template <typename T> T IY_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 3);
}

template <typename T> T IY_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 2);
}

template <typename T> T IY_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * abb[2];
}

template <typename T> T IY_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4);
}

template <typename T> T IY_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5);
}

template <typename T> T IY_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 4);
}

template <typename T> T IY_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 3);
}

template <typename T> T IY_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 2);
}

template <typename T> T IY_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * abb[2];
}

template <typename T> T IY_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5);
}

template <typename T> T IY_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6);
}

template <typename T> T IY_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[1]) * prod_pow(abb[2], 5);
}

template <typename T> T IY_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 2) * prod_pow(abb[2], 4);
}

template <typename T> T IY_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 3) * prod_pow(abb[2], 3);
}

template <typename T> T IY_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 4) * prod_pow(abb[2], 2);
}

template <typename T> T IY_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 5) * abb[2];
}

template <typename T> T IY_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1] + -abb[0], 6);
}

template <typename T, size_t D>
std::vector<T> N_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(mc[1]),
          sq(mc[2]),
          sq(l[0] + mc[2]),
          sq(-mc[4] + l[1]),
          sq(-mc[1] + -mc[4] + l[0]),
          sq(-mc[1] + -mc[4] + l[1])};
}

template <typename T> T N_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T N_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[5];
}

template <typename T> T N_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4];
}

template <typename T> T N_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return T(-1) * abb[0] + abb[3];
}

template <typename T> T N_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return T(-1) * abb[1] + abb[2];
}

template <typename T> T N_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[5], 2);
}

template <typename T> T N_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4] * abb[5];
}

template <typename T> T N_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 2);
}

template <typename T> T N_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * abb[5];
}

template <typename T> T N_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * abb[4];
}

template <typename T> T N_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2);
}

template <typename T> T N_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * abb[5];
}

template <typename T> T N_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * abb[4];
}

template <typename T> T N_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]);
}

template <typename T> T N_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2);
}

template <typename T> T N_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[5], 3);
}

template <typename T> T N_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 3);
}

template <typename T> T N_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * abb[4] * abb[5];
}

template <typename T> T N_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * abb[5];
}

template <typename T> T N_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * abb[4];
}

template <typename T> T N_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3);
}

template <typename T> T N_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * abb[4] * abb[5];
}

template <typename T> T N_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * abb[5];
}

template <typename T> T N_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * abb[4];
}

template <typename T> T N_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2);
}

template <typename T> T N_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * abb[5];
}

template <typename T> T N_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * abb[4];
}

template <typename T> T N_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]);
}

template <typename T> T N_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3);
}

template <typename T> T N_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[5], 4);
}

template <typename T> T N_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_40(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 4);
}

template <typename T> T N_ten_41(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_42(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_43(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_44(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_45(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_46(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * abb[4] * abb[5];
}

template <typename T> T N_ten_47(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_48(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * abb[5];
}

template <typename T> T N_ten_49(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * abb[4];
}

template <typename T> T N_ten_50(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4);
}

template <typename T> T N_ten_51(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_52(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_53(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_54(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_55(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_56(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * abb[4] * abb[5];
}

template <typename T> T N_ten_57(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_58(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * abb[5];
}

template <typename T> T N_ten_59(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * abb[4];
}

template <typename T> T N_ten_60(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3);
}

template <typename T> T N_ten_61(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_62(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * abb[4] * abb[5];
}

template <typename T> T N_ten_63(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_64(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * abb[5];
}

template <typename T> T N_ten_65(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * abb[4];
}

template <typename T> T N_ten_66(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2);
}

template <typename T> T N_ten_67(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * abb[5];
}

template <typename T> T N_ten_68(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * abb[4];
}

template <typename T> T N_ten_69(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]);
}

template <typename T> T N_ten_70(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4);
}

template <typename T> T N_ten_71(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[5], 5);
}

template <typename T> T N_ten_72(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_73(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_74(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_75(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_76(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 5);
}

template <typename T> T N_ten_77(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_78(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_79(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_80(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_81(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_82(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_83(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_84(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_85(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_86(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_87(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * abb[4] * abb[5];
}

template <typename T> T N_ten_88(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_89(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * abb[5];
}

template <typename T> T N_ten_90(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * abb[4];
}

template <typename T> T N_ten_91(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 5);
}

template <typename T> T N_ten_92(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_93(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_94(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_95(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_96(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_97(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_98(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_99(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_100(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_101(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_102(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * abb[4] * abb[5];
}

template <typename T> T N_ten_103(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_104(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * abb[5];
}

template <typename T> T N_ten_105(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * abb[4];
}

template <typename T> T N_ten_106(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4);
}

template <typename T> T N_ten_107(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_108(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_109(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_110(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_111(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_112(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * abb[4] * abb[5];
}

template <typename T> T N_ten_113(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_114(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * abb[5];
}

template <typename T> T N_ten_115(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * abb[4];
}

template <typename T> T N_ten_116(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3);
}

template <typename T> T N_ten_117(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_118(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * abb[4] * abb[5];
}

template <typename T> T N_ten_119(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_120(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * abb[5];
}

template <typename T> T N_ten_121(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * abb[4];
}

template <typename T> T N_ten_122(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2);
}

template <typename T> T N_ten_123(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * abb[5];
}

template <typename T> T N_ten_124(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * abb[4];
}

template <typename T> T N_ten_125(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]);
}

template <typename T> T N_ten_126(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5);
}

template <typename T> T N_ten_127(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[5], 6);
}

template <typename T> T N_ten_128(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4] * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_129(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_130(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_131(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_132(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 5) * abb[5];
}

template <typename T> T N_ten_133(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 6);
}

template <typename T> T N_ten_134(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_135(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_136(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_137(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_138(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_139(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 5);
}

template <typename T> T N_ten_140(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_141(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_142(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_143(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_144(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_145(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_146(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_147(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_148(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_149(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_150(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * abb[4] * abb[5];
}

template <typename T> T N_ten_151(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_152(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 5) * abb[5];
}

template <typename T> T N_ten_153(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 5) * abb[4];
}

template <typename T> T N_ten_154(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 6);
}

template <typename T> T N_ten_155(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_156(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_157(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_158(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_159(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_160(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 5);
}

template <typename T> T N_ten_161(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_162(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_163(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_164(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_165(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_166(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_167(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_168(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_169(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_170(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_171(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * abb[4] * abb[5];
}

template <typename T> T N_ten_172(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_173(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4) * abb[5];
}

template <typename T> T N_ten_174(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4) * abb[4];
}

template <typename T> T N_ten_175(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 5);
}

template <typename T> T N_ten_176(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_177(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_178(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_179(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_180(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_181(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_182(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_183(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_184(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_185(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_186(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * abb[4] * abb[5];
}

template <typename T> T N_ten_187(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_188(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * abb[5];
}

template <typename T> T N_ten_189(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * abb[4];
}

template <typename T> T N_ten_190(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 4);
}

template <typename T> T N_ten_191(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_192(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_193(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_194(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_195(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_196(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * abb[4] * abb[5];
}

template <typename T> T N_ten_197(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_198(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * abb[5];
}

template <typename T> T N_ten_199(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * abb[4];
}

template <typename T> T N_ten_200(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 3);
}

template <typename T> T N_ten_201(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_202(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * abb[4] * abb[5];
}

template <typename T> T N_ten_203(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_204(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]) * abb[5];
}

template <typename T> T N_ten_205(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]) * abb[4];
}

template <typename T> T N_ten_206(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 2);
}

template <typename T> T N_ten_207(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * abb[5];
}

template <typename T> T N_ten_208(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * abb[4];
}

template <typename T> T N_ten_209(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * (T(-1) * abb[0] + abb[3]);
}

template <typename T> T N_ten_210(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 6);
}

template <typename T> T N_ten_211(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[4] * prod_pow(abb[5], 6);
}

template <typename T> T N_ten_212(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 2) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_213(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 3) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_214(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 4) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_215(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 5) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_216(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 6) * abb[5];
}

template <typename T> T N_ten_217(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_218(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_219(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_220(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_221(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 5) * abb[5];
}

template <typename T> T N_ten_222(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 6);
}

template <typename T> T N_ten_223(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_224(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_225(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_226(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_227(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 5);
}

template <typename T> T N_ten_228(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_229(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_230(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_231(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_232(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_233(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_234(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_235(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 5) * abb[4] * abb[5];
}

template <typename T> T N_ten_236(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 5) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_237(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 6) * abb[4];
}

template <typename T> T N_ten_238(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[5], 6);
}

template <typename T> T N_ten_239(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * abb[4] * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_240(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_241(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_242(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_243(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 5) * abb[5];
}

template <typename T> T N_ten_244(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_245(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_246(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 3);
}

template <typename T> T N_ten_247(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_248(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_249(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 5);
}

template <typename T> T N_ten_250(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_251(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_252(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_253(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_254(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_255(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_256(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_257(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_258(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_259(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_260(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4) * abb[4] * abb[5];
}

template <typename T> T N_ten_261(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_262(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 5) * abb[5];
}

template <typename T> T N_ten_263(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 5) * abb[4];
}

template <typename T> T N_ten_264(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 6);
}

template <typename T> T N_ten_265(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_266(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_267(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_268(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_269(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_270(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_271(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_272(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_273(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_274(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_275(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_276(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * abb[4] *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_277(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) *
         abb[5];
}

template <typename T> T N_ten_278(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_279(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_280(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * abb[4] * abb[5];
}

template <typename T> T N_ten_281(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_282(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 4) * abb[5];
}

template <typename T> T N_ten_283(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 4) * abb[4];
}

template <typename T> T N_ten_284(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 5);
}

template <typename T> T N_ten_285(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_286(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_287(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_288(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_289(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_290(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_291(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_292(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_293(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_294(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * abb[4] * abb[5];
}

template <typename T> T N_ten_295(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_296(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 3) * abb[5];
}

template <typename T> T N_ten_297(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 3) * abb[4];
}

template <typename T> T N_ten_298(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 4);
}

template <typename T> T N_ten_299(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_300(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_301(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_302(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_303(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]) * abb[4] * abb[5];
}

template <typename T> T N_ten_304(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_305(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 2) * abb[5];
}

template <typename T> T N_ten_306(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 2) * abb[4];
}

template <typename T> T N_ten_307(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 3);
}

template <typename T> T N_ten_308(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_309(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * abb[4] * abb[5];
}

template <typename T> T N_ten_310(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * (T(-1) * abb[0] + abb[3]) * abb[5];
}

template <typename T> T N_ten_311(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * (T(-1) * abb[0] + abb[3]) * abb[4];
}

template <typename T> T N_ten_312(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * prod_pow(abb[3] + -abb[0], 2);
}

template <typename T> T N_ten_313(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 6) * abb[5];
}

template <typename T> T N_ten_314(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 6) * (T(-1) * abb[0] + abb[3]);
}

template <typename T> T N_ten_315(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 2) * prod_pow(abb[5], 6);
}

template <typename T> T N_ten_316(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 3) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_317(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 4) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_318(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 5) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_319(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[4], 6) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_320(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_321(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_322(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_323(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 5) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_324(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 6) * abb[5];
}

template <typename T> T N_ten_325(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_326(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_327(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_328(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 5) * abb[5];
}

template <typename T> T N_ten_329(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 6);
}

template <typename T> T N_ten_330(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_331(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_332(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_333(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 5);
}

template <typename T> T N_ten_334(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_335(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_336(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_337(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 5) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_338(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 5) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_339(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[3] + -abb[0], 6) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_340(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * abb[4] * prod_pow(abb[5], 6);
}

template <typename T> T N_ten_341(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 2) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_342(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 3) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_343(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 4) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_344(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[4], 5) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_345(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_346(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 4);
}

template <typename T> T N_ten_347(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) *
         prod_pow(abb[5], 3);
}

template <typename T> T N_ten_348(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_349(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 5) * abb[5];
}

template <typename T> T N_ten_350(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_351(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 3);
}

template <typename T> T N_ten_352(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_353(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_354(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 5);
}

template <typename T> T N_ten_355(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_356(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_357(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_358(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_359(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_360(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_361(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_362(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 5) * abb[4] * abb[5];
}

template <typename T> T N_ten_363(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 5) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_364(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (T(-1) * abb[1] + abb[2]) * prod_pow(abb[3] + -abb[0], 6) * abb[4];
}

template <typename T> T N_ten_365(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[5], 6);
}

template <typename T> T N_ten_366(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * abb[4] * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_367(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_368(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_369(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[4], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_370(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_371(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_372(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 3);
}

template <typename T> T N_ten_373(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_374(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 4) * abb[5];
}

template <typename T> T N_ten_375(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_376(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * abb[4] *
         prod_pow(abb[5], 3);
}

template <typename T> T N_ten_377(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_378(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3) *
         abb[5];
}

template <typename T> T N_ten_379(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 4);
}

template <typename T> T N_ten_380(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_381(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * abb[4] *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_382(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2) *
         abb[5];
}

template <typename T> T N_ten_383(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_384(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_385(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 4) * abb[4] * abb[5];
}

template <typename T> T N_ten_386(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 4) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_387(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 5) * abb[5];
}

template <typename T> T N_ten_388(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 5) * abb[4];
}

template <typename T> T N_ten_389(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 2) * prod_pow(abb[3] + -abb[0], 6);
}

template <typename T> T N_ten_390(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[5], 5);
}

template <typename T> T N_ten_391(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * abb[4] * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_392(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[4], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_393(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[4], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_394(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_395(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_396(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_397(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 3) * abb[5];
}

template <typename T> T N_ten_398(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_399(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * abb[4] *
         prod_pow(abb[5], 2);
}

template <typename T> T N_ten_400(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2) *
         abb[5];
}

template <typename T> T N_ten_401(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 3);
}

template <typename T> T N_ten_402(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_403(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 3) * abb[4] * abb[5];
}

template <typename T> T N_ten_404(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 3) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_405(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 4) * abb[5];
}

template <typename T> T N_ten_406(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 4) * abb[4];
}

template <typename T> T N_ten_407(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 3) * prod_pow(abb[3] + -abb[0], 5);
}

template <typename T> T N_ten_408(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[5], 4);
}

template <typename T> T N_ten_409(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * abb[4] * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_410(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[4], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_411(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_412(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_413(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[4], 2) * abb[5];
}

template <typename T> T N_ten_414(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_415(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 2) * abb[4] * abb[5];
}

template <typename T> T N_ten_416(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 2) * prod_pow(abb[4], 2);
}

template <typename T> T N_ten_417(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 3) * abb[5];
}

template <typename T> T N_ten_418(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 3) * abb[4];
}

template <typename T> T N_ten_419(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 4) * prod_pow(abb[3] + -abb[0], 4);
}

template <typename T> T N_ten_420(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * prod_pow(abb[5], 3);
}

template <typename T> T N_ten_421(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * abb[4] * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_422(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * (T(-1) * abb[0] + abb[3]) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_423(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * (T(-1) * abb[0] + abb[3]) * abb[4] * abb[5];
}

template <typename T> T N_ten_424(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * prod_pow(abb[3] + -abb[0], 2) * abb[5];
}

template <typename T> T N_ten_425(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * prod_pow(abb[3] + -abb[0], 2) * abb[4];
}

template <typename T> T N_ten_426(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 5) * prod_pow(abb[3] + -abb[0], 3);
}

template <typename T> T N_ten_427(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 6) * prod_pow(abb[5], 2);
}

template <typename T> T N_ten_428(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 6) * (T(-1) * abb[0] + abb[3]) * abb[5];
}

template <typename T> T N_ten_429(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2] + -abb[1], 6) * prod_pow(abb[3] + -abb[0], 2);
}

template <typename T, size_t D>
std::vector<T> xIY_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(-mc[1] + l[0]), sq(mc[1]), sq(-mc[1] + -mc[4] + l[0])};
}

template <typename T> T xIY_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T xIY_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[2];
}

template <typename T> T xIY_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] + T(-1) * abb[1];
}

template <typename T> T xIY_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 2);
}

template <typename T> T xIY_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * abb[2];
}

template <typename T> T xIY_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2);
}

template <typename T> T xIY_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 3);
}

template <typename T> T xIY_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 2);
}

template <typename T> T xIY_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * abb[2];
}

template <typename T> T xIY_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3);
}

template <typename T> T xIY_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 4);
}

template <typename T> T xIY_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 3);
}

template <typename T> T xIY_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 2);
}

template <typename T> T xIY_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * abb[2];
}

template <typename T> T xIY_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4);
}

template <typename T> T xIY_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 5);
}

template <typename T> T xIY_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 4);
}

template <typename T> T xIY_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 3);
}

template <typename T> T xIY_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 2);
}

template <typename T> T xIY_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * abb[2];
}

template <typename T> T xIY_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5);
}

template <typename T> T xIY_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 6);
}

template <typename T> T xIY_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 5);
}

template <typename T> T xIY_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 4);
}

template <typename T> T xIY_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 3);
}

template <typename T> T xIY_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 2);
}

template <typename T> T xIY_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * abb[2];
}

template <typename T> T xIY_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6);
}

template <typename T> T xIY_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 7);
}

template <typename T> T xIY_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 6);
}

template <typename T> T xIY_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 5);
}

template <typename T> T xIY_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 4);
}

template <typename T> T xIY_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 3);
}

template <typename T> T xIY_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 2);
}

template <typename T> T xIY_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * abb[2];
}

template <typename T> T xIY_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7);
}

template <typename T> T xIY_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[2], 8);
}

template <typename T> T xIY_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return (abb[0] + T(-1) * abb[1]) * prod_pow(abb[2], 7);
}

template <typename T> T xIY_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 2) * prod_pow(abb[2], 6);
}

template <typename T> T xIY_ten_40(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 3) * prod_pow(abb[2], 5);
}

template <typename T> T xIY_ten_41(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 4) * prod_pow(abb[2], 4);
}

template <typename T> T xIY_ten_42(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 5) * prod_pow(abb[2], 3);
}

template <typename T> T xIY_ten_43(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 6) * prod_pow(abb[2], 2);
}

template <typename T> T xIY_ten_44(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 7) * abb[2];
}

template <typename T> T xIY_ten_45(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0] + -abb[1], 8);
}

template <typename T, size_t D>
std::vector<T> IY_BoxTri_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(l[1] + mc[2] + mc[3])};
}

template <typename T> T IY_BoxTri_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T IY_BoxTri_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0];
}

template <typename T> T IY_BoxTri_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2);
}

template <typename T> T IY_BoxTri_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3);
}

template <typename T> T IY_BoxTri_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4);
}

template <typename T> T IY_BoxTri_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5);
}

template <typename T> T IY_BoxTri_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6);
}

template <typename T, size_t D>
std::vector<T> Fan_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(l[0] + mc[2] + mc[3]), sq(l[1] + mc[2] + mc[3])};
}

template <typename T> T Fan_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T Fan_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[1];
}

template <typename T> T Fan_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0];
}

template <typename T> T Fan_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 2);
}

template <typename T> T Fan_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * abb[1];
}

template <typename T> T Fan_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2);
}

template <typename T> T Fan_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 3);
}

template <typename T> T Fan_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 2);
}

template <typename T> T Fan_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * abb[1];
}

template <typename T> T Fan_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3);
}

template <typename T> T Fan_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 4);
}

template <typename T> T Fan_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 3);
}

template <typename T> T Fan_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 2);
}

template <typename T> T Fan_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * abb[1];
}

template <typename T> T Fan_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4);
}

template <typename T> T Fan_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 5);
}

template <typename T> T Fan_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 4);
}

template <typename T> T Fan_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 3);
}

template <typename T> T Fan_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 2);
}

template <typename T> T Fan_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * abb[1];
}

template <typename T> T Fan_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5);
}

template <typename T> T Fan_ten_22(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[1], 6);
}

template <typename T> T Fan_ten_23(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 5);
}

template <typename T> T Fan_ten_24(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 4);
}

template <typename T> T Fan_ten_25(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 3);
}

template <typename T> T Fan_ten_26(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 2);
}

template <typename T> T Fan_ten_27(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * abb[1];
}

template <typename T> T Fan_ten_28(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6);
}

template <typename T> T Fan_ten_29(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0] * prod_pow(abb[1], 6);
}

template <typename T> T Fan_ten_30(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 5);
}

template <typename T> T Fan_ten_31(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 4);
}

template <typename T> T Fan_ten_32(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 3);
}

template <typename T> T Fan_ten_33(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 2);
}

template <typename T> T Fan_ten_34(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * abb[1];
}

template <typename T> T Fan_ten_35(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2) * prod_pow(abb[1], 6);
}

template <typename T> T Fan_ten_36(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3) * prod_pow(abb[1], 5);
}

template <typename T> T Fan_ten_37(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4) * prod_pow(abb[1], 4);
}

template <typename T> T Fan_ten_38(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5) * prod_pow(abb[1], 3);
}

template <typename T> T Fan_ten_39(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6) * prod_pow(abb[1], 2);
}

template <typename T, size_t D>
std::vector<T> BowTie_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {};
}

template <typename T> T BowTie_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T, size_t D>
std::vector<T> xIY_BoxBox_coordinates(const OnShellPoint<T, D> &l, const momD_conf<T, D> &mc) {
  return {sq(l[0] + mc[2] + mc[3])};
}

template <typename T> T xIY_BoxBox_ten_1(const TopologyCoordinates<T> &cs) { return T(1); }

template <typename T> T xIY_BoxBox_ten_2(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return abb[0];
}

template <typename T> T xIY_BoxBox_ten_3(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 2);
}

template <typename T> T xIY_BoxBox_ten_4(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 3);
}

template <typename T> T xIY_BoxBox_ten_5(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 4);
}

template <typename T> T xIY_BoxBox_ten_6(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 5);
}

template <typename T> T xIY_BoxBox_ten_7(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 6);
}

template <typename T> T xIY_BoxBox_ten_8(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 7);
}

template <typename T> T xIY_BoxBox_ten_9(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 8);
}

template <typename T> T xIY_BoxBox_ten_10(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 9);
}

template <typename T> T xIY_BoxBox_ten_11(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 10);
}

template <typename T> T xIY_BoxBox_ten_12(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 11);
}

template <typename T> T xIY_BoxBox_ten_13(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 12);
}

template <typename T> T xIY_BoxBox_ten_14(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 13);
}

template <typename T> T xIY_BoxBox_ten_15(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 14);
}

template <typename T> T xIY_BoxBox_ten_16(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 15);
}

template <typename T> T xIY_BoxBox_ten_17(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 16);
}

template <typename T> T xIY_BoxBox_ten_18(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 17);
}

template <typename T> T xIY_BoxBox_ten_19(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 18);
}

template <typename T> T xIY_BoxBox_ten_20(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 19);
}

template <typename T> T xIY_BoxBox_ten_21(const TopologyCoordinates<T> &cs) {
  const auto &abb = cs.special;
  return prod_pow(abb[0], 20);
}

inline void load() {
  using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[4, 1]], Node[]], Connection[Strand[Link[0]], "
                "Strand[Link[1], Bead[Leg[1, 1]], Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], "
                "Link[2]], Strand[Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",   "ten_2",   "ten_3",   "ten_4",   "ten_5",   "ten_6",   "ten_7",   "ten_8",
        "ten_9",   "ten_10",  "ten_11",  "ten_12",  "ten_13",  "ten_14",  "ten_15",  "ten_16",
        "ten_17",  "ten_18",  "ten_19",  "ten_20",  "ten_21",  "ten_22",  "ten_23",  "ten_24",
        "ten_25",  "ten_26",  "ten_27",  "ten_28",  "ten_29",  "ten_30",  "ten_31",  "ten_32",
        "ten_33",  "ten_34",  "ten_35",  "ten_36",  "ten_37",  "ten_38",  "ten_39",  "ten_40",
        "ten_41",  "ten_42",  "ten_43",  "ten_44",  "ten_45",  "ten_46",  "ten_47",  "ten_48",
        "ten_49",  "ten_50",  "ten_51",  "ten_52",  "ten_53",  "ten_54",  "ten_55",  "ten_56",
        "ten_57",  "ten_58",  "ten_59",  "ten_60",  "ten_61",  "ten_62",  "ten_63",  "ten_64",
        "ten_65",  "ten_66",  "ten_67",  "ten_68",  "ten_69",  "ten_70",  "ten_71",  "ten_72",
        "ten_73",  "ten_74",  "ten_75",  "ten_76",  "ten_77",  "ten_78",  "ten_79",  "ten_80",
        "ten_81",  "ten_82",  "ten_83",  "ten_84",  "ten_85",  "ten_86",  "ten_87",  "ten_88",
        "ten_89",  "ten_90",  "ten_91",  "ten_92",  "ten_93",  "ten_94",  "ten_95",  "ten_96",
        "ten_97",  "ten_98",  "ten_99",  "ten_100", "ten_101", "ten_102", "ten_103", "ten_104",
        "ten_105", "ten_106", "ten_107", "ten_108", "ten_109", "ten_110", "ten_111", "ten_112",
        "ten_113", "ten_114", "ten_115", "ten_116", "ten_117", "ten_118", "ten_119", "ten_120",
        "ten_121", "ten_122", "ten_123", "ten_124", "ten_125", "ten_126", "ten_127", "ten_128",
        "ten_129", "ten_130", "ten_131", "ten_132", "ten_133", "ten_134", "ten_135", "ten_136",
        "ten_137", "ten_138", "ten_139", "ten_140", "ten_141", "ten_142", "ten_143", "ten_144",
        "ten_145", "ten_146", "ten_147", "ten_148", "ten_149", "ten_150", "ten_151", "ten_152",
        "ten_153", "ten_154", "ten_155", "ten_156", "ten_157", "ten_158", "ten_159", "ten_160",
        "ten_161", "ten_162", "ten_163", "ten_164", "ten_165", "ten_166", "ten_167", "ten_168",
        "ten_169", "ten_170", "ten_171", "ten_172", "ten_173", "ten_174", "ten_175", "ten_176",
        "ten_177", "ten_178", "ten_179", "ten_180", "ten_181", "ten_182", "ten_183", "ten_184",
        "ten_185", "ten_186", "ten_187", "ten_188", "ten_189", "ten_190", "ten_191", "ten_192"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = g11_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        g11_ten_1<C>,   g11_ten_2<C>,   g11_ten_3<C>,   g11_ten_4<C>,   g11_ten_5<C>,
        g11_ten_6<C>,   g11_ten_7<C>,   g11_ten_8<C>,   g11_ten_9<C>,   g11_ten_10<C>,
        g11_ten_11<C>,  g11_ten_12<C>,  g11_ten_13<C>,  g11_ten_14<C>,  g11_ten_15<C>,
        g11_ten_16<C>,  g11_ten_17<C>,  g11_ten_18<C>,  g11_ten_19<C>,  g11_ten_20<C>,
        g11_ten_21<C>,  g11_ten_22<C>,  g11_ten_23<C>,  g11_ten_24<C>,  g11_ten_25<C>,
        g11_ten_26<C>,  g11_ten_27<C>,  g11_ten_28<C>,  g11_ten_29<C>,  g11_ten_30<C>,
        g11_ten_31<C>,  g11_ten_32<C>,  g11_ten_33<C>,  g11_ten_34<C>,  g11_ten_35<C>,
        g11_ten_36<C>,  g11_ten_37<C>,  g11_ten_38<C>,  g11_ten_39<C>,  g11_ten_40<C>,
        g11_ten_41<C>,  g11_ten_42<C>,  g11_ten_43<C>,  g11_ten_44<C>,  g11_ten_45<C>,
        g11_ten_46<C>,  g11_ten_47<C>,  g11_ten_48<C>,  g11_ten_49<C>,  g11_ten_50<C>,
        g11_ten_51<C>,  g11_ten_52<C>,  g11_ten_53<C>,  g11_ten_54<C>,  g11_ten_55<C>,
        g11_ten_56<C>,  g11_ten_57<C>,  g11_ten_58<C>,  g11_ten_59<C>,  g11_ten_60<C>,
        g11_ten_61<C>,  g11_ten_62<C>,  g11_ten_63<C>,  g11_ten_64<C>,  g11_ten_65<C>,
        g11_ten_66<C>,  g11_ten_67<C>,  g11_ten_68<C>,  g11_ten_69<C>,  g11_ten_70<C>,
        g11_ten_71<C>,  g11_ten_72<C>,  g11_ten_73<C>,  g11_ten_74<C>,  g11_ten_75<C>,
        g11_ten_76<C>,  g11_ten_77<C>,  g11_ten_78<C>,  g11_ten_79<C>,  g11_ten_80<C>,
        g11_ten_81<C>,  g11_ten_82<C>,  g11_ten_83<C>,  g11_ten_84<C>,  g11_ten_85<C>,
        g11_ten_86<C>,  g11_ten_87<C>,  g11_ten_88<C>,  g11_ten_89<C>,  g11_ten_90<C>,
        g11_ten_91<C>,  g11_ten_92<C>,  g11_ten_93<C>,  g11_ten_94<C>,  g11_ten_95<C>,
        g11_ten_96<C>,  g11_ten_97<C>,  g11_ten_98<C>,  g11_ten_99<C>,  g11_ten_100<C>,
        g11_ten_101<C>, g11_ten_102<C>, g11_ten_103<C>, g11_ten_104<C>, g11_ten_105<C>,
        g11_ten_106<C>, g11_ten_107<C>, g11_ten_108<C>, g11_ten_109<C>, g11_ten_110<C>,
        g11_ten_111<C>, g11_ten_112<C>, g11_ten_113<C>, g11_ten_114<C>, g11_ten_115<C>,
        g11_ten_116<C>, g11_ten_117<C>, g11_ten_118<C>, g11_ten_119<C>, g11_ten_120<C>,
        g11_ten_121<C>, g11_ten_122<C>, g11_ten_123<C>, g11_ten_124<C>, g11_ten_125<C>,
        g11_ten_126<C>, g11_ten_127<C>, g11_ten_128<C>, g11_ten_129<C>, g11_ten_130<C>,
        g11_ten_131<C>, g11_ten_132<C>, g11_ten_133<C>, g11_ten_134<C>, g11_ten_135<C>,
        g11_ten_136<C>, g11_ten_137<C>, g11_ten_138<C>, g11_ten_139<C>, g11_ten_140<C>,
        g11_ten_141<C>, g11_ten_142<C>, g11_ten_143<C>, g11_ten_144<C>, g11_ten_145<C>,
        g11_ten_146<C>, g11_ten_147<C>, g11_ten_148<C>, g11_ten_149<C>, g11_ten_150<C>,
        g11_ten_151<C>, g11_ten_152<C>, g11_ten_153<C>, g11_ten_154<C>, g11_ten_155<C>,
        g11_ten_156<C>, g11_ten_157<C>, g11_ten_158<C>, g11_ten_159<C>, g11_ten_160<C>,
        g11_ten_161<C>, g11_ten_162<C>, g11_ten_163<C>, g11_ten_164<C>, g11_ten_165<C>,
        g11_ten_166<C>, g11_ten_167<C>, g11_ten_168<C>, g11_ten_169<C>, g11_ten_170<C>,
        g11_ten_171<C>, g11_ten_172<C>, g11_ten_173<C>, g11_ten_174<C>, g11_ten_175<C>,
        g11_ten_176<C>, g11_ten_177<C>, g11_ten_178<C>, g11_ten_179<C>, g11_ten_180<C>,
        g11_ten_181<C>, g11_ten_182<C>, g11_ten_183<C>, g11_ten_184<C>, g11_ten_185<C>,
        g11_ten_186<C>, g11_ten_187<C>, g11_ten_188<C>, g11_ten_189<C>, g11_ten_190<C>,
        g11_ten_191<C>, g11_ten_192<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = g11_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        g11_ten_1<CHP>,   g11_ten_2<CHP>,   g11_ten_3<CHP>,   g11_ten_4<CHP>,   g11_ten_5<CHP>,
        g11_ten_6<CHP>,   g11_ten_7<CHP>,   g11_ten_8<CHP>,   g11_ten_9<CHP>,   g11_ten_10<CHP>,
        g11_ten_11<CHP>,  g11_ten_12<CHP>,  g11_ten_13<CHP>,  g11_ten_14<CHP>,  g11_ten_15<CHP>,
        g11_ten_16<CHP>,  g11_ten_17<CHP>,  g11_ten_18<CHP>,  g11_ten_19<CHP>,  g11_ten_20<CHP>,
        g11_ten_21<CHP>,  g11_ten_22<CHP>,  g11_ten_23<CHP>,  g11_ten_24<CHP>,  g11_ten_25<CHP>,
        g11_ten_26<CHP>,  g11_ten_27<CHP>,  g11_ten_28<CHP>,  g11_ten_29<CHP>,  g11_ten_30<CHP>,
        g11_ten_31<CHP>,  g11_ten_32<CHP>,  g11_ten_33<CHP>,  g11_ten_34<CHP>,  g11_ten_35<CHP>,
        g11_ten_36<CHP>,  g11_ten_37<CHP>,  g11_ten_38<CHP>,  g11_ten_39<CHP>,  g11_ten_40<CHP>,
        g11_ten_41<CHP>,  g11_ten_42<CHP>,  g11_ten_43<CHP>,  g11_ten_44<CHP>,  g11_ten_45<CHP>,
        g11_ten_46<CHP>,  g11_ten_47<CHP>,  g11_ten_48<CHP>,  g11_ten_49<CHP>,  g11_ten_50<CHP>,
        g11_ten_51<CHP>,  g11_ten_52<CHP>,  g11_ten_53<CHP>,  g11_ten_54<CHP>,  g11_ten_55<CHP>,
        g11_ten_56<CHP>,  g11_ten_57<CHP>,  g11_ten_58<CHP>,  g11_ten_59<CHP>,  g11_ten_60<CHP>,
        g11_ten_61<CHP>,  g11_ten_62<CHP>,  g11_ten_63<CHP>,  g11_ten_64<CHP>,  g11_ten_65<CHP>,
        g11_ten_66<CHP>,  g11_ten_67<CHP>,  g11_ten_68<CHP>,  g11_ten_69<CHP>,  g11_ten_70<CHP>,
        g11_ten_71<CHP>,  g11_ten_72<CHP>,  g11_ten_73<CHP>,  g11_ten_74<CHP>,  g11_ten_75<CHP>,
        g11_ten_76<CHP>,  g11_ten_77<CHP>,  g11_ten_78<CHP>,  g11_ten_79<CHP>,  g11_ten_80<CHP>,
        g11_ten_81<CHP>,  g11_ten_82<CHP>,  g11_ten_83<CHP>,  g11_ten_84<CHP>,  g11_ten_85<CHP>,
        g11_ten_86<CHP>,  g11_ten_87<CHP>,  g11_ten_88<CHP>,  g11_ten_89<CHP>,  g11_ten_90<CHP>,
        g11_ten_91<CHP>,  g11_ten_92<CHP>,  g11_ten_93<CHP>,  g11_ten_94<CHP>,  g11_ten_95<CHP>,
        g11_ten_96<CHP>,  g11_ten_97<CHP>,  g11_ten_98<CHP>,  g11_ten_99<CHP>,  g11_ten_100<CHP>,
        g11_ten_101<CHP>, g11_ten_102<CHP>, g11_ten_103<CHP>, g11_ten_104<CHP>, g11_ten_105<CHP>,
        g11_ten_106<CHP>, g11_ten_107<CHP>, g11_ten_108<CHP>, g11_ten_109<CHP>, g11_ten_110<CHP>,
        g11_ten_111<CHP>, g11_ten_112<CHP>, g11_ten_113<CHP>, g11_ten_114<CHP>, g11_ten_115<CHP>,
        g11_ten_116<CHP>, g11_ten_117<CHP>, g11_ten_118<CHP>, g11_ten_119<CHP>, g11_ten_120<CHP>,
        g11_ten_121<CHP>, g11_ten_122<CHP>, g11_ten_123<CHP>, g11_ten_124<CHP>, g11_ten_125<CHP>,
        g11_ten_126<CHP>, g11_ten_127<CHP>, g11_ten_128<CHP>, g11_ten_129<CHP>, g11_ten_130<CHP>,
        g11_ten_131<CHP>, g11_ten_132<CHP>, g11_ten_133<CHP>, g11_ten_134<CHP>, g11_ten_135<CHP>,
        g11_ten_136<CHP>, g11_ten_137<CHP>, g11_ten_138<CHP>, g11_ten_139<CHP>, g11_ten_140<CHP>,
        g11_ten_141<CHP>, g11_ten_142<CHP>, g11_ten_143<CHP>, g11_ten_144<CHP>, g11_ten_145<CHP>,
        g11_ten_146<CHP>, g11_ten_147<CHP>, g11_ten_148<CHP>, g11_ten_149<CHP>, g11_ten_150<CHP>,
        g11_ten_151<CHP>, g11_ten_152<CHP>, g11_ten_153<CHP>, g11_ten_154<CHP>, g11_ten_155<CHP>,
        g11_ten_156<CHP>, g11_ten_157<CHP>, g11_ten_158<CHP>, g11_ten_159<CHP>, g11_ten_160<CHP>,
        g11_ten_161<CHP>, g11_ten_162<CHP>, g11_ten_163<CHP>, g11_ten_164<CHP>, g11_ten_165<CHP>,
        g11_ten_166<CHP>, g11_ten_167<CHP>, g11_ten_168<CHP>, g11_ten_169<CHP>, g11_ten_170<CHP>,
        g11_ten_171<CHP>, g11_ten_172<CHP>, g11_ten_173<CHP>, g11_ten_174<CHP>, g11_ten_175<CHP>,
        g11_ten_176<CHP>, g11_ten_177<CHP>, g11_ten_178<CHP>, g11_ten_179<CHP>, g11_ten_180<CHP>,
        g11_ten_181<CHP>, g11_ten_182<CHP>, g11_ten_183<CHP>, g11_ten_184<CHP>, g11_ten_185<CHP>,
        g11_ten_186<CHP>, g11_ten_187<CHP>, g11_ten_188<CHP>, g11_ten_189<CHP>, g11_ten_190<CHP>,
        g11_ten_191<CHP>, g11_ten_192<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = g11_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        g11_ten_1<CVHP>,   g11_ten_2<CVHP>,   g11_ten_3<CVHP>,   g11_ten_4<CVHP>,
        g11_ten_5<CVHP>,   g11_ten_6<CVHP>,   g11_ten_7<CVHP>,   g11_ten_8<CVHP>,
        g11_ten_9<CVHP>,   g11_ten_10<CVHP>,  g11_ten_11<CVHP>,  g11_ten_12<CVHP>,
        g11_ten_13<CVHP>,  g11_ten_14<CVHP>,  g11_ten_15<CVHP>,  g11_ten_16<CVHP>,
        g11_ten_17<CVHP>,  g11_ten_18<CVHP>,  g11_ten_19<CVHP>,  g11_ten_20<CVHP>,
        g11_ten_21<CVHP>,  g11_ten_22<CVHP>,  g11_ten_23<CVHP>,  g11_ten_24<CVHP>,
        g11_ten_25<CVHP>,  g11_ten_26<CVHP>,  g11_ten_27<CVHP>,  g11_ten_28<CVHP>,
        g11_ten_29<CVHP>,  g11_ten_30<CVHP>,  g11_ten_31<CVHP>,  g11_ten_32<CVHP>,
        g11_ten_33<CVHP>,  g11_ten_34<CVHP>,  g11_ten_35<CVHP>,  g11_ten_36<CVHP>,
        g11_ten_37<CVHP>,  g11_ten_38<CVHP>,  g11_ten_39<CVHP>,  g11_ten_40<CVHP>,
        g11_ten_41<CVHP>,  g11_ten_42<CVHP>,  g11_ten_43<CVHP>,  g11_ten_44<CVHP>,
        g11_ten_45<CVHP>,  g11_ten_46<CVHP>,  g11_ten_47<CVHP>,  g11_ten_48<CVHP>,
        g11_ten_49<CVHP>,  g11_ten_50<CVHP>,  g11_ten_51<CVHP>,  g11_ten_52<CVHP>,
        g11_ten_53<CVHP>,  g11_ten_54<CVHP>,  g11_ten_55<CVHP>,  g11_ten_56<CVHP>,
        g11_ten_57<CVHP>,  g11_ten_58<CVHP>,  g11_ten_59<CVHP>,  g11_ten_60<CVHP>,
        g11_ten_61<CVHP>,  g11_ten_62<CVHP>,  g11_ten_63<CVHP>,  g11_ten_64<CVHP>,
        g11_ten_65<CVHP>,  g11_ten_66<CVHP>,  g11_ten_67<CVHP>,  g11_ten_68<CVHP>,
        g11_ten_69<CVHP>,  g11_ten_70<CVHP>,  g11_ten_71<CVHP>,  g11_ten_72<CVHP>,
        g11_ten_73<CVHP>,  g11_ten_74<CVHP>,  g11_ten_75<CVHP>,  g11_ten_76<CVHP>,
        g11_ten_77<CVHP>,  g11_ten_78<CVHP>,  g11_ten_79<CVHP>,  g11_ten_80<CVHP>,
        g11_ten_81<CVHP>,  g11_ten_82<CVHP>,  g11_ten_83<CVHP>,  g11_ten_84<CVHP>,
        g11_ten_85<CVHP>,  g11_ten_86<CVHP>,  g11_ten_87<CVHP>,  g11_ten_88<CVHP>,
        g11_ten_89<CVHP>,  g11_ten_90<CVHP>,  g11_ten_91<CVHP>,  g11_ten_92<CVHP>,
        g11_ten_93<CVHP>,  g11_ten_94<CVHP>,  g11_ten_95<CVHP>,  g11_ten_96<CVHP>,
        g11_ten_97<CVHP>,  g11_ten_98<CVHP>,  g11_ten_99<CVHP>,  g11_ten_100<CVHP>,
        g11_ten_101<CVHP>, g11_ten_102<CVHP>, g11_ten_103<CVHP>, g11_ten_104<CVHP>,
        g11_ten_105<CVHP>, g11_ten_106<CVHP>, g11_ten_107<CVHP>, g11_ten_108<CVHP>,
        g11_ten_109<CVHP>, g11_ten_110<CVHP>, g11_ten_111<CVHP>, g11_ten_112<CVHP>,
        g11_ten_113<CVHP>, g11_ten_114<CVHP>, g11_ten_115<CVHP>, g11_ten_116<CVHP>,
        g11_ten_117<CVHP>, g11_ten_118<CVHP>, g11_ten_119<CVHP>, g11_ten_120<CVHP>,
        g11_ten_121<CVHP>, g11_ten_122<CVHP>, g11_ten_123<CVHP>, g11_ten_124<CVHP>,
        g11_ten_125<CVHP>, g11_ten_126<CVHP>, g11_ten_127<CVHP>, g11_ten_128<CVHP>,
        g11_ten_129<CVHP>, g11_ten_130<CVHP>, g11_ten_131<CVHP>, g11_ten_132<CVHP>,
        g11_ten_133<CVHP>, g11_ten_134<CVHP>, g11_ten_135<CVHP>, g11_ten_136<CVHP>,
        g11_ten_137<CVHP>, g11_ten_138<CVHP>, g11_ten_139<CVHP>, g11_ten_140<CVHP>,
        g11_ten_141<CVHP>, g11_ten_142<CVHP>, g11_ten_143<CVHP>, g11_ten_144<CVHP>,
        g11_ten_145<CVHP>, g11_ten_146<CVHP>, g11_ten_147<CVHP>, g11_ten_148<CVHP>,
        g11_ten_149<CVHP>, g11_ten_150<CVHP>, g11_ten_151<CVHP>, g11_ten_152<CVHP>,
        g11_ten_153<CVHP>, g11_ten_154<CVHP>, g11_ten_155<CVHP>, g11_ten_156<CVHP>,
        g11_ten_157<CVHP>, g11_ten_158<CVHP>, g11_ten_159<CVHP>, g11_ten_160<CVHP>,
        g11_ten_161<CVHP>, g11_ten_162<CVHP>, g11_ten_163<CVHP>, g11_ten_164<CVHP>,
        g11_ten_165<CVHP>, g11_ten_166<CVHP>, g11_ten_167<CVHP>, g11_ten_168<CVHP>,
        g11_ten_169<CVHP>, g11_ten_170<CVHP>, g11_ten_171<CVHP>, g11_ten_172<CVHP>,
        g11_ten_173<CVHP>, g11_ten_174<CVHP>, g11_ten_175<CVHP>, g11_ten_176<CVHP>,
        g11_ten_177<CVHP>, g11_ten_178<CVHP>, g11_ten_179<CVHP>, g11_ten_180<CVHP>,
        g11_ten_181<CVHP>, g11_ten_182<CVHP>, g11_ten_183<CVHP>, g11_ten_184<CVHP>,
        g11_ten_185<CVHP>, g11_ten_186<CVHP>, g11_ten_187<CVHP>, g11_ten_188<CVHP>,
        g11_ten_189<CVHP>, g11_ten_190<CVHP>, g11_ten_191<CVHP>, g11_ten_192<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = g11_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        g11_ten_1<F32>,   g11_ten_2<F32>,   g11_ten_3<F32>,   g11_ten_4<F32>,   g11_ten_5<F32>,
        g11_ten_6<F32>,   g11_ten_7<F32>,   g11_ten_8<F32>,   g11_ten_9<F32>,   g11_ten_10<F32>,
        g11_ten_11<F32>,  g11_ten_12<F32>,  g11_ten_13<F32>,  g11_ten_14<F32>,  g11_ten_15<F32>,
        g11_ten_16<F32>,  g11_ten_17<F32>,  g11_ten_18<F32>,  g11_ten_19<F32>,  g11_ten_20<F32>,
        g11_ten_21<F32>,  g11_ten_22<F32>,  g11_ten_23<F32>,  g11_ten_24<F32>,  g11_ten_25<F32>,
        g11_ten_26<F32>,  g11_ten_27<F32>,  g11_ten_28<F32>,  g11_ten_29<F32>,  g11_ten_30<F32>,
        g11_ten_31<F32>,  g11_ten_32<F32>,  g11_ten_33<F32>,  g11_ten_34<F32>,  g11_ten_35<F32>,
        g11_ten_36<F32>,  g11_ten_37<F32>,  g11_ten_38<F32>,  g11_ten_39<F32>,  g11_ten_40<F32>,
        g11_ten_41<F32>,  g11_ten_42<F32>,  g11_ten_43<F32>,  g11_ten_44<F32>,  g11_ten_45<F32>,
        g11_ten_46<F32>,  g11_ten_47<F32>,  g11_ten_48<F32>,  g11_ten_49<F32>,  g11_ten_50<F32>,
        g11_ten_51<F32>,  g11_ten_52<F32>,  g11_ten_53<F32>,  g11_ten_54<F32>,  g11_ten_55<F32>,
        g11_ten_56<F32>,  g11_ten_57<F32>,  g11_ten_58<F32>,  g11_ten_59<F32>,  g11_ten_60<F32>,
        g11_ten_61<F32>,  g11_ten_62<F32>,  g11_ten_63<F32>,  g11_ten_64<F32>,  g11_ten_65<F32>,
        g11_ten_66<F32>,  g11_ten_67<F32>,  g11_ten_68<F32>,  g11_ten_69<F32>,  g11_ten_70<F32>,
        g11_ten_71<F32>,  g11_ten_72<F32>,  g11_ten_73<F32>,  g11_ten_74<F32>,  g11_ten_75<F32>,
        g11_ten_76<F32>,  g11_ten_77<F32>,  g11_ten_78<F32>,  g11_ten_79<F32>,  g11_ten_80<F32>,
        g11_ten_81<F32>,  g11_ten_82<F32>,  g11_ten_83<F32>,  g11_ten_84<F32>,  g11_ten_85<F32>,
        g11_ten_86<F32>,  g11_ten_87<F32>,  g11_ten_88<F32>,  g11_ten_89<F32>,  g11_ten_90<F32>,
        g11_ten_91<F32>,  g11_ten_92<F32>,  g11_ten_93<F32>,  g11_ten_94<F32>,  g11_ten_95<F32>,
        g11_ten_96<F32>,  g11_ten_97<F32>,  g11_ten_98<F32>,  g11_ten_99<F32>,  g11_ten_100<F32>,
        g11_ten_101<F32>, g11_ten_102<F32>, g11_ten_103<F32>, g11_ten_104<F32>, g11_ten_105<F32>,
        g11_ten_106<F32>, g11_ten_107<F32>, g11_ten_108<F32>, g11_ten_109<F32>, g11_ten_110<F32>,
        g11_ten_111<F32>, g11_ten_112<F32>, g11_ten_113<F32>, g11_ten_114<F32>, g11_ten_115<F32>,
        g11_ten_116<F32>, g11_ten_117<F32>, g11_ten_118<F32>, g11_ten_119<F32>, g11_ten_120<F32>,
        g11_ten_121<F32>, g11_ten_122<F32>, g11_ten_123<F32>, g11_ten_124<F32>, g11_ten_125<F32>,
        g11_ten_126<F32>, g11_ten_127<F32>, g11_ten_128<F32>, g11_ten_129<F32>, g11_ten_130<F32>,
        g11_ten_131<F32>, g11_ten_132<F32>, g11_ten_133<F32>, g11_ten_134<F32>, g11_ten_135<F32>,
        g11_ten_136<F32>, g11_ten_137<F32>, g11_ten_138<F32>, g11_ten_139<F32>, g11_ten_140<F32>,
        g11_ten_141<F32>, g11_ten_142<F32>, g11_ten_143<F32>, g11_ten_144<F32>, g11_ten_145<F32>,
        g11_ten_146<F32>, g11_ten_147<F32>, g11_ten_148<F32>, g11_ten_149<F32>, g11_ten_150<F32>,
        g11_ten_151<F32>, g11_ten_152<F32>, g11_ten_153<F32>, g11_ten_154<F32>, g11_ten_155<F32>,
        g11_ten_156<F32>, g11_ten_157<F32>, g11_ten_158<F32>, g11_ten_159<F32>, g11_ten_160<F32>,
        g11_ten_161<F32>, g11_ten_162<F32>, g11_ten_163<F32>, g11_ten_164<F32>, g11_ten_165<F32>,
        g11_ten_166<F32>, g11_ten_167<F32>, g11_ten_168<F32>, g11_ten_169<F32>, g11_ten_170<F32>,
        g11_ten_171<F32>, g11_ten_172<F32>, g11_ten_173<F32>, g11_ten_174<F32>, g11_ten_175<F32>,
        g11_ten_176<F32>, g11_ten_177<F32>, g11_ten_178<F32>, g11_ten_179<F32>, g11_ten_180<F32>,
        g11_ten_181<F32>, g11_ten_182<F32>, g11_ten_183<F32>, g11_ten_184<F32>, g11_ten_185<F32>,
        g11_ten_186<F32>, g11_ten_187<F32>, g11_ten_188<F32>, g11_ten_189<F32>, g11_ten_190<F32>,
        g11_ten_191<F32>, g11_ten_192<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[Leg[1, 1]], Node[]], Connection[Strand[Link[0]], Strand[Link[1], "
        "Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0]], Strand[Link[LoopMomentum[-1], 0], "
        "Bead[Leg[2, 2]], Link[2], Bead[Leg[3, 2]], Link[0]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",   "ten_2",   "ten_3",   "ten_4",   "ten_5",   "ten_6",   "ten_7",   "ten_8",
        "ten_9",   "ten_10",  "ten_11",  "ten_12",  "ten_13",  "ten_14",  "ten_15",  "ten_16",
        "ten_17",  "ten_18",  "ten_19",  "ten_20",  "ten_21",  "ten_22",  "ten_23",  "ten_24",
        "ten_25",  "ten_26",  "ten_27",  "ten_28",  "ten_29",  "ten_30",  "ten_31",  "ten_32",
        "ten_33",  "ten_34",  "ten_35",  "ten_36",  "ten_37",  "ten_38",  "ten_39",  "ten_40",
        "ten_41",  "ten_42",  "ten_43",  "ten_44",  "ten_45",  "ten_46",  "ten_47",  "ten_48",
        "ten_49",  "ten_50",  "ten_51",  "ten_52",  "ten_53",  "ten_54",  "ten_55",  "ten_56",
        "ten_57",  "ten_58",  "ten_59",  "ten_60",  "ten_61",  "ten_62",  "ten_63",  "ten_64",
        "ten_65",  "ten_66",  "ten_67",  "ten_68",  "ten_69",  "ten_70",  "ten_71",  "ten_72",
        "ten_73",  "ten_74",  "ten_75",  "ten_76",  "ten_77",  "ten_78",  "ten_79",  "ten_80",
        "ten_81",  "ten_82",  "ten_83",  "ten_84",  "ten_85",  "ten_86",  "ten_87",  "ten_88",
        "ten_89",  "ten_90",  "ten_91",  "ten_92",  "ten_93",  "ten_94",  "ten_95",  "ten_96",
        "ten_97",  "ten_98",  "ten_99",  "ten_100", "ten_101", "ten_102", "ten_103", "ten_104",
        "ten_105", "ten_106", "ten_107", "ten_108", "ten_109", "ten_110", "ten_111", "ten_112",
        "ten_113", "ten_114", "ten_115", "ten_116", "ten_117", "ten_118", "ten_119", "ten_120",
        "ten_121", "ten_122", "ten_123", "ten_124", "ten_125", "ten_126", "ten_127", "ten_128",
        "ten_129", "ten_130", "ten_131", "ten_132", "ten_133", "ten_134", "ten_135", "ten_136",
        "ten_137", "ten_138", "ten_139", "ten_140", "ten_141", "ten_142", "ten_143", "ten_144",
        "ten_145", "ten_146", "ten_147", "ten_148", "ten_149", "ten_150", "ten_151", "ten_152",
        "ten_153", "ten_154", "ten_155", "ten_156", "ten_157", "ten_158", "ten_159", "ten_160",
        "ten_161", "ten_162", "ten_163", "ten_164", "ten_165", "ten_166", "ten_167", "ten_168",
        "ten_169", "ten_170", "ten_171", "ten_172", "ten_173", "ten_174", "ten_175", "ten_176",
        "ten_177", "ten_178", "ten_179", "ten_180", "ten_181", "ten_182", "ten_183", "ten_184",
        "ten_185", "ten_186", "ten_187", "ten_188", "ten_189", "ten_190", "ten_191", "ten_192"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = g12_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        g12_ten_1<C>,   g12_ten_2<C>,   g12_ten_3<C>,   g12_ten_4<C>,   g12_ten_5<C>,
        g12_ten_6<C>,   g12_ten_7<C>,   g12_ten_8<C>,   g12_ten_9<C>,   g12_ten_10<C>,
        g12_ten_11<C>,  g12_ten_12<C>,  g12_ten_13<C>,  g12_ten_14<C>,  g12_ten_15<C>,
        g12_ten_16<C>,  g12_ten_17<C>,  g12_ten_18<C>,  g12_ten_19<C>,  g12_ten_20<C>,
        g12_ten_21<C>,  g12_ten_22<C>,  g12_ten_23<C>,  g12_ten_24<C>,  g12_ten_25<C>,
        g12_ten_26<C>,  g12_ten_27<C>,  g12_ten_28<C>,  g12_ten_29<C>,  g12_ten_30<C>,
        g12_ten_31<C>,  g12_ten_32<C>,  g12_ten_33<C>,  g12_ten_34<C>,  g12_ten_35<C>,
        g12_ten_36<C>,  g12_ten_37<C>,  g12_ten_38<C>,  g12_ten_39<C>,  g12_ten_40<C>,
        g12_ten_41<C>,  g12_ten_42<C>,  g12_ten_43<C>,  g12_ten_44<C>,  g12_ten_45<C>,
        g12_ten_46<C>,  g12_ten_47<C>,  g12_ten_48<C>,  g12_ten_49<C>,  g12_ten_50<C>,
        g12_ten_51<C>,  g12_ten_52<C>,  g12_ten_53<C>,  g12_ten_54<C>,  g12_ten_55<C>,
        g12_ten_56<C>,  g12_ten_57<C>,  g12_ten_58<C>,  g12_ten_59<C>,  g12_ten_60<C>,
        g12_ten_61<C>,  g12_ten_62<C>,  g12_ten_63<C>,  g12_ten_64<C>,  g12_ten_65<C>,
        g12_ten_66<C>,  g12_ten_67<C>,  g12_ten_68<C>,  g12_ten_69<C>,  g12_ten_70<C>,
        g12_ten_71<C>,  g12_ten_72<C>,  g12_ten_73<C>,  g12_ten_74<C>,  g12_ten_75<C>,
        g12_ten_76<C>,  g12_ten_77<C>,  g12_ten_78<C>,  g12_ten_79<C>,  g12_ten_80<C>,
        g12_ten_81<C>,  g12_ten_82<C>,  g12_ten_83<C>,  g12_ten_84<C>,  g12_ten_85<C>,
        g12_ten_86<C>,  g12_ten_87<C>,  g12_ten_88<C>,  g12_ten_89<C>,  g12_ten_90<C>,
        g12_ten_91<C>,  g12_ten_92<C>,  g12_ten_93<C>,  g12_ten_94<C>,  g12_ten_95<C>,
        g12_ten_96<C>,  g12_ten_97<C>,  g12_ten_98<C>,  g12_ten_99<C>,  g12_ten_100<C>,
        g12_ten_101<C>, g12_ten_102<C>, g12_ten_103<C>, g12_ten_104<C>, g12_ten_105<C>,
        g12_ten_106<C>, g12_ten_107<C>, g12_ten_108<C>, g12_ten_109<C>, g12_ten_110<C>,
        g12_ten_111<C>, g12_ten_112<C>, g12_ten_113<C>, g12_ten_114<C>, g12_ten_115<C>,
        g12_ten_116<C>, g12_ten_117<C>, g12_ten_118<C>, g12_ten_119<C>, g12_ten_120<C>,
        g12_ten_121<C>, g12_ten_122<C>, g12_ten_123<C>, g12_ten_124<C>, g12_ten_125<C>,
        g12_ten_126<C>, g12_ten_127<C>, g12_ten_128<C>, g12_ten_129<C>, g12_ten_130<C>,
        g12_ten_131<C>, g12_ten_132<C>, g12_ten_133<C>, g12_ten_134<C>, g12_ten_135<C>,
        g12_ten_136<C>, g12_ten_137<C>, g12_ten_138<C>, g12_ten_139<C>, g12_ten_140<C>,
        g12_ten_141<C>, g12_ten_142<C>, g12_ten_143<C>, g12_ten_144<C>, g12_ten_145<C>,
        g12_ten_146<C>, g12_ten_147<C>, g12_ten_148<C>, g12_ten_149<C>, g12_ten_150<C>,
        g12_ten_151<C>, g12_ten_152<C>, g12_ten_153<C>, g12_ten_154<C>, g12_ten_155<C>,
        g12_ten_156<C>, g12_ten_157<C>, g12_ten_158<C>, g12_ten_159<C>, g12_ten_160<C>,
        g12_ten_161<C>, g12_ten_162<C>, g12_ten_163<C>, g12_ten_164<C>, g12_ten_165<C>,
        g12_ten_166<C>, g12_ten_167<C>, g12_ten_168<C>, g12_ten_169<C>, g12_ten_170<C>,
        g12_ten_171<C>, g12_ten_172<C>, g12_ten_173<C>, g12_ten_174<C>, g12_ten_175<C>,
        g12_ten_176<C>, g12_ten_177<C>, g12_ten_178<C>, g12_ten_179<C>, g12_ten_180<C>,
        g12_ten_181<C>, g12_ten_182<C>, g12_ten_183<C>, g12_ten_184<C>, g12_ten_185<C>,
        g12_ten_186<C>, g12_ten_187<C>, g12_ten_188<C>, g12_ten_189<C>, g12_ten_190<C>,
        g12_ten_191<C>, g12_ten_192<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = g12_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        g12_ten_1<CHP>,   g12_ten_2<CHP>,   g12_ten_3<CHP>,   g12_ten_4<CHP>,   g12_ten_5<CHP>,
        g12_ten_6<CHP>,   g12_ten_7<CHP>,   g12_ten_8<CHP>,   g12_ten_9<CHP>,   g12_ten_10<CHP>,
        g12_ten_11<CHP>,  g12_ten_12<CHP>,  g12_ten_13<CHP>,  g12_ten_14<CHP>,  g12_ten_15<CHP>,
        g12_ten_16<CHP>,  g12_ten_17<CHP>,  g12_ten_18<CHP>,  g12_ten_19<CHP>,  g12_ten_20<CHP>,
        g12_ten_21<CHP>,  g12_ten_22<CHP>,  g12_ten_23<CHP>,  g12_ten_24<CHP>,  g12_ten_25<CHP>,
        g12_ten_26<CHP>,  g12_ten_27<CHP>,  g12_ten_28<CHP>,  g12_ten_29<CHP>,  g12_ten_30<CHP>,
        g12_ten_31<CHP>,  g12_ten_32<CHP>,  g12_ten_33<CHP>,  g12_ten_34<CHP>,  g12_ten_35<CHP>,
        g12_ten_36<CHP>,  g12_ten_37<CHP>,  g12_ten_38<CHP>,  g12_ten_39<CHP>,  g12_ten_40<CHP>,
        g12_ten_41<CHP>,  g12_ten_42<CHP>,  g12_ten_43<CHP>,  g12_ten_44<CHP>,  g12_ten_45<CHP>,
        g12_ten_46<CHP>,  g12_ten_47<CHP>,  g12_ten_48<CHP>,  g12_ten_49<CHP>,  g12_ten_50<CHP>,
        g12_ten_51<CHP>,  g12_ten_52<CHP>,  g12_ten_53<CHP>,  g12_ten_54<CHP>,  g12_ten_55<CHP>,
        g12_ten_56<CHP>,  g12_ten_57<CHP>,  g12_ten_58<CHP>,  g12_ten_59<CHP>,  g12_ten_60<CHP>,
        g12_ten_61<CHP>,  g12_ten_62<CHP>,  g12_ten_63<CHP>,  g12_ten_64<CHP>,  g12_ten_65<CHP>,
        g12_ten_66<CHP>,  g12_ten_67<CHP>,  g12_ten_68<CHP>,  g12_ten_69<CHP>,  g12_ten_70<CHP>,
        g12_ten_71<CHP>,  g12_ten_72<CHP>,  g12_ten_73<CHP>,  g12_ten_74<CHP>,  g12_ten_75<CHP>,
        g12_ten_76<CHP>,  g12_ten_77<CHP>,  g12_ten_78<CHP>,  g12_ten_79<CHP>,  g12_ten_80<CHP>,
        g12_ten_81<CHP>,  g12_ten_82<CHP>,  g12_ten_83<CHP>,  g12_ten_84<CHP>,  g12_ten_85<CHP>,
        g12_ten_86<CHP>,  g12_ten_87<CHP>,  g12_ten_88<CHP>,  g12_ten_89<CHP>,  g12_ten_90<CHP>,
        g12_ten_91<CHP>,  g12_ten_92<CHP>,  g12_ten_93<CHP>,  g12_ten_94<CHP>,  g12_ten_95<CHP>,
        g12_ten_96<CHP>,  g12_ten_97<CHP>,  g12_ten_98<CHP>,  g12_ten_99<CHP>,  g12_ten_100<CHP>,
        g12_ten_101<CHP>, g12_ten_102<CHP>, g12_ten_103<CHP>, g12_ten_104<CHP>, g12_ten_105<CHP>,
        g12_ten_106<CHP>, g12_ten_107<CHP>, g12_ten_108<CHP>, g12_ten_109<CHP>, g12_ten_110<CHP>,
        g12_ten_111<CHP>, g12_ten_112<CHP>, g12_ten_113<CHP>, g12_ten_114<CHP>, g12_ten_115<CHP>,
        g12_ten_116<CHP>, g12_ten_117<CHP>, g12_ten_118<CHP>, g12_ten_119<CHP>, g12_ten_120<CHP>,
        g12_ten_121<CHP>, g12_ten_122<CHP>, g12_ten_123<CHP>, g12_ten_124<CHP>, g12_ten_125<CHP>,
        g12_ten_126<CHP>, g12_ten_127<CHP>, g12_ten_128<CHP>, g12_ten_129<CHP>, g12_ten_130<CHP>,
        g12_ten_131<CHP>, g12_ten_132<CHP>, g12_ten_133<CHP>, g12_ten_134<CHP>, g12_ten_135<CHP>,
        g12_ten_136<CHP>, g12_ten_137<CHP>, g12_ten_138<CHP>, g12_ten_139<CHP>, g12_ten_140<CHP>,
        g12_ten_141<CHP>, g12_ten_142<CHP>, g12_ten_143<CHP>, g12_ten_144<CHP>, g12_ten_145<CHP>,
        g12_ten_146<CHP>, g12_ten_147<CHP>, g12_ten_148<CHP>, g12_ten_149<CHP>, g12_ten_150<CHP>,
        g12_ten_151<CHP>, g12_ten_152<CHP>, g12_ten_153<CHP>, g12_ten_154<CHP>, g12_ten_155<CHP>,
        g12_ten_156<CHP>, g12_ten_157<CHP>, g12_ten_158<CHP>, g12_ten_159<CHP>, g12_ten_160<CHP>,
        g12_ten_161<CHP>, g12_ten_162<CHP>, g12_ten_163<CHP>, g12_ten_164<CHP>, g12_ten_165<CHP>,
        g12_ten_166<CHP>, g12_ten_167<CHP>, g12_ten_168<CHP>, g12_ten_169<CHP>, g12_ten_170<CHP>,
        g12_ten_171<CHP>, g12_ten_172<CHP>, g12_ten_173<CHP>, g12_ten_174<CHP>, g12_ten_175<CHP>,
        g12_ten_176<CHP>, g12_ten_177<CHP>, g12_ten_178<CHP>, g12_ten_179<CHP>, g12_ten_180<CHP>,
        g12_ten_181<CHP>, g12_ten_182<CHP>, g12_ten_183<CHP>, g12_ten_184<CHP>, g12_ten_185<CHP>,
        g12_ten_186<CHP>, g12_ten_187<CHP>, g12_ten_188<CHP>, g12_ten_189<CHP>, g12_ten_190<CHP>,
        g12_ten_191<CHP>, g12_ten_192<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = g12_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        g12_ten_1<CVHP>,   g12_ten_2<CVHP>,   g12_ten_3<CVHP>,   g12_ten_4<CVHP>,
        g12_ten_5<CVHP>,   g12_ten_6<CVHP>,   g12_ten_7<CVHP>,   g12_ten_8<CVHP>,
        g12_ten_9<CVHP>,   g12_ten_10<CVHP>,  g12_ten_11<CVHP>,  g12_ten_12<CVHP>,
        g12_ten_13<CVHP>,  g12_ten_14<CVHP>,  g12_ten_15<CVHP>,  g12_ten_16<CVHP>,
        g12_ten_17<CVHP>,  g12_ten_18<CVHP>,  g12_ten_19<CVHP>,  g12_ten_20<CVHP>,
        g12_ten_21<CVHP>,  g12_ten_22<CVHP>,  g12_ten_23<CVHP>,  g12_ten_24<CVHP>,
        g12_ten_25<CVHP>,  g12_ten_26<CVHP>,  g12_ten_27<CVHP>,  g12_ten_28<CVHP>,
        g12_ten_29<CVHP>,  g12_ten_30<CVHP>,  g12_ten_31<CVHP>,  g12_ten_32<CVHP>,
        g12_ten_33<CVHP>,  g12_ten_34<CVHP>,  g12_ten_35<CVHP>,  g12_ten_36<CVHP>,
        g12_ten_37<CVHP>,  g12_ten_38<CVHP>,  g12_ten_39<CVHP>,  g12_ten_40<CVHP>,
        g12_ten_41<CVHP>,  g12_ten_42<CVHP>,  g12_ten_43<CVHP>,  g12_ten_44<CVHP>,
        g12_ten_45<CVHP>,  g12_ten_46<CVHP>,  g12_ten_47<CVHP>,  g12_ten_48<CVHP>,
        g12_ten_49<CVHP>,  g12_ten_50<CVHP>,  g12_ten_51<CVHP>,  g12_ten_52<CVHP>,
        g12_ten_53<CVHP>,  g12_ten_54<CVHP>,  g12_ten_55<CVHP>,  g12_ten_56<CVHP>,
        g12_ten_57<CVHP>,  g12_ten_58<CVHP>,  g12_ten_59<CVHP>,  g12_ten_60<CVHP>,
        g12_ten_61<CVHP>,  g12_ten_62<CVHP>,  g12_ten_63<CVHP>,  g12_ten_64<CVHP>,
        g12_ten_65<CVHP>,  g12_ten_66<CVHP>,  g12_ten_67<CVHP>,  g12_ten_68<CVHP>,
        g12_ten_69<CVHP>,  g12_ten_70<CVHP>,  g12_ten_71<CVHP>,  g12_ten_72<CVHP>,
        g12_ten_73<CVHP>,  g12_ten_74<CVHP>,  g12_ten_75<CVHP>,  g12_ten_76<CVHP>,
        g12_ten_77<CVHP>,  g12_ten_78<CVHP>,  g12_ten_79<CVHP>,  g12_ten_80<CVHP>,
        g12_ten_81<CVHP>,  g12_ten_82<CVHP>,  g12_ten_83<CVHP>,  g12_ten_84<CVHP>,
        g12_ten_85<CVHP>,  g12_ten_86<CVHP>,  g12_ten_87<CVHP>,  g12_ten_88<CVHP>,
        g12_ten_89<CVHP>,  g12_ten_90<CVHP>,  g12_ten_91<CVHP>,  g12_ten_92<CVHP>,
        g12_ten_93<CVHP>,  g12_ten_94<CVHP>,  g12_ten_95<CVHP>,  g12_ten_96<CVHP>,
        g12_ten_97<CVHP>,  g12_ten_98<CVHP>,  g12_ten_99<CVHP>,  g12_ten_100<CVHP>,
        g12_ten_101<CVHP>, g12_ten_102<CVHP>, g12_ten_103<CVHP>, g12_ten_104<CVHP>,
        g12_ten_105<CVHP>, g12_ten_106<CVHP>, g12_ten_107<CVHP>, g12_ten_108<CVHP>,
        g12_ten_109<CVHP>, g12_ten_110<CVHP>, g12_ten_111<CVHP>, g12_ten_112<CVHP>,
        g12_ten_113<CVHP>, g12_ten_114<CVHP>, g12_ten_115<CVHP>, g12_ten_116<CVHP>,
        g12_ten_117<CVHP>, g12_ten_118<CVHP>, g12_ten_119<CVHP>, g12_ten_120<CVHP>,
        g12_ten_121<CVHP>, g12_ten_122<CVHP>, g12_ten_123<CVHP>, g12_ten_124<CVHP>,
        g12_ten_125<CVHP>, g12_ten_126<CVHP>, g12_ten_127<CVHP>, g12_ten_128<CVHP>,
        g12_ten_129<CVHP>, g12_ten_130<CVHP>, g12_ten_131<CVHP>, g12_ten_132<CVHP>,
        g12_ten_133<CVHP>, g12_ten_134<CVHP>, g12_ten_135<CVHP>, g12_ten_136<CVHP>,
        g12_ten_137<CVHP>, g12_ten_138<CVHP>, g12_ten_139<CVHP>, g12_ten_140<CVHP>,
        g12_ten_141<CVHP>, g12_ten_142<CVHP>, g12_ten_143<CVHP>, g12_ten_144<CVHP>,
        g12_ten_145<CVHP>, g12_ten_146<CVHP>, g12_ten_147<CVHP>, g12_ten_148<CVHP>,
        g12_ten_149<CVHP>, g12_ten_150<CVHP>, g12_ten_151<CVHP>, g12_ten_152<CVHP>,
        g12_ten_153<CVHP>, g12_ten_154<CVHP>, g12_ten_155<CVHP>, g12_ten_156<CVHP>,
        g12_ten_157<CVHP>, g12_ten_158<CVHP>, g12_ten_159<CVHP>, g12_ten_160<CVHP>,
        g12_ten_161<CVHP>, g12_ten_162<CVHP>, g12_ten_163<CVHP>, g12_ten_164<CVHP>,
        g12_ten_165<CVHP>, g12_ten_166<CVHP>, g12_ten_167<CVHP>, g12_ten_168<CVHP>,
        g12_ten_169<CVHP>, g12_ten_170<CVHP>, g12_ten_171<CVHP>, g12_ten_172<CVHP>,
        g12_ten_173<CVHP>, g12_ten_174<CVHP>, g12_ten_175<CVHP>, g12_ten_176<CVHP>,
        g12_ten_177<CVHP>, g12_ten_178<CVHP>, g12_ten_179<CVHP>, g12_ten_180<CVHP>,
        g12_ten_181<CVHP>, g12_ten_182<CVHP>, g12_ten_183<CVHP>, g12_ten_184<CVHP>,
        g12_ten_185<CVHP>, g12_ten_186<CVHP>, g12_ten_187<CVHP>, g12_ten_188<CVHP>,
        g12_ten_189<CVHP>, g12_ten_190<CVHP>, g12_ten_191<CVHP>, g12_ten_192<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = g12_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        g12_ten_1<F32>,   g12_ten_2<F32>,   g12_ten_3<F32>,   g12_ten_4<F32>,   g12_ten_5<F32>,
        g12_ten_6<F32>,   g12_ten_7<F32>,   g12_ten_8<F32>,   g12_ten_9<F32>,   g12_ten_10<F32>,
        g12_ten_11<F32>,  g12_ten_12<F32>,  g12_ten_13<F32>,  g12_ten_14<F32>,  g12_ten_15<F32>,
        g12_ten_16<F32>,  g12_ten_17<F32>,  g12_ten_18<F32>,  g12_ten_19<F32>,  g12_ten_20<F32>,
        g12_ten_21<F32>,  g12_ten_22<F32>,  g12_ten_23<F32>,  g12_ten_24<F32>,  g12_ten_25<F32>,
        g12_ten_26<F32>,  g12_ten_27<F32>,  g12_ten_28<F32>,  g12_ten_29<F32>,  g12_ten_30<F32>,
        g12_ten_31<F32>,  g12_ten_32<F32>,  g12_ten_33<F32>,  g12_ten_34<F32>,  g12_ten_35<F32>,
        g12_ten_36<F32>,  g12_ten_37<F32>,  g12_ten_38<F32>,  g12_ten_39<F32>,  g12_ten_40<F32>,
        g12_ten_41<F32>,  g12_ten_42<F32>,  g12_ten_43<F32>,  g12_ten_44<F32>,  g12_ten_45<F32>,
        g12_ten_46<F32>,  g12_ten_47<F32>,  g12_ten_48<F32>,  g12_ten_49<F32>,  g12_ten_50<F32>,
        g12_ten_51<F32>,  g12_ten_52<F32>,  g12_ten_53<F32>,  g12_ten_54<F32>,  g12_ten_55<F32>,
        g12_ten_56<F32>,  g12_ten_57<F32>,  g12_ten_58<F32>,  g12_ten_59<F32>,  g12_ten_60<F32>,
        g12_ten_61<F32>,  g12_ten_62<F32>,  g12_ten_63<F32>,  g12_ten_64<F32>,  g12_ten_65<F32>,
        g12_ten_66<F32>,  g12_ten_67<F32>,  g12_ten_68<F32>,  g12_ten_69<F32>,  g12_ten_70<F32>,
        g12_ten_71<F32>,  g12_ten_72<F32>,  g12_ten_73<F32>,  g12_ten_74<F32>,  g12_ten_75<F32>,
        g12_ten_76<F32>,  g12_ten_77<F32>,  g12_ten_78<F32>,  g12_ten_79<F32>,  g12_ten_80<F32>,
        g12_ten_81<F32>,  g12_ten_82<F32>,  g12_ten_83<F32>,  g12_ten_84<F32>,  g12_ten_85<F32>,
        g12_ten_86<F32>,  g12_ten_87<F32>,  g12_ten_88<F32>,  g12_ten_89<F32>,  g12_ten_90<F32>,
        g12_ten_91<F32>,  g12_ten_92<F32>,  g12_ten_93<F32>,  g12_ten_94<F32>,  g12_ten_95<F32>,
        g12_ten_96<F32>,  g12_ten_97<F32>,  g12_ten_98<F32>,  g12_ten_99<F32>,  g12_ten_100<F32>,
        g12_ten_101<F32>, g12_ten_102<F32>, g12_ten_103<F32>, g12_ten_104<F32>, g12_ten_105<F32>,
        g12_ten_106<F32>, g12_ten_107<F32>, g12_ten_108<F32>, g12_ten_109<F32>, g12_ten_110<F32>,
        g12_ten_111<F32>, g12_ten_112<F32>, g12_ten_113<F32>, g12_ten_114<F32>, g12_ten_115<F32>,
        g12_ten_116<F32>, g12_ten_117<F32>, g12_ten_118<F32>, g12_ten_119<F32>, g12_ten_120<F32>,
        g12_ten_121<F32>, g12_ten_122<F32>, g12_ten_123<F32>, g12_ten_124<F32>, g12_ten_125<F32>,
        g12_ten_126<F32>, g12_ten_127<F32>, g12_ten_128<F32>, g12_ten_129<F32>, g12_ten_130<F32>,
        g12_ten_131<F32>, g12_ten_132<F32>, g12_ten_133<F32>, g12_ten_134<F32>, g12_ten_135<F32>,
        g12_ten_136<F32>, g12_ten_137<F32>, g12_ten_138<F32>, g12_ten_139<F32>, g12_ten_140<F32>,
        g12_ten_141<F32>, g12_ten_142<F32>, g12_ten_143<F32>, g12_ten_144<F32>, g12_ten_145<F32>,
        g12_ten_146<F32>, g12_ten_147<F32>, g12_ten_148<F32>, g12_ten_149<F32>, g12_ten_150<F32>,
        g12_ten_151<F32>, g12_ten_152<F32>, g12_ten_153<F32>, g12_ten_154<F32>, g12_ten_155<F32>,
        g12_ten_156<F32>, g12_ten_157<F32>, g12_ten_158<F32>, g12_ten_159<F32>, g12_ten_160<F32>,
        g12_ten_161<F32>, g12_ten_162<F32>, g12_ten_163<F32>, g12_ten_164<F32>, g12_ten_165<F32>,
        g12_ten_166<F32>, g12_ten_167<F32>, g12_ten_168<F32>, g12_ten_169<F32>, g12_ten_170<F32>,
        g12_ten_171<F32>, g12_ten_172<F32>, g12_ten_173<F32>, g12_ten_174<F32>, g12_ten_175<F32>,
        g12_ten_176<F32>, g12_ten_177<F32>, g12_ten_178<F32>, g12_ten_179<F32>, g12_ten_180<F32>,
        g12_ten_181<F32>, g12_ten_182<F32>, g12_ten_183<F32>, g12_ten_184<F32>, g12_ten_185<F32>,
        g12_ten_186<F32>, g12_ten_187<F32>, g12_ten_188<F32>, g12_ten_189<F32>, g12_ten_190<F32>,
        g12_ten_191<F32>, g12_ten_192<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0]], Strand[Link[0], "
        "Bead[Leg[1, 1]], Link[1], Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0]], "
        "Strand[Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2], Bead[Leg[3, 2]], Link[0]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",  "ten_2",  "ten_3",  "ten_4",  "ten_5",  "ten_6",  "ten_7",  "ten_8",  "ten_9",
        "ten_10", "ten_11", "ten_12", "ten_13", "ten_14", "ten_15", "ten_16", "ten_17", "ten_18",
        "ten_19", "ten_20", "ten_21", "ten_22", "ten_23", "ten_24", "ten_25", "ten_26", "ten_27",
        "ten_28", "ten_29", "ten_30", "ten_31", "ten_32", "ten_33", "ten_34", "ten_35", "ten_36",
        "ten_37", "ten_38", "ten_39", "ten_40", "ten_41", "ten_42", "ten_43", "ten_44", "ten_45",
        "ten_46", "ten_47", "ten_48", "ten_49", "ten_50", "ten_51", "ten_52", "ten_53", "ten_54",
        "ten_55", "ten_56", "ten_57", "ten_58", "ten_59", "ten_60", "ten_61", "ten_62", "ten_63",
        "ten_64", "ten_65", "ten_66", "ten_67", "ten_68", "ten_69", "ten_70", "ten_71"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = H_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        H_ten_1<C>,  H_ten_2<C>,  H_ten_3<C>,  H_ten_4<C>,  H_ten_5<C>,  H_ten_6<C>,  H_ten_7<C>,
        H_ten_8<C>,  H_ten_9<C>,  H_ten_10<C>, H_ten_11<C>, H_ten_12<C>, H_ten_13<C>, H_ten_14<C>,
        H_ten_15<C>, H_ten_16<C>, H_ten_17<C>, H_ten_18<C>, H_ten_19<C>, H_ten_20<C>, H_ten_21<C>,
        H_ten_22<C>, H_ten_23<C>, H_ten_24<C>, H_ten_25<C>, H_ten_26<C>, H_ten_27<C>, H_ten_28<C>,
        H_ten_29<C>, H_ten_30<C>, H_ten_31<C>, H_ten_32<C>, H_ten_33<C>, H_ten_34<C>, H_ten_35<C>,
        H_ten_36<C>, H_ten_37<C>, H_ten_38<C>, H_ten_39<C>, H_ten_40<C>, H_ten_41<C>, H_ten_42<C>,
        H_ten_43<C>, H_ten_44<C>, H_ten_45<C>, H_ten_46<C>, H_ten_47<C>, H_ten_48<C>, H_ten_49<C>,
        H_ten_50<C>, H_ten_51<C>, H_ten_52<C>, H_ten_53<C>, H_ten_54<C>, H_ten_55<C>, H_ten_56<C>,
        H_ten_57<C>, H_ten_58<C>, H_ten_59<C>, H_ten_60<C>, H_ten_61<C>, H_ten_62<C>, H_ten_63<C>,
        H_ten_64<C>, H_ten_65<C>, H_ten_66<C>, H_ten_67<C>, H_ten_68<C>, H_ten_69<C>, H_ten_70<C>,
        H_ten_71<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = H_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        H_ten_1<CHP>,  H_ten_2<CHP>,  H_ten_3<CHP>,  H_ten_4<CHP>,  H_ten_5<CHP>,  H_ten_6<CHP>,
        H_ten_7<CHP>,  H_ten_8<CHP>,  H_ten_9<CHP>,  H_ten_10<CHP>, H_ten_11<CHP>, H_ten_12<CHP>,
        H_ten_13<CHP>, H_ten_14<CHP>, H_ten_15<CHP>, H_ten_16<CHP>, H_ten_17<CHP>, H_ten_18<CHP>,
        H_ten_19<CHP>, H_ten_20<CHP>, H_ten_21<CHP>, H_ten_22<CHP>, H_ten_23<CHP>, H_ten_24<CHP>,
        H_ten_25<CHP>, H_ten_26<CHP>, H_ten_27<CHP>, H_ten_28<CHP>, H_ten_29<CHP>, H_ten_30<CHP>,
        H_ten_31<CHP>, H_ten_32<CHP>, H_ten_33<CHP>, H_ten_34<CHP>, H_ten_35<CHP>, H_ten_36<CHP>,
        H_ten_37<CHP>, H_ten_38<CHP>, H_ten_39<CHP>, H_ten_40<CHP>, H_ten_41<CHP>, H_ten_42<CHP>,
        H_ten_43<CHP>, H_ten_44<CHP>, H_ten_45<CHP>, H_ten_46<CHP>, H_ten_47<CHP>, H_ten_48<CHP>,
        H_ten_49<CHP>, H_ten_50<CHP>, H_ten_51<CHP>, H_ten_52<CHP>, H_ten_53<CHP>, H_ten_54<CHP>,
        H_ten_55<CHP>, H_ten_56<CHP>, H_ten_57<CHP>, H_ten_58<CHP>, H_ten_59<CHP>, H_ten_60<CHP>,
        H_ten_61<CHP>, H_ten_62<CHP>, H_ten_63<CHP>, H_ten_64<CHP>, H_ten_65<CHP>, H_ten_66<CHP>,
        H_ten_67<CHP>, H_ten_68<CHP>, H_ten_69<CHP>, H_ten_70<CHP>, H_ten_71<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = H_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        H_ten_1<CVHP>,  H_ten_2<CVHP>,  H_ten_3<CVHP>,  H_ten_4<CVHP>,  H_ten_5<CVHP>,
        H_ten_6<CVHP>,  H_ten_7<CVHP>,  H_ten_8<CVHP>,  H_ten_9<CVHP>,  H_ten_10<CVHP>,
        H_ten_11<CVHP>, H_ten_12<CVHP>, H_ten_13<CVHP>, H_ten_14<CVHP>, H_ten_15<CVHP>,
        H_ten_16<CVHP>, H_ten_17<CVHP>, H_ten_18<CVHP>, H_ten_19<CVHP>, H_ten_20<CVHP>,
        H_ten_21<CVHP>, H_ten_22<CVHP>, H_ten_23<CVHP>, H_ten_24<CVHP>, H_ten_25<CVHP>,
        H_ten_26<CVHP>, H_ten_27<CVHP>, H_ten_28<CVHP>, H_ten_29<CVHP>, H_ten_30<CVHP>,
        H_ten_31<CVHP>, H_ten_32<CVHP>, H_ten_33<CVHP>, H_ten_34<CVHP>, H_ten_35<CVHP>,
        H_ten_36<CVHP>, H_ten_37<CVHP>, H_ten_38<CVHP>, H_ten_39<CVHP>, H_ten_40<CVHP>,
        H_ten_41<CVHP>, H_ten_42<CVHP>, H_ten_43<CVHP>, H_ten_44<CVHP>, H_ten_45<CVHP>,
        H_ten_46<CVHP>, H_ten_47<CVHP>, H_ten_48<CVHP>, H_ten_49<CVHP>, H_ten_50<CVHP>,
        H_ten_51<CVHP>, H_ten_52<CVHP>, H_ten_53<CVHP>, H_ten_54<CVHP>, H_ten_55<CVHP>,
        H_ten_56<CVHP>, H_ten_57<CVHP>, H_ten_58<CVHP>, H_ten_59<CVHP>, H_ten_60<CVHP>,
        H_ten_61<CVHP>, H_ten_62<CVHP>, H_ten_63<CVHP>, H_ten_64<CVHP>, H_ten_65<CVHP>,
        H_ten_66<CVHP>, H_ten_67<CVHP>, H_ten_68<CVHP>, H_ten_69<CVHP>, H_ten_70<CVHP>,
        H_ten_71<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = H_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        H_ten_1<F32>,  H_ten_2<F32>,  H_ten_3<F32>,  H_ten_4<F32>,  H_ten_5<F32>,  H_ten_6<F32>,
        H_ten_7<F32>,  H_ten_8<F32>,  H_ten_9<F32>,  H_ten_10<F32>, H_ten_11<F32>, H_ten_12<F32>,
        H_ten_13<F32>, H_ten_14<F32>, H_ten_15<F32>, H_ten_16<F32>, H_ten_17<F32>, H_ten_18<F32>,
        H_ten_19<F32>, H_ten_20<F32>, H_ten_21<F32>, H_ten_22<F32>, H_ten_23<F32>, H_ten_24<F32>,
        H_ten_25<F32>, H_ten_26<F32>, H_ten_27<F32>, H_ten_28<F32>, H_ten_29<F32>, H_ten_30<F32>,
        H_ten_31<F32>, H_ten_32<F32>, H_ten_33<F32>, H_ten_34<F32>, H_ten_35<F32>, H_ten_36<F32>,
        H_ten_37<F32>, H_ten_38<F32>, H_ten_39<F32>, H_ten_40<F32>, H_ten_41<F32>, H_ten_42<F32>,
        H_ten_43<F32>, H_ten_44<F32>, H_ten_45<F32>, H_ten_46<F32>, H_ten_47<F32>, H_ten_48<F32>,
        H_ten_49<F32>, H_ten_50<F32>, H_ten_51<F32>, H_ten_52<F32>, H_ten_53<F32>, H_ten_54<F32>,
        H_ten_55<F32>, H_ten_56<F32>, H_ten_57<F32>, H_ten_58<F32>, H_ten_59<F32>, H_ten_60<F32>,
        H_ten_61<F32>, H_ten_62<F32>, H_ten_63<F32>, H_ten_64<F32>, H_ten_65<F32>, H_ten_66<F32>,
        H_ten_67<F32>, H_ten_68<F32>, H_ten_69<F32>, H_ten_70<F32>, H_ten_71<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0]], Strand[Link[1], "
        "Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]], Strand[Link[1], "
        "Bead[Leg[1, 1]], Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",  "ten_2",  "ten_3",  "ten_4",  "ten_5",  "ten_6",  "ten_7",  "ten_8",  "ten_9",
        "ten_10", "ten_11", "ten_12", "ten_13", "ten_14", "ten_15", "ten_16", "ten_17", "ten_18",
        "ten_19", "ten_20", "ten_21", "ten_22", "ten_23", "ten_24", "ten_25", "ten_26", "ten_27",
        "ten_28", "ten_29", "ten_30", "ten_31", "ten_32", "ten_33", "ten_34", "ten_35", "ten_36",
        "ten_37", "ten_38", "ten_39", "ten_40", "ten_41", "ten_42", "ten_43", "ten_44", "ten_45",
        "ten_46", "ten_47", "ten_48", "ten_49", "ten_50", "ten_51", "ten_52", "ten_53", "ten_54",
        "ten_55", "ten_56", "ten_57", "ten_58", "ten_59", "ten_60", "ten_61", "ten_62", "ten_63",
        "ten_64", "ten_65", "ten_66", "ten_67", "ten_68", "ten_69", "ten_70", "ten_71"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = III_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        III_ten_1<C>,  III_ten_2<C>,  III_ten_3<C>,  III_ten_4<C>,  III_ten_5<C>,  III_ten_6<C>,
        III_ten_7<C>,  III_ten_8<C>,  III_ten_9<C>,  III_ten_10<C>, III_ten_11<C>, III_ten_12<C>,
        III_ten_13<C>, III_ten_14<C>, III_ten_15<C>, III_ten_16<C>, III_ten_17<C>, III_ten_18<C>,
        III_ten_19<C>, III_ten_20<C>, III_ten_21<C>, III_ten_22<C>, III_ten_23<C>, III_ten_24<C>,
        III_ten_25<C>, III_ten_26<C>, III_ten_27<C>, III_ten_28<C>, III_ten_29<C>, III_ten_30<C>,
        III_ten_31<C>, III_ten_32<C>, III_ten_33<C>, III_ten_34<C>, III_ten_35<C>, III_ten_36<C>,
        III_ten_37<C>, III_ten_38<C>, III_ten_39<C>, III_ten_40<C>, III_ten_41<C>, III_ten_42<C>,
        III_ten_43<C>, III_ten_44<C>, III_ten_45<C>, III_ten_46<C>, III_ten_47<C>, III_ten_48<C>,
        III_ten_49<C>, III_ten_50<C>, III_ten_51<C>, III_ten_52<C>, III_ten_53<C>, III_ten_54<C>,
        III_ten_55<C>, III_ten_56<C>, III_ten_57<C>, III_ten_58<C>, III_ten_59<C>, III_ten_60<C>,
        III_ten_61<C>, III_ten_62<C>, III_ten_63<C>, III_ten_64<C>, III_ten_65<C>, III_ten_66<C>,
        III_ten_67<C>, III_ten_68<C>, III_ten_69<C>, III_ten_70<C>, III_ten_71<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = III_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        III_ten_1<CHP>,  III_ten_2<CHP>,  III_ten_3<CHP>,  III_ten_4<CHP>,  III_ten_5<CHP>,
        III_ten_6<CHP>,  III_ten_7<CHP>,  III_ten_8<CHP>,  III_ten_9<CHP>,  III_ten_10<CHP>,
        III_ten_11<CHP>, III_ten_12<CHP>, III_ten_13<CHP>, III_ten_14<CHP>, III_ten_15<CHP>,
        III_ten_16<CHP>, III_ten_17<CHP>, III_ten_18<CHP>, III_ten_19<CHP>, III_ten_20<CHP>,
        III_ten_21<CHP>, III_ten_22<CHP>, III_ten_23<CHP>, III_ten_24<CHP>, III_ten_25<CHP>,
        III_ten_26<CHP>, III_ten_27<CHP>, III_ten_28<CHP>, III_ten_29<CHP>, III_ten_30<CHP>,
        III_ten_31<CHP>, III_ten_32<CHP>, III_ten_33<CHP>, III_ten_34<CHP>, III_ten_35<CHP>,
        III_ten_36<CHP>, III_ten_37<CHP>, III_ten_38<CHP>, III_ten_39<CHP>, III_ten_40<CHP>,
        III_ten_41<CHP>, III_ten_42<CHP>, III_ten_43<CHP>, III_ten_44<CHP>, III_ten_45<CHP>,
        III_ten_46<CHP>, III_ten_47<CHP>, III_ten_48<CHP>, III_ten_49<CHP>, III_ten_50<CHP>,
        III_ten_51<CHP>, III_ten_52<CHP>, III_ten_53<CHP>, III_ten_54<CHP>, III_ten_55<CHP>,
        III_ten_56<CHP>, III_ten_57<CHP>, III_ten_58<CHP>, III_ten_59<CHP>, III_ten_60<CHP>,
        III_ten_61<CHP>, III_ten_62<CHP>, III_ten_63<CHP>, III_ten_64<CHP>, III_ten_65<CHP>,
        III_ten_66<CHP>, III_ten_67<CHP>, III_ten_68<CHP>, III_ten_69<CHP>, III_ten_70<CHP>,
        III_ten_71<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = III_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        III_ten_1<CVHP>,  III_ten_2<CVHP>,  III_ten_3<CVHP>,  III_ten_4<CVHP>,  III_ten_5<CVHP>,
        III_ten_6<CVHP>,  III_ten_7<CVHP>,  III_ten_8<CVHP>,  III_ten_9<CVHP>,  III_ten_10<CVHP>,
        III_ten_11<CVHP>, III_ten_12<CVHP>, III_ten_13<CVHP>, III_ten_14<CVHP>, III_ten_15<CVHP>,
        III_ten_16<CVHP>, III_ten_17<CVHP>, III_ten_18<CVHP>, III_ten_19<CVHP>, III_ten_20<CVHP>,
        III_ten_21<CVHP>, III_ten_22<CVHP>, III_ten_23<CVHP>, III_ten_24<CVHP>, III_ten_25<CVHP>,
        III_ten_26<CVHP>, III_ten_27<CVHP>, III_ten_28<CVHP>, III_ten_29<CVHP>, III_ten_30<CVHP>,
        III_ten_31<CVHP>, III_ten_32<CVHP>, III_ten_33<CVHP>, III_ten_34<CVHP>, III_ten_35<CVHP>,
        III_ten_36<CVHP>, III_ten_37<CVHP>, III_ten_38<CVHP>, III_ten_39<CVHP>, III_ten_40<CVHP>,
        III_ten_41<CVHP>, III_ten_42<CVHP>, III_ten_43<CVHP>, III_ten_44<CVHP>, III_ten_45<CVHP>,
        III_ten_46<CVHP>, III_ten_47<CVHP>, III_ten_48<CVHP>, III_ten_49<CVHP>, III_ten_50<CVHP>,
        III_ten_51<CVHP>, III_ten_52<CVHP>, III_ten_53<CVHP>, III_ten_54<CVHP>, III_ten_55<CVHP>,
        III_ten_56<CVHP>, III_ten_57<CVHP>, III_ten_58<CVHP>, III_ten_59<CVHP>, III_ten_60<CVHP>,
        III_ten_61<CVHP>, III_ten_62<CVHP>, III_ten_63<CVHP>, III_ten_64<CVHP>, III_ten_65<CVHP>,
        III_ten_66<CVHP>, III_ten_67<CVHP>, III_ten_68<CVHP>, III_ten_69<CVHP>, III_ten_70<CVHP>,
        III_ten_71<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = III_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        III_ten_1<F32>,  III_ten_2<F32>,  III_ten_3<F32>,  III_ten_4<F32>,  III_ten_5<F32>,
        III_ten_6<F32>,  III_ten_7<F32>,  III_ten_8<F32>,  III_ten_9<F32>,  III_ten_10<F32>,
        III_ten_11<F32>, III_ten_12<F32>, III_ten_13<F32>, III_ten_14<F32>, III_ten_15<F32>,
        III_ten_16<F32>, III_ten_17<F32>, III_ten_18<F32>, III_ten_19<F32>, III_ten_20<F32>,
        III_ten_21<F32>, III_ten_22<F32>, III_ten_23<F32>, III_ten_24<F32>, III_ten_25<F32>,
        III_ten_26<F32>, III_ten_27<F32>, III_ten_28<F32>, III_ten_29<F32>, III_ten_30<F32>,
        III_ten_31<F32>, III_ten_32<F32>, III_ten_33<F32>, III_ten_34<F32>, III_ten_35<F32>,
        III_ten_36<F32>, III_ten_37<F32>, III_ten_38<F32>, III_ten_39<F32>, III_ten_40<F32>,
        III_ten_41<F32>, III_ten_42<F32>, III_ten_43<F32>, III_ten_44<F32>, III_ten_45<F32>,
        III_ten_46<F32>, III_ten_47<F32>, III_ten_48<F32>, III_ten_49<F32>, III_ten_50<F32>,
        III_ten_51<F32>, III_ten_52<F32>, III_ten_53<F32>, III_ten_54<F32>, III_ten_55<F32>,
        III_ten_56<F32>, III_ten_57<F32>, III_ten_58<F32>, III_ten_59<F32>, III_ten_60<F32>,
        III_ten_61<F32>, III_ten_62<F32>, III_ten_63<F32>, III_ten_64<F32>, III_ten_65<F32>,
        III_ten_66<F32>, III_ten_67<F32>, III_ten_68<F32>, III_ten_69<F32>, III_ten_70<F32>,
        III_ten_71<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[2], 0], "
        "Bead[Leg[4, 1]], Link[1]], Strand[Link[2], Bead[Leg[3, 2]], Link[0]], Strand[Link[2], "
        "Bead[Leg[2, 2]], Link[LoopMomentum[1], 0], Bead[Leg[1, 1]], Link[1]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",  "ten_2",  "ten_3",  "ten_4",  "ten_5",  "ten_6",  "ten_7",  "ten_8",  "ten_9",
        "ten_10", "ten_11", "ten_12", "ten_13", "ten_14", "ten_15", "ten_16", "ten_17", "ten_18",
        "ten_19", "ten_20", "ten_21", "ten_22", "ten_23", "ten_24", "ten_25", "ten_26", "ten_27",
        "ten_28", "ten_29", "ten_30", "ten_31", "ten_32", "ten_33", "ten_34", "ten_35", "ten_36",
        "ten_37", "ten_38", "ten_39", "ten_40", "ten_41", "ten_42", "ten_43", "ten_44", "ten_45",
        "ten_46", "ten_47", "ten_48", "ten_49", "ten_50", "ten_51", "ten_52", "ten_53", "ten_54",
        "ten_55", "ten_56", "ten_57", "ten_58", "ten_59", "ten_60", "ten_61", "ten_62", "ten_63",
        "ten_64", "ten_65", "ten_66", "ten_67", "ten_68", "ten_69", "ten_70", "ten_71", "ten_72",
        "ten_73", "ten_74", "ten_75", "ten_76", "ten_77", "ten_78"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = IX_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        IX_ten_1<C>,  IX_ten_2<C>,  IX_ten_3<C>,  IX_ten_4<C>,  IX_ten_5<C>,  IX_ten_6<C>,
        IX_ten_7<C>,  IX_ten_8<C>,  IX_ten_9<C>,  IX_ten_10<C>, IX_ten_11<C>, IX_ten_12<C>,
        IX_ten_13<C>, IX_ten_14<C>, IX_ten_15<C>, IX_ten_16<C>, IX_ten_17<C>, IX_ten_18<C>,
        IX_ten_19<C>, IX_ten_20<C>, IX_ten_21<C>, IX_ten_22<C>, IX_ten_23<C>, IX_ten_24<C>,
        IX_ten_25<C>, IX_ten_26<C>, IX_ten_27<C>, IX_ten_28<C>, IX_ten_29<C>, IX_ten_30<C>,
        IX_ten_31<C>, IX_ten_32<C>, IX_ten_33<C>, IX_ten_34<C>, IX_ten_35<C>, IX_ten_36<C>,
        IX_ten_37<C>, IX_ten_38<C>, IX_ten_39<C>, IX_ten_40<C>, IX_ten_41<C>, IX_ten_42<C>,
        IX_ten_43<C>, IX_ten_44<C>, IX_ten_45<C>, IX_ten_46<C>, IX_ten_47<C>, IX_ten_48<C>,
        IX_ten_49<C>, IX_ten_50<C>, IX_ten_51<C>, IX_ten_52<C>, IX_ten_53<C>, IX_ten_54<C>,
        IX_ten_55<C>, IX_ten_56<C>, IX_ten_57<C>, IX_ten_58<C>, IX_ten_59<C>, IX_ten_60<C>,
        IX_ten_61<C>, IX_ten_62<C>, IX_ten_63<C>, IX_ten_64<C>, IX_ten_65<C>, IX_ten_66<C>,
        IX_ten_67<C>, IX_ten_68<C>, IX_ten_69<C>, IX_ten_70<C>, IX_ten_71<C>, IX_ten_72<C>,
        IX_ten_73<C>, IX_ten_74<C>, IX_ten_75<C>, IX_ten_76<C>, IX_ten_77<C>, IX_ten_78<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = IX_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        IX_ten_1<CHP>,  IX_ten_2<CHP>,  IX_ten_3<CHP>,  IX_ten_4<CHP>,  IX_ten_5<CHP>,
        IX_ten_6<CHP>,  IX_ten_7<CHP>,  IX_ten_8<CHP>,  IX_ten_9<CHP>,  IX_ten_10<CHP>,
        IX_ten_11<CHP>, IX_ten_12<CHP>, IX_ten_13<CHP>, IX_ten_14<CHP>, IX_ten_15<CHP>,
        IX_ten_16<CHP>, IX_ten_17<CHP>, IX_ten_18<CHP>, IX_ten_19<CHP>, IX_ten_20<CHP>,
        IX_ten_21<CHP>, IX_ten_22<CHP>, IX_ten_23<CHP>, IX_ten_24<CHP>, IX_ten_25<CHP>,
        IX_ten_26<CHP>, IX_ten_27<CHP>, IX_ten_28<CHP>, IX_ten_29<CHP>, IX_ten_30<CHP>,
        IX_ten_31<CHP>, IX_ten_32<CHP>, IX_ten_33<CHP>, IX_ten_34<CHP>, IX_ten_35<CHP>,
        IX_ten_36<CHP>, IX_ten_37<CHP>, IX_ten_38<CHP>, IX_ten_39<CHP>, IX_ten_40<CHP>,
        IX_ten_41<CHP>, IX_ten_42<CHP>, IX_ten_43<CHP>, IX_ten_44<CHP>, IX_ten_45<CHP>,
        IX_ten_46<CHP>, IX_ten_47<CHP>, IX_ten_48<CHP>, IX_ten_49<CHP>, IX_ten_50<CHP>,
        IX_ten_51<CHP>, IX_ten_52<CHP>, IX_ten_53<CHP>, IX_ten_54<CHP>, IX_ten_55<CHP>,
        IX_ten_56<CHP>, IX_ten_57<CHP>, IX_ten_58<CHP>, IX_ten_59<CHP>, IX_ten_60<CHP>,
        IX_ten_61<CHP>, IX_ten_62<CHP>, IX_ten_63<CHP>, IX_ten_64<CHP>, IX_ten_65<CHP>,
        IX_ten_66<CHP>, IX_ten_67<CHP>, IX_ten_68<CHP>, IX_ten_69<CHP>, IX_ten_70<CHP>,
        IX_ten_71<CHP>, IX_ten_72<CHP>, IX_ten_73<CHP>, IX_ten_74<CHP>, IX_ten_75<CHP>,
        IX_ten_76<CHP>, IX_ten_77<CHP>, IX_ten_78<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = IX_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        IX_ten_1<CVHP>,  IX_ten_2<CVHP>,  IX_ten_3<CVHP>,  IX_ten_4<CVHP>,  IX_ten_5<CVHP>,
        IX_ten_6<CVHP>,  IX_ten_7<CVHP>,  IX_ten_8<CVHP>,  IX_ten_9<CVHP>,  IX_ten_10<CVHP>,
        IX_ten_11<CVHP>, IX_ten_12<CVHP>, IX_ten_13<CVHP>, IX_ten_14<CVHP>, IX_ten_15<CVHP>,
        IX_ten_16<CVHP>, IX_ten_17<CVHP>, IX_ten_18<CVHP>, IX_ten_19<CVHP>, IX_ten_20<CVHP>,
        IX_ten_21<CVHP>, IX_ten_22<CVHP>, IX_ten_23<CVHP>, IX_ten_24<CVHP>, IX_ten_25<CVHP>,
        IX_ten_26<CVHP>, IX_ten_27<CVHP>, IX_ten_28<CVHP>, IX_ten_29<CVHP>, IX_ten_30<CVHP>,
        IX_ten_31<CVHP>, IX_ten_32<CVHP>, IX_ten_33<CVHP>, IX_ten_34<CVHP>, IX_ten_35<CVHP>,
        IX_ten_36<CVHP>, IX_ten_37<CVHP>, IX_ten_38<CVHP>, IX_ten_39<CVHP>, IX_ten_40<CVHP>,
        IX_ten_41<CVHP>, IX_ten_42<CVHP>, IX_ten_43<CVHP>, IX_ten_44<CVHP>, IX_ten_45<CVHP>,
        IX_ten_46<CVHP>, IX_ten_47<CVHP>, IX_ten_48<CVHP>, IX_ten_49<CVHP>, IX_ten_50<CVHP>,
        IX_ten_51<CVHP>, IX_ten_52<CVHP>, IX_ten_53<CVHP>, IX_ten_54<CVHP>, IX_ten_55<CVHP>,
        IX_ten_56<CVHP>, IX_ten_57<CVHP>, IX_ten_58<CVHP>, IX_ten_59<CVHP>, IX_ten_60<CVHP>,
        IX_ten_61<CVHP>, IX_ten_62<CVHP>, IX_ten_63<CVHP>, IX_ten_64<CVHP>, IX_ten_65<CVHP>,
        IX_ten_66<CVHP>, IX_ten_67<CVHP>, IX_ten_68<CVHP>, IX_ten_69<CVHP>, IX_ten_70<CVHP>,
        IX_ten_71<CVHP>, IX_ten_72<CVHP>, IX_ten_73<CVHP>, IX_ten_74<CVHP>, IX_ten_75<CVHP>,
        IX_ten_76<CVHP>, IX_ten_77<CVHP>, IX_ten_78<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = IX_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        IX_ten_1<F32>,  IX_ten_2<F32>,  IX_ten_3<F32>,  IX_ten_4<F32>,  IX_ten_5<F32>,
        IX_ten_6<F32>,  IX_ten_7<F32>,  IX_ten_8<F32>,  IX_ten_9<F32>,  IX_ten_10<F32>,
        IX_ten_11<F32>, IX_ten_12<F32>, IX_ten_13<F32>, IX_ten_14<F32>, IX_ten_15<F32>,
        IX_ten_16<F32>, IX_ten_17<F32>, IX_ten_18<F32>, IX_ten_19<F32>, IX_ten_20<F32>,
        IX_ten_21<F32>, IX_ten_22<F32>, IX_ten_23<F32>, IX_ten_24<F32>, IX_ten_25<F32>,
        IX_ten_26<F32>, IX_ten_27<F32>, IX_ten_28<F32>, IX_ten_29<F32>, IX_ten_30<F32>,
        IX_ten_31<F32>, IX_ten_32<F32>, IX_ten_33<F32>, IX_ten_34<F32>, IX_ten_35<F32>,
        IX_ten_36<F32>, IX_ten_37<F32>, IX_ten_38<F32>, IX_ten_39<F32>, IX_ten_40<F32>,
        IX_ten_41<F32>, IX_ten_42<F32>, IX_ten_43<F32>, IX_ten_44<F32>, IX_ten_45<F32>,
        IX_ten_46<F32>, IX_ten_47<F32>, IX_ten_48<F32>, IX_ten_49<F32>, IX_ten_50<F32>,
        IX_ten_51<F32>, IX_ten_52<F32>, IX_ten_53<F32>, IX_ten_54<F32>, IX_ten_55<F32>,
        IX_ten_56<F32>, IX_ten_57<F32>, IX_ten_58<F32>, IX_ten_59<F32>, IX_ten_60<F32>,
        IX_ten_61<F32>, IX_ten_62<F32>, IX_ten_63<F32>, IX_ten_64<F32>, IX_ten_65<F32>,
        IX_ten_66<F32>, IX_ten_67<F32>, IX_ten_68<F32>, IX_ten_69<F32>, IX_ten_70<F32>,
        IX_ten_71<F32>, IX_ten_72<F32>, IX_ten_73<F32>, IX_ten_74<F32>, IX_ten_75<F32>,
        IX_ten_76<F32>, IX_ten_77<F32>, IX_ten_78<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[], Node[Leg[1, 1]]], Connection[Strand[Link[LoopMomentum[2], 0], "
        "Bead[Leg[4, 1]], Link[1]], Strand[Link[2], Bead[Leg[3, 2]], Link[0]], Strand[Link[2], "
        "Bead[Leg[2, 2]], Link[LoopMomentum[1], 0]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",   "ten_2",   "ten_3",   "ten_4",   "ten_5",   "ten_6",   "ten_7",   "ten_8",
        "ten_9",   "ten_10",  "ten_11",  "ten_12",  "ten_13",  "ten_14",  "ten_15",  "ten_16",
        "ten_17",  "ten_18",  "ten_19",  "ten_20",  "ten_21",  "ten_22",  "ten_23",  "ten_24",
        "ten_25",  "ten_26",  "ten_27",  "ten_28",  "ten_29",  "ten_30",  "ten_31",  "ten_32",
        "ten_33",  "ten_34",  "ten_35",  "ten_36",  "ten_37",  "ten_38",  "ten_39",  "ten_40",
        "ten_41",  "ten_42",  "ten_43",  "ten_44",  "ten_45",  "ten_46",  "ten_47",  "ten_48",
        "ten_49",  "ten_50",  "ten_51",  "ten_52",  "ten_53",  "ten_54",  "ten_55",  "ten_56",
        "ten_57",  "ten_58",  "ten_59",  "ten_60",  "ten_61",  "ten_62",  "ten_63",  "ten_64",
        "ten_65",  "ten_66",  "ten_67",  "ten_68",  "ten_69",  "ten_70",  "ten_71",  "ten_72",
        "ten_73",  "ten_74",  "ten_75",  "ten_76",  "ten_77",  "ten_78",  "ten_79",  "ten_80",
        "ten_81",  "ten_82",  "ten_83",  "ten_84",  "ten_85",  "ten_86",  "ten_87",  "ten_88",
        "ten_89",  "ten_90",  "ten_91",  "ten_92",  "ten_93",  "ten_94",  "ten_95",  "ten_96",
        "ten_97",  "ten_98",  "ten_99",  "ten_100", "ten_101", "ten_102", "ten_103", "ten_104",
        "ten_105", "ten_106", "ten_107", "ten_108", "ten_109", "ten_110", "ten_111", "ten_112",
        "ten_113", "ten_114", "ten_115", "ten_116", "ten_117", "ten_118", "ten_119", "ten_120",
        "ten_121", "ten_122", "ten_123", "ten_124", "ten_125", "ten_126", "ten_127", "ten_128",
        "ten_129", "ten_130", "ten_131", "ten_132", "ten_133", "ten_134", "ten_135", "ten_136",
        "ten_137", "ten_138", "ten_139", "ten_140", "ten_141", "ten_142", "ten_143", "ten_144",
        "ten_145", "ten_146", "ten_147", "ten_148", "ten_149", "ten_150", "ten_151", "ten_152",
        "ten_153", "ten_154", "ten_155", "ten_156", "ten_157", "ten_158", "ten_159", "ten_160",
        "ten_161", "ten_162", "ten_163", "ten_164", "ten_165", "ten_166", "ten_167", "ten_168",
        "ten_169", "ten_170", "ten_171", "ten_172", "ten_173", "ten_174", "ten_175", "ten_176",
        "ten_177", "ten_178", "ten_179", "ten_180", "ten_181", "ten_182", "ten_183", "ten_184",
        "ten_185", "ten_186", "ten_187", "ten_188", "ten_189", "ten_190", "ten_191", "ten_192",
        "ten_193", "ten_194", "ten_195", "ten_196", "ten_197", "ten_198", "ten_199", "ten_200",
        "ten_201", "ten_202", "ten_203", "ten_204", "ten_205", "ten_206", "ten_207", "ten_208",
        "ten_209", "ten_210", "ten_211", "ten_212", "ten_213", "ten_214", "ten_215", "ten_216",
        "ten_217", "ten_218", "ten_219", "ten_220", "ten_221", "ten_222", "ten_223", "ten_224",
        "ten_225", "ten_226", "ten_227", "ten_228", "ten_229", "ten_230", "ten_231", "ten_232",
        "ten_233", "ten_234", "ten_235", "ten_236", "ten_237", "ten_238", "ten_239", "ten_240",
        "ten_241", "ten_242", "ten_243", "ten_244", "ten_245", "ten_246", "ten_247", "ten_248",
        "ten_249", "ten_250", "ten_251"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = IX_BoxTri_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        IX_BoxTri_ten_1<C>,   IX_BoxTri_ten_2<C>,   IX_BoxTri_ten_3<C>,   IX_BoxTri_ten_4<C>,
        IX_BoxTri_ten_5<C>,   IX_BoxTri_ten_6<C>,   IX_BoxTri_ten_7<C>,   IX_BoxTri_ten_8<C>,
        IX_BoxTri_ten_9<C>,   IX_BoxTri_ten_10<C>,  IX_BoxTri_ten_11<C>,  IX_BoxTri_ten_12<C>,
        IX_BoxTri_ten_13<C>,  IX_BoxTri_ten_14<C>,  IX_BoxTri_ten_15<C>,  IX_BoxTri_ten_16<C>,
        IX_BoxTri_ten_17<C>,  IX_BoxTri_ten_18<C>,  IX_BoxTri_ten_19<C>,  IX_BoxTri_ten_20<C>,
        IX_BoxTri_ten_21<C>,  IX_BoxTri_ten_22<C>,  IX_BoxTri_ten_23<C>,  IX_BoxTri_ten_24<C>,
        IX_BoxTri_ten_25<C>,  IX_BoxTri_ten_26<C>,  IX_BoxTri_ten_27<C>,  IX_BoxTri_ten_28<C>,
        IX_BoxTri_ten_29<C>,  IX_BoxTri_ten_30<C>,  IX_BoxTri_ten_31<C>,  IX_BoxTri_ten_32<C>,
        IX_BoxTri_ten_33<C>,  IX_BoxTri_ten_34<C>,  IX_BoxTri_ten_35<C>,  IX_BoxTri_ten_36<C>,
        IX_BoxTri_ten_37<C>,  IX_BoxTri_ten_38<C>,  IX_BoxTri_ten_39<C>,  IX_BoxTri_ten_40<C>,
        IX_BoxTri_ten_41<C>,  IX_BoxTri_ten_42<C>,  IX_BoxTri_ten_43<C>,  IX_BoxTri_ten_44<C>,
        IX_BoxTri_ten_45<C>,  IX_BoxTri_ten_46<C>,  IX_BoxTri_ten_47<C>,  IX_BoxTri_ten_48<C>,
        IX_BoxTri_ten_49<C>,  IX_BoxTri_ten_50<C>,  IX_BoxTri_ten_51<C>,  IX_BoxTri_ten_52<C>,
        IX_BoxTri_ten_53<C>,  IX_BoxTri_ten_54<C>,  IX_BoxTri_ten_55<C>,  IX_BoxTri_ten_56<C>,
        IX_BoxTri_ten_57<C>,  IX_BoxTri_ten_58<C>,  IX_BoxTri_ten_59<C>,  IX_BoxTri_ten_60<C>,
        IX_BoxTri_ten_61<C>,  IX_BoxTri_ten_62<C>,  IX_BoxTri_ten_63<C>,  IX_BoxTri_ten_64<C>,
        IX_BoxTri_ten_65<C>,  IX_BoxTri_ten_66<C>,  IX_BoxTri_ten_67<C>,  IX_BoxTri_ten_68<C>,
        IX_BoxTri_ten_69<C>,  IX_BoxTri_ten_70<C>,  IX_BoxTri_ten_71<C>,  IX_BoxTri_ten_72<C>,
        IX_BoxTri_ten_73<C>,  IX_BoxTri_ten_74<C>,  IX_BoxTri_ten_75<C>,  IX_BoxTri_ten_76<C>,
        IX_BoxTri_ten_77<C>,  IX_BoxTri_ten_78<C>,  IX_BoxTri_ten_79<C>,  IX_BoxTri_ten_80<C>,
        IX_BoxTri_ten_81<C>,  IX_BoxTri_ten_82<C>,  IX_BoxTri_ten_83<C>,  IX_BoxTri_ten_84<C>,
        IX_BoxTri_ten_85<C>,  IX_BoxTri_ten_86<C>,  IX_BoxTri_ten_87<C>,  IX_BoxTri_ten_88<C>,
        IX_BoxTri_ten_89<C>,  IX_BoxTri_ten_90<C>,  IX_BoxTri_ten_91<C>,  IX_BoxTri_ten_92<C>,
        IX_BoxTri_ten_93<C>,  IX_BoxTri_ten_94<C>,  IX_BoxTri_ten_95<C>,  IX_BoxTri_ten_96<C>,
        IX_BoxTri_ten_97<C>,  IX_BoxTri_ten_98<C>,  IX_BoxTri_ten_99<C>,  IX_BoxTri_ten_100<C>,
        IX_BoxTri_ten_101<C>, IX_BoxTri_ten_102<C>, IX_BoxTri_ten_103<C>, IX_BoxTri_ten_104<C>,
        IX_BoxTri_ten_105<C>, IX_BoxTri_ten_106<C>, IX_BoxTri_ten_107<C>, IX_BoxTri_ten_108<C>,
        IX_BoxTri_ten_109<C>, IX_BoxTri_ten_110<C>, IX_BoxTri_ten_111<C>, IX_BoxTri_ten_112<C>,
        IX_BoxTri_ten_113<C>, IX_BoxTri_ten_114<C>, IX_BoxTri_ten_115<C>, IX_BoxTri_ten_116<C>,
        IX_BoxTri_ten_117<C>, IX_BoxTri_ten_118<C>, IX_BoxTri_ten_119<C>, IX_BoxTri_ten_120<C>,
        IX_BoxTri_ten_121<C>, IX_BoxTri_ten_122<C>, IX_BoxTri_ten_123<C>, IX_BoxTri_ten_124<C>,
        IX_BoxTri_ten_125<C>, IX_BoxTri_ten_126<C>, IX_BoxTri_ten_127<C>, IX_BoxTri_ten_128<C>,
        IX_BoxTri_ten_129<C>, IX_BoxTri_ten_130<C>, IX_BoxTri_ten_131<C>, IX_BoxTri_ten_132<C>,
        IX_BoxTri_ten_133<C>, IX_BoxTri_ten_134<C>, IX_BoxTri_ten_135<C>, IX_BoxTri_ten_136<C>,
        IX_BoxTri_ten_137<C>, IX_BoxTri_ten_138<C>, IX_BoxTri_ten_139<C>, IX_BoxTri_ten_140<C>,
        IX_BoxTri_ten_141<C>, IX_BoxTri_ten_142<C>, IX_BoxTri_ten_143<C>, IX_BoxTri_ten_144<C>,
        IX_BoxTri_ten_145<C>, IX_BoxTri_ten_146<C>, IX_BoxTri_ten_147<C>, IX_BoxTri_ten_148<C>,
        IX_BoxTri_ten_149<C>, IX_BoxTri_ten_150<C>, IX_BoxTri_ten_151<C>, IX_BoxTri_ten_152<C>,
        IX_BoxTri_ten_153<C>, IX_BoxTri_ten_154<C>, IX_BoxTri_ten_155<C>, IX_BoxTri_ten_156<C>,
        IX_BoxTri_ten_157<C>, IX_BoxTri_ten_158<C>, IX_BoxTri_ten_159<C>, IX_BoxTri_ten_160<C>,
        IX_BoxTri_ten_161<C>, IX_BoxTri_ten_162<C>, IX_BoxTri_ten_163<C>, IX_BoxTri_ten_164<C>,
        IX_BoxTri_ten_165<C>, IX_BoxTri_ten_166<C>, IX_BoxTri_ten_167<C>, IX_BoxTri_ten_168<C>,
        IX_BoxTri_ten_169<C>, IX_BoxTri_ten_170<C>, IX_BoxTri_ten_171<C>, IX_BoxTri_ten_172<C>,
        IX_BoxTri_ten_173<C>, IX_BoxTri_ten_174<C>, IX_BoxTri_ten_175<C>, IX_BoxTri_ten_176<C>,
        IX_BoxTri_ten_177<C>, IX_BoxTri_ten_178<C>, IX_BoxTri_ten_179<C>, IX_BoxTri_ten_180<C>,
        IX_BoxTri_ten_181<C>, IX_BoxTri_ten_182<C>, IX_BoxTri_ten_183<C>, IX_BoxTri_ten_184<C>,
        IX_BoxTri_ten_185<C>, IX_BoxTri_ten_186<C>, IX_BoxTri_ten_187<C>, IX_BoxTri_ten_188<C>,
        IX_BoxTri_ten_189<C>, IX_BoxTri_ten_190<C>, IX_BoxTri_ten_191<C>, IX_BoxTri_ten_192<C>,
        IX_BoxTri_ten_193<C>, IX_BoxTri_ten_194<C>, IX_BoxTri_ten_195<C>, IX_BoxTri_ten_196<C>,
        IX_BoxTri_ten_197<C>, IX_BoxTri_ten_198<C>, IX_BoxTri_ten_199<C>, IX_BoxTri_ten_200<C>,
        IX_BoxTri_ten_201<C>, IX_BoxTri_ten_202<C>, IX_BoxTri_ten_203<C>, IX_BoxTri_ten_204<C>,
        IX_BoxTri_ten_205<C>, IX_BoxTri_ten_206<C>, IX_BoxTri_ten_207<C>, IX_BoxTri_ten_208<C>,
        IX_BoxTri_ten_209<C>, IX_BoxTri_ten_210<C>, IX_BoxTri_ten_211<C>, IX_BoxTri_ten_212<C>,
        IX_BoxTri_ten_213<C>, IX_BoxTri_ten_214<C>, IX_BoxTri_ten_215<C>, IX_BoxTri_ten_216<C>,
        IX_BoxTri_ten_217<C>, IX_BoxTri_ten_218<C>, IX_BoxTri_ten_219<C>, IX_BoxTri_ten_220<C>,
        IX_BoxTri_ten_221<C>, IX_BoxTri_ten_222<C>, IX_BoxTri_ten_223<C>, IX_BoxTri_ten_224<C>,
        IX_BoxTri_ten_225<C>, IX_BoxTri_ten_226<C>, IX_BoxTri_ten_227<C>, IX_BoxTri_ten_228<C>,
        IX_BoxTri_ten_229<C>, IX_BoxTri_ten_230<C>, IX_BoxTri_ten_231<C>, IX_BoxTri_ten_232<C>,
        IX_BoxTri_ten_233<C>, IX_BoxTri_ten_234<C>, IX_BoxTri_ten_235<C>, IX_BoxTri_ten_236<C>,
        IX_BoxTri_ten_237<C>, IX_BoxTri_ten_238<C>, IX_BoxTri_ten_239<C>, IX_BoxTri_ten_240<C>,
        IX_BoxTri_ten_241<C>, IX_BoxTri_ten_242<C>, IX_BoxTri_ten_243<C>, IX_BoxTri_ten_244<C>,
        IX_BoxTri_ten_245<C>, IX_BoxTri_ten_246<C>, IX_BoxTri_ten_247<C>, IX_BoxTri_ten_248<C>,
        IX_BoxTri_ten_249<C>, IX_BoxTri_ten_250<C>, IX_BoxTri_ten_251<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = IX_BoxTri_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        IX_BoxTri_ten_1<CHP>,   IX_BoxTri_ten_2<CHP>,   IX_BoxTri_ten_3<CHP>,
        IX_BoxTri_ten_4<CHP>,   IX_BoxTri_ten_5<CHP>,   IX_BoxTri_ten_6<CHP>,
        IX_BoxTri_ten_7<CHP>,   IX_BoxTri_ten_8<CHP>,   IX_BoxTri_ten_9<CHP>,
        IX_BoxTri_ten_10<CHP>,  IX_BoxTri_ten_11<CHP>,  IX_BoxTri_ten_12<CHP>,
        IX_BoxTri_ten_13<CHP>,  IX_BoxTri_ten_14<CHP>,  IX_BoxTri_ten_15<CHP>,
        IX_BoxTri_ten_16<CHP>,  IX_BoxTri_ten_17<CHP>,  IX_BoxTri_ten_18<CHP>,
        IX_BoxTri_ten_19<CHP>,  IX_BoxTri_ten_20<CHP>,  IX_BoxTri_ten_21<CHP>,
        IX_BoxTri_ten_22<CHP>,  IX_BoxTri_ten_23<CHP>,  IX_BoxTri_ten_24<CHP>,
        IX_BoxTri_ten_25<CHP>,  IX_BoxTri_ten_26<CHP>,  IX_BoxTri_ten_27<CHP>,
        IX_BoxTri_ten_28<CHP>,  IX_BoxTri_ten_29<CHP>,  IX_BoxTri_ten_30<CHP>,
        IX_BoxTri_ten_31<CHP>,  IX_BoxTri_ten_32<CHP>,  IX_BoxTri_ten_33<CHP>,
        IX_BoxTri_ten_34<CHP>,  IX_BoxTri_ten_35<CHP>,  IX_BoxTri_ten_36<CHP>,
        IX_BoxTri_ten_37<CHP>,  IX_BoxTri_ten_38<CHP>,  IX_BoxTri_ten_39<CHP>,
        IX_BoxTri_ten_40<CHP>,  IX_BoxTri_ten_41<CHP>,  IX_BoxTri_ten_42<CHP>,
        IX_BoxTri_ten_43<CHP>,  IX_BoxTri_ten_44<CHP>,  IX_BoxTri_ten_45<CHP>,
        IX_BoxTri_ten_46<CHP>,  IX_BoxTri_ten_47<CHP>,  IX_BoxTri_ten_48<CHP>,
        IX_BoxTri_ten_49<CHP>,  IX_BoxTri_ten_50<CHP>,  IX_BoxTri_ten_51<CHP>,
        IX_BoxTri_ten_52<CHP>,  IX_BoxTri_ten_53<CHP>,  IX_BoxTri_ten_54<CHP>,
        IX_BoxTri_ten_55<CHP>,  IX_BoxTri_ten_56<CHP>,  IX_BoxTri_ten_57<CHP>,
        IX_BoxTri_ten_58<CHP>,  IX_BoxTri_ten_59<CHP>,  IX_BoxTri_ten_60<CHP>,
        IX_BoxTri_ten_61<CHP>,  IX_BoxTri_ten_62<CHP>,  IX_BoxTri_ten_63<CHP>,
        IX_BoxTri_ten_64<CHP>,  IX_BoxTri_ten_65<CHP>,  IX_BoxTri_ten_66<CHP>,
        IX_BoxTri_ten_67<CHP>,  IX_BoxTri_ten_68<CHP>,  IX_BoxTri_ten_69<CHP>,
        IX_BoxTri_ten_70<CHP>,  IX_BoxTri_ten_71<CHP>,  IX_BoxTri_ten_72<CHP>,
        IX_BoxTri_ten_73<CHP>,  IX_BoxTri_ten_74<CHP>,  IX_BoxTri_ten_75<CHP>,
        IX_BoxTri_ten_76<CHP>,  IX_BoxTri_ten_77<CHP>,  IX_BoxTri_ten_78<CHP>,
        IX_BoxTri_ten_79<CHP>,  IX_BoxTri_ten_80<CHP>,  IX_BoxTri_ten_81<CHP>,
        IX_BoxTri_ten_82<CHP>,  IX_BoxTri_ten_83<CHP>,  IX_BoxTri_ten_84<CHP>,
        IX_BoxTri_ten_85<CHP>,  IX_BoxTri_ten_86<CHP>,  IX_BoxTri_ten_87<CHP>,
        IX_BoxTri_ten_88<CHP>,  IX_BoxTri_ten_89<CHP>,  IX_BoxTri_ten_90<CHP>,
        IX_BoxTri_ten_91<CHP>,  IX_BoxTri_ten_92<CHP>,  IX_BoxTri_ten_93<CHP>,
        IX_BoxTri_ten_94<CHP>,  IX_BoxTri_ten_95<CHP>,  IX_BoxTri_ten_96<CHP>,
        IX_BoxTri_ten_97<CHP>,  IX_BoxTri_ten_98<CHP>,  IX_BoxTri_ten_99<CHP>,
        IX_BoxTri_ten_100<CHP>, IX_BoxTri_ten_101<CHP>, IX_BoxTri_ten_102<CHP>,
        IX_BoxTri_ten_103<CHP>, IX_BoxTri_ten_104<CHP>, IX_BoxTri_ten_105<CHP>,
        IX_BoxTri_ten_106<CHP>, IX_BoxTri_ten_107<CHP>, IX_BoxTri_ten_108<CHP>,
        IX_BoxTri_ten_109<CHP>, IX_BoxTri_ten_110<CHP>, IX_BoxTri_ten_111<CHP>,
        IX_BoxTri_ten_112<CHP>, IX_BoxTri_ten_113<CHP>, IX_BoxTri_ten_114<CHP>,
        IX_BoxTri_ten_115<CHP>, IX_BoxTri_ten_116<CHP>, IX_BoxTri_ten_117<CHP>,
        IX_BoxTri_ten_118<CHP>, IX_BoxTri_ten_119<CHP>, IX_BoxTri_ten_120<CHP>,
        IX_BoxTri_ten_121<CHP>, IX_BoxTri_ten_122<CHP>, IX_BoxTri_ten_123<CHP>,
        IX_BoxTri_ten_124<CHP>, IX_BoxTri_ten_125<CHP>, IX_BoxTri_ten_126<CHP>,
        IX_BoxTri_ten_127<CHP>, IX_BoxTri_ten_128<CHP>, IX_BoxTri_ten_129<CHP>,
        IX_BoxTri_ten_130<CHP>, IX_BoxTri_ten_131<CHP>, IX_BoxTri_ten_132<CHP>,
        IX_BoxTri_ten_133<CHP>, IX_BoxTri_ten_134<CHP>, IX_BoxTri_ten_135<CHP>,
        IX_BoxTri_ten_136<CHP>, IX_BoxTri_ten_137<CHP>, IX_BoxTri_ten_138<CHP>,
        IX_BoxTri_ten_139<CHP>, IX_BoxTri_ten_140<CHP>, IX_BoxTri_ten_141<CHP>,
        IX_BoxTri_ten_142<CHP>, IX_BoxTri_ten_143<CHP>, IX_BoxTri_ten_144<CHP>,
        IX_BoxTri_ten_145<CHP>, IX_BoxTri_ten_146<CHP>, IX_BoxTri_ten_147<CHP>,
        IX_BoxTri_ten_148<CHP>, IX_BoxTri_ten_149<CHP>, IX_BoxTri_ten_150<CHP>,
        IX_BoxTri_ten_151<CHP>, IX_BoxTri_ten_152<CHP>, IX_BoxTri_ten_153<CHP>,
        IX_BoxTri_ten_154<CHP>, IX_BoxTri_ten_155<CHP>, IX_BoxTri_ten_156<CHP>,
        IX_BoxTri_ten_157<CHP>, IX_BoxTri_ten_158<CHP>, IX_BoxTri_ten_159<CHP>,
        IX_BoxTri_ten_160<CHP>, IX_BoxTri_ten_161<CHP>, IX_BoxTri_ten_162<CHP>,
        IX_BoxTri_ten_163<CHP>, IX_BoxTri_ten_164<CHP>, IX_BoxTri_ten_165<CHP>,
        IX_BoxTri_ten_166<CHP>, IX_BoxTri_ten_167<CHP>, IX_BoxTri_ten_168<CHP>,
        IX_BoxTri_ten_169<CHP>, IX_BoxTri_ten_170<CHP>, IX_BoxTri_ten_171<CHP>,
        IX_BoxTri_ten_172<CHP>, IX_BoxTri_ten_173<CHP>, IX_BoxTri_ten_174<CHP>,
        IX_BoxTri_ten_175<CHP>, IX_BoxTri_ten_176<CHP>, IX_BoxTri_ten_177<CHP>,
        IX_BoxTri_ten_178<CHP>, IX_BoxTri_ten_179<CHP>, IX_BoxTri_ten_180<CHP>,
        IX_BoxTri_ten_181<CHP>, IX_BoxTri_ten_182<CHP>, IX_BoxTri_ten_183<CHP>,
        IX_BoxTri_ten_184<CHP>, IX_BoxTri_ten_185<CHP>, IX_BoxTri_ten_186<CHP>,
        IX_BoxTri_ten_187<CHP>, IX_BoxTri_ten_188<CHP>, IX_BoxTri_ten_189<CHP>,
        IX_BoxTri_ten_190<CHP>, IX_BoxTri_ten_191<CHP>, IX_BoxTri_ten_192<CHP>,
        IX_BoxTri_ten_193<CHP>, IX_BoxTri_ten_194<CHP>, IX_BoxTri_ten_195<CHP>,
        IX_BoxTri_ten_196<CHP>, IX_BoxTri_ten_197<CHP>, IX_BoxTri_ten_198<CHP>,
        IX_BoxTri_ten_199<CHP>, IX_BoxTri_ten_200<CHP>, IX_BoxTri_ten_201<CHP>,
        IX_BoxTri_ten_202<CHP>, IX_BoxTri_ten_203<CHP>, IX_BoxTri_ten_204<CHP>,
        IX_BoxTri_ten_205<CHP>, IX_BoxTri_ten_206<CHP>, IX_BoxTri_ten_207<CHP>,
        IX_BoxTri_ten_208<CHP>, IX_BoxTri_ten_209<CHP>, IX_BoxTri_ten_210<CHP>,
        IX_BoxTri_ten_211<CHP>, IX_BoxTri_ten_212<CHP>, IX_BoxTri_ten_213<CHP>,
        IX_BoxTri_ten_214<CHP>, IX_BoxTri_ten_215<CHP>, IX_BoxTri_ten_216<CHP>,
        IX_BoxTri_ten_217<CHP>, IX_BoxTri_ten_218<CHP>, IX_BoxTri_ten_219<CHP>,
        IX_BoxTri_ten_220<CHP>, IX_BoxTri_ten_221<CHP>, IX_BoxTri_ten_222<CHP>,
        IX_BoxTri_ten_223<CHP>, IX_BoxTri_ten_224<CHP>, IX_BoxTri_ten_225<CHP>,
        IX_BoxTri_ten_226<CHP>, IX_BoxTri_ten_227<CHP>, IX_BoxTri_ten_228<CHP>,
        IX_BoxTri_ten_229<CHP>, IX_BoxTri_ten_230<CHP>, IX_BoxTri_ten_231<CHP>,
        IX_BoxTri_ten_232<CHP>, IX_BoxTri_ten_233<CHP>, IX_BoxTri_ten_234<CHP>,
        IX_BoxTri_ten_235<CHP>, IX_BoxTri_ten_236<CHP>, IX_BoxTri_ten_237<CHP>,
        IX_BoxTri_ten_238<CHP>, IX_BoxTri_ten_239<CHP>, IX_BoxTri_ten_240<CHP>,
        IX_BoxTri_ten_241<CHP>, IX_BoxTri_ten_242<CHP>, IX_BoxTri_ten_243<CHP>,
        IX_BoxTri_ten_244<CHP>, IX_BoxTri_ten_245<CHP>, IX_BoxTri_ten_246<CHP>,
        IX_BoxTri_ten_247<CHP>, IX_BoxTri_ten_248<CHP>, IX_BoxTri_ten_249<CHP>,
        IX_BoxTri_ten_250<CHP>, IX_BoxTri_ten_251<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = IX_BoxTri_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        IX_BoxTri_ten_1<CVHP>,   IX_BoxTri_ten_2<CVHP>,   IX_BoxTri_ten_3<CVHP>,
        IX_BoxTri_ten_4<CVHP>,   IX_BoxTri_ten_5<CVHP>,   IX_BoxTri_ten_6<CVHP>,
        IX_BoxTri_ten_7<CVHP>,   IX_BoxTri_ten_8<CVHP>,   IX_BoxTri_ten_9<CVHP>,
        IX_BoxTri_ten_10<CVHP>,  IX_BoxTri_ten_11<CVHP>,  IX_BoxTri_ten_12<CVHP>,
        IX_BoxTri_ten_13<CVHP>,  IX_BoxTri_ten_14<CVHP>,  IX_BoxTri_ten_15<CVHP>,
        IX_BoxTri_ten_16<CVHP>,  IX_BoxTri_ten_17<CVHP>,  IX_BoxTri_ten_18<CVHP>,
        IX_BoxTri_ten_19<CVHP>,  IX_BoxTri_ten_20<CVHP>,  IX_BoxTri_ten_21<CVHP>,
        IX_BoxTri_ten_22<CVHP>,  IX_BoxTri_ten_23<CVHP>,  IX_BoxTri_ten_24<CVHP>,
        IX_BoxTri_ten_25<CVHP>,  IX_BoxTri_ten_26<CVHP>,  IX_BoxTri_ten_27<CVHP>,
        IX_BoxTri_ten_28<CVHP>,  IX_BoxTri_ten_29<CVHP>,  IX_BoxTri_ten_30<CVHP>,
        IX_BoxTri_ten_31<CVHP>,  IX_BoxTri_ten_32<CVHP>,  IX_BoxTri_ten_33<CVHP>,
        IX_BoxTri_ten_34<CVHP>,  IX_BoxTri_ten_35<CVHP>,  IX_BoxTri_ten_36<CVHP>,
        IX_BoxTri_ten_37<CVHP>,  IX_BoxTri_ten_38<CVHP>,  IX_BoxTri_ten_39<CVHP>,
        IX_BoxTri_ten_40<CVHP>,  IX_BoxTri_ten_41<CVHP>,  IX_BoxTri_ten_42<CVHP>,
        IX_BoxTri_ten_43<CVHP>,  IX_BoxTri_ten_44<CVHP>,  IX_BoxTri_ten_45<CVHP>,
        IX_BoxTri_ten_46<CVHP>,  IX_BoxTri_ten_47<CVHP>,  IX_BoxTri_ten_48<CVHP>,
        IX_BoxTri_ten_49<CVHP>,  IX_BoxTri_ten_50<CVHP>,  IX_BoxTri_ten_51<CVHP>,
        IX_BoxTri_ten_52<CVHP>,  IX_BoxTri_ten_53<CVHP>,  IX_BoxTri_ten_54<CVHP>,
        IX_BoxTri_ten_55<CVHP>,  IX_BoxTri_ten_56<CVHP>,  IX_BoxTri_ten_57<CVHP>,
        IX_BoxTri_ten_58<CVHP>,  IX_BoxTri_ten_59<CVHP>,  IX_BoxTri_ten_60<CVHP>,
        IX_BoxTri_ten_61<CVHP>,  IX_BoxTri_ten_62<CVHP>,  IX_BoxTri_ten_63<CVHP>,
        IX_BoxTri_ten_64<CVHP>,  IX_BoxTri_ten_65<CVHP>,  IX_BoxTri_ten_66<CVHP>,
        IX_BoxTri_ten_67<CVHP>,  IX_BoxTri_ten_68<CVHP>,  IX_BoxTri_ten_69<CVHP>,
        IX_BoxTri_ten_70<CVHP>,  IX_BoxTri_ten_71<CVHP>,  IX_BoxTri_ten_72<CVHP>,
        IX_BoxTri_ten_73<CVHP>,  IX_BoxTri_ten_74<CVHP>,  IX_BoxTri_ten_75<CVHP>,
        IX_BoxTri_ten_76<CVHP>,  IX_BoxTri_ten_77<CVHP>,  IX_BoxTri_ten_78<CVHP>,
        IX_BoxTri_ten_79<CVHP>,  IX_BoxTri_ten_80<CVHP>,  IX_BoxTri_ten_81<CVHP>,
        IX_BoxTri_ten_82<CVHP>,  IX_BoxTri_ten_83<CVHP>,  IX_BoxTri_ten_84<CVHP>,
        IX_BoxTri_ten_85<CVHP>,  IX_BoxTri_ten_86<CVHP>,  IX_BoxTri_ten_87<CVHP>,
        IX_BoxTri_ten_88<CVHP>,  IX_BoxTri_ten_89<CVHP>,  IX_BoxTri_ten_90<CVHP>,
        IX_BoxTri_ten_91<CVHP>,  IX_BoxTri_ten_92<CVHP>,  IX_BoxTri_ten_93<CVHP>,
        IX_BoxTri_ten_94<CVHP>,  IX_BoxTri_ten_95<CVHP>,  IX_BoxTri_ten_96<CVHP>,
        IX_BoxTri_ten_97<CVHP>,  IX_BoxTri_ten_98<CVHP>,  IX_BoxTri_ten_99<CVHP>,
        IX_BoxTri_ten_100<CVHP>, IX_BoxTri_ten_101<CVHP>, IX_BoxTri_ten_102<CVHP>,
        IX_BoxTri_ten_103<CVHP>, IX_BoxTri_ten_104<CVHP>, IX_BoxTri_ten_105<CVHP>,
        IX_BoxTri_ten_106<CVHP>, IX_BoxTri_ten_107<CVHP>, IX_BoxTri_ten_108<CVHP>,
        IX_BoxTri_ten_109<CVHP>, IX_BoxTri_ten_110<CVHP>, IX_BoxTri_ten_111<CVHP>,
        IX_BoxTri_ten_112<CVHP>, IX_BoxTri_ten_113<CVHP>, IX_BoxTri_ten_114<CVHP>,
        IX_BoxTri_ten_115<CVHP>, IX_BoxTri_ten_116<CVHP>, IX_BoxTri_ten_117<CVHP>,
        IX_BoxTri_ten_118<CVHP>, IX_BoxTri_ten_119<CVHP>, IX_BoxTri_ten_120<CVHP>,
        IX_BoxTri_ten_121<CVHP>, IX_BoxTri_ten_122<CVHP>, IX_BoxTri_ten_123<CVHP>,
        IX_BoxTri_ten_124<CVHP>, IX_BoxTri_ten_125<CVHP>, IX_BoxTri_ten_126<CVHP>,
        IX_BoxTri_ten_127<CVHP>, IX_BoxTri_ten_128<CVHP>, IX_BoxTri_ten_129<CVHP>,
        IX_BoxTri_ten_130<CVHP>, IX_BoxTri_ten_131<CVHP>, IX_BoxTri_ten_132<CVHP>,
        IX_BoxTri_ten_133<CVHP>, IX_BoxTri_ten_134<CVHP>, IX_BoxTri_ten_135<CVHP>,
        IX_BoxTri_ten_136<CVHP>, IX_BoxTri_ten_137<CVHP>, IX_BoxTri_ten_138<CVHP>,
        IX_BoxTri_ten_139<CVHP>, IX_BoxTri_ten_140<CVHP>, IX_BoxTri_ten_141<CVHP>,
        IX_BoxTri_ten_142<CVHP>, IX_BoxTri_ten_143<CVHP>, IX_BoxTri_ten_144<CVHP>,
        IX_BoxTri_ten_145<CVHP>, IX_BoxTri_ten_146<CVHP>, IX_BoxTri_ten_147<CVHP>,
        IX_BoxTri_ten_148<CVHP>, IX_BoxTri_ten_149<CVHP>, IX_BoxTri_ten_150<CVHP>,
        IX_BoxTri_ten_151<CVHP>, IX_BoxTri_ten_152<CVHP>, IX_BoxTri_ten_153<CVHP>,
        IX_BoxTri_ten_154<CVHP>, IX_BoxTri_ten_155<CVHP>, IX_BoxTri_ten_156<CVHP>,
        IX_BoxTri_ten_157<CVHP>, IX_BoxTri_ten_158<CVHP>, IX_BoxTri_ten_159<CVHP>,
        IX_BoxTri_ten_160<CVHP>, IX_BoxTri_ten_161<CVHP>, IX_BoxTri_ten_162<CVHP>,
        IX_BoxTri_ten_163<CVHP>, IX_BoxTri_ten_164<CVHP>, IX_BoxTri_ten_165<CVHP>,
        IX_BoxTri_ten_166<CVHP>, IX_BoxTri_ten_167<CVHP>, IX_BoxTri_ten_168<CVHP>,
        IX_BoxTri_ten_169<CVHP>, IX_BoxTri_ten_170<CVHP>, IX_BoxTri_ten_171<CVHP>,
        IX_BoxTri_ten_172<CVHP>, IX_BoxTri_ten_173<CVHP>, IX_BoxTri_ten_174<CVHP>,
        IX_BoxTri_ten_175<CVHP>, IX_BoxTri_ten_176<CVHP>, IX_BoxTri_ten_177<CVHP>,
        IX_BoxTri_ten_178<CVHP>, IX_BoxTri_ten_179<CVHP>, IX_BoxTri_ten_180<CVHP>,
        IX_BoxTri_ten_181<CVHP>, IX_BoxTri_ten_182<CVHP>, IX_BoxTri_ten_183<CVHP>,
        IX_BoxTri_ten_184<CVHP>, IX_BoxTri_ten_185<CVHP>, IX_BoxTri_ten_186<CVHP>,
        IX_BoxTri_ten_187<CVHP>, IX_BoxTri_ten_188<CVHP>, IX_BoxTri_ten_189<CVHP>,
        IX_BoxTri_ten_190<CVHP>, IX_BoxTri_ten_191<CVHP>, IX_BoxTri_ten_192<CVHP>,
        IX_BoxTri_ten_193<CVHP>, IX_BoxTri_ten_194<CVHP>, IX_BoxTri_ten_195<CVHP>,
        IX_BoxTri_ten_196<CVHP>, IX_BoxTri_ten_197<CVHP>, IX_BoxTri_ten_198<CVHP>,
        IX_BoxTri_ten_199<CVHP>, IX_BoxTri_ten_200<CVHP>, IX_BoxTri_ten_201<CVHP>,
        IX_BoxTri_ten_202<CVHP>, IX_BoxTri_ten_203<CVHP>, IX_BoxTri_ten_204<CVHP>,
        IX_BoxTri_ten_205<CVHP>, IX_BoxTri_ten_206<CVHP>, IX_BoxTri_ten_207<CVHP>,
        IX_BoxTri_ten_208<CVHP>, IX_BoxTri_ten_209<CVHP>, IX_BoxTri_ten_210<CVHP>,
        IX_BoxTri_ten_211<CVHP>, IX_BoxTri_ten_212<CVHP>, IX_BoxTri_ten_213<CVHP>,
        IX_BoxTri_ten_214<CVHP>, IX_BoxTri_ten_215<CVHP>, IX_BoxTri_ten_216<CVHP>,
        IX_BoxTri_ten_217<CVHP>, IX_BoxTri_ten_218<CVHP>, IX_BoxTri_ten_219<CVHP>,
        IX_BoxTri_ten_220<CVHP>, IX_BoxTri_ten_221<CVHP>, IX_BoxTri_ten_222<CVHP>,
        IX_BoxTri_ten_223<CVHP>, IX_BoxTri_ten_224<CVHP>, IX_BoxTri_ten_225<CVHP>,
        IX_BoxTri_ten_226<CVHP>, IX_BoxTri_ten_227<CVHP>, IX_BoxTri_ten_228<CVHP>,
        IX_BoxTri_ten_229<CVHP>, IX_BoxTri_ten_230<CVHP>, IX_BoxTri_ten_231<CVHP>,
        IX_BoxTri_ten_232<CVHP>, IX_BoxTri_ten_233<CVHP>, IX_BoxTri_ten_234<CVHP>,
        IX_BoxTri_ten_235<CVHP>, IX_BoxTri_ten_236<CVHP>, IX_BoxTri_ten_237<CVHP>,
        IX_BoxTri_ten_238<CVHP>, IX_BoxTri_ten_239<CVHP>, IX_BoxTri_ten_240<CVHP>,
        IX_BoxTri_ten_241<CVHP>, IX_BoxTri_ten_242<CVHP>, IX_BoxTri_ten_243<CVHP>,
        IX_BoxTri_ten_244<CVHP>, IX_BoxTri_ten_245<CVHP>, IX_BoxTri_ten_246<CVHP>,
        IX_BoxTri_ten_247<CVHP>, IX_BoxTri_ten_248<CVHP>, IX_BoxTri_ten_249<CVHP>,
        IX_BoxTri_ten_250<CVHP>, IX_BoxTri_ten_251<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = IX_BoxTri_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        IX_BoxTri_ten_1<F32>,   IX_BoxTri_ten_2<F32>,   IX_BoxTri_ten_3<F32>,
        IX_BoxTri_ten_4<F32>,   IX_BoxTri_ten_5<F32>,   IX_BoxTri_ten_6<F32>,
        IX_BoxTri_ten_7<F32>,   IX_BoxTri_ten_8<F32>,   IX_BoxTri_ten_9<F32>,
        IX_BoxTri_ten_10<F32>,  IX_BoxTri_ten_11<F32>,  IX_BoxTri_ten_12<F32>,
        IX_BoxTri_ten_13<F32>,  IX_BoxTri_ten_14<F32>,  IX_BoxTri_ten_15<F32>,
        IX_BoxTri_ten_16<F32>,  IX_BoxTri_ten_17<F32>,  IX_BoxTri_ten_18<F32>,
        IX_BoxTri_ten_19<F32>,  IX_BoxTri_ten_20<F32>,  IX_BoxTri_ten_21<F32>,
        IX_BoxTri_ten_22<F32>,  IX_BoxTri_ten_23<F32>,  IX_BoxTri_ten_24<F32>,
        IX_BoxTri_ten_25<F32>,  IX_BoxTri_ten_26<F32>,  IX_BoxTri_ten_27<F32>,
        IX_BoxTri_ten_28<F32>,  IX_BoxTri_ten_29<F32>,  IX_BoxTri_ten_30<F32>,
        IX_BoxTri_ten_31<F32>,  IX_BoxTri_ten_32<F32>,  IX_BoxTri_ten_33<F32>,
        IX_BoxTri_ten_34<F32>,  IX_BoxTri_ten_35<F32>,  IX_BoxTri_ten_36<F32>,
        IX_BoxTri_ten_37<F32>,  IX_BoxTri_ten_38<F32>,  IX_BoxTri_ten_39<F32>,
        IX_BoxTri_ten_40<F32>,  IX_BoxTri_ten_41<F32>,  IX_BoxTri_ten_42<F32>,
        IX_BoxTri_ten_43<F32>,  IX_BoxTri_ten_44<F32>,  IX_BoxTri_ten_45<F32>,
        IX_BoxTri_ten_46<F32>,  IX_BoxTri_ten_47<F32>,  IX_BoxTri_ten_48<F32>,
        IX_BoxTri_ten_49<F32>,  IX_BoxTri_ten_50<F32>,  IX_BoxTri_ten_51<F32>,
        IX_BoxTri_ten_52<F32>,  IX_BoxTri_ten_53<F32>,  IX_BoxTri_ten_54<F32>,
        IX_BoxTri_ten_55<F32>,  IX_BoxTri_ten_56<F32>,  IX_BoxTri_ten_57<F32>,
        IX_BoxTri_ten_58<F32>,  IX_BoxTri_ten_59<F32>,  IX_BoxTri_ten_60<F32>,
        IX_BoxTri_ten_61<F32>,  IX_BoxTri_ten_62<F32>,  IX_BoxTri_ten_63<F32>,
        IX_BoxTri_ten_64<F32>,  IX_BoxTri_ten_65<F32>,  IX_BoxTri_ten_66<F32>,
        IX_BoxTri_ten_67<F32>,  IX_BoxTri_ten_68<F32>,  IX_BoxTri_ten_69<F32>,
        IX_BoxTri_ten_70<F32>,  IX_BoxTri_ten_71<F32>,  IX_BoxTri_ten_72<F32>,
        IX_BoxTri_ten_73<F32>,  IX_BoxTri_ten_74<F32>,  IX_BoxTri_ten_75<F32>,
        IX_BoxTri_ten_76<F32>,  IX_BoxTri_ten_77<F32>,  IX_BoxTri_ten_78<F32>,
        IX_BoxTri_ten_79<F32>,  IX_BoxTri_ten_80<F32>,  IX_BoxTri_ten_81<F32>,
        IX_BoxTri_ten_82<F32>,  IX_BoxTri_ten_83<F32>,  IX_BoxTri_ten_84<F32>,
        IX_BoxTri_ten_85<F32>,  IX_BoxTri_ten_86<F32>,  IX_BoxTri_ten_87<F32>,
        IX_BoxTri_ten_88<F32>,  IX_BoxTri_ten_89<F32>,  IX_BoxTri_ten_90<F32>,
        IX_BoxTri_ten_91<F32>,  IX_BoxTri_ten_92<F32>,  IX_BoxTri_ten_93<F32>,
        IX_BoxTri_ten_94<F32>,  IX_BoxTri_ten_95<F32>,  IX_BoxTri_ten_96<F32>,
        IX_BoxTri_ten_97<F32>,  IX_BoxTri_ten_98<F32>,  IX_BoxTri_ten_99<F32>,
        IX_BoxTri_ten_100<F32>, IX_BoxTri_ten_101<F32>, IX_BoxTri_ten_102<F32>,
        IX_BoxTri_ten_103<F32>, IX_BoxTri_ten_104<F32>, IX_BoxTri_ten_105<F32>,
        IX_BoxTri_ten_106<F32>, IX_BoxTri_ten_107<F32>, IX_BoxTri_ten_108<F32>,
        IX_BoxTri_ten_109<F32>, IX_BoxTri_ten_110<F32>, IX_BoxTri_ten_111<F32>,
        IX_BoxTri_ten_112<F32>, IX_BoxTri_ten_113<F32>, IX_BoxTri_ten_114<F32>,
        IX_BoxTri_ten_115<F32>, IX_BoxTri_ten_116<F32>, IX_BoxTri_ten_117<F32>,
        IX_BoxTri_ten_118<F32>, IX_BoxTri_ten_119<F32>, IX_BoxTri_ten_120<F32>,
        IX_BoxTri_ten_121<F32>, IX_BoxTri_ten_122<F32>, IX_BoxTri_ten_123<F32>,
        IX_BoxTri_ten_124<F32>, IX_BoxTri_ten_125<F32>, IX_BoxTri_ten_126<F32>,
        IX_BoxTri_ten_127<F32>, IX_BoxTri_ten_128<F32>, IX_BoxTri_ten_129<F32>,
        IX_BoxTri_ten_130<F32>, IX_BoxTri_ten_131<F32>, IX_BoxTri_ten_132<F32>,
        IX_BoxTri_ten_133<F32>, IX_BoxTri_ten_134<F32>, IX_BoxTri_ten_135<F32>,
        IX_BoxTri_ten_136<F32>, IX_BoxTri_ten_137<F32>, IX_BoxTri_ten_138<F32>,
        IX_BoxTri_ten_139<F32>, IX_BoxTri_ten_140<F32>, IX_BoxTri_ten_141<F32>,
        IX_BoxTri_ten_142<F32>, IX_BoxTri_ten_143<F32>, IX_BoxTri_ten_144<F32>,
        IX_BoxTri_ten_145<F32>, IX_BoxTri_ten_146<F32>, IX_BoxTri_ten_147<F32>,
        IX_BoxTri_ten_148<F32>, IX_BoxTri_ten_149<F32>, IX_BoxTri_ten_150<F32>,
        IX_BoxTri_ten_151<F32>, IX_BoxTri_ten_152<F32>, IX_BoxTri_ten_153<F32>,
        IX_BoxTri_ten_154<F32>, IX_BoxTri_ten_155<F32>, IX_BoxTri_ten_156<F32>,
        IX_BoxTri_ten_157<F32>, IX_BoxTri_ten_158<F32>, IX_BoxTri_ten_159<F32>,
        IX_BoxTri_ten_160<F32>, IX_BoxTri_ten_161<F32>, IX_BoxTri_ten_162<F32>,
        IX_BoxTri_ten_163<F32>, IX_BoxTri_ten_164<F32>, IX_BoxTri_ten_165<F32>,
        IX_BoxTri_ten_166<F32>, IX_BoxTri_ten_167<F32>, IX_BoxTri_ten_168<F32>,
        IX_BoxTri_ten_169<F32>, IX_BoxTri_ten_170<F32>, IX_BoxTri_ten_171<F32>,
        IX_BoxTri_ten_172<F32>, IX_BoxTri_ten_173<F32>, IX_BoxTri_ten_174<F32>,
        IX_BoxTri_ten_175<F32>, IX_BoxTri_ten_176<F32>, IX_BoxTri_ten_177<F32>,
        IX_BoxTri_ten_178<F32>, IX_BoxTri_ten_179<F32>, IX_BoxTri_ten_180<F32>,
        IX_BoxTri_ten_181<F32>, IX_BoxTri_ten_182<F32>, IX_BoxTri_ten_183<F32>,
        IX_BoxTri_ten_184<F32>, IX_BoxTri_ten_185<F32>, IX_BoxTri_ten_186<F32>,
        IX_BoxTri_ten_187<F32>, IX_BoxTri_ten_188<F32>, IX_BoxTri_ten_189<F32>,
        IX_BoxTri_ten_190<F32>, IX_BoxTri_ten_191<F32>, IX_BoxTri_ten_192<F32>,
        IX_BoxTri_ten_193<F32>, IX_BoxTri_ten_194<F32>, IX_BoxTri_ten_195<F32>,
        IX_BoxTri_ten_196<F32>, IX_BoxTri_ten_197<F32>, IX_BoxTri_ten_198<F32>,
        IX_BoxTri_ten_199<F32>, IX_BoxTri_ten_200<F32>, IX_BoxTri_ten_201<F32>,
        IX_BoxTri_ten_202<F32>, IX_BoxTri_ten_203<F32>, IX_BoxTri_ten_204<F32>,
        IX_BoxTri_ten_205<F32>, IX_BoxTri_ten_206<F32>, IX_BoxTri_ten_207<F32>,
        IX_BoxTri_ten_208<F32>, IX_BoxTri_ten_209<F32>, IX_BoxTri_ten_210<F32>,
        IX_BoxTri_ten_211<F32>, IX_BoxTri_ten_212<F32>, IX_BoxTri_ten_213<F32>,
        IX_BoxTri_ten_214<F32>, IX_BoxTri_ten_215<F32>, IX_BoxTri_ten_216<F32>,
        IX_BoxTri_ten_217<F32>, IX_BoxTri_ten_218<F32>, IX_BoxTri_ten_219<F32>,
        IX_BoxTri_ten_220<F32>, IX_BoxTri_ten_221<F32>, IX_BoxTri_ten_222<F32>,
        IX_BoxTri_ten_223<F32>, IX_BoxTri_ten_224<F32>, IX_BoxTri_ten_225<F32>,
        IX_BoxTri_ten_226<F32>, IX_BoxTri_ten_227<F32>, IX_BoxTri_ten_228<F32>,
        IX_BoxTri_ten_229<F32>, IX_BoxTri_ten_230<F32>, IX_BoxTri_ten_231<F32>,
        IX_BoxTri_ten_232<F32>, IX_BoxTri_ten_233<F32>, IX_BoxTri_ten_234<F32>,
        IX_BoxTri_ten_235<F32>, IX_BoxTri_ten_236<F32>, IX_BoxTri_ten_237<F32>,
        IX_BoxTri_ten_238<F32>, IX_BoxTri_ten_239<F32>, IX_BoxTri_ten_240<F32>,
        IX_BoxTri_ten_241<F32>, IX_BoxTri_ten_242<F32>, IX_BoxTri_ten_243<F32>,
        IX_BoxTri_ten_244<F32>, IX_BoxTri_ten_245<F32>, IX_BoxTri_ten_246<F32>,
        IX_BoxTri_ten_247<F32>, IX_BoxTri_ten_248<F32>, IX_BoxTri_ten_249<F32>,
        IX_BoxTri_ten_250<F32>, IX_BoxTri_ten_251<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0]], Strand[Link[0], "
                "Bead[Leg[3, 2]], Link[2], Bead[Leg[2, 2]], Link[LoopMomentum[1], 0], Bead[Leg[1, "
                "1]], Link[1]], Strand[Link[LoopMomentum[2], 0], Bead[Leg[4, 1]], Link[1]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",  "ten_2",  "ten_3",  "ten_4",  "ten_5",  "ten_6",  "ten_7",
        "ten_8",  "ten_9",  "ten_10", "ten_11", "ten_12", "ten_13", "ten_14",
        "ten_15", "ten_16", "ten_17", "ten_18", "ten_19", "ten_20", "ten_21",
        "ten_22", "ten_23", "ten_24", "ten_25", "ten_26", "ten_27", "ten_28"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = IY_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        IY_ten_1<C>,  IY_ten_2<C>,  IY_ten_3<C>,  IY_ten_4<C>,  IY_ten_5<C>,  IY_ten_6<C>,
        IY_ten_7<C>,  IY_ten_8<C>,  IY_ten_9<C>,  IY_ten_10<C>, IY_ten_11<C>, IY_ten_12<C>,
        IY_ten_13<C>, IY_ten_14<C>, IY_ten_15<C>, IY_ten_16<C>, IY_ten_17<C>, IY_ten_18<C>,
        IY_ten_19<C>, IY_ten_20<C>, IY_ten_21<C>, IY_ten_22<C>, IY_ten_23<C>, IY_ten_24<C>,
        IY_ten_25<C>, IY_ten_26<C>, IY_ten_27<C>, IY_ten_28<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = IY_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        IY_ten_1<CHP>,  IY_ten_2<CHP>,  IY_ten_3<CHP>,  IY_ten_4<CHP>,  IY_ten_5<CHP>,
        IY_ten_6<CHP>,  IY_ten_7<CHP>,  IY_ten_8<CHP>,  IY_ten_9<CHP>,  IY_ten_10<CHP>,
        IY_ten_11<CHP>, IY_ten_12<CHP>, IY_ten_13<CHP>, IY_ten_14<CHP>, IY_ten_15<CHP>,
        IY_ten_16<CHP>, IY_ten_17<CHP>, IY_ten_18<CHP>, IY_ten_19<CHP>, IY_ten_20<CHP>,
        IY_ten_21<CHP>, IY_ten_22<CHP>, IY_ten_23<CHP>, IY_ten_24<CHP>, IY_ten_25<CHP>,
        IY_ten_26<CHP>, IY_ten_27<CHP>, IY_ten_28<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = IY_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        IY_ten_1<CVHP>,  IY_ten_2<CVHP>,  IY_ten_3<CVHP>,  IY_ten_4<CVHP>,  IY_ten_5<CVHP>,
        IY_ten_6<CVHP>,  IY_ten_7<CVHP>,  IY_ten_8<CVHP>,  IY_ten_9<CVHP>,  IY_ten_10<CVHP>,
        IY_ten_11<CVHP>, IY_ten_12<CVHP>, IY_ten_13<CVHP>, IY_ten_14<CVHP>, IY_ten_15<CVHP>,
        IY_ten_16<CVHP>, IY_ten_17<CVHP>, IY_ten_18<CVHP>, IY_ten_19<CVHP>, IY_ten_20<CVHP>,
        IY_ten_21<CVHP>, IY_ten_22<CVHP>, IY_ten_23<CVHP>, IY_ten_24<CVHP>, IY_ten_25<CVHP>,
        IY_ten_26<CVHP>, IY_ten_27<CVHP>, IY_ten_28<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = IY_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        IY_ten_1<F32>,  IY_ten_2<F32>,  IY_ten_3<F32>,  IY_ten_4<F32>,  IY_ten_5<F32>,
        IY_ten_6<F32>,  IY_ten_7<F32>,  IY_ten_8<F32>,  IY_ten_9<F32>,  IY_ten_10<F32>,
        IY_ten_11<F32>, IY_ten_12<F32>, IY_ten_13<F32>, IY_ten_14<F32>, IY_ten_15<F32>,
        IY_ten_16<F32>, IY_ten_17<F32>, IY_ten_18<F32>, IY_ten_19<F32>, IY_ten_20<F32>,
        IY_ten_21<F32>, IY_ten_22<F32>, IY_ten_23<F32>, IY_ten_24<F32>, IY_ten_25<F32>,
        IY_ten_26<F32>, IY_ten_27<F32>, IY_ten_28<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[4, 1]], Node[Leg[2, 2]]], Connection[Strand[Link[0]], "
                "Strand[Link[1], Bead[Leg[1, 1]], Link[LoopMomentum[-1], 0]], "
                "Strand[Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",   "ten_2",   "ten_3",   "ten_4",   "ten_5",   "ten_6",   "ten_7",   "ten_8",
        "ten_9",   "ten_10",  "ten_11",  "ten_12",  "ten_13",  "ten_14",  "ten_15",  "ten_16",
        "ten_17",  "ten_18",  "ten_19",  "ten_20",  "ten_21",  "ten_22",  "ten_23",  "ten_24",
        "ten_25",  "ten_26",  "ten_27",  "ten_28",  "ten_29",  "ten_30",  "ten_31",  "ten_32",
        "ten_33",  "ten_34",  "ten_35",  "ten_36",  "ten_37",  "ten_38",  "ten_39",  "ten_40",
        "ten_41",  "ten_42",  "ten_43",  "ten_44",  "ten_45",  "ten_46",  "ten_47",  "ten_48",
        "ten_49",  "ten_50",  "ten_51",  "ten_52",  "ten_53",  "ten_54",  "ten_55",  "ten_56",
        "ten_57",  "ten_58",  "ten_59",  "ten_60",  "ten_61",  "ten_62",  "ten_63",  "ten_64",
        "ten_65",  "ten_66",  "ten_67",  "ten_68",  "ten_69",  "ten_70",  "ten_71",  "ten_72",
        "ten_73",  "ten_74",  "ten_75",  "ten_76",  "ten_77",  "ten_78",  "ten_79",  "ten_80",
        "ten_81",  "ten_82",  "ten_83",  "ten_84",  "ten_85",  "ten_86",  "ten_87",  "ten_88",
        "ten_89",  "ten_90",  "ten_91",  "ten_92",  "ten_93",  "ten_94",  "ten_95",  "ten_96",
        "ten_97",  "ten_98",  "ten_99",  "ten_100", "ten_101", "ten_102", "ten_103", "ten_104",
        "ten_105", "ten_106", "ten_107", "ten_108", "ten_109", "ten_110", "ten_111", "ten_112",
        "ten_113", "ten_114", "ten_115", "ten_116", "ten_117", "ten_118", "ten_119", "ten_120",
        "ten_121", "ten_122", "ten_123", "ten_124", "ten_125", "ten_126", "ten_127", "ten_128",
        "ten_129", "ten_130", "ten_131", "ten_132", "ten_133", "ten_134", "ten_135", "ten_136",
        "ten_137", "ten_138", "ten_139", "ten_140", "ten_141", "ten_142", "ten_143", "ten_144",
        "ten_145", "ten_146", "ten_147", "ten_148", "ten_149", "ten_150", "ten_151", "ten_152",
        "ten_153", "ten_154", "ten_155", "ten_156", "ten_157", "ten_158", "ten_159", "ten_160",
        "ten_161", "ten_162", "ten_163", "ten_164", "ten_165", "ten_166", "ten_167", "ten_168",
        "ten_169", "ten_170", "ten_171", "ten_172", "ten_173", "ten_174", "ten_175", "ten_176",
        "ten_177", "ten_178", "ten_179", "ten_180", "ten_181", "ten_182", "ten_183", "ten_184",
        "ten_185", "ten_186", "ten_187", "ten_188", "ten_189", "ten_190", "ten_191", "ten_192",
        "ten_193", "ten_194", "ten_195", "ten_196", "ten_197", "ten_198", "ten_199", "ten_200",
        "ten_201", "ten_202", "ten_203", "ten_204", "ten_205", "ten_206", "ten_207", "ten_208",
        "ten_209", "ten_210", "ten_211", "ten_212", "ten_213", "ten_214", "ten_215", "ten_216",
        "ten_217", "ten_218", "ten_219", "ten_220", "ten_221", "ten_222", "ten_223", "ten_224",
        "ten_225", "ten_226", "ten_227", "ten_228", "ten_229", "ten_230", "ten_231", "ten_232",
        "ten_233", "ten_234", "ten_235", "ten_236", "ten_237", "ten_238", "ten_239", "ten_240",
        "ten_241", "ten_242", "ten_243", "ten_244", "ten_245", "ten_246", "ten_247", "ten_248",
        "ten_249", "ten_250", "ten_251", "ten_252", "ten_253", "ten_254", "ten_255", "ten_256",
        "ten_257", "ten_258", "ten_259", "ten_260", "ten_261", "ten_262", "ten_263", "ten_264",
        "ten_265", "ten_266", "ten_267", "ten_268", "ten_269", "ten_270", "ten_271", "ten_272",
        "ten_273", "ten_274", "ten_275", "ten_276", "ten_277", "ten_278", "ten_279", "ten_280",
        "ten_281", "ten_282", "ten_283", "ten_284", "ten_285", "ten_286", "ten_287", "ten_288",
        "ten_289", "ten_290", "ten_291", "ten_292", "ten_293", "ten_294", "ten_295", "ten_296",
        "ten_297", "ten_298", "ten_299", "ten_300", "ten_301", "ten_302", "ten_303", "ten_304",
        "ten_305", "ten_306", "ten_307", "ten_308", "ten_309", "ten_310", "ten_311", "ten_312",
        "ten_313", "ten_314", "ten_315", "ten_316", "ten_317", "ten_318", "ten_319", "ten_320",
        "ten_321", "ten_322", "ten_323", "ten_324", "ten_325", "ten_326", "ten_327", "ten_328",
        "ten_329", "ten_330", "ten_331", "ten_332", "ten_333", "ten_334", "ten_335", "ten_336",
        "ten_337", "ten_338", "ten_339", "ten_340", "ten_341", "ten_342", "ten_343", "ten_344",
        "ten_345", "ten_346", "ten_347", "ten_348", "ten_349", "ten_350", "ten_351", "ten_352",
        "ten_353", "ten_354", "ten_355", "ten_356", "ten_357", "ten_358", "ten_359", "ten_360",
        "ten_361", "ten_362", "ten_363", "ten_364", "ten_365", "ten_366", "ten_367", "ten_368",
        "ten_369", "ten_370", "ten_371", "ten_372", "ten_373", "ten_374", "ten_375", "ten_376",
        "ten_377", "ten_378", "ten_379", "ten_380", "ten_381", "ten_382", "ten_383", "ten_384",
        "ten_385", "ten_386", "ten_387", "ten_388", "ten_389", "ten_390", "ten_391", "ten_392",
        "ten_393", "ten_394", "ten_395", "ten_396", "ten_397", "ten_398", "ten_399", "ten_400",
        "ten_401", "ten_402", "ten_403", "ten_404", "ten_405", "ten_406", "ten_407", "ten_408",
        "ten_409", "ten_410", "ten_411", "ten_412", "ten_413", "ten_414", "ten_415", "ten_416",
        "ten_417", "ten_418", "ten_419", "ten_420", "ten_421", "ten_422", "ten_423", "ten_424",
        "ten_425", "ten_426", "ten_427", "ten_428", "ten_429"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = N_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        N_ten_1<C>,   N_ten_2<C>,   N_ten_3<C>,   N_ten_4<C>,   N_ten_5<C>,   N_ten_6<C>,
        N_ten_7<C>,   N_ten_8<C>,   N_ten_9<C>,   N_ten_10<C>,  N_ten_11<C>,  N_ten_12<C>,
        N_ten_13<C>,  N_ten_14<C>,  N_ten_15<C>,  N_ten_16<C>,  N_ten_17<C>,  N_ten_18<C>,
        N_ten_19<C>,  N_ten_20<C>,  N_ten_21<C>,  N_ten_22<C>,  N_ten_23<C>,  N_ten_24<C>,
        N_ten_25<C>,  N_ten_26<C>,  N_ten_27<C>,  N_ten_28<C>,  N_ten_29<C>,  N_ten_30<C>,
        N_ten_31<C>,  N_ten_32<C>,  N_ten_33<C>,  N_ten_34<C>,  N_ten_35<C>,  N_ten_36<C>,
        N_ten_37<C>,  N_ten_38<C>,  N_ten_39<C>,  N_ten_40<C>,  N_ten_41<C>,  N_ten_42<C>,
        N_ten_43<C>,  N_ten_44<C>,  N_ten_45<C>,  N_ten_46<C>,  N_ten_47<C>,  N_ten_48<C>,
        N_ten_49<C>,  N_ten_50<C>,  N_ten_51<C>,  N_ten_52<C>,  N_ten_53<C>,  N_ten_54<C>,
        N_ten_55<C>,  N_ten_56<C>,  N_ten_57<C>,  N_ten_58<C>,  N_ten_59<C>,  N_ten_60<C>,
        N_ten_61<C>,  N_ten_62<C>,  N_ten_63<C>,  N_ten_64<C>,  N_ten_65<C>,  N_ten_66<C>,
        N_ten_67<C>,  N_ten_68<C>,  N_ten_69<C>,  N_ten_70<C>,  N_ten_71<C>,  N_ten_72<C>,
        N_ten_73<C>,  N_ten_74<C>,  N_ten_75<C>,  N_ten_76<C>,  N_ten_77<C>,  N_ten_78<C>,
        N_ten_79<C>,  N_ten_80<C>,  N_ten_81<C>,  N_ten_82<C>,  N_ten_83<C>,  N_ten_84<C>,
        N_ten_85<C>,  N_ten_86<C>,  N_ten_87<C>,  N_ten_88<C>,  N_ten_89<C>,  N_ten_90<C>,
        N_ten_91<C>,  N_ten_92<C>,  N_ten_93<C>,  N_ten_94<C>,  N_ten_95<C>,  N_ten_96<C>,
        N_ten_97<C>,  N_ten_98<C>,  N_ten_99<C>,  N_ten_100<C>, N_ten_101<C>, N_ten_102<C>,
        N_ten_103<C>, N_ten_104<C>, N_ten_105<C>, N_ten_106<C>, N_ten_107<C>, N_ten_108<C>,
        N_ten_109<C>, N_ten_110<C>, N_ten_111<C>, N_ten_112<C>, N_ten_113<C>, N_ten_114<C>,
        N_ten_115<C>, N_ten_116<C>, N_ten_117<C>, N_ten_118<C>, N_ten_119<C>, N_ten_120<C>,
        N_ten_121<C>, N_ten_122<C>, N_ten_123<C>, N_ten_124<C>, N_ten_125<C>, N_ten_126<C>,
        N_ten_127<C>, N_ten_128<C>, N_ten_129<C>, N_ten_130<C>, N_ten_131<C>, N_ten_132<C>,
        N_ten_133<C>, N_ten_134<C>, N_ten_135<C>, N_ten_136<C>, N_ten_137<C>, N_ten_138<C>,
        N_ten_139<C>, N_ten_140<C>, N_ten_141<C>, N_ten_142<C>, N_ten_143<C>, N_ten_144<C>,
        N_ten_145<C>, N_ten_146<C>, N_ten_147<C>, N_ten_148<C>, N_ten_149<C>, N_ten_150<C>,
        N_ten_151<C>, N_ten_152<C>, N_ten_153<C>, N_ten_154<C>, N_ten_155<C>, N_ten_156<C>,
        N_ten_157<C>, N_ten_158<C>, N_ten_159<C>, N_ten_160<C>, N_ten_161<C>, N_ten_162<C>,
        N_ten_163<C>, N_ten_164<C>, N_ten_165<C>, N_ten_166<C>, N_ten_167<C>, N_ten_168<C>,
        N_ten_169<C>, N_ten_170<C>, N_ten_171<C>, N_ten_172<C>, N_ten_173<C>, N_ten_174<C>,
        N_ten_175<C>, N_ten_176<C>, N_ten_177<C>, N_ten_178<C>, N_ten_179<C>, N_ten_180<C>,
        N_ten_181<C>, N_ten_182<C>, N_ten_183<C>, N_ten_184<C>, N_ten_185<C>, N_ten_186<C>,
        N_ten_187<C>, N_ten_188<C>, N_ten_189<C>, N_ten_190<C>, N_ten_191<C>, N_ten_192<C>,
        N_ten_193<C>, N_ten_194<C>, N_ten_195<C>, N_ten_196<C>, N_ten_197<C>, N_ten_198<C>,
        N_ten_199<C>, N_ten_200<C>, N_ten_201<C>, N_ten_202<C>, N_ten_203<C>, N_ten_204<C>,
        N_ten_205<C>, N_ten_206<C>, N_ten_207<C>, N_ten_208<C>, N_ten_209<C>, N_ten_210<C>,
        N_ten_211<C>, N_ten_212<C>, N_ten_213<C>, N_ten_214<C>, N_ten_215<C>, N_ten_216<C>,
        N_ten_217<C>, N_ten_218<C>, N_ten_219<C>, N_ten_220<C>, N_ten_221<C>, N_ten_222<C>,
        N_ten_223<C>, N_ten_224<C>, N_ten_225<C>, N_ten_226<C>, N_ten_227<C>, N_ten_228<C>,
        N_ten_229<C>, N_ten_230<C>, N_ten_231<C>, N_ten_232<C>, N_ten_233<C>, N_ten_234<C>,
        N_ten_235<C>, N_ten_236<C>, N_ten_237<C>, N_ten_238<C>, N_ten_239<C>, N_ten_240<C>,
        N_ten_241<C>, N_ten_242<C>, N_ten_243<C>, N_ten_244<C>, N_ten_245<C>, N_ten_246<C>,
        N_ten_247<C>, N_ten_248<C>, N_ten_249<C>, N_ten_250<C>, N_ten_251<C>, N_ten_252<C>,
        N_ten_253<C>, N_ten_254<C>, N_ten_255<C>, N_ten_256<C>, N_ten_257<C>, N_ten_258<C>,
        N_ten_259<C>, N_ten_260<C>, N_ten_261<C>, N_ten_262<C>, N_ten_263<C>, N_ten_264<C>,
        N_ten_265<C>, N_ten_266<C>, N_ten_267<C>, N_ten_268<C>, N_ten_269<C>, N_ten_270<C>,
        N_ten_271<C>, N_ten_272<C>, N_ten_273<C>, N_ten_274<C>, N_ten_275<C>, N_ten_276<C>,
        N_ten_277<C>, N_ten_278<C>, N_ten_279<C>, N_ten_280<C>, N_ten_281<C>, N_ten_282<C>,
        N_ten_283<C>, N_ten_284<C>, N_ten_285<C>, N_ten_286<C>, N_ten_287<C>, N_ten_288<C>,
        N_ten_289<C>, N_ten_290<C>, N_ten_291<C>, N_ten_292<C>, N_ten_293<C>, N_ten_294<C>,
        N_ten_295<C>, N_ten_296<C>, N_ten_297<C>, N_ten_298<C>, N_ten_299<C>, N_ten_300<C>,
        N_ten_301<C>, N_ten_302<C>, N_ten_303<C>, N_ten_304<C>, N_ten_305<C>, N_ten_306<C>,
        N_ten_307<C>, N_ten_308<C>, N_ten_309<C>, N_ten_310<C>, N_ten_311<C>, N_ten_312<C>,
        N_ten_313<C>, N_ten_314<C>, N_ten_315<C>, N_ten_316<C>, N_ten_317<C>, N_ten_318<C>,
        N_ten_319<C>, N_ten_320<C>, N_ten_321<C>, N_ten_322<C>, N_ten_323<C>, N_ten_324<C>,
        N_ten_325<C>, N_ten_326<C>, N_ten_327<C>, N_ten_328<C>, N_ten_329<C>, N_ten_330<C>,
        N_ten_331<C>, N_ten_332<C>, N_ten_333<C>, N_ten_334<C>, N_ten_335<C>, N_ten_336<C>,
        N_ten_337<C>, N_ten_338<C>, N_ten_339<C>, N_ten_340<C>, N_ten_341<C>, N_ten_342<C>,
        N_ten_343<C>, N_ten_344<C>, N_ten_345<C>, N_ten_346<C>, N_ten_347<C>, N_ten_348<C>,
        N_ten_349<C>, N_ten_350<C>, N_ten_351<C>, N_ten_352<C>, N_ten_353<C>, N_ten_354<C>,
        N_ten_355<C>, N_ten_356<C>, N_ten_357<C>, N_ten_358<C>, N_ten_359<C>, N_ten_360<C>,
        N_ten_361<C>, N_ten_362<C>, N_ten_363<C>, N_ten_364<C>, N_ten_365<C>, N_ten_366<C>,
        N_ten_367<C>, N_ten_368<C>, N_ten_369<C>, N_ten_370<C>, N_ten_371<C>, N_ten_372<C>,
        N_ten_373<C>, N_ten_374<C>, N_ten_375<C>, N_ten_376<C>, N_ten_377<C>, N_ten_378<C>,
        N_ten_379<C>, N_ten_380<C>, N_ten_381<C>, N_ten_382<C>, N_ten_383<C>, N_ten_384<C>,
        N_ten_385<C>, N_ten_386<C>, N_ten_387<C>, N_ten_388<C>, N_ten_389<C>, N_ten_390<C>,
        N_ten_391<C>, N_ten_392<C>, N_ten_393<C>, N_ten_394<C>, N_ten_395<C>, N_ten_396<C>,
        N_ten_397<C>, N_ten_398<C>, N_ten_399<C>, N_ten_400<C>, N_ten_401<C>, N_ten_402<C>,
        N_ten_403<C>, N_ten_404<C>, N_ten_405<C>, N_ten_406<C>, N_ten_407<C>, N_ten_408<C>,
        N_ten_409<C>, N_ten_410<C>, N_ten_411<C>, N_ten_412<C>, N_ten_413<C>, N_ten_414<C>,
        N_ten_415<C>, N_ten_416<C>, N_ten_417<C>, N_ten_418<C>, N_ten_419<C>, N_ten_420<C>,
        N_ten_421<C>, N_ten_422<C>, N_ten_423<C>, N_ten_424<C>, N_ten_425<C>, N_ten_426<C>,
        N_ten_427<C>, N_ten_428<C>, N_ten_429<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = N_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        N_ten_1<CHP>,   N_ten_2<CHP>,   N_ten_3<CHP>,   N_ten_4<CHP>,   N_ten_5<CHP>,
        N_ten_6<CHP>,   N_ten_7<CHP>,   N_ten_8<CHP>,   N_ten_9<CHP>,   N_ten_10<CHP>,
        N_ten_11<CHP>,  N_ten_12<CHP>,  N_ten_13<CHP>,  N_ten_14<CHP>,  N_ten_15<CHP>,
        N_ten_16<CHP>,  N_ten_17<CHP>,  N_ten_18<CHP>,  N_ten_19<CHP>,  N_ten_20<CHP>,
        N_ten_21<CHP>,  N_ten_22<CHP>,  N_ten_23<CHP>,  N_ten_24<CHP>,  N_ten_25<CHP>,
        N_ten_26<CHP>,  N_ten_27<CHP>,  N_ten_28<CHP>,  N_ten_29<CHP>,  N_ten_30<CHP>,
        N_ten_31<CHP>,  N_ten_32<CHP>,  N_ten_33<CHP>,  N_ten_34<CHP>,  N_ten_35<CHP>,
        N_ten_36<CHP>,  N_ten_37<CHP>,  N_ten_38<CHP>,  N_ten_39<CHP>,  N_ten_40<CHP>,
        N_ten_41<CHP>,  N_ten_42<CHP>,  N_ten_43<CHP>,  N_ten_44<CHP>,  N_ten_45<CHP>,
        N_ten_46<CHP>,  N_ten_47<CHP>,  N_ten_48<CHP>,  N_ten_49<CHP>,  N_ten_50<CHP>,
        N_ten_51<CHP>,  N_ten_52<CHP>,  N_ten_53<CHP>,  N_ten_54<CHP>,  N_ten_55<CHP>,
        N_ten_56<CHP>,  N_ten_57<CHP>,  N_ten_58<CHP>,  N_ten_59<CHP>,  N_ten_60<CHP>,
        N_ten_61<CHP>,  N_ten_62<CHP>,  N_ten_63<CHP>,  N_ten_64<CHP>,  N_ten_65<CHP>,
        N_ten_66<CHP>,  N_ten_67<CHP>,  N_ten_68<CHP>,  N_ten_69<CHP>,  N_ten_70<CHP>,
        N_ten_71<CHP>,  N_ten_72<CHP>,  N_ten_73<CHP>,  N_ten_74<CHP>,  N_ten_75<CHP>,
        N_ten_76<CHP>,  N_ten_77<CHP>,  N_ten_78<CHP>,  N_ten_79<CHP>,  N_ten_80<CHP>,
        N_ten_81<CHP>,  N_ten_82<CHP>,  N_ten_83<CHP>,  N_ten_84<CHP>,  N_ten_85<CHP>,
        N_ten_86<CHP>,  N_ten_87<CHP>,  N_ten_88<CHP>,  N_ten_89<CHP>,  N_ten_90<CHP>,
        N_ten_91<CHP>,  N_ten_92<CHP>,  N_ten_93<CHP>,  N_ten_94<CHP>,  N_ten_95<CHP>,
        N_ten_96<CHP>,  N_ten_97<CHP>,  N_ten_98<CHP>,  N_ten_99<CHP>,  N_ten_100<CHP>,
        N_ten_101<CHP>, N_ten_102<CHP>, N_ten_103<CHP>, N_ten_104<CHP>, N_ten_105<CHP>,
        N_ten_106<CHP>, N_ten_107<CHP>, N_ten_108<CHP>, N_ten_109<CHP>, N_ten_110<CHP>,
        N_ten_111<CHP>, N_ten_112<CHP>, N_ten_113<CHP>, N_ten_114<CHP>, N_ten_115<CHP>,
        N_ten_116<CHP>, N_ten_117<CHP>, N_ten_118<CHP>, N_ten_119<CHP>, N_ten_120<CHP>,
        N_ten_121<CHP>, N_ten_122<CHP>, N_ten_123<CHP>, N_ten_124<CHP>, N_ten_125<CHP>,
        N_ten_126<CHP>, N_ten_127<CHP>, N_ten_128<CHP>, N_ten_129<CHP>, N_ten_130<CHP>,
        N_ten_131<CHP>, N_ten_132<CHP>, N_ten_133<CHP>, N_ten_134<CHP>, N_ten_135<CHP>,
        N_ten_136<CHP>, N_ten_137<CHP>, N_ten_138<CHP>, N_ten_139<CHP>, N_ten_140<CHP>,
        N_ten_141<CHP>, N_ten_142<CHP>, N_ten_143<CHP>, N_ten_144<CHP>, N_ten_145<CHP>,
        N_ten_146<CHP>, N_ten_147<CHP>, N_ten_148<CHP>, N_ten_149<CHP>, N_ten_150<CHP>,
        N_ten_151<CHP>, N_ten_152<CHP>, N_ten_153<CHP>, N_ten_154<CHP>, N_ten_155<CHP>,
        N_ten_156<CHP>, N_ten_157<CHP>, N_ten_158<CHP>, N_ten_159<CHP>, N_ten_160<CHP>,
        N_ten_161<CHP>, N_ten_162<CHP>, N_ten_163<CHP>, N_ten_164<CHP>, N_ten_165<CHP>,
        N_ten_166<CHP>, N_ten_167<CHP>, N_ten_168<CHP>, N_ten_169<CHP>, N_ten_170<CHP>,
        N_ten_171<CHP>, N_ten_172<CHP>, N_ten_173<CHP>, N_ten_174<CHP>, N_ten_175<CHP>,
        N_ten_176<CHP>, N_ten_177<CHP>, N_ten_178<CHP>, N_ten_179<CHP>, N_ten_180<CHP>,
        N_ten_181<CHP>, N_ten_182<CHP>, N_ten_183<CHP>, N_ten_184<CHP>, N_ten_185<CHP>,
        N_ten_186<CHP>, N_ten_187<CHP>, N_ten_188<CHP>, N_ten_189<CHP>, N_ten_190<CHP>,
        N_ten_191<CHP>, N_ten_192<CHP>, N_ten_193<CHP>, N_ten_194<CHP>, N_ten_195<CHP>,
        N_ten_196<CHP>, N_ten_197<CHP>, N_ten_198<CHP>, N_ten_199<CHP>, N_ten_200<CHP>,
        N_ten_201<CHP>, N_ten_202<CHP>, N_ten_203<CHP>, N_ten_204<CHP>, N_ten_205<CHP>,
        N_ten_206<CHP>, N_ten_207<CHP>, N_ten_208<CHP>, N_ten_209<CHP>, N_ten_210<CHP>,
        N_ten_211<CHP>, N_ten_212<CHP>, N_ten_213<CHP>, N_ten_214<CHP>, N_ten_215<CHP>,
        N_ten_216<CHP>, N_ten_217<CHP>, N_ten_218<CHP>, N_ten_219<CHP>, N_ten_220<CHP>,
        N_ten_221<CHP>, N_ten_222<CHP>, N_ten_223<CHP>, N_ten_224<CHP>, N_ten_225<CHP>,
        N_ten_226<CHP>, N_ten_227<CHP>, N_ten_228<CHP>, N_ten_229<CHP>, N_ten_230<CHP>,
        N_ten_231<CHP>, N_ten_232<CHP>, N_ten_233<CHP>, N_ten_234<CHP>, N_ten_235<CHP>,
        N_ten_236<CHP>, N_ten_237<CHP>, N_ten_238<CHP>, N_ten_239<CHP>, N_ten_240<CHP>,
        N_ten_241<CHP>, N_ten_242<CHP>, N_ten_243<CHP>, N_ten_244<CHP>, N_ten_245<CHP>,
        N_ten_246<CHP>, N_ten_247<CHP>, N_ten_248<CHP>, N_ten_249<CHP>, N_ten_250<CHP>,
        N_ten_251<CHP>, N_ten_252<CHP>, N_ten_253<CHP>, N_ten_254<CHP>, N_ten_255<CHP>,
        N_ten_256<CHP>, N_ten_257<CHP>, N_ten_258<CHP>, N_ten_259<CHP>, N_ten_260<CHP>,
        N_ten_261<CHP>, N_ten_262<CHP>, N_ten_263<CHP>, N_ten_264<CHP>, N_ten_265<CHP>,
        N_ten_266<CHP>, N_ten_267<CHP>, N_ten_268<CHP>, N_ten_269<CHP>, N_ten_270<CHP>,
        N_ten_271<CHP>, N_ten_272<CHP>, N_ten_273<CHP>, N_ten_274<CHP>, N_ten_275<CHP>,
        N_ten_276<CHP>, N_ten_277<CHP>, N_ten_278<CHP>, N_ten_279<CHP>, N_ten_280<CHP>,
        N_ten_281<CHP>, N_ten_282<CHP>, N_ten_283<CHP>, N_ten_284<CHP>, N_ten_285<CHP>,
        N_ten_286<CHP>, N_ten_287<CHP>, N_ten_288<CHP>, N_ten_289<CHP>, N_ten_290<CHP>,
        N_ten_291<CHP>, N_ten_292<CHP>, N_ten_293<CHP>, N_ten_294<CHP>, N_ten_295<CHP>,
        N_ten_296<CHP>, N_ten_297<CHP>, N_ten_298<CHP>, N_ten_299<CHP>, N_ten_300<CHP>,
        N_ten_301<CHP>, N_ten_302<CHP>, N_ten_303<CHP>, N_ten_304<CHP>, N_ten_305<CHP>,
        N_ten_306<CHP>, N_ten_307<CHP>, N_ten_308<CHP>, N_ten_309<CHP>, N_ten_310<CHP>,
        N_ten_311<CHP>, N_ten_312<CHP>, N_ten_313<CHP>, N_ten_314<CHP>, N_ten_315<CHP>,
        N_ten_316<CHP>, N_ten_317<CHP>, N_ten_318<CHP>, N_ten_319<CHP>, N_ten_320<CHP>,
        N_ten_321<CHP>, N_ten_322<CHP>, N_ten_323<CHP>, N_ten_324<CHP>, N_ten_325<CHP>,
        N_ten_326<CHP>, N_ten_327<CHP>, N_ten_328<CHP>, N_ten_329<CHP>, N_ten_330<CHP>,
        N_ten_331<CHP>, N_ten_332<CHP>, N_ten_333<CHP>, N_ten_334<CHP>, N_ten_335<CHP>,
        N_ten_336<CHP>, N_ten_337<CHP>, N_ten_338<CHP>, N_ten_339<CHP>, N_ten_340<CHP>,
        N_ten_341<CHP>, N_ten_342<CHP>, N_ten_343<CHP>, N_ten_344<CHP>, N_ten_345<CHP>,
        N_ten_346<CHP>, N_ten_347<CHP>, N_ten_348<CHP>, N_ten_349<CHP>, N_ten_350<CHP>,
        N_ten_351<CHP>, N_ten_352<CHP>, N_ten_353<CHP>, N_ten_354<CHP>, N_ten_355<CHP>,
        N_ten_356<CHP>, N_ten_357<CHP>, N_ten_358<CHP>, N_ten_359<CHP>, N_ten_360<CHP>,
        N_ten_361<CHP>, N_ten_362<CHP>, N_ten_363<CHP>, N_ten_364<CHP>, N_ten_365<CHP>,
        N_ten_366<CHP>, N_ten_367<CHP>, N_ten_368<CHP>, N_ten_369<CHP>, N_ten_370<CHP>,
        N_ten_371<CHP>, N_ten_372<CHP>, N_ten_373<CHP>, N_ten_374<CHP>, N_ten_375<CHP>,
        N_ten_376<CHP>, N_ten_377<CHP>, N_ten_378<CHP>, N_ten_379<CHP>, N_ten_380<CHP>,
        N_ten_381<CHP>, N_ten_382<CHP>, N_ten_383<CHP>, N_ten_384<CHP>, N_ten_385<CHP>,
        N_ten_386<CHP>, N_ten_387<CHP>, N_ten_388<CHP>, N_ten_389<CHP>, N_ten_390<CHP>,
        N_ten_391<CHP>, N_ten_392<CHP>, N_ten_393<CHP>, N_ten_394<CHP>, N_ten_395<CHP>,
        N_ten_396<CHP>, N_ten_397<CHP>, N_ten_398<CHP>, N_ten_399<CHP>, N_ten_400<CHP>,
        N_ten_401<CHP>, N_ten_402<CHP>, N_ten_403<CHP>, N_ten_404<CHP>, N_ten_405<CHP>,
        N_ten_406<CHP>, N_ten_407<CHP>, N_ten_408<CHP>, N_ten_409<CHP>, N_ten_410<CHP>,
        N_ten_411<CHP>, N_ten_412<CHP>, N_ten_413<CHP>, N_ten_414<CHP>, N_ten_415<CHP>,
        N_ten_416<CHP>, N_ten_417<CHP>, N_ten_418<CHP>, N_ten_419<CHP>, N_ten_420<CHP>,
        N_ten_421<CHP>, N_ten_422<CHP>, N_ten_423<CHP>, N_ten_424<CHP>, N_ten_425<CHP>,
        N_ten_426<CHP>, N_ten_427<CHP>, N_ten_428<CHP>, N_ten_429<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = N_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        N_ten_1<CVHP>,   N_ten_2<CVHP>,   N_ten_3<CVHP>,   N_ten_4<CVHP>,   N_ten_5<CVHP>,
        N_ten_6<CVHP>,   N_ten_7<CVHP>,   N_ten_8<CVHP>,   N_ten_9<CVHP>,   N_ten_10<CVHP>,
        N_ten_11<CVHP>,  N_ten_12<CVHP>,  N_ten_13<CVHP>,  N_ten_14<CVHP>,  N_ten_15<CVHP>,
        N_ten_16<CVHP>,  N_ten_17<CVHP>,  N_ten_18<CVHP>,  N_ten_19<CVHP>,  N_ten_20<CVHP>,
        N_ten_21<CVHP>,  N_ten_22<CVHP>,  N_ten_23<CVHP>,  N_ten_24<CVHP>,  N_ten_25<CVHP>,
        N_ten_26<CVHP>,  N_ten_27<CVHP>,  N_ten_28<CVHP>,  N_ten_29<CVHP>,  N_ten_30<CVHP>,
        N_ten_31<CVHP>,  N_ten_32<CVHP>,  N_ten_33<CVHP>,  N_ten_34<CVHP>,  N_ten_35<CVHP>,
        N_ten_36<CVHP>,  N_ten_37<CVHP>,  N_ten_38<CVHP>,  N_ten_39<CVHP>,  N_ten_40<CVHP>,
        N_ten_41<CVHP>,  N_ten_42<CVHP>,  N_ten_43<CVHP>,  N_ten_44<CVHP>,  N_ten_45<CVHP>,
        N_ten_46<CVHP>,  N_ten_47<CVHP>,  N_ten_48<CVHP>,  N_ten_49<CVHP>,  N_ten_50<CVHP>,
        N_ten_51<CVHP>,  N_ten_52<CVHP>,  N_ten_53<CVHP>,  N_ten_54<CVHP>,  N_ten_55<CVHP>,
        N_ten_56<CVHP>,  N_ten_57<CVHP>,  N_ten_58<CVHP>,  N_ten_59<CVHP>,  N_ten_60<CVHP>,
        N_ten_61<CVHP>,  N_ten_62<CVHP>,  N_ten_63<CVHP>,  N_ten_64<CVHP>,  N_ten_65<CVHP>,
        N_ten_66<CVHP>,  N_ten_67<CVHP>,  N_ten_68<CVHP>,  N_ten_69<CVHP>,  N_ten_70<CVHP>,
        N_ten_71<CVHP>,  N_ten_72<CVHP>,  N_ten_73<CVHP>,  N_ten_74<CVHP>,  N_ten_75<CVHP>,
        N_ten_76<CVHP>,  N_ten_77<CVHP>,  N_ten_78<CVHP>,  N_ten_79<CVHP>,  N_ten_80<CVHP>,
        N_ten_81<CVHP>,  N_ten_82<CVHP>,  N_ten_83<CVHP>,  N_ten_84<CVHP>,  N_ten_85<CVHP>,
        N_ten_86<CVHP>,  N_ten_87<CVHP>,  N_ten_88<CVHP>,  N_ten_89<CVHP>,  N_ten_90<CVHP>,
        N_ten_91<CVHP>,  N_ten_92<CVHP>,  N_ten_93<CVHP>,  N_ten_94<CVHP>,  N_ten_95<CVHP>,
        N_ten_96<CVHP>,  N_ten_97<CVHP>,  N_ten_98<CVHP>,  N_ten_99<CVHP>,  N_ten_100<CVHP>,
        N_ten_101<CVHP>, N_ten_102<CVHP>, N_ten_103<CVHP>, N_ten_104<CVHP>, N_ten_105<CVHP>,
        N_ten_106<CVHP>, N_ten_107<CVHP>, N_ten_108<CVHP>, N_ten_109<CVHP>, N_ten_110<CVHP>,
        N_ten_111<CVHP>, N_ten_112<CVHP>, N_ten_113<CVHP>, N_ten_114<CVHP>, N_ten_115<CVHP>,
        N_ten_116<CVHP>, N_ten_117<CVHP>, N_ten_118<CVHP>, N_ten_119<CVHP>, N_ten_120<CVHP>,
        N_ten_121<CVHP>, N_ten_122<CVHP>, N_ten_123<CVHP>, N_ten_124<CVHP>, N_ten_125<CVHP>,
        N_ten_126<CVHP>, N_ten_127<CVHP>, N_ten_128<CVHP>, N_ten_129<CVHP>, N_ten_130<CVHP>,
        N_ten_131<CVHP>, N_ten_132<CVHP>, N_ten_133<CVHP>, N_ten_134<CVHP>, N_ten_135<CVHP>,
        N_ten_136<CVHP>, N_ten_137<CVHP>, N_ten_138<CVHP>, N_ten_139<CVHP>, N_ten_140<CVHP>,
        N_ten_141<CVHP>, N_ten_142<CVHP>, N_ten_143<CVHP>, N_ten_144<CVHP>, N_ten_145<CVHP>,
        N_ten_146<CVHP>, N_ten_147<CVHP>, N_ten_148<CVHP>, N_ten_149<CVHP>, N_ten_150<CVHP>,
        N_ten_151<CVHP>, N_ten_152<CVHP>, N_ten_153<CVHP>, N_ten_154<CVHP>, N_ten_155<CVHP>,
        N_ten_156<CVHP>, N_ten_157<CVHP>, N_ten_158<CVHP>, N_ten_159<CVHP>, N_ten_160<CVHP>,
        N_ten_161<CVHP>, N_ten_162<CVHP>, N_ten_163<CVHP>, N_ten_164<CVHP>, N_ten_165<CVHP>,
        N_ten_166<CVHP>, N_ten_167<CVHP>, N_ten_168<CVHP>, N_ten_169<CVHP>, N_ten_170<CVHP>,
        N_ten_171<CVHP>, N_ten_172<CVHP>, N_ten_173<CVHP>, N_ten_174<CVHP>, N_ten_175<CVHP>,
        N_ten_176<CVHP>, N_ten_177<CVHP>, N_ten_178<CVHP>, N_ten_179<CVHP>, N_ten_180<CVHP>,
        N_ten_181<CVHP>, N_ten_182<CVHP>, N_ten_183<CVHP>, N_ten_184<CVHP>, N_ten_185<CVHP>,
        N_ten_186<CVHP>, N_ten_187<CVHP>, N_ten_188<CVHP>, N_ten_189<CVHP>, N_ten_190<CVHP>,
        N_ten_191<CVHP>, N_ten_192<CVHP>, N_ten_193<CVHP>, N_ten_194<CVHP>, N_ten_195<CVHP>,
        N_ten_196<CVHP>, N_ten_197<CVHP>, N_ten_198<CVHP>, N_ten_199<CVHP>, N_ten_200<CVHP>,
        N_ten_201<CVHP>, N_ten_202<CVHP>, N_ten_203<CVHP>, N_ten_204<CVHP>, N_ten_205<CVHP>,
        N_ten_206<CVHP>, N_ten_207<CVHP>, N_ten_208<CVHP>, N_ten_209<CVHP>, N_ten_210<CVHP>,
        N_ten_211<CVHP>, N_ten_212<CVHP>, N_ten_213<CVHP>, N_ten_214<CVHP>, N_ten_215<CVHP>,
        N_ten_216<CVHP>, N_ten_217<CVHP>, N_ten_218<CVHP>, N_ten_219<CVHP>, N_ten_220<CVHP>,
        N_ten_221<CVHP>, N_ten_222<CVHP>, N_ten_223<CVHP>, N_ten_224<CVHP>, N_ten_225<CVHP>,
        N_ten_226<CVHP>, N_ten_227<CVHP>, N_ten_228<CVHP>, N_ten_229<CVHP>, N_ten_230<CVHP>,
        N_ten_231<CVHP>, N_ten_232<CVHP>, N_ten_233<CVHP>, N_ten_234<CVHP>, N_ten_235<CVHP>,
        N_ten_236<CVHP>, N_ten_237<CVHP>, N_ten_238<CVHP>, N_ten_239<CVHP>, N_ten_240<CVHP>,
        N_ten_241<CVHP>, N_ten_242<CVHP>, N_ten_243<CVHP>, N_ten_244<CVHP>, N_ten_245<CVHP>,
        N_ten_246<CVHP>, N_ten_247<CVHP>, N_ten_248<CVHP>, N_ten_249<CVHP>, N_ten_250<CVHP>,
        N_ten_251<CVHP>, N_ten_252<CVHP>, N_ten_253<CVHP>, N_ten_254<CVHP>, N_ten_255<CVHP>,
        N_ten_256<CVHP>, N_ten_257<CVHP>, N_ten_258<CVHP>, N_ten_259<CVHP>, N_ten_260<CVHP>,
        N_ten_261<CVHP>, N_ten_262<CVHP>, N_ten_263<CVHP>, N_ten_264<CVHP>, N_ten_265<CVHP>,
        N_ten_266<CVHP>, N_ten_267<CVHP>, N_ten_268<CVHP>, N_ten_269<CVHP>, N_ten_270<CVHP>,
        N_ten_271<CVHP>, N_ten_272<CVHP>, N_ten_273<CVHP>, N_ten_274<CVHP>, N_ten_275<CVHP>,
        N_ten_276<CVHP>, N_ten_277<CVHP>, N_ten_278<CVHP>, N_ten_279<CVHP>, N_ten_280<CVHP>,
        N_ten_281<CVHP>, N_ten_282<CVHP>, N_ten_283<CVHP>, N_ten_284<CVHP>, N_ten_285<CVHP>,
        N_ten_286<CVHP>, N_ten_287<CVHP>, N_ten_288<CVHP>, N_ten_289<CVHP>, N_ten_290<CVHP>,
        N_ten_291<CVHP>, N_ten_292<CVHP>, N_ten_293<CVHP>, N_ten_294<CVHP>, N_ten_295<CVHP>,
        N_ten_296<CVHP>, N_ten_297<CVHP>, N_ten_298<CVHP>, N_ten_299<CVHP>, N_ten_300<CVHP>,
        N_ten_301<CVHP>, N_ten_302<CVHP>, N_ten_303<CVHP>, N_ten_304<CVHP>, N_ten_305<CVHP>,
        N_ten_306<CVHP>, N_ten_307<CVHP>, N_ten_308<CVHP>, N_ten_309<CVHP>, N_ten_310<CVHP>,
        N_ten_311<CVHP>, N_ten_312<CVHP>, N_ten_313<CVHP>, N_ten_314<CVHP>, N_ten_315<CVHP>,
        N_ten_316<CVHP>, N_ten_317<CVHP>, N_ten_318<CVHP>, N_ten_319<CVHP>, N_ten_320<CVHP>,
        N_ten_321<CVHP>, N_ten_322<CVHP>, N_ten_323<CVHP>, N_ten_324<CVHP>, N_ten_325<CVHP>,
        N_ten_326<CVHP>, N_ten_327<CVHP>, N_ten_328<CVHP>, N_ten_329<CVHP>, N_ten_330<CVHP>,
        N_ten_331<CVHP>, N_ten_332<CVHP>, N_ten_333<CVHP>, N_ten_334<CVHP>, N_ten_335<CVHP>,
        N_ten_336<CVHP>, N_ten_337<CVHP>, N_ten_338<CVHP>, N_ten_339<CVHP>, N_ten_340<CVHP>,
        N_ten_341<CVHP>, N_ten_342<CVHP>, N_ten_343<CVHP>, N_ten_344<CVHP>, N_ten_345<CVHP>,
        N_ten_346<CVHP>, N_ten_347<CVHP>, N_ten_348<CVHP>, N_ten_349<CVHP>, N_ten_350<CVHP>,
        N_ten_351<CVHP>, N_ten_352<CVHP>, N_ten_353<CVHP>, N_ten_354<CVHP>, N_ten_355<CVHP>,
        N_ten_356<CVHP>, N_ten_357<CVHP>, N_ten_358<CVHP>, N_ten_359<CVHP>, N_ten_360<CVHP>,
        N_ten_361<CVHP>, N_ten_362<CVHP>, N_ten_363<CVHP>, N_ten_364<CVHP>, N_ten_365<CVHP>,
        N_ten_366<CVHP>, N_ten_367<CVHP>, N_ten_368<CVHP>, N_ten_369<CVHP>, N_ten_370<CVHP>,
        N_ten_371<CVHP>, N_ten_372<CVHP>, N_ten_373<CVHP>, N_ten_374<CVHP>, N_ten_375<CVHP>,
        N_ten_376<CVHP>, N_ten_377<CVHP>, N_ten_378<CVHP>, N_ten_379<CVHP>, N_ten_380<CVHP>,
        N_ten_381<CVHP>, N_ten_382<CVHP>, N_ten_383<CVHP>, N_ten_384<CVHP>, N_ten_385<CVHP>,
        N_ten_386<CVHP>, N_ten_387<CVHP>, N_ten_388<CVHP>, N_ten_389<CVHP>, N_ten_390<CVHP>,
        N_ten_391<CVHP>, N_ten_392<CVHP>, N_ten_393<CVHP>, N_ten_394<CVHP>, N_ten_395<CVHP>,
        N_ten_396<CVHP>, N_ten_397<CVHP>, N_ten_398<CVHP>, N_ten_399<CVHP>, N_ten_400<CVHP>,
        N_ten_401<CVHP>, N_ten_402<CVHP>, N_ten_403<CVHP>, N_ten_404<CVHP>, N_ten_405<CVHP>,
        N_ten_406<CVHP>, N_ten_407<CVHP>, N_ten_408<CVHP>, N_ten_409<CVHP>, N_ten_410<CVHP>,
        N_ten_411<CVHP>, N_ten_412<CVHP>, N_ten_413<CVHP>, N_ten_414<CVHP>, N_ten_415<CVHP>,
        N_ten_416<CVHP>, N_ten_417<CVHP>, N_ten_418<CVHP>, N_ten_419<CVHP>, N_ten_420<CVHP>,
        N_ten_421<CVHP>, N_ten_422<CVHP>, N_ten_423<CVHP>, N_ten_424<CVHP>, N_ten_425<CVHP>,
        N_ten_426<CVHP>, N_ten_427<CVHP>, N_ten_428<CVHP>, N_ten_429<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = N_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        N_ten_1<F32>,   N_ten_2<F32>,   N_ten_3<F32>,   N_ten_4<F32>,   N_ten_5<F32>,
        N_ten_6<F32>,   N_ten_7<F32>,   N_ten_8<F32>,   N_ten_9<F32>,   N_ten_10<F32>,
        N_ten_11<F32>,  N_ten_12<F32>,  N_ten_13<F32>,  N_ten_14<F32>,  N_ten_15<F32>,
        N_ten_16<F32>,  N_ten_17<F32>,  N_ten_18<F32>,  N_ten_19<F32>,  N_ten_20<F32>,
        N_ten_21<F32>,  N_ten_22<F32>,  N_ten_23<F32>,  N_ten_24<F32>,  N_ten_25<F32>,
        N_ten_26<F32>,  N_ten_27<F32>,  N_ten_28<F32>,  N_ten_29<F32>,  N_ten_30<F32>,
        N_ten_31<F32>,  N_ten_32<F32>,  N_ten_33<F32>,  N_ten_34<F32>,  N_ten_35<F32>,
        N_ten_36<F32>,  N_ten_37<F32>,  N_ten_38<F32>,  N_ten_39<F32>,  N_ten_40<F32>,
        N_ten_41<F32>,  N_ten_42<F32>,  N_ten_43<F32>,  N_ten_44<F32>,  N_ten_45<F32>,
        N_ten_46<F32>,  N_ten_47<F32>,  N_ten_48<F32>,  N_ten_49<F32>,  N_ten_50<F32>,
        N_ten_51<F32>,  N_ten_52<F32>,  N_ten_53<F32>,  N_ten_54<F32>,  N_ten_55<F32>,
        N_ten_56<F32>,  N_ten_57<F32>,  N_ten_58<F32>,  N_ten_59<F32>,  N_ten_60<F32>,
        N_ten_61<F32>,  N_ten_62<F32>,  N_ten_63<F32>,  N_ten_64<F32>,  N_ten_65<F32>,
        N_ten_66<F32>,  N_ten_67<F32>,  N_ten_68<F32>,  N_ten_69<F32>,  N_ten_70<F32>,
        N_ten_71<F32>,  N_ten_72<F32>,  N_ten_73<F32>,  N_ten_74<F32>,  N_ten_75<F32>,
        N_ten_76<F32>,  N_ten_77<F32>,  N_ten_78<F32>,  N_ten_79<F32>,  N_ten_80<F32>,
        N_ten_81<F32>,  N_ten_82<F32>,  N_ten_83<F32>,  N_ten_84<F32>,  N_ten_85<F32>,
        N_ten_86<F32>,  N_ten_87<F32>,  N_ten_88<F32>,  N_ten_89<F32>,  N_ten_90<F32>,
        N_ten_91<F32>,  N_ten_92<F32>,  N_ten_93<F32>,  N_ten_94<F32>,  N_ten_95<F32>,
        N_ten_96<F32>,  N_ten_97<F32>,  N_ten_98<F32>,  N_ten_99<F32>,  N_ten_100<F32>,
        N_ten_101<F32>, N_ten_102<F32>, N_ten_103<F32>, N_ten_104<F32>, N_ten_105<F32>,
        N_ten_106<F32>, N_ten_107<F32>, N_ten_108<F32>, N_ten_109<F32>, N_ten_110<F32>,
        N_ten_111<F32>, N_ten_112<F32>, N_ten_113<F32>, N_ten_114<F32>, N_ten_115<F32>,
        N_ten_116<F32>, N_ten_117<F32>, N_ten_118<F32>, N_ten_119<F32>, N_ten_120<F32>,
        N_ten_121<F32>, N_ten_122<F32>, N_ten_123<F32>, N_ten_124<F32>, N_ten_125<F32>,
        N_ten_126<F32>, N_ten_127<F32>, N_ten_128<F32>, N_ten_129<F32>, N_ten_130<F32>,
        N_ten_131<F32>, N_ten_132<F32>, N_ten_133<F32>, N_ten_134<F32>, N_ten_135<F32>,
        N_ten_136<F32>, N_ten_137<F32>, N_ten_138<F32>, N_ten_139<F32>, N_ten_140<F32>,
        N_ten_141<F32>, N_ten_142<F32>, N_ten_143<F32>, N_ten_144<F32>, N_ten_145<F32>,
        N_ten_146<F32>, N_ten_147<F32>, N_ten_148<F32>, N_ten_149<F32>, N_ten_150<F32>,
        N_ten_151<F32>, N_ten_152<F32>, N_ten_153<F32>, N_ten_154<F32>, N_ten_155<F32>,
        N_ten_156<F32>, N_ten_157<F32>, N_ten_158<F32>, N_ten_159<F32>, N_ten_160<F32>,
        N_ten_161<F32>, N_ten_162<F32>, N_ten_163<F32>, N_ten_164<F32>, N_ten_165<F32>,
        N_ten_166<F32>, N_ten_167<F32>, N_ten_168<F32>, N_ten_169<F32>, N_ten_170<F32>,
        N_ten_171<F32>, N_ten_172<F32>, N_ten_173<F32>, N_ten_174<F32>, N_ten_175<F32>,
        N_ten_176<F32>, N_ten_177<F32>, N_ten_178<F32>, N_ten_179<F32>, N_ten_180<F32>,
        N_ten_181<F32>, N_ten_182<F32>, N_ten_183<F32>, N_ten_184<F32>, N_ten_185<F32>,
        N_ten_186<F32>, N_ten_187<F32>, N_ten_188<F32>, N_ten_189<F32>, N_ten_190<F32>,
        N_ten_191<F32>, N_ten_192<F32>, N_ten_193<F32>, N_ten_194<F32>, N_ten_195<F32>,
        N_ten_196<F32>, N_ten_197<F32>, N_ten_198<F32>, N_ten_199<F32>, N_ten_200<F32>,
        N_ten_201<F32>, N_ten_202<F32>, N_ten_203<F32>, N_ten_204<F32>, N_ten_205<F32>,
        N_ten_206<F32>, N_ten_207<F32>, N_ten_208<F32>, N_ten_209<F32>, N_ten_210<F32>,
        N_ten_211<F32>, N_ten_212<F32>, N_ten_213<F32>, N_ten_214<F32>, N_ten_215<F32>,
        N_ten_216<F32>, N_ten_217<F32>, N_ten_218<F32>, N_ten_219<F32>, N_ten_220<F32>,
        N_ten_221<F32>, N_ten_222<F32>, N_ten_223<F32>, N_ten_224<F32>, N_ten_225<F32>,
        N_ten_226<F32>, N_ten_227<F32>, N_ten_228<F32>, N_ten_229<F32>, N_ten_230<F32>,
        N_ten_231<F32>, N_ten_232<F32>, N_ten_233<F32>, N_ten_234<F32>, N_ten_235<F32>,
        N_ten_236<F32>, N_ten_237<F32>, N_ten_238<F32>, N_ten_239<F32>, N_ten_240<F32>,
        N_ten_241<F32>, N_ten_242<F32>, N_ten_243<F32>, N_ten_244<F32>, N_ten_245<F32>,
        N_ten_246<F32>, N_ten_247<F32>, N_ten_248<F32>, N_ten_249<F32>, N_ten_250<F32>,
        N_ten_251<F32>, N_ten_252<F32>, N_ten_253<F32>, N_ten_254<F32>, N_ten_255<F32>,
        N_ten_256<F32>, N_ten_257<F32>, N_ten_258<F32>, N_ten_259<F32>, N_ten_260<F32>,
        N_ten_261<F32>, N_ten_262<F32>, N_ten_263<F32>, N_ten_264<F32>, N_ten_265<F32>,
        N_ten_266<F32>, N_ten_267<F32>, N_ten_268<F32>, N_ten_269<F32>, N_ten_270<F32>,
        N_ten_271<F32>, N_ten_272<F32>, N_ten_273<F32>, N_ten_274<F32>, N_ten_275<F32>,
        N_ten_276<F32>, N_ten_277<F32>, N_ten_278<F32>, N_ten_279<F32>, N_ten_280<F32>,
        N_ten_281<F32>, N_ten_282<F32>, N_ten_283<F32>, N_ten_284<F32>, N_ten_285<F32>,
        N_ten_286<F32>, N_ten_287<F32>, N_ten_288<F32>, N_ten_289<F32>, N_ten_290<F32>,
        N_ten_291<F32>, N_ten_292<F32>, N_ten_293<F32>, N_ten_294<F32>, N_ten_295<F32>,
        N_ten_296<F32>, N_ten_297<F32>, N_ten_298<F32>, N_ten_299<F32>, N_ten_300<F32>,
        N_ten_301<F32>, N_ten_302<F32>, N_ten_303<F32>, N_ten_304<F32>, N_ten_305<F32>,
        N_ten_306<F32>, N_ten_307<F32>, N_ten_308<F32>, N_ten_309<F32>, N_ten_310<F32>,
        N_ten_311<F32>, N_ten_312<F32>, N_ten_313<F32>, N_ten_314<F32>, N_ten_315<F32>,
        N_ten_316<F32>, N_ten_317<F32>, N_ten_318<F32>, N_ten_319<F32>, N_ten_320<F32>,
        N_ten_321<F32>, N_ten_322<F32>, N_ten_323<F32>, N_ten_324<F32>, N_ten_325<F32>,
        N_ten_326<F32>, N_ten_327<F32>, N_ten_328<F32>, N_ten_329<F32>, N_ten_330<F32>,
        N_ten_331<F32>, N_ten_332<F32>, N_ten_333<F32>, N_ten_334<F32>, N_ten_335<F32>,
        N_ten_336<F32>, N_ten_337<F32>, N_ten_338<F32>, N_ten_339<F32>, N_ten_340<F32>,
        N_ten_341<F32>, N_ten_342<F32>, N_ten_343<F32>, N_ten_344<F32>, N_ten_345<F32>,
        N_ten_346<F32>, N_ten_347<F32>, N_ten_348<F32>, N_ten_349<F32>, N_ten_350<F32>,
        N_ten_351<F32>, N_ten_352<F32>, N_ten_353<F32>, N_ten_354<F32>, N_ten_355<F32>,
        N_ten_356<F32>, N_ten_357<F32>, N_ten_358<F32>, N_ten_359<F32>, N_ten_360<F32>,
        N_ten_361<F32>, N_ten_362<F32>, N_ten_363<F32>, N_ten_364<F32>, N_ten_365<F32>,
        N_ten_366<F32>, N_ten_367<F32>, N_ten_368<F32>, N_ten_369<F32>, N_ten_370<F32>,
        N_ten_371<F32>, N_ten_372<F32>, N_ten_373<F32>, N_ten_374<F32>, N_ten_375<F32>,
        N_ten_376<F32>, N_ten_377<F32>, N_ten_378<F32>, N_ten_379<F32>, N_ten_380<F32>,
        N_ten_381<F32>, N_ten_382<F32>, N_ten_383<F32>, N_ten_384<F32>, N_ten_385<F32>,
        N_ten_386<F32>, N_ten_387<F32>, N_ten_388<F32>, N_ten_389<F32>, N_ten_390<F32>,
        N_ten_391<F32>, N_ten_392<F32>, N_ten_393<F32>, N_ten_394<F32>, N_ten_395<F32>,
        N_ten_396<F32>, N_ten_397<F32>, N_ten_398<F32>, N_ten_399<F32>, N_ten_400<F32>,
        N_ten_401<F32>, N_ten_402<F32>, N_ten_403<F32>, N_ten_404<F32>, N_ten_405<F32>,
        N_ten_406<F32>, N_ten_407<F32>, N_ten_408<F32>, N_ten_409<F32>, N_ten_410<F32>,
        N_ten_411<F32>, N_ten_412<F32>, N_ten_413<F32>, N_ten_414<F32>, N_ten_415<F32>,
        N_ten_416<F32>, N_ten_417<F32>, N_ten_418<F32>, N_ten_419<F32>, N_ten_420<F32>,
        N_ten_421<F32>, N_ten_422<F32>, N_ten_423<F32>, N_ten_424<F32>, N_ten_425<F32>,
        N_ten_426<F32>, N_ten_427<F32>, N_ten_428<F32>, N_ten_429<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph(
        "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0], Bead[Leg[3, 2]], Link[2]], "
        "Strand[Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]], Strand[Link[0], Bead[Leg[1, "
        "1]], Link[1], Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",  "ten_2",  "ten_3",  "ten_4",  "ten_5",  "ten_6",  "ten_7",  "ten_8",  "ten_9",
        "ten_10", "ten_11", "ten_12", "ten_13", "ten_14", "ten_15", "ten_16", "ten_17", "ten_18",
        "ten_19", "ten_20", "ten_21", "ten_22", "ten_23", "ten_24", "ten_25", "ten_26", "ten_27",
        "ten_28", "ten_29", "ten_30", "ten_31", "ten_32", "ten_33", "ten_34", "ten_35", "ten_36",
        "ten_37", "ten_38", "ten_39", "ten_40", "ten_41", "ten_42", "ten_43", "ten_44", "ten_45"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = xIY_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        xIY_ten_1<C>,  xIY_ten_2<C>,  xIY_ten_3<C>,  xIY_ten_4<C>,  xIY_ten_5<C>,  xIY_ten_6<C>,
        xIY_ten_7<C>,  xIY_ten_8<C>,  xIY_ten_9<C>,  xIY_ten_10<C>, xIY_ten_11<C>, xIY_ten_12<C>,
        xIY_ten_13<C>, xIY_ten_14<C>, xIY_ten_15<C>, xIY_ten_16<C>, xIY_ten_17<C>, xIY_ten_18<C>,
        xIY_ten_19<C>, xIY_ten_20<C>, xIY_ten_21<C>, xIY_ten_22<C>, xIY_ten_23<C>, xIY_ten_24<C>,
        xIY_ten_25<C>, xIY_ten_26<C>, xIY_ten_27<C>, xIY_ten_28<C>, xIY_ten_29<C>, xIY_ten_30<C>,
        xIY_ten_31<C>, xIY_ten_32<C>, xIY_ten_33<C>, xIY_ten_34<C>, xIY_ten_35<C>, xIY_ten_36<C>,
        xIY_ten_37<C>, xIY_ten_38<C>, xIY_ten_39<C>, xIY_ten_40<C>, xIY_ten_41<C>, xIY_ten_42<C>,
        xIY_ten_43<C>, xIY_ten_44<C>, xIY_ten_45<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = xIY_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        xIY_ten_1<CHP>,  xIY_ten_2<CHP>,  xIY_ten_3<CHP>,  xIY_ten_4<CHP>,  xIY_ten_5<CHP>,
        xIY_ten_6<CHP>,  xIY_ten_7<CHP>,  xIY_ten_8<CHP>,  xIY_ten_9<CHP>,  xIY_ten_10<CHP>,
        xIY_ten_11<CHP>, xIY_ten_12<CHP>, xIY_ten_13<CHP>, xIY_ten_14<CHP>, xIY_ten_15<CHP>,
        xIY_ten_16<CHP>, xIY_ten_17<CHP>, xIY_ten_18<CHP>, xIY_ten_19<CHP>, xIY_ten_20<CHP>,
        xIY_ten_21<CHP>, xIY_ten_22<CHP>, xIY_ten_23<CHP>, xIY_ten_24<CHP>, xIY_ten_25<CHP>,
        xIY_ten_26<CHP>, xIY_ten_27<CHP>, xIY_ten_28<CHP>, xIY_ten_29<CHP>, xIY_ten_30<CHP>,
        xIY_ten_31<CHP>, xIY_ten_32<CHP>, xIY_ten_33<CHP>, xIY_ten_34<CHP>, xIY_ten_35<CHP>,
        xIY_ten_36<CHP>, xIY_ten_37<CHP>, xIY_ten_38<CHP>, xIY_ten_39<CHP>, xIY_ten_40<CHP>,
        xIY_ten_41<CHP>, xIY_ten_42<CHP>, xIY_ten_43<CHP>, xIY_ten_44<CHP>, xIY_ten_45<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = xIY_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        xIY_ten_1<CVHP>,  xIY_ten_2<CVHP>,  xIY_ten_3<CVHP>,  xIY_ten_4<CVHP>,  xIY_ten_5<CVHP>,
        xIY_ten_6<CVHP>,  xIY_ten_7<CVHP>,  xIY_ten_8<CVHP>,  xIY_ten_9<CVHP>,  xIY_ten_10<CVHP>,
        xIY_ten_11<CVHP>, xIY_ten_12<CVHP>, xIY_ten_13<CVHP>, xIY_ten_14<CVHP>, xIY_ten_15<CVHP>,
        xIY_ten_16<CVHP>, xIY_ten_17<CVHP>, xIY_ten_18<CVHP>, xIY_ten_19<CVHP>, xIY_ten_20<CVHP>,
        xIY_ten_21<CVHP>, xIY_ten_22<CVHP>, xIY_ten_23<CVHP>, xIY_ten_24<CVHP>, xIY_ten_25<CVHP>,
        xIY_ten_26<CVHP>, xIY_ten_27<CVHP>, xIY_ten_28<CVHP>, xIY_ten_29<CVHP>, xIY_ten_30<CVHP>,
        xIY_ten_31<CVHP>, xIY_ten_32<CVHP>, xIY_ten_33<CVHP>, xIY_ten_34<CVHP>, xIY_ten_35<CVHP>,
        xIY_ten_36<CVHP>, xIY_ten_37<CVHP>, xIY_ten_38<CVHP>, xIY_ten_39<CVHP>, xIY_ten_40<CVHP>,
        xIY_ten_41<CVHP>, xIY_ten_42<CVHP>, xIY_ten_43<CVHP>, xIY_ten_44<CVHP>, xIY_ten_45<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = xIY_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        xIY_ten_1<F32>,  xIY_ten_2<F32>,  xIY_ten_3<F32>,  xIY_ten_4<F32>,  xIY_ten_5<F32>,
        xIY_ten_6<F32>,  xIY_ten_7<F32>,  xIY_ten_8<F32>,  xIY_ten_9<F32>,  xIY_ten_10<F32>,
        xIY_ten_11<F32>, xIY_ten_12<F32>, xIY_ten_13<F32>, xIY_ten_14<F32>, xIY_ten_15<F32>,
        xIY_ten_16<F32>, xIY_ten_17<F32>, xIY_ten_18<F32>, xIY_ten_19<F32>, xIY_ten_20<F32>,
        xIY_ten_21<F32>, xIY_ten_22<F32>, xIY_ten_23<F32>, xIY_ten_24<F32>, xIY_ten_25<F32>,
        xIY_ten_26<F32>, xIY_ten_27<F32>, xIY_ten_28<F32>, xIY_ten_29<F32>, xIY_ten_30<F32>,
        xIY_ten_31<F32>, xIY_ten_32<F32>, xIY_ten_33<F32>, xIY_ten_34<F32>, xIY_ten_35<F32>,
        xIY_ten_36<F32>, xIY_ten_37<F32>, xIY_ten_38<F32>, xIY_ten_39<F32>, xIY_ten_40<F32>,
        xIY_ten_41<F32>, xIY_ten_42<F32>, xIY_ten_43<F32>, xIY_ten_44<F32>, xIY_ten_45<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0]], Strand[Link[0], "
                "Bead[Leg[1]], Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]], "
                "Strand[Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"ten_1", "ten_2", "ten_3", "ten_4",
                                                 "ten_5", "ten_6", "ten_7"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = IY_BoxTri_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        IY_BoxTri_ten_1<C>, IY_BoxTri_ten_2<C>, IY_BoxTri_ten_3<C>, IY_BoxTri_ten_4<C>,
        IY_BoxTri_ten_5<C>, IY_BoxTri_ten_6<C>, IY_BoxTri_ten_7<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = IY_BoxTri_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        IY_BoxTri_ten_1<CHP>, IY_BoxTri_ten_2<CHP>, IY_BoxTri_ten_3<CHP>, IY_BoxTri_ten_4<CHP>,
        IY_BoxTri_ten_5<CHP>, IY_BoxTri_ten_6<CHP>, IY_BoxTri_ten_7<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = IY_BoxTri_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        IY_BoxTri_ten_1<CVHP>, IY_BoxTri_ten_2<CVHP>, IY_BoxTri_ten_3<CVHP>, IY_BoxTri_ten_4<CVHP>,
        IY_BoxTri_ten_5<CVHP>, IY_BoxTri_ten_6<CVHP>, IY_BoxTri_ten_7<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = IY_BoxTri_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        IY_BoxTri_ten_1<F32>, IY_BoxTri_ten_2<F32>, IY_BoxTri_ten_3<F32>, IY_BoxTri_ten_4<F32>,
        IY_BoxTri_ten_5<F32>, IY_BoxTri_ten_6<F32>, IY_BoxTri_ten_7<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[Leg[1]], Node[]], Connection[Strand[Link[0]], "
                "Strand[Link[LoopMomentum[-2], 0], Bead[Leg[3, 2]], Link[2]], "
                "Strand[Link[LoopMomentum[-1], 0], Bead[Leg[2, 2]], Link[2]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",  "ten_2",  "ten_3",  "ten_4",  "ten_5",  "ten_6",  "ten_7",  "ten_8",
        "ten_9",  "ten_10", "ten_11", "ten_12", "ten_13", "ten_14", "ten_15", "ten_16",
        "ten_17", "ten_18", "ten_19", "ten_20", "ten_21", "ten_22", "ten_23", "ten_24",
        "ten_25", "ten_26", "ten_27", "ten_28", "ten_29", "ten_30", "ten_31", "ten_32",
        "ten_33", "ten_34", "ten_35", "ten_36", "ten_37", "ten_38", "ten_39"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = Fan_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        Fan_ten_1<C>,  Fan_ten_2<C>,  Fan_ten_3<C>,  Fan_ten_4<C>,  Fan_ten_5<C>,  Fan_ten_6<C>,
        Fan_ten_7<C>,  Fan_ten_8<C>,  Fan_ten_9<C>,  Fan_ten_10<C>, Fan_ten_11<C>, Fan_ten_12<C>,
        Fan_ten_13<C>, Fan_ten_14<C>, Fan_ten_15<C>, Fan_ten_16<C>, Fan_ten_17<C>, Fan_ten_18<C>,
        Fan_ten_19<C>, Fan_ten_20<C>, Fan_ten_21<C>, Fan_ten_22<C>, Fan_ten_23<C>, Fan_ten_24<C>,
        Fan_ten_25<C>, Fan_ten_26<C>, Fan_ten_27<C>, Fan_ten_28<C>, Fan_ten_29<C>, Fan_ten_30<C>,
        Fan_ten_31<C>, Fan_ten_32<C>, Fan_ten_33<C>, Fan_ten_34<C>, Fan_ten_35<C>, Fan_ten_36<C>,
        Fan_ten_37<C>, Fan_ten_38<C>, Fan_ten_39<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = Fan_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        Fan_ten_1<CHP>,  Fan_ten_2<CHP>,  Fan_ten_3<CHP>,  Fan_ten_4<CHP>,  Fan_ten_5<CHP>,
        Fan_ten_6<CHP>,  Fan_ten_7<CHP>,  Fan_ten_8<CHP>,  Fan_ten_9<CHP>,  Fan_ten_10<CHP>,
        Fan_ten_11<CHP>, Fan_ten_12<CHP>, Fan_ten_13<CHP>, Fan_ten_14<CHP>, Fan_ten_15<CHP>,
        Fan_ten_16<CHP>, Fan_ten_17<CHP>, Fan_ten_18<CHP>, Fan_ten_19<CHP>, Fan_ten_20<CHP>,
        Fan_ten_21<CHP>, Fan_ten_22<CHP>, Fan_ten_23<CHP>, Fan_ten_24<CHP>, Fan_ten_25<CHP>,
        Fan_ten_26<CHP>, Fan_ten_27<CHP>, Fan_ten_28<CHP>, Fan_ten_29<CHP>, Fan_ten_30<CHP>,
        Fan_ten_31<CHP>, Fan_ten_32<CHP>, Fan_ten_33<CHP>, Fan_ten_34<CHP>, Fan_ten_35<CHP>,
        Fan_ten_36<CHP>, Fan_ten_37<CHP>, Fan_ten_38<CHP>, Fan_ten_39<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = Fan_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        Fan_ten_1<CVHP>,  Fan_ten_2<CVHP>,  Fan_ten_3<CVHP>,  Fan_ten_4<CVHP>,  Fan_ten_5<CVHP>,
        Fan_ten_6<CVHP>,  Fan_ten_7<CVHP>,  Fan_ten_8<CVHP>,  Fan_ten_9<CVHP>,  Fan_ten_10<CVHP>,
        Fan_ten_11<CVHP>, Fan_ten_12<CVHP>, Fan_ten_13<CVHP>, Fan_ten_14<CVHP>, Fan_ten_15<CVHP>,
        Fan_ten_16<CVHP>, Fan_ten_17<CVHP>, Fan_ten_18<CVHP>, Fan_ten_19<CVHP>, Fan_ten_20<CVHP>,
        Fan_ten_21<CVHP>, Fan_ten_22<CVHP>, Fan_ten_23<CVHP>, Fan_ten_24<CVHP>, Fan_ten_25<CVHP>,
        Fan_ten_26<CVHP>, Fan_ten_27<CVHP>, Fan_ten_28<CVHP>, Fan_ten_29<CVHP>, Fan_ten_30<CVHP>,
        Fan_ten_31<CVHP>, Fan_ten_32<CVHP>, Fan_ten_33<CVHP>, Fan_ten_34<CVHP>, Fan_ten_35<CVHP>,
        Fan_ten_36<CVHP>, Fan_ten_37<CVHP>, Fan_ten_38<CVHP>, Fan_ten_39<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = Fan_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        Fan_ten_1<F32>,  Fan_ten_2<F32>,  Fan_ten_3<F32>,  Fan_ten_4<F32>,  Fan_ten_5<F32>,
        Fan_ten_6<F32>,  Fan_ten_7<F32>,  Fan_ten_8<F32>,  Fan_ten_9<F32>,  Fan_ten_10<F32>,
        Fan_ten_11<F32>, Fan_ten_12<F32>, Fan_ten_13<F32>, Fan_ten_14<F32>, Fan_ten_15<F32>,
        Fan_ten_16<F32>, Fan_ten_17<F32>, Fan_ten_18<F32>, Fan_ten_19<F32>, Fan_ten_20<F32>,
        Fan_ten_21<F32>, Fan_ten_22<F32>, Fan_ten_23<F32>, Fan_ten_24<F32>, Fan_ten_25<F32>,
        Fan_ten_26<F32>, Fan_ten_27<F32>, Fan_ten_28<F32>, Fan_ten_29<F32>, Fan_ten_30<F32>,
        Fan_ten_31<F32>, Fan_ten_32<F32>, Fan_ten_33<F32>, Fan_ten_34<F32>, Fan_ten_35<F32>,
        Fan_ten_36<F32>, Fan_ten_37<F32>, Fan_ten_38<F32>, Fan_ten_39<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[Link[0], Bead[Leg[1, 1]], Link[1], "
                "Bead[Leg[4, 1]], Link[LoopMomentum[-2], 0]], Strand[Link[LoopMomentum[-1], 0], "
                "Bead[Leg[2, 2]], Link[2], Bead[Leg[3, 2]], Link[0]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {"ten_1"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = BowTie_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {BowTie_ten_1<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = BowTie_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BowTie_ten_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = BowTie_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BowTie_ten_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = BowTie_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {BowTie_ten_1<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
  {
    gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[0], Bead[Leg[3, 2]], "
                "Link[2]], Strand[Link[LoopMomentum[-1]], Bead[Leg[2, 2]], Link[2]], "
                "Strand[Link[0], Bead[Leg[1]], Link[LoopMomentum[-2], 0]]]]");
    MasterTermData mdata;
    std::get<lGraph::xGraph>(mdata) = graph;
    std::get<std::vector<std::string>>(mdata) = {
        "ten_1",  "ten_2",  "ten_3",  "ten_4",  "ten_5",  "ten_6",  "ten_7",
        "ten_8",  "ten_9",  "ten_10", "ten_11", "ten_12", "ten_13", "ten_14",
        "ten_15", "ten_16", "ten_17", "ten_18", "ten_19", "ten_20", "ten_21"};
    std::get<MasterCoordinatesFP<C, 6>>(mdata) = xIY_BoxBox_coordinates<C, 6>;
    std::get<std::vector<BasisFunction<C>>>(mdata) = {
        xIY_BoxBox_ten_1<C>,  xIY_BoxBox_ten_2<C>,  xIY_BoxBox_ten_3<C>,  xIY_BoxBox_ten_4<C>,
        xIY_BoxBox_ten_5<C>,  xIY_BoxBox_ten_6<C>,  xIY_BoxBox_ten_7<C>,  xIY_BoxBox_ten_8<C>,
        xIY_BoxBox_ten_9<C>,  xIY_BoxBox_ten_10<C>, xIY_BoxBox_ten_11<C>, xIY_BoxBox_ten_12<C>,
        xIY_BoxBox_ten_13<C>, xIY_BoxBox_ten_14<C>, xIY_BoxBox_ten_15<C>, xIY_BoxBox_ten_16<C>,
        xIY_BoxBox_ten_17<C>, xIY_BoxBox_ten_18<C>, xIY_BoxBox_ten_19<C>, xIY_BoxBox_ten_20<C>,
        xIY_BoxBox_ten_21<C>};
#ifdef HIGH_PRECISION
    std::get<MasterCoordinatesFP<CHP, 6>>(mdata) = xIY_BoxBox_coordinates<CHP, 6>;
    std::get<std::vector<BasisFunction<CHP>>>(mdata) = {
        xIY_BoxBox_ten_1<CHP>,  xIY_BoxBox_ten_2<CHP>,  xIY_BoxBox_ten_3<CHP>,
        xIY_BoxBox_ten_4<CHP>,  xIY_BoxBox_ten_5<CHP>,  xIY_BoxBox_ten_6<CHP>,
        xIY_BoxBox_ten_7<CHP>,  xIY_BoxBox_ten_8<CHP>,  xIY_BoxBox_ten_9<CHP>,
        xIY_BoxBox_ten_10<CHP>, xIY_BoxBox_ten_11<CHP>, xIY_BoxBox_ten_12<CHP>,
        xIY_BoxBox_ten_13<CHP>, xIY_BoxBox_ten_14<CHP>, xIY_BoxBox_ten_15<CHP>,
        xIY_BoxBox_ten_16<CHP>, xIY_BoxBox_ten_17<CHP>, xIY_BoxBox_ten_18<CHP>,
        xIY_BoxBox_ten_19<CHP>, xIY_BoxBox_ten_20<CHP>, xIY_BoxBox_ten_21<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
    std::get<MasterCoordinatesFP<CVHP, 6>>(mdata) = xIY_BoxBox_coordinates<CVHP, 6>;
    std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {
        xIY_BoxBox_ten_1<CVHP>,  xIY_BoxBox_ten_2<CVHP>,  xIY_BoxBox_ten_3<CVHP>,
        xIY_BoxBox_ten_4<CVHP>,  xIY_BoxBox_ten_5<CVHP>,  xIY_BoxBox_ten_6<CVHP>,
        xIY_BoxBox_ten_7<CVHP>,  xIY_BoxBox_ten_8<CVHP>,  xIY_BoxBox_ten_9<CVHP>,
        xIY_BoxBox_ten_10<CVHP>, xIY_BoxBox_ten_11<CVHP>, xIY_BoxBox_ten_12<CVHP>,
        xIY_BoxBox_ten_13<CVHP>, xIY_BoxBox_ten_14<CVHP>, xIY_BoxBox_ten_15<CVHP>,
        xIY_BoxBox_ten_16<CVHP>, xIY_BoxBox_ten_17<CVHP>, xIY_BoxBox_ten_18<CVHP>,
        xIY_BoxBox_ten_19<CVHP>, xIY_BoxBox_ten_20<CVHP>, xIY_BoxBox_ten_21<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
    std::get<MasterCoordinatesFP<F32, 6>>(mdata) = xIY_BoxBox_coordinates<F32, 6>;
    std::get<std::vector<BasisFunction<F32>>>(mdata) = {
        xIY_BoxBox_ten_1<F32>,  xIY_BoxBox_ten_2<F32>,  xIY_BoxBox_ten_3<F32>,
        xIY_BoxBox_ten_4<F32>,  xIY_BoxBox_ten_5<F32>,  xIY_BoxBox_ten_6<F32>,
        xIY_BoxBox_ten_7<F32>,  xIY_BoxBox_ten_8<F32>,  xIY_BoxBox_ten_9<F32>,
        xIY_BoxBox_ten_10<F32>, xIY_BoxBox_ten_11<F32>, xIY_BoxBox_ten_12<F32>,
        xIY_BoxBox_ten_13<F32>, xIY_BoxBox_ten_14<F32>, xIY_BoxBox_ten_15<F32>,
        xIY_BoxBox_ten_16<F32>, xIY_BoxBox_ten_17<F32>, xIY_BoxBox_ten_18<F32>,
        xIY_BoxBox_ten_19<F32>, xIY_BoxBox_ten_20<F32>, xIY_BoxBox_ten_21<F32>};
#endif
    master_term_data_holder[std::get<lGraph::xGraph>(mdata).get_topology()] = std::move(mdata);
  }
}

} // namespace MassiveGravity
} // namespace MasterIntegrands
} // namespace FunctionSpace
} // namespace Caravel
