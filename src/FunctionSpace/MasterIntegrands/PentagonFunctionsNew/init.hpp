/*  
THIS FILE IS GENERATED AUTOMATICALLY

by submodules/SurfaceTerms/CaravelGraph/Integrands.m

  */

#include "FunctionSpace/FunctionSpace.h"

#include "FunctionSpace/MasterIntegrands.h"

namespace Caravel{
namespace FunctionSpace{
namespace MasterIntegrands{
namespace PentagonFunctionsNew{

using namespace MasterIntegrands;

template <typename T, size_t D> std::vector<T> Sunrise_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {sq(mc[1])};
}

template <typename T> std::vector<T> Sunrise_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(-1) / T(16)) * (T(1) / abb[0]);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(960) + d * (T(-1208) + d * (T(566) + d * (T(-117) + T(9) * d)));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> TriangleBubble1_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {};
}

template <typename T> std::vector<T> TriangleBubble1_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
T expr = T(1) / T(16);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(480) + d * (T(-544) + d * (T(230) + d * (T(-43) + T(3) * d)));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> TriangleBubble2_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {};
}

template <typename T> std::vector<T> TriangleBubble2_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
T expr = T(1) / T(16);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(480) + d * (T(-544) + d * (T(230) + d * (T(-43) + T(3) * d)));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> TriangleTriangle_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {mc[1] * mc[2]};
}

template <typename T> std::vector<T> TriangleTriangle_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(1) / T(8)) * abb[0];
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> BoxBubble1_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {mc[1] * mc[2]};
}

template <typename T> std::vector<T> BoxBubble1_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(-1) / T(16)) * abb[0];
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(192) + d * (T(-208) + d * (T(84) + (T(-15) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> BoxBubble2_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {mc[1] * mc[2], sq(mc[1])};
}

template <typename T> std::vector<T> BoxBubble2_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(1) / T(32)) * (T(2) * abb[0] + abb[1]);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(192) + d * (T(-208) + d * (T(84) + (T(-15) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> TriangleTriangle2MastersDot_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {mc[1] * mc[2], mc[1] * mc[3], mc[2] * mc[3], sq(l[0]), sq(l[1]), sq(-mc[1] + l[1]), sq(-mc[2] + l[0]), sq(l[1] + mc[4])};
}

template <typename T> std::vector<T> TriangleTriangle2MastersDot_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(1) / T(8)) * (T(-1) * abb[0] + T(-1) * abb[2]);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T> std::array<T,5> TriangleTriangle2MastersDot_2(const TopologyCoordinates<T>& cs)
{
const auto& abb = cs.special;
return {T(-72) * abb[0] + T(-112) * abb[1] + T(-72) * abb[2] + T(30) * (T(1) / abb[0]) * (T(1) / abb[2]) * (abb[0] * (abb[0] + abb[1]) + T(-1) * (T(3) * abb[0] + abb[1]) * abb[2]) * abb[3] + T(-20) * (T(1) / abb[0]) * (T(1) / abb[2]) * prod_pow(abb[0] + abb[2], 2) * abb[5] + T(-30) * (T(1) / abb[0]) * (abb[0] + abb[1]) * (abb[0] + T(-1) * abb[2]) * (T(1) / abb[2]) * abb[6] + T(-20) * (T(1) / abb[0]) * abb[4] * abb[6] + T(40) * (T(1) / abb[2]) * abb[4] * abb[6] + T(-50) * (T(1) / abb[0]) * abb[3] * abb[7] + T(-20) * (T(1) / abb[2]) * abb[3] * abb[7], T(74) * abb[0] + T(116) * abb[1] + T(74) * abb[2] + T(-34) * (T(1) / abb[0]) * (T(1) / abb[2]) * (abb[0] * (abb[0] + abb[1]) + T(-1) * (T(3) * abb[0] + abb[1]) * abb[2]) * abb[3] + T(21) * (T(1) / abb[0]) * (T(1) / abb[2]) * prod_pow(abb[0] + abb[2], 2) * abb[5] + T(34) * (T(1) / abb[0]) * (abb[0] + abb[1]) * (abb[0] + T(-1) * abb[2]) * (T(1) / abb[2]) * abb[6] + (T(47) / T(2)) * (T(1) / abb[0]) * abb[4] * abb[6] + T(-52) * (T(1) / abb[2]) * abb[4] * abb[6] + (T(245) / T(4)) * (T(1) / abb[0]) * abb[3] * abb[7] + (T(47) / T(2)) * (T(1) / abb[2]) * abb[3] * abb[7], (T(-57) / T(2)) * abb[0] + T(-45) * abb[1] + (T(-57) / T(2)) * abb[2] + (T(115) / T(8)) * (T(1) / abb[0]) * (T(1) / abb[2]) * (abb[0] * (abb[0] + abb[1]) + T(-1) * (T(3) * abb[0] + abb[1]) * abb[2]) * abb[3] + (T(-33) / T(4)) * (T(1) / abb[0]) * (T(1) / abb[2]) * prod_pow(abb[0] + abb[2], 2) * abb[5] + (T(-115) / T(8)) * (T(1) / abb[0]) * (abb[0] + abb[1]) * (abb[0] + T(-1) * abb[2]) * (T(1) / abb[2]) * abb[6] + (T(-41) / T(4)) * (T(1) / abb[0]) * abb[4] * abb[6] + (T(201) / T(8)) * (T(1) / abb[2]) * abb[4] * abb[6] + (T(-447) / T(16)) * (T(1) / abb[0]) * abb[3] * abb[7] + (T(-41) / T(4)) * (T(1) / abb[2]) * abb[3] * abb[7], (T(39) / T(8)) * abb[0] + (T(31) / T(4)) * abb[1] + (T(39) / T(8)) * abb[2] + (T(-43) / T(16)) * (T(1) / abb[0]) * (T(1) / abb[2]) * (abb[0] * (abb[0] + abb[1]) + T(-1) * (T(3) * abb[0] + abb[1]) * abb[2]) * abb[3] + (T(23) / T(16)) * (T(1) / abb[0]) * (T(1) / abb[2]) * prod_pow(abb[0] + abb[2], 2) * abb[5] + (T(43) / T(16)) * (T(1) / abb[0]) * (abb[0] + abb[1]) * (abb[0] + T(-1) * abb[2]) * (T(1) / abb[2]) * abb[6] + (T(63) / T(32)) * (T(1) / abb[0]) * abb[4] * abb[6] + (T(-171) / T(32)) * (T(1) / abb[2]) * abb[4] * abb[6] + (T(45) / T(8)) * (T(1) / abb[0]) * abb[3] * abb[7] + (T(63) / T(32)) * (T(1) / abb[2]) * abb[3] * abb[7], (T(-5) / T(16)) * abb[0] + (T(-1) / T(2)) * abb[1] + (T(-5) / T(16)) * abb[2] + (T(3) / T(16)) * (T(1) / abb[0]) * (T(1) / abb[2]) * (abb[0] * (abb[0] + abb[1]) + T(-1) * (T(3) * abb[0] + abb[1]) * abb[2]) * abb[3] + (T(-3) / T(32)) * (T(1) / abb[0]) * (T(1) / abb[2]) * prod_pow(abb[0] + abb[2], 2) * abb[5] + (T(-3) / T(16)) * (T(1) / abb[0]) * (abb[0] + abb[1]) * (abb[0] + T(-1) * abb[2]) * (T(1) / abb[2]) * abb[6] + (T(-9) / T(64)) * (T(1) / abb[0]) * abb[4] * abb[6] + (T(27) / T(64)) * (T(1) / abb[2]) * abb[4] * abb[6] + (T(-27) / T(64)) * (T(1) / abb[0]) * abb[3] * abb[7] + (T(-9) / T(64)) * (T(1) / abb[2]) * abb[3] * abb[7]};
}

template <typename T, size_t D> std::vector<T> TriangleTriangleGeneric1Master_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {mc[1] * mc[2], mc[2] * mc[3]};
}

template <typename T> std::vector<T> TriangleTriangleGeneric1Master_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(1) / T(8)) * (abb[0] + abb[1]);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> BoxTriangleSemisimple_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {mc[1] * mc[2], mc[2] * mc[3], sq(mc[2])};
}

template <typename T> std::vector<T> BoxTriangleSemisimple_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(-1) / T(8)) * abb[1] * (T(2) * abb[0] + abb[2]);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> BoxBoxSimple_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {mc[1] * mc[2], mc[2] * mc[3], sq(mc[1]), sq(l[0] + mc[4])};
}

template <typename T> std::vector<T> BoxBoxSimple_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(-1) / T(8)) * abb[0] * (T(2) * abb[0] + abb[2]) * abb[3];
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T> std::vector<T> BoxBoxSimple_2(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(-1) / T(8)) * abb[1] * prod_pow(abb[2] + abb[0] * T(2), 2);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> PentagonBubble_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {levi(mc[1], mc[2], mc[3], mc[4]), mc[1] * mc[2], mc[1] * mc[3], mc[2] * mc[3], mc[2] * mc[4], mc[3] * mc[4], sq(l[0]), sq(-mc[1] + l[0]), sq(-mc[1] + -mc[2] + l[0]), sq(l[0] + mc[5]), sq(l[0] + mc[4] + mc[5])};
}

template <typename T> std::vector<T> PentagonBubble_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(1) / T(16)) * abb[1] * abb[3];
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(192) + d * (T(-208) + d * (T(84) + (T(-15) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T> std::vector<T> PentagonBubble_2(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(1) / T(64)) * (T(1) / abb[0]) * (T(2) * prod_pow(abb[1], 2) * prod_pow(abb[3], 2) + T(2) * abb[1] * abb[2] * prod_pow(abb[3], 2) + T(2) * abb[1] * prod_pow(abb[3], 3) + T(2) * abb[1] * abb[2] * abb[3] * abb[4] + T(2) * abb[1] * prod_pow(abb[3], 2) * abb[4] + T(2) * prod_pow(abb[1], 2) * abb[3] * abb[5] + T(4) * abb[1] * abb[2] * abb[3] * abb[5] + T(2) * abb[1] * prod_pow(abb[3], 2) * abb[5] + T(-1) * abb[1] * prod_pow(abb[3], 2) * abb[6] + T(-1) * abb[2] * prod_pow(abb[3], 2) * abb[6] + T(-1) * prod_pow(abb[3], 3) * abb[6] + T(-1) * abb[2] * abb[3] * abb[4] * abb[6] + T(-1) * prod_pow(abb[3], 2) * abb[4] * abb[6] + T(-1) * abb[1] * abb[3] * abb[5] * abb[6] + T(-1) * prod_pow(abb[3], 2) * abb[5] * abb[6] + abb[1] * abb[2] * abb[3] * abb[7] + prod_pow(abb[2], 2) * abb[3] * abb[7] + abb[1] * prod_pow(abb[3], 2) * abb[7] + T(2) * abb[2] * prod_pow(abb[3], 2) * abb[7] + prod_pow(abb[3], 3) * abb[7] + prod_pow(abb[2], 2) * abb[4] * abb[7] + T(2) * abb[2] * abb[3] * abb[4] * abb[7] + prod_pow(abb[3], 2) * abb[4] * abb[7] + T(-1) * abb[1] * abb[2] * abb[5] * abb[7] + abb[1] * abb[3] * abb[5] * abb[7] + abb[2] * abb[3] * abb[5] * abb[7] + prod_pow(abb[3], 2) * abb[5] * abb[7] + prod_pow(abb[1], 2) * abb[3] * abb[8] + T(-1) * prod_pow(abb[2], 2) * abb[3] * abb[8] + abb[1] * prod_pow(abb[3], 2) * abb[8] + T(-1) * abb[2] * prod_pow(abb[3], 2) * abb[8] + T(-1) * abb[1] * abb[2] * abb[4] * abb[8] + T(-1) * prod_pow(abb[2], 2) * abb[4] * abb[8] + abb[1] * abb[3] * abb[4] * abb[8] + T(-1) * abb[2] * abb[3] * abb[4] * abb[8] + prod_pow(abb[1], 2) * abb[5] * abb[8] + abb[1] * abb[2] * abb[5] * abb[8] + abb[1] * abb[3] * abb[5] * abb[8] + T(-1) * abb[2] * abb[3] * abb[5] * abb[8] + T(-2) * abb[1] * abb[2] * abb[3] * abb[9] + T(-1) * prod_pow(abb[1], 2) * abb[3] * abb[10] + abb[1] * abb[2] * abb[3] * abb[10] + T(-1) * abb[1] * prod_pow(abb[3], 2) * abb[10] + abb[1] * abb[2] * abb[4] * abb[10] + T(-1) * abb[1] * abb[3] * abb[4] * abb[10] + T(-1) * prod_pow(abb[1], 2) * abb[5] * abb[10] + T(-1) * abb[1] * abb[3] * abb[5] * abb[10]);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(192) + d * (T(-208) + d * (T(84) + (T(-15) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> BoxTriangleGeneric_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {levi(mc[1], mc[2], mc[3], mc[4]), mc[1] * mc[2], mc[1] * mc[3], mc[2] * mc[3], mc[2] * mc[4], mc[3] * mc[4], sq(l[0]), sq(l[1]), sq(-mc[1] + l[1]), sq(-mc[2] + l[0]), sq(l[1] + mc[5]), sq(l[1] + mc[4] + mc[5])};
}

template <typename T> std::vector<T> BoxTriangleGeneric_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(-1) / T(4)) * (abb[1] + abb[2] + abb[3]) * (abb[3] + abb[4]);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T> T BoxTriangleGeneric_2(const TopologyCoordinates<T>& cs, const T& d)
{
const auto& abb = cs.special;
return (T(1) / T(8)) * prod_pow(d + T(-4), 4) * (T(1) / abb[0]) * (T(2) * (abb[1] + abb[2] + abb[3]) * ((abb[3] + abb[4]) * ((abb[2] + abb[3]) * (abb[3] + abb[4]) + abb[3] * abb[5]) + abb[1] * (prod_pow(abb[3], 2) + T(-1) * abb[4] * abb[5] + abb[3] * (abb[4] + abb[5]))) + T(-2) * (T(1) / (d + T(-4))) * (T(-3) + d) * (abb[1] + abb[2] + abb[3]) * (abb[2] * (abb[3] + abb[4]) + (abb[3] + T(2) * abb[4]) * (abb[3] + abb[4] + abb[5]) + abb[1] * (abb[3] + T(2) * abb[4] + abb[5])) * abb[6] + T(-1) * abb[4] * ((abb[2] + abb[3]) * (abb[3] + abb[4]) + abb[1] * (abb[3] + T(-1) * abb[5]) + T(-1) * abb[3] * abb[5]) * abb[7] + T(2) * (abb[1] + abb[2] + abb[3]) * abb[4] * (abb[1] + abb[3] + abb[4]) * abb[8] + T(2) * (T(1) / (d + T(-4))) * (T(-3) + d) * (abb[1] + abb[2] + abb[3]) * (abb[2] * (abb[3] + abb[4]) + (abb[3] + T(2) * abb[4]) * (abb[3] + abb[4] + abb[5]) + abb[1] * (abb[3] + T(2) * abb[4] + abb[5])) * abb[9] + (prod_pow(abb[1], 2) * (abb[3] + abb[5]) + abb[3] * ((abb[2] + abb[3]) * (abb[3] + abb[4]) + abb[3] * abb[5]) + abb[1] * (abb[2] * (abb[3] + T(-1) * abb[4]) + abb[3] * (T(2) * abb[3] + abb[4] + T(2) * abb[5]))) * abb[10] + T(-1) * (abb[1] + abb[3] + abb[4]) * ((abb[2] + abb[3]) * (abb[3] + abb[4]) + abb[3] * abb[5] + abb[1] * (abb[3] + T(2) * abb[4] + abb[5])) * abb[11]);
}

template <typename T, size_t D> std::vector<T> BoxBoxSemiSimple_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {levi(mc[1], mc[2], mc[3], mc[4]), mu(l[0], l[1]), mc[1] * mc[2], mc[1] * mc[3], mc[2] * mc[3], mc[2] * mc[4], mc[3] * mc[4], sq(l[0] + mc[5])};
}

template <typename T> std::vector<T> BoxBoxSemiSimple_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(1) / T(4)) * abb[2] * (abb[2] + abb[3] + abb[4]) * abb[7];
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T> std::vector<T> BoxBoxSemiSimple_2(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(-1) / T(2)) * abb[2] * (abb[2] + abb[3] + abb[4]) * (abb[4] + abb[5] + abb[6]);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T> T BoxBoxSemiSimple_3(const TopologyCoordinates<T>& cs)
{
const auto& abb = cs.special;
return abb[0] * abb[1];
}

template <typename T, size_t D> std::vector<T> PentagonBox_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {levi(mc[1], mc[2], mc[3], mc[4]), mu(l[0], l[1]), mc.s(4, 5), mc[1] * mc[2], mc[1] * mc[3], mc[2] * mc[3], sq(l[0] + mc[5])};
}

template <typename T> std::vector<T> PentagonBox_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(-1) / T(2)) * abb[3] * abb[5] * (abb[3] + abb[4] + abb[5]) * abb[6];
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(256) + d * (T(-256) + d * (T(96) + (T(-16) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T> T PentagonBox_2(const TopologyCoordinates<T>& cs)
{
const auto& abb = cs.special;
return abb[0] * abb[1] * abb[2];
}

template <typename T> T PentagonBox_3(const TopologyCoordinates<T>& cs)
{
const auto& abb = cs.special;
return abb[0] * abb[1] * abb[6];
}

template <typename T, size_t D> std::vector<T> FactorizedBubbleBubble_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {};
}

template <typename T> std::vector<T> FactorizedBubbleBubble_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
T expr = T(1) / T(16);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(144) + d * (T(-168) + d * (T(73) + (T(-14) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> FactorizedBubbleBubbleGeneric_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {};
}

template <typename T> std::vector<T> FactorizedBubbleBubbleGeneric_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
T expr = T(1) / T(4);
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(144) + d * (T(-168) + d * (T(73) + (T(-14) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

template <typename T, size_t D> std::vector<T> FactorizedBoxBubble_coordinates(const OnShellPoint<T,D>& l, const momD_conf<T,D>& mc)
{
return {mc[1] * mc[2], mc[2] * mc[3]};
}

template <typename T> std::vector<T> FactorizedBoxBubble_1(const TopologyCoordinates<T>& cs, const std::vector<T>& d_values)
{
const auto& abb = cs.special;
T expr = (T(1) / T(16)) * abb[0] * abb[1];
std::vector<T> result(d_values.size(), expr);
auto di = [](const T& d){return T(192) + d * (T(-208) + d * (T(84) + (T(-15) + d) * d));};
for( size_t i = 0; i < d_values.size(); i++)
{
result[i] *= di(d_values[i]);
}
return result;
}

inline void load()
{
using gType = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;
{
gType graph("CaravelGraph[Nodes[Node[Leg[2]], Node[Leg[1]]], Connection[Strand[Link[]], Strand[Link[LoopMomentum[-1]]], Strand[Link[LoopMomentum[-2]]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = Sunrise_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {Sunrise_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = Sunrise_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {Sunrise_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = Sunrise_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {Sunrise_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = Sunrise_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {Sunrise_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2, 0]]], Connection[Strand[Link[]], Strand[Link[LoopMomentum[-1]]], Strand[Link[LoopMomentum[-2]], Bead[Leg[3]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = TriangleBubble1_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {TriangleBubble1_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = TriangleBubble1_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {TriangleBubble1_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = TriangleBubble1_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {TriangleBubble1_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = TriangleBubble1_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {TriangleBubble1_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[3, 0]], Node[Leg[2, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1]], Link[]], Strand[Link[LoopMomentum[2]]], Strand[Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = TriangleBubble2_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {TriangleBubble2_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = TriangleBubble2_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {TriangleBubble2_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = TriangleBubble2_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {TriangleBubble2_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = TriangleBubble2_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {TriangleBubble2_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1, 0]], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[3]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = TriangleTriangle_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {TriangleTriangle_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = TriangleTriangle_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {TriangleTriangle_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = TriangleTriangle_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {TriangleTriangle_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = TriangleTriangle_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {TriangleTriangle_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[4, 0]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2]], Link[]], Strand[Link[LoopMomentum[2]]], Strand[Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = BoxBubble1_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxBubble1_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = BoxBubble1_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxBubble1_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = BoxBubble1_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxBubble1_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = BoxBubble1_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxBubble1_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2, 0]]], Connection[Strand[Link[]], Strand[Link[LoopMomentum[-1]]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[], Bead[Leg[3, 0]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = BoxBubble2_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxBubble2_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = BoxBubble2_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxBubble2_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = BoxBubble2_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxBubble2_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = BoxBubble2_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxBubble2_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1", "2"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = TriangleTriangle2MastersDot_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {TriangleTriangle2MastersDot_1<C>, TriangleTriangle2MastersDot_2<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = TriangleTriangle2MastersDot_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {TriangleTriangle2MastersDot_1<CHP>, TriangleTriangle2MastersDot_2<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = TriangleTriangle2MastersDot_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {TriangleTriangle2MastersDot_1<CVHP>, TriangleTriangle2MastersDot_2<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = TriangleTriangle2MastersDot_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {TriangleTriangle2MastersDot_1<F32>, TriangleTriangle2MastersDot_2<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = TriangleTriangleGeneric1Master_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {TriangleTriangleGeneric1Master_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = TriangleTriangleGeneric1Master_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {TriangleTriangleGeneric1Master_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = TriangleTriangleGeneric1Master_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {TriangleTriangleGeneric1Master_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = TriangleTriangleGeneric1Master_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {TriangleTriangleGeneric1Master_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1, 0]], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[], Bead[Leg[3, 0]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = BoxTriangleSemisimple_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxTriangleSemisimple_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = BoxTriangleSemisimple_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxTriangleSemisimple_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = BoxTriangleSemisimple_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxTriangleSemisimple_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = BoxTriangleSemisimple_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxTriangleSemisimple_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1]], Link[], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[], Bead[Leg[3, 0]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1", "2"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = BoxBoxSimple_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxBoxSimple_1<C>, BoxBoxSimple_2<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = BoxBoxSimple_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxBoxSimple_1<CHP>, BoxBoxSimple_2<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = BoxBoxSimple_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxBoxSimple_1<CVHP>, BoxBoxSimple_2<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = BoxBoxSimple_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxBoxSimple_1<F32>, BoxBoxSimple_2<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[5, 0]], Node[Leg[4, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[LoopMomentum[2]]], Strand[Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1", "2"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = PentagonBubble_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {PentagonBubble_1<C>, PentagonBubble_2<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = PentagonBubble_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {PentagonBubble_1<CHP>, PentagonBubble_2<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = PentagonBubble_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {PentagonBubble_1<CVHP>, PentagonBubble_2<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = PentagonBubble_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {PentagonBubble_1<F32>, PentagonBubble_2<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1", "2"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = BoxTriangleGeneric_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxTriangleGeneric_1<C>, BoxTriangleGeneric_2<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = BoxTriangleGeneric_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxTriangleGeneric_1<CHP>, BoxTriangleGeneric_2<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = BoxTriangleGeneric_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxTriangleGeneric_1<CVHP>, BoxTriangleGeneric_2<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = BoxTriangleGeneric_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxTriangleGeneric_1<F32>, BoxTriangleGeneric_2<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1", "2", "3"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = BoxBoxSemiSimple_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {BoxBoxSemiSimple_1<C>, BoxBoxSemiSimple_2<C>, BoxBoxSemiSimple_3<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = BoxBoxSemiSimple_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {BoxBoxSemiSimple_1<CHP>, BoxBoxSemiSimple_2<CHP>, BoxBoxSemiSimple_3<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = BoxBoxSemiSimple_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {BoxBoxSemiSimple_1<CVHP>, BoxBoxSemiSimple_2<CVHP>, BoxBoxSemiSimple_3<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = BoxBoxSemiSimple_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {BoxBoxSemiSimple_1<F32>, BoxBoxSemiSimple_2<F32>, BoxBoxSemiSimple_3<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1", "2", "3"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = PentagonBox_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {PentagonBox_1<C>, PentagonBox_2<C>, PentagonBox_3<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = PentagonBox_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {PentagonBox_1<CHP>, PentagonBox_2<CHP>, PentagonBox_3<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = PentagonBox_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {PentagonBox_1<CVHP>, PentagonBox_2<CVHP>, PentagonBox_3<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = PentagonBox_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {PentagonBox_1<F32>, PentagonBox_2<F32>, PentagonBox_3<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1]], Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[2]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = FactorizedBubbleBubble_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {FactorizedBubbleBubble_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = FactorizedBubbleBubble_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {FactorizedBubbleBubble_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = FactorizedBubbleBubble_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {FactorizedBubbleBubble_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = FactorizedBubbleBubble_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {FactorizedBubbleBubble_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[3]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = FactorizedBubbleBubbleGeneric_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {FactorizedBubbleBubbleGeneric_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = FactorizedBubbleBubbleGeneric_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {FactorizedBubbleBubbleGeneric_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = FactorizedBubbleBubbleGeneric_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {FactorizedBubbleBubbleGeneric_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = FactorizedBubbleBubbleGeneric_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {FactorizedBubbleBubbleGeneric_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
{
gType graph("CaravelGraph[Nodes[Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4]], Link[]]]]");
MasterTermData mdata;
std::get<lGraph::xGraph>(mdata) = graph;
std::get<std::string>(mdata) = "Integrands`Private`grName";
std::get<std::vector<std::string>>(mdata) = {"1"};
std::get<MasterCoordinatesFP<C,6>>(mdata) = FactorizedBoxBubble_coordinates<C,6>;
std::get<std::vector<BasisFunction<C>>>(mdata) = {FactorizedBoxBubble_1<C>};
#ifdef HIGH_PRECISION
std::get<MasterCoordinatesFP<CHP,6>>(mdata) = FactorizedBoxBubble_coordinates<CHP,6>;
std::get<std::vector<BasisFunction<CHP>>>(mdata) = {FactorizedBoxBubble_1<CHP>};
#endif
#ifdef VERY_HIGH_PRECISION
std::get<MasterCoordinatesFP<CVHP,6>>(mdata) = FactorizedBoxBubble_coordinates<CVHP,6>;
std::get<std::vector<BasisFunction<CVHP>>>(mdata) = {FactorizedBoxBubble_1<CVHP>};
#endif
#ifdef USE_FINITE_FIELDS
std::get<MasterCoordinatesFP<F32,6>>(mdata) = FactorizedBoxBubble_coordinates<F32,6>;
std::get<std::vector<BasisFunction<F32>>>(mdata) = {FactorizedBoxBubble_1<F32>};
#endif
master_term_data_holder[graph.filter()] = std::move(mdata);
}
}

}}}}

