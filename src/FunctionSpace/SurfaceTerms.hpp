#ifndef SURFACETERMS_H_INC
#define SURFACETERMS_H_INC

#include "Core/typedefs.h"

#include "IBPhelper.h"

#include "Core/enumerate.h"


namespace Caravel {
namespace FunctionSpace {

namespace SurfaceTerms {

inline size_t common_transverse_count(const TensorInsertion& t, size_t common_dim) {
    auto& fourD_part = t.first;
    size_t transverse_count = 0;

    for (auto loop : fourD_part) { transverse_count += loop.at(common_dim); }

    return transverse_count;
}

// This function takes a tensor insertion which can be tracelessly completed
// (i.e. one with an even common transverse rank) and builds a function which
// vanishes upon integration due to symmetry in the D-Dimensional common transverse space.
template <typename T> BasisFunction<T> d_dimensional_traceless_completion(const TensorInsertion& tensor, size_t num_common) {

    if (num_common > 3) {
        return BasisFunction<T>([](const TopologyCoordinates<T>& coords, T d) {
            std::cout << "ERROR: Attempt to evaluate invalid BasisFunction as:" << std::endl
                      << "D_dimensional_traceless_completion only implemented for num_common={1,2,3}." << std::endl;
            exit(1);
            return T(0);
        });
    }

    auto& fourD_part = tensor.first;
    if (fourD_part.size() != 2) {
        std::cout << "ERROR: D_dimensional_traceless_completion only implemented for L=2." << std::endl;
        exit(1);
    }

    size_t common_direction = 0;
    if (num_common >= 2 && common_transverse_count(tensor, 1) != 0) { common_direction = 1; }
    else if (num_common == 3 && common_transverse_count(tensor, 2) != 0) {
        common_direction = 2;
    }

    auto& l1_part = fourD_part.at(0);
    auto& l2_part = fourD_part.at(1);

    int num_left_c = l1_part.at(common_direction);
    int num_right_c = l2_part.at(common_direction);

    if (num_left_c > 0   ) {
        auto mu2_part = tensor;
        auto mu12_part = tensor;
        if (num_left_c > 1) {
            mu2_part.first.at(0).decrease_by(common_direction, 2);
            mu2_part.second.at(0) = 1;
        }

        if (num_right_c > 0) {
            mu12_part.first.at(0).decrease_by(common_direction, 1);
            mu12_part.first.at(1).decrease_by(common_direction, 1);
            mu12_part.second.at(2) = 1;
        }
        return BasisFunction<T>([tensor, mu2_part, mu12_part, num_left_c, num_right_c](const TopologyCoordinates<T>& coords) {
            auto tensor_value = evaluate_tensor_insertion(tensor, coords);

            auto mu2_value = evaluate_tensor_insertion(mu2_part, coords);
            auto mu12_value = evaluate_tensor_insertion(mu12_part, coords);

            auto constant = -(T(4) * tensor_value + (T(num_left_c) - T(1)) * mu2_value + T(num_right_c) * mu12_value);
            auto linear = tensor_value;

            return std::make_pair(constant, linear);
        });
    }

    if ((num_left_c == 0 && num_right_c >= 2) ) {

        auto mu2_part = tensor;
        mu2_part.first.at(1).decrease_by(common_direction, 2);
        mu2_part.second.at(1) = 1;
        return BasisFunction<T>([tensor, mu2_part, num_right_c](const TopologyCoordinates<T>& coords) {
            auto tensor_value = evaluate_tensor_insertion(tensor, coords);
            auto mu2_value = evaluate_tensor_insertion(mu2_part, coords);

            auto constant = -(T(4) * tensor_value + (T(num_right_c) - T(1)) * mu2_value);
            auto linear = tensor_value;

            return std::make_pair(constant, linear);
        });
    }  

    std::cout << "ERROR: Unhandled case in d_dimensional_traceless_completion: " << tensor << ". common_direction: " << common_direction << std::endl;
    exit(1);
}

template <typename T, size_t SIZE> std::vector<BasisFunction<T>> IBP_array_to_vector(const std::array<SurfaceTermsFunctionPointer<T>, SIZE>& IBP_functions) {

    std::vector<BasisFunction<T>> deletable_list;

    for (auto& expression : IBP_functions) {
        if (expression.second < 0) {
            deletable_list.push_back(BasisFunction<T>([fp = expression.first](const TopologyCoordinates<T>& coords) {
                auto& props = coords.propagators;
                auto& invariants = coords.invariants;
                auto& vector_components = coords.vector_components;
                return (*fp)(&invariants[0], &props[0], vector_components, nullptr);
            }));
        }
        else {
            deletable_list.push_back(BasisFunction<T>([fp = expression.first, bi = expression.second](const TopologyCoordinates<T>& coords) {
                auto& props = coords.propagators;
                auto& invariants = coords.invariants;
                auto& vector_components = coords.vector_components;
                return (*fp)(&invariants[0], &props[0], vector_components, coords.b_coeffs->operator[](bi).data());
            }));
        }

    }

    return deletable_list;
}

template <typename T> BasisFunction<T> factorizing_d_dimensional_traceless_completion(const TensorInsertion& tensor, int loop) {

    auto mu_part = tensor;
    // Go over each monomial factor of relevant loop
    for (auto& coord : mu_part.first.at(loop)) {


        // Complete power 2 guy
        if (coord.second == 2) {
            coord.second -= 2;
            mu_part.second.at(loop) += 1;

            return BasisFunction<T>([tensor, mu_part](const TopologyCoordinates<T>& coords) {
                auto tensor_val = evaluate_tensor_insertion(tensor, coords);
                auto constant = -(T(4) * tensor_val + evaluate_tensor_insertion(mu_part, coords));

                return std::make_pair(constant, tensor_val);
            });
        }

        // Complete power 4 guy
        // TODO: speed optimize with better choice of completion.
        if (coord.second == 4) {
            coord.second -= 4;
            mu_part.second.at(loop) += 2;

            return BasisFunction<T>([tensor, mu_part](const TopologyCoordinates<T>& coords, T d) {
                return evaluate_tensor_insertion(tensor, coords) * (d - T(4)) * (d - T(2)) - evaluate_tensor_insertion(mu_part, coords) * T(3);
            });
        }
        // Complete power 6 guy
        if (coord.second == 6) {
            coord.second -= 6;
            mu_part.second.at(loop) += 3;

            return BasisFunction<T>([tensor, mu_part](const TopologyCoordinates<T>& coords, T d) {
                return evaluate_tensor_insertion(tensor, coords) * (d - T(4)) * (d - T(2))*d - evaluate_tensor_insertion(mu_part, coords) * T(15);
            });
        }
         // Complete power 8 guy
        if (coord.second == 8) {
            coord.second -= 8;
            mu_part.second.at(loop) += 4;

            return BasisFunction<T>([tensor, mu_part](const TopologyCoordinates<T>& coords, T d) {
                return evaluate_tensor_insertion(tensor, coords) * (d - T(4)) * (d - T(2))* d * (d + T(2)) - evaluate_tensor_insertion(mu_part, coords) * T(105);
            });
        }
        /*
        * this continues as:
        * evaluate_tensor_insertion(tensor, coords) * (d - T(4)) * (d - T(2))* d * (d + T(2)) * (d + T(4)) - evaluate_tensor_insertion(mu_part, coords) * T(9!!) etc.
        */

    }

    return BasisFunction<T>([tensor, loop](const TopologyCoordinates<T>& coords, T d) {
        std::cerr << "Failure in factorizing_d_dimensional_traceless_completion." << std::endl;
        std::cerr << "Input tensor: " << tensor << std::endl;
        std::cerr << "Loop: " << loop << std::endl;
        exit(1);
        return T(0);
    });
}


template <typename T> BasisFunction<T> mu12_traceless_completion(const TensorInsertion& tensor){
    auto mu12_count = tensor.second.at(2);
    if(mu12_count==2){
        auto other_mu = tensor;
        other_mu.second.at(2) = 0;
        other_mu.second.at(1) = 1;
        other_mu.second.at(0) = 1;
        BasisFunction<T> mu12_completed([tensor, other_mu](const TopologyCoordinates<T>& coords) {
                auto tensor_val = evaluate_tensor_insertion(tensor, coords);
                auto mu_val = evaluate_tensor_insertion(other_mu, coords);
                return std::make_pair(-(mu_val + T(4) * tensor_val), tensor_val);
        });
        return mu12_completed;
    }
    if(mu12_count==4){
        auto other_mu = tensor;
        other_mu.second.at(2) = 0;
        other_mu.second.at(1) = 2;
        other_mu.second.at(0) = 2;
        return BasisFunction<T>([tensor, other_mu](const TopologyCoordinates<T>& coords, T d) {
                   return evaluate_tensor_insertion(tensor, coords) * (d - T(4)) * (d - T(2)) - evaluate_tensor_insertion(other_mu, coords) * T(3);
        });
    }
    if(mu12_count==6){
        auto other_mu = tensor;
        other_mu.second.at(2) = 0;
        other_mu.second.at(1) = 3;
        other_mu.second.at(0) = 3;
        return BasisFunction<T>([tensor, other_mu](const TopologyCoordinates<T>& coords, T d) {
                   return evaluate_tensor_insertion(tensor, coords) * (d - T(4)) * (d - T(2))* d - evaluate_tensor_insertion(other_mu, coords) * T(15);
        });
    }
    if(mu12_count==8){
        auto other_mu = tensor;
        other_mu.second.at(2) = 0;
        other_mu.second.at(1) = 4;
        other_mu.second.at(0) = 4;
        return BasisFunction<T>([tensor, other_mu](const TopologyCoordinates<T>& coords, T d) {
                   return evaluate_tensor_insertion(tensor, coords) * (d - T(4)) * (d - T(2))* d *(d + T(2)) - evaluate_tensor_insertion(other_mu, coords) * T(105);
        });
    }
    
    /*
    * this continues as:
    * evaluate_tensor_insertion(tensor, coords) * (d - T(4)) * (d - T(2))* d * (d + T(2)) * (d + T(4)) - evaluate_tensor_insertion(mu_part, coords) * T(9!!) etc.
    */

    std::cerr << "Traceless completion for mu12 power "<<mu12_count <<" not implemented yet" << std::endl;
    exit(1);

}

namespace detail {
template <typename T>
std::pair<std::vector<BasisFunction<T>>, std::vector<BasisFunction<T>>> factorizing_master_surface_functions(std::vector<TensorInsertion> topo_tensors,
                                                                                                             const lGraph::xGraph& gr) {
    const auto& topo = gr.get_topology();

    if(gr.get_n_loops()!=2){
        _WARNING_R("Only works for two loops!");
        std::exit(1);
    }

    std::vector<TensorInsertion> trivial_transverse;
    std::vector<TensorInsertion> left_completable_transverse;
    std::vector<TensorInsertion> right_completable_transverse;
    std::vector<TensorInsertion> mu12_completable_transverse;
    std::vector<TensorInsertion> IBP_requiring;

    for (auto& t : topo_tensors) {
        auto mu12_count = t.second.at(2);
        if (mu12_count == 1 || mu12_count == 3 || mu12_count == 5) {
            trivial_transverse.push_back(t);
            continue;
        }
        if (mu12_count==2||mu12_count==4||mu12_count==6||mu12_count==8) {
            mu12_completable_transverse.push_back(t);
            continue;
        }

        size_t total_left_transverse_count = 0;
        size_t total_right_transverse_count = 0;
        for (auto individual_left_transverse_count : t.first.at(0)) {
            if (individual_left_transverse_count.second % 2 == 1) {
                trivial_transverse.push_back(t);
                goto next_tensor;
            }
            total_left_transverse_count += individual_left_transverse_count.second;
        }
        for (auto individual_right_transverse_count : t.first.at(1)) {
            if (individual_right_transverse_count.second % 2 == 1) {
                trivial_transverse.push_back(t);
                goto next_tensor;
            }
            total_right_transverse_count += individual_right_transverse_count.second;
        }
        if (total_left_transverse_count + total_right_transverse_count == 0) { IBP_requiring.push_back(t); }
        else if (total_left_transverse_count % 2 == 0 && total_left_transverse_count > 0) {
            left_completable_transverse.push_back(t);
        }
        else if (total_right_transverse_count % 2 == 0 && total_right_transverse_count > 0) {
            right_completable_transverse.push_back(t);
        }
        else {
            std::cerr << "Error classifying butterfly surface terms.";
            exit(1);
        }
    next_tensor:
        continue;
    }

    // The aim now is to build all surface terms.
    std::vector<BasisFunction<T>> surface_terms;
    for (auto& tensor : trivial_transverse) { surface_terms.push_back(tensor_insertion_evaluator<T>(tensor)); }

    for (auto& tensor : left_completable_transverse) { surface_terms.push_back(factorizing_d_dimensional_traceless_completion<T>(tensor, 0)); }
    for (auto& tensor : right_completable_transverse) { surface_terms.push_back(factorizing_d_dimensional_traceless_completion<T>(tensor, 1)); }
    for (auto& tensor : mu12_completable_transverse) {
        surface_terms.push_back(mu12_traceless_completion<T>(tensor));
    }

    // The true IBPs are not as organized as the IBP_requiring tensor insertions.
    std::vector<BasisFunction<T>> topology_IBPs;
    auto it = surface_term_data_holder.find(topo);
    if (it != surface_term_data_holder.end()) { topology_IBPs = std::get<std::vector<BasisFunction<T>>>(it->second); }
    for (auto& IBP : topology_IBPs) { surface_terms.push_back(IBP); }

    std::vector<BasisFunction<T>> master_functions;
    {
        auto f = master_term_data_holder.find(topo);
        if (f != master_term_data_holder.end()) { 
            master_functions = std::get<std::vector<BasisFunction<T>>>(f->second);
            auto& function_names = std::get<std::vector<std::string>>(f->second);
            if (function_names.size() > 0) {
                assert(function_names.size() == master_functions.size());
                for (size_t ii = 0; ii < function_names.size(); ++ii) master_functions[ii].set_integral_insertion_string(function_names[ii]);
            }
        }
    }

    assert(master_functions.size() == IBP_requiring.size() - topology_IBPs.size());

    return {master_functions, surface_terms};
}
} // namespace detail

} // namespace SurfaceTerms



template <typename T>
std::pair<std::vector<BasisFunction<T>>, std::vector<BasisFunction<T>>> master_surface_functions_2L(const lGraph::xGraph& gr_in) {
    using namespace SurfaceTerms;

    auto& gr = gr_in.get_special_routing();
    
    auto topo_tensors = tensor_insertions(gr);
    const auto& topo = gr.get_topology();

    DEBUG_PRINT(gr);
    DEBUG_MESSAGE("Tensors:");
    DEBUG(for (auto&& [i, obj] : enumerate(topo_tensors)) { _MESSAGE(i, ": ", obj); })

    auto is_factor = gr.get_base_graph().is_factorizable();

    if (is_factor) { return SurfaceTerms::detail::factorizing_master_surface_functions<T>(topo_tensors, gr); }

    std::vector<TensorInsertion> trivial_transverse;
    std::vector<TensorInsertion> completable_transverse;
    std::vector<TensorInsertion> IBP_requiring;

    const size_t num_common = gr.dimension_common_transverse();

    DEBUG_PRINT(num_common);

    for (auto& t : topo_tensors) {
        size_t total_transverse_count = 0;
        for (size_t i = 0; i < num_common; i++) {
            auto individual_transverse_count = common_transverse_count(t, i);
            if (individual_transverse_count % 2 == 1) {
                trivial_transverse.push_back(t);
                goto next_tensor;
            }
            total_transverse_count += individual_transverse_count;
        }
        if (total_transverse_count == 0) { IBP_requiring.push_back(t); }
        else {
            completable_transverse.push_back(t);
        }
    next_tensor:
        continue;
    }

    DEBUG_PRINT(topo_tensors.size());
    DEBUG_PRINT(trivial_transverse.size());
    DEBUG_PRINT(completable_transverse.size());
    DEBUG_PRINT(IBP_requiring.size());

    // The aim now is to build all surface terms.
    std::vector<BasisFunction<T>> surface_terms;
    for (auto& tensor : trivial_transverse) { surface_terms.push_back(tensor_insertion_evaluator<T>(tensor)); }

    for (auto& tensor : completable_transverse) { surface_terms.push_back(d_dimensional_traceless_completion<T>(tensor, num_common)); }

    // The true IBPs are not as organized as the IBP_requiring tensor insertions.
    // Sanity Check
    {
        auto sf_term = surface_term_data_holder.find(topo);
        if (sf_term == surface_term_data_holder.end()) {
            std::cerr << "master_surface_functions_2L(.) could not find surface terms for topology" << std::endl;
            topo.show(std::cerr);
            std::exit(1);
        }
    }
    auto topology_IBPs = std::get<std::vector<BasisFunction<T>>>(surface_term_data_holder.at(topo));
    for (auto& IBP : topology_IBPs) { surface_terms.push_back(IBP); }

    DEBUG_PRINT(topology_IBPs.size());

    std::vector<BasisFunction<T>> master_functions;
    {
        auto f = master_term_data_holder.find(topo);
        if (f != master_term_data_holder.end()) { 
            master_functions = std::get<std::vector<BasisFunction<T>>>(f->second);
            auto& function_names = std::get<std::vector<std::string>>(f->second);
            if (function_names.size() > 0) {
                assert(function_names.size() == master_functions.size());
                for (size_t ii = 0; ii < function_names.size(); ++ii) master_functions[ii].set_integral_insertion_string(function_names[ii]);
            }
        }
    }

    DEBUG_PRINT(master_functions.size());

    if (master_functions.size() != IBP_requiring.size() - topology_IBPs.size()) {
        _WARNING_R_N(3,"Could not find (enough) master insertions for\n", gr);
        if (master_term_data_holder.count(topo) > 0) {
            _WARNING_N(3,"master_term_data_holder contains this topology, but with only ", master_functions.size(), " defined masters, while ",
                     IBP_requiring.size() - topology_IBPs.size(), " were determined to be needed.");
        }
        // FIX ME: Sub leading colour hack!
        // throw std::runtime_error("Missing master insertions!");
    }

    return {master_functions, surface_terms};
}

namespace detail {

inline void handle_naming_LoopMonomial(const lGraph::xGraph& gr, const LoopMonomial& lm, size_t loop, std::vector<std::string>& alphas, size_t num_common) {
    if (lm.contributions.size() == 0) return;
    using namespace OnShellStrategies;
    for (auto& c : lm.contributions) {
        assert(c.first >= num_common);
        auto& external_mom = StandardInsertions::insertion_momenta(gr, loop, c.first - num_common + 1);
        std::string toadd = "Alpha[";
        toadd += "Loop[" + std::to_string(loop) + "], ";
        toadd += "External[";
        for (size_t ll = 0; ll < external_mom.size(); ll++) {
            if (ll > 0) toadd += ",";
            toadd += std::to_string(external_mom[ll]);
        }
        toadd += "], ";
        toadd += "Pow[" + std::to_string(c.second) + "]";
        toadd += "]";
        alphas.push_back(toadd);
    }
}

inline std::string get_naming_insertion(const lGraph::xGraph& gr, const TensorInsertion& t, size_t num_common) {
    // check no muij's
    static const std::vector<size_t> compare = {0, 0, 0};
    assert(t.second == compare);
    // scalar
    if (t.power_counting() == 0) return "Insertion[Scalar[1]]";

    std::vector<std::string> alphas;
    for (size_t ii = 0; ii < t.first.size(); ii++) {
        if (t.first[ii].power_counting() == 0) continue;
        handle_naming_LoopMonomial(gr, t.first[ii], ii + 1, alphas, num_common);
    }
    std::string t2str = "Insertion[";
    for (size_t kk = 0; kk < alphas.size(); kk++) {
        if (kk > 0) t2str += ", ";
        t2str += alphas[kk];
    }
    t2str += "]";

    return t2str;
}

template <typename T>
std::pair<std::vector<BasisFunction<T>>, std::vector<BasisFunction<T>>> scattering_plane_tensor_insertion_functions(const lGraph::xGraph& gr_in) {
    using namespace SurfaceTerms;

    auto& gr = gr_in.get_special_routing();

    auto topo_tensors = tensor_insertions(gr);

    DEBUG_PRINT(gr);
    DEBUG_MESSAGE("Tensors:");
    DEBUG(for (auto&& [i, obj] : enumerate(topo_tensors)) { _MESSAGE(i, ": ", obj); })

    auto is_factor = gr.get_base_graph().is_factorizable();

    if (is_factor) { return SurfaceTerms::detail::factorizing_master_surface_functions<T>(topo_tensors, gr); }

    std::vector<TensorInsertion> trivial_transverse;
    std::vector<TensorInsertion> completable_transverse;
    std::vector<TensorInsertion> scattering_plane_tensor_masters;

    const size_t num_common = gr.dimension_common_transverse();

    DEBUG_PRINT(num_common);

    for (auto& t : topo_tensors) {
        size_t total_transverse_count = 0;
        for (size_t i = 0; i < num_common; i++) {
            auto individual_transverse_count = common_transverse_count(t, i);
            if (individual_transverse_count % 2 == 1) {
                trivial_transverse.push_back(t);
                goto next_tensor;
            }
            total_transverse_count += individual_transverse_count;
        }
        if (total_transverse_count == 0) { scattering_plane_tensor_masters.push_back(t); }
        else { completable_transverse.push_back(t); }
    next_tensor:
        continue;
    }

    DEBUG_PRINT(topo_tensors.size());
    DEBUG_PRINT(trivial_transverse.size());
    DEBUG_PRINT(completable_transverse.size());
    DEBUG_PRINT(scattering_plane_tensor_masters.size());

    // The aim now is to build all surface terms.
    std::vector<BasisFunction<T>> surface_terms;
    for (auto& tensor : trivial_transverse) { surface_terms.push_back(tensor_insertion_evaluator<T>(tensor)); }

    for (auto& tensor : completable_transverse) { surface_terms.push_back(d_dimensional_traceless_completion<T>(tensor, num_common)); }

    std::vector<BasisFunction<T>> tensor_master_terms;
    for(auto& tensor: scattering_plane_tensor_masters) tensor_master_terms.push_back(tensor_insertion_evaluator<T>(tensor));

    // sanity check
    assert(surface_terms.size() + tensor_master_terms.size() == topo_tensors.size());

    // set the integral insertion for each BasisFunction
    for(auto& s: surface_terms) s.set_integral_insertion(IntegralInsertion::surface);
    for(size_t ii = 0; ii < tensor_master_terms.size(); ii++) {
        auto& m = tensor_master_terms[ii];
        auto& s = scattering_plane_tensor_masters[ii];
        std::string mn = get_naming_insertion(gr, s, num_common);
        m.set_integral_insertion_string(mn);
    }

    return {tensor_master_terms, surface_terms};
}
} // namespace detail

template <typename T>
std::pair<std::vector<BasisFunction<T>>, std::vector<BasisFunction<T>>> master_surface_functions_1L(const lGraph::xGraph& gr, bool include_mu2_as_master_insertions) {
    using namespace SurfaceTerms;

    const size_t num_common = 4- gr.get_span_dim();

    std::vector<BasisFunction<T>> surface_terms;
    std::vector<BasisFunction<T>> master_functions;

    auto gravity_power_counting = (settings::general::power_counting_of_theory == settings::general::power_counting::Gravity);
    auto pentagon_muSq_insertion = (settings::OneLoopIntegrals::pentagon_mu2_insertion).value;
    bool has_special_1L_bubble = gr.is_bubble_with_single_massless_leg();


    if (pentagon_muSq_insertion > 5) {
        std::cerr << "ERROR: pentagon insertion mu2^" << pentagon_muSq_insertion << " not supported." << std::endl;
        exit(1);
    }

    if (pentagon_muSq_insertion > 1 && !gravity_power_counting) {
        std::cerr << "ERROR: higher mu insertions are only supported for gravity power counting." << std::endl;
        exit(1);
    }

	/*
		construct independent tensor basis from (1+a1)^1*(1+a2)^n *...*(1+mu2)^((n-n%2)/2) truncated by max_power 
		odd monomials in ai are surface terms
		even monomials in ai give surface terms when derived from the vector 
			v=ai\partial_a1-a1\partial_ai on ai^(n-1)a1: v(ai^(n-1)a1)=ai^n-(n-1)ai^(n-2)a1^2.
		ai free monomials are treated as master insertions
	*/

	/*
		D-dependent completions computed with IBP vector:
			v=mu2^m*a1*(mu2\partial_{a1} - a1 mu_i \partial_{mu_i})
			surface-term = div(v) = mu2^(m+1) - a1^2 mu2^m [ D-4 + 2m ]

	*/

	
	//implemented master integrals 


    if (num_common == 0 /* Pentagon*/) {
        master_functions.push_back(
            tensor_insertion_evaluator<T>(TensorInsertion({{{0, 0}}, {pentagon_muSq_insertion}, IntegralInsertion(pentagon_muSq_insertion)})));
    }
    else if (num_common == 1 /* Box */) {

		size_t max_power = 4; //QCD
		if (gravity_power_counting) {      
#ifdef INCLUDE_GB_GRAVITY
		//max_power=2*4+1; //Gauss-Bonnet
		max_power=2*4; //Gauss-Bonnet ; experimental
#else
			max_power=2*4; //Einstein gravity
#endif
		}

		for (size_t pmu2 = 0; pmu2 <= (max_power - max_power % 2)/2; pmu2++) {
			for (size_t pa1 = 0; pa1 <= 1 && (2*pmu2+pa1 <= max_power) ; pa1++) {

				if(pa1 == 1) {
					TensorInsertion mon{{{pa1}}, {pmu2}};
					surface_terms.push_back(tensor_insertion_evaluator<T>(mon));
				}
				else {
                                  if(!include_mu2_as_master_insertions) {
					if(pmu2 == 0){
						TensorInsertion scalar{{{0}}, {0}, IntegralInsertion::scalar};
						master_functions.push_back(tensor_insertion_evaluator<T>(scalar));
					}
					else {
						TensorInsertion mon{{{2}}, {pmu2-1}};
						TensorInsertion mon_max{{{0}}, {pmu2}};
						// D-dependent factor (D-4+(2*pmu2-2));
						int d=(-4+(2*int(pmu2)-2));

						surface_terms.push_back(BasisFunction<T>(
						[mon, mon_max, d](const TopologyCoordinates<T>& coords) { 
                                                        auto mu2n = evaluate_tensor_insertion(mon_max, coords);
                                                        auto alpha2n = evaluate_tensor_insertion(mon, coords);
                                                        auto constant = mu2n - T(d) * alpha2n;
                                                        auto linear = - alpha2n; 
	
   		         			return std::make_pair(constant, linear);
						}));
					}
                                  }
                                  else {
					switch( pmu2 ){
						case 0:  
							{TensorInsertion scalar{{{0}}, {0}, IntegralInsertion::scalar};
							master_functions.push_back(tensor_insertion_evaluator<T>(scalar));} break;
						case 1:  
							{TensorInsertion mu2{{{0}}, {1}, IntegralInsertion::mu2};
							master_functions.push_back(tensor_insertion_evaluator<T>(mu2));} break;
						case 2:  
							{TensorInsertion mu4{{{0}}, {2}, IntegralInsertion::mu4};
							master_functions.push_back(tensor_insertion_evaluator<T>(mu4));} break;
						case 3:  
							{TensorInsertion mu6{{{0}}, {3}, IntegralInsertion::mu6};
							master_functions.push_back(tensor_insertion_evaluator<T>(mu6));} break;
						case 4:  
							{TensorInsertion mu8{{{0}}, {4}, IntegralInsertion::mu8};
							master_functions.push_back(tensor_insertion_evaluator<T>(mu8));} break;
						case 5:
							{TensorInsertion mu10{{{0}}, {5}, IntegralInsertion::mu10};
							master_functions.push_back(tensor_insertion_evaluator<T>(mu10));} break;
						default: std::cerr << "ERROR: Master integral not implemented." << std::endl;
					}
                                  }
				}
				
			}
		}
        if (pentagon_muSq_insertion == 5) { 
        	TensorInsertion mu8al{{{1}}, {4}};
			surface_terms.push_back(
				tensor_insertion_evaluator<T>(TensorInsertion(mu8al))
			);
		}
    }
	else if (num_common == 2 /* Triangle */) {
    
		size_t max_power = 3; //QCD
		if (gravity_power_counting) {      
#ifdef INCLUDE_GB_GRAVITY
		//max_power=2*3+3; //Gauss-Bonnet
		max_power=2*3; //Gauss-Bonnet experimental
#else
			max_power=2*3; //Einstein gravity
#endif
		}
		for (size_t pmu2 = 0; pmu2 <= (max_power - max_power % 2)/2; pmu2++) {
			for (size_t pa1 = 0; pa1 <= 1 && (2*pmu2+pa1 <= max_power) ; pa1++) {
				for (size_t pa2 = 0; 2*pmu2+pa1+pa2 <= max_power ; pa2++) {

					if(pa1 == 1 || pa2 % 2 == 1 ) {
						TensorInsertion mon{{{pa1,pa2}}, {pmu2}};
						surface_terms.push_back(tensor_insertion_evaluator<T>(mon));
					}
					else if( pa1 == 0 && pa2 != 0 && pa2 % 2 == 0 ) {
						TensorInsertion mon{{{0,pa2}}, {pmu2}};
						TensorInsertion mon_flip{{{2,pa2-2}}, {pmu2}};

						int pa2m1(pa2-1);	
      					surface_terms.push_back(BasisFunction<T>(
								[mon, mon_flip, pa2m1](const TopologyCoordinates<T>& coords) { 
									return evaluate_tensor_insertion(mon, coords) 
										- T(pa2m1) * evaluate_tensor_insertion(mon_flip, coords); }
						));

          			} 
					else if (pa1 == 0 && pa2 == 0) {
                                  if(!include_mu2_as_master_insertions) {
					if(pmu2 == 0){
						TensorInsertion scalar{{{0}}, {0}, IntegralInsertion::scalar};
						master_functions.push_back(tensor_insertion_evaluator<T>(scalar));
					}
					else {
						TensorInsertion mon{{{2,0}}, {pmu2-1}};
						TensorInsertion mon_max{{{0,0}}, {pmu2}};
						// D-dependent factor (D-4+(2*pmu2-2));
						int d=(-4+(2*int(pmu2)-2));

						surface_terms.push_back(BasisFunction<T>(
						[mon, mon_max, d](const TopologyCoordinates<T>& coords) { 
                                                        auto mu2n = evaluate_tensor_insertion(mon_max, coords);
                                                        auto alpha2n = evaluate_tensor_insertion(mon, coords);
                                                        auto constant = mu2n - T(d) * alpha2n;
                                                        auto linear = - alpha2n; 
	
   		         			return std::make_pair(constant, linear);
						}));
					}
                                  }
                                  else {
						switch( pmu2 ){
							case 0:  
								{TensorInsertion scalar{{{0,0}}, {0}, IntegralInsertion::scalar};
								master_functions.push_back(tensor_insertion_evaluator<T>(scalar));} break;
							case 1:  
								{TensorInsertion mu2{{{0,0}}, {1}, IntegralInsertion::mu2};
								master_functions.push_back(tensor_insertion_evaluator<T>(mu2));} break;
							case 2:  
								{TensorInsertion mu4{{{0,0}}, {2}, IntegralInsertion::mu4};
								master_functions.push_back(tensor_insertion_evaluator<T>(mu4));} break;
							case 3:  
								{TensorInsertion mu6{{{0,0}}, {3}, IntegralInsertion::mu6};
								master_functions.push_back(tensor_insertion_evaluator<T>(mu6));} break;
							case 4:  
								{TensorInsertion mu8{{{0,0}}, {4}, IntegralInsertion::mu8};
								master_functions.push_back(tensor_insertion_evaluator<T>(mu8));} break;
							case 5:  
								{TensorInsertion mu10{{{0,0}}, {5}, IntegralInsertion::mu10};
								master_functions.push_back(tensor_insertion_evaluator<T>(mu10));} break;
							default: std::cerr << "ERROR: Master integral not implemented." << std::endl;
						}
                                  }
					}
					else {
					   std::cerr << "ERROR: Bad surface term." << std::endl;
        				exit(1);
					}
				}
			}
		} 
	}
    else if (num_common == 3 and !has_special_1L_bubble /* Bubble */) {

		size_t max_power = 2; //QCD
		if (gravity_power_counting) {      
#ifdef INCLUDE_GB_GRAVITY
		//max_power=2*2+4; //Gauss-Bonnet
		max_power=2*2; //Gauss-Bonnet experimental
#else
			max_power=2*2; //Einstein gravity
#endif
		}
		for (size_t pmu2 = 0; pmu2 <= (max_power - max_power % 2)/2; pmu2++) {
			for (size_t pa1 = 0; pa1 <= 1 && (2*pmu2+pa1 <= max_power) ; pa1++) {
				for (size_t pa2 = 0; 2*pmu2+pa1+pa2 <= max_power ; pa2++) {
				for (size_t pa3 = 0; 2*pmu2+pa1+pa2+pa3 <= max_power ; pa3++) {

						if(pa1 == 1 || pa2 % 2 == 1 || pa3 % 2 == 1  ) {
							TensorInsertion mon{{{pa1,pa2,pa3}}, {pmu2}};
							surface_terms.push_back(tensor_insertion_evaluator<T>(mon));
						}
						else if( pa1 == 0 && pa2 != 0 && pa2 % 2 ==0 && pa3 % 2 == 0) {
							TensorInsertion mon{{{0,pa2,pa3}}, {pmu2}};
							TensorInsertion mon_flip{{{2,pa2-2,pa3}}, {pmu2}};
						
							int pa2m1=(pa2-1);	
							surface_terms.push_back(BasisFunction<T>(
								[mon, mon_flip, pa2m1](const TopologyCoordinates<T>& coords) { 
									return evaluate_tensor_insertion(mon, coords) 
										- T(pa2m1) * evaluate_tensor_insertion(mon_flip, coords); }
							));
          				}
						else if( pa1 == 0 && pa2 == 0 && pa3 != 0 && pa3 % 2 == 0) {
							TensorInsertion mon{{{0,pa2,pa3}}, {pmu2}};
							TensorInsertion mon_flip{{{2,pa2,pa3-2}}, {pmu2}};

							int pa3m1(pa3-1);	
							surface_terms.push_back(BasisFunction<T>(
								[mon, mon_flip, pa3m1](const TopologyCoordinates<T>& coords) { 
									return evaluate_tensor_insertion(mon, coords) 
										- T(pa3m1) * evaluate_tensor_insertion(mon_flip, coords); }
							));

          				}
						else if (pa1 == 0 && pa2 == 0 && pa3 == 0) {
                                  if(!include_mu2_as_master_insertions) {
							if(pmu2 == 0){
								TensorInsertion scalar{{{0}}, {0}, IntegralInsertion::scalar};
								master_functions.push_back(tensor_insertion_evaluator<T>(scalar));
							}
							else {
 								TensorInsertion mon{{{2,0,0}}, {pmu2-1}};
								TensorInsertion mon_max{{{0,0,0}}, {pmu2}};
								// D-dependent factor (D-4+(2*pmu2-2));
								int d=(-4+(2*int(pmu2)-2));

								surface_terms.push_back(BasisFunction<T>(
								[mon, mon_max, d](const TopologyCoordinates<T>& coords) { 
                                                                        auto mu2n = evaluate_tensor_insertion(mon_max, coords);
                                                                        auto alpha2n = evaluate_tensor_insertion(mon, coords);
                                                                        auto constant = mu2n - T(d) * alpha2n;
                                                                        auto linear = - alpha2n; 
	
   		         					return std::make_pair(constant, linear);
								}));
							}
                                  }
                                  else {
							switch( pmu2 ){
								case 0:  
									{TensorInsertion scalar{{{0,0,0}}, {0}, IntegralInsertion::scalar};
									master_functions.push_back(tensor_insertion_evaluator<T>(scalar));} break;
								case 1:  
									{TensorInsertion mu2{{{0,0,0}}, {1}, IntegralInsertion::mu2};
									master_functions.push_back(tensor_insertion_evaluator<T>(mu2));} break;
								case 2:  
									{TensorInsertion mu4{{{0,0,0}}, {2}, IntegralInsertion::mu4};
									master_functions.push_back(tensor_insertion_evaluator<T>(mu4));} break;
								case 3:  
									{TensorInsertion mu6{{{0,0,0}}, {3}, IntegralInsertion::mu6};
									master_functions.push_back(tensor_insertion_evaluator<T>(mu6));} break;
								case 4:  
									{TensorInsertion mu8{{{0,0,0}}, {4}, IntegralInsertion::mu8};
									master_functions.push_back(tensor_insertion_evaluator<T>(mu8));} break;
								case 5:  
									{TensorInsertion mu10{{{0,0,0}}, {5}, IntegralInsertion::mu10};
									master_functions.push_back(tensor_insertion_evaluator<T>(mu10));} break;
								default: std::cerr << "ERROR: Master integral not implemented." << std::endl;
							}
                                  }
						}
						else {
						   std::cerr << "ERROR: Bad surface term." << std::endl;
        					exit(1);
						}
				}
				}
			}
		} 
	}
    else if (num_common == 3 and has_special_1L_bubble /* massive Bubble with massless external leg*/) {
        // Master integrals
        TensorInsertion scalar{{{0, 0, 0}}, {0}, IntegralInsertion::scalar};
        TensorInsertion mu2{{{0, 0, 0}}, {1}, IntegralInsertion::mu2};
        master_functions.push_back(tensor_insertion_evaluator<T>(scalar));
        master_functions.push_back(tensor_insertion_evaluator<T>(mu2));
        // Surface terms parametrized by { al1, al2, chi}
        TensorInsertion al1{{{1, 0, 0}}, {0}};
        TensorInsertion al2{{{0, 1, 0}}, {0}};
        TensorInsertion chi{{{0, 0, 1}}, {0}};
        TensorInsertion al12{{{1, 1, 0}}, {0}};
        TensorInsertion al1chi{{{1, 0, 1}}, {0}};
        TensorInsertion al2chi{{{0, 1, 1}}, {0}};
        TensorInsertion al1sq{{{2, 0, 0}}, {0}};
        TensorInsertion al2sq{{{0, 2, 0}}, {0}};
        TensorInsertion chisq{{{0, 0, 2}}, {0}};

        surface_terms.push_back(tensor_insertion_evaluator<T>(al1));
        surface_terms.push_back(tensor_insertion_evaluator<T>(al2));
        surface_terms.push_back(BasisFunction<T>([al1sq, al2sq](const TopologyCoordinates<T>& coords) {
            return evaluate_tensor_insertion(al1sq, coords) - evaluate_tensor_insertion(al2sq, coords);
        }));
        surface_terms.push_back(tensor_insertion_evaluator<T>(al12));
        surface_terms.push_back(tensor_insertion_evaluator<T>(al1chi));
        surface_terms.push_back(tensor_insertion_evaluator<T>(al2chi));
        // Normalization is chosen such that (n*p) = 1
        surface_terms.push_back(BasisFunction<T>([chi](const TopologyCoordinates<T>& coords) { return T(1) - T(2)*evaluate_tensor_insertion(chi, coords); }));
        surface_terms.push_back(BasisFunction<T>([chisq](const TopologyCoordinates<T>& coords) { return T(1) - T(3) * evaluate_tensor_insertion(chisq, coords); }));
    } 
    else if (num_common == 4 /* Tadpole */) {
        TensorInsertion ala{{{1, 0, 0, 0}}, {0}};
        TensorInsertion alb{{{0, 1, 0, 0}}, {0}};
        TensorInsertion alc{{{0, 0, 1, 0}}, {0}};
        TensorInsertion ald{{{0, 0, 0, 1}}, {0}};
        surface_terms.push_back(tensor_insertion_evaluator<T>(ala));
        surface_terms.push_back(tensor_insertion_evaluator<T>(alb));
        surface_terms.push_back(tensor_insertion_evaluator<T>(alc));
        surface_terms.push_back(tensor_insertion_evaluator<T>(ald));
        TensorInsertion scalar{{{0, 0, 0, 0}}, {0}, IntegralInsertion::scalar};
        master_functions.push_back(tensor_insertion_evaluator<T>(scalar));
    }
    else {
	gr.show();
        std::cerr << "ERROR: Unknown size of common transverse space. num_common = " << num_common << std::endl;
        exit(1);
    }

    return {master_functions, surface_terms};
}

template <typename T>
void _search_1loop_pure_insertions(const lGraph::xGraph& gr, std::pair<std::vector<BasisFunction<T>>, std::vector<BasisFunction<T>>>& basis) {
    // there should be a single (scalar) master
    assert(basis.first.size() == 1);
    // nothing to do
    if (surface_term_data_holder.empty() && master_term_data_holder.empty()) throw std::runtime_error("Empty surface/master containers");
    auto topo = gr.get_topology();
    auto p_surface = surface_term_data_holder.find(topo);
    auto p_master = master_term_data_holder.find(topo);
    // only one function should be present
    if (p_surface == surface_term_data_holder.end() && p_master == master_term_data_holder.end()) throw std::runtime_error("No 'pure' basis replacement found");
    std::vector<BasisFunction<T>> replace_s;
    if (p_surface != surface_term_data_holder.end()) replace_s = std::get<std::vector<BasisFunction<T>>>(p_surface->second);
    std::vector<BasisFunction<T>> replace_m;
    if (p_master != master_term_data_holder.end()) {
        replace_m = std::get<std::vector<BasisFunction<T>>>(p_master->second);
        auto& function_names = std::get<std::vector<std::string>>(p_master->second);
        if (function_names.size() > 0) {
            assert(function_names.size() == replace_m.size());
            for (size_t ii = 0; ii < function_names.size(); ++ii) replace_m[ii].set_integral_insertion_string(function_names[ii]);
        }
    }
    if (replace_s.size() + replace_m.size() != 1) throw std::runtime_error("More than one replacement for 'pure' basis found");
    basis.first.clear();
    if (replace_s.size() > 0) basis.second.push_back(replace_s[0]);
    if (replace_m.size() > 0) basis.first.push_back(replace_m[0]);
}

template <typename T> std::pair<std::vector<BasisFunction<T>>, std::vector<BasisFunction<T>>> master_surface_functions(const lGraph::xGraph& gr) {
    switch (gr.get_n_loops()) {
        case 1: {
            if (settings::general::one_loop_master_basis == settings::general::OneLoopMasterBasis::pure) {
                auto basis = master_surface_functions_1L<T>(gr, false);
                try {
                    _search_1loop_pure_insertions(gr, basis);
                }
                catch (std::runtime_error& e) {
                    _WARNING_R("WARNING: no replacement performed in 'pure' 1-loop basis for the following diagram!");
                    std::cout << e.what() << std::endl;
                    gr.show();
                }
                return basis;
            }
            else if (settings::general::one_loop_master_basis == settings::general::OneLoopMasterBasis::only_scalar)
                return master_surface_functions_1L<T>(gr, false);
            else
                return master_surface_functions_1L<T>(gr, true);
        }
        case 2: return master_surface_functions_2L<T>(gr);
        default: _WARNING_R("Not implemented!"); std::exit(1);
    }
}

} // namespace FunctionSpace
} // namespace Caravel

#endif // SURFACETERMS_H_INC
