#ifndef FUNCTIONSPACE_H_INC
#define FUNCTIONSPACE_H_INC

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>

#include "AmplitudeCoefficients/Hierarchy.h"
#include "FunctionSpace/IntegralInsertion.h"
#include "Graph/GraphKin.h"
#include "OnShellStrategies/osm.h"
#include "Core/settings.h"
#include "Core/MathInterface.h"
#include "Graph/Graph.h"
#include <cassert>
#include <functional>
#include <vector>
#include <utility>

#include "Core/Utilities.h"


namespace Caravel {
namespace FunctionSpace {

using lGraph::lGraphKin::GraphKin;

// TODO: All of these things Could be better abstracted

template <typename T> using CoordPowers = std::vector<T>;

/**
 * A list of loop coordinates arranged as common, uncommon
 */
template <typename T> using LoopCoordinates = std::vector<CoordPowers<T>>;
template <typename T> using RegulatingCoordinates = std::vector<T>;

/**
 * Interface safe structure to hold all invariants necessary for
 * evaluating basis functions May be rearranged in the future for
 * optimization purposes.  There is some redundancy in including
 * fourD_coords and the propagators. Removing this redundancy
 * requires lining up the directions chosen for the fourD_coords with
 * Generic implementation for normalizable field.
 */
template <typename T> struct TopologyCoordinates {
    std::vector<LoopCoordinates<T>> fourD_coords;
    RegulatingCoordinates<T> mu_coords;
    std::vector<T> propagators;
    std::vector<T> invariants;
    std::vector<T> common_transverse_norms;
    std::vector<std::vector<T>> vector_components;
    /**
     * Utility member for experimentation.
     */
    std::vector<T> special;
    /**
     * Store only the pointer which can be used for acessing the values
     * of the coefficients for surface terms
     */
    const std::vector<std::vector<T>>* b_coeffs;
};

namespace SurfaceTerms {

/**
 * Structure for evaluations of expressions relevant for evaluation
 * of surface terms.
 */
template <typename T> struct IBPCoordinates {
    std::vector<T> propagators;
    std::vector<T> invariants;
    std::vector<std::vector<T>> vector_components;
};

} // namespace SurfaceTerms

/**
 * Structure for describing the product of 4-dimensional tensor
 * coordinates contributions represents a list of powers, to which
 * each coordinate should be raised.
 */
class LoopMonomial {
    // Pair of coord-power
    using Contributions = std::vector<std::pair<size_t, size_t>>;
    Contributions::iterator at_it(size_t n);

  public:
    Contributions contributions;
    LoopMonomial(const std::vector<size_t>& contributions);
    LoopMonomial(std::initializer_list<size_t> contributions);

    size_t at(size_t n) const;
    void set(size_t direction, size_t n);
    void decrease_by(size_t direction, size_t n);

    Contributions::iterator begin();
    Contributions::iterator end();

    Contributions::const_iterator begin() const;
    Contributions::const_iterator end() const;

    size_t power_counting() const;

    friend std::ostream& operator<<(std::ostream& out, const LoopMonomial& monomial);
};

using RegulatingMonomial = std::vector<size_t>;

struct TensorInsertion {
    std::vector<LoopMonomial> first;
    RegulatingMonomial second;
    IntegralInsertion insertion{IntegralInsertion::undefined};
    size_t power_counting() const;
};

std::ostream& operator<<(std::ostream& o, const TensorInsertion& ti);

/**
 * Class that represents a d-dependent basis function to be used in a
 * function basis. Can be constructed in a few different ways,
 * offering different ways to perform the d caching.
 */
template <typename T> class BasisFunction {
  private:
    std::function<std::vector<T>(const TopologyCoordinates<T>&, const std::vector<T>& d_values)> action;

    IntegralInsertion insertion_enum{IntegralInsertion::undefined};
    std::string insertion{};

  public:
    /**
     * Construct the basis function from a function object Func.
     * The return type of Func determines how d-dependence is handled.
     * NOTE: the latter can be a cause of huge difference in performance.
     */
    template <typename Func> BasisFunction(Func&&);

    /**
     * Evaluate the basis function at a single value of d for given coords.
     */
    T operator()(const TopologyCoordinates<T>& coords, T d) const;

    /**
     * Evaluate the basis function at a multiple values of d for given
     * coords. If it has been well constructed then this will be more
     * efficient than a simple loop.
     */
    std::vector<T> operator()(const TopologyCoordinates<T>& coords, const std::vector<T>& d_values) const;

    /**
     * Sets information about the type of IntegralInsertion of *this
     */
    void set_integral_insertion(const IntegralInsertion& in) { insertion_enum = in; insertion = wise_enum::to_string(in); }
    void set_integral_insertion_string(const std::string& in) { insertion = in; }

    /**
     * Method that returns the integral insertion associated with the given entry in the basis
     */
    IntegralInsertion get_integral_insertion() const { return insertion_enum; }
    std::string get_integral_insertion_string() const { return insertion; }

    template <typename Func, typename = std::enable_if_t<!std::is_same<std::decay_t<Func>, BasisFunction<T>>::value>> BasisFunction<T>& operator=(Func&&);

    BasisFunction& operator=(const BasisFunction&) = default;
    BasisFunction& operator=(BasisFunction&&) = default;
    BasisFunction(const BasisFunction&) = default;
    BasisFunction(BasisFunction&) = default;
    BasisFunction(BasisFunction&&) = default;
    ~BasisFunction() = default;
};

/**
 * A struct to help customization of master insertions
 */
struct CustomBasis {
    std::vector<lGraph::MomentumRouting> new_internal; /**< Holds (in an ordered way) the remapping of momenta (e.g. new_loop_i = new_internal[i]) */
    std::vector<size_t> new_external; /**< Holds corresponding permutation for external momenta (e.g. new_k_i = momDconf.p(new_external[i])) */
};

/**
 * A vector valued function which spans the integrand space for
 * non-exceptional values of the dimensional regulator d
 */

template <typename T, size_t D> class FunctionBasis {
    std::function<TopologyCoordinates<T>(const OnShellPoint<T, D>&)> coord_evaluator;
    std::vector<BasisFunction<T>> basis;
    size_t number_of_masters{0}; /**< Holds information on how many master insertions are in *this */
    size_t dimension{0};         /**< Holds the full size of the basis (not considering truncation) */

  public:
    FunctionBasis() = default;
    FunctionBasis(const std::function<TopologyCoordinates<T>(const OnShellPoint<T, D>&)>& _coord_evaluator, const std::vector<BasisFunction<T>>& _basis,
                  size_t masters, size_t dim)
        : coord_evaluator(_coord_evaluator), basis(_basis), number_of_masters(masters), dimension(dim) {
        if ((settings::general::power_counting_of_theory != settings::general::power_counting::renormalizable) &&
            (settings::general::power_counting_of_theory != settings::general::power_counting::Gravity)) {
            std::cerr << "ERROR: So far only renormalizable power counting and Gravity considered for FunctionBasis!" << std::endl;
            std::exit(1);
        }
    }

    /**
     * Evaluate the full function basis on a single value of d.
     */
    std::vector<T> operator()(const OnShellPoint<T, D>& p, T d) const;

    /**
     * Evaluate the full function basis on a multiple values of d. More
     * efficient for well constructed function bases. Outputs a list of
     * d associated values  for each function.
     */
    std::vector<std::vector<T>> operator()(const OnShellPoint<T, D>& p, const std::vector<T>& d_values) const;

    size_t size() const { return basis.size(); }

    /**
     * Method that returns the number of master integrals in *this
     */
    size_t get_n_masters() const { return number_of_masters; }

    /**
     * Method that returns the dimension of this basis (not considering truncation)
     */
    size_t get_total_dimension() const { return dimension; }

    /**
     * Method that returns the integral insertion associated with the given entry in the basis
     */
    IntegralInsertion get_integral_insertion(size_t entry) const {
        assert(entry < basis.size());
        return basis[entry].get_integral_insertion();
    }
    std::string get_integral_insertion_string(size_t entry) const {
        assert(entry < basis.size());
        return basis[entry].get_integral_insertion_string();
    }
};


/**
 * The type which is the generic topology indentifier
 */
using TopoIDType = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;
/// A function poiner for surface terms coordinates
template <typename T, size_t D> using IBPCoordinatesFP = SurfaceTerms::IBPCoordinates<T> (*)(const OnShellPoint<T,D>&, const std::vector<momentumD<T,D>>&, const std::vector<std::vector<T>>&);
/// A function poiner for master terms coordinates
template <typename T, size_t D> using MasterCoordinatesFP = std::vector<T>(*)(const OnShellPoint<T, D>&, const momD_conf<T, D>&);

template <typename T, size_t D> using PCECoeffsFP = std::vector<std::pair<std::vector<T>,std::vector<T>>> (*)(const std::vector<momentumD<T,D>>&, const std::vector<std::vector<T>>&);
template <typename T, size_t D> using IBPvecCoeffsFP = std::vector<std::pair<std::vector<T>,std::vector<T>>> (*)(const std::vector<momentumD<T,D>>&);

//FIXME
//adding also D=5 here because of a spurious instantiation
#define _add_type(T)\
    std::vector<BasisFunction<T>>, MasterCoordinatesFP<T,6>, MasterCoordinatesFP<T,5>

/**
 * A type which stores all data about maser terms:
 *  representative graph,
 *  term and coordinate evaluators
 *  names of the inertions (can be empty)
 *
 * The decision on which types to include is hard-coded by corresponding code-generation.
 */
using MasterTermData =
std::tuple<
    lGraph::xGraph,     // representative graph
    std::string,        // its name (optional)
    _add_type(C)
#ifdef HIGH_PRECISION
    ,_add_type(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
    , _add_type(CVHP)
#endif
#ifdef USE_FINITE_FIELDS
    , _add_type(F32)
#ifdef INSTANTIATE_RATIONAL
    , _add_type(BigRat)
#endif
#endif
    ,
    std::vector<std::string> // insertion names (optional)
>;

#undef _add_type

//FIXME
//adding also D=5 here because of a spurious instantiation
#define _add_type(T)\
    std::vector<BasisFunction<T>>, IBPCoordinatesFP<T,6>, IBPCoordinatesFP<T,5>, PCECoeffsFP<T,6>, PCECoeffsFP<T,5>, IBPvecCoeffsFP<T,6>, IBPvecCoeffsFP<T,5>

/**
 * A type which stores all data about surface terms:
 *  representative graph,
 *  number of masters,
 *  term and coordinate evaluators
 *
 * The decision on which types to include is hard-coded by corresponding code-generation.
 */
using SurfaceTermData =
std::tuple<lGraph::xGraph,size_t,
    _add_type(C)
#ifdef HIGH_PRECISION
    ,_add_type(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
    , _add_type(CVHP)
#endif
#ifdef USE_FINITE_FIELDS
    , _add_type(F32)
#ifdef INSTANTIATE_RATIONAL
    , _add_type(BigRat)
#endif
#endif
>;

#undef _add_type

/// A global container for loading master terms
extern std::unordered_map<TopoIDType,MasterTermData> master_term_data_holder;
/// A global container for loading surface terms
extern std::unordered_map<TopoIDType,SurfaceTermData> surface_term_data_holder;

template <typename TF, typename T, size_t D>
std::vector<TF> evaluate_function_in_basis(const std::vector<std::reference_wrapper<const std::vector<TF>>>& coefficients, const OnShellPoint<T, D>& os_point,
                                           const std::vector<T> d_values, const FunctionBasis<T, D>& basis, size_t container_dimension);

/**
 * A list of indices defining which functions are to be dropped from a
 * full (renormalizable) function space.
 */
using BasisTruncation = std::vector<size_t>;
using HierarchyBasisTruncation = std::vector<std::vector<BasisTruncation>>;

/**
 * Function which constructs a (truncated) basis of function space for
 * the topology specified by a given xGraph.
 * Typical strategy is to regenerate the entire function space when a truncation is known.
 * @param topology
 * @param external kinematics holder
 * @param truncation Which function positions to remove.
 */
// template< typename T, size_t D, size_t L>
template <typename T, size_t D>
FunctionBasis<T, D> build_topology_function_basis(const lGraph::xGraph& gr, const GraphKin<T,D>& kinematics, BasisTruncation truncation = {});

template <typename T>
std::pair<std::vector<BasisFunction<T>>, std::vector<BasisFunction<T>>> master_surface_functions(const lGraph::xGraph& gr);

std::vector<TensorInsertion> tensor_insertions(const lGraph::xGraph&);
template <typename T> BasisFunction<T> tensor_insertion_evaluator(const TensorInsertion& t);

template <typename T> T evaluate_tensor_insertion(const TensorInsertion& t, const TopologyCoordinates<T>& coords);

/**
 * Return how many powers of loop momenta each vertex in a graph adds to the
 * power counting given an abstract power counting sepcification.
 */
constexpr int loop_powers_per_vertex(settings::general::power_counting p) {
    using namespace settings::general;
    switch (p) {
        case power_counting::renormalizable: return 1;
        case power_counting::Gravity:
        case power_counting::Higgs_EFT_plus_one: return 2;
    }
    // should not be reached
    return -1;
}

std::pair<std::vector<std::vector<size_t>>, std::vector<std::vector<size_t>>> get_left_right_indices(const lGraph::xGraph&);

template <typename T, size_t D> bool is_transversal_basis(const lGraph::xGraph&, const std::vector<std::vector<momD<T, D>>>&, const momD_conf<T, D>&);

template <typename T, size_t D> T evaluate_propagator(const lGraph::MomentumRouting&, const momD_conf<T, D>&, const OnShellPoint<T, D>&);

template <typename T, size_t D> bool is_on_shell_strand(const lGraph::Strand&, const momD_conf<T, D>&, const OnShellPoint<T, D>&);

template <typename T, size_t D> bool is_on_shell_parametrization(const lGraph::xGraph&, const momD_conf<T, D>&, const OnShellPoint<T, D>&);

size_t get_gravity_power_couting_offset(const lGraph::xGraph&);

} // namespace FunctionSpace
} // namespace Caravel

// Sub-imports
#include "TensorBasis.hpp"
#include "SurfaceTerms.hpp"
#include "FunctionSpace.hpp"

#endif // FUNCTIONSPACE_H_INC
