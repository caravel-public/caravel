#ifndef IBP_HELPER_H_INC
#define IBP_HELPER_H_INC

#include <array>
#include <vector>
#include <complex>

#include "Core/typedefs.h"

#include "misc/MiscMath.h"

namespace Caravel {
namespace FunctionSpace {
namespace SurfaceTerms {

/**
 * Typedef for IBP function pointer.
 */
template <typename T> using SurfaceTermsFunctionPointer = std::pair<std::vector<T> (*)(const T*, const T*, const std::vector<std::vector<T>>&, const T*),int>;

} // namespace SurfaceTerms
} // namespace FunctionSpace
} // namespace Caravel

#endif // IBP_HELPER_H_INC
