#include <vector>
#include <iostream>
#include <cstdlib>

template <typename T, class coefficientProvider, class integralProvider>
Amplitude<T, coefficientProvider, integralProvider>::Amplitude(coefficientProvider&& cfs)
    : coefficients(std::move(cfs)),
      integrals(coefficients.get_integral_graphs()) {
    // defaults at construction time mu^2 = 1
    integrals.set_mu_squared(T(1));
}

template <typename T, class coefficientProvider, class integralProvider>
Series<T> Amplitude<T, coefficientProvider, integralProvider>::compute(const momD_conf<T, 4>& mom_conf) {
    // initialize to 0
    Series<T> toret(min_n_eps, max_n_eps);

    // get integrals and coefficients from providers
    auto cf_poly = coefficients.compute(mom_conf);
    auto int_fkt = integrals.compute(mom_conf);

    if (cf_poly.size() != int_fkt.size()) {
        std::cout << "ERROR: Coefficient list and integral list have different length --- exit 978" << '\n';
        std::cout << cf_poly.size() << '\n';
        std::cout << int_fkt.size() << '\n';
        std::exit(978);
    }

    // sum up coefficient*integral
    for (size_t i = 0; i < cf_poly.size(); i++) {
        //std::cout <<cf_poly[i].taylor_expand_around_zero(max_n_eps)<<" "<<int_fkt[i] << std::endl;
        toret += cf_poly[i].taylor_expand_around_zero(max_n_eps) * int_fkt[i];
    }
    return toret;
}

namespace ampdetail {

template <typename T, class vcoefficientType, class vintegralType>
Integrals::NumericFunctionBasisExpansion<Series<T>,
Integrals::StdBasisFunction<Integrals::floating_partner_t<T>,
Integrals::floating_partner_t<T>>>
INTEGRATE(const vcoefficientType& coeffs, const vintegralType& ints){
    if (coeffs.size() != ints.size()) {
        std::cout << "ERROR: Coefficient list and integral list have different length" << '\n';
        std::cout << coeffs.size() << '\n';
        std::cout << ints.size() << '\n';
        std::exit(1);
    }

    using SFType = Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>;
    using NFBExpansion = Integrals::NumericFunctionBasisExpansion<Series<T>, SFType>;

    // now assemble the result
    NFBExpansion result;

    for(size_t ii=0;ii<coeffs.size();ii++){
        // We write it as a series from ep^0 .. ep^4 (inclusive) for usage in two-loop computations
        auto scoeff = coeffs[ii].taylor_expand_around_zero(2);
        Series<T> coeff(0,4);
        coeff[0]=scoeff[0];
        coeff[1]=scoeff[1];
        coeff[2]=scoeff[2];
        result+=coeff*ints[ ii ];
    }
    return result;
}

}

template <typename T, class coefficientProvider, class integralProvider>
Integrals::NumericFunctionBasisExpansion<Series<T>,
Integrals::StdBasisFunction<Integrals::floating_partner_t<T>,
Integrals::floating_partner_t<T>>>
Amplitude<T,coefficientProvider,integralProvider>::integrate(const momD_conf<T, 4>& in){
    // compute coefficients
    auto coeffs = coefficients.compute(in);

    std::vector<Integrals::NumericFunctionBasisExpansion<Series<T>, SFType>> SF_int_coeffs_results = integrals.get_abstract_integral(in) ;

    return ampdetail::INTEGRATE<T, decltype(coeffs), decltype(SF_int_coeffs_results)>(coeffs, SF_int_coeffs_results);
}

template <typename T, class coefficientProvider, class integralProvider>
std::function<Integrals::NumericFunctionBasisExpansion<
    Series<T>, Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>>(
    const momD_conf<T, 4>&)>
Amplitude<T, coefficientProvider, integralProvider>::get_new_integrate_evaluator() const {

    auto coeffevaluator = coefficients.get_coeff_evaluator();
    auto intevaluator = integrals.get_int_evaluator();

    std::function<NFBExpansion(const momD_conf<T, 4>&)> lambda = [coeffevaluator = std::move(coeffevaluator), intevaluator = std::move(intevaluator)](const momD_conf<T, 4>& in){
            auto coeffs = coeffevaluator(in);
            auto ints = intevaluator(in);
            return ampdetail::INTEGRATE<T, decltype(coeffs), decltype(ints)>(coeffs, ints);
        };
    return lambda;
}
