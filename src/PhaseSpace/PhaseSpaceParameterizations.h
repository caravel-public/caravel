#pragma once

#include "Core/momD_conf.h"
#include "Core/type_traits_extra.h"
#include "Core/typedefs.h"

#include <exception>
#include <iostream>
#include <limits>
#include <string>

#include "FunctionSpace/MasterIntegrands.h"
#include "Core/Particle.h"
#include "AmplitudeCoefficients/NumericalTools.h"

namespace Caravel {
namespace ps_parameterizations{

/**
 * Interface for rational momentum_configuration for given number of points
 * @param multiplicity
 * @param momentum-twistor variables
 * @param mass indices
 * @param do a random boost
 */
template <typename F> momentumD_configuration<F, 4> rational_mom_conf(size_t n, const std::vector<F>& xs, const std::vector<size_t>& m = std::vector<size_t>(), bool do_boost = true);

/**
 * Four point twistor parameterization
 * 1_mass -> p4 is massive
 * 2_mass -> (p3,p4) are massive
 * 4_mass -> (p1,p2,p3,p4) are massive
 */
template <typename F> momentumD_configuration<F, 4> rational_four_point_mom_conf(const std::vector<F>& xs, bool do_boost = true);
template <typename F> momentumD_configuration<F, 4> rational_four_point_1mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost = true);
template <typename F> momentumD_configuration<F, 4> rational_four_point_2mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost = true);
template <typename F> momentumD_configuration<F, 4> rational_four_point_4mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost = true);
template <typename F>
momentumD_configuration<F, 4> rational_four_point_4mass_soft_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost = true);

/**
 * Parameterization of 5-point massless kinematics using twistor
 * variables. Original formulation can be found in Peraro's paper
 * on functional reconstruction/finite fields.
 *
 * Can return parity flipped momenta which changes the sign of levi_civita(l1,l2,l3,l4)
 */
template <typename F> momentumD_configuration<F, 4> rational_five_point_mom_conf(const std::vector<F>& xs, bool do_boost = true);
/**
 * Parameterization of 5-point massive kinematics using twistor variables. 
 * 1_mass -> p5 is massive
 * 2_mass -> (p4,p5) are massive
 * 3_mass -> (p3,p4,p5) are massive
 */
template <typename F> momentumD_configuration<F, 4> rational_five_point_1mass_mom_conf(const std::vector<F>& xs, const std::vector<F>& m, bool do_boost = true);
template <typename F> momentumD_configuration<F, 4> rational_five_point_2mass_mom_conf(const std::vector<F>& xs, const std::vector<F>& m, bool do_boost = true);
template <typename F> momentumD_configuration<F, 4> rational_five_point_3mass_mom_conf(const std::vector<F>& xs, const std::vector<F>& m, bool do_boost = true);

template <typename F> momentumD_configuration<F, 4> rational_six_point_mom_conf(const std::vector<F>& xs, bool do_boost = true);
template <typename F> momentumD_configuration<F, 4> rational_six_point_1mass_mom_conf(const std::vector<F>& xs, const std::vector<F>& m, bool do_boost = true);



namespace _private{

template <typename F> F rcos(F x) { return (x * x - F(1)) / (x * x + F(1)); }
template <typename F> F rsin(F x) { return (F(2) * x) / (x * x + F(1)); }
template <typename F> F rcosh(F x) { return (x * x + F(1)) / (x * x - F(1)); }
template <typename F> F rsinh(F x) { return (F(2) * x) / (x * x - F(1)); }

template <typename F> std::vector<std::vector<F>> Lxz(F x) {
    using std::vector;
    auto c = rcos(x);
    auto s = rsin(x);
    vector<vector<F>> M = {
        {F(1), F(0), F(0), F(0)},
        {F(0), F(c), F(0), F(s)},
        {F(0), F(0), F(1), F(0)},
        {F(0), F(-s), F(0), F(c)},
    };

    return M;
}

template <typename F> std::vector<std::vector<F>> Lyz(F x) {
    using std::vector;
    if constexpr (is_complex_v<F>) {
        auto c = rcos(x);
        auto s = rsin(x);
        vector<vector<F>> M = {
            {F(1), F(0), F(0), F(0)},
            {F(0), F(1), F(0), F(0)},
            {F(0), F(0), F(c), F(s)},
            {F(0), F(0), F(-s), F(c)},
        };

        return M;
    }
    else{
        auto c = rcosh(x);
        auto s = rsinh(x);
        vector<vector<F>> M = {
            {F(1), F(0), F(0), F(0)},
            {F(0), F(1), F(0), F(0)},
            {F(0), F(0), F(c), F(s)},
            {F(0), F(0), F(s), F(c)},
        };

        return M;
    }
}

template <typename F> std::vector<std::vector<F>> Ltz(F x) {
    using std::vector;
    auto c = rcosh(x);
    auto s = rsinh(x);
    vector<vector<F>> M = {
        {F(c), F(0), F(0), F(s)}, {F(0), F(1), F(0), F(0)}, {F(0), F(0), F(1), F(0)}, {F(s), F(0), F(0), F(c)},
    };

    return M;
}

template <typename F> std::vector<std::vector<F>> Ltx(F x) {
    using std::vector;
    auto c = rcosh(x);
    auto s = rsinh(x);
    vector<vector<F>> M = {
        {F(c), F(s), F(0), F(0)}, {F(s), F(c), F(0), F(0)}, {F(0), F(0), F(1), F(0)}, {F(0), F(0), F(0), F(1)},
    };

    return M;
}


template <typename F> void boost(momentumD_configuration<F, 4>& k) {
    auto boost1 = Ltz(F(rand()) / F(RAND_MAX));
    auto boost2 = Lyz(F(rand()) / F(RAND_MAX));
    auto boost3 = Lxz(F(rand()) / F(RAND_MAX));
    auto boost4 = Ltx(F(rand()) / F(RAND_MAX));

    auto matrix_product = [](const std::vector<std::vector<F>>& M, const std::vector<F>& x) {
        std::vector<F> result;

        auto sc_prod = [](auto& x, auto& y) {
            F result(0);
            for (size_t i = 0; i < x.size(); i++) { result += x.at(i) * y.at(i); }
            return result;
        };

        for (auto& row : M) { result.push_back(sc_prod(row, x)); }
        return result;
    };

    momentumD_configuration<F, 4> r;
    for (auto& it : k) { r.insert(matrix_product(boost4, matrix_product(boost3, matrix_product(boost2, matrix_product(boost1, it.get_vector()))))); }
    k = std::move(r);
}

template <typename F> bool _quick_zero_check(const F& v) {
    if constexpr (is_floating_point_v<F>) {
        using std::abs;
        if (abs(v) > std::numeric_limits<remove_complex_t<F>>::epsilon() * remove_complex_t<F>{10000}) { return false; }
    }
    else if constexpr (is_exact_v<F>) {
        if (v != F(0)) { return false; }
    }
    else {
        std::cerr << "Logical type error in ps_parameterizations::_quick_zero_check(.)" << std::endl;
        std::exit(1);
    }
    return true;
}

template <typename F> void assert_on_shell_mom_conservation(momentumD_configuration<F, 4>& result_conf, const std::vector<F>& m = std::vector<F>{}) {
    momD<F, 4> tot{};
    if (m.size() != 0 && m.size() != result_conf.size()) {
        std::cerr << "Mass vector of size " << m.size() << " for " << result_conf.size() << "-point configuration!" << std::endl;
        exit(1);
    }

    std::vector<F> mass = std::vector<F>(result_conf.size(), F(0));
    if (m.size() > 0) mass = m;

    for (unsigned n = 0; n < result_conf.size(); n++) {

        momD<F, 4> mom = result_conf[n + 1];

        if (!_quick_zero_check(mom * mom - mass[n] * mass[n])) {
            std::cerr << "On-shellness of " << result_conf.size() << "-point configuration failed." << std::endl;
            std::cout << "mom = " << n + 1 << "\t" << mom << "\t" << mom * mom << "\t" << mass[n] * mass[n] << std::endl;
            exit(1);
        }
        tot += mom;
    }

    for (auto& pi : tot) {
        if (!_quick_zero_check(pi)) {
            std::cerr << "Momentum conservation failure in " << result_conf.size() << "-point configuration." << std::endl;
            std::cout << "Sum p_i = " << tot << std::endl;
            exit(1);
        }
    }
}

} // namespace _private


template <typename F, size_t D> void print_mom_conf_invariants(const momentumD_configuration<F, D>& psp, const std::vector<F>& mass = std::vector<F>(), std::ostream& o = std::cout){
    o << "--" << "\n";
    o << "Momentum configuration:\n" << psp << "\n";

    using namespace FunctionSpace;

    momentumD<F,D> sum;
    for (unsigned i = 1; i <= psp.size(); i++) {
        sum += psp[i];
        for (unsigned j = i; j <= psp.size(); j++) {
            if (is_exact<F>::value){
                if (i == j) {
                    if( mass.size() > 0 ){
                      assert(psp[i] * psp[j] == mass[i-1]*mass[i-1]);
                    } else { 
                      assert(psp[i] * psp[j] == (F)0);
                    }
                    continue;
                }
            }
            o << "s" << i << j << " = " << (psp[i] + psp[j]) * (psp[i] + psp[j]) << "\n";
        }
    }
    o << "momentum conservation:" << sum << "\n";
    o << "tr5 = " << F(4)*MasterIntegrands::levi(psp[1], psp[2], psp[3], psp[4]) << "\n";
    o << "sign_tr5 = " << MasterIntegrands::sign_tr5(psp[1], psp[2], psp[3], psp[4]) << "\n";
    o << "--" << std::endl;
}



// Four point twistor parameterization
template <typename F> momentumD_configuration<F, 4> rational_four_point_mom_conf(const std::vector<F>& xs, bool do_boost) {

    F s(xs.at(0));
    F t(xs.at(1));

    std::vector<F> k1 {F(1), F(1), F(-1), F(1)};
    std::vector<F> k2 {s / F(4), F(0), F(0), -s / F(4)};
    std::vector<F> k3 {-s / F(4) + t / s, F(-1) + t / F(4), F(1) + t / F(4), s / F(4) + t / s};
    std::vector<F> k4 {F(-1) - t / s, t / F(-4), t / F(-4), F(-1) - t / s};

    momentumD_configuration<F,4> result_conf(k1,k2,k3,k4);

    if(do_boost){ _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf);

    DEBUG( print_mom_conf_invariants(result_conf));

    return result_conf;
}

/**
 * Parametrization of 4-point - 1 mass kinematics using twistor variables
 */
template <typename F> momentumD_configuration<F, 4> rational_four_point_1mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {
    assert( m.size() == 1);
    F s(xs.at(0));
    F t(xs.at(1));
    F m4 = ParticleMass::particle_mass_container.at(m[0]-1).get_mass(F(1));
    F m4sq = m4*m4;

    std::vector<momentumD<F, 4>> moms(4);
    moms[0] = momentumD<F, 4>(F(-1) / F(2), (t - m4sq) / (F(2) * t), (m4sq / t - F(1)) / F(2), F(1) / F(2));
    moms[1] = momentumD<F, 4>(-s / F(2), s / F(2), -s / F(2), -s / F(2));
    moms[2] = momentumD<F, 4>((s - t - t / s) / F(2), (-s + t - F(1)) / F(2), (s + t + F(1)) / F(2), (s + t + t / s) / F(2));
    moms[3] = momentumD<F, 4>((s + t + s * t) / (F(2) * s), (m4sq - t * t) / (F(2) * t), -(t * t + m4sq) / (F(2) * t), -(s + t + s * t) / (F(2) * s));
    std::vector<F> mass = {F(0), F(0), F(0), m4};

    momentumD_configuration<F,4> result_conf(moms);

    if(do_boost){ _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf, mass);

    DEBUG( print_mom_conf_invariants(result_conf, mass));

    return result_conf;
}

/**
 * Parametrization of 4-point - 2 mass kinematics using twistor variables
 */
template <typename F> momentumD_configuration<F, 4> rational_four_point_2mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {

    assert( m.size() == 2);
    F s(xs.at(0));
    F t(xs.at(1));
    F m3 = ParticleMass::particle_mass_container.at(m[0]-1).get_mass(F(1));
    F m4 = ParticleMass::particle_mass_container.at(m[1]-1).get_mass(F(1));
    F m4sq = m4*m4;
    F m3sq = m3*m3;

    std::vector<momentumD<F, 4>> moms(4);
    moms[0] = momentumD<F, 4>(F(-1) / F(2), m4sq / (F(2) * t), -m4sq / (F(2) * t), F(1) / F(2));
    moms[1] = momentumD<F, 4>(-s / F(2), F(2) * s, F(-2) * s, -s / F(2));
    moms[2] = momentumD<F, 4>((m3sq + s * s - t + F(5) * s * t) / (F(2) * s), (-m3sq + s - F(4) * s * s + t - F(5) * s * t) / (F(2) * s),
                              (m3sq - s + F(4) * s * s - t + F(3) * s * t) / (F(2) * s), (-m3sq + s * s + t - F(3) * s * t) / (F(2) * s));
    moms[3] = momentumD<F, 4>((-m3sq + s + t - F(5) * s * t) / (F(2) * s), (F(-1) + m3sq / s - m4sq / t + F(5) * t - t / s) / F(2),
                              (m4sq * s + t * (s + t - F(3) * s * t - m3sq)) / (F(2) * s * t), (m3sq - s - t + F(3) * s * t) / (F(2) * s));
    std::vector<F> mass = {F(0), F(0), m3, m4};

    momentumD_configuration<F,4> result_conf(moms);

    if(do_boost){ _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf, mass);

    DEBUG( print_mom_conf_invariants(result_conf, mass));

    return result_conf;
}

/**
 * Parametrization of 4-point - 4 mass kinematics using twistor variables
 * The momenta are parametrized by {x,s23} and {m1,m2,m3,m4} with
 * s12 = m3^2 + m4^2 + 2*m3*m4*(1+x^2)/(2*x)
 */
template <typename F> momentumD_configuration<F, 4> rational_four_point_4mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {
    assert(m.size() == 4);
    F x(xs.at(0));
    F s23(xs.at(1));
    F m1 = ParticleMass::particle_mass_container.at(m[0] - 1).get_mass(F(1)), m1sq = m1 * m1;
    F m2 = ParticleMass::particle_mass_container.at(m[1] - 1).get_mass(F(1)), m2sq = m2 * m2;
    F m3 = ParticleMass::particle_mass_container.at(m[2] - 1).get_mass(F(1)), m3sq = m3 * m3;
    F m4 = ParticleMass::particle_mass_container.at(m[3] - 1).get_mass(F(1)), m4sq = m4 * m4;
    std::vector<F> mass{m1, m2, m3, m4};

    std::vector<momD<F, 4>> moms(4);
    moms[0] = momD<F, 4>(F(2) - m2sq + m4sq + m3 * (-m3 + m4 / x - m4 * x) + F(1) / (m3sq + m3 * m4 * x),
                         m1sq + F(2) * m3 * (m3 + m4 * x) - m4 * (F(1) + F(2) * m3 * (m3 + m4 * x)) / (m3 * x) + m2sq * (F(2) + F(1) / (m3sq + m3 * m4 * x)),
                         -m1sq - F(2) * (F(1) + m2sq + m3sq - m4sq) + (m4 + F(2) * m3sq * m4) / (m3 * x) - F(2) * m3 * m4 * x - m2sq / (m3sq + m3 * m4 * x),
                         F(-2) - m2sq + (m4 - m3 * x) * (m3 + m4 * x) / x - F(1) / (m3sq + m3 * m4 * x)) /
              F(2);
    moms[1] = momD<F, 4>(F(-1) + m2sq + F(2) * m3 * (m3 + m4 * x) - F(1) / (m3sq + m3 * m4 * x),
                         F(-1) * (m2sq + m3 * (m3 + m4 * x)) * (F(2) + F(1) / (m3sq + m3 * m4 * x)),
                         F(3) + F(2) * m3 * (m3 + m4 * x) + m2sq * (F(2) + F(1) / (m3sq + m3 * m4 * x)),
                         F(1) + m2sq + F(2) * m3 * (m3 + m4 * x) + F(1) / (m3sq + m3 * m4 * x)) /
              F(2);
    moms[2] = momD<F, 4>(-m3 * (F(1) + (m3 + m4 * x) * (m3 + m4 * x)) / (m3 + m4 * x), F(1) - s23 + m4 * x * (m3 + m2sq / (m3 + m4 * x)),
                         F(-1) + s23 - m3 * m4 * x - m2sq * m4 * x / (m3 + m4 * x), m3 * (-m3 - m4 * x + F(1) / (m3 + m4 * x))) /
              F(2);
    moms[3] = momD<F, 4>(F(-1) - m4sq - m3 * m4 / x + m3 / (m3 + m4 * x),
                         -m1sq + s23 + m4 * (F(2) * m4 + F(1) / (m3 * x) + F(2) * m3 / x - m3 * x - m2sq * x / (m3 + m4 * x)),
                         m1sq - s23 + m3 * m4 * x + m2sq * m4 * x / (m3 + m4 * x) - m4 * (F(1) + F(2) * m3 * (m3 + m4 * x)) / (m3 * x),
                         m4 * (-m4 - m3 / x + x / (m4 * x + m3))) /
              F(2);
    momentumD_configuration<F, 4> result_conf(moms);

    if (do_boost) { _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf, mass);

    DEBUG(print_mom_conf_invariants(result_conf, mass));

    return result_conf;
}

/*
 * p1^2 = p4^2 = m1^2, p2^2 = p3^2 = m2^2
 * Input: {m1b,m2b,x,Sqrt[t]} such that m1 = Sqrt[ m1b^2 + t/4 ] and  sigma = (p1*p2)/(m1*m2) = (1+x^2)/(2x) = (s-m1^2-m2^2)/(2*m1*m2), s = (p1+p2)^2
 */
template <typename F>
momentumD_configuration<F, 4> rational_four_point_4mass_soft_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {
    assert(m.size() == 4);
    assert(xs.size() == 4);
    assert(m[0] == m[3] && m[1] == m[2]);
    F m1b = xs[0];
    F m2b = xs[1];
    F x = xs[2];
    F sqrt_t = xs[3];
    F m1 = ParticleMass::particle_mass_container.at(m[0] - 1).get_mass(F(1));
    F m2 = ParticleMass::particle_mass_container.at(m[1] - 1).get_mass(F(1));
    F m3 = ParticleMass::particle_mass_container.at(m[2] - 1).get_mass(F(1));
    F m4 = ParticleMass::particle_mass_container.at(m[3] - 1).get_mass(F(1));
    std::vector<F> mass{m1, m2, m3, m4};

    momD<F, 4> u1 = momD<F, 4>(F(1), F(0), F(0), F(0));
    momD<F, 4> u2 = momD<F, 4>((F(1) + x * x) / (F(2) * x), F(0), F(0), (F(1) - x * x) / (F(2) * x));
    momD<F, 4> q = momD<F, 4>(F(0), F(0), sqrt_t, F(0));

    std::vector<momD<F, 4>> moms(4);
    moms[0] = -m1b * u1 + q / F(2);
    moms[1] = -m2b * u2 - q / F(2);
    moms[2] = m2b * u2 - q / F(2);
    moms[3] = m1b * u1 + q / F(2);

    momentumD_configuration<F, 4> result_conf(moms);

    if (do_boost) { _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf, mass);

    DEBUG(print_mom_conf_invariants(result_conf, mass));

    return result_conf;
}

/**
 * Parameterization of 5-point massless kinematics using twistor
 * variables. Original formulation can be found in Peraro's paper
 * on functional reconstruction/finite fields.
 */
template <typename F> momentumD_configuration<F, 4> rational_five_point_mom_conf(const std::vector<F>& xs, bool do_boost) {

    using Caravel::la;
    using Caravel::lat;

    std::vector<momentumD<F, 4>> moms(5);

    //// This is badger's phase space point.
    //moms[0] = {-(F(1)/F(2)), F(225)/F(629), F(225)/F(629), -(F(1)/F(2))};
    //moms[1] = {F(1)/F(2), 0, 0, -(F(1)/F(2))};
    //moms[2] = {-(F(115)/F(156)), F(115)/F(156), F(41)/F(156), F(41)/F(156)};
    //moms[3] = {F(107335007)/F(545607180), F(21384421)/F(89783460), F(3631573)/F(5985564), F(21521617)/F(36373812)};
    //moms[4] = {F(958345096)/F(1773223335), -(F(3520219)/F(2640690)), -(F(216053)/F(176046)), F(17200336)/F(118214889)};

    moms[0] = LvBA(lat<F>{F(1), (xs[2] - xs[3]) / xs[2]}, la<F>{F(1), F(0)});
    moms[1] = LvBA(lat<F>{F(0), xs[4]}, la<F>{F(0), F(1)});
    moms[2] = LvBA(lat<F>{xs[4] * xs[2], -xs[4]}, la<F>{F(1) / xs[4], F(1)});
    moms[3] = LvBA(lat<F>{xs[4] * (xs[0] * xs[1] - xs[1] * xs[2] - xs[2]), -(xs[4] * xs[0] * xs[1] * xs[3]) / xs[2]}, la<F>{F(1) / (xs[4]) + F(1) / (xs[4] * xs[0]), F(1)});
    moms[4] = LvBA(lat<F>{xs[4] * xs[1] * (xs[2] - xs[0]), (xs[4] * xs[0] * xs[1] * xs[3]) / xs[2]}, la<F>{F(1) / (xs[4]) + F(1) / (xs[4] * xs[0]) + F(1) / (xs[4] * xs[0] * xs[1]), F(1)});

    momentumD_configuration<F, 4> result_conf(moms);

    if(do_boost){ _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf);

    DEBUG( print_mom_conf_invariants(result_conf));

    return result_conf;
}

template <typename F> momentumD_configuration<F, 4> rational_five_point_1mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {
    assert( m.size() == 1);
    F m5 = ParticleMass::particle_mass_container.at(m[0]-1).get_mass(F(1));
    F m5sq = m5*m5;

    F x1 = xs[0], x2 = xs[1], x3 = xs[2], x4 = xs[3], x5 = xs[4];
    F x6 = x5 * (m5sq * x2 + x3 * (x2 + F(2) * x4 - x5) / F(2)) / (x2 * x3 * x4);

    std::vector<momentumD<F, 4>> moms(5);
    moms[0] = momentumD<F, 4>(F(-1) / F(2), (x5 - x4) / (F(2) * x5), (x4 / x5 - F(1)) / F(2), F(1) / F(2));
    moms[1] = momentumD<F, 4>(-x1 / F(2), F(0), F(0), -x1 / F(2));
    moms[2] = momentumD<F, 4>((x1 - x5 / x1) / F(2), (x5 - F(1)) / F(2), (F(1) + x5) / F(2), (x1 + x5 / x1) / F(2));
    moms[3] = momentumD<F, 4>((x3 + F(2) * (F(1) / x1 + F(1) / x2) * (x5 + x3 * x5 / x2 - x3 * x6)) / F(4),
                              (-x5 - x3 * (x1 + x2 + F(2) * x1 * x5) / (F(2) * x1 * x2) + x3 * x6) / F(2),
                              (x2 * x3 + x1 * (x3 - F(2) * x2 * x5 - F(2) * x3 * x5 + F(2) * x2 * x3 * x6)) / (F(4) * x1 * x2),
                              (x3 - F(2) * (F(1) / x1 + F(1) / x2) * (x5 + x3 * x5 / x2 - x3 * x6)) / F(4));
    moms[4] = momentumD<F, 4>(
        (F(2) * x2 * x3 * (-x5 + x2 * x6) - x1 * (x2 * x2 * (x3 - F(2)) + F(2) * x3 * x5 + F(2) * x2 * (x5 - x3 * x6))) / (F(4) * x1 * x2 * x2),
        (x3 / x1 + F(2) * x4 / x5 + (x3 + F(2) * x3 * x5) / x2 - F(2) * x3 * x6) / F(4),
        (-x3 / x1 + x3 * (F(2) * x5 - F(1)) / x2 - F(2) * (x4 + x3 * x5 * x6) / x5) / F(4),
        (F(2) * x2 * x3 * (x5 - x2 * x6) + x1 * (-x2 * x2 * (F(2) + x3) + F(2) * x3 * x5 + F(2) * x2 * (x5 - x3 * x6))) / (F(4) * x1 * x2 * x2));
    std::vector<F> mass = { F(0), F(0), F(0), F(0), m5 };

    momentumD_configuration<F,4> result_conf(moms);

    if(do_boost){ _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf, mass);

    DEBUG( print_mom_conf_invariants(result_conf, mass));

    return result_conf;
}

template <typename F> momentumD_configuration<F, 4> rational_five_point_2mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {
    assert( m.size() == 2);
    F m4 = ParticleMass::particle_mass_container.at(m[0]-1).get_mass(F(1));
    F m5 = ParticleMass::particle_mass_container.at(m[1]-1).get_mass(F(1));
    F m4sq = m4*m4;
    F m5sq = m5*m5;

    F x1 = xs[0], x2 = xs[1], x3 = xs[2], x4 = xs[3], x5 = xs[4];
    F z1 = -(-F(2) * m5sq * x3 + m4sq * x5 + x2 * x5 + x3 * x5 - x2 * x4 * x5 - x3 * x4 * x5) / (F(2) * x2 * x3);
    F z2 = -(m4sq + x2 + x3 - x2 * x4 - x3 * x4) / (F(2) * x2);

    std::vector<momentumD<F, 4>> moms(5);
    moms[0] = momentumD<F, 4>(-x5 / F(2), (F(1) - z1) / F(2), (-F(1) + z1) / F(2), x5 / F(2));
    moms[1] = momentumD<F, 4>(-(x1 * x4) / F(2), F(0), F(0), -(x1 * x4) / F(2));
    moms[2] = momentumD<F, 4>((-F(1) - x3 / x1 + x4 + x1 * x4) / F(2), (x3 + (F(1) - (F(1) + x1) * x4) / x1) / F(2),
                              (-F(1) + x4 + x1 * (x3 + x4)) / (F(2) * x1), (-F(1) + x3 / x1 + x4 + x1 * x4) / F(2));
    moms[3] = momentumD<F, 4>((x3 * (-x2 + x3) + x1 * (-(x3 * (-F(1) + F(2) * x2 + x4)) + x2 * z2)) / (F(2) * x1 * x3),
                              -(x1 * x3 * x3 - x3 * (-F(1) + x4 + x1 * (-F(1) + x2 + x4)) + (F(1) + F(2) * x1) * x2 * z2) / (F(2) * x1 * x3),
                              (-(x1 * x3 * x3) + x3 * (F(1) + x1 * (F(1) + x2 - x4) - x4) + (F(1) + F(2) * x1) * x2 * z2) / (F(2) * x1 * x3),
                              ((x2 - x3) * x3 + x1 * (x3 + F(2) * x2 * x3 - x3 * x4 + x2 * z2)) / (F(2) * x1 * x3));
    moms[4] =
        momentumD<F, 4>((x5 + x2 * (F(2) + F(1) / x1 - z2 / x3)) / F(2), (-(x1 * x2 * x3) + x1 * x3 * z1 + x2 * z2 + F(2) * x1 * x2 * z2) / (F(2) * x1 * x3),
                        -(x1 * x2 * x3 + x1 * x3 * z1 + x2 * z2 + F(2) * x1 * x2 * z2) / (F(2) * x1 * x3),
                        -(x1 * x3 * x5 + x2 * (x3 + F(2) * x1 * x3 + x1 * z2)) / (F(2) * x1 * x3));
    std::vector<F> mass = { F(0), F(0), F(0), m4, m5 };

    momentumD_configuration<F,4> result_conf(moms);

    if(do_boost){ _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf, mass);

    DEBUG( print_mom_conf_invariants(result_conf, mass));

    return result_conf;
}

template <typename F> momentumD_configuration<F, 4> rational_five_point_3mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {
    //FIXME: Check if the parametrizaton coveres the complete phase space
    DEBUG_MESSAGE("rational_five_point_3mass_mom_conf: Check if Parametrization coveres whole Phase space!");
    assert( m.size() == 3);
    F m3 = ParticleMass::particle_mass_container.at(m[0]-1).get_mass(F(1));
    F m4 = ParticleMass::particle_mass_container.at(m[1]-1).get_mass(F(1));
    F m5 = ParticleMass::particle_mass_container.at(m[2]-1).get_mass(F(1));
    F m3sq = m3*m3, m4sq = m4*m4, m5sq = m5*m5;

    F x1 = xs[0], x2 = xs[1], x3 = xs[2], x4 = xs[3], x5 = xs[4];
    F z1 = (m3sq * (x2 * x2 - x3) * (x2 - x4 + x2 * x4) +
            x1 * (m5sq * x2 * x3 + m4sq * (x2 * x2 * x2 - x2 * x3) + (-F(1) + x2) * x4 * (-x3 * x3 + x2 * x2 * x5))) /
           (x1 * (x2 * x2 - x3) * (x2 - x4 + x2 * x4));
    F z2 = (-(m5sq * x2 * x3) + x4 * ((-F(1) + x2) * x3 * x3 + x2 * x2 * x5 - x2 * x3 * x5)) / ((x2 * x2 - x3) * x4);
    F z3 = -((x1 * (m5sq * x2 * x3 + m4sq * (x2 * x2 * x2 - x2 * x3) + (-F(1) + x2) * x4 * (-x3 * x3 + x2 * x2 * x5))) /
             (m3sq * (x2 * x2 - x3) * (x2 - x4 + x2 * x4) +
              x1 * (m5sq * x2 * x3 + m4sq * (x2 * x2 * x2 - x2 * x3) + (-F(1) + x2) * x4 * (-x3 * x3 + x2 * x2 * x5))));

    std::vector<momentumD<F, 4>> moms(5);
    moms[0] = momentumD<F, 4>(-x3 / F(2), (-x2 + x3) / (F(2) * x3), (-F(1) + x2 / x3) / F(2), x3 / F(2));
    moms[1] = momentumD<F, 4>(-x1 / F(2), F(0), F(0), -x1 / F(2));
    moms[2] = momentumD<F, 4>((x1 * x1 + z1 * z3 + x1 * z1 * (F(1) + z3)) / (F(2) * x1), (-F(1) - z1 * z3) / F(2), (F(1) - z1 * z3) / F(2),
                              (x1 * x1 - z1 * z3 - x1 * z1 * (F(1) + z3)) / (F(2) * x1));
    moms[3] = momentumD<F, 4>(
        ((-F(1) + F(1) / x2) * x4 + (-(x4 * x5) + z2 + x4 * z2) * (F(2) + F(1) / x1 + F(1) / z3) + (F(1) + F(1) / x1 + F(1) / z3) * (-z2 - z1 * z3)) / F(2),
        ((-F(1) + x2) * x4 * z3 + x1 * (-(x4 * (F(1) + F(2) * z3)) + x2 * (x4 + (F(1) + x4 * (F(2) + x5 - z2)) * z3 + z1 * z3 * z3))) / (F(2) * x1 * x2 * z3),
        (-((-F(1) + x2) * x4 * z3) + x1 * (x4 + F(2) * x4 * z3 + x2 * (-x4 - (F(1) + x4 * (F(2) - x5 + z2)) * z3 + z1 * z3 * z3))) / (F(2) * x1 * x2 * z3),
        ((-F(1) + F(1) / x2) * x4 - (-(x4 * x5) + z2 + x4 * z2) * (F(2) + F(1) / x1 + F(1) / z3) + ((x1 + z3 + x1 * z3) * (z2 + z1 * z3)) / (x1 * z3)) / F(2));
    moms[4] = momentumD<F, 4>(
        (x2 * x4 * (x5 - z2) * z3 + x1 * (-(x4 * z3) + x2 * (x4 * (x5 - z2) + x3 * z3 + (x4 + F(2) * x4 * x5 - z2 - F(2) * x4 * z2) * z3))) /
            (F(2) * x1 * x2 * z3),
        (-((-F(1) + x2) * x3 * x4 * z3) + x1 * (x2 * x2 * z3 + x3 * x4 * (F(1) + F(2) * z3) + x2 * x3 * (-x4 + (-F(1) + x4 * (-F(2) - x5 + z2)) * z3))) /
            (F(2) * x1 * x2 * x3 * z3),
        ((-F(1) + x2) * x3 * x4 * z3 + x1 * (-(x2 * x2 * z3) - x3 * x4 * (F(1) + F(2) * z3) + x2 * x3 * (x4 + (F(1) + x4 * (F(2) - x5 + z2)) * z3))) /
            (F(2) * x1 * x2 * x3 * z3),
        (x2 * x4 * (-x5 + z2) * z3 + x1 * (-(x4 * z3) + x2 * (x4 * (-x5 + z2) - x3 * z3 + (x4 - F(2) * x4 * x5 + z2 + F(2) * x4 * z2) * z3))) /
            (F(2) * x1 * x2 * z3));
    std::vector<F> mass = {F(0), F(0), m3, m4, m5};

    momentumD_configuration<F,4> result_conf(moms);

    if(do_boost){ _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf, mass);

    DEBUG( print_mom_conf_invariants(result_conf, mass));

    return result_conf;
}

/**
 * Six point twistor parametrization. From Max's thesis.
 */
template <typename F> momentumD_configuration<F, 4> rational_six_point_mom_conf(const std::vector<F>& xs, bool do_boost) {

    using Caravel::la;
    using Caravel::lat;

    std::vector<momentumD<F, 4>> moms(6);
    moms[0] = LvBA(lat<F>(F(1), -xs[1-1] / xs[8-1]), la<F>(F(1), F(0)));
    moms[1] = LvBA(lat<F>(xs[8-1], F(0)), la<F>(F(0), F(1)));
    moms[2] = LvBA(lat<F>(xs[1-1] * xs[7-1] + -(xs[8-1] * (xs[2-1] + F(1))), xs[1-1] * xs[2-1]), la<F>(F(1) / xs[1-1], F(1)));
    moms[3] = LvBA(lat<F>(xs[2-1] * xs[8-1] + xs[1-1] * xs[3-1] * (xs[5-1] + -xs[7-1]) + -(xs[1-1] * xs[7-1]), xs[1-1] * xs[2-1] * (F(-1) + xs[3-1] * (xs[6-1] + F(-1)))),
                   la<F>((xs[2-1] + F(1)) / (xs[1-1] * xs[2-1]), F(1)));
    moms[4] = LvBA(lat<F>(xs[1-1] * xs[3-1] * (xs[2-1] * xs[4-1] + xs[7-1] + -(xs[5-1] * (xs[4-1] + F(1)))),
                          -(xs[1-1] * xs[2-1] * xs[3-1] * (xs[1-1] * xs[4-1] + xs[8-1] * (xs[6-1] + xs[4-1] * xs[6-1] + F(-1)))) / xs[8-1]),
                   la<F>((F(1) + xs[3-1] * (xs[2-1] + F(1))) / (xs[1-1] * xs[2-1] * xs[3-1]), F(1)));
    moms[5] = LvBA(lat<F>(-(xs[1-1] * xs[3-1] * xs[4-1] * (xs[2-1] + xs[5-1] * F(-1))), xs[1-1] * xs[2-1] * xs[3-1] * xs[4-1] * (xs[6-1] + xs[1-1] / xs[8-1])),
                   la<F>((F(1) + xs[4-1] * (F(1) + xs[3-1] * (xs[2-1] + F(1)))) / (xs[1-1] * xs[2-1] * xs[3-1] * xs[4-1]), F(1)));

    momentumD_configuration<F, 4> result_conf(moms);

    if (do_boost) { _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf);

    DEBUG(print_mom_conf_invariants(result_conf));

    return result_conf;
}
template <typename F> momentumD_configuration<F, 4> rational_six_point_1mass_mom_conf(const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {
    //FIXME: Check if the parametrizaton coveres the complete phase space
    DEBUG_MESSAGE("rational_six_point_1mass_mom_conf: Check if Parametrization coveres whole Phase space!");
    assert( m.size() == 1);
    F m6 = ParticleMass::particle_mass_container.at(m[0]-1).get_mass(F(1));
    F m6sq = m6*m6;

    F x1 = xs[0], x2 = xs[1], x3 = xs[2], x4 = xs[3], x5 = xs[4], x6 = xs[5], x7 = xs[6], x8 = xs[7];
    F z1 = -(((-x4 + m6sq * x5) * x6) / (x4 * x5 * x8));

    std::vector<momentumD<F, 4>> moms(6);
    moms[0] = momentumD<F, 4>(-x8 / F(2), (-F(1) + x5) / (F(2) * x5), (-F(1) + F(1) / x5) / F(2), x8 / F(2));
    moms[1] = momentumD<F, 4>(-(x1 * x7) / F(2), F(0), F(0), -(x1 * x7) / F(2));
    moms[2] = momentumD<F, 4>((-(x6 / x1) + x2 * (-F(1) + x7) + x1 * x7) / F(2), (x2 + x1 * x6 - (x1 + x2) * x7) / (F(2) * x1),
                              (x2 * (-F(1) + x7) + x1 * (x6 + x7)) / (F(2) * x1), (x6 / x1 + x2 * (-F(1) + x7) + x1 * x7) / F(2));
    moms[3] = momentumD<F, 4>((x2 + ((x1 + x2) * (x2 + x3) * x6) / (x1 * x2 * x2) - x2 * x7) / F(2),
                              (-(((x2 + x3) * x6) / x2) + ((x1 + x2) * (-F(1) + x7)) / x1) / F(2),
                              -(x2 * x2 * (-F(1) + x7) + x1 * (x3 * x6 + x2 * (-F(1) + x6 + x7))) / (F(2) * x1 * x2),
                              (x2 - ((x1 + x2) * (x2 + x3) * x6) / (x1 * x2 * x2) - x2 * x7) / F(2));
    moms[4] = momentumD<F, 4>((-(((x2 * x3 + x1 * (x2 + x3)) * (x2 * x4 + x3 * x6)) / (x1 * x2 * x2 * x3)) + (x4 * z1) / x6) / F(2),
                              (x4 + (x3 * x6) / x2 - ((x2 * x3 + x1 * (x2 + x3)) * x4 * z1) / (x1 * x2 * x3 * x6)) / F(2),
                              (x1 * x3 * x6 * (x2 * x4 + x3 * x6) + x2 * x3 * x4 * z1 + x1 * (x2 + x3) * x4 * z1) / (F(2) * x1 * x2 * x3 * x6),
                              (((x2 * x3 + x1 * (x2 + x3)) * (x2 * x4 + x3 * x6)) / (x1 * x2 * x2 * x3) + (x4 * z1) / x6) / F(2));
    moms[5] = momentumD<F, 4>(
        (x4 / x1 + x4 / x2 + x4 / x3 + x8 - (x4 * z1) / x6) / F(2),
        (x2 * x3 * x4 * x5 * z1 + x1 * (x3 * x4 * x5 * z1 + x2 * (x3 * x6 - x3 * x4 * x5 * x6 + x4 * x5 * z1))) / (F(2) * x1 * x2 * x3 * x5 * x6),
        -(x2 * x3 * x4 * x5 * z1 + x1 * (x3 * x4 * x5 * z1 + x2 * (x3 * x6 + x3 * x4 * x5 * x6 + x4 * x5 * z1))) / (F(2) * x1 * x2 * x3 * x5 * x6),
        (-(x4 / x1) - x4 / x2 - x4 / x3 - x8 - (x4 * z1) / x6) / F(2));
    std::vector<F> mass = { F(0), F(0), F(0), F(0), F(0), m6 };

    momentumD_configuration<F,4> result_conf(moms);

    if(do_boost){ _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf, mass);

    DEBUG( print_mom_conf_invariants(result_conf, mass));

    return result_conf;
}

/**
 * Generates rational momentum_configuration for given number of points
 */
template <typename F> momentumD_configuration<F, 4> rational_mom_conf(size_t n, const std::vector<F>& xs, const std::vector<size_t>& m, bool do_boost) {
    using std::to_string;
    using std::string;

    if (n >= 4 && xs.size() != 3 * n - 10) {
        throw std::runtime_error(string("Got ") + to_string(xs.size()) + string(" invariants for momentum configuration of size ") + to_string(n) +
                                 string(". Expected ") + to_string(3 * n - 10));
    }

    switch (m.size()) {
        case 0: {
            switch (n) {
                case 4: return rational_four_point_mom_conf(xs, do_boost);
                case 5: return rational_five_point_mom_conf(xs, do_boost);
                case 6: return rational_six_point_mom_conf(xs, do_boost);
                default: throw std::runtime_error(string("Asked for rational massless mom_conf for unimplemented number of points = ") + to_string(n));
            }
        }
        case 1: {
            switch (n) {
                case 4: return rational_four_point_1mass_mom_conf(xs, m, do_boost);
                case 5: return rational_five_point_1mass_mom_conf(xs, m, do_boost);
                case 6: return rational_six_point_1mass_mom_conf(xs, m, do_boost);
                default: throw std::runtime_error(string("Asked for rational 1mass mom_conf for unimplemented number of points = ") + to_string(n));
            }
        }
        case 2: {
            switch (n) {
                case 4: return rational_four_point_2mass_mom_conf(xs, m, do_boost);
                case 5: return rational_five_point_2mass_mom_conf(xs, m, do_boost);
                default: throw std::runtime_error(string("Asked for rational 2mass mom_conf for unimplemented number of points = ") + to_string(n));
            }
        }
        case 3: {
            switch (n) {
                case 5: return rational_five_point_3mass_mom_conf(xs, m, do_boost);
                default: throw std::runtime_error(string("Asked for rational 3mass mom_conf for unimplemented number of points = ") + to_string(n));
            }
        }
	case 4: {
            switch (n) {
                case 4: return rational_four_point_4mass_mom_conf(xs, m, do_boost);
                default: throw std::runtime_error(string("Asked for rational 4mass mom_conf for unimplemented number of points = ") + to_string(n));
            }
	}
        default: throw std::runtime_error(string("Asked for too many massive particles in mom_conf. Number of massive particles = ") + to_string(m.size()));
    }
}

namespace _private{

template <typename F> Caravel::lat<F> conjugate_spinor(size_t i, const std::vector<la<F>>& tMatrixla, const std::vector<lat<F>>& tMatrixlat) {
    size_t n(tMatrixla.size());

    assert(tMatrixla.size() == tMatrixlat.size());
    assert(i < tMatrixla.size() && i < tMatrixlat.size());

    size_t im1(i);
    size_t ip1(i + 1);
    if (i == 0)
        im1 = n - 1;
    else if (i + 1 == n) {
        im1--;
        ip1 = 0;
    }
    else
        --im1;

    auto csp = (tMatrixla[i] * tMatrixla[ip1]) * tMatrixlat[im1] + (tMatrixla[ip1] * tMatrixla[im1]) * tMatrixlat[i] +
               (tMatrixla[im1] * tMatrixla[i]) * tMatrixlat[ip1];
    csp /= ((tMatrixla[im1] * tMatrixla[i]) * (tMatrixla[i] * tMatrixla[ip1]));
    return csp;
}

} // namespace _private

/**
 * Use a twistor matrix to produce a valid random massless momentum configuration (see e.g. appendix C of https://inspirehep.net/literature/1813359)
 */
template <typename F> momD_conf<F, 4> generic_random_rational_mom_conf(size_t n, bool do_boost = true) {
    using Caravel::la;
    using Caravel::lat;

    std::vector<la<F>> tMatrixla(n, la<F>(F(1), F(0)));
    std::vector<lat<F>> tMatrixlat(n, lat<F>(F(1), F(0)));

    for (size_t ii = 0; ii < n; ii++) {
        tMatrixla[ii] = la<F>(random_scaled_number<F>(F(1)), random_scaled_number<F>(F(1)));
        tMatrixlat[ii] = lat<F>(random_scaled_number<F>(F(1)), random_scaled_number<F>(F(1)));
    }

    std::vector<lat<F>> momlat(n, lat<F>(F(1), F(0)));

    for (size_t ii = 0; ii < n; ii++) momlat[ii] = _private::conjugate_spinor(ii, tMatrixla, tMatrixlat);

    std::vector<momD<F, 4>> moms(n);
    for (size_t ii = 0; ii < n; ii++) moms[ii] = LvBA(momlat[ii], tMatrixla[ii]);

    momD_conf<F, 4> result_conf(moms);

    if (do_boost) { _private::boost(result_conf); }
    _private::assert_on_shell_mom_conservation(result_conf);

    DEBUG(print_mom_conf_invariants(result_conf));

    return result_conf;
}

/**
 * Generates random rational momD_conf for a given number of massless momenta
 */
template <typename F> momD_conf<F, 4> random_rational_mom_conf(size_t n, bool do_boost = true) {
    if (n < 4) { throw std::runtime_error("ERROR: random_rational_mom_conf(.) called for less than 4-pt kinematics!"); }

    // we choose to use the lower-point parametrizations available
    if (n < 7) {
        std::vector<F> xs(3 * n - 10);
        for (size_t ii = 0; ii < 3 * n - 10; ii++) xs[ii] = random_scaled_number<F>(F(1));
        return rational_mom_conf(n, xs, std::vector<size_t>{}, do_boost);
    }
    else
        return generic_random_rational_mom_conf<F>(n, do_boost);
}

template <typename F> momD_conf<F, 4> random_rational_mom_conf(size_t n, const std::vector<size_t>& masses, bool do_boost = true) {
    if (n < 4) { throw std::runtime_error("ERROR: random_rational_mom_conf(.) [massive case] called for less than 4-pt kinematics!"); }

    std::vector<F> xs(3 * n - 10);
    for (size_t ii = 0; ii < 3 * n - 10; ii++) xs[ii] = random_scaled_number<F>(F(1));
    return rational_mom_conf(n, xs, masses, do_boost);
}

#ifdef USE_FINITE_FIELDS
extern template momentumD_configuration<F32, 4> rational_mom_conf(size_t, const std::vector<F32>&, const std::vector<size_t>&, bool);
extern template momentumD_configuration<BigRat, 4> rational_mom_conf(size_t, const std::vector<BigRat>&, const std::vector<size_t>&, bool);
extern template momD_conf<F32, 4> random_rational_mom_conf(size_t, bool);
extern template momD_conf<BigRat, 4> random_rational_mom_conf(size_t, bool);
extern template momD_conf<F32, 4> random_rational_mom_conf(size_t, const std::vector<size_t>&, bool);
extern template momD_conf<BigRat, 4> random_rational_mom_conf(size_t, const std::vector<size_t>&, bool);
#endif

} // namespace ps_parameterizations
} // namespace Caravel

// export the interface to avoid rewriting all existing code
namespace Caravel{
    using ps_parameterizations::rational_mom_conf;
}

