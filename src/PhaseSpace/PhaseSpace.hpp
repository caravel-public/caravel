#include "Core/momD_conf.h"

namespace Caravel{

template <class T,size_t D> Caravel::momentumD_configuration<std::complex<T>,D> to_momDconf(const std::vector<Caravel::momentum<T>>& vec){
    std::vector<Caravel::momD<std::complex<T>,D>> v;

    for(const auto& it: vec){
        v.emplace_back(it[0],it[1],it[2],it[3]);
    }

    return Caravel::momentumD_configuration<std::complex<T>,D>(v);
}

}
