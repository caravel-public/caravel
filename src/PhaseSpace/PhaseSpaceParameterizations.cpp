#include "PhaseSpace/PhaseSpaceParameterizations.h"

namespace Caravel{
namespace ps_parameterizations{
#ifdef USE_FINITE_FIELDS
template momentumD_configuration<F32, 4> rational_mom_conf(size_t, const std::vector<F32>&, const std::vector<size_t>&, bool);
template momentumD_configuration<BigRat, 4> rational_mom_conf(size_t, const std::vector<BigRat>&, const std::vector<size_t>&, bool);
template momD_conf<F32, 4> random_rational_mom_conf(size_t, bool);
template momD_conf<BigRat, 4> random_rational_mom_conf(size_t, bool);
template momD_conf<F32, 4> random_rational_mom_conf(size_t, const std::vector<size_t>&, bool);
template momD_conf<BigRat, 4> random_rational_mom_conf(size_t, const std::vector<size_t>&, bool);
#endif
}
}
