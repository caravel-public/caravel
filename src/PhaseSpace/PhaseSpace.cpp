/**
 *  @file PhaseSpace.cpp
 *
 *  For comments see PhaseSpace.h
 *
 */
#include "PhaseSpace.h"

#include "Core/Utilities.h"
#include "Core/Particle.h"
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif
#include "Core/spinor/spinor.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <signal.h>
#include <stdlib.h>
#include <vector>

#include "misc/DeltaZero.h"

#ifdef INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

double to_double(double x) { return x; }

namespace Caravel {

// WARNING this function should map flavors to mass indices, it does not exist in Caravel, replaced with a dummy
//extern unsigned get_mass_index_from_flavor(unsigned);
unsigned get_mass_index_from_flavor(unsigned i){ return 0;}

template <class T> bool PhaseSpace<T>::no_negative_energy = false;

template <class T> T get_mass(unsigned i){
    return ParticleMass::particle_mass_container.at(i).get_mass(T(0));
}

/*       Template for pi         */
template <class T> inline T pi();

template <> inline R pi() { return M_PI; }
#ifdef HIGH_PRECISION
template <> inline RHP pi() { return dd_real::_pi; }
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline RVHP pi() { return qd_real::_pi; }
#endif
template <> inline C pi() { return C(M_PI, 0.); }
#ifdef HIGH_PRECISION
template <> inline CHP pi() { return CHP(dd_real::_pi, 0.); }
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline CVHP pi() { return CVHP(qd_real::_pi, 0.); }
#endif

#if INSTANTIATE_GMP
template <> inline RGMP pi() { return get_pi(); }
template <> inline std::complex<RGMP> pi() { return std::complex<RGMP>(get_pi(), RGMP(0)); }
#endif

//
// double square_HP(const Caravel::momentum<double>& mom){
// double result;
// dd_real e(mom[0]);
// dd_real x(mom[1]);
// dd_real y(mom[2]);
// dd_real z(mom[3]);
// result = to_double((e-x)*(e+x)-y*y-z*z);
// return result;
//}

/******************************************************************************************
 *  PhaseSpace
 *****************************************************************************************/

// we use normal metric with real types here
template <class T,size_t D> T square(const momentumD<T,D>& a){
    T res(a[0]*a[0]);
    for(unsigned i=1; i<D; i++){
        res -= a[i]*a[i];
    }
    return res;
}

template <class T>
T Caravel::PhaseSpace<T>::solvexi(
    const std::vector<unsigned>& range) { // solves equation F(x)=0 using the secant method
    using std::abs;
    T x1(0.5), x0(1.2);
    unsigned n = 2;
    T x;
    do {
        x = x1 - T(0.5) * (F(x1, range) * (x1 - x0)) / (F(x1, range) - F(x0, range));
        x0 = x1;
        x1 = x;
        n++;
    } while ((abs(F(x1, range)) > Caravel::DeltaZero<T>() * 1e2 &&
              abs((F(x1, range) - F(x0, range))) > Caravel::DeltaZero<T>()) &&
             (n <= 1000));
    return x;
}

template <class T> T Caravel::PhaseSpace<T>::F(T& x, const std::vector<unsigned> range) { // eq (4.3)
    using std::pow;
    T result = w;
    for (size_t i = 0; i < range.size(); i++)
        result -= sqrt(pow(masses[range[i]], 2) + pow(x, 2) * pow(momenta[range[i]][0], 2));
    return result;
}

template <class T> void Caravel::PhaseSpace<T>::generatepart(int s, const std::vector<unsigned>& range) {
    using std::cos;
    using std::sin;
    using std::abs;
    using std::sqrt;
    using std::pow;
    //
    // first we generate ligh-like momenta (eq  3.1)
    Caravel::momentum<T> Q{};
    for (size_t i = 0; i < range.size(); i++) {
        T rho[] = {grnd(), grnd(), grnd(), grnd()};
        T ci = T(2) * rho[0] - 1;
        T phi = T(2) * Caravel::pi<T>() * rho[1];

        T q0 = -log(rho[2] * rho[3]);
        T qx = q0 * sqrt(T(1) - ci * ci) * cos(phi);
        T qy = q0 * sqrt(T(1) - ci * ci) * sin(phi);
        T qz = q0 * ci;

        momenta[range[i]] = Caravel::momentum<T>(q0, qx, qy, qz);
        Q += momenta[range[i]];
    }

    // then we transform them to impose momentum conservation (eq 2.4 - 2.5)
    {
        T M = sqrt(square(Q));
        Caravel::momentum<T> b = -Q / M;
        T x = T(s) * w / M;
        T gamma = Q[0] / M;
        T alpha = T(1) / (T(1) + gamma);

        for (size_t i = 0; i < range.size(); i++) {
            T q0 = (momenta[range[i]][0]), qx = (momenta[range[i]][1]),
              qy = (momenta[range[i]][2]), qz = (momenta[range[i]][3]);
            T bxq = (b[1]) * qx + (b[2]) * qy + (b[3]) * qz;

            T p0 = x * (gamma * q0 + bxq);
            T px = x * (qx + (b[1]) * (q0 + alpha * bxq));
            T py = x * (qy + (b[2]) * (q0 + alpha * bxq));
            T pz = x * (qz + (b[3]) * (q0 + alpha * bxq));

            momenta[range[i]] = Caravel::momentum<T>(p0, px, py, pz);
        }
    }
    // now  transform to massive momenta (eq. 4.2)
    if (!allmassless) {
        T xi = solvexi(range);
        for (size_t i = 0; i < range.size(); i++) {
            T p0 =
                T(s) * sqrt(pow(masses[range[i]], 2) + pow(xi, 2) * pow(momenta[range[i]][0], 2));
            T px = momenta[range[i]][1] * xi;
            T py = momenta[range[i]][2] * xi;
            T pz = momenta[range[i]][3] * xi;

            momenta[range[i]] = Caravel::momentum<T>(p0, px, py, pz);
        }
    }
}

template <class T>
Caravel::PhaseSpace<T>::PhaseSpace(unsigned size, const T& energy, const std::vector<unsigned>& indices,
                                const std::vector<unsigned>& ing)
    : N(size), momenta(size), w(energy), allmassless(true), resetseed(true) {

    if (ing.size() < 2) {
        pos.push_back(0);
        pos.push_back(1);
    } else {
        pos = ing;
    }

    {
        size_t i = 0;
        for (; i < indices.size(); i++) {
            masses.push_back(get_mass<T>(indices[i]));
        }
        if (i != size)
            for (; i < N; i++) masses.push_back(T(0));
    }

    T inm(0);
    for (size_t j = 0; j < pos.size(); j++) { inm += masses[pos[j]]; }
    if (w < inm) {
        std::cerr << "Inconsistent specifications of the phase space: initial energy sqrt(s) "
                     "should be greater than the sum of the masses of ingoing particles!"
                  << std::endl;
        raise(SIGABRT);
    }

    T outm(0);
    for (size_t j = 0; j < N; j++) {
        if (find(pos.begin(), pos.end(), j) == pos.end()) outm += masses[j];
    }
    if (w < outm) {
        std::cerr << "Inconsistent specifications of the phase space: initial energy sqrt(s) is "
                     "not enough to produce required outgoing particles."
                  << std::endl;
        raise(SIGABRT);
    }

    for (unsigned i = 0; i < N; i++) {
        if (indices[i] != 0) {
            allmassless = false;
            break;
        }
    }
}

template <class T>
Caravel::PhaseSpace<T>::PhaseSpace(const T& energy, const std::vector<unsigned>& pro)
    : N(pro.size()), momenta(pro.size()), w(energy), allmassless(true), resetseed(true) {

    pos.push_back(0);
    pos.push_back(1);

    for (unsigned i = 0; i < N; i++) {
        unsigned ind = get_mass_index_from_flavor(pro[i]);
        if (ind != 0) allmassless = false;
        masses.push_back(get_mass<T>(ind));
    }

    T inm(0);
    for (size_t j = 0; j < pos.size(); j++) { inm += masses[pos[j]]; }
    if (w < inm) {
        std::cerr << "Inconsistent specifications of the phase space: initial energy sqrt(s) "
                     "should be greater than the sum of masses of ingoing particles!"
                  << std::endl;
        raise(SIGABRT);
    }

    T outm(0);
    for (size_t j = 0; j < N; j++) {
        if (find(pos.begin(), pos.end(), j) == pos.end()) outm += masses[j];
    }
    if (w < outm) {
        std::cerr << "Inconsistent specifications of the phase space: initial energy sqrt(s) is "
                     "not enough to produce required outgoing particles."
                  << std::endl;
        raise(SIGABRT);
    }
}

template <class T>
Caravel::PhaseSpace<T>::PhaseSpace(unsigned size, const T& energy, const std::vector<T>& set_masses,
                                const std::vector<unsigned>& ing)
    : N(size), momenta(size), w(energy), allmassless(true), resetseed(true) {

    if (ing.size() < 2) {
        pos.push_back(0);
        pos.push_back(1);
    } else {
        pos = ing;
    }

    unsigned i = 0;
    for (; i < set_masses.size(); i++) {
        masses.push_back(set_masses[i]);
        if (set_masses[i] != T(0)) allmassless = false;
    }
    if (i != size)
        for (; i < N; i++) masses.push_back(T(0));

    T inm(0);
    for (size_t j = 0; j < pos.size(); j++) { inm += masses[pos[j]]; }
    if (w < inm) {
        std::cerr << "Inconsistent specifications of the phase space: initial energy sqrt(s) "
                     "should be greater than the sum of the masses of ingoing particles!"
                  << std::endl;
        raise(SIGABRT);
    }

    T outm(0);
    for (size_t j = 0; j < N; j++) {
        if (find(pos.begin(), pos.end(), j) == pos.end()) outm += masses[j];
    }
    if (w < outm) {
        std::cerr << "Inconsistent specifications of the phase space: initial energy sqrt(s) is "
                     "not enough to produce required outgoing particles!"
                  << std::endl;
        raise(SIGABRT);
    }
}

template <class T> void Caravel::PhaseSpace<T>::print_with_presicion() {
    std::cout << "N = " << N << std::endl;
    std::cout << "Using the precision of calculation." << std::endl;
    /*std::cout<<std::setprecision(16);*/
    for (unsigned i = 0; i < N; i++)
        std::cout << i << "\t" << momenta[i] << "            " << square(momenta[i]) << std::endl;
    Caravel::momentum<T> sum{};
    for (unsigned i = 0; i < N; i++) sum += momenta[i];
    std::cout << "Momentum conservation:\t" << sum << std::endl;
}

template <class T> std::vector<Caravel::momentum<double>> Caravel::PhaseSpace<T>::mom_double() const {
    std::vector<Caravel::momentum<double>> dmom;
    for (unsigned i = 0; i < N; i++)
        dmom.push_back(Caravel::momentum<double>(to_double(momenta[i][0]), to_double(momenta[i][1]),
                                            to_double(momenta[i][2]), to_double(momenta[i][3])));
    return dmom;
}

template <class T> void Caravel::PhaseSpace<T>::generate() {
    std::vector<unsigned> outpos;
    for (unsigned i = 0; i < N; i++)
        if (find(pos.begin(), pos.end(), i) == pos.end()) outpos.push_back(i);

    if (resetseed) std::srand(time(NULL));
    generatepart(-1, pos);   // first generate "ingoing" momenta
    generatepart(1, outpos); // then generate "outgoing"

    if (no_negative_energy) {
        for (unsigned i : pos) { momenta[i] *= T(-1); }
    }
}

template <class T> std::ostream& operator<<(std::ostream& s, const Caravel::PhaseSpace<T>& ph) {
    s << std::scientific << std::setprecision(8);
    s << "N = " << ph.N << std::endl;
    std::vector<Caravel::momentum<double>> dmom = ph.mom_double();
    for (unsigned i = 0; i < ph.N; i++)
        s << i << "\t" << dmom[i] << "            " << square(dmom[i]) << std::endl;
    // for (int i = 0; i < ph.N; i++)  s << i <<"\t" <<  dmom[i] << "            " <<
    // square_HP(dmom[i])<<std::endl;  //
    Caravel::momentum<double> sum{};
    for (unsigned i = 0; i < ph.N; i++) sum += dmom[i];
    s << "Momentum conservation:\t" << sum << std::endl;
    return s;
}


template <class T> std::vector<std::vector<T>> Caravel::PhaseSpace<T>::vectors() const {
    using std::vector;
    vector<vector<T>> moms;
    for (size_t i = 0; i < N; i++)
        moms.push_back(vector<T>{momenta[i][0], momenta[i][1], momenta[i][2], momenta[i][3]});
    return moms;
}

template <class T> std::vector<std::vector<double>> Caravel::PhaseSpace<T>::vectors_double() const {
    using std::vector;
    vector<vector<double>> moms;
    std::vector<Caravel::momentum<double>> dmom(mom_double());
    for (size_t i = 0; i < N; i++)
        moms.push_back(vector<double>{dmom[i][0], dmom[i][1], dmom[i][2], dmom[i][3]});
    return moms;
}

template <class T> bool Caravel::PhaseSpace<T>::write_to_file(const std::string& name) {
    std::ofstream ofile;
    ofile.open(name.c_str(), std::ios::out | std::ios::trunc);
    if (ofile.is_open()) {
        ofile << std::scientific << std::setprecision(17);
        for (size_t i = 0; i < N; i++) {
            ofile << momenta[i][0] << " " << momenta[i][1] << " " << momenta[i][2] << " "
                  << momenta[i][3] << " " << std::endl;
        }
        std::cout << "Generated momenta are saved to " << name << std::endl;
        return true;
    } else {
        std::cout << "Unable to write to file " << name << std::endl;
        return false;
    }
}

/********************************************************************************
*    PhaseSpace_factorized
********************************************************************************/

#if 0 

template <class T>
void Caravel::PhaseSpace_factorized<T>::boost_mom(const T& gamma, const std::vector<T>& ni,
                                              Caravel::momentum<T>& p) {
    std::vector<T> p_i;
    p_i.push_back(p[1]);
    p_i.push_back(p[2]);
    p_i.push_back(p[3]);

    T ndp(0), p0;
    T gb = sqrt(gamma * gamma - T(1));

    for (size_t i = 0; i < 3; i++) ndp += ni[i] * p_i[i];
    p0 = gamma * p[0] + gb * ndp;
    for (size_t i = 0; i < 3; i++) {
        p_i.at(i) = (gb * p[0] + ndp * (gamma - T(1))) * ni[i] + p_i[i];
    }

    p = Caravel::momentum<T>(p0, p_i[0], p_i[1], p_i[2]);
}

template <class T>
Caravel::PhaseSpace_factorized<T>::PhaseSpace_factorized(unsigned sizeN, unsigned sizen,
                                                      const T& energy, unsigned legj,
                                                      unsigned massj, const std::vector<unsigned>& ing)
    : N(sizeN), n(sizen), j(legj), mj(massj), resetseed(true) {

    if (massj == 0) {
        std::cerr << "Factorization pole should have non-zero mass! For collinear splitting use "
                     "PhaseSpace_collinear."
                  << std::endl;
        raise(SIGABRT);
    }
    // if (ing.size()<2) {pos.push_back(0);pos.push_back(1);}
    // else {pos=ing;}

    std::vector<unsigned> masses1(N - n, 0);
    if (j == N - n)
        masses1.push_back(mj);
    else
        masses1.insert(masses1.begin() + j, mj); // insert the leg on which amplitude will be
                                                 // factorized such that the order is conserved
    //_PRINT(masses1);

    std::vector<unsigned> masses2(n, 0);
    masses2.insert(masses2.begin(), mj); // insert the same leg as a first
    //_PRINT(masses2);

    std::vector<unsigned> pos;
    for (size_t i = 1; i < n + 1; i++)
        pos.push_back(i); // this is used only to pass a check in PhaseSpace
    ps1 = PhaseSpace<T>(N - n + 1, energy, masses1, ing);
    ps2 = PhaseSpace<T>(n + 1, get_mass<T>(mj), masses2, pos);
}

template <class T>
Caravel::PhaseSpace_factorized<T>::PhaseSpace_factorized(unsigned sizeN, unsigned sizen,
                                                      const T& energy,
                                                      const std::vector<unsigned>& masses, unsigned legj,
                                                      unsigned massj, const std::vector<unsigned>& ing)
    : N(sizeN), n(sizen), j(legj), mj(massj), resetseed(true) {

    // if (ing.size()<2) {pos.push_back(0);pos.push_back(1);}
    // else {pos=ing;}

    std::vector<unsigned> masses1;
    std::vector<unsigned> masses2;
    for (size_t i = 0; i < j; i++) masses1.push_back(masses[i]);
    masses1.push_back(mj);
    masses2.push_back(mj);
    for (size_t i = j; i < j + n; i++) masses2.push_back(masses[i]);
    for (size_t i = j + n; i < N; i++) masses1.push_back(masses[i]);
    //_PRINT(masses1);
    //_PRINT(masses2);

    std::vector<unsigned> pos;
    for (size_t i = 1; i < n + 1; i++)
        pos.push_back(i); // this is used only to pass a check in PhaseSpace
    ps1 = PhaseSpace<T>(N - n + 1, energy, masses1, ing);
    ps2 = PhaseSpace<T>(n + 1, get_mass<T>(mj), masses2, pos);

    ps1.set_reset_seed(false);
    ps2.set_reset_seed(false);
}

template <class T> void Caravel::PhaseSpace_factorized<T>::generate() {
    if (resetseed) std::srand(time(NULL));

    ps1.generate(); // generate usual configuration
    std::vector<unsigned> range;
    for (size_t i = 1; i < n + 1; i++) range.push_back(i);
    ps2.generatepart(
        1,
        range); // generate n momenta starting from the second such that they sum to p={mj,0,0,0};

    // calculate parameters of the boost, which transforms p to p_j
    using std::abs;
    Caravel::momentum<T> q(ps1.momenta[j]);
    T gamma = abs(q[0]) / ps2.w;
    T beta = sqrt(T(1) - T(1) / (gamma * gamma));
    std::vector<T> ni;
    T qob = beta * q[0];
    ni.push_back(q[1] / qob);
    ni.push_back(q[2] / qob);
    ni.push_back(q[3] / qob);

    // T nsq=ni[0]*ni[0]+ni[1]*ni[1]+ni[2]*ni[2];
    //_PRINT(nsq);

    // boost all momenta from configuration 2
    for (size_t i = 1; i < n + 1; i++)
        boost_mom(gamma, ni, ps2.momenta.at(i)); // q = -( p_j + p_{j+1} + ... + p_{j+n-1} )
    ps2.momenta.at(0) = -q;
}

template <class T>
std::ostream& operator<<(std::ostream& s, const Caravel::PhaseSpace_factorized<T>& ph) {
    s << std::scientific << std::setprecision(8);
    s << "Factorization chanel: " << ph.j;
    for (size_t i = 1; i < ph.n; i++) s << "," << ph.j + i;
    s << std::endl;

    s << "Configuration 1:" << std::endl;
    s << ph.ps1;
    s << "Configuration 2:" << std::endl;
    s << ph.ps2;

    s << "Total momentum conservation: ";
    Caravel::momentum<T> sum;
    for (size_t i = 0; i < ph.ps1.N; i++) {
        if (i == ph.j) continue;
        sum += ph.ps1.momenta[i];
    }
    for (size_t i = 1; i < ph.ps2.N; i++) sum += ph.ps2.momenta[i];
    s << sum << std::endl;

    return s;
}

template <class T> std::vector<Caravel::Cmom<double>> Caravel::PhaseSpace_factorized<T>::cmom() {
    std::vector<Caravel::Cmom<double>> outv;
    for (size_t i = 0; i < j; i++) outv.push_back(ps1.cmom_double()[i]);
    for (size_t i = 1; i < n + 1; i++) outv.push_back(ps2.cmom_double()[i]);
    for (size_t i = j + 1; i < N; i++) outv.push_back(ps1.cmom_double()[i]);
    return outv;
}
#endif

/******************************************************************************************
 *  PhaseSpace_collinear
 *****************************************************************************************/

#if 0
template <class T>
Caravel::PhaseSpace_collinear<T>::PhaseSpace_collinear(unsigned sizeN, const T& energy, unsigned legj,
                                                    const T& angle, const std::vector<unsigned>& ing)
    : N(sizeN), j(legj), phi(angle), resetseed(true) {

    std::vector<unsigned> masses1(N - 1, 0);
    ps1 = PhaseSpace<T>(N - 1, energy, masses1, ing);
    ps2 =
        PhaseSpace<T>(3, 1000); // 1000 -- is just a random number, it will not be used to generate

    ps1.set_reset_seed(false);
}

template <class T>
Caravel::PhaseSpace_collinear<T>::PhaseSpace_collinear(unsigned sizeN, const T& energy,
                                                    const std::vector<unsigned>& masses, unsigned legj,
                                                    const T& angle, const std::vector<unsigned>& ing)
    : N(sizeN), j(legj), phi(angle), resetseed(true) {

    if (masses[j] != 0 or masses[j + 1] != 0) {
        std::cerr << "Inconsistent specifications of the phase space: collinear factorization "
                     "should be produced by two massless legs!"
                  << std::endl;
        raise(SIGABRT);
    }

    std::vector<unsigned> masses1;
    for (size_t i = 0; i < j; i++) masses1.push_back(masses[i]);
    masses1.push_back(0);
    for (size_t i = j + 2; i < N - 1; i++) masses1.push_back(masses[i]);

    ps1 = PhaseSpace<T>(N - 1, energy, masses1, ing);
    ps2 =
        PhaseSpace<T>(3, 1000); // 1000 -- is just a random number, it will not be used to generate

    ps1.set_reset_seed(false);
}

template <class T> void Caravel::PhaseSpace_collinear<T>::generate() {
    if (resetseed) std::srand(time(NULL));

    ps1.generate();

    Caravel::momentum<T> q(ps1.momenta[j]); // find momentum, which splits
    T z = ps1.grnd();                  // chose randomly a fraction of momentum
    Caravel::momentum<T> p1 = z * q;
    rotate_mom(p1); // rotate momentum a little, so the limit p1*p2-->0 can be approached smoothly
    Caravel::momentum<T> p2 = q - p1;

    ps1.momenta.at(0) = -q;
    ps1.momenta.at(1) = p1;
    ps1.momenta.at(2) = p2;
}

template <class T>
void Caravel::PhaseSpace_collinear<T>::rotate_mom(
    Caravel::momentum<T>& p) { // rotate momentum p about y axes
    using std::cos;
    T pe = p[0], px = p[1], py = p[2], pz = p[3];
    px = px * cos(phi) + pz * sin(phi);
    pz = pz * cos(phi) - p[1] * sin(phi);
    p = Caravel::momentum<T>(pe, px, py, pz);
}

template <class T>
std::ostream& operator<<(std::ostream& s, const Caravel::PhaseSpace_collinear<T>& ph) {
    s << std::scientific << std::setprecision(8);
    s << "Collinear splitting in the channel: " << ph.j << "," << ph.j + 1 << std::endl;
    s << "Main configuration:" << std::endl;
    s << ph.ps1;
    s << "Factorized configuration:" << std::endl;
    s << ph.ps2;

    s << "Total momentum conservation: ";
    Caravel::momentum<T> sum;
    for (size_t i = 0; i < ph.ps1.N; i++) {
        if (i == ph.j) continue;
        sum += ph.ps1.momenta[i];
    }
    for (size_t i = 1; i < ph.ps2.N; i++) sum += ph.ps2.momenta[i];
    s << sum << std::endl;

    return s;
}
#endif

//********************************************************************************
#define _inst(T)\
template class PhaseSpace<T>;\
template std::ostream& operator<<(std::ostream&, const PhaseSpace<T>&);\

_inst(double)
#ifdef HIGH_PRECISION
_inst(dd_real)
#endif
#ifdef VERY_HIGH_PRECISION
_inst(qd_real)
#endif

#undef _inst

/*
 *template class PhaseSpace_factorized<T>;\
 *template class PhaseSpace_collinear<T>;\
 *template std::ostream& operator<<(std::ostream&, const PhaseSpace_factorized<T>&);\
 *template std::ostream& operator<<(std::ostream&, const PhaseSpace_collinear<T>&);
 */

}
