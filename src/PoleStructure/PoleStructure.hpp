#include "FunctionalReconstruction/DensePolynomial.h"
#include "IntegralLibrary/SpecialFunctions/Power.h"
#include "IntegralLibrary/SpecialFunctions/Zeta.h"
#include "IntegralLibrary/SpecialFunctions/Pi.h"
#include "IntegralLibrary/SpecialFunctions/PentagonFunctionsZurich.h"
#include "IntegralLibrary/SpecialFunctions/Icomplex.h"
#include "misc/MiscMath.h"
#include "wise_enum.h"

namespace Caravel{
namespace PoleStructure{
  using Integrals::StdBasisFunction;
  using Integrals::NumericFunctionBasisExpansion;
  using Caravel::Integrals::prodpow;
  using Caravel::Integrals::Power;
  using Caravel::Integrals::Pi;
  using Caravel::Integrals::zeta;
  using Caravel::Integrals::pentagon_f_1_1;
  using Caravel::Integrals::icomplex;

  /**
   * \beta_0/N_c
   * Will NOT do any checks.
   */
  template <typename F> inline F beta0(unsigned nf) {
      switch(nf){
          case 0: return F(11) / F(3);
          case 1: return F(-2)/F(3);
          default: return F(0);
      }
  }

  /**
   * \beta_1/N_c^2
   * Will NOT do any checks.
   */
  template <typename F> inline F beta1(unsigned nf) {
      switch(nf){
          case 0: return F(17) / F(3);
          case 1: return F(-13)/F(6);
          default: return F(0);
      }
  }

  /**
   * Implementation of Exp[EulerGamma*ep]/Gamma[1-ep]/ep
   */
  template <typename F, typename Out>
  Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<Out,Out>> weightedPrefactorH(size_t weight){
    if (weight > 3){
      throw std::runtime_error("Error in weightedPrefactor: prefactor not implemented to high enough weight:" + std::to_string(weight));
    }

    static const Series<F> one(-1, 3, std::vector<F>{F(1)});
    static const StdBasisFunction<Out,Out> T1([](std::vector<Out>){return Out(1);}, "T(1)", {});

    static const Series<F> ep2(1, 3, std::vector<F>{F(1)});
    static const StdBasisFunction<Out,Out> pi2([](std::vector<Out>){return prodpow(Pi<Out>,2);}, "prodpow(Pi<Out>,2)", {});

    static const Series<F> ep3(2, 3, std::vector<F>{F(1)});
    static const StdBasisFunction<Out,Out> zeta3([](std::vector<Out>){return zeta<Out>(3);}, "zeta<Out>(3)", {});

    static const Series<F> ep4(3, 3, std::vector<F>{F(1)});
    static const StdBasisFunction<Out,Out> pi4([](std::vector<Out>){return prodpow(Pi<Out>,4);}, "prodpow(Pi<Out>,4)", {});

    Integrals::SemiAnalyticExpression<F,Out> factor({{T1, one}, {pi2, ep2*F(1)/F(-12)}, {zeta3, ep3*F(-1)/F(3)}, {pi4, ep4*F(1)/F(1440)}});

    if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new) {
	static const StdBasisFunction<Out,Out> bci12([](std::vector<Out>){return -prodpow(Pi<Out>,2) ;}, "prodpow(bci<Out>(1),2)", {});
	static const StdBasisFunction<Out,Out> bci14([](std::vector<Out>){return prodpow(Pi<Out>,4);}, "prodpow(bci<Out>(1),4)", {});
	static const StdBasisFunction<Out,Out> bc8([](std::vector<Out>){return zeta<Out>(3);}, "bc<Out>(8)", {});
	factor = Integrals::SemiAnalyticExpression<F,Out>({{T1, one}, {bci12, ep2*F(1)/F(12)}, {bc8, ep3*F(-1)/F(3)}, {bci14, ep4*F(1)/F(1440)}});
    }

    for (auto& el : factor){
      el.second.truncate(weight);
    }


    return factor;
  }


  /**
   * Implementation of Exp[EulerGamma*ep]/Gamma[1-ep]
   */
  template <typename F, typename Out>
  Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<Out,Out>> weightedPrefactor(size_t weight){
    if (weight > 4){
      throw std::runtime_error("Error in weightedPrefactor: prefactor not implemented to high enough weight:" + std::to_string(weight));
    }

    static const Series<F> one(0, 4, std::vector<F>{F(1)});
    static const StdBasisFunction<Out,Out> T1([](std::vector<Out>){return Out(1);}, "T(1)", {});

    static const Series<F> ep2(2, 4, std::vector<F>{F(1)});
    static const StdBasisFunction<Out,Out> pi2([](std::vector<Out>){return prodpow(Pi<Out>,2);}, "prodpow(Pi<Out>,2)", {});

    static const Series<F> ep3(3, 4, std::vector<F>{F(1)});
    static const StdBasisFunction<Out,Out> zeta3([](std::vector<Out>){return zeta<Out>(3);}, "zeta<Out>(3)", {});

    static const Series<F> ep4(4, 4, std::vector<F>{F(1)});
    static const StdBasisFunction<Out,Out> pi4([](std::vector<Out>){return prodpow(Pi<Out>,4);}, "prodpow(Pi<Out>,4)", {});

    Integrals::SemiAnalyticExpression<F,Out> factor({{T1, one}, {pi2, ep2*F(1)/F(-12)}, {zeta3, ep3*F(-1)/F(3)}, {pi4, ep4*F(1)/F(1440)}});

    if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new) {
	static const StdBasisFunction<Out,Out> bci12([](std::vector<Out>){return -prodpow(Pi<Out>,2) ;}, "prodpow(bci<Out>(1),2)", {});
	static const StdBasisFunction<Out,Out> bci14([](std::vector<Out>){return prodpow(Pi<Out>,4);}, "prodpow(bci<Out>(1),4)", {});
	static const StdBasisFunction<Out,Out> bc8([](std::vector<Out>){return zeta<Out>(3);}, "bc<Out>(8)", {});
	factor = Integrals::SemiAnalyticExpression<F,Out>({{T1, one}, {bci12, ep2*F(1)/F(12)}, {bc8, ep3*F(-1)/F(3)}, {bci14, ep4*F(1)/F(1440)}});
    }

    for (auto& el : factor){
      el.second.truncate(weight);
    }


    return factor;
  }

  /**
   * Implementation of Exp[-EulerGamma*ep]*Gamma[1-2*ep]/Gamma[1-ep]
   */
  template <typename F, typename Out>
  Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<Out,Out>> weightedPrefactor2(size_t weight){
    if (weight > 4){
      throw std::runtime_error("Error in weightedPrefactor2: prefactor not implemented to high enough weight:" + std::to_string(weight));
    }

    Series<F> one(0, weight + 1, std::vector<F>{F(1)});
    StdBasisFunction<Out,Out> T1([](std::vector<Out>){return Out(1);}, "T(1)", {});

    Series<F> ep2(2, weight + 1, std::vector<F>{F(1)});
    StdBasisFunction<Out,Out> pi2([](std::vector<Out>){return prodpow(Pi<Out>,2);}, "prodpow(Pi<Out>,2)", {});

    Series<F> ep3(3, weight + 1, std::vector<F>{F(1)});
    StdBasisFunction<Out,Out> zeta3([](std::vector<Out>){return zeta<Out>(3);}, "zeta<Out>(3)", {});

    Series<F> ep4(4, weight + 1, std::vector<F>{F(1)});
    StdBasisFunction<Out,Out> pi4([](std::vector<Out>){return prodpow(Pi<Out>,4);}, "prodpow(Pi<Out>,4)", {});

    Integrals::SemiAnalyticExpression<F,Out> factor({{T1, one}, {pi2, ep2*F(1)/F(4)}, {zeta3, ep3*F(7)/F(3)}, {pi4, ep4*F(7)/F(96)}});

    if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new) {
	static const StdBasisFunction<Out,Out> bci12([](std::vector<Out>){return -prodpow(Pi<Out>,2) ;}, "prodpow(bci<Out>(1),2)", {});
	static const StdBasisFunction<Out,Out> bci14([](std::vector<Out>){return prodpow(Pi<Out>,4);}, "prodpow(bci<Out>(1),4)", {});
	static const StdBasisFunction<Out,Out> bc8([](std::vector<Out>){return zeta<Out>(3);}, "bc<Out>(8)", {});
	factor = Integrals::SemiAnalyticExpression<F,Out>({{T1, one}, {bci12, ep2*F(-1)/F(4)}, {bc8, ep3*F(7)/F(3)}, {bci14, ep4*F(7)/F(96)}});
    }

    return factor;
  }


  template <typename F, typename TF>
  NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<TF,TF>> exponential(const DensePolynomial<F>& coeff, const StdBasisFunction<TF,TF>& func, size_t max_order){

    Series<F> coeff_series(0, max_order, coeff.coefficients);

    StdBasisFunction<TF,TF> working_function([](std::vector<TF> in){return TF(1);}, "T(1)", {});
    Series<F> working_coeff(0, max_order, std::vector<F>{F(1)});

    std::unordered_map<StdBasisFunction<TF,TF>, Series<F>> result_map;

    for (size_t i = 0; i < max_order; i++){
      if (i > 0) {working_coeff /= i;}
      result_map[working_function]  = working_coeff;
      working_function *= func;
      working_coeff *= coeff_series;
    }

    return {result_map};

  }
  template <typename F, typename TF>
  NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<TF,TF>> to_minus_epsilon(std::string var, size_t expansion_power){
      StdBasisFunction<TF,TF> log([](std::vector<TF> in){return TF(1);}, "G<Out>(T(0),args[0]*T(-1))", {var});

      DensePolynomial<F> minus_epsilon({0, F(-1)}, "ep");

      return exponential(minus_epsilon, log, expansion_power);
  }

  template <typename F, typename Out>
  NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<Out, Out>> F_1_1_to_minus_epsilon(std::string var, size_t expansion_power) {

	  std::unordered_map<StdBasisFunction<Out, Out>, Series<F>> result_map;
      Series<F> working_coeff(0, expansion_power, std::vector<F>{F(1)});

	  bool iPi=false;
	  if (PhysRegSelector::get_n() > 0 && (var == "s12" || var == "s34" || var == "s45" || var == "s35"))
		  iPi=true;

	  std::map<std::string, std::string> invInd{{"s12", "1"}, {"s23", "2"}, {"s34", "3"}, {"s45", "4"}, {"s15", "5"}, {"s13", "6"}, {"s24", "7"}, {"s35", "8"}, {"s14", "9"}, {"s25", "10"}};

	  for (int i = 0; i <= int(expansion_power); i++) {
		  if (i > 0) { working_coeff *= Series<F>(0, expansion_power, std::vector<F>{F(0), F(-1) / F(i)}); }
		  std::string func;
		  if (i == 0) {
			  func = "T(1)";
			  result_map[StdBasisFunction<Out, Out>([](const std::vector<Out>& in) { return Out(1); }, func, {var})] = working_coeff;
		  }
		  else if (i == 1) {
			  if (!iPi)
				  func = "F<Out>(1,1,"+invInd[var]+")";
			  else
				  func = "Fac<Out>(1,1,"+invInd[var]+")";
			  result_map[StdBasisFunction<Out, Out>([](const std::vector<Out>& in) { std::cout<<"WARNING: try to evaluate new pentagon functions"<<std::endl; return Out(0); }, func, {var})] = working_coeff;
		  }
		  else {
			  if (!iPi)
				  func = "prodpow(F<Out>(1,1," + invInd[var] + ")," + std::to_string(i) + ")";
			  else
				  func = "prodpow(Fac<Out>(1,1," + invInd[var] + ")," + std::to_string(i) + ")";
			  result_map[StdBasisFunction<Out, Out>([](const std::vector<Out>& in) { std::cout<<"WARNING: try to evaluate new pentagon functions"<<std::endl; return Out(0); }, func, {var})] = working_coeff;
		  }
	  }

	  return {result_map};
  }
  template <typename F, typename Out>
  NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<Out, Out>> f_1_1_to_minus_epsilon(std::string var, size_t expansion_power) {

      std::unordered_map<StdBasisFunction<Out, Out>, Series<F>> result_map;
      Series<F> working_coeff(0, expansion_power, std::vector<F>{F(1)});

      for (int i = 0; i <= int(expansion_power); i++) {
          if (i > 0) { working_coeff *= Series<F>(0, expansion_power, std::vector<F>{F(0), F(-1) / F(i)}); }
          std::string func;
          if (i == 0) {
              func = "T(1)";
              result_map[StdBasisFunction<Out, Out>([](const std::vector<Out>& in) { return Out(1); }, func, {var})] = working_coeff;
          }
          else if (i == 1) {
              func = "pentagon_f_1_1<Out>(" + var + ")";
              result_map[StdBasisFunction<Out, Out>([](const std::vector<Out>& in) { return std::log(in[0]); }, func, {var})] = working_coeff;
          }
          else {
              func = "prodpow(pentagon_f_1_1<Out>(" + var + ")," + std::to_string(i) + ")";
              result_map[StdBasisFunction<Out, Out>([i](const std::vector<Out>& in) { return prodpow(std::log(in[0]), i); }, func, {var})] =
                  working_coeff;
          }
      }

      return {result_map};
  }
  template <typename F, typename TF>
  NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<TF,TF>> goncharov_to_minus_epsilon(std::string var, size_t expansion_power){

      std::unordered_map<StdBasisFunction<TF,TF>, Series<F>> result_map;
      for (size_t i = 0; i < expansion_power; i++){
        Series<F> coeff(i, expansion_power, std::vector<F>{prod_pow(F(-1),i)});
        if (i==0){
          result_map[StdBasisFunction<TF,TF>([](std::vector<TF> in){return TF(1);}, "T(1)", {var})] = coeff;
        }
        else{
          std::string func = "G<Out>(";
          for (size_t j = 0; j < i; j++){
            func += "T(0),";
          }
          func +="args[0]*T(-1))";
          result_map[StdBasisFunction<TF,TF>([](std::vector<TF> in){return TF(1);}, func, {var})] = coeff;
        }
      }

      return {result_map};
  }

  namespace detail {
  template <typename F, typename Out> NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<Out,Out>> get_catani_gamma(cataniGtype type, unsigned nf, int max_order) {
      std::map<cataniGtype, std::vector<F>> gammas_0 = {
          {cataniGtype::gg, std::vector<F>{F(-1), F(-1) / F(2) * beta0<F>(0)}},
          {cataniGtype::qQ, std::vector<F>{F(-1), F(-3) / F(2)}},
          {cataniGtype::qq, {F(0)}},
          {cataniGtype::gq, std::vector<F>{F(-1), F(-1) / F(4) * (beta0<F>(0) + F(3))}},
	  {cataniGtype::yg, {F(0)}},
	  {cataniGtype::yq, {F(0)}},
	  {cataniGtype::yy, {F(0)}}
      };
      std::map<cataniGtype, std::vector<F>> gammas_1 = {
          {cataniGtype::gg, std::vector<F>{F(0), F(-1) / F(2) * beta0<F>(1)}},
          {cataniGtype::qq, {F(0)}},
          {cataniGtype::qQ, {F(0)}},
          {cataniGtype::gq, std::vector<F>{F(0), F(-1) / F(4) * beta0<F>(1)}},
	  {cataniGtype::yg, {F(0)}},
	  {cataniGtype::yq, {F(0)}},
	  {cataniGtype::yy, {F(0)}}
      };
      Series<F> coeff;
      switch (nf) {
          case 0: coeff=Series<F>(-2,max_order,gammas_0.at(type)); break;
          case 1: coeff=Series<F>(-2,max_order,gammas_1.at(type)); break;
          default: coeff=Series<F>(-2, max_order);
      }
      NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<Out,Out>> result_map;
      result_map[StdBasisFunction<Out,Out>([](std::vector<Out>){return Out(1);}, "T(1)", {})] = coeff;
      return {result_map};
  }
  inline std::vector<cataniGtype> get_catani_gamma_types(const PartialAmplitudeInput& pa) {
      static auto is_quark = [](const Particle& p) {
          return in_container<ParticleFlavor::q>(p.get_flavor()) or in_container<ParticleFlavor::qb>(p.get_flavor());
      };
      static auto is_gluon = [](const Particle& p) { return p.get_flavor() == ParticleFlavor::gluon; };

      static auto is_photon = [](const Particle& p) { return p.get_flavor() == ParticleFlavor::photon; };

      static auto same_flavor = [](const Particle& p1, const Particle& p2) {
          return p1.get_flavor() == p2.get_flavor() or p1.get_anti_flavor() == p2.get_flavor();
      };

      std::vector<cataniGtype> result;

      const auto& particles = pa.get_particles();
      const size_t N = pa.get_multiplicity();

      size_t colored=0;
      for(auto&p:particles)
	  if(p.is_colored())
	      colored++;

      for (size_t i = 0; i < N; ++i) {
          auto& p = particles.at(i);
          auto& pn = particles.at((i + 1) % N);

          if (is_gluon(p) and is_gluon(pn))
              result.push_back(cataniGtype::gg);
          else if (is_quark(p) and is_quark(pn)) {
              if (!same_flavor(p, pn))
                  result.push_back(cataniGtype::qQ);
	      else {
		  if(colored==2)
		      result.push_back(cataniGtype::qQ);
		  else
		      result.push_back(cataniGtype::qq);
	      }
          }
          else if (is_gluon(p) and is_quark(pn))
              result.push_back(cataniGtype::gq);
          else if (is_quark(p) and is_gluon(pn))
              result.push_back(cataniGtype::gq);
          else if (is_photon(p) and is_photon(pn))
              result.push_back(cataniGtype::yy);
          else if (is_photon(p) and is_quark(pn))
              result.push_back(cataniGtype::yq);
          else if (is_quark(p) and is_photon(pn))
              result.push_back(cataniGtype::yq);
          else if (is_photon(p) and is_gluon(pn))
              result.push_back(cataniGtype::yg);
          else if (is_gluon(p) and is_photon(pn))
              result.push_back(cataniGtype::yg);
          else {
              _WARNING_R("Unknown case encountered for ", pa);
              std::exit(1);
          }
      }

      assert(result.size() == N);

      return result;
  }

  } // namespace detail

  template <typename F, typename TF>
  Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF, TF>> cataniI1Operator(size_t max_order, unsigned nf,
                                                                                                          const std::vector<cataniGtype>& types) {


      static const std::vector<std::string> invariant_names=types.size()==5?std::vector<std::string>{"s12","s23","s34","s45","s15"}:std::vector<std::string>{"s12","s14","s12","s14"};

      Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF, TF>> sum;

      for (size_t i = 0; i < types.size(); ++i) {
	  sum += detail::get_catani_gamma<F,TF>(types.at(i), nf, max_order) * (types.size() == 5 ? (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new ? F_1_1_to_minus_epsilon<F,TF> : f_1_1_to_minus_epsilon<F,TF>) : goncharov_to_minus_epsilon<F,TF>)(invariant_names.at(i), max_order + 2);
      }

      auto fac = weightedPrefactor<F,TF>(max_order + 2);
      // i1
      auto res = fac * sum;
      return res;
  }

  /**
   * Rescales series in x to series in a*x
   */
  template<typename F>
  Series<F> rescale_parameter(Series<F> s, F a){
      for (int i = s.leading(); i < s.last(); i++){
        if (i < 0){s[i] /= prod_pow(a, size_t(-i));}
        if (i > 0){s[i] *= prod_pow(a, size_t(i));}
      }

      return s;
  }

  namespace detail {
  template <typename F, typename Out, size_t max_order = 0> inline Integrals::SemiAnalyticExpression<F, Out> cataniHg(unsigned nf) {
      using Expr = Integrals::SemiAnalyticExpression<F, Out>;
      static const StdBasisFunction<Out,Out> zeta3([](std::vector<Out>){return zeta<Out>(3);}, "zeta<Out>(3)", {});
      static const StdBasisFunction<Out,Out> pi2([](std::vector<Out>){return prodpow(Pi<Out>,2);}, "prodpow(Pi<Out>,2)", {});
      static const Series<F> one(0, max_order + 1, std::vector<F>{F(1)});
      static const StdBasisFunction<Out,Out> constant([](std::vector<Out>){return Out(1);}, "T(1)", {});

    if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new) {
	static const StdBasisFunction<Out,Out> bci12([](std::vector<Out>){return -prodpow(Pi<Out>,2) ;}, "prodpow(bci<Out>(1),2)", {});
	static const StdBasisFunction<Out,Out> bc8([](std::vector<Out>){return zeta<Out>(3);}, "bc<Out>(8)", {});
	switch (nf) {
	case 0: return Expr{{{bc8, one / F(2)}, {bci12, one * F(-11) / F(144)}, {constant, one * F(5) / F(12)}}};
	case 1: return Expr{{{bci12, one * F(1) / F(72)}, {constant, one * F(-89) / F(108)}}};
	case 2: return Expr{{{constant, one * F(5) / F(27)}}};
	default: return Expr{};
	}
    }

      switch (nf) {
          case 0: return Expr{{{zeta3, one / F(2)}, {pi2, one * F(11) / F(144)}, {constant, one * F(5) / F(12)}}};
          case 1: return Expr{{{pi2, one * F(-1) / F(72)}, {constant, one * F(-89) / F(108)}}};
          case 2: return Expr{{{constant, one * F(5) / F(27)}}};
          default: return Expr{};
      }
  }

  template <typename F, typename Out, size_t max_order = 0> inline Integrals::SemiAnalyticExpression<F, Out> cataniHq(unsigned nf) {
      using Expr = Integrals::SemiAnalyticExpression<F, Out>;
      static const StdBasisFunction<Out, Out> zeta3([](std::vector<Out>) { return Out(1); }, "zeta<Out>(3)", {});
      static const StdBasisFunction<Out, Out> pi2([](std::vector<Out>) { return Out(1); }, "Pi<Out>*Pi<Out>", {});
      static const Series<F> one(0, max_order + 1, std::vector<F>{F(1)});
      static const StdBasisFunction<Out,Out> constant([](std::vector<Out>){return Out(1);}, "T(1)", {});

      if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new) {
	  static const StdBasisFunction<Out,Out> bci12([](std::vector<Out>){return -prodpow(Pi<Out>,2) ;}, "prodpow(bci<Out>(1),2)", {});
	  static const StdBasisFunction<Out,Out> bc8([](std::vector<Out>){return zeta<Out>(3);}, "bc<Out>(8)", {});
	  switch (nf) {
	  case 0: return Expr{{{bc8, one *F(7)/ F(4)}, {bci12, one * F(11) / F(96)}, {constant, one * F(409) / F(864)}}};
	  case 1: return Expr{{{bci12, one * F(-1) / F(48)}, {constant, one * F(-25) / F(216)}}};
	  default: return Expr{};
	  }
      }

      switch (nf) {
          case 0: return Expr{{{zeta3, one *F(7)/ F(4)}, {pi2, one * F(-11) / F(96)}, {constant, one * F(409) / F(864)}}};
          case 1: return Expr{{{pi2, one * F(1) / F(48)}, {constant, one * F(-25) / F(216)}}};
          default: return Expr{};
      }
  }

  template <typename F, typename Out, size_t max_order = 0> inline Integrals::SemiAnalyticExpression<F, Out> cataniK(unsigned nf) {
      using Expr = Integrals::SemiAnalyticExpression<F, Out>;
      static const StdBasisFunction<Out, Out> pi2([](std::vector<Out>) { return Out(1); }, "Pi<Out>*Pi<Out>", {});
      static const Series<F> one(0, max_order + 2, std::vector<F>{F(1)});
      static const StdBasisFunction<Out,Out> constant([](std::vector<Out>){return Out(1);}, "T(1)", {});

      if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new) {
	  static const StdBasisFunction<Out,Out> bci12([](std::vector<Out>){return -prodpow(Pi<Out>,2) ;}, "prodpow(bci<Out>(1),2)", {});
	  switch (nf) {
          case 0: return Expr{{{bci12, one/F(3)}, {constant, one*F(67)/F(9)}}};
          case 1: return Expr{{{constant, one*F(-10)/F(9)}}};
          default: return Expr{};
	  }
      }

      switch (nf) {
          case 0: return Expr{{{pi2, one/F(-3)}, {constant, one*F(67)/F(9)}}};
          case 1: return Expr{{{constant, one*F(-10)/F(9)}}};
          default: return Expr{};
      }
  }

  } // namespace detail

  template <typename F, typename TF>
  NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<TF,TF>> cataniI2Operator(int max_order, const PartialAmplitudeInput& pa){
    Series<F> beta0OverEp_0(-1, 3 + max_order, std::vector<F>{beta0<F>(0)});
    Series<F> beta0OverEp_1(-1, 3 + max_order, std::vector<F>{beta0<F>(1)});
    auto fac2 = weightedPrefactor2<F,TF>(3);
    auto fac1 = weightedPrefactor<F,TF>(3);
    auto facH = weightedPrefactorH<F,TF>(3);

    const unsigned nf = pa.get_nf_powers();
    const unsigned n_gluons = pa.get_number_of_gluons();
    const unsigned n_quarks = 2*pa.get_number_of_quarks();

    auto i2 = Series<F>{-1, 3 + max_order, std::vector<F>{F(n_gluons)}} *detail::cataniHg<F,TF>(nf);
    i2 += Series<F>{-1, 3 + max_order, std::vector<F>{F(n_quarks)}} *detail::cataniHq<F,TF>(nf);
    auto gtypes = detail::get_catani_gamma_types(pa);

    switch (nf) {
        case 0: {
            auto i1 = cataniI1Operator<F,TF>(max_order + 2, 0, gtypes);
            auto i1twoep = i1;
            for (auto& term : i1twoep) { term.second = rescale_parameter(term.second, F(2)); }
            i2 += F(-1) / F(2) * (i1 * i1) + beta0OverEp_0 * (fac2 * i1twoep - i1) + fac2 * detail::cataniK<F, TF>(0) * i1twoep;
            break;
        }

        case 1: {
            auto i1_0 = cataniI1Operator<F,TF>(max_order + 2, 0, gtypes);
            auto i1_1 = cataniI1Operator<F,TF>(max_order + 2, 1, gtypes);
            auto i1twoep_0 = i1_0;
            for (auto& term : i1twoep_0) { term.second = rescale_parameter(term.second, F(2)); }
            auto i1twoep_1 = i1_1;
            for (auto& term : i1twoep_1) { term.second = rescale_parameter(term.second, F(2)); }

            i2 += F(-1) * (i1_0 * i1_1);
            i2 += beta0OverEp_0 * (fac2 * i1twoep_1 - i1_1);
            i2 += beta0OverEp_1 * (fac2 * i1twoep_0 - i1_0);

            i2 += fac2 * detail::cataniK<F, TF>(1) * i1twoep_0;
            i2 += fac2 * detail::cataniK<F, TF>(0) * i1twoep_1;

            break;
        }
        case 2: {
            auto i1 = cataniI1Operator<F,TF>(max_order + 2, 1, gtypes);
            auto i1twoep = i1;
            for (auto& term : i1twoep) { term.second = rescale_parameter(term.second, F(2)); }
            i2 += F(-1) / F(2) * (i1 * i1) + beta0OverEp_1 * (fac2 * i1twoep - i1) + fac2 * detail::cataniK<F, TF>(1) * i1twoep;
            break;
        }
    }

    // Drop zero terms
    i2.select([](const std::pair<StdBasisFunction<TF,TF>,Series<F>>& term){return !(term.second == F(0));});

    for (auto& el : i2){el.second.truncate(max_order);}
    return i2;

  }

  template <typename F, typename TF>
  Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF,TF>> finiteOneLoopPoleOperator(size_t max_order){

    Series<F> minusOne(-2, max_order, std::vector<F>{F(-1)});
    auto prefactor = minusOne;

    if(settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new ) {
	auto invariant_sum = F_1_1_to_minus_epsilon<F,TF>("s12", max_order + 2)
                           + F_1_1_to_minus_epsilon<F,TF>("s23", max_order + 2)
                           + F_1_1_to_minus_epsilon<F,TF>("s34", max_order + 2)
                           + F_1_1_to_minus_epsilon<F,TF>("s45", max_order + 2)
                           + F_1_1_to_minus_epsilon<F,TF>("s15", max_order + 2);
	// i1
	return prefactor * invariant_sum;
    } else {
	auto invariant_sum = f_1_1_to_minus_epsilon<F,TF>("s12", max_order + 2)
                           + f_1_1_to_minus_epsilon<F,TF>("s23", max_order + 2)
                           + f_1_1_to_minus_epsilon<F,TF>("s34", max_order + 2)
                           + f_1_1_to_minus_epsilon<F,TF>("s45", max_order + 2)
                           + f_1_1_to_minus_epsilon<F,TF>("s15", max_order + 2);
	// i1
	return prefactor * invariant_sum;
    }
  }

  template <typename F, typename TF> Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF, TF>> poleJ1Operator(size_t max_order, unsigned nf, const std::vector<cataniGtype>& gtypes,int la) {

      using NFBE = NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<TF, TF>>;
      std::unordered_map<StdBasisFunction<TF, TF>, Series<F>> one_map;
      one_map[StdBasisFunction<TF, TF>([](std::vector<TF> in) { return TF(1); }, "T(1)", {})] = Series<F>(0, max_order + 2, std::vector<F>{F(1)});
      NFBE one(one_map);

      auto i1 = cataniI1Operator<F,TF>(max_order,nf,gtypes);

      auto b2 = one * Series<F>(-1, max_order + 1, std::vector<F>{F(-(la+2)) / F(2) * beta0<F>(nf)});

      return i1 - b2;

}

  template <typename F, typename TF> Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF, TF>> poleJ1Operator1L(size_t max_order, unsigned nf, const std::vector<cataniGtype>& gtypes,int la) {

      using NFBE = NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<TF, TF>>;
      std::unordered_map<StdBasisFunction<TF, TF>, Series<F>> one_map;
      one_map[StdBasisFunction<TF, TF>([](std::vector<TF> in) { return TF(1); }, "T(1)", {})] = Series<F>(0, max_order + 2, std::vector<F>{F(1)});
      NFBE one(one_map);

      auto i1 = cataniI1Operator<F,TF>(max_order,nf,gtypes);

      auto b2 = one * Series<F>(-1, max_order + 1, std::vector<F>{F(-la) / F(2) * beta0<F>(nf)});

      return i1 - b2;

}

template <typename F, typename TF>
Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF,TF>> poleJ2Operator(size_t max_order, const PartialAmplitudeInput& pa){
  // One as a NumericFunctionBasisExpansion
  using NFBE = NumericFunctionBasisExpansion<Series<F>, StdBasisFunction<TF,TF>>;
  std::unordered_map<StdBasisFunction<TF,TF>, Series<F>> one_map;
  one_map[StdBasisFunction<TF,TF>([](std::vector<TF> in){return TF(1);}, "T(1)", {})] = Series<F>(0,4+max_order, std::vector<F>{F(1)});
  NFBE one(one_map);

  int la=2*pa.get_number_of_quarks()+pa.get_number_of_gluons()-2;

  const unsigned nf = pa.get_nf_powers();
  auto gtypes = detail::get_catani_gamma_types(pa);

  auto i2 = cataniI2Operator<F,TF>(max_order,pa);

  switch(nf){
      case 0:
          {
              auto b2TwoLoop = one * Series<F>(-2, max_order + 2, std::vector<F>{F(la*(la+2)) / F(8) * (beta0<F>(0) * beta0<F>(0)), F(-la) / F(2) * beta1<F>(0)});
              auto b1OneLoop = one * Series<F>(-1, max_order + 2, std::vector<F>{F(-la) / F(2) * beta0<F>(0)});
              auto i1b1 = cataniI1Operator<F,TF>(max_order + 1, 0, gtypes) * b1OneLoop;
              i2 += i1b1 - b2TwoLoop;
              break;
          }
      case 1:
          {
              auto b2TwoLoop = one * Series<F>(-2, max_order + 2, std::vector<F>{F(la*(la+2)) / F(4) * (beta0<F>(0) * beta0<F>(1)), F(-la) / F(2) * beta1<F>(1)});
              auto b1OneLoop_0 = one * Series<F>(-1, max_order + 2, std::vector<F>{F(-la) / F(2) * beta0<F>(0)});
              auto b1OneLoop_1 = one * Series<F>(-1, max_order + 2, std::vector<F>{F(-la) / F(2) * beta0<F>(1)});
              auto i1b1 = cataniI1Operator<F,TF>(max_order + 1, 0, gtypes) * b1OneLoop_1;
              i1b1 += cataniI1Operator<F,TF>(max_order + 1, 1, gtypes) * b1OneLoop_0;
              i2 += i1b1 - b2TwoLoop;
              break;
          }
        case 2:
          {
              auto b2TwoLoop = one * Series<F>(-2, max_order + 2, std::vector<F>{F(15) / F(8) * (beta0<F>(1) * beta0<F>(1))});
              auto b1OneLoop = one * Series<F>(-1, max_order + 2, std::vector<F>{F(-3) / F(2) * beta0<F>(1)});
              auto i1b1 = cataniI1Operator<F,TF>(max_order + 1, 1, gtypes) * b1OneLoop;
              i2 += i1b1 - b2TwoLoop;
              break;
          }
  }

  return i2;
}

}
}
