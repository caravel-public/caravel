#include "PoleStructure/PoleStructure.h"
#include <stdexcept>
#include <cassert>
#include "Core/Utilities.h"

#include "Core/typedefs.h"

namespace Caravel {

template <typename F>
bool check_if_poles_vanish(const Integrals::NumericFunctionBasisExpansion<Series<F>, std::string>& input){
    size_t n_of_non_matching = 0;
    size_t total_N = 0;
    for (const auto& entry: input){
        bool matched = true;
        for(int i = entry.second.leading(); i < 0 && i <= entry.second.last(); ++i){
            if (!(entry.second[i] == F(0))){
                std::cout << "Poles not cancelling for function " << entry.first << std::endl;
                std::cout << "ep^" << i << ": " << entry.second[i] << std::endl;
                matched = false;
            }
            else
                if( verbosity_settings.remainder_pole_check )
                    _MESSAGE("ep^",i," pole cancels for function ",entry.first);
        }
        if(!matched) n_of_non_matching++;
        total_N++;
    }

    if(n_of_non_matching>0){
        _WARNING(n_of_non_matching,"/",total_N," basis functions have uncancelled poles.");
        return false;
    }

    return true;
}

template <typename F>
Integrals::SemiAnalyticExpression<F, C> remove_poles(Integrals::SemiAnalyticExpression<F,C>&& input) {
    //auto input = _input;
    // Check if there is something remaining after dropping the poles.
    if (input.last() < 0){
        throw std::runtime_error("Input only consists of poles. Nothing would remain after removing them.");
    }


    // For each coefficient, drop the poles and only keep the ep^0 coefficient.
    for (auto& entry: input){
        entry.second = Series<F>(0, 0, std::vector<F>({entry.second[0]}));
    }

    return input;
}

// Extract the finite pieces from a function basis expansion.
template <typename F>
std::vector<F> extract_finite(Integrals::NumericFunctionBasisExpansion<Series<F>, std::string>&& input) {
    std::vector<F> to_return;
    for (const auto& elem: input) {
        if (elem.second[0] != 0){
            to_return.push_back(elem.second[0]);
        }
    }
    return to_return;
}

namespace FiniteRemainder{

template <typename F, typename TF>
TwoLoopRemainder<F, TF>::TwoLoopRemainder(const Model& model, const PartialAmplitudeInput& pa, NormalizationVanishingTreeType n) : TwoLoopRemainder(model, pa) {
    norm_vtr = n;
}

template <typename F, typename TF>
TwoLoopRemainder<F, TF>::TwoLoopRemainder(const Model& model, const PartialAmplitudeInput& pa)
    : partial_amplitude{pa}, vanishing_tree(is_vanishing_tree(pa)), two_loop_coeffs(model, pa), two_loop_integrals( two_loop_coeffs.get_integral_graphs()) {
    if (pa.get_multiplicity() != 5) {
        _WARNING_R("Only implemented for 5-point amplitudes so far!");
        std::exit(1);
    }

    //FIXME: this is a bit awkward, we need to set this setting for one loop construction, because
    // it doesn't use Ds decomposition
    auto restore_setting = settings::BG::partial_trace_normalization.value;
    settings::BG::partial_trace_normalization.value = settings::BG::trI::full_Ds;

    switch (pa.get_nf_powers()) {
        case 0:
            one_loop_ampl.emplace_back(typename OneLoopAmplType::CoefficientProviderType{std::make_shared<Model>(model), pa.get_copy_with_nf_powers_set_to(0)});
            break;
        case 1:
            one_loop_ampl.emplace_back(typename OneLoopAmplType::CoefficientProviderType{std::make_shared<Model>(model), pa.get_copy_with_nf_powers_set_to(0)});
            one_loop_ampl.emplace_back(typename OneLoopAmplType::CoefficientProviderType{std::make_shared<Model>(model), pa.get_copy_with_nf_powers_set_to(1)});
            break;
        case 2:
            one_loop_ampl.emplace_back(typename OneLoopAmplType::CoefficientProviderType{std::make_shared<Model>(model), pa.get_copy_with_nf_powers_set_to(1)});
            // we add this just to normalize by it as per our convention
            if(vanishing_tree){
                one_loop_ampl.emplace_back(typename OneLoopAmplType::CoefficientProviderType{std::make_shared<Model>(model), pa.get_copy_with_nf_powers_set_to(0)});
                norm_pos = 1;
            }
            break;
        default: _WARNING_R(pa, " asks for two many Nf powers!"); std::exit(1);
    }

    settings::BG::partial_trace_normalization.value = restore_setting;

    for (auto& it : one_loop_ampl) { it.set_laurent_range(-2, 4); }

    construct_catani_operators();
}

template <typename F, typename TF> void TwoLoopRemainder<F, TF>::construct_catani_operators(){
    const unsigned nf = partial_amplitude.get_nf_powers();

    if (!vanishing_tree) { cataniJ2evaluator = PoleStructure::poleJ2Operator<F,TF>(0, partial_amplitude); }

    auto gtypes = PoleStructure::detail::get_catani_gamma_types(partial_amplitude);

    switch (nf) {
        case 0:
            cataniJ1evaluators.resize(1);
            cataniJ1evaluators[0] = PoleStructure::poleJ1Operator<F,TF>(2, 0,gtypes,2*partial_amplitude.get_number_of_quarks()+partial_amplitude.get_number_of_gluons()-2);
            break;
        case 1:
	    if(partial_amplitude.get_number_of_photons()) {
		cataniJ1evaluators.resize(1);
		cataniJ1evaluators[0] = PoleStructure::poleJ1Operator<F,TF>(2, 1,gtypes,2*partial_amplitude.get_number_of_quarks()+partial_amplitude.get_number_of_gluons()-2);
	    }
	    else {
            cataniJ1evaluators.resize(2);
		cataniJ1evaluators[0] = PoleStructure::poleJ1Operator<F,TF>(2, 1,gtypes,2*partial_amplitude.get_number_of_quarks()+partial_amplitude.get_number_of_gluons()-2);
		cataniJ1evaluators[1] = PoleStructure::poleJ1Operator<F,TF>(2, 0,gtypes,2*partial_amplitude.get_number_of_quarks()+partial_amplitude.get_number_of_gluons()-2);
            // NOTE the sign from Nf loop
            cataniJ1evaluators[1] *= F(-1);
	    }
            break;
        case 2:
            cataniJ1evaluators.resize(1);
            cataniJ1evaluators[0] = PoleStructure::poleJ1Operator<F,TF>(2, 1,gtypes);
            // NOTE the sign from Nf loop
            cataniJ1evaluators[0] *= F(-1);
            break;
        default: break;
    }
}

template <typename F, typename TF> void TwoLoopRemainder<F, TF>::update_cardinality() {
    uint64_t current_cardinality = cardinality_update_manager<F>::get_instance()->get_current_cardinality();
    if (last_cardinality != current_cardinality) {
        last_cardinality = current_cardinality;
        construct_catani_operators();
    }
}

template <typename F, typename TF> typename TwoLoopRemainder<F, TF>::OutputType TwoLoopRemainder<F, TF>::evaluate_subtraction(const momentumD_configuration<F, 4>& conf) {
    // NOTE. Dropping of functions is computed phase space point by
    // phase space point, so this is not suitable for functional
    // reconsturction.


    // update catani operators if cardinality had changed since the last evaluation
    update_cardinality();

    OutputType result{};

    std::vector<OutputType> one_loop;

    for (size_t i = 0; i < cataniJ1evaluators.size(); ++i) {
        one_loop.push_back(one_loop_ampl[i].integrate(conf));
        result += one_loop.back() * cataniJ1evaluators[i];
    }

    if (vanishing_tree ) {
        if (norm_vtr == NormalizationVanishingTreeType::one_loop_nf0) {
            static const Integrals::StdBasisFunction<TF, TF> T1([](std::vector<TF>) { return TF(1); }, "T(1)", {});
            if (norm_pos) one_loop.push_back(one_loop_ampl[norm_pos].integrate(conf));
            auto coeff = one_loop[norm_pos].get_coeff(T1);
            assert(coeff.leading() == -2);
            assert(coeff(-2) == F(0));
            assert(coeff(-1) == F(0));

            auto norm = coeff(0);

            assert(norm != F(0));

            result /= norm;
        }
    }
    else {
        result += cataniJ2evaluator;
    }

    for (auto& el : result) { el.second.truncate(0); }
    result.select([](auto& el){
       return !(el.second == 0);
    });

    return result;
}

template <typename F, typename TF> typename TwoLoopRemainder<F, TF>::OutputType TwoLoopRemainder<F, TF>::operator()(const momentumD_configuration<F, 4>& conf) {
    // update catani operators if cardinality had changed since the last evaluation
    update_cardinality();

    OutputType result{};

    START_TIMER(two_loop_ampl);
    {
        auto mi_coeffs_2L = two_loop_coeffs.compute(conf);
        auto result_integrals_2L = two_loop_integrals.get_abstract_integral(conf);
        for (auto [i,ci] : enumerate(to_Series(mi_coeffs_2L))) {
            result += ci * result_integrals_2L[i];
        }
    }
    STOP_TIMER(two_loop_ampl);

    /// FIXME: why does this fudge factor have to be here??
    if(partial_amplitude.get_number_of_photons()) result*=F(-1);

    result -= evaluate_subtraction(conf);

    for (auto& el : result) { el.second.truncate(0); }

    if(zeros_dropped){
        for(auto& it : non_zero_functions){
            it.second = result.get_coeff(it.first);
        }
    }
    else{
        for(auto& it : result){
            if(!(it.second == 0)) non_zero_functions[it.first] = it.second;
        }
        zeros_dropped=true;
    }

    return non_zero_functions;
}

template <typename F, typename TF> F TwoLoopRemainder<F, TF>::get_normalization(const momentumD_configuration<F, 4>& conf) {
    if (!vanishing_tree or (vanishing_tree and norm_vtr==NormalizationVanishingTreeType::spinor_weight)) return static_cast<F>(1);

    update_cardinality();

    static const Integrals::StdBasisFunction<TF, TF> T1([](std::vector<TF>) { return TF(1); }, "T(1)", {});
    auto coeff = one_loop_ampl[norm_pos].integrate(conf).get_coeff(T1);
    assert(coeff.leading() == -2);
    assert(coeff(-2) == F(0));
    assert(coeff(-1) == F(0));
    auto norm = coeff(0);

    assert(norm != F(0));

    return norm;
}

#ifdef USE_FINITE_FIELDS
extern template class TwoLoopRemainder<F32, C>;
#endif

} // namespace FiniteRemainder
} // namespace Caravel
