#ifndef REMAINDER_H_INC
#define REMAINDER_H_INC

#include "Amplitude.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "IntegralLibrary/FunctionBasisExpansion.h"
#include "AmpEng/CoefficientEngine.h"
#include "IntegralLibrary/IntegralProvider.h"
#include "PoleStructure/PoleStructure.h"
#include "Core/type_traits_extra.h"

#include <functional>
#include <string>
#include <vector>

#include "Core/CaravelInput.h"

namespace Caravel {

/**
 * Check whether the poles vanish.
 *
 * This is important in the remainder.
 *
 * @param input     The SemiAnalyticExpression from which the vanishing of the poles shall be checked.
 * @return          true if the poles vanish, else false.
 */
template <typename F> bool check_if_poles_vanish(const Integrals::NumericFunctionBasisExpansion<Series<F>, std::string>& input);

/**
 * In a given SemiAnalyticExpression `input` remove the poles and only
 * keep the ep^0 term.
 *
 * @param input The SemiAnalyticExpression where the poles shall be removed.
 */
template <typename F> Integrals::SemiAnalyticExpression<F, C> remove_poles(Integrals::SemiAnalyticExpression<F, C>&& input);

/// Extract the finite pieces from a function basis expansion.
template <typename F>
std::vector<F> extract_finite(Integrals::NumericFunctionBasisExpansion<Series<F>, std::string>&& input);

namespace FiniteRemainder {

template <typename F, typename TF> class TwoLoopRemainder {
  public:
    static_assert(is_finite_field<F>::value, "Implemented only for finite field types so far!");
    using OutputType = Integrals::SemiAnalyticExpression<F, TF>;
    using theIntProvider = Integrals::IntegralProvider<Integrals::FBCoefficient<F, F>, 
                                                                     Integrals::StdBasisFunction<Integrals::floating_partner_t<F>, Integrals::floating_partner_t<F>>>;
    using OneLoopAmplType = Amplitude<F, AmpEng::CoefficientEngine<F>, theIntProvider>;

    /**
     * In the vanishing tree case, this represens the choice of removing the spinor weight by
     * either spinor_weight or the corresponding 1-loop Nf^0 amplitude.
     */
    WISE_ENUM_CLASS_MEMBER(NormalizationVanishingTreeType, spinor_weight, one_loop_nf0)

  private:
    const PartialAmplitudeInput partial_amplitude;
    const bool vanishing_tree{false};

    bool zeros_dropped{false};
    /**
     * This contains a map which we construct on the first run with only non-zero elements.
     * Later we just reassign the values of coefficients to not repeat the construction.
     * Of course we assume that vanishing functions will not change.
     */
    OutputType non_zero_functions;

    unsigned norm_pos{0}; // which of one-loop amplitudes use to normalize
    IntegrandHierarchy::ColourExpandedAmplitude<F, 2> two_loop_coeffs;
    Integrals::IntegralProvider<Integrals::FBCoefficient<F, F>, Integrals::StdBasisFunction<C, C>> two_loop_integrals;

    std::vector<OneLoopAmplType> one_loop_ampl; // for two Nf contributions

    OutputType cataniJ2evaluator;
    std::vector<OutputType> cataniJ1evaluators; // to be multiplied by the corresponding element from one_loop_ampl

    uint64_t last_cardinality{0};

    NormalizationVanishingTreeType norm_vtr{NormalizationVanishingTreeType::spinor_weight};

    void construct_catani_operators(); // to update with cardinality
    void update_cardinality();

  public:
    TwoLoopRemainder() = delete;
    TwoLoopRemainder(const TwoLoopRemainder&) = delete;
    TwoLoopRemainder& operator=(const TwoLoopRemainder&) = delete;
    TwoLoopRemainder(TwoLoopRemainder&&) = default;
    TwoLoopRemainder& operator=(TwoLoopRemainder&&) = default;

    TwoLoopRemainder(const Model&, const PartialAmplitudeInput&);
    TwoLoopRemainder(const Model&, const PartialAmplitudeInput&, NormalizationVanishingTreeType);

    /**
     * Evaluates the remainder on a given PS point
     */
    OutputType operator()(const momentumD_configuration<F, 4>&);

    /**
     * Evaluates the subtraction defining the remainder on a given PS
     * point
     */
    OutputType evaluate_subtraction(const momentumD_configuration<F, 4>&);

    /**
     * Returns something non-trivial only for vanishing tree case.
     * In case when NormalizationVanishingTreeType is one_loop_nf0, this returns the ratio
     * of 1-loop Nf^0 amplitude to the spinor weight (or whatever little-group-removing-normalization was used)
     * TODO: for floating point should just return the normalization
     */
    F get_normalization(const momentumD_configuration<F, 4>&);
};

} // namespace FiniteRemainder

} // namespace Caravel

#include "Remainder.hpp"

#endif // REMAINDER_H_INC
