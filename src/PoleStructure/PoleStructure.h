#ifndef POLE_STRUCTURE_H_INC
#define POLE_STRUCTURE_H_INC
#include "IntegralLibrary/FunctionBasisExpansion.h"
#include <vector>
#include "Core/CaravelInput.h"

namespace Caravel{
namespace PoleStructure {

WISE_ENUM_CLASS(cataniGtype, gg, qq,qQ,QQ, gq, yg, yq, yy)

/**
 * Implementation of universal pole structure suitable for functional
 * reconstruction.
 */


/**
 * Universal IR pole structure I1 operator. See (e.g.) equation (B.6)
 * of 1809.09067.  Constructed in terms of pentagon functions.
 */
template<typename F, typename TF>
Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF,TF>> cataniI1Operator(size_t num_orders, const std::vector<cataniGtype>&);

/**
 * Universal IR pole structure I2 operator. See (e.g.) equation (B.8)
 * of 1809.09067.  Constructed in terms of pentagon functions.
 */
template<typename F, typename TF>
Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF,TF>> cataniI2Operator(int num_orders, const PartialAmplitudeInput&);

/**
 * Universal pole structure J1 operator. I1 including (two-loop) renormalization.
 */
template<typename F, typename TF>
Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF,TF>> poleJ1Operator(size_t num_orders, unsigned nf, const std::vector<cataniGtype>&,int la=3);

/**
 * Universal pole structure J1 operator. I1 including (one-loop) renormalization.
 */
template<typename F, typename TF>
Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF,TF>> poleJ1Operator1L(size_t num_orders, unsigned nf, const std::vector<cataniGtype>&,int la=3);

/**
 * Universal pole structure J2 operator. I2 including (two-loop) renormalization.
 */
template<typename F, typename TF>
Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF,TF>> poleJ2Operator(size_t num_orders, const PartialAmplitudeInput&);

/**
 * Finite One loop pole operator. Divergence structure of bare amplitude.
 * See (e.g.) eq 8 of 1511.05409. Constructed in terms of pentagon functions.
 */
template<typename F, typename TF>
Integrals::NumericFunctionBasisExpansion<Series<F>, Integrals::StdBasisFunction<TF,TF>> finiteOneLoopPoleOperator(size_t num_orders);
} // namespace PoleStructure
}

#include "PoleStructure.hpp"

#endif //POLE_STRUCTURE_H_INC
