#include "Remainder.h"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "Amplitude.h"

namespace Caravel {
namespace FiniteRemainder {

#ifdef USE_FINITE_FIELDS
template class TwoLoopRemainder<F32, C>;
#endif

}
} // namespace Caravel
