#include <exception>
#include <random>
#include <iostream>

// Math interface stuffs
#include "mathlink.h" // NOTE: Must be first
#include "WolframLibrary.h"
#include "MathPrimitives.h"

#include "Core/typedefs.h"
#include "Core/type_traits_extra.h"
#include "AmplitudeCoefficients/NumericalTools.h" // Required for "random_scaled_number"
#include "AmplitudeCoefficients/Topology.h"
#include "Core/MathInterface.h"
#include "Core/momD_conf.h"
#include "Forest/Builder.h"
#include "Forest/Forest.h"
#include "Graph/Graph.h"
#include "Graph/GraphKin.h"
#include "OnShellStrategies/OnShellStrategies.h"

using namespace Caravel;

template <typename T>
T squaring_pow(T x, int64_t n){
  T t(1);
    while (n > 0){
        if (n % 2 != 0)
          t = (t * x);

        x = (x * x);
        n /= 2;
      }
  return t;
}

bool quadratic_residue_q(F32 x) {
    auto p = F32::get_p();
    auto legendre = squaring_pow(x, (p - 1) / 2);

    if (legendre == F32(-1)) { return false; }
    else {
        return true;
    }
}

namespace Caravel{
/**
 * Implementation of tonelli-shanks algorithm
 */
F32 sqrt(F32 x) {
    if (!quadratic_residue_q(x)) { throw std::invalid_argument("Non-quadratic residue passed to sqrt"); }

    int64_t p = F32::get_p();
    auto q = p - 1;
    auto s = 0;
    while (q % 2 == 0) {
        q /= 2;
        s += 1;
    }
    if (s == 1) { return squaring_pow(x, (p + 1) / 4); }

    // Compute non-residue
    const auto z = [p]() {
        int64_t z;
        for (z = 2; z < p; z++) {
            if (!quadratic_residue_q(z)) { return F32(z); }
        }

        throw std::domain_error("Unable to find non quadratic residue.");
    }();

    auto c = squaring_pow(z, q);
    auto r = squaring_pow(x, (q + 1) / 2);
    auto t = squaring_pow(x, q);
    auto m = s;

    F32 t2(0);

    while (t != F32(1)) {
        t2 = (t * t);

        int64_t i;
        for (i = 1; i < m; i++){
          if (t2 == F32(1)) { break; }
          t2 = (t2 * t2);
        }

        auto b = squaring_pow(c, 1 << (m - i - 1));
        r = (r * b);
        c = (b * b);
        t = (t * c);
        m = i;
    }

    return r;
}

}

/**
 * Global random number generator.
 */

std::mt19937 MT;

template <typename T, size_t D> std::vector<momD<T,D>> mom_list_from_mtensor(WolframLibraryData libData, const MTensor& t) {

    auto vec_list = vector_vector_from_mtensor<T>(libData, t);

    std::vector<momD<T, D>> mom_list;
    for (auto& vec : vec_list) { mom_list.emplace_back(vec); }

    return mom_list;
}
// exported functions
extern "C" {

// Args = cut, external kinematics
int random_on_shell_momenta(WolframLibraryData libData, mint, MArgument* Args, MArgument Res) {

    using F = F32;
    const size_t D = 6;

    int err = LIBRARY_NO_ERROR;

    char* cut_schematic = MArgument_getUTF8String(Args[0]);
    momD_conf<F, D> conf(mom_list_from_mtensor<F, D>(libData, MArgument_getMTensor(Args[1])));

    try {
        ColourOrderedResidue<2> topo(cut_schematic);

        auto graph = topo.get_graph();
        lGraph::lGraphKin::GraphKin<F, D> graph_kin(graph, conf);
        auto point_gen = random_natural_point_generation(graph, graph_kin);
        auto point = [&point_gen](){
                       while(true){
                         auto point = point_gen(&MT)();
                         if (quadratic_residue_q(EpsilonBasis<F>::squares[0])
                             && quadratic_residue_q(EpsilonBasis<F>::squares[1])){
                           return point;
                         }
                       }
                     }();

        std::vector<std::vector<F>> point_components;
        for (auto& mom : point) { point_components.push_back(mom.get_vector()); }

        // Remove effect of EpsilonBasis.
        auto r0 = sqrt(EpsilonBasis<F>::squares[0]);
        auto r1 = sqrt(EpsilonBasis<F>::squares[1]);
        EpsilonBasis<F>::squares[0] = F(1);
        EpsilonBasis<F>::squares[1] = F(1);

        for (auto& mom : point_components){
          mom.at(4) *= r0;
          mom.at(5) *= r1;
        }

        MTensor res;
        int64_mtensor_from_vector_vector(libData, res, point_components);

        MArgument_setMTensor(Res, res);
    }
    catch (const std::invalid_argument& exception) {
        message(libData, "CaravelError", exception.what());
        err = LIBRARY_FUNCTION_ERROR;
    }
    catch (const std::exception& exception) {
        message(libData, "CaravelError", exception.what());
        err = LIBRARY_FUNCTION_ERROR;
    }
    catch (...) {
      libData->Message("unhandledException");
      err = LIBRARY_FUNCTION_ERROR;
    }

    libData->UTF8String_disown(cut_schematic);

    return err;
}

// Args = cut, external kinematics, internal kinematics
// This is quite a laborious setup, yes.
int compute_two_loop_cut_6d(WolframLibraryData libData, mint, MArgument *Args, MArgument Res) {

  using F = F32;
  const size_t D = 6;

  int err = LIBRARY_NO_ERROR;

  // This could be cleaned up with some RAII.
  char* proc_string = MArgument_getUTF8String(Args[0]);
  char* cut_schematic = MArgument_getUTF8String(Args[1]);

  try{

    cardinality_update_manager<F>::get_instance()->set_cardinality(F::get_p());

    MathList particles_list(proc_string, "Particles");
    std::vector<Particle> particles;
    for (auto& particle : particles_list){
      particles.emplace_back(MathList(particle));
    }

    momD_conf<F, 4> point(mom_list_from_mtensor<F, 4>(libData, MArgument_getMTensor(Args[2])));
    auto loop_point = mom_list_from_mtensor<F, D>(libData, MArgument_getMTensor(Args[3]));
    auto model2use = std::make_unique<SM_color_ordered>();
    Forest forest(model2use.get());

    Cut<2> cut(cut_schematic);
    cut.add_to_forest(forest, particles);

    std::vector<size_t> dims { D };
    forest.define_internal_states_and_close_forest( dims );

    Builder<F, D> bg(&forest, point.n());

    bg.set_p(point); // Set external kinematics
    bg.set_loop(loop_point);   // Set internal kinematics

    auto eval = cut.evaluate(bg, true);
    //auto eval = topo.evaluate(bg, true);
    MTensor res;
    err = int64_mtensor_from_vector(libData, res, eval);

    MArgument_setMTensor (Res, res);
  }
  catch(const std::invalid_argument& exception){
    message(libData, "CaravelException", exception.what());
    err = LIBRARY_FUNCTION_ERROR;
  }
  catch(const std::exception& exception){
    message(libData, "CaravelException", exception.what());
    err = LIBRARY_FUNCTION_ERROR;
  }
  catch(...){
    libData -> Message("unhandledException");
    err = LIBRARY_FUNCTION_ERROR;
  }

  libData -> UTF8String_disown(proc_string);
  libData -> UTF8String_disown(cut_schematic);

  return err;
}

// Args = process, mom_conf
// This is quite a laborious setup, yes.
int compute_tree_4d(WolframLibraryData libData, mint, MArgument* Args, MArgument Res) {

    using F = F32;
    const size_t D = 4;

    int err = LIBRARY_NO_ERROR;

    // This could be cleaned up with some RAII.
    char* proc_string = MArgument_getUTF8String(Args[0]);
    try {

        cardinality_update_manager<F>::get_instance()->set_cardinality(F::get_p());
        Process proc(MathList(proc_string, "Particles"));

        unsigned N = proc.get_process().size();

        auto point = momD_conf<F, D>(mom_list_from_mtensor<F, D>(libData, MArgument_getMTensor(Args[1])));

        auto model2use = std::make_unique<SM_color_ordered>();
        Forest forest(model2use.get());
        auto tree_id = forest.add_process(proc);
        std::vector<size_t> dims{4};
        forest.define_internal_states_and_close_forest(dims);

        Builder<F, 4> bg(&forest, N);

        bg.set_p(point);
        bg.compute();
        bg.contract();

        auto res = bg.get_tree(tree_id);

        MArgument_setInteger(Res, int64_t(res));
    }
    catch (const std::invalid_argument& exception) {
        libData->Message("badAmplitude");
        err = LIBRARY_FUNCTION_ERROR;
    }
    catch (...) {
        libData->Message("unhandledException");
        err = LIBRARY_FUNCTION_ERROR;
    }

    libData->UTF8String_disown(proc_string);

    return err;
}

int onshell_check(WolframLibraryData libData, mint, MArgument* Args, MArgument Res) {

    using F = F32;
    const size_t D = 4;

    auto point = momD_conf<F, D>(mom_list_from_mtensor<F, D>(libData, MArgument_getMTensor(Args[0])));

    F tot(0);
    for (size_t i = 1; i <= point.n(); i++) { tot += point.p(i) * point.p(i); }

    MArgument_setInteger(Res, int64_t(tot));
    return LIBRARY_NO_ERROR;
}

int getCardinality(WolframLibraryData, mint, MArgument*, MArgument Res) {
    auto p = F32::get_p();
    MArgument_setInteger(Res, p);
    return LIBRARY_NO_ERROR;
}

  int setCardinality(WolframLibraryData, mint, MArgument* Args, MArgument) {
    F32::set_p(MArgument_getInteger(Args[0]));
    return LIBRARY_NO_ERROR;
  }

int tonelli_shanks(WolframLibraryData libData, mint, MArgument* Args, MArgument Res) {
    int err = LIBRARY_NO_ERROR;
    F32 x(MArgument_getInteger(Args[0]));

    try {
      auto r = sqrt(x);
      MArgument_setInteger(Res, int64_t(r));
    }
    catch (const std::invalid_argument& exception) {
      print(libData, exception.what());
      libData->Message("unhandledException");
      err = LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
  }
}
