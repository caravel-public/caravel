/**
 * @file Provider.h
 *
 * @date 22.07.2017
 *
 * @brief Giving IntegralsBH an standard IntegralProvider interface
 */

#ifndef PROVIDERBH_H_
#define PROVIDERBH_H_

#include <vector>
#include <iostream>
#include <cmath>
#include <exception>
#include <algorithm>
#include <cassert>
#include <functional>
#include <utility>
#include <memory>

#include "Core/Utilities.h"
#include "Core/type_traits_extra.h"
#include "Core/settings.h"
#include "Core/typedefs.h"

#include "Core/momD_conf.h"
#include "Graph/IntegralGraph.h"
#include "Core/Series.h"
#include "integrals.h"

// to load 'floating_partner_t'
#include "IntegralLibrary/IntegralProvider.h"

namespace Caravel {
namespace IntegralsBH {

template <typename T>
class ProviderBH {

  protected:

    std::vector<T> masses;	/**< Vector containing masses (as many as added according to ParticleMass::mass_index_global) */
    T mu_square;	/**< Regularization/renormalization scale squared */
    std::vector<std::function<Series<T>(const momD_conf<T, 4>&)>> integral_series;	/**< Container with functors necessary for the compute(.) method */
    /**
     * The type used for the coefficients in the function basis
     * expansion of the integral.
     */
    using CType = Caravel::Integrals::FBCoefficient<Series<T>,T>;
    /**
     * The type of special functions used in
     * the master integral evaluation.
     */
    using SFType = Caravel::Integrals::StdBasisFunction<Caravel::Integrals::floating_partner_t<T>, Caravel::Integrals::floating_partner_t<T>>;
    /**
     * The type that a function Caravel::Integrals::master<CType,SFType>.get_basis() returns
     * when evaluated on a PS point (a set of invariants extracted from a given momD_conf)
     */
    using NFBExpansion = Caravel::Integrals::NumericFunctionBasisExpansion<Series<T>, SFType>;
    std::vector<std::function<NFBExpansion(const momD_conf<T,4>&)>> integral_nfbe;	/**< Container with functors necessary for the get_abstract_integral(.) method */
    std::vector<lGraph::IntegralGraph> integral_graphs;    /**< We store a copy of the integrals added to *this */

  public:
    ProviderBH() = default;
    ProviderBH(const std::vector<lGraph::IntegralGraph>&);

    /**
     * Method to evaluate integrals and retrieve numerical (Series<T>) results
     */
    std::vector<Series<T>> compute(const momD_conf<T, 4>&);

    /**
     * Method to evaluate integrals and retrieve semi-analytical (NBFExpansion<T>) results
     */
    std::vector<NFBExpansion> get_abstract_integral(const momD_conf<T, 4>&) const;
    std::function<std::vector<NFBExpansion>(const momD_conf<T, 4>&)> get_int_evaluator() const;

    /**
     * Sets the square of the regularization scale used for integrals computations.
     * Notice: default value is '1' before any call is done
     */
    void set_mu_squared(const T&);
  private:
    void construct_integrals();
};


// IMPLEMENTATIONS
#include "ProviderBH.hpp"

}
}

#endif   // PROVIDERBH_H_
