/**
 * @file integrals.h
 *
 * @date 10.7.2017 as coming and adapted from BHlib_Massive git repo
 *
 * @author Vasily Sotnikov
 *
 *  This file is a part of libIntegrals.
 *
 *  Contains complete set of master integrals with internal masses.
 *
 *  The integral expressions have a factor of c_\Gamma removed, so that 
 *  those with 1/e^2 divergences start with precisely -n/e^2 as their 
 *  leading term. Then, the 'Series' returned by a call to 'integral'
 *  is normalized to have Caravel's standard integral normalization 
 *  (particularized to 1 loop):
 *
 *  I_{\Gamma,i} = (e^{\gamma_E \epsilon})/(i\pi^{D/2}) \int d^D l \frac{1}{\rho_1 ... \rho_n}
 *
 *  To make this change in normalization we use the Series 
 *  'convert_intsBH_to_caravel_normalization'.
 *
 * Implementations added below through integrals_complex.hpp and
 * integrals.hpp
 *
 * libIntegrals implements all 1-loop master integrals up to order eps^0,
 * based on arXiv:1105.4319 (for divergent integrals) and arXiv:1007.4716
 * together with the library OneLOop.
 * 
 */

#ifndef INTEGRALS_H_
#define INTEGRALS_H_

#include <complex>
#include <functional>
#include <string>

#include "Core/Debug.h"
#include "Graph/IntegralGraph.h"
#include "Core/Series.h"
#include "integrals_utilities.h"
#include "riemann.h"

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif

namespace Caravel {
namespace IntegralsBH {

/**
 * Interface to 1-loop integrals
 *
 * @param IntegralGraph containing all topological information of the integral and corresponding indices to extract invariants and masses
 * @param momD_conf<T,4> containing the external momenta to be evaluated
 * @param std::vector<T> containing the set of masses employed
 * @param T giving the regularization(/renormalization) scale
 * @return Series<T> representing the Laurent series of the integral requested
 */
template <class T>
Series<std::complex<T>> integral(const lGraph::IntegralGraph&, const momD_conf<std::complex<T>, 4>&, const std::vector<std::complex<T>>&, const std::complex<T>&);

template <class T> std::complex<T> I4f4mC(int order, const std::vector<T>& pp_in, const std::vector<std::complex<T>>& mm_in);
}
}

#define CXT std::complex<T>

#endif /* INTEGRALS_H_ */
