/**********************************************************************
 *  This file is a part of libIntegrals.
 *
 *  Definition for complex numbers on Riemann surface of logarithm.
 *  For comments see header file.
 ***********************************************************************/

namespace std {
template <class T> std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<T>& r) { return s << r.getz() << "[" << r.getn() << "]"; }
}

namespace Caravel {
namespace IntegralsBH {

template <typename T> const std::complex<T> C1{1,0};
template <typename T> const std::complex<T> ipi{0,Caravel::IntegralsBH::pi<T>()};

using std::abs;

/*************************************************************************************************/
/*      Constructor for class riemann should be able to see small imaginary part, so it should be set to
 *      zero only when it is smaller then  IEPS and NOT then the standard DeltaZero<T>()
 */
template <class T> inline T ltEPS();
template <> inline R ltEPS<R>() { return R(1.0e-60); }
#ifdef HIGH_PRECISION
template <> inline RHP ltEPS<RHP>() { return RHP("1.0e-90"); }
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline RVHP ltEPS<RVHP>() { return RVHP("1.0e-120"); }
#endif
#if INSTANTIATE_GMP
template <> inline RGMP ltEPS<RGMP>() { return DeltaZero<RGMP>() * RGMP("1.0e-20"); }
#endif
/*************************************************************************************************/

/*************************************************************************************************/

template <class TT> const riemann<TT> operator*(TT real, const riemann<TT>& xriemann) {
    riemann<TT> result;

    result.z = xriemann.z * abs(real);
    result.n = (real >= TT(0)) ? xriemann.n : xriemann.n + 1;

    return result;
}

template <class TT> const riemann<TT> operator*(const riemann<TT>& xriemann, TT real) {
    riemann<TT> result;

    result.z = xriemann.z * abs(real);
    result.n = (real >= TT(0)) ? xriemann.n : xriemann.n + 1;

    return result;
}

template <class TT> const riemann<TT> operator*(const riemann<TT>& z1, const riemann<TT>& z2) {
    riemann<TT> result;
    std::complex<TT> arg = z1.z * z2.z;

    result.z = arg;
    result.n = z1.n + z2.n;

    if (arg.real() < TT(0)) {
        result.z *= TT(-1);
        result.n += Caravel::IntegralsBH::sgn(z2.z.imag()); /* this actually doesn't change if we use Im(z1) here instead */
    }

    return result;
}

template <class TT> const riemann<TT> operator/(const riemann<TT>& xriemann, TT real) {
    riemann<TT> result;

    result.z = xriemann.z / abs(real);
    result.n = (real >= TT(0)) ? xriemann.n : xriemann.n - 1;

    return result;
}

template <class TT> const riemann<TT> operator/(TT real, const riemann<TT>& xriemann) {
    riemann<TT> result;

    result.z = abs(real) / xriemann.z;
    result.n = (real >= TT(0)) ? -xriemann.n : -xriemann.n + 1;

    return result;
}

template <class TT> const riemann<TT> operator/(const riemann<TT>& z1, const riemann<TT>& z2) {
    riemann<TT> result;
    std::complex<TT> arg = z1.z / z2.z;

    result.z = arg;
    result.n = z1.n - z2.n;

    if (arg.real() < TT(0)) {
        result.z *= TT(-1);
        result.n -= Caravel::IntegralsBH::sgn(z2.z.imag()); /* this actually doesn't change if we use Im(z1) here instead */
    }

    return result;
}

template <class TT> const riemann<TT> operator/(const std::complex<TT>& z1, const riemann<TT>& z2) { return riemann<TT>(z1) / z2; }
template <class TT> const riemann<TT> operator/(const riemann<TT>& z1, const std::complex<TT>& z2) { return z1 / riemann<TT>(z2); }

template <class T> riemann<T>::riemann(T setz) {
    if (setz >= T(0)) {
        z = setz;
        n = 0;
    } else {
        z = -setz;
        n = 1;
    }
}

template <class T> riemann<T>::riemann(std::complex<T> setz) {
    T zIm = setz.imag();

    if (setz.real() >= T(0)) {
        z = setz;
        n = 0;
    } else {
        z = -setz;
        n = abs(zIm) < ltEPS<T>() ? 1 : Caravel::IntegralsBH::sgn(zIm);
    }
}

template <class T> riemann<T>::riemann(T setz, int setn) {
    if (setz >= T(0)) {
        z = setz;
        n = 0;
    } else {
        z = -setz;
        n = setn;
    }
}

template <class T> riemann<T>::riemann(std::complex<T> setz, int setn) {
    T zIm = setz.imag();

    if (setz.real() >= T(0)) {
        z = setz;
        n = 0;
    } else {
        z = -setz;
        n = abs(zIm) < ltEPS<T>() ? setn : Caravel::IntegralsBH::sgn(zIm);
    }
}

template <class T> riemann<T>::riemann(T setz, T signre) {
    if (setz >= T(0)) {
        z = setz;
        n = 0;
    } else {
        z = -setz;
        n = Caravel::IntegralsBH::sgn(signre);
    }
}

template <class T> riemann<T>::riemann(std::complex<T> setz, T signre) {
    T zIm = setz.imag();

    if (setz.real() >= T(0)) {
        z = setz;
        n = 0;
    } else {
        z = -setz;
        n = abs(zIm) < ltEPS<T>() ? Caravel::IntegralsBH::sgn(signre) : Caravel::IntegralsBH::sgn(zIm);
    }
}

template <class T> void riemann<T>::print() { std::cout << z << "[" << n << "]" << std::endl; }

template <class T> std::complex<T> riemann<T>::tocomplex() const {
    int nm2 = n % 2;
    return (nm2 == 1 or nm2 == -1) ? -z : z;
}

template <class T> riemann<T>& riemann<T>::operator=(const riemann<T>& rhs) {
    z = rhs.z;
    n = rhs.n;
    return *this;
}

template <class T> const riemann<T> riemann<T>::operator-() const {
    riemann<T> result;
    result.z = z;
    result.n = n + 1;
    return result;
}

/*************************************************************************************************/

/*  Based on eq.(2.10) from 0712.1851v4  */
template <class T> std::complex<T> log(const riemann<T>& x) {
    using std::log;

    std::complex<T> z = x.getz();
    int n = x.getn();

    if (iszeroI(z.imag()))
        return (log(z.real()) + ipi<T> * T(n));
    else
        return (log(z) + ipi<T> * T(n));
}

template <class T> std::complex<T> logM1(const riemann<T>& x) {
    using std::log;
    using Caravel::IntegralsBH::sq;

    std::complex<T> z = x.tocomplex();
    T arg = z.real() - T(1);

    if (iszeroI(z.imag()) and (abs(arg) < Caravel::DeltaZeroSqr<T>()))
        return (C1<T> - arg / T(2) + sq(arg) / T(3));
    else
        return (log(x) / (z - C1<T>));
}

/*************************************************************************************************/

/*  Based on eq.(2.11) and (2.12) from 0712.1851v4  */
template <class T> std::complex<T> li2(const riemann<T>& xin) {
    using std::log;
    using std::abs;
    using Caravel::IntegralsBH::sq;

    riemann<T> x = xin;

    std::complex<T> z = x.tocomplex();

    std::complex<T> result(0, 0);
    T ressign = T(1);

    if (iszeroI(z.imag())) {
        T rez = z.real();
        if (rez == T(1)) return T(0);
        if (rez == T(0)) return pisqo6<T>();
        if (abs(rez) > T(1)) {
            ressign = T(-1);
            rez = T(1) / rez;
            result += sq(log(x)) / T(2);
        }
        result += ReLi2(T(1) - rez);
        result *= ressign;
        result -= ipi<T> * T(xin.getn()) * log(T(1) - rez); /* add branch cut value */
    } else {
        if (abs(z) > T(1)) {
            ressign = T(-1);
            result += sq(log(x)) / T(2);
            x = T(1) / xin;
            z = x.tocomplex();
        }
        result += pisqo6<T>() - Li2(z) - log(C1<T> - z) * log(x);
        result *= ressign;
    }

    return result;
}

template <class T> std::complex<T> li2M1(const riemann<T>& x1, const riemann<T>& x2) {
    using std::log;
    using std::abs;

    std::complex<T> z1 = x1.tocomplex(), z2 = x2.tocomplex();
    int n1 = x1.getn(), n2 = x2.getn();

    T rez1 = z1.real(), rez2 = z2.real(), dz = rez1 - rez2;

    bool gt1 = (abs(rez1) > T(1)) and (abs(rez2) > T(1));

    std::complex<T> result(0, 0);

    if (iszeroI(z1.imag()) and iszeroI(z2.imag()) and iszeroI(dz) and (n1 == n2)) {
        riemann<T> x1t = x1;

        if (gt1) {
            x1t = T(1) / x1;
            rez1 = T(1) / rez1;
            rez2 = T(1) / rez2;
            n2 *= -1;
            dz = rez1 - rez2;
        }

        std::complex<T> log1 = logM1(x1t);
        result -= log1;
        result += (T(1) / rez1 - logM1(x1t)) * (iszeroI(rez1 - T(1)) ? C1<T> : dz / (rez1 - T(1))) / T(2);

        if (gt1) {
            result *= rez1 * rez2;
            result -= log(x1 * x2) / T(2) * rez1 * logM1(x2 / x1);
        }
    } else {
        result = (li2(x1) - li2(x2)) / (z1 - z2);
    }
    return result;
}

// Declare instantiations in accordance with instantiation definitions in riemann.cpp

#define EXTERN_INSTANTIATE_RIEMANN(TT)                                                                                                                         \
    extern template class riemann<TT>;                                                                                                                         \
    extern template const riemann<TT> operator*(TT, const riemann<TT>&);                                                                                       \
    extern template const riemann<TT> operator*(const riemann<TT>&, TT);                                                                                       \
    extern template const riemann<TT> operator*(const riemann<TT>&, const riemann<TT>&);                                                                       \
    extern template const riemann<TT> operator/(TT, const riemann<TT>&);                                                                                       \
    extern template const riemann<TT> operator/(const riemann<TT>&, TT);                                                                                       \
    extern template const riemann<TT> operator/(const riemann<TT>&, const riemann<TT>&);                                                                       \
    extern template const riemann<TT> operator/(const std::complex<TT>& z1, const riemann<TT>& z2);                                                            \
    extern template const riemann<TT> operator/(const riemann<TT>& z1, const std::complex<TT>& z2);                                                            \
    extern template std::complex<TT> log(const riemann<TT>&);                                                                                                  \
    extern template std::complex<TT> logM1(const riemann<TT>&);                                                                                                \
    extern template std::complex<TT> li2(const riemann<TT>&);                                                                                                  \
    extern template std::complex<TT> li2M1(const riemann<TT>&, const riemann<TT>&);

EXTERN_INSTANTIATE_RIEMANN(R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_RIEMANN(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_RIEMANN(RVHP)
#endif

#ifdef INSTANTIATE_GMP
EXTERN_INSTANTIATE_RIEMANN(CGMP)
#endif
}
}

namespace std {
extern template std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<Caravel::R>& r);
#ifdef HIGH_PRECISION
extern template std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<Caravel::RHP>& r);
#endif
#ifdef VERY_HIGH_PRECISION
extern template std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<Caravel::RVHP>& r);
#endif

#ifdef INSTANTIATE_GMP
extern template std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<RGMP>& r);
#endif
}
