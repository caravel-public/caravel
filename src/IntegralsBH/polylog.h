/**
 * @file polylog.h
 *
 * @date 19.11.2007 original source
 *
 * @author David A. Kosower and adaptations in BlackHat and Caravel
 *
 * @brief Dilogarithm and Clausen function
 *
 * Templated implementations are included in the file polylog.hpp, and explicit
 * overloading in polylog.cpp, polylog_HP.cpp and polylog_VHP.cpp
 *
 */

#ifndef POLYLOG_H_
#define POLYLOG_H_ 1

#include <cassert>
#include <cmath>

#include "Core/typedefs.h"
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "Core/qd_suppl.h"
#endif

#if INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

namespace Caravel {
namespace IntegralsBH {

template <class T> inline T pi();

template <> inline R pi() { return M_PI; }
#ifdef HIGH_PRECISION
template <> inline RHP pi() { return dd_real::_pi; }
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline RVHP pi() { return qd_real::_pi; }
#endif
template <> inline C pi() { return C(M_PI); }
#ifdef HIGH_PRECISION
template <> inline CHP pi() { return CHP(dd_real::_pi, 0.); }
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline CVHP pi() { return CVHP(qd_real::_pi, 0.); }
#endif

#if INSTANTIATE_GMP
template <> inline RGMP pi() { return get_pi(); }
template <> inline std::complex<RGMP> pi() { return std::complex<RGMP>(get_pi(), RGMP(0)); }
#endif

R ReLi2(R);
C Li2(C);
R Cl2(R);

#ifdef HIGH_PRECISION
RHP ReLi2(RHP);
RHP Cl2(RHP);
CHP Li2(CHP);
#endif

#ifdef VERY_HIGH_PRECISION
RVHP ReLi2(RVHP);
RVHP Cl2(RVHP);
CVHP Li2(CVHP);
#endif

#if INSTANTIATE_GMP
RGMP ReLi2(const RGMP&);
RGMP Cl2(const RGMP&);
std::complex<RGMP> Li2(const std::complex<RGMP>&);
#endif

template <class T> inline T sq(const T& x) { return (x * x); }

}
}

#endif /* POLYLOG_H_ */
