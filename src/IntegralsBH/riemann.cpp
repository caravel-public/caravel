/**********************************************************************
 *  This file is a part of libIntegrals.
 *
 *  Definition for complex numbers on Riemann surface of logarithm.
 *  For comments see header file.
 ***********************************************************************/

#include "riemann.h"

namespace Caravel {
namespace IntegralsBH {

// Defining instantiations, matching declarations in riemann.hpp

#define INSTANTIATE(TT)                                                                                                                                        \
    template class riemann<TT>;                                                                                                                                \
    template const riemann<TT> operator*(TT, const riemann<TT>&);                                                                                              \
    template const riemann<TT> operator*(const riemann<TT>&, TT);                                                                                              \
    template const riemann<TT> operator*(const riemann<TT>&, const riemann<TT>&);                                                                              \
    template const riemann<TT> operator/(TT, const riemann<TT>&);                                                                                              \
    template const riemann<TT> operator/(const riemann<TT>&, TT);                                                                                              \
    template const riemann<TT> operator/(const riemann<TT>&, const riemann<TT>&);                                                                              \
    template const riemann<TT> operator/(const std::complex<TT>& z1, const riemann<TT>& z2);                                                                   \
    template const riemann<TT> operator/(const riemann<TT>& z1, const std::complex<TT>& z2);                                                                   \
    template std::complex<TT> log(const riemann<TT>&);                                                                                                         \
    template std::complex<TT> logM1(const riemann<TT>&);                                                                                                       \
    template std::complex<TT> li2(const riemann<TT>&);                                                                                                         \
    template std::complex<TT> li2M1(const riemann<TT>&, const riemann<TT>&);

INSTANTIATE(R)
#ifdef HIGH_PRECISION
INSTANTIATE(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE(RVHP)
#endif

#ifdef INSTANTIATE_GMP
INSTANTIATE(RGMP)
#endif
}
}

namespace std {
template std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<Caravel::R>& r);
#ifdef HIGH_PRECISION
template std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<Caravel::RHP>& r);
#endif
#ifdef VERY_HIGH_PRECISION
template std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<Caravel::RVHP>& r);
#endif

#ifdef INSTANTIATE_GMP
template std::ostream& operator<<(std::ostream& s, const Caravel::IntegralsBH::riemann<RGMP>& r);
#endif
}
