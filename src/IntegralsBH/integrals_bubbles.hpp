/********************************************************************************************
 *
 *      Massive bubble.
 *
 ********************************************************************************************/
template <class T> complex<T> I2_0(int order, const T& musq, const T& msq) {
#if CALLING
    cout << "*******I2_0 called******" << endl;
#endif

    switch (order) {
        case -2: return C0;
        case -1: return C1;
        case 0: return log(musq / msq);
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I2_0m(int order, const T& musq, const T& s) {
#if CALLING
    cout << "*******I2_0m called******" << endl;
#endif

    switch (order) {
        case -2: return C0;
        case -1: return C1;
        case 0: riemann<T> rs(-s / musq, -1); return (T(2) - log(rs));
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I2_1m(int order, const T& musq, const T& s, const T& m2sq) {
#if CALLING
    cout << "*******I2_1m called******" << endl;
#endif

    switch (order) {
        case -2: return C0;
        case -1: return C1;
        case 0: riemann<T> rms(m2sq - s, -1); return (T(2) + log(musq / m2sq) + (m2sq / s - T(1)) * log(rms / m2sq));
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I2_1m_d(int order, const T& musq, const T& m2sq) {
#if CALLING
    cout << "*******I2_1m degenerate called******" << endl;
#endif

    switch (order) {
        case -2: return C0;
        case -1: return C1;
        case 0: return (T(2) + log(musq / m2sq));
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I2_full(int order, const T& musq, const T& s, const T& m1sq, const T& m2sq /* should not be 0 */) {
#if CALLING
    cout << "*******I2 called******" << endl;
#endif
    switch (order) {
        case -2: return C0;
        case -1: return C1;
        case 0: {
            riemann<T> rs(-s / musq, -1), rsp(s / musq, -1);
            riemann<T> rms(m2sq - s, -1);

            T arg = m2sq - m1sq - s;

            complex<T> gammai[2];
            CXT np2{sqrt(complex<T>(sq(arg)) - complex<T>(4, 0) * s * m1sq)};
            if (abs(arg) >= 0) {
                gammai[1] = (-arg - np2) / (T(2) * s);
                gammai[0] = T(2) * m1sq / (-arg - np2);
            } else {
                gammai[0] = (-arg + np2) / (T(2) * s);
                gammai[1] = T(2) * m1sq / (-arg + np2);
            }

            complex<T> result = T(2) - log(rsp);

            for (int i = 0; i < 2; i++) {
                riemann<T> gim1r(C1 - C1 / gammai[i], 1 - 2 * i);
                riemann<T> gim1(gammai[i] - C1, 1 - 2 * i);
                result += gammai[i] * log(gim1r) - log(gim1);
            }

            return (result);
        }
    }
    // all other powers set to zero
    return C0;
}
