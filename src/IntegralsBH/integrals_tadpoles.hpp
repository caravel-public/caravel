/********************************************************************************************
 *
 *      Massive tadpole.
 *
 ********************************************************************************************/
template <class T> complex<T> I1(int order, const T& musq, const T& msq) {
#if CALLING
    cout << "*******I1 called******" << endl;
#endif
    using std::log;

    switch (order) {
        case -2: return C0;
        case -1: return msq;
        case 0: return (msq - msq * log(msq / musq));
    }
    // all other powers set to zero
    return C0;
}
