/**
 * @file riemann.h
 *
 * @date 10.7.2017 as coming and adapted from BHlib_Massive git repo
 *
 * @author Vasily Sotnikov
 *
 *  This file is a part of libIntegrals.
 *
 *  Contains:
 *  - Definition for complex numbers on Riemann surface of logarithm.
 *      z = x exp(i\pi n),
 *    where x is complex number with Re(x)>=0 and n is integer;
 *  - Analytic continuation of log and dilog.
 *
 *  Logarithm log(z) defined on Riemann surface has a property
 *  log(ab) = log(a)+log(b) for any complex a and b.
 *  Relation with principle branch Log(z) is then
 *  log(z) = Log(z) + i\pi n
 *
 *  logM1(z) = log(z)/(z-1)
 *  li2M1(z1, z2) = ( li2(z1) - li2(z2) )/ (z1-z2)
 *  In the vicinity of z=1 and z1=z2 are calculated via the expansion in series.
 *
 *  Warning! For multiplication by real numbers assumptions
 *      1 = exp(i 0)} and -1 = exp(i \pi)
 *  are used.
 *
 * Related templated implementations added below through file riemann.hpp
 */

#ifndef RIEMANN_H_
#define RIEMANN_H_

#include "integrals_utilities.h"
#include "polylog.h"

#if INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

namespace Caravel {
namespace IntegralsBH {

template <class T> class riemann {
    std::complex<T> z;
    int n;

  public:
    std::complex<T> getz() const { return z; }
    int getn() const { return n; }
    riemann() {
        z = T(0);
        n = 0;
    }
    riemann(std::complex<T>, int); /* if Im(z)<0 n is set to int */
    riemann(std::complex<T>, T);   /* if Im(z)<0 n is set to the sign of T */
    riemann(T, int);               /* if Im(z)<0 n is set to int */
    riemann(T, T);                 /* if Im(z)<0 n is set to the sign of T */
    riemann(std::complex<T>);      /* if Im(z)<0 n is set to 1 */
    riemann(T);                    /* if Im(z)<0 n is set to 1 */
    void print();
    std::complex<T> tocomplex() const;
    const riemann<T> operator-() const;
    riemann<T>& operator=(const riemann<T>&);
    template <class TT> friend const riemann<TT> operator*(TT, const riemann<TT>&);
    template <class TT> friend const riemann<TT> operator*(const riemann<TT>&, TT);
    template <class TT> friend const riemann<TT> operator*(const riemann<TT>&, const riemann<TT>&);
    template <class TT> friend const riemann<TT> operator/(TT, const riemann<TT>&);
    template <class TT> friend const riemann<TT> operator/(const riemann<TT>&, TT);
    template <class TT> friend const riemann<TT> operator/(const std::complex<TT>&, const riemann<TT>&);
    template <class TT> friend const riemann<TT> operator/(const riemann<TT>&, const std::complex<TT>&);
    template <class TT> friend const riemann<TT> operator/(const riemann<TT>&, const riemann<TT>&);
};

template <class T> std::complex<T> log(const riemann<T>&);
template <class T> std::complex<T> logM1(const riemann<T>&);
template <class T> std::complex<T> li2(const riemann<T>&);
template <class T> std::complex<T> li2M1(const riemann<T>&, const riemann<T>&);

/*      IEPS for explicit small imaginary part.
 *      Its value is below the threshold which is used to compare numbers with zero.
 */
template <class T> inline std::complex<T> IEPS();
template <> inline std::complex<R> IEPS() { return std::complex<R>(0., 1.0e-30); }
#ifdef HIGH_PRECISION
template <> inline std::complex<RHP> IEPS() { return std::complex<RHP>(0, "1.0e-50"); }
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline std::complex<RVHP> IEPS() { return std::complex<RVHP>(0, "1.0e-80"); }
#endif

#if INSTANTIATE_GMP
template <> inline std::complex<RGMP> IEPS() { return std::complex<RGMP>(0, DeltaZero<RGMP>()); }
#endif
}
}

namespace std {
template <class T> std::ostream& operator<<(std::ostream&, const Caravel::IntegralsBH::riemann<T>&);
}

// IMPLEMENTATIONS
#include "riemann.hpp"

#endif /* RIEMANN_H_ */
