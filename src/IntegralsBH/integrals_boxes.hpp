/********************************************************************************************
 *
 *      Divergent boxes with internal masses.
 *      I name integrals as in paper by EZ.
 *      Some of them coincide with massless integrals:
 *      I4w0m = I4n1
 *      I4w1m = I4n2
 *      I4w2me = I4n3
 *      I4w2mh = I4n4
 *      I4w3m = I4n5
 *
 *      I4n4 calculated just as I4n5 with p2sq=0, this doesn't cause any numerical
 *      instabilities.
 *
 ********************************************************************************************/

template <class T> complex<T> I4n1(int order, const T& musq, const T& s12, const T& s23) {
    complex<T> tFact = T(1) / (s12 * s23);

    if (order == -2) return T(4) * tFact;

    riemann<T> rs12(-s12 / musq, -1), rs23(-s23 / musq, -1);
    complex<T> result(0, 0);

    switch (order) {
        case -1:
            result = log(rs12) + log(rs23);
            result *= T(-2);
            return tFact * result;
        case 0: result = T(2) * log(rs12) * log(rs23) - pisqo6<T>() * T(6); return tFact * result;
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I4n2(int order, const T& musq, const T& s4, const T& s12, const T& s23) {
    complex<T> tFact = T(1) / (s12 * s23);

    if (order == -2) return T(2) * tFact;

    riemann<T> rs12(-s12 / musq, -1), rs23(-s23 / musq, -1), rs4(-s4 / musq, -1);
    complex<T> result(0, 0);

    switch (order) {
        case -1:
            result = log(rs12) + log(rs23) - log(rs4);
            result *= T(-2);
            return tFact * result;
        case 0:
            result = li2(rs4 / rs12) + li2(rs4 / rs23) + pisqo6<T>();
            result *= T(-2);
            result += T(2) * log(rs12) * log(rs23) - sq(log(rs4));
            return tFact * result;
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I4n3(int order, const T& musq, const T& s2, const T& s4, const T& s12, const T& s23) {
    if (order == -2) return C0;

    riemann<T> rs12(-s12 / musq, -1), rs23(-s23 / musq, -1), rs4(-s4 / musq, -1), rs2(-s2 / musq, -1);

    complex<T> logy = -logM1(rs4 * rs2 / (rs12 * rs23)) / (s12 * s23);

    switch (order) {
        case -1: return T(2) * logy;
        case 0:
            complex<T> result(0, 0);
            result = li2M1(rs23 / rs4, rs2 / rs12) / (s4 * s12) + li2M1(rs12 / rs4, rs2 / rs23) / (s4 * s23);
            result -= li2M1(riemann<T>(1), rs4 * rs2 / (rs12 * rs23)) / (s12 * s23);
            result -= logy * log(rs12 * rs2 * rs23 / rs4) * T(0.5);
            result *= T(2);

            return result;
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I4n4(int order, const T& musq, const T& s3, const T& s4, const T& s12, const T& s23) {
    T tFact = T(1) / (s12 * s23);

    riemann<T> rs12(-s12 / musq, -1), rs23(-s23 / musq, -1), rs4(-s4 / musq, -1), rs3(-s3 / musq, -1);

    complex<T> log1 = log(rs3 * rs4 / (rs12 * rs23 * rs23)), log34 = log(rs3 / rs4), log12 = log(rs12);

    switch (order) {
        case -2: return tFact;
        case -1: return tFact * log1;
        case 0:
            complex<T> result = T(2) * (li2(rs3 / rs23) + li2(rs4 / rs23));
            result += T(0.5) * (sq(log34) + sq(log12)) + log1 * log12;
            return -tFact * result;
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I4n5(int order, const T& musq, const T& s2, const T& s3, const T& s4, const T& s12, const T& s23) {
    if (order == -2) return C0;

    riemann<T> rs12(-s12 / musq, -1), rs23(-s23 / musq, -1), rs4(-s4 / musq, -1), rs2(-s2 / musq, -1), rs3(-s3 / musq, -1);

    riemann<T> arg1 = rs2 * rs4 / (rs12 * rs23), arg2 = rs23 * rs23 * rs23 * rs2 * rs12 / (rs4 * rs3 * rs3);

    complex<T> logy = -logM1(arg1) / (s12 * s23);

    switch (order) {
        case -1: return logy;
        case 0:
            complex<T> result(0, 0);
            result = li2M1(rs23 / rs4, rs2 / rs12) / (s4 * s12);
            result -= li2M1(riemann<T>(1), arg1) / (s12 * s23);
            result -= logy * log(arg2) * T(0.25);
            result *= T(2);

            return result;
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I4n6(int order, const T& musq, const T& s12, const T& s23, const T& msq) {
    T tFact = T(-1) / (s12 * (msq - s23));
    complex<T> result[3];

    result[2] = T(2);
    if (order == -2) return tFact * result[2];

    riemann<T> rs23m(msq - s23, -1), rs12(-s12, -1);
    complex<T> logmu = log(musq / msq), log12 = log(rs12 / msq), log23 = log(rs23m / msq);

    result[1] = T(2) * log23 + log12;
    result[1] *= T(-1);

    if (order == -1) return tFact * (result[2] * logmu + result[1]);

    result[0] = T(2) * log23 * log12 - pisqo6<T>() * T(3);

    return tFact * (result[2] * sq(logmu) * T(0.5) + result[1] * logmu + result[0]);
}

template <class T> complex<T> I4n7(int order, const T& musq, const T& s4, const T& s12, const T& s23, const T& msq) {
    T tFact = T(1) / (s12 * (s23 - msq));
    complex<T> result[3];

    result[2] = T(1.5);
    if (order == -2) return tFact * result[2];

    riemann<T> rs23m(msq - s23, -1), rs12(-s12, -1), rs4m(msq - s4, -1);
    complex<T> logmu = log(musq / msq), log12 = log(rs12 / msq), log23 = log(rs23m / msq), log4 = log(rs4m / msq);

    result[1] = log4 - T(2) * log23 - log12;

    if (order == -1) return tFact * (result[2] * logmu + result[1]);

    result[0] = T(2) * log23 * log12 - pisqo6<T>() * T(2.5) - sq(log4);
    result[0] -= T(2) * li2(rs4m / rs23m);

    return tFact * (result[2] * sq(logmu) * T(0.5) + result[1] * logmu + result[0]);
}

template <class T> complex<T> I4n8(int order, const T& musq, const T& s3, const T& s4, const T& s12, const T& s23, const T& msq) {
    T tFact = T(1) / (s12 * (s23 - msq));
    if (order == -2) return tFact;

    riemann<T> rs12(-s12, -1), rs23m(msq - s23, -1), rs4m(msq - s4, -1), rs3m(msq - s3, -1);

    complex<T> logs12mu = log(rs12 / musq);

    switch (order) {
        case -1: return -tFact * (logs12mu + log(rs23m * rs23m / (rs3m * rs4m)));
        case 0:
            complex<T> result(0, 0);

            result -= T(2) * (li2(rs3m / rs23m) + li2(rs4m / rs23m)) + li2(rs3m * rs4m / (rs12 * msq));
            result -= log(rs3m / musq) * log(rs3m / msq) + log(rs4m / musq) * log(rs4m / msq);
            result += (sq(logs12mu) - sq(log(rs12 / msq))) * T(0.5) + T(2) * logs12mu * log(rs23m / msq) - pisqo6<T>();

            return tFact * result;
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I4n9(int order, const T& musq, const T& s2, const T& s3, const T& s12, const T& s23, const T& msq) {
    T tFact = T(1) / (s12 * (s23 - msq));
    if (order == -2) return tFact * T(0.5);

    riemann<T> rs12(-s12, -1), rs23m(msq - s23, -1), rs2(-s2, -1), rs3m(msq - s3, -1);

    complex<T> log1 = log(rs12 / rs2 * rs23m / sqrt(musq * msq));

    switch (order) {
        case -1: return -tFact * log1;
        case 0:
            complex<T> result(0, 0);

            result += li2(rs3m * rs23m / (rs2 * msq)) + T(2) * li2(rs12 / rs2);
            result += pisqo6<T>() * T(0.5) + sq(log1);

            return tFact * result;
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I4n10(int order, const T& musq, const T& s2, const T& s3, const T& s4, const T& s12, const T& s23, const T& msq) {
    if (order == -2) return C0;

    complex<T> cs2 = s2, cs4 = s4, cs12 = s12, cs23 = s23;
    if (abs(s12 * (msq - s23)) < abs(s2 * (msq - s4))) {
        cs2 = s12;
        cs4 = s23;
        cs12 = s2, cs23 = s4;
    }

    riemann<T> r23(-cs2, -1), r13(-cs12, -1), r34(msq - s3, -1), r14(msq - cs4, -1), r24(msq - cs23, -1);
    riemann<T> qx1 = r23 / r13, qx2 = r24 / r14;

    complex<T> z0, z1;

    z1 = logM1(qx1 / qx2) / (msq - cs23);

    if (order == -1) return z1 / cs12;

    z0 = (iszeroI(msq - s3)) ? C0 : li2M1(r34 * r24 / (msq * r23), r34 * r14 / (msq * r13)) * (msq - s3) / (T(2) * msq * cs2);
    z0 += li2M1(qx1 / qx2, riemann<T>(1)) / (msq - cs23) - li2M1(qx1, qx2) / (msq - cs4);
    z0 += z1 * (log(msq / r24) - log(msq / musq) * T(0.5));

    return T(2) * z0 / cs12;
}

template <class T> complex<T> I4n11(int order, const T& musq, const T& s3, const T& s12, const T& s23, const T& m3sq, const T& m4sq) {
    T tFact = T(1) / (m3sq - s12) / (m4sq - s23);

    if (order == -2) return tFact;

    riemann<T> rs23m(m4sq - s23, -1), rs12m(m3sq - s12, -1);
    complex<T> log4 = log(rs23m / sqrt(m4sq * musq)), log3 = log(rs12m / sqrt(m3sq * musq));

    if (order == -1) return -tFact * (log4 + log3);

    complex<T> xs = xsij(s3, sqrt(m3sq), sqrt(m4sq));

    riemann<T> rxs(T(1) / xs, -1);

    complex<T> result = pisqo6<T>() * T(-3);

    result += T(2) * log3 * log4;
    result -= sq(log(rxs));

    return tFact * result;
}

template <class T> complex<T> I4n12(int order, const T& musq, const T& s3, const T& s4, const T& s12, const T& s23, const T& m3sq, const T& m4sq) {
    T tFact = T(1) / (m3sq - s12) / (m4sq - s23);

    if (order == -2) return tFact / T(2);

    riemann<T> rs23m((m4sq - s23) / sqrt(m4sq * musq), -1), rs12m((m3sq - s12) / sqrt(musq * m3sq), -1), rs4m((m4sq - s4) / sqrt(musq * m4sq), -1);
    complex<T> log13 = log(rs12m), log14 = log(rs4m), log24 = log(rs23m);

    if (order == -1) return tFact * (log14 - log24 - log13);

    riemann<T> rxs(T(1) / xsij(s3, sqrt(m3sq), sqrt(m4sq)), -1);
    complex<T> log34 = log(rxs);

    complex<T> result = -pisqo6<T>() / T(2);

    result += T(2) * log13 * log24 - sq(log14) - sq(log34);
    result -= T(2) * li2(rs4m / rs23m) + li2(rs4m / rs12m * rxs) + li2(rs4m / (rxs * rs12m));

    return tFact * result;
}

template <class T> complex<T> I4n13(int order, const T& musq, const T& s2, const T& s3, const T& s4, const T& s12, const T& s23, const T& m3sq, const T& m4sq) {
    if (order == -2) return C0;

    T tFact = T(1) / (sqrt(m3sq * m4sq) * musq);

    complex<T> cs2 = s2, cs4 = s4, cs12 = s12, cs23 = s23;
    if (abs((m4sq - s23) * (m3sq - s12)) < abs((m4sq - s4) * (m3sq - s2))) {
        cs2 = s12;
        cs4 = s23;
        cs12 = s2, cs23 = s4;
    }

    complex<T> r13 = (m3sq - cs12) / sqrt(musq * m3sq), r14 = (m4sq - cs4) / sqrt(musq * m4sq), r23 = (m3sq - cs2) / sqrt(musq * m3sq),
               r24 = (m4sq - cs23) / sqrt(musq * m4sq), r34 = T(1) / xsij(s3, sqrt(m3sq), sqrt(m4sq));

    riemann<T> q13(r13, -1), q14(r14, -1), q23(r23, -1), q24(r24, -1), q34(r34, -1), qy1 = q14 * q23 / q13 / q24;

    complex<T> logd = -logM1(qy1) / (r13 * r24);

    if (order == -1) return tFact * logd;

    complex<T> result = T(-2) * logd * log(q13);

    result += li2M1(q23 * q34 / q24, q13 * q34 / q14) * r34 / (r14 * r24);
    result += li2M1(q23 / (q34 * q24), q13 / (q34 * q14)) / (r34 * r14 * r24);
    result += T(2) * (li2M1(q14 / q24, q13 / q23) / (r23 * r24) - li2M1(qy1, riemann<T>(1)) / (r13 * r24));

    return tFact * result;
}

template <class T> complex<T> I4n14(int order, const T& musq, const T& s12, const T& s23, const T& m2sq, const T& m4sq) {
    if (order == -2) return C0;

    complex<T> x23 = xsij(s23, sqrt(m2sq), sqrt(m4sq));
    riemann<T> rs23(x23, 1), rs12(-s12, -1);

    complex<T> tFact = T(2) / (sqrt(m2sq * m4sq) * s12) * x23 / (C1 + x23) * logM1(rs23);

    if (order == -1) return tFact;

    return tFact * log(musq / rs12);
}

template <class T> complex<T> I4n15(int order, const T& musq, const T& s2, const T& s3, const T& s12, const T& s23, const T& m2sq, const T& m4sq) {
    if (order == -2) return C0;

    T cm2 = m2sq, cm4 = m4sq, cs2 = s2, cs3 = s3;
    if (abs(m2sq - s2) < abs(m4sq - s3)) {
        cm2 = m4sq;
        cm4 = m2sq;
        cs2 = s3;
        cs3 = s2;
    }

    complex<T> r13 = (-s12) / sqrt(musq * cm2), r23 = (cm2 - cs2) / cm2, r34 = (cm4 - cs3) / sqrt(cm2 * cm4), r24 = T(1) / xsij(s23, sqrt(cm2), sqrt(cm4));

    complex<T> tFact = r24 / (sqrt(m2sq * m4sq) * s12);

    riemann<T> q13(r13, -1), q23(r23, -1), q24(r24, -1), q34(r34, -1), qss = q13 * q13 / (q23 * q23 * q24);

    complex<T> log24 = logM1(q24) / (C1 + r24);

    if (order == -1) return tFact * log24;

    complex<T> result = -log24 * log(qss) + li2M1(q24 * q24, riemann<T>(1));
    result -= (iszeroI(r34) ? C0 : li2M1(q34 / q23 * q24, q34 / (q23 * q24)) * r34 / (r23 * r24));

    return tFact * result;
}

template <class T>
complex<T> I4n16(int order, const T& musq, const T& s2, const T& s3, const T& s12, const T& s23, const T& m2sq, const T& m3sq, const T& m4sq) {
    if (order == -2) return C0;

    T cm2 = m2sq, cm4 = m4sq, cs2 = s2, cs3 = s3;
    if (abs(m2sq) < abs(m4sq)) {
        cm2 = m4sq;
        cm4 = m2sq;
        cs2 = s3;
        cs3 = s2;
    }

    complex<T> r13 = (m3sq - s12) / sqrt(musq * m3sq), r23 = T(1) / xsij(cs2, sqrt(cm2), sqrt(m3sq)), r34 = T(1) / xsij(cs3, sqrt(m3sq), sqrt(cm4)),
               r24 = T(1) / xsij(s23, sqrt(cm2), sqrt(cm4));

    T tFact = T(1) / sqrt(m2sq * m4sq) / (s12 - m3sq);

    riemann<T> q13(r13, -1), q23(r23, -1), q24(r24, -1), q34(r34, -1), qss = q13 * q13 * q23 * q23 / q24;

    complex<T> log24 = logM1(q24) * r24 / (C1 + r24);

    if (order == -1) return tFact * log24;

    complex<T> result = -log24 * log(qss) + r24 * li2M1(q24 * q24, riemann<T>(1));
    result -= li2M1(q34 * q23 * q24, q34 * q23 / q24) * r23 * r34;
    result -= li2M1(q23 / q34 * q24, q23 / (q34 * q24)) * r23 / r34;

    return tFact * result;
}

/********************************************************************************************
 *      Convergent boxes with internal masses.
 *      Integrals named according to number of non-zero internal masses
 *      Some of them coincide with massless integrals:
 *      I4w4m = I4f0m
 *
 ********************************************************************************************/

template <class T>
complex<T> I4f4m(int order, const T& s1, const T& s2, const T& s3, const T& s4, const T& s12, const T& s23, const T& m1sq, const T& m2sq, const T& m3sq,
                 const T& m4sq) {
#if CALLING
    cout << "*******I4f4m called******" << endl;
#endif

    if (order == -2 or order == -1) return C0;

    T m1 = sqrt(m1sq);
    T m2 = sqrt(m2sq);
    T m3 = sqrt(m3sq);
    T m4 = sqrt(m4sq);

    complex<T> x1, x2, d12, d13, d14, d23, d24, d34,

        k12 = (m1sq + m2sq - s1 - s1 * IEPS<T>()) / (m1 * m2), k14 = (m1sq + m4sq - s4 - s4 * IEPS<T>()) / (m1 * m4),
        k23 = (m2sq + m3sq - s2 - s2 * IEPS<T>()) / (m2 * m3), k34 = (m3sq + m4sq - s3 - s3 * IEPS<T>()) / (m3 * m4),

        r12 = ixsij(d12, s1 + s1 * IEPS<T>(), m1, m2), r13 = ixsij(d13, s12 + s12 * IEPS<T>(), m1, m3), r14 = ixsij(d14, s4 + s4 * IEPS<T>(), m1, m4),
        r23 = ixsij(d23, s2 + s2 * IEPS<T>(), m2, m3), r24 = ixsij(d24, s23 + s23 * IEPS<T>(), m2, m4), r34 = ixsij(d34, s3 + s3 * IEPS<T>(), m3, m4),

        a = k34 / r24 + r13 * k12 - k14 * r13 / r24 - k23, b = d13 * d24 + k12 * k34 - k23 * k14, c = k12 / r13 + r24 * k34 - k14 * r24 / r13 - k23;

    solabc(x1, x2, a, b, c);

    T h1 = real(k23 - r13 * k12 - r24 * k34 + r13 * r24 * k14), h2;
    h2 = h1 * real(a) * real(x1);
    h1 *= real(a) * real(x2);

    riemann<T> qx1(-x1, -h1), qx2(-x2, -h2), q23(r23, -1), q12(r12, -1), q13(r13, -1), q14(r14, -1), q24(r24, -1), q34(r34, -1), qtt(r13 / r24, -r24.real());

    complex<T> result(0, 0);

    result += (li2M1(qx1 / q24 * q12, qx2 / q24 * q12) * r12 + li2M1(qx1 / (q24 * q12), qx2 / (q24 * q12)) / r12) / r24;
    result -= (li2M1(qtt * qx1 * q23, qtt * qx2 * q23) * r23 + li2M1(qtt * qx1 / q23, qtt * qx2 / q23) / r23) * r13 / r24;
    result += (li2M1(qx1 * q13 * q34, qx2 * q13 * q34) * r34 + li2M1(qx1 * q13 / q34, qx2 * q13 / q34) / r34) * r13;
    result -= li2M1(qx1 * q14, qx2 * q14) * r14 + li2M1(qx1 / q14, qx2 / q14) / r14;

    return -result / (a * m2 * m3 * m1 * m4);
}

//  Box with m3=0
template <class T>
complex<T> I4f3m(int order, const T& s1, const T& s2, const T& s3, const T& s4, const T& s12, const T& s23, const T& m1sq, const T& m2sq, const T& m4sq) {

    if (order == -2 or order == -1) return C0;

    T m1 = sqrt(m1sq);
    T m2 = sqrt(m2sq);
    T m4 = sqrt(m4sq);

    complex<T> x1, x2, d12, d14, d24,
        //
        k12 = (m1sq + m2sq - s1 - s1 * IEPS<T>()) / (m1 * m2), r13 = (m1sq - s12 - s12 * IEPS<T>()) / (m1 * m2),
        k14 = (m1sq + m4sq - s4 - s4 * IEPS<T>()) / (m1 * m4), r23 = (m2sq - s2 - s2 * IEPS<T>()) / m2sq, r34 = (m4sq - s3 - s3 * IEPS<T>()) / (m2 * m4),
        //
        r12 = ixsij(d12, s1 + s1 * IEPS<T>(), m1, m2), r14 = ixsij(d14, s4 + s4 * IEPS<T>(), m1, m4), r24 = ixsij(d24, s23 + s23 * IEPS<T>(), m2, m4),
        //
        a = r34 / r24 - r23, b = -r13 * d24 + k12 * r34 - k14 * r23, c = k12 * r13 + r24 * r34 - k14 * r24 * r13 - r23;

    solabc(x1, x2, a, b, c);
    x1 = -x1;
    x2 = -x2;

    riemann<T> qx1(x1, 1), qx2(x2, 1), q23(r23, -1), q12(r12, -1), q13(r13, -1), q14(r14, -1), q24(r24, -1), q34(r34, -1), qy1 = qx1 / q24, qy2 = qx2 / q24;

    complex<T> result(0, 0);

    result += li2M1(qy1 * q12, qy2 * q12) * r12 / r24 + li2M1(qy1 / q12, qy2 / q12) / (r12 * r24);
    result -= li2M1(qx1 * q14, qx2 * q14) * r14 + li2M1(qx1 / q14, qx2 / q14) / r14;

    if (iszeroI(r13)) {
        result += logM1(qx1 / qx2) * log(q23 / (q24 * q34)) / x2;
    } else {
        qy1 = q23 / (q13 * q24);
        result -= (iszeroI(r23) ? C0 : li2M1(qx1 * qy1, qx2 * qy1) * r23 / (r13 * r24));
        qy1 = q34 / q13;
        result += (iszeroI(r34) ? C0 : li2M1(qx1 * qy1, qx2 * qy1) * r34 / r13);
    }

    return -result / (a * m2sq * m1 * m4);
}

//  Box with m1=m2=0
template <class T> complex<T> I4f2m(int order, const T& s1, const T& s2, const T& s3, const T& s4, const T& s12, const T& s23, const T& m3sq, const T& m4sq) {

    if (order == -2 or order == -1) return C0;

    T m3 = sqrt(m3sq);
    T m4 = sqrt(m4sq);

    complex<T> x1, x2, d14,
        //
        r12 = (m4sq - s4 - s4 * IEPS<T>()) / (m3 * m4), r13 = (m4sq - s23 - s23 * IEPS<T>()) / (m3 * m4), k14 = (m3sq + m4sq - s3 - s3 * IEPS<T>()) / (m3 * m4),
        r23 = (-s1 - s1 * IEPS<T>()) / m3sq, r24 = (m3sq - s12 - s12 * IEPS<T>()) / m3sq, r34 = (m3sq - s2 - s2 * IEPS<T>()) / m3sq,
        //
        r14 = ixsij(d14, s3 + s3 * IEPS<T>(), m3, m4),
        //
        a = r34 * r24 - r23, b = r13 * r24 + r12 * r34 - k14 * r23, c = r12 * r13 - r23;

    solabc(x1, x2, a, b, c);
    x1 = -x1;
    x2 = -x2;

    riemann<T> qx1(x1, 1), qx2(x2, 1), q23(r23, -1), q12(r12, -1), q13(r13, -1), q14(r14, -1), q24(r24, -1), q34(r34, -1), qss;

    complex<T> result(0, 0);

    result -= li2M1(qx1 * q14, qx2 * q14) * r14 + li2M1(qx1 / q14, qx2 / q14) / r14;

    bool Or12 = iszeroI(r12), Or13 = iszeroI(r13);

    if (Or12 and Or13) {
        qss = qx1 * qx2 * q34 * q24 / q23;
        qss = qss * qss;
        result -= logM1(qx1 / qx2) * log(qss) / (x2 * T(2));
    } else {
        if (Or13) {
            qss = q34 * q12 / q23;
            qss = qx1 * qx2 * qss * qss;
            result -= logM1(qx1 / qx2) * log(qss) / (x2 * T(2));
        } else if (not iszeroI(r34)) {
            qss = q34 / q13;
            result += li2M1(qx1 * qss, qx2 * qss) * r34 / r13;
        }
        if (Or12) {
            qss = q24 * q13 / q23;
            qss = qx1 * qx2 * qss * qss;
            result -= logM1(qx1 / qx2) * log(qss) / (x2 * T(2));
        } else if (not iszeroI(r24)) {
            qss = q24 / q12;
            result += li2M1(qx1 * qss, qx2 * qss) * r24 / r12;
        }
        if ((not Or12) and (not Or13)) result -= logM1(qx1 / qx2) * log(q12 * q13 / q23) / x2;
    }

    return -result / (a * m3sq * m3 * m4);
}

template <class T> complex<T> I4f1m(int order, const T& s1, const T& s2, const T& s3, const T& s4, const T& s12, const T& s23, const T& m4sq) {

    if (order == -2 or order == -1) return C0;

    // T m4 = sqrt(m4sq);

    complex<T> x1, x2,
        //
        r12 = (m4sq - s4 - s4 * IEPS<T>()) / m4sq, r13 = (m4sq - s23 - s23 * IEPS<T>()) / m4sq, r14 = (m4sq - s3 - s3 * IEPS<T>()) / m4sq,
        r23 = (-s1 - s1 * IEPS<T>()) / m4sq, r24 = (-s12 - s12 * IEPS<T>()) / m4sq, r34 = (-s2 - s2 * IEPS<T>()) / m4sq,
        //
        a = r34 * r24, b = r13 * r24 + r12 * r34 - r14 * r23, c = r12 * r13 - r23;

    solabc(x1, x2, a, b, c);
    x1 = -x1;
    x2 = -x2;

    riemann<T> qx1(x1, 1), qx2(x2, 1), q23(r23, -1), q12(r12, -1), q13(r13, -1), q14(r14, -1), q24(r24, -1), q34(r34, -1), qss;

    complex<T> result(0, 0);

    bool Or12 = iszeroI(r12), Or13 = iszeroI(r13);

    if (Or12 and Or13) {
        qss = qx1 * qx2 * q34 * q24 / q23;
        qss = qss * qss;
        result -= logM1(qx1 / qx2) * log(qss) / (x2 * T(2));
    } else {
        if (Or13) {
            qss = q34 * q12 / q23;
            qss = qx1 * qx2 * qss * qss;
            result -= logM1(qx1 / qx2) * log(qss) / (x2 * T(2));
        } else {
            qss = q34 / q13;
            result += li2M1(qx1 * qss, qx2 * qss) * r34 / r13;
        }
        if (Or12) {
            qss = q24 * q13 / q23;
            qss = qx1 * qx2 * qss * qss;
            result -= logM1(qx1 / qx2) * log(qss) / (x2 * T(2));
        } else {
            qss = q24 / q12;
            result += li2M1(qx1 * qss, qx2 * qss) * r24 / r12;
        }
        if ((not Or12) and (not Or13)) result -= logM1(qx1 / qx2) * log(q12 * q13 / q23) / x2;
    }

    result -= iszeroI(r14) ? C0 : li2M1(qx1 * q14, qx2 * q14) * r14;

    return -result / (a * sq(m4sq));
}

template <class T> complex<T> I4f0m(int order, const T& s1, const T& s2, const T& s3, const T& s4, const T& s12, const T& s23) {

    if (order == -2 or order == -1) return C0;

    complex<T> x1, x2,
        //
        a = s3 * s23, b = s12 * s23 + s1 * s3 - s4 * s2, c = s1 * s12;

    solabc(x1, x2, a, b, c);
    x1 = -x1;
    x2 = -x2;

    riemann<T> qx1(x1, -s2), qx2(x2, s2), q23(-s2, -1), q12(-s1, -1), q13(-s12, -1), q14(-s4, -1), q24(-s23, -1), q34(-s3, -1), qss = q34 / q13;

    complex<T> result = li2M1(qx1 * qss, qx2 * qss) * s3 / s12;

    qss = q24 / q12;
    result += li2M1(qx1 * qss, qx2 * qss) * s23 / s1;

    result += logM1(qx1 / qx2) / x2 * (log(qx1 * qx2) / T(2) - log(q12 * q13 / (q14 * q23)));

    return -result / a;
}
