/*****************************************************************************************
 * This file is a part of libIntegrals.
 *
 * Contains utilities and auxiliary functions for calculating dilogs and integrals.
 *****************************************************************************************/

#ifndef INTEGRALS_UTILITIES_H_
#define INTEGRALS_UTILITIES_H_

#include <complex>
#include <numeric>
#include <vector>

#include "Core/Utilities.h"
#include "Core/momD_conf.h"
#include "polylog.h"
#if INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

#include "misc/DeltaZero.h"

namespace Caravel {
namespace IntegralsBH {

using std::complex;

template <class T> inline T SqSum_1(const momD_conf<complex<T>, 4>& mc, const std::vector<size_t>& v1) {
    momD<std::complex<T>, 4> sum(mc.p(v1[0]));
    sum = std::accumulate(v1.begin() + 1, v1.end(), sum, [mc](momD<std::complex<T>, 4>& m, size_t i) { return m += mc.p(i); });
    return real(sum * sum);
}

template <class T> inline T SqSum_1(const momD_conf<complex<T>, 4>& mc, const std::vector<size_t>& v1, const std::vector<size_t>& v2) {
    momD<std::complex<T>, 4> sum(mc.p(v1[0]));
    sum = std::accumulate(v1.begin() + 1, v1.end(), sum, [mc](momD<std::complex<T>, 4>& m, size_t i) { return m += mc.p(i); });
    sum = std::accumulate(v2.begin(), v2.end(), sum, [mc](momD<std::complex<T>, 4>& m, size_t i) { return m += mc.p(i); });
    return real(sum * sum);
}

#if 0
template <class T> inline T SqSum(const eval_param<T> & k, const std::vector<int>& v1) {
  momentum <std::complex<T> > sum;
  for (int j = 0;  j < v1.size();  j += 1) sum += k.mom(v1[j]);
  return real(sum*sum);
}

template <class T> inline T SqSum(const eval_param<T> & k, const std::vector<int>& v1, const std::vector<int>& v2) {
    momentum <std::complex<T> > sum;
    for (int j = 0;  j < v1.size();  j += 1) sum += k.mom(v1[j]);
    for (int j = 0;  j < v2.size();  j += 1) sum += k.mom(v2[j]);
    return real(sum*sum);
}

#endif

//      Epsilon to compare with zero
template <class T> inline T _epsilon();
template <> inline R _epsilon<R>() { return R(1e-11); }
#ifdef HIGH_PRECISION
template <> inline RHP _epsilon<RHP>() { return RHP(1e-27); }
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline RVHP _epsilon<RVHP>() { return RVHP(1e-59); }
#endif
#if INSTANTIATE_GMP
template <> inline RGMP _epsilon<RGMP>() { return RGMP(1e-70); }
#endif

template <class T> inline bool iszeroI(const T& x) {
    using std::abs;
    if (abs(x) < _epsilon<T>())
        return true;
    else
        return false;
}
template <class T> inline bool iszeroI(const std::complex<T>& x) {
    using std::abs;
    if (abs(x.real()) < _epsilon<T>() and abs(x.imag()) < _epsilon<T>())
        return true;
    else
        return false;
}

/*      A function to find a sign for any type of real argument
 *      (returns 1 if argument is 0)
 */
template <class T> inline int sgn(const T& x) { return (x >= T(0)) - (x < T(0)); }

template <class T> inline T pisqo6() { return sq(Caravel::IntegralsBH::pi<T>()) / T(6); }
}
}
#endif /* INTEGRALS_UTILITIES_H_ */
