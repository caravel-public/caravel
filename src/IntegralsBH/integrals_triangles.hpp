/********************************************************************************************
 *      Divergent triangles with internal masses.
 *      I name integrals as in paper by EZ.
 *      Some of them coincide with massless integrals:
 *      I3w1m = I3n1
 *      I3w2m = I3n2
 *
 ********************************************************************************************/

template <class T> complex<T> I3n1(int order, const T& musq, const T& s3) {
#if CALLING
    cout << "*******I3n1 called******" << endl;
#endif

    T tFact = T(1) / s3;
    riemann<T> r3(-s3 / musq, -1);

    switch (order) {
        case -2: return tFact;
        case -1: return -tFact * log(r3);
        case 0: return tFact * T(0.5) * sq(log(r3));
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I3n2(int order, const T& musq, const T& s2, const T& s3) {
#if CALLING
    cout << "*******I3n2 called******" << endl;
#endif

    if (order == -2) return C0;

    riemann<T> rs2(-s2 / musq, -1), rs3(-s3 / musq, -1);
    T tFact = T(1) / musq;

    switch (order) {
        case -1: return tFact * mlog12(rs3, rs2);
        case 0: return tFact * mlog12sq(rs2, rs3) * T(-0.5);
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I3n3(int order, const T& musq, const T& s2, const T& s3, const T& msq) {
#if CALLING
    cout << "*******I3n3 called******" << endl;
#endif

    if (order == -2) return C0;

    complex<T> result[2];

    riemann<T> rs2m(T(1) - s2 / msq, -1), rs3m(T(1) - s3 / msq, -1);
    T tFact = T(1) / msq;

    result[1] = mlog12(rs3m, rs2m);

    if (order == -1) return tFact * result[1];

    result[0] = -li2M1(rs2m, rs3m) - log(rs3m * rs2m) * result[1];
    result[0] += result[1] * log(musq / msq);

    return (tFact * result[0]);
}

template <class T> complex<T> I3n4(int order, const T& musq, const T& s2, const T& msq) {
#if CALLING
    cout << "*******I3n4 called******" << endl;
#endif

    T tFact = T(1) / (s2 - msq);
    complex<T> result[3];

    result[2] = T(0.5);
    if (order == -2) return tFact * result[2];

    riemann<T> rs2m(msq - s2, -1);
    complex<T> logmu = log(musq / msq), log2 = -log(rs2m / msq);

    result[1] = log2;

    if (order == -1) return tFact * (result[2] * logmu + result[1]);

    result[0] = pisqo6<T>() * T(0.5) + sq(log2) * T(0.5) - li2(msq / rs2m);

    return tFact * (result[2] * sq(logmu) * T(0.5) + result[1] * logmu + result[0]);
}

template <class T> complex<T> I3n5(int order, const T& musq, const T& msq) {
#if CALLING
    cout << "*******I3n5 called******" << endl;
#endif

    switch (order) {
        case -2: return C0;
        case -1: return T(-0.5) / msq;
        case 0: return (T(1) + log(msq / musq) * T(0.5)) / msq;
    }
    // all other powers set to zero
    return C0;
}

template <class T> complex<T> I3n6(int order, const T& musq, const T& s, const T& m2sq, const T& m3sq) {
#if CALLING
    cout << "*******I3n6 called******" << endl;
#endif

    if (order == -2) return C0;
    complex<T> result[2];

    T m2 = sqrt(m2sq);
    T m3 = sqrt(m3sq);

    complex<T> xs = xsij(s, m2, m3);
    riemann<T> rxs(xs, 1);

    complex<T> tFact = C1 / (m2 * m3);

    result[1] = xs / (xs + C1) * logM1(rxs);

    if (order == -1) return tFact * result[1];

    T rel = m3 / m2;
    result[0] = result[1] * (log(musq / m3sq) + log(rxs));
    result[0] += li2M1(rxs * rxs, riemann<T>(1)) * xs - rel * li2M1(rxs * rel, T(1) / rxs * rel);

    return tFact * result[0];
}

/********************************************************************************************
 *      Convergent triangles with internal masses.
 *      Integrals named according to number of non-zero internal masses
 *      Some of them coincide with massless integrals:
 *      I3w3m = I3f0m
 *
 *      Overall minus sign for triangles compared to EZ and Hameren is
 *      included as in all massless triangles.
 ********************************************************************************************/

template <class T> complex<T> I3f0m(int order, const T& s1, const T& s2, const T& s3) {
#if CALLING
    cout << "*******I3f0m called******" << endl;
#endif

    if (order == -2 or order == -1) return C0;

    complex<T> x1, x2, a = s2 * s3, b = s1 - s2 - s3;

    solabc(x1, x2, a, b, C1);
    x1 = -x1;
    x2 = -x2;

    riemann<T> qx1(x1, -s1), qx2(x2, s1), q23(-s1, -1), q24(-s3, -1), q34(-s2, -1);

    complex<T> result = -s2 * li2M1(qx1 * q34, qx2 * q34);

    result -= s3 * li2M1(qx1 * q24, qx2 * q24);
    result += logM1(qx1 / qx2) / x2 * (log(qx1 * qx2) * T(0.5) + log(q23));

    return result / a;
}

template <class T> complex<T> I3f1m(int order, const T& s1, const T& s2, const T& s3, const T& m3sq) {
#if CALLING
    cout << "*******I3f1m called******" << endl;
#endif

    if (order == -2 or order == -1) return C0;

    T m3 = sqrt(m3sq);
    complex<T> x1, x2, r23 = (-s1 - s1 * IEPS<T>()) / m3sq, r24 = (m3sq - s3 - s3 * IEPS<T>()) / m3sq, r34 = (m3sq - s2 - s2 * IEPS<T>()) / m3sq,
                       a = r34 * r24 - r23, b = (r24 + r34 - r23) / m3;

    solabc(x1, x2, a, b, C1 / m3sq);
    x1 = -x1;
    x2 = -x2;

    riemann<T> qx1(x1, 1), qx2(x2, 1), q23(r23, -1), q24(r24, -1), q34(r34, -1);

    complex<T> result = logM1(qx1 / qx2) * log(qx1 * qx2 / m3sq) / (T(2) * x2);

    result -= m3 * li2M1(qx1 * m3, qx2 * m3);
    result += (iszeroI(r34) ? C0 : m3 * r34 * li2M1(qx1 * q34 * m3, qx2 * q34 * m3));
    result += (iszeroI(r24) ? C0 : m3 * r24 * li2M1(qx1 * q24 * m3, qx2 * q24 * m3));
    result += logM1(qx1 / qx2) * log(q23 * m3sq) / x2;

    return result / (a * m3 * m3sq);
}

template <class T> complex<T> I3f2m(int order, const T& s1, const T& s2, const T& s3, const T& m2sq, const T& m3sq) {
#if CALLING
    cout << "*******I3f2m called******" << endl;
#endif

    if (order == -2 or order == -1) return C0;

    T m2 = sqrt(m2sq);
    T m3 = sqrt(m3sq);

    complex<T> x1, x2, d24, r23 = (m3sq - s3 - s3 * IEPS<T>()) / m3sq, r24 = ixsij(d24, s2 + s2 * IEPS<T>(), m3, m2),
                            r34 = (m2sq - s1 - s1 * IEPS<T>()) / (m2 * m3), a = r34 / r24 - r23, b = -d24 / m3 + r34 / m3 - r23 / m2,
                            c = (m2 / m3 - r24) / (m3 * m2);

    solabc(x1, x2, a, b, c);
    x1 = -x1;
    x2 = -x2;

    riemann<T> qx1(x1, 1), qx2(x2, 1), q23(r23, -1), q24(r24, -1), q34(r34, -1), qy1 = qx1 / q24, qy2 = qx2 / q24;

    complex<T> result = li2M1(qy1 * m3, qy2 * m3) / r24 * m3;
    result -= li2M1(qx1 * m2, qx2 * m2) * m2;
    result -= (iszeroI(x2) ? C0 : (logM1(qy1 / qy2) * log(qy1 * qy2 / m3sq) - logM1(qx1 / qx2) * log(qx1 * qx2 / m2sq)) / (x2 * T(2)));
    result -= (iszeroI(r23) ? C0 : li2M1(qx1 * q23 * m3 / q24, qx2 * q23 * m3 / q24) * r23 * m3 / r24);
    result += (iszeroI(r34) ? C0 : li2M1(qx1 * q34 * m3, qx2 * q34 * m3) * r34 * m3);

    return result / (a * m2 * m3sq);
}

template <class T> complex<T> I3f3m(int order, const T& s1i, const T& s2i, const T& s3i, const T& m1sqi, const T& m2sqi, const T& m3sqi) {
#if CALLING
    cout << "*******I3f3m called******" << endl;
#endif

    if (order == -2 or order == -1) return C0;

    T s1 = s3i, s2 = s1i, s3 = s2i, m1sq = m3sqi, m2sq = m1sqi, m3sq = m2sqi;

    T m1 = sqrt(m1sq);
    T m2 = sqrt(m2sq);
    T m3 = sqrt(m3sq);

    complex<T> x1, x2, d12, d13, d23, k12 = (m1sq + m2sq - s1 - s1 * IEPS<T>()) / (m1 * m2), k23 = (m2sq + m3sq - s2 - s2 * IEPS<T>()) / (m3 * m2),
                                      r12 = ixsij(d12, s1 + s1 * IEPS<T>(), m1, m2), r13 = ixsij(d13, s3 + s3 * IEPS<T>(), m1, m3),
                                      r23 = ixsij(d23, s2 + s2 * IEPS<T>(), m2, m3), a = m2 / m3 - k23 + r13 * (k12 - m2 / m1),
                                      b = d13 / m2 + k12 / m3 - k23 / m1, c = (m1 / m3 - T(1) / r13) / (m1 * m2);

    solabc(x1, x2, a, b, c);
    x1 = -x1;
    x2 = -x2;

    riemann<T> qx1(x1, 1), qx2(x2, 1), q23(r23, -1), q12(r12, -1), q13(r13, -1);

    complex<T> result(0, 0);

    result += (li2M1(qx1 * m2 * q12, qx2 * m2 * q12) * r12 + li2M1(qx1 * m2 / q12, qx2 * m2 / q12) / r12) * m2;
    result -= (li2M1(q13 * qx1 * q23 * m2, q13 * qx2 * q23 * m2) * r23 + li2M1(q13 * qx1 / q23 * m2, q13 * qx2 / q23 * m2) / r23) * m2 * r13;
    result += li2M1(qx1 * m3 * q13, qx2 * m3 * q13) * r13 * m3 - li2M1(qx1 * m1, qx2 * m1) * m1;
    result -= (iszeroI(x2) ? C0 : (logM1(qx1 / qx2) * log(qx1 * q13 * qx2 * q13 / m3sq) - logM1(qx1 / qx2) * log(qx1 * qx2 / m1sq)) / (x2 * T(2)));

    return result / (a * m2 * m3 * m1);
}
