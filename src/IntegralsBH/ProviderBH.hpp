
template <typename T> ProviderBH<T>::ProviderBH(const std::vector<Caravel::lGraph::IntegralGraph>& intGr) : mu_square(T(1)), integral_graphs(intGr) {
    construct_integrals();
}

template <typename T> void lambdas_dummy(const Caravel::lGraph::IntegralGraph& iG, std::vector<std::function<Series<T>(const momD_conf<T, 4>&)>>& lambdas) {
    lambdas.push_back([=](const momD_conf<T, 4>& in) mutable {
        std::cout << "ProviderBH can only work in complex floating-point types!" << std::endl;
        throw std::runtime_error("Only complex floating-point types in ProviderBH!");
        return Series<T>(0, 0);
    });
}

template <typename T>
void dummy_integrals(const Caravel::lGraph::IntegralGraph& iG,
                     std::vector<std::function<Integrals::NumericFunctionBasisExpansion<
                         Series<T>, Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>>(const momD_conf<T, 4>&)>>&
                         integral_nfbe) {
    integral_nfbe.push_back([=](const momD_conf<T, 4>& in) mutable {
        std::cout << "ProviderBH doesn't provide a (NumericFunctionBasisExpansion Dummy) semi-analytic integral evaluation!" << std::endl;
        throw std::runtime_error("No NFBE evaluation in ProviderBH!");
        return Integrals::NumericFunctionBasisExpansion<Series<T>,
                                                        Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>>();
    });
}

template <typename T>
std::enable_if_t<!is_floating_point<T>::value> fill_lambdas_dummy(const Caravel::lGraph::IntegralGraph& iG,
                                                                  std::vector<std::function<Series<T>(const momD_conf<T, 4>&)>>& lambdas) {
    lambdas.push_back([=](const momD_conf<T, 4>& in) mutable {
        std::cout << "You are trying to evaluate a 1-loop integral with exact kinematics! Returned 0!" << std::endl;
        return Series<T>(0, 0);
    });
}

template <typename T>
std::enable_if_t<!is_floating_point<T>::value> fill_lambdas_pentagon_IntLib(const Caravel::lGraph::IntegralGraph& iG,
                                                                            std::vector<std::function<Series<T>(const momD_conf<T, 4>&)>>& lambdas) {
    fill_lambdas_dummy(iG, lambdas);
}

void message_and_throw_invalid_integral(const Caravel::lGraph::IntegralGraph& iG) {
    std::cerr << "ERROR: calling 1-loop integral " << iG << " of type: " << settings::integrals::integral_family_1L.value
              << " when linking with ProviderBH! Use instead: settings::use_setting(\"integrals::integral_family_1L integrals_BH\");" << std::endl;
    throw std::runtime_error("Got invalid option for integral family with ProviderBH!");
}

// Generic cases not covered by integralsBH
template <typename T>
std::enable_if_t<!is_floating_point<T>::value || !is_complex<T>::value>
generic_lambdas_eps0(const Caravel::lGraph::IntegralGraph& iG, std::vector<std::function<Series<T>(const momD_conf<T, 4>&)>>& lambdas, const std::vector<T>& masses,
                     const T& mu_square) {
    lambdas_dummy(iG, lambdas);
}

template <typename T>
std::function<Series<T>(const momD_conf<T, 4>&)> zero_series_lambda() {
    auto toret = [](const momD_conf<T, 4>& in) mutable {
            // return 0
            return Series<T>(-2,0);
        };
    return toret;
}

// here we link to integralsBH
template <typename T>
std::enable_if_t<is_floating_point<T>::value && is_complex<T>::value>
generic_lambdas_eps0(const Caravel::lGraph::IntegralGraph& iG, std::vector<std::function<Series<T>(const momD_conf<T, 4>&)>>& lambdas, const std::vector<T>& masses,
                     const T& mu_square) {

    // pentagon is trivial to O(eps)
    if ( iG.number_of_vertices() == 5 ){
        lambdas.push_back(zero_series_lambda<T>());
        return;
    }
    // scalar cases, not including pentagon, coded on the IntegralsBH library
    else if (iG.get_insertion() == IntegralInsertion::scalar && iG.number_of_vertices() < 5) {
#ifdef DEBUG_ON
        bool trackeval(false);
#endif
        lambdas.push_back([&mu_square, lmasses = masses
                              , iGlocal = iG
#ifdef DEBUG_ON               
                        ,trackeval = trackeval
#endif
                          ](const momD_conf<T, 4>& in) mutable {
#ifdef DEBUG_ON
            if (!trackeval && iGlocal.get_graph().has_massive_link()) {
                std::cout << "CAREFUL: evaluating IntegralsBH for " << iGlocal
                          << " assuming constant massive internal particles or massive on-shell external legs."
                          << std::endl;
                trackeval = true;
            }
#endif
            return IntegralsBH::integral(iGlocal, in, lmasses, mu_square);
        });
        return;
    }
    // box with mu^2 insertion
    else if (iG.get_insertion() == IntegralInsertion::mu2 && iG.number_of_vertices() == 4) {
        lambdas.push_back(zero_series_lambda<T>());
        return;
    }
    // box with mu^4 insertion
    else if (iG.get_insertion() == IntegralInsertion::mu4 && iG.number_of_vertices() == 4) {
        lambdas.push_back(
                [](const momD_conf<T, 4>& in) mutable {
                   // return 0
                   return Series<T>(-2,0,T(0),T(0),T(-1)/T(6));
                }
            );
        return;
    }
    // triangle with mu^2 insertion
    else if (iG.get_insertion() == IntegralInsertion::mu2 && iG.number_of_vertices() == 3) {
        lambdas.push_back(
                [](const momD_conf<T, 4>& in) mutable {
                   // return 0
                   return Series<T>(-2,0,T(0),T(0),T(-1)/T(2));
                }
            );
        return;
    }
    // bubble with mu^2 insertion
    else if (iG.get_insertion() == IntegralInsertion::mu2 && iG.number_of_vertices() == 2) {
        size_t i1 = iG.get_propagator_mass_index(1);
        size_t i2 = iG.get_propagator_mass_index(2);
        auto momindices = iG.vertex_momentum_indices(1);
        if (i1 == 0 && i2 == 0) { /* massless bubble */
            lambdas.push_back(
                [momindices](const momD_conf<T, 4>& in) mutable { return Series<T>(-2, 0, T(0), T(0), T(1) / T(6) * in.s(momindices)); });
        }
        else if (i1 == 0 || i2 == 0) { /* one-mass bubble */
            lambdas.push_back([momindices, m = masses, i = std::max(i1, i2)](const momD_conf<T, 4>& in) mutable {
                return Series<T>(-2, 0, T(0), T(0), T(1) / T(6) * in.s(momindices) - m[i-1]*m[i-1] / T(2) );
            });
        }
        else { /* two-mass bubble */
            lambdas.push_back([momindices, m = masses, i1, i2](const momD_conf<T, 4>& in) mutable {
                return Series<T>(-2, 0, T(0), T(0), T(1) / T(6) * in.s(momindices) - (m[i1-1] * m[i1-1] + m[i2-1] * m[i2-1]) / T(2));
            });
        }
        return;
    }
    else {
        throw std::runtime_error("Unknown 1-loop " + std::to_string(iG.number_of_vertices()) + "-point integral (in IntegralsBH) with insertion \"" +
                                 iG.get_insertion_string() + "\"");
    }
}

template <typename T>
void fill_integral_containers(
    const Caravel::lGraph::IntegralGraph& iG, std::vector<std::function<Series<T>(const momD_conf<T, 4>&)>>& integral_series,
    std::vector<std::function<Integrals::NumericFunctionBasisExpansion<
        Series<T>, Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>>(const momD_conf<T, 4>&)>>& integral_nfbe,
    const std::vector<T>& masses, const T& mu_square) {
    if (iG.get_n_loops() != 1) {
        std::cout << "ERROR: ProviderBH received a " << iG.get_n_loops() << "-loop integral and it only knows how to handle 1-loop integrals"
                  << std::endl;
        return;
    }
    // check that request is for corresponding integrals
    if (settings::integrals::integral_family_1L.value != settings::integrals::family_types_1l::integrals_BH) {
        message_and_throw_invalid_integral(iG);
    }

    generic_lambdas_eps0(iG, integral_series, masses, mu_square);
    dummy_integrals(iG, integral_nfbe);
}

template <typename T> void ProviderBH<T>::construct_integrals() {

    // allocate space for masses
    masses = std::vector<T>(ParticleMass::mass_index_global, T(0));
    // no storage for massless particles (as they are set to zero always)
    for (size_t ii = 0; ii < masses.size(); ii++) { masses[ii] = ParticleMass::particle_mass_container[ii].get_mass(T(1)); }

    // fill the lambdas
    for (size_t ii = 0; ii < integral_graphs.size(); ii++) fill_integral_containers(integral_graphs[ii], integral_series, integral_nfbe, masses, mu_square);
}

template <typename T> std::vector<Series<T>> ProviderBH<T>::compute(const momD_conf<T, 4>& in) {
    std::vector<Series<T>> toret(integral_graphs.size());
    for (size_t ii = 0; ii < integral_series.size(); ii++) {
        toret[ii] = integral_series[ii](in);
        // std::cout<<"The integral_"<<ii<<": "<<integral_graphs[ii]<<" evaluates to: "<<integral_values[ii]<<std::endl;
    }
    return toret;
}

template <typename T>
std::vector<
    Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>>>
ProviderBH<T>::get_abstract_integral(const momD_conf<T, 4>& in) const {
    std::vector<
        Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>>>
        toret(integral_nfbe.size());

    for (size_t ii = 0; ii < integral_nfbe.size(); ii++) { toret[ii]=integral_nfbe[ii](in); }
    return toret;
}

template <typename T>
std::function<
  std::vector<
    Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>>
  >(const momD_conf<T, 4>& in)> ProviderBH<T>::get_int_evaluator() const {
    return [lintegral_nfbe=integral_nfbe](const momD_conf<T, 4>& in){
            std::vector<
                Integrals::NumericFunctionBasisExpansion<Series<T>, Integrals::StdBasisFunction<Integrals::floating_partner_t<T>, Integrals::floating_partner_t<T>>>>
                toret(lintegral_nfbe.size());
            for (size_t ii = 0; ii < lintegral_nfbe.size(); ii++) { toret[ii]=lintegral_nfbe[ii](in); }
            return toret;
        };
}


template <typename T>
void ProviderBH<T>::set_mu_squared(const T& in) { mu_square=in; }

// Declare instantiations in accordance with instantiation definitions in ProviderBH.cpp
#define _INST_PROVIDERBH(KEY,TYPE) \
    KEY template class ProviderBH<TYPE>;

_INST_PROVIDERBH(extern,C)

#ifdef HIGH_PRECISION
_INST_PROVIDERBH(extern,CHP)
#endif

#ifdef VERY_HIGH_PRECISION
_INST_PROVIDERBH(extern,CVHP)
#endif

