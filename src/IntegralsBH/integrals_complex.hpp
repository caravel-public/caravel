#define CZRO std::complex<T>(0, 0)
#define RZRO T(0)
#define CONE std::complex<T>(1, 0)

#define IEPS IEPS<T>()
#define EPSN2 (IEPS.imag() * (T(1e-20)))
#define neglig(prcpar) (IEPS.imag() * T(1e10))
#define ipi std::complex<T>(0, Caravel::IntegralsBH::pi<T>())
#define logc log
#define li2c li2
#define li2c2 li2M1

#define base(i) base[i - 1]
#define casetable(i) casetable[i]

namespace Caravel {
namespace IntegralsBH {

using std::complex;

// finds solutions x1,x2 of  aa*x^2+bb*x+cc=0
template <class T> void solabc_c(complex<T>& x1, complex<T>& x2, const complex<T>& aa, const complex<T>& bb, const complex<T>& cc) {
    if (aa == CZRO) {
        if (bb == CZRO) {
            x1 = T(0);
            x2 = T(0);
        } else {
            x1 = -cc / bb;
            x2 = x1;
        }
    } else if (cc == CZRO) {
        x1 = -bb / aa;
        x2 = T(0);
    } else {
        complex<T> dd = sqrt(bb * bb - T(4) * aa * cc);
        // if (abs(dd.imag()) < IEPS*T(100)) dd -= IEPS<T>();
        // dd = sqrt(dd);
        complex<T> qq = -bb + dd, hh = -bb - dd;
        //     cout <<  "qq, hh: " << qq << hh<< endl;
        if (abs(qq) > abs(hh)) {
            x1 = qq / (T(2) * aa);
            x2 = (T(2) * cc) / qq;
        } else {
            x2 = hh / (T(2) * aa);
            x1 = (T(2) * cc) / hh;
        }
    }
}

template <class T> void solabc2(complex<T>& x1, complex<T>& x2, const T& aa, const complex<T>& bb, const complex<T>& cc) {
    if (aa == RZRO) {
        if (bb == CZRO) {
            x1 = RZRO;
            x2 = RZRO;
        } else {
            x1 = -cc / bb;
            x2 = x1;
        }
    } else if (cc == CZRO) {
        x1 = -bb / aa;
        x2 = RZRO;
    } else {
        CXT t1, t2;
        T xx, yy, pp, qq, uu, vv, pq1, pq2, uv1, uv2, dd, xd1, xd2, yd1, yd2, gg, hh, rx1, rx2, ix1, ix2;
        t1 = cc / aa;
        xx = real(t1);
        yy = imag(t1);
        t2 = bb / (aa * T(2));
        pp = -real(t2);
        uu = -imag(t2);
        t2 = sqrt(t2 * t2 - t1);
        qq = real(t2);
        vv = imag(t2);
        pq1 = pp + qq;
        uv1 = uu + vv;
        pq2 = pp - qq;
        uv2 = uu - vv;
        dd = pq1 * pq1 + uv1 * uv1;
        xd1 = xx / dd;
        yd1 = yy / dd;
        dd = pq2 * pq2 + uv2 * uv2;
        xd2 = xx / dd;
        yd2 = yy / dd;
        if (abs(pq1) > abs(pq2)) {
            rx1 = pq1;
            gg = xd1 * pq1;
            hh = yd1 * uv1;
            rx2 = gg + hh;
            if (abs(rx2) < neglig(prcpar) * max(abs(gg), abs(hh))) rx2 = RZRO;
        } else if (abs(pq2) > abs(pq1)) {
            rx2 = pq2;
            gg = xd2 * pq2;
            hh = yd2 * uv2;
            rx1 = gg + hh;
            if (abs(rx1) < neglig(prcpar) * max(abs(gg), abs(hh))) rx1 = RZRO;
        } else {
            rx1 = pq1;
            rx2 = pq2;
        }
        if (abs(uv1) > abs(uv2)) {
            ix1 = uv1;
            gg = yd1 * pq1;
            hh = xd1 * uv1;
            ix2 = gg - hh;
            if (abs(ix2) < neglig(prcpar) * max(abs(gg), abs(hh))) ix2 = RZRO;
        } else if (abs(uv2) > abs(uv1)) {
            ix2 = uv2;
            gg = yd2 * pq2;
            hh = xd2 * uv2;
            ix1 = gg - hh;
            if (abs(ix1) < neglig(prcpar) * max(abs(gg), abs(hh))) ix1 = RZRO;
        } else {
            ix1 = uv1;
            ix2 = uv2;
        }
        x1 = CXT(rx1, ix1);
        x2 = CXT(rx2, ix2);
    }
}

template <class T> inline int sgnIm(const complex<T>& zz, int ii) {
    if (iszeroI(zz.imag())) return sgn(ii);
    return sgn(zz.imag());
}

template <class T> inline complex<T> eta3(const complex<T>& aa, const T& sa, const complex<T>& bb, const T& sb, const complex<T>& cc, const T& sc) {
    T ima = imag(aa), imb = imag(bb), imc = imag(cc);

    if (ima == T(0)) ima = sa;
    if (imb == T(0)) imb = sb;
    if (imc == T(0)) imc = sc;

    ima = sgn(ima);
    imb = sgn(imb);
    imc = sgn(imc);

    if (ima == imb and ima != imc)
        return T(2) * imc * ipi;
    else
        return CZRO;
}

template <class T> inline complex<T> r0fun(const complex<T>& y1, const complex<T>& y2) {
    complex<T> oy1 = CONE - y1, oy2 = CONE - y2;
    return -logM1(riemann<T>(-y2) / riemann<T>(-y1)) / y1 - logM1(riemann<T>(oy2) / riemann<T>(oy1)) / oy1;
}

template <class T>
complex<T> plnr(const complex<T>& y1, const complex<T>& y2, int p1, int p2, const complex<T>& aa, const complex<T>& bb, const complex<T>& cc) {
    complex<T> xx;

    int ptrn = p1 | p2 << 1;

    switch (ptrn) {
        case 3: {
            xx = aa / (bb * y1 + cc);
            riemann<T> q1(xx);
            xx = aa / (bb * y2 + cc);
            riemann<T> q2(xx);
            return -logM1(q2 / q1) * T(2) * ipi * bb * xx / aa;
        }
        case 1: {
            xx = aa / (bb * y1 + cc);
            riemann<T> q1(xx);
            return log(q1) * T(2) * ipi / (y1 - y2);
        }
        case 2: {
            xx = aa / (bb * y2 + cc);
            riemann<T> q2(xx);

            return log(q2) * T(2) * ipi / (y2 - y1);
        }
        default: return CZRO;
    }
}

template <class T> complex<T> r1fun(const complex<T>& zz, const complex<T>& y1, const complex<T>& y2, const complex<T>& fy1y2) {
    complex<T> rslt(0, 0), oz;
    T h12, hz1, hz2, hzz, hoz;

    bool zzsmal = false, ozsmal = false;

    oz = CONE - zz;
    h12 = abs(y1 - y2);
    hz1 = abs(y1 - zz);
    hz2 = abs(y2 - zz);
    hzz = abs(zz);
    hoz = abs(oz);

    riemann<T> q1z(y1 - zz), q2z(y2 - zz);

    if (hzz < hz1 and hzz < hz2 and hzz < hoz) {
        zzsmal = true;
        rslt = fy1y2 * log(q1z) + (log(q1z * q2z) / T(2) + log(riemann<T>((y2 - T(1)) / y2)) - log(riemann<T>(oz))) * logM1(q1z / q2z) / (y2 - zz);
    } else if (hoz < hz1 and hoz < hz2) {
        ozsmal = true;
        rslt = fy1y2 * log(q1z) + (-log(q1z * q2z) / T(2) + log(riemann<T>((y2 - T(1)) / y2)) + log(riemann<T>(-zz))) * logM1(q1z / q2z) / (y2 - zz);
    } else if (h12 <= hz2 and hz2 <= hz1)
        rslt = fy1y2 * logc(q1z) + r0fun(y2, zz) * logM1(q1z / q2z);
    else if (h12 <= hz1 and hz1 <= hz2)
        rslt = fy1y2 * logc(q2z) + r0fun(y1, zz) * logM1(q2z / q1z);
    else {
        if (hz1 != RZRO) rslt += (y1 - zz) * logc(q1z) * r0fun(y1, zz);
        if (hz2 != RZRO) rslt -= (y2 - zz) * logc(q2z) * r0fun(y2, zz);
        rslt /= y1 - y2;
    }

    if (zzsmal) {
        riemann<T> qq(-zz);
        rslt += (li2c(qq / q1z) - li2c(qq / q2z)) / (y1 - y2);
    } else {
        riemann<T> qq(-zz);
        rslt += li2c2(q1z / qq, q2z / qq) / zz;
    }

    if (ozsmal) {
        riemann<T> qq(oz);
        rslt -= (li2c(qq / q1z) - li2c(qq / q2z)) / (y1 - y2);
    } else {
        riemann<T> qq(oz);
        rslt += li2c2(q1z / qq, q2z / qq) / oz;
    }

    return rslt;
}

template <class T>
complex<T> s3fun(const complex<T>& y1i, const complex<T>& y2i, const complex<T>& dd, const complex<T>& ee, const complex<T>& aa, const complex<T>& bb,
                 const complex<T>& cin) {
    if (ee == CZRO) return CZRO;

    complex<T> rslt, fy1y2, z1, z2, cc = cin;

    T rea = abs(aa), reb = abs(bb), simc = abs(cc), rez1, rez2, hh, imz1, imz2;

    if (simc < T(1e5) * IEPS.imag() * min(rea, reb)) cc = T(0);

    simc = imag(cc);
    if (simc == RZRO) {
        simc = imag(bb);
        if (simc == RZRO) simc = T(-1);
    }

    simc = sgn(simc);

    complex<T> y1 = (dd + y1i) / ee, y2 = (dd + y2i) / ee;

    fy1y2 = r0fun(y1, y2);

    if (aa != CZRO) {
        solabc2(z1, z2, real(aa), bb, cc);

        rea = sgn(real(aa));
        rez1 = real(z1);
        rez2 = real(z2);
        imz1 = imag(z1);
        imz2 = imag(z2);

        hh = abs(EPSN2 * rez1);
        if (imz1 == RZRO) imz1 = simc * rea * sgn(rez2) * hh;
        hh = abs(EPSN2 * rez2);
        if (imz2 == RZRO) imz2 = simc * rea * sgn(rez1) * hh;

        z1 = complex<T>(rez1, imz1);
        z2 = complex<T>(rez2, imz2);

        rslt = fy1y2 * (log(riemann<T>(aa, simc)) + eta3(-z1, -imz1, -z2, -imz2, CZRO, simc * rea)) + r1fun(z1, y1, y2, fy1y2) + r1fun(z2, y1, y2, fy1y2);
    } else if (bb != CZRO) {
        z1 = -cc / bb;
        reb = real(bb);
        rez1 = real(z1);
        imz1 = imag(z1);
        if (abs(imz1) == RZRO) {
            imz1 = -simc * reb * abs(EPSN2 * rez1 / reb);
            z1 = complex<T>(rez1, imz1);
        }

        rslt = fy1y2 * (log(riemann<T>(bb, simc)) + eta3(bb, simc, -z1, -imz1, cc, simc)) + r1fun(z1, y1, y2, fy1y2);
    } else if (cc != CZRO)
        rslt = log(riemann<T>(cc, simc)) * fy1y2;
    else
        rslt = CZRO;

    return rslt / ee;
}

template <class T>
complex<T> t13fun(const complex<T>& aa, const complex<T>& cc, const complex<T>& gg, const complex<T>& hh, const complex<T>& dd, const complex<T>& ee,
                  const complex<T>& ff, const complex<T>& jj, const complex<T>& dpe, const complex<T>& dpj, const complex<T>& dpf) {
    complex<T> rslt, y1, y2;

    complex<T> kk = hh * aa - cc * gg, ll = aa * dd + hh * ee - dd * gg - cc * jj, nn = dd * (ee - jj) + (hh - cc) * (ff - IEPS * abs(real(ff)));
    solabc_c(y1, y2, kk, ll, nn);

    rslt = -s3fun(y1, y2, CZRO, CONE, aa, ee + cc, dpf) + s3fun(y1, y2, CZRO, CONE, gg, jj + hh, dpf) - s3fun(y1, y2, CZRO, CONE, gg + hh, dpj, ff) +
           s3fun(y1, y2, CZRO, CONE, aa + cc, dpe, ff);

    rslt /= kk;

    return rslt;
}

template <class T>
complex<T> t1fun(const complex<T>& aa, const complex<T>& cc, const complex<T>& gg, const complex<T>& hh, const complex<T>& dd, const complex<T>& ee,
                 const complex<T>& ff, const complex<T>& jj, const complex<T>& dpe) {
    complex<T> rslt, y1, y2;

    complex<T> kk = hh * aa - cc * gg, ll = hh * dd - cc * jj - ee * gg, nn = hh * (ff - IEPS * abs(real(ff))) - ee * jj;
    solabc_c(y1, y2, kk, ll, nn);

    rslt = -s3fun(y1, y2, CZRO, CONE, aa + cc, dpe, ff) + s3fun(y1, y2, CZRO, CONE, CZRO, gg + hh, jj) - s3fun(y1, y2, CZRO, CONE, CZRO, gg, jj) +
           s3fun(y1, y2, CZRO, CONE, aa, dd, ff);

    rslt /= kk;

    return rslt;
}

template <class T>
complex<T> tfun(const complex<T>& aa, const complex<T>& bb, const complex<T>& cc, const complex<T>& gin, const complex<T>& hin, const complex<T>& dd,
                const complex<T>& ee, const complex<T>& ff, const complex<T>& jin, const complex<T>& dpe, const complex<T>& dpf) {

    complex<T> rslt, zz[2], beta, ll, nn, tmpa[2], tmpb[2], tmpc[2], kiz[2], yy[2][2], kk, y1, y2;

    T ab1, ab2, ac1, ac2, abab, acac, abac, det, ap1, ap2, apab, apac, x1[2][2], x2[2][2], xmin;

    complex<T> sj(sgnIm(jin, -1), 0), gg = -sj * gin, hh = -sj * hin, jj = -sj * jin;

    if (bb == CZRO)
        return -sj * t1fun(aa, cc, gg, hh, dd, ee, ff, jj, dpe);
    else if (aa == CZRO)
        return -sj * t1fun(bb + cc, -cc, -gg - hh, gg, -dpe - T(2) * (bb + cc), dd + cc, dpe + bb + cc + ff, gg + hh + jj, -ee - T(2) * bb - cc);

    solabc_c(zz[0], zz[1], bb, cc, aa);

    if (abs(zz[0]) > abs(zz[1])) { swap(zz[0], zz[1]); }

    for (int iz = 0; iz < 2; iz++) {
        beta = zz[iz];
        tmpa[iz] = gg + beta * hh;
        tmpb[iz] = cc + T(2) * beta * bb;
        tmpc[iz] = dd + beta * ee;
        kiz[iz] = bb * tmpa[iz] - hh * tmpb[iz];
        ll = ee * tmpa[iz] - hh * tmpc[iz] - jj * tmpb[iz];
        nn = (ff - IEPS * IEPS * abs(real(ff))) * tmpa[iz] - jj * tmpc[iz];

        solabc_c(yy[iz][0], yy[iz][1], kiz[iz], ll, nn);

        if (beta.imag() != T(0)) {
            ab1 = -real(beta);
            ab2 = -imag(beta);
            ac1 = ab1 + T(1);
            ac2 = ab2;
            abab = ab1 * ab1 + ab2 * ab2;
            acac = ac1 * ac1 + ac2 * ac2;
            abac = ab1 * ac1 + ab2 * ac2;
            det = abab * acac - abac * abac;

            for (int iy = 0; iy < 2; iy++) {
                ap1 = real(yy[iz][iy]);
                ap2 = imag(yy[iz][iy]);
                apab = ap1 * ab1 + ap2 * ab2;
                apac = ap1 * ac1 + ap2 * ac2;
                x1[iz][iy] = (acac * apab - abac * apac) / det;
                x2[iz][iy] = (-abac * apab + abab * apac) / det;
            }
        } else {
            for (int iy = 0; iy < 2; iy++) {
                x1[iz][iy] = T(-1);
                x2[iz][iy] = T(-1);
            }
        }
    }

    xmin = T(1);
    int izmin = 1, pp[2][2], p1, p2;

    for (int iz = 0; iz < 2; iz++) {
        for (int iy = 0; iy < 2; iy++) {
            if ((x1[iz][iy] > T(0)) and (x2[iz][iy] > T(0)) and (x1[iz][iy] + x2[iz][iy] <= T(1))) {
                pp[iz][iy] = 1;
                if (x1[iz][iy] < xmin) {
                    xmin = x1[iz][iy];
                    izmin = iz;
                }
                if (x2[iz][iy] < xmin) {
                    xmin = x2[iz][iy];
                    izmin = iz;
                }
            } else
                pp[iz][iy] = 0;
        }
    }

    int iz = izmin + 1;
    if (iz == 2) iz = 0;

    beta = zz[iz];
    kk = kiz[iz];
    y1 = yy[iz][0];
    y2 = yy[iz][1];
    p1 = pp[iz][0];
    p2 = pp[iz][1];

    rslt = s3fun(y1, y2, beta, CONE, CZRO, hh, gg + jj) - s3fun(y1, y2, CZRO, CONE - beta, CZRO, gg + hh, jj) + s3fun(y1, y2, CZRO, -beta, CZRO, gg, jj) -
           s3fun(y1, y2, beta, CONE, bb, cc + ee, aa + dpf) + s3fun(y1, y2, CZRO, CONE - beta, aa + bb + cc, dpe, ff) - s3fun(y1, y2, CZRO, -beta, aa, dd, ff);

    complex<T> sdnt = plnr(y1, y2, p1, p2, tmpa[iz], tmpb[iz], tmpc[iz]);

    if (imag(beta) <= T(0))
        rslt += sdnt;
    else
        rslt -= sdnt;

    return -sj * rslt / kk;
}

template <class T> complex<T> I4f4mC(int order, const std::vector<T>& pp_in, const std::vector<complex<T>>& mm_in) {
#if CALLING
    cout << "*******I4f4mC called******" << endl;
#endif

    if (order == -2 or order == -1) return CZRO;

    static int casetable[16] = {0, 1, 1, 2, 1, 5, 2, 3, 1, 2, 5, 3, 2, 3, 3, 4};
    static int base[4] = {8, 4, 2, 1};
    static int permtable[16][6] = {{1, 2, 3, 4, 5, 6}, {1, 2, 3, 4, 5, 6}, {4, 1, 2, 3, 6, 5}, {1, 2, 3, 4, 5, 6}, {3, 4, 1, 2, 5, 6}, {1, 2, 3, 4, 5, 6},
                                   {4, 1, 2, 3, 6, 5}, {1, 2, 3, 4, 5, 6}, {2, 3, 4, 1, 6, 5}, {2, 3, 4, 1, 6, 5}, {4, 1, 2, 3, 6, 5}, {2, 3, 4, 1, 6, 5},
                                   {3, 4, 1, 2, 5, 6}, {3, 4, 1, 2, 5, 6}, {4, 1, 2, 3, 6, 5}, {1, 2, 3, 4, 5, 6}};

    static int lp_table[3][6] = {{1, 2, 3, 4, 5, 6}, {5, 2, 6, 4, 1, 3}, {1, 6, 3, 5, 4, 2}};
    static int lm_table[3][4] = {{1, 2, 3, 4}, {1, 3, 2, 4}, {1, 2, 4, 3}};

    auto ll = [](int i, int j) { return permtable[j][i - 1] - 1; };
    auto lp = [](int i, int j) { return lp_table[j - 1][i] - 1; };
    auto lm = [](int i, int j) { return lm_table[j - 1][i] - 1; };

    CXT pp[6];
    CXT mm[4];

    int icase = 0;
    constexpr int jj = 1;
    for (int i = 1; i <= 4; i++) {
        if (abs(mm_in[lm(i - 1, jj)].real()) > T(0)) { icase += base(i); };
    }
    for (int i = 0; i <= 5; i++) { pp[i] = CXT(pp_in[lp(ll(i + 1, icase), jj)], 0); }

    // static T smalle = IEPS.imag()*T(1e4);
    // if(abs(pp[4].real())<DeltaZero<T>()) pp[4]=smalle+IEPS;
    // if(abs(pp[5].real())<DeltaZero<T>()) pp[5]=smalle+IEPS;

    for (int i = 0; i <= 3; i++) {
        mm[i] = mm_in[lm(ll(i + 1, icase), jj)];
        if (abs(mm[i].imag()) < DeltaZero<T>()) mm[i] -= abs(mm[i].real()) * IEPS;
    }

    icase = 0;
    for (int i = 1; i <= 4; i++)
        if (abs(pp[i - 1].real()) > T(0)) icase += base(i);

    complex<T> a, b, g, c, h, j, d, e, k, f, abc, bgj, jph, cph, dpe, epk, dek, dpf, def, dpk;

    if (icase < 15) {
        int jcase = casetable(icase);
        if (jcase == 0 or jcase == 1 or jcase == 5) {
            a = pp[ll(5, icase)] - pp[ll(1, icase)];
            c = pp[ll(4, icase)] - pp[ll(5, icase)] - pp[ll(3, icase)];
            g = pp[ll(2, icase)];
            h = pp[ll(6, icase)] - pp[ll(2, icase)] - pp[ll(3, icase)];
            d = (mm[ll(3, icase)] - mm[ll(4, icase)]) - pp[ll(3, icase)];
            e = (mm[ll(1, icase)] - mm[ll(3, icase)]) + pp[ll(3, icase)] - pp[ll(4, icase)];
            f = mm[ll(4, icase)];
            k = (mm[ll(2, icase)] - mm[ll(3, icase)]) - pp[ll(6, icase)] + pp[ll(3, icase)];
            dpe = (mm[ll(1, icase)] - mm[ll(4, icase)]) - pp[ll(4, icase)];
            dpk = (mm[ll(2, icase)] - mm[ll(4, icase)]) - pp[ll(6, icase)];
            dpf = mm[ll(3, icase)] - pp[ll(3, icase)];
            return t13fun(a, c, g, h, d, e, f, k, dpe, dpk, dpf);
        } else {
            a = pp[ll(3, icase)];
            b = pp[ll(2, icase)];
            c = pp[ll(6, icase)] - pp[ll(2, icase)] - pp[ll(3, icase)];
            h = pp[ll(4, icase)] - pp[ll(5, icase)] - pp[ll(6, icase)] + pp[ll(2, icase)];
            j = pp[ll(5, icase)] - pp[ll(1, icase)] - pp[ll(2, icase)];
            d = (mm[ll(3, icase)] - mm[ll(4, icase)]) - pp[ll(3, icase)];
            e = (mm[ll(2, icase)] - mm[ll(3, icase)]) - pp[ll(6, icase)] + pp[ll(3, icase)];
            k = (mm[ll(1, icase)] - mm[ll(2, icase)]) + pp[ll(6, icase)] - pp[ll(4, icase)];
            f = mm[ll(4, icase)];
            cph = pp[ll(4, icase)] - pp[ll(5, icase)] - pp[ll(3, icase)];
            dpe = (mm[ll(2, icase)] - mm[ll(4, icase)]) - pp[ll(6, icase)];
            epk = (mm[ll(1, icase)] - mm[ll(3, icase)]) + pp[ll(3, icase)] - pp[ll(4, icase)];
            dek = (mm[ll(1, icase)] - mm[ll(4, icase)]) - pp[ll(4, icase)];
            dpf = mm[ll(3, icase)] - pp[ll(3, icase)];
            return tfun(a, b, c, h, j, d, e, f, k, dpe, dpf) - tfun(a, b + j, cph, h, j, d, epk, f, k, dek, dpf);
        }
    } else {
        auto sq = [](std::complex<T>&& p){return p*p;};
        if (real(sq(pp[5 - 1] - pp[1 - 1] - pp[2 - 1]) - T(4) * pp[1 - 1] * pp[2 - 1]) > RZRO) {
            icase = 0;
        } else if (real(sq(pp[6 - 1] - pp[2 - 1] - pp[3 - 1]) - T(4) * pp[2 - 1] * pp[3 - 1]) > RZRO) {
            icase = 8;
        } else if (real(sq(pp[4 - 1] - pp[5 - 1] - pp[3 - 1]) - T(4) * pp[5 - 1] * pp[3 - 1]) > RZRO) {
            icase = 4;
        } else if (real(sq(pp[4 - 1] - pp[1 - 1] - pp[6 - 1]) - T(4) * pp[1 - 1] * pp[6 - 1]) > RZRO) {
            icase = 2;
        }

        a = pp[ll(3, icase)];
        b = pp[ll(2, icase)];
        g = pp[ll(1, icase)];
        c = pp[ll(6, icase)] - pp[ll(2, icase)] - pp[ll(3, icase)];
        h = pp[ll(4, icase)] - pp[ll(5, icase)] - pp[ll(6, icase)] + pp[ll(2, icase)];
        j = pp[ll(5, icase)] - pp[ll(1, icase)] - pp[ll(2, icase)];
        d = (mm[ll(3, icase)] - mm[ll(4, icase)]) - pp[ll(3, icase)];
        e = (mm[ll(2, icase)] - mm[ll(3, icase)]) - pp[ll(6, icase)] + pp[ll(3, icase)];
        k = (mm[ll(1, icase)] - mm[ll(2, icase)]) + pp[ll(6, icase)] - pp[ll(4, icase)];
        f = mm[ll(4, icase)];
        abc = pp[ll(6, icase)];
        bgj = pp[ll(5, icase)];
        jph = pp[ll(4, icase)] - pp[ll(1, icase)] - pp[ll(6, icase)];
        cph = pp[ll(4, icase)] - pp[ll(5, icase)] - pp[ll(3, icase)];
        dpe = (mm[ll(2, icase)] - mm[ll(4, icase)]) - pp[ll(6, icase)];
        epk = (mm[ll(1, icase)] - mm[ll(3, icase)]) + pp[ll(3, icase)] - pp[ll(4, icase)];
        dek = (mm[ll(1, icase)] - mm[ll(4, icase)]) - pp[ll(4, icase)];
        dpf = mm[ll(3, icase)] - pp[ll(3, icase)];
        def = mm[ll(2, icase)] - pp[ll(6, icase)];
    }

    complex<T> x1, x2;

    solabc_c(x1, x2, g, j, b);

    if (abs(x1.real()) < abs(x2.real())) {
        complex<T> temp = x1;
        x1 = x2;
        x2 = temp;
    }

    complex<T> o1 = T(1) - x1, j1 = j + T(2) * g * x1, e1 = e + k * x1;

    complex<T> result = -tfun(abc, g, jph, c + T(2) * b + (h + j) * x1, j1, dpe, k, f, e1, dek, def) +
                        o1 * tfun(a, bgj, cph, c + h * x1, o1 * j1, d, epk, f, e1, dek, dpf) + x1 * tfun(a, b, c, c + h * x1, -j1 * x1, d, e, f, e1, dpe, dpf);

    return result;
}
}
}

#undef IEPS
