/**********************************************************************
 *  This file is a part of libIntegrals.
 *
 *  The implementation is mostly a C++ port of OneLOop Fortran library by A.van Hameren
 *  http://helac-phegas.web.cern.ch/helac-phegas/OneLOop.html
 *
 *  adapted to multiprecision evaluation with qd.
 *
 ***********************************************************************/

#define CALLING 0

#if CALLING == 1
#include <bitset>
#endif

using namespace std;
using namespace Caravel;
using namespace Caravel::IntegralsBH;

/********************************************************************************************
 *      Auxiliary routines
 ********************************************************************************************/

#define C1 std::complex<T>(1, 0)
#define C0 std::complex<T>(0, 0)

// finds solutions x1,x2 of  aa*x^2+bb*x+cc=0 when on the branch cut, square root is computed with negative imaginary part
template <class T> void solabc(complex<T>& x1, complex<T>& x2, const complex<T>& aa, const complex<T>& bb, const complex<T>& cc) {
    if (iszeroI(aa)) {
        if (iszeroI(bb)) {
            x1 = T(0);
            x2 = T(0);
        } else {
            x1 = -cc / bb;
            x2 = x1;
        }
    } else if (iszeroI(cc)) {
        x1 = -bb / aa;
        x2 = T(0);
    } else {
        complex<T> dd = (bb * bb - T(4) * aa * cc);
        if (abs(dd.imag()) < T(1e-32)) dd -= IEPS<T>();
        // cout <<endl <<  "discr: " << dd << endl;
        dd = sqrt(dd);
        // cout <<endl <<  "discr: " << dd << endl;
        complex<T> qq = -bb + dd, hh = -bb - dd;
        //     cout <<  "qq, hh: " << qq << hh<< endl;
        if (abs(qq) > abs(hh)) {
            x1 = qq / (T(2) * aa);
            x2 = (T(2) * cc) / qq;
        } else {
            x2 = hh / (T(2) * aa);
            x1 = (T(2) * cc) / hh;
        }
    }
}

// eq.(4.15) from EZ
template <class T> complex<T> inline xsij(const T& s, const T& m1, const T& m2) {
    T dif = s - sq(m1 - m2);
    if (iszeroI(dif)) return C1;

    complex<T> arg = T(1) - T(4) * m1 * m2 / (s - sq(m1 - m2));
    return ((sqrt(arg) - C1) / (sqrt(arg) + C1));
}

template <class T> complex<T> inline ixsij(complex<T>& dd, const complex<T>& s, const T& m1, const T& m2) {
    complex<T> dif = s - sq(m1 - m2), r;
    if (iszeroI(dif)) return C1;

    complex<T> arg = T(1) - T(4) * m1 * m2 / (s - sq(m1 - m2));
    r = (sqrt(arg) + C1) / (sqrt(arg) - C1);
    dd = r - C1 / r;
    return r;
}

#define mlog12(x1, x2) logM1(x1 / x2) / x2.tocomplex()
#define mlog12sq(x1, x2) log(x1* x2) * logM1(x1 / x2) / x2.tocomplex()

/********************************************************************************************/

#include "integrals_boxes.hpp"
#include "integrals_bubbles.hpp"
#include "integrals_tadpoles.hpp"
#include "integrals_triangles.hpp"

/********************************************************************************************
 *
 *      WRAPPERS
 *
 ********************************************************************************************/

namespace Caravel {
namespace IntegralsBH {

#define HighestPole -2
#define FiniteTerm 0

#define setcoeffs(xx)                                                                                                                                          \
    for (int j = HighestPole; j <= FiniteTerm; j += 1) {                                                                                                       \
        complex<T> arg = xx;                                                                                                                                   \
        coeffs[j - HighestPole] = iszeroI(imag(arg)) ? real(arg) : arg;                                                                                        \
    }

template <class T> inline T get_lmass(const lGraph::IntegralGraph& IS, const std::vector<complex<T>>& masses, size_t i) {
    if (IS.get_propagator_mass_index(i) == 0) return T(0);
    return real(masses[IS.get_propagator_mass_index(i) - 1] * masses[IS.get_propagator_mass_index(i) - 1]);
}

template <class T> inline CXT get_lmass_c(const lGraph::IntegralGraph& IS, const std::vector<complex<T>>& masses, size_t i) {
    if (IS.get_propagator_mass_index(i) == 0) return CXT(0);
    return masses[IS.get_propagator_mass_index(i) - 1] * masses[IS.get_propagator_mass_index(i) - 1];
}
// We will return a complex Series from -2 to 0 so far
template <class T> inline Series<CXT> make_series(const function<CXT(int)>& setc) { return Series<CXT>(-2, 0, setc(-2), setc(-1), setc(0)); }

template <class T> T stof_T(const std::string&);

template <> std::complex<double> stof_T<std::complex<double>>(const std::string& s) { return std::complex<double>(std::stod(s)); }

template <class T> T stof_T(const std::string& s) { return T(s.c_str()); }

// Transforms c_\Gamma normalization to (e^{\gamma_E \epsilon})/(i\pi^{D/2})
// this is just:
// ( GAMMA(1+epsilon)*GAMMA(1-epsilon)^2/GAMMA(1-2*epsilon)/(4*Pi)^(2-epsilon) ) * exp(gamma*ep)*(4*Pi)^(-ep)
template <typename T>
Series<T> convert_intsBH_to_caravel_normalization{0,
                                     7,
                                     stof_T<T>("1.000000000000000000000000000000000000000000000000000000000000000000"),
                                     stof_T<T>("0"),
                                     stof_T<T>("-0.822467033424113218236207583323012594609474950603399218867779114"),
                                     stof_T<T>("-2.804799440705719999266055710193383311784968015461164057515300300"),
                                     stof_T<T>("-3.179324499026468437578260858589680714400830921261260291318742628"),
                                     stof_T<T>("-4.122097006542048072024533000967632007858058323141702857428201931"),
                                     stof_T<T>("-3.609195094202520090927414263199252085730233463450561970156106632"),
                                     stof_T<T>("-4.089368166135090033710010497377090449720964961885879605183059569")};

template <class T>
Series<complex<T>> integral(const lGraph::IntegralGraph& IS, const momD_conf<complex<T>, 4>& mc, const std::vector<complex<T>>& masses, const complex<T>& musqC) {
    T musq(real(musqC));

    using placeholders::_1;
    function<CXT(int)> setc;

#define assign_si(i)                                                                                                                                           \
    if (single_leg_corner(i)) {                                                                                                                                \
        if (get_corner_mass(i) == 0)                                                                                                                           \
            s##i = T(0);                                                                                                                                       \
        else                                                                                                                                                   \
            s##i = real(masses[get_corner_mass(i) - 1] * masses[get_corner_mass(i) - 1]);                                                                      \
    } else {                                                                                                                                                   \
        s##i = SqSum_1(mc, IS.vertex_momentum_indices(i));                                                                                                     \
    }

    auto iszero_prop = [IS](int i) { return IS.is_massless_propagator(i); };
    auto iszero_leg = [IS](int i) { return IS.is_massless_vertex(i); };
    auto onshell_leg = [IS](int legn, int propn) { return IS.check_on_shell_leg(legn, propn); };
    auto single_leg_corner = [IS](int cor) { return IS.vertex_size(cor) == 1; };
    auto get_corner_mass = [IS](int cor) { return IS.get_on_shell_vertex_mass_index(cor); };

    switch (IS.number_of_vertices()) {
        case 1:
            if (!iszero_prop(1))
                setc = bind(I1<T>, _1, musq, get_lmass<T>(IS, masses, 1));
            else
                setc = bind([]() { return CXT(0, 0); });
            break;

        case 2: { // Bubbles
            int patern = ((iszero_leg(1) or iszero_leg(2)) && IS.get_propagator_mass_index(1) == IS.get_propagator_mass_index(2)) << 3 | iszero_prop(1) << 4 |
                         iszero_prop(2) << 5;
#if CALLING
            cout << "pattern: " << patern << endl;
#endif
            T s1 = SqSum_1(mc, IS.vertex_momentum_indices(1));
            switch (patern) {
                case 0x08: setc = bind(I2_0<T>, _1, musq, get_lmass<T>(IS, masses, 1)); break;
                case 0x10:
                    if (onshell_leg(1, 2) or onshell_leg(2, 2))
                        setc = bind(I2_1m_d<T>, _1, musq, get_lmass<T>(IS, masses, 2));
                    else
                        setc = bind(I2_1m<T>, _1, musq, s1, get_lmass<T>(IS, masses, 2));
                    break;
                case 0x20:
                    if (onshell_leg(1, 1) or onshell_leg(2, 1))
                        setc = bind(I2_1m_d<T>, _1, musq, get_lmass<T>(IS, masses, 1));
                    else
                        setc = bind(I2_1m<T>, _1, musq, s1, get_lmass<T>(IS, masses, 1));
                    break;
                case 0x30: setc = bind(I2_0m<T>, _1, musq, s1); break;
                default: setc = bind(I2_full<T>, _1, musq, s1, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2)); break;
            }
            break;
        }

        case 3: { // Triangles
            int patern = iszero_prop(1) << 4 | iszero_prop(2) << 5 | iszero_prop(3) << 6 | iszero_leg(1) << 0 | iszero_leg(2) << 1 | iszero_leg(3) << 2;
            T s1, s2, s3;
            assign_si(1);
            assign_si(2);
            assign_si(3);
#if CALLING
            cout << "pattern: " << std::bitset<8>(patern) << endl;
#endif
            switch (patern) {
                /* I3n1 */
                case 115: setc = bind(I3n1<T>, _1, musq, s3); break;
                case 117: setc = bind(I3n1<T>, _1, musq, s2); break;
                case 118:
                    setc = bind(I3n1<T>, _1, musq, s1);
                    break;
                /* I3n2 */
                case 116: setc = bind(I3n2<T>, _1, musq, s1, s2); break;
                case 113: setc = bind(I3n2<T>, _1, musq, s2, s3); break;
                case 114:
                    setc = bind(I3n2<T>, _1, musq, s1, s3);
                    break;
                /* I3n3, I3n4, I3n5 */
                case 49:
                case 51:
                case 53:
                case 55: {
                    int flags = onshell_leg(2, 3) << 0 | onshell_leg(3, 3) << 1;
                    switch (flags) {
                        case 3: setc = bind(I3n5<T>, _1, musq, get_lmass<T>(IS, masses, 3)); break;
                        case 2: setc = bind(I3n4<T>, _1, musq, s2, get_lmass<T>(IS, masses, 3)); break;
                        case 1: setc = bind(I3n4<T>, _1, musq, s3, get_lmass<T>(IS, masses, 3)); break;
                        case 0: setc = bind(I3n3<T>, _1, musq, s2, s3, get_lmass<T>(IS, masses, 3)); break;
                    }
                    break;
                }
                case 98:
                case 102:
                case 99:
                case 103: {
                    int flags = onshell_leg(1, 1) << 0 | onshell_leg(3, 1) << 1;
                    switch (flags) {
                        case 3: setc = bind(I3n5<T>, _1, musq, get_lmass<T>(IS, masses, 1)); break;
                        case 2: setc = bind(I3n4<T>, _1, musq, s1, get_lmass<T>(IS, masses, 1)); break;
                        case 1: setc = bind(I3n4<T>, _1, musq, s3, get_lmass<T>(IS, masses, 1)); break;
                        case 0: setc = bind(I3n3<T>, _1, musq, s1, s3, get_lmass<T>(IS, masses, 1)); break;
                    }
                    break;
                }
                case 84:
                case 86:
                case 85:
                case 87: {
                    int flags = onshell_leg(2, 2) << 0 | onshell_leg(1, 2) << 1;
                    switch (flags) {
                        case 3: setc = bind(I3n5<T>, _1, musq, get_lmass<T>(IS, masses, 2)); break;
                        case 2: setc = bind(I3n4<T>, _1, musq, s2, get_lmass<T>(IS, masses, 2)); break;
                        case 1: setc = bind(I3n4<T>, _1, musq, s1, get_lmass<T>(IS, masses, 2)); break;
                        case 0: setc = bind(I3n3<T>, _1, musq, s2, s1, get_lmass<T>(IS, masses, 2)); break;
                    }
                    break;
                }
                /* I3n6 */
                case 16:
                case 18:
                    if (onshell_leg(1, 2) and onshell_leg(3, 3)) {
                        setc = bind(I3n6<T>, _1, musq, s2, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3));
                    } else {
                        setc = bind(I3f2m<T>, _1, s1, s2, s3, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3));
                    }
                    break;
                case 32:
                case 36:
                    if (onshell_leg(2, 3) and onshell_leg(1, 1)) {
                        setc = bind(I3n6<T>, _1, musq, s3, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 1));
                    } else {
                        setc = bind(I3f2m<T>, _1, s2, s3, s1, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 1));
                    }
                    break;
                case 64:
                case 65:
                    if (onshell_leg(3, 1) and onshell_leg(2, 2)) {
                        setc = bind(I3n6<T>, _1, musq, s1, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2));
                    } else {
                        setc = bind(I3f2m<T>, _1, s3, s1, s2, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2));
                    }
                    break;
                default:
                    patern = (patern >> 4 & 7);
                    switch (patern) {
                        case 0:
                            setc = bind(I3f3m<T>, _1, s3, s1, s2, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3));
                            break;
                        case 1: setc = bind(I3f2m<T>, _1, s1, s2, s3, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3)); break;
                        case 2: setc = bind(I3f2m<T>, _1, s2, s3, s1, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 1)); break;
                        case 3: setc = bind(I3f1m<T>, _1, s1, s2, s3, get_lmass<T>(IS, masses, 3)); break;
                        case 4: setc = bind(I3f2m<T>, _1, s3, s1, s2, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2)); break;
                        case 5: setc = bind(I3f1m<T>, _1, s3, s1, s2, get_lmass<T>(IS, masses, 2)); break;
                        case 6: setc = bind(I3f1m<T>, _1, s2, s3, s1, get_lmass<T>(IS, masses, 1)); break;
                        case 7: setc = bind(I3f0m<T>, _1, s1, s2, s3); break;
                    }
                    break;
            }
            break;
        }

        case 4: { // Boxes
            int paternmasses = iszero_prop(1) << 3 | iszero_prop(2) << 2 | iszero_prop(3) << 1 | iszero_prop(4) << 0;
            int paternmom = iszero_leg(1) << 3 | iszero_leg(2) << 2 | iszero_leg(3) << 1 | iszero_leg(4) << 0;
#if CALLING
            cout << "pattern: " << std::bitset<8>(paternmasses) << endl;
	    cout << "patternmasses: " << paternmasses << endl;
	    cout << "patternmom: " << paternmom << endl;
#endif
            T s1, s2, s3, s4;
            assign_si(1);
            assign_si(2);
            assign_si(3);
            assign_si(4);
            T s12 = SqSum_1(mc, IS.vertex_momentum_indices(1), IS.vertex_momentum_indices(2));
            T s23 = SqSum_1(mc, IS.vertex_momentum_indices(2), IS.vertex_momentum_indices(3));

            bool useboxc = (s1 >= T(0) && s2 >= T(0) && s3 >= T(0) && s4 >= T(0)) or (s12 >= T(0) && s23 >= T(0));

            std::vector<T> pp = {s1, s2, s3, s4, s12, s23};
            std::vector<CXT> mm = {get_lmass_c<T>(IS, masses, 1), get_lmass_c<T>(IS, masses, 2), get_lmass_c<T>(IS, masses, 3), get_lmass_c<T>(IS, masses, 4)};

            switch (paternmasses) {
                case 15:
                    switch (paternmom) {
                        case 15: setc = bind(I4n1<T>, _1, musq, s12, s23); break;
                        case 14: setc = bind(I4n2<T>, _1, musq, s4, s12, s23); break;
                        case 13: setc = bind(I4n2<T>, _1, musq, s3, s23, s12); break;
                        case 11: setc = bind(I4n2<T>, _1, musq, s2, s12, s23); break;
                        case 7: setc = bind(I4n2<T>, _1, musq, s1, s23, s12); break;
                        case 10: setc = bind(I4n3<T>, _1, musq, s2, s4, s12, s23); break;
                        case 5: setc = bind(I4n3<T>, _1, musq, s3, s1, s23, s12); break;
                        case 12: setc = bind(I4n4<T>, _1, musq, s3, s4, s12, s23); break;
                        case 6: setc = bind(I4n4<T>, _1, musq, s4, s1, s23, s12); break;
                        case 3: setc = bind(I4n4<T>, _1, musq, s1, s2, s12, s23); break;
                        case 9: setc = bind(I4n4<T>, _1, musq, s2, s3, s23, s12); break;
                        case 8: setc = bind(I4n5<T>, _1, musq, s2, s3, s4, s12, s23); break;
                        case 4: setc = bind(I4n5<T>, _1, musq, s3, s4, s1, s23, s12); break;
                        case 2: setc = bind(I4n5<T>, _1, musq, s4, s1, s2, s12, s23); break;
                        case 1: setc = bind(I4n5<T>, _1, musq, s1, s2, s3, s23, s12); break;
                        default: setc = bind(I4f0m<T>, _1, s1, s2, s3, s4, s12, s23);
                    }
                    break;
                case 14:
                    paternmom += onshell_leg(3, 4) << 5 | onshell_leg(4, 4) << 4;
                    switch (paternmom) {
                        case 60: setc = bind(I4n6<T>, _1, musq, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                        case 44:
                        case 45: setc = bind(I4n7<T>, _1, musq, s4, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                        case 28:
                        case 30: setc = bind(I4n7<T>, _1, musq, s3, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                        case 12:
                        case 13:
                        case 14:
                        case 15: setc = bind(I4n8<T>, _1, musq, s3, s4, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                        case 24:
                        case 26:
                        case 56:
                        case 58: setc = bind(I4n9<T>, _1, musq, s2, s3, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                        case 36:
                        case 37:
                        case 52:
                        case 53: setc = bind(I4n9<T>, _1, musq, s1, s4, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 40:
                        case 41: setc = bind(I4n10<T>, _1, musq, s2, s3, s4, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 20:
                        case 22: setc = bind(I4n10<T>, _1, musq, s1, s4, s3, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                        default: setc = bind(I4f1m<T>, _1, s1, s2, s3, s4, s12, s23, get_lmass<T>(IS, masses, 4)); break;
                    }
                    break;
                case 7:
                    paternmom += onshell_leg(4, 1) << 5 | onshell_leg(1, 1) << 4;
                    switch (paternmom) {
                        case 54: setc = bind(I4n6<T>, _1, musq, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                        case 38:
                        case 46: setc = bind(I4n7<T>, _1, musq, s1, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                        case 22:
                        case 23: setc = bind(I4n7<T>, _1, musq, s4, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                        case 6:
                        case 14:
                        case 7:
                        case 15: setc = bind(I4n8<T>, _1, musq, s4, s1, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                        case 20:
                        case 21:
                        case 52:
                        case 53: setc = bind(I4n9<T>, _1, musq, s3, s4, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                        case 34:
                        case 42:
                        case 50:
                        case 58: setc = bind(I4n9<T>, _1, musq, s2, s1, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                        case 4:
                        case 12:
                        case 5:
                        case 13:
                        case 36:
                        case 44: setc = bind(I4n10<T>, _1, musq, s3, s4, s1, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                        case 2:
                        case 10:
                        case 3:
                        case 11:
                        case 18:
                        case 19: setc = bind(I4n10<T>, _1, musq, s2, s1, s4, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                        default: setc = bind(I4f1m<T>, _1, s2, s3, s4, s1, s23, s12, get_lmass<T>(IS, masses, 1)); break;
                    }
                    break;
                case 11:
                    paternmom += onshell_leg(1, 2) << 5 | onshell_leg(2, 2) << 4;
                    switch (paternmom) {
                        case 51: setc = bind(I4n6<T>, _1, musq, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                        case 35:
                        case 39: setc = bind(I4n7<T>, _1, musq, s2, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                        case 19:
                        case 27: setc = bind(I4n7<T>, _1, musq, s1, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                        case 3:
                        case 7:
                        case 11:
                        case 15: setc = bind(I4n8<T>, _1, musq, s1, s2, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                        case 18:
                        case 26:
                        case 50:
                        case 58: setc = bind(I4n9<T>, _1, musq, s4, s1, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                        case 33:
                        case 37:
                        case 49:
                        case 53: setc = bind(I4n9<T>, _1, musq, s3, s2, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                        case 2:
                        case 6:
                        case 10:
                        case 14:
                        case 34:
                        case 38: setc = bind(I4n10<T>, _1, musq, s4, s1, s2, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                        case 1:
                        case 5:
                        case 9:
                        case 13:
                        case 17:
                        case 25: setc = bind(I4n10<T>, _1, musq, s3, s2, s1, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                        default: setc = bind(I4f1m<T>, _1, s3, s4, s1, s2, s12, s23, get_lmass<T>(IS, masses, 2)); break;
                    }
                    break;
                case 13:
                    paternmom += onshell_leg(2, 3) << 5 | onshell_leg(3, 3) << 4;
                    switch (paternmom) {
                        case 57: setc = bind(I4n6<T>, _1, musq, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                        case 41:
                        case 43: setc = bind(I4n7<T>, _1, musq, s3, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                        case 25:
                        case 29: setc = bind(I4n7<T>, _1, musq, s2, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                        case 9:
                        case 11:
                        case 13:
                        case 15: setc = bind(I4n8<T>, _1, musq, s2, s3, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                        case 17:
                        case 21:
                        case 49:
                        case 53: setc = bind(I4n9<T>, _1, musq, s1, s2, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                        case 40:
                        case 42:
                        case 56:
                        case 58: setc = bind(I4n9<T>, _1, musq, s4, s3, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                        case 1:
                        case 3:
                        case 5:
                        case 7:
                        case 33:
                        case 35: setc = bind(I4n10<T>, _1, musq, s1, s2, s3, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                        case 8:
                        case 10:
                        case 12:
                        case 14:
                        case 24:
                        case 28: setc = bind(I4n10<T>, _1, musq, s4, s3, s2, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                        default: setc = bind(I4f1m<T>, _1, s4, s1, s2, s3, s23, s12, get_lmass<T>(IS, masses, 3)); break;
                    }
                    break;
                case 12:
                    paternmom += onshell_leg(2, 3) << 5 | onshell_leg(4, 4) << 4;
                    switch (paternmom) {
                        case 56:
                        case 58: setc = bind(I4n11<T>, _1, musq, s3, s12, s23, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 4)); break;
                        case 40:
                        case 42:
                        case 41:
                        case 43: setc = bind(I4n12<T>, _1, musq, s3, s4, s12, s23, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 4)); break;
                        case 24:
                        case 26:
                        case 28:
                        case 30: setc = bind(I4n12<T>, _1, musq, s3, s2, s23, s12, get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 3)); break;
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15: setc = bind(I4n13<T>, _1, musq, s2, s3, s4, s12, s23, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 4)); break;
                        default: setc = bind(I4f2m<T>, _1, s1, s2, s3, s4, s12, s23, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 4)); break;
                    }
                    break;
                case 6:
                    paternmom += onshell_leg(3, 4) << 5 | onshell_leg(1, 1) << 4;
                    switch (paternmom) {
                        case 52:
                        case 53: setc = bind(I4n11<T>, _1, musq, s4, s23, s12, get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 1)); break;
                        case 36:
                        case 37:
                        case 44:
                        case 45: setc = bind(I4n12<T>, _1, musq, s4, s1, s23, s12, get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 1)); break;
                        case 20:
                        case 21:
                        case 22:
                        case 23: setc = bind(I4n12<T>, _1, musq, s4, s3, s12, s23, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 4)); break;
                        case 4:
                        case 12:
                        case 5:
                        case 13:
                        case 6:
                        case 14:
                        case 7:
                        case 15: setc = bind(I4n13<T>, _1, musq, s3, s4, s1, s23, s12, get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 1)); break;
                        default: setc = bind(I4f2m<T>, _1, s2, s3, s4, s1, s23, s12, get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 1)); break;
                    }
                    break;
                case 3:
                    paternmom += onshell_leg(4, 1) << 5 | onshell_leg(2, 2) << 4;
                    switch (paternmom) {
                        case 50:
                        case 58: setc = bind(I4n11<T>, _1, musq, s1, s12, s23, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2)); break;
                        case 34:
                        case 42:
                        case 38:
                        case 46: setc = bind(I4n12<T>, _1, musq, s1, s2, s12, s23, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2)); break;
                        case 18:
                        case 26:
                        case 19:
                        case 27: setc = bind(I4n12<T>, _1, musq, s1, s4, s23, s12, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 1)); break;
                        case 2:
                        case 6:
                        case 10:
                        case 14:
                        case 3:
                        case 7:
                        case 11:
                        case 15: setc = bind(I4n13<T>, _1, musq, s4, s1, s2, s12, s23, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2)); break;
                        default: setc = bind(I4f2m<T>, _1, s3, s4, s1, s2, s12, s23, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2)); break;
                    }
                    break;
                case 9:
                    paternmom += onshell_leg(1, 2) << 5 | onshell_leg(3, 3) << 4;
                    switch (paternmom) {
                        case 49:
                        case 53: setc = bind(I4n11<T>, _1, musq, s2, s23, s12, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3)); break;
                        case 33:
                        case 37:
                        case 35:
                        case 39: setc = bind(I4n12<T>, _1, musq, s2, s3, s23, s12, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3)); break;
                        case 17:
                        case 21:
                        case 25:
                        case 29: setc = bind(I4n12<T>, _1, musq, s2, s1, s12, s23, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 2)); break;
                        case 1:
                        case 3:
                        case 5:
                        case 7:
                        case 9:
                        case 11:
                        case 13:
                        case 15: setc = bind(I4n13<T>, _1, musq, s1, s2, s3, s23, s12, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3)); break;
                        default: setc = bind(I4f2m<T>, _1, s4, s1, s2, s3, s23, s12, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3)); break;
                    }
                    break;
                case 10: {
                    int flags = (onshell_leg(1, 2) && onshell_leg(4, 4)) << 1 | (onshell_leg(2, 2) && onshell_leg(3, 4)) << 0;
                    switch (flags) {
                        case 3: setc = bind(I4n14<T>, _1, musq, s12, s23, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 4)); break;
                        case 2: setc = bind(I4n15<T>, _1, musq, s2, s3, s12, s23, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 4)); break;
                        case 1: setc = bind(I4n15<T>, _1, musq, s1, s4, s12, s23, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 4)); break;
                        case 0: setc = bind(I4f2m<T>, _1, s12, s2, s23, s4, s1, s3, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 4)); break;
                    }
                    break;
                }
                case 5: {
                    int flags = (onshell_leg(2, 3) && onshell_leg(1, 1)) << 1 | (onshell_leg(3, 3) && onshell_leg(4, 1)) << 0;
                    switch (flags) {
                        case 3: setc = bind(I4n14<T>, _1, musq, s23, s12, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 1)); break;
                        case 2: setc = bind(I4n15<T>, _1, musq, s3, s4, s23, s12, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 1)); break;
                        case 1: setc = bind(I4n15<T>, _1, musq, s2, s1, s23, s12, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 1)); break;
                        case 0: setc = bind(I4f2m<T>, _1, s23, s3, s12, s1, s2, s4, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 1)); break;
                    }
                    break;
                }
                case 8:
                    if (onshell_leg(1, 2) and onshell_leg(4, 4)) {
                        setc =
                            bind(I4n16<T>, _1, musq, s2, s3, s12, s23, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 4));
                    } else {
                        if (useboxc) {
                            setc = bind(I4f4mC<T>, _1, pp, mm);
                        } else {
                            setc = bind(I4f3m<T>, _1, s3, s4, s1, s2, s12, s23, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 4),
                                        get_lmass<T>(IS, masses, 2));
                        }
                    }
                    break;
                case 4:
                    if (onshell_leg(1, 1) and onshell_leg(2, 3)) {
                        setc =
                            bind(I4n16<T>, _1, musq, s3, s4, s23, s12, get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 1));
                    } else {
                        if (useboxc) {
                            setc = bind(I4f4mC<T>, _1, pp, mm);
                        } else
                            setc = bind(I4f3m<T>, _1, s4, s1, s2, s3, s23, s12, get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 1),
                                        get_lmass<T>(IS, masses, 3));
                    }
                    break;
                case 2:
                    if (onshell_leg(2, 2) and onshell_leg(3, 4)) {
                        setc =
                            bind(I4n16<T>, _1, musq, s4, s1, s12, s23, get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2));
                    } else {
                        if (useboxc) {
                            setc = bind(I4f4mC<T>, _1, pp, mm);
                        } else
                            setc = bind(I4f3m<T>, _1, s4, s12, s2, s23, s3, s1, get_lmass<T>(IS, masses, 4), get_lmass<T>(IS, masses, 1),
                                        get_lmass<T>(IS, masses, 2));
                    }
                    break;
                case 1:
                    if (onshell_leg(3, 3) and onshell_leg(4, 1)) {
                        setc =
                            bind(I4n16<T>, _1, musq, s1, s2, s23, s12, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3));
                    } else {
                        if (useboxc) {
                            setc = bind(I4f4mC<T>, _1, pp, mm);
                        } else
                            setc = bind(I4f3m<T>, _1, s2, s3, s4, s1, s23, s12, get_lmass<T>(IS, masses, 2), get_lmass<T>(IS, masses, 3),
                                        get_lmass<T>(IS, masses, 1));
                    }
                    break;

                case 0:
                    if (useboxc) {
                        setc = bind(I4f4mC<T>, _1, pp, mm);
                    } else
                        setc = bind(I4f4m<T>, _1, s1, s2, s3, s4, s12, s23, get_lmass<T>(IS, masses, 1), get_lmass<T>(IS, masses, 2),
                                    get_lmass<T>(IS, masses, 3), get_lmass<T>(IS, masses, 4));
                    break;
            }
            break;
        }

        default: _WARNING("There are no integrals with the number of propagators higher than four, returning zero"); setc = [](int i) { return CXT(0); };
    }

    return convert_intsBH_to_caravel_normalization<complex<T>>*make_series(setc);
#undef assign_si
}

#define EXTERN_INST_INTEGRALS(T)                                                                                                                               \
    extern template Series<complex<T>> integral(const lGraph::IntegralGraph&, const momD_conf<complex<T>, 4>&, const std::vector<complex<T>>&, const complex<T>&);

EXTERN_INST_INTEGRALS(R)
#ifdef HIGH_PRECISION
EXTERN_INST_INTEGRALS(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INST_INTEGRALS(RVHP)
#endif
#if INSTANTIATE_GMP
EXTERN_INST_INTEGRALS(RGMP)
#endif

#undef EXTERN_INST_INTEGRALS
}
}
