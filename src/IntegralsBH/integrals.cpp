/**********************************************************************
 *  This file is a part of libIntegrals.
 *
 *  The implementation is mostly a C++ port of OneLOop Fortran library by A.van Hameren
 *  http://helac-phegas.web.cern.ch/helac-phegas/OneLOop.html
 *
 *  adapted to multiprecision evaluation with qd.
 *
 ***********************************************************************/

#include "integrals.h"

// IMPLEMENTATIONS
#include "integrals.hpp"
#include "integrals_complex.hpp"


using namespace std;

namespace Caravel {
namespace IntegralsBH {

#define INST_INTEGRALS(T)                                                                                                                                      \
    template Series<complex<T>> integral(const lGraph::IntegralGraph& IS, const momD_conf<complex<T>, 4>& mc, const std::vector<complex<T>>& masses,                \
                                         const complex<T>& musqC);

INST_INTEGRALS(R)
#ifdef HIGH_PRECISION
INST_INTEGRALS(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INST_INTEGRALS(RVHP)
#endif
#if INSTANTIATE_GMP
INST_INTEGRALS(RGMP)
#endif

#undef INST_INTEGRALS
}
}
