#include "ProviderBH.h"

namespace Caravel{
namespace IntegralsBH{

_INST_PROVIDERBH(,C)

#ifdef HIGH_PRECISION
_INST_PROVIDERBH(,CHP)
#endif

#ifdef VERY_HIGH_PRECISION
_INST_PROVIDERBH(,CVHP)
#endif

}
}
