namespace Caravel {
namespace IntegrandHierarchy {

template <typename T, size_t L, typename TF> std::vector<lGraph::xGraph> IntegrandHierarchy<T, L, TF>::get_master_topologies() const {
    std::vector<lGraph::xGraph> toreturn;
    for (const auto& pos : *master_positions) { toreturn.push_back(graph_representation->nodes[pos.first.first][pos.first.second].get_graph()); }
    return toreturn;
}

template <typename T, size_t L, typename TF> std::vector<lGraph::IntegralGraph> IntegrandHierarchy<T, L, TF>::get_integral_graphs() const {
    using namespace lGraph;
    std::vector<IntegralGraph> toreturn;
    for (const auto& pos : *master_positions) {
        const auto& gr = graph_representation->nodes[pos.first.first][pos.first.second].get_graph();

        const auto& master_data = FunctionSpace::master_term_data_holder.at(gr.get_topology());
        std::vector<std::string> names = std::get<decltype(names)>(master_data);
        std::string topology_name = std::get<std::string>(master_data);

        if(names.empty()){
            // if there are no names, we just use the index
            toreturn.emplace_back(gr, std::to_string(pos.second),topology_name);
        }
        else{
            toreturn.emplace_back(gr, names.at(pos.second),topology_name);
        }

        // add info about how this integral is mapped to a representative master,
        // only if the representative graph was provided
        const auto& repr_graph = std::get<lGraph::xGraph>(master_data);
        if (repr_graph.get_base_graph() != decltype(repr_graph.get_base_graph()){}){
            toreturn.back().to_representative_kinematics_map = lGraph::lGraphKin::abstract_external_momentum_mapper(gr,repr_graph);
        }

    }
    return toreturn;
}

template <typename T, size_t L> std::vector<lGraph::xGraph> ColourExpandedAmplitude<T, L>::get_master_topologies() const {
    return all_hierarchies.at(0).get_master_topologies();
}

template <typename T, size_t L> std::vector<lGraph::IntegralGraph> ColourExpandedAmplitude<T, L>::get_integral_graphs() const {
    return all_hierarchies.at(0).get_integral_graphs();
}

template <typename T, size_t L> std::shared_ptr<const std::vector<MasterPosition>> ColourExpandedAmplitude<T, L>::get_master_positions() const {
    return all_hierarchies.at(0).master_positions;
}

template <typename T, size_t L>
std::function<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients(const momentumD_configuration<T, 4>& mom_conf)>
AmplitudeLikeContribution<T, L>::get_coeff_evaluator() {
    _WARNING_R("Not implemented!");
    std::exit(1);
}

template <typename T, size_t L> std::vector<lGraph::xGraph> AmplitudeLikeContribution<T, L>::get_master_topologies() const {
    return hierarchy.get_master_topologies();
}

template <typename T, size_t L> std::vector<lGraph::IntegralGraph> AmplitudeLikeContribution<T, L>::get_integral_graphs() const {
    return hierarchy.get_integral_graphs();
}

// has to be implemented in the header for auto deduction type to work
template <typename T, size_t L>
std::function<std::vector<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients>(const momentumD_configuration<T, 4>& mom_conf)>
ColourExpandedAmplitude<T, L>::get_new_master_coefficients_evaluator() {

    // This should be unique_ptr and lambdas below should own it, disallowing copies intrinsically.
    // However std::function cannot be constructed from move only lambda, so we cannot
    // have known type for these functions and this is very inconvenient for many reasons.
    // The decision is to make it shared pointer for now and then the user should not copy it
    auto new_builder = std::make_shared<Builder<T, D>>(forest.get(), multiplicity);
    size_t index = add_residue_evaluators({new_builder});

    using namespace Caravel::settings::IntegrandHierarchy;

    switch (normalization) {
        case norm_type::spinor_weight:
            return [index, this ](const momentumD_configuration<T, 4>& mom_conf) {

                std::vector<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients> results;

                for (auto& it : all_hierarchies) {
                    auto r = (it.get_master_coefficients(mom_conf, index));
                    auto norm_val = T(1) / it.compute_normalization(mom_conf);
                    for (auto& i : r) { i.numerator *= norm_val; }
                    results.push_back(std::move(r));
                }

                // we assume warmup is there alredy otherwise thread safety can not be guaranteed

                return results;
            };

        case norm_type::tree:
            return [ b = new_builder, index, this ](const momentumD_configuration<T, 4>& mom_conf) {

                std::vector<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients> results;

                for (auto& it : all_hierarchies) {
                    auto r = (it.get_master_coefficients(mom_conf, index));
                    auto norm_val = T(1) / (b->get_tree(it.tree_id));
                    for (auto& i : r) { i.numerator *= norm_val; }
                    results.push_back(std::move(r));
                }

                // we assume warmup is there alredy otherwise thread safety can not be guaranteed

                return results;
            };

        default:
            return [ b = new_builder, index, this ](const momentumD_configuration<T, 4>& mom_conf) {

                std::vector<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients> results;

                for (auto& it : all_hierarchies) { results.push_back(it.get_master_coefficients(mom_conf, index)); }

                // we assume warmup is there alredy otherwise thread safety can not be guaranteed

                return results;
            };
    }
}

namespace internal{

// Fit the results for Ds = Ds1,Ds2,Ds3 to get the polynomial in Ds = 4-2*ep
// Notice implicit assumtion Ds=f(ep) (e.g. Ds = 4-2*ep)
template <typename F, typename TDs>
std::vector<DenseRational<F>> dimensional_reconstruct_quadratic(const std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>& in, const std::vector<size_t>& Dss_s,
                                                 DensePolynomial<TDs> Ds_value) {

    Ds_value -= static_cast<TDs>(4);

    // TODO
    // Coefficients for extraction should be evaluated at construction time (and updated with cardinality if it changes)
    std::vector<int> Dss;
    for (auto it : Dss_s) Dss.push_back(it);

    std::vector<F> A0_coeffs = {F(((-4 + Dss[1]) * (-4 + Dss[2]))) / F(((Dss[0] - Dss[1]) * (Dss[0] - Dss[2]))),
                           F(-(((-4 + Dss[0]) * (-4 + Dss[2])))) / F(((Dss[0] - Dss[1]) * (Dss[1] - Dss[2]))),
                           F(((-4 + Dss[0]) * (-4 + Dss[1]))) / F(((Dss[0] - Dss[2]) * (Dss[1] - Dss[2])))};

    std::vector<F> A1_coeffs = {F(8 - Dss[1] - Dss[2]) / F(((Dss[0] - Dss[1]) * (Dss[0] - Dss[2]))),
                           F(-8 + Dss[0] + Dss[2]) / F(((Dss[0] - Dss[1]) * (Dss[1] - Dss[2]))),
                           F(-8 + Dss[0] + Dss[1]) / F(((Dss[0] - Dss[2]) * (-Dss[1] + Dss[2])))};

    std::vector<F> A2_coeffs = {F(1) / F(((Dss[0] - Dss[1]) * (Dss[0] - Dss[2]))), F(-1) / F(((Dss[0] - Dss[1]) * (Dss[1] - Dss[2]))),
                           F(1) / F(((Dss[0] - Dss[2]) * (Dss[1] - Dss[2])))};

    std::vector<DenseRational<F>> out;

    std::string var_name = Ds_value.var_name;
    DensePolynomial<F> Ds_value_casted(Ds_value);

    for (auto& coeff : in) {

        // here we get coefficients
        // A_0 + A_1 (D_s-4) + A2 (D_s-4)^2
        std::vector<F> A0, A1, A2;
        for (auto& order : coeff.numerator.coefficients) {
            if(order.size() == 0){
                // empty coeff
                A0.push_back(F(0));
                A1.push_back(F(0));
                A2.push_back(F(0));
            }
            else{
                A0.push_back(order[0] * A0_coeffs[0] + order[1] * A0_coeffs[1] + order[2] * A0_coeffs[2]);
                A1.push_back(order[0] * A1_coeffs[0] + order[1] * A1_coeffs[1] + order[2] * A1_coeffs[2]);
                A2.push_back(order[0] * A2_coeffs[0] + order[1] * A2_coeffs[1] + order[2] * A2_coeffs[2]);
            }
        }

        DensePolynomial<F> num_const(std::move(A0), var_name);
        DensePolynomial<F> num_lin(std::move(A1), var_name);
        DensePolynomial<F> num_quad(std::move(A2), var_name);

        num_const += (num_lin *= Ds_value_casted);
        num_const += ((num_quad *= Ds_value_casted) *= Ds_value_casted);

        out.push_back(DenseRational<F>(std::move(num_const), std::move(coeff.denominator)));
    }

    return out;
}

// dimensional reconstruction for a generic polynomial a0+a1*Ds+...+aN*Ds^N (with an implicit assumtion Ds=f(ep), like Ds=4-2*ep)
template <typename F, typename TDs>
std::vector<DenseRational<F>> dimensional_reconstruct_generic(const std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>& in, const std::vector<size_t>& Dss_s,
                                                 DensePolynomial<TDs> Ds_value) {

    const size_t N = (in[0].numerator.coefficients)[0].size();

    // FIXME: the inversion is very inefficient, it should be made static (with updates after cardinality changes in finite fields)
    std::vector<F> A;
    std::vector<F> entries(Dss_s.size(),F(1));
    for(size_t ii=0;ii<N;ii++){
        A.insert(A.end(),entries.begin(),entries.end());
        std::transform(Dss_s.begin(),Dss_s.end(),entries.begin(),entries.begin(), [](const size_t& dim, const F& previous) { return previous*F(int(dim)); } );
    }

    std::vector<DenseRational<F>> out;

    std::string var_name = Ds_value.var_name;

    for (auto& coeff : in) {

        DensePolynomial<F> Ds_value_casted(Ds_value);

        // here we get the ai coefficients
        // a0 + a1 (D_s-4) + ... + aN (Ds-4)^N
        std::vector<std::vector<F>> as(N, std::vector<F>(coeff.numerator.coefficients.size()));
        for (size_t eppower=0; eppower<coeff.numerator.coefficients.size(); eppower++) {
            const auto& evals = coeff.numerator.coefficients[eppower];
            std::vector<F> b(N);
            for (size_t ii=0; ii<N; ii++)    b[ii] = evals[ii];
            // FIXME: this copy should be avoided, but plu_solver takes an rvalue reference as argument
            std::vector<F> Alocal = A;
            auto result = plu_solver(std::move(Alocal), std::move(b));
            for (size_t ii=0; ii<N; ii++)    as[ii][eppower] = result[ii];
        }

        DensePolynomial<F> num_const(as[0], var_name);
        for ( size_t ii=1; ii<N; ii++ ) {
            DensePolynomial<F> local(as[ii], var_name);
            local *= Ds_value_casted;
            num_const += local;
            Ds_value_casted *= Ds_value_casted;
        }

        out.push_back(DenseRational<F>(std::move(num_const), std::move(coeff.denominator)));
    }

    return out;
}

// From coefficients of expansion in (Ds-6) set a value of Ds
// Notice implicit assumtion Ds=f(ep) (e.g. Ds = 4-2*ep)
template <typename F, typename TDs>
std::vector<DenseRational<F>> dimensional_reconstruct_quadratic_2(const std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>& in,
                                                   DensePolynomial<TDs> Ds_value) {

    Ds_value -= static_cast<TDs>(6);

    std::vector<DenseRational<F>> out;

    std::string var_name = Ds_value.var_name;
    DensePolynomial<F> Ds_value_casted(Ds_value);

    for (auto& coeff : in) {

        // here we get coefficients
        // A_0 + A_1 (D_s-6) + A2 (D_s-6)^2
        std::vector<F> A0, A1, A2;
        for (auto& order : coeff.numerator.coefficients) {
            if(order.size() == 0){
                // empty coeff
                A0.push_back(F(0));
                A1.push_back(F(0));
                A2.push_back(F(0));
            }
            else{
                A0.push_back(order[0]);
                A1.push_back(order[1]);
                A2.push_back(order[2]);
            }
        }

        DensePolynomial<F> num_const(std::move(A0), var_name);
        DensePolynomial<F> num_lin(std::move(A1), var_name);
        DensePolynomial<F> num_quad(std::move(A2), var_name);

        num_const += (num_lin *= Ds_value_casted);
        num_const += ((num_quad *= Ds_value_casted) *= Ds_value_casted);

        out.push_back(DenseRational<F>(std::move(num_const), std::move(coeff.denominator)));
    }

    return out;
}

// Fit the results for Ds = Ds1,Ds2,Ds3 to get the polynomial in Ds = 4-2*ep
// Notice implicit assumtion Ds=f(ep) (e.g. Ds = 4-2*ep)
template <typename F, typename TDs>
std::vector<DenseRational<DensePolynomial<F>, F>> dimensional_reconstruct_quadratic_Ds(const std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>& in, const std::vector<size_t>& Dss_s,
                                                 DensePolynomial<TDs> Ds_value) {

    Ds_value -= static_cast<TDs>(4);

    // TODO
    // Coefficients for extraction should be evaluated at construction time (and updated with cardinality if it changes)
    std::vector<int> Dss;
    for (auto it : Dss_s) Dss.push_back(it);

    std::vector<F> A0_coeffs = {F(((-4 + Dss[1]) * (-4 + Dss[2]))) / F(((Dss[0] - Dss[1]) * (Dss[0] - Dss[2]))),
                           F(-(((-4 + Dss[0]) * (-4 + Dss[2])))) / F(((Dss[0] - Dss[1]) * (Dss[1] - Dss[2]))),
                           F(((-4 + Dss[0]) * (-4 + Dss[1]))) / F(((Dss[0] - Dss[2]) * (Dss[1] - Dss[2])))};

    std::vector<F> A1_coeffs = {F(8 - Dss[1] - Dss[2]) / F(((Dss[0] - Dss[1]) * (Dss[0] - Dss[2]))),
                           F(-8 + Dss[0] + Dss[2]) / F(((Dss[0] - Dss[1]) * (Dss[1] - Dss[2]))),
                           F(-8 + Dss[0] + Dss[1]) / F(((Dss[0] - Dss[2]) * (-Dss[1] + Dss[2])))};

    std::vector<F> A2_coeffs = {F(1) / F(((Dss[0] - Dss[1]) * (Dss[0] - Dss[2]))), F(-1) / F(((Dss[0] - Dss[1]) * (Dss[1] - Dss[2]))),
                           F(1) / F(((Dss[0] - Dss[2]) * (Dss[1] - Dss[2])))};

    std::vector<DenseRational<DensePolynomial<F>, F>> out;

    // TODO: this should somehow be built from Ds_value
    std::string var_name = "(Ds-4)";
    std::string var_name_ep;

    if (in.size() > 0) 
        var_name_ep = in[0].numerator.var_name;
    else
        return out;

    for (auto& coeff : in) {

        // here we get coefficients
        // A_0 + A_1 (D_s-4) + A2 (D_s-4)^2
        std::vector<F> A0, A1, A2;
        for (auto& order : coeff.numerator.coefficients) {
            if(order.size() == 0){
                // empty coeff
                A0.push_back(F(0));
                A1.push_back(F(0));
                A2.push_back(F(0));
            }
            else{
                A0.push_back(order[0] * A0_coeffs[0] + order[1] * A0_coeffs[1] + order[2] * A0_coeffs[2]);
                A1.push_back(order[0] * A1_coeffs[0] + order[1] * A1_coeffs[1] + order[2] * A1_coeffs[2]);
                A2.push_back(order[0] * A2_coeffs[0] + order[1] * A2_coeffs[1] + order[2] * A2_coeffs[2]);
            }
        }

        std::vector<DensePolynomial<F>> toconst;

        for (size_t power=0; power<A0.size(); power++)
            toconst.push_back( DensePolynomial<F>( { A0[power], A1[power], A2[power] }, var_name )  );

        out.push_back(DenseRational<DensePolynomial<F>, F>( DensePolynomial<DensePolynomial<F>, F>(toconst, var_name_ep), std::move(coeff.denominator)));
    }

    return out;
}

// dimensional reconstruction for a generic polynomial a0+a1*Ds+...+aN*Ds^N (with an implicit assumtion Ds=f(ep), like Ds=4-2*ep)
template <typename F, typename TDs>
std::vector<DenseRational<DensePolynomial<F>, F>> dimensional_reconstruct_generic_Ds(const std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>& in, const std::vector<size_t>& Dss_s,
                                                 DensePolynomial<TDs> Ds_value) {

    const size_t N = (in[0].numerator.coefficients)[0].size();

    // FIXME: the inversion is very inefficient, it should be made static (with updates after cardinality changes in finite fields)
    std::vector<F> A;
    std::vector<F> entries(Dss_s.size(),F(1));
    for(size_t ii=0;ii<N;ii++){
        A.insert(A.end(),entries.begin(),entries.end());
        std::transform(Dss_s.begin(),Dss_s.end(),entries.begin(),entries.begin(), [](const size_t& dim, const F& previous) { return previous*F(int(dim)); } );
    }

    std::vector<DenseRational<DensePolynomial<F>, F>> out;

    // TODO: this should somehow be built from Ds_value
    std::string var_name = "Ds";
    std::string var_name_ep;

    if (in.size() > 0) 
        var_name_ep = in[0].numerator.var_name;
    else
        return out;

    for (auto& coeff : in) {

        // here we get the ai coefficients
        // a0 + a1 (D_s-4) + ... + aN (Ds-4)^N
        std::vector<std::vector<F>> as(N, std::vector<F>(coeff.numerator.coefficients.size()));
        for (size_t eppower=0; eppower<coeff.numerator.coefficients.size(); eppower++) {
            const auto& evals = coeff.numerator.coefficients[eppower];
            std::vector<F> b(N);
            for (size_t ii=0; ii<N; ii++)    b[ii] = evals[ii];
            // FIXME: this copy should be avoided, but plu_solver takes an rvalue reference as argument
            std::vector<F> Alocal = A;
            auto result = plu_solver(std::move(Alocal), std::move(b));
            for (size_t ii=0; ii<N; ii++)    as[ii][eppower] = result[ii];
        }

        std::vector<DensePolynomial<F>> toconst;

        for (size_t power=0; power<as[0].size(); power++){
            std::vector<F> local(N);
            for(size_t ii=0; ii<N; ii++)
                local[ii] = as[ii][power];
            toconst.push_back( DensePolynomial<F>(local, var_name) );
        }

        out.push_back(DenseRational<DensePolynomial<F>, F>( DensePolynomial<DensePolynomial<F>, F>(toconst, var_name_ep), std::move(coeff.denominator)));
    }

    return out;
}

// Trivial conversion from Vec<F> representing polynomial in (Ds-6) to dense polynomial
template <typename F>
std::vector<DenseRational<DensePolynomial<F>, F>> vec_to_dsm6_polynomials(const std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>& in) {

    std::vector<DenseRational<DensePolynomial<F>, F>> out;

    static const std::string dsm6_var{"(Ds-6)"};

    std::function<DensePolynomial<F>(Vec<F>)> f = [](Vec<F> vec) -> DensePolynomial<F> {
        return DensePolynomial<F>{vec.get_components(), dsm6_var};
    };

    for (auto& ri : in) {
        out.emplace_back(fmap(f,ri.numerator),ri.denominator);
    }

    return out;
}

} // namespace internal
} // namespace IntegrandHierarchy

namespace detail {
template <typename T, size_t D> inline std::enable_if_t<is_exact_v<T>, T> plus_for_exact(const momentumD<T, D>& m) { return _spinor_private::plus(m); }
template <typename T, size_t D> inline std::enable_if_t<is_floating_point_v<T>, T> plus_for_exact(const momentumD<T, D>& m) { return static_cast<T>(1); }
} // namespace detail

/**
 * Implementation of normalization in +-+- metric from eq (20) of arXiv:1605.02172 (for gluons)
 * For quarks pairs {q_i,qb_j} we divide by <i|k|j] with k being the first momentum such that k!=i and k!=j.
 * NOTE: this function computes the spinor weight itself, not the inverse.
 */
template <typename T> std::function<T(const momentumD_configuration<T, 4>&)> construct_normalizer(std::vector<Particle> process) {
    // order in momenta indices if input was not ordered
    std::sort(process.begin(), process.end(), [](auto& a, auto& b) { return a.mom_index() < b.mom_index(); });

    auto plus_helicity = [](const SingleState& x) {
        switch (x.get_helicity()) {
            case helicity::p: return true;
            case helicity::m: return false;
            default: _WARNING_R("Unknown helicity encountered while constructing normalization"); std::exit(1);
        }
    };

    std::vector<size_t> q_ind;
    std::vector<size_t> qb_ind;

    // find all quark and antiquark external states
    for (size_t i = 0; i < process.size(); i++) {
        auto state = process.at(i).external_state().get_name();

        if (state == "qm")
            q_ind.push_back(i);
        else if (state == "qbp")
            qb_ind.push_back(i);
    };

    assert(q_ind.size() == qb_ind.size());

    typedef std::function<void(T&, const momentumD_configuration<T, 4>&)> op_type;
    // we collect all operations to be performed in a vector,
    // so the final function simply applies them consequently
    std::vector<op_type> operations;

    using detail::plus_for_exact;

    // if first particle is a gluon
    if (process.front().get_statistics() == ParticleStatistics::vector or process.front().get_type() == ParticleType("G")) {
        if (plus_helicity(process.front().external_state().get_name()))
            operations.push_back([](T& r, const momentumD_configuration<T, 4>& conf) {
                T h1 = spaa(conf.p(1), conf.p(3)) / (spbb(conf.p(1), conf.p(2)) * spaa(conf.p(3), conf.p(2)));
                r /= h1;
            });
        else
            operations.push_back([](T& r, const momentumD_configuration<T, 4>& conf) {
                T h1 = spaa(conf.p(1), conf.p(3)) / (spbb(conf.p(1), conf.p(2)) * spaa(conf.p(3), conf.p(2)));
                r *= h1;
            });
    }

    // for all other gluons
    for (size_t i = 1; i < process.size(); i++) {
        const auto& it = process[i];
        if (it.get_statistics() == ParticleStatistics::vector or it.get_type() == ParticleType("G")) {
            if (plus_helicity(it.external_state().get_name())) {
                operations.push_back([i](T& r, const momentumD_configuration<T, 4>& conf) {
                    auto pi = conf.p(i + 1);
                    T hinum = (spaa(pi, conf.p(1)) * spaa(pi, conf.p(1)) * spbb(conf.p(1), conf.p(2)) * spaa(conf.p(3), conf.p(2)));
                    T hidenom = (spaa(conf.p(1), conf.p(3)) );
                    T hi = hinum / hidenom;
                    r /= hi;
                });
            }
            else {
                operations.push_back([i](T& r, const momentumD_configuration<T, 4>& conf) {
                    auto pi = conf.p(i + 1);
                    T hinum = (spaa(pi, conf.p(1)) * spaa(pi, conf.p(1)) * spbb(conf.p(1), conf.p(2)) * spaa(conf.p(3), conf.p(2)));
                    T hidenom = (spaa(conf.p(1), conf.p(3)));
                    T hi = hinum / hidenom;
                    r *= hi;
                });
            }
        }
    }

    {
        auto find_mom_ind = [size = process.size()](size_t i1, size_t i2) {
            for (size_t n = 0; n < size; n++) {
                if (n != i1 and n != i2) return n;
            }
            throw std::logic_error("Couldn't find a suitable momentum");
        };

        // now normalization for quark pairs
        for (size_t i = 0; i < q_ind.size(); i++) {
            operations.push_back([il = q_ind.at(i) + 1, ir = qb_ind.at(i) + 1, k = find_mom_ind(q_ind.at(i), qb_ind.at(i)) + 1](
                                     T& r, const momentumD_configuration<T, 4>& conf) { r *= spab(conf.p(il), conf.p(k), conf.p(ir)); });
        }
    }

    // for gravitons we square the normalization
    {
        bool allgravitons(true);
        for (auto& p : process) {
            if (p.get_type() != ParticleType("G")) {
                allgravitons = false;
                break;
            }
        }
        if (allgravitons) operations.push_back([](T& r, const momentumD_configuration<T, 4>&) { r *= r; });
    }

    auto ret = [operations](const momentumD_configuration<T, 4>& conf) {
        T result(1);
        for (auto& it : operations) it(result, conf);
        return result;
    };

    return ret;
}

template <typename T> std::function<T(const momentumD_configuration<T, 4>&)> construct_normalizer(std::vector<Particle*> process) {
    std::vector<Particle> forward;
    for (auto p : process) forward.push_back(*p);
    return construct_normalizer<T>(forward);
}

} // namespace Caravel
