#ifndef SUBTRACTIONENGINE_H_INC
#define SUBTRACTIONENGINE_H_INC

#include "Forest/Builder.h"
#include "FunctionSpace/FunctionSpace.h"
#include "Graph/GraphKin.h"
#include "AmplitudeCoefficients/Hierarchy.h"
#include "AmplitudeCoefficients/NumericalTools.h"
#include "AmplitudeCoefficients/ResidueEvaluator.h"
#include "FunctionalReconstruction/UnivariateReconstruction.h"
#include <functional>
#include <utility>
#include <vector>
#include "Core/typedefs.h"
#include "Core/type_traits_extra.h"
#include "Core/InversionStrategies.h"

namespace Caravel {

using FunctionSpace::FunctionBasis;

using HierarchyWarmupInfo = std::tuple<FunctionSpace::HierarchyBasisTruncation, std::vector<DensePolynomial<BigRat>>, std::vector<int>>;

/**
 * A pair of position/ID for a given master integral.
 */
using MasterPosition = std::pair<HierarchyPos, size_t>;

/**
 * Constructs a list of MasterPositions from a given hierarchy slice.
 */
template <class Hierarchy> std::vector<MasterPosition> slice_master_positions(const Hierarchy& h, const HierarchySlice& slice);

template <typename T, size_t D, typename TOPOLOGY>
using PointGenStrategy = std::function<std::function<OnShellPoint<T, D>()>(const lGraph::lGraphKin::GraphKin<T,D>&, const TOPOLOGY&)>;

/**
 * Data structure for setting up an amplitude-like rational
 * function in terms of numerator contributions.
 */
template <typename T, size_t D> using AmplitudeContributions = std::unordered_map<HierarchyPos, std::function<T(const OnShellPoint<T, D>&)>>;

template <typename T> using HierarchyCoefficients = std::vector<std::vector<std::pair<bool, std::vector<T>>>>;

template <class Hierarchy> FunctionSpace::HierarchyBasisTruncation empty_hierarchy_basis_truncation(const Hierarchy& h) {
    using std::vector;

    FunctionSpace::HierarchyBasisTruncation basis_truncations;
    for (auto& gen : h.nodes) {
        std::vector<FunctionSpace::BasisTruncation> gen_truncation(gen.size(), std::vector<size_t>());
        basis_truncations.push_back(gen_truncation);
    }

    return basis_truncations;
}

/**
 * A class describing which describes how to compute the
 * subtracted residue, against which we fit on-shell.
 */
template <typename TF, typename T, size_t D, size_t L> class SubtractedResidue {

    using BasisList = std::vector<std::vector<FunctionBasis<T, D>>>;

    HierarchyPos top_pos;
    std::vector<std::pair<HierarchyPos, LinkInfo<L>>> subtracted_ancestors;
    const BasisList& bases;
    ResidueEvaluator<TF, T, D> residue_evaluator;

  public:
    SubtractedResidue(HierarchyPos _top_pos, const std::vector<std::pair<HierarchyPos, LinkInfo<L>>>& _subtracted_ancestors,
                      const BasisList& _bases, const ResidueEvaluator<TF, T, D>& _residue_evaluator)
        : top_pos(_top_pos), subtracted_ancestors(_subtracted_ancestors), bases(_bases), residue_evaluator(_residue_evaluator) {}

    /**
     * Evaluate the subtracted residue on a given kinematic
     * point, for a series of values of d. Allows for caching of
     * information repeated across many values of d.
     */
    std::vector<TF> operator()(const momentumD_configuration<T, D>&, const OnShellPoint<T, D>& p, std::vector<T> d_values, 
                               const std::vector<HierarchyCoefficients<TF>>& all_coefficients, size_t container_dimension /* = 0 default value means scalar */) const;
    void change_point(const momentumD_configuration<T,4>& new_point);
};

/**
 * A pair of a (type-erased) subtracted residue and the list of
 * unsubtracted ancestors to be used in the ansatz.
 */
template <typename TF, typename T, size_t D, size_t L>
using OnShellFunctionPrescription = std::pair<std::function<std::vector<TF>(const momentumD_configuration<T,D>&,const OnShellPoint<T, D>&, std::vector<T>, const std::vector<HierarchyCoefficients<TF>>&, size_t)>,
                                              std::vector<std::pair<HierarchyPos, LinkInfo<L>>>>;

template <typename TF, typename T, size_t D, size_t L> using HierarchySubtractedResidues = std::vector<std::vector<OnShellFunctionPrescription<TF, T, D, L>>>;

/**
 * Builds a subtracted residue and its associated unsubtracted
 * ancestors ready for the ansatzing procedure.
 */
template <typename TF, typename T, size_t D, size_t L>
OnShellFunctionPrescription<TF, T, D, L>
build_subtracted_residue(const std::vector<std::vector<std::pair<bool, std::vector<TF>>>>& coefficients, const std::vector<std::vector<FunctionBasis<T, D>>>& bases,
                         ResidueEvaluator<TF, T, D> residue_evaluator, const momentumD_configuration<T, D>& phys_momD_conf,
                         HierarchyPos top_pos, const HierarchySlice& slice);

/**
 * Given a hierarchy of function bases this function constructs an
 * empty set of coefficients for that basis.
 */
template <typename TF, typename T, size_t D>
HierarchyCoefficients<TF> build_empty_hierarchy_coefficients(const std::vector<std::vector<FunctionBasis<T, D>>>& hierarchy_function_bases);

/**
 * Converts a vector valued function, into a vector of functions
 * returning each returning a component of the original function. To
 * do this it simply calls the original function and returns the
 * associated component. It memoizes the call in order to avoid
 * unnecessary calls to the base function.
 */
template <typename A, typename... B> std::vector<std::function<A(B...)>> to_vector_of_functions(const std::function<std::vector<A>(B...)>& vec_f, const size_t n);

template <typename TF, typename T, size_t D, size_t L> class SubtractionEngine {
  private:
    using GraphKin = lGraph::lGraphKin::GraphKin<T, D>;
    using Hierarchy = HierarchyGraph<ColourOrderedResidue<L>, LinkInfo<L>>;
    std::vector<std::vector<std::vector<HierarchyPos>>> parallel_scheme;

  public:
    SubtractionEngine(std::shared_ptr<const SubtractionEngine::Hierarchy>, std::shared_ptr<const HierarchySlice>, ResidueEvaluator<TF, T, D>,
                      std::shared_ptr<const std::vector<MasterPosition>>, std::shared_ptr<HierarchyWarmupInfo>,
                      bool set_verbose = false);

    /**
     * Runs the reduction, returning the results. If there is a
     * warmup, the denominator of the result is build from
     * there. If there is no warmup, one will be (implicitly)
     * present after the function is run.
     *
     * @param true: attemp dropping functions for one D, then run with settings::IntegrandHierarchy::n_cached_D_values_warmup (default 30) D values, and runs BG for cut computation two times
     *        false: run BG for cuts only ONCE with all D values simultaniously with attemping truncation
     */
    std::vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>> reduce(const momentumD_configuration<T, 4>&, bool /* = true */, size_t container_dimension /* = 0 default value means scalar */);

    /**
     * Update function bases with the existing warmup information. Can be used by external handler.
     */
    void build_hierarchy_function_bases_with_warmup();

    /**
     * Communicate the scheme of parallel computation of topologies on each level of the hierarachy.
     * It is of course the responsibility of the caller that the residue evaluators of these topologies are thread safe
     * w.r.t. each other.
     * The scheme should be compatible with the slice.
     */
    void assign_parallelization_scheme(const std::vector<std::vector<std::vector<HierarchyPos>>>& s) { parallel_scheme = s; }

  private:
    /**
     * Calculates the warmup info.
     */
    template <typename TTT = T> std::enable_if_t<is_exact<TTT>::value, HierarchyWarmupInfo> get_warmup_info();
    template <typename TTT = T> std::enable_if_t<!is_exact<TTT>::value, HierarchyWarmupInfo> get_warmup_info();
    /**
     * Do something when cardinality changes
     */
    template <typename TT = T> std::enable_if_t<is_finite_field<TT>::value> update_cardinality();
    template <typename TT = T> std::enable_if_t<!is_finite_field<TT>::value> update_cardinality() {}
    /**
     * Constructs the ansatz to be fitted for a given topology.
     * TODO: Could we avoid doing this once per kinematic point?
     */
    Ansatz<T, D> construct_ansatz(HierarchyPos topo_pos, const std::vector<std::pair<HierarchyPos, LinkInfo<L>>>& unsubtracted_ancestors,
                                  const momentumD_configuration<T, D>& physical_mom_conf, T d);
    void truncate_basis(std::vector<HierarchyCoefficients<TF>>&, HierarchyPos topo_pos, size_t d_i, const lGraph::xGraph& graph, const GraphKin&, size_t container_dimension);
    /**
     * Reduces the system for a fixed set of d values.
     */
    std::vector<std::vector<TF>> coefficient_worker(const momentumD_configuration<T, D>&, const std::vector<T>&, bool, size_t container_dimension);
    /**
     * Reduces the function using warmup information
     */
    std::vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>> reduce_with_warmup(const momentumD_configuration<T, D>&, size_t container_dimension);
    /**
     * Reduces the function withou using warmup information.
     */
    template <typename TT = T>
    std::enable_if_t<is_exact<TT>::value, std::vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>>>
    reduce_without_warmup(const momentumD_configuration<T, D>&, bool, size_t container_dimension);
    template <typename TT = T>
    std::enable_if_t<!is_exact<TT>::value, std::vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>>>
    reduce_without_warmup(const momentumD_configuration<T, D>&, bool, size_t container_dimension);
    /**
     * Utility function to fit the ansatz
     */
    template <typename FType>
    std::vector<std::vector<TF>> fit_ansatz(const Ansatz<T, D>& ansatz, const FType& f, const std::function<OnShellPoint<T, D>()>& point_gen,
                                            bool attempt_truncation, size_t container_dimension);

    void build_hierarchy_subtracted_residues(ResidueEvaluator<TF, T, D>);
    std::vector<TF> extract_master_coeffs(const HierarchyCoefficients<TF>& coeffs, const int Ds_dim = 0);

    /**
     * Stores kinematical information for each topology. Changed per external phase space point.
     * Only stores for topologies in the slice and corresponds directly to the order of iterating through slice.
     */
    std::shared_ptr<std::vector<GraphKin>> hierarchy_kinematics_cache{std::make_shared<std::vector<GraphKin>>()};
    std::shared_ptr<std::mt19937> rng;
    std::vector<std::function<std::function<OnShellPoint<T,D>()>(decltype(rng))>> os_generators;

    std::shared_ptr<FunctionSpace::HierarchyFunctionBases<T, D>> function_bases;
    FunctionSpace::HierarchyBasisTruncation basis_truncations;
    std::vector<DensePolynomial<T>> master_denominators;
    std::vector<int> numerator_ranks;


    HierarchySubtractedResidues<TF, T, D, L> hierarchy_subtracted_residues;

    std::shared_ptr<const Hierarchy> hierarchy;
    std::shared_ptr<const HierarchySlice> slice;
    std::shared_ptr<const std::vector<MasterPosition>> master_positions;
    std::shared_ptr<HierarchyWarmupInfo> warmup_info;
    /**
     * Change external phase space point for residues.
     */
    std::function<void(const momentumD_configuration<T,4>&)> residue_change_point;
    bool verbose{false};

    uint64_t last_cardinality{0};

    void assign_rng_state(const typename decltype(rng)::element_type&);
};

/**
 * Loads warmup information from the given file.
 * If there is no file found then an empty warmup is returned.
 * @param warmup_filename Path to the file containing the warmup.
 */
std::vector<HierarchyWarmupInfo> load_warmups(std::string warmup_filename);
/**
 * Writes a list of warmup informations to the path given by warmup_file.
 */
void write_warmup(std::string warmup_file, const std::vector<std::shared_ptr<HierarchyWarmupInfo>>& infos);
}

#include "SubtractionEngine.hpp"

#endif // SUBTRACTIONENGINE_H_INC
