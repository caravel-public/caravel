/*
 * Topology.h
 * First created on 1.12.2015
 *
 * Headers for Topologies and related classes
 *
*/

#ifndef TOPOLOGY_H_INC
#define TOPOLOGY_H_INC

#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <utility>
#include <map>

#include "Core/Utilities.h"

#include "Core/MathInterface.h"
#include "Core/AlgebraicStructures.h"
#include "Core/Particle.h"
#include "Core/momD.h"
#include "Core/momD_conf.h"

#include "Core/cutTopology.h"

#include "Forest/Builder.h"

#include "Graph/Graph.h"

namespace Caravel {

template <size_t L> class Cut;
class PartialAmplitudeInput;

class ParticleSchematic{
    private:
        std::string name{""};
        size_t momindex{0};
        int internal_index{0};
        int flavor{0};

        int spinor_chain_index{0};

        bool opposite_representation{false};

        template <size_t L> friend class Cut;

    public:
        ParticleSchematic() = default;
        ParticleSchematic(std::string set_name, size_t set_ind, int set_ii = 0): name(set_name), momindex(set_ind), internal_index(set_ii) {} 
        ParticleSchematic(MathList, size_t set_momindex);

        /**
         * Method that returns particle name
         */
        std::string get_name() const {return  name;}

        /**
         * Method that returns the momentum index assigned to the particle
         */
        size_t mom_index() const {return momindex;}

        int get_flavor() const {return flavor;}

        int get_internal_index() const {return internal_index;}
        int get_spinor_chain_index() const {return spinor_chain_index;}

        bool has_opposite_representation() const {return opposite_representation;}
};

// [BEGIN CutVertex]
enum class CutParticleType {INTERNAL, EXTERNAL};
typedef std::pair<CutParticleType,ParticleSchematic> CutParticle;

// Representation of a cut configuration
class CutVertex{
   public:
       std::vector< CutParticle > particles;
       std::vector< size_t > links;             /**< to which vertex link connects */
       std::vector< size_t > link_particle_positions;    /**< position in the list of particles (for easier location) */
       SelectionRule sr;

   public:
       CutVertex() = default;
       CutVertex( std::vector< CutParticle > new_particles);
       size_t count_internals() const; 
       size_t count_externals() const; 
       bool is_massive() const;
       bool is_multi_rung() const;
       std::vector<int> external_momentum_ids() const;

};

std::ostream& operator <<(std::ostream& out, const ParticleSchematic& v);
std::ostream& operator <<(std::ostream& out, const CutVertex& v);
// [/END CutVertex]

// [BEGIN Cut]

using LoopParticle = std::tuple<int,int,int,int>;
using LoopParticles = std::vector<LoopParticle>;
using Rung = std::vector<CutVertex>;

class Forest;

namespace detail{
    struct ForestPostitionsContainer{
        std::vector<int> forest_ids; /**< only gluons, Ds-independend */
        std::vector<LinearCombination<Constant,std::vector<int>>> forest_ids_ds; /** for Ds coefficients */
    };
}

template <size_t L>
class Cut{
    private:
        bool pure_imaginary{false};

        detail::ForestPostitionsContainer forest_positions; /**< An id obtained from the forest to reference the cut. Can be multiple for the partial tracing. */
        std::vector< CutVertex > vertices; /**< Store all vertices which form the S-tree of this */

        /**
         * A structure representing decomposition by particle content of the diagram.
         * Contains a vector of powers of (Ds-6) starting from (Ds-6)^1 (pure gluon contribution is excluded)
         *
         * Linear combination is of terms which contribute to a particular power coefficient.
         */
        std::vector<LinearCombination<Constant, std::vector<CutVertex>>> ds_decomposition;

	LoopParticles loop_particles; /**< Store which particles of the S-tree are loop particles as needed for add_cut in Forest */

    public:

        Cut() = default; 
        Cut(std::string schematic);
        Cut(std::vector<CutVertex> new_vertices, LoopParticles new_loop_particles);

        std::vector< const CutVertex* > get_linked_vertices(size_t focus_vertex) const ;
        size_t count_externals() const;

        std::vector<CutVertex>::const_iterator begin() const;
        std::vector<CutVertex>::const_iterator end() const;


        const LoopParticles& get_loop_particles() const;

	std::vector<Rung> get_rungs() const;

        const std::vector<CutVertex>& get_vertices() const {return vertices;}

        /**
         * Get position in a forest of one of the cuts
         * (method for compatibility for non-Ds-decomposed cuts)
         */
        int get_first_forest_position() const;

        decltype(ds_decomposition) get_ds_decomposition() const {return ds_decomposition;}

        /**
         * Evaluate topology using given Builder (loop momenta assumed to be set beforehand!)
         */
        template <typename T, size_t D> std::vector<T> evaluate(Builder<T, D>& builder,const bool& always_recompute=false) const;

        /**
         * A method to add *this to a given Forest
         *
         * @param Forest reference
         * @param A vector of particles representing the external process
         * @param What to do with traces of (D_s-4)-indices of fermions. By default take the whole trace
         */
        void add_to_forest(Forest&, const std::vector<Particle>&, settings::BG::trace_scheme = settings::BG::trace_scheme::full);

        /**
         * Same as above, but accepts a template parameter as a hint to what numerical type will be used to evaluate this cut.
         * The information is used to adjust some properties, such as what to do with the parial traces of external fermions.
         */
        template <typename T> void add_to_forest(Forest&, const std::vector<Particle>&);

        /**
         * Number of closed fermion (quark) loops
         */
        unsigned nf_powers{0};

        void set_pure_imaginary(bool s = true) {pure_imaginary = s;}

};


template <size_t L> std::ostream& operator <<(std::ostream& out, const Cut<L>& c);


// [BEGIN Cut Decompositions]

// Decompositions contain how the Cut-Topology is decomposed into cut
// diagrams. This is then templated because the representation of
// the cut may well change.

template <typename T>
class ConstantDecomposition{
    public:
        LinearCombination<T, Constant> decomposition;
        ConstantDecomposition() = default;
        ConstantDecomposition(std::string schematic): decomposition(schematic) {}
        ConstantDecomposition(LinearCombination<T, Constant> new_decomposition): decomposition(new_decomposition) {}
        T operator[](size_t index) const{ return decomposition[index].first;};
};


// [END Cut Decompositions]

// [BEGIN Topology]

// Topology declaration.
// We take a Decomposition container, because perhaps more than the cut
// info will need to enter.
template< template <class> class Decomposition, size_t L >
class Topology{
private:
    lGraph::xGraph graph;
    int multiplicity;
    bool related_cut;
    bool parameterizable;
    Decomposition<Cut<L>> decomposition;

    int symmetry_factor{1}; /**< Master coefficients need to be multiplied by this factor to get the amplitude correctly. */

    static constexpr size_t MAX_DS_COEFFICENTS = 3; /**< Maximum number of Ds coefficients (non-adjustable so far) */

public:
    Topology() = default;
    explicit Topology(std::string schematic);

    const lGraph::xGraph& get_graph() const {return graph;}
    int get_multiplicity() const;
    bool ask_related_cut() const;
    bool is_parameterizable() const;

    int get_symmetry_factor() const {return symmetry_factor;}

    /**
     * Evaluate topology using given Builder (loop momenta assumed to be set beforehand!)
     */
    template <typename T, size_t D> std::vector<T> evaluate(Builder<T, D>& builder,const bool& always_recompute=false) const;

    Decomposition<Cut<L>>& get_decomposition() {return decomposition;}
    const Decomposition<Cut<L>>& get_decomposition() const {return decomposition;}

    template <typename T, size_t D>
      std::vector<T> propagator_values(const momentumD_configuration<T,D>& cut_mom_conf, OnShellPoint<T,D> loop_point, bool report_info = false) const;

    template<typename T, size_t D>
      void assert_on_shell(const momentumD_configuration<T,D>& physical_mom_conf, OnShellPoint<T,D> loop_point, bool report_info = false) const;
};

/**
 * Typedef for most commonly used residue/topology type - where the
 * associated expression is a field-linear combination of products of
 * trees.
 */
template <size_t L>
using ColourOrderedResidue = Topology<ConstantDecomposition, L>; 

}

#include "Topology.hpp"

#endif //TOPOLOGY_H_INC
