/*
 * Topology.cpp 
 * First created on 1.12.2015
 *
 * Implementation of classes related to Topology
 *
*/

#include <algorithm>
#include <iostream>
#include <set>
#include <stdlib.h>
#include <tuple>

#include "Core/MathInterface.h"
#include "AmplitudeCoefficients/Topology.h"

#include "Core/Utilities.h"

#include "Core/Particle.h"

#include "partial_trace.h"
#include "Core/settings.h"

#include "Core/CaravelInput.h"

#define _DEBUG_EXTERNAL_PARTICLES_HACK

namespace Caravel {


// string used to identify quarks from process library
// what is quark or antiquark does not matter for now, because
// we choose the states to match external particles always
// NOTE: this does not change the color structures (so quark or antiquark is misleading in this sense)
static const std::string PL_QUARK_NAME = "\"Quark\"";
static const std::string PL_ANTI_QUARK_NAME = "\"AntiQuark\"";

ParticleSchematic::ParticleSchematic(MathList l, size_t set_ind): momindex(set_ind) {
    assert(l.head == std::string("Field"));

    switch(l.size()){
        case 2:{
                   MathList qNumbers(l[1],"QuantumNumbers");
                   for(auto it: qNumbers){
                       MathList qn(it);
                       if (qn.head == "Flavour"){
                           flavor = std::stoi(MathList(qn[0],"ID")[0]);
                       }
                       else if(qn.head == "QuarkLine"){
                           internal_index = std::stoi(MathList(qn[0],"ID")[0]);
                           ++internal_index; // do not start from 0
                       }
                       else if(qn.head == "ScalarLine"){
                           internal_index = std::stoi(MathList(qn[0],"ID")[0]);
                       }
                       else if(qn.head == "ScPowers"){
                            // just ignore
                       }
                       else if(qn.head == "GlobalQuarkLine"){
                            spinor_chain_index = std::stoi(MathList(qn[0],"ID")[0]);
                       }
                       else if(qn.head == "RepresentationFlip"){
                           opposite_representation = true;
                       }
                       else{
                           throw std::logic_error("Encountered an unknown particle property "+qn.head);
                       }
                   }
               }
               __FALLTHROUGH;
        case 1:
            name = l[0];
            break;
        default:
        throw std::logic_error("Do not understand Field format");
    }
}

// BEGIN[CutVertex]
CutVertex::CutVertex( std::vector< CutParticle >
           new_particles) : particles(new_particles) {}

size_t CutVertex::count_internals() const {
    size_t count = 0;
    for (auto& particle: particles) {
      if (particle.first == CutParticleType::INTERNAL){
	    count++;
      }
    }
    return count;
}

size_t CutVertex::count_externals() const {
    size_t count = 0;
    for (auto& particle: particles) {
      if (particle.first == CutParticleType::EXTERNAL){
	    count++;
      }
    }
    return count;
}

bool CutVertex::is_massive() const{
  return (count_externals()>1);
}

bool CutVertex::is_multi_rung() const{
  return (count_internals()>2);
}

std::vector<int> CutVertex::external_momentum_ids() const{
  std::vector<int> vertex_momentum_ids;

  for (auto& particle : particles){
    if (particle.first == CutParticleType::EXTERNAL){
      auto position = particle.second.mom_index();
      vertex_momentum_ids.push_back((int)position);
    }
  }

  return vertex_momentum_ids;
}
  

std::ostream& operator <<(std::ostream& out, const ParticleSchematic& v){
    out << v.get_name()<< "<";
    if(v.get_flavor() != 0){
         out << v.get_flavor();
    }
    if(v.get_internal_index()!=0){
        out << "i:" << v.get_internal_index();
    }
    if(v.get_spinor_chain_index() != 0) { out<<"sp:"<<v.get_spinor_chain_index(); }

    if (v.has_opposite_representation()) {
        out << "-F";
    }

    out << ">(" << v.mom_index() << ")";
    return out;
}

std::ostream& operator<<(std::ostream& out, const CutVertex& v) {
    bool first = true;
    unsigned linkcount = 0;
    for (auto p : v.particles) {
        if (!first) { out << "--"; }

        if (p.first == CutParticleType::EXTERNAL) {
            out << "External[" << p.second << "]";
        } else if (p.first == CutParticleType::INTERNAL) {
            out << "Internal[" << p.second << " -> " << v.links.at(linkcount) << "]";
            ++linkcount;
        }

        first = false;
    }
    if(v.is_multi_rung()){
        out << "  #I=" << v.count_internals();
    }
    if(!v.sr.is_empty()){
        out << "   SR: " << v.sr.get_qqs_coupling();
    }
    return out << "\n";
}

// END[CutVertex]

// BEGIN[Cut]
template<>
std::vector<Rung> Cut<1>::get_rungs() const{
  Rung single_rung = vertices;
  return {single_rung};
}

// TWO LOOP
// Returns list of rungs stored as:
//   {left, mid, right}
// The mid rung contains the multi-rung vertices and could be either:
// Butterfly: mid = {multi}
// Non-butterfly: mid = {start, ..., end} 
template<>
std::vector<Rung> Cut<2>::get_rungs() const{
    Rung current_rung;
    std::vector<Rung> all_rungs;
    
    size_t rung_index = 0; // 0 == left, 1 == mid, 2 == right
    for (auto& vertex : vertices){
        auto vertex_internals = vertex.count_internals();
        if (vertex_internals == 4) {
	    // Butterfly diagram.
	    all_rungs.push_back(current_rung);
	    current_rung.clear();

	    Rung butterfly_middle {vertex};
	    all_rungs.push_back(butterfly_middle);
	    rung_index +=2;
	}
	else if (vertex_internals == 3){
	  // The push_backs here achieve adding the multi rung vertices to the middle rung
	    if (rung_index == 1){
	      current_rung.push_back(vertex);
	    }
	    all_rungs.push_back(current_rung);
	    current_rung.clear();
	    rung_index++;
	    if (rung_index == 1){
	      current_rung.push_back(vertex);
	    }
	}
	else if (vertex_internals == 2){
	    current_rung.push_back(vertex);
	}
	else{
	  std::cout << "Found vertex with strange number of internal particles" << std::endl;
	  exit(1);
	}
    }

    all_rungs.push_back(current_rung); // Save final rung.

    // Reverse middle, as per convention.
    std::reverse(all_rungs[1].begin(), all_rungs[1].end());

    return all_rungs;
}

template <size_t L>
Cut<L>::Cut(std::vector<CutVertex> new_vertices, LoopParticles new_loop_particles) : 
    vertices(new_vertices), loop_particles(new_loop_particles) { 
        std::cout << "WARNING - Unchecked cut setup provided. One must manually "
            << "check that the vertices are provided in the canonical tree "
            << "order." << std::endl;
}


template <size_t L>
Cut<L>::Cut(std::string schematic) {

    MathList diag(schematic, "Diag");

    // read trees
    {
        // Read a vector of CutVertex from Trees[...]
        auto get_vertices = [](MathList trees) -> decltype(vertices) {
            assert(trees.head == "Trees");
            // Resize containers to appropriate length.
            const size_t trees_size = trees.tail.size();
            decltype(vertices) toret(trees.tail.size());

            toret.resize(trees_size);

            for (size_t vert_i = 0; vert_i < trees_size; vert_i++) {
                auto& vert = toret.at(vert_i);

                MathList tree_info(trees.tail.at(vert_i), "Vertex");

                // auto particle_types = tree_info.head;
                // const auto& particle_infos = tree_info.tail;

                for (size_t i = 0; i < tree_info.size(); i++) {

                    MathList p_info(tree_info[i]);

                    // read selection rule
                    {
                        MathList p(p_info[0], "Field");
                        if (p[0] == PL_ANTI_QUARK_NAME) {
                            MathList qNumbers(p[1], "QuantumNumbers");
                            int internal_index = -1;
                            std::vector<int> scpowers;
                            for (auto it : qNumbers) {
                                MathList qn(it);
                                if (qn.head == "QuarkLine") {
                                    internal_index = std::stoi(MathList(qn[0], "ID")[0]);
                                    ++internal_index; // do not start from 0
                                } else if (qn.head == "ScPowers") {
                                    for(auto& it : MathList(qn[0],"ID")){
                                        scpowers.push_back(std::stoi(it));
                                    }
                                }
                            }
                            if (!scpowers.empty()) {
                                assert(internal_index > -1);
                                vert.sr.add_qqs(internal_index, scpowers);
                            }
                        }
                    }

                    if (p_info.head == "ExternalLeg") {

                        auto p_id = -std::stoi(p_info[2]) / 2; // Map QGRAF to mom indices
                        vert.particles.emplace_back(CutParticleType::EXTERNAL, ParticleSchematic(MathList(p_info[0], "Field"), p_id));
                    } else if (p_info.head == "InternalLink") {

                        vert.particles.emplace_back(CutParticleType::INTERNAL, ParticleSchematic(MathList(p_info[0], "Field"), 0));

                        // Link the two vertices at the cut level.
                        vert.links.push_back(math_string_to_cpp_index(p_info[1]));
                        vert.link_particle_positions.push_back(vert.particles.size() - 1);
                    } else {
                        throw std::logic_error("Internal inconsistency detected while reading a cut content form a file!");
                    }
                }
            }
            return toret;
        };


        MathList trees(diag[0]);

        if(trees.head == "Trees"){
            vertices = get_vertices(trees);
        }
        else if(trees.head == "DsDecomposition"){
            {
                LinearCombination<Constant,MathList> read_decomposition(trees[0]);
                vertices = get_vertices(read_decomposition[0].second);
            }
            for(size_t i = 1; i < trees.size() ; ++i){
                LinearCombination<Constant,MathList> read_decomposition(trees[i]);
                ds_decomposition.push_back({});
                for(auto it: read_decomposition.elements){
                    auto vs = get_vertices(it.second);
                    //if (vs.front().particles.front().second.get_name() == "\"DsScalar\"" and vs.back().particles.back().second.get_name() == "\"DsScalar\"")
                        ds_decomposition.back().elements.emplace_back(it.first, vs);
                }
            }
        }

        // determine if we have closed quark loops (just but inspecting flavors,
        // not actually doing it)
        std::set<int> nf_flavors;
        for(auto& v: vertices){
            for(auto& p : v.particles){
                if(p.first == CutParticleType::INTERNAL){
                    int fl = p.second.get_spinor_chain_index();
                    if(fl > 100) {
                        nf_flavors.insert(fl);
                    }
                }
            }
        }

        DEBUG_PRINT(nf_flavors);

        nf_powers = nf_flavors.size();
    }


    // read loop particle locations
    {
        int mom_pos = count_externals();

        MathList loop_ps(diag[1], "LoopParticles");
        if (loop_ps.size() != L){ throw("Found " + std::to_string(loop_ps.size()) + " (not " + std::to_string(L) + ") loop particles!"); }

        int loop_num = 0;
        for (auto& pair_schematic : loop_ps){
            loop_num++;

            MathList loop_pair(pair_schematic, "LoopPair"); // Pair (Fields, Positions)
            MathList fields(loop_pair[0], "Fields");
            MathList positions(loop_pair[1], "Positions");

            assert(fields.size() == L);

            int mli_cut_pos, li_cut_pos;

            // Current conventions:
            // - Mathematica: Provided in order found in tree, -li after li
            // - Fernando: l1 is start, l2 is end, negatives are their partners.
            //   (This will not scale to higher loops).
            // Here we convert between the two conventions.

            if (loop_num == 1){
                mli_cut_pos = math_string_to_cpp_index(positions[1]);
                li_cut_pos = math_string_to_cpp_index(positions[0]);
            }
            else if (loop_num == 2){
                mli_cut_pos = math_string_to_cpp_index(positions[0]);
                li_cut_pos = math_string_to_cpp_index(positions[1]);
            }
            else{
                throw("Naming conventions for l"+std::to_string(loop_num)+" and (-l"+std::to_string(loop_num) +") not yet established!");
            }

            auto li_mom_pos = ++mom_pos;
            auto mli_mom_pos = ++mom_pos;

            loop_particles.emplace_back(mli_cut_pos, li_cut_pos, li_mom_pos, mli_mom_pos);

        }
    }

    // reset internal indices to default (0) if they are not necessary
    // Forest gets confused if they are used when not required
    for(auto& it: vertices){
        std::set<int> internal_indices;
        for (auto& p : it.particles) { if( p.second.get_internal_index() != 0) internal_indices.insert(p.second.get_internal_index()); }
        if ( internal_indices.size() < 2 ){
            for (auto& p : it.particles) { p.second.internal_index = 0; }
        }
    }

    auto is_fermion = [](const ParticleSchematic& p ){
        return (p.get_name() == PL_QUARK_NAME or p.get_name() == PL_ANTI_QUARK_NAME);
    };

    for (auto& lc : ds_decomposition) {
        for (auto& vertices : lc.elements) {
            for (auto& it : vertices.second) {
                std::set<int> internal_indices;
                for (auto& p : it.particles) {
                    if (is_fermion(p.second) && p.second.get_internal_index() != 0) internal_indices.insert(p.second.get_internal_index());
                }
                if (internal_indices.size() < 2) {
                    for (auto& p : it.particles) { if(is_fermion(p.second)) p.second.internal_index = 0; }
                    // put the index in the selection rule to zero also
                    if (!it.sr.is_empty()) {
                        assert(it.sr.qqs_coupling.size() == 1);
                        it.sr.qqs_coupling = decltype(it.sr.qqs_coupling){{0, it.sr.qqs_coupling.begin()->second}};
                    }
                }
            }
        }
    }

    // check that all quarks have spinor chain index
    for(auto& v: vertices){
        for(auto& p: v.particles){
            if(p.second.get_name()==PL_QUARK_NAME or p.second.get_name() == PL_ANTI_QUARK_NAME){
                if(p.second.spinor_chain_index == 0){
                    _WARNING_R("Found a (anti)quark with zero spinor chain index! This is should not happen!");
                    std::exit(1);
                }
            }
        }
    }

    DEBUG_MESSAGE("Contructed Cut: ",*this);
}

template <size_t L> void Cut<L>::add_to_forest(Forest& f, const std::vector<Particle>& process, settings::BG::trace_scheme use_trace_scheme) {

    std::vector<Particle> new_process = process;

    auto find_index_in_process = [&new_process](size_t index) {
        for(Particle& it: new_process){ if(it.mom_index() == index) return &it; }
        throw std::logic_error("Momentum index obtained from the process library does not match any index in the external process");
    };

    // save external indices to restore later from it
    std::vector<int> external_dtt_indices;
    for(auto& it: new_process){
        external_dtt_indices.push_back(it.external_state().get_dtt_index());
    }

    // find the list of all flavors of external particles
    static const std::map<int,ParticleFlavor> flavours_map = {
        {1,ParticleFlavor::u},
        {2,ParticleFlavor::d},
        {3,ParticleFlavor::s},
        {4,ParticleFlavor::c},
        {5,ParticleFlavor::b},
        {6,ParticleFlavor::t},
    };

    // because we use Weyl spinors for computation, sometimes we need to replace quark<-->antiquark,
    std::map<int,bool> flip_weyl_spinors;

    // Weyl spinor chains have to possible "directions" < .. ] and [..>,
    // where < and [ denote the representation of the spinor with dtt_index=1.
    // For each spionr chain we determine which one should we use. We then need to assign representation
    // for each particle in each vertex such that BG knows this correctly.
    // We do this by finding which spinor chains should change representation compared to the standard assignment (q <--> repr::La; qb <--> repr::Lat)

    // which spinor chains should be flipped
    std::set<int> flip_representation_container;

    auto update_flip_representation_container = [this,&flip_representation_container,find_index_in_process](){
        flip_representation_container.clear();
        for (const auto& it : vertices) {
            for (unsigned i = 1; i < it.particles.size() - 1; i++) {
                auto& pi = it.particles.at(i);
                if (pi.first == CutParticleType::EXTERNAL) {
                    auto ptarget = find_index_in_process(pi.second.mom_index());
                    int sindex = pi.second.get_spinor_chain_index();
                    ParticleFlavor matching_flavor = ptarget->get_flavor();

                    if (in_container<ParticleFlavor::q>(matching_flavor) && ptarget->external_state().get_representation() != repr::La) {
                        flip_representation_container.insert(sindex);
                    } else if (in_container<ParticleFlavor::qb>(matching_flavor) && ptarget->external_state().get_representation() != repr::Lat) {
                        flip_representation_container.insert(sindex);
                    }
                }
            }
        }

    };

    auto flip_representation = [&flip_representation_container](int i) -> bool {
        if(flip_representation_container.count(i)>0) return true;
        return false;
    };

    // find all quark pairs  {qb,q} per spionr chain index
    std::map<int,std::vector<Particle*>> all_quark_pairs;


    // create a table of how to assign flavor ids from process library to a physical external flavor
    {
        for (const auto& it : vertices) {
            for (unsigned i = 1; i < it.particles.size() - 1; i++) {
                auto& pi = it.particles.at(i);
                if (pi.first == CutParticleType::EXTERNAL) {
                    auto ptarget = find_index_in_process(pi.second.mom_index());
                    int sindex = pi.second.get_spinor_chain_index();

                    ParticleFlavor matching_flavor = ptarget->get_flavor();

                    if (in_container<ParticleFlavor::q>(matching_flavor)){
                        if (pi.second.get_name() == PL_QUARK_NAME) flip_weyl_spinors[sindex] = false;
                        else if (pi.second.get_name() == PL_ANTI_QUARK_NAME) flip_weyl_spinors[sindex] = true;
                        all_quark_pairs[sindex].push_back(ptarget);
                    }
                    else if (in_container<ParticleFlavor::qb>(matching_flavor)){
                        if (pi.second.get_name() == PL_QUARK_NAME) flip_weyl_spinors[sindex] = true;
                        else if (pi.second.get_name() == PL_ANTI_QUARK_NAME) flip_weyl_spinors[sindex] = false;
                        all_quark_pairs[sindex].push_back(ptarget);
                    }
                }
            }
        }

        DEBUG_PRINT(flip_representation_container);

        for(auto& s : all_quark_pairs){
            if(s.second.size() != 2){
                _WARNING_R("Expected pairs of quarks for each spinor chain in the external process! ",all_quark_pairs); 
                std::exit(1);
            }
            // sort such that we always have quarks first
            if(in_container<ParticleFlavor::qb>(s.second.front()->get_flavor())){
                std::swap(s.second.at(0),s.second.at(1));
            }
        }

        DEBUG_PRINT(all_quark_pairs);
    }



    DEBUG_PRINT(flip_weyl_spinors);

    // flip selection rules for chains which we need to flip
    for(auto s : flip_weyl_spinors){
        if(s.second){
            for(auto& lc: ds_decomposition){
                for(auto& el: lc.elements){
                    for(auto& v : el.second){
                        std::set<int> indices;
                        for(auto& p : v.particles){
                            if(p.second.get_spinor_chain_index() == s.first){
                                indices.insert(p.second.get_internal_index());
                            }
                        }

                        for(auto i_index : indices){
                            auto srule = v.sr.qqs_coupling.find(i_index);
                            if(srule != v.sr.qqs_coupling.end() && srule->second.size()>1){
                                std::reverse(srule->second.begin(),srule->second.end());
                            }
                        }
                    }
                }
            }
        }
    }

    // this is the main method which constructs actual Particles for ParticleSchematic,
    // a lot of conventions are encoded here
    auto get_particle_type_from_schematic = [&flip_weyl_spinors](const ParticleSchematic& s){
        if (s.get_name() == "\"Gluon\"") return ParticleType(ParticleStatistics::vector,ParticleFlavor::gluon,ParticleMass(),true);
        else if (s.get_name() == "\"Graviton\"") return ParticleType(ParticleStatistics::tensor, ParticleFlavor::G, ParticleMass(), false);

        else if (s.get_name() == "\"Photon\"") return ParticleType(ParticleStatistics::vector,ParticleFlavor::photon,ParticleMass(),false);

        else if (s.get_name() == "\"AntiNu\"") return ParticleType(ParticleStatistics::antifermion,ParticleFlavor::veb,ParticleMass(),false);
        else if (s.get_name() == "\"Nu\"") return ParticleType(ParticleStatistics::fermion,ParticleFlavor::ve,ParticleMass(),false);

        else if (s.get_name() == "\"Electron\"") return ParticleType(ParticleStatistics::fermion,ParticleFlavor::e,ParticleMass(),false);
        else if (s.get_name() == "\"AntiElectron\"") return ParticleType(ParticleStatistics::antifermion,ParticleFlavor::eb,ParticleMass(),false);

        else if (s.get_name() == "\"DsScalar\"") return ParticleType(ParticleStatistics::scalar,ParticleFlavor::ds_scalar,ParticleMass(),true);

        else if (s.get_name() == PL_QUARK_NAME) {
            ParticleType ret(ParticleStatistics::fermion,ParticleFlavor::undefined,ParticleMass(),true);
            switch (s.get_spinor_chain_index()) {
                case 101:
                case 103: ret.set_flavor(ParticleFlavor::q_nf1); break;
                case 102: ret.set_flavor(ParticleFlavor::q_nf2); break;
                case 104: ret.set_flavor(ParticleFlavor::q_nf1_y); break;
                case 105: ret.set_flavor(ParticleFlavor::q_nf2_y); break;
                default:
                    ret.set_flavor(flavours_map.at(s.get_flavor()));
                    if (flip_weyl_spinors.at(s.get_spinor_chain_index())) ret = ret.get_anti_type();
                    break;
            }
            return ret;
        }
        else if (s.get_name() == PL_ANTI_QUARK_NAME) {
            ParticleType ret(ParticleStatistics::antifermion,ParticleFlavor::undefined,ParticleMass(),true);
            switch (s.get_spinor_chain_index()) {
                case 101:
                case 103: ret.set_flavor(ParticleFlavor::qb_nf1); break;
                case 102: ret.set_flavor(ParticleFlavor::qb_nf2); break;
                case 104: ret.set_flavor(ParticleFlavor::qb_nf1_y); break;
                case 105: ret.set_flavor(ParticleFlavor::qb_nf2_y); break;
                default:
                    ret.set_flavor(anti_flavor(flavours_map.at(s.get_flavor())));
                    if (flip_weyl_spinors.at(s.get_spinor_chain_index())) ret = ret.get_anti_type();
                    break;
            }
            return ret;
        };

        throw std::logic_error("Did not recognize particle "+s.get_name()+"!");
    };

    // to remove all new particles created
    // later may be replace with a better identification mechanism for thread safety
    std::vector<Particle*> to_remove;

    auto construct_loop_particle = [&](const ParticleSchematic& in) {
        auto pfind = get_particle_type_from_schematic(in);
        auto p = new Particle("_INT_", pfind, SingleState("default"), 0, 0);
        p->set_internal_index(in.get_internal_index());

        auto fl = p->get_flavor();
        if(in_container<ParticleFlavor::q>(fl)){
            if(flip_representation(in.get_spinor_chain_index()) xor in.opposite_representation) p->set_dtt_index(repr::Lat);
            else p->set_dtt_index(repr::La);
        }
        if(in_container<ParticleFlavor::qb>(fl)){
            if(flip_representation(in.get_spinor_chain_index()) xor in.opposite_representation) p->set_dtt_index(repr::La);
            else p->set_dtt_index(repr::Lat);
        }

        to_remove.push_back(p);
        return p;
    };

    // defines a scheme for partial tracing. connected to flavor now, but should in principle
    // be connected to a *global* quark line index
    std::vector<partial_trace::embedding_type> partial_trace_scheme;

    bool handle_trace_automatically = true;
    {
        for (auto it : external_dtt_indices) {
            if (it != 0) {
                handle_trace_automatically = false;
                break;
            }
        }
        DEBUG_PRINT(handle_trace_automatically);

        int n_quarks = count_quarks(new_process);

        if (handle_trace_automatically && n_quarks > 0) {
            static const Caravel::partial_trace::reduced trace_reduced;
            static const Caravel::partial_trace::full trace_full;

            switch (use_trace_scheme) {
                // This can be used only for exact computations, but we don't know what numerical type will be used for computations,
                // so the responsibility is on users to set this properly
                case settings::BG::trace_scheme::reduced: partial_trace_scheme = trace_reduced.get(n_quarks, nf_powers); break;
                case settings::BG::trace_scheme::full: partial_trace_scheme = trace_full.get(n_quarks, nf_powers); break;
                default: throw std::runtime_error("Not implemented!");
            }
        }
        else{
            update_flip_representation_container();
        }
    }

    DEBUG_PRINT(partial_trace_scheme);
    DEBUG_PRINT(*this);

#ifdef _DEBUG_EXTERNAL_PARTICLES_HACK
    static thread_local std::vector<Particle*> different_external_particles;
#endif

    // function adds one cut to forest for specific set of vertives and a component of partial trace scheme
    auto add_cut = [&](const decltype(vertices)& vs, size_t i) -> int {
        // Construct cutTopology object
        cutTopology ctopology;

        for (const auto& it : vs) {

            assert(it.particles.size() >= 2);

            // We require left and right particles in the vertex to be internal
            assert(it.particles.front().first == CutParticleType::INTERNAL);
            assert(it.particles.back().first == CutParticleType::INTERNAL);

            std::vector<Particle*> to_insert;

            for (unsigned i = 1; i < it.particles.size() - 1; i++) {

                auto& pi = it.particles.at(i);

                if (pi.first == CutParticleType::EXTERNAL) {
                    // Find match external index
                    Particle* found = find_index_in_process(pi.second.mom_index());

                    // Check if the particle is known
                    auto pfind = get_particle_type_from_schematic(pi.second);
                    // Check if the particle matches the external process
                    if (pfind != found->get_type()) {
                        DEBUG_PRINT(pfind);
                        DEBUG_PRINT(found->get_type());
                        throw std::logic_error(std::string("Particle with momentum ") + std::to_string(i) + " did not match the one from external process!");
                    }

                    if (found->get_statistics() != ParticleStatistics::vector) { DEBUG_PRINT(pi.second.get_internal_index()); }
                    found->set_internal_index(pi.second.get_internal_index());

                    to_insert.push_back(found);
                } else {
                    // Note it is imporatant that the name is the same for all particles created
                    // For thread safety more elaborate scheme will be used
                    Particle* t = construct_loop_particle(pi.second);
                    to_insert.push_back(t);
                }
            }

            Particle* pL = construct_loop_particle(it.particles.front().second);
            Particle* pR = construct_loop_particle(it.particles.back().second);

// FIXME
// TERRIBLE HACK: new external particle each time to disable BG cache
#ifdef _DEBUG_EXTERNAL_PARTICLES_HACK
            for (auto& it : to_insert) {
                if (it->external_state().is_external()) {
                    auto fp = std::find_if(different_external_particles.begin(), different_external_particles.end(), [it](auto p) { return *it == *p; });
                    // if doesn't exist yet create a new copy
                    if (fp == different_external_particles.end()) {
                        different_external_particles.push_back(new Particle(*it));
                        it = different_external_particles.back();
                    } else {
                        (*fp)->set_internal_index(it->get_internal_index());
                        it = *fp;
                    }
                }
            }
#endif

            int n_coloured = std::count_if(to_insert.begin(), to_insert.end(), [](auto p) { return p->is_colored(); });
            if(pL->is_colored()) ++n_coloured;
            if(pR->is_colored()) ++n_coloured;

            int n_gs = (n_coloured>=2) ? (n_coloured-2) : 0;

            // TODO
            // Coupling information should be read off from process library probably in the future
            // For now we fill in only the QCD coupling assuming that all trees have only QCD and/or non-loop EW interactions
            
            //! FIXME this is a very hacky way to select gravity couplings
            if (n_coloured > 0) {
                ctopology.add_vertex(*pL, to_insert, *pR,
                                     {
                                         {"gs", n_gs},
                                         {"gw", static_cast<int>(it.particles.size()) - n_coloured},
                                     },
                                     it.sr);
            }
            else { // Gravitons
                ctopology.add_vertex(*pL, to_insert, *pR, {{"K", static_cast<int>(it.particles.size()) - 2}}, it.sr);
            }
        }

        ctopology.set_n_closed_light_fermion_loops(nf_powers);

        // set number of embeddings for correct normalization by builder
        if (!partial_trace_scheme.empty()) {
            ctopology.set_n_external_embeddings(partial_trace_scheme.size());
            ctopology.set_n_copies(partial_trace_scheme.at(i).second);
        }

        if(pure_imaginary) ctopology.set_pure_imaginary();

        //_MESSAGE("Constructed CutTopology:");
        //_MESSAGE(vs);

        int forest_position = f.add_cut(loop_particles, ctopology);

        if (forest_position == 0) throw std::runtime_error("Failed to add a cut to forest!");

        return forest_position;
    };

    size_t iN = 1;
    if (!partial_trace_scheme.empty()) iN = partial_trace_scheme.size();

    auto assign_new_internal_indices = [&update_flip_representation_container,&all_quark_pairs](const partial_trace::embedding_type& e){
        for(auto& s : e.first){
            all_quark_pairs.at(s.first).at(0) ->set_dtt_index(s.second);
            all_quark_pairs.at(s.first).at(1) ->set_dtt_index(s.second);
        }
        update_flip_representation_container();
    };

    // for each helicity component add gluon cuts
    for (size_t i = 0; i < iN; ++i) {
        if(!partial_trace_scheme.empty()){
            assign_new_internal_indices(partial_trace_scheme.at(i));
        }
        DEBUG_PRINT(new_process);
        DEBUG_PRINT(flip_representation_container);
        forest_positions.forest_ids.push_back(add_cut(vertices,i));
    }

#ifdef DS_BY_PARTICLE_CONTENT
    // for each helicity component add cuts for Ds decomposition
    for(const auto& lc : ds_decomposition){
        LinearCombination<Constant,std::vector<int>> to_add;
        for(const auto& c : lc.elements){
            std::vector<int> helicity_components;
            for (size_t i = 0; i < iN; ++i){
                if(!partial_trace_scheme.empty()){
                    assign_new_internal_indices(partial_trace_scheme.at(i));
                }
                helicity_components.push_back(add_cut(c.second,i));
            }
            to_add.elements.emplace_back(c.first,helicity_components);
        }
        forest_positions.forest_ids_ds.push_back(to_add);
    }
#endif

    // register all created cuts for joining
    std::vector<int> flattened_cutids = forest_positions.forest_ids;
    for(auto& it : forest_positions.forest_ids_ds){
        for(auto id : it){
            flattened_cutids.insert(flattened_cutids.end(),id.second.begin(),id.second.end());
        }
    }
    f.join_cuts(flattened_cutids);

    // Fernando says that it is ok to delete all created particles after adding to the Forest
    for(auto it: to_remove) delete it;

    //exit(1);
}

template <size_t L> int Cut<L>::get_first_forest_position() const {
    return forest_positions.forest_ids.front();
}


template <size_t L>
std::vector< const CutVertex* > Cut<L>::get_linked_vertices(size_t focus_vertex) const{
    std::vector< const CutVertex* > verts_linked_to_input;

    for (auto linked_index : vertices.at(focus_vertex).links){
        auto linked_vertex = &(vertices.at(linked_index));
        verts_linked_to_input.push_back(linked_vertex);
    }

    return verts_linked_to_input;
}

template <size_t L>
std::ostream& operator <<(std::ostream& out, const Cut<L>& c){
    using std::endl;    

    out << "Cut[\n";

    unsigned i = 0;
    for (auto v : c){
        out << "  " << i++ << ": ";
        out << v;
    }

    out << endl << "]\n";
    out << "DsDecomposition[\n" << c.get_ds_decomposition() << "]\n";

    return out;

}


template <size_t L>
std::vector<CutVertex>::const_iterator Cut<L>::begin() const{
    return vertices.begin();

}

template <size_t L>
std::vector<CutVertex>::const_iterator Cut<L>::end() const{
    return vertices.end();
}

template <size_t L>
size_t Cut<L>::count_externals() const{
    size_t count = 0;
    for (auto& vertex : vertices){
        count += vertex.count_externals();
    }
    return count;
}

// END[Cut]

namespace detail {
bool is_zero(const C& x) {
    if (abs(x) < 1e-4) { return true; }
    return false;
}

#ifdef HIGH_PRECISION
bool is_zero(const CHP& x) {
    if (abs(x) < 1e-20) { return true; }
    return false;
}
#endif

#ifdef VERY_HIGH_PRECISION
bool is_zero(const CVHP& x) {
    if (abs(x) < 1e-50) { return true; }
    return false;
}
#endif
} // namespace detail
}

namespace Caravel {
namespace partial_trace {

full::full(){
    for (unsigned nfi : {0, 1, 2}) {
        unsigned imax = 0;
        switch (nfi) {
            case 0: imax = 8; break;
            case 1: imax = 4; break;
            case 2: imax = 2; break;
            default: break;
        }
        for (unsigned i = 1; i <= imax; ++i) {
            for (unsigned j = 1; j <= imax; ++j) { container[{2,nfi}].push_back({{{1, i}, {2, j}}, 1}); }
        }
        for (unsigned i = 1; i <= imax; ++i) { container[{1,nfi}].push_back({{{1, i}}, 1}); }
    }
}

full::full(unsigned Ds){

    assert(Ds>=6 && Ds%2==0);

    for (unsigned nfi : {0, 1, 2}) {

        unsigned imax = ::std::pow(2,Ds/2-2);

        for (unsigned i = 1; i <= imax; ++i) {
            for (unsigned j = 1; j <= imax; ++j) { container[{2,nfi}].push_back({{{1, i}, {2, j}}, 1}); }
        }
        for (unsigned i = 1; i <= imax; ++i) { container[{1,nfi}].push_back({{{1, i}}, 1}); }
    }

}

reduced::reduced() {
    using std::declval;
    using maptype = decltype(declval<embedding_type>().first);

#ifndef DS_BY_PARTICLE_CONTENT

    // one line
    container[{1,0}].emplace_back(maptype{{1,1}},2);
    container[{1,0}].emplace_back(maptype{{1,3}},2);
    container[{1,0}].emplace_back(maptype{{1,5}},4);

    container[{1,1}].emplace_back(maptype{{1,1}},1);
    container[{1,1}].emplace_back(maptype{{1,2}},1);
    container[{1,1}].emplace_back(maptype{{1,3}},2);

    container[{1,2}].emplace_back(maptype{{1,1}},1);
    container[{1,2}].emplace_back(maptype{{1,2}},1);


    //two lines
    container[{2, 0}].emplace_back(maptype{{1, 1}, {2, 1}}, 2);
    container[{2, 0}].emplace_back(maptype{{1, 2}, {2, 1}}, 2);

    container[{2, 1}].emplace_back(maptype{{1, 1}, {2, 1}}, 1);
    container[{2, 1}].emplace_back(maptype{{1, 2}, {2, 2}}, 1);
    container[{2, 1}].emplace_back(maptype{{1, 2}, {2, 1}}, 1);
    container[{2, 1}].emplace_back(maptype{{1, 1}, {2, 2}}, 1);

    for (unsigned nfi : {0, 1}) {
        container[{2,nfi}].emplace_back(maptype{{1, 4}, {2, 1}}, 4);
        container[{2,nfi}].emplace_back(maptype{{1, 4}, {2, 2}}, 4);
        container[{2,nfi}].emplace_back(maptype{{1, 4}, {2, 3}}, 2);
        container[{2,nfi}].emplace_back(maptype{{1, 4}, {2, 4}}, 2);
    }

    container[{2,0}].emplace_back(maptype{{1, 8}, {2, 8}}, 4);
    container[{2,0}].emplace_back(maptype{{1, 8}, {2, 7}}, 4);
    container[{2,0}].emplace_back(maptype{{1, 8}, {2, 6}}, 12);
    container[{2,0}].emplace_back(maptype{{1, 8}, {2, 5}}, 12);
    container[{2,0}].emplace_back(maptype{{1, 8}, {2, 2}}, 8);
    container[{2,0}].emplace_back(maptype{{1, 8}, {2, 1}}, 8);


    container[{2, 2}].emplace_back(maptype{{1, 1}, {2, 1}}, 1);
    container[{2, 2}].emplace_back(maptype{{1, 2}, {2, 2}}, 1);
    container[{2, 2}].emplace_back(maptype{{1, 2}, {2, 1}}, 1);
    container[{2, 2}].emplace_back(maptype{{1, 1}, {2, 2}}, 1);

#else

    container[{1,0}].emplace_back(maptype{{1,1}},2);

    container[{1,1}].emplace_back(maptype{{1,1}},1);
    container[{1,1}].emplace_back(maptype{{1,2}},1);

    container[{1,2}].emplace_back(maptype{{1,1}},1);
    container[{1,2}].emplace_back(maptype{{1,2}},1);

    //two lines
    container[{2, 0}].emplace_back(maptype{{1, 1}, {2, 1}}, 2);
    container[{2, 0}].emplace_back(maptype{{1, 2}, {2, 1}}, 2);

    container[{2, 1}].emplace_back(maptype{{1, 1}, {2, 1}}, 1);
    container[{2, 1}].emplace_back(maptype{{1, 2}, {2, 2}}, 1);
    container[{2, 1}].emplace_back(maptype{{1, 2}, {2, 1}}, 1);
    container[{2, 1}].emplace_back(maptype{{1, 1}, {2, 2}}, 1);

    container[{2, 2}].emplace_back(maptype{{1, 1}, {2, 1}}, 1);
    container[{2, 2}].emplace_back(maptype{{1, 2}, {2, 2}}, 1);
    container[{2, 2}].emplace_back(maptype{{1, 2}, {2, 1}}, 1);
    container[{2, 2}].emplace_back(maptype{{1, 1}, {2, 2}}, 1);

#endif
}


}}


// INSTANTIATIONS
namespace Caravel {

template class Cut<1>;
template class Cut<2>;
template std::ostream& operator<<(std::ostream& out, const Cut<1>& c);
template std::ostream& operator<<(std::ostream& out, const Cut<2>& c);

}
