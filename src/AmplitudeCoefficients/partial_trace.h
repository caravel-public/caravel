#ifndef PARTIAL_TRACE_H
#define PARTIAL_TRACE_H

#include <tuple>
#include <map>
#include <vector>

namespace Caravel {
namespace partial_trace {

using flavor_id = int;
using embedding_index = int;
using factor = int;
using embedding_type = std::pair<std::map<flavor_id, embedding_index>, factor>;

class scheme {
  public:
    std::vector<embedding_type> get(unsigned n_q_pairs, unsigned nf_powers) const {return container.at({n_q_pairs,nf_powers});}

  protected:
    std::map<std::tuple<unsigned,unsigned>,std::vector<embedding_type>> container;
};

class full : public scheme {
  public:
    full(); // all for Ds=6,8,10
    full(unsigned Ds); // for given Ds
};

class reduced : public scheme {
  public:
    reduced();
};

}}

#endif
