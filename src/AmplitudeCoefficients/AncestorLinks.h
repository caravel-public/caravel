#ifndef ANCESTOR_LINKS_H_INC
#define ANCESTOR_LINKS_H_INC

#include <vector>
#include <string>
#include <iostream>
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/MathInterface.h"
#include "Core/typedefs.h"

namespace Caravel{

enum class Sign {POSITIVE, NEGATIVE};
class MomentumSum{
    public:
      std::vector<std::pair<Sign, int>> combination;
      MomentumSum() = default;
      MomentumSum(std::string schematic);
      bool operator==(const MomentumSum& other);
      template<typename T, size_t D> 
	momentumD<T,D> operator() (const momentumD_configuration<T,D>& physical_mom_conf,
				   const OnShellPoint<T,D>& loop_point) const;
};

template <typename T, size_t D>
momentumD<T, D> MomentumSum::operator()(const momentumD_configuration<T, D>& physical_mom_conf, const OnShellPoint<T, D>& loop_point) const {
    // li = n_point+2*i-1
    // mli = n_point+2*i
    momentumD<T, D> total_mom;

    // The external momenta are -= due to the outgoing convention
    for (const auto& mom : combination) {
        if (mom.first == Sign::NEGATIVE) {
            switch (mom.second) {
                case -1:
                    total_mom -= loop_point[0]; // ml1
                    break;
                case -2:
                    total_mom -= loop_point[1]; // ml2
                    break;
                default: total_mom += physical_mom_conf[mom.second]; // NEGATIVE
            }
        }
        else {
            switch (mom.second) {
                case -1:
                    total_mom += loop_point[0]; // l1
                    break;
                case -2:
                    total_mom += loop_point[1]; // l2
                    break;
                default: total_mom -= physical_mom_conf[mom.second]; // POSITIVE
            }
        }
    }

    return total_mom;
}

std::ostream& operator <<(std::ostream& out, const MomentumSum& mom_sum);

class ExtraPropagatorInfo{
  public:
      MomentumSum prop_mom;
      template<typename T, size_t D> 
	T operator() (const momentumD_configuration<T, D>& physical_mom_conf,
		      const OnShellPoint<T,D>& loop_point) const;

      bool operator == (const ExtraPropagatorInfo& other);
      ExtraPropagatorInfo() = default;
      ExtraPropagatorInfo(std::string schematic);
};

template<typename T, size_t D>
  T ExtraPropagatorInfo::operator()(const momentumD_configuration<T,D>& physical_mom_conf, const OnShellPoint<T,D>& loop_point) const{
  auto prop_mom_val = prop_mom(physical_mom_conf, loop_point);
  return prop_mom_val*prop_mom_val;
}

std::ostream& operator <<(std::ostream& out, const ExtraPropagatorInfo& prop);

template<size_t L>
class ReroutingFunction{
  public:
      std::array<MomentumSum, L> new_loop_mom_expressions;
      ReroutingFunction();
      ReroutingFunction(std::string schematic);
      template<typename T, size_t D> 
      OnShellPoint<T,D> operator() (const momentumD_configuration<T,D>& physical_mom_conf, const OnShellPoint<T,D>&) const;
};

template<size_t L>
ReroutingFunction<L>::ReroutingFunction(std::string schematic){
  MathList structure(schematic);
  for (size_t i = 0; i < L; i++){
    new_loop_mom_expressions[i] = MomentumSum(structure[i]);
  }
}

template<size_t L>
ReroutingFunction<L>::ReroutingFunction(){
  for (size_t i = 0; i < L; i++){
    new_loop_mom_expressions[i] = MomentumSum("SUM[PLOOP["+std::to_string(i+1) + "]]");
  }
}

template<size_t L>
template<typename T, size_t D>
  OnShellPoint<T,D> ReroutingFunction<L>::operator() (const momentumD_configuration<T,D>& physical_mom_conf, const OnShellPoint<T,D>& loop_point) const{
  OnShellPoint<T,D> rerouted_point;
  for (const auto& mom : new_loop_mom_expressions){
    rerouted_point.push_back(mom(physical_mom_conf, loop_point));
  }

  return rerouted_point;
}

template<size_t L>
std::ostream& operator <<(std::ostream& out, const ReroutingFunction<L>& rerouter){

  for (size_t l = 1; l <= L; l++){
    out << "l" << l << " = " << rerouter.new_loop_mom_expressions[l-1];
    if (l != L){ out << ", ";}
  }

  return out;
}

template<size_t L>
class LinkInfo{
  public:
    std::string my_schematic;
    std::vector<ExtraPropagatorInfo> extra_props;
    ReroutingFunction<L> ancestor_to_this;
    ReroutingFunction<L> this_to_ancestor;

    LinkInfo(std::string schematic);
    template<typename T, size_t D>
      T denominator(const momentumD_configuration<T,D>& physical_mom_conf, const OnShellPoint<T,D>& loop_point) const;
};

template<size_t L>
LinkInfo<L>::LinkInfo(std::string schematic){
  my_schematic = schematic;
  MathList link_info(schematic, "LinkInfo");
  ancestor_to_this = ReroutingFunction<L>(link_info[1]);
  this_to_ancestor = ReroutingFunction<L>(link_info[2]);

  MathList extra_props_info(link_info[0], "Propagators");
  for (auto& prop_info : extra_props_info){
    extra_props.push_back(ExtraPropagatorInfo(prop_info));
  }
}

template<size_t L>
template<typename T, size_t D>
  T LinkInfo<L>::denominator(const momentumD_configuration<T,D>& physical_mom_conf, const OnShellPoint<T,D>& loop_point) const{

    T denominator_val = T(1);

    for (const auto& prop : extra_props){
      denominator_val *= prop(physical_mom_conf, loop_point);
    }

    return denominator_val;
}


template<size_t L>
std::ostream& operator <<(std::ostream& out, const LinkInfo<L>& info){
  out << "LinkInfo[";
  for (auto& prop : info.extra_props){
    out << prop << ", ";
  }
  out << "AncestorToThis[";
  out << info.ancestor_to_this;
  out << "], ThisToAncestor[";
  out << info.this_to_ancestor;
  out << "]]";
  return out;
}



}

#endif //ANCESTOR_LINKS_H_INC
