#include "AmplitudeCoefficients/NumericalTools.h"

namespace Caravel{

    namespace detail{
	int convert_to_int(double x){return static_cast<int>(x);}
#ifdef HIGH_PRECISION
	int convert_to_int(dd_real x){return to_int(x);}
#endif
#ifdef VERY_HIGH_PRECISION
	int convert_to_int(qd_real x){return to_int(x);}
#endif

	R non_zero_zero_scale(C x){
	    return double(1e-9);
	}

#ifdef HIGH_PRECISION
	RHP non_zero_zero_scale(CHP x){
	    return RHP(1e-20);
	}
#endif

#ifdef VERY_HIGH_PRECISION
	RVHP non_zero_zero_scale(CVHP x){
	    return RVHP(1e-40);
	}
#endif

      // TODO: Use non_zero_zero_scale
#ifdef HIGH_PRECISION
	bool are_equal(const CHP& x, const CHP& y){
	    if (abs((x - y)/(x + y)) < 1e-20){
	    return true;
	    }

	    if (abs(x) < 1e-20 && abs(y) < 1e-20){
	    return true;
	    }

	    return false;
	}
#endif

      // TODO: Use non_zero_zero_scale
#ifdef VERY_HIGH_PRECISION
	bool are_equal(const CVHP& x, const CVHP& y){
	    if (abs((x - y)/(x + y)) < 1e-40){
	    return true;
	    }

	    if (abs(x) < 1e-40 && abs(y) < 1e-40){
	    return true;
	    }

	    return false;
	}
#endif



    } // namespace detail

#ifdef USE_FINITE_FIELDS
  std::vector<size_t> non_zero_positions(const std::vector<F32>& xs, const std::vector<size_t>& pos_list){

    using std::vector;
    std::vector<size_t> new_pos_list;
    for (size_t coeff_i = 0; coeff_i < xs.size(); coeff_i++){
      auto is_zero = xs.at(coeff_i) == F32(0);
      if (!is_zero){
        new_pos_list.push_back(pos_list.at(coeff_i));
      }
    }

    return new_pos_list;
  }

  std::vector<size_t> non_zero_positions(const std::vector<Vec<F32>>& xs, const std::vector<size_t>& pos_list){

    using std::vector;
    std::vector<size_t> new_pos_list;
    for (size_t coeff_i = 0; coeff_i < xs.size(); coeff_i++){
      auto is_zero = true;
      auto& x = xs.at(coeff_i);
      for (size_t i = 0; i < x.size(); i++){
        auto el = x[i];
        if (el != F32(0)){is_zero = false;}
      }

      if (!is_zero){
        new_pos_list.push_back(pos_list.at(coeff_i));
      }
    }

    return new_pos_list;
  }
#endif //USE_FINITE_FIELDS

} // namespace Caravel
