#include <set>
#include <map>
#include <vector>

namespace Caravel{

template < typename Node, typename Edge >
  HierarchySlice full_slice(const HierarchyGraph<Node, Edge>& hierarchy){
  HierarchySlice hierarchy_slice;
  for (size_t gen_i = 0; gen_i < hierarchy.nodes.size(); gen_i++){
    size_t gen = hierarchy.nodes.size() - 1 - gen_i;
    std::vector<HierarchyPos> generation_slice;
    for (size_t i = 0; i < hierarchy.nodes.at(gen).size(); i++){
      generation_slice.push_back({gen, i});
    }
    hierarchy_slice.push_back(generation_slice);
  }

  return hierarchy_slice;
}

    // construct a slice with a specified topology at the bottom
    template<typename Node, typename Edge>
    HierarchySlice with_ancestors_slice(const HierarchyGraph<Node,Edge>& hierarchy, HierarchyPos pos){
	auto ancestors = hierarchy.get_ancestors(pos);

	std::set<size_t,std::greater<size_t>> gens_set;
	std::multimap<size_t, size_t> ancestor_map;
	for (auto& ancestor : ancestors){
	    auto& pos = ancestor.first;
	    ancestor_map.insert(pos);
	    gens_set.insert(pos.first);
	}

	HierarchySlice slice;
	for (auto& gen : gens_set){
	    std::set<size_t> gen_slice;
	    auto begin_end = ancestor_map.equal_range(gen);
	    for (auto it = begin_end.first; it != begin_end.second; it++){
	    gen_slice.insert(it->second);
	    }
	    std::vector<HierarchyPos> gen_with_gen;
	    for (auto& el : gen_slice){
	    gen_with_gen.push_back({gen, el});
	    }
	    slice.push_back(gen_with_gen);
	}

	slice.push_back({pos});

	return slice;
    }

    // construct a slice with all childrens and their ancestors of a specified toplogy
    template<typename Node, typename Edge>
    HierarchySlice all_children_with_ancestors_slice(const HierarchyGraph<Node,Edge>& hierarchy, HierarchyPos pos){

	std::set<size_t,std::greater<size_t>> gens_set;
	std::multimap<size_t, size_t> ancestor_map;

        for(auto& it: hierarchy.get_child_positions(pos)){
            for (auto& ancestor : hierarchy.get_ancestors(it)){
                auto& pos = ancestor.first;
                ancestor_map.insert(pos);
                gens_set.insert(pos.first);
            }
        }

	HierarchySlice slice;
	for (auto& gen : gens_set){
	    std::set<size_t> gen_slice;
	    auto begin_end = ancestor_map.equal_range(gen);
	    for (auto it = begin_end.first; it != begin_end.second; it++){
	    gen_slice.insert(it->second);
	    }
	    std::vector<HierarchyPos> gen_with_gen;
	    for (auto& el : gen_slice){
	    gen_with_gen.push_back({gen, el});
	    }
	    slice.push_back(gen_with_gen);
	}

	slice.push_back(hierarchy.get_child_positions(pos));

	return slice;
    }

    template<typename Node, typename Edge>
    HierarchySlice all_down_to_level(const HierarchyGraph<Node,Edge>& hierarchy, size_t level){
        HierarchySlice hierarchy_slice;
        for (size_t gen_i = 0; gen_i < hierarchy.nodes.size(); gen_i++) {
            size_t gen = hierarchy.nodes.size() - 1 - gen_i;
            if(gen < level) break;
            std::vector<HierarchyPos> generation_slice;
            for (size_t i = 0; i < hierarchy.nodes.at(gen).size(); i++) { generation_slice.push_back({gen, i}); }
            hierarchy_slice.push_back(generation_slice);
        }

        return hierarchy_slice;
    }

    template<typename Node, typename Edge>
    HierarchySlice combined_all_ancestors(const HierarchyGraph<Node,Edge>& hierarchy,const std::vector<HierarchyPos>& positions){
        std::vector<HierarchySlice> all_slices;
	for(const HierarchyPos& pos:positions)
		// build all individual slices
		all_slices.push_back(with_ancestors_slice(hierarchy,pos));
        HierarchySlice hierarchy_slice;
	for(const HierarchySlice& lslice:all_slices)
		hierarchy_slice=combine_slices(hierarchy_slice,lslice);

        return hierarchy_slice;
    }

}
