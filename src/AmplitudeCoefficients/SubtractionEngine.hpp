#include "Core/Utilities.h"
#include "OnShellStrategies/OnShellStrategies.h"
#include "Core/typedefs.h"
#ifdef USE_FINITE_FIELDS
#include "Core/RationalReconstruction.h"
#endif
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "Core/qd_suppl.h"
#endif


#include "Core/Vec.h"
#include "FunctionalReconstruction/UnivariateReconstruction.h"

#include "Core/Timing.h"
#include "Core/enumerate.h"
#include "Core/type_traits_extra.h"

#include "misc/ParallelTools.h"

#include <thread>
#include <future>
#include <random>

namespace Caravel {

template <typename T, size_t D> std::enable_if_t<!is_exact<T>::value,OnShellMomentumParameterization<T, D>> default_parameterization(const lGraph::xGraph& graph, const lGraph::lGraphKin::GraphKin<T, D>& kinematics) {
    //return osm_parameterization(kinematics);
    return surd_parameterization(graph,kinematics);
}

template <typename T, size_t D> std::enable_if_t<is_exact<T>::value,OnShellMomentumParameterization<T, 6>> default_parameterization(const lGraph::xGraph& graph, const lGraph::lGraphKin::GraphKin<T, D>& kinematics) {
    static_assert(D>=5,"Not implemented for D<5");
    return surd_parameterization(graph,kinematics);
}

template <typename TF, typename T, size_t D>
HierarchyCoefficients<TF> build_empty_hierarchy_coefficients(const std::vector<std::vector<FunctionBasis<T, D>>>& hierarchy_function_bases) {
    size_t num_generations = hierarchy_function_bases.size();

    HierarchyCoefficients<TF> coefficients(num_generations);
    for (size_t gen_i = 0; gen_i < num_generations; gen_i++) {
        size_t gen = num_generations - 1 - gen_i;
        for (size_t i = 0; i < hierarchy_function_bases.at(gen).size(); i++) {
            std::vector<TF> empty_coeffs;
            coefficients.at(gen).push_back({false, empty_coeffs});
        }
    }

    return coefficients;
}

template <typename T, size_t D, typename Hierarchy, typename = decltype(std::declval<Hierarchy>().nodes)>
FunctionSpace::HierarchyFunctionBases<T,D> build_hierarchy_function_bases(const Hierarchy& hierarchy, const HierarchySlice& slice, const std::vector<lGraph::lGraphKin::GraphKin<T, D>>& topocache,
                                    const FunctionSpace::HierarchyBasisTruncation& truncation = {}) {
    size_t num_generations = hierarchy.nodes.size();

    // Build hierarchy basis functions
    FunctionSpace::HierarchyFunctionBases<T,D> function_bases(num_generations);

    for (size_t gen_i = 0; gen_i < num_generations; gen_i++) {

        size_t gen = num_generations - 1 - gen_i;
        size_t gen_size = hierarchy.nodes.at(gen).size();

        std::vector<FunctionSpace::FunctionBasis<T, D>> empty_gen(gen_size);
        function_bases.at(gen) = empty_gen;
    }

    size_t i = 0;
    for (auto& gen : slice) {
        for (auto& pos : gen) {
            auto topo_truncation = (truncation.size() == 0) ? std::vector<size_t>() : truncation.at(pos.first).at(pos.second);
            auto topology_basis_functions = FunctionSpace::build_topology_function_basis(hierarchy[pos].get_graph(),topocache.at(i), topo_truncation);
            function_bases.at(pos.first).at(pos.second) = topology_basis_functions;
            ++i;
        }
    }

    return function_bases;
}

namespace detail{
template <typename T> inline auto exact_polys_to_numeric(const std::vector<DensePolynomial<BigRat>>& exact_polynomials) {
    using TR = remove_complex_t<T>;
    std::vector<DensePolynomial<T>> numeric_polynomials;
    for (auto& polynomial : exact_polynomials) {
        std::vector<T> coefficients;
        for (auto& coefficient : polynomial.coefficients) { coefficients.push_back(TR(coefficient.num()) / TR(coefficient.den())); }
        numeric_polynomials.push_back(DensePolynomial<T>(coefficients, "ep"));
    }
    return numeric_polynomials;
}
}

template <typename TF, typename T, size_t D, size_t L>
void SubtractionEngine<TF, T, D, L>::build_hierarchy_function_bases_with_warmup() {
    *function_bases = build_hierarchy_function_bases<T, D>(*hierarchy, *slice, *hierarchy_kinematics_cache, std::get<0>(*warmup_info));
    basis_truncations = std::get<0>(*warmup_info);
    numerator_ranks = std::get<2>(*warmup_info);
    // Build the numerical denominators from the rational denominators.
    master_denominators = detail::exact_polys_to_numeric<T>(std::get<1>(*warmup_info));
}

template <typename TF, typename T, size_t D, size_t L>
SubtractionEngine<TF, T, D, L>::SubtractionEngine(std::shared_ptr<const Hierarchy> s_hierarchy, std::shared_ptr<const HierarchySlice> s_slice,
                                                  ResidueEvaluator<TF, T, D> residue_evaluator,
                                                  std::shared_ptr<const std::vector<MasterPosition>> s_master_positions,
                                                  std::shared_ptr<HierarchyWarmupInfo> s_warmup_info, bool set_verbose)
    : hierarchy(s_hierarchy), slice(s_slice), master_positions(s_master_positions), warmup_info(s_warmup_info),
      residue_change_point(residue_evaluator.get_adjuster()), verbose(set_verbose) {

    assert(hierarchy);
    assert(slice);
    assert(warmup_info);
    assert(master_positions);

    hierarchy_kinematics_cache->reserve(size(*slice));
    os_generators.reserve(size(*slice));

    // seed the random generator
    {
        std::array<int, decltype(rng)::element_type::state_size> seed_data;
        std::generate(seed_data.begin(),seed_data.end(),rand);
        std::seed_seq seq(seed_data.begin(),seed_data.end());
        rng = std::make_shared<typename decltype(rng)::element_type>(seq);
    }

    // construct the cached kinematic structure for all topologies
    for (auto& gen_slice : *slice) {
        for (auto& pos : gen_slice) {
            using lGraph::xGraph;
            using namespace FunctionSpace;

            auto& graph = (*hierarchy)[pos].get_graph();
            hierarchy_kinematics_cache->emplace_back(graph);

            if (settings::general::numerator_decomposition == settings::general::IntegralBasis::master_surface) {

                const SurfaceTermData& surfacedata = surface_term_data_holder.at(graph.get_topology());
                auto matrix_coefficients_evaluator = std::get<PCECoeffsFP<T, D>>(surfacedata);
                auto ibp_vector_matrix_coefficients_evaluator = std::get<IBPvecCoeffsFP<T, D>>(surfacedata);

                GraphKin& kinematics = hierarchy_kinematics_cache->back();
                const xGraph& IBPcandidate_xGraph = std::get<xGraph>(surfacedata);
                xGraph copy_relabeled = graph.get_relabeled_copy(IBPcandidate_xGraph);
                auto conventions_map_ext_surface = lGraph::lGraphKin::external_momentum_mapper<T, D>(copy_relabeled, IBPcandidate_xGraph);

                if (ibp_vector_matrix_coefficients_evaluator) {
                    kinematics.compute_ibp_vector_coefficients = [ibp_vector_matrix_coefficients_evaluator,
                                                                  conventions_map_ext_surface](const momD_conf<T, D>& mc) {
                        auto matrix_coeffs = ibp_vector_matrix_coefficients_evaluator(conventions_map_ext_surface(mc));
                        std::vector<std::vector<T>> result_coeffs;
                        result_coeffs.reserve(matrix_coeffs.size());
                        for (auto&& it : matrix_coeffs) {
                            auto sol = plu_solver(std::move(it.first), std::move(it.second));
                            // the last coefficient is always one by construction
                            sol.push_back(T(1));
                            result_coeffs.push_back(std::move(sol));
                        }
                        return result_coeffs;
                    };

                }
                if (matrix_coefficients_evaluator) {
                    kinematics.compute_surface_terms_coefficients =
                        [matrix_coefficients_evaluator, conventions_map_ext_surface](const momD_conf<T, D>& mc, const std::vector<std::vector<T>>& c) {
                            auto matrix_coeffs = matrix_coefficients_evaluator(conventions_map_ext_surface(mc), c);
                            std::vector<std::vector<T>> result_coeffs;
                            result_coeffs.reserve(matrix_coeffs.size());
                            for (auto&& it : matrix_coeffs) {
                                auto sol = plu_solver(std::move(it.first), std::move(it.second));
                                // the last coefficient is always one by construction
                                sol.push_back(T(1));
                                result_coeffs.push_back(std::move(sol));
                            }
                            return result_coeffs;
                        };
                }
            }
        }
    }

    {
        size_t i = 0;
        for (auto& gen_slice : *slice) {
            for (auto& pos : gen_slice) {
                auto& graph = (*hierarchy)[pos].get_graph();
                os_generators.emplace_back(random_natural_point_generation(graph,(*hierarchy_kinematics_cache)[i]));
                ++i;
            }
        }
    }

    function_bases = std::make_shared<typename decltype(function_bases)::element_type>();

    if (!warmup_info or std::get<2>(*warmup_info).size() == 0) {
        // Empty warmup
        *function_bases = build_hierarchy_function_bases<T,D>(*hierarchy, *slice,*hierarchy_kinematics_cache);
        basis_truncations = empty_hierarchy_basis_truncation(*hierarchy);
    } else {
        // Provided warmup
        build_hierarchy_function_bases_with_warmup();
    }

    build_hierarchy_subtracted_residues(residue_evaluator);
}

template <typename TF, typename T, size_t D, size_t L>
void SubtractionEngine<TF, T, D, L>::assign_rng_state(const typename decltype(rng)::element_type& newstate) {
    static std::mutex m;
    std::lock_guard<std::mutex> lock(m);
    *rng = newstate;
}

// TODO
// this is a huge function which does a lot of things at evaluation, it doesn't use cached kinematics everywhere
template <typename TF, typename T, size_t D, size_t L>
std::vector<std::vector<TF>> SubtractionEngine<TF, T, D, L>::coefficient_worker(const momentumD_configuration<T, D>& phys_mom_conf,
                                                                                const std::vector<T>& d_values, bool attempt_truncation, size_t container_dimension) {
    // Build empty coefficients for each value of d.
    std::vector<HierarchyCoefficients<TF>> coefficients(d_values.size(), build_empty_hierarchy_coefficients<TF>(*function_bases));

    if (verbose && attempt_truncation) { _MESSAGE("Attempting truncation..."); }

    // construct flattened index of the slice
    std::unordered_map<HierarchyPos,int> index_pos;
    {
        size_t i = 0;
        for (auto& gen : *slice) {
            for (auto& t : gen) { index_pos[t] = i++; }
        }
    }

    size_t gen_i = 0;
    for (auto& gen_slice : *slice) {
        if (!gen_slice.empty()) { 
            if (verbose) {
                std::cout << std::endl
                          << ("Gen " + std::to_string(gen_slice.front().first) + " (" + std::to_string(gen_slice.size()) + " numerators): ") << std::endl;
            }
        }

        // we make a deep copy of the global rng generator here
        auto fit_topo = [&,this](HierarchyPos topo_pos) -> void {
            auto rng_local = std::make_shared<typename decltype(rng)::element_type>(*(this->rng));
            auto& subtracted_residue = hierarchy_subtracted_residues.at(topo_pos.first).at(topo_pos.second);

            std::vector<Ansatz<T, D>> ansaetze;
            for (auto& d : d_values) {
                auto ansatz = construct_ansatz(topo_pos, subtracted_residue.second, phys_mom_conf, d);
                ansaetze.push_back(ansatz);
            }

            // This is the function that finally has to be upgradedinto a "better" subtracted_residue.
            std::function<std::vector<TF>(OnShellPoint<T, D>)> d_vals_subtracted_residues = [&d_values, &subtracted_residue, &coefficients,
                                                                                             &phys_mom_conf, container_dimension = container_dimension](OnShellPoint<T, D> p) {
                return subtracted_residue.first(phys_mom_conf, p, d_values, coefficients, container_dimension);
            };

            auto fixed_d_subtracted_residues = to_vector_of_functions(d_vals_subtracted_residues, d_values.size());
            auto gen = os_generators.at(index_pos.at(topo_pos))(rng_local);

            auto rng_saved_state = *rng_local;

            for (size_t d_i = 0; d_i < d_values.size(); d_i++) {
                auto& active_coefficients = coefficients.at(d_i);
                auto& ansatz = ansaetze.at(d_i);

                // std::cout << "active coefficients" << active_coefficients << std::endl;

                // NOTE: this is extremely important here to reset the state to make
                // sure that the same loop momenta are generated for each d value
                // and we can recycle the cut values from memoization
                *rng_local = rng_saved_state;

                std::vector<std::vector<TF>> coeffs;
                try {
                    coeffs = fit_ansatz(ansatz, fixed_d_subtracted_residues.at(d_i), gen, attempt_truncation && (d_i == 0), container_dimension);
                } catch (const n_eq_n_exception& e) {
                    std::cout << "ERROR: " << e.what() << std::endl;
                    /* The following is for debugging in case the subtraction fails (rational instead of polynomial function) */
                    // Current diagram
                    auto& graph = (*hierarchy)[topo_pos].get_graph();
                    // Build the univariate random sampler 'x_generator'
                    auto x_gen_rng = std::make_shared<typename decltype(rng)::element_type>(*(this->rng));
                    std::function<T()> x_gen = [&, &kinematics = (*hierarchy_kinematics_cache).at(index_pos.at(topo_pos))]() {
                            auto random_source = [&x_gen_rng, scale = natural_scale(kinematics)]() { return random_scaled_number(scale, *x_gen_rng); };
                            return random_source();
                    };
                    // Build the (vector-valued) univariate function 'f' which needs to be reconstructed
                    std::function<TF(T)> f = [&, &kinematics = (*hierarchy_kinematics_cache).at(index_pos.at(topo_pos))](T x) {
                        *rng_local = rng_saved_state;
                        auto on_shell_generator = random_natural_point_generation(graph, kinematics);
                        auto seeded_on_shell_generator = on_shell_generator(rng_local);
                        return fixed_d_subtracted_residues.at(d_i)(seeded_on_shell_generator(x));
                    };

                    _PRINT(topo_pos);
                    _PRINT(graph);

                    auto ancestors = hierarchy->get_ancestors(topo_pos);
                    _MESSAGE("Ancestors: ");
                    {
                        int link_index = 0;
                        for(auto& [pos, an] : ancestors) {
                            _MESSAGE(link_index++," : ", pos, an);
                        }
                    }

                    // Build the vector of propagators to match uncancelled denominator factors
                    std::function<std::vector<T>(T)> props = [&, &kinematics = (*hierarchy_kinematics_cache).at(index_pos.at(topo_pos))](T x) {
                        *rng_local = rng_saved_state;
                        auto on_shell_generator = random_natural_point_generation(graph, kinematics);
                        auto seeded_on_shell_generator = on_shell_generator(rng_local);
                        auto ls = seeded_on_shell_generator(x);
                        auto ps = phys_mom_conf;

                        std::vector<T> prop2s;
                        for(auto& [pos, an]: ancestors) {
                            prop2s.push_back(an.denominator(ps,ls));
                        }
                        return prop2s;
                    };

                    auto analytical_function = reconstruct_vector_univariate_function(f, x_gen);
                    auto selected_propgators = reconstruct_vector_univariate_function(props, x_gen);

                    _PRINT(analytical_function);

                    {
                        int link_index = 0;
                        for(auto& prop : selected_propgators) {
                            auto num0 = prop.numerator.coefficients.at(0);
                            if(num0 != T{}){
                                prop.numerator /= num0;
                                //prop.denominator /= num0;
                            }
                            _MESSAGE("Inv. den ",link_index++,": ", prop.numerator);
                        }
                    }

                    //_PRINT(selected_propgators);

                    throw n_eq_n_exception();
                }
                catch (const DivisionByZeroException& e){
                    _WARNING_R("Cought exception while fitting ", topo_pos,  "\n", e.what());
                    _WARNING((*hierarchy)[topo_pos].get_graph());
                    throw e;
                };

                // Add all newly computed functions.
                for (size_t coeff_set_i = 0; coeff_set_i < coeffs.size(); coeff_set_i++) {
                    auto& new_pos = ansatz.associated_positions.at(coeff_set_i);
                    auto& new_coeff_set = coeffs.at(coeff_set_i);
                    auto& new_numerator = active_coefficients.at(new_pos.first).at(new_pos.second);
                    new_numerator.first = true;
                    new_numerator.second = new_coeff_set;
                }
            }
            if (attempt_truncation) {
                for (auto& new_pos : ansaetze.front().associated_positions) {
                    truncate_basis(coefficients, new_pos, 0, (*hierarchy)[new_pos].get_graph(), hierarchy_kinematics_cache->at(index_pos.at(new_pos)), container_dimension);
                }
            }

            // advance the global rng gen, in a multithreaded setup this just means that the last thread to finish will advance it in the end
            assign_rng_state(*rng_local);
        };

        // Fit and store off shell functions
        if (parallel_scheme.empty()) {
            for (auto& topo_pos : gen_slice) {
                if (verbose) {
                    std::cout << topo_pos.second << " " << std::flush;
                }
                if (!(*hierarchy)[topo_pos].ask_related_cut()) continue;
                fit_topo(topo_pos);
            }
        }
        else {
            for (auto& bunch : parallel_scheme.at(gen_i)) {
                std::vector<std::future<void>> futures;
                if (verbose) {
                    for (auto& topo_pos : bunch) {
                        std::cout << topo_pos.second << " ";
                    }
                    std::cout << std::endl;
                }
                for (auto& topo_pos : bunch) {
                    if (!(*hierarchy)[topo_pos].ask_related_cut()) continue;
                    futures.push_back(std::async(std::launch::async, fit_topo, topo_pos));
                }
                for (auto& it : futures) it.wait();
            }
        }

        ++gen_i;
    }

    if (verbose) std::cout << std::endl;

    // FIXME
    // Horrible hack!
    // We don't know anything about D_s here, but we have to insert zeroes for truncated coefficients.
    // For now we just assume that all coefficients have the same number of D_s values (zero or not),
    // so we just find the first coefficient to get their dimensions
    int Ds_dim = 0;

    if constexpr (std::is_same_v<TF, Vec<T>>) {
        for (const auto& it1 : coefficients) {
            for (const auto& it2 : it1) {
                for (const auto& it3 : it2) {
                    if (!it3.second.empty()) {
                        Ds_dim = it3.second[0].size();
                        goto done;
                    }
                }
            }
        }
    done:;
    }

    // Extract the master coefficients from the now computed full hierarchy coefficients.
    std::vector<std::vector<TF>> master_coefficient_samples;
    for (auto& d_coeffs : coefficients) { master_coefficient_samples.push_back(extract_master_coeffs(d_coeffs,Ds_dim)); }

    return master_coefficient_samples;
}

template <typename TF, typename T, size_t D, size_t L> template <typename TT>
std::enable_if_t<!is_exact<TT>::value,std::vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>>> SubtractionEngine<TF, T, D, L>::reduce_without_warmup(const momentumD_configuration<T, D>&,bool,size_t) {
    throw std::logic_error("Cannot reduce without warmup with floating point");
}

template <typename TF, typename T, size_t D, size_t L> template <typename TT>
std::enable_if_t<is_exact<TT>::value,std::vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>>> SubtractionEngine<TF, T, D, L>::reduce_without_warmup(const momentumD_configuration<T, D>& mc, 
                                                                                                                                                           bool trunc_option, size_t container_dimension) {
    using std::function;
    using std::vector;
    std::mt19937 mt;

    constexpr const char* epname = "ep";

    size_t Nep = settings::IntegrandHierarchy::n_cached_D_values_warmup;
    
    // multiple evaluations are only nescessary if the coefficients have a d-dependence
    if(settings::general::numerator_decomposition == settings::general::IntegralBasis::tensors){
        Nep=1;
    }

    function<T()> ep_gen = [mt]() mutable { return T(mt()); };
    auto ep_gen_copy = ep_gen;

    std::unordered_map<T, std::vector<TF>> table;

    if(trunc_option){
        auto first_d = T(4) - T(2) * ep_gen_copy();
        table[first_d] = coefficient_worker(mc,{first_d}, true, container_dimension).at(0);
    }

    // After the first, we do the thirty evaluations in a cached way to speed up warmups.
    std::vector<T> next_thirty_d;
    for (size_t i = 0; i < Nep; i++) { next_thirty_d.push_back(T(4) - T(2) * T(ep_gen_copy())); }


    auto pre_computed_coeffs = coefficient_worker(mc,next_thirty_d, !trunc_option, container_dimension);

    for (size_t i = 0; i < Nep; i++) { table[next_thirty_d.at(i)] = pre_computed_coeffs.at(i); }

    function<vector<TF>(T)> cached_f = [&mc,this,table = std::move(table), container_dimension = container_dimension](T ep) {
        auto cached = table.find(T(4) - T(2) * ep);
        if (cached != table.end()) {
            return cached->second;
        } else {
            return coefficient_worker(mc,{T(4) - T(2) * ep}, false, container_dimension).at(0);
        }
    };

    vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>> result_functions;
    if(settings::general::numerator_decomposition == settings::general::IntegralBasis::tensors){
        // for d-independent cases we take the single pre-computed evaluation
        for(const auto& master: pre_computed_coeffs[0])
            result_functions.push_back( Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>(
                             DensePolynomial<TF, T>({master}, epname), DensePolynomial<T>({ T(1) }, epname) ));
    }
    else
        // for non-trivial d-dependence we make a reconstruction
        result_functions = reconstruct_vector_univariate_function(cached_f, ep_gen, epname);

    // Set remaining the warmup info
    for (auto& func : result_functions) {
        auto& num = func.numerator;
        auto& den = func.denominator;

        if constexpr (std::is_same_v<TF,Vec<T>>) {
            // first unpack into multiple DensePolynomial per each Ds
            size_t vecdim = num.coefficients.front().size();
            std::vector<DensePolynomial<T>> vec_numerators(vecdim);
            for (size_t i = 0; i < vecdim; ++i) {
                vec_numerators.at(i).var_name = epname;
                vec_numerators.at(i).coefficients.resize(num.coefficients.size());
                for (size_t j = 0; j < num.coefficients.size(); ++j) {
                    vec_numerators.at(i).coefficients.at(j) = num.coefficients.at(j)[i];
                }
                vec_numerators[i] = DensePolynomial<T>(vec_numerators[i].coefficients, epname);
            }

            // compute their common GCD
            vec_numerators.push_back(den);
            auto gcd = polynomial_GCD(vec_numerators);

            if (!gcd.is_one()) { 
                DEBUG_PRINT(gcd);
                den = polynomial_divide(den, gcd).first; 
                vec_numerators.pop_back();

                for(auto& pn : vec_numerators){
                    pn = polynomial_divide(pn,gcd).first;
                }

                // pack pack into DensePolynomial of Vec coefficients
                std::vector<TF> num_coefficients_vec(num.coefficients.size(),vecdim);

                for (size_t i = 0; i < vecdim; ++i) {
                    for (size_t j = 0; j < vec_numerators.at(i).coefficients.size(); ++j) {
                        num_coefficients_vec.at(j)[i] = vec_numerators.at(i).coefficients.at(j);
                    }
                }

                num = DensePolynomial<TF,T>(num_coefficients_vec, epname);
            }
        }
        else {
            auto gcd = polynomial_GCD(num,den);
            if (!gcd.is_one()) {
                DEBUG_PRINT(gcd);
                num = polynomial_divide(num,gcd).first;
                den = polynomial_divide(den,gcd).first;
            }
        }

        numerator_ranks.push_back(num.coefficients.size() - 1);
        master_denominators.push_back(den);
    }

    return result_functions;
}

template <typename TF, typename T, size_t D, size_t L>
std::vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>> SubtractionEngine<TF, T, D, L>::reduce_with_warmup(const momentumD_configuration<T, D>& mc, size_t container_dimension) {
    using std::vector;
    using std::function;

    auto max_rank = *std::max_element(numerator_ranks.begin(), numerator_ranks.end());

    vector<T> ep_vals;
    function<T()> ep_gen = [](){return random_scaled_number<T>(T(1));};
    
    for (int i = 0; i <= max_rank; i++) { ep_vals.push_back(ep_gen()); }

    vector<T> d_vals;
    for (auto& ep : ep_vals) { d_vals.push_back(T(4) - T(2) * ep); }

    // Do the hard work
    auto master_coeffs = coefficient_worker(mc,d_vals, false, container_dimension);

    // Multiply by denominator
    for (size_t i = 0; i < master_coeffs.at(0).size(); i++) {
        for (size_t j = 0; j < ep_vals.size(); j++) { master_coeffs.at(j).at(i) *= master_denominators.at(i)(ep_vals.at(j)); }
    }

    // Convert samples values to epsilon polynomial coefficients.
    vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>> master_coeff_functions;
    for (size_t i = 0; i < master_denominators.size(); i++) {
        size_t num_rank = numerator_ranks.at(i);

        vector<TF> relevant_vals;
        for (size_t j = 0; j <= num_rank; j++) { relevant_vals.push_back(master_coeffs.at(j).at(i)); }

        vector<T> relevant_eps(ep_vals.begin(), ep_vals.begin() + num_rank + 1);
        DensePolynomial<TF, T> master_numerator(relevant_vals, relevant_eps, "ep");

        Ratio<DensePolynomial<TF, T>, DensePolynomial<T>> coeff_function(master_numerator, master_denominators.at(i));
        master_coeff_functions.push_back(coeff_function);
    }

    return master_coeff_functions;
}

template <typename TF, typename T, size_t D, size_t L>
template <typename TT>
std::enable_if_t<is_finite_field<TT>::value> SubtractionEngine<TF, T, D, L>::update_cardinality() {
    if (last_cardinality != cardinality_update_manager<T>::get_instance()->get_current_cardinality()) {
        master_denominators = detail::exact_polys_to_numeric<T>(std::get<1>(*warmup_info));
    }
}

template <typename TF, typename T, size_t D, size_t L> std::vector<Ratio<DensePolynomial<TF, T>, DensePolynomial<T>>> SubtractionEngine<TF, T, D, L>::reduce(const momentumD_configuration<T, 4>& mc, bool trunc_option, size_t container_dimension) {
     auto casted_mc = static_cast<momentumD_configuration<T,D>>(mc);

     update_cardinality();

     // FIXME, see comments on the method itseld for why is this here
     EpsilonBasis<T>::_make_sure_squares_are_initialized();

     residue_change_point(mc);
     
     for (auto& it : *hierarchy_kinematics_cache) { it.change_point(casted_mc); }

     if (master_denominators.size() > 0) { return reduce_with_warmup(casted_mc, container_dimension); }
     else {
         auto result = reduce_without_warmup(casted_mc, trunc_option, container_dimension);
         auto wi = get_warmup_info();
         *warmup_info = get_warmup_info();
         // The warmup info is now there, so save it. Note that writing to a file and initiating rebuilding bases is
         // handled externally.
         return result;
    }
}

template <typename TF, typename T, size_t D, size_t L> template <typename TTT> std::enable_if_t<!is_exact<TTT>::value,HierarchyWarmupInfo> SubtractionEngine<TF, T, D, L>::get_warmup_info() {
    throw std::logic_error("Warmup can be obtained only with finite fields!");
}

template <typename TF, typename T, size_t D, size_t L> template <typename TTT> std::enable_if_t<is_exact<TTT>::value,HierarchyWarmupInfo> SubtractionEngine<TF, T, D, L>::get_warmup_info() {
#ifdef USE_FINITE_FIELDS
    // These are the only epsilon factors which should be encountered in reduction to UT integrals
    // We are going to try to factor these out before doing a rational guess on the remainder. 
    // The goal is to not have to rationally reconstruct the coefficients in denominator which can get quite large in the expanded form.
    std::array possible_ep_factors = {
        DensePolynomial<BigRat>{BigRat(0),BigRat(1)}, 
        DensePolynomial<BigRat>{BigRat(1),BigRat(1)}, 
        DensePolynomial<BigRat>{BigRat(1),BigRat(-1)}, 
        DensePolynomial<BigRat>{BigRat(1),BigRat(-2)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-3)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-4)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-3,2)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-2,3)},
        //below added for gravity
        DensePolynomial<BigRat>{BigRat(1),BigRat(-1,2)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-1,3)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-4,3)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-3,4)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-2,5)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-3,5)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-4,5)},
        DensePolynomial<BigRat>{BigRat(1),BigRat(-2,7)},
    };
    for(auto& pi : possible_ep_factors) pi.var_name = "ep";

    std::vector<DensePolynomial<BigRat>> exact_denominators;

    auto rational_guess_checked = [](T& x) {
        auto guess = RationalReconstruction::rational_guess(x);
        
        if(abs(guess.num()) > 2000 or abs(guess.den()) > 30) {
            _WARNING("WARNING: epsilon-dependent denominators were determined to include suspiciously large rational numbers ", guess);
            _WARNING("This likely means that denominators are kinematically-dependent or something else went wrong, take care!");
        }
        return guess;
    };

    // Returns maximal power of fac which divides poly
    // modifies poly in place such that it doesn't contain this factor
    auto check_factor = [](DensePolynomial<T>& poly, const DensePolynomial<BigRat>& fac) {
        int exponent = 0;
        auto fac_degree = fac.get_degree();

        if (poly.get_degree() < fac_degree) return 0;

        DensePolynomial<T> fac_F(fac);
        do {
            auto [q, r] = polynomial_divide(poly, fac_F);
            if (!r.is_zero()) return exponent;
            ++exponent;
            poly = q;
        } while (poly.get_degree() >= fac_degree);

        return exponent;
    };


    for (auto denom : master_denominators) {

        auto denom_copy = denom;

        std::unordered_map<DensePolynomial<BigRat>, int> factors;

        for(auto pf : possible_ep_factors){
            factors[pf] = check_factor(denom, pf);
        }

        std::vector<BigRat> left_denom_coeffs;

        for (auto& coeff : denom.coefficients) { left_denom_coeffs.push_back(rational_guess_checked(coeff)); }

        DensePolynomial<BigRat> denom_rat{left_denom_coeffs,"ep"};

        if(denom_rat.get_degree() > 0){
            _WARNING_R("Encountered an unexpected irreducible factor ", denom_rat, " in ep-dependent denominator during warmup.");
        }

        DEBUG_PRINT(denom_rat);

        for (auto& [pf, ex] : factors) {
            for (int i = 0; i < ex; ++i) { denom_rat *= pf; }
        }

        // consistency check
        if (denom_copy != static_cast<DensePolynomial<T>>(denom_rat)) {
            _WARNING_R(denom_copy, " != ", static_cast<DensePolynomial<T>>(denom_rat));
            throw std::runtime_error("Inconsistency during warmup!");
        }

        exact_denominators.push_back(denom_rat);
    }

    return std::make_tuple(basis_truncations, exact_denominators, numerator_ranks);
#else
    _WARNING_R("'finite-fields' was not enabled! Consider doing 'meson configure -Dfinite-fields=true'");
    std::exit(1);
#endif
}

template <typename TF, typename T, size_t D, size_t L>
Ansatz<T, D> SubtractionEngine<TF, T, D, L>::construct_ansatz(HierarchyPos topo_pos,
                                                              const std::vector<std::pair<HierarchyPos, LinkInfo<L>>>& unsubtracted_ancestors,
                                                              const momentumD_configuration<T, D>& physical_mom_conf, T d) {

    Ansatz<T, D> ansatz;

    auto specialize_dimension = [d](const FunctionBasis<T, D>& basis) {
        std::function<std::vector<T>(const OnShellPoint<T, D>&)> specialized_dim = [&basis, d](const OnShellPoint<T, D>& p) { return basis(p, d); };
        return specialized_dim;
    };

    // Add given topology basis to the ansatz
    auto& topology_basis_functions = function_bases->at(topo_pos.first).at(topo_pos.second);
    ansatz.add_topology(specialize_dimension(topology_basis_functions), topology_basis_functions.size(), topo_pos);

    // Some ancestors are repeated, but with different routings.
    std::map<HierarchyPos, std::vector<LinkInfo<L>>> unsubtracted_by_pos;
    for (auto& unsubtracted_ancestor : unsubtracted_ancestors) { unsubtracted_by_pos[unsubtracted_ancestor.first].push_back(unsubtracted_ancestor.second); }

    // Add necessary ancestors to the ansatz.
    for (auto& unsubtracted_ancestor_group : unsubtracted_by_pos) {
        auto& ancestor_pos = unsubtracted_ancestor_group.first;
        auto& ancestor_links = unsubtracted_ancestor_group.second;

        auto& ancestor_basis_functions_d = function_bases->at(ancestor_pos.first).at(ancestor_pos.second);
        auto ancestor_basis_functions = specialize_dimension(ancestor_basis_functions_d);

        // Repeated copies of the same ancestor will have the same coefficients, so we combine their basis functions in the ansatz.

        std::function<std::vector<T>(OnShellPoint<T, D>)> ancestor_basis_functions_with_denominator = [ancestor_links, ancestor_basis_functions,
                                                                                             &physical_mom_conf](OnShellPoint<T, D> os_point) {

            std::vector<std::vector<T>> link_fs;

            for (auto& ancestor_link : ancestor_links) {
                // Build and evaluate each
                auto ancestor_point = ancestor_link.this_to_ancestor(physical_mom_conf, os_point);
                auto fs = ancestor_basis_functions(ancestor_point);

                T denominator_val = ancestor_link.denominator(physical_mom_conf, os_point);
                for (auto& f : fs) { f = f / denominator_val; }

                link_fs.push_back(fs);
            }

            // Vector sum the link_fs into fs.
            auto add_vectors = [](std::vector<T> v1, const std::vector<T>& v2) {
                for (size_t i = 0; i < v1.size(); i++) { v1.at(i) += v2.at(i); }
                return v1;
            };

            std::vector<T> fs = link_fs.at(0);
            for (size_t i = 1; i < link_fs.size(); i++) { fs = add_vectors(fs, link_fs.at(i)); }

            return fs;
        };

        ansatz.add_topology(ancestor_basis_functions_with_denominator, ancestor_basis_functions_d.size(), ancestor_pos);
    }

    return ansatz;
}

template <typename TF, typename T, size_t D, size_t L>
std::vector<TF> SubtractionEngine<TF, T, D, L>::extract_master_coeffs(const HierarchyCoefficients<TF>& coeffs, const int Ds_dim) {
    std::vector<TF> master_coeffs_d;
    // Extra master coefficients from full set of coefficients.
    for (auto& master_pos : *master_positions) {

        auto hierarchy_pos = master_pos.first;
        auto& associated_truncation = basis_truncations.at(hierarchy_pos.first).at(hierarchy_pos.second);
        auto& topo_coeffs = coeffs.at(hierarchy_pos.first).at(hierarchy_pos.second).second;

        size_t actual_position = master_pos.second;
        bool was_truncated = false;
        for (auto& el : associated_truncation) {
            if (el == actual_position) { was_truncated = true; }
            if (el < actual_position) { actual_position--; }
        }

        if (was_truncated) {
            // Ds_dim = 0 if the coefficients do not have multiple Ds entries, and does zero initialization in this case
            // (yes, it's a bit weird, feel free to fix that)
            master_coeffs_d.emplace_back(Ds_dim);
        }
        else {
            // FIXME
            // this is a hack until this whole function becomes obsolete
            if (actual_position >= topo_coeffs.size()) {
                _WARNING_R("Tried to access a non-existent master coefficient!");
                std::exit(1);
            }
            else {
                master_coeffs_d.push_back(topo_coeffs[actual_position]);
            }
        }
    }

    return master_coeffs_d;
}

template <typename TF, typename T, size_t D, size_t L>
void SubtractionEngine<TF, T, D, L>::truncate_basis(std::vector<HierarchyCoefficients<TF>>& coefficients, HierarchyPos topo_pos, size_t d_i,const lGraph::xGraph& gr, 
                                                    const lGraph::lGraphKin::GraphKin<T, D>& kinematics, size_t container_dimension) {

    std::vector<size_t> zero_positions_a;

    for(auto&& [index, obj] : enumerate(coefficients)){
        auto& numerator_coefficients = obj.at(topo_pos.first).at(topo_pos.second).second;

        std::vector<size_t> zero_positions;
        for (int i = numerator_coefficients.size() - 1; i >= 0; i--) {
                                                // we convert to int to avoid problem with QD types
            if (numerator_coefficients.at(i) == TF(static_cast<int>(container_dimension))) {
                zero_positions.push_back(i);
                numerator_coefficients.erase(numerator_coefficients.begin() + i);
            }
        }

        if( index == d_i) zero_positions_a = zero_positions;
    }

    if (zero_positions_a.size() > 0) {
        auto& function_basis = function_bases->at(topo_pos.first).at(topo_pos.second);
        function_basis = FunctionSpace::build_topology_function_basis(gr,kinematics, zero_positions_a);
        basis_truncations.at(topo_pos.first).at(topo_pos.second) = zero_positions_a;
    }
}

template <typename A, typename... B> std::vector<std::function<A(B...)>> to_vector_of_functions(const std::function<std::vector<A>(B...)>& vec_f, const size_t n) {
    auto memoized_vector_f = memoize(vec_f);
    auto shared_memoized_vector_f = std::make_shared<std::function<std::vector<A>(B...)>>(memoized_vector_f);

    std::vector<std::function<A(B...)>> vector_of_functions;
    for (size_t i = 0; i < n; i++) {
        std::function<A(B...)> component_function = [shared_memoized_vector_f, i](B... b) { return (*shared_memoized_vector_f)(b...).at(i); };
        vector_of_functions.push_back(component_function);
    }

    return vector_of_functions;
}

template <typename TF, typename T, size_t D, size_t L> template <typename FType>
std::vector<std::vector<TF>> SubtractionEngine<TF, T, D, L>::fit_ansatz(const Ansatz<T, D>& ansatz, const FType& f,
                                                                        const std::function<OnShellPoint<T, D>()>& point_gen, bool attempt_truncation, size_t container_dimension) {
    return fit_functions(f, point_gen, ansatz.basis_functions, ansatz.basis_dimensions, attempt_truncation, container_dimension);
}

template <typename TF, typename T, size_t D, size_t L>
std::vector<TF> SubtractedResidue<TF, T, D, L>::operator()(const momentumD_configuration<T,D>& phys_momD_conf, const OnShellPoint<T, D>& p, std::vector<T> d_values,
                                                                      const std::vector<HierarchyCoefficients<TF>>& all_coefficients, size_t container_dimension) const {

    START_TIMER_T(subtractions,T);

                                                 // we convert to int to avoid problem with QD types
    std::vector<TF> all_results(d_values.size(), TF(static_cast<int>(container_dimension)));

    for (auto& ancestor : subtracted_ancestors) {
        auto ancestor_pos = ancestor.first;
        const auto& ancestor_basis = bases[ancestor_pos.first][ancestor_pos.second];
        auto& ancestor_link = ancestor.second;

        T ancestor_denominator = ancestor_link.denominator(phys_momD_conf, p);

        auto point_on_ancestor_space = ancestor_link.this_to_ancestor(phys_momD_conf, p);

        std::vector<std::reference_wrapper<const std::vector<TF>>> ancestor_coefficients;
        for (size_t i = 0; i < d_values.size(); i++) {
            auto& coeffs = all_coefficients[i][ancestor_pos.first][ancestor_pos.second].second;

            ancestor_coefficients.push_back(std::ref(coeffs));
        }

        auto ancestor_numerators = FunctionSpace::evaluate_function_in_basis<TF, T, D>(ancestor_coefficients, point_on_ancestor_space, d_values, ancestor_basis, container_dimension);

        for (size_t i = 0; i < d_values.size(); i++) {
            auto individual_ancestor_contribution = ancestor_numerators[i] / ancestor_denominator;

            all_results[i] -= individual_ancestor_contribution;

// Check if NaN
#ifdef DEBUG_ON
            if (individual_ancestor_contribution != individual_ancestor_contribution) {
                std::cerr << "ERROR: individual_ancestor_contribution is NaN" << std::endl;
                std::cerr << "Belongs to the following topology: " << std::endl;
                std::cerr << "ancestor_numerator: " << ancestor_numerators[i] << std::endl;
                std::cerr << "ancestor_denominator: " << ancestor_denominator << std::endl;
                exit(1);
            }
#endif
        }
    }
    STOP_TIMER(subtractions);

    TF cut_contribution = residue_evaluator(top_pos, phys_momD_conf, p);

#ifdef DEBUG_ON
    if (cut_contribution != cut_contribution) {
        std::cerr << "ERROR: cut_contribution is NaN" << std::endl;
        std::cerr << "phys_momD_conf = " << phys_momD_conf << std::endl;
        std::cerr << "os point = " << p << std::endl;
        exit(1);
    }
#endif

    for (auto& result : all_results) { result += cut_contribution; }

    return all_results;
}

template <typename TF, typename T, size_t D, size_t L, typename Hierarchy>
OnShellFunctionPrescription<TF, T, D, L>
build_subtracted_residue(const std::vector<std::vector<FunctionBasis<T, D>>>& bases, ResidueEvaluator<TF, T, D> residue_evaluator, Hierarchy& hierarchy, HierarchyPos top_pos, const HierarchySlice& slice) {

    auto& topology = hierarchy[top_pos];
    auto ancestors = hierarchy.get_ancestors(top_pos);

    std::vector<std::pair<HierarchyPos, LinkInfo<L>>> subtracted_ancestors;
    std::vector<std::pair<HierarchyPos, LinkInfo<L>>> unsubtracted_ancestors;

    if (!topology.ask_related_cut() || !topology.is_parameterizable()) { return std::make_pair(nullptr, unsubtracted_ancestors); }

    for (auto& ancestor : ancestors) {
        if(!in_slice(slice,ancestor.first)) continue;
        auto ancestor_pos = ancestor.first;
        auto ancestor_link = ancestor.second;

        bool numerator_function_exists = hierarchy[ancestor_pos].ask_related_cut();

        if (numerator_function_exists || (ancestor_pos.first - top_pos.first) > 1) {
            subtracted_ancestors.push_back({ancestor_pos, ancestor_link});
        } else {
            unsubtracted_ancestors.push_back({ancestor_pos, ancestor_link});
        }
    }

    return {SubtractedResidue<TF, T, D, L>(top_pos, subtracted_ancestors, bases, residue_evaluator), unsubtracted_ancestors};
}

template <typename TF, typename T, size_t D, size_t L>
void SubtractionEngine<TF, T, D, L>::build_hierarchy_subtracted_residues(ResidueEvaluator<TF, T, D> residue_evaluator) {

    auto full_hierarchy = full_slice(*hierarchy);

    HierarchySubtractedResidues<TF, T, D, L> new_hierarchy_subtracted_residues(full_hierarchy.size());
    for (auto& gen_slice : full_hierarchy) {
        if(!gen_slice.empty()){
            auto gen = gen_slice.at(0).first;
            std::vector<OnShellFunctionPrescription<TF, T, D, L>> gen_subtracted_residues(gen_slice.size());
            new_hierarchy_subtracted_residues.at(gen) = gen_subtracted_residues;
        }
    }

    for (auto& gen_slice : *slice) {
        for (auto& topo_pos : gen_slice) {
            auto on_shell_f_infoD = build_subtracted_residue<TF, T, D, L>(*function_bases, residue_evaluator, *hierarchy, topo_pos, *slice);
            new_hierarchy_subtracted_residues.at(topo_pos.first).at(topo_pos.second) = on_shell_f_infoD;
        }
    }

    hierarchy_subtracted_residues = new_hierarchy_subtracted_residues;
}

template <class Hierarchy> std::vector<MasterPosition> slice_master_positions(const Hierarchy& h, const HierarchySlice& slice) {

    // Find where the masters are
    std::vector<std::pair<HierarchyPos, size_t>> master_positions;
    for (auto& gen_slice : slice) {
        for (auto& topo_pos : gen_slice) {
            auto& graph = h[topo_pos].get_graph().get_topology();

            size_t num_masters = 0;

            using namespace settings::general;
       
            if (numerator_decomposition == IntegralBasis::tensors) {
                // //! This is fixed for now otherwise construction takes forever. 
                // //! The proper fix would be to disentangle the construction of graphs and the construction of the master insertions
                // num_masters=2;
                // #if 0 
                //TODO: this number should really be the number of ISP monomials
                if (!h[topo_pos].ask_related_cut())
                    num_masters = 0;
                else
                    num_masters = FunctionSpace::tensor_insertions(h[topo_pos].get_graph()).size();
                //#endif
            }
            else{
                auto f = FunctionSpace::surface_term_data_holder.find(graph);
                if (f == FunctionSpace::surface_term_data_holder.end()){
                    _WARNING_R("Could not find master count for ");
                    graph.show();
                    std::exit(1);
                }
                else{
                    num_masters =  std::get<size_t>(f->second);
                }
            }


            // For each found master, add a master to the list, with an "id" assuming all masters are at the beginning
            for (size_t i = 0; i < num_masters; i++) { master_positions.push_back({topo_pos, i}); }
        }
    }

    return master_positions;
}

// Instantiations
#define _INSTANTIATE_SUBTRACTION_ENGINE(W,T) \
W template class SubtractionEngine<Vec<T>, T, 6, 2>;\
W template class SubtractionEngine<T, T, 6, 2>;\

_INSTANTIATE_SUBTRACTION_ENGINE(extern,C)
#ifdef HIGH_PRECISION
_INSTANTIATE_SUBTRACTION_ENGINE(extern,CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_SUBTRACTION_ENGINE(extern,CVHP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_SUBTRACTION_ENGINE(extern,F32)
#endif
}
