#include <map>
#include <random>
#include <exception>
#include "Core/typedefs.h"
#include "Core/Vec.h"
#include "FunctionSpace/FunctionSpace.h"

namespace Caravel{

template <typename T> std::enable_if_t<is_exact<T>::value && !is_complex<T>::value,T> random_scaled_number(const T& scale){ return T(rand()); }
template <typename T> std::enable_if_t<!is_exact<T>::value && !is_complex<T>::value,T> random_scaled_number(const T& scale){ return scale*(T(2)*(T (rand()) / T(RAND_MAX))-T(1)); }

template <typename T> std::enable_if_t<is_exact<T>::value && !is_complex<T>::value,T> random_scaled_number(const T& scale, std::mt19937& mt){ return T(mt()); }
template <typename T> std::enable_if_t<!is_exact<T>::value && !is_complex<T>::value,T> random_scaled_number(const T& scale, std::mt19937& mt){ return scale*(T(2)*(T((int)mt()) / T(RAND_MAX))-T(1)); }

template <typename T> std::enable_if_t<is_complex<T>::value,T> random_scaled_number(const T& scale, std::mt19937& mt){
    using TR = typename T::value_type;
    return scale*T(random_scaled_number(TR(1),mt),random_scaled_number(TR(1),mt)); 
}
template <typename T> std::enable_if_t<is_complex<T>::value,T> random_scaled_number(const T& scale){
    using TR = typename T::value_type;
    return scale*T(random_scaled_number(TR(1)),random_scaled_number(TR(1))); 
}


template<typename T>  
T kahan_sum(const std::vector<T>& xs){
    long i;
    T correction{0}, corrected_next_term, new_sum;

    long ARR_SIZE = xs.size();
    if (ARR_SIZE == 0){return T(0);}

    T sum = xs[0];
    for (i = 1; i < ARR_SIZE; i++)
      {
	corrected_next_term = xs[i] - correction;
	new_sum = sum + corrected_next_term;
	correction = (new_sum - sum) - corrected_next_term;
	sum = new_sum;
      }
    return sum;
}

template<typename T>
T naive_sum_vector(const std::vector<T>& xs){
  T result(0);
  for (auto& x : xs){ result += x;}
  return result;
}

template<typename T>
T sum_vector(const std::vector<T>& xs){
  //return naive_sum_vector(xs);
  return kahan_sum(xs);
}

template< typename T, size_t D>
momentumD<T,D> project_into_space(momentumD<T,D> vec, std::vector<momentumD<T,D>> basis){
  momentumD<T,D> result;
  for (auto& basis_vec : basis){
    auto coeff = (vec*basis_vec);
    result += basis_vec * coeff;
  }
  return result;
}

template< typename T, size_t D>
momentumD<T,D> project_into_complement_space(momentumD<T,D> vec, std::vector<momentumD<T,D>> basis){
  return vec - project_into_space(vec, basis);
}

template< typename T, size_t D>
momentumD<T,D> massless_physical_part(momentumD<T,D> vec, momentumD<T,D> massless){
  auto parity_flip = parity_conjugate(massless);
  return (massless*vec)/(massless*parity_flip)*parity_flip;
}

template< typename T, size_t D>
momentumD<T,D> vector_4d_part(momentumD<T,D> vec){
  T vals[D];
  for (size_t i = 0; i < 4; i++){
    vals[i] = vec.pi(i);
  }
  for (size_t i = 4; i < D; i++){
      vals[i] = T(0);
  }

  momentumD<T,D> four_d_part(vals);
  return four_d_part;
}

template<typename T, size_t D>    
momentumD<T,D> parity_conjugate(momentumD<T,D> vec){
    T values[D];
    values[0] = -vec.pi(0);
    for (size_t i = 1; i < D; i++){
      values[i] = vec.pi(i);
    }
    
    momentumD<T,D> parity_flip(values);
    return parity_flip;
}

template<typename T, size_t D>
T euclidean_norm(momentumD<T,D> p ){
    T e_norm2 = T(0);
    for (size_t i = 0; i < D; i++){
        e_norm2 += p.pi(i);
    }
    return std::sqrt(e_norm2);
}

template<typename T, size_t D, size_t L, template <class> class Decomposition>
int count_off_shellness(Topology<Decomposition,L> topology, momentumD_configuration<T,D> physical_conf, OnShellPoint<T,D> p){
      // Single propagator off shell check.
      auto prop_vals = topology.propagator_values(physical_conf, p);
      int non_zero_count = 0;
      for (auto& prop_val : prop_vals){
	if (std::abs(prop_val) > (1e-6)){
	  non_zero_count++;
	}
      }

      return non_zero_count;
}



  /*
template<typename T, typename X>
FitFunction<T,X>::FitFunction(std::vector<T> coefficients, std::function<std::vector<T>(X)> basis_functions) : coefficients(coefficients), basis_functions(basis_functions){
}
  */

  /*
template<typename T, typename X>
T FitFunction<T,X>::operator() (X x) const{
  auto basis_vals = basis_functions(x);

  #ifdef DEBUG_ON
  if (basis_vals.size() != coefficients.size()){
    std::cerr << "Error in FitFunction. Coefficient array is not the same size as basis function array." << std::endl;
    exit(1);
  }
  #endif 

  return linear_combine(coefficients, basis_vals);
}

template<typename T, typename X>
std::ostream& operator <<(std::ostream& out, const FitFunction<T,X>& f){
  out << "FitFunction[";
  for (size_t i = 0; i < f.coefficients.size(); i++){
    if (f.coefficients.at(i) != T(0)){
        out << i << " -> " << f.coefficients.at(i) << ", ";
    }
  }
  out << "]" << std::endl;

  return out;
}
  */

struct n_eq_n_exception: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Function equality test failed.";
  }
};

template<typename F, typename X>
std::enable_if_t<is_exact<F>::value> check_function_equality(std::function<F(X)> f, std::function<F(X)> g, std::function<X()> xgen, std::function<void(int, size_t, size_t, size_t)> result_reporter = nullptr){
  size_t num_tests = 2; // We test very few points with exact kinematics - if it's wrong it should be obvious.
   for (size_t i = 0; i < num_tests; i++){
       auto x_val = xgen();
       auto f_val = f(x_val);
       auto g_val = g(x_val);

       auto is_equal = f_val == g_val;
       if (!is_equal){
	 std::cerr << "ERROR: Function equality test failed (not vectorised)." << std::endl;
	 std::cerr << "f_val = " << f_val << std::endl;
	 std::cerr << "g_val = " << g_val << std::endl;
   exit(1);
       }
   }
}

template<typename F, typename X>
std::enable_if_t<is_exact<F>::value> check_function_equality(std::function<Vec<F>(X)> f, std::function<Vec<F>(X)> g, std::function<X()> xgen, std::function<void(int, size_t, size_t, size_t)> result_reporter = nullptr){
  size_t num_tests = 2; // We test very few points with exact kinematics - if it's wrong it should be obvious.
   for (size_t i = 0; i < num_tests; i++){
       auto x_val = xgen();
       auto f_val = f(x_val);
       auto g_val = g(x_val);

       if (i >= f_val.size())
           break;
       auto is_equal = (f_val[i] == g_val[i]);
       if (!is_equal){
          std::cerr << "ERROR: Function equality test (vectorised): failed." << std::endl;
          std::cerr << "f_val = " << f_val << std::endl;
          std::cerr << "g_val = " << g_val << std::endl;
          throw n_eq_n_exception();
       } else {
          DEBUG_MESSAGE("Function equality test (vectorised): ", "passed.")
          // DEBUG_MESSAGE("f_val = ", f_val)
          // DEBUG_MESSAGE("g_val = ", g_val)
       }
   }
}


template<typename T, typename X>
std::enable_if_t<!is_exact<T>::value> check_function_equality(std::function<Vec<T>(X)> f, std::function<Vec<T>(X)> g, std::function<X()> xgen, std::function<void(int, size_t, size_t, size_t)> result_reporter = nullptr){
   using std::log10;
   using std::abs;

   size_t nan_count = 0;
   size_t zero_count = 0;
   size_t num_tests = 100;

   using TR = typename remove_complex<T>::type;
   std::vector<TR> precision_samples;
   for (size_t i = 0; i < num_tests; i++){

       auto x_val = xgen();
       auto f_result = f(x_val);
       auto g_result = g(x_val);

       // do for all Ds values
       for(size_t j = 0; j<f_result.size(); j++){

           auto f_val = f_result[j];
           auto g_val = g_result[j];

           auto rel_error = abs((g_val - f_val))/abs(f_val);
           precision_samples.push_back(rel_error);

           if (abs(g_val) < abs(T(1e-6))){
               zero_count++;
               continue;
           }

           if (rel_error > abs(T(1e-4))){
               std::cerr << "ERROR: Equivalence check failed on test " << i << "/" << num_tests <<"." << std::endl;
               std::cerr << "f(x) = " << f_val << std::endl;
               std::cerr << "g(x) = " << g_val << std::endl;
               std::cerr << "Relative error: " << rel_error << std::endl;
               exit(1);
           }
           else if (rel_error != rel_error){
               std::cout << "WARNING: Equivalence check hits NaN:" << std::endl;
               nan_count++;
           }
       }
   }

   TR average_precision = sum_vector(precision_samples)/TR(int(num_tests));
   int average_digits = detail::convert_to_int((-log10(average_precision)));

   if (result_reporter == nullptr){
       if (zero_count == num_tests){
         std::cout << "Functions are numerically zero." << std::endl;
       }

       std::cout << "Numerical equivalence check passed. Average digits = "<< average_digits <<". "
       << nan_count << " NaNs."<< std::endl;
   }
   else result_reporter(average_digits, num_tests, nan_count, zero_count);
}

template<typename T, typename X>
std::enable_if_t<!is_exact<T>::value> check_function_equality(std::function<T(X)> f, std::function<T(X)> g, std::function<X()> xgen, std::function<void(int, size_t, size_t, size_t)> result_reporter = nullptr){
   using std::log10;
   using std::abs;

   size_t nan_count = 0;
   size_t zero_count = 0;
   size_t num_tests = 100;

   using TR = typename remove_complex<T>::type;
   std::vector<TR> precision_samples;

   for (size_t i = 0; i < num_tests; i++) {
       auto x_val = xgen();

       auto f_val = f(x_val);
       auto g_val = g(x_val);

       auto rel_error = abs((g_val - f_val)) / abs(f_val);
       precision_samples.push_back(rel_error);

       if (abs(g_val) < abs(T(1e-6))) {
           zero_count++;
           continue;
       }

       if (rel_error > abs(T(1e-4))) {
           std::cerr << "ERROR: Equivalence check failed on test " << i << "/" << num_tests << "." << std::endl;
           std::cerr << "f(x) = " << f_val << std::endl;
           std::cerr << "g(x) = " << g_val << std::endl;
           std::cerr << "Relative error: " << rel_error << std::endl;
           exit(1);
       } else if (rel_error != rel_error) {
           std::cout << "WARNING: Equivalence check hits NaN:" << std::endl;
           nan_count++;
       }
   }

   TR average_precision = sum_vector(precision_samples)/TR(int(num_tests));
   int average_digits = detail::convert_to_int((-log10(average_precision)));

   if (result_reporter == nullptr){
       if (zero_count == num_tests){
         std::cout << "Functions are numerically zero." << std::endl;
       }

       std::cout << "Numerical equivalence check passed. Average digits = "<< average_digits <<". "
       << nan_count << " NaNs."<< std::endl;
   }
   else result_reporter(average_digits, num_tests, nan_count, zero_count);
}

// exact kinematics require no natural scales.
template <typename T, size_t D> std::enable_if_t<is_exact_v<T>, T> natural_scale(const lGraph::lGraphKin::GraphKin<T, D>& kinematics) { return T(1); }

template<typename T, size_t D>
std::enable_if_t<!is_exact_v<T>,T> natural_scale(const lGraph::lGraphKin::GraphKin<T, D>& kinematics){

  return T(2); // ~ sqrt(5)
  // here was something to improve numerical stability at some point...
}

template<typename T, size_t D>
class Ansatz{
  public:

    using BasisGroup = std::function<std::vector<T>(OnShellPoint<T,D>)>;

    std::vector<BasisGroup> basis_functions;
    std::vector<size_t> basis_dimensions;
    std::vector<HierarchyPos> associated_positions;

    void add_topology(const BasisGroup& functions, size_t dimension, HierarchyPos position);
};

template<typename T, size_t D>
void Ansatz<T,D>::add_topology(const std::function<std::vector<T>(OnShellPoint<T,D>)>& functions, size_t dimension, HierarchyPos position){
  basis_functions.push_back(functions);
  basis_dimensions.push_back(dimension);
  associated_positions.push_back(position);
}

  /**
   * Finds the positions (as given in pos_list) of the xs which are "non-zero"
   */

template<typename T>
std::vector<size_t> non_zero_positions(const std::vector<T>& xs, const std::vector<size_t>& pos_list){

    auto max_norm = [](std::vector<T> list){
    	return  abs(*std::max_element(list.begin(), list.end(),
    					    [](T a, T b){return abs(a)<abs(b);}));
    };

    auto scale = detail::non_zero_zero_scale(T(0));

    std::vector<size_t> new_pos_list;
    auto max_coeff_norm = max_norm(xs);
    for (size_t coeff_i = 0; coeff_i < xs.size(); coeff_i++){
    	  auto coeff_mag = abs(xs.at(coeff_i));
	  auto tiny_coeff = ( coeff_mag < (max_coeff_norm*scale) || coeff_mag < scale);
    	  if (!tiny_coeff){
	    new_pos_list.push_back(pos_list.at(coeff_i));
        }
    }

    return new_pos_list;

}


template<typename TF, typename T, typename X>
std::enable_if_t<!is_exact<T>::value,std::vector<TF>> fit_function_removing_zeros(std::function<TF(X)> f, std::function<std::vector<T>(X)> basis_functions, int basis_dim, std::function<X()> point_gen, size_t container_dimension){
    throw std::logic_error("Fit removing zeroes should not be used with floating point types");
}

template<typename TF, typename T, typename X>
std::enable_if_t<is_exact<T>::value,std::vector<TF>> fit_function_removing_zeros(std::function<TF(X)> f, std::function<std::vector<T>(X)> basis_functions, int basis_dim, std::function<X()> point_gen, size_t container_dimension){
  using std::function;
  using std::vector;

    // A priori all tensors are relevant.
    vector<TF> relevant_coeffs;

    vector<size_t> relevant_list;
    size_t num_fs = static_cast<size_t>(basis_dim);
    for (size_t i = 0; i < num_fs; i++){
    	relevant_list.push_back(i);
    }

    bool have_dropped_functions = true;
    size_t fit_count = 0;
    do{
	fit_count++;
    	// Build relevant tensors
      function<vector<T>(X)> relevant_basis_functions
    	  = [basis_functions, relevant_list](X os_point){
    	    auto full_functions = basis_functions(os_point);
    	    vector<T> relevant_functions(relevant_list.size());
          for (size_t i = 0; i < relevant_list.size(); i++){
    	      relevant_functions.at(i) = full_functions.at(relevant_list.at(i));
          }
          return relevant_functions;
      };

      // Run the fit.
      relevant_coeffs = get_coeffs_default_strategy(f, relevant_basis_functions, basis_dim, point_gen);
      if (relevant_coeffs.size() == 0){break;}

      // Figure out which are relevant
	auto new_relevant_list = non_zero_positions(relevant_coeffs, relevant_list);
    	if (new_relevant_list.size() == relevant_list.size()){
    	  have_dropped_functions = false;
    	}

    	relevant_list = new_relevant_list;
	basis_dim = new_relevant_list.size();

	// Stop refitting if the answer is zero
	if (new_relevant_list.size() == 0){
	  break;
	}

    }
    while(have_dropped_functions);

    // Build the coeffs of the original 
    vector<TF> coeffs = vector<TF>(num_fs, TF(uint64_t(container_dimension)));
    if (relevant_list.size() == num_fs){
    	  coeffs = relevant_coeffs;
    }
    else{
    	// Refill coeffs with relevant coeffs and exact zeros
    	// Combine relevant_coeffs back into
    	for (size_t i=0; i < relevant_list.size(); i++){
    	  coeffs.at(relevant_list.at(i)) = relevant_coeffs.at(i);
    	}

        if(verbosity_settings.fit_function_removing_zeros == true){
            std::cout << "Dropped " <<  num_fs - relevant_list.size() << "/" << num_fs << " functions. ";
            std::cout << "Fit " << fit_count << " times. ";
            std::cout << std::endl;
        }
    }

    return coeffs;
}

  template<typename TF, typename T, typename X>
std::vector<std::vector<TF>> fit_functions(std::function<TF(X)> f, const std::function<X()>& point_gen,
				     std::vector<std::function<std::vector<T>(X)>> list_basis_functions,
				     std::vector<size_t> list_basis_sizes,
				     bool remove_zeros, size_t container_dimension){
    using std::function; using std::vector;

    // Compress list_basis_functions into one big basis function vector
    function<vector<T>(X)> basis_functions = [&list_basis_functions](X p){
      vector<T> combined;
      for (auto& this_basis_functions : list_basis_functions){
        vector<T> this_vals = this_basis_functions(p);
	combined.insert(std::end(combined), std::begin(this_vals), std::end(this_vals));
      }
      return combined;
    };

    auto basis_dim = std::accumulate(list_basis_sizes.begin(), list_basis_sizes.end(), 0);
    auto coeffs = remove_zeros ? fit_function_removing_zeros(f, basis_functions, basis_dim,
							     point_gen, container_dimension)
      : get_coeffs_default_strategy(f, basis_functions, basis_dim, point_gen);

    #ifdef DEBUG_ON
    // Check for NaNs
    for (auto& coeff : coeffs){
        if (coeff != coeff){
    	  std::cerr << "ERROR: NaN coefficient in fit function." << std::endl;
    	  exit(1);
        }
    }
    #endif 

    vector<function<TF(X)>> fit_fs;
    vector<vector<TF>> function_coeffs;
    size_t start_pos = 0;
    for (size_t i = 0; i < list_basis_functions.size(); i++){
	auto& this_basis_functions = list_basis_functions.at(i);
	auto num_fs = list_basis_sizes.at(i);
    	vector<TF> this_coeffs(coeffs.begin()+start_pos, coeffs.begin()+start_pos+num_fs);
	std::function<TF(X)> new_fit_f = [this_coeffs, this_basis_functions, container_dimension](X x){
	  auto basis_vals = this_basis_functions(x);
          // we convert to int to avoid problem with QD types
	  TF result(static_cast<int>(container_dimension));
          for (size_t i = 0; i < basis_vals.size(); i++){
          	result += this_coeffs[i]*basis_vals[i];
          }
	  return result;
	};

    	fit_fs.push_back(new_fit_f);
    	function_coeffs.push_back(this_coeffs);
    	start_pos += num_fs;
    }

  if (settings::general::do_n_eq_n_test == settings::answer::yes){

	// Compress fit_fs into one function
	std::function<TF(X)> total_fit_f = [fit_fs](X x){
	  TF combined = fit_fs.at(0)(x);
	  for (size_t i = 1; i < fit_fs.size(); i++){
	    combined += fit_fs.at(i)(x);
	  }
	  return combined;
	};

	std::function<void(int,size_t, size_t, size_t)> n_equals_n_reporter =
	  [](int average_digits, size_t num_tests, size_t nan_count, size_t zero_coutn){
	    std::cout << " (digits=" << average_digits << ")";
            if (nan_count != 0){
        	 std::cout << nan_count << " NaNs. ";
           }
        };

        check_function_equality(f, total_fit_f, point_gen, n_equals_n_reporter);
    }

   return function_coeffs;
}

}
