#include <iostream>
#include <cstring>
#include <mutex>
#include "misc/TestUtilities.hpp"
#include "AmplitudeCoefficients/IntegrandHierarchy.h"
#include "Core/Utilities.h"
#include "Core/settings.h"
#include "Forest/Model.h"
#include "Core/type_traits_extra.h"

#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "AmplitudeCoefficients/ResidueEvaluator.h"

#include "Core/spinor/spinor.h"
#include "Core/settings.h"

#include "FunctionSpace/IBPs/IBPImport.hpp"
#include "FunctionSpace/MasterIntegrands/Import.hpp"

#include "Core/enumerate.h"

#include "Graph/Graph.h"

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "Core/qd_suppl.h"
#endif

//! if defined will check all received on-shell points
//#define DEBUG_CHEK_ONSHELLNESS

namespace Caravel{
namespace IntegrandHierarchy{

void load_surface_terms(const Model& mod, const PartialAmplitudeInput& process){
    static std::mutex m;
    std::lock_guard<std::mutex> lock(m);
    // TODO: a better mechanism to check what has been loaded already
    static bool loaded = false;
    if (!loaded) {
        if(mod.get_name() == "CubicGravity"){
            FunctionSpace::SurfaceTerms::Four_Point_Gravity::load();
            FunctionSpace::SurfaceTerms::Factorizable::load();
        }
        else{
            FunctionSpace::SurfaceTerms::Five_Point_Planar_QCD::load();
            FunctionSpace::SurfaceTerms::Factorizable::load();
        }
        // the manual overload goes always at the end
        FunctionSpace::SurfaceTerms::Other::load();
        using namespace settings::general;

        using ::Caravel::operator<<;
        switch (master_basis) {
            case MasterBasis::simple_tensors: break;
            case MasterBasis::pure:
                FunctionSpace::MasterIntegrands::PureMasterBasis::load();
                break;
            case MasterBasis::standard:
                FunctionSpace::MasterIntegrands::Standard::load();
                break;
            case MasterBasis::alternative:
                // first load all the standard choices
                FunctionSpace::MasterIntegrands::Standard::load();
                FunctionSpace::MasterIntegrands::Alternative::load();
                break;
	    case MasterBasis::pentagon_functions_new:
		FunctionSpace::MasterIntegrands::PentagonFunctionsNew::load();
		break;

            default: _WARNING_R("Master basis set ", master_basis.value, " not implemented!"); std::exit(1);
        }
        if (verbosity_settings.report_integral_basis){
            _MESSAGE("Loaded master terms set: ",master_basis.value);
        }
        loaded = true;
    }
}

// Schematic loading
std::string load_schematic(std::string filename){
    std::ifstream run_card;
    run_card.open(filename);

    if (run_card.is_open()){
        std::stringstream string_buffer;
        string_buffer << run_card.rdbuf();
        run_card.close();

        return string_buffer.str();
    }
    else{
        std::cerr << "ERROR COULD NOT OPEN RUN CARD FILE: "<<filename << std::endl;
        std::cerr << "Check filename and if tarball is unpacked?" << std::endl;
        std::cerr << "Error: " << std::strerror(errno) << std::endl;

        return "";
    }
}

std::string load_schematic(const Model& m, const PartialAmplitudeInput& input){


    std::string process_name;

    switch (input.get_number_of_quarks()) {
        case 1: process_name += "QQbar"; break;
        case 2: process_name += "Q1Q1barQ2Q2bar"; break;
    }

    for (size_t i = 0; i < input.get_number_of_gluons(); i++){process_name += "G";}
    for (size_t i = 0; i < input.get_number_of_photons(); i++){process_name += "Y";}

    if (input.get_nf_powers()>0){
        process_name += "Nf" + std::to_string(input.get_nf_powers());
    }

    std::string filename  = std::string(DATADIR) + "/";

    if (m.get_name() == "CubicGravity"){
        filename += "/PL_Gravity/hhhh";
    }
    else {
        if (input.switch_quarks_to_adjoint()) { 
            filename += "leadingAdjoint"; 
        } else if (input.get_nc_order() == 0) {
            filename += "leading";  // this is out defautl for now
        } else if (input.get_nc_order() == 1) {
            filename += "subleading";
        }

        filename += process_name;
    }

    filename +=  "TwoLoop.cuts";

    _MESSAGE("Loading process library from: ",filename);

    //TODO: is there a better place to do this?
    load_surface_terms(m, input);

    return load_schematic(filename);
}

std::string to_string(const slice_selector& s){
    std::string out;
    std::stringstream sout;

    sout << "[" << static_cast<int>(s.type) << "]";

    switch(s.type){
        case slice_selector::type_choices::all_children_with_ancestors_slice:
            sout << s.s_hierarchy_pos;
            break;
        case slice_selector::type_choices::with_ancestors_slice:
            sout << s.s_hierarchy_pos;
            break;
        case slice_selector::type_choices::all_down_to_level:
            sout << s.level;
            break;
        case slice_selector::type_choices::combined_all_ancestors:
            sout << s.v_hierarchy_pos;
            break;
    }

    return sout.str();
}

slice_selector::slice_selector(MathList l) : slice_selector() {
    if(l.head != "SliceSelector"){
        _WARNING_R("Expected head \"SliceSelector\", got \"",l.head,"\"");
        std::exit(1);
    }

    auto read_pos = [](MathList m) -> HierarchyPos {
        return {std::stoul(m[0]),std::stoul(m[1])};
    };

    for(auto& its : l){
        MathList it(its);
        if(it.head == "Type"){
            auto t = wise_enum::from_string<type_choices>(it[0].c_str());
            if(!t){
                _WARNING_R("Unrecognized type ", it[0],"\nPossible types:");
                for (auto it : wise_enum::range<type_choices>) { _WARNING("\t", it.name); }
                std::exit(1);
            }
            type = t.value();
        }
        else if(it.head == "HierarchyPos"){
            switch(type){
                case type_choices::with_ancestors_slice:
                case type_choices::all_children_with_ancestors_slice:
                    s_hierarchy_pos = read_pos(it);
                    break;
                default:
                    _WARNING_R("Inconsistent slice selection of type ",type,"!");
                    std::exit(1);
            }
        }
        else if(it.head == "Level"){
            switch(type){
                case type_choices::all_down_to_level:
                    level = std::stoul(it[0]);
                    break;
                default:
                    _WARNING_R("Inconsistent slice selection of type ",type,"!");
                    std::exit(1);
            }
        }
        else if(it.head == "HierarchyPosVector"){
            switch(type){
                case type_choices::combined_all_ancestors:
                    break;
                default:
                    _WARNING_R("Inconsistent slice selection of type ",type,"!");
                    std::exit(1);
            }
            for(auto el : it){
                MathList p(el,"Pos");
                v_hierarchy_pos.push_back(read_pos(p));
            }
        }
        else{
            _WARNING_R("Unrecognized input ", it);
            std::exit(1);
        }
    }
}

template <typename T,size_t L,typename TF> void IntegrandHierarchy<T,L,TF>::init(){
    symmetry_factors.clear();
    for (const auto& top : *master_positions){
        symmetry_factors.push_back((*graph_representation)[top.first].get_symmetry_factor());
    }
    assert(master_positions->size() == symmetry_factors.size());
}

template <typename T,size_t L,typename TF> void IntegrandHierarchy<T,L,TF>::grow_forest(Forest& f, const PartialAmplitudeInput& input, bool add_tree){
    for (auto& gen : *test_slice){
        for (auto& top_pos : gen){
            auto& top = (*graph_representation)[top_pos];
            if (top.ask_related_cut()) {
                for(auto& term : top.get_decomposition().decomposition.elements){
                    std::get<0>(term).template add_to_forest<T>(f,input.get_particles());
                }
            }
        }
    }

    using namespace Caravel::settings::IntegrandHierarchy;

    if(add_tree){
        // FIXME
        // fix when no pointer in Forest interface
        std::vector<Particle*> tree_process_particles;
        // this is a slight memory leak, but has to be done because Forest uses poiners to compare particles...
        for(auto& it : input.get_particles()) tree_process_particles.push_back(new Particle(it));
        // we set all external indices to deafult for tree amplitudes, this has to be done
        // since builder uses Ds>4 to compute trees
        for(auto it: tree_process_particles) it->set_dtt_index(0);

        Process tree_process(tree_process_particles);

        tree_id = f.add_process(tree_process);

    }
}

template <typename T, size_t L, typename TF>
typename IntegrandHierarchy<T, L, TF>::ReturnType_Coefficients
IntegrandHierarchy<T, L, TF>::get_master_coefficients(const momentumD_configuration<T, 4>& mom_conf, size_t i) {

    ReturnType_Coefficients result;

    if constexpr (std::is_same_v<TF, Vec<T>>) {
        auto optimize_warmup = (Caravel::settings::IntegrandHierarchy::optimize_warmup_for_cuts == settings::answer::yes);
#ifdef DS_BY_PARTICLE_CONTENT
        result = internal::dimensional_reconstruct_quadratic_2(subtraction_engines.at(i).reduce(mom_conf, !optimize_warmup, 3), Ds_value);
#else
        if (Dss.size() == 3)
            result = internal::dimensional_reconstruct_quadratic(subtraction_engines.at(i).reduce(mom_conf, !optimize_warmup, 3), Dss, Ds_value);
        else
            result = internal::dimensional_reconstruct_generic(subtraction_engines.at(i).reduce(mom_conf, !optimize_warmup, Dss.size()), Dss, Ds_value);
#endif
    }
    else if constexpr (std::is_same_v<T, TF>) {
        auto optimize_warmup = (Caravel::settings::IntegrandHierarchy::optimize_warmup_for_cuts == settings::answer::yes);
        auto reduced = subtraction_engines.at(i).reduce(mom_conf, !optimize_warmup, 0);
        for (auto&& coeff : reduced) { result.push_back(DenseRational<T>(std::move(coeff.numerator), std::move(coeff.denominator))); }
    }
    else {
        static_assert(_delayed_static_false_v<TF>);
    }

    for (unsigned ii = 0; ii < result.size(); ii++) result[ii].numerator /= T(symmetry_factors.at(ii));
    return result;
}

template <typename T,size_t L,typename TF>
typename IntegrandHierarchy<T,L,TF>::ReturnType_Ds_Coefficients
IntegrandHierarchy<T,L,TF>::get_master_coefficients_full_Ds(const momentumD_configuration<T,4>& mom_conf, size_t i){

    ReturnType_Ds_Coefficients result;

    if constexpr (std::is_same_v<TF, Vec<T>>) {
        auto optimize_warmup =(Caravel::settings::IntegrandHierarchy::optimize_warmup_for_cuts == settings::answer::yes);
#ifdef DS_BY_PARTICLE_CONTENT
        result = internal::vec_to_dsm6_polynomials(subtraction_engines.at(i).reduce(mom_conf, !optimize_warmup, 3));
#else
        // we choose to always use dimensional_reconstruct_generic_Ds (to get a Ds polynomial, and not a (Ds-4) polynomial)
        result = internal::dimensional_reconstruct_generic_Ds(subtraction_engines.at(i).reduce(mom_conf, !optimize_warmup, Dss.size()), Dss, Ds_value);
#endif
    }
    else {
        _WARNING_R("IntegrandHierarchy::get_master_coefficients_full_Ds(.,",i,") called when no Vec type used");
        std::exit(1);
    }

    for (unsigned ii = 0; ii < result.size(); ii++) result[ii].numerator /= T(symmetry_factors.at(ii));

    return result;
}

template <typename T,size_t L> bool ColourExpandedAmplitude<T,L>::reload_warmup(){
    auto warmup_infos = load_warmups(warmup_filename);

    if (warmup_infos.size() != all_hierarchies.size()) {
        if (!warmup_infos.empty()) _WARNING("WARNING: warmup size does not seem to match the number of hierarchies! Will write a new warmup.");
        need_warmup = true;
    } else {
        need_warmup = false;
        for (auto&& [i, obj] : enumerate(all_hierarchies)) { *obj.warmup_info = std::move(warmup_infos.at(i)); }
    }

    return need_warmup;
}


template <typename T,size_t L> void ColourExpandedAmplitude<T,L>::set_Ds_value(ColourExpandedAmplitude<T,L>::Ds_type ds){
    for(auto& it: all_hierarchies){
        it.Ds_value = ds;
    }
}

template <typename T,size_t L> void ColourExpandedAmplitude<T,L>::finish_construction(const Model& m, const PartialAmplitudeInput& input){

    if(all_hierarchies.size() != 1){
        _WARNING_R("Only works for one hierarchy so far!");
        std::exit(1);
    }

    const auto& process = input.get_particles();

    normalization = settings::IntegrandHierarchy::amplitude_normalization;

    if (normalization == norm_type::automatic) {
        if (is_vanishing_tree(input)) { normalization = norm_type::spinor_weight; }
        else {
            normalization = norm_type::tree;
        }
    }

    _MESSAGE("Normalizing by ", normalization);

    DEBUG(
        auto& ih = all_hierarchies.front();
        for(auto& it: *(ih.test_slice)){
            for( auto& jt: it){
                _MESSAGE("");
                _MESSAGE("HierarchyPos: ",jt);
                _MESSAGE((*ih.graph_representation)[jt].get_graph());
                _MESSAGE(math_string((*ih.graph_representation)[jt].get_graph()));
                _MESSAGE("Children: ",ih.graph_representation->get_child_positions(jt));
                _MESSAGE("Ancestors: ",ih.graph_representation->get_ancestors(jt));
            }
        }
        _MESSAGE("Master positions:");
        for(unsigned i=1; i <= ih.master_positions->size(); ++i){
            _MESSAGE(i,": ",ih.master_positions->at(i-1));
        };
    );

    std::set<unsigned> nf_powers_found;
    // check that we have all cuts with the same Nf power
    {
        for(auto& ih : all_hierarchies){
            for (auto& it : *(ih.test_slice)) {
                for (auto& jt : it) { nf_powers_found.insert((*ih.graph_representation)[jt].get_decomposition()[0].nf_powers); }
            }
        }
        if(nf_powers_found.size() != 1){
            _WARNING_R(nf_powers_found);
            throw std::runtime_error("Only amplitudes with Nf factored out are supported so far."); 
        }
    }

    DEBUG_PRINT(nf_powers_found);

    // Choose how to handle Ds dependence depending on the model
    if(m.get_name() == "YM"){
        Dss = std::vector<size_t>{6,7,8};
    }
    else if(m.get_name() == "CubicGravity"){
        Dss = std::vector<size_t>{6,7,8};
        if(settings::IntegrandHierarchy::number_of_Ds_values == 5){
            Dss = std::vector<size_t>{6,7,8,9,10};
        }
        else if(settings::IntegrandHierarchy::number_of_Ds_values != 3){
            throw std::runtime_error("IntegrandHierarchy allows only 3 and 5 values of Ds (so far). Received: "+std::to_string(settings::IntegrandHierarchy::number_of_Ds_values)); 
        }
    }
    else{
        Dss = std::vector<size_t>{6,8,10};
    }


#ifdef DS_BY_PARTICLE_CONTENT
    forest->set_minimal_states_in_currents(false);
#else
    forest->set_minimal_states_in_currents(true);
#endif

    // Create forest for all hieararchies
    for(auto& it: all_hierarchies){
        it.grow_forest(*forest,input,normalization == norm_type::tree);
        it.Dss = Dss;
    }

#ifndef DS_BY_PARTICLE_CONTENT
    forest->define_internal_states_and_close_forest(Dss);
#else
    forest->define_internal_states_and_close_forest({6});
#endif


    // Set up warmup information
    {
        for(auto& it: process){
            std::string name(it.get_name());
            if (name == "gluon") name = "g"; // just to make it shorter
            warmup_filename += name += "<";
            warmup_filename += it.external_state().get_name();

            if( it.external_state().get_dtt_index() != 0){
                warmup_filename += ":";
                warmup_filename += std::to_string(it.external_state().get_dtt_index());
            }

            warmup_filename += ">";
        }

        if(*nf_powers_found.begin()!=0){
            warmup_filename += "_Nf^";
            warmup_filename += std::to_string(*nf_powers_found.begin());
        }

        if(input.switch_quarks_to_adjoint()) {
            warmup_filename += std::string(".ADJ");
        }

        warmup_filename += ".";

        {
            using namespace settings;
            if (general::numerator_decomposition == general::IntegralBasis::tensors) { warmup_filename += "tensor"; }
            else {
                warmup_filename += wise_enum::to_string(settings::general::master_basis.value);
            }
        }

        warmup_filename += std::string(".info");

        reload_warmup();
    }


    // Construct builder
    auto builder = std::make_shared<Builder<T,D>>(forest.get(), process.size());

    // Construct residue evaluators
    {
        using namespace settings::IntegrandHierarchy;

        std::vector<std::shared_ptr<Builder<T, D>>> builders;
        builders.push_back(builder);

        switch (parallelize_topology_fitting) {
            case parallelize_topologies_options::warmup:
                if (has_warmup()) break;
                __FALLTHROUGH;
            case parallelize_topologies_options::always: {
                std::vector<std::future<std::shared_ptr<Builder<T, D>>>> futures;
                auto make_builder = [f = this->forest.get(), N = process.size()]() { return std::make_shared<Builder<T, D>>(f, N); };
                // FIXME: we should have at most max_size_hierarchy_level builders
                for (int i = 0; i < topology_fitting_max_nthreads; ++i) { futures.push_back(std::async(std::launch::async, make_builder)); }
                for (auto& it : futures) { builders.push_back(it.get()); }
            } break;
            case parallelize_topologies_options::never: break;
        }
        add_residue_evaluators(builders);
    }

    for(auto& it: all_hierarchies){
        switch(normalization){
            case norm_type::spinor_weight:
                it.compute_normalization = construct_normalizer<T>(process);
                break;
            case norm_type::tree:
                assert(it.tree_id);
                it.compute_normalization = [builder,&it](const momentumD_configuration<T,4>& conf){
                    T result = builder->get_tree(it.tree_id);
                    return result;
                };
                break;
            default:
                break;
        }
    }

    if (verbosity_settings.process_library_construction == true) {
        _MESSAGE("Constructed ", all_hierarchies.size(), " colour structures");
        _MESSAGE("\twith ", all_hierarchies.size(), " hierarchies in total");
        {
            size_t count{0};
            for (auto& ih : all_hierarchies) {
                for (auto& it : *(ih.test_slice)) { count += it.size(); }
            }
            _MESSAGE("\twith ", count, " topologies in total");
        }
    }

    if (verbosity_settings.process_library_construction == true) { _PRINT(normalization); }
}

template <typename T, size_t L> size_t ColourExpandedAmplitude<T, L>::add_residue_evaluators(const std::vector<std::shared_ptr<Builder<T, D>>>& builders) {
    for (auto& it : all_hierarchies) {
        using std::vector;
        using std::function;

        using TopoEvaluator = function<vector<T>(const OnShellPoint<T, D>&)>;

        std::unordered_map<HierarchyPos, TopoEvaluator> residue_evaluators;

        auto hierarchy = it.graph_representation;

        // gen --> list of bunches, each bunch is a list of positions in gen
        std::vector<std::vector<std::vector<HierarchyPos>>> parallel_scheme;

        for (auto& gen : *(it.test_slice)) {

            std::vector<std::vector<HierarchyPos>> bunches_on_gen;

            std::vector<HierarchyPos> clean_gen = gen;
            clean_gen.erase(std::remove_if(clean_gen.begin(),clean_gen.end(),[&hierarchy](auto& t){return !((*hierarchy)[t].ask_related_cut());}),  clean_gen.end());

            // FIXME: it makes sense to order the bunches on a sorted way, depending on the dimension of the function space
            auto bunches = split_into_bunches(clean_gen.size(),builders.size());

            for(auto& b : bunches){
                std::vector<HierarchyPos> current_bunch;
                for (auto&& [i, obj] : enumerate(b)) {
                    HierarchyPos jt = clean_gen[obj];
                    current_bunch.push_back(jt);
                    const auto& h = (*hierarchy)[jt];

                    if (!h.ask_related_cut()) {
                        residue_evaluators[jt] = nullptr;
                        continue;
                    }

#ifndef DS_BY_PARTICLE_CONTENT
                    // here we have to assume that all cuts in the topology have the same Ds values in builder
                    auto lDss = builders[i]->get_cut_dimensions(h.get_decomposition().decomposition.elements.front().first.get_first_forest_position()).size();

                    // Builder determines which Ds values it can pick for efficiency. If it says us that less Ds
                    // values were used, we assume that missing ones can be obtained trivially
                    TopoEvaluator topology_evaluator = [builder_l = builders[i], &h, diff = Dss.size() - lDss, always_recompute_tree_currents = always_recompute_tree_currents](const OnShellPoint<T, D>& p) {
                        builder_l->set_loop(p);
#ifdef DEBUG_CHEK_ONSHELLNESS
                        h.assert_on_shell(builder_l->get_phys_momD_conf(), p); // Automatic on-shell checks.
#endif
                        switch (diff) {
                            case 0: {
                                return h.evaluate(*builder_l, always_recompute_tree_currents);
                            } break;
                            case 1: {
                                auto ds_samples = h.evaluate(*builder_l, always_recompute_tree_currents);
                                // interpolate Ds = 8 from Ds = 6,7
                                ds_samples.push_back(ds_samples.at(1) * T(2) - ds_samples.at(0));
                                return ds_samples;
                            }
                            case 2: {
                                auto ds_samples = h.evaluate(*builder_l, always_recompute_tree_currents);
                                ds_samples.push_back(ds_samples.at(0));
                                ds_samples.push_back(ds_samples.at(0));
                                return ds_samples;
                            }
                            default: throw std::logic_error("Internal inconsistency");
                        };
                    };
                    residue_evaluators.emplace(jt, topology_evaluator);
#else
                    // TODO: max power of (Ds-6) not hard-coded
                    residue_evaluators.emplace(jt, [builder_l = builders[i], &h, always_recompute_tree_currents = always_recompute_tree_currents](const OnShellPoint<T, D>& p) {
                        builder_l->set_loop(p);
#ifdef DEBUG_CHEK_ONSHELLNESS
                        h.assert_on_shell(builder_l->get_phys_momD_conf(), p); // Automatic on-shell checks.
#endif
                        return h.evaluate(*builder_l, always_recompute_tree_currents);
                    });
#endif

                }
                bunches_on_gen.push_back(current_bunch);
            }

            parallel_scheme.push_back(std::move(bunches_on_gen));
        }

        ResidueEvaluator<TF, T, D> re{ [residue_evaluators = std::move(residue_evaluators)](HierarchyPos pos, const momentumD_configuration<T, D>&,
                const OnShellPoint<T, D>& p) mutable { return residue_evaluators.at(pos)(p); },
            [builders](const momentumD_configuration<T, 4>& mom_conf) {
                for(auto& it : builders){
                    it->set_p(mom_conf);
                    it->compute();
                    it->contract();
                }
            }};

        it.subtraction_engines.emplace_back(it.graph_representation, it.test_slice, re, it.master_positions, it.warmup_info, verbosity_settings.subtraction_engine);
        if (builders.size() > 1) { it.subtraction_engines.back().assign_parallelization_scheme(parallel_scheme); }
    }
    return all_hierarchies.front().subtraction_engines.size() - 1;
}

template <typename T, size_t L>
void ColourExpandedAmplitude<T, L>::select_hierarchy(const PartialAmplitudeInput& input,
                                                     LinearCombination<ColourStructure, LinearCombination<NColPower, std::string>>&& all_colour_structures) {
    size_t size_colour(all_colour_structures.size());

    if(size_colour==0) _WARNING("Process library is empty!");

    const size_t N = input.get_multiplicity();

    std::vector<size_t> p_order = input.get_momentum_indices();
    std::vector<std::vector<size_t>> all_rotations(N,p_order);

    for(unsigned i = 1 ; i < N; ++i){
        std::rotate(all_rotations[i].begin(),all_rotations[i].begin()+1, all_rotations[i].end());
    }

    DEBUG_PRINT(size_colour);
    DEBUG_PRINT(input);
    DEBUG_PRINT(all_rotations);


    auto match_cyclic = [&](const std::vector<size_t>& v){
        for(auto& r : all_rotations){
            if(v == r) return true;
        }
        return false;
    };

    auto find_index_in_process = [&input](size_t index) -> const Particle& {
        for(const Particle& it: input.get_particles()){ if(it.mom_index() == index) return it; }
        _WARNING_R("Momentum index obtained from the process library does not match any index in the external process");
        std::exit(1);
    };

    int powers_of_minus_one;

    auto match_ordered = [&](const ColourStructure& cs) -> bool {
        DEBUG_PRINT(cs);
        if(!(cs.closed_funamental.size() == 1 and cs.open_fundamental.empty()) and !(!cs.open_fundamental.empty() and cs.closed_funamental.empty())){
            _WARNING_RED("Colour structure matching not implemented for subleading-colour!");
            // std::exit(1);
        }

        powers_of_minus_one = 0;

        if(input.get_number_of_gluons() == input.get_multiplicity() or input.switch_quarks_to_adjoint()){
            // assert(cs.closed_funamental.size() == 1);
            if (match_cyclic(cs.closed_funamental.front())) {
                return true;
            }
            else
                return false;
        }

        auto open = cs.open_fundamental;

        for(auto& c : open){
            if(in_container<ParticleFlavor::q>(find_index_in_process(c.front()).get_flavor())){
                // if we reverse an open_fundamental colour string, we receive a factor of (-1)^n, where
                // n is the number of T^a in the string ( = number of gluons indices) 
                powers_of_minus_one += c.size()-2;
                std::reverse(c.begin(),c.end());
            }
        }

        // we try to match against any pairwise swaps since this is the same structure
        do{
            std::vector<size_t> to_match;
            for (auto& c : open) { std::copy(c.begin(), c.end(), std::back_inserter(to_match)); }

            DEBUG_PRINT(to_match);
            if (match_cyclic(to_match)) { 
                return true; 
            }
        } while (std::prev_permutation(open.begin(),open.end()));
        return false;
    };

    size_t i =0;
    for (; i < all_colour_structures.size(); ++i) {
        if (match_ordered(all_colour_structures[i].first)) { break; }
    };

    // DEBUG_MESSAGE("All colour structures ", all_colour_structures);

    if(i == all_colour_structures.size()){
        _WARNING_RED("Could not find suitable colour structure!");
        if (all_colour_structures.size() == 1) {
            _WARNING_RED("Since there is only a single colour structure this will be used despite not being matched.");
        } else {
            std::exit(1);
        }
    } else {
        // remove all but selected colour structure
        DEBUG_MESSAGE("Matched colour structure ", i, ": ", all_colour_structures.elements.at(i), ". Moving it to first position.");
        std::swap(all_colour_structures.elements.at(0), all_colour_structures.elements.at(i));
        if (size_colour > 1) { all_colour_structures.elements.erase(all_colour_structures.elements.begin() + 1, all_colour_structures.elements.end()); }
    }

    decltype(auto) selected_structure = std::move(all_colour_structures.elements.at(0));

    //if (selector.s_colour_structure >= size_colour ) throw std::range_error("Received hierarchy colour structure selector which is out of bounds");
    //if (selector.s_Nc >= all_colour_structures[selector.s_colour_structure].second.size() ) throw std::range_error("Received hierarchy Nc power selector which is out of bounds");


    // TODO: implement remove all but selected Nc powers
    // so far we just take the first
    //std::swap(all_colour_structures.elements.at(0).second.elements.at(0),all_colour_structures.elements.at(0).second.elements.at(selector.s_Nc));
    if(size_colour > 1) {
        selected_structure.second.elements.erase(selected_structure.second.elements.begin()+1,selected_structure.second.elements.end());
    }

    all_hierarchies.emplace_back(selected_structure.second.elements[0].second);

    if(powers_of_minus_one %2 != 0){
        for (auto& topology_vec : all_hierarchies.front().graph_representation->nodes) {
            for (auto& topology : topology_vec) {
                for (auto& e : topology.get_decomposition().decomposition.elements) {
                    e.second.negate();
                }
            }
        }
    }

    // TODO: this should be Nc order and proper colour structure output (for now we just rely on the order of particles)
    // Fix me: set this according to required colour structure - Fixed now?
    if (input.get_nc_order() == 0) {
        warmup_filename += "LC_0";
    } else if (input.get_nc_order() == 1) {
        warmup_filename += "SLC_0";
    }
    warmup_filename += "_";
}

template <typename T,size_t L> void ColourExpandedAmplitude<T,L>::select_slice(const slice_selector& selector){
    // only makes sense when only one hierarchy has been already selected
    assert(all_hierarchies.size()==1);

    // replace with a sliced hiararchy (this inefficient since cosntructs IntegrandHierarchy again, but should be not a big deal)
    auto& ih = all_hierarchies.front();
    {
        HierarchySlice slice;
        switch(selector.type){
            case slice_selector::type_choices::all_children_with_ancestors_slice:
                slice = all_children_with_ancestors_slice(*ih.graph_representation, selector.s_hierarchy_pos);
                break;
            case slice_selector::type_choices::with_ancestors_slice:
                slice = with_ancestors_slice(*ih.graph_representation, selector.s_hierarchy_pos);
                break;
            case slice_selector::type_choices::all_down_to_level:
                slice = all_down_to_level(*ih.graph_representation, selector.level);
                break;
            case slice_selector::type_choices::combined_all_ancestors:
                slice = combined_all_ancestors(*ih.graph_representation, selector.v_hierarchy_pos);
                break;
        }
        ih = IntegrandHierarchy<T,2,TF>(ih.graph_representation,std::move(slice));
    }

    warmup_filename += to_string(selector);
    warmup_filename += "_";
}

template <typename T, size_t L>
ColourExpandedAmplitude<T, L>::ColourExpandedAmplitude(const Model& m, const PartialAmplitudeInput& process)
    : multiplicity(process.get_multiplicity()), forest(std::make_unique<Forest>(m)), always_recompute_tree_currents(m.get_name() == "CubicGravity") {
    if (m.get_name() == "CubicGravity") {
        all_hierarchies.emplace_back(load_schematic(m,process));
    }
    else {
        LinearCombination<ColourStructure, LinearCombination<NColPower, std::string>> all_colour_structures{load_schematic(m, process)};
        if (all_colour_structures.size() == 0) _WARNING_R("Process library is empty!");
        warmup_filename += "partial_";
        select_hierarchy(process.get_copy_only_coloured(), std::move(all_colour_structures));
    }
    finish_construction(m, process);
}

template <typename T, size_t L>
ColourExpandedAmplitude<T, L>::ColourExpandedAmplitude(const Model& m, const PartialAmplitudeInput& process, const slice_selector& sselector)
    : multiplicity(process.get_multiplicity()), forest(std::make_unique<Forest>(m)), always_recompute_tree_currents(m.get_name() == "CubicGravity") {
    if (m.get_name() == "CubicGravity") {
        all_hierarchies.emplace_back(load_schematic(m,process));
    }
    else {
        LinearCombination<ColourStructure, LinearCombination<NColPower, std::string>> all_colour_structures{load_schematic(m, process)};
        if (all_colour_structures.size() == 0) _WARNING_R("Process library is empty!");
        warmup_filename += "partial_";
        select_hierarchy(process.get_copy_only_coloured(), std::move(all_colour_structures));
    }
    select_slice(sselector);
    finish_construction(m, process);
}

template <typename T,size_t L> std::vector<typename IntegrandHierarchy<T,L>::ReturnType_Coefficients> ColourExpandedAmplitude<T,L>::get_master_coefficients(const momentumD_configuration<T,4>& mom_conf){


    std::vector<typename IntegrandHierarchy<T,L>::ReturnType_Coefficients> results;

    for(auto& it: all_hierarchies){
        auto r  = (it.get_master_coefficients(mom_conf));
        auto norm_val = T(1)/it.compute_normalization(mom_conf);
        for(auto& i : r){ i.numerator*=norm_val; }
        results.push_back(std::move(r));
    }

    if(need_warmup){
        std::vector<std::shared_ptr<HierarchyWarmupInfo>> warmup_infos;
        for (auto& h : all_hierarchies) { warmup_infos.push_back(h.warmup_info); }
        write_warmup(warmup_filename,warmup_infos);
        for(auto& h : all_hierarchies){
            for(auto& s : h.subtraction_engines){
                s.build_hierarchy_function_bases_with_warmup();
            }
        }
        need_warmup = false;
    }

    return results;
}

template <typename T,size_t L> std::vector<typename IntegrandHierarchy<T,L>::ReturnType_Ds_Coefficients> ColourExpandedAmplitude<T,L>::get_master_coefficients_full_Ds(const momentumD_configuration<T,4>& mom_conf){

    std::vector<typename IntegrandHierarchy<T,L>::ReturnType_Ds_Coefficients> results;

    for(auto& it: all_hierarchies){
        auto r  = (it.get_master_coefficients_full_Ds(mom_conf));
        auto norm_val = T(1)/it.compute_normalization(mom_conf);
        for(auto& i : r){ i.numerator*=norm_val; }
        results.push_back(std::move(r));
    }

    if(need_warmup){
        std::vector<std::shared_ptr<HierarchyWarmupInfo>> warmup_infos;
        for (auto& h : all_hierarchies) { warmup_infos.push_back(h.warmup_info); }
        write_warmup(warmup_filename,warmup_infos);
        for(auto& h : all_hierarchies){
            for(auto& s : h.subtraction_engines){
                s.build_hierarchy_function_bases_with_warmup();
            }
        }
        need_warmup = false;
    }

    return results;
}

template <typename T,size_t L> bool ColourExpandedAmplitude<T,L>::warmup_run(const momentumD_configuration<T,4>& mom_conf){
    bool did_warmup = false;
    if(need_warmup) {
        get_master_coefficients(mom_conf);
        did_warmup = true;
    }
    return did_warmup;
}



template <typename T, size_t L> void AmplitudeLikeContribution<T, L>::check_warmup() {
    auto t = load_warmups(warmup_filename);

    if (t.empty())
        need_warmup = true;
    else if (t.size()==1) {
        need_warmup = false;
        *hierarchy.warmup_info = t.front();
    } else {
        _WARNING("WARNING: warmup file seems to be of wrong size. Will overwrite.");
        need_warmup = true;
    }
}

template <typename T, size_t L> AmplitudeLikeContribution<T, L>::AmplitudeLikeContribution(size_t n_ext) : multiplicity(n_ext) {
    select_hierarchy();
    finish_construction();
}
template <typename T, size_t L> AmplitudeLikeContribution<T, L>::AmplitudeLikeContribution(size_t n_ext, const slice_selector& s) : multiplicity(n_ext) {
    select_hierarchy();
    select_slice(s);
    finish_construction();
}

template <typename T, size_t L> AmplitudeLikeContribution<T, L>::AmplitudeLikeContribution(size_t n_ext, const std::string& filename) : multiplicity(n_ext) {
    auto schematic = load_schematic(filename);
    try{
        hierarchy = IntegrandHierarchy<T, L>(schematic);
    }
    catch(...){
        LinearCombination<ColourStructure, LinearCombination<NColPower, std::string>> all_colour_structures(schematic);
        hierarchy = IntegrandHierarchy<T, L>(all_colour_structures.elements.front().second.elements.front().second);
    }
    finish_construction();
}

template <typename T, size_t L> AmplitudeLikeContribution<T, L>::AmplitudeLikeContribution(size_t n_ext, const std::string& filename, const slice_selector& s) : multiplicity(n_ext) {
    auto schematic = load_schematic(filename);
    try{
        hierarchy = IntegrandHierarchy<T, L>(schematic);
    }
    catch(...){
        LinearCombination<ColourStructure, LinearCombination<NColPower, std::string>> all_colour_structures(schematic);
        hierarchy = IntegrandHierarchy<T, L>(all_colour_structures.elements.front().second.elements.front().second);
    }
    select_slice(s);
    finish_construction();
}

template <typename T, size_t L> void AmplitudeLikeContribution<T, L>::finish_construction() {
    warmup_filename += ".";
    {
        using namespace settings;
        if (general::numerator_decomposition == general::IntegralBasis::tensors) { warmup_filename += "tensor"; }
        else {
            warmup_filename += wise_enum::to_string(settings::general::master_basis.value);
        }
    }
    warmup_filename += ".info";
    check_warmup();
}

template <typename T, size_t L> void AmplitudeLikeContribution<T, L>::select_hierarchy() {
    warmup_filename += std::to_string(multiplicity);
    std::string library_filename = std::string(DATADIR);

    // we get the hierarchy structure from the gluon process libraries
    std::vector<Particle> gluons(multiplicity,Particle("",ParticleType("gluon")));
    std::string schematic = load_schematic(YM{},PartialAmplitudeInput{gluons});

    LinearCombination<ColourStructure, LinearCombination<NColPower, std::string>> all_colour_structures(schematic);

    size_t size_colour(all_colour_structures.size());

    if(size_colour==0) _WARNING("Process library is empty!");

    hierarchy = std::move(IntegrandHierarchy<T, L>(all_colour_structures.elements.front().second.elements.front().second));
}

template <typename T,size_t L> void AmplitudeLikeContribution<T,L>::select_slice(const slice_selector& selector){
    HierarchySlice slice;
    switch(selector.type){
        case slice_selector::type_choices::all_children_with_ancestors_slice:
           slice = all_children_with_ancestors_slice(*hierarchy.graph_representation, selector.s_hierarchy_pos);
                break;
        case slice_selector::type_choices::with_ancestors_slice:
            slice = with_ancestors_slice(*hierarchy.graph_representation, selector.s_hierarchy_pos);
            break;
        case slice_selector::type_choices::all_down_to_level:
            slice = all_down_to_level(*hierarchy.graph_representation, selector.level);
            break;
        case slice_selector::type_choices::combined_all_ancestors:
            slice = combined_all_ancestors(*hierarchy.graph_representation, selector.v_hierarchy_pos);
            break;
    }

    hierarchy = IntegrandHierarchy<T,L>(hierarchy.graph_representation,std::move(slice));

    warmup_filename += to_string(selector);

}

template <typename T,size_t L> void AmplitudeLikeContribution<T,L>::add_numerator_evaluator(HierarchyPos pos, numerator_evaluator_type f){

    if(closed) throw std::runtime_error("An attemt to add numerator evaluators after closing.");

    {
        // Check if an iterable container v contains element el
        auto contains = [](const auto& v, const auto& el){
            using std::begin;
            using std::end;
            return std::find(begin(v), end(v), el) != end(v);
        };
        for(auto& gen : *hierarchy.test_slice){
            if(contains(gen, pos)){
                goto found;
            }
        }
        _WARNING("Trying to add numerator to pos ",pos," not in the slice");
        _MESSAGE(*hierarchy.test_slice);
        found:;
    }


    if(contributions.find(pos)!=contributions.end()){
        _WARNING("WARNING: numerator evaluator at position ", pos, " has been already added before, will overwrite!");
    }

    contributions[ pos ] = f;
}

template <typename T, size_t L> void AmplitudeLikeContribution<T, L>::add_numerator_evaluator(const lGraph::xGraph& gr, numerator_evaluator_type f) {
    using lGraph::xGraph;

    // For now we allow to insert only on graphs from the hierarchy which have the same orientation of external legs.
    // TODO: lift this restriction

    std::vector<std::decay_t<decltype(gr.get_base_graph())>> candidates;

    for (auto&& [gen_i, gen_obj] : enumerate(hierarchy.graph_representation->nodes)) {
        for (auto&& [top_i, top_obj] : enumerate(gen_obj)) {
            const auto& ref_graph = top_obj.get_graph();
            if (ref_graph.get_topology() == gr.get_topology()) {
                candidates.push_back(ref_graph.get_base_graph());

                if (ref_graph.get_base_graph() == gr.get_base_graph() and ref_graph.get_internal_momenta() == gr.get_internal_momenta()) {
                    if (verbosity_settings.process_library_construction) {
                        _MESSAGE("Found matching topology at pos ", std::make_pair(gen_i, top_i), ":\n", ref_graph, "for insertion of\n", gr);
                        auto ff = FunctionSpace::master_term_data_holder.find(ref_graph.get_topology());
                        if (ff != FunctionSpace::master_term_data_holder.end()) {
                            xGraph candidate_xGraph = std::get<xGraph>(ff->second);

                            _MESSAGE(" ... found defined master insertions for: \n", candidate_xGraph);

                            xGraph copy_relabeled_insertion = ref_graph.get_relabeled_copy(candidate_xGraph);
                            auto conventions_map_loop_master = OnShellStrategies::abstract_OnShellPointMap(ref_graph, copy_relabeled_insertion);
                            auto conventions_map_ext_master = lGraph::lGraphKin::abstract_external_momentum_mapper(copy_relabeled_insertion, candidate_xGraph);

                            _MESSAGE(math_list_with_head("ExternalMap", conventions_map_ext_master));
                            _MESSAGE(math_list_with_head("LoopMomentaMap", conventions_map_loop_master));
                        }
                    }
                    add_numerator_evaluator({gen_i, top_i}, std::move(f));
                    return;
                }
            }
        }
    }

    if (!candidates.empty()) {
        _MESSAGE("Found some topologically equivalaent graphs for in the hierarchy for graph\n", gr);
        _MESSAGE("but no matching external configuration!");
        _MESSAGE("The candidates are");
        for (const auto& it : candidates) { _MESSAGE(it, "\n", math_string(it)); }
    };

    throw std::runtime_error("Trying to add numerator for a graph not found in the hierarchy");
}

template <typename T, size_t L> void AmplitudeLikeContribution<T, L>::set_include_symmetry_factors(bool s) {
    if (closed) { _WARNING("WARNING: setting include_symmetry_factors after closing AmplitudeLikeContribution does nothing!"); }
    include_symmetry_factors = s;
}

template <typename T, size_t L> void AmplitudeLikeContribution<T, L>::close(const std::function<void(const momD_conf<T, 4>& conf)>& adjuster) {

    if(closed){
        _WARNING("Attempted to closed already closed AmplitudeLikeContribution. Did nothing.");
        return;
    }

    // truncate the slice to only contain direct ancestors of all topologies to where insertions have been made
    {
        auto new_slice = std::make_shared<HierarchySlice>(*hierarchy.test_slice);

        const auto& graph_hierarchy = *hierarchy.graph_representation; 

        std::vector<HierarchyPos> all_insertions;
        size_t top_level = 0;
        for(auto& it : contributions){
            if(it.first.first>top_level) top_level = it.first.first;
            all_insertions.push_back(it.first);
        }

        new_slice->erase(std::remove_if(new_slice->begin(), new_slice->end(), [top_level](const auto& gen) { return (gen.at(0).first > top_level); }),
                         new_slice->end());

        {
            auto& top_gen = new_slice->at(0);
            top_gen.erase(std::remove_if(top_gen.begin(), top_gen.end(),
                                         [&all_insertions](const auto& t) {
                                             return std::find(all_insertions.begin(), all_insertions.end(), t) == all_insertions.end();
                                         }),
                          top_gen.end());
        }

        for (size_t i = 1; i < new_slice->size(); ++i) {
            const auto& parent_gen = new_slice->at(i - 1);
            std::set<HierarchyPos> all_children;
            for (auto& it : parent_gen) {
                auto ch = hierarchy.graph_representation->get_child_positions(it);
                all_children.insert(ch.begin(), ch.end());
            }
            auto& gen = new_slice->at(i);
            gen.erase(std::remove_if(gen.begin(), gen.end(), [&all_children](const auto& t) { return all_children.count(t) == 0; }),
                      gen.end());
        }

        // add back subleading-pole topologies which are to be fit by any of the topologies we already have in the slice
        {
            std::map<size_t,std::set<size_t>,std::greater<size_t>> all_non_parametrizable;
            std::map<size_t,std::set<size_t>,std::greater<size_t>> all_other;
            for(auto& gen : *new_slice){
                for(auto& t : gen){
                    all_other[t.first].insert(t.second);
                    if(!graph_hierarchy[t].ask_related_cut()) continue; // do not add higher order parents of subleading-pole topologies
                    auto all_ancestors = graph_hierarchy.get_ancestors(t);
                    for(auto& it : all_ancestors){
                        const auto& topology = graph_hierarchy[it.first];
                        // we only go one level above, otherwise the topology does not contribute
                        if ( (it.first.first == t.first+1) and !topology.ask_related_cut()) all_non_parametrizable[it.first.first].insert(it.first.second);
                    }
                }
            }
            DEBUG_PRINT(all_non_parametrizable);
            DEBUG_PRINT(all_other);

            for(auto& it : all_non_parametrizable){
                all_other[it.first].insert(it.second.begin(),it.second.end());
            }

            DEBUG_PRINT(all_other);

            *new_slice = std::vector<std::vector<HierarchyPos>>(all_other.size());
            for(auto&& [i, obj] : enumerate(all_other)){
                for(auto& it2 : obj.second){
                    (*new_slice)[i].emplace_back(obj.first,it2);
                }
            }
        }

        DEBUG_PRINT(all_insertions);
        DEBUG_PRINT(*hierarchy.test_slice);
        DEBUG_PRINT(*new_slice);

        hierarchy.test_slice = new_slice;
        hierarchy.master_positions = std::make_shared<std::vector<MasterPosition>>(slice_master_positions(*hierarchy.graph_representation,*new_slice));
        hierarchy.init();
        if (!include_symmetry_factors) {
            for (auto& it : hierarchy.symmetry_factors) { it = 1; }
        }

        DEBUG(
            for(auto& it: (*hierarchy.test_slice)){
                for( auto& jt: it){
                    _MESSAGE("");
                    _MESSAGE("HierarchyPos: ",jt);
                    _MESSAGE((*hierarchy.graph_representation)[jt].get_graph());
                    _MESSAGE(math_string((*hierarchy.graph_representation)[jt].get_graph()));
                    _MESSAGE("Children: ",hierarchy.graph_representation->get_child_positions(jt));
                    _MESSAGE("Ancestors: ",hierarchy.graph_representation->get_ancestors(jt));
                    _MESSAGE("SF = ",(*hierarchy.graph_representation)[jt].get_symmetry_factor());
                }
            }
            _MESSAGE("Master positions:");
            for(unsigned i=1; i <= hierarchy.master_positions->size(); ++i){
                _MESSAGE(i,": ",hierarchy.master_positions->at(i-1));
            };
        );
    }


    ResidueEvaluator<T, T, D> re{[this](HierarchyPos pos, const momentumD_configuration<T, D>& phys, const OnShellPoint<T, D>& point) {

#ifdef DEBUG_CHEK_ONSHELLNESS
                                    (*hierarchy.graph_representation)[pos].assert_on_shell(phys, point); // Automatic on-shell checks.
#endif
                                     T result(0);
                                     auto this_contribution = contributions.find(pos);
                                     if (this_contribution != contributions.end()) { result += (this_contribution->second)(point, phys); }

                                     for (auto ancestor : hierarchy.graph_representation->get_ancestors(pos)) {
                                         auto ancestor_contribution = contributions.find(ancestor.first);
                                         if (ancestor_contribution != contributions.end()) {

                                             auto& ancestor_link = ancestor.second;
                                             auto denom = ancestor_link.template denominator<T>(phys, point);
                                             result += (ancestor_contribution->second)(ancestor_link.this_to_ancestor(phys, point), phys) / denom;
                                         }
                                     }

                                     return result;
                                 },
                                 adjuster};

    hierarchy.subtraction_engines.emplace_back(hierarchy.graph_representation, hierarchy.test_slice, re,  hierarchy.master_positions, hierarchy.warmup_info, verbosity_settings.subtraction_engine);

    //Construct parallelizatoin scheme

    // gen --> list of bunches, each bunch is a list of positions in gen
    std::vector<std::vector<std::vector<HierarchyPos>>> parallel_scheme;
    std::vector<std::vector<HierarchyPos>> bunches_on_gen;

    for(auto& gen : *(hierarchy.test_slice)){
        std::vector<std::vector<HierarchyPos>> bunches_on_gen;
        // FIXME: it makes sense to order the bunches on a sorted way, depending on the dimension of the function space
        auto bunches = split_into_bunches(gen.size());
        for(auto& b : bunches){
            std::vector<HierarchyPos> current_bunch;
            for (auto& it : b) {
                current_bunch.push_back(gen[it]);
            }
            bunches_on_gen.push_back(current_bunch);
        }
        parallel_scheme.push_back(std::move(bunches_on_gen));
    }

    hierarchy.subtraction_engines.back().assign_parallelization_scheme(parallel_scheme);

    closed = true;
}

template <typename T,size_t L> typename IntegrandHierarchy<T,L>::ReturnType_Coefficients AmplitudeLikeContribution<T,L>::get_master_coefficients(const momentumD_configuration<T,4>& mom_conf){
    auto r  = hierarchy.get_master_coefficients(mom_conf);

    if(need_warmup){
        write_warmup(warmup_filename,{hierarchy.warmup_info});
        for (auto& s : hierarchy.subtraction_engines) { s.build_hierarchy_function_bases_with_warmup(); }
        need_warmup = false;
    }

    return r;
}

} // namespace IntegrandHierarchy


bool is_vanishing_tree(const PartialAmplitudeInput& input) {
    unsigned n_gluons = input.get_number_of_gluons();
    unsigned n_photons = input.get_number_of_photons();
    unsigned n_quarks = input.get_number_of_quarks();
    unsigned n_p{0};
    unsigned n_m{0};
    for (auto& it : input.get_particles()) {
        if ((it.get_flavor() == ParticleFlavor::gluon or it.get_type() == ParticleType("G") or it.get_flavor() == ParticleFlavor::photon) and it.external_state().get_helicity() == helicity::p) ++n_p;
        if ((it.get_flavor() == ParticleFlavor::gluon or it.get_type() == ParticleType("G") or it.get_flavor() == ParticleFlavor::photon) and it.external_state().get_helicity() == helicity::m) ++n_m;
    }

    // FIXME: Is there a general procedure to take massive quarks into account?
    // For now, we treat massive quarks always as non-zero
    for (auto& it : input.get_particles()) {
        if (it.get_flavor() == ParticleFlavor::t or it.get_flavor() == ParticleFlavor::tb) return false;
    }

    // hard code some known cases when tree vanishes
    if (n_quarks > 0) {
        if ((n_gluons > 1 || n_photons > 1) && (n_p == n_gluons or n_m == n_gluons or n_p == n_photons or n_m == n_photons)) return true;
    }
    else if (n_p >= (n_gluons - 1) or n_m >= (n_gluons - 1)) {
        return true;
    }

    // photons have to couple to somethig with QED charge at tree level
    if (n_photons > 0) {
        unsigned n_qed_charged = 0;
        for (auto& p : input.get_particles()) {
            if (QED_charge(p.get_flavor()).first != 0) ++n_qed_charged;
        }
        if (n_qed_charged == 0) return true;
    }

    return false;
}

  
} // namespace Caravel

namespace Caravel{ namespace IntegrandHierarchy{

#define _INSTANTIATE(T) \
    template class ColourExpandedAmplitude<T,2>;    \
    template class IntegrandHierarchy<T,2,T>;   \
    template class IntegrandHierarchy<T,2,Vec<T>>;    \
    template class AmplitudeLikeContribution<T,2>;    \

#ifdef USE_FINITE_FIELDS
_INSTANTIATE(F32)
#endif

_INSTANTIATE(C)
#ifdef HIGH_PRECISION
#ifdef HIGH_PRECISION
_INSTANTIATE(CHP)
#endif
#endif
#ifdef VERY_HIGH_PRECISION
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE(CVHP)
#endif
#endif

}}
