#include "AncestorLinks.h"
#include <vector>
#include <string>
#include <iostream>
#include "Forest/Forest.h" 
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/MathInterface.h"

namespace Caravel{

MomentumSum::MomentumSum(std::string schematic){
  MathList momentum_sum(schematic, "SUM");
  for (auto& mom : momentum_sum){
    MathList momenta(mom);
    int mom_index = std::stoi(momenta[0]);

    if (momenta.head == "PLOOP"){
      combination.push_back({Sign::POSITIVE, -mom_index});
    }
    if (momenta.head == "NLOOP"){
      combination.push_back({Sign::NEGATIVE, -mom_index});
    }
    if (momenta.head == "PINTERNAL"){
      combination.push_back({Sign::POSITIVE, mom_index});
    }
    if (momenta.head == "NINTERNAL"){
      combination.push_back({Sign::NEGATIVE, mom_index});
    }
  }
}

std::ostream& operator <<(std::ostream& out, const MomentumSum& mom_sum){
  for (auto mom : mom_sum.combination){
    if (mom.first == Sign::POSITIVE) out << "+";
    if (mom.first == Sign::NEGATIVE) out << "-";

    if (mom.second < 0) out << "l" << (-mom.second);
    else out << "k" << (mom.second);
  }

  return out;
}

bool MomentumSum::operator==(const MomentumSum& other){
  if (combination.size() != other.combination.size()){return false;}

  for (size_t i = 0; i < combination.size(); i++){
    if (combination.at(i) != other.combination.at(i)){return false;}
  }

  return true;
}

ExtraPropagatorInfo::ExtraPropagatorInfo(std::string schematic) : prop_mom() {
  MathList prop_info(schematic, "PropInfo");
  prop_mom = prop_info[0];
}

bool ExtraPropagatorInfo::operator==(const ExtraPropagatorInfo& other){
  return prop_mom == other.prop_mom;
}

std::ostream& operator <<(std::ostream& out, const ExtraPropagatorInfo& prop){
  out << "(";
  out << prop.prop_mom;
  out << ")^2";
  return out;
}




}
