#include "AmplitudeCoefficients/SubtractionEngine.h"
#include "Core/Utilities.h"
#include "Core/MathInterface.h"
#include "Core/typedefs.h"
#include "Core/InputParser.h"
#include "Core/Vec.h"
#include <fstream>
#include <string>
#include <vector>

namespace Caravel {

HierarchyWarmupInfo warmup_from_schematic(std::string info_string) {
    // TODO replace all I/O by the universal InputParser::parse_MathList 
    MathList info_list(info_string, "WarmupInfo");
    MathList ds_value(info_list[0], "Ds");
    MathList truncation_list(info_list[1], "Hierarchy");
    auto master_denominators = InputParser::parse_MathList<std::vector<DensePolynomial<BigRat>>>(MathList(info_list[2],  "MasterDenominators"));
    auto numerator_ranks = InputParser::parse_MathList<std::vector<int>>(MathList(info_list[3], "MasterNumeratorRanks"));

    FunctionSpace::HierarchyBasisTruncation hierarchy_truncation;
    for (auto& gen_string : truncation_list) {
        MathList gen_truncation_list(gen_string, "Generation");
        std::vector<FunctionSpace::BasisTruncation> gen_truncation;
        for (auto& truncation_string : gen_truncation_list) {
            FunctionSpace::BasisTruncation truncation;
            MathList truncation_list(truncation_string, "Truncation");
            for (auto& el : truncation_list) { truncation.push_back(static_cast<size_t>(stoi(el))); }
            gen_truncation.push_back(truncation);
        }
        hierarchy_truncation.push_back(gen_truncation);
    }

    return std::make_tuple(hierarchy_truncation, master_denominators, numerator_ranks);
}

std::vector<HierarchyWarmupInfo> load_warmups(std::string warmup_filename) {
    using std::make_tuple;
    using std::vector;

    std::ifstream warmup_file(warmup_filename);
    if (!warmup_file.is_open()) {
        _WARNING("Could not find warmup file. Returning empty warmup.");
        return {};

    } else {

        std::stringstream string_buffer;
        string_buffer << warmup_file.rdbuf();
        warmup_file.close();

        auto infos_string = string_buffer.str();
        MathList infos_list(infos_string, "WarmupInfos");
        std::vector<HierarchyWarmupInfo> warmups;
        for (auto& info_string : infos_list) { warmups.push_back(warmup_from_schematic(info_string)); }

        return warmups;
    }
}

namespace{
    std::string riffle_comma(const std::vector<std::string>& list){
        std::string result = "";
        bool first = true;
        for (auto& el : list){
            if (!first){
                result += ",";
            }
            first = false;
            result += el;
        }
        return result;
    }
}


void write_warmup(std::string warmup_file, const std::vector<std::shared_ptr<HierarchyWarmupInfo>>& infos) {

    using std::vector;
    using std::string;
    using std::ofstream;
    using std::to_string;

    ofstream output_file(warmup_file);

    size_t ds = 6;

    output_file << "WarmupInfos[";
    for (auto info : infos) {
        output_file << "WarmupInfo[";
        output_file << "Ds[" << ds++ << "],";

        vector<string> all_gen_strings;
        for (auto& gen_truncation : std::get<0>(*info)) {

            vector<string> this_gen_strings;
            for (auto& node : gen_truncation) {
                vector<string> truncation_strings;
                for (auto& el : node) { truncation_strings.push_back(std::to_string(el)); }
                this_gen_strings.push_back(string("Truncation[") + riffle_comma(truncation_strings) + "]");
            }

            all_gen_strings.push_back(string("Generation[") + riffle_comma(this_gen_strings) + "]");
        }

        output_file << "Hierarchy[" << riffle_comma(all_gen_strings) << "],\n";

        output_file << math_list_with_head("MasterDenominators",std::get<1>(*info)) << ",\n";

        output_file << math_list_with_head("MasterNumeratorRanks", std::get<2>(*info)) << ",\n";

        output_file << "]";
    }
    output_file << "]";
}

_INSTANTIATE_SUBTRACTION_ENGINE(,C)
#ifdef HIGH_PRECISION
_INSTANTIATE_SUBTRACTION_ENGINE(,CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_SUBTRACTION_ENGINE(,CVHP)
#endif
#ifdef USE_FINITE_FIELDS
_INSTANTIATE_SUBTRACTION_ENGINE(,F32)
#endif
}
