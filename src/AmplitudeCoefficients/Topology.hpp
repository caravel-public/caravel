#include "Forest/Forest.h"
#include "Core/Timing.h"

namespace Caravel{

// Utility function to permute a vector given a (1 indexed) permutation 
template<typename T>
std::vector<T> permute_vector(std::vector<T> input, std::vector<size_t> permutation){
    std::vector<T> permuted;

    for (auto mom_index : permutation){
        // mom_index is a momentum index, not an array index. Hence the
        // -1
        permuted.push_back(input[mom_index-1]);
    }

    return permuted;
}


template < template <class> class Decomposition, size_t L >
int Topology<Decomposition, L>::get_multiplicity() const{return multiplicity;}
template < template <class> class Decomposition, size_t L >
bool Topology<Decomposition, L>::ask_related_cut() const{ return related_cut; }
template < template <class> class Decomposition, size_t L >
bool Topology<Decomposition, L>::is_parameterizable() const{ return parameterizable; }


template < template <class> class Decomposition, size_t L > Topology<Decomposition, L>::Topology(std::string schematic){
    MathList topology_info(schematic, "TopologyInfo");

    {
        int new_loop_order;
        try{
            new_loop_order = std::stoi(topology_info[0]);
        }
        catch(...){
            std::cerr << "Error, expected integer at first position in TopologyInfo, "
                << "representing the loop order. Instead found:" << std::endl
                << topology_info[0] << std::endl << "ABORTING..."<< std::endl;
            exit(1);
        }

        if (new_loop_order != L){
            std::cerr << "ERROR: Trying to build a " << L << "-loop topology with a " 
                << new_loop_order <<"-loop schematic.  Aborting...";

            exit(1);
        }
    }

    try{
        symmetry_factor = std::stoi(MathList(topology_info[2],"SymmetryFactor")[0]);
    }
    catch(...){
        throw std::logic_error("Unexpected input in place where SymmetryFactor is expected.");
    }

    if (topology_info[3] == "True"){ related_cut = false; }
    else if (topology_info[3] == "False"){ related_cut = true; }
    else {
        std::cerr << "Error: Expected True or False for related_cut in TopologyInfo." << std::endl;
        std::cerr << "Instead found: " << topology_info[3] << std::endl;
        exit(1);
    }
    if (topology_info[4] == "True"){ parameterizable = false; }
    else if (topology_info[4] == "False"){ parameterizable = true; }
    else {
        std::cerr << "Error: Expected True or False for parameterizability in TopologyInfo." << std::endl;
        std::cerr << "Instead found: " << topology_info[4] << std::endl;
        exit(1);
    }

    // check if the cut is a linear combination, if we find only a single diagram, we wrap it inside a trivial linear combination
    Decomposition<Cut<L>> new_decomposition;
    MathList math_list(topology_info[5]);

    if(math_list.head == "LC"){
        new_decomposition =Decomposition<Cut<L>>(topology_info[5]);
    }

    else if(math_list.head == "Diag"){
        std::string wrapped_schematic = "LC[Prod["+ topology_info[5] + ",Compl[Rat[1, 1], Rat[0, 1]]]]";
         new_decomposition =Decomposition<Cut<L>>(wrapped_schematic);
    }
    else{
        _WARNING_R("ERROR: Recieved head",math_list.head, ". Expected LC, Diag.");
        std::exit(1);
    }

    // propagate I inside Cuts so it can be later added to normalization in Builders
    for(auto& e : new_decomposition.decomposition.elements){
        if(e.second.is_imaginary()){
            e.first.set_pure_imaginary();
            e.second.extract_I();
        }
        else if (e.second.is_real()){
        }
        else{
            _WARNING_R("Full complex constant for a cut: not ready to handle that yet!");
            _WARNING("\t",e);
            std::exit(1);
        }
    }

    decomposition = new_decomposition;

    auto sample_cut = decomposition[0];

    // Calculate the multiplicity/number of points
    multiplicity = 0;
    for (auto& vertex : sample_cut.get_vertices()){
        auto vertex_externals = vertex.count_externals();
        multiplicity += vertex_externals;
    }

    // Build the physical info
    using RungMomentaIDs = std::vector<std::vector<int>>;

    // Get correct format of rungs from graph
    auto phys_rungs = [&sample_cut]() {
        switch (L) {
            case 1: return sample_cut.get_rungs();
            // More complicated at two-loops as the input data is not
            // specified in the correct format. The middle rung has has
            // connecting vertices in it, which are supposed to be in
            // the final entry of phys_rungs.
            case 2: {
                auto original_rungs = sample_cut.get_rungs();

                std::vector<Rung> new_rungs = original_rungs;

                if (original_rungs.at(1).size() == 1) {
                    auto& original_middle_rung = original_rungs.at(1).front();
                    new_rungs.push_back({original_middle_rung});
                    // Middle rung now lives at the end, so we *EMPTY* it.
                    new_rungs.at(1).erase(new_rungs.at(1).begin());
                }
                else {
                    // Here we push the connecting vertices to the end of the rung list. 
                    auto& new_middle_rung = new_rungs.at(1);
                    auto& original_middle_rung = original_rungs.at(1);

                    // Move top and bottom vertex to new end "rung".
                    new_middle_rung.erase(new_middle_rung.begin());
                    new_middle_rung.pop_back();
                    new_rungs.push_back({original_middle_rung.front(), original_middle_rung.back()});
                }

                return new_rungs;
            }
            default: _WARNING_R("Not implemented!"); std::exit(1);
        }
    }();


    // this remainded from very old times
    std::vector< std::vector< std::vector<int>>> physical_info;

    for (auto& rung : phys_rungs){
        RungMomentaIDs this_rung;
        for (auto& vertex : rung){
            this_rung.push_back(vertex.external_momentum_ids());
        }
        physical_info.push_back(this_rung);
    }

    {
        using namespace lGraph;

        auto get_Legs= [](const std::vector<int>& in) -> std::vector<Leg> {
            //TODO here mass index is set to zero, make generic
            std::vector<Leg> legs;
            for(auto& it : in){
                legs.emplace_back(it,0);
            }
            return legs;
        };

        auto get_Strand = [get_Legs](const std::vector<std::vector<int>>& in, int li = 0, bool at_begin = true) -> Strand {
            std::vector<Bead> beads;
            std::vector<Link> links;

            if(li==0 or (li!=0 and !at_begin)){
                links.emplace_back();
            }
            else if (at_begin)  {
                links.emplace_back(LoopMomentum{li});
            }

            for(auto& it : in){
                beads.emplace_back(get_Legs(it));
                links.emplace_back();
            }

            if(li!=0 and !at_begin){
                if(links.empty()) links.emplace_back(LoopMomentum{li});
                else{
                    links.back() = LoopMomentum{li};
                }
            }

            return {beads,links};
        };

        if(physical_info[3].size()>=2){
            graph = xGraph({{Node{get_Legs(physical_info[3].at(0))}, Node{get_Legs(physical_info[3].at(1))}},
                    Connection<Strand>{{get_Strand(physical_info[0], 1, true), get_Strand(physical_info[1]), get_Strand(physical_info[2], -2, false)}}});
        }
        else{
            graph = xGraph({{Node{get_Legs(physical_info[3].at(0))}},
                    Connection<Strand>{{get_Strand(physical_info[0], 1, true), get_Strand(physical_info[2], -2, false)}}});
        }
    }

    if(L == 2){
        // Top/Bottom entries must be zero momenta if they are not present.
        for (size_t i = physical_info[3].size(); i < 2; i++){
            physical_info.back().push_back({});
        }
    }



    {
        size_t new_num_propagators; 
        try{
            new_num_propagators = std::stoul(topology_info[1]);
        }
        catch(...){
            std::cerr << "Error, expected integer at second position in TopologyInfo, "
                << "representing the number of propagators. Instead found:" << std::endl
                << topology_info[1] << std::endl << "ABORTING..."<< std::endl;
            exit(1);
        }
        if (new_num_propagators != graph.get_n_edges()){
            _WARNING_R("Number of propagators ",new_num_propagators, " does not match the number of edges ",graph.get_n_edges()," in the graph!");
            _PRINT(*this);
            graph.show();
            std::exit(1);
        }
    }


}

template <size_t L> template <typename T> void Cut<L>::add_to_forest(Forest& f, const std::vector<Particle>& input) {
    if constexpr (is_floating_point_v<T>) {
        add_to_forest(f,input, settings::BG::trace_scheme::full);
    }
    else if constexpr (is_exact_v<T>){
        add_to_forest(f,input, settings::BG::trace_scheme::reduced);
    }
    else {
        static_assert(_delayed_static_false_v<T>, "Not implemented!");
    }
}


template <template <class> class Decomposition, size_t L> std::ostream& operator<<(std::ostream& out, const Topology<Decomposition, L>& cut) {
    out << cut.get_graph() << std::endl;
    out << "Loop order: " << L << std::endl;
    out << "Is parameterizable: " << cut.is_parameterizable() << std::endl;
    out << cut.get_decomposition().decomposition;
    return out;
}

template <template <class> class Decomposition, size_t L >
template <typename T, size_t D>
  std::vector<T> Topology<Decomposition, L>::propagator_values(const momentumD_configuration<T,D>& physical_mom_conf, OnShellPoint<T,D> loop_point, bool report_info) const{

      auto cut = decomposition[0];

      std::vector<T> prop_values;

      momentumD<T,D> prop_momentum = loop_point.at(0);
      if (report_info){
          std::cout << "prop_momentum: " << prop_momentum << std::endl;
          std::cout << "square: " << prop_momentum*prop_momentum << std::endl;
      }

      prop_values.push_back(prop_momentum*prop_momentum);

      size_t count_loops = 0; 
      for ( auto vertex : cut ){
          auto vertex_externals = vertex.external_momentum_ids();
          if (report_info){std::cout << "Adding external momenta in a vertex:" << vertex_externals << std::endl;}
          for(auto mind : vertex_externals){
              prop_momentum -= physical_mom_conf(mind);
          }
          if (vertex.count_internals() == 3){
              count_loops++;
              if (count_loops > L){
                  std::cerr << "ERROR. TOO MANY LOOP LEGS ENCOUNTERED IN ON SHELL CHECK." <<std::endl;
                  exit(1);
              }
              else if (count_loops == 1){
                  if (report_info){std::cout << "Subtracting l2" << std::endl;}
                  prop_momentum -= loop_point.at(1);
              }
              else if (count_loops == 2){
                  if (report_info){std::cout << "Subtracting l1" << std::endl;}
                  prop_momentum -= loop_point.at(0);
              }
          }
          else if (vertex.count_internals() == 4){
              prop_momentum -= loop_point.at(0);
              prop_momentum -= loop_point.at(1);
          }

          if (report_info){
              std::cout << "prop_momentum: " << prop_momentum << std::endl;
              std::cout << "square: " << prop_momentum*prop_momentum << std::endl;
          }

          // Do the pushback here
          prop_values.push_back(prop_momentum*prop_momentum);

      }

      return prop_values;


}

template <template <class> class Decomposition, size_t L>
template <typename T, size_t D>
std::vector<T> Topology<Decomposition, L>::evaluate(Builder<T, D>& builder,const bool& always_recompute) const {
    START_TIMER_T(evaluation_of_cuts,T);

#ifndef DS_BY_PARTICLE_CONTENT
    const size_t n = builder.get_cut_dimensions(decomposition.decomposition.elements.front().first.get_first_forest_position()).size();

    std::vector<T> result(n, T(0));
    for (const auto& term : decomposition.decomposition.elements) {
        //auto cut_id = term.first.get_forest_position();
        auto const_val = term.second.template get_num<T>();
        auto cut_val = term.first.evaluate(builder, always_recompute);
        for (size_t i = 0; i < n; i++) { result[i] += (cut_val[i] *= const_val); }
    }
#else
    std::vector<T> result(MAX_DS_COEFFICENTS, T(0));
    result.shrink_to_fit();
    for (const auto& term : decomposition.decomposition.elements) {
        //auto cut_id = term.first.get_forest_position();
        auto const_val = term.second.template get_num<T>();
        auto cut_val = term.first.evaluate(builder, always_recompute);
        const size_t size = cut_val.size();
        for (size_t i = 0; i < size; i++) { result[i] += (cut_val[i] *= const_val); }
    }
#endif

    STOP_TIMER(evaluation_of_cuts);

    return result;
}

template <size_t L> template <typename T, size_t D> std::vector<T> Cut<L>::evaluate(Builder<T, D>& builder,const bool& always_recompute) const {
    // here we assume that join cuts was called on all forest cutids of the diagram,
    // so these two steps should be performed only on one of them
    if(always_recompute) builder.compute(); 
    builder.compute_cut(get_first_forest_position());

#ifndef DS_BY_PARTICLE_CONTENT
    const size_t n = builder.get_cut_dimensions(get_first_forest_position()).size();
    std::vector<T> result(n,T(0));

    for (auto cut_id : forest_positions.forest_ids) {
        builder.contract_cut(cut_id);
        auto cut_val = builder.get_cut(cut_id);
        for (size_t i = 0; i < n; i++) { result[i] += cut_val[i]; }
    }
#else
    const size_t result_size = forest_positions.forest_ids_ds.size()+1;
    std::vector<T> result(result_size,T(0));

    for (auto cut_id : forest_positions.forest_ids) {
        builder.contract_cut(cut_id);
        result[0] += builder.get_cut(cut_id).front();
    }
    for(size_t i = 1; i < result_size; ++i){
        const auto& elements = forest_positions.forest_ids_ds[i-1].elements;
        for(const auto& it: elements){
            T c = it.first.template get_num<T>();
            for (auto cut_id : it.second) {
                builder.contract_cut(cut_id);
                result[i] += c*builder.get_cut(cut_id).front();
            }
        }
    }
#endif

    return result;
}

namespace detail {
/**
 * Internal function to check if numerical types are
 * equal. Semantics are different for floating point and exact
 * numbers.
 */

bool is_zero(const C& x);
#ifdef HIGH_PRECISION
bool is_zero(const CHP& x);
#endif
#ifdef VERY_HIGH_PRECISION
bool is_zero(const CVHP& x);
#endif

#ifdef USE_FINITE_FIELDS
  template<typename T> 
  std::enable_if_t<is_exact<T>::value, bool> is_zero(const T& x){
    return x == T(0);
  }

  template<typename T> 
  std::enable_if_t<is_exact<T>::value, bool> is_zero(const std::complex<T>& x){
    return x.real() == T(0) and x.imag() == T(0);
  }
#endif //USE_FINITE_FIELDS

}

template < template <class> class Decomposition, size_t L >
template< typename T, size_t D>
  void Topology<Decomposition, L>::assert_on_shell(const momentumD_configuration<T,D>& physical_mom_conf, OnShellPoint<T,D> loop_point, bool report_info) const{

      auto prop_values = propagator_values(physical_mom_conf, loop_point, false);

      for (size_t i = 0; i < prop_values.size(); i++ ){
        auto prop_val = prop_values.at(i);
	if (!detail::is_zero(prop_val)){
		std::cout << "Inside topology:" << std::endl;
		std::cout << *this << std::endl;
		std::cout << "ON SHELLNESS ERROR. PROPAGATOR " << i << " SQUARES TO: " << prop_val << std::endl;
		propagator_values(physical_mom_conf, loop_point, true);
		exit(1);
	}
	else if (report_info){std::cout << "Test passed: "<< prop_val << std::endl;}
      }
}

// [/END Topology]
}
