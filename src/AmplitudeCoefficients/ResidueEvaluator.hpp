namespace Caravel{
    template<typename TF, typename T, size_t D>
    ResidueEvaluator<TF, T, D>::ResidueEvaluator(
	std::function<TF(HierarchyPos, const momentumD_configuration<T,D>&,
			 const OnShellPoint<T,D>&)> _evaluator,
	std::function<void(const momentumD_configuration<T,4>&)> _adjuster) : evaluator(_evaluator), adjuster(_adjuster) {}

    template<typename TF, typename T, size_t D>
    TF ResidueEvaluator<TF, T, D>::operator()(HierarchyPos pos, const momentumD_configuration<T,D>& phys, const OnShellPoint<T,D>& loop) const{
      return evaluator(pos, phys, loop);
    }

    template<typename TF, typename T, size_t D>
    void ResidueEvaluator<TF, T, D>::change_point(const momentumD_configuration<T,4>& new_point){
      adjuster(new_point);
    }
  
}
