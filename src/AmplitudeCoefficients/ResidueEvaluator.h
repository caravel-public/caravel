#include "Core/momD_conf.h"
#include "AmplitudeCoefficients/Hierarchy.h"
#include "Forest/Forest.h"

#ifndef RESIDUE_EVALUATOR_H_INC
#define RESIDUE_EVALUATOR_H_INC
namespace Caravel{

    /**
     * A prescription for evaluating a residue of an amplitude-like
     * rational function. Main purpose is to tell the subtraction
     * engine what to do. Should be "unitary".
     */
  template<typename TF, typename T, size_t D>
  class ResidueEvaluator{

    /**
     * Performs evaluation logic
     */
    std::function<TF(HierarchyPos,
		     const momentumD_configuration<T,D>&, const OnShellPoint<T,D>&)> evaluator;

    /**
     * Updates state dependent on external phase space point
     */
    std::function<void(const momentumD_configuration<T,4>&)> adjuster;

    public:
    ResidueEvaluator() = default;
    /**
     * Construct from a prescription of how to evaluate any given residue
     */
    ResidueEvaluator(std::function<TF(HierarchyPos,
				      const momentumD_configuration<T,D>&,
				      const OnShellPoint<T,D>&)> evaluator,
		     std::function<void(const momentumD_configuration<T,4>&)> adjuster);

    /** 
     * Evaluate the requested residue on a given point.
     */
    TF operator()(HierarchyPos, const momentumD_configuration<T,D>&, const OnShellPoint<T,D>&) const;
    /**
     * If the ResidueEvaluator manages some (external) phase space
     * dependent state, which only needs to be updated when the
     * (external) phase space point changes, then call this function.
     */

    void change_point(const momentumD_configuration<T,4>& new_point);

    decltype(adjuster) get_adjuster() const {return adjuster;}
  };

}

#include "ResidueEvaluator.hpp"

#endif //RESIDUE_EVALUATOR_H_INC
