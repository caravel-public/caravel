#ifndef NUMERICALTOOLS_H_INC
#define NUMERICALTOOLS_H_INC

#include <vector>
#include <functional>
#include <random>

#include "AmplitudeCoefficients/Topology.h"
#include "Forest/Builder.h"
#include "AncestorLinks.h"
#include "FunctionSpace/FunctionSpace.h"
#include "Core/Vec.h"

#include "Core/typedefs.h"

namespace Caravel{

template<typename T, typename X>
void check_function_equality(std::function<T(X)> f, std::function<T(X)> g, std::function<X()> xgen);


  /**
   * Fits the set of functions over the supplied bases. Mainly a
   * function which dispatches to the true logic depending on the
   * appropriate settings. The function f takes values in TF, usually
   * expected to be either T or std::vector<T>.
   *
   * Note that the point_gen is assumed to store internally a reference
   * to some external random number generator, and its state will be always
   * updated whenever point_gen (or a copy of it) is invoked.
   * If this is not the case, N=N test will not be working properly.
   */
 template<typename TF, typename T, typename X>
  std::vector<std::vector<TF>> fit_functions(std::function<TF(X)> f, const std::function<X()>& point_gen,
				       std::vector<std::function<std::vector<T>(X)>> list_basis_functions,
				       std::vector<size_t> list_basis_sizes,
				       bool remove_zeros = true, size_t container_dimension = 0 /* default value means scalar TF */);

    namespace detail{
      /**
       * Utility function which converts arbitrary (real) floating
       * point type to integer with uniform interface.
       */
      int convert_to_int(double x);
      int convert_to_int(dd_real x);
      int convert_to_int(qd_real x);

      /**
       * Utility function which sets scale of "zero-ness" for
       * (complex) floating point type with uniform interface.
       */
      R non_zero_zero_scale(C x);
#ifdef HIGH_PRECISION
      RHP non_zero_zero_scale(CHP x);
#endif
#ifdef VERY_HIGH_PRECISION
      RVHP non_zero_zero_scale(CVHP x);
#endif

	/**
	* Internal function to check if numerical types are
	* equal. Semantics are different for floating point and exact
	* numbers.
	*/
#ifdef HIGH_PRECISION
	bool are_equal(const CHP& x, const CHP& y);
#endif
#ifdef VERY_HIGH_PRECISION
	bool are_equal(const CVHP& x, const CVHP& y);
#endif
    }


#ifdef USE_FINITE_FIELDS
  std::vector<size_t> non_zero_positions(const std::vector<Vec<F32>>& xs, const std::vector<size_t>& pos_list);
  std::vector<size_t> non_zero_positions(const std::vector<F32>& xs, const std::vector<size_t>& pos_list);
#endif

}

#include "NumericalTools.hpp" // Source


#endif // NUMERICALTOOLS_H_INC
