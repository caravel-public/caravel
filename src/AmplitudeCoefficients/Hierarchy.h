#ifndef HIERARCHY_H_INC
#define HIERARCHY_H_INC

#include "Core/MathInterface.h"
#include "Core/Utilities.h"
#include <vector>
#include <functional>
#include <algorithm>
#include <sstream>
#include <unordered_set>

#include "Core/Debug.h"

namespace Caravel {

using HierarchyPos = std::pair<size_t, size_t>;
using HierarchySlice = std::vector<std::vector<HierarchyPos>>;

// Hierarchy graph data structure
 template < typename Node, typename Edge >
class HierarchyGraph{
    public:
        template <typename S> using NodeArray = std::vector< std::vector <S> >;
        using NodeType = Node;
        // TODO - Make private, but accessible to the functor
        // constructor using friends.
        NodeArray< std::vector< size_t > > children;
        NodeArray< std::vector< HierarchyPos > > ancestors;
        NodeArray<Node> nodes;
	NodeArray<std::vector<Edge> > upward_edges; // Empty for parent row.

        HierarchyGraph() = default;
        HierarchyGraph(const std::string& schematic );

        // Functor constructor
        template <typename S> 
        HierarchyGraph( HierarchyGraph<S, Edge> h, std::function<Node(S)> f ) : 
            children(h.children), ancestors(h.ancestors){
            nodes.resize(h.nodes.size());

            for (size_t gen_i = 0; gen_i < h.nodes.size(); gen_i++){
                for (size_t col_i = 0; col_i < h.nodes.at(gen_i).size(); col_i++){
                    auto old_node = h[ {gen_i, col_i} ];
                    auto new_node = f( old_node );
                    nodes.at(gen_i).push_back( new_node );
                }
            }

        }

        HierarchyGraph( NodeArray<Node> nodes, NodeArray<std::vector<
        size_t>> children, NodeArray<std::vector<HierarchyPos>> ancestors,
        NodeArray<std::vector<Edge> > upward_edges);

        std::vector< HierarchyPos > get_child_positions(HierarchyPos parent) const;

        std::vector< std::pair<HierarchyPos, Edge> > get_ancestors(HierarchyPos child) const;

        Node& operator[] (HierarchyPos pos){
            return nodes.at(pos.first).at(pos.second);
        }

        const Node& operator[] (HierarchyPos pos) const{
            return nodes.at(pos.first).at(pos.second);
        }

        const std::string details() const;
	size_t num_nodes() const;
};

// Initializing constructor
template <typename Node, typename Edge>
HierarchyGraph<Node, Edge>::HierarchyGraph( NodeArray<Node> new_nodes, NodeArray<std::vector<size_t>> new_children, NodeArray<std::vector<HierarchyPos>> new_ancestors, NodeArray<std::vector<Edge>> new_upward_edges) :
 children(new_children), ancestors(new_ancestors), nodes(new_nodes), upward_edges(new_upward_edges)  {}

template <typename Node, typename Edge>
HierarchyGraph<Node, Edge>::HierarchyGraph(const std::string& schematic ){

    MathList hierarchy_info(schematic);

    // First extract the Nodes into nodes.
    MathList ts_info(hierarchy_info.tail.at(0));

    for (size_t gen_i = 0; gen_i < ts_info.tail.size(); gen_i++){
        DEBUG_MESSAGE("Loading hierarchy generation ",gen_i,"." );
        auto& gen = ts_info.tail.at(gen_i);
        MathList gen_info(gen);
        std::vector<Node> new_node_gen;
        for (size_t t_i = 0; t_i < gen_info.tail.size(); t_i++){
            DEBUG_MESSAGE("Loading node ",t_i,".");
            auto& t_info = gen_info.tail.at(t_i);
            Node new_Node(t_info);
            new_node_gen.push_back(new_Node);

        }
        nodes.push_back(new_node_gen);
    }

    // Helper function for reading mathematica array indices as cpp
    // array indices. 

    auto string_to_cpp_indices = [](std::string s) ->
    std::vector<size_t> {
        MathList index_strings(s);
        std::vector<size_t> result;

        std::transform(index_strings.tail.begin(),
        index_strings.tail.end(),
        std::back_inserter(result), math_string_to_cpp_index);

        return result;
    };


    // Next extract the children info into children
    MathList child_info(hierarchy_info.tail.at(1));
    for (auto& gen : child_info){
        MathList gen_info(gen);
        std::vector< std::vector<size_t> > new_child_gen;
        for (auto& child_info : gen_info){
            auto child_indices = string_to_cpp_indices(child_info);
            new_child_gen.push_back(child_indices);
        }
        children.push_back(new_child_gen);
    }

    // Next extract the parent info into parents.
    MathList full_ancestor_info(hierarchy_info.tail.at(2));
    for (auto& gen : full_ancestor_info){
        MathList gen_info(gen);
        std::vector< std::vector<HierarchyPos> > new_ancestor_gen;
        std::vector< std::vector<Edge> > new_upward_edge_gen;
        for (auto& parents : gen_info){
	    std::vector<HierarchyPos> new_ancestor_list;
	    std::vector<Edge> new_upward_edge_list;
	    MathList ancestors_info(parents, "Ancestors");
	    for (auto& ancestor : ancestors_info){
	      MathList ancestor_info(ancestor, "Ancestor");
	      MathList ancestor_pos(ancestor_info[0], "Pos");
	      auto upward_edge_schematic = ancestor_info[1];
	      Edge upward_edge(upward_edge_schematic);
	      
	      new_ancestor_list.push_back({math_string_to_cpp_index(ancestor_pos[0]),math_string_to_cpp_index(ancestor_pos[1])});
	      new_upward_edge_list.push_back(upward_edge);
	    }
            new_ancestor_gen.push_back(new_ancestor_list);
            new_upward_edge_gen.push_back(new_upward_edge_list);

        }
        ancestors.push_back(new_ancestor_gen);
        upward_edges.push_back(new_upward_edge_gen);
    }
}

template< typename Node, typename Edge>
const std::string HierarchyGraph<Node, Edge>::details() const{
    std::stringstream details_builder;
    details_builder << "Num generations: " << nodes.size() << "\n";

    details_builder << "Members per generation: (";
    bool first = true;
    for (auto gen : nodes){
        if (!first) details_builder << ", ";
        details_builder << gen.size();
        first = false;
    }
    details_builder << ") \n";
    
    return details_builder.str();
}

template< typename Node, typename Edge >
size_t HierarchyGraph<Node, Edge>:: num_nodes() const{

    size_t count = 0;
    for (auto& row : nodes){
       count += row.size();
    }

    return count;
}

template< typename Node, typename Edge >
std::vector< HierarchyPos > HierarchyGraph<Node, Edge>::get_child_positions(HierarchyPos pos) const{
    auto row = pos.first, column = pos.second;
    auto& children_indices = children.at(row).at(column);
    
    std::vector< HierarchyPos > children_positions;

    for (auto& index : children_indices){
        children_positions.push_back({row-1,index});
    }

    return children_positions;
}


template< typename Node, typename Edge>
  std::vector< std::pair<HierarchyPos, Edge> > HierarchyGraph<Node, Edge>::get_ancestors(HierarchyPos pos) const{
    auto row = pos.first, column = pos.second;
    auto& this_ancestors = ancestors.at(row).at(column);
    auto& this_edges = upward_edges.at(row).at(column);
    std::vector<std::pair<HierarchyPos, Edge>> ancestors_with_edges;

    for (size_t i = 0; i < this_ancestors.size(); i++){
      ancestors_with_edges.push_back({this_ancestors.at(i), this_edges.at(i)});
    }
    return ancestors_with_edges;
}

HierarchySlice combine_slices(const HierarchySlice& s1, const HierarchySlice& s2);

inline size_t size(const HierarchySlice& s){
    size_t r = 0;
    for(auto& it: s){
        r += it.size();
    }
    return r;
}

/**
 * Constructs a slice containing every note in the hierarchy.
 */
template < typename Node, typename Edge >
  HierarchySlice full_slice(const HierarchyGraph<Node, Edge>& hierarchy);

/**
 * Checks if the HierarchyPos lives in the slice.
 */
bool in_slice(const HierarchySlice& slice, HierarchyPos test_pos);

template<typename T>
    std::vector<T> delete_duplicates(const std::vector<T>& list){
    std::unordered_set<T> set(std::begin(list), std::end(list));
    std::vector<T> result(std::begin(set), std::end(set));
    return result;
}

    /**
     * Builds a HierarchySlice of the given hierarchy, which contains
     * the element at the given position and all of its ancestors.
     */
template < typename Node, typename Edge >
    HierarchySlice with_ancestors_slice(const HierarchyGraph<Node,Edge>& hierarchy, HierarchyPos pos);

}

#include "Hierarchy.hpp" // Implementations


namespace Caravel{

template< template <class> class Decomposition, size_t L > class Topology;
template <typename T> class ConstantDecomposition;
template <size_t L> using ColourOrderedResidue = Topology<ConstantDecomposition, L>;
template<size_t L> class LinkInfo;

extern template class HierarchyGraph<ColourOrderedResidue<2>,LinkInfo<2>>;
extern template class HierarchyGraph<ColourOrderedResidue<1>,LinkInfo<1>>;

}

#endif //HIERARCHY_H_INC
