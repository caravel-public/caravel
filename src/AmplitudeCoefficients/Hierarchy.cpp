#include "AmplitudeCoefficients/Hierarchy.h"
#include <vector>
#include "AmplitudeCoefficients/Topology.h"
#include "AncestorLinks.h"

namespace Caravel{

    bool in_slice(const HierarchySlice& slice, HierarchyPos test_pos){
	for (auto& gen : slice){
	    for (auto& pos : gen){
		    if (pos.first == test_pos.first && pos.second == test_pos.second){
		    return true;
		}
	    }
	}
	return false;
    }

    HierarchySlice combine_slices(const HierarchySlice& s1, const HierarchySlice& s2){
        std::vector<HierarchyPos> all_positions;
	for (auto& gen : s1){
	    all_positions.insert(all_positions.end(), std::begin(gen), std::end(gen));
	}
	for (auto& gen : s2){
	    all_positions.insert(all_positions.end(), std::begin(gen), std::end(gen));
	}

	std::sort(std::begin(all_positions), std::end(all_positions),
		  [](HierarchyPos i, HierarchyPos j){return i.first> j.first;});

	HierarchySlice new_slice;
	auto current_gen = all_positions.at(0).first;
	std::vector<HierarchyPos> current_gen_slice;

	for (auto& el : all_positions){
	    if (el.first != current_gen){
	    new_slice.push_back(current_gen_slice);
	    current_gen_slice.clear();
	    current_gen = el.first;
	    }
	    current_gen_slice.push_back(el);
	}
	new_slice.push_back(current_gen_slice);

	for (auto& el : new_slice){
	    auto gen = el.at(0).first;
            std::vector<size_t> poss;
	    for (auto& pos : el){
	    poss.push_back(pos.second);
	    }
	    auto unique_poss = delete_duplicates(poss);
            std::vector<HierarchyPos> poss_with_gen;
	    for (auto& pos : unique_poss){
	    poss_with_gen.push_back({gen, pos});
	    }
	    el = poss_with_gen;
	}

	return new_slice;
    }


template class HierarchyGraph<ColourOrderedResidue<2>,LinkInfo<2>>;
template class HierarchyGraph<ColourOrderedResidue<1>,LinkInfo<1>>;

  
}
