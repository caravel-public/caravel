#ifndef _PROCESS_LIBRARY_H_
#define _PROCESS_LIBRARY_H_

/*
 * Code for loading process libraries from file.
 */
#include "Core/AlgebraicStructures.h"
#include "AmplitudeCoefficients/Hierarchy.h"
#include "Core/settings.h"
#include "Core/type_traits_extra.h"
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <algorithm>

#include "Core/MathOutput.h"

#include "FunctionalReconstruction/DenseRational.h"
#include "FunctionSpace/FunctionSpace.h"


#include "AmplitudeCoefficients/SubtractionEngine.h"

#ifdef USE_FINITE_FIELDS
#include "Core/RationalReconstruction.h"
#endif

#include <optional>
#include "Core/CaravelInput.h"

#include "wise_enum.h"

#include "Graph/Graph.h"
#include "Graph/IntegralGraph.h"

namespace Caravel {

// Forward declarations
class Model;
template <class T, size_t D> class Builder;
class Forest;

using BasisTruncation = std::vector<size_t>;
using HierarchyBasisTruncation = std::vector<std::vector<BasisTruncation>>;
template <typename TF, typename T, size_t D> class ResidueEvaluator;
using MasterPosition = std::pair<HierarchyPos, size_t>;
template <class T> class Vec;
//

namespace IntegrandHierarchy {

/**
 * Load a process library from file
 */
std::string load_schematic(std::string filename);
/**
 * Guess the process library filename from the model and process
 */
std::string load_schematic(const Model& model, const PartialAmplitudeInput& process);

/**
 * Guess which surface terms to load
 * @param model in which evaluation happens
 * @param process
 */
void load_surface_terms(const Model&, const PartialAmplitudeInput&);

/**
 * A struct abstracting slice selection inside one hierarcy. Very simple for now, but might be extanaded later.
 */
struct slice_selector {
    WISE_ENUM_CLASS_MEMBER(type_choices, with_ancestors_slice, all_children_with_ancestors_slice, all_down_to_level, combined_all_ancestors)

    type_choices type{type_choices::with_ancestors_slice}; /**< specify type of slice */

    HierarchyPos s_hierarchy_pos;              /**< focus position used for first two types of slices */
    size_t level{0};                           /**< level used for the third slice type */
    std::vector<HierarchyPos> v_hierarchy_pos; /**< list of diagrams which build the slice used for the 'combined_all_ancestors' type */

    slice_selector() = default;
    slice_selector(const HierarchyPos& spos) : s_hierarchy_pos(spos) {}
    slice_selector(const std::vector<HierarchyPos>& vpos) : v_hierarchy_pos(vpos) { type = type_choices::combined_all_ancestors; }
    slice_selector(MathList);
};

/**
 * Interface class for full amplitudes
 */
template <typename T, size_t L> class ColourExpandedAmplitude;

/**
 * Interface class for amplitude-like rational functions instead of cuts
 */
template <typename T, size_t L> class AmplitudeLikeContribution;

template <typename T, size_t L, typename TF = T> class IntegrandHierarchy {
    static_assert(L == 2, "Only implemented for two-loop so far.");

  private:
    bool recompute_currents{false};
    static constexpr size_t D = 6;
    friend class ColourExpandedAmplitude<T, L>;
    friend class AmplitudeLikeContribution<T, L>;

    int tree_id;                          /**< store where in the builder is tree amplitude */
    std::vector<size_t> Dss;              /**< set of Ds values used ONLY for dimensional recontruction input (not realted to e.g. how Forest was closed) */

    using Ds_type = Caravel::DensePolynomial<int>;
    Ds_type Ds_value{{4, -2}, "ep"};

    std::shared_ptr<HierarchyGraph<Topology<ConstantDecomposition, L>, LinkInfo<L>>> graph_representation;

    std::vector<SubtractionEngine<TF,T,D,L>> subtraction_engines;

    std::shared_ptr<const HierarchySlice> test_slice;
    std::shared_ptr<const std::vector<MasterPosition>> master_positions;
    std::vector<int> symmetry_factors{}; /**< multiply each master with a symmetry factor */

    /**< Function which evaluates the full spinor weight of the amplitude */
    std::function<T(const momentumD_configuration<T, 4>&)> compute_normalization{[](const momentumD_configuration<T, 4>&) { return T(1); }};

    // Stores (does not own) a pointer to warmup.
    // Necessary until construction of subtraction enginge is fixed und there is no need anymore to pass it each time for evaluation
    // Needs to be set before the first evaluation from outside
    std::shared_ptr<HierarchyWarmupInfo> warmup_info{std::make_shared<HierarchyWarmupInfo>()};

  public:
    typedef std::vector<DenseRational<T>> ReturnType_Coefficients;
    typedef std::vector<DenseRational<DensePolynomial<T>, T>> ReturnType_Ds_Coefficients;

  public:
    IntegrandHierarchy() = default;
    /// Construct full hiararchy from schematic
    IntegrandHierarchy(const std::string& schematic)
        : graph_representation{std::make_shared<HierarchyGraph<Topology<ConstantDecomposition, L>, LinkInfo<L>>>(schematic)},
          test_slice{std::make_shared<const HierarchySlice>(full_slice(*graph_representation))},
          master_positions{std::make_shared<const std::vector<MasterPosition>>(slice_master_positions(*graph_representation, *test_slice))} {
        init();
    };

    /// Construct a slice of the hierarchy given by set_slice from schematic
    IntegrandHierarchy(const std::string& schematic, const HierarchySlice& set_slice)
        : graph_representation{std::make_shared<HierarchyGraph<Topology<ConstantDecomposition, L>, LinkInfo<L>>>(schematic)},
          test_slice{std::make_shared<HierarchySlice>(set_slice)}, master_positions{std::make_shared<std::vector<MasterPosition>>(
                                                                       slice_master_positions(*graph_representation, *test_slice))} {
        init();
    };

    /// Construct a slice of the hierarchy given by set_slice from  graph_representation
    IntegrandHierarchy(decltype(graph_representation) set_gr, HierarchySlice&& set_slice)
        : graph_representation(set_gr), test_slice{std::make_shared<HierarchySlice>(set_slice)}, master_positions{std::make_shared<std::vector<MasterPosition>>(slice_master_positions(*graph_representation, *test_slice))} {
        init();
    };

    void grow_forest(Forest&, const PartialAmplitudeInput&, bool add_tree);

    ReturnType_Coefficients get_master_coefficients(const momentumD_configuration<T, 4>& mom_conf, size_t i = 0);
    ReturnType_Ds_Coefficients get_master_coefficients_full_Ds(const momentumD_configuration<T, 4>& mom_conf, size_t i = 0);

    const auto& get_graph_representation() const { return *graph_representation; }

    /**
     * Return the master topologies.
     * The order is the same as in master positions.
     * @return A vector containing the master positions.
     */
    std::vector<lGraph::xGraph> get_master_topologies() const ;
    std::vector<lGraph::IntegralGraph> get_integral_graphs() const;

  private:
    void init(); // run at the end of all constructors
};

template <typename T, size_t L> class ColourExpandedAmplitude {
    static_assert(L == 2, "Only implemented for two-loop so far.");

  private:
    static constexpr size_t D = 6;
    using TF = Vec<T>;
    using norm_type = settings::IntegrandHierarchy::norm_type;

    const size_t multiplicity;

    std::unique_ptr<Forest> forest;

    //TODO: so far everything only work when there's exactly one integrand hierarchy here
    std::vector<IntegrandHierarchy<T, L, TF>> all_hierarchies;

    using Ds_type = typename decltype(all_hierarchies)::value_type::Ds_type;

    std::string warmup_filename{"warmup_info_"};
    bool need_warmup{true};

    std::vector<size_t> Dss;

    norm_type normalization;

    bool always_recompute_tree_currents{false};

  public:
    ColourExpandedAmplitude() = delete;
    ColourExpandedAmplitude(const ColourExpandedAmplitude&) = delete;
    ColourExpandedAmplitude(ColourExpandedAmplitude&&) = default;

    /**
     * Construct a partial amplitude
     * @param Model ot use
     * @param external amplitude
     */
    ColourExpandedAmplitude(const Model&, const PartialAmplitudeInput&);
    /**
     * As above but select only a slice in a hierarchy
     */
    ColourExpandedAmplitude(const Model&, const PartialAmplitudeInput&, const slice_selector&);

    /**
     * Evaluates and returns all master integrand coefficients for all constructed hierarchies.
     */
    std::vector<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients> get_master_coefficients(const momentumD_configuration<T, 4>& mom_conf);
    /**
     * Evaluates and returns all master integrand coefficients for all constructed hierarchies, including full Ds dependence
     */
    std::vector<typename IntegrandHierarchy<T, L>::ReturnType_Ds_Coefficients> get_master_coefficients_full_Ds(const momentumD_configuration<T, 4>& mom_conf);
    /**
     * CoefficientProvider complying interface for above
     */
    typename IntegrandHierarchy<T, L>::ReturnType_Coefficients compute(const momentumD_configuration<T, 4>& mom_conf) {
        return get_master_coefficients(mom_conf).at(0);
    }

    /**
     * Do a warmup run. If a valid wamup exists already do nothing.
     */
    bool warmup_run(const momentumD_configuration<T, 4>& mom_conf);
    bool reload_warmup();
    bool has_warmup() const { return !need_warmup; }
    std::string get_warmup_filename() const { return warmup_filename; }

    /**
     * Set the value which will be used to set Ds to after dimensional reconstruction
     */
    void set_Ds_value(Ds_type s);

    /**
     * Creates a THREAD SAFE functor, which outputs
     * the result as if get_master_coefficients(mom_conf) is called.
     * Can be called any number of times to create any number of threads.
     *
     * IMPORTANT: DO NOT make copies of the functor obtained from here (this will create race condition)!
     * Each new functor obtained from this method should be run only in one thread.
     *
     * IMPORTANT: valid warmup has to exist (e.g. call warmup_run(mom_conf)) before the obtained functor is used!
     * Otherwise there is an undefined behaviour caused by a race to create a warmup.
     * Reimplements functionality of
     * get_master_coefficients related to the
     * normalization as this part is not automatically
     * thread-safe.
     *
     */
    std::function<std::vector<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients>(const momentumD_configuration<T, 4>& mom_conf)>
    get_new_master_coefficients_evaluator();
    /**
     * CoefficientProvider complying interface for above (evaluates only the first integrand hierarchy)
     */
    std::function<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients(const momentumD_configuration<T, 4>& mom_conf)> get_coeff_evaluator() {
        return [f = get_new_master_coefficients_evaluator()](const momentumD_configuration<T, 4>& mom_conf) { return f(mom_conf).at(0); };
    }

    // For each hierarchy return the master topologies.
    // @return For each hierarchy the topologies of the master integrals.
    std::vector<lGraph::xGraph> get_master_topologies() const ;
    std::vector<lGraph::IntegralGraph> get_integral_graphs() const;
    std::shared_ptr<const std::vector<MasterPosition>> get_master_positions() const;

    const auto& get_hierarchy_graph(size_t i = 0) const { return all_hierarchies.at(i).get_graph_representation(); }
    const auto& get_slice() const { return *(all_hierarchies.at(0).test_slice); }

    /**
     * Return the multiplicity.
     * @return The multiplicity.
     */
    std::size_t get_multiplicity() const { return multiplicity; }

  private:
    void finish_construction(const Model&, const PartialAmplitudeInput&);
    void select_hierarchy(const PartialAmplitudeInput&, LinearCombination<ColourStructure, LinearCombination<NColPower, std::string>>&&);
    void select_slice(const slice_selector&);
    size_t add_residue_evaluators(const std::vector<std::shared_ptr<Builder<T, D>>>& builders);
};

template <typename T, size_t L> class AmplitudeLikeContribution {
  public:
    static constexpr size_t D = 6;
    typedef std::function<T(const OnShellPoint<T, D>&, const momentumD_configuration<T, D>&)> numerator_evaluator_type;

  public:
    IntegrandHierarchy<T, L> hierarchy;

  private:
    size_t multiplicity; /**< number of external legs in maximal topology */

    std::unordered_map<HierarchyPos, numerator_evaluator_type> contributions; /**< store all numerator evaluators */

    std::string warmup_filename{"warmup_info_FUNC_"};
    bool need_warmup{true};

    bool closed{false};

    bool include_symmetry_factors{false}; /**< for reduction of functions symmetry factors should not be included */

  public:
    AmplitudeLikeContribution() = delete;
    /**
     * Construct full hierarchy with maximal n-point
     */
    AmplitudeLikeContribution(size_t);
    /**
     * Select a slice from the n-point hierarchy
     */
    AmplitudeLikeContribution(size_t, const slice_selector&);
    /**
     * Create from PL-file
     */
    AmplitudeLikeContribution(size_t, const std::string&);
    /**
     * Create from PL-file, and specify a slice
     */
    AmplitudeLikeContribution(size_t, const std::string&, const slice_selector&);

    /**
     * Add function that evaluates topology
     * @param toplogy position
     * @param function
     */
    void add_numerator_evaluator(HierarchyPos, numerator_evaluator_type);
    /**
     * Add function that evaluates topology
     * @param toplogy
     * @param function
     */
    void add_numerator_evaluator(const lGraph::xGraph&, numerator_evaluator_type);

    /**
     * After calling this method no more numerators can be added
     * and residue evaluators are constructed
     * @param (optional) function that does something once per external point change
     */
    void close(const std::function<void(const momD_conf<T, 4>& conf)>& = [](const momD_conf<T, 4>& conf) {});

    /**
     * Evaluates and returns all master integrand coefficients
     */
    typename IntegrandHierarchy<T, L>::ReturnType_Coefficients get_master_coefficients(const momentumD_configuration<T, 4>& mom_conf);
    /**
     * CoefficientProvider complying interface
     */
    typename IntegrandHierarchy<T, L>::ReturnType_Coefficients compute(const momentumD_configuration<T, 4>& mom_conf) {
        return get_master_coefficients(mom_conf);
    }

    std::function<typename IntegrandHierarchy<T, L>::ReturnType_Coefficients(const momentumD_configuration<T, 4>& mom_conf)> get_coeff_evaluator() ;

    /**
     * Can be used to enable/disable including symmetry factors for topologies
     */
    void set_include_symmetry_factors(bool s);

    /**
     * Return posisitons of master insertions in the graph hierarchy
     */
    std::vector<MasterPosition> get_master_positions() const { return *(hierarchy.master_positions); }

    /**
     * Return the master topologies.
     * The order is the same as in master positions.
     * @return A vector containing the master positions.
     */
    std::vector<lGraph::xGraph> get_master_topologies() const ;
    std::vector<lGraph::IntegralGraph> get_integral_graphs() const;

    bool has_warmup() const { return !need_warmup; }

  private:
    void select_hierarchy();
    void select_slice(const slice_selector&);
    void check_warmup();
    void finish_construction();
};

namespace internal {
/**
 * Given a vector of numerators in Dss={Ds1,Ds2,Ds3} output a polynomial obtained by settings Ds to some value (polynomial of regularization parameter)
 * (default = 4-2ep)
 */
template <typename F, typename TDs = int>
std::vector<DenseRational<F>> dimensional_reconstruct_quadratic(const std::vector<Ratio<DensePolynomial<Vec<F>, F>, DensePolynomial<F>>>& in, const std::vector<size_t>& Dss,
                                                 DensePolynomial<TDs> Ds_value = DensePolynomial<TDs>{{TDs(4), TDs(-2)}, "ep"});
}

template <typename T> 
[[deprecated("Do not pass pointers!")]]
std::function<T(const momentumD_configuration<T, 4>&)> construct_normalizer(std::vector<Particle*> process);

} // namespace IntegrandHierarchy

/**
 * Guess if a tree amplitudes for the process is vanishing using some simple rules.
 * Is not guaranteed to guess 100% right!
 */
bool is_vanishing_tree(const PartialAmplitudeInput&);
  
/**
 * Construct a function which computes full little group scaling factor for a given process
 */
template <typename T> std::function<T(const momentumD_configuration<T, 4>&)> construct_normalizer(std::vector<Particle>);


} // namespace Caravel

#include "IntegrandHierarchy.hpp"

#endif // _PROCESS_LIBRARY_H_
