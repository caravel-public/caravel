#include "Forest/Builder.h"
#include "Core/typedefs.h"

#if INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

namespace Caravel {

// Defining instantiations, matching declarations in Builder.hpp

#ifdef INSTANTIATE_GMP
_BUILDER_NEW(,RGMP)
#endif

}
