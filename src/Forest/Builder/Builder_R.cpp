#include "Forest/Builder.h"
#include "Core/typedefs.h"

namespace Caravel {

// Dummy virtual destructor for pure abstract class CurrentWorker_Base
CurrentWorker_Base::~CurrentWorker_Base() {}

void EntryNormalization::add_real_normalization_entry(int r_num,int r_den,int r_exponent){
	real_normalization_factors.push_back({r_num,r_den,r_exponent});
}

void EntryNormalization::add_imaginary_normalization_entry(int i_num,int i_den,int i_exponent){
	imaginary_normalization_factors.push_back({i_num,i_den,i_exponent,"i"});
}

					// parent					daughter
size_t get_index_for_match(size_t Ds,const PhysicalStateContainer& all_states,const PhysicalState& state){
	size_t toreturn(0);
	// sample states until match
	for(size_t ii=0;ii<all_states.size(Ds);ii++){
			// parent (less leaves)			daughter (more leaves)
		if(all_states.get_state(ii,Ds).matches_daughter(state)){
			toreturn=ii;
			break;
		}
	}
	return toreturn;
}

std::ostream& operator<<(std::ostream& s, SingleNormalizationFactor f) {
    s << "(";
    if(f.factor != "") s<<f.factor<<"*";
    s << f.num;
    if (f.den != 1) { s << "/" << f.den; }
    s << ")^" << f.exponent;
    return s;
}

// Defining instantiations, matching declarations in Builder.hpp

_BUILDER_NEW(,R)


}
