/**
 * @file currents.cpp 
 *
 * @date 2.5.2017
 *
 * @brief Implementations of abstract Current's
 *
 * Implementations related to derived classes of the class Current and member classes
 *
*/

#include <iostream>
#include <algorithm>
#include <set>

#include "Forest/Forest.h"
#include "Core/Debug.h"
#include "Forest/Model.h"

namespace Caravel {

PhysicalState PhysicalState::permutation(const std::vector<size_t>& v) const {
	if(v.size()!=leaves.size()){
		std::cout<<"PhysicalState::permutation(.) received a request with wrong size of permutation indices! Returned default PhysicalState"<<std::endl;
		return PhysicalState();
	}
	PhysicalState toret;
	toret.dims=this->dims;
	for(size_t ii=0;ii<v.size();ii++){
		toret.leaves.push_back(leaves[v[ii]]);
		toret.state_entries.push_back(state_entries[v[ii]]);
	}
	return toret;
}

PhysicalState PhysicalState::prune(const std::vector<Particle*>& new_leaves) const{
	// check all leaves are indeed included
	for(auto& p:new_leaves){
		if(std::find(leaves.begin(),leaves.end(),p)==leaves.end()){
			std::cout<<"PhysicalState::prune(.) received a particle which isn't included in this PhysicalState!! -- Returned empty state"<<std::endl;
			return PhysicalState();
		}
	}
	PhysicalState toret;
	// pruning doesn't change dims
	toret.dims=dims;
	toret.leaves=new_leaves;
	// take care of complement leaves
	for(auto& complement_leaf:leaves){
		if(std::find(new_leaves.begin(),new_leaves.end(),complement_leaf)==new_leaves.end()){
			toret.correlated_complement_leaves.push_back(complement_leaf);
		}
	}
	toret.state_entries=std::vector<SingleState>();
	for(auto& local_leaf:new_leaves){
		size_t index(std::find(leaves.begin(),leaves.end(),local_leaf)-leaves.begin());
		toret.state_entries.push_back(state_entries[index]);
	}
	// take care of complement states
	for(auto& local_leaf:toret.correlated_complement_leaves){
		size_t index(std::find(leaves.begin(),leaves.end(),local_leaf)-leaves.begin());
		toret.correlated_complement_state_entries.push_back(state_entries[index]);
	}
#if 0
std::cout<<"From pruning: "<<this->get_short_name()<<" we got: "<<toret.get_short_name()<<std::endl;
#endif
	return toret;
}

bool PhysicalState::extra_dim(const PhysicalState& in){
	if(in.leaves==leaves&&in.state_entries==state_entries){
		for(auto& d:in.dims){
			if(std::find(dims.begin(),dims.end(),d)==dims.end())
				dims.push_back(d);
		}
#if 0
std::cout<<"From extra_dim: "<<this->get_short_name();
#endif
		// sort dimensions
		std::sort(dims.begin(),dims.end());
#if 0
std::cout<<" we got: "<<this->get_short_name()<<std::endl;
#endif
		return true;
	}
	return false;
}

bool operator==(const PhysicalState& a,const PhysicalState& b){
	return a.dims==b.dims&&a.leaves==b.leaves&&a.state_entries==b.state_entries;
}

SingleState PhysicalState::SingleState_of_particle_state(Particle* p) const {
	SingleState toret;
	if(std::find(leaves.begin(),leaves.end(),p)!=leaves.end())
		toret=state_entries[std::find(leaves.begin(),leaves.end(),p)-leaves.begin()];
	return toret;
}

		// THIS: parent (less leaves)			daughter (more leaves)
bool PhysicalState::matches_daughter(const PhysicalState& in) const {
	std::vector<SingleState> states_parent;
	std::vector<SingleState> states_daughter;
	for(size_t ii=0;ii<leaves.size();ii++){
		if(std::find(in.leaves.begin(),in.leaves.end(),leaves[ii])==in.leaves.end()){
			return false;
		}
		else{
			size_t match_ii(std::find(in.leaves.begin(),in.leaves.end(),leaves[ii])-in.leaves.begin());
			states_parent.push_back(state_entries[ii]);
			states_daughter.push_back(in.state_entries[match_ii]);
		}
	}
	return states_parent==states_daughter;
}

size_t PhysicalState::get_index_correlated_state(const PhysicalStateContainer& container) const {
	size_t toret(0);
	bool match(false);
	for(size_t ii=0;ii<container.size();ii++){
		if(container[ii].leaves==correlated_complement_leaves&&container[ii].state_entries==correlated_complement_state_entries){
			match=true;
			toret=ii;
			break;
		}
	}
	if(!match){
		// maybe the trouble is that the check:
			//if(container[ii].leaves==correlated_complement_leaves&&container[ii].state_entries==correlated_complement_state_entries)
		// is made with the vectors with an implicit order! When the check is for a single leaf, there is no problem, but more complicated cases
		// like in potentially tree-loop cuts, should be carefully reviewed!
		std::cout<<"ERROR in PhysicalState::get_index_correlated_state(.)!! Did not find match!! Returning zero"<<std::endl;
	}
	return toret;
}

bool PhysicalState::contains_one_of_these_dims(const std::vector<size_t>& indims) const {
	bool toret(false);
	for(auto& d:indims){
		if(std::find(dims.begin(),dims.end(),d)!=dims.end()){
			toret=true;
			break;
		}
	}
	return toret;
}

PhysicalState PhysicalState::dimension_filter(const std::vector<size_t>& dims) const {
	PhysicalState toret(*this);
	std::vector<size_t> new_dims;
	for(size_t ii=0;ii<toret.dims.size();ii++){
		if(std::find(dims.begin(),dims.end(),toret.dims[ii])!=dims.end())
			new_dims.push_back(toret.dims[ii]);
	}
	toret.dims=new_dims;
	return toret;
}

std::string PhysicalState::get_short_name() const {
	if(state_entries.size()==0)
		return "empty state";
	std::string toret("{"+leaves[0]->get_name()+":"+state_entries[0].get_name());
	for(size_t ii=1;ii<leaves.size();ii++)
		toret+=","+leaves[ii]->get_name()+":"+state_entries[ii].get_name();
	toret+="}[Ds="+std::to_string(dims[0]);
	for(size_t ii=1;ii<dims.size();ii++)
		toret+=","+std::to_string(dims[ii]);
	toret+="]";
	return toret;
}

std::ostream& operator<<(std::ostream& o, const Caravel::PhysicalState& st)  {
        auto& state_entries = st.state_entries;

	if(state_entries.size()==0) return o<<"{}";

        auto& leaves = st.leaves;
        auto& dims = st.dims;

	o<< "{"+leaves[0]->get_name()+":"+state_entries[0].get_name();

	for(size_t ii=1;ii<leaves.size();ii++)
		o<<","+leaves[ii]->get_name()+":"+state_entries[ii].get_name();
	o<<"}[Ds="+std::to_string(dims[0]);

	for(size_t ii=1;ii<dims.size();ii++)
		o<<","+std::to_string(dims[ii]);
	o<<"]";
	return o;
}

void PhysicalState::show() const {
	std::cout<<"PhysicalState: "<<this->get_short_name()<<std::endl;
}

PhysicalStateContainer::PhysicalStateContainer(Current* c): parent_current(c) {
//std::cout<<"MORE FOR PhysicalStateContainer default constructor??"<<std::endl;
}

size_t PhysicalStateContainer::size() const {
	return all_states.size();
}

std::ostream& operator<<(std::ostream& o, const Caravel::PhysicalStateContainer& st){
    o<<"PhysicalStateContainer: ";
    o<<st.all_states;
    return o;
}

size_t PhysicalStateContainer::size(size_t Dim) const {
	if(std::find_if(dim_tracker.begin(),dim_tracker.end(),[Dim](const dim_counter& c) {return c.dim==Dim;})==dim_tracker.end())
		return 0;
/////////////////////////////////////////////////
/////////////////////////////////////////////////
// STRATEGIES for Forest for counting D-dim states
/////////////////////////////////////////////////
/////////////////////////////////////////////////
	else{
		return dim_tracker[std::find_if(dim_tracker.begin(),dim_tracker.end(),[Dim](const dim_counter& c) {return c.dim==Dim;})-dim_tracker.begin()].states_counter;
	}
}

void PhysicalStateContainer::set_leaves() {
	const std::vector<Current*>& all_leaves(parent_current->get_leaves().get_all_leaves());
#if 0
std::cout<<"For current: "<<parent_current->get_long_name()<<" these are all leaves: ";
for(auto& l:all_leaves)	std::cout<<l->get_name()<<" ";
std::cout<<std::endl;
#endif
	for(auto& leaf:all_leaves){
		Particle* local_particle(leaf->get_unique_particle_leaf());
		if(local_particle!=nullptr){
			if(local_particle->external_state().get_name()=="default")
				default_leaves.push_back(local_particle);
		}
		else{
			std::cout<<"Error? A leaf has no associated particle?!"<<std::endl;
		}
	}
	leaves_set=true;
}

void PhysicalStateContainer::set_default_states(const std::vector<PhysicalState>& v,int maximum_dims){
	if(all_states.size()>0){
		std::cout<<"PhysicalStateContainer::set_default_states(.) adding states when all_states is not empty!! We clear it and keep going!!"<<std::endl;
		all_states.clear();
	}

	// In this case we add all dimensions
	if(maximum_dims<0){
		all_states=v;
	}
	else{
		// we need to prune the states and keep only states with at most maximum_dims dimensions
		std::set<size_t> set_local_dimensions;
		for(size_t ii=0;ii<v.size();ii++){
			// we get all dimensions considered
			for(auto& d:v[ii].dims)	set_local_dimensions.insert(d);
		}
		// if there is less equal dimensions than maximum_dims, we can proceed directly
		if(set_local_dimensions.size()<=size_t(maximum_dims)){
			all_states=v;
		}
		else{
			// we need keep these dimensions
			std::vector<size_t> dimensions_to_keep;
			std::set<size_t>::iterator iter = set_local_dimensions.begin();
			for(int ii=0;ii<maximum_dims;ii++){	
				dimensions_to_keep.push_back(*iter);
				iter++;
			}
			for(auto& phy:v){
				if(phy.contains_one_of_these_dims(dimensions_to_keep)){
					all_states.push_back(phy.dimension_filter(dimensions_to_keep));
				}
			}
		}
	}
	// and update countings
	for(size_t ii=0;ii<all_states.size();ii++){
#ifdef DEBUG_ON
		// check that all PhysicalState are consistent with leaves in *this
		if(all_states[ii].leaves!=default_leaves)
			std::cout<<"Wrong PhysicalState passed to PhysicalStateContainer!!"<<std::endl;
#endif
		for(auto& d:all_states[ii].dims){
			// check if it is a new dimension
			if(std::find_if(dim_tracker.begin(),dim_tracker.end(),[d](const dim_counter& c) {return c.dim==d;})==dim_tracker.end()){
				dim_tracker.push_back({d,0,0});
				// and sort
				std::sort(dim_tracker.begin(),dim_tracker.end(),[](const dim_counter& a,const dim_counter& b) {return a.dim<b.dim;});
			}
			size_t entry(std::find_if(dim_tracker.begin(),dim_tracker.end(),[d](const dim_counter& c) {return c.dim==d;})-dim_tracker.begin());
			// 1 state contributing to this entry
			dim_tracker[entry].states_counter++;
			// check that the state does not appear in a dimension greater that d (we assume all_states[ii].dims is sorted!)
			if(d==all_states[ii].dims.back())
				dim_tracker[entry].states_strictly_contributing_up_to_Ds++;
		}
	}
}

const std::vector<Particle*>& PhysicalStateContainer::get_default_state_particles() const {
	return default_leaves;
}

const PhysicalState& PhysicalStateContainer::get_state(size_t i,size_t Ds) const {
	size_t counter(0);
	for(size_t kk=0;kk<all_states.size();kk++){
		// see if Ds is included in this state
		if(std::find(all_states[kk].dims.begin(),all_states[kk].dims.end(),Ds)!=all_states[kk].dims.end())
			counter++;
		if(counter>i)
			return all_states[kk];
	}
	std::cout<<"ERROR PhysicalStateContainer::get_state("<<i<<","<<Ds<<") did not find given physical state!! Returned first entry!!"<<std::endl;
	return all_states[0];
}

dim_counter PhysicalStateContainer::get_dim_counter(size_t d) const {
	if(std::find_if(dim_tracker.begin(),dim_tracker.end(),[d](const dim_counter& a) {return d==a.dim;})!=dim_tracker.end()){
		size_t entry(std::find_if(dim_tracker.begin(),dim_tracker.end(),[d](const dim_counter& a) {return d==a.dim;})-dim_tracker.begin());
		return dim_tracker[entry];
	}
	return {d,0,0};
}

std::string Current::get_long_name() {
	std::string toret("name: "+this->get_name()+" "+this->get_coupling_string()+" of type(contract): "+this->get_current_type().to_string()+"("+this->get_current_anti_type().to_string()+")");
	if(this->left_color_adjacent()!=nullptr && this->right_color_adjacent()!=nullptr){
		toret+=" and adjacency: "+this->left_color_adjacent()->get_name()+"|"+this->right_color_adjacent()->get_name();
	}
	else if(this->left_color_adjacent()!=nullptr){
		toret+=" and adjacency: "+this->left_color_adjacent()->get_name()+"|undef";
	}
	else if(this->right_color_adjacent()!=nullptr){
		toret+=" and adjacency: undef|"+this->right_color_adjacent()->get_name();
	}
	else{
		toret+=" uncolored, ";
	}
	if(this->internal_left_color()!=nullptr && this->internal_right_color()!=nullptr){
		toret+=" color extremes: "+this->internal_left_color()->get_name()+"|"+this->internal_right_color()->get_name();
	}
	else if(this->internal_left_color()!=nullptr){
		toret+=" color extremes: "+this->internal_left_color()->get_name()+"|undef";
	}
	else if(this->internal_right_color()!=nullptr){
		toret+=" color extremes: undef|"+this->internal_right_color()->get_name();
	}
	if(this->get_internal_index()!=0)
		toret+=" internal_index: "+std::to_string(this->get_internal_index());
	toret+=" #leaves: "+std::to_string(this->get_n_leaves());
        toret+=" with momenta: ";
	auto indices = this->get_leaves().get_all_momenta_indices();
	toret+="{";
        for(size_t ww=0;ww<indices.size();ww++){
		toret+=std::to_string(indices[ww]);
		if(ww+1<indices.size())
			toret+=",";
	}
	toret+="}";
	return toret;
}

// Top implementation for virtual function in Current class
std::string Current::get_name() {
	std::string lname("(");
	for(std::vector<Current*>::iterator it=(this->get_parents())->begin();it!=(this->get_parents())->end();it++)
		lname+=(*it)->get_name();
	lname+=")";
	return lname;
}

void Current::drop_propagator_and_pass_internal_indices(const ColorHandledProcess& chp){
    internal_indices.drop_propagator_and_pass_internal_indices(chp);
    closed_qqs_powers.push_back(qqs_powers);
    qqs_powers.clear();
}

void Current::set_default_state_particles(){
	default_particles_processed=true;
	physical_states.set_leaves();
}

bool Current::get_default_particles_processed() const {
	return default_particles_processed;
}

void Current::set_default_states(const std::vector<PhysicalState>& v){
	physical_states.set_default_states(v,maximal_number_of_dimensions_accepted);
}

const std::vector<Particle*>& Current::get_default_state_particles() const {
	return physical_states.get_default_state_particles();
}

const PhysicalStateContainer& Current::get_physical_states() const {
	return physical_states;
}

void Current::define_default_momDconf(size_t i){
	if(default_momD_conf==0){
		default_momD_conf=i;
	}
	else{
		if(default_momD_conf!=i){
#if _BUILDER_THREAD_SAFE==1
			std::cout<<"CAREFUL: Current: "<<this->get_name()<<" was asked to change default momD_conf from: "<<default_momD_conf<<" to "<<i<<std::endl;
#endif
			default_momD_conf=i;
		}
	}
}

Current::~Current() {
}

// Implementation of CurrentBuild
CurrentBuild::CurrentBuild() : name("empty CurrentBuild"), Lcurrent(nullptr), Rcurrent(nullptr), includepropagator(PropagatorType::identity), wasflipped(false) {
	// set default value for Current::states_set
	default_particles_processed=false;
}

// CurrentBuild constructor (out of input CurrentBasic (sub currents) we built the current (from the sum)
CurrentBuild::CurrentBuild(const std::vector<Current *>& subcurrents,const std::vector<Coupling>& vcps,PropagatorType includeprop,
		const ColorHandledProcess& chp) : couplings(vcps), includepropagator(includeprop), wasflipped(false) {
	// set default value for Current::states_set
	default_particles_processed=false;

	// From first sub current I build all information common to all subcurrents
	Current * First(subcurrents[0]);

	// fill particle information
	Current::particle=First->get_current_particle();

	// color adjacency
	Lcurrent=First->left_color_adjacent();
	InternalLcurrent=First->internal_left_color();
	Rcurrent=First->right_color_adjacent();
	InternalRcurrent=First->internal_right_color();

	// number of leaves
	size=First->get_n_leaves();
	// Construct vector of leaves 
	leaves=First->get_leaves();
	if(includepropagator!=PropagatorType::propagator)
		leaves.drop_propagator();

	// copy CurrentIndices
        Current::internal_indices=First->get_current_indices();
	// no parent indices at the level of CurrentBuild
        Current::internal_indices.reset_internal_indices_parents();
	if(includepropagator!=PropagatorType::propagator)
		Current::internal_indices.drop_propagator_and_pass_internal_indices(chp);

	// coupling string
	cstring=First->get_coupling_string();
	// remove info of indices in CurrentBasic which isn't needed
	cstring=cstring.substr(1,cstring.find("]"));

	// powers of couplings
	for(size_t ll=0;ll<couplings.size();ll++){
		powers.push_back(First->get_power(couplings[ll]));
	};

        qqs_powers = First->qqs_powers;
        closed_qqs_powers = First->closed_qqs_powers;

	// Build parents - build name
	name="{";
	for(size_t kk=0;kk<subcurrents.size();kk++){
		Current * lCurrent(subcurrents[kk]);
		name+=lCurrent->get_name();
		if(kk<subcurrents.size()-1)
			name+="+";
		for(size_t jj=0;jj<lCurrent->get_parents()->size();jj++){
			Current * lparent((*(lCurrent->get_parents()))[jj]);
			// If current is not in parents, include it
			if(std::find(parents.begin(),parents.end(),lparent)==parents.end()){
				parents.push_back(lparent);
			}
		};
	};
	if(includepropagator==PropagatorType::propagator)
		name+="}";
	else
		name+="}|";

	pure_name=leaves.get_short_name();
        {
            std::stringstream ss;
            ss << this->get_current_type().get_flavor();
            pure_name += ss.str();
        }
        pure_name += "-->";
	// store all subcurrents as they are used for kinematic rules
	for(size_t kk=0;kk<subcurrents.size();kk++){
		basicblocks.push_back(subcurrents[kk]);
		CurrentBasic* lc( dynamic_cast<CurrentBasic*>(subcurrents[kk]) );
		lc->set_parent_CurrentBuild(this);
	}

}

void CurrentBuild::add_parent(Current * c) {
	parents.push_back(c);
}

void CurrentBuild::add_daughter(Current * c) {
	daughters.push_back(c);
}

Current * CurrentBuild::left_color_adjacent() {
	return Lcurrent;
}

Current * CurrentBuild::right_color_adjacent() {
	return Rcurrent;
}

Current * CurrentBuild::internal_left_color() {
	return InternalLcurrent;
}

Current * CurrentBuild::internal_right_color() {
	return InternalRcurrent;
}

void CurrentBasic::set_parent_CurrentBuild(Current* in){
	if(parent_CurrentBuild==nullptr){
		parent_CurrentBuild=in;
	}
	else{
		std::cout<<"CAREFUL: you are resetting parent_CurrentBuild of *this!"<<std::endl;
		parent_CurrentBuild=in;
	}
}

const PhysicalStateContainer& CurrentBasic::get_physical_states() const {
	if(parent_CurrentBuild!=nullptr)
		return parent_CurrentBuild->get_physical_states();
	else{
		std::cout<<"Calling CurrentBasic::get_physical_states() when parent_CurrentBuild hasn't been set! Called inherited method"<<std::endl;
		return Current::get_physical_states();
	}
}

void CurrentBuild::set_left_color_adjacent(Current * c) {
	if(c==nullptr)
		Lcurrent=nullptr;
	else
		Lcurrent=c->internal_right_color();
}

void CurrentBuild::set_right_color_adjacent(Current * c) {
	if(c==nullptr)
		Rcurrent=nullptr;
	else
		Rcurrent=c->internal_left_color();
}

std::vector<Current*> * CurrentBuild::get_parents(){
	return &parents;
}

std::vector<Current*> * CurrentBuild::get_daughters(){
	return &daughters;
}

size_t CurrentBuild::get_n_leaves() const {
	return size;
}

const CurrentLeaves& CurrentBuild::get_leaves() const {
	return leaves;
}

CurrentLeaves& CurrentBuild::get_leaves() {
	return leaves;
}

void CurrentBuild::drop_propagator_and_pass_internal_indices(const ColorHandledProcess& chp){
        Current::drop_propagator_and_pass_internal_indices(chp);
	leaves.drop_propagator();
}

// Specific name for CurrentBuild
std::string CurrentBuild::get_name() {
//	return name;
	return pure_name;
}

std::string CurrentBuild::get_coupling_string() const {
	return cstring;
}

// To check coupling content of the current
int CurrentBuild::get_power(const Coupling& g) const{
	int power(0);
	for(size_t ii=0;ii<couplings.size();ii++){
		if(g.get_name() == couplings[ii].get_name()){
			power=powers[ii];
			break;
		};
	};
	return power;
}

bool CurrentBuild::was_flipped() const{
	return wasflipped;
}

void CurrentBuild::set_flipped(){
	wasflipped=true;
}

bool CurrentBuild::check_couplings(const cutTopology::couplings_t& v) const {
    for(auto& it: v){
        auto found = std::find_if(couplings.begin(),couplings.end(),[&it](const Coupling& c){return c.get_name()==it.first;});
        if (found == couplings.end()){
            if(it.second == 0) continue;
            else return false;
        }
        if(powers.at(std::distance(couplings.begin(),found)) != it.second) return false;
    }
    return true;
}

PropagatorType CurrentBuild::include_propagator() const {
	return includepropagator;
}

bool replace_string(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

CurrentBuild::~CurrentBuild() {
	for(size_t ii=0;ii<basicblocks.size();ii++)
		delete basicblocks[ii];
}


// Implementation of CurrentBasic
CurrentBasic::CurrentBasic() : parent_CurrentBuild(nullptr), name("empty CurrentBasic"), Lcurrent(nullptr), Rcurrent(nullptr), wasflipped(false) , rule(nullptr) {
	// set default value for Current::states_set
	default_particles_processed=false;

}

CurrentBasic::CurrentBasic(const Particle& p,std::vector<Current *> v,CurrentRule * r,std::vector<size_t> st,const std::vector<Coupling>& vcps) : parent_CurrentBuild(nullptr), 
		couplings(vcps), wasflipped(false), rule(r) {
	// store particle information
	Current::particle=p;
	// we set default value for the internal_index
	set_internal_index(0);

	// set default value for Current::states_set
	default_particles_processed=false;
#if 0
std::cout<<"CurrentBasic CONSTRUCT "<<r->get_name()<<" ";
for(auto& c:v) std::cout<<c->get_name()<<" ";
std::cout<<std::endl;
#endif
	size_t ncs(v.size());
	if(rule->is_color_ordered()){
		// if CurrentRule is color ordered, all Currents should be colored
		Lcurrent=v[0]->left_color_adjacent();
		InternalLcurrent=v[0]->internal_left_color();
		Rcurrent=v[ncs-1]->right_color_adjacent();
		InternalRcurrent=v[ncs-1]->internal_right_color();
	}
	else{
		// Initialize as colorless
		Lcurrent=nullptr;
		InternalLcurrent=nullptr;
		Rcurrent=nullptr;
		InternalRcurrent=nullptr;
		size_t loc;
		for(loc=0;loc<rule->get_current_type_list().size();++loc)
		  if(Current::particle.get_type()==rule->get_current_type_list()[loc]
		     ||(in_container<ParticleFlavor::q>(Current::particle.get_type().get_flavor())&&rule->get_current_type_list()[loc].get_flavor()==ParticleFlavor::q)
		     ||(in_container<ParticleFlavor::qb>(Current::particle.get_type().get_flavor())&&rule->get_current_type_list()[loc].get_flavor()==ParticleFlavor::qb))
		    break;
		//size_t loc(std::find(rule->get_current_type_list().begin(),rule->get_current_type_list().end(),Current::particle.get_type())-rule->get_current_type_list().begin());
		if(loc>=rule->get_current_type_list().size()){
			std::cout<<"ERROR: did not find current type: "<<Current::particle.get_type().to_string()<<" in current types in CurrentRule: "<<rule->get_name()<<std::endl;
		}
		else{
			// Is this current colored? -- otherwise, leave nullptr's
			if((rule->get_color_info_list())[loc]){
				for(size_t ii=0;ii<v.size();ii++){
					if(v[ii]->is_colored()){
						Lcurrent=v[ii]->left_color_adjacent();
						InternalLcurrent=v[ii]->internal_left_color();
						break;
					}
				}
				for(size_t ii=v.size();ii>0;ii--){
					if(v[ii-1]->is_colored()){
						Rcurrent=v[ii-1]->right_color_adjacent();
						InternalRcurrent=v[ii-1]->internal_right_color();
						break;
					}
				}
			}
		}
	}
	// Add parents
	for(size_t i=0;i<ncs;i++)	this->add_parent(v[i]);
	// After daughters defined, we can write CurrentBasic name (employing base get_name() function)
	name=this->Current::get_name();
        name += "~" + r->get_name() + "~";
// I remove parent addition here, as this is only kept as long as this is added to Forest
/*
	// Add daughters
	for(size_t i=0;i<ncs;i++)	v[i]->add_daughter(this);
*/
	// number of leaves
	size=0;
	for(size_t i=0;i<ncs;i++)	size+=v[i]->get_n_leaves();

	// Construct leaves
	for(size_t i=0;i<ncs;i++) leaves.add(v[i]->get_leaves(),v[i]->is_colored());
        // Build internal_indices
        Current::internal_indices=CurrentIndices(v);

	// index ordering
	indices=new int[ncs+1];
	for(size_t ii=0;ii<ncs+1;ii++)	*(indices+ii)=st[ii];

	// set powers of couplings
	cstring=" [";
	for(size_t ll=0;ll<couplings.size();ll++){
		const Coupling& lc(couplings[ll]);
		int lpower(0);
		// Add contribution of rule
		lpower+=rule->get_power(lc);
		// Add contributions from each subcurrents
		for(size_t i=0;i<ncs;i++){
			lpower+=v[i]->get_power(lc);
		};
		// Now store
		powers.push_back(lpower);
		// If different from 0, add coupling info to cstring:
		if(lpower!=0){
			cstring+=lc.get_name();
			cstring+="^";
			cstring+=std::to_string(lpower);
		};
	};
	cstring+="]";
	// Print index ordering together with cstring
	cstring+=" {";
	for(size_t ii=0;ii<ncs;ii++)	cstring+=std::to_string(indices[ii])+",";
	cstring+=std::to_string(indices[ncs])+"}";

//std::cout<<"ASSIGN: "<<Current::internal_indices<<std::endl;
//std::cout<<"	for: "<<*this<<std::endl;

    // fill information about closed qqs_powers
    // a single parent should have this!
    int has_closed(-1);
    for(size_t ii=0;ii<v.size();ii++){
        if(v[ii]->closed_qqs_powers.size()>0){
            if(has_closed>-1){
                _WARNING_R("ERROR: two parents with closed indices!");
                _WARNING("	", has_closed, ": ", *(v[has_closed]));
                _WARNING("	", ii, ": ", *(v[ii]));
                std::exit(1);
            }
            has_closed=int(ii);
	}
    }
    if(has_closed>-1)
        this->closed_qqs_powers=v[has_closed]->closed_qqs_powers;
}

void CurrentBasic::add_parent(Current * c) {
	parents.push_back(c);
}

void CurrentBasic::add_daughter(Current * c) {
	daughters.push_back(c);
}

Current * CurrentBasic::left_color_adjacent() {
	return Lcurrent;
}

Current * CurrentBasic::right_color_adjacent() {
	return Rcurrent;
}

Current * CurrentBasic::internal_left_color() {
	return InternalLcurrent;
}

Current * CurrentBasic::internal_right_color() {
	return InternalRcurrent;
}

void CurrentBasic::set_left_color_adjacent(Current * c) {
	if(c==nullptr)
		Lcurrent=nullptr;
	else
		Lcurrent=c->internal_right_color();
}

void CurrentBasic::set_right_color_adjacent(Current * c) {
	if(c==nullptr)
		Rcurrent=nullptr;
	else
		Rcurrent=c->internal_left_color() ;
}

std::vector<Current*> * CurrentBasic::get_parents(){
	return &parents;
}

std::vector<Current*> * CurrentBasic::get_daughters(){
	return &daughters;
}

size_t CurrentBasic::get_n_leaves() const {
	return size;
}

const CurrentLeaves& CurrentBasic::get_leaves() const {
	return leaves;
}

CurrentLeaves& CurrentBasic::get_leaves(){
	return leaves;
}

void CurrentBasic::drop_propagator_and_pass_internal_indices(const ColorHandledProcess& chp){
        Current::drop_propagator_and_pass_internal_indices(chp);
	leaves.drop_propagator();
}


// Specific name for CurrentBasic
std::string CurrentBasic::get_name() {
	return name;
}

std::string CurrentBasic::get_coupling_string() const {
	return cstring;
}

// To check coupling content of the current
int CurrentBasic::get_power(const Coupling& g) const{
	int power(0);
	for(size_t ii=0;ii<couplings.size();ii++){
		if(g.get_name() == couplings[ii].get_name()){
			power=powers[ii];
			break;
		};
	};
	return power;
}

bool CurrentBasic::was_flipped() const{
	return wasflipped;
}

void CurrentBasic::set_flipped(){
	wasflipped=true;
}

bool CurrentBasic::check_couplings(const cutTopology::couplings_t& v) const {
    for (auto& it : v) {
        auto found = std::find_if(couplings.begin(), couplings.end(), [&it](const Coupling& c) { return c.get_name() == it.first; });
        if (found == couplings.end()){
            if(it.second == 0) continue;
            else return false;
        }
        if (powers.at(std::distance(couplings.begin(), found)) != it.second) return false;
    }
    return true;
}

int * CurrentBasic::get_indices() {
	return indices;
}

int CurrentBasic::get_assigned_internal_index(size_t i) const {
    if(i>=parents.size()){
        std::cout<<"ERROR: CurrentBasic::get_assigned_internal_index(.) called for parent "<<i<<" (0-based) when we have only "<<parents.size()<<" --- returned 0! "<<std::endl;
        return 0;
    }
    return Current::internal_indices.get_assigned_internal_index(i);
}

void CurrentBasic::set_internal_indices_parents(const std::vector<int>& s) {
    if(s.size()!=parents.size()){
        _WARNING("Internal inconsistency in CurrentBasic::set_internal_indices_parents(.)");
        std::exit(1);
    }
    internal_indices.set_internal_indices_parents(s);
}

CurrentBasic::~CurrentBasic() {
	delete[] indices;
}

// Specific name for CurrentExternal
std::string CurrentExternal::get_name() {
	return Current::particle.get_long_name();
}

std::string CurrentExternal::get_coupling_string() const {
	return "";
}

// Constructor for CurrentExternal
CurrentExternal::CurrentExternal(Particle * p) : Lcurrent(nullptr), Rcurrent(nullptr), psource(p) {
	// set default value for Current::states_set
	default_particles_processed=false;

	// Copy the particle to the corresponding base class member
	Current::particle=*p;
	rule=nullptr;
	leaf=CurrentLeaves(this,p->mom_index());
}

std::vector<Current*> * CurrentExternal::get_parents(){
	return nullptr;
}

std::vector<Current*> * CurrentExternal::get_daughters(){
	return &daughters;
}

void CurrentExternal::add_parent(Current * c) {
	std::cout<<"Error, CurrentExternal can't add_parent(Current *)"<<std::endl;
}

void CurrentExternal::add_daughter(Current * c) {
	daughters.push_back(c);
}

size_t CurrentExternal::get_n_leaves() const{
	return 1;
}

const CurrentLeaves& CurrentExternal::get_leaves() const{
	return leaf;
}

CurrentLeaves& CurrentExternal::get_leaves(){
	return leaf;
}

void CurrentExternal::drop_propagator_and_pass_internal_indices(const ColorHandledProcess& chp){
	std::cout<<"Can not drop propagator in CurrentExternal!! -- Did nothing"<<std::endl;
}

Current * CurrentExternal::left_color_adjacent() {
	return Lcurrent;
}

Current * CurrentExternal::right_color_adjacent() {
	return Rcurrent;
}

Current * CurrentExternal::internal_left_color(){
	if(Current::particle.is_colored())
		return this;
	else
		return nullptr;
}

Current * CurrentExternal::internal_right_color(){
	if(Current::particle.is_colored())
		return this;
	else
		return nullptr;
}

void CurrentExternal::set_left_color_adjacent(Current * c) {
	if(c==nullptr)
		Lcurrent=nullptr;
	else
		Lcurrent=c->internal_right_color();
}

void CurrentExternal::set_right_color_adjacent(Current * c) {
	if(c==nullptr)
		Rcurrent=nullptr;
	else
		Rcurrent=c->internal_left_color();
}

int CurrentExternal::get_power(const Coupling& g) const{
	return 0;
}

// as this is invariant under flip, we always return true
bool CurrentExternal::was_flipped() const{
	return true;
}

// do nothing
void CurrentExternal::set_flipped(){
	return;
}

bool CurrentExternal::is_associated_to_particle(Particle*p) const {return (p==psource) ? true : false;}

Particle* CurrentExternal::get_unique_particle_leaf() const {return psource;}

bool CurrentExternal::check_couplings(const cutTopology::couplings_t& v) const {
    for (auto& it : v) {
        if (it.second != 0) return false;
    }
    return true;
}

Particle * CurrentExternal::get_source() {
	return psource;
}

CurrentExternal::~CurrentExternal() {
}

std::string Current::get_index_info() {
	return internal_indices.get_index_info();
}

std::ostream& operator<< (std::ostream& s, const Current& c){
    // this operator should by const but get_long_name() is not const and pulls a lot of non-const functions
    // may be some day these non-consts will be resolved...
    s << const_cast<Current&>(c).get_long_name();
	s<<" "<<const_cast<Current&>(c).get_index_info();
    return s;
}

}
