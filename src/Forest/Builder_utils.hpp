#include <algorithm>
#include "Coupling.h"
#include "Forest/Model.h"
#include "Core/settings.h"

#include "Forest/cardinality.h"

#define _DEBUG_CURRENTS 0
#define _DEBUG_NORMALIZATION 0
#define _REFV_TRIALS 4

namespace Caravel {


// main Propagator constructor
template <class T,size_t D> Propagator<T,D>::Propagator(CurrentBuild * cb,const momD_conf<T,D>& mconf, const Model& model) : prop(T(1)) {
	
	nmomenta=cb->get_n_leaves();

	momenta=std::vector<momD<T,D> const*>(nmomenta);
        midx = cb->get_current_particle().get_ParticleMass().get_mass_index();

        // TODO: Implement width

	const std::vector<size_t>& lcleaves(cb->get_leaves().get_all_momenta_indices());
	// Now identify the corresponding momenta
	for(size_t ii=0;ii<nmomenta;ii++){
		momenta[ii]=&mconf.p(lcleaves[ii]);
	}
	// resolve propagator
	compute_prop=get_compute_propagator<T,Tcur,D>(cb,model);
}

template <class T,size_t D> std::vector<typename CTS<T>::Tcur> Propagator<T,D>::compute(const std::vector<typename CTS<T>::Tcur>& in, const BuilderContainer<T>& bc){
	momentum=*(momenta[0]);
	momentum=std::accumulate(momenta.begin()+1,momenta.end(),momentum,[](momD<T,D>& m,const momD<T,D> * c) {return m+=*c;});
	return compute_prop(in, prop, momentum, bc);
}

template <class T,size_t D> T Propagator<T,D>::get_scalar_propagator() const {
	return prop;
}

template <class T,size_t D> const momentumD<T,D> * Propagator<T,D>::get_momentum() const {
	return &momentum;
}

template <class T,size_t D> const std::vector<typename CTS<T>::Tcur>& CurrentWorker_Template_Base<T,D>::get_current() const {
	return current.get_current();
}

template <class T,size_t D> CurrentWorker_Template_Base<T,D>::~CurrentWorker_Template_Base(){
}

// Implementation of CurrentWorker
//template <class T,size_t D> std::vector<std::string> CurrentWorkerExternal<T,D>::collectall = std::vector<std::string>();

template <class T,size_t D> CurrentWorkerExternal<T,D>::CurrentWorkerExternal(CurrentExternal * incurrent,momD_conf<T,D>& inmc,size_t& imom,size_t& iref, size_t& mref): 
		CurrentWorker_Template_Base<T,D>(incurrent->get_statistics(),incurrent->get_source()->get_flavor()),
		momconf(&inmc), k(imom), q(iref), midx(mref),
		external_rule(get_external_rule<T,Tcur,D>(incurrent->get_current_particle(),(incurrent->psource)->external_state())) {
		DEBUG_MESSAGE("Build a CurrentWorkerExternal in D=",D," dim");
#ifdef DEBUG_ON
	CurrentWorker_Base::name=incurrent->get_name();
#endif
}

template <class T,size_t D> CurrentWorkerExternal<T,D>::CurrentWorkerExternal(CurrentExternal * incurrent,momD_conf<T,D>& inmc,size_t& imom,size_t& iref,size_t& mref, const SingleState& statelabel):
				CurrentWorker_Template_Base<T,D>(incurrent->get_statistics(),incurrent->get_source()->get_flavor()),
				momconf(&inmc), k(imom), q(iref), midx(mref),
				external_rule(get_external_rule<T,Tcur,D>(incurrent->get_current_particle(),statelabel)) {
	DEBUG_MESSAGE("Build a CurrentWorkerExternal in D=",D," dim");
#ifdef DEBUG_ON
	CurrentWorker_Base::name=incurrent->get_name()+"{"+statelabel.get_name()+"}";
#endif
}

template <class T,size_t D> void CurrentWorkerExternal<T,D>::compute(const BuilderContainer<T>& bc) {
	// Get external wave function
	CurrentWorker_Template_Base<T,D>::current.set(external_rule(*momconf,k,q,bc));
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
	std::cout<<"CurrentWorkerExternal "<<CurrentWorker_Base::name<<": current = "<<CurrentWorker_Template_Base<T,D>::current<<std::endl;
	std::cout<<"	(k,q)= ("<<k<<","<<q<<"), momenta= "<<*(this->get_momentum())<<std::endl;
#endif
#endif
}

template <class T,size_t D> void CurrentWorkerExternal<T,D>::compute_with_ref(size_t ref,bool& trackfail,const BuilderContainer<T>& bc) {
	T testvar((momconf->p(k))*(momconf->p(ref)));
// T_is_zero defined in States_Vector_Boson.hpp
	if(T_is_zero(testvar)){
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
std::cout<<"Failed to choose ref="<<ref<<" vector for k="<<k<<": k*ref= "<<(momconf->p(k))*(momconf->p(ref))<<" --- did nothing"<<std::endl;
#endif
#endif
		trackfail=true;
		return;
	}
	// Get external wave function
	CurrentWorker_Template_Base<T,D>::current.set(external_rule(*momconf,k,ref,bc));
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
	std::cout<<"CurrentWorkerExternal "<<CurrentWorker_Base::name<<": current = "<<CurrentWorker_Template_Base<T,D>::current<<std::endl;
	std::cout<<"	(k,ref)= ("<<k<<","<<ref<<"), momenta= "<<*(this->get_momentum())<<std::endl;
#endif
#endif
}

template <class T,size_t D> const momD<T,D> * CurrentWorkerExternal<T,D>::get_momentum() const {
	return &(momconf->p(k));
}

template <class T,size_t D> SubCurrentWorker<T,D>::SubCurrentWorker(CurrentBasic * incurrent,std::unordered_map<Current *,std::vector<CurrentWorker_Template_Base<T,D>* >>& match): 
		CurrentWorker_Template_Base<T,D>(incurrent->get_statistics(),incurrent->get_current_particle().get_flavor()), combine_rule(get_combine_rule<T,Tcur,D>(incurrent)) {
	DEBUG_MESSAGE("Build a SubCurrentWorker in D=",D," dim");

	// parents of a subcurrent are corresponding CurrentWorkers from CurrentBuild 

	std::vector<Current*> * lcparents(incurrent->get_parents());
	nparents=lcparents->size();
	// ordering of parents
	//int * indices(incurrent->get_indices());

	// create space for all parents
	parents=std::vector<CurrentWorker_Template_Base<T,D> const *>(nparents);
	momenta=std::vector<momD<T,D> const *>(nparents);

//std::cout<<"incurrent: "<<incurrent->get_name()<<" nparents: "<<nparents<<std::endl;
	// Now identify the corresponding parents
	for(int ii=0;ii<nparents;ii++){
		Current * lCurrent((*lcparents)[ii]);
		CurrentWorker_Template_Base<T,D>* lCW(nullptr);
		momD<T,D> const * lmom(nullptr);
		// no "state" passed, so there is a unique CurrentWorker
		lCW=(match[lCurrent])[0];
		lmom=lCW->get_momentum();
#ifdef DEBUG_ON
		if(match[lCurrent].size()!=1){
			std::cout<<"Trouble with current: "<<lCurrent->get_name()<<" it should have a single associated state!"<<std::endl;
		}
#endif
		parents[ii]=lCW;
		momenta[ii]=lmom;
//std::cout<<"Current: "<<lCurrent->get_name()<<" *(incurrent->get_indices()+ii)= "<<*(incurrent->get_indices()+ii)<<std::endl;
	}
#ifdef DEBUG_ON
	CurrentWorker_Base::name="";
#endif
}
					// parent					daughter
size_t get_index_for_match(size_t Ds,const PhysicalStateContainer& all_states,const PhysicalState& state);

template <class T,size_t D> SubCurrentWorker<T,D>::SubCurrentWorker(CurrentBasic * incurrent,
		std::unordered_map<Current *,std::vector<CurrentWorker_Template_Base<T,D>* >>& match,
		const PhysicalState& state): CurrentWorker_Template_Base<T,D>(incurrent->get_statistics(),incurrent->get_current_particle().get_flavor()), combine_rule(get_combine_rule<T,Tcur,D>(incurrent)) {
	DEBUG_MESSAGE("Build a SubCurrentWorker in D=",D," dim");

	// parents of a subcurrent are corresponding CurrentWorkers from CurrentBuild 

	std::vector<Current*> * lcparents(incurrent->get_parents());
	nparents=lcparents->size();
	// ordering of parents
	//int * indices(incurrent->get_indices());

	// create space for all parents
	parents=std::vector<CurrentWorker_Template_Base<T,D> const *>(nparents);
	momenta=std::vector<momD<T,D> const *>(nparents);

//std::cout<<"incurrent: "<<incurrent->get_name()<<" nparents: "<<nparents<<std::endl;
	// Now identify the corresponding parents
	for(int ii=0;ii<nparents;ii++){
		Current * lCurrent((*lcparents)[ii]);
		CurrentWorker_Template_Base<T,D>* lCW(nullptr);
		momD<T,D> const * lmom(nullptr);
		if(lCurrent->get_default_particles_processed()){
			// find which one of the currentworkers associated to lCurrent is the proper state match
			size_t k(get_index_for_match(D,lCurrent->get_physical_states(),state));
			lCW=(match[lCurrent])[k];
#ifdef DEBUG_ON
//std::cout<<"k: "<<k<<" "<<lCurrent->get_name()<<" daugther: "<<incurrent->get_name()<<" size of second: "<<match[jj].second.size() <<std::endl;
const PhysicalStateContainer& all_states(lCurrent->get_physical_states());
std::string toprint("Matched the state: "+state.get_short_name());
toprint+=" with the parent: "+all_states.get_state(k,D).get_short_name();
DEBUG_MESSAGE(toprint);
//std::cout<<"All parents: "; for(size_t ll=0;ll<all_states.size(D);ll++) std::cout<<ll<<" "<<all_states.get_state(ll,D).get_short_name()<<"; ";
//std::cout<<std::endl;
#endif
		}
		else{
			lCW=(match[lCurrent])[0];
		}
		lmom=lCW->get_momentum();
		parents[ii]=lCW;
		momenta[ii]=lmom;
//std::cout<<"Current: "<<lCurrent->get_name()<<" *(incurrent->get_indices()+ii)= "<<*(incurrent->get_indices()+ii)<<" their sizes are: "<<nparents<<std::endl;
	}
#ifdef DEBUG_ON
	CurrentWorker_Base::name="";
#endif
}

template <class T,size_t D> void SubCurrentWorker<T,D>::compute(const BuilderContainer<T>& bc) {
	CurrentWorker_Template_Base<T,D>::current.set(combine_rule(momenta,parents,bc));
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
	std::cout<<"SubCurrentWorker: current = "<<CurrentWorker_Template_Base<T,D>::current<<std::endl;
#endif
#endif
}

template <class T,size_t D> const momD<T,D> * SubCurrentWorker<T,D>::get_momentum() const {
	return nullptr;
}

template <class T, size_t D>
CurrentWorker<T, D>::CurrentWorker(CurrentBuild* incurrent, std::unordered_map<Current*, std::vector<CurrentWorker_Template_Base<T, D>*>>& match,
                                   momD_conf<T, D>& mconf, const Model& model)
    : CurrentWorker_Template_Base<T, D>(incurrent->get_statistics(), incurrent->get_current_particle().get_flavor()), propagator(incurrent, mconf, model) {
    DEBUG_MESSAGE("Build a CurrentWorker in D=", D, " dim");
    // store the number of subcurrents in CurrentBuild
    nsubcurrents = (incurrent->basicblocks).size();

    // create space for all subcurrents
    sub_currents = std::vector<SubCurrentWorker<T, D>>(nsubcurrents);
    for (int ii = 0; ii < nsubcurrents; ii++) {
        Current* lCBasic(incurrent->basicblocks[ii]);
        CurrentBasic* lcb(dynamic_cast<CurrentBasic*>(lCBasic));
        sub_currents[ii] = SubCurrentWorker<T, D>(lcb, match);
    }
#ifdef DEBUG_ON
	CurrentWorker_Base::name=incurrent->get_name();
#endif
}

template <class T, size_t D>
CurrentWorker<T, D>::CurrentWorker(CurrentBuild* incurrent, std::unordered_map<Current*, std::vector<CurrentWorker_Template_Base<T, D>*>>& match,
                                   momD_conf<T, D>& mconf, const PhysicalState& state, const Model& model)
    : CurrentWorker_Template_Base<T, D>(incurrent->get_statistics(), incurrent->get_current_particle().get_flavor()), propagator(incurrent, mconf, model) {
    DEBUG_MESSAGE("Build a CurrentWorker in D=", D, " dim");
    // store the number of subcurrents in CurrentBuild
    nsubcurrents = (incurrent->basicblocks).size();

    // create space for all subcurrents
    sub_currents = std::vector<SubCurrentWorker<T, D>>(nsubcurrents);
    for (int ii = 0; ii < nsubcurrents; ii++) {
        Current* lCBasic(incurrent->basicblocks[ii]);
        CurrentBasic* lcb(dynamic_cast<CurrentBasic*>(lCBasic));
        sub_currents[ii] = SubCurrentWorker<T, D>(lcb, match, state);
    }
#ifdef DEBUG_ON
	std::string idstate(state.get_short_name());
	CurrentWorker_Base::name=incurrent->get_name()+idstate;
#endif
}

template <class T,size_t D> void CurrentWorker<T,D>::compute(const BuilderContainer<T>& bc) {
	// Compute subcurrent
	sub_currents[0].compute(bc);
	CurrentWorker_Template_Base<T,D>::current.set(sub_currents[0].get_current());
	
	for(size_t ii=1;ii<sub_currents.size();ii++){
		sub_currents[ii].compute(bc);
		CurrentWorker_Template_Base<T,D>::current.add(sub_currents[ii].get_current());
	}

	// Include propagator
	CurrentWorker_Template_Base<T,D>::current.set(propagator.compute( this->get_current(), bc ));
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
	std::cout<<"CurrentWorker "<<CurrentWorker_Base::name<<": current = "<<CurrentWorker_Template_Base<T,D>::current<<std::endl;
	std::cout<<"	momenta= "<<*(this->get_momentum())<<"	corresponding prop= "<<propagator.get_scalar_propagator()<<std::endl;
#endif
#endif
}

template <class T,size_t D> const momD<T,D> * CurrentWorker<T,D>::get_momentum() const {
	return propagator.get_momentum();
}

// function to fill containers to traverse
template <class T,size_t D> void set_array_to_traverse(const std::vector<std::vector<Current *>>& currents,std::vector<CurrentWorker_Template_Base<T,D>*>& workers,
			const std::unordered_map<Current *,std::vector<CurrentWorker_Template_Base<T,D>* >>& match){
	size_t addedcurrents(0);
	size_t maxentry(workers.size());
	for(size_t ii=0;ii<currents.size();ii++){
		for(size_t jj=0;jj<currents[ii].size();jj++){
			Current* lcurrent(currents[ii][jj]);
			const std::vector<CurrentWorker_Template_Base<T,D>* >& vworkers(match.at(lcurrent));
			for(size_t kk=0;kk<vworkers.size();kk++){
				if(addedcurrents<maxentry)
					workers[addedcurrents]=vworkers[kk];
				else
					std::cout<<"ERROR in set_array_to_traverse(.): can't add entry "<<addedcurrents<<" to the worker vector with size: "<<workers.size()<<" --- did nothing!"<<std::endl;
				addedcurrents++;
			}
		}
	}
}

// function for cut to fill containers to traverse; calls set_array_to_traverse and add info for loop-particle external currents
template <class T,size_t D> void set_array_to_traverse_for_cut(const std::vector<std::vector<Current *>>& currents,
			std::vector<CurrentWorker_Template_Base<T,D>*>& workers,
			const std::unordered_map<Current *,std::vector<CurrentWorker_Template_Base<T,D>* >>& match,
			int& count_level_1){
	// First call set_array_to_traverse
	set_array_to_traverse(currents,workers,match);

	// Now deal with level one currents (external states) to allow to compute with explicit ref vector
	size_t addedcurrents(0);
	for(size_t jj=0;jj<currents[0].size();jj++){
		Current* lcurrent(currents[0][jj]);
		addedcurrents+=(match.at(lcurrent)).size();
	}
	count_level_1=addedcurrents;
}

#ifdef DEBUG_ON
        template <typename T,typename std::enable_if_t<is_floating_point<T>::value>* = nullptr> void __check_state_and_numerical_type(int ncutsum){
            if(ncutsum!=0 and settings::general::cut_propagator_numerators.value == settings::general::cut_states::projective){
                _WARNING("WARNING: Builder with floating point type ", _typeid(T),
                        " is constructed with projective cut propagators. Perhaps the setting  cut_propagator_numerators is not set correctly?");
            }
        }
        template <typename T,typename std::enable_if_t<!is_floating_point<T>::value>* = nullptr> void __check_state_and_numerical_type(int ncutsum){}
#endif




// derecated constructor
template <class T,size_t D,size_t Ds> Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::Builder_fixed_Ds(Forest* forest,const momD_conf<T,4>& emom): Builder_fixed_Ds(forest,emom.n()) {
    set_p(emom);
}

// Implementation of Builder_fixed_Ds
template <class T,size_t D,size_t Ds> Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::Builder_fixed_Ds(Forest* forest,size_t external_multiplicity): nprocs(0), ntreescuts(0), nlevels(0), 
		totaltreecurrents(0), ncutsum(0), extra_momenta_space(10), max_n_loops(0), physical_PS_multiplicity(external_multiplicity) {
      DEBUG_MESSAGE("Starting assembly of current workers in D=",D," Ds=",Ds," dimensions");	

	// Check that there is at least one current for Ds
	if(forest->get_total_number_of_treelike_currents(Ds)==0){
		std::cout<<"Builder_fixed_Ds constructor received Forest without any tree or cut. Did nothing"<<std::endl;
		return;
	}

	// access all currents in the forest
	const std::vector<std::vector<Current *>> fcurrents(forest->get_all_currents(Ds));
	nlevels=fcurrents.size();
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
std::cout<<"Size of currents needed: "<<std::endl;
for(size_t ii=0;ii<fcurrents.size();ii++){
	std::cout<<"	level: "<<ii+1<<" has "<<fcurrents[ii].size()<<" currents"<<std::endl;
}
#endif
#endif

	// store locally loop orders contained in forest
	loop_orders=forest->get_loop_count_present();
	std::sort(loop_orders.begin(),loop_orders.end());
	DEBUG_MESSAGE("Builder_fixed_Ds has currents with the following possible loop orders: ",loop_orders);
	if(loop_orders.size()==0){
		max_n_loops=0;
	}
	else{
		max_n_loops=loop_orders.back();
	}

        momD_conf<T,D> emom(external_multiplicity);

	// initilialize momentum configurations
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
	size_t n_momDconfs(forest->number_of_momDconf_copies());
	localmom=std::vector<momD_conf<T,D>>(n_momDconfs,static_cast<decltype(localmom)>(emom));
	localmomDs=std::vector<momD_conf<T,Ds>>(n_momDconfs,static_cast<decltype(localmom)>(emom));
	// build information for map
	for(int xx=1;xx<=int(forest->get_size_of_cut_register());xx++){
		cut_index_to_momD_conf_map[xx]=forest->corresponding_momDconf_for_cut(xx);
	}
	// add a default value ---> 0
	cut_index_to_momD_conf_map[0]=0;
	DEBUG_MESSAGE("Builder has been constructed with the possibility of handling up to ",n_momDconfs," independent threads");
#else
	localmom=static_cast<decltype(localmom)>(emom);
	localmomDs=static_cast<decltype(localmomDs)>(emom);
#endif

	physical_PS_multiplicity=emom.n();
	momentumD<T,D> dummy_loop;
#ifdef DEBUG_ON
	int dummy_index(0);
#endif
	for(size_t ll=0;ll<max_n_loops;ll++){
		// two allocations added per loop momenta
#ifdef DEBUG_ON
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		for(size_t yy=0;yy<n_momDconfs;yy++){
			dummy_index=localmom[yy].insert(dummy_loop);
			dummy_index=localmom[yy].insert(dummy_loop);

			dummy_index=localmomDs[yy].insert(dummy_loop);
			dummy_index=localmomDs[yy].insert(dummy_loop);
		}
#else
		dummy_index=localmom.insert(dummy_loop);
		dummy_index=localmom.insert(dummy_loop);

		dummy_index=localmomDs.insert(dummy_loop);
		dummy_index=localmomDs.insert(dummy_loop);
#endif
#else
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		for(size_t yy=0;yy<n_momDconfs;yy++){
			localmom[yy].insert(dummy_loop);
			localmom[yy].insert(dummy_loop);

			localmomDs[yy].insert(dummy_loop);
			localmomDs[yy].insert(dummy_loop);
		}
#else
		localmom.insert(dummy_loop);
		localmom.insert(dummy_loop);

		localmomDs.insert(dummy_loop);
		localmomDs.insert(dummy_loop);
#endif
#endif
	}
	for(size_t ll=0;ll<extra_momenta_space;ll++){
		// extra allocations for handling ref mom
#ifdef DEBUG_ON
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		for(size_t yy=0;yy<n_momDconfs;yy++){
			dummy_index=localmom[yy].insert(dummy_loop);

			dummy_index=localmomDs[yy].insert(dummy_loop);
		}
#else
		dummy_index=localmom.insert(dummy_loop);

		dummy_index=localmomDs.insert(dummy_loop);
#endif
#else
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		for(size_t yy=0;yy<n_momDconfs;yy++){
			localmom[yy].insert(dummy_loop);

			localmomDs[yy].insert(dummy_loop);
		}
#else
		localmom.insert(dummy_loop);

		localmomDs.insert(dummy_loop);
#endif
#endif
	}
#ifdef DEBUG_ON
	DEBUG_MESSAGE("Physical space with dimension: "+std::to_string(physical_PS_multiplicity)+" and cut PS with up to "+std::to_string(2*max_n_loops)+" dimensions ("+std::to_string(dummy_index)+")");
#endif
	// create needed partial_momD_conf
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
	if(n_momDconfs>0)
		physical_momD_conf=partial_momD_conf<T,D>(localmom[0],1,physical_PS_multiplicity);
	else
		std::cout<<"ERROR: can not assign physical_momD_conf when forest has "<<n_momDconfs<<" number of momD_conf copies!"<<std::endl;
	loop_momD_conf=std::vector<partial_momD_conf<T,D>>(n_momDconfs);
	for(size_t yy=0;yy<n_momDconfs;yy++){
		loop_momD_conf[yy]=partial_momD_conf<T,D>(localmom[yy],physical_PS_multiplicity+1,2*max_n_loops);
	}
#else
	physical_momD_conf=partial_momD_conf<T,D>(localmom,1,physical_PS_multiplicity);
	loop_momD_conf=partial_momD_conf<T,D>(localmom,physical_PS_multiplicity+1,2*max_n_loops);
#endif
	
	

	// vector to wire CurrentWorker inheritance
	std::unordered_map<Current *,std::vector<current_type_worker* >> matchcurrents;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// In what follows all worker currents are created, first level 1 and then loop over the rest
// 
// Temporarily all information is kept on the matching vector "matchcurrents"
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	external_current_owner=std::vector<current_external_type_worker>(forest->get_treelike_currents_per_level(Ds,1));
//std::cout<<"Check size: "<<fcurrents.size()<<" levels: "<<nlevels<<std::endl;
//std::cout<<"CWE SPACE requested: "<<forest->get_treelike_currents_per_level(Ds,1)<<std::endl;

	if(fcurrents.size()==0){
		std::cout<<"ERROR: Builder_fixed_Ds constructor in D="<<D<<" Ds="<<Ds<<" dimensions, received empty set of current from Forest! -- Did nothing!"<<std::endl;
		return;
	}
	size_t counter_external_current_owner(0);
	// Now add all external (level 1) currents
	for(size_t ii=0;ii<fcurrents[0].size();ii++){
		CurrentExternal * lCE(dynamic_cast<CurrentExternal*>( fcurrents[0][ii] ));
		current_type_worker* lCW;

		size_t lmomi((lCE->psource)->mom_index());
		size_t lrefmomi((lCE->psource)->ref_index());
		size_t mrefmomi((lCE->psource)->get_mass_index());

		// Check how many physical states has lCE associated
		if(!lCE->get_default_particles_processed()){
			// unique state

			external_current_owner[counter_external_current_owner]=current_external_type_worker(lCE,
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				localmomDs[0]
#else
				localmomDs
#endif
				,lmomi,lrefmomi,mrefmomi);
			lCW=&external_current_owner[counter_external_current_owner];
			counter_external_current_owner++;
			std::vector<current_type_worker* > lv;
			lv.push_back(lCW);
			// add pair for inheritance
			matchcurrents[lCE]=lv;
		}
		else{
			// multiple states

			std::vector<current_type_worker* > lv;
			const PhysicalStateContainer& physical_states(lCE->get_physical_states());
//std::cout<<"# Ds="<<Ds<<" physical states: "<<physical_states.size(Ds)<<" current: "<<lCE->get_name()<<std::endl;
			for(size_t jj=0;jj<physical_states.size(Ds);jj++){
				// only one state, find it
				SingleState statelabel(physical_states.get_state(jj,Ds).SingleState_of_particle_state(lCE->psource));
				external_current_owner[counter_external_current_owner]=current_external_type_worker(lCE,
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
					localmomDs[lCE->get_default_momDconf()]
#else
					localmomDs
#endif
					,lmomi,lrefmomi,mrefmomi,statelabel);
				lCW=&external_current_owner[counter_external_current_owner];
				counter_external_current_owner++;
				lv.push_back(lCW);
			}
			// add pair for inheritance
			matchcurrents[lCE]=lv;
		}
	}
//std::cout<<"CWE SPACE used: "<<counter_external_current_owner<<std::endl;
	
	current_owner=std::vector<std::vector<current_type>>(nlevels-1);
//std::cout<<"CW levels SPACE requested: "<<nlevels-1<<std::endl;
	for(int ii=1;ii<nlevels;ii++){
		current_owner[ii-1]=std::vector<current_type>(forest->get_treelike_currents_per_level(Ds,ii+1));
//std::cout<<"CW level: "<<ii<<" SPACE requested: "<<forest->get_treelike_currents_per_level(Ds,ii+1)<<std::endl;
		size_t counter_level_current_owner(0);
		for(size_t jj=0;jj<fcurrents[ii].size();jj++){
			CurrentBuild * lCB(dynamic_cast<CurrentBuild*>( fcurrents[ii][jj] ));
			current_type * lCW;
			// Check how many physical states has lCB associated
			if(!lCB->get_default_particles_processed()){
				// unique state

				current_owner[ii-1][counter_level_current_owner]=current_type(lCB,matchcurrents,
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
					localmomDs[0],
#else
					localmomDs,
#endif
					forest->get_model());
				lCW=&current_owner[ii-1][counter_level_current_owner];
				counter_level_current_owner++;
				std::vector<current_type_worker* > lv;
				lv.push_back(lCW);
				// add pair for inheritance
				matchcurrents[lCB]=lv;				
			}
			else{
				// multiple states

				std::vector<current_type_worker* > lv;
				const PhysicalStateContainer& physical_states(lCB->get_physical_states());
//std::cout<<"# Ds="<<Ds<<" physical states: "<<physical_states.size(Ds)<<std::endl;
				for(size_t kk=0;kk<physical_states.size(Ds);kk++){
//std::cout<<"Trying to fill: current_owner["<<ii-1<<"]["<<counter_level_current_owner<<"]"<<std::endl;
					if(counter_level_current_owner<current_owner[ii-1].size()){
						current_owner[ii-1][counter_level_current_owner]=current_type(lCB,matchcurrents,
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
							localmomDs[lCB->get_default_momDconf()]
#else
							localmomDs
#endif
							,physical_states.get_state(kk,Ds),forest->get_model());
						lCW=&current_owner[ii-1][counter_level_current_owner];
						counter_level_current_owner++;
						lv.push_back(lCW);
					}
					else{
						std::cout<<"ERROR: trying to fill current_owner["<<ii-1<<"]["<<counter_level_current_owner<<"] which has max size: "<<current_owner[ii-1].size()
								<<" -- Did nothing! "<<std::endl;
						counter_level_current_owner++;
						lv.push_back(nullptr);
					}
				}
				// add pair for inheritance
				matchcurrents[lCB]=lv;				
			}
		}
//std::cout<<"CW level: "<<ii<<" SPACE used: "<<counter_level_current_owner<<std::endl;
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Now we build the containers for traversing the tree or cut_i currents
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// build the space for storing all info
	totaltreecurrents=forest->get_total_number_of_tree_currents();
#ifdef DEBUG_ON
std::string toprint("total # of tree currents = "+std::to_string(totaltreecurrents));
DEBUG_MESSAGE(toprint);
#endif
	// Allocate array for all CurrentWorker's
	tree_workers=std::vector<current_type_worker*>(totaltreecurrents);

	// fill up tree_workers
	const std::vector<std::vector<Current *>>& treecurrents(forest->get_all_tree_currents());
	set_array_to_traverse(treecurrents,tree_workers,matchcurrents);

	// store the number of cuts added to the forest for this Ds
	ncutsum=int(forest->get_size_of_cut_register());

#ifdef DEBUG_ON
        __check_state_and_numerical_type<T>(ncutsum);
#endif

		
	for(size_t ii=0;ii<size_t(ncutsum);ii++){
		// initialize to zero the counter for each cut register
		totalcutcurrents.push_back(0);
		totalcutcurrents_level_1.push_back(0);
		// Store in the Builder_fixed_Ds the associated reference vectors to the external loop-particle currents (ii+1 as cut index is 1-based)
		ref_index_for_cut.push_back(forest->get_ref_mom_for_cut(int(ii+1)));
	}

	// each element in current register generates a register of currents to traverse
	cut_workers=std::vector<std::vector<current_type_worker*>>(totalcutcurrents.size());
	cut_workers_external=std::vector<std::vector<current_external_type_worker*>>(totalcutcurrents.size());

	// now generate registers for each cut
	for(size_t ii=0;ii<totalcutcurrents.size();ii++){
		// the corresponding top level current
		Current* lcurrent(std::get<1>((forest->all_processes)[forest->get_current_index_for_cut(int(ii+1))-1]));
		if(fcurrents.size()>0){
			// check that the current is included in this Ds
			if(std::find(fcurrents.back().begin(),fcurrents.back().end(),lcurrent)==fcurrents.back().end())
				totalcutcurrents[ii]=0;
			else
				totalcutcurrents[ii]=forest->get_count_of_all_level_states_for_cut(Ds,ii+1);
		}
		else{
			totalcutcurrents[ii]=0;
		}
//std::cout<<"The counting value: "<<ii<<" "<<totalcutcurrents[ii]<<std::endl;
		// Allocate array for all CurrentWorker's
		cut_workers[ii]=std::vector<current_type_worker*>(totalcutcurrents[ii]);

		// fill up cut_workers[ii]
		const std::vector<std::vector<Current *>>& cutcurrents(std::get<3>((forest->all_processes)[forest->get_current_index_for_cut(int(ii+1))-1]));
		int count_level_1_currents(0);
		if(totalcutcurrents[ii]>0)
			set_array_to_traverse_for_cut(cutcurrents,cut_workers[ii],matchcurrents,count_level_1_currents);
		totalcutcurrents_level_1[ii]=count_level_1_currents;
		// Allocate array for all CurrentWorkerExternal's
		cut_workers_external[ii]=std::vector<current_external_type_worker*>(totalcutcurrents_level_1[ii]);
		for(int jj=0;jj<totalcutcurrents_level_1[ii];jj++){
			cut_workers_external[ii][jj]=dynamic_cast<current_external_type_worker*>(cut_workers[ii][jj]);
//std::cout<<"Storing lCWE: "<<jj<<" "<<cut_workers_external[ii][jj]<<std::endl;
		}
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Now built containers to properly make contractions for trees and cuts
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// copies forest dimensions
	dimensions=forest->get_added_dimensions();
	// sort (just in case)
	std::sort(dimensions.begin(),dimensions.end());
	// and only keep dims up to Ds
	while(dimensions.back()>Ds)
		dimensions.pop_back();
	// create an entry for each dimension in process_register
	for(size_t d=0;d<dimensions.size();d++){
		process_register.push_back({dimensions[d],std::vector<std::pair<int,std::vector<int>>>()});
	}

	// Prepare currents for final external particles
	nprocs=(forest->all_processes).size();
	// store how many values associated with each nprocs
	// initialize to 1 all nprocs entries
	std::vector<int> values_each_proc(nprocs,1);
	for(size_t ii=0;ii<size_t(ncutsum);ii++){
		// the corresponding top level current
		Current* lcurrent(std::get<1>((forest->all_processes)[forest->get_current_index_for_cut(int(ii+1))-1]));
		if(fcurrents.size()>0){
			if(std::find(fcurrents.back().begin(),fcurrents.back().end(),lcurrent)==fcurrents.back().end())
				values_each_proc[forest->get_current_index_for_cut(int(ii+1))-1]=0;
			else
				values_each_proc[forest->get_current_index_for_cut(int(ii+1))-1]=forest->get_count_of_states_for_cut(Ds,int(ii+1));
		}
		else
			values_each_proc[forest->get_current_index_for_cut(int(ii+1))-1]=0;
//std::cout<<"IN Ds: "<<Ds<<" ii: "<<ii<<" count is: "<<forest->get_count_of_states_for_cut(Ds,int(ii+1))<<std::endl;
	}
	// find out how many individual processes will be computed
	ntreescuts=0;
	for(int ii=0;ii<nprocs;ii++){
		ntreescuts+=values_each_proc[ii];
	}

	fullcontract=std::vector<T>(ntreescuts,T(0));
	FinalExternal=std::vector<current_type_worker*>(ntreescuts);
	FinalCurrentContract=std::vector<current_type_worker*>(ntreescuts);
	int tracking_added_values(0);
	for(int ii=0;ii<nprocs;ii++){

		std::vector<std::pair<int,std::vector<int>>> local_register;
		for(size_t nn=0;nn<dimensions.size();nn++)	local_register.push_back(std::make_pair(ii,std::vector<int>()));
		if(values_each_proc[ii]>0){
			// check if case has multiple currents associated
			if(!std::get<1>(forest->all_processes[ii])->get_default_particles_processed()){
				// single state
	
				Current * lCE(std::get<2>(forest->all_processes[ii]));
				if(tracking_added_values<ntreescuts)
					FinalExternal[tracking_added_values]=(matchcurrents[lCE])[0];
				else
					std::cout<<"ERROR: trying to add entry: "<<tracking_added_values<<" to FinalExternal which accepts only "<<ntreescuts<<" entries! -- Did nothing"<<std::endl;
	
				Current * lCTop(std::get<1>(forest->all_processes[ii]));
				if(tracking_added_values<ntreescuts){
					FinalCurrentContract[tracking_added_values]=(matchcurrents[lCTop])[0];
					// Define contraction rule for this top-level current
					if(FinalCurrentContract[tracking_added_values]!=nullptr)
						FinalCurrentContract[tracking_added_values]->final_contraction_rule=get_final_contraction_rule<T,Tcur,Ds>(lCTop);
					else
						std::cout<<"ERROR: traking a nullptr as top level current for: "<<lCTop->get_name()<<" -- Did not define a final contraction rule!"<<std::endl;
				}
				else
					std::cout<<"ERROR: trying to add entry: "<<tracking_added_values<<" to FinalCurrentContract which accepts only "<<ntreescuts<<" entries! -- Did nothing"<<std::endl;
	
				// compute overall factors
				tree_normalization_info[tracking_added_values]=get_overall_normalization_info(forest,ii).front();
				tree_overall_factors[tracking_added_values]=T(1);
				tree_normalization_info[tracking_added_values].set_normalization_factor(tree_overall_factors[tracking_added_values]);
//std::cout<<"New overall tree factor: "<<tracking_added_values<<" "<<tree_overall_factors[tracking_added_values]<<std::endl;
	
				// all dimensions carry the same tree-level information
				for(size_t nn=0;nn<local_register.size();nn++){
					local_register[nn].second.push_back(tracking_added_values);
				}
				// increase counting
				tracking_added_values++;
	
				// Add tree_location info
				tree_location.push_back(ii);
			}
			else{
				// multiple states
	
				// The two relevant Current*
				Current * lCE(std::get<2>(forest->all_processes[ii]));
				Current * lCTop(std::get<1>(forest->all_processes[ii]));

				auto Eentry=matchcurrents[lCE];
				auto Topentry=matchcurrents[lCTop];
	
				// one per each full combination of states
				for(size_t iTop=0;iTop<lCTop->get_physical_states().size(Ds);iTop++){
					PhysicalState top_state(lCTop->get_physical_states().get_state(iTop,Ds));
					size_t iE(top_state.get_index_correlated_state(lCE->get_physical_states()));
	
					DEBUG_MESSAGE(lCTop->get_name(),top_state.get_short_name()," contracted with "+lCE->get_name(),lCE->get_physical_states()[iE].get_short_name());
	
					if(tracking_added_values<ntreescuts)
						FinalExternal[tracking_added_values]=Eentry[iE];
					else
						std::cout<<"ERROR: trying to add entry: "<<tracking_added_values<<" to FinalExternal which accepts only "<<ntreescuts<<" entries! -- Did nothing"<<std::endl;
		
					if(tracking_added_values<ntreescuts){
						FinalCurrentContract[tracking_added_values]=Topentry[iTop];
						// Define contraction rule for this top-level current
						if(FinalCurrentContract[tracking_added_values]!=nullptr)
							FinalCurrentContract[tracking_added_values]->final_contraction_rule=get_final_contraction_rule<T,Tcur,Ds>(lCTop);
						else
							std::cout<<"ERROR: traking a nullptr as top level current for: "<<lCTop->get_name()<<" "<<top_state.get_short_name()<<" -- Did not define a final contraction rule!"<<std::endl;
					}
					else
						std::cout<<"ERROR: trying to add entry: "<<tracking_added_values<<" to FinalCurrentContract which accepts only "<<ntreescuts<<" entries! -- Did nothing"<<std::endl;
		
		
					for(size_t nn=0;nn<dimensions.size();nn++){
						// check if dimensions[nn] is included in dims of the states considered
						if(std::find(top_state.dims.begin(),top_state.dims.end(),dimensions[nn])!=top_state.dims.end())
							local_register[nn].second.push_back(tracking_added_values);
					}
					// increase counting
					tracking_added_values++;
				}
	
				// Add cut_location info
				cut_location.push_back(ii);
			}
		}
		else{
			// Add cut_location info
			cut_location.push_back(ii);
		}
		// Add info to process_register
		for(size_t nn=0;nn<local_register.size();nn++){
			process_register[nn].cut_tracker.push_back(local_register[nn]);
		}
	}
#if 0
for(size_t ii=0;ii<process_register.size();ii++){
	std::cout<<"process_register in D= "<<process_register[ii].dim<<std::endl;
  for(size_t jj=0;jj<process_register[ii].cut_tracker.size();jj++)
	std::cout<<"	"<<jj<<" "<<process_register[ii].cut_tracker[jj].first<<" --- "<<process_register[ii].cut_tracker[jj].second<<std::endl;
}
#endif
	// prepare register for cutsum
	cutsum.clear();
	for(auto& pr_dim:process_register)
		cutsum.push_back(std::vector<T>(pr_dim.cut_tracker.size(),T(0)));

	for(int ll=0;ll<ncutsum;ll++){
		// the index of the cut
		int ii_local(forest->get_current_index_for_cut(ll+1));
		// compute overall factors
		cut_normalization_info[ll+1]=get_overall_normalization_info(forest,ii_local-1);
		cut_overall_factors[ll+1]=std::vector<T>(dimensions.size(),T(1));
                for(size_t i = 0; i< dimensions.size(); i++){
                    cut_normalization_info[ll+1][i].set_normalization_factor(cut_overall_factors[ll+1][i]);
                }
//std::cout<<"New overall cut factor: "<<tracking_added_values<<" "<<cut_overall_factors[ll+1]<<std::endl;
	}

        // register to update constants when cardinality is changed
        register_for_cardinality_upate();

        // checks all tree and cut normalizations and (in case of real evaluation) reports missing factors
        normalization_report();
}

template <class T,size_t D,size_t Ds> template <typename TT> std::enable_if_t<is_finite_field<TT>::value> Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::register_for_cardinality_upate(){
    cardinality_update_manager<T>::get_instance()->add_builder(this);
}
template <class T,size_t D,size_t Ds> template <typename TT> std::enable_if_t<is_finite_field<TT>::value> Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::un_register_for_cardinality_upate(){
    cardinality_update_manager<T>::get_instance()->remove_builder(this);
}
template <class T,size_t D,size_t Ds> Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::~Builder_fixed_Ds(){ un_register_for_cardinality_upate(); }


// traverse the tree currents
template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::compute(){
	DEBUG_MESSAGE("Will go through evaluating all tree currents");	
	std::for_each(tree_workers.begin(),tree_workers.end(),[&](auto* tw) {tw->compute(bcontainer);});
}

// traverse the cut_i currents
template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::compute_cut(int k){
	DEBUG_MESSAGE("Will go through evaluating all currents for cut ",k," including all dimensions");	
	bool failref(true);
	size_t attempts(0);
	size_t ref2use(ref_index_for_cut[k-1]);
	// default ref
	if(ref2use==0){
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
std::cout<<"This cut will use default reference for loop particles --- P conjugate"<<std::endl;
#endif
#endif
		std::for_each(cut_workers_external[k-1].begin(),cut_workers_external[k-1].end(),[&](current_external_type_worker *cwe){cwe->compute(bcontainer);});
	}
	else{
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
std::cout<<"This cut will use ref: "<<ref2use<<" for loop particles --- first attempt"<<std::endl;
#endif
#endif
		while(failref && attempts<_REFV_TRIALS){
			failref=false;
			for(int ii=0;ii<totalcutcurrents_level_1[k-1];ii++){
				cut_workers_external[k-1][ii]->compute_with_ref(ref2use+attempts,failref,bcontainer);
				if(failref)
					break;
			}
			if(attempts==0)	ref2use=1;
			attempts++;
		}
		// In this case report the trouble
		if(attempts==_REFV_TRIALS && failref)
			_WARNING("For cut="+std::to_string(k)+" compute_cut() failed to find a suitable ref mom. Started attempt with: "+std::to_string(ref_index_for_cut[k-1]));
	}
	std::for_each(cut_workers[k-1].begin()+totalcutcurrents_level_1[k-1],cut_workers[k-1].end(),
		[&](auto* cw){cw->compute(bcontainer);});
}

// < ... | . > contractions to get trees
template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::contract(){
	DEBUG_MESSAGE("Final tree contraction and storing of results");	

	// Let's now compute explicitly all contractions for top level currents with external wave function of last particle
	for(size_t ii=0;ii<tree_location.size();ii++){
		// ii_local points to proper container of tree
		int ii_local((process_register.back().cut_tracker[tree_location[ii]].second)[0]);
		fullcontract[ii_local]=FinalCurrentContract[ii_local]->final_contraction_rule(FinalCurrentContract[ii_local]->get_current(),FinalExternal[ii_local]->get_current(),bcontainer);
		// include couplings
		fullcontract[ii_local]*=tree_overall_factors[ii_local];
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
		std::cout<<"Corresponding upper level Current gets: "<<FinalCurrentContract[ii_local]->get_current()<<std::endl;
		std::cout<<"And I will contract with the pol vector: "<<FinalExternal[ii_local]->get_current()<<std::endl;
		//std::cout<<"With mom: "<<localmom.p(nlevels+1)<<std::endl;
		std::cout<<"(including overall factor (couplings and external states): "<<tree_overall_factors[ii_local]<<")"<<std::endl;
		std::cout<<"================="<<std::endl;
		std::cout<<"Tree("<<ii<<","<<ii_local<<") = "<<std::setprecision(6)<<fullcontract[ii_local]<<std::setprecision(6)<<std::endl;
		std::cout<<"================="<<std::endl;
#endif
#endif
	}
}

// < ... | . > contractions to get cut_i
template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::contract_cut(int k){
	DEBUG_MESSAGE("Contractions and storing of results for cut_",k," for all dimensions considered");	
	
	if(k>int(cut_location.size())){
		_WARNING("Called contract_cut with int: "+std::to_string(k)+" which is larger than the registered cuts -- do nothing");
		return;	
	}
	
	// fill the fullcontract array employing the top dimension
	const std::vector<int>& locations_top(process_register.back().cut_tracker[cut_location[k-1]].second);
	for(size_t ii=0;ii<locations_top.size();ii++){
		int ii_local(locations_top[ii]);
		fullcontract[ii_local]=FinalCurrentContract[ii_local]->final_contraction_rule(FinalCurrentContract[ii_local]->get_current(),FinalExternal[ii_local]->get_current(),bcontainer);
	}

	// now make sums for each dimension
	for(size_t dd=0;dd<dimensions.size();dd++){
		const std::vector<int>& locations(process_register[dd].cut_tracker[cut_location[k-1]].second);

		// Let's now compute explicitly all contractions for top level currents with external wave function of last particle
		cutsum[dd][k-1]=T(0);
		for(size_t ii=0;ii<locations.size();ii++){
			int ii_local(locations[ii]);
			cutsum[dd][k-1]+=fullcontract[ii_local];
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
			std::cout<<"Corresponding upper level Current gets: "<<FinalCurrentContract[ii_local]->get_current()<<std::endl;
			std::cout<<"And I will contract with the pol vector: "<<FinalExternal[ii_local]->get_current()<<std::endl;
			//std::cout<<"With mom: "<<localmom.p(nlevels+1)<<std::endl;
			std::cout<<"================="<<std::endl;
			std::cout<<"Cut(Ds="<<dimensions[dd]<<","<<ii<<","<<ii_local<<") = "<<std::setprecision(6)<<fullcontract[ii_local]<<std::setprecision(6)<<std::endl;
			std::cout<<"================="<<std::endl;
#endif
#endif
		}
		// include couplings
		cutsum[dd][k-1]*=cut_overall_factors[k][dd];
#ifdef DEBUG_ON
#if _DEBUG_CURRENTS
		std::cout<<"================="<<std::endl;
		std::cout<<"Cut (including overall factor (couplings and external states): "<<cut_overall_factors[k][dd]<<") Sum(Ds="<<dimensions[dd]<<","<<k<<") = "<<cutsum[dd][k-1]<<std::endl;
		std::cout<<"================="<<std::endl;
#endif
#endif
	}
}

// Returns value of computed trees
template <class T,size_t D,size_t Ds> T Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::get_tree(int i) const {
	if(i==0){	_WARNING("Called zeroth tree --- returned zero!");	return T(0);	}
	if(ntreescuts==0){	_WARNING("No trees to compute --- returned zero!");	return T(0);	}
	if(i<1||i>int(process_register.back().cut_tracker.size())){	_WARNING("get_tree(.) called out of bounds --- returned zero!");	return T(0);	}

	// ii_local points to proper container of tree
	int ii_local((process_register.back().cut_tracker[i-1].second)[0]);

	if(ii_local<0||ii_local+1>ntreescuts){	_WARNING("get_tree(.) called out of bounds --- returned zero!");	return T(0);	}
	return fullcontract[ii_local];
}

template <class T,size_t D,size_t Ds> T Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::get_cut(int i,size_t dim) const {
	if(i<1||i>ncutsum){	_WARNING("get_cut(i) with i="+std::to_string(i)+" out of bounds=(0,"+std::to_string(ncutsum)+"] --- returned zero!");	return T(0);	}
	if(ntreescuts==0){	_WARNING("No cuts to compute --- returned zero!");	return T(0);	}
	size_t dim_index(0);
	// default value returns the highest dim
	if(dim==0)
		dim_index=dimensions.size()-1;
	else{
		auto dimit(std::find(dimensions.begin(),dimensions.end(),dim));
		if(dimit==dimensions.end()){
			_WARNING("Dimension requested: "+std::to_string(dim)+" is not included in Builder_fixed_Ds! Returned zero!");
			return T(0);
		}
		else
			dim_index=dimit-dimensions.begin();
	}
	return cutsum[dim_index][i-1];
}

template <class T,size_t D,size_t Ds> const momD_conf<T,D>& Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::get_phys_momD_conf() const {
	return physical_momD_conf;
}

template <class T,size_t D,size_t Ds> const momD_conf<T,D>& Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::get_loop_momD_conf(
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		int cutindex
#endif
	) const {
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
	return loop_momD_conf[cut_index_to_momD_conf_map.at(cutindex)];
#else
	return loop_momD_conf;
#endif
}

template <class T,size_t D,size_t Ds> size_t Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::get_phys_size() const {
	return physical_PS_multiplicity;
}

template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::set_p(const momD<T,4>& m,size_t n){
	// bounds check
	if(n<1||n>physical_PS_multiplicity)	_WARNING("Trying set_p(mom,i="+std::to_string(n)+") in Builder_fixed_Ds, out of bounds -- Did nothing");
	else{
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		for(size_t ii=0;ii<localmom.size();ii++){
			localmom[ii].set_p(m,n);
			localmomDs[ii].set_p(m,n);
		}
#else
		localmom.set_p(m,n);
		localmomDs.set_p(m,n);
#endif
	}
}

template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::set_p(const momD_conf<T,4>& m){
    assert(m.size()==physical_PS_multiplicity);
    for(size_t n=1; n<= physical_PS_multiplicity; n++){
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		for(size_t ii=0;ii<localmom.size();ii++){
			localmom[ii].set_p(m[n],n);
			localmomDs[ii].set_p(m[n],n);
		}
#else
		localmom.set_p(m[n],n);
		localmomDs.set_p(m[n],n);
#endif
    }
}

template <class T,size_t D,size_t Ds> template <size_t DD, typename> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::set_p(const momD_conf<T,DD>& m){
    assert(m.size()==physical_PS_multiplicity);
    for(size_t n=1; n<= physical_PS_multiplicity; n++){
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		for(size_t ii=0;ii<localmom.size();ii++){
			localmom[ii].set_p(m[n],n);
			localmomDs[ii].set_p(m[n],n);
		}
#else
		localmom.set_p(m[n],n);
		localmomDs.set_p(m[n],n);
#endif
    }
}

template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::set_loop(const momD<T,D>& m,size_t n
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, int cutindex
#endif
		){
	// bounds check
	if(n<1||n>max_n_loops)	_WARNING("Trying set_loop(mom,i="+std::to_string(n)+") in Builder_fixed_Ds, out of bounds -- Did nothing");
	else{
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		localmom[cut_index_to_momD_conf_map.at(cutindex)].set_p(m,physical_PS_multiplicity+2*n-1);
		localmom[cut_index_to_momD_conf_map.at(cutindex)].set_p(-m,physical_PS_multiplicity+2*n);

		localmomDs[cut_index_to_momD_conf_map.at(cutindex)].set_p(m,physical_PS_multiplicity+2*n-1);
		localmomDs[cut_index_to_momD_conf_map.at(cutindex)].set_p(-m,physical_PS_multiplicity+2*n);
#else
		localmom.set_p(m,physical_PS_multiplicity+2*n-1);
		localmom.set_p(-m,physical_PS_multiplicity+2*n);

		localmomDs.set_p(m,physical_PS_multiplicity+2*n-1);
		localmomDs.set_p(-m,physical_PS_multiplicity+2*n);
#endif
	}
}

// to insert a momentum into nth location of the Builder_fixed_Ds's mom_conf's (checks that it does not overwrites phys and loop momneta)
template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::insert_extra_4D_mom(const momD<T,4>& m,size_t n){
	// bounds check
	if(n<physical_PS_multiplicity+2*max_n_loops||n>physical_PS_multiplicity+2*max_n_loops+extra_momenta_space)	_WARNING("insert_extra_4D_mom(mom,i="+std::to_string(n)+") in Builder_fixed_Ds, out of bounds -- Did nothing");
	else{
		// now copy
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		for(size_t ii=0;ii<localmom.size();ii++){
			localmom[ii].set_p(m,n);
			localmomDs[ii].set_p(m,n);
		}
#else
		localmom.set_p(m,n);
		localmomDs.set_p(m,n);
#endif
	}
}

template <class T, size_t D, size_t Ds> void Builder_fixed_Ds<T, D, Ds, typename std::enable_if_t<(Ds >= D)>>::update_builder_container() {
    bcontainer.mass.clear();
    bcontainer.mass = ParticleMass::get_vector_of_real_masses<T>();
}

template <class T> T I_factor_overall(const T& dummy,const cutTopology& cT){
	std::cout<<"An overall imaginary 'I' is missing in the tree/cut: "<<cT.get_short_name()<<" with Builder instantiated with real type!"<<std::endl;
	// we return 1
	return T(1);
}
template <class T> std::complex<T> I_factor_overall(const std::complex<T>& dummy,const cutTopology& cT){
	// we return I
	return std::complex<T>(0,1);
}

template <class T,size_t D,size_t Ds> void Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::update_normalizations(){
	// update tree normalization factors
	for(size_t ii=0;ii<tree_location.size();ii++){
		int ii_local((process_register.back().cut_tracker[tree_location[ii]].second)[0]);
//std::cout<<" ii:"<<ii_local<<" old TREE factor: "<<tree_overall_factors[ii_local]<<std::endl;
		tree_normalization_info[ii_local].set_normalization_factor(tree_overall_factors[ii_local]);
//std::cout<<" 	new TREE factor: "<<tree_overall_factors[ii_local]<<std::endl;
	}
	// update cut normalization factors
	for(int ll=0;ll<ncutsum;ll++){
//std::cout<<" ll:"<<ll<<" old CUT factor: "<<cut_overall_factors[ll+1]<<std::endl;
		// compute overall factors
            for (unsigned i = 0; i < dimensions.size(); i++ ){
                cut_normalization_info[ll+1][i].set_normalization_factor(cut_overall_factors[ll+1][i]);
            }
//std::cout<<"	new CUT factor: "<<cut_overall_factors[ll+1]<<std::endl;
	}
}

// computes overall normalization factor associated with forest->all_processes[i]
template <class T,size_t D,size_t Ds> std::vector<EntryNormalization> Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::get_overall_normalization_info(Forest* forest,int i){
	EntryNormalization toret;

	// reference to the corresponding cutTopology
	cutTopology& cT(std::get<0>(forest->all_processes[i]));
	// pointer to the corresponding top level Current
	Current* lTop(std::get<1>(forest->all_processes[i]));

        // reference to the model 
        auto& model = forest->get_model();

	int n_external_particles(0);
	// need to abstract this better later for multiple fields!
	int n_external_massless_vectors(0);
	int n_external_massive_vectors(0);
        unsigned n_external_fermions(0);
#if WEYL == 0
        unsigned n_external_dirac_fermions(0);
#endif
        int n_external_massless_tensors(0);
#if 0
        unsigned n_external_scalars(0);
#endif

	// (sqrt(2))^2
	int square_of_sqrt2(2);
	// track total powers of 1/sqrt(2) contained
	int powers_one_over_sqrt2(0);
	// track total I factors contained
	int powers_of_I(0);

        if(cT.is_multiplied_by_I()){
            static size_t report(0);
            if(report<3){
	        _WARNING("WARNING: cutTopology passed to Builder requests explicit addition of 'i' factor! Will report this only three times!");
                ++report;
            }
            DEBUG(
	        _WARNING("WARNING: a factor of 'i' was included on cutTopology!");
            );
            powers_of_I += 1;
        }

	// get info from external wave functions
        for (auto& particle : cT.get_particles_vertices()) {
            if (particle->external_state().get_name() !="default") {
                n_external_particles++;
                switch (particle->get_statistics()) {
                    case ParticleStatistics::vector: {
                        if (particle->is_massless() and particle->is_colored()) n_external_massless_vectors++;
                        if (particle->is_massive()) n_external_massive_vectors++;
                        break;
                    }
                    case ParticleStatistics::tensor: {
                        if (particle->is_massless()) n_external_massless_tensors++;
                        break;
                    }
                    case ParticleStatistics::fermion:
                    case ParticleStatistics::antifermion: {
                        if (particle->is_colored()) n_external_fermions++;
#if WEYL == 0
                        if (particle->is_colored() and is_dirac(particle->get_flavor())) n_external_dirac_fermions++;
#endif
			break;
                    }
                    case ParticleStatistics::scalar: {
#if 0
                        n_external_scalars++;
#endif
                        break;
                    }
                    default: _WARNING_R("Unkown external Particle!"); std::exit(1);
                }
            }
#if _DEBUG_NORMALIZATION
            else {
                std::cout << particle->get_name() << " has 'default' external state" << std::endl;
            }
#endif
        }

	// For a gravity calculation, an overall factor of 2 required (as from Cheung and Remmen 2*(2*K)^{n-2} missing factor)
        if (n_external_massless_tensors > 0 && model.get_name() == "CubicGravity") toret.add_real_normalization_entry(2, 1, 1);

        // each external massless vector contributes a 1/sqrt(2)
        powers_one_over_sqrt2 += n_external_massless_vectors;

        // each external massive vector contributes a 1/sqrt(2)
        // FIXME: There shouldn't be a 1/sqrt(2) for long. pol but without it there can be odd powers of sqrt(2)
        powers_one_over_sqrt2 += n_external_massive_vectors;

        // each external massless tensor contributes a 1/sqrt(2)^2
        powers_one_over_sqrt2 += 2*n_external_massless_tensors;

#if WEYL == 0
        // each external dirac fermion in Dirac representation contributes a 1/sqrt(2)
        powers_one_over_sqrt2 += n_external_dirac_fermions;
#endif

        int n_loop_particles(int(lTop->get_n_leaves()) + 1);
        n_loop_particles -= n_external_particles;
#if _DEBUG_NORMALIZATION
int n_vertices(cT.size());
std::cout << "found " << n_external_massless_vectors << " external (non 'default') massless vectors, " << n_external_massive_vectors
          << " external massive vectors, " << n_external_massless_tensors << " external massless tensors and \n"
          << n_external_fermions << " external fermions (out of " << n_external_particles << " external particles) in the process " << cT.get_short_name()
          << std::endl;
std::cout << "	and " << n_loop_particles << " loop particles with " << n_vertices << " vertices in the cutTopology" << std::endl;
#endif

	// get info from coupling constants

	std::vector<int> powers_per_coupling;
	// compute the coupling factors
	for(auto& coupling:forest->get_model_couplings()){
		powers_per_coupling.push_back(lTop->get_power(coupling));
	}

	for(size_t jj=0;jj<powers_per_coupling.size();jj++){
		if(powers_per_coupling[jj]!=0){
			const Coupling& lcoupling(forest->get_model_couplings()[jj]);

			// get powers of coupling
			toret.add_real_normalization_entry(lcoupling.get_real_info().first,lcoupling.get_real_info().second,powers_per_coupling[jj]);
#if _DEBUG_NORMALIZATION
std::cout<<"Coupling "<<lcoupling.get_name()<<" contributes with power "<<powers_per_coupling[jj]<<std::endl;
std::cout<<"	factor in toret: "<<lcoupling.get_value<T>()<<"^"<<powers_per_coupling[jj]<<std::endl;
#endif

			// now handle together 1/sqrt(2) factor from external wave functions and from the coupling
			if(lcoupling.get_square_num_normalization()==1&&lcoupling.get_square_den_normalization()==2){
				powers_one_over_sqrt2+=powers_per_coupling[jj];
			}
			else if(lcoupling.get_square_num_normalization()!=1||lcoupling.get_square_den_normalization()!=2){
				if(lcoupling.get_square_num_normalization()!=1&&lcoupling.get_square_den_normalization()!=1){
					std::cout<<"Probably wrong normalization with factor sqrt("<<lcoupling.get_square_num_normalization()<<
						"/"<<lcoupling.get_square_den_normalization()<<")^"<<powers_per_coupling[jj]<<std::endl;
				}
			}

			// get powers of I factored out in this coupling
			powers_of_I+=powers_per_coupling[jj]*lcoupling.get_powers_of_I();
	
		}
	}

	// Handle factors 1/sqrt(2)
	int powers_one_over_2(powers_one_over_sqrt2/2);
	int powers_one_over_2_reminder(powers_one_over_sqrt2%2);
	toret.add_real_normalization_entry(1,square_of_sqrt2,powers_one_over_2);
#if _DEBUG_NORMALIZATION
std::cout<<"From external vectors and the couplings we include a factor: sqrt(1/"<<square_of_sqrt2<<")^(2*"<<powers_one_over_2<<")"<<std::endl;
#endif
	if(powers_one_over_2_reminder!=0)
		std::cout<<"WARNING: total_power of 1/sqrt(2) not even!! There is a missing 1/sqrt(2) in the amplitude!!"<<std::endl;


	// Handle factors of 'I'
	int total_number_of_propagators(0);
	// assuming 3-pt vertices everywhere, the number of propagators in the tree-like structure is easy to compute:
	total_number_of_propagators+=n_external_particles+n_loop_particles-3;
	// and we must account for the 'I' from cut propagators implicitly done through state sums (one per loop)
	total_number_of_propagators+=(int)(n_loop_particles/2);

	int total_power_of_I(powers_of_I+total_number_of_propagators);
	// I^4=1
	int power_of_I(total_power_of_I%4);
#if _DEBUG_NORMALIZATION
std::cout<<"	finally (I)^"<<power_of_I<<" times 1=(I)^(4*"<<total_power_of_I/4<<")"<<std::endl;
#endif
	switch (power_of_I) {
		// I
		case 1: toret.add_imaginary_normalization_entry(1,1,1); break;
		// -1
		case 2: toret.add_real_normalization_entry(-1,1,1); break;
		// -I = (-1) * (I)
		case 3: toret.add_real_normalization_entry(-1,1,1); toret.add_imaginary_normalization_entry(1,1,1); break;
		// 1
		default: break;
	}

        toret.add_real_normalization_entry(cT.get_n_copies(),1,1);

	// check if some overall I's have been dropped (as it is the case when instantiating *this with real T)
//std::cout<<"Final overall factor: "<<toret<<std::endl;

        std::vector<EntryNormalization> ret(dimensions.size(),toret);

        for (size_t i = 0; i < dimensions.size(); i++) {
            // For each Nf loop we normalize with by Tr(I) in Dtt and multiply by Nf.
            // We do not verify the input here and assume that if we find at least one quark with nf flavor we have a closed loop
            if (dimensions[i] >= 6) {
                unsigned nfcount = cT.get_n_closed_light_fermion_loops(); // Weyl fermions
                unsigned nhcount = cT.get_n_closed_heavy_fermion_loops(); // Dirac fermions

                if (nfcount > 0) {
                    // Here we need to get Tr(I) = 4 correctly.
                    int p = (dimensions[i] / 2 - 2);
                    // If we use Weyl spinors, we need to multiply by 2 for each closed loop because we compute only
                    // half of the full trace in this case by inserting only one pair of states in the state sum.
                    p -= 1;
                    if (p != 0) { ret[i].add_real_normalization_entry(1, 2, nfcount * p); }
                    ret[i].add_real_normalization_entry(settings::general::Nf, 1, nfcount);
                }

                if (nhcount > 0) {
                    // Here we need to get Tr(I) = 4 correctly.
                    unsigned Dtt = clifford_algebra::two_to_the(dimensions[i] / 2 - 2); // Dtt = (Dt/4) = (2^(D/2))/4 = 2^(D/2-2)
                    ret[i].add_real_normalization_entry(1, Dtt, nhcount);
                    ret[i].add_real_normalization_entry(settings::general::Nh, 1, nhcount);
                }

                // if partial trace is done, for each external pair we divide by Dtt
                // tree amplitudes are sometimes computed in Ds>4 and we don't do the trace in that case
                if (n_loop_particles>0 && dimensions[i] >= 6) {
                    assert(n_external_fermions % 2 == 0);
                    // this is what we get if we do full partial trace
                    unsigned norm_ds = clifford_algebra::two_to_the((dimensions[i]/2-2) * n_external_fermions/2 );
                    // we do it this way such that when not-full-tracing scheme is in use, there's still a correct normalization
                    if (n_external_fermions != 0) {
                        if(settings::BG::partial_trace_scheme == settings::BG::trace_scheme::single_trace){
                            // normalize by 1/(Dt/4) with Dt = 2^(D/2)
                            ret[i].add_real_normalization_entry(1, clifford_algebra::two_to_the(dimensions[i]/2-2) ,1);
                        } else {
                            switch (settings::BG::partial_trace_normalization) {
                                case settings::BG::trI::full_Ds: ret[i].add_real_normalization_entry(1, norm_ds, 1); break;
                                case settings::BG::trI::two: ret[i].add_real_normalization_entry(1, 2, n_external_fermions / 2); break;
                                case settings::BG::trI::automatic: {
                                    ret[i].add_real_normalization_entry(1, std::min(norm_ds, cT.get_n_external_embeddings()), 1);
                                    break;
                                }
                                case settings::BG::trI::none:
                                default: break;
                            }
                        }
                    }
                }
            }
        }

	return ret;
}


}
