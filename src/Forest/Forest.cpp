/**
 * @file Forest.cpp
 *
 * @date 24.11.2015
 *
 * @brief Implementation of class Forest
 *
 * Class Forest is implemented, for solving generic recursions in a off-shell set up for computing trees and multi-loop cuts
 *
*/

#include <iostream>
#include <algorithm>

#include "Forest/Forest.h"
#include "Core/Debug.h"
#include "Forest/Model.h"
#include "Forest/CurrentRules/current_helper.h"
#include "Core/Utilities.h"

#define _DEBUG_FOREST 0

namespace std{

template <> struct hash<Caravel::InternalIndexVertex> {
    size_t operator()(const Caravel::InternalIndexVertex& c) const {
        size_t result = static_cast<size_t>(c.needs_color_handling);
        Caravel::hash_combine(result, c.index_left);
        Caravel::hash_combine(result, c.index_right);
        Caravel::hash_combine(result, c.all_colored_internal);
        Caravel::hash_combine(result, c.all_colorless_internal);
        return result;
    }
};

template <> struct hash<Caravel::Current> {
    size_t operator()(const Caravel::Current& c) const {
        using Caravel::Current;
        size_t result =c.get_n_leaves();
        Caravel::hash_combine(result,c.get_statistics());
        {
            const auto& type = c.get_current_type();
            Caravel::hash_combine(result,type.get_flavor());
            Caravel::hash_combine(result,type.get_mass_index());
            Caravel::hash_combine(result,type.is_colored());
        }
        {
            const auto& ind = c.get_current_indices();
            Caravel::hash_combine(result,ind.propagator_dropped());
            Caravel::hash_combine(result,ind.internal_indices_closed);
            Caravel::hash_combine(result,ind.internal_indices_open);
        }
        Caravel::hash_combine(result,c.qqs_powers);
        Caravel::hash_combine(result,c.closed_qqs_powers);
        {
            const auto& leaves = c.get_leaves();
            Caravel::hash_combine(result,leaves.propagator_dropped());
            for(auto p : leaves.open_leaves){
                Caravel::hash_combine(result,reinterpret_cast<uintptr_t>(p));
            }
            for(auto& b : leaves.closed_leaves){
                for(auto p : b){
                    Caravel::hash_combine(result,reinterpret_cast<uintptr_t>(p));
                }
            }
        }
        Caravel::hash_combine(result,c.get_coupling_string());
        return result;
    }
};

size_t hash<Caravel::Current*>::operator()(Caravel::Current* c) const {
    return hash<Caravel::Current>{}(*c);
}

}


namespace Caravel {

bool InternalIndexVertex::is_trivial() {
    return !needs_color_handling && index_left == 0 && index_right == 0 && all_colored_internal.empty() && all_colorless_internal.empty();
}

// Implementations of the Forest base class
const std::vector<std::vector<Current*>> Forest::get_all_currents(size_t Ds) const {
    // Default case
    if (Ds == 0) {
        std::vector<std::vector<Current*>> r;
        for (auto& v : all_currents_set) { r.emplace_back(v.begin(), v.end()); }
        r.shrink_to_fit();
        return r;
    }
    else
        return states_manager.get_all_currents(Ds);
}

const std::vector<std::vector<Current*>>& Forest::get_all_tree_currents() const {
	return all_tree_currents;
}

Forest::Forest(const Model& md,bool bin) : Forest(md.clone(),bin) {}

Forest::Forest(Model* md,bool bin) : model(md->clone()),  states_manager(this), forest_closed(false), minimal_states_in_currents(bin),
		location_new_forest_entry(0), global_handle_request(true), global_switch_to_planar(false), global_is_factorizable(false),
		global_vertex_locate_l1(0), global_vertex_locate_l2(0), global_index_in_vertex_locate_l1(0), global_index_in_vertex_locate_l2(0), global_start_current(nullptr),
		global_close_current(nullptr) {
	DEBUG_MESSAGE("Start building of Forest");
}

size_t Forest::get_total_number_of_tree_currents() const {
	return states_manager.get_total_number_of_tree_currents();
}

size_t Forest::get_total_number_of_treelike_currents(size_t Ds) const {
	return states_manager.get_total_number_of_treelike_currents(Ds);
}

size_t Forest::get_treelike_currents_per_level(size_t Ds,size_t level) const {
	return states_manager.get_treelike_currents_per_level(Ds,level);
}

size_t Forest::get_count_of_states_for_cut(size_t Dim,size_t cut_i) const {
	if(forest_closed){
		return states_manager.get_count_of_states_for_cut(Dim,cut_i);
	}
	else
		return 0;
}

size_t Forest::get_count_of_all_level_states_for_cut(size_t Dim,size_t cut_i) const {
	if(forest_closed){
		return states_manager.get_count_of_all_level_states_for_cut(Dim,cut_i);
	}
	else
		return 0;
}

size_t Forest::get_total_number_of_cut_currents(size_t Dim) const {
	if(forest_closed){
		return states_manager.get_total_number_of_cut_currents(Dim);
	}
	else
		return 0;
}

void Forest::define_internal_states_and_close_forest(const std::vector<size_t>& vDim){

	// No more processes/cuts can now be added to the forest
	forest_closed=true;

	// forward request to states_manager
	states_manager.define_internal_states(vDim);

	// now add information on states for each current containing a default state
	for(size_t ii=0;ii<default_state_particles.get_all_defaulted_currents().size();ii++){
		// level
		for(size_t jj=0;jj<default_state_particles.get_all_defaulted_currents()[ii].size();jj++){
			// add information to the current on its states
			states_manager.add_states_to_current(default_state_particles.get_all_defaulted_currents()[ii][jj]);
		}
	}

	// Now we can make all merging calls
	for(size_t ii=0;ii<join_cuts_calls.size();ii++)
		this->join_cuts(join_cuts_calls[ii]);

	// now update current counting of Forest
	states_manager.update_counting_all_dims_and_currents();

        //print_all_currents(*this);
        //explore_current_inheritance(*this);
}

std::vector<size_t> Forest::get_added_dimensions() const {
	return states_manager.get_added_dimensions();
}

// this function can be used to merge currents between cuts added by add_cut()
//	for example, it can help to no over compute cuts that overlap considerably, like for example the boxes <nl1m|2p|3m|4p| & <nl1m|2p|3m|4m|
void Forest::join_cuts(std::vector<int> merge){
	if(!forest_closed){
		join_cuts_calls.push_back(merge);
		return;
	}
	// check that all cuts pointed at are actually in the forest
	std::vector<int> lmerge=merge;
	// sort
	std::sort(lmerge.begin(),lmerge.end());
	if(lmerge.back()>int(states_manager.get_size_of_cut_register())){
		std::cout<<"Maximum # of cuts is "<<states_manager.get_size_of_cut_register()<<" but join_cuts received: "<<merge<<" --- did nothing "<<std::endl;
		return;
	}

        DEBUG_MESSAGE("Merging the following cuts: ",lmerge);
	// Will merge everything into the first (sorted) cut vector of currents
	std::vector<std::vector<Current*>>& merge_into_cut_first(std::get<3>(all_processes[this->get_current_index_for_cut(lmerge[0])-1]));

	for(size_t kk=1;kk<lmerge.size();kk++){
		std::vector<std::vector<Current*>>& merger(std::get<3>(all_processes[this->get_current_index_for_cut(lmerge[kk])-1]));
		for(size_t ii=0;ii<merger.size();ii++){
			for(size_t jj=0;jj<merger[ii].size();jj++){
				Current* lcurrent(merger[ii][jj]);
				if(std::find(merge_into_cut_first[ii].begin(),merge_into_cut_first[ii].end(),lcurrent)==merge_into_cut_first[ii].end()){
					merge_into_cut_first[ii].push_back(lcurrent);
					DEBUG_MESSAGE("The current: "+lcurrent->get_name()+" was merged into cut_",lmerge[0]);
				}
			}
		}
		// empty out merger
		for(size_t ii=0;ii<merger.size();ii++){
			merger[ii].clear();
		}
	}
}

size_t Forest::number_of_momDconf_copies() const {
	return states_manager.number_of_momDconf_copies();
}

size_t Forest::corresponding_momDconf_for_cut(int i) const {
	return states_manager.corresponding_momDconf_for_cut(i);
}


std::vector<size_t> Forest::get_loop_count_present(){
	return loop_count_present;
}

const Model& Forest::get_model() const {
    return *model;
}

Forest::~Forest() {
	// Destroy owned Model
	delete model;
	// Destroy all currents and vectors
        for (auto& vi : all_currents_set) {
            for (auto ci : vi) delete ci;
        }
        // Destroy all particles in generated_loop_particles
	for(size_t i=0;i<generated_loop_particles.size();i++){
		delete std::get<0>(generated_loop_particles[i]);
		delete std::get<1>(generated_loop_particles[i]);
	}
}


namespace{

bool current_overlap(Current *, Current *);

/**
 * This function produces currents for external particles
 * and if related ones appear in all_currents, then returns a pointer to corresponding one.
 * Otherwise creates new external current and append to all_currents
*/
Current* get_external_current(Particle* p, std::vector<Forest::CurrentsSetType>& allcurrents, std::vector<std::vector<Current*>>& all_tree_currents) {
    assert(!allcurrents.empty());
    for (auto ci : allcurrents[0]) {
        if (ci->is_associated_to_particle(p)) {
            DEBUG_MESSAGE("Avoided extra current for particle: ", *p, " using the 1-current # ");
            return ci;
        }
    }
    // Construct new external current
    DEBUG_MESSAGE("Adding a new 1-current for particle: ", *p, " of type: ", p->get_type());
    Current* ExtCurrent;
    ExtCurrent = new CurrentExternal(p);
    allcurrents[0].insert(ExtCurrent);
    if (p->external_state().get_name() != "default") {
        if (all_tree_currents.size() == 0) all_tree_currents.push_back(std::vector<Current*>());
        all_tree_currents[0].push_back(ExtCurrent);
    }
    return ExtCurrent;
}

// This function to checks if two currents are (color) adjacent
bool is_adjacent(Current * cL, Current * cR){
//std::cout<<"Testing adjacency with: "<<cL->get_name()<<" and: "<<cR->get_name()<<std::endl;
	if((!cL->is_colored())||(!cR->is_colored())){
		// both currents must be colored!
		return false;
	}
	if(cL->right_color_adjacent()!=cR->internal_left_color()){
		return false;
	}
	if(cL->internal_right_color()!=cR->left_color_adjacent()){
		return false;
	}
	return true;
}

// This function to checks if two currents (colored) or not shared some leaves
bool current_overlap(Current * cL, Current * cU){
	return (cL->get_leaves()).leaves_overlap(cU->get_leaves());
}

bool align_currents_and_rule(CurrentRule* rule,const std::vector<Current *>& currents,const std::vector<ParticleType>& typelist,std::vector<size_t>& match_indices,size_t& open_index_in_rule){
	bool toreturn(false);
	size_t multiplicity(typelist.size());
	size_t * sample;
	sample=new size_t[multiplicity-1];
	for(size_t ii=0;ii<multiplicity-1;ii++)		sample[ii]=ii;
	for(size_t i=0;i<multiplicity;i++){
	  open_index_in_rule=(i+multiplicity-1)%multiplicity;
	  bool check(true);
	  if(rule->is_color_ordered()){
		for(size_t j=0;j<(multiplicity-1);j++){
			if(!almost_equal(typelist[(i+j)%multiplicity],currents[sample[j]]->get_current_anti_type())){
				check=false;
				break;
			};
		};
	  }
	  else{
		// Non color ordered case. I sample over all possible orders
		std::sort (sample,sample+multiplicity-1);
//std::cout<<"Non color\n";
		do {
//std::cout<<"permutation\n";
			check=true;
			for(size_t j=0;j<(multiplicity-1);j++){
//std::cout<<typelist[(i+j)%multiplicity]<<" "<<currents[sample[j]]->get_current_anti_type()<<std::endl;
				if(!almost_equal(typelist[(i+j)%multiplicity],currents[sample[j]]->get_current_anti_type())){
					check=false;
//std::cout<<"false\n";
					break;
				};
			};
//std::cout<<"check: "<<check<<std::endl;
			if(check) break;
		} while ( std::next_permutation(sample,sample+multiplicity-1) );
	  };
	  if(check){
//std::cout<<"success\n";
		toreturn=true;
		for(size_t ii=0;ii<multiplicity-1;ii++)		match_indices.push_back((open_index_in_rule+1+sample[ii])%multiplicity);
		// add as last term, open_index_in_rule
		match_indices.push_back(open_index_in_rule);
		break;
	  };
	};
	delete [] sample;
	return toreturn;
}

}

// Builds nested loops to construct currents associated to a given partition
void recursive_current_construction(const size_t& partition_size,size_t filling_level,std::vector<size_t>& list_of_indices,const size_t& multiplicity,
		CurrentRule * rule,const std::vector<ParticleType>& lctypelist,const std::vector<Particle>& lparticlelist,
		const std::vector<size_t>& partition,std::vector<Current *>& hold_sub_currents,const std::vector<std::vector<Current*>>& all_currents,
					const std::vector<Coupling>& needed_couplings,const cutTopology::couplings_t& coupling_powers,bool needs_internal_index_handling,
					const InternalIndexVertex& global_internal_index_vertex){
	if(filling_level==partition_size){
	  // list_of_indices has been filled, now we can check if a current will be constructed

	  Current ** pointer_current_container;
	  pointer_current_container=new Current * [partition_size];
	  for(size_t ii=0;ii<partition_size;ii++){
	  	pointer_current_container[ii]=all_currents[partition[ii]-1][list_of_indices[ii]];
	  };

          // First check if order in couplings allow construction
	  bool couplingspowers(true);
	  for(size_t ll=0;ll<needed_couplings.size();ll++){
		const Coupling& lc(needed_couplings[ll]);
		int lpower(0);
		// Add contribution of rule
		lpower+=rule->get_power(lc);
		// Add contributions from each subcurrents
		for(size_t tt=0;tt<partition_size;tt++){
			lpower+=pointer_current_container[tt]->get_power(lc);
		};

                int compare_power = 0;
                auto found = coupling_powers.find(lc.get_name());
                if(found!=coupling_powers.end()) compare_power = found->second;
		if(lpower>compare_power) couplingspowers=false;
	  };
	  if(couplingspowers){
#if 0
std::cout<<"Now on level: "<<filling_level<<" out of: "<<partition_size<<std::endl;
std::cout<<"All indices: ";
for(size_t ii=0;ii<partition_size;ii++)	std::cout<<list_of_indices[ii]<<" ";
std::cout<<std::endl;
std::cout<<"With rule: "<<rule->get_name()<<", considering currents: ";
for(size_t ii=0;ii<partition_size;ii++){
	std::cout<<pointer_current_container[ii]->get_long_name()<<" "<<std::endl;
}
std::cout<<std::endl;
#endif
		// Let me now sort over all the orders of the objects in pointer_current_container
		std::sort (pointer_current_container,pointer_current_container+partition_size);
		do {
			// Check for color ordered currents
			bool acceptcurrents(true);
			std::vector<Current*> colored_pointer_current_container;
			for(size_t ii=0;ii<partition_size;ii++){
				if(pointer_current_container[ii]->is_colored())
					colored_pointer_current_container.push_back(pointer_current_container[ii]);
			}
			for(size_t ii=0;ii+1<colored_pointer_current_container.size();ii++){
				// two contiguous colored currents must be adjacent
				if(!is_adjacent(colored_pointer_current_container[ii],colored_pointer_current_container[ii+1])){
					acceptcurrents=false;
					break;
				}
			}
			// Check that Currents do not overlap
			for(size_t ii=0;ii<(partition_size-1);ii++){
			  for(size_t jj=ii+1;jj<partition_size;jj++){
				Current * Lower(pointer_current_container[ii]);
				Current * Upper(pointer_current_container[jj]);
				if(current_overlap(Lower,Upper)){
					acceptcurrents=false;
					break;
				}
			  }
			}

#if 0
std::cout<<"End of recursion!! acceptcurrents: "<<(acceptcurrents ? "true" : "false")<<std::endl;
std::cout<<"	c1= "<<pointer_current_container[0]->get_long_name()<<" internal index: "<<pointer_current_container[0]->get_internal_index()<<std::endl;
std::cout<<"	c2= "<<pointer_current_container[1]->get_long_name()<<" internal index: "<<pointer_current_container[1]->get_internal_index()<<std::endl;
#endif

			if(acceptcurrents){
	  			// Vector of currents for building CurrentBasic
	  			std::vector<Current *> lvc;
				for(size_t ii=0;ii<partition_size;ii++)		lvc.push_back(pointer_current_container[ii]);
				std::vector<size_t> lshift;
				size_t open_index_in_rule;
				bool checkshift=align_currents_and_rule(rule,lvc,lctypelist,lshift,open_index_in_rule);
				// Check if normal alignment (no mirror)
                                if (checkshift) {
                                    // how many different index flows can the rule have
                                    DEBUG_PRINT(rule->get_name());
                                    // for(auto it : lvc) _MESSAGE("\t\t",it->get_internal_index());
                                    for (int niii = 1; niii <= rule->n_of_modes(); ++niii) {
                                        DEBUG_MESSAGE("\tmode: ", niii);
                                        Particle p(lparticlelist[open_index_in_rule]);
                                        if (!rule->propagate_indices(p, lvc, {static_cast<size_t>(niii)})) continue;

                                        Current* l_i_sub_current;
                                        l_i_sub_current = new CurrentBasic(p, lvc, rule, lshift, needed_couplings);

                                        // in case we need to track internal_index
                                        if (needs_internal_index_handling) {
                                            l_i_sub_current->set_internal_index(p.get_internal_index());
                                            l_i_sub_current->set_internal_indices_open(global_internal_index_vertex);
                                        }

                                        rule->update_qqs_couplings(dynamic_cast<CurrentBasic*>(l_i_sub_current), niii);

                                        // l_i_sub_current pushed_back into the Forest
                                        // Checking of existence and adding of parents to daughter currents shall be performed outside of recursion
                                        hold_sub_currents.push_back(l_i_sub_current);
                                        DEBUG_MESSAGE("\taccepted: ", l_i_sub_current->get_name(), "\tSR: ", l_i_sub_current->qqs_powers);
                                    }
                                }
                                // Either for checkshift TRUE or FALSE, we break the do loop
				break;
			}
		} while ( std::next_permutation(pointer_current_container,pointer_current_container+partition_size) );
	  }
	  delete [] pointer_current_container;
	}
	else{
		// should now loop of corresponding indices for filling_level-th entry

		// In case I only want currents leading to the amplitude while contracting with the wave function of the last particle in the process
		size_t sampleupto;

		sampleupto=all_currents[partition[filling_level]-1].size();

		for(size_t ii=0;ii<sampleupto;ii++){
		  // Define corresponding index in list_of_indices
		  list_of_indices[filling_level]=ii;
		  // We cover in full always first level
		  if(filling_level==0){
/*
std::cout<<"Now on level: "<<filling_level<<" out of: "<<partition_size<<std::endl;
std::cout<<"Entry list_of_indices["<<filling_level<<"] = "<<list_of_indices[filling_level]<<std::endl;
std::cout<<"ii max: "<<all_currents[partition[filling_level]-1].size()<<std::endl;
*/
			// built next recursion level (notice ++filling_level)
			size_t next_filling_level=filling_level+1;
			recursive_current_construction(partition_size,next_filling_level,list_of_indices,multiplicity,rule,lctypelist,lparticlelist,partition,hold_sub_currents,all_currents,
					needed_couplings,coupling_powers,needs_internal_index_handling,global_internal_index_vertex);
		  }
		  // If we are sampling same level, avoid double counting
		  else if(partition[filling_level]!=partition[filling_level-1] || ii>list_of_indices[filling_level-1]){
			// built next recursion level (notice ++filling_level)
			size_t next_filling_level=filling_level+1;
			recursive_current_construction(partition_size,next_filling_level,list_of_indices,multiplicity,rule,lctypelist,lparticlelist,partition,hold_sub_currents,all_currents,
					needed_couplings,coupling_powers,needs_internal_index_handling,global_internal_index_vertex);
		  }
		}
	}
}

namespace {

// Function to construct needed currents
void push_needed_currents(CurrentRule * rule,const std::vector<size_t>& partition,std::vector<Current *>& hold_sub_currents,const std::vector<std::vector<Current*>>& all_currents,
					const std::vector<Coupling>& needed_couplings,const cutTopology::couplings_t& coupling_powers,bool needs_internal_index_handling,
					const InternalIndexVertex& global_internal_index_vertex){
	const size_t lmultiplicity(rule->get_rule_multiplicity());
	const std::vector<ParticleType>& lctypelist(rule->get_current_type_list());
	const std::vector<Particle>& lparticlelist(rule->get_particle_content());

	// Prepare list to be built by recursive loop
        // partition size is rule multiplicity -1 by construction
	const size_t partition_size(partition.size());
	size_t filling_level(0);
	std::vector<size_t> list_of_indices(partition_size);
	// Recursive construction of currents: (some redundant information passed, but this to make it more efficient)
	recursive_current_construction(partition_size,filling_level,list_of_indices,lmultiplicity,rule,lctypelist,lparticlelist,partition,hold_sub_currents,all_currents,
					needed_couplings,coupling_powers,needs_internal_index_handling,global_internal_index_vertex);
}

bool internal_indices_match(Current * c1,Current * c2){
    return CurrentIndices::internal_indices_match(c1->get_current_indices(),c2->get_current_indices());
}

// Check if two currents or sub currents match: # leaves && couplings && type && spin && same leaves
bool check_current_match(Current * c1,Current * c2,const std::vector<Coupling>& couplings){
        DEBUG_MESSAGE("Checking if currents match:\n\t",c1->get_name(),"\n\t",c2->get_name());
	// should check: 1) # leaves ; 2) same couplings ; 3) same type ; 4) spin ; 5) matching indices ; 6) same leaves
	if(c1->get_n_leaves()!=c2->get_n_leaves()){
                DEBUG_MESSAGE("\tNO MATCH: ","number of leaves");
		return false;
	}
	else{
		// should check: 2) same couplings
		for(size_t kk=0;kk<couplings.size();kk++){
			if(c1->get_power(couplings[kk])!=c2->get_power(couplings[kk])){
                            DEBUG_MESSAGE("\tNO MATCH: ","powers of ",couplings[kk]," (",c1->get_power(couplings[kk])," != ",c2->get_power(couplings[kk]),")");
                            return false;
                        }
		};
		// should check: 3) same type
		if(c1->get_current_type()!=c2->get_current_type()){
                    DEBUG_MESSAGE("\tNO MATCH: ","current types");
                    return false;
                }
		// should check: 4) same spin
		if(c1->get_statistics() !=c2->get_statistics()){
                    DEBUG_MESSAGE("\tNO MATCH: ","statistics");
                    return false;
                }
                // should check matching internal indices: notice: no operator== used!
                if(!internal_indices_match(c1,c2)){
                    DEBUG_MESSAGE("\tNO MATCH: ","internal indices");
                    return false;
                }

                if(c1->qqs_powers != c2->qqs_powers){
                    DEBUG_MESSAGE("\tNO MATCH: ","selection rules OPEN");
                    return false;
                }
                if(c1->closed_qqs_powers != c2->closed_qqs_powers){
                    DEBUG_MESSAGE("\tNO MATCH: ","selection rules CLOSED");
                    return false;
                }

		// should check: 6) same leaves
                if(! (c1->get_leaves() == c2->get_leaves()) ) {
                    DEBUG_MESSAGE("\tNO MATCH: ","leaves");
                    return false;
                }


                DEBUG_MESSAGE("\tcurrents matched");

                return true;
	}
}

}

int Forest::add_process(Process proc, const cutTopology::couplings_t& cpowers, const SelectionRule& sr) {
	cutTopology simple_tree_topology(proc,cpowers,sr);
	return this->add_cutTopology(std::vector<std::tuple<int,int,int,int>>(),simple_tree_topology,std::vector<int>());
}

  int Forest::add_process(Process proc) {
        // TODO
        // Coupling information should be read off from process library probably in the future
        // For now we fill in only the QCD coupling assuming that all trees have only QCD and/or non-loop EW interactions
        int n_coloured = std::count_if(proc.get_process().begin(), proc.get_process().end(), [](auto& p) { return p -> is_colored(); });
        int n_gs = (n_coloured >= 2) ? (n_coloured - 2) : 0;
        
        //! FIXME this is a very hacky way to select gravity couplings
        if (n_coloured > 0) {
            return add_process(proc, {
                                           {"gs", n_gs},
                                           {"gw", static_cast<int>(proc.get_process().size()) - n_coloured},
                                     });
        }
        else {
            return add_process(proc, {
                                        {"K", static_cast<int>(proc.get_process().size()) - 2},
                                       });
        }
  }


// FIXME: left here for backwards compatability, just assume QCD coupling in the first entry and that's it
int Forest::add_process(Process proc,std::vector<int> cpowers) {
	cutTopology simple_tree_topology(proc,{{"gs",cpowers.at(0)}});
	return this->add_cutTopology(std::vector<std::tuple<int,int,int,int>>(),simple_tree_topology,std::vector<int>());
}

void Forest::store_variables(std::vector<std::tuple<int,int,int,int>> loop_parts_in,cutTopology& topology,std::vector<int> loop_gen_location,bool switch_to_planar_standard){
	DEBUG_MESSAGE("Offshell build up step: (I) initialization");

	// check how many loops has this current and if necessary, add this to loop_count_present
	size_t n_loops_in_current(loop_parts_in.size());
	if(std::find(loop_count_present.begin(),loop_count_present.end(),n_loops_in_current)==loop_count_present.end()){
		loop_count_present.push_back(n_loops_in_current);
		// keep monotonic order
		std::sort(loop_count_present.begin(),loop_count_present.end());
	}

	// reset left current
	global_start_current=nullptr;

	global_handle_request=true;
	if(loop_gen_location.size()!=loop_parts_in.size()){
		std::cout<<"ERROR!! Forest::store_variables(.) should receive loop_gen_location and loop_parts_in of same size!! Did nothing!!"<<std::endl;
		global_handle_request=false;
		return;
	}

        if (all_currents_set.empty()) {
            all_currents_set.emplace_back(100,std::hash<Current*>{},detail::CurrentEquality{model});
        }

	global_switch_to_planar=switch_to_planar_standard;

	// assign loop level (tree/1-loop/2-loop)
	global_loop_level=loop_gen_location.size();
	global_location_generated_loop_momentum=loop_gen_location;

	// prepare global_level_1_currents_default to track currents that depend on default loop momenta
	global_currents_with_loop_particles.clear();
	global_level_1_currents_default.clear();

        if(global_loop_level==1){
		// retrieve left loop particle
		Particle * local_loop_particle(std::get<1>(generated_loop_particles[global_location_generated_loop_momentum[0]]));
		// and store level-1 current
		global_level_1_currents_default.push_back(get_external_current(local_loop_particle,all_currents_set,all_tree_currents));
		// add info to track default states
		default_state_particles.add_entry(global_level_1_currents_default.back());

		if(global_currents_with_loop_particles.size()==0){
			global_currents_with_loop_particles.push_back(std::vector<Current*>());
		}
		// and also to container of all loop-particle currents
		global_currents_with_loop_particles[0].push_back(global_level_1_currents_default.back());

		// and l1
		local_loop_particle=std::get<0>(generated_loop_particles[global_location_generated_loop_momentum[0]]);
		global_level_1_currents_default.push_back(get_external_current(local_loop_particle,all_currents_set,all_tree_currents));
		default_state_particles.add_entry(global_level_1_currents_default.back());
		global_currents_with_loop_particles[0].push_back(global_level_1_currents_default.back());
	}
	else if(global_loop_level==2){
		// retrieve loop particle nl1
		Particle * local_loop_particle(std::get<1>(generated_loop_particles[global_location_generated_loop_momentum[0]]));
		// and store level-1 current
		global_level_1_currents_default.push_back(get_external_current(local_loop_particle,all_currents_set,all_tree_currents));
		// add info to track default states
		default_state_particles.add_entry(global_level_1_currents_default.back());

		if(global_currents_with_loop_particles.size()==0){
			global_currents_with_loop_particles.push_back(std::vector<Current*>());
		}
		// and also to container of all loop-particle currents
		global_currents_with_loop_particles[0].push_back(global_level_1_currents_default.back());


		// retrieve loop particle l1
		local_loop_particle=std::get<0>(generated_loop_particles[global_location_generated_loop_momentum[0]]);
		// and store level-1 current
		global_level_1_currents_default.push_back(get_external_current(local_loop_particle,all_currents_set,all_tree_currents));
		// add info to track default states
		default_state_particles.add_entry(global_level_1_currents_default.back());
		global_currents_with_loop_particles[0].push_back(global_level_1_currents_default.back());

		// retrieve loop particle l2
		local_loop_particle=std::get<0>(generated_loop_particles[global_location_generated_loop_momentum[1]]);
		// and store level-1 current
		global_level_1_currents_default.push_back(get_external_current(local_loop_particle,all_currents_set,all_tree_currents));
		// add info to track default states
		default_state_particles.add_entry(global_level_1_currents_default.back());
		global_currents_with_loop_particles[0].push_back(global_level_1_currents_default.back());

		// and l1
		local_loop_particle=std::get<1>(generated_loop_particles[global_location_generated_loop_momentum[1]]);
		global_level_1_currents_default.push_back(get_external_current(local_loop_particle,all_currents_set,all_tree_currents));
		default_state_particles.add_entry(global_level_1_currents_default.back());
		global_currents_with_loop_particles[0].push_back(global_level_1_currents_default.back());
	}

	// in case topology corresponds to a cut, store replaced cutTopology
	if(loop_gen_location.size()<1)
		global_topology=topology;
	else{
		std::vector<std::pair<int,Particle*>> lPs;
		for(size_t ii=0;ii<loop_gen_location.size();ii++){
			// add loop particle
			lPs.push_back(std::make_pair(std::get<0>(loop_parts_in[ii]),std::get<0>(generated_loop_particles[loop_gen_location[ii]])));
			// add loop antiparticle
			lPs.push_back(std::make_pair(std::get<1>(loop_parts_in[ii]),std::get<1>(generated_loop_particles[loop_gen_location[ii]])));
		}
		global_topology=topology.particle_substitute(lPs);
		if(loop_gen_location.size()==2){
			std::tuple<int,int,Particle*> tproc(topology.get_location_in_vertex(lPs[0].first));
			global_vertex_locate_l1=size_t(std::get<0>(tproc));
			global_index_in_vertex_locate_l1=size_t(std::get<1>(tproc));

			tproc=topology.get_location_in_vertex(lPs[2].first);
			global_vertex_locate_l2=size_t(std::get<0>(tproc));
			global_index_in_vertex_locate_l2=size_t(std::get<1>(tproc));
			DEBUG_MESSAGE("l1 is located in vertex ",global_vertex_locate_l1," and l2 in vertex ",global_vertex_locate_l2);
			DEBUG_MESSAGE("	and within corresponding vertices in: ",global_index_in_vertex_locate_l1," and ",global_index_in_vertex_locate_l2);
		}
	}
#if _DEBUG_FOREST
std::cout<<"global_topology set to: "<<global_topology.get_short_name()<<std::endl;
#endif

	// if planar requested, set information for global_is_factorizable and global_vertex_locate_l1 and global_vertex_locate_l2
	global_is_factorizable=false;
	if(loop_parts_in.size()==2){
		if(global_vertex_locate_l1==global_vertex_locate_l2){
			DEBUG_MESSAGE("We have a two-loop factorized cut!");
			global_is_factorizable=true;
		}
	}

}

thread_local CurrentPartitions Forest::partitions = CurrentPartitions();

void dispose_current(Current* c){
	// Before erasing, take care of removing related pointers from parents
	std::vector<Current*> * localpv(c->get_parents());
	for(size_t k=0;k<localpv->size();k++){
		Current * localparent((*localpv)[k]);
		// now remove daughter (that will be deleted)
		localparent->get_daughters()->erase(std::find(localparent->get_daughters()->begin(),localparent->get_daughters()->end(),c));
	}

	delete c;
}

void dispose_currents(std::vector<Current*>& vcs){
	// go from back (higher level currents) to front of vector
	for(size_t ii=vcs.size();ii>0;ii--){
		dispose_current(vcs[ii-1]);
	}
	vcs.clear();
}

void Forest::offshell_currents_from_vertex(size_t i,bool is_last_vertex){
#if _DEBUG_FOREST
std::cout<<"Forest::offshell_currents_from_vertex step: "<<i<<" with ";
if(global_start_current==nullptr) std::cout<<"null";
else	std::cout<<global_start_current->get_name()<<" "<<global_start_current->get_coupling_string()<<" of type: "<<global_start_current->get_current_particle()<<std::endl;
std::cout<<" current as main input"<<std::endl;
#endif
	// prepare the local level 1 currents plus the corresponding one from global_start_current, considering global_switch_to_planar in the definition of color ordering (as well as global_is_factorizable)

	// get colored and colorless particles
	std::vector<Particle*> colored(global_topology.get_colored(i));
	std::vector<Particle*> colorless(global_topology.get_colorless(i));
	std::vector<Particle*> local_all_particles;
	ParticleType contract_type_particle_on_the_right(global_topology.get_vertex(i).get_right().get_anti_type());

	// assign global indices for internal_index tracking
	bool needs_internal_index_handling(global_topology.get_vertex(i).needs_internal_index_handling());

        // The Tadpole is special since it consists out of a single vertex but is not a tree
        // We track the tadpole in order to steer the program flow a little to its benefit
        bool is_tadpole = false;

        // if the on-shell filtering has been active in the current vertex
        bool is_onshell_flagged = false;

	// normal ordering: colored[0],colorless[0]...colorless[n],colored[1]...colored[m]
	if(colored.size()>0){
		local_all_particles.push_back(colored[0]);
		local_all_particles.insert(local_all_particles.end(),colorless.begin(),colorless.end());
		local_all_particles.insert(local_all_particles.end(),colored.begin()+1,colored.end());
	}
	else{
		local_all_particles.insert(local_all_particles.end(),colorless.begin(),colorless.end());
	}

	if(colored.size()==0&&colorless.size()==0){
		DEBUG_MESSAGE("A vertex received with no particles -- Did nothing");
		return;
	}

	// create CurrentExternal's with care on color ordering

	// Now take care of preparing the starting configuration of local_all_currents:
	// First, the 'left' current; in case of a tree, also organizes global_close_current and contract_type_particle_on_the_right
	Current * left_current;
	if(i==0){
		// check if left particle is empty, such that we have that is a tree-level case
		const ColorHandledProcess& lch0(global_topology.get_vertex(0));

                // Make sure that the Tadpole is not confused with a tree
                is_tadpole = (global_topology.size() == 1) && lch0.get_left().get_name() != "NOPARTICLE" && lch0.get_right().get_name() != "NOPARTICLE";
                if( is_tadpole ) {
                  DEBUG_MESSAGE("Handling the special case of a Tadpole cut!");
                }

		// tree level
		if(lch0.get_left().get_name()=="NOPARTICLE"&&lch0.get_right().get_name()=="NOPARTICLE"){
			// remove first entry in local_all_particles for producing left current
			// if necessary, all_currents will be modified!
			left_current=get_external_current(local_all_particles[0],all_currents_set,all_tree_currents);
			// tree level calculations never track internal_index
			left_current->set_internal_index(0);
			local_all_particles.erase(local_all_particles.begin());

			// assign global_close_current for this tree level case, and remove last particle from local_all_particles
			global_close_current=get_external_current(local_all_particles.back(),all_currents_set,all_tree_currents);
			contract_type_particle_on_the_right=local_all_particles.back()->get_anti_type();
			local_all_particles.pop_back();
		}
		// error ??
		else if(lch0.get_left().get_name()=="NOPARTICLE"||lch0.get_right().get_name()=="NOPARTICLE"){
			std::cout<<"ERROR got a single vertex cutTopology with only right or left empty particles!! -- Did nothing"<<std::endl;
			return;
		}
		// loop-level cut
		else{
			if(global_location_generated_loop_momentum.size()==0){
				std::cout<<"There has to be at least one loop particle!! -- did nothing"<<std::endl;
				global_handle_request=false;
				return;
			}
			// retrieve left loop particle
			Particle * left_loop_particle(std::get<1>(generated_loop_particles[global_location_generated_loop_momentum[0]]));
			// and set left current
			left_current=get_external_current(left_loop_particle,all_currents_set,all_tree_currents);
			// Fix the internal_index of the CurrentExternal to match assignment, if needed
			if(needs_internal_index_handling)
				left_current->set_internal_index(global_topology.get_vertex(i).get_internal_index_left());
			else
				left_current->set_internal_index(0);

		}
	}

        // The Tadpole has to go through both blocks (i=0 and i+1=number_of_vertices) at the same time
        if( i > 0 || is_tadpole){
		if(global_start_current==nullptr && !is_tadpole){
			std::cout<<"Reached vertex: "<<i<<" with a null global_start_current -- did nothing"<<std::endl;
			return;
		}
		// we are processing a cut (NOTICE: this current has already proper 'dynamical_internal_index', and (in case necessary) assignment of 'internal_indices_open')
                // For the Tadpole the left_current is the external particle from the previous block
		if(!is_tadpole) left_current=global_start_current;
		// next isn't needed, as already assigned to zero in Forest::build_offshell_currents()
		//if(!needs_internal_index_handling)
		//	// simplify by dropping when no handling needed
		//	left_current->set_internal_index(0);
		// in case of a loop level cut, when reaching final (non-zero) vertex, assign global_close_current
		if(i+1==global_topology.size()){
			// assign global_close_current
			// 1-loop
			if(global_loop_level==1){
				Particle * right_loop_particle(std::get<0>(generated_loop_particles[global_location_generated_loop_momentum[0]]));
				global_close_current=get_external_current(right_loop_particle,all_currents_set,all_tree_currents);
			}
			else if(global_loop_level==2){
				Particle * right_loop_particle(std::get<1>(generated_loop_particles[global_location_generated_loop_momentum[1]]));
				global_close_current=get_external_current(right_loop_particle,all_currents_set,all_tree_currents);
			}
		}
	}

	size_t n_start_current(left_current->get_n_leaves());

	size_t n_total_vertex(local_all_particles.size());
	// local_all_currents, sized accordingly
	std::vector<std::vector<Current*>> local_all_currents(n_total_vertex+n_start_current);

	// add left_current
	local_all_currents[n_start_current-1].push_back(left_current);
	Current* lastcolored(nullptr);
	if(left_current->is_colored()){
		lastcolored=left_current;
		lastcolored->set_left_color_adjacent(nullptr);
	}
	// and now all local_all_particles
	for(Particle* p : local_all_particles){
		Current* lc(get_external_current(p,all_currents_set,all_tree_currents));
		if(lc->is_colored()){
			// color stripped color ordering
			if(lastcolored!=nullptr){
				lastcolored->set_right_color_adjacent(lc);
			}
			lc->set_left_color_adjacent(lastcolored);
			lastcolored=lc;
		}
		if(needs_internal_index_handling){
			lc->set_internal_index(global_topology.get_vertex(i).get_particle_internal_index_in_vertex(p));
		}
		else
			lc->set_internal_index(0);
		local_all_currents[0].push_back(lc);
	}
	if(lastcolored!=nullptr)
		lastcolored->set_right_color_adjacent(nullptr);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// When we consider 2-loop planar configurations, check if we need color reordering!
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if(global_switch_to_planar&&i>=global_vertex_locate_l1&&global_loop_level==2){
		std::cout<<"CAREFUL: global_switch_to_planar considerably outdated!!"<<std::endl;
#if _DEBUG_FOREST
std::cout<<"CACHING a planar configuration!"<<std::endl;
#endif
		if(global_is_factorizable&&i==global_vertex_locate_l1){
#if _DEBUG_FOREST
std::cout<<"BOWTIE vertex: "<<global_is_factorizable<<" "<<global_vertex_locate_l1<<" "<<global_vertex_locate_l2<<" locations of l1 and l2 in this vertex: "<<global_index_in_vertex_locate_l1<<" "<<global_index_in_vertex_locate_l2<<std::endl;
for(auto e:local_all_currents[0]) std::cout<<"Input bowtie level-1 currents: "<<e->get_name()<<std::endl;
#endif

			// build a vector with proper ordering to help defining color ordering of currents
			std::vector<Current*> bowtie_currents;
			bowtie_currents.insert(bowtie_currents.end(),local_all_currents[0].rbegin(),local_all_currents[0].rend()-global_index_in_vertex_locate_l1);
			bowtie_currents.push_back(left_current);
			bowtie_currents.insert(bowtie_currents.end(),local_all_currents[0].begin(),local_all_currents[0].begin()+global_index_in_vertex_locate_l1);
//for(auto e:bowtie_currents)	std::cout<<"vector bowtie_currents: "<<e->get_name()<<std::endl;

			lastcolored=nullptr;
			if(bowtie_currents[0]->is_colored()){
				lastcolored=bowtie_currents[0];
				lastcolored->set_right_color_adjacent(nullptr);
			}
			// and now all bowtie_currents
			for(size_t ii=1;ii<bowtie_currents.size();ii++){
				Current* lc(bowtie_currents[ii]);
				if(lc->is_colored()){
					// planar color ordering
					if(lastcolored!=nullptr){
						lastcolored->set_left_color_adjacent(lc);
					}
					lc->set_right_color_adjacent(lastcolored);
					lastcolored=lc;
				}
			}
			lastcolored->set_left_color_adjacent(nullptr);
		}
		else{
			lastcolored=nullptr;
			if(left_current->is_colored()){
				lastcolored=left_current;
				lastcolored->set_right_color_adjacent(nullptr);
			}
			// and now all in local_all_currents[0]
			for(size_t ii=0;ii<local_all_currents[0].size();ii++){
				Current* lc(local_all_currents[0][ii]);
				if(lc->is_colored()){
					// planar color ordering
					if(lastcolored!=nullptr){
						lastcolored->set_left_color_adjacent(lc);
					}
					lc->set_right_color_adjacent(lastcolored);
					lastcolored=lc;
				}
			}
			lastcolored->set_left_color_adjacent(nullptr);
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CORE of recursive production!
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// get copy of Coupling pointers
	const std::vector<Coupling>& lcoups(model->v_of_couplings());
	// the necessary powers
        cutTopology::couplings_t lpowers(global_topology.get_couplings_up_to(i));
        SelectionRule s_rule(global_topology.get_selection_rules_of_v(i));
	// tracks all new CurrentBuild's created locally
	std::vector<Current*> all_local_new;

        // recursive level-by-level construction of currents
	for(int j=2;j<=int(local_all_currents.size());j++){
		// sub currents will build currents like ((12)3) in: <123> = ((12)3)+(1(23))+(123)
		std::vector<Current *> level_i_sub_currents;

		// CurrentRule's loop
		// Each vertex will (potentially) contribute new currents at this level
		for(CurrentRule * vlocal : model->v_of_rules()){
                    // ignore the rule if ds decomposition is disabled or no selection rule provided
                    if(vlocal->selection_rule_relevant() and (!enable_Ds_decomposition or s_rule.is_empty())) continue;

			size_t lmultiplicity(vlocal->get_rule_multiplicity());
			// to construct a level "j" current, find break up in (lmultiplicity-1) partitions
			for(const auto& current_partition : partitions.get_partitions(j,lmultiplicity-1)){
				// Delegate for now construction of all currents for each partition to the function "push_needed_currents"
				// --> might this be a Strategy???
				push_needed_currents(vlocal,current_partition,level_i_sub_currents,local_all_currents,lcoups,lpowers,needs_internal_index_handling,global_internal_index_vertex);
			}
		}

                // Remove top level currents which do not match selection rule
                if (j == int(n_total_vertex + n_start_current) && !s_rule.is_empty()) {
                    // first we need to find contractions with scalars on the right if ir is possible
                    if(contract_type_particle_on_the_right.get_flavor() == ParticleFlavor::ds_scalar){
                        for (auto c : level_i_sub_currents) {
                            std::vector<int*> locations;
                            for (auto& line : c->qqs_powers) {
                                for (auto& ind : line.second) {
                                    if (ind == current_helper::_default_scalar_contract_index) locations.push_back(&ind);
                                }
                            }

                            // TODO: for multiple quark lines take into an account that contraction between lines are possible
                            if (locations.size() > 1) {
                                _WARNING_R("Found an ambiguity in possible contraction of indices!)");
                                std::exit(1);
                            }
                            if (!locations.empty()) *locations.at(0) = global_topology.get_vertex(i).get_internal_index_right();
                        }
                    }

                    level_i_sub_currents.erase(std::remove_if(level_i_sub_currents.begin(), level_i_sub_currents.end(),
                                                              [&s_rule](Current* c) {
                                                                  bool matched_s_rule = true;
                                                                  if (c->qqs_powers.empty()) matched_s_rule = false;
                                                                  else{
                                                                      for (auto ps : c->qqs_powers) {
                                                                          if (!s_rule.is_match(ps.first, ps.second)) {
                                                                              matched_s_rule = false;
                                                                              break;
                                                                          }
                                                                      }
                                                                  }
                                                                  if (matched_s_rule == false) {
#if _DEBUG_FOREST
                                                                      _MESSAGE("Found that subcurrent ", c->get_name());
                                                                      _MESSAGE("\tdid not match selection rule ", s_rule.get_qqs_coupling());
                                                                      _MESSAGE("\tcurrent has ", c->qqs_powers);
#endif
                                                                      delete c;
                                                                      return true;
                                                                  } else {
                                                                      return false;
                                                                  }
                                                              }),
                                               level_i_sub_currents.end());
                }

#if _DEBUG_FOREST
std::cout<<"Result after recursion: j= "<<j<<std::endl;
std::cout<<"level_i_sub_currents.size(): "<<level_i_sub_currents.size()<<std::endl;
for(size_t kk=0;kk<level_i_sub_currents.size();kk++){
	std::cout<<"Subcurrent "<<kk<<" with name: "<<level_i_sub_currents[kk]->get_name()<<std::endl;
	if(needs_internal_index_handling)
		std::cout<<"	and internal index: "<<level_i_sub_currents[kk]->get_internal_index()<<std::endl;
}
#endif
		// merging CurrentBasic's and check if new current

                // Merge (or not) sub currents to promote them to currents
		while( !level_i_sub_currents.empty() ) {
			std::vector<Current*> merge;
			merge.push_back(level_i_sub_currents.back());
			level_i_sub_currents.pop_back();
//std::cout<<"Testing for merging subcurrent: "<<merge[0]->get_name()<<std::endl;
			// Find which sub currents should be merged with merge[0]
			for(int k=level_i_sub_currents.size()-1;k>-1;k--){
				bool shouldmerge(false);
//std::cout<<"	against: "<<level_i_sub_currents[k]->get_name()<<std::endl;
				shouldmerge=check_current_match(merge[0],level_i_sub_currents[k],lcoups);
				if(shouldmerge){
//std::cout<<"	merge!!"<<std::endl;
					merge.push_back(level_i_sub_currents[k]);
					level_i_sub_currents.erase(level_i_sub_currents.begin()+k);
				}
			}

                        // Filter intermediate on-shell currents (except of maximal currents -> by definition on-shell)
                        bool onshell = false;
                        if (j != int(n_total_vertex + n_start_current)) onshell = is_onshell(merge[0]);
                        if (onshell) {
                            DEBUG_MESSAGE("Dropping on-shell current: ", merge[0]->get_name(), " Number of sub-currents: ", merge.size());
                            DEBUG_MESSAGE("Dropping on-shell current: ", merge[0]->get_current_type(), " ", merge[0]->get_leaves().get_all_momenta_indices());
                            DEBUG_MESSAGE("Dropping on-shell current: Level: ", j, "/", int(n_total_vertex + n_start_current));
                            is_onshell_flagged = true;
                            for (size_t ii = 0; ii < merge.size(); ii++) { delete merge[ii]; }
                            continue;
                        }

                        // Check if corresponding current is already stored in all_currents
			bool newcurrent(true);
			Current * addcurrent;
			// in case necessary, drop propagator of merge[0]
			if(j==int(n_total_vertex+n_start_current))
				merge[0]->drop_propagator_and_pass_internal_indices(global_topology.get_vertex(i));

                        if(all_currents_set.size()>=static_cast<size_t>(j)){
                            auto f = all_currents_set[j-1].find(merge[0]);
                            if(f!=all_currents_set[j-1].end()){
                                newcurrent = false;
                                addcurrent=*f;
                                addcurrent->set_internal_index(merge[0]->get_internal_index());
                            }
                        }


			if(newcurrent){
				// Now create current from sub currents and store
				PropagatorType includeprop(PropagatorType::propagator);
//std::cout<<"Adding new current: j= "<<j<<" n_total_vertex= "<<n_total_vertex<<" n_start_current "<<n_start_current<<std::endl;
				if(j==int(n_total_vertex+n_start_current)){
					// This propagator is cut
					includeprop=PropagatorType::cut;
					if(is_last_vertex)
						// In this case, we just need an identity propagator for final contraction
						includeprop=PropagatorType::identity;
				}
				addcurrent=new CurrentBuild(merge,lcoups,includeprop,global_topology.get_vertex(i));

				local_all_currents[j-1].push_back(addcurrent);
				all_local_new.push_back(addcurrent);
				// add daughter to parents of addcurrent
				std::vector<Current*> * pcurrents(addcurrent->get_parents());
				for(size_t kk=0;kk<pcurrents->size();kk++){
					Current * lcurrent((*pcurrents)[kk]);
					lcurrent->add_daughter(addcurrent);
				}
			}
			else{
				// existing current
				local_all_currents[j-1].push_back(addcurrent);
				// redefine color adjacency to match new configuration
//std::cout<<"CALL with: "<<merge[0]->get_long_name()<<std::endl;
				addcurrent->set_left_color_adjacent(merge[0]->left_color_adjacent());
				addcurrent->set_right_color_adjacent(merge[0]->right_color_adjacent());
//std::cout<<"MATCHED with: "<<addcurrent->get_long_name()<<std::endl;
				// delete all unneded sub currents
				for(size_t ii=0;ii<merge.size();ii++){
					delete merge[ii];
				}
			}

		}

	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// filter top-level currents that don't properly contract
	if(local_all_currents.back().size()>0){
		for(size_t yy=local_all_currents.back().size();yy>0;yy--){
			if(local_all_currents.back()[yy-1]->get_current_type()!=contract_type_particle_on_the_right){
#if _DEBUG_FOREST
std::cout<<"Removing the top-level current: "<<local_all_currents.back()[yy-1]->get_long_name()<<" as it does not contract well with right particle ("<<contract_type_particle_on_the_right<<")"<<std::endl;
#endif
				Current* toerase(local_all_currents.back()[yy-1]);
				// check if current is contained in all_local_new
				auto position(std::find(all_local_new.begin(),all_local_new.end(),toerase));
				if(position!=all_local_new.end()){
					all_local_new.erase(position);
				}
				dispose_current(toerase);
				local_all_currents.back().erase(local_all_currents.back().begin()+yy-1);
			}
		}
	}

	// filter top-level currents that don't match coupling request
	if(local_all_currents.back().size()>0){
                auto comulative_couplings = global_topology.get_couplings_up_to(i);
                for(size_t yy=local_all_currents.back().size();yy>0;yy--){
                        if(!local_all_currents.back()[yy-1]->check_couplings(comulative_couplings)){
#if _DEBUG_FOREST
std::cout<<"Removing the top-level current: "<<local_all_currents.back()[yy-1]->get_long_name()<<" as it does not match required couplings ("<<comulative_couplings<<")"<<std::endl;
#endif
				Current* toerase(local_all_currents.back()[yy-1]);
				// check if current is contained in all_local_new
				auto position(std::find(all_local_new.begin(),all_local_new.end(),toerase));
				if(position!=all_local_new.end()){
					all_local_new.erase(position);
				}
				dispose_current(toerase);
				local_all_currents.back().erase(local_all_currents.back().begin()+yy-1);
                        }
                }
        }

#if _DEBUG_FOREST
std::cout<<"Size of local_all_currents: "<<local_all_currents.size()<<std::endl;
for(size_t ii=0;ii<local_all_currents.size();ii++){
	std::cout<<"\t["<<ii<<"]: "<<local_all_currents[ii].size()<<std::endl;
	for(size_t jj=0;jj<local_all_currents[ii].size();jj++){
		std::cout<<"\t  Current_"<<ii<<"_"<<jj<<"= "<<local_all_currents[ii][jj]->get_name()<<std::endl;
	}
}
#endif

	// check that/or which maximal-level current contracts well with right particle of vertex
	// if several maximal level, send error
	if(local_all_currents.back().size()!=1){
            if (is_onshell_flagged) {
                _WARNING_R("Forest::offshell_currents_from_vertex(", i, "): on-shell vertex has been added to the Forest");
                global_topology.show();
            }
            else {
                _WARNING_R("Forest::offshell_currents_from_vertex(", i, ") ended with ", local_all_currents.back().size(), " currents, it should be 1!");
            }
            dispose_currents(all_local_new);
            global_handle_request = false;
            throw std::runtime_error("Forest ended with no currents");
            return;
        }

	// check that final current is of proper type
	if(local_all_currents.back()[0]->get_current_type()!=contract_type_particle_on_the_right){
            _WARNING_R("Forest::offshell_currents_from_vertex(", i, ") ended with current of type ", local_all_currents.back()[0]->get_current_type(),
                       " which doesn't contract well with request of: ", contract_type_particle_on_the_right);
            dispose_currents(all_local_new);
            global_handle_request = false;
            throw std::runtime_error("Forest ended with wrong maximal current");
            return;
        }

	// remove daughterless currents (except for maximal)
	for(size_t ii=all_local_new.size();ii>1;ii--){
		if(all_local_new[ii-2]->get_daughters()->size()==0){
//std::cout<<"TESTING daughterless current: "<<all_local_new[ii-2]->get_long_name()<<std::endl;
			// remove it from local_all_currents
			size_t level(all_local_new[ii-2]->get_n_leaves());
			local_all_currents[level-1].erase(
				std::remove(local_all_currents[level-1].begin(),local_all_currents[level-1].end(),all_local_new[ii-2]),
				local_all_currents[level-1].end()
			  );
			dispose_current(all_local_new[ii-2]);
			all_local_new.erase(all_local_new.begin()+ii-2);
		}
	}


	// store information on currents containing default loop particles
	for(size_t jj=1;jj<local_all_currents.size();jj++){
	  for(size_t nn=0;nn<local_all_currents[jj].size();nn++){
		Current* localcurrent(local_all_currents[jj][nn]);
		bool is_tree_current(true);
		bool is_new_current(true);
		if(std::find(all_local_new.begin(),all_local_new.end(),localcurrent)==all_local_new.end())
			is_new_current=false;
		for(size_t ii=0;ii<global_level_1_currents_default.size();ii++){
			if(current_overlap(global_level_1_currents_default[ii],localcurrent)){
//std::cout<<"Found that current: "<<localcurrent->get_name()<<" contains default state: "<<global_level_1_currents_default[ii]->get_name()<<std::endl;
				is_tree_current=false;
				if(is_new_current)
					// add to default_state_particles
					default_state_particles.add_current_to_entry(global_level_1_currents_default[ii],localcurrent);
				while(global_currents_with_loop_particles.size()<localcurrent->get_n_leaves()){
					global_currents_with_loop_particles.push_back(std::vector<Current*>());
				}
				// and also to container of all loop-particle currents -- only if not contained!
				if(std::find(global_currents_with_loop_particles[localcurrent->get_n_leaves()-1].begin(),
						global_currents_with_loop_particles[localcurrent->get_n_leaves()-1].end(),localcurrent)
						==global_currents_with_loop_particles[localcurrent->get_n_leaves()-1].end())
					global_currents_with_loop_particles[localcurrent->get_n_leaves()-1].push_back(localcurrent);
			}
		}
		// if the current had no dependence on loop particles, store as external
		if(is_tree_current&&is_new_current){
			while(all_tree_currents.size()<localcurrent->get_n_leaves()){
				all_tree_currents.push_back(std::vector<Current*>());
			}
			// container of all external-state currents
			all_tree_currents[localcurrent->get_n_leaves()-1].push_back(localcurrent);
		}
	  }
	}

	// move to global_all_currents
	for(auto ci : all_local_new){
		size_t llevel(ci->get_n_leaves());
		while(all_currents_set.size()<llevel){
                    all_currents_set.emplace_back(1000,std::hash<Current*>{},detail::CurrentEquality{model});
		}
		all_currents_set[llevel-1].insert(ci);
	}

	// define left current
	global_start_current=local_all_currents.back()[0];

}

void Forest::build_offshell_currents(){
	DEBUG_MESSAGE("Offshell build up step: (II) vertex-by-vertex recursion");

	// shall we proceed?
	if(!global_handle_request)
		return;

	bool is_last_vertex(false);
	// we go vertex-by-vertex in global_topology
	for(size_t ii=0;ii<global_topology.size();ii++){
		// on the last step of the construction, tell so to offshell_currents_from_vertex(.)
		if(ii+1==global_topology.size())
			is_last_vertex=true;

		// set global_internal_index_vertex
		if(global_topology.get_vertex(ii).needs_internal_index_handling()){
			// we also assign full information current indices
			global_internal_index_vertex={global_topology.get_vertex(ii).needs_internal_index_handling(),
				global_topology.get_vertex(ii).get_internal_index_left(),
				global_topology.get_vertex(ii).get_internal_index_right(),
				global_topology.get_vertex(ii).get_all_internal_index().first,
				global_topology.get_vertex(ii).get_all_internal_index().second};
		}
		else
			global_internal_index_vertex=InternalIndexVertex{};

		// recursively builds all offshell currents associated with this
		this->offshell_currents_from_vertex(ii,is_last_vertex);

		if(ii+1<global_topology.size()){
			// do we need to handle in next step internal indices?
			if(global_topology.get_vertex(ii+1).needs_internal_index_handling()){
	                	// here we adjust current internal_index
				int in_index(global_topology.get_vertex(ii+1).get_internal_index_left());
				global_start_current->set_internal_index(in_index);
			}
			else{
				global_start_current->set_internal_index(0);
			}
		}
	}

}

void Forest::finish_add_cutTopology(unsigned Ds_count){
	DEBUG_MESSAGE("Offshell build up step: (III) closing addition of offshell currents");

	// shall we proceed?
	if(!global_handle_request){
		std::cout<<"Failed to add to Forest: "<<global_topology.get_short_name()<<std::endl;
		return;
	}

	// check that we end up with proper closing currents
	if(global_start_current==nullptr){
		std::cout<<"ERROR -- final current not available in Forest::finish_add_cutTopology() -- Set location_new_forest_entry to zero!!"<<std::endl;
		location_new_forest_entry=0;
		return;
	}
	if(global_close_current==nullptr){
		std::cout<<"ERROR -- after finishing addition of currents, Forest::finish_add_cutTopology() finds no contract final current -- Set location_new_forest_entry to zero!!"<<std::endl;
		location_new_forest_entry=0;
		return;
	}

#if 0
std::cout<<"BEFORE STORING:"<<std::endl;
for(size_t ii=0;ii<global_currents_with_loop_particles.size();ii++){
	for(size_t jj=0;jj<global_currents_with_loop_particles[ii].size();jj++)
		std::cout<<ii<<" "<<jj<<" "<<global_currents_with_loop_particles[ii][jj]<<std::endl;
}
#endif

	// store info
	all_processes.push_back(
			std::make_tuple(
				global_topology,
				global_start_current,
				global_close_current,
				global_currents_with_loop_particles
			)
		);

	// In case asked to the Forest, take care of defining the number of states that can be accepted by maximal cut
	if(minimal_states_in_currents){
            global_start_current->set_maximal_number_of_dimensions_accepted(Ds_count);
	}

	location_new_forest_entry=all_processes.size();
}

int Forest::add_cutTopology(std::vector<std::tuple<int,int,int,int>> loop_parts_in,cutTopology& topology,std::vector<int> loop_gen_location,bool switch_to_planar_standard){

	DEBUG_MESSAGE("Starting addition to Forest of cutTopology: "+topology.get_short_name()+" "+topology.show_internal_indices());
	// set to zero as default
	location_new_forest_entry=0;

	if(forest_closed){
		std::cout<<"Forest has been closed: all \"default\" state particles resolved by calling this->define_internal_states_and_close_forest(.)"<<std::endl;
		std::cout<<" --- return 0"<<std::endl;
		return 0;
	}

	// (I) initialization

	// Makes sure that global containers of *this are empty before looping over the vertex subprocesses
	// copies the input variables of *this into the global variables
	this->store_variables(loop_parts_in,topology,loop_gen_location,switch_to_planar_standard);

	// (II) vertex-by-vertex recursion
	this->build_offshell_currents();

	// (III) closing
	// After checks have been performed, and if needed, it stores information on the variables processed
	// notice that inside here location_new_forest_entry is set to proper value

        // In general Ds count is [number of loops] + 1 - [number of closed quark loops]
        unsigned Ds_count = loop_parts_in.size() + 1 - (topology.get_n_closed_light_fermion_loops() + topology.get_n_closed_heavy_fermion_loops());

        // For pure gluons gluons we know that we have at most Ds^1 when topology is not factorizable
        if (model->get_name() == "YM" && !global_is_factorizable) Ds_count = 2;

        // Gravity needs a different counting
        if (model->get_name() == "CubicGravity" || model->get_name() == "EHGravity"){
            Ds_count = 2*loop_parts_in.size()+1;
        }

        this->finish_add_cutTopology(Ds_count);

	DEBUG_MESSAGE("Location in forest added: ",location_new_forest_entry);

	return location_new_forest_entry;
}

const std::vector<Coupling>& Forest::get_model_couplings() const {
	return model->v_of_couplings();
}

int Forest::location_loop_momenta(std::pair<Particle*, Particle*> lp, int i1,int i2,int i3,int i4){
	int toret(0);
	// find out if lp was already used
	bool newlooppart(true);

        // For the purpose of idenifying in the Forest we will not use operator==(Partice, Particle) because
        // we want to check only specific things
        auto equivalent_particles = [](const Particle& p1, const Particle& p2, size_t ind){
            if(p1.get_name() != p2.get_name()) return false;
            if (ind != p2.mom_index() ) return false;
            if (p1.get_type() != p2.get_type()) return false;
            if (p1.ref_index() != p2.ref_index()) return false;
            if (p1.external_state() != p2.external_state()) return false;
            return true;
        };

        using std::get;

	for(size_t ii=0;ii<generated_loop_particles.size();ii++){
		// check performed only for corresponding Particle. Other location info is irrelevant: should change for example for thread-safe computations -- we use local_to_test to match what we store
            if (equivalent_particles(*get<0>(lp), *std::get<0>(generated_loop_particles[ii]),i3) &&
                equivalent_particles(*get<1>(lp), *std::get<1>(generated_loop_particles[ii]),i4)) {
                DEBUG_MESSAGE("Found that loop particles ",lp," have been already used!");
                newlooppart = false;
                toret = int(ii);
                break;
            }
        }
        if(newlooppart){
		toret=int(generated_loop_particles.size());
		// create particles that are associated to the loop momenta passed, with "default" states
                //check that momenta were already assigned correctly
                Particle* newl1 = new Particle(*get<0>(lp));
                Particle* newl2 = new Particle(*get<1>(lp));

                newl1->set_momentum_index(i3);
                newl2->set_momentum_index(i4);

		generated_loop_particles.emplace_back(newl1,newl2);

                DEBUG_MESSAGE("Created new loop particles: ",generated_loop_particles.back());
	}
	return toret;
}


int Forest::add_cut(std::vector<std::tuple<int,int,int,int>>& loop_parts_in,cutTopology& topology,size_t ref_mom,bool switch_to_planar_standard){
	DEBUG_MESSAGE("Called Forest::add_cut with(.) cutTopology: ",topology.get_short_name());
	
	//int n_loops(loop_parts_in.size());
	std::vector<Particle*>& all_external(topology.get_particles_vertices());
	size_t totalps(all_external.size());

	DEBUG_MESSAGE("Adding "+std::to_string(loop_parts_in.size())+"-loop Cut to Forest");

	std::vector<int> gen_locations;

	if(topology.get_left()==nullptr||topology.get_right()==nullptr){
		_WARNING_R("Forest::add_cut(.) can't deal with left or right missing particles!");
                std::exit(1);
	}

	// fill info in gen_locations for each entry in loop_parts_in
	for(size_t ii=0;ii<loop_parts_in.size();ii++){
                using std::get;
                auto find_l = [&](int lmom) {
                    if (lmom == 0) return const_cast<Particle*>(topology.get_left());
                    if (lmom == int(totalps) + 1) return const_cast<Particle*>(topology.get_right());
                    return all_external[lmom - 1];
                };
                // std::cout<<"THE FOUND part is: "<<loopmom->get_name()<<" "<<loopmom->get_type()<<std::endl;
                gen_locations.push_back(this->location_loop_momenta({find_l(get<0>(loop_parts_in[ii])), find_l(get<1>(loop_parts_in[ii]))}, get<0>(loop_parts_in[ii]), get<1>(loop_parts_in[ii]),
                                                                    get<2>(loop_parts_in[ii]), get<3>(loop_parts_in[ii])));
#if 0
std::cout<<"and location is: "<<gen_locations.back()<<std::endl;
for(size_t ii=0;ii<generated_loop_particles.size();ii++)
std::cout<<"i: "<<ii<<" "<<std::get<0>(generated_loop_particles[ii])->get_name()<<" "<<std::get<1>(generated_loop_particles[ii])->get_name()<<std::endl;
#endif
	}
        DEBUG_PRINT(gen_locations);
        DEBUG_PRINT(loop_parts_in);

	int p_ids(0);
	p_ids=this->add_cutTopology(loop_parts_in,topology,gen_locations,switch_to_planar_standard);

	// check if cut addition was successful
	if(p_ids==0){
		std::cout<<"Forest::add_cut(.) failed for: "<<topology.get_short_name()<<" -- returned 0!"<<std::endl;
		return 0;
	}
	else{
		// register cut in states_manager
		int itoreturn(states_manager.register_cut(p_ids,ref_mom,gen_locations));

		return itoreturn;
	}
}

size_t Forest::get_size_of_cut_register() const {
	// Forwards request to states_manager
	return states_manager.get_size_of_cut_register();
}

size_t Forest::get_ref_mom_for_cut(int cut_i) const {
	// Forwards request to states_manager
	return states_manager.get_ref_mom_for_cut(cut_i);
}

int Forest::get_current_index_for_cut(int cut_i) const {
	// Forwards request to states_manager
	return states_manager.get_current_index_for_cut(cut_i);
}

/** 
 * This routine checks if the input current corresponds to an on-shell particle by checking momentum conservation
 * All particles with ParticleFlavor::renormalon are assumed to have zero momentum!
 */
bool Forest::is_onshell(Current* c) const {
  bool onshell = false;
  std::vector<Particle*> external = global_topology.get_particles_vertices();

  // Find number of external auxillary fields and their zero momenta
  size_t number_of_renormalons(0);
  std::vector<size_t> ren_momentum_indices;
  for (auto p : external)
      if (p->get_flavor() == ParticleFlavor::renormalon) {
          number_of_renormalons++;
          ren_momentum_indices.push_back(p->mom_index());
      }

  size_t check;
  if (global_loop_level == 0) { check = external.size() - number_of_renormalons; }
  else {
      check = external.size() - 2 * (global_loop_level - 1) - number_of_renormalons;
  }

  // Remove internal particles and renormalons
  external.erase(remove_if(begin(external), end(external), [](Particle* p){
    return (p->external_state().is_external() == false or p->get_flavor() == ParticleFlavor::renormalon);
  }), end(external) );

  size_t n = external.size();

  // Sanity check that we got all the external particles
  if( n != check ) {
    _MESSAGE("is_onshell: The number of external particles are not what they are supposed to be! Current: ",c->get_name());
    _MESSAGE("Number of loops: ", global_loop_level);
    _MESSAGE("Number of check/n: ", check," \t ", n);
    std::exit(1);
  }

  // Mometum flowing out of the current
  std::vector<size_t> momentum = c->get_leaves().get_all_momenta_indices();
  // Remove vanishing momenta
  momentum.erase(std::remove_if(begin(momentum), end(momentum), [v = ren_momentum_indices](size_t i) { return (std::find(v.begin(), v.end(), i) != v.end()); }),
                 end(momentum));
  momentum = mom_conservation(momentum, n);

  /* Distinguish 3 cases:
   * a) momentum adds up to zero (internal Tadpoles)
   * b) momentum adds up to exactly one ext. leg (self energies)
   * c) momentum is a superposition (regular contribution)
   */

  if( momentum.size() == 0){
    // a) momentum adds up to zero (internal Tadpoles)
    // check if current type corresponds to massless propagator
    if( c->get_current_type().is_massless() == true ) onshell = true;
  }

  if( momentum.size() == 1){
    // b) momentum adds up to exactly one ext. leg (self energies)
    // We have to check: if momentum flowing through the leg corresponds to the propagator mass
   
    if( momentum[0] <= n){ 
        for (auto p : external) {
            if (p->mom_index() != momentum[0]) continue;
            if (p->get_flavor() == c->get_current_type().get_anti_flavor()) onshell = true;
            if (p->get_flavor() == c->get_current_type().get_flavor()) onshell = true;
        }
    }
    else {
        // at 2 Loop we have to match not only with external momenta but also cut loop momenta
        std::vector<Particle*> cutLoop = global_topology.get_particles_vertices();
        cutLoop.erase(remove_if(begin(cutLoop), end(cutLoop), [](Particle* p) { return p->external_state().is_external() == true; }), end(cutLoop));

        for (auto p : cutLoop) {
            std::vector<size_t> match_ext = {momentum[0], p->mom_index()};
            // remove duplicates
            match_ext.erase(unique(match_ext.begin(), match_ext.end()), match_ext.end());
            match_ext = mom_conservation(match_ext, n);
            // Distinguish 2 cases: 
            // a) if (-l,l) has been matched then match_ext is empty after momentum conservation
            if (match_ext.size() == 0 && p->get_flavor() == c->get_current_type().get_anti_flavor()) onshell = true;
            // b) {l} is left and is part of cutLoop
            if (match_ext.size() == 1 && p->mom_index() == momentum[0] && p->get_flavor() == c->get_current_type().get_anti_flavor()) onshell = true;
        }
    }
  }

  // c) momentum is a superposition (regular contribution)
  // if more than one momentum is flowing through the leg -> off-shell 
  // Nothing to do

  return onshell;
}

std::vector<size_t> Forest::mom_conservation(const std::vector<size_t>& mom, size_t n) const {

  /* separate momenta into internal and external momenta */
  std::vector<size_t> internal, external, qext;
  for( auto i : mom ){
    if( i > n )  internal.push_back(i);
    if( i <= n ) external.push_back(i);
  }

  /* Momentum conservation implies p1 + p2 + ... + pn = 0 
   * We represent it as a list {p1,p2,...,pn} and remove all 
   * momenta that are present in the original momentum configuration.
   * The left over momenta are those as applied by momentum conservation */
  std::vector<size_t> conserv(n);
  std::iota(conserv.begin(), conserv.end(),1);
  
  // Apply momentum conservation to external momenta only if (n-1) momenta are present
  if( external.size() >=  n-1){
    /* the external momenta have to be sorted */
    std::sort(external.begin(),external.end(), std::greater<size_t>());

    for( auto i : external)
      conserv.erase( conserv.begin() + i-1 );

    external = conserv;
  }

  /* Find pairings of internal loop momenta 
   * 1) We subtract the number of ext. particles from the loop labels and sort them, then the 
   *    internal momenta look always like {1,2,3,4,5,...} and always (1,2) and (3,4) and so 
   *    on should be merged.
   * 2) We find pairs by re-assigning the even numbers x -> -(x-1), thus the list takes the form 
   *    {1,-1,2,-2,3,-3,..} so we only have to check if neighbors add up to zero and remove them
   */
  /* the internal momenta have to be sorted */
  std::sort(internal.begin(),internal.end(), std::less<size_t>());

  std::vector<int> pair;
  for( auto i : internal){
    int x = i - n;
    if( x % 2 == 0 ){ x = -(x-1);}
    pair.push_back( x );
  }

  /* Remove all loop pairs from internal */
  for(int i = pair.size()-1; i > 0; i--){
    if( pair[i] + pair[i-1] == 0 ){
      internal.erase(internal.begin() + i);
      internal.erase(internal.begin() + i-1); 
      i--;
    }
  }

  external.insert(external.end(), internal.begin(), internal.end());

  return external;
}

std::ostream& operator<<(std::ostream& s, Forest& forest){
  print_all_currents(forest);
  explore_current_inheritance(forest);
  return s;
}

void print_all_currents(Forest& forest){
        auto currents_processes = forest.get_all_currents();
	size_t totalnumber(0);
	for(size_t i=0;i<currents_processes.size();i++)
		totalnumber+=currents_processes[i].size();
	std::cout<<"############"<<std::endl;
	std::cout<<"Forest with at total of "<<totalnumber<<" currents"<<std::endl;
	for(size_t i=0;i<currents_processes.size();i++){
		std::cout<<"############"<<std::endl;
		std::cout<<"Start printing level "<<i+1<<" currents (total currents in this level: "<<currents_processes[i].size()<<")"<<std::endl;
		std::vector<Current *> * currents_this_level(&(currents_processes[i]));
		for(size_t j=0;j<currents_this_level->size();j++){
			std::cout<<"Current "<<j<<" with "<<(*currents_this_level)[j]->get_long_name()<<std::endl;
		}
		std::cout<<"############"<<std::endl;
	}
}

void explore_current_inheritance(Forest& forest){
        auto currents_processes = forest.get_all_currents();
	while (true) {
		size_t desiredlevel,desiredcurrent;
		std::cout<<"Please write level of current to explore (\"0\" will take you out): ";
		std::cin>>desiredlevel;
		if(desiredlevel==0)	break;
		std::cout<<"Please write current to explore: ";
		std::cin>>desiredcurrent;
		if(0<desiredlevel && desiredlevel<(currents_processes.size()+1)){
			std::vector<Current *> * currents_this_level(&(currents_processes[desiredlevel-1]));
			if(desiredcurrent<(currents_this_level->size())){
				Current* plC((*currents_this_level)[desiredcurrent]);
				CurrentBuild* plCB(dynamic_cast<CurrentBuild*>(plC));
				std::cout<<"============ \n";
				std::cout<<"Current "<<desiredcurrent<<" with "<<*plC<<std::endl;
				if(desiredlevel>1){
					std::cout<<"============ \n";
					std::cout<<"associated (scalar) propagator type: "<<plCB->includepropagator<<std::endl;
				}
				const PhysicalStateContainer& lps(plC->get_physical_states());
				if(lps.size()>0){
					std::cout<<"============ \n";
					std::cout<<"This current has default states, and have been set to:"<<std::endl;
					for(size_t ii=0;ii<lps.size();ii++){
						std::cout<<ii<<":"<<lps[ii].get_short_name()<<std::endl;
					}
				}
				std::cout<<"============ \n";
				std::vector<Current *> * parents(plC->get_parents());
				if(parents){
				  if(parents->size()>0){
					std::cout<<"has parents: \n";
					std::cout<<"============ \n";
					for(size_t i=0;i<parents->size()-1;i++){
						std::cout<<(*parents)[i]->get_name()<<" "<<(*parents)[i]->get_coupling_string()<<", ";
						if(i>0 and (i%4)==0)	std::cout<<std::endl;
					}
					std::cout<<(*parents)[parents->size()-1]->get_name()<<" "<<(*parents)[parents->size()-1]->get_coupling_string()<<std::endl;
				  }
				  else{
					std::cout<<"and has no parents \n";
				  }
				}
				else{
					std::cout<<"has no parents \n";
				}
				std::cout<<"============ \n";
				std::vector<Current *> * daughters(plC->get_daughters());
				if(daughters){
				  if(daughters->size()>0){
					std::cout<<"and has as daughters: \n";
					std::cout<<"============ \n";
					for(size_t i=0;i<daughters->size()-1;i++){
						std::cout<<(*daughters)[i]->get_name()<<" "<<(*daughters)[i]->get_coupling_string()<<", ";
						if(i>0 and (i%4)==0)	std::cout<<std::endl;
					}
					std::cout<<(*daughters)[daughters->size()-1]->get_name()<<" "<<(*daughters)[daughters->size()-1]->get_coupling_string()<<std::endl;
				  }
				  else{
					std::cout<<"has no daughters \n";
				  }
				}
				else{
					std::cout<<"has no daughters \n";
				}
				if(desiredlevel>1){
				  if((plCB->basicblocks).size()>0){
					std::cout<<"============ \n";
					std::cout<<"has ";
					if((plCB->basicblocks).size()==1)
						std::cout<<"1 subcurrent:"<<std::endl;
					else
						std::cout<<(plCB->basicblocks).size()<<" subcurrents:"<<std::endl;
					std::cout<<"============ \n";
					for(size_t mm=0;mm<(plCB->basicblocks).size();mm++){
						CurrentBasic* plSC(dynamic_cast<CurrentBasic*>((plCB->basicblocks)[mm]));
						std::cout<<plSC->rule->get_name()<<": ";
						std::vector<Current*>* pSCps(plSC->get_parents());
						for(size_t pp=0;pp<pSCps->size();pp++){
							Current* lpc((*pSCps)[pp]);
							size_t level_lpc(lpc->get_n_leaves());
							size_t location_in_level(0);
							std::vector<Current *> * currents_lpc_level(&(currents_processes[level_lpc-1]));
							location_in_level=std::find(currents_lpc_level->begin(),currents_lpc_level->end(),lpc)-currents_lpc_level->begin();
							std::cout<<"	("<<(plSC->indices)[pp]<<")["<<level_lpc<<","<<location_in_level<<"]: "<<*lpc<<std::endl;
						}
					}
				  }
				}
				std::cout<<"============ \n";
				if(desiredlevel==currents_processes.size()){
					bool has_pair(false);
        				const std::vector<std::tuple<cutTopology,Current*,Current*,std::vector<std::vector<Current*>>>>& all(forest.all_processes);
					size_t pairings(0);
					for(size_t ww=0;ww<all.size();ww++){
						if(plC==std::get<1>(all[ww])){
							if(!has_pair){
								std::cout<<"Maximal current paired to: "<<std::endl;
								has_pair=true;
							}
							++pairings;
							std::cout<<"	"<<pairings<<": "<<*(std::get<2>(all[ww]))<<std::endl;
						}
					}
					if(has_pair)
						std::cout<<"============ \n";
				}
			}
			else{
				std::cout<<"WARNING: there is no Current: "<<desiredcurrent<<" in level: "<<desiredlevel<<" of the Forest\n";
			}
		}
		else{
			std::cout<<"WARNING: there is no level: "<<desiredlevel<<" in the Forest\n";
		}
	}
}

bool detail::CurrentEquality::operator()(Current* c1, Current* c2) const { return check_current_match(c1, c2, m->v_of_couplings()); }

}

