#ifndef CARDINALITY_H_
#define CARDINALITY_H_

#include "Core/type_traits_extra.h"
#include <cstdint>
#include <memory>
#include <set>

#include <mutex>

namespace Caravel{

class Builder_normalizations_base;

/**
 * A class to do all stuff necessary to update finite field
 * cardinality in a centralized way
 * Template parameters:
 * T - Underlying finite field type.
 * D - Dimensionality of any builders being controlled by the system.
 */
template <typename T> class cardinality_update_manager {
    static_assert(is_finite_field<T>::value, "Managing cardinality for non finite field types doesn't make sense.");

  private:
    cardinality_update_manager() {};

    static std::unique_ptr<cardinality_update_manager<T>> instance;
    std::set<Builder_normalizations_base*> builders;
    static std::mutex b_access;

    static std::once_flag initInstanceFlag;
    static void init();

  public:
    static cardinality_update_manager<T>* get_instance();

    // filled by builder constructor
    void add_builder(Builder_normalizations_base* b);
    void remove_builder(Builder_normalizations_base* b);

    /**
     * Change cardinality. Updates everything required.
     * If you need to add more stuff to be updated, add to the implementation of this function.
     */
    void set_cardinality(uint64_t);

    uint64_t get_current_cardinality();

    cardinality_update_manager(const cardinality_update_manager<T>& ) = delete;
    cardinality_update_manager& operator=(const cardinality_update_manager<T>& ) = delete;
};

}

#endif

