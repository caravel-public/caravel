/**
 * @file Coupling.h
 *
 * @date 9.6.2017
 *
 * @brief Class Coupling implentation for handling generic couplings in Model's
 *
 * Class Coupling implentation for handling generic couplings in Model's. We try to have a unified description of couplings, although explicit 
 * specializations of Coupling::get_value() are provided.
 *
*/
#ifndef COUPLING_H_
#define COUPLING_H_

#include <utility>
#include <string>
#include <iostream>

#include "Core/typedefs.h"

#ifdef INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif


namespace Caravel {

/**
 * Class to hold information for couplings
 */
class Coupling {
		std::string name;
		int value_num_r;	/**< Real part of numerator of the coupling */
		int value_num_i;	/**< Imaginary part of numerator of the coupling */
		int value_den_r;	/**< Real part of denominator of the coupling */
		int value_den_i;	/**< Imaginary part of denominator of the coupling */
		int square_num_normalization;	/**< sqrt(square_num_normalization) is included in numerator of the coupling */
		int square_den_normalization;	/**< sqrt(square_num_normalization) is included in denominator of the coupling */
		int power_of_factor_out_I;	/**< The powers of imaginary 'I' that have been factored out in this coupling */
	public:
                Coupling() = default;
		/**
		 * Coupling constructor, providing a name for the coupling. By default set to one, to change, use Coupling::set_value(.)
		 *
		 * @param string that names the coupling
		 */
		Coupling(std::string n): name(n) , value_num_r(1), value_num_i(0), value_den_r(1), value_den_i(0), 
				square_num_normalization(1), square_den_normalization(1), power_of_factor_out_I(0) {};
		std::string get_name() const { return name; };
		/**
		 * Templated method to get value of coupling. Specializations provided for C, CHP, CVHP and CGMP types in Coupling.cpp
		 */
		template <class T> T get_value() const { 
				if(value_num_i!=0||value_den_i!=0)
					std::cout<<"WARNING: coupling ("<<value_num_i<<","<<value_den_i<<") "<<name<<" evaluated to T(real num)/T(real den)!"<<std::endl; 
				return T(value_num_r)/T(value_den_r); }
		/**
		 * Method to get real part of den and num of coupling. 
		 */
		std::pair<int,int> get_real_info() const { 
				if(value_num_i!=0||value_den_i!=0)
					std::cout<<"WARNING: coupling ("<<value_num_i<<","<<value_den_i<<") "<<name<<" has complex piece!"<<std::endl; 
				return std::make_pair(value_num_r,value_den_r); }
		/**
		 * Method to set the values of the numerator and denominator of the coupling as well as overall normalization values
		 *
		 * @param int representing the real part of numerator of the coupling
		 * @param int representing the imaginary part of numerator of the coupling
		 * @param int representing the real part of the denominator of the coupling
		 * @param int representing the imaginary part of the denominator of the coupling
		 * @param int representing the square of the numerator of an overall normalization factor (defaulted to 1)
		 * @param int representing the square of the denominator of an overall normalization factor (defaulted to 1)
		 * @param int representing the powers of imaginary 'I' that have been factored out with this Coupling (defaulted to 0)
		 */
		void set_value(int i1,int i2,int i3,int i4,int sqrnum = 1,int sqrden = 1,int powsI = 0) {value_num_r=i1; value_num_i=i2; value_den_r=i3; value_den_i=i4;
				square_num_normalization=sqrnum; square_den_normalization=sqrden; power_of_factor_out_I=powsI;};
		/**
		 * Gives access to the square of the numerator used in normalization
		 */
		int get_square_num_normalization() const { return square_num_normalization; }
		/**
		 * Gives access to the square of the denominator used in normalization
		 */
		int get_square_den_normalization() const { return square_den_normalization; }
		/**
		 * Gives access to the powers of 'I' factored out in the coupling
		 */
		int get_powers_of_I() const { return power_of_factor_out_I; }
};

template <> C Coupling::get_value<C>() const;
#ifdef HIGH_PRECISION
template <> CHP Coupling::get_value<CHP>() const;
#endif
#ifdef VERY_HIGH_PRECISION
template <> CVHP Coupling::get_value<CVHP>() const;
#endif

#if INSTANTIATE_GMP
template <> CGMP Coupling::get_value<CGMP>() const;
#endif

// we implemented the ordering based on the name of the coupling
inline bool operator<(const Coupling& c1, const Coupling& c2){return c1.get_name()<c2.get_name();}
std::ostream& operator<<(std::ostream&, const Coupling&);

}
#endif	// COUPLING_H_
