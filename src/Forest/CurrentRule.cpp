/**
 * @file CurrentRule.cpp
 *
 * @date 15.4.2020
 *
 * @brief Implementation of CurrentRule
 */

#include "CurrentRule.h"
#include "Core/Utilities.h"
#include <iostream>

namespace Caravel {

void CurrentRule::fill(const std::vector<Particle>& in) {
    if (content.size() > 0 || type_name_list.size() > 0 || anti_type_name_list.size() > 0 || color_info.size() > 0 || spin_list.size() > 0) {
        std::cout << "CurrentRule::file(" << in << ") called when the containers were not empty! Did nothing!" << std::endl;
        return;
    }
    content = in;
    multiplicity = int(content.size());
    for (size_t ii = 0; ii < content.size(); ii++) {
        // fill info for all containers
        type_name_list.push_back(content[ii].get_type());
        anti_type_name_list.push_back(content[ii].get_anti_type());
        color_info.push_back(content[ii].is_colored());
        spin_list.push_back(content[ii].get_flipped_statistics());
    }
}

void CurrentRule::set_name(std::string in) { name = in; }

void CurrentRule::set_colorordered(bool in) { colororder = in; }

void CurrentRule::set_coupling(const Coupling& cin, int i) {
    auto locate = gs.find(cin.get_name());
    if (locate == gs.end()) { gs[cin.get_name()] = std::make_pair(cin, i); }
    else {
        std::cerr << "ERROR: trying to set coupling " << cin.get_name() << " in rule " << name << " which was already set!" << std::endl;
        std::exit(1);
    }
}

bool CurrentRule::is_color_ordered() const { return colororder; }

std::string CurrentRule::get_name() const { return name; }

int CurrentRule::get_rule_multiplicity() const { return multiplicity; }

int CurrentRule::get_power(const Coupling& coup) const {
    auto locate = gs.find(coup.get_name());
    if (locate == gs.end()) { return 0; }
    else {
        return (locate->second).second;
    }
}

const std::vector<Particle>& CurrentRule::get_particle_content() const { return content; }

const std::vector<ParticleType>& CurrentRule::get_current_type_list() const { return type_name_list; }

const std::vector<ParticleType>& CurrentRule::get_contract_type_list() const { return anti_type_name_list; }

const std::vector<bool>& CurrentRule::get_color_info_list() const { return color_info; }

const std::vector<ParticleStatistics>& CurrentRule::get_statistics_list() const { return spin_list; }

CurrentRule::~CurrentRule() {}

std::ostream& operator<<(std::ostream& s, const CurrentRule& r) {
    s << r.get_name() << "<";
    if (r.is_color_ordered())
        s << "color ordered"
          << ">:\n";
    else
        s << "not color ordered"
          << ">:\n";
    s << "\tcurrent list: " << r.get_current_type_list() << "\n";
    s << "\tcontract list: " << r.get_contract_type_list();
    return s;
}

} // namespace Caravel
