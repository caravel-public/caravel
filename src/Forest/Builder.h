/**
 * @file Builder.h 
 *
 * @date 24.11.2015 original source
 *
 * @brief Header file for all class templates associated with Builder
 *
 * Builder is an engine to transform an abstract Forest into computable structures for trees and cuts with generic types.
 * Here we define all class templates that are associated with Builder and template implementations are added at the
 * bottom through the files Builder_utils.hpp and Builder.hpp.
 *
 * Instantiation definitions, for explicit compilation at library building time, are included in Builder.cpp
 *
*/
#ifndef BUILDER_H_
#define BUILDER_H_

#include <vector>
#include <string>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <functional>
#include <set>
#include <map>

#include "Core/Debug.h"
#include "Core/momD_conf.h"
#include "Core/Particle.h"
#include "Core/type_traits_extra.h"
#include "Forest/Forest.h"

#include "Core/typedefs.h"
#ifdef USE_FINITE_FIELDS
#include "Core/extended_t.h"
#endif


namespace Caravel {

/**
 * Pure abstract base class for all CurrentWorker classes (class templates)
 */
class CurrentWorker_Base {
#ifdef DEBUG_ON
	protected:
		std::string name;
#endif
	public:
		virtual ~CurrentWorker_Base() = 0;
#ifdef DEBUG_ON
		std::string get_name() const {return name;};
#endif
};

/**
 * A struct to select current type for Builder_fixed_Ds dependeing on the numerical type
 * of the cut. This type can differ from T as we need for fermionic currents for example.
 */
template <typename T, typename Enable = void> struct Current_Type_Selector{};

template <typename T> struct Current_Type_Selector<T,std::enable_if_t<is_exact<T>::value>>{
#ifdef USE_EXTENDED_T
    typedef extended_t<T> Tcur;
#else
    typedef T Tcur;
#endif
};
template <typename T> struct Current_Type_Selector<T,std::enable_if_t<!is_exact<T>::value>>{
    typedef T Tcur;
};
//! A shortcut of the above
template <typename T> using CTS = Current_Type_Selector<T>;


// Forward declaration
template <class T> struct BuilderContainer;
// type alias to handle computing function of Propagator: parameters: vector<T> representing the input current, T where propagator (e.g. 1/p^2) shall be stored, 
// momentum running in the propagator, BuilderContainer (containing masses etc)
template <class T,typename Tcur,size_t D> using ComputeProp = std::function<std::vector<Tcur>(const std::vector<Tcur>&, T&, const momentumD<T,D>&, const BuilderContainer<T>&)>;
// Template function to resolve needed ComputeProp
class CurrentBuild;
template <class T,typename Tcur,size_t D> ComputeProp<T,Tcur,D> get_compute_propagator(const CurrentBuild*, const Model&);

class CurrentBuild;

/**
 * Class to resolve computation of propagators
 * It should allow to compute massive/massless propagators and also drop propagators for cutted lines
 * it also takes of computing and storing momentum flowing through propagator
 */
template <class T,size_t D> class Propagator {
        public:
                typedef typename CTS<T>::Tcur Tcur;
	protected:
		T width;	/**< width parameter of propagator */
		momD<T,D> momentum;	/**< Momentum flowing through propagator */
		size_t nmomenta, midx;
		std::vector<momD<T,D> const *> momenta;	/**< store associated momenta */
		T prop;		/**< store (scalar) propagator */
		ComputeProp<T,Tcur,D> compute_prop;	/**< Rule to compute propagator: it processes the incoming current passed through Propagator::compute(.) and sets the scalar propagator stored in the member 'prop' */
	public:
		Propagator() = default;
		/**
		 * Main constructor
		 *
		 * @param Pointer to associated CurrentBuild
		 * @param momD_conf from where the momenta associated with *this will be extracted
		 * @param Model of the theory being used
		 */
		Propagator(CurrentBuild*, const momD_conf<T,D>&, const Model&);
		/**
		 * Returns the scalar propagator associated with this (e.g. 1/p^2)
		 */
		T get_scalar_propagator() const;
		/**
		 * Returns total momentum flowing in Current
		 */
		const momD<T,D> * get_momentum() const;
		/** 
		 * To compute the propagator
		 *
		 * @param Receives the corresponding current to be added the propagator
		 * @param BuilderContainer required for evaluating compute_prop
		 * @return The current including the propagator (for a vector current in Feynman gauge, this is only a multiplication by 1/p^2, for a fermion it also should include manipulation of the numerator)
		 */
		std::vector<Tcur> compute(const std::vector<Tcur>&, const BuilderContainer<T>&);
};

template <class T> class CurrentContainer;
template <class T> std::ostream& operator<<(std::ostream&,const CurrentContainer<T>&);

/**
 * Class template to represent all types of (off-shell/on-shell) currents that are handled by Builder_fixed_Ds. 
 *
 * The class 'T' is a type to be used to compute the different currents (either floating-point types, real or complex, finite fields, etc)
 *
 * The two size_t members, Emin ('entry minimum') and Emax ('entry maximum'), help to define the number of entries in the
 * corresponding 'T' container (so far only interpreted as a 1-D container). It is assumed that it runs in the range [Emin,Emax). For 
 * example, a scalar current will have Emin=0 and Emax=1, while a vector current will have at least Emin=0 and at most Emax=Ds (where Ds 
 * is the dimension of the internal spin space)
 */
template <class T> class CurrentContainer {
		//size_t Emin;	[>*< Lower closed bound of container  <]
		//size_t Emax;	[>*< Upper open bound of container  <]
		unsigned size;	/**< For ease of handling we store the size of this array  */
		ParticleStatistics spin;	/**< Tracks spin-statistics associated with *this current */
		std::vector<T> container;	/**< Contiguous array that contains all entries of the current */
	public:
		friend std::ostream& operator<< <>(std::ostream& o,const CurrentContainer<T>& cc);
		/**
		 * Default constructor
		 */
		CurrentContainer();
		/**
		 * Main constructor, defines size of container (and spin-statistics associated with *this)
		 *
		 * @param ParticleStatistics representing the spin-statistics of *this
		 * @param size_t representing the dimensionality of this current
		 */
		CurrentContainer(ParticleStatistics in_spin, ParticleFlavor f, unsigned Dim);
                /**
		 * This method gives const access to container of *this
		 */
		const std::vector<T>& get_current() const { return container; }
		/**
		 * Method which allows to update the container of *this. No bound check will be performed (unless in debug mode)
		 */
		void set(const std::vector<T>& in);
		/**
		 * Method to add the passed vector to *this current. No bound check will be performed (unless in debug mode)
		 */
		void add(const std::vector<T>& in);
		/**
		 * Method to multiply by a scalar *this current
		 */
		void scalar_multiply(const T& in);
};

class CurrentBasic;

// Forward declaration
template <class T,size_t D> class CurrentWorker_Template_Base;
// To handle building rules of CurrentWorker_Template_Base
template <class T,typename Tcur,size_t D> using CombineCurrent = std::function<std::vector<Tcur>(const std::vector<momentumD<T,D> const*>&, const std::vector<CurrentWorker_Template_Base<T,D> const*>&, const BuilderContainer<T>&)>;
// Template function to resolve needed CombineCurrent
template <class T,typename Tcur,size_t D> CombineCurrent<T,Tcur,D> get_combine_rule(CurrentBasic*);
// Similar but for external currents
// Arguments are: momD_conf, mom_index, ref_index, BuilderContainer (containing masses etc)
template <class T,typename Tcur,size_t D> using ExternalCurrent = std::function<std::vector<Tcur>(const momD_conf<T,D>&, const size_t&, const size_t&, const BuilderContainer<T>&)>;
template <class T,typename Tcur,size_t D> ExternalCurrent<T,Tcur,D> get_external_rule(const Particle&,const SingleState&);

// type alias to handle final contraction of currents (only active for top-level currents)
template <class T,typename Tcur,size_t D> using CurrentContraction = std::function<T(const std::vector<Tcur>&, const std::vector<Tcur>&, const BuilderContainer<T>&)>;
// To fix necessary CurrentContraction
template <class T,typename Tcur,size_t D> CurrentContraction<T,Tcur,D> get_final_contraction_rule(Current*);

/**
 * Base class for all currents employed by Builder_fixed_Ds. It is the main container of current's information through the
 * member CurrentContainer<T>
 */
template <class T,size_t D> class CurrentWorker_Template_Base {
	protected:
                typedef typename Current_Type_Selector<T>::Tcur Tcur;
		CurrentWorker_Template_Base() = default;
		CurrentWorker_Template_Base(ParticleStatistics spin, ParticleFlavor f): current(spin,f,D) {}
	public:
		virtual void compute(const BuilderContainer<T>&) = 0;
		/** 
		 * Returns total momentum flowing in Current
		 */
		virtual const momD<T,D> * get_momentum() const = 0;
		CurrentContainer<Tcur> current;	/**< Container for the current */
		const std::vector<Tcur>& get_current() const;	/**< Give access to the container */
		CurrentContraction<T,Tcur,D> final_contraction_rule;	/**< Store information of how *this current contracts to give a tree/cut (nullptr unless top-level current) */
		virtual ~CurrentWorker_Template_Base() = 0;
};

template <class T,size_t D> class SubCurrentWorker;
class Current;
class PhysicalState;

/**
 * CurrentWorker is the main BG (off-shell) current for computational purposes.
 * It is not inherited from Current to keep its interface minimal.
 * It is a template to allow multiprecision and D dimensional trees/cuts.
 * The corresponding objects should be built after having the full Forest.
 */
template <class T,size_t D> class CurrentWorker : public CurrentWorker_Base, public CurrentWorker_Template_Base<T,D> {
	protected:
		// number of subcurrents for associated CurrentBuild, 0 for CurrentBasic (subcurrent) and -1 for External
		int nsubcurrents;
		// store the associated subcurrents
		std::vector<SubCurrentWorker<T,D>> sub_currents;
		// Propagator of cut
		Propagator<T,D> propagator;
	public:
		// Default constructor
		CurrentWorker() = default;
		// Constructor for off-shell currents
		CurrentWorker(CurrentBuild*,std::unordered_map<Current *,std::vector<CurrentWorker_Template_Base<T,D>* >>&,momD_conf<T,D>&, const Model&);
		// for when a given set of states is needed
		CurrentWorker(CurrentBuild*,std::unordered_map<Current *,std::vector<CurrentWorker_Template_Base<T,D>* >>&,momD_conf<T,D>&,const PhysicalState&, const Model&);
	// As inherited from CurrentWorker_Template_Base
		// to add all subcurrents
		void compute(const BuilderContainer<T>&);
		const momD<T,D> * get_momentum() const;
};

class CurrentExternal;

/**
 * CurrentWorkerExternal is the main BG (on-shell) external current for computational purposes.
 * It is not inherited from Current to keep its interface minimal.
 * It is a template to allow multiprecision and D dimensional trees/cuts.
 * The corresponding objects should be built after having the full Forest.
 */
template <class T,size_t D> class CurrentWorkerExternal : public CurrentWorker_Base, public CurrentWorker_Template_Base<T,D> {
        public:
                typedef typename Current_Type_Selector<T>::Tcur Tcur;
	private:
		//static std::vector<std::string> collectall;
		// momentum configuration reference
		const momD_conf<T,D>* momconf;
		// identifier for external momentum
		size_t k;
		// identifier for reference momentum
		size_t q;
		// identifier for mass index
		size_t midx;
	protected:
		// rule/function to build external currents
		ExternalCurrent<T,Tcur,D> external_rule;
	public:
		// Default
		CurrentWorkerExternal() = default;
		// Constructor for external currents
		CurrentWorkerExternal(CurrentExternal*,momD_conf<T,D>&,size_t&,size_t&,size_t&);
		// Constructor for external currents with explicit state
		CurrentWorkerExternal(CurrentExternal*,momD_conf<T,D>&,size_t&,size_t&,size_t&,const SingleState&);
	// As inherited from CurrentWorker_Template_Base
		// to compute external currents (argument passes parameter info from owner Builder)
		void compute(const BuilderContainer<T>&);
		// to compute external currents passing explicitly a reference vector (argument passes parameter info from owner Builder)
		void compute_with_ref(size_t,bool&,const BuilderContainer<T>&);
		const momD<T,D> * get_momentum() const;
};

/**
 * SubCurrentWorker is the main BG subcurrent for computational purposes.
 * It is not inherited from Current to keep its interface minimal.
 * It is a template to allow multiprecision and D dimensional trees/cuts.
 * The corresponding objects should be built after having the full Forest.
 */
template <class T,size_t D> class SubCurrentWorker : public CurrentWorker_Base, public CurrentWorker_Template_Base<T,D> {
	protected:
                typedef typename Current_Type_Selector<T>::Tcur Tcur;
		// number of subcurrents for associated CurrentBuild, 0 for CurrentBasic (subcurrent) and -1 for External
		int nparents;
		// store the parent currents
		std::vector<CurrentWorker_Template_Base<T,D> const *> parents;
		// store associated momenta
		std::vector<momD<T,D> const *> momenta;
		// rule/function to build subcurrent
		CombineCurrent<T,Tcur,D> combine_rule;
	public:
		// Default constructor
		SubCurrentWorker() = default;
		// Constructor for off-shell subcurrents
		SubCurrentWorker(CurrentBasic*,std::unordered_map<Current *,std::vector<CurrentWorker_Template_Base<T,D>* >>&);
		// for when a given set of states is needed
		SubCurrentWorker(CurrentBasic*,std::unordered_map<Current *,std::vector<CurrentWorker_Template_Base<T,D>* >>&,const PhysicalState&);
	// As inherited from CurrentWorker_Template_Base
		// to compute the subcurrent (argument passes parameter info from owner Builder)
		void compute(const BuilderContainer<T>&);
		const momD<T,D> * get_momentum() const;
};

class Builder_normalizations_base {
  public:
    /**
     * Overall normalization constants for trees and cuts are computed once at construction time, then resused over phase space.
     * But if necessary, for example when changing the calculation field (e.g. switching the cardinality of the finite
     * field used) this methods allows for an update (it is not the same 1/2 in Z_3 as in Z_5).
     */
    virtual void update_normalizations() = 0;

    // Disallow delete from a pointer to this base class
  protected:
    ~Builder_normalizations_base() {};
};

template <class T,size_t D> class Builder_Ds_base : public Builder_normalizations_base {
	public:
		// To fully compute all tree currents from lower to upper level
		virtual void compute() = 0;
		// To fully compute all cut_i currents from lower to upper level
		virtual void compute_cut(int) = 0;
		// contract tree upper level currents
		virtual void contract() = 0;
		// contract cut upper level currents
		virtual void contract_cut(int) = 0;
		// get tree related to nth process added
		virtual T get_tree(int) const = 0;
		// get cut (summed over physical states) related to nth cut added
		virtual T get_cut(int,size_t) const = 0;
		// get access to the momD_conf
		virtual const momD_conf<T,D>& get_phys_momD_conf() const = 0;
		// get access to the momD_conf
		virtual const momD_conf<T,D>& get_loop_momD_conf(
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				int cutindex
#endif
			) const = 0;
		// size of physical space
		virtual size_t get_phys_size() const = 0;
		// to set physical space
		virtual void set_p(const momD<T,4>&,size_t) = 0;
		// to set ith-loop momenta and its negative
		virtual void set_loop(const momD<T,D>&,size_t
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, int
#endif
			) = 0;
		virtual void set_loop(const OnShellPoint<T,D>&
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, int
#endif
			) = 0;
		// to insert a momentum into nth location of the Builder's mom_conf's (checks that it does not overwrites phys and loop momneta)
		virtual void insert_extra_4D_mom(const momD<T,4>&,size_t) = 0;

		// takes care of updating kinematical information contained in Builder's BuilderContainer
                virtual void update_builder_container() = 0;

		// Destructor
		virtual ~Builder_Ds_base() = 0;
};

struct dimension_register{
	size_t dim;
	std::vector<std::pair<int,std::vector<int>>> cut_tracker;
};

/**
 * A class to store CurrentWoker's needed by Builder
 */
template <class T,size_t Ds> class WorkerContainer {
		bool size_given;
		std::vector<CurrentWorkerExternal<T,Ds>> level_one_currents;
		std::vector<std::vector<CurrentWorker<T,Ds>>> upper_level_current;
	public:
		WorkerContainer(): size_given(false) {};
};

/**
 * Simple struct representing a normalization factor of the form (num/den)^exponent
 */
struct SingleNormalizationFactor {
	int num;
	int den;
	int exponent;
        std::string factor{""};
};

std::ostream& operator<<(std::ostream&, SingleNormalizationFactor f);

/**
 * This structure contains all normalization information associated to each entry of the associated Builder
 */
class EntryNormalization{
		/**
		 * It is supposed that the normalization factor is computed as:
		 *
		 * s1 * s2 * ...
		 *
		 * where si are the SingleNormalizationFactor associated with corresponding entry in the vector
		 */
		std::vector<SingleNormalizationFactor> real_normalization_factors;
		/**
		 * These are purely imaginary normalization factors, so they are computed as:
		 *
		 * (I*s1) * (I*s2) * ...
		 *
		 * where si are the SingleNormalizationFactor associated with corresponding entry in the vector
		 */
		std::vector<SingleNormalizationFactor> imaginary_normalization_factors;
	public:
		EntryNormalization() = default;
		/**
		 * A method to add a normalization factor to *this, which is purely real
		 *
		 * @param int representing the numerator of the factor
		 * @param int representing the denominator of the factor
		 * @param int representing the exponent of the factor
		 */
		void add_real_normalization_entry(int,int,int);
		/**
		 * A method to add a normalization factor to *this, which is purely imaginary
		 *
		 * @param int representing the numerator of the factor
		 * @param int representing the denominator of the factor
		 * @param int representing the exponent of the factor
		 */
		void add_imaginary_normalization_entry(int,int,int);
		/**
		 * Sets the corresponding normalization factor in the reference passed. This template works for complex fields
		 *
		 * @param a reference to a T variable that will take the corresponding normalization factor
		 */
                template <class R> void set_normalization_factor(std::complex<R>& getnorm);
                /**
		 * Sets the corresponding normalization factor in the reference passed. This template works for real fields and throws a warning if complex
		 * normalization factors are present in imaginary_normalization_factors
		 *
		 * @param a reference to a T variable that will take the corresponding normalization factor
		 */
                template <class R, class RR = R> void set_normalization_factor(R& getnorm, typename std::enable_if<!is_rational<RR>::value>::type* = nullptr);
#ifdef INSTANTIATE_RATIONAL
                template <class R, class RR = R> void set_normalization_factor(R& getnorm, typename std::enable_if<is_rational<RR>::value>::type* = nullptr);
#endif
		const auto& get_real_normalization_factors() const { return real_normalization_factors; }
		const auto& get_imaginary_normalization_factors() const { return imaginary_normalization_factors; }
};
/**
 * Struct with potential information that might be necessary to pass around to key kinematic functions like
 * ExternalCurrent, CombineCurrent, ComputeProp or CurrentContraction.
 *
 * Templated only on the numeric type 'T'
 *
 * When first introduced, it allowed simpler handling of particle's masses in Builder
 */
template <class T> struct BuilderContainer {
    std::vector<T> mass;    /**< Vector that holds all masses, with entry 'i' associated to ParticleMass::get_mass_index() */
};

class Forest;

/**
 * Builder_fixed_Ds class creates the army of CurrentWorker's to compute trees/cuts
 * class T imposes level of complex arithmetics: either C, CHP, CVHP or CGMP (the latter if included)
 */
template <class T,size_t D,size_t Ds,typename Enable = void> class Builder_fixed_Ds;

template <class T,size_t D,size_t Ds> class Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>> final : public Builder_Ds_base<T,D> {
        public:
            typedef typename Current_Type_Selector<T>::Tcur Tcur;
            typedef CurrentWorker_Template_Base<T,Ds> current_type_worker;
            typedef CurrentWorkerExternal<T,Ds> current_external_type_worker;
            typedef CurrentWorker<T,Ds> current_type;

	protected:

		std::vector<current_type_worker*> tree_workers;	/**< To store pointers to all tree_workers */
		std::vector<std::vector<current_type_worker*>> cut_workers;	/**< To store pointers to all cut_workers */
		std::vector<std::vector<current_external_type_worker*>> cut_workers_external;	/**< To store pointers to all cut_workers externals */
		std::vector<current_external_type_worker> external_current_owner;	/**< Container of all current_external_type_worker */
		std::vector<std::vector<current_type>> current_owner;	/**< Container of all current_type */
		std::vector<T> fullcontract;	/**< explicit double values of contractions of upper level tree currents with wave functions */
		std::unordered_map<int,EntryNormalization> tree_normalization_info;	/**< Here we keep an unorder map with all necessary info for normalization of each tree. Used for updating info in tree_overall_factors */
		std::unordered_map<int,T> tree_overall_factors;	/**< An unordered map that contains explicit overall factors (from couplings and external states) for each tree is set with contract() */
		std::unordered_map<int,std::vector<EntryNormalization>> cut_normalization_info;	/**< Here we keep an unorder map with all necessary info for normalization of each cut. Used for updating info in cut_overall_factors */
		std::unordered_map<int,std::vector<T>> cut_overall_factors;	/**< An unordered map that contains explicit overall factors (from couplings and external states) for each cut set with contract_cut(int) */
		std::vector<size_t> dimensions;	/**< Copies dimensions included while closing forest */
		/**
		 * Explicit double values of contractions of upper level currents with wave functions.
		 * One entry per dimension, as stored in dimensions
		 */
		std::vector<std::vector<T>> cutsum;
		std::vector<current_type_worker*> FinalExternal;	/**< External tree currents for final particles in associated Processes */
		std::vector<current_type_worker*> FinalCurrentContract;	/**< Associated off-shell tree current */
		int nprocs;	/**< to count number of processes */
		int ntreescuts;	/**< to count all trees/cuts to be computed */
		int nlevels;	/**< To count the number of levels */
		int totaltreecurrents;	/**< Total # of tree currents */
		int ncutsum;	/**< size of cutsum */
		std::vector<int> totalcutcurrents;	/**< Total # of cut currents (each entry for each register) */
		std::vector<int> totalcutcurrents_level_1;	/**< Total # of cut level 1 currents (each entry for each register) */
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		std::unordered_map<int,size_t> cut_index_to_momD_conf_map;	/**< Map from index of cut to corresponding momD_conf in localmom and localmomDs */
		std::vector<momD_conf<T,D>> localmom;	/**< momentumD_configuration's */
		std::vector<momD_conf<T,Ds>> localmomDs;	/**< Ds-dimensional copy of D-dimensional momentumD_configuration's */
#else
		momD_conf<T,D> localmom;	/**< momentumD_configuration */
		momD_conf<T,Ds> localmomDs;	/**< Ds-dimensional copy of D-dimensional momentumD_configuration */
#endif
		size_t extra_momenta_space;	/**< to allow extra setting of few momenta */
		partial_momD_conf<T,D> physical_momD_conf;	/**< for safe access to physical momenta */
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		std::vector<partial_momD_conf<T,D>> loop_momD_conf;	/**< for safe access to loop momenta */
#else
		partial_momD_conf<T,D> loop_momD_conf;	/**< for safe access to loop momenta */
#endif
		/**
		 * Contains the mappings to get full cuts as sums over all possible internal states.
		 * An entry for each dim included in dimensions
		 */
		std::vector<dimension_register> process_register;
		std::vector<int> tree_location;	/**< map from tree indices to corresponding entry in process_register */
		std::vector<int> cut_location;	/**< map from cut indices to corresponding entry in process_register */
		std::vector<int> ref_index_for_cut;	/**< for each cut in cut_location it stores the default value for ref vector use for phys state of corresponding loop particles */
		std::vector<size_t> loop_orders;	/**< to know which number of loops are explored on the currents */
		size_t max_n_loops;
		size_t physical_PS_multiplicity;	/**< what is the size of the physical PS (with convention 0->physical_PS_multiplicity) */
		BuilderContainer<T> bcontainer;	/**< Tracks parameters that might be required in computation (like masses of particles) */
	public:
		// Construct from a Forest
                [[deprecated("momD_conf is not required at construction time anymore")]]
		explicit Builder_fixed_Ds(Forest*,const momD_conf<T,4>&);
                /**
                 * Construct builder from Forest
                 * @param forest
                 * @param multiplicity of the external phase space
                 */
		explicit Builder_fixed_Ds(Forest*,size_t);
		// To fully compute all tree currents from lower to upper level
		void compute() override;
		// To fully compute all cut_i currents from lower to upper level
		void compute_cut(int) override;
		// contract tree upper level currents
		void contract() override;
		// contract cut upper level currents
		void contract_cut(int) override;
		// get tree related to nth process added
		T get_tree(int) const override;
		// get cut (summed over physical states) related to nth cut added
		T get_cut(int,size_t) const override;
		// get access to the momD_conf
		const momD_conf<T,D>& get_phys_momD_conf() const override;
		// get access to the momD_conf
		const momD_conf<T,D>& get_loop_momD_conf(
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				int cutindex
#endif
			) const override;
		// size of physical space
		size_t get_phys_size() const override;
		// to set physical space
		void set_p(const momD<T,4>&,size_t) override;
		/**
		 * Method to set (change) the whole external phase-space point in one go.
		 */
		void set_p(const momD_conf<T,4>&);
                template <size_t DD, typename = typename std::enable_if_t<(DD>4) && (DD<=D)>> void set_p (const momD_conf<T,DD>&);
		// to set ith-loop momenta and its negative
		void set_loop(const momD<T,D>&,size_t
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, int
#endif
			) override;
		void set_loop(const OnShellPoint<T,D>&
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, int
#endif
			) override;
		// to insert a momentum into nth location of the Builder_fixed_Ds's mom_conf's (checks that it does not overwrites phys and loop momneta)
		void insert_extra_4D_mom(const momD<T,4>&,size_t) override;

		// takes care of updating kinematical information contained in Builder's BuilderContainer
                void update_builder_container() override;
		/**
		 * Overall normalization constants for trees and cuts are computed once at construction time, then resused over phase space. 
		 * But if necessary, for example when changing the calculation field (e.g. switching the cardinality of the finite
		 * field used) this methods allows for an update (it is not the same 1/2 in Z_2 as in Z_3).
		 */
		void update_normalizations() override;

                ~Builder_fixed_Ds();
	private:
		/**
		 * This method takes care of constructing the overall normalization information that is associated with the Current pointed add with the parameters.
		 * This overall normalization information is build out of:
		 *
		 * 1) The overall normalization factor associated with the external states that build corresponding Current
		 * 2) The overall normalization factor that is associated with each coupling included in forest->model (using the method Coupling::get_value<T>())
		 *
		 * @param Forest* pointer to the Forest that contains the Current's used in *this
		 * @param int which points to the corresponding entry in forest->all_processes that is being considered (zero based)
		 * @return EntryNormalization associated to the overall normalization factor in associated Current. In case of finite fields, be careful that some sqrt's might be dropped (with a warning). In real instantiations of Builder, no imaginary 'i' is included
		 */
                std::vector<EntryNormalization> get_overall_normalization_info(Forest*,int);

                template <typename TT=T> std::enable_if_t<is_finite_field<TT>::value> register_for_cardinality_upate();
                template <typename TT=T> std::enable_if_t<!is_finite_field<TT>::value> register_for_cardinality_upate() {}
                template <typename TT=T> std::enable_if_t<is_finite_field<TT>::value> un_register_for_cardinality_upate();
                template <typename TT=T> std::enable_if_t<!is_finite_field<TT>::value> un_register_for_cardinality_upate() {}
		/**
		 * This allows to show information tree by tree, cut by cut, of potentially missing factors in real computations
		 */
		void normalization_report() const;
};

template <class T,size_t D,size_t Ds> class Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds<D)>> : public Builder_Ds_base<T,D> {
    public:
        typedef typename Current_Type_Selector<T>::Tcur Tcur;
        explicit Builder_fixed_Ds(Forest*,size_t){throw std::logic_error("Builder with Ds<D should never be used!");};
        explicit Builder_fixed_Ds(Forest*,const momD_conf<T,4>&){throw std::logic_error("Builder with Ds<D should never be used!");};
        void compute(){};
        void compute_cut(int){};
        void contract(){};
        void contract_cut(int){};
        T get_tree(int) const{return T(0);};
        T get_cut(int,size_t) const{return T(0);};
        const momD_conf<T,D>& get_phys_momD_conf() const{throw;}
        const momD_conf<T,D>& get_loop_momD_conf() const{throw;}
        size_t get_phys_size() const{return 0;};
        void set_p(const momD<T,4>&,size_t){};
        void set_loop(const momD<T,D>&,size_t){};
        void set_loop(const OnShellPoint<T,D>&){};
        void insert_extra_4D_mom(const momD<T,4>&,size_t){};
        void update_builder_container(){};
        void update_normalizations(){};
};

template<typename T, size_t D, size_t Ds>
void  Builder_fixed_Ds<T,D,Ds,typename std::enable_if_t<(Ds>=D)>>::set_loop(const OnShellPoint<T,D>& p
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, int cutindex
#endif
	){
   for (size_t i = 0; i < p.size(); i++){
     set_loop(p.at(i),i+1
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		, cutindex
#endif
	);
   }
}

/**
 * Builder class template handles a closed Forest for computations of trees and cuts in a type T
 * employing a dimension D for internal loop momenta (commonly 4 for tree-level amplitudes, 5 for 1-loop
 * calculations and 6 for two loops).
 */
template <class T,size_t D> class Builder {
		std::vector<size_t> dimensions;	/**< Stores the dimensions included when the parent Forest was closed */
		std::unordered_map<int,std::vector<size_t>> cut_builderDs_map_for_compute;	/**< An unordered map from cut_i to the corresponding vector of associated dimensions */
		std::unordered_map<std::pair<int,size_t>,size_t> cut_builderDs_map_for_get;	/**< An unordered map from cut_i(Ds) to the corresponding builder_ds_j (<i,Ds> -> builder_ds_container[j]) */
		std::map<int,std::set<size_t>> cut_dimensions;	/**< Contains Ds values for which this builder can compute cuts */
		std::vector<Builder_Ds_base<T,D>*> builder_ds_container;	/**< Stores builders to which calls are forwarded */

		momD_conf<T,4> external_momenta;	/**< Copy of external momenta */
	public:
                /**
		 * Unique constructor. Builder transforms a Forest into a kinematically computable object for getting trees and multi-loop cuts
                 * @param forest
                 * @param multiplicity of the external phase space
                 */
		explicit Builder(Forest*,size_t);

                [[deprecated("momD_conf is not required at construction time anymore")]]
		explicit Builder(Forest*,const momD_conf<T,4>&);
		/**
		 * Computes all currents built from only external states, from level 1 to top ones.
		 * Previous step to contract(), get_tree(.)'s and compute_cut(.)'s
		 */
		void compute();
		/**
		 * Makes contractions to obtain trees.
		 * Should follow a compute() call. Previous step to get_tree(.)'s and compute_cut(.)'s
		 */
		void contract();
		/**
		 * For a given cut added to the parent Forest, this method allows to compute all related currents and 
		 * making contractions and sums over Ds-dimensional internal degrees of freedom to produce cuts
		 *
		 * @param int representing the cut as returned by forest->add_cut(.). If cuts where merged with forest->join_cuts(.), this int should be the smallest one in the list of merged cuts
		 * @param size_t representing the dimension in which the cut will be computed. Defaults to zero, which represents the highest dimension in which the Forest was closed
		 */
		void compute_cut(int /* ,size_t = 0 */);
		void contract_cut(int /* ,size_t = 0 */);
		/**
		 * After compute() has been called, user gets access to corresponding trees with this method.
		 *
		 * @param int representing the added tree as returned by forest->add_process(.)
		 */
		T get_tree(int) const;
		/**
		 * Allows access of cuts computed after compute_cut(.), summed over all physical states corresponding to the dimension requested
		 *
		 * @param int representing the cut as returned by forest->add_cut(.)
		 * @param size_t representing the dimension for the request. zero represents the highest dimension in which the Forest was closed
		 */
		T get_cut(int,size_t) const;
                /**
                 * As above, but return cuts in all Ds which this builder evaluates
                 */
                std::vector<T> get_cut(int) const;
		/**
		 * Gives a const reference access to the physical (external) momentum configuration (embedded into D dimensions)
		 */
		const momD_conf<T,D>& get_phys_momD_conf() const;
		/**
		 * Gives a const reference access to the stored internal loop momenta in D dimensions
		 */
		const momD_conf<T,D>& get_loop_momD_conf(
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				int cutindex
#endif
			) const;
		/**
		 * Returns a size_t representing the amount of external momenta passed when Builder was constructed
		 */
		size_t get_phys_size() const;
		/**
		 * Method to set (change) the external phase-space point (momentum configuration)
		 */
		void set_p(const momD<T,4>&,size_t);

		/**
		 * Method to set (change) the whole external phase-space point in one go.
		 */
		void set_p(const momD_conf<T,4>&);

		/**
		 * Method to set the loop momenta stored in *this
		 *
		 * @param input momentum
		 * @param size_t to tag which loop momentum (1 based, e.g. 1-loop calculations this is always 1)
		 * @param integer associated to cut under study, only included when openMP enable and when the preprocessor variable _BUILDER_THREAD_SAFE is set to 1
		 */
		void set_loop(const momD<T,D>&,size_t
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, int
#endif
			);
		/**
		 * Method to set the loop momenta stored in *this
		 */
		void set_loop(const OnShellPoint<T,D>&
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, int = 0
#endif
			);
		/**
		 * Inserts a momentum into nth location of the Builder's mom_conf's (checks that it does not overwrites phys and loop momenta)
		 */
		void insert_extra_4D_mom(const momD<T,4>&,size_t);

		/**
		 * Takes care of updating kinematical information contained in Builder's BuilderContainer (e.g. mass information)
		 */
                void update_builder_container();

		/**
		 * Returns the set of state dimensions that the builder has been constructed with
		 */
		 std::vector<size_t> get_state_dimensions() const;
                /**
                 *  Obtain a vector of Ds for which this builder can compute given cut
                 *  @param cut id
                 */
                std::set<size_t> get_cut_dimensions(int) const;

		/**
		 * Overall normalization constants for trees and cuts are computed once at construction time, then resused over phase space. 
		 * But if necessary, for example when changing the calculation field (e.g. switching the cardinality of the finite
		 * field used) this methods allows for an update (it is not the same 1/2 in Z_2 as in Z_3).
		 */
		void update_normalizations();

		// Destructor
		~Builder();
};

}

// IMPLEMENTATIONS
#include "Builder_report.hpp"
#include "Builder_utils.hpp"
#include "Builder.hpp"

#endif	// BUILDER_H_
