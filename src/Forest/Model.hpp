/**
 * @file Model.hpp
 *
 * @date 12.6.2017
 *
 * @brief Implementation of the 'distributing' template functions get_external_rule, get_combine_rule,
 * get_compute_propagator and get_final_contraction_rule
 *
 */

#include "Core/Debug.h"
#include "Core/Particle.h"
#include "Forest/Builder.h"
#include <iostream>
#include "Core/typedefs.h"
#include "Core/settings.h"
#include "Core/type_traits_extra.h"
#include <cassert>
#include <map>
#include <string>
#include <tuple>
#include <utility>

namespace Caravel {
namespace Model_Helper {
/*********************************************************************************************************
 * VECTOR BOSON STATES
 *********************************************************************************************************/
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_vector_boson_states_impl() {
    static_assert(Ds >= 4, "Dimensional mismatch");
    std::vector<std::pair<SingleState, SingleState>> states;

    using namespace settings::general;
    if (cut_propagator_numerators.value == cut_states::states) {
        switch (Ds) {
            default: // insert trivial states for all Ds>6
                for (unsigned i = 7; i <= Ds; i++) {
                    states.emplace_back(SingleState("uD" + std::to_string(i - 2)), SingleState("ucD" + std::to_string(i - 2)));
                }
                __FALLTHROUGH;
            case 6: states.emplace_back(SingleState("tD"), SingleState("tcD")); __FALLTHROUGH;
            case 5:
                states.emplace_back(SingleState("mD"), SingleState("pD"));
                states.emplace_back(SingleState("pD"), SingleState("mD"));
                states.emplace_back(SingleState("sD"), SingleState("scD"));
                break;
        };
    }
    else if (cut_propagator_numerators.value == cut_states::projective) {
        for (unsigned i = 1; i <= Ds; i++) { states.emplace_back(SingleState("mu" + std::to_string(i)), SingleState("cont" + std::to_string(i))); }
    }

    return states;
}
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_massive_vector_boson_states_impl() {
    static_assert(Ds >= 4, "Dimensional mismatch");
    std::vector<std::pair<SingleState, SingleState>> states;

    using namespace settings::general;
    if (cut_propagator_numerators.value == cut_states::states) {
        switch (Ds) {
            default: { // insert trivial states for all Ds>6
                for (unsigned i = 6; i <= Ds - 1; i++) states.emplace_back(SingleState("uD" + std::to_string(i)), SingleState("ucD" + std::to_string(i)));
                __FALLTHROUGH;
            }
            case 6: {
                states.emplace_back(SingleState("tD"), SingleState("tcD"));
                __FALLTHROUGH;
            }
            case 5: {
                states.emplace_back(SingleState("mD"), SingleState("mcD"));
                states.emplace_back(SingleState("pD"), SingleState("pcD"));
                states.emplace_back(SingleState("sD"), SingleState("scD"));
                states.emplace_back(SingleState("vD"), SingleState("vcD"));
                break;
            }
        };
    }
    else if (cut_propagator_numerators.value == cut_states::projective) {
        std::cout << "Projective states for massive vector bosons not implemented!" << std::endl;
	std::exit(1);
        for (unsigned i = 1; i <= Ds; i++) { states.emplace_back(SingleState("mu" + std::to_string(i)), SingleState("cont" + std::to_string(i))); }
    }

    return states;
}
// DEFINE vector boson states
template <unsigned Ds> void add_vector_boson_states(const ParticleType& p, ParticleStates& particle_states) {
    if (p.is_massless())
        particle_states.add_state(p, Ds, add_vector_boson_states_impl<Ds>());
    else
        particle_states.add_state(p, Ds, add_massive_vector_boson_states_impl<Ds>());
}

template <unsigned Ds, unsigned... rest, typename T> void add_vector_boson_states(const ParticleType& p, ParticleStates& particle_states) {
    if (p.is_massless())
        particle_states.add_state(p, Ds, add_vector_boson_states_impl<Ds>());
    else
        particle_states.add_state(p, Ds, add_massive_vector_boson_states_impl<Ds>());
    add_vector_boson_states<rest...>(p, particle_states);
}
/*********************************************************************************************************
 * FERMION STATES
 *********************************************************************************************************/
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_fermionic_states_impl() {
    std::vector<std::pair<SingleState, SingleState>> states;
    for (unsigned i = 1; i <= Caravel::clifford_algebra::two_to_the(Ds / 2 - 2); i++) {
        states.emplace_back(SingleState(q_CUT_STATE_PREFIX + std::to_string(i)), SingleState(q_CUT_STATE_PREFIX + "b" + std::to_string(i)));
    }
    return states;
}
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_fermionic_dirac_states_impl() {
    std::vector<std::pair<SingleState, SingleState>> states;
    for (unsigned i = 1; i <= Caravel::clifford_algebra::two_to_the(Ds / 2 - 2); i++) {
        states.emplace_back(SingleState(Q_CUT_STATE_PREFIX + "p" + std::to_string(i)), SingleState(Q_CUT_STATE_PREFIX + "bp" + std::to_string(i)));
        states.emplace_back(SingleState(Q_CUT_STATE_PREFIX + "m" + std::to_string(i)), SingleState(Q_CUT_STATE_PREFIX + "bm" + std::to_string(i)));
    }
    return states;
}

// DEFINE fermionic states
template <unsigned Dim> void add_fermionic_states(const ParticleType& p, ParticleStates& particle_states) {
    if (!is_dirac(p.get_flavor()))
        particle_states.add_state(p, Dim, add_fermionic_states_impl<Dim>());
    else
        particle_states.add_state(p, Dim, add_fermionic_dirac_states_impl<Dim>());
}

template <unsigned Dim, unsigned... rest, typename T> void add_fermionic_states(const ParticleType& p, ParticleStates& particle_states) {
    if (!is_dirac(p.get_flavor()))
        particle_states.add_state(p, Dim, add_fermionic_states_impl<Dim>());
    else
        particle_states.add_state(p, Dim, add_fermionic_dirac_states_impl<Dim>());
    add_fermionic_states<rest...>(p, particle_states);
}
/*********************************************************************************************************
 * SCALAR STATES
 *********************************************************************************************************/
template <unsigned Dim> void add_scalar_states(const ParticleType& p, ParticleStates& particle_states) {
    if (p.get_flavor() == ParticleFlavor::ds_scalar)
        particle_states.add_state(p, Dim, {{SingleState("SC"), SingleState("SCb")}});
    else
        particle_states.add_state(p, Dim, {{SingleState("RealSC"), SingleState("RealSC")}});
}

template <unsigned Dim, unsigned... rest, typename T> void add_scalar_states(const ParticleType& p, ParticleStates& particle_states) {
    if (p.get_flavor() == ParticleFlavor::ds_scalar)
        particle_states.add_state(p, Dim, {{SingleState("SC"), SingleState("SCb")}});
    else
        particle_states.add_state(p, Dim, {{SingleState("RealSC"), SingleState("RealSC")}});
    add_scalar_states<rest...>(p, particle_states);
}
/*********************************************************************************************************
 * TENSOR
 *********************************************************************************************************/
// the implemenation of tensor states for Ds
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_tensor_boson_states_impl() {
    static_assert(Ds >= 4, "Dimensional mismatch");
    std::vector<std::pair<SingleState, SingleState>> states;

    using namespace settings::general;
    if (cut_propagator_numerators.value == cut_states::states) {
        // ++ and -- states
        states.emplace_back(SingleState("hmmD"), SingleState("hmmcD"));
        states.emplace_back(SingleState("hppD"), SingleState("hppcD"));

        // states.emplace_back(SingleState("htr"),SingleState("htrc"));
        // +i and -i states
        for (unsigned i = 5; i <= Ds; i++) {
            states.emplace_back(SingleState("hm" + std::to_string(i - 2) + "D"), SingleState("hm" + std::to_string(i - 2) + "cD"));
            states.emplace_back(SingleState("hp" + std::to_string(i - 2) + "D"), SingleState("hp" + std::to_string(i - 2) + "cD"));
        }
        // ij states
        for (unsigned i = 5; i <= Ds; i++) {
            for (unsigned j = i + 1; j <= Ds; j++) {
                states.emplace_back(SingleState("h" + std::to_string(i - 2) + std::to_string(j - 2) + "D"),
                                    SingleState("h" + std::to_string(i - 2) + std::to_string(j - 2) + "cD"));
            }
        }
        // ij states
        for (unsigned i = 5; i <= Ds; i++) {
            states.emplace_back(SingleState("h" + std::to_string(i - 2) + std::to_string(i - 2) + "D"),
                                SingleState("h" + std::to_string(i - 2) + std::to_string(i - 2) + "cD"));
        }
    }
    else if (cut_propagator_numerators.value == cut_states::projective) {
        // TODO: fix for a proper set! Check out add_vector_boson_states_impl and add_fermionic_states_impl for examples
        states.emplace_back(SingleState("Ph" + std::to_string(Ds) + "TEST"), SingleState("Phc" + std::to_string(Ds) + "TEST"));
    }

    return states;
}

// Here we define the tensor boson states for an explicit dimension
template <unsigned Ds> void add_tensor_boson_states(const ParticleType& p, ParticleStates& particle_states) {
    particle_states.add_state(p, Ds, add_tensor_boson_states_impl<Ds>());
}

// handle the template parameter pack
template <unsigned Ds, unsigned... rest, typename T> void add_tensor_boson_states(const ParticleType& p, ParticleStates& particle_states) {
    particle_states.add_state(p, Ds, add_tensor_boson_states_impl<Ds>());
    add_tensor_boson_states<rest...>(p, particle_states);
}

} // namespace Model_Helper

template <class T, typename Tcur, size_t D> ExternalCurrent<T, Tcur, D> get_external_rule(const Particle& particle, const SingleState& state) {
    DEBUG_MESSAGE("External state for ", state, " for the particle: ", particle);

    // particle's characteristics
    const ParticleStatistics& spin(particle.get_statistics());
    bool is_massless(particle.is_massless());
    size_t midx = particle.get_mass_index();

    switch (spin) {

        case ParticleStatistics::scalar:
            DEBUG_MESSAGE("External state for scalar (just 1). The passed 'state' was: ", state, " for the particle: ", particle);
            if (state.get_name() == "SC" or state.get_name() == "RealSC")
                return [](const momD_conf<T, D>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) { return std::vector<Tcur>(1, Tcur(1)); };
            else if (state.get_name() == "SCb")
                return [](const momD_conf<T, D>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) { return std::vector<Tcur>(1, Tcur(-1)); };
            else
                throw std::runtime_error("Unknown state for scalar: " + state.get_name() + " for particle: " + particle.get_name() + " in get_external_rule");

        case ParticleStatistics::vector: {
            if (is_massless) {
                static const BG::vector_states<T, D> fs((T*)nullptr);
                const auto& states = fs.get();

                const auto r = states.find(state);
                if (r != states.end())
                    return r->second;
                else
                    throw std::runtime_error("Unknown state for vector: " + state.get_name() + " for particle: " + particle.get_name() +
                                             " in get_external_rule");
            }
            else {
                BG::massive_vector_states<T, D> fs(midx);
                const auto& states = fs.get();

                const auto r = states.find(state);
                if (r != states.end())
                    return r->second;
                else
                    throw std::runtime_error("Unknown state for vector: " + state.get_name() + " for particle: " + particle.get_name() +
                                             " in get_external_rule");
            }
        }

        case ParticleStatistics::tensor: {
#if INCLUDE_CUBIC_GRAVITY || INCLUDE_EH_GRAVITY
            static const BG::tensor_states<T, D> fs((T*)nullptr);
            const auto& states = fs.get();

            const auto r = states.find(state);
            if (r != states.end() && is_massless) {
                DEBUG_MESSAGE("External state for ", state, " for the particle: ", particle);
                return r->second;
            }
#else
            {
                std::cerr << "Requested a tensor-particle state without including corresponding gravity model. Consider doing 'meson configure "
                             "-Dgravity-model=Cubic,EH,EH-GB-R3' "
                          << std::endl;
                std::exit(1);
            }
#endif
            throw std::runtime_error("Unknown state for tensor: " + state.get_name() + " for particle: " + particle.get_name() + " in get_external_rule");
        }

        default: break;
    }

    if constexpr (D % 2 == 0) {
        switch (spin) {
            case ParticleStatistics::fermion:
            case ParticleStatistics::antifermion: {
                if (particle.external_state().get_representation() != repr::undefined) { // Weyl fermions
                    static const BG::fermion_states<T, D> fs{};
                    const auto& states = fs.get();

                    SingleState lstate(state);
                    lstate.set_dtt_index(particle.external_state().get_dtt_index());
                    const auto r = states.find(lstate);
                    if (r != states.end() && is_massless)
                        return r->second;
                    else
                        throw std::runtime_error("Unknown state for Weyl fermion: " + state.get_name() + " for particle: " + particle.get_name() +
                                                 " in get_external_rule");
                }
                else { // Dirac fermions
                    // We have to do all the decision here, since the mass index does not always survive the lambda capture (undefined behavior)
                    BG::dirac_fermion_states<T, D> fs(midx);
                    const auto& states = fs.get();

                    SingleState lstate(state);
                    lstate.set_dtt_index(particle.external_state().get_dtt_index());

                    const auto r = states.find(lstate);
                    if (r != states.end())
                        return r->second;
                    else
                        throw std::runtime_error("Unknown state for Dirac fermion: " + state.get_name() + " for particle: " + particle.get_name() +
                                                 " in get_external_rule");
                }
            }
            default: break;
        }
    }

    _WARNING("External get_external_rule not implemented yet, for: ", spin, " ", state, " Ds = ", D, " for the particle: ", particle);
    throw std::runtime_error("External get_external_rule not implemented!");
    return nullptr;
}

namespace __internal {
inline std::string map_to_containers(const ParticleType& t) {
    ParticleFlavor f = t.get_flavor();

    switch (f) {
        case ParticleFlavor::photon:
        case ParticleFlavor::Wm:
        case ParticleFlavor::Wp: return "photon";
        default: break;
    }

    if (in_container<ParticleFlavor::q>(f) and !is_dirac(f)) return "q";
    if (in_container<ParticleFlavor::qb>(f) and !is_dirac(f)) return "qb";
    if (in_container<ParticleFlavor::l>(f)) return "l";
    if (in_container<ParticleFlavor::lb>(f)) return "lqb";

    return t.to_string();
}
} // namespace __internal

/**
 * This function selects the combination rule for the currents to be merged in the gttL and gttR vertex
 **/
template <class T, typename Tcur, size_t D, int sign> CombineCurrent<T, Tcur, D> get_combine_rule_gtt(CurrentBasic* cr) {
    using namespace clifford_algebra;
    using namespace BG;
    // statistics of the current (fermion, antifermion or vector)
    ParticleStatistics cspin = cr->get_statistics();
    // statistics of the parent currents
    ParticleStatistics parentspins[2]{(*(cr->get_parents()))[0]->get_statistics(), (*(cr->get_parents()))[1]->get_statistics()};

    // anti-fermion = |Psi> -> |Psi'> = A_slash |Psi>
    if (cspin == ParticleStatistics::antifermion) {
        if (parentspins[0] == ParticleStatistics::antifermion && parentspins[1] == ParticleStatistics::vector)
            return CombineRule_gtt_t<T, Tcur, D, mult_from::R, 0, 1, sign>;
        if (parentspins[1] == ParticleStatistics::antifermion && parentspins[0] == ParticleStatistics::vector)
            return CombineRule_gtt_t<T, Tcur, D, mult_from::R, 1, 0, sign>;
    }
    // fermion = <Psi| ->> <Psi'| = <Psi| A_slash
    if (cspin == ParticleStatistics::fermion) {
        if (parentspins[0] == ParticleStatistics::fermion && parentspins[1] == ParticleStatistics::vector)
            return CombineRule_gtt_t<T, Tcur, D, mult_from::L, 0, 1, sign>;
        if (parentspins[1] == ParticleStatistics::fermion && parentspins[0] == ParticleStatistics::vector)
            return CombineRule_gtt_t<T, Tcur, D, mult_from::L, 1, 0, sign>;
    }
    // A_mu = <Psi_1|gamma_mu|Psi_2>
    if (cspin == ParticleStatistics::vector) {
        if (parentspins[0] == ParticleStatistics::fermion && parentspins[1] == ParticleStatistics::antifermion)
            return CombineRule_gtt_g<T, Tcur, D, 0, 1, sign>;
        if (parentspins[0] == ParticleStatistics::antifermion && parentspins[1] == ParticleStatistics::fermion)
            return CombineRule_gtt_g<T, Tcur, D, 1, 0, sign>;
    }

    std::cerr << "Did not find current gtt combine rule: " << cr->rule->get_name() << " with current stats " << cspin << " --- exit 1" << std::endl;
    std::exit(1);
}

/**
 * This function selects the combination rule for the currents to be merged in the (scalar,fermion,anti-fermion) vertex
 **/
template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_SFF(CurrentBasic* cr) {
    // statistics of the current (fermion, antifermion or scalar)
    ParticleStatistics cspin = cr->get_statistics();
    // statistics of the parent currents
    ParticleStatistics parentspins[2]{(*(cr->get_parents()))[0]->get_statistics(), (*(cr->get_parents()))[1]->get_statistics()};

    //(fermion,scalar) -> fermion
    if (parentspins[0] == ParticleStatistics::fermion && parentspins[1] == ParticleStatistics::scalar) {
        size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
        return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                   const BuilderContainer<T>& bc) { return BG::CombineRule_SFF_F<T, Tcur, D, 0, 1>(momlist, currentlist, bc, M); };
    }

    //(antifermion,scalar) -> antifermion
    if (parentspins[0] == ParticleStatistics::antifermion && parentspins[1] == ParticleStatistics::scalar) {
        size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
        return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                   const BuilderContainer<T>& bc) { return BG::CombineRule_SFF_F<T, Tcur, D, 0, 1>(momlist, currentlist, bc, M); };
    }

    //(scalar, fermion) -> fermion
    if (parentspins[0] == ParticleStatistics::scalar && parentspins[1] == ParticleStatistics::fermion) {
        size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
        return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                   const BuilderContainer<T>& bc) { return BG::CombineRule_SFF_F<T, Tcur, D, 1, 0>(momlist, currentlist, bc, M); };
    }

    //(scalar, antifermion) -> antifermion
    if (parentspins[0] == ParticleStatistics::scalar && parentspins[1] == ParticleStatistics::antifermion) {
        size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
        return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                   const BuilderContainer<T>& bc) { return BG::CombineRule_SFF_F<T, Tcur, D, 1, 0>(momlist, currentlist, bc, M); };
    }

    // (fermion, antifermion) -> scalar
    if (parentspins[0] == ParticleStatistics::fermion && parentspins[1] == ParticleStatistics::antifermion) {
        _WARNING_R("Rule: (fermion, antifermion) -> scalar not yet implemented!");
        std::exit(1);
    }
    // (antifermion, fermion) -> scalar
    if (parentspins[0] == ParticleStatistics::antifermion && parentspins[1] == ParticleStatistics::fermion) {
        _WARNING_R("Rule: (antifermion, fermion) -> scalar not yet implemented!");
        std::exit(1);
    }

    std::cerr << "Did not find current SFF combine rule: " << cr->rule->get_name() << " with current stats " << cspin << " --- exit 1" << std::endl;
    std::exit(1);
}

#ifdef INCLUDE_EH_GRAVITY
/**
 * This function selects the combination rule for the currents to be merged in the (scalar,scalar,graviton) vertex
 **/
template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_SSG(CurrentBasic* cr) {
    // statistics of the parent currents
    std::tuple<ParticleStatistics, ParticleStatistics> pspins = {
        (*(cr->get_parents()))[0]->get_statistics(),
        (*(cr->get_parents()))[1]->get_statistics(),
    };

    // which vertex combination to use (AB) -> cspin
    static const std::map<std::tuple<ParticleStatistics, ParticleStatistics>, int> rules = {
        {{ParticleStatistics::scalar, ParticleStatistics::scalar}, 0},
        {{ParticleStatistics::tensor, ParticleStatistics::scalar}, 1},
        {{ParticleStatistics::scalar, ParticleStatistics::tensor}, 2},
    };

    auto r = rules.find(pspins);
    if (r == rules.end()) {
        std::cerr << "Did not find current SSG combine rule: " << cr->rule->get_name() << " with current stats " << cr->get_statistics()
                  << " and parents: " << pspins << " --- exit 1" << std::endl;
        std::exit(1);
    }

    switch (r->second) {
        case 0: {
            // (SS) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSG_G<T, Tcur, D>(momlist, currentlist, bc, M); };
            break;
        }
        case 1: {
            // (GS) -> S
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSG_S<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1); };
            break;
        }
        case 2: {
            // (SG) -> S
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSG_S<T, Tcur, D>(momlist, currentlist, bc, M, 1, 0); };
            break;
        }
        default: {
            std::cerr << "Rule for " << pspins << " not implemented!" << std::endl;
            std::exit(1);
        }
    }
}

/**
 * This function selects the combination rule for the currents to be merged in the (scalar,scalar,graviton,graviton) vertex
 **/
template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_SSGG(CurrentBasic* cr) {
    // statistics of the parent currents
    std::tuple<ParticleStatistics, ParticleStatistics, ParticleStatistics> pspins = {
        (*(cr->get_parents()))[0]->get_statistics(),
        (*(cr->get_parents()))[1]->get_statistics(),
        (*(cr->get_parents()))[2]->get_statistics(),
    };

    // which vertex combination to use (ABC) -> cspin
    static const std::map<std::tuple<ParticleStatistics, ParticleStatistics, ParticleStatistics>, int> rules = {
        {{ParticleStatistics::scalar, ParticleStatistics::scalar, ParticleStatistics::tensor}, 0},
        {{ParticleStatistics::scalar, ParticleStatistics::tensor, ParticleStatistics::scalar}, 1},
        {{ParticleStatistics::tensor, ParticleStatistics::scalar, ParticleStatistics::scalar}, 2},
        {{ParticleStatistics::scalar, ParticleStatistics::tensor, ParticleStatistics::tensor}, 3},
        {{ParticleStatistics::tensor, ParticleStatistics::scalar, ParticleStatistics::tensor}, 4},
        {{ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::scalar}, 5},
    };

    auto r = rules.find(pspins);
    if (r == rules.end()) {
        std::cerr << "Did not find current SSGG combine rule: " << cr->rule->get_name() << " with current stats " << cr->get_statistics()
                  << " and parents: " << pspins << " --- exit 1" << std::endl;
        std::exit(1);
    }

    switch (r->second) {
        case 0: {
            // (SSG) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1, 2); };
            break;
        }
        case 1: {
            // (SGS) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 2, 1); };
            break;
        }
        case 2: {
            // (GSS) -> G
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 1, 2, 0); };
            break;
        }
        case 3: {
            // (SGG) -> S
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGG_S<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1, 2); };
            break;
        }
        case 4: {
            // (GSG) -> S
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGG_S<T, Tcur, D>(momlist, currentlist, bc, M, 1, 0, 2); };
            break;
        }
        case 5: {
            // (GGS) -> S
            size_t M = (*(cr->get_parents()))[2]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGG_S<T, Tcur, D>(momlist, currentlist, bc, M, 2, 0, 1); };
            break;
        }
        default: {
            std::cerr << "Rule for " << pspins << " not implemented!" << std::endl;
            std::exit(1);
        }
    }
}

/**
 * This function selects the combination rule for the currents to be merged in the (scalar,scalar,graviton,graviton,graviton) vertex
 **/
template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_SSGGG(CurrentBasic* cr) {
    // statistics of the parent currents
    std::tuple<ParticleStatistics, ParticleStatistics, ParticleStatistics, ParticleStatistics> pspins = {
        (*(cr->get_parents()))[0]->get_statistics(),
        (*(cr->get_parents()))[1]->get_statistics(),
        (*(cr->get_parents()))[2]->get_statistics(),
        (*(cr->get_parents()))[3]->get_statistics(),
    };

    // which vertex combination to use (ABCD) -> cspin
    static const std::map<std::tuple<ParticleStatistics, ParticleStatistics, ParticleStatistics, ParticleStatistics>, int> rules = {
        {{ParticleStatistics::scalar, ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::tensor}, 0},
        {{ParticleStatistics::tensor, ParticleStatistics::scalar, ParticleStatistics::tensor, ParticleStatistics::tensor}, 1},
        {{ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::scalar, ParticleStatistics::tensor}, 2},
        {{ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::scalar}, 3},
        {{ParticleStatistics::scalar, ParticleStatistics::scalar, ParticleStatistics::tensor, ParticleStatistics::tensor}, 4},
        {{ParticleStatistics::scalar, ParticleStatistics::tensor, ParticleStatistics::scalar, ParticleStatistics::tensor}, 5},
        {{ParticleStatistics::scalar, ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::scalar}, 6},
        {{ParticleStatistics::tensor, ParticleStatistics::scalar, ParticleStatistics::scalar, ParticleStatistics::tensor}, 7},
        {{ParticleStatistics::tensor, ParticleStatistics::scalar, ParticleStatistics::tensor, ParticleStatistics::scalar}, 8},
        {{ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::scalar, ParticleStatistics::scalar}, 9},
    };

    auto r = rules.find(pspins);
    if (r == rules.end()) {
        std::cerr << "Did not find current SSGGG combine rule: " << cr->rule->get_name() << " with current stats " << cr->get_statistics()
                  << " and parents: " << pspins << " --- exit 1" << std::endl;
        std::exit(1);
    }

    switch (r->second) {
        case 0: {
            // (SGGG) -> S
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_S<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1, 2, 3); };
            break;
        }
        case 1: {
            // (GSGG) -> S
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_S<T, Tcur, D>(momlist, currentlist, bc, M, 1, 0, 2, 3); };
            break;
        }
        case 2: {
            // (GGSG) -> S
            size_t M = (*(cr->get_parents()))[2]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_S<T, Tcur, D>(momlist, currentlist, bc, M, 2, 0, 1, 3); };
            break;
        }
        case 3: {
            // (GGGS) -> S
            size_t M = (*(cr->get_parents()))[3]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_S<T, Tcur, D>(momlist, currentlist, bc, M, 3, 0, 1, 2); };
            break;
        }
        case 4: {
            // (SSGG) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1, 2, 3); };
            break;
        }
        case 5: {
            // (SGSG) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 2, 1, 3); };
            break;
        }
        case 6: {
            // (SGGS) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 3, 1, 2); };
            break;
        }
        case 7: {
            // (GSSG) -> G
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 1, 2, 0, 3); };
            break;
        }
        case 8: {
            // (GSGS) -> G
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 1, 3, 0, 2); };
            break;
        }
        case 9: {
            // (GGSS) -> G
            size_t M = (*(cr->get_parents()))[2]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_SSGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 2, 3, 0, 1); };
            break;
        }
        default: {
            std::cerr << "Rule for " << pspins << " not implemented!" << std::endl;
            std::exit(1);
        }
    }
}

/**
 * This function selects the combination rule for the currents to be merged in the (vector,vector,graviton) vertex
 **/
template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_VVG(CurrentBasic* cr) {
    // statistics of the parent currents
    std::tuple<ParticleStatistics, ParticleStatistics> pspins = {
        (*(cr->get_parents()))[0]->get_statistics(),
        (*(cr->get_parents()))[1]->get_statistics(),
    };

    // which vertex combination to use (AB) -> cspin
    static const std::map<std::tuple<ParticleStatistics, ParticleStatistics>, int> rules = {
        {{ParticleStatistics::vector, ParticleStatistics::vector}, 0},
        {{ParticleStatistics::tensor, ParticleStatistics::vector}, 1},
        {{ParticleStatistics::vector, ParticleStatistics::tensor}, 2},
    };

    auto r = rules.find(pspins);
    if (r == rules.end()) {
        std::cerr << "Did not find current VVG combine rule: " << cr->rule->get_name() << " with current stats " << cr->get_statistics()
                  << " and parents: " << pspins << " --- exit 1" << std::endl;
        std::exit(1);
    }

    switch (r->second) {
        case 0: {
            // (VV) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVG_G<T, Tcur, D>(momlist, currentlist, bc, M); };
            break;
        }
        case 1: {
            // (GV) -> V
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVG_V<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1); };
            break;
        }
        case 2: {
            // (VG) -> V
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVG_V<T, Tcur, D>(momlist, currentlist, bc, M, 1, 0); };
            break;
        }
        default: {
            std::cerr << "Rule for " << pspins << " not implemented!" << std::endl;
            std::exit(1);
        }
    }
}

/**
 * This function selects the combination rule for the currents to be merged in the (vector,vector,graviton,graviton) vertex
 **/
template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_VVGG(CurrentBasic* cr) {
    // statistics of the parent currents
    std::tuple<ParticleStatistics, ParticleStatistics, ParticleStatistics> pspins = {
        (*(cr->get_parents()))[0]->get_statistics(),
        (*(cr->get_parents()))[1]->get_statistics(),
        (*(cr->get_parents()))[2]->get_statistics(),
    };

    // which vertex combination to use (ABC) -> cspin
    static const std::map<std::tuple<ParticleStatistics, ParticleStatistics, ParticleStatistics>, int> rules = {
        {{ParticleStatistics::vector, ParticleStatistics::vector, ParticleStatistics::tensor}, 0},
        {{ParticleStatistics::vector, ParticleStatistics::tensor, ParticleStatistics::vector}, 1},
        {{ParticleStatistics::tensor, ParticleStatistics::vector, ParticleStatistics::vector}, 2},
        {{ParticleStatistics::vector, ParticleStatistics::tensor, ParticleStatistics::tensor}, 3},
        {{ParticleStatistics::tensor, ParticleStatistics::vector, ParticleStatistics::tensor}, 4},
        {{ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::vector}, 5},
    };

    auto r = rules.find(pspins);
    if (r == rules.end()) {
        std::cerr << "Did not find current VVGG combine rule: " << cr->rule->get_name() << " with current stats " << cr->get_statistics()
                  << " and parents: " << pspins << " --- exit 1" << std::endl;
        std::exit(1);
    }

    switch (r->second) {
        case 0: {
            // (VVG) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1, 2); };
            break;
        }
        case 1: {
            // (VGV) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 2, 1); };
            break;
        }
        case 2: {
            // (GVV) -> G
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 1, 2, 0); };
            break;
        }
        case 3: {
            // (VGG) -> V
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGG_V<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1, 2); };
            break;
        }
        case 4: {
            // (GVG) -> V
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGG_V<T, Tcur, D>(momlist, currentlist, bc, M, 1, 0, 2); };
            break;
        }
        case 5: {
            // (GGV) -> V
            size_t M = (*(cr->get_parents()))[2]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGG_V<T, Tcur, D>(momlist, currentlist, bc, M, 2, 0, 1); };
            break;
        }
        default: {
            std::cerr << "Rule for " << pspins << " not implemented!" << std::endl;
            std::exit(1);
        }
    }
}

/**
 * This function selects the combination rule for the currents to be merged in the (vector,vector,graviton,graviton,graviton) vertex
 **/
template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_VVGGG(CurrentBasic* cr) {
    // statistics of the parent currents
    std::tuple<ParticleStatistics, ParticleStatistics, ParticleStatistics, ParticleStatistics> pspins = {
        (*(cr->get_parents()))[0]->get_statistics(),
        (*(cr->get_parents()))[1]->get_statistics(),
        (*(cr->get_parents()))[2]->get_statistics(),
        (*(cr->get_parents()))[3]->get_statistics(),
    };

    // which vertex combination to use (ABCD) -> cspin
    static const std::map<std::tuple<ParticleStatistics, ParticleStatistics, ParticleStatistics, ParticleStatistics>, int> rules = {
        {{ParticleStatistics::vector, ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::tensor}, 0},
        {{ParticleStatistics::tensor, ParticleStatistics::vector, ParticleStatistics::tensor, ParticleStatistics::tensor}, 1},
        {{ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::vector, ParticleStatistics::tensor}, 2},
        {{ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::vector}, 3},
        {{ParticleStatistics::vector, ParticleStatistics::vector, ParticleStatistics::tensor, ParticleStatistics::tensor}, 4},
        {{ParticleStatistics::vector, ParticleStatistics::tensor, ParticleStatistics::vector, ParticleStatistics::tensor}, 5},
        {{ParticleStatistics::vector, ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::vector}, 6},
        {{ParticleStatistics::tensor, ParticleStatistics::vector, ParticleStatistics::vector, ParticleStatistics::tensor}, 7},
        {{ParticleStatistics::tensor, ParticleStatistics::vector, ParticleStatistics::tensor, ParticleStatistics::vector}, 8},
        {{ParticleStatistics::tensor, ParticleStatistics::tensor, ParticleStatistics::vector, ParticleStatistics::vector}, 9},
    };

    auto r = rules.find(pspins);
    if (r == rules.end()) {
        std::cerr << "Did not find current VVGGG combine rule: " << cr->rule->get_name() << " with current stats " << cr->get_statistics()
                  << " and parents: " << pspins << " --- exit 1" << std::endl;
        std::exit(1);
    }

    switch (r->second) {
        case 0: {
            // (VGGG) -> V
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_V<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1, 2, 3); };
            break;
        }
        case 1: {
            // (GVGG) -> V
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_V<T, Tcur, D>(momlist, currentlist, bc, M, 1, 0, 2, 3); };
            break;
        }
        case 2: {
            // (GGVG) -> V
            size_t M = (*(cr->get_parents()))[2]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_V<T, Tcur, D>(momlist, currentlist, bc, M, 2, 0, 1, 3); };
            break;
        }
        case 3: {
            // (GGGV) -> V
            size_t M = (*(cr->get_parents()))[3]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_V<T, Tcur, D>(momlist, currentlist, bc, M, 3, 0, 1, 2); };
            break;
        }
        case 4: {
            // (VVGG) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 1, 2, 3); };
            break;
        }
        case 5: {
            // (VGVG) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 2, 1, 3); };
            break;
        }
        case 6: {
            // (VGGV) -> G
            size_t M = (*(cr->get_parents()))[0]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 0, 3, 1, 2); };
            break;
        }
        case 7: {
            // (GVVG) -> G
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 1, 2, 0, 3); };
            break;
        }
        case 8: {
            // (GVGV) -> G
            size_t M = (*(cr->get_parents()))[1]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 1, 3, 0, 2); };
            break;
        }
        case 9: {
            // (GGVV) -> G
            size_t M = (*(cr->get_parents()))[2]->get_current_particle().get_mass_index();
            return [M](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                       const BuilderContainer<T>& bc) { return BG::CombineRule_VVGGG_G<T, Tcur, D>(momlist, currentlist, bc, M, 2, 3, 0, 1); };
            break;
        }
        default: {
            std::cerr << "Rule for " << pspins << " not implemented!" << std::endl;
            std::exit(1);
        }
    }
}

#endif

// wiring both photon CurrenRule's pqq and pll
template <class T, typename Tcur, size_t D, repr RE> CombineCurrent<T, Tcur, D> get_combine_rule_with_photon(CurrentBasic* cr) {
    // statistics of the current (fermion, antifermion or vector)
    ParticleStatistics cspin = cr->get_statistics();
    // statistics of the parent currents
    ParticleStatistics parentspins[2]{(*(cr->get_parents()))[0]->get_statistics(), (*(cr->get_parents()))[1]->get_statistics()};

    using namespace __internal;
    using namespace BG;

    // now the map
    // out			first parent		 i 	 j	rule
    static const std::map<std::pair<ParticleStatistics, ParticleStatistics>, CombineCurrent<T, Tcur, D>> rules = {
        // (photon,fermion) --> fermion
        {{ParticleStatistics::fermion, ParticleStatistics::vector}, CombineRule_pff_f<T, Tcur, D, RE, 1, 0>},
        // (fermion,photon) --> fermion
        {{ParticleStatistics::fermion, ParticleStatistics::fermion}, CombineRule_pff_f<T, Tcur, D, RE, 0, 1>},
        // (photon,antifermion) --> antifermion
        {{ParticleStatistics::antifermion, ParticleStatistics::vector}, CombineRule_pff_fb<T, Tcur, D, RE, 1, 0>},
        // (antifermion,photon) --> antifermion
        {{ParticleStatistics::antifermion, ParticleStatistics::antifermion}, CombineRule_pff_fb<T, Tcur, D, RE, 0, 1>},
        // // (antifermion,fermion) --> photon
        {{ParticleStatistics::vector, ParticleStatistics::antifermion}, CombineRule_pff_p<T, Tcur, D, RE, 0, 1>},
        // (fermion,antifermion) --> photon
        {{ParticleStatistics::vector, ParticleStatistics::fermion}, CombineRule_pff_p<T, Tcur, D, RE, 1, 0>},
        // (antifermion,fermion) --> photon
        {{ParticleStatistics::vector, ParticleStatistics::antifermion}, CombineRule_pff_p<T, Tcur, D, RE, 0, 1>},
        // (fermion,antifermion) --> photon
        {{ParticleStatistics::vector, ParticleStatistics::fermion}, CombineRule_pff_p<T, Tcur, D, RE, 1, 0>}};
    // out			first parent		 i 	 j	rule
    decltype((rules.find(std::make_pair(cspin, parentspins[0])))) r;

    // the D>4 makes choice consistent with corresponding choices in Builder.hpp (CurrentContainer constructor) and in BG_fermion.hpp in class fermion_states
    r = rules.find(std::make_pair(cspin, parentspins[0]));

    if (r != rules.end()) {
        DEBUG_MESSAGE("Combine rule for ", cr->rule->get_name(), " with current stats ", cspin, " in D=", D);
        return r->second;
    }

    _WARNING("Did not find current photon combine rule: ", cr->rule->get_name(), " with current stats ", cspin, ", returning DUMMY rule!");
    return [](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
              const BuilderContainer<T>& bc) { return std::vector<Tcur>(1, T(0)); };
}

template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> combine_rules_with_scalars(CurrentBasic* cr) {
    using namespace Caravel::BG;

    std::string lcurrentrule((*cr).rule->get_name());
    std::string ctype = __internal::map_to_containers(cr->get_current_type());

    if (lcurrentrule == "gss") {
        if (ctype == "gluon") { return CombineCurrent_CurrentRule_gss_g<T, Tcur, D>; }
        else {
            const auto& parents = *(cr->get_parents());
            if (parents.at(0)->get_statistics() == ParticleStatistics::scalar) { return CombineCurrent_CurrentRule_ssg<T, Tcur, D>; }
            else {
                return CombineCurrent_CurrentRule_sgs<T, Tcur, D>;
            }
        }
    }

    if (lcurrentrule == "ggss") {
        if (ctype == "gluon") {
            const auto& parents = *(cr->get_parents());
            unsigned g_pos = 0;
            for (size_t i = 0; i < parents.size(); ++i) {
                if (parents.at(i)->get_statistics() == ParticleStatistics::vector) {
                    g_pos = i;
                    break;
                }
            }
            switch (g_pos) {
                case 0: return CombineCurrent_CurrentRule_ggss_g<T, Tcur, D, 1, 0, 1, 2>;
                case 2: return CombineCurrent_CurrentRule_ggss_g<T, Tcur, D, 1, 2, 0, 1>;
                default: break;
            }
        }
        else {
            const auto& parents = *(cr->get_parents());
            unsigned s_pos = 0;
            for (size_t i = 0; i < parents.size(); ++i) {
                if (parents.at(i)->get_statistics() == ParticleStatistics::scalar) {
                    s_pos = i;
                    break;
                }
            }
            switch (s_pos) {
                case 0: return CombineCurrent_CurrentRule_ggss_s<T, Tcur, D, 1, 0, 1, 2>;
                case 2: return CombineCurrent_CurrentRule_ggss_s<T, Tcur, D, 1, 2, 0, 1>;
                default: break;
            }
        }
    }

    if (lcurrentrule == "gsgs") {
        if (ctype == "gluon")
            return CombineCurrent_CurrentRule_ggss_g<T, Tcur, D, -2, 1, 0, 2>;
        else
            return CombineCurrent_CurrentRule_ggss_s<T, Tcur, D, -2, 1, 0, 2>;
    }

    if (lcurrentrule == "ssss") {
        int fl1 = cr->get_assigned_internal_index(0);
        int fl2 = cr->get_assigned_internal_index(1);
        int fl3 = cr->get_assigned_internal_index(2);

        assert(fl1 != 0);
        assert(fl2 != 0);
        assert(fl3 != 0);

        // if adjacent scalars join
        if (fl1 == fl2 or fl2 == fl3) { return CombineCurrent_CurrentRule_ssss<T, Tcur, D, 1>; }
        else {
            return CombineCurrent_CurrentRule_ssss<T, Tcur, D, -2>;
        }
    }

    return CombineCurrent<T, Tcur, D>{nullptr};
}

#ifdef INCLUDE_CUBIC_GRAVITY
template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_GGA(CurrentBasic* cr) {
    // statistics of the current (either tensor or cubictensor)
    ParticleStatistics cspin = cr->get_statistics();

    // if it is a cubic tensor, nothing to do
    if (cspin == ParticleStatistics::cubictensor) return BG::CurrentRule_GGA_A<T, Tcur, D>;

    // statistics of the parent currents
    ParticleStatistics parentspins[2]{(*(cr->get_parents()))[0]->get_statistics(), (*(cr->get_parents()))[1]->get_statistics()};

    if (parentspins[0] == ParticleStatistics::cubictensor) {
        // the cubictensor comes in first ('0') entry
        return [](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                  const BuilderContainer<T>& bc) { return BG::CurrentRule_GGA_G<T, Tcur, D>(1, 0, momlist, currentlist, bc); };
    }
    else {
        // the cubictensor comes in second ('1') entry
        return [](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                  const BuilderContainer<T>& bc) { return BG::CurrentRule_GGA_G<T, Tcur, D>(0, 1, momlist, currentlist, bc); };
    }
}

template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule_GAA(CurrentBasic* cr) {
    // statistics of the current (either tensor or cubictensor)
    ParticleStatistics cspin = cr->get_statistics();

    // if it is a graviton, nothing to do
    if (cspin == ParticleStatistics::tensor) return BG::CurrentRule_GAA_G<T, Tcur, D>;

    // statistics of the parent currents
    ParticleStatistics parentspins[2]{(*(cr->get_parents()))[0]->get_statistics(), (*(cr->get_parents()))[1]->get_statistics()};

    if (parentspins[0] == ParticleStatistics::tensor) {
        // the graviton comes in first ('0') entry
        return [](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                  const BuilderContainer<T>& bc) { return BG::CurrentRule_GAA_A<T, Tcur, D>(0, 1, momlist, currentlist, bc); };
    }
    else {
        // the graviton comes in second ('1') entry
        return [](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
                  const BuilderContainer<T>& bc) { return BG::CurrentRule_GAA_A<T, Tcur, D>(1, 0, momlist, currentlist, bc); };
    }
}
#endif

template <class T, typename Tcur, size_t D> CombineCurrent<T, Tcur, D> get_combine_rule(CurrentBasic* cr) {
    using namespace __internal;
    using namespace Caravel::BG;

    std::string lcurrentrule((*cr).rule->get_name());
    std::string ctype = map_to_containers(cr->get_current_type());

    DEBUG_PRINT(*cr);
    DEBUG_PRINT(*(cr->rule));

#ifdef INCLUDE_CUBIC_GRAVITY
    if (lcurrentrule == "GGA") return get_combine_rule_GGA<T, Tcur, D>(cr);
    if (lcurrentrule == "Cubic_GGG") return BG::CurrentRule_GGG<T, Tcur, D>;
    if (lcurrentrule == "GAA") return get_combine_rule_GAA<T, Tcur, D>(cr);
#else
    if (lcurrentrule == "GGA" || lcurrentrule == "Cubic_GGG" || lcurrentrule == "GAA") {
        std::cerr << "Requested a " << lcurrentrule
                  << " current rule without including corresponding gravity model. Consider doing 'meson configure -Dgravity-model=Cubic' " << std::endl;
        std::exit(1);
    }
#endif

#ifdef INCLUDE_EH_GRAVITY
    if (lcurrentrule == "GGG") return BG::CurrentRule_EH_GGG<T, Tcur, D>;
    if (lcurrentrule == "GGGG") return BG::CurrentRule_EH_GGGG<T, Tcur, D>;
    if (lcurrentrule == "GGGGG") return BG::CurrentRule_EH_GGGGG<T, Tcur, D>;
    if (lcurrentrule == "ssG1" or lcurrentrule == "ssG2") return get_combine_rule_SSG<T, Tcur, D>(cr);
    if (lcurrentrule == "ssGG1" or lcurrentrule == "ssGG2") return get_combine_rule_SSGG<T, Tcur, D>(cr);
    if (lcurrentrule == "ssGGG1" or lcurrentrule == "ssGGG2") return get_combine_rule_SSGGG<T, Tcur, D>(cr);
    if (lcurrentrule == "VVG1" or lcurrentrule == "VVG2") return get_combine_rule_VVG<T, Tcur, D>(cr);
    if (lcurrentrule == "VVGG1" or lcurrentrule == "VVGG2") return get_combine_rule_VVGG<T, Tcur, D>(cr);
    if (lcurrentrule == "VVGGG1" or lcurrentrule == "VVGGG2") return get_combine_rule_VVGGG<T, Tcur, D>(cr);

#ifdef INCLUDE_GB_GRAVITY
    if (lcurrentrule == "GB_GGG") return BG::CurrentRule_GB_GGG<T, Tcur, D>;
    if (lcurrentrule == "GB_GGGG") return BG::CurrentRule_GB_GGGG<T, Tcur, D>;
    if (lcurrentrule == "R3_GGG") return BG::CurrentRule_R3_GGG<T, Tcur, D>;
    if (lcurrentrule == "R3_GGGG") return BG::CurrentRule_R3_GGGG<T, Tcur, D>;
#else
    if (lcurrentrule == "GB_GGG" || lcurrentrule == "GB_GGGG" || lcurrentrule == "R3_GGG" || lcurrentrule == "R3_GGGG") {
        std::cerr << "Requested a " << lcurrentrule
                  << " current rule without including corresponding gravity model. Consider doing 'meson configure -Dgravity-model=EH-GB-R3' " << std::endl;
        std::exit(1);
    }
#endif
#else
    if (lcurrentrule == "GGG" || lcurrentrule == "GGGG" || lcurrentrule == "GGGGG") {
        std::cerr << "Requested a " << lcurrentrule
                  << " current rule without including corresponding gravity model. Consider doing 'meson configure -Dgravity-model=EH' " << std::endl;
        std::exit(1);
    }
#endif

    if (lcurrentrule == "ggT") {
        // in g(0)g(1)T(2) find which one is the open index
        int openindex(-1);
        if ((cr->indices)[0] == 0)
            openindex = 2;
        else if ((cr->indices)[0] == 1)
            openindex = 0;
        else if ((cr->indices)[0] == 2)
            openindex = 1;
        if (openindex == -1) std::cout << "ERROR: wrong open index in ggT current rule!" << std::endl;
        switch (openindex) {
            case 0: return CombineCurrent_CurrentRule_ggT_0<T, Tcur, D>;
            case 1: return CombineCurrent_CurrentRule_ggT_1<T, Tcur, D>;
            case 2: return CombineCurrent_CurrentRule_ggT_2<T, Tcur, D>;
            default: break;
        }
    }

    static const std::map<std::pair<std::string, std::string>, CombineCurrent<T, Tcur, D>> rules = {

        {{"ggg", "gluon"}, CombineCurrent_CurrentRule_ggg<T, Tcur, D>},
        {{"gggg", "gluon"}, CombineCurrent_CurrentRule_gggg<T, Tcur, D>},

    };

    auto r = rules.find(std::make_pair(lcurrentrule, ctype));
    if (r != rules.end()) {
        DEBUG_MESSAGE("Combine rule for ", lcurrentrule, " with current type ", ctype, " in D=", D);
        return r->second;
    }

    // for vertices with scalars we need more complicated matching rules than just a map
#ifdef DS_BY_PARTICLE_CONTENT
    {
        auto r = combine_rules_with_scalars<T, Tcur, D>(cr);
        if (r) return r;
    }
#endif

    // activate rules with fermions only in even dimension
    if constexpr (D % 2 == 0) {
        // Initialize global slahsed matrices
        clifford_algebra::sparse_smatrix<D, T, true>::init();
        clifford_algebra::sparse_smatrix<D, T, false>::init();

	// Massive fermion vertices
        if (lcurrentrule == "gttL") return get_combine_rule_gtt<T, Tcur, D, 1>(cr);
        if (lcurrentrule == "gttR") return get_combine_rule_gtt<T, Tcur, D, -1>(cr);
        if (lcurrentrule == "Htt" || lcurrentrule == "Zm") return get_combine_rule_SFF<T, Tcur, D>(cr);


        using map_type = std::map<std::tuple<std::string, std::string, repr>, CombineCurrent<T, Tcur, D>>;

        static const map_type rules = {

            {{"gqqR", "q", repr::La}, CombineRule_pff_f<T, Tcur, D, repr::La, 0, 1, -1>},
            {{"gqqR", "qb", repr::Lat}, CombineRule_pff_fb<T, Tcur, D, repr::Lat, 1, 0, -1>},
            {{"gqqR", "gluon", repr::Lat}, CombineRule_pff_p<T, Tcur, D, repr::Lat, 0, 1, -1>},
            {{"gqqL", "q", repr::La}, CombineRule_pff_f<T, Tcur, D, repr::La, 1, 0, 1>},
            {{"gqqL", "qb", repr::Lat}, CombineRule_pff_fb<T, Tcur, D, repr::Lat, 0, 1, 1>},
            {{"gqqL", "gluon", repr::Lat}, CombineRule_pff_p<T, Tcur, D, repr::La, 1, 0, 1>},

            {{"gqqR", "q", repr::Lat}, CombineRule_pff_f<T, Tcur, D, repr::Lat, 0, 1, -1>},
            {{"gqqR", "qb", repr::La}, CombineRule_pff_fb<T, Tcur, D, repr::La, 1, 0, -1>},
            {{"gqqR", "gluon", repr::La}, CombineRule_pff_p<T, Tcur, D, repr::La, 0, 1, -1>},
            {{"gqqL", "q", repr::Lat}, CombineRule_pff_f<T, Tcur, D, repr::Lat, 1, 0, 1>},
            {{"gqqL", "qb", repr::La}, CombineRule_pff_fb<T, Tcur, D, repr::La, 0, 1, 1>},
            {{"gqqL", "gluon", repr::La}, CombineRule_pff_p<T, Tcur, D, repr::Lat, 1, 0, 1>},

            {{"sqqR", "q", repr::Lat}, CombineRule_sqq_q<T, Tcur, D, repr::Lat, -1, 0>},
            {{"sqqR", "qb", repr::Lat}, CombineRule_sqq_q<T, Tcur, D, repr::Lat, -1, 1>},
            {{"sqqR", "q", repr::La}, CombineRule_sqq_q<T, Tcur, D, repr::La, -1, 0>},
            {{"sqqR", "qb", repr::La}, CombineRule_sqq_q<T, Tcur, D, repr::La, -1, 1>},

            {{"sqqL", "q", repr::Lat}, CombineRule_sqq_q<T, Tcur, D, repr::Lat, 1, 1>},
            {{"sqqL", "qb", repr::Lat}, CombineRule_sqq_q<T, Tcur, D, repr::Lat, 1, 0>},
            {{"sqqL", "q", repr::La}, CombineRule_sqq_q<T, Tcur, D, repr::La, 1, 1>},
            {{"sqqL", "qb", repr::La}, CombineRule_sqq_q<T, Tcur, D, repr::La, 1, 0>},

            {{"sqqR", "ds_scalar", repr::La}, CombineRule_sqq_s<T, Tcur, D, repr::La, 1>},
            {{"sqqR", "ds_scalar", repr::Lat}, CombineRule_sqq_s<T, Tcur, D, repr::Lat, 1>},
            {{"sqqL", "ds_scalar", repr::La}, CombineRule_sqq_s<T, Tcur, D, repr::La, 1>},
            {{"sqqL", "ds_scalar", repr::Lat}, CombineRule_sqq_s<T, Tcur, D, repr::Lat, 1>},
        };

        typename map_type::const_iterator r;

        if ((ctype == "gluon" or ctype == "ds_scalar") and (lcurrentrule.substr(1) == "qqR" or lcurrentrule.substr(1) == "qqL")) {
            // we choose index depending on which index has one of the parent (anti)quarks
            // this of course works only when we choose to preserve Weyl representation
            DEBUG_MESSAGE("\tusing (anti)quark index of a parent current");
            r = rules.find(std::make_tuple(lcurrentrule, ctype, cr->get_parents()->front()->get_current_particle().external_state().get_representation()));
        }
        else {
            r = rules.find(std::make_tuple(lcurrentrule, ctype, cr->get_current_particle().external_state().get_representation()));
        }

        if (r != rules.end()) {
            DEBUG_MESSAGE("Combine rule ", r->first, " for ", lcurrentrule, " with current type ", ctype, " in D=", D);
            return r->second;
        }

        static const std::set<std::string> photon_like_rules = {"pqq", "pll", "Wm_u_dbar", "Wp_e_vebar", "Wp_ubar_d", "Wm_ebar_ve"};

        if (photon_like_rules.count(lcurrentrule) > 0) {
            Particle p;
            if (ctype == "photon") {
                p = cr->get_parents()->front()->get_current_particle();
                if ((p.external_state().get_representation() == repr::La && p.get_statistics() == ParticleStatistics::antifermion) ||
                    (p.external_state().get_representation() == repr::Lat && p.get_statistics() == ParticleStatistics::fermion)) {
                    return get_combine_rule_with_photon<T, Tcur, D, repr::La>(cr);
                }
                else if ((p.external_state().get_representation() == repr::Lat && p.get_statistics() == ParticleStatistics::antifermion) ||
                         (p.external_state().get_representation() == repr::La && p.get_statistics() == ParticleStatistics::fermion)) {
                    return get_combine_rule_with_photon<T, Tcur, D, repr::Lat>(cr);
                }
                else {
                    std::cout << "TAKE CARE! undefined representation" << std::endl;
                    return get_combine_rule_with_photon<T, Tcur, D, repr::La>(cr);
                }
            }
            else {
                p = cr->get_current_particle();
                if (p.external_state().get_representation() == repr::La)
                    return get_combine_rule_with_photon<T, Tcur, D, repr::La>(cr);
                else if (p.external_state().get_representation() == repr::Lat)
                    return get_combine_rule_with_photon<T, Tcur, D, repr::Lat>(cr);
            }
        }
    } // (D % 2 == 0)

    _WARNING("Did not find current combine rule: ", lcurrentrule, " with current type ", ctype, ", returning DUMMY rule!");
    return [](const std::vector<momentumD<T, D> const*>& momlist, const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist,
              const BuilderContainer<T>& bc) { return std::vector<Tcur>(1, Tcur(0)); };
}

template <class T, typename Tcur, size_t D> ComputeProp<T, Tcur, D> get_compute_propagator(const CurrentBuild* cb, const Model& model) {
    const auto& include = cb->include_propagator();

    // return a unit propagator if requested
    if (include == PropagatorType::identity) {
        DEBUG_MESSAGE("identity propagator in D=", D, " dims");
        return BG::Unit_Propagator<T, Tcur, D>;
    }

    const Particle& particle = cb->get_current_particle();
    // const auto& spin = cb->get_statistics();
    const ParticleStatistics& spin(particle.get_statistics());
    // Mass index & Flavor
    size_t M = particle.get_mass_index();
    const ParticleFlavor& flavor(particle.get_flavor());

    using namespace __internal;

    if (spin == ParticleStatistics::tensor) {
        // particular handling of QCD tensors
        if (flavor == ParticleFlavor::T) {
            DEBUG_MESSAGE("For auxiliary tensor in QCD always trivial propagator");
            return BG::Trivial_Tensor_Propagator<T, Tcur, D>;
        }
        if (flavor == ParticleFlavor::G) {
#if INCLUDE_CUBIC_GRAVITY || INCLUDE_EH_GRAVITY
            DEBUG_MESSAGE("Found propagator for ", particle, " with type ", include, " in D=", D);
            if (include == PropagatorType::propagator) {
                if (model.get_name() == "CubicGravity")
                    return BG::Graviton_Propagator<T, Tcur, D>;
                else if (model.get_name() == "EHGravity")
                    return BG::deDonder_Graviton_Propagator<T, Tcur, D, 0>;
                else {
                    std::cerr << "Requested a propagator for " << particle << " without using CubicGravity or EHGravity model: " << model.get_name()
                              << std::endl;
                    std::exit(1);
                }
            }
            if (include == PropagatorType::cut && model.get_name() == "CubicGravity") { return BG::Cut_Graviton_Propagator<T, Tcur, D>; }
            if (include == PropagatorType::cut && model.get_name() == "EHGravity") { return BG::deDonder_Graviton_Propagator<T, Tcur, D, 1>; }
#else
            std::cerr << "Requested a propagator for " << particle
                      << " without including corresponding gravity model. Consider doing 'meson configure -Dgravity-model=Cubic,EH,EH-GB-R3' " << std::endl;
            std::exit(1);
#endif
        }
    }

    if (spin == ParticleStatistics::cubictensor) {
#ifdef INCLUDE_CUBIC_GRAVITY
        DEBUG_MESSAGE("Found propagator for ", particle, " with type ", include, " in D=", D);
        return BG::CubicTensor_Propagator<T, Tcur, D>;
#else
        std::cerr << "Requested a propagator for " << particle
                  << " without including corresponding gravity model. Consider doing 'meson configure -Dgravity-model=Cubic' " << std::endl;
        std::exit(1);
#endif
    }

#ifdef DEBUG_ON
    std::string ctype = cb->get_current_type().to_string();

    DEBUG_MESSAGE("Prop ctype ", ctype);
    DEBUG_MESSAGE("Statistics ", cb->get_statistics());
#endif

    // massless Vectors
    if (spin == ParticleStatistics::vector && particle.is_massless()) {
        if (include == PropagatorType::propagator) {
            return [](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::Vector_Propagator<T, Tcur, D>(in, prop, l, bc);
            };
        }
        if (include == PropagatorType::cut) {
            return [](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::Cut_Vector_Propagator<T, Tcur, D>(in, prop, l, bc);
            };
        }
    }
    // Massive Vectors
    if (spin == ParticleStatistics::vector && particle.is_massive()) {
        if (include == PropagatorType::propagator) {
            return [M](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::massive_Vector_Propagator<T, Tcur, D, PropagatorType::propagator>(in, prop, l, M, bc);
            };
        }
        if (include == PropagatorType::cut) {
            return [M](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::massive_Vector_Propagator<T, Tcur, D, PropagatorType::cut>(in, prop, l, M, bc);
            };
        }
    }

    // Scalars
    if (spin == ParticleStatistics::scalar && include == PropagatorType::propagator) {
        if (flavor == ParticleFlavor::ds_scalar)
            return BG::ds_Scalar_Propagator<T, Tcur, D>;
        else {
            return [M](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::Scalar_Propagator<T, Tcur, D, PropagatorType::propagator>(in, prop, l, M, bc);
            };
        }
    }
    if (spin == ParticleStatistics::scalar && include == PropagatorType::cut) {
        if (flavor == ParticleFlavor::ds_scalar)
            return BG::ds_Scalar_Cut_Propagator<T, Tcur, D>;
        else {
            return [M](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::Scalar_Propagator<T, Tcur, D, PropagatorType::cut>(in, prop, l, M, bc);
            };
        }
    }

    // activate fermion propagators only in even dimensions
    if constexpr (D % 2 == 0) {
        using map_type = std::map<std::tuple<ParticleStatistics, PropagatorType, repr>, ComputeProp<T, Tcur, D>>;
        size_t M = particle.get_mass_index();

        // take care of massive fermions
        if (spin == ParticleStatistics::fermion && include == PropagatorType::propagator &&
            flip(particle.external_state().get_representation()) == repr::undefined)
            return [M](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::Dirac_Fermion_Propagator<T, Tcur, D>(in, prop, l, M, bc);
            };
        if (spin == ParticleStatistics::fermion && include == PropagatorType::cut && flip(particle.external_state().get_representation()) == repr::undefined)
            return [M](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::Cut_Dirac_Fermion_Propagator<T, Tcur, D>(in, prop, l, M, bc);
            };
        if (spin == ParticleStatistics::antifermion && include == PropagatorType::propagator &&
            flip(particle.external_state().get_representation()) == repr::undefined)
            return [M](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::Dirac_AntiFermion_Propagator<T, Tcur, D>(in, prop, l, M, bc);
            };
        if (spin == ParticleStatistics::antifermion && include == PropagatorType::cut &&
            flip(particle.external_state().get_representation()) == repr::undefined)
            return [M](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& l, const BuilderContainer<T>& bc) {
                return BG::Cut_Dirac_AntiFermion_Propagator<T, Tcur, D>(in, prop, l, M, bc);
            };

        static const map_type rules = {

            {{ParticleStatistics::fermion, PropagatorType::propagator, repr::Lat}, BG::Fermion_Propagator<T, Tcur, D, repr::Lat>},
            {{ParticleStatistics::fermion, PropagatorType::cut, repr::Lat}, BG::Cut_Fermion_Propagator<T, Tcur, D, repr::Lat>},
            {{ParticleStatistics::antifermion, PropagatorType::propagator, repr::La}, BG::AntiFermion_Propagator<T, Tcur, D, repr::La>},
            {{ParticleStatistics::antifermion, PropagatorType::cut, repr::La}, BG::Cut_AntiFermion_Propagator<T, Tcur, D, repr::La>},

            {{ParticleStatistics::fermion, PropagatorType::propagator, repr::La}, BG::Fermion_Propagator<T, Tcur, D, repr::La>},
            {{ParticleStatistics::fermion, PropagatorType::cut, repr::La}, BG::Cut_Fermion_Propagator<T, Tcur, D, repr::La>},
            {{ParticleStatistics::antifermion, PropagatorType::propagator, repr::Lat}, BG::AntiFermion_Propagator<T, Tcur, D, repr::Lat>},
            {{ParticleStatistics::antifermion, PropagatorType::cut, repr::Lat}, BG::Cut_AntiFermion_Propagator<T, Tcur, D, repr::Lat>},
        };

        auto r = rules.find(std::make_tuple(spin, include, flip(particle.external_state().get_representation())));

        if (r != rules.end() && particle.is_massless()) {
            DEBUG_MESSAGE("Found propagator for ", particle, " with type ", include, " in D=", D);
            return r->second;
        }
    } // ( D % 2 == 0 )

    _WARNING("Did not find case for computing propagator! Returned identity propagator! Case: ", particle, " ", include, "Ds=", D);
    return [](const std::vector<Tcur>& in, T& prop, const momentumD<T, D>& mom, const BuilderContainer<T>& bc) {
        prop = T(1);
        return in;
    };
}

template <class T, typename Tcur, size_t D> CurrentContraction<T, Tcur, D> get_final_contraction_rule(Current* lc) {
    // statistics of the current
    ParticleStatistics spin(lc->get_statistics());

    switch (spin) {
        case ParticleStatistics::vector:
            return [](const std::vector<Tcur>& c1, const std::vector<Tcur>& c2, const BuilderContainer<T>& bc) {
                return get_e0(contract_vector_currents_from_0_to<D>(c1, c2));
            };

        case ParticleStatistics::tensor:
#if INCLUDE_CUBIC_GRAVITY || INCLUDE_EH_GRAVITY
            return BG::contract_GG<D, T, Tcur>;
#else
        {
            std::cerr << "Requested a tensor-particle contraction without including corresponding gravity model. Consider doing 'meson configure "
                         "-Dgravity-model=Cubic,EH,EH-GB-R3' "
                      << std::endl;
            std::exit(1);
        }
#endif

        case ParticleStatistics::scalar:
            return [](const std::vector<Tcur>& c1, const std::vector<Tcur>& c2, const BuilderContainer<T>& bc) { return get_e0(c1.front() * c2.front()); };

        default: break;
    }

    if constexpr (D % 2 == 0) {
        switch (spin) {

            case ParticleStatistics::antifermion:
                DEBUG_MESSAGE("Final contracion antifermion BB");
                switch (lc->get_current_particle().external_state().get_representation()) {
                    case repr::La: return BG::contract_BB<D, T, Tcur, repr::La>;
                    case repr::Lat: return BG::contract_BB<D, T, Tcur, repr::Lat>;
                    case repr::undefined: return BG::Dirac_contract<D,T,Tcur>;
                    default: break;
                }
                break;

            case ParticleStatistics::fermion:
                DEBUG_MESSAGE("Final contracion fermion AA");
                switch (lc->get_current_particle().external_state().get_representation()) {
                    case repr::La: return BG::contract_AA<D, T, Tcur, repr::La>;
                    case repr::Lat: return BG::contract_AA<D, T, Tcur, repr::Lat>;
                    case repr::undefined: return BG::Dirac_contract<D,T,Tcur>;
                    default: break;
                }
                break;
            default: break;
        }
    } // ( D % 2 == 0 )

    _WARNING("Did not find case for final current contraction! Return always 0! Case: ", spin, " Ds=", D);
    return [](const std::vector<Tcur>& c1, const std::vector<Tcur>& c2, const BuilderContainer<T>& bc) { return T(0); };
}

} // namespace Caravel
