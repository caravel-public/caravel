#include "Forest/Model.h"

namespace Caravel {
// Implementation of pure gravity model
EHGravity::EHGravity() {
    DEBUG_MESSAGE("Here we start constructing EHGravity Model");

    name = "EHGravity";

    using namespace settings::general;
    using namespace Model_Helper;
    // Set power counting
    settings::use_setting("general::power_counting_of_theory Gravity");
    settings::use_setting("IntegrandHierarchy::n_cached_D_values_warmup 60");

    // Kappa coupling
    v_couplings.push_back(Coupling("K"));
    // factorizing 2*i in the coupling (as in CubicGravity)
    v_couplings[0].set_value(2, 0, 1, 0, 1, 1, 1);

    // GB counterterm (no term factorized from it)
    v_couplings.push_back(Coupling("c"));
    // R3 counterterm (no term factorized from it)
    v_couplings.push_back(Coupling("cR3"));

    // particles included

    // internal gravitons
    // Ddim = 4, 5, 6, 7, 8, 9, 10, 11, 12
    add_tensor_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("G"), particle_states);
    add_vector_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("V1"), particle_states);
    add_vector_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("V2"), particle_states);
    add_scalar_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("Phi1"), particle_states);
    add_scalar_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("Phi2"), particle_states);

    // 3-pt vertex
    CurrentRule* GGG = new CurrentRule_GGG();
    v_rules.push_back(GGG);

    // 4-pt vertex
    CurrentRule* GGGG = new CurrentRule_GGGG();
    v_rules.push_back(GGGG);

    // 5-pt vertex
    CurrentRule* GGGGG = new CurrentRule_GGGGG();
    v_rules.push_back(GGGGG);

    // ssG vertex
    CurrentRule* ssG1 = new CurrentRule_ssG1();
    v_rules.push_back(ssG1);
    CurrentRule* ssG2 = new CurrentRule_ssG2();
    v_rules.push_back(ssG2);

    // ssGG vertex
    CurrentRule* ssGG1 = new CurrentRule_ssGG1();
    v_rules.push_back(ssGG1);
    CurrentRule* ssGG2 = new CurrentRule_ssGG2();
    v_rules.push_back(ssGG2);

    // ssGGG vertex
    CurrentRule* ssGGG1 = new CurrentRule_ssGGG1();
    v_rules.push_back(ssGGG1);
    CurrentRule* ssGGG2 = new CurrentRule_ssGGG2();
    v_rules.push_back(ssGGG2);

    // VVG vertex
    CurrentRule* VVG1 = new CurrentRule_VVG1();
    v_rules.push_back(VVG1);
    CurrentRule* VVG2 = new CurrentRule_VVG2();
    v_rules.push_back(VVG2);

    // VVGG vertex
    CurrentRule* VVGG1 = new CurrentRule_VVGG1();
    v_rules.push_back(VVGG1);
    CurrentRule* VVGG2 = new CurrentRule_VVGG2();
    v_rules.push_back(VVGG2);

    // VVGGG vertex
    CurrentRule* VVGGG1 = new CurrentRule_VVGGG1();
    v_rules.push_back(VVGGG1);
    CurrentRule* VVGGG2 = new CurrentRule_VVGGG2();
    v_rules.push_back(VVGGG2);

    // GB terms

    // 3-pt vertex
    CurrentRule* GB_GGG = new CurrentRule_GB_GGG();
    v_rules.push_back(GB_GGG);
    // 4-pt vertex
    CurrentRule* GB_GGGG = new CurrentRule_GB_GGGG();
    v_rules.push_back(GB_GGGG);

    // R3 terms

    // 3-pt vertex
    CurrentRule* R3_GGG = new CurrentRule_R3_GGG();
    v_rules.push_back(R3_GGG);
    // 4-pt vertex
    CurrentRule* R3_GGGG = new CurrentRule_R3_GGGG();
    v_rules.push_back(R3_GGGG);
}

// Implementation of cubic gravity model of Cheung and Remmen
CubicGravity::CubicGravity() {
    DEBUG_MESSAGE("Here we start constructing CubicGravity Model");

    name = "CubicGravity";

    using namespace settings::general;
    using namespace Model_Helper;
    // Set power counting
    // TODO: add way to handle gravity power counting
    settings::use_setting("general::power_counting_of_theory Gravity");
    settings::use_setting("IntegrandHierarchy::n_cached_D_values_warmup 60");

    // Kappa coupling
    v_couplings.push_back(Coupling("K"));
    // factorizing 2*i in the coupling
    v_couplings[0].set_value(2, 0, 1, 0, 1, 1, 1);

    // particles included

    // internal gravitons
    // Ddim = 4, 5, 6, 7, 8, 9, 10, 11, 12
    add_tensor_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("G"), particle_states);

    // 3-pt vertex among gravitons
    CurrentRule* GGG = new CurrentRule_Cubic_GGG();
    v_rules.push_back(GGG);
    // 3-pt vertex with two gravitons and an auxiliary field
    CurrentRule* GGA = new CurrentRule_GGA();
    v_rules.push_back(GGA);
    // 3-pt vertex one graviton, two A's
    CurrentRule* GAA = new CurrentRule_GAA();
    v_rules.push_back(GAA);
}

} // namespace Caravel
