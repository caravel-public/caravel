namespace Caravel {
/**
 * Model implementation for color ordered SM. So far this means:
 *
 * 1) Gluon vertices 'ggg' and 'gggg'
 *
 * 2) Left turner and right turner
 *
 * For vector internal states, works with both settings::general::cut_states::projective (particularly helpful for exact
 * calculations) and settings::general::cut_states::states (which works for floating-point types)
 *
 * For fermions, work is in progress to also handle exact calculations
 */
class SM_color_ordered : public Model {
  public:
    // constructor:
    SM_color_ordered();
    // virtual copy parttern
    SM_color_ordered* clone() const { return new SM_color_ordered(*this); };
    ~SM_color_ordered(){};
};
} // namespace Caravel
