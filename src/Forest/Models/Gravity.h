namespace Caravel {

/**
 * Model implementation for Einstein-Hilbert gravity. Basically considers only
 * gravitons vertices 'GGG' and 'GGGG'. No 5-pt or higher-pt contact terms
 * included so far. Up to 4-pt vertices is what's needed for a 2->2 calculation at 1 loop
 */
class EHGravity : public Model {
  public:
    // Constructor
    EHGravity();
    // virtual copy parttern
    EHGravity* clone() const { return new EHGravity(*this); };
    ~EHGravity(){};
};

/**
 * Model implementation for quantum gravity employing the explicitly cubic action proposed in
 * arXiv:1705.00626. Contains the vertices 'GGG', 'GGA' and 'GAA' where 'G' is a graviton and
 * 'A' is an auxiliary field
 */
class CubicGravity : public Model {
  public:
    // Constructor
    CubicGravity();
    // virtual copy parttern
    CubicGravity* clone() const { return new CubicGravity(*this); };
    ~CubicGravity(){};
};

} // namespace Caravel
