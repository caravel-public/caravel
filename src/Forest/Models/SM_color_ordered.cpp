#include "Forest/Model.h"

namespace Caravel {

// Implementation of color ordered model
SM_color_ordered::SM_color_ordered() {
    DEBUG_MESSAGE("Here we start constructing SM_color_ordered Model");

    name = "SM_color_ordered";

    using namespace settings::general;
    using namespace Model_Helper;
    // Set power counting
    settings::use_setting("general::power_counting_of_theory renormalizable");

    // Strong coupling
    v_couplings.push_back(Coupling("gs"));
    // factorizing i/sqrt(2) in the strong coupling
    v_couplings[0].set_value(1, 0, 1, 0, 1, 2, 1);
    // EW coupling
    v_couplings.push_back(Coupling("gw"));
    // factorizing i in the EW coupling
    v_couplings[1].set_value(1, 0, 1, 0, 1, 1, 1);
    // Top-quark Yukawa coupling
    v_couplings.push_back(Coupling("yt"));
    v_couplings[2].set_value(1, 0, 1, 0, 1, 1, 1);
    // Axuiliary Mass renormalisation coupling (count the numbers of mass insertions)
    v_couplings.push_back(Coupling("zm"));
    v_couplings[3].set_value(1, 0, 1, 0, 1, 1, 1);

    // particles included

    // gluon
    add_vector_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("gluon"), particle_states);

    // photon in 4 dim
    add_vector_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("photon"), particle_states);

    // fermions
    add_fermionic_states<4, 6, 8, 10, 12>(ParticleType("q"), particle_states);
    add_fermionic_states<4, 6, 8, 10, 12>(ParticleType("t"), particle_states);

    add_fermionic_states<4, 6, 8, 10, 12>({ParticleStatistics::fermion, ParticleFlavor::e, ParticleMass{}, false}, particle_states);
    add_fermionic_states<4, 6, 8, 10, 12>({ParticleStatistics::fermion, ParticleFlavor::ve, ParticleMass{}, false}, particle_states);

#ifdef DS_BY_PARTICLE_CONTENT
    add_scalar_states<6>(ParticleType(ParticleStatistics::scalar, ParticleFlavor::ds_scalar, ParticleMass{}, true), particle_states);
#endif

    // three point gluon rule color ordered
    CurrentRule* ggg = new CurrentRule_ggg();
    v_rules.push_back(ggg);
    // four point gluon rule color ordered
    CurrentRule* gggg = new CurrentRule_gggg();
    v_rules.push_back(gggg);

    // three point gluon-q-qb color ordered rule
    CurrentRule* gqqL = new CurrentRule_gqqL();
    v_rules.push_back(gqqL);
    // three point gluon-q-qb color ordered rule
    CurrentRule* gqqR = new CurrentRule_gqqR();
    v_rules.push_back(gqqR);

    // three point gluon-t-tb color ordered rule
    CurrentRule* gttL = new CurrentRule_gttL();
    v_rules.push_back(gttL);
    // three point gluon-t-tb color ordered rule
    CurrentRule* gttR = new CurrentRule_gttR();
    v_rules.push_back(gttR);

    // Higgs and two tops
    CurrentRule* Htt = new CurrentRule_Htt();
    v_rules.push_back(Htt);

    // Mass Renormalisation
    CurrentRule* Zm = new CurrentRule_Zm();
    v_rules.push_back(Zm);

#ifdef DS_BY_PARTICLE_CONTENT
    v_rules.push_back(new CurrentRule_gss);
    v_rules.push_back(new CurrentRule_ggss);
    v_rules.push_back(new CurrentRule_gsgs);
    v_rules.push_back(new CurrentRule_ssss);
    v_rules.push_back(new CurrentRule_sqqL);
    v_rules.push_back(new CurrentRule_sqqR);
#endif

    // three point photon-q-qb rule
    CurrentRule* pqq = new CurrentRule_pqq();
    v_rules.push_back(pqq);

    // three point photon-l-lb rule
    CurrentRule* pll = new CurrentRule_pll();
    v_rules.push_back(pll);

    v_rules.push_back(new CurrentRule_Wm_u_dbar);
    v_rules.push_back(new CurrentRule_Wp_ubar_d);
    v_rules.push_back(new CurrentRule_Wm_ebar_ve);
    v_rules.push_back(new CurrentRule_Wp_e_vebar);
#if 0
	// three point photon (fake) rule
	CurrentRule * PPP = new CurrentRulePPP();
	v_rules.push_back(PPP);

	// three point photon-u-ub rule
	CurrentRule * PUU = new CurrentRulePUU();
	v_rules.push_back(PUU);
#endif
}

} // namespace Caravel
