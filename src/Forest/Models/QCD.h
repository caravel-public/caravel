namespace Caravel {
/**
 * Model implementation for color ordered pure gluonic theory. Basically considers only
 * gluons with the three-pt vertex 'ggg' and the four-pt vertex 'gggg'
 */
class YM : public Model {
  public:
    // Constructor
    YM();
    // virtual copy parttern
    YM* clone() const { return new YM(*this); };
    ~YM(){};
};

/**
 * Model implementation for color ordered pure gluonic theory, including auxiliary tensor vertices
 * to avoid a 4-gluon vertex.
 */
class YMwTensor : public Model {
  public:
    // Constructor
    YMwTensor();
    // virtual copy parttern
    YMwTensor* clone() const { return new YMwTensor(*this); };
    ~YMwTensor(){};
};

} // namespace Caravel
