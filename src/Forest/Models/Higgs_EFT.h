namespace Caravel {

/**
 * Model implementation for Higgs efective field theory. So far this means:
 *
 * 1) Gluon vertices 'ggg' and 'gggg'
 *
 * 2) Higgs gluon vertices 'Hgg', 'Hggg' and 'Hgggg'
 *
 * 3) A quark flavor that does not couples to the Higgs (more flavors can be added if needed)
 *
 * For vector internal states, works with both settings::general::cut_states::projective (particularly helpful for exact
 * calculations) and settings::general::cut_states::states (which works for floating-point types)
 *
 * For fermions, work is in progress to also handle exact calculations
 */
class Higgs_EFT : public Model {
  public:
    // constructor:
    Higgs_EFT();
    // virtual copy parttern
    Higgs_EFT* clone() const { return new Higgs_EFT(*this); };
    ~Higgs_EFT(){};
};
} // namespace Caravel
