#include "Forest/Model.h"

namespace Caravel {
// Implementation of Higgs effective field theory
Higgs_EFT::Higgs_EFT() {
    DEBUG_MESSAGE("Here we start constructing Higgs_EFT Model");

    name = "Higgs_EFT";

    using namespace settings::general;
    using namespace Model_Helper;
    // Set power counting
    settings::use_setting("general::power_counting_of_theory Higgs_EFT_plus_one");

    // Strong coupling and there is implicit a y_t per Higgs present
    v_couplings.push_back(Coupling("gs"));
    // factorizing i/sqrt(2) in the strong coupling
    v_couplings[0].set_value(1, 0, 1, 0, 1, 2, 1);
    // EW coupling
    v_couplings.push_back(Coupling("gw"));
    // factorizing i in the EW coupling
    v_couplings[1].set_value(1, 0, 1, 0, 1, 1, 1);

    // particles included

    // internal gluons
    add_vector_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("gluon"), particle_states);

    // internal fermions
    add_fermionic_states<4, 6, 8, 10, 12>(ParticleType("q"), particle_states);

    // three point gluon rule color ordered
    CurrentRule* ggg = new CurrentRule_ggg();
    v_rules.push_back(ggg);
    // four point gluon rule color ordered
    CurrentRule* gggg = new CurrentRule_gggg();
    v_rules.push_back(gggg);

    // three point gluon-q-qb color ordered rule
    CurrentRule* gqqL = new CurrentRule_gqqL();
    v_rules.push_back(gqqL);
    // three point gluon-q-qb color ordered rule
    CurrentRule* gqqR = new CurrentRule_gqqR();
    v_rules.push_back(gqqR);

    // Higgs and two gluons
    CurrentRule* Hgg = new CurrentRule_Hgg();
    v_rules.push_back(Hgg);

    // Higgs and three gluons
    CurrentRule* Hggg = new CurrentRule_Hggg();
    v_rules.push_back(Hggg);

    // Higgs and four gluons
    CurrentRule* Hgggg = new CurrentRule_Hgggg();
    v_rules.push_back(Hgggg);
}

} // namespace Caravel
