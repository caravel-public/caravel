#include "Forest/Model.h"

namespace Caravel {

// Implementation of color ordered pure gluonic model
YM::YM() {
    DEBUG_MESSAGE("Here we start constructing YM Model");

    name = "YM";

    using namespace settings::general;
    using namespace Model_Helper;
    // Set power counting
    settings::use_setting("general::power_counting_of_theory renormalizable");

    // Strong coupling
    v_couplings.push_back(Coupling("gs"));
    // factorizing i/sqrt(2) in the strong coupling
    v_couplings[0].set_value(1, 0, 1, 0, 1, 2, 1);

    // particles included

    // gluon
    add_vector_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("gluon"), particle_states);
#ifdef DS_BY_PARTICLE_CONTENT
    add_scalar_states<6>(ParticleType(ParticleStatistics::scalar, ParticleFlavor::ds_scalar, ParticleMass{}, true), particle_states);
#endif

    // three point gluon rule color ordered
    CurrentRule* ggg = new CurrentRule_ggg();
    v_rules.push_back(ggg);
    // four point gluon rule color ordered
    CurrentRule* gggg = new CurrentRule_gggg();
    v_rules.push_back(gggg);

#ifdef DS_BY_PARTICLE_CONTENT
    v_rules.push_back(new CurrentRule_gss);
    v_rules.push_back(new CurrentRule_ggss);
    v_rules.push_back(new CurrentRule_gsgs);
    v_rules.push_back(new CurrentRule_ssss);
#endif
}

// Implementation of color ordered pure gluonic model without 4-pt vertex
YMwTensor::YMwTensor() {
    DEBUG_MESSAGE("Here we start constructing YMwTensor Model");

    name = "YMwTensor";

    using namespace settings::general;
    using namespace Model_Helper;
    // Set power counting
    settings::use_setting("general::power_counting_of_theory renormalizable");

    // Strong coupling
    v_couplings.push_back(Coupling("gs"));
    // factorizing i/sqrt(2) in the strong coupling
    v_couplings[0].set_value(1, 0, 1, 0, 1, 2, 1);

    // particles included

    // gluon
    // Ddim = 4, 5, 6, 7, 8, 9, 10, 11, 12
    add_vector_boson_states<4, 5, 6, 7, 8, 9, 10, 11, 12>(ParticleType("gluon"), particle_states);

    // three point gluon rule color ordered
    CurrentRule* ggg = new CurrentRule_ggg();
    v_rules.push_back(ggg);
    // gluon-gluon-tensor interaction
    CurrentRule* ggT = new CurrentRule_ggT();
    v_rules.push_back(ggT);
}

} // namespace Caravel
