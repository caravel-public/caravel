/**
 * @file Model.h
 *
 * @date 26.2.2016
 *
 * @brief Header file for models to be handled
 *
 * Definition of classes CurrentRule (Feynman rules) and Model (theories) for generic calculation in Caravel through polymorphism.
 * Until now, three Model's (theories) defined YM, SM_color_ordered
 * Below, this header includes templated code for defining states (States_Vector_Boson.hpp, States_Model_Projective.hpp) and
 * for kinematic rules of CurrentRule's (CurrentRules_GGG_GGGG.hpp)
 *
 * We also include Model.hpp below, that implements the templated functions get_external_rule and get_combine_rule which
 * distribute states and Feynman rules respectively
 *
 * Instantiations and other non-templated implementations are found in Model.cpp
 *
*/
#pragma once

#include "Core/Particle.h"
#include "Forest/Coupling.h"
#include "Forest/CurrentRule.h"
#include <cstdlib>
#include <map>
#include <string>
#include <utility>
#include <vector>

// TODO: ADD YOUR CURRENTS HERE
#include "Forest/CurrentRules/currents_Gravity.h"
#include "Forest/CurrentRules/currents_HEFT.h"
#include "Forest/CurrentRules/currents_SM.h"
#include "Forest/CurrentRules/currents_ds_scalar.h"

namespace Caravel {

/**
 * Class to handle particles included in a model and the physical states that they can hold in D dimensions
 */
class ParticleStates {
    std::vector<ParticleType> types_of_particles; /**< Contains types of particles included in model */
    std::vector<ParticleType>
        types_of_antiparticles; /**< Contains types of corresponding antiparticles included in model. This is stored separately for convenience */
    std::vector<std::vector<size_t>> dimensions; /**< The dimensions the states are defined */
    std::vector<std::vector<std::vector<std::pair<SingleState, SingleState>>>>
        states; /**< For each particle and each dimension, a list of correlated states (particle,anti-particle) */
  public:
    ParticleStates() = default;
    /**
     * Method to add a particle/dimension/state to *this
     *
     * @param ParticleType representing the type the particle
     * @param size_t for the dimension
     * @param std::vector of std::pair's that represent correlated states (particle/anti-particle)
     */
    void add_state(const ParticleType&, size_t, const std::vector<std::pair<SingleState, SingleState>>&);
    /**
     * To get states for a particle and a given dimension
     *
     * @param ParticleType representing the particle (or the anti-particle, in which case the correlated states are reversed)
     * @param size_t for the requested dimension of the states
     */
    std::vector<std::pair<SingleState, SingleState>> get_states(const ParticleType&, size_t) const;
};

/**
 * Abstract class for models to be implemented - will give Feynman rules, color, couplings, etc
 */
class Model {
  protected:
    std::vector<Coupling> v_couplings; /**< Couplings tracked through out the Model */
    std::vector<CurrentRule*> v_rules; /**< Owned pointers to CurrentRule's included in *this. We handle memory to allow polymorphism */
    ParticleStates particle_states;    /**< Contains information on the particles included and the states they can have */
    std::string name;                  /**< Name of the model */
  public:
    // default
    Model() = default;

    std::string get_name() const { return name; }
    /**
     * Gives access to the couplings included in the Model
     */
    const std::vector<Coupling>& v_of_couplings() const;
    /**
     * Gives access to the CurrentRule's (Feynman rules) in *this
     */
    const std::vector<CurrentRule*>& v_of_rules() const;
    /**
     * To get states for a particle and a given dimension
     *
     * @param ParticleType representing the particle (or the anti-particle, in which case the correlated states are reversed)
     * @param size_t for the requested dimension of the states
     */
    std::vector<std::pair<SingleState, SingleState>> get_states(const ParticleType&, size_t) const;
    /**
     * Explicit copy constructor. As we handle memory, it is important to correctly copy v_rules
     */
    Model(const Model&);
    /**
     * Assignment operator. As we handle memory, it is important to correctly copy v_rules
     */
    Model& operator=(const Model&);
    /**
     * Virtual copy pattern to copy a Model
     */
    virtual Model* clone() const { return new Model(*this); };
    /**
     * destructor
     */
    virtual ~Model();
};

/**
 * A helper function which constructs a particular model from it's name (string ID).
 * New models have to be added manually.
 * The pointer is not owned by this function and should be managed by the caller.
 */
Model* model_from_name(std::string name);

namespace Model_Helper {
// Vector Bosons
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_vector_boson_states_impl();
template <unsigned Ds> void add_vector_boson_states(const ParticleType& p, ParticleStates& particle_states);
template <unsigned Ds, unsigned... rest, typename = typename std::enable_if<sizeof...(rest) >= 1>::type>
void add_vector_boson_states(const ParticleType& p, ParticleStates& particle_states);
// Fermion
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_fermionic_states_impl();
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_fermionic_dirac_states_impl();
template <unsigned Dim> void add_fermionic_states(const ParticleType& p, ParticleStates& particle_states);
template <unsigned Dim, unsigned... rest, typename = typename std::enable_if<sizeof...(rest) >= 1>::type>
void add_fermionic_states(const ParticleType& p, ParticleStates& particle_states);
// Scalar
template <unsigned Dim> void add_scalar_states(const ParticleType& p, ParticleStates& particle_states);
template <unsigned Dim, unsigned... rest, typename = typename std::enable_if<sizeof...(rest) >= 1>::type>
void add_scalar_states(const ParticleType& p, ParticleStates& particle_states);
// Tensors
template <unsigned Ds> std::vector<std::pair<SingleState, SingleState>> add_tensor_boson_states_impl();
template <unsigned Ds> void add_tensor_boson_states(const ParticleType& p, ParticleStates& particle_states);
template <unsigned Ds, unsigned... rest, typename = typename std::enable_if<sizeof...(rest) >= 1>::type>
void add_tensor_boson_states(const ParticleType& p, ParticleStates& particle_states);
} // namespace Model_Helper

} // namespace Caravel

// TODO: REGISTER YOUR MODEL HERE
#include "Models/Gravity.h"
#include "Models/Higgs_EFT.h"
#include "Models/QCD.h"
#include "Models/SM_color_ordered.h"


// IMPLEMENTATIONS
// FIXME: Do NOT clang format this! Order matters!
#include "Contact/Utilities.hpp"
#include "Contact/BG_Fermions.hpp"
#include "Contact/BG_Fermions_Dirac.hpp"
#include "Contact/States_Vector_Boson.hpp"
#include "Contact/States_Model_Projective.hpp"
#include "Contact/States_Massive_Vector_Boson.hpp"
#include "Contact/CurrentRules_ggT.hpp"
#include "Contact/CurrentRules_ggg_gggg.hpp"
#include "Contact/BG_Scalars.hpp"
#include "Contact/BG_Vectors.hpp"

#if INCLUDE_CUBIC_GRAVITY || INCLUDE_EH_GRAVITY
#include "Contact/Graviton_states_propagators.hpp"
#endif

#ifdef INCLUDE_CUBIC_GRAVITY
#include "Contact/BG_Gravitons.hpp"
#endif

#ifdef INCLUDE_EH_GRAVITY
#include "Contact/BG_Gravitons_EH.hpp"
#include "Contact/BG_Gravitons_EH_Scalars.hpp"
#include "Contact/BG_Gravitons_EH_Vectors.hpp"
#endif

#include "Model.hpp"


