/**
 * Implementation of rules for EHGraviton model
 */

#include "Core/extended_t.h"
#include "Tensor.hpp"
#include <vector>

#define pow2(x) (x * x)
#define pow3(x) (x * x * x)

namespace Caravel {
namespace BG {

/**
 * Vertex function that combines two gravitons into a graviton
 * Note: Caravel removes a factor of 2*I*kappa from the Vertex rule
 **/
template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CurrentRule_EH_GGG(const std::vector<momentumD<T, D> const*>& momlist,
                                     const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {

    // incoming momenta
    std::vector<momD<T, D>> k;
    k.emplace_back(-(*(momlist[0])));
    k.emplace_back(-(*(momlist[1])));

    // the input gravitons
    std::vector<Tensor<T, D, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[0]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[1]->get_current()));

    // metric
    Tensor<T, D, 2> Hout(0), g(0);
    const std::vector<T>& metric(Hout.get_metric_extended());
    for (size_t ii = 0; ii < D; ii++) g[ii][ii] = T(1) / metric[ii];

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1}, {1, 0}};

    // build Tensors
    using T2array1D = std::vector<Tensor<T, D, 2>>;
    using T2array2D = std::vector<T2array1D>;

    using T1array1D = std::vector<Tensor<T, D, 1>>;
    using T1array2D = std::vector<T1array1D>;
    using T1array3D = std::vector<T1array2D>;

    T2array2D HH(2, T2array1D(2, Tensor<T, D, 2>(0)));
    T1array3D HHk(2, T1array2D(2, T1array1D(2, Tensor<T, D, 1>(0))));
    T1array2D Hk(2, T1array1D(2, Tensor<T, D, 1>(0)));

    for (size_t ip = 0; ip < 2; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1];
        HH[p1][p2] = index_contraction<1, 0>(H[p1], H[p2]);
        for (size_t ii = 0; ii < 2; ii++) {
            Hk[p1][ii] = index_contraction<1>(k[ii], H[p1]);
            HHk[p1][p2][ii] = index_contraction<1>(k[ii], HH[p1][p2]);
        }
    }

    // build scalars
    using array1D = std::vector<T>;
    using array2D = std::vector<array1D>;
    using array3D = std::vector<array2D>;
    using array4D = std::vector<array3D>;
    array2D kk(2, array1D(2, T(0)));
    array3D kHk(2, array2D(2, array1D(2, T(0))));
    array4D kHHk(2, array3D(2, array2D(2, array1D(2, T(0)))));
    array1D trH(2, T(0));
    array2D trHH(2, array1D(2, T(0)));
    for (size_t ip = 0; ip < 2; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1];
        trH[p1] = trace_rank2(H[p1]);
        trHH[p1][p2] = trace_rank2(HH[p1][p2]);
        for (size_t ii = 0; ii < 2; ii++) {
            Tensor<T, D, 1> ki = Tensor<T, D, 1>(k[ii]);
            for (size_t jj = 0; jj < 2; jj++) {
                kk[ii][jj] = k[ii] * k[jj];
                kHk[ii][p1][jj] = ki * Hk[p1][jj];
                kHHk[ii][p1][p2][jj] = ki * HHk[p1][p2][jj];
            }
        }
    }

    for (size_t a = 0; a < D; a++) {
        for (size_t b = a; b < D; b++) {
            T theentry = T(0);
            for (size_t ip = 0; ip < 2; ip++) {
                size_t p1 = perm[ip][0], p2 = perm[ip][1];

                theentry +=
                    -(k[p1][a] * k[p2][b] * trHH[p1][p2]) - T(2) * k[p2][a] * k[p2][b] * trHH[p2][p1] +
                    trH[p2] * ((kk[p1][p2] + T(2) * kk[p2][p2]) * H[p1][a][b] - (k[p1][b] + T(2) * k[p2][b]) * Hk[p1][p2][a] -
                               (k[p1][a] + T(2) * k[p2][a]) * Hk[p1][p2][b]) +
                    Hk[p1][p2][b] * (Hk[p2][p1][a] + T(2) * Hk[p2][p2][a]) + Hk[p1][p2][a] * (Hk[p2][p1][b] + T(2) * Hk[p2][p2][b]) +
                    trH[p1] * (trH[p2] * k[p2][a] * k[p2][b] + kk[p2][p2] * H[p2][b][a] - k[p2][b] * Hk[p2][p2][a] - k[p2][a] * Hk[p2][p2][b]) -
                    T(2) * (H[p2][a][b] * kHk[p2][p1][p1] + H[p2][b][a] * kHk[p2][p1][p2] + H[p1][a][b] * kHk[p2][p2][p2] + kk[p1][p2] * HH[p1][p2][b][a] +
                            kk[p2][p2] * (HH[p1][p2][a][b] + HH[p1][p2][b][a]) - (k[p1][b] + k[p2][b]) * HHk[p1][p2][p2][a] -
                            (k[p1][a] + k[p2][a]) * HHk[p1][p2][p2][b] - k[p2][b] * HHk[p2][p1][p2][a] - k[p2][a] * HHk[p2][p1][p2][b]) +
                    g[a][b] *
                        ((T(3) * kk[p1][p2] * trHH[p1][p2]) / T(2) + T(2) * kk[p2][p2] * trHH[p2][p1] + T(2) * trH[p2] * (kHk[p2][p1][p1] + kHk[p2][p1][p2]) +
                         trH[p1] * (trH[p2] * (-(kk[p1][p2] / T(2)) - kk[p2][p2]) + kHk[p2][p2][p2]) - T(2) * kHHk[p1][p1][p2][p2] - kHHk[p2][p1][p2][p1] -
                         T(2) * (kHHk[p2][p1][p2][p2] + kHHk[p2][p2][p1][p2]));
            }
            Hout[a][b] = theentry / T(2);
            Hout[b][a] = Hout[a][b];
        }
    }

    return v_t_to_v_extended_t<T, Tcur>(Hout.vectorize());
}

/**
 * Vertex function that combines three gravitons into a graviton
 * Note: Caravel removes a factor of 2*I*kappa from the Vertex rule
 **/
template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CurrentRule_EH_GGGG(const std::vector<momentumD<T, D> const*>& momlist,
                                      const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {

    // incoming momenta
    std::vector<momD<T, D>> k;
    k.emplace_back(-(*(momlist[0])));
    k.emplace_back(-(*(momlist[1])));
    k.emplace_back(-(*(momlist[2])));

    // the input gravitons
    std::vector<Tensor<T, D, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[0]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[2]->get_current()));

    // metric
    Tensor<T, D, 2> Hout(0), g(0);
    const std::vector<T>& metric(Hout.get_metric_extended());
    for (size_t ii = 0; ii < D; ii++) g[ii][ii] = T(1) / metric[ii];

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 1, 0}, {2, 0, 1}};

    // build Tensors
    using T2array1D = std::vector<Tensor<T, D, 2>>;
    using T2array2D = std::vector<T2array1D>;
    using T2array3D = std::vector<T2array2D>;

    using T1array1D = std::vector<Tensor<T, D, 1>>;
    using T1array2D = std::vector<T1array1D>;
    using T1array3D = std::vector<T1array2D>;
    using T1array4D = std::vector<T1array3D>;

    T2array3D HHH(3, T2array2D(3, T2array1D(3, Tensor<T, D, 2>(0))));
    T2array2D HH(3, T2array1D(3, Tensor<T, D, 2>(0)));
    T1array4D HHHk(3, T1array3D(3, T1array2D(3, T1array1D(3, Tensor<T, D, 1>(0)))));
    T1array3D HHk(3, T1array2D(3, T1array1D(3, Tensor<T, D, 1>(0))));
    T1array2D Hk(3, T1array1D(3, Tensor<T, D, 1>(0)));

    for (size_t ip = 0; ip < 6; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2];
        HH[p1][p2] = index_contraction<1, 0>(H[p1], H[p2]);
        HHH[p1][p2][p3] = index_contraction<1, 0>(HH[p1][p2], H[p3]);
        for (size_t ii = 0; ii < 3; ii++) {
            Hk[p1][ii] = index_contraction<1>(k[ii], H[p1]);
            HHk[p1][p2][ii] = index_contraction<1>(k[ii], HH[p1][p2]);
            HHHk[p1][p2][p3][ii] = index_contraction<1>(k[ii], HHH[p1][p2][p3]);
        }
    }

    // build scalars
    using array1D = std::vector<T>;
    using array2D = std::vector<array1D>;
    using array3D = std::vector<array2D>;
    using array4D = std::vector<array3D>;
    using array5D = std::vector<array4D>;
    array5D kHHHk(3, array4D(3, array3D(3, array2D(3, array1D(3, T(0))))));
    array4D kHHk(3, array3D(3, array2D(3, array1D(3, T(0)))));
    array3D kHk(3, array2D(3, array1D(3, T(0))));
    array2D kk(3, array1D(3, T(0)));
    array3D trHHH(3, array2D(3, array1D(3, T(0))));
    array2D trHH(3, array1D(3, T(0)));
    array1D trH(3, T(0));

    for (size_t ip = 0; ip < 6; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2];
        trH[p1] = trace_rank2(H[p1]);
        trHH[p1][p2] = trace_rank2(HH[p1][p2]);
        trHHH[p1][p2][p3] = trace_rank2(HHH[p1][p2][p3]);
        for (size_t ii = 0; ii < 3; ii++) {
            Tensor<T, D, 1> ki = Tensor<T, D, 1>(k[ii]);
            for (size_t jj = 0; jj < 3; jj++) {
                kk[ii][jj] = k[ii] * k[jj];
                kHk[ii][p1][jj] = ki * Hk[p1][jj];
                kHHk[ii][p1][p2][jj] = ki * HHk[p1][p2][jj];
                kHHHk[ii][p1][p2][p3][jj] = ki * HHHk[p1][p2][p3][jj];
            }
        }
    }

    for (size_t a = 0; a < D; a++) {
        for (size_t b = a; b < D; b++) {
            T theentry = T(0);
            for (size_t ip = 0; ip < 6; ip++) {
                size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2];

                theentry +=
                    trHH[p2][p3] * (-((T(9) * kk[p2][p3] * H[p1][a][b]) / T(2)) + T(3) * k[p2][b] * Hk[p1][p3][a] + T(3) * k[p2][a] * Hk[p1][p3][b]) +
                    trHH[p3][p2] * (-(T(6) * kk[p3][p3] * H[p1][a][b]) + T(6) * k[p3][b] * Hk[p1][p3][a] + T(6) * k[p3][a] * Hk[p1][p3][b]) +
                    trHH[p3][p1] * (-(T(3) * kk[p2][p3] * H[p2][a][b]) + T(3) * k[p2][b] * Hk[p2][p3][a] + T(3) * k[p2][a] * Hk[p2][p3][b]) -
                    (T(3) * trHH[p2][p1] * (kk[p3][p3] * H[p3][b][a] - k[p3][b] * Hk[p3][p3][a] - k[p3][a] * Hk[p3][p3][b])) / T(2) +
                    trH[p2] * (trH[p3] * ((T(3) * (kk[p2][p3] + T(2) * kk[p3][p3]) * H[p1][a][b]) / T(2) -
                                          T(3) * (k[p3][b] * Hk[p1][p3][a] + k[p3][a] * Hk[p1][p3][b])) +
                               T(3) * (Hk[p1][p3][b] * Hk[p3][p3][a] + Hk[p1][p3][a] * Hk[p3][p3][b] - H[p1][a][b] * kHk[p3][p3][p3] -
                                       kk[p3][p3] * (HH[p1][p3][a][b] + HH[p1][p3][b][a]) + k[p3][b] * HHk[p1][p3][p3][a] + k[p3][a] * HHk[p1][p3][p3][b])) +
                    trH[p3] * (-((T(3) * k[p3][a] * k[p3][b] * trHH[p2][p1]) / T(2)) +
                               T(3) * (Hk[p1][p2][b] * Hk[p2][p3][a] + (Hk[p1][p2][a] + T(2) * Hk[p1][p3][a]) * Hk[p2][p3][b] - H[p2][a][b] * kHk[p3][p1][p2] -
                                       T(2) * H[p1][a][b] * (kHk[p3][p2][p2] + kHk[p3][p2][p3]) - (kk[p2][p3] + T(2) * kk[p3][p3]) * HH[p1][p2][a][b] -
                                       kk[p2][p3] * HH[p1][p2][b][a] + (k[p2][b] + T(2) * k[p3][b]) * HHk[p1][p2][p3][a] +
                                       (k[p2][a] + T(2) * k[p3][a]) * HHk[p1][p2][p3][b] + k[p2][b] * HHk[p2][p1][p3][a] + k[p2][a] * HHk[p2][p1][p3][b])) +
                    trH[p1] * (-((T(3) * k[p2][a] * k[p3][b] * trHH[p2][p3]) / T(2)) - T(3) * k[p3][a] * k[p3][b] * trHH[p3][p2] +
                               (T(3) * trH[p3] * (kk[p2][p3] * H[p2][a][b] - k[p2][b] * Hk[p2][p3][a] - k[p2][a] * Hk[p2][p3][b])) / T(2) +
                               trH[p2] * ((T(3) * trH[p3] * k[p3][a] * k[p3][b]) / T(4) +
                                          (T(3) * (kk[p3][p3] * H[p3][b][a] - k[p3][b] * Hk[p3][p3][a] - k[p3][a] * Hk[p3][p3][b])) / T(4)) +
                               (T(3) * (Hk[p2][p3][b] * Hk[p3][p2][a] + Hk[p2][p3][a] * Hk[p3][p2][b] +
                                        T(2) * (-(H[p3][a][b] * kHk[p3][p2][p2]) - H[p3][b][a] * kHk[p3][p2][p3] - kk[p2][p3] * HH[p2][p3][b][a] +
                                                k[p2][b] * HHk[p2][p3][p3][a] + k[p2][a] * HHk[p2][p3][p3][b] + k[p3][b] * HHk[p3][p2][p3][a] +
                                                k[p3][a] * HHk[p3][p2][p3][b]))) /
                                   T(2)) +
                    T(3) * (T(2) * kHk[p3][p3][p3] * HH[p1][p2][a][b] + T(2) * (kHk[p3][p2][p2] + kHk[p3][p2][p3]) * (HH[p1][p3][a][b] + HH[p1][p3][b][a]) +
                            (kHk[p2][p1][p3] + kHk[p3][p1][p2]) * HH[p2][p3][b][a] - T(2) * Hk[p3][p3][b] * HHk[p1][p2][p3][a] -
                            (Hk[p3][p2][a] + T(2) * Hk[p3][p3][a]) * HHk[p1][p2][p3][b] - Hk[p2][p3][a] * HHk[p1][p3][p2][b] -
                            Hk[p3][p2][b] * (HHk[p1][p2][p3][a] + HHk[p2][p1][p3][a]) - T(2) * Hk[p3][p2][a] * HHk[p2][p1][p3][b] -
                            T(2) * Hk[p1][p2][b] * HHk[p2][p3][p3][a] - T(2) * (Hk[p1][p2][a] + Hk[p1][p3][a]) * HHk[p2][p3][p3][b] -
                            Hk[p2][p3][b] * (HHk[p1][p3][p2][a] + T(2) * HHk[p1][p3][p3][a] + HHk[p3][p1][p2][a]) - T(2) * Hk[p1][p3][b] * HHk[p3][p2][p3][a] -
                            T(2) * Hk[p1][p3][a] * HHk[p3][p2][p3][b] + H[p2][a][b] * kHHk[p2][p3][p1][p3] +
                            H[p3][a][b] * (kHHk[p2][p1][p2][p3] + T(2) * kHHk[p3][p1][p2][p2]) + T(2) * H[p3][b][a] * kHHk[p3][p1][p2][p3] +
                            H[p1][a][b] * (T(2) * kHHk[p2][p2][p3][p3] + kHHk[p3][p2][p3][p2] + T(2) * (kHHk[p3][p2][p3][p3] + kHHk[p3][p3][p2][p3])) +
                            T(2) * kk[p3][p3] * (HHH[p1][p2][p3][a][b] + HHH[p1][p2][p3][b][a] + HHH[p2][p3][p1][b][a]) +
                            kk[p2][p3] * (T(2) * HHH[p1][p2][p3][b][a] + T(2) * HHH[p1][p3][p2][a][b] + HHH[p2][p1][p3][b][a] + HHH[p3][p1][p2][a][b]) -
                            T(2) * k[p2][b] * (HHHk[p1][p2][p3][p3][a] + HHHk[p2][p1][p3][p3][a] + HHHk[p2][p3][p1][p3][a]) +
                            k[p2][a] * (k[p3][b] * (trHHH[p1][p3][p2] + trHHH[p3][p1][p2]) -
                                        T(2) * (HHHk[p1][p2][p3][p3][b] + HHHk[p2][p1][p3][p3][b] + HHHk[p2][p3][p1][p3][b])) +
                            T(2) * k[p3][a] * (k[p3][b] * trHHH[p1][p2][p3] - HHHk[p1][p2][p3][p3][b] - HHHk[p1][p3][p2][p3][b] - HHHk[p3][p1][p2][p3][b]) -
                            T(2) * k[p3][b] * (HHHk[p1][p2][p3][p3][a] + HHHk[p1][p3][p2][p3][a] + HHHk[p3][p2][p1][p3][a])) +
                    g[a][b] *
                        (-((T(3) * trHH[p2][p3] * (T(2) * kHk[p2][p1][p3] + kHk[p3][p1][p2])) / T(2)) - T(3) * trHH[p3][p2] * kHk[p3][p1][p3] -
                         T(3) * trHH[p1][p3] * kHk[p3][p2][p2] - T(3) * trHH[p3][p1] * (kHk[p3][p2][p2] + kHk[p3][p2][p3]) -
                         (T(3) * trHH[p2][p1] * kHk[p3][p3][p3]) / T(2) +
                         trH[p2] *
                             ((T(3) * kk[p2][p3] * trHH[p3][p1]) / T(2) + (T(3) * trH[p3] * kHk[p3][p1][p2]) / T(2) - (T(3) * kHHk[p2][p3][p1][p3]) / T(2)) +
                         trH[p3] *
                             ((T(3) * kk[p2][p3] * trHH[p1][p2]) / T(2) + (T(3) * kk[p3][p3] * trHH[p2][p1]) / T(2) -
                              (T(3) * (T(2) * kHHk[p2][p1][p2][p3] + T(4) * (kHHk[p3][p1][p2][p2] + kHHk[p3][p1][p2][p3]) + kHHk[p3][p2][p1][p2])) / T(2)) +
                         trH[p1] *
                             ((T(9) * kk[p2][p3] * trHH[p2][p3]) / T(4) + T(3) * kk[p3][p3] * trHH[p3][p2] +
                              T(3) * trH[p3] * (kHk[p3][p2][p2] + kHk[p3][p2][p3]) +
                              trH[p2] * (-((T(3) * trH[p3] * (kk[p2][p3] + kk[p3][p3])) / T(4)) + (T(3) * kHk[p3][p3][p3]) / T(4)) -
                              (T(3) * (T(2) * kHHk[p2][p2][p3][p3] + kHHk[p3][p2][p3][p2] + T(2) * (kHHk[p3][p2][p3][p3] + kHHk[p3][p3][p2][p3]))) / T(2)) +
                         (T(3) *
                          (-(T(4) * kk[p3][p3] * trHHH[p1][p2][p3]) - kk[p2][p3] * (trHHH[p1][p2][p3] + T(2) * trHHH[p1][p3][p2] + T(3) * trHHH[p3][p1][p2]) +
                           T(4) * kHHHk[p2][p1][p2][p3][p3] + T(4) * kHHHk[p2][p2][p1][p3][p3] + T(3) * kHHHk[p2][p3][p2][p1][p3] +
                           T(4) * kHHHk[p3][p1][p2][p3][p3] + T(4) * kHHHk[p3][p1][p3][p2][p2] + T(4) * kHHHk[p3][p1][p3][p2][p3] +
                           T(2) * kHHHk[p3][p2][p1][p3][p2] + T(4) * kHHHk[p3][p2][p1][p3][p3] + kHHHk[p3][p2][p3][p1][p2])) /
                             T(2));
            }
            Hout[a][b] = theentry / T(-6);
            Hout[b][a] = Hout[a][b];
        }
    }

    return v_t_to_v_extended_t<T, Tcur>(Hout.vectorize());
}

/**
 * Vertex function that combines four gravitons into a graviton
 * Note: Caravel removes a factor of 2*I*kappa from the Vertex rule
 **/
template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CurrentRule_EH_GGGGG(const std::vector<momentumD<T, D> const*>& momlist,
                                       const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {

    // incoming momenta
    std::vector<momD<T, D>> k;
    k.emplace_back(-(*(momlist[0])));
    k.emplace_back(-(*(momlist[1])));
    k.emplace_back(-(*(momlist[2])));
    k.emplace_back(-(*(momlist[3])));

    // the input gravitons
    std::vector<Tensor<T, D, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[0]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[2]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[3]->get_current()));

    // metric
    Tensor<T, D, 2> Hout(0), g(0);
    const std::vector<T>& metric(Hout.get_metric_extended());
    for (size_t ii = 0; ii < D; ii++) g[ii][ii] = T(1) / metric[ii];

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1},
                                                          {1, 0, 2, 3}, {1, 0, 3, 2}, {1, 2, 0, 3}, {1, 2, 3, 0}, {1, 3, 0, 2}, {1, 3, 2, 0},
                                                          {2, 0, 1, 3}, {2, 0, 3, 1}, {2, 1, 0, 3}, {2, 1, 3, 0}, {2, 3, 0, 1}, {2, 3, 1, 0},
                                                          {3, 0, 1, 2}, {3, 0, 2, 1}, {3, 1, 0, 2}, {3, 1, 2, 0}, {3, 2, 0, 1}, {3, 2, 1, 0}};

    // build Tensors
    using T2array1D = std::vector<Tensor<T, D, 2>>;
    using T2array2D = std::vector<T2array1D>;
    using T2array3D = std::vector<T2array2D>;
    using T2array4D = std::vector<T2array3D>;

    using T1array1D = std::vector<Tensor<T, D, 1>>;
    using T1array2D = std::vector<T1array1D>;
    using T1array3D = std::vector<T1array2D>;
    using T1array4D = std::vector<T1array3D>;
    using T1array5D = std::vector<T1array4D>;

    T2array2D HH(4, T2array1D(4, Tensor<T, D, 2>(0)));
    T2array3D HHH(4, T2array2D(4, T2array1D(4, Tensor<T, D, 2>(0))));
    T2array4D HHHH(4, T2array3D(4, T2array2D(4, T2array1D(4, Tensor<T, D, 2>(0)))));
    T1array2D Hk(4, T1array1D(4, Tensor<T, D, 1>(0)));
    T1array3D HHk(4, T1array2D(4, T1array1D(4, Tensor<T, D, 1>(0))));
    T1array4D HHHk(4, T1array3D(4, T1array2D(4, T1array1D(4, Tensor<T, D, 1>(0)))));
    T1array5D HHHHk(4, T1array4D(4, T1array3D(4, T1array2D(4, T1array1D(4, Tensor<T, D, 1>(0))))));

    for (size_t ip = 0; ip < 24; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];
        HH[p1][p2] = index_contraction<1, 0>(H[p1], H[p2]);
        HHH[p1][p2][p3] = index_contraction<1, 0>(HH[p1][p2], H[p3]);
        HHHH[p1][p2][p3][p4] = index_contraction<1, 0>(HHH[p1][p2][p3], H[p4]);
        for (size_t ii = 0; ii < 4; ii++) {
            Hk[p1][ii] = index_contraction<1>(k[ii], H[p1]);
            HHk[p1][p2][ii] = index_contraction<1>(k[ii], HH[p1][p2]);
            HHHk[p1][p2][p3][ii] = index_contraction<1>(k[ii], HHH[p1][p2][p3]);
            HHHHk[p1][p2][p3][p4][ii] = index_contraction<1>(k[ii], HHHH[p1][p2][p3][p4]);
        }
    }

    // build scalars
    using array1D = std::vector<T>;
    using array2D = std::vector<array1D>;
    using array3D = std::vector<array2D>;
    using array4D = std::vector<array3D>;
    using array5D = std::vector<array4D>;
    using array6D = std::vector<array5D>;
    array1D trH(4, T(0));
    array2D trHH(4, array1D(4, T(0)));
    array3D trHHH(4, array2D(4, array1D(4, T(0))));
    array4D trHHHH(4, array3D(4, array2D(4, array1D(4, T(0)))));
    array2D kk(4, array1D(4, T(0)));
    array3D kHk(4, array2D(4, array1D(4, T(0))));
    array4D kHHk(4, array3D(4, array2D(4, array1D(4, T(0)))));
    array5D kHHHk(4, array4D(4, array3D(4, array2D(4, array1D(4, T(0))))));
    array6D kHHHHk(4, array5D(4, array4D(4, array3D(4, array2D(4, array1D(4, T(0)))))));

    for (size_t ip = 0; ip < 24; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];
        trH[p1] = trace_rank2(H[p1]);
        trHH[p1][p2] = trace_rank2(HH[p1][p2]);
        trHHH[p1][p2][p3] = trace_rank2(HHH[p1][p2][p3]);
        trHHHH[p1][p2][p3][p4] = trace_rank2(HHHH[p1][p2][p3][p4]);
        for (size_t ii = 0; ii < 4; ii++) {
            Tensor<T, D, 1> ki = Tensor<T, D, 1>(k[ii]);
            for (size_t jj = 0; jj < 4; jj++) {
                kk[ii][jj] = k[ii] * k[jj];
                kHk[ii][p1][jj] = ki * Hk[p1][jj];
                kHHk[ii][p1][p2][jj] = ki * HHk[p1][p2][jj];
                kHHHk[ii][p1][p2][p3][jj] = ki * HHHk[p1][p2][p3][jj];
                kHHHHk[ii][p1][p2][p3][p4][jj] = ki * HHHHk[p1][p2][p3][p4][jj];
            }
        }
    }

    for (size_t a = 0; a < D; a++) {
        for (size_t b = a; b < D; b++) {
            T theentry = T(0);
            for (size_t ip = 0; ip < 24; ip++) {
                size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];

                theentry +=
                    T(12) * trHH[p2][p4] * H[p1][a][b] * kHk[p4][p3][p3] -
                    T(6) * trHH[p3][p4] *
                        (T(2) * Hk[p1][p3][a] * Hk[p2][p4][b] - H[p1][a][b] * (T(2) * kHk[p3][p2][p4] + kHk[p4][p2][p3]) -
                         T(3) * kk[p3][p4] * HH[p1][p2][a][b] + T(2) * k[p3][b] * HHk[p1][p2][p4][a] + T(2) * k[p3][a] * HHk[p1][p2][p4][b]) -
                    T(12) * trHH[p4][p3] *
                        (T(2) * Hk[p1][p4][a] * Hk[p2][p4][b] - H[p1][a][b] * kHk[p4][p2][p4] - T(2) * kk[p4][p4] * HH[p1][p2][a][b] +
                         T(2) * k[p4][b] * HHk[p1][p2][p4][a] + T(2) * k[p4][a] * HHk[p1][p2][p4][b]) -
                    T(12) * trHH[p4][p2] *
                        (Hk[p1][p3][b] * Hk[p3][p4][a] + Hk[p1][p3][a] * Hk[p3][p4][b] - H[p1][a][b] * (kHk[p4][p3][p3] + kHk[p4][p3][p4]) -
                         kk[p3][p4] * (HH[p1][p3][a][b] + HH[p1][p3][b][a]) + k[p3][b] * HHk[p1][p3][p4][a] + k[p3][a] * HHk[p1][p3][p4][b]) +
                    trHH[p4][p1] * (T(6) * k[p4][a] * k[p4][b] * trHH[p3][p2] +
                                    T(12) * (H[p3][a][b] * kHk[p4][p2][p3] - k[p3][b] * HHk[p3][p2][p4][a] - k[p3][a] * HHk[p3][p2][p4][b])) +
                    trHH[p2][p1] *
                        (T(3) * k[p3][a] * k[p4][b] * trHH[p3][p4] -
                         T(3) * (Hk[p3][p4][b] * Hk[p4][p3][a] + Hk[p3][p4][a] * Hk[p4][p3][b] - T(2) * H[p4][a][b] * kHk[p4][p3][p3] -
                                 T(2) * kk[p3][p4] * HH[p3][p4][b][a] + T(2) * k[p3][b] * HHk[p3][p4][p4][a] + T(2) * k[p3][a] * HHk[p3][p4][p4][b])) -
                    T(6) * trHH[p3][p2] *
                        (Hk[p1][p4][b] * Hk[p4][p4][a] + Hk[p1][p4][a] * Hk[p4][p4][b] - H[p4][b][a] * kHk[p4][p1][p4] - H[p1][a][b] * kHk[p4][p4][p4] -
                         kk[p4][p4] * (HH[p1][p4][a][b] + HH[p1][p4][b][a]) + k[p4][b] * (HHk[p1][p4][p4][a] + HHk[p4][p1][p4][a]) +
                         k[p4][a] * (HHk[p1][p4][p4][b] + HHk[p4][p1][p4][b])) +
                    trH[p3] * (-(T(6) * kk[p3][p4] * trHH[p4][p2] * H[p1][a][b]) +
                               trHH[p2][p1] * (-(T(3) * kk[p4][p4] * H[p4][b][a]) + T(3) * k[p4][b] * Hk[p4][p4][a] + T(3) * k[p4][a] * Hk[p4][p4][b]) +
                               trH[p4] * (-(T(3) * k[p4][a] * k[p4][b] * trHH[p2][p1]) + T(12) * Hk[p1][p4][a] * Hk[p2][p4][b] -
                                          T(6) * (H[p1][a][b] * kHk[p4][p2][p3] + kk[p3][p4] * HH[p1][p2][a][b])) +
                               T(6) * (-(T(2) * Hk[p2][p4][b] * HHk[p1][p4][p4][a]) - T(2) * Hk[p1][p4][a] * HHk[p2][p4][p4][b] +
                                       H[p1][a][b] * kHHk[p3][p4][p2][p4] + T(2) * kk[p4][p4] * HHH[p2][p4][p1][b][a])) +
                    trH[p2] *
                        (trHH[p3][p4] * (-(T(9) * kk[p3][p4] * H[p1][a][b]) + T(6) * k[p3][b] * Hk[p1][p4][a] + T(6) * k[p3][a] * Hk[p1][p4][b]) +
                         T(12) * trHH[p4][p3] * (-(kk[p4][p4] * H[p1][a][b]) + k[p4][b] * Hk[p1][p4][a] + k[p4][a] * Hk[p1][p4][b]) +
                         T(6) * trH[p4] *
                             (Hk[p1][p3][b] * Hk[p3][p4][a] + Hk[p1][p3][a] * Hk[p3][p4][b] - T(2) * H[p1][a][b] * (kHk[p4][p3][p3] + kHk[p4][p3][p4]) -
                              (kk[p3][p4] + T(2) * kk[p4][p4]) * HH[p1][p3][a][b] - kk[p3][p4] * HH[p1][p3][b][a] +
                              (k[p3][b] + T(2) * k[p4][b]) * HHk[p1][p3][p4][a] + (k[p3][a] + T(2) * k[p4][a]) * HHk[p1][p3][p4][b]) +
                         trH[p3] *
                             (-(T(3) * k[p4][a] * k[p4][b] * trHH[p4][p1]) +
                              trH[p4] * (T(3) * (kk[p3][p4] + kk[p4][p4]) * H[p1][a][b] - T(3) * k[p4][b] * Hk[p1][p4][a] - T(3) * k[p4][a] * Hk[p1][p4][b]) +
                              T(3) * (Hk[p1][p4][b] * Hk[p4][p4][a] + Hk[p1][p4][a] * Hk[p4][p4][b] - H[p4][b][a] * kHk[p4][p1][p4] -
                                      H[p1][a][b] * kHk[p4][p4][p4] - kk[p4][p4] * (HH[p1][p4][a][b] + HH[p1][p4][b][a]) +
                                      k[p4][b] * (HHk[p1][p4][p4][a] + HHk[p4][p1][p4][a]) + k[p4][a] * (HHk[p1][p4][p4][b] + HHk[p4][p1][p4][b]))) +
                         T(6) *
                             (T(2) * kHk[p4][p4][p4] * HH[p1][p3][a][b] + T(2) * (kHk[p4][p3][p3] + kHk[p4][p3][p4]) * (HH[p1][p4][a][b] + HH[p1][p4][b][a]) -
                              (Hk[p4][p3][b] + T(2) * Hk[p4][p4][b]) * HHk[p1][p3][p4][a] - (Hk[p4][p3][a] + T(2) * Hk[p4][p4][a]) * HHk[p1][p3][p4][b] -
                              Hk[p3][p4][b] * HHk[p1][p4][p3][a] - Hk[p3][p4][a] * HHk[p1][p4][p3][b] - T(2) * Hk[p1][p3][b] * HHk[p3][p4][p4][a] -
                              T(2) * Hk[p1][p3][a] * HHk[p3][p4][p4][b] - T(2) * Hk[p1][p4][b] * HHk[p4][p3][p4][a] -
                              T(2) * Hk[p1][p4][a] * HHk[p4][p3][p4][b] +
                              H[p1][a][b] * (T(2) * kHHk[p3][p3][p4][p4] + kHHk[p4][p3][p4][p3] + T(2) * (kHHk[p4][p3][p4][p4] + kHHk[p4][p4][p3][p4])) +
                              T(2) *
                                  (kk[p4][p4] * (HHH[p1][p3][p4][a][b] + HHH[p1][p3][p4][b][a]) + kk[p3][p4] * (HHH[p1][p3][p4][b][a] + HHH[p1][p4][p3][a][b]) -
                                   (k[p3][b] + k[p4][b]) * HHHk[p1][p3][p4][p4][a] - (k[p3][a] + k[p4][a]) * HHHk[p1][p3][p4][p4][b] -
                                   k[p4][b] * HHHk[p1][p4][p3][p4][a] - k[p4][a] * HHHk[p1][p4][p3][p4][b]))) +
                    trH[p4] *
                        (-(T(6) * kk[p3][p4] * trHH[p2][p3] * H[p1][a][b]) +
                         trHH[p3][p2] * (-(T(6) * kk[p4][p4] * H[p1][a][b]) + T(6) * k[p4][b] * Hk[p1][p4][a] + T(6) * k[p4][a] * Hk[p1][p4][b]) +
                         trHH[p2][p1] * (-(T(3) * kk[p3][p4] * H[p3][a][b]) + T(3) * k[p3][b] * Hk[p3][p4][a] + T(3) * k[p3][a] * Hk[p3][p4][b]) +
                         T(4) * k[p4][a] * (k[p4][b] * trHHH[p1][p2][p3] - T(6) * HHHk[p1][p2][p3][p4][b]) +
                         T(6) *
                             (T(4) * (kHk[p4][p3][p3] + kHk[p4][p3][p4]) * HH[p1][p2][a][b] + T(2) * kHk[p4][p2][p3] * (HH[p1][p3][a][b] + HH[p1][p3][b][a]) -
                              T(2) * Hk[p3][p4][b] * HHk[p1][p2][p3][a] - T(2) * Hk[p3][p4][a] * HHk[p1][p2][p3][b] -
                              T(2) * (Hk[p2][p3][b] + T(2) * Hk[p2][p4][b]) * HHk[p1][p3][p4][a] - T(4) * Hk[p1][p4][a] * HHk[p2][p3][p4][b] -
                              T(2) * Hk[p1][p3][b] * HHk[p3][p2][p4][a] - T(2) * Hk[p1][p3][a] * (HHk[p2][p3][p4][b] + HHk[p3][p2][p4][b]) +
                              H[p1][a][b] * (T(2) * kHHk[p3][p2][p3][p4] + T(4) * (kHHk[p4][p2][p3][p3] + kHHk[p4][p2][p3][p4]) + kHHk[p4][p3][p2][p3]) +
                              T(2) * (H[p3][a][b] * kHHk[p4][p2][p1][p3] + T(2) * kk[p4][p4] * HHH[p1][p2][p3][a][b] +
                                      kk[p3][p4] * (HHH[p1][p2][p3][a][b] + HHH[p1][p2][p3][b][a] + HHH[p1][p3][p2][a][b]) -
                                      T(2) * k[p4][b] * HHHk[p1][p2][p3][p4][a] -
                                      k[p3][b] * (HHHk[p1][p2][p3][p4][a] + HHHk[p1][p3][p2][p4][a] + HHHk[p3][p1][p2][p4][a]) -
                                      k[p3][a] * (HHHk[p1][p2][p3][p4][b] + HHHk[p1][p3][p2][p4][b] + HHHk[p3][p1][p2][p4][b])))) +
                    trH[p1] *
                        (trHH[p4][p2] * (-(T(6) * kk[p3][p4] * H[p3][a][b]) + T(6) * k[p3][b] * Hk[p3][p4][a] + T(6) * k[p3][a] * Hk[p3][p4][b]) +
                         trH[p4] * (-(T(6) * H[p3][a][b] * kHk[p4][p2][p3]) + T(6) * k[p3][b] * HHk[p3][p2][p4][a] + T(6) * k[p3][a] * HHk[p3][p2][p4][b]) +
                         trH[p2] *
                             (-((T(3) * k[p3][a] * k[p4][b] * trHH[p3][p4]) / T(2)) +
                              (T(3) * trH[p4] * (kk[p3][p4] * H[p3][a][b] - k[p3][b] * Hk[p3][p4][a] - k[p3][a] * Hk[p3][p4][b])) / T(2) +
                              trH[p3] * ((trH[p4] * k[p4][a] * k[p4][b]) / T(2) +
                                         (kk[p4][p4] * H[p4][b][a] - k[p4][b] * Hk[p4][p4][a] - k[p4][a] * Hk[p4][p4][b]) / T(2)) +
                              (T(3) * (Hk[p3][p4][b] * Hk[p4][p3][a] + Hk[p3][p4][a] * Hk[p4][p3][b] - T(2) * H[p4][a][b] * kHk[p4][p3][p3] -
                                       T(2) * kk[p3][p4] * HH[p3][p4][b][a] + T(2) * k[p3][b] * HHk[p3][p4][p4][a] + T(2) * k[p3][a] * HHk[p3][p4][p4][b])) /
                                  T(2)) +
                         T(6) * ((kHk[p3][p2][p4] + kHk[p4][p2][p3]) * HH[p3][p4][b][a] - Hk[p4][p3][b] * HHk[p3][p2][p4][a] -
                                 T(2) * Hk[p4][p3][a] * HHk[p3][p2][p4][b] - Hk[p3][p4][b] * HHk[p4][p2][p3][a] + H[p3][a][b] * kHHk[p3][p4][p2][p4] +
                                 H[p4][a][b] * (kHHk[p3][p2][p3][p4] + T(2) * kHHk[p4][p2][p3][p3]) + T(2) * H[p4][b][a] * kHHk[p4][p2][p3][p4] +
                                 kk[p3][p4] * (HHH[p3][p2][p4][b][a] + HHH[p4][p2][p3][a][b]) -
                                 T(2) * k[p3][b] * (HHHk[p3][p2][p4][p4][a] + HHHk[p3][p4][p2][p4][a]) +
                                 k[p3][a] * (k[p4][b] * (trHHH[p2][p4][p3] + trHHH[p4][p2][p3]) - T(2) * (HHHk[p3][p2][p4][p4][b] + HHHk[p3][p4][p2][p4][b])) +
                                 T(2) * k[p4][a] * (k[p4][b] * trHHH[p2][p3][p4] - HHHk[p4][p2][p3][p4][b]) - T(2) * k[p4][b] * HHHk[p4][p3][p2][p4][a])) +
                    T(2) *
                        (T(6) *
                             (T(2) * HHk[p1][p4][p4][a] * HHk[p2][p3][p4][b] + HHk[p1][p3][p4][a] * (HHk[p2][p4][p3][b] + T(2) * HHk[p2][p4][p4][b]) +
                              HHk[p1][p4][p3][b] * HHk[p3][p2][p4][a] + HHk[p1][p4][p3][a] * (HHk[p2][p3][p4][b] + T(2) * HHk[p3][p2][p4][b]) +
                              T(2) * HHk[p1][p2][p3][b] * HHk[p3][p4][p4][a] + T(2) * HHk[p1][p2][p3][a] * HHk[p3][p4][p4][b] +
                              HHk[p3][p2][p4][b] * HHk[p4][p1][p3][a] + (HHk[p1][p3][p4][b] + HHk[p3][p1][p4][b]) * HHk[p4][p2][p3][a] +
                              T(2) * HHk[p1][p2][p4][b] * HHk[p4][p3][p4][a] + T(2) * HHk[p1][p2][p4][a] * HHk[p4][p3][p4][b] -
                              (HH[p1][p3][a][b] + HH[p1][p3][b][a]) * kHHk[p3][p4][p2][p4] - HH[p3][p4][b][a] * (kHHk[p3][p1][p2][p4] + kHHk[p4][p2][p1][p3]) -
                              (HH[p1][p4][a][b] + HH[p1][p4][b][a]) * (kHHk[p3][p2][p3][p4] + T(2) * (kHHk[p4][p2][p3][p3] + kHHk[p4][p2][p3][p4])) -
                              HH[p1][p2][a][b] * (T(2) * kHHk[p3][p3][p4][p4] + kHHk[p4][p3][p4][p3] + T(2) * (kHHk[p4][p3][p4][p4] + kHHk[p4][p4][p3][p4])) -
                              T(2) * kHk[p4][p4][p4] * HHH[p1][p2][p3][a][b] -
                              (kHk[p3][p2][p4] + kHk[p4][p2][p3]) * (HHH[p1][p3][p4][b][a] + HHH[p1][p4][p3][a][b]) -
                              T(2) * (kHk[p4][p3][p3] * (HHH[p1][p2][p4][a][b] + HHH[p1][p2][p4][b][a] + HHH[p1][p4][p2][a][b]) +
                                      kHk[p4][p3][p4] * (HHH[p1][p2][p4][a][b] + HHH[p1][p2][p4][b][a] + HHH[p2][p4][p1][b][a])) -
                              kHk[p3][p1][p4] * HHH[p3][p2][p4][b][a] - kHk[p4][p2][p3] * HHH[p4][p1][p3][a][b] +
                              T(2) * Hk[p4][p4][a] * HHHk[p1][p2][p3][p4][b] + T(2) * (Hk[p2][p3][b] + Hk[p2][p4][b]) * HHHk[p1][p3][p4][p4][a] +
                              T(2) * Hk[p2][p4][b] * HHHk[p1][p4][p3][p4][a] +
                              Hk[p4][p3][a] * (HHHk[p1][p2][p3][p4][b] + T(2) * (HHHk[p1][p3][p2][p4][b] + HHHk[p3][p1][p2][p4][b])) +
                              Hk[p4][p3][b] * (HHHk[p1][p2][p3][p4][a] + HHHk[p1][p3][p2][p4][a] + HHHk[p3][p2][p1][p4][a]) +
                              T(2) * Hk[p1][p3][b] * (HHHk[p3][p2][p4][p4][a] + HHHk[p3][p4][p2][p4][a]) +
                              T(2) * Hk[p1][p3][a] * (HHHk[p2][p3][p4][p4][b] + HHHk[p3][p2][p4][p4][b] + HHHk[p3][p4][p2][p4][b]) -
                              H[p4][a][b] * (kHHHk[p3][p1][p2][p3][p4] + kHHHk[p3][p1][p3][p2][p4] + T(2) * kHHHk[p4][p1][p2][p3][p3]) -
                              T(2) * H[p4][b][a] * kHHHk[p4][p1][p2][p3][p4]) -
                         T(6) * H[p3][a][b] * (kHHHk[p4][p1][p4][p2][p3] + kHHHk[p4][p2][p1][p4][p3]) -
                         T(3) * H[p1][a][b] *
                             (T(4) * kHHHk[p3][p2][p3][p4][p4] + T(4) * kHHHk[p3][p3][p2][p4][p4] + T(3) * kHHHk[p3][p4][p3][p2][p4] +
                              T(4) * kHHHk[p4][p2][p3][p4][p4] + T(4) * kHHHk[p4][p2][p4][p3][p3] + T(4) * kHHHk[p4][p2][p4][p3][p4] +
                              T(2) * kHHHk[p4][p3][p2][p4][p3] + T(4) * kHHHk[p4][p3][p2][p4][p4] + kHHHk[p4][p3][p4][p2][p3]) +
                         T(2) * kk[p4][p4] *
                             (H[p4][b][a] * trHHH[p1][p2][p3] + T(6) * H[p1][a][b] * trHHH[p2][p3][p4] -
                              T(6) * (HHHH[p1][p2][p3][p4][a][b] + HHHH[p1][p2][p3][p4][b][a] + HHHH[p1][p3][p4][p2][a][b] + HHHH[p1][p4][p3][p2][a][b])) +
                         T(3) * kk[p3][p4] *
                             (T(2) * H[p3][a][b] * trHHH[p1][p2][p4] + H[p1][a][b] * (trHHH[p2][p3][p4] + T(2) * trHHH[p2][p4][p3] + T(3) * trHHH[p4][p2][p3]) -
                              T(2) * (T(2) * HHHH[p1][p2][p3][p4][b][a] + T(2) * HHHH[p1][p2][p4][p3][a][b] + T(2) * HHHH[p1][p3][p2][p4][b][a] +
                                      T(2) * HHHH[p1][p4][p2][p3][a][b] + T(2) * HHHH[p1][p4][p3][p2][a][b] + HHHH[p3][p1][p2][p4][b][a] +
                                      HHHH[p4][p2][p1][p3][a][b])) +
                         T(6) *
                             (Hk[p3][p4][a] * HHHk[p1][p2][p4][p3][b] +
                              Hk[p3][p4][b] * (-(k[p3][a] * trHHH[p1][p2][p4]) + HHHk[p1][p2][p4][p3][a] + HHHk[p1][p4][p2][p3][a] + HHHk[p4][p2][p1][p3][a]) +
                              T(2) * Hk[p1][p4][a] * (HHHk[p2][p3][p4][p4][b] + HHHk[p2][p4][p3][p4][b] + HHHk[p4][p2][p3][p4][b]) +
                              k[p3][b] * (-(Hk[p3][p4][a] * trHHH[p1][p2][p4]) - Hk[p1][p4][a] * (trHHH[p2][p3][p4] + trHHH[p3][p2][p4]) +
                                          T(2) * HHHHk[p1][p2][p3][p4][p4][a] + T(2) * HHHHk[p1][p3][p2][p4][p4][a] + T(2) * HHHHk[p1][p3][p4][p2][p4][a] +
                                          T(2) * HHHHk[p3][p1][p2][p4][p4][a] + T(2) * HHHHk[p3][p2][p4][p1][p4][a] + HHHHk[p3][p4][p1][p2][p4][a] +
                                          HHHHk[p3][p4][p2][p1][p4][a] + HHHHk[p4][p1][p3][p2][p4][a] - HHHHk[p4][p2][p3][p1][p4][a])) -
                         T(2) * (-(T(6) * (Hk[p4][p4][b] * HHHk[p1][p2][p3][p4][a] + Hk[p1][p4][b] * HHHk[p4][p3][p2][p4][a])) +
                                 k[p4][a] * (Hk[p4][p4][b] * trHHH[p1][p2][p3] + T(6) * Hk[p1][p4][b] * trHHH[p2][p3][p4] -
                                             T(6) * (HHHHk[p1][p2][p3][p4][p4][b] + HHHHk[p1][p2][p4][p3][p4][b] + HHHHk[p1][p4][p2][p3][p4][b] +
                                                     HHHHk[p4][p1][p2][p3][p4][b])) +
                                 T(3) * k[p3][a] *
                                     (Hk[p1][p4][b] * (trHHH[p2][p4][p3] + trHHH[p4][p2][p3]) - T(2) * HHHHk[p1][p2][p3][p4][p4][b] -
                                      T(2) * HHHHk[p1][p3][p2][p4][p4][b] - T(2) * HHHHk[p1][p3][p4][p2][p4][b] - T(2) * HHHHk[p3][p1][p2][p4][p4][b] -
                                      T(2) * HHHHk[p3][p2][p4][p1][p4][b] - HHHHk[p3][p4][p1][p2][p4][b] - HHHHk[p3][p4][p2][p1][p4][b] -
                                      HHHHk[p4][p1][p3][p2][p4][b] + HHHHk[p4][p2][p3][p1][p4][b])) -
                         T(2) * k[p4][b] *
                             (Hk[p4][p4][a] * trHHH[p1][p2][p3] + T(6) * Hk[p1][p4][a] * trHHH[p2][p3][p4] + T(6) * k[p4][a] * trHHHH[p3][p4][p1][p2] +
                              T(3) * k[p3][a] * (trHHHH[p2][p1][p4][p3] + trHHHH[p2][p3][p1][p4] + trHHHH[p3][p4][p1][p2]) -
                              T(6) * (HHHHk[p1][p2][p3][p4][p4][a] + HHHHk[p1][p2][p4][p3][p4][a] + HHHHk[p1][p4][p3][p2][p4][a] +
                                      HHHHk[p4][p3][p2][p1][p4][a]))) +
                    g[a][b] *
                        (T(4) * kHk[p4][p4][p4] * trHHH[p1][p2][p3] + T(6) * trHH[p1][p3] * kHHk[p3][p4][p2][p4] +
                         T(6) * trHH[p3][p4] * (T(2) * kHHk[p3][p1][p2][p4] + kHHk[p4][p2][p1][p3]) +
                         T(6) * trHH[p1][p4] * (kHHk[p3][p2][p3][p4] + T(2) * kHHk[p4][p2][p3][p3]) +
                         trHH[p4][p1] * (-(T(6) * kk[p4][p4] * trHH[p3][p2]) + T(12) * (kHHk[p4][p2][p3][p3] + kHHk[p4][p2][p3][p4])) +
                         trHH[p4][p2] *
                             (-(T(6) * kk[p3][p4] * trHH[p1][p3]) + T(6) * (kHHk[p3][p1][p3][p4] + T(2) * kHHk[p4][p1][p3][p4] + kHHk[p4][p3][p1][p3])) +
                         trHH[p2][p1] * (-((T(9) * kk[p3][p4] * trHH[p3][p4]) / T(2)) + T(3) * (T(2) * kHHk[p3][p3][p4][p4] + kHHk[p4][p3][p4][p3])) +
                         T(6) * trHH[p3][p2] * (kHHk[p4][p1][p4][p4] + kHHk[p4][p4][p1][p4]) +
                         trH[p2] * trH[p3] *
                             (T(3) * kk[p4][p4] * trHH[p4][p1] + T(3) * trH[p4] * kHk[p4][p1][p4] - T(3) * (kHHk[p4][p1][p4][p4] + kHHk[p4][p4][p1][p4])) +
                         trH[p3] * (-(T(6) * trHH[p4][p1] * kHk[p4][p2][p3]) - T(3) * trHH[p2][p1] * kHk[p4][p4][p4] +
                                    trH[p4] * ((T(3) * (kk[p3][p4] + T(2) * kk[p4][p4]) * trHH[p2][p1]) / T(2) - T(6) * kHHk[p4][p2][p1][p3]) +
                                    T(6) * (-(kk[p3][p4] * trHHH[p1][p2][p4]) + kHHHk[p4][p1][p4][p2][p3] + kHHHk[p4][p2][p1][p4][p3])) +
                         trH[p4] * (-(T(6) * trHH[p3][p2] * kHk[p4][p1][p4]) - T(6) * trHH[p1][p3] * kHk[p4][p2][p3] - T(6) * trHH[p2][p1] * kHk[p4][p3][p3] -
                                    T(2) * (T(3) * kk[p3][p4] + T(2) * kk[p4][p4]) * trHHH[p1][p2][p3] +
                                    T(6) * (T(2) * kHHHk[p3][p1][p2][p3][p4] + T(2) * kHHHk[p3][p1][p3][p2][p4] + kHHHk[p3][p2][p1][p3][p4] +
                                            T(2) * (kHHHk[p4][p1][p2][p3][p3] + T(2) * kHHHk[p4][p1][p2][p3][p4] + kHHHk[p4][p2][p1][p3][p3]) +
                                            kHHHk[p4][p2][p3][p1][p3])) +
                         trH[p1] *
                             (-(T(3) * trHH[p3][p4] * (T(2) * kHk[p3][p2][p4] + kHk[p4][p2][p3])) - T(6) * trHH[p4][p3] * kHk[p4][p2][p4] -
                              T(6) * trHH[p2][p4] * kHk[p4][p3][p3] - T(6) * trHH[p4][p2] * (kHk[p4][p3][p3] + kHk[p4][p3][p4]) +
                              trH[p3] * (T(3) * kk[p3][p4] * trHH[p4][p2] + T(3) * trH[p4] * kHk[p4][p2][p3] - T(3) * kHHk[p3][p4][p2][p4]) +
                              trH[p4] * (T(3) * kk[p3][p4] * trHH[p2][p3] -
                                         T(3) * (T(2) * kHHk[p3][p2][p3][p4] + T(4) * (kHHk[p4][p2][p3][p3] + kHHk[p4][p2][p3][p4]) + kHHk[p4][p3][p2][p3])) +
                              trH[p2] * ((T(9) * kk[p3][p4] * trHH[p3][p4]) / T(4) + T(3) * trH[p4] * kHk[p4][p3][p3] +
                                         trH[p3] * ((trH[p4] * (-(T(3) * kk[p3][p4]) - T(2) * kk[p4][p4])) / T(4) + kHk[p4][p4][p4] / T(2)) -
                                         (T(3) * (T(2) * kHHk[p3][p3][p4][p4] + kHHk[p4][p3][p4][p3])) / T(2)) +
                              T(3) * (-(T(4) * kk[p4][p4] * trHHH[p2][p3][p4]) -
                                      kk[p3][p4] * (trHHH[p2][p3][p4] + T(2) * trHHH[p2][p4][p3] + T(3) * trHHH[p4][p2][p3]) +
                                      T(4) * kHHHk[p3][p2][p3][p4][p4] + T(4) * kHHHk[p3][p3][p2][p4][p4] + T(3) * kHHHk[p3][p4][p3][p2][p4] +
                                      T(4) * kHHHk[p4][p2][p3][p4][p4] + T(4) * kHHHk[p4][p2][p4][p3][p3] + T(4) * kHHHk[p4][p2][p4][p3][p4] +
                                      T(2) * kHHHk[p4][p3][p2][p4][p3] + T(4) * kHHHk[p4][p3][p2][p4][p4] + kHHHk[p4][p3][p4][p2][p3])) +
                         T(6) * (T(4) * kHk[p4][p3][p3] * trHHH[p1][p2][p4] + T(2) * kHk[p4][p2][p4] * trHHH[p1][p3][p4] +
                                 T(2) * kHk[p4][p1][p4] * trHHH[p2][p3][p4] + kHk[p3][p2][p4] * trHHH[p4][p1][p3] +
                                 kHk[p4][p2][p3] * (trHHH[p1][p4][p3] + trHHH[p4][p1][p3]) +
                                 kHk[p3][p1][p4] * (trHHH[p2][p3][p4] + trHHH[p2][p4][p3] + trHHH[p4][p2][p3]) + T(4) * kk[p4][p4] * trHHHH[p3][p4][p1][p2] +
                                 kk[p3][p4] * (trHHHH[p1][p2][p4][p3] + trHHHH[p2][p1][p3][p4] + trHHHH[p2][p1][p4][p3] +
                                               T(3) * (trHHHH[p2][p3][p1][p4] + trHHHH[p3][p4][p1][p2])) -
                                 T(2) * kHHHHk[p3][p1][p2][p3][p4][p4] - kHHHHk[p3][p1][p2][p4][p3][p4] - T(2) * kHHHHk[p3][p1][p3][p2][p4][p4] -
                                 T(3) * kHHHHk[p3][p1][p3][p4][p2][p4] - kHHHHk[p3][p1][p4][p3][p2][p4] - T(2) * kHHHHk[p3][p2][p1][p3][p4][p4] -
                                 kHHHHk[p3][p2][p4][p3][p1][p4] - T(4) * kHHHHk[p3][p3][p1][p2][p4][p4] - T(2) * kHHHHk[p3][p3][p2][p4][p1][p4] -
                                 T(2) * kHHHHk[p3][p3][p4][p1][p2][p4] + kHHHHk[p3][p4][p1][p2][p3][p4] - T(2) * kHHHHk[p3][p4][p1][p3][p2][p4] -
                                 T(2) * kHHHHk[p3][p4][p2][p1][p3][p4] + kHHHHk[p3][p4][p2][p3][p1][p4] - kHHHHk[p4][p1][p2][p3][p4][p3] -
                                 T(4) * kHHHHk[p4][p1][p2][p3][p4][p4] - T(2) * kHHHHk[p4][p1][p2][p4][p3][p3] + kHHHHk[p4][p1][p3][p2][p4][p3] -
                                 T(2) * kHHHHk[p4][p1][p3][p4][p2][p4] - T(2) * kHHHHk[p4][p1][p4][p2][p3][p3] - T(2) * kHHHHk[p4][p1][p4][p2][p3][p4] -
                                 T(2) * kHHHHk[p4][p2][p1][p3][p4][p3] - T(3) * kHHHHk[p4][p2][p3][p1][p4][p3] - T(2) * kHHHHk[p4][p2][p3][p4][p1][p4] -
                                 T(2) * kHHHHk[p4][p2][p4][p1][p3][p4] - kHHHHk[p4][p2][p4][p3][p1][p3] + kHHHHk[p4][p3][p1][p2][p4][p3] -
                                 kHHHHk[p4][p3][p1][p4][p2][p3] -
                                 T(2) * (kHHHHk[p4][p3][p2][p1][p4][p3] + T(2) * kHHHHk[p4][p3][p2][p1][p4][p4] + kHHHHk[p4][p4][p2][p3][p1][p3])));
            }
            Hout[a][b] = theentry / T(24);
            Hout[b][a] = Hout[a][b];
        }
    }

    return v_t_to_v_extended_t<T, Tcur>(Hout.vectorize());
}
/* end Einstein Hilbert */

#ifdef INCLUDE_GB_GRAVITY
/* start Gauss Bonnet */
template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term01(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
-(pow2(kk[p1][p2])*trH[p1]*trH[p2]*g[a][b])
+pow2(kk[p1][p2])*g[a][b]*HH[p1][p2]
+T(2)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p1][p1]
-(T(5)*HH[p1][p2]*k[p1][b]*k[p2][a]*kk[p1][p2])
+trH[p1]*trH[p2]*T(2)*k[p1][a]*k[p2][b]*kk[p1][p2]
+T(3)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p1][p2]
-(T(6)*g[a][b]*HH[p1][p2]*kk[p1][p1]*kk[p1][p2])
-(trH[p1]*trH[p2]*T(2)*k[p1][a]*k[p1][b]*kk[p2][p2])
+T(2)*HH[p1][p2]*k[p1][a]*k[p1][b]*kk[p2][p2]
-(T(2)*HH[p1][p2]*k[p1][b]*k[p2][a]*kk[p2][p2])
+trH[p1]*trH[p2]*g[a][b]*kk[p1][p1]*kk[p2][p2]
-(g[a][b]*HH[p1][p2]*kk[p1][p1]*kk[p2][p2])
+T(6)*g[a][b]*HH[p1][p2]*kk[p1][p2]*kk[p2][p2]
+pow2(kk[p1][p2])*trH[p2]*H[p1][a][b]
-(trH[p2]*T(4)*kk[p1][p1]*kk[p1][p2]*H[p1][a][b])
-(trH[p2]*kk[p1][p1]*kk[p2][p2]*H[p1][a][b])
+pow2(kk[p1][p2])*trH[p1]*H[p2][a][b]
-(trH[p1]*kk[p1][p1]*kk[p2][p2]*H[p2][a][b])
+trH[p1]*T(4)*kk[p1][p2]*kk[p2][p2]*H[p2][a][b]
+trH[p2]*T(2)*k[p1][b]*kk[p1][p2]*Hk[p1][p1][a]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term02(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
-(trH[p2]*T(2)*k[p2][b]*kk[p1][p2]*Hk[p1][p1][a])
+trH[p2]*k[p1][b]*kk[p2][p2]*Hk[p1][p1][a]
+trH[p2]*T(2)*k[p1][a]*kk[p1][p2]*Hk[p1][p1][b]
+trH[p2]*k[p1][a]*kk[p2][p2]*Hk[p1][p1][b]
+trH[p2]*T(2)*k[p1][b]*kk[p1][p1]*Hk[p1][p2][a]
+trH[p2]*T(2)*k[p2][b]*kk[p1][p1]*Hk[p1][p2][a]
-(trH[p2]*k[p1][b]*kk[p1][p2]*Hk[p1][p2][a])
-(trH[p2]*T(2)*k[p2][b]*kk[p1][p2]*Hk[p1][p2][a])
+trH[p2]*T(2)*k[p1][a]*kk[p1][p1]*Hk[p1][p2][b]
-(trH[p2]*k[p1][a]*kk[p1][p2]*Hk[p1][p2][b])
-(trH[p2]*T(2)*k[p2][a]*kk[p1][p2]*Hk[p1][p2][b])
+trH[p1]*T(2)*k[p1][b]*kk[p1][p2]*Hk[p2][p1][a]
-(trH[p1]*k[p2][b]*kk[p1][p2]*Hk[p2][p1][a])
-(trH[p1]*T(2)*k[p2][b]*kk[p2][p2]*Hk[p2][p1][a])
+T(2)*kk[p2][p2]*Hk[p1][p1][b]*Hk[p2][p1][a]
-(T(2)*kk[p1][p1]*Hk[p1][p2][b]*Hk[p2][p1][a])
+T(2)*kk[p1][p2]*Hk[p1][p2][b]*Hk[p2][p1][a]
+kk[p2][p2]*Hk[p1][p2][b]*Hk[p2][p1][a]
+trH[p1]*T(2)*k[p1][a]*kk[p1][p2]*Hk[p2][p1][b]
-(trH[p1]*k[p2][a]*kk[p1][p2]*Hk[p2][p1][b])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term03(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
trH[p1]*T(2)*k[p1][a]*kk[p2][p2]*Hk[p2][p1][b]
-(trH[p1]*T(2)*k[p2][a]*kk[p2][p2]*Hk[p2][p1][b])
-(kk[p1][p2]*Hk[p1][p1][a]*Hk[p2][p1][b])
-(T(4)*kk[p2][p2]*Hk[p1][p1][a]*Hk[p2][p1][b])
-(kk[p1][p1]*Hk[p1][p2][a]*Hk[p2][p1][b])
+T(2)*kk[p2][p2]*Hk[p1][p2][a]*Hk[p2][p1][b]
+trH[p1]*k[p2][b]*kk[p1][p1]*Hk[p2][p2][a]
-(trH[p1]*T(2)*k[p2][b]*kk[p1][p2]*Hk[p2][p2][a])
-(T(2)*kk[p1][p2]*Hk[p1][p1][b]*Hk[p2][p2][a])
+T(2)*kk[p1][p1]*Hk[p1][p2][b]*Hk[p2][p2][a]
+kk[p1][p2]*Hk[p1][p2][b]*Hk[p2][p2][a]
+trH[p1]*k[p2][a]*kk[p1][p1]*Hk[p2][p2][b]
-(trH[p1]*T(2)*k[p1][a]*kk[p1][p2]*Hk[p2][p2][b])
-(trH[p1]*T(2)*k[p2][a]*kk[p1][p2]*Hk[p2][p2][b])
+T(4)*kk[p1][p2]*Hk[p1][p1][a]*Hk[p2][p2][b]
-(T(4)*kk[p1][p1]*Hk[p1][p2][a]*Hk[p2][p2][b])
-(trH[p2]*g[a][b]*kk[p2][p2]*Hkk[p1][p1][p1])
-(kk[p1][p2]*H[p2][a][b]*Hkk[p1][p1][p1])
-(trH[p2]*T(2)*k[p1][a]*k[p1][b]*Hkk[p1][p1][p2])
+trH[p2]*T(6)*k[p1][a]*k[p2][b]*Hkk[p1][p1][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term04(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
trH[p2]*T(4)*k[p2][a]*k[p2][b]*Hkk[p1][p1][p2]
-(trH[p2]*T(2)*g[a][b]*kk[p1][p1]*Hkk[p1][p1][p2])
+trH[p2]*T(2)*g[a][b]*kk[p2][p2]*Hkk[p1][p1][p2]
+T(3)*kk[p1][p1]*H[p2][a][b]*Hkk[p1][p1][p2]
-(T(2)*kk[p1][p2]*H[p2][a][b]*Hkk[p1][p1][p2])
+T(4)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p1][p2]
+k[p2][b]*Hk[p2][p1][a]*Hkk[p1][p1][p2]
+k[p2][a]*Hk[p2][p1][b]*Hkk[p1][p1][p2]
+T(2)*k[p1][b]*Hk[p2][p2][a]*Hkk[p1][p1][p2]
-(T(4)*k[p2][a]*Hk[p2][p2][b]*Hkk[p1][p1][p2])
-(trH[p2]*T(2)*k[p1][a]*k[p1][b]*Hkk[p1][p2][p1])
+trH[p2]*T(2)*g[a][b]*kk[p1][p1]*Hkk[p1][p2][p1]
-(kk[p1][p1]*H[p2][a][b]*Hkk[p1][p2][p1])
-(T(2)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p2][p1])
-(k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p2][p1])
-(T(4)*k[p1][b]*Hk[p2][p2][a]*Hkk[p1][p2][p1])
-(T(4)*k[p1][a]*Hk[p2][p2][b]*Hkk[p1][p2][p1])
+trH[p2]*T(2)*k[p1][a]*k[p1][b]*Hkk[p1][p2][p2]
-(trH[p2]*T(2)*k[p1][b]*k[p2][a]*Hkk[p1][p2][p2])
-(trH[p2]*T(4)*g[a][b]*kk[p1][p1]*Hkk[p1][p2][p2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term05(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
trH[p2]*T(2)*g[a][b]*kk[p1][p2]*Hkk[p1][p2][p2]
+kk[p1][p1]*H[p2][a][b]*Hkk[p1][p2][p2]
-(T(4)*kk[p1][p2]*H[p2][a][b]*Hkk[p1][p2][p2])
-(T(4)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p2][p2])
-(T(3)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p2][p2])
+T(6)*k[p2][a]*Hk[p2][p1][b]*Hkk[p1][p2][p2]
+trH[p1]*T(2)*k[p1][a]*k[p2][b]*Hkk[p2][p1][p1]
-(trH[p1]*T(2)*g[a][b]*kk[p1][p2]*Hkk[p2][p1][p1])
+trH[p1]*T(2)*g[a][b]*kk[p2][p2]*Hkk[p2][p1][p1]
+T(4)*kk[p1][p2]*H[p1][a][b]*Hkk[p2][p1][p1]
+kk[p2][p2]*H[p1][a][b]*Hkk[p2][p1][p1]
+T(2)*k[p2][b]*Hk[p1][p2][a]*Hkk[p2][p1][p1]
-(T(6)*k[p1][a]*Hk[p1][p2][b]*Hkk[p2][p1][p1])
+k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p1][p1]
-(T(8)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p2][p1][p1])
+T(4)*g[a][b]*Hkk[p1][p2][p1]*Hkk[p2][p1][p1]
+g[a][b]*Hkk[p1][p2][p2]*Hkk[p2][p1][p1]
-(trH[p1]*T(4)*k[p1][b]*k[p2][a]*Hkk[p2][p1][p2])
-(trH[p1]*T(2)*k[p1][a]*k[p2][b]*Hkk[p2][p1][p2])
+trH[p1]*T(4)*k[p2][a]*k[p2][b]*Hkk[p2][p1][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term06(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
trH[p1]*T(4)*g[a][b]*kk[p1][p2]*Hkk[p2][p1][p2]
-(T(6)*kk[p1][p2]*H[p1][a][b]*Hkk[p2][p1][p2])
-(kk[p2][p2]*H[p1][a][b]*Hkk[p2][p1][p2])
+T(4)*k[p2][b]*Hk[p1][p1][a]*Hkk[p2][p1][p2]
+T(6)*k[p2][a]*Hk[p1][p1][b]*Hkk[p2][p1][p2]
-(T(3)*k[p1][b]*Hk[p1][p2][a]*Hkk[p2][p1][p2])
-(T(2)*k[p2][b]*Hk[p1][p2][a]*Hkk[p2][p1][p2])
+T(5)*k[p1][a]*Hk[p1][p2][b]*Hkk[p2][p1][p2]
+k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p1][p2]
+T(4)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p2][p1][p2]
-(T(2)*g[a][b]*Hkk[p1][p2][p1]*Hkk[p2][p1][p2])
+T(4)*g[a][b]*Hkk[p1][p2][p2]*Hkk[p2][p1][p2]
-(trH[p1]*T(4)*k[p1][a]*k[p1][b]*Hkk[p2][p2][p1])
-(trH[p1]*T(4)*k[p1][b]*k[p2][a]*Hkk[p2][p2][p1])
-(trH[p1]*T(2)*g[a][b]*kk[p1][p1]*Hkk[p2][p2][p1])
+T(4)*kk[p1][p2]*H[p1][a][b]*Hkk[p2][p2][p1]
-(kk[p2][p2]*H[p1][a][b]*Hkk[p2][p2][p1])
+T(4)*k[p1][a]*Hk[p1][p1][b]*Hkk[p2][p2][p1]
+T(4)*k[p1][b]*Hk[p1][p2][a]*Hkk[p2][p2][p1]
-(T(4)*k[p1][a]*Hk[p1][p2][b]*Hkk[p2][p2][p1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term07(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
T(4)*g[a][b]*Hkk[p1][p1][p1]*Hkk[p2][p2][p1]
-(T(4)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p2][p2][p1])
+trH[p1]*T(2)*k[p1][a]*k[p1][b]*Hkk[p2][p2][p2]
-(trH[p1]*g[a][b]*kk[p1][p1]*Hkk[p2][p2][p2])
+T(2)*kk[p1][p1]*H[p1][a][b]*Hkk[p2][p2][p2]
+kk[p1][p2]*H[p1][a][b]*Hkk[p2][p2][p2]
-(T(2)*k[p1][b]*Hk[p1][p1][a]*Hkk[p2][p2][p2])
-(T(2)*k[p1][a]*Hk[p1][p1][b]*Hkk[p2][p2][p2])
+g[a][b]*Hkk[p1][p1][p1]*Hkk[p2][p2][p2]
-(T(4)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p2][p2][p2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term11(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
-(metric[i1]*pow2(kk[p1][p2])*T(2)*H[p1][b][i1]*H[p2][a][i1])
+metric[i1]*T(2)*kk[p1][p1]*kk[p1][p2]*H[p1][b][i1]*H[p2][a][i1]
-(metric[i1]*T(2)*kk[p1][p1]*kk[p2][p2]*H[p1][b][i1]*H[p2][a][i1])
-(metric[i1]*T(2)*kk[p1][p2]*kk[p2][p2]*H[p1][b][i1]*H[p2][a][i1])
+metric[i1]*T(2)*kk[p1][p1]*kk[p1][p2]*H[p1][a][i1]*H[p2][b][i1]
+metric[i1]*T(4)*kk[p1][p1]*kk[p2][p2]*H[p1][a][i1]*H[p2][b][i1]
-(metric[i1]*T(2)*kk[p1][p2]*kk[p2][p2]*H[p1][a][i1]*H[p2][b][i1])
-(metric[i1]*T(6)*k[p1][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p1][i1])
+metric[i1]*k[p2][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p1][i1]
+metric[i1]*T(2)*k[p1][b]*kk[p2][p2]*H[p2][a][i1]*Hk[p1][p1][i1]
-(metric[i1]*T(2)*k[p1][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p1][i1])
+metric[i1]*k[p2][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p1][i1]
+metric[i1]*T(4)*k[p1][a]*kk[p2][p2]*H[p2][b][i1]*Hk[p1][p1][i1]
+metric[i1]*T(4)*k[p2][a]*kk[p2][p2]*H[p2][b][i1]*Hk[p1][p1][i1]
-(metric[i1]*k[p2][b]*kk[p1][p1]*H[p2][a][i1]*Hk[p1][p2][i1])
+metric[i1]*T(4)*k[p1][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p2][i1]
+metric[i1]*T(4)*k[p2][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p2][i1]
-(metric[i1]*k[p2][a]*kk[p1][p1]*H[p2][b][i1]*Hk[p1][p2][i1])
+metric[i1]*T(3)*k[p1][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p2][i1]
-(metric[i1]*T(2)*k[p2][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term12(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(4)*k[p1][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p1][i1])
-(metric[i1]*T(2)*k[p2][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p1][i1])
-(metric[i1]*k[p1][b]*kk[p2][p2]*H[p1][a][i1]*Hk[p2][p1][i1])
+metric[i1]*T(2)*k[p1][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p1][i1]
-(metric[i1]*k[p2][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p1][i1])
-(metric[i1]*k[p1][a]*kk[p2][p2]*H[p1][b][i1]*Hk[p2][p1][i1])
-(metric[i1]*T(4)*k[p1][a]*k[p2][b]*Hk[p1][p1][i1]*Hk[p2][p1][i1])
+metric[i1]*T(8)*g[a][b]*kk[p1][p2]*Hk[p1][p1][i1]*Hk[p2][p1][i1]
-(metric[i1]*T(4)*g[a][b]*kk[p2][p2]*Hk[p1][p1][i1]*Hk[p2][p1][i1])
+metric[i1]*T(6)*k[p1][a]*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p1][i1]
+metric[i1]*T(4)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p1][i1]
-(metric[i1]*T(2)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p1][i1])
-(metric[i1]*T(6)*k[p2][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p1][i1])
+metric[i1]*T(4)*g[a][b]*kk[p1][p1]*Hk[p1][p2][i1]*Hk[p2][p1][i1]
-(metric[i1]*T(2)*g[a][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p1][i1])
-(metric[i1]*T(4)*g[a][b]*kk[p2][p2]*Hk[p1][p2][i1]*Hk[p2][p1][i1])
-(metric[i1]*T(4)*k[p2][b]*kk[p1][p1]*H[p1][a][i1]*Hk[p2][p2][i1])
+metric[i1]*k[p1][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p2][i1]
+metric[i1]*T(6)*k[p2][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p2][i1]
-(metric[i1]*T(4)*k[p1][a]*kk[p1][p1]*H[p1][b][i1]*Hk[p2][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGG_Term13(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(6)*k[p2][a]*kk[p1][p1]*H[p1][b][i1]*Hk[p2][p2][i1])
+metric[i1]*k[p1][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p2][i1]
+metric[i1]*T(2)*k[p2][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p2][i1]
+metric[i1]*T(4)*k[p1][a]*k[p1][b]*Hk[p1][p1][i1]*Hk[p2][p2][i1]
+metric[i1]*T(2)*k[p1][b]*k[p2][a]*Hk[p1][p1][i1]*Hk[p2][p2][i1]
-(metric[i1]*T(4)*k[p2][a]*k[p2][b]*Hk[p1][p1][i1]*Hk[p2][p2][i1])
-(metric[i1]*T(4)*g[a][b]*kk[p1][p1]*Hk[p1][p1][i1]*Hk[p2][p2][i1])
-(metric[i1]*T(2)*g[a][b]*kk[p1][p2]*Hk[p1][p1][i1]*Hk[p2][p2][i1])
+metric[i1]*T(4)*g[a][b]*kk[p2][p2]*Hk[p1][p1][i1]*Hk[p2][p2][i1]
-(metric[i1]*T(4)*k[p1][a]*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p2][i1])
+metric[i1]*T(4)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p2][i1]
+metric[i1]*T(8)*g[a][b]*kk[p1][p1]*Hk[p1][p2][i1]*Hk[p2][p2][i1]
-(metric[i1]*T(8)*g[a][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p2][i1])
;
      return theentry;
}

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CurrentRule_GB_GGG(const std::vector<momentumD<T, D> const*>& momlist,
                                     const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {

    // incoming momenta
    std::vector<momD<T, D>> k;
    k.emplace_back(-(*(momlist[0])));
    k.emplace_back(-(*(momlist[1])));

    // the input gravitons
    std::vector<Tensor<T, D, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[0]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[1]->get_current()));

    // metric
    Tensor<T, D, 2> hout(0), g(0);
    const std::vector<T>& metric(hout.get_metric_extended());

    for (size_t ii = 0; ii < D; ii++) { g[ii][ii] = T(1)/metric[ii]; }

    // tensors contractions
    std::vector<std::vector<Tensor<T, D, 1>>> Hk;
    for (size_t p1 = 0; p1 < 2; p1++) {
        std::vector<Tensor<T, D, 1>> fill_Hk;
        for (size_t p2 = 0; p2 < 2; p2++) { fill_Hk.emplace_back(index_contraction<0>(k[p2], H[p1])); }
        Hk.emplace_back(fill_Hk);
    }

    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array3D Hkk(2, array2D(2, array1D(2, T(0))));
    array2D HH(2, array1D(2, T(0)));
    array2D kk(2, array1D(2, T(0)));
    array1D trH(2, T(0));

    for (size_t p1 = 0; p1 < 2; p1++) {

        for (size_t ii = 0; ii < D; ii++) { 
				trH[p1] += H[p1][ii][ii] * metric[ii]; 
		}

        for (size_t p2 = 0; p2 < 2; p2++) {
            for (size_t ii = 0; ii < D; ii++) {
                kk[p1][p2] += k[p1][ii] * k[p2][ii] * metric[ii];
                for (size_t jj = 0; jj < D; jj++) { HH[p1][p2] += H[p1][ii][jj] * H[p2][ii][jj] * metric[ii] * metric[jj]; }
            }
            for (size_t p3 = 0; p3 < 2; p3++) {
                for (size_t ii = 0; ii < D; ii++) { Hkk[p1][p2][p3] += Hk[p1][p2][ii] * k[p3][ii] * metric[ii]; }
            }
        }
    }

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1}, {1, 0}};

    for (size_t a = 0; a < D; a++) {
        for (size_t b = 0; b < D; b++) {

            T theentry(0);

            for (size_t ip = 0; ip < 2; ip++) {
                size_t p1 = perm[ip][0];
                size_t p2 = perm[ip][1];

                for (size_t i1 = 0; i1 < D; i1++) {
            	 theentry += CurrentRule_GB_GGG_Term11<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2);
            	 theentry += CurrentRule_GB_GGG_Term12<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2);
            	 theentry += CurrentRule_GB_GGG_Term13<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2);
                        ;
                }

                theentry += CurrentRule_GB_GGG_Term01<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);
                theentry += CurrentRule_GB_GGG_Term02<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);
                theentry += CurrentRule_GB_GGG_Term03<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);
                theentry += CurrentRule_GB_GGG_Term04<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);
                theentry += CurrentRule_GB_GGG_Term05<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);
                theentry += CurrentRule_GB_GGG_Term06<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);
                theentry += CurrentRule_GB_GGG_Term07<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);

                    ;
            }
            hout[a][b] = theentry;
        }
    }

    // factor of I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
    // hout*= T(0,1);
    // hout *= T(1) / T(2);

    return v_t_to_v_extended_t<T, Tcur>(hout.vectorize());
}

// begin terms

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term01(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((pow2(kk[p2][p3])*trH[p1]*trH[p2]*trH[p3]*T(3)*g[a][b])/T(2))
+pow2(kk[p2][p3])*trH[p2]*T(6)*g[a][b]*HH[p1][p3]
+(pow2(kk[p2][p3])*trH[p1]*T(3)*g[a][b]*HH[p2][p3])/T(2)
-(trH[p3]*T(6)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p1][p3])
-(trH[p1]*T(3)*HH[p2][p3]*k[p1][a]*k[p2][b]*kk[p1][p3])
+(trH[p1]*trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p1][p2]*kk[p1][p3])/T(2)
-(trH[p3]*T(3)*g[a][b]*HH[p1][p2]*kk[p1][p2]*kk[p1][p3])
-(trH[p2]*T(6)*g[a][b]*HH[p1][p3]*kk[p1][p2]*kk[p1][p3])
+trH[p1]*T(3)*HH[p2][p3]*k[p2][a]*k[p3][b]*kk[p2][p2]
-((trH[p1]*trH[p2]*trH[p3]*T(3)*k[p1][a]*k[p1][b]*kk[p2][p3])/T(2))
+trH[p3]*T(3)*HH[p1][p2]*k[p1][a]*k[p1][b]*kk[p2][p3]
+(trH[p1]*T(9)*HH[p2][p3]*k[p1][a]*k[p1][b]*kk[p2][p3])/T(2)
+trH[p3]*T(6)*HH[p1][p2]*k[p1][b]*k[p2][a]*kk[p2][p3]
-(trH[p2]*T(3)*HH[p1][p3]*k[p1][a]*k[p2][b]*kk[p2][p3])
-((trH[p1]*T(15)*HH[p2][p3]*k[p2][b]*k[p3][a]*kk[p2][p3])/T(2))
+trH[p1]*trH[p2]*trH[p3]*T(3)*k[p2][a]*k[p3][b]*kk[p2][p3]
-(trH[p3]*T(6)*HH[p1][p2]*k[p2][a]*k[p3][b]*kk[p2][p3])
-(trH[p2]*T(6)*HH[p1][p3]*k[p2][a]*k[p3][b]*kk[p2][p3])
+(trH[p1]*T(9)*HH[p2][p3]*k[p2][a]*k[p3][b]*kk[p2][p3])/T(2)
+(trH[p1]*trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p1][p1]*kk[p2][p3])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term02(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p3]*T(3)*g[a][b]*HH[p1][p2]*kk[p1][p1]*kk[p2][p3])
+trH[p2]*T(3)*g[a][b]*HH[p1][p3]*kk[p1][p1]*kk[p2][p3]
-((trH[p1]*T(3)*g[a][b]*HH[p2][p3]*kk[p1][p1]*kk[p2][p3])/T(2))
-((trH[p1]*trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p1][p2]*kk[p2][p3])/T(2))
+trH[p3]*T(9)*g[a][b]*HH[p1][p2]*kk[p1][p2]*kk[p2][p3]
+trH[p3]*T(3)*g[a][b]*HH[p1][p2]*kk[p1][p3]*kk[p2][p3]
-(trH[p1]*T(9)*g[a][b]*HH[p2][p3]*kk[p2][p2]*kk[p2][p3])
+trH[p3]*T(3)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p3][p3]
-(trH[p1]*trH[p2]*trH[p3]*T(3)*k[p2][a]*k[p2][b]*kk[p3][p3])
+trH[p3]*T(6)*HH[p1][p2]*k[p2][a]*k[p2][b]*kk[p3][p3]
+trH[p2]*T(6)*HH[p1][p3]*k[p2][a]*k[p2][b]*kk[p3][p3]
+trH[p1]*T(3)*HH[p2][p3]*k[p2][a]*k[p2][b]*kk[p3][p3]
-(trH[p1]*T(3)*HH[p2][p3]*k[p2][b]*k[p3][a]*kk[p3][p3])
+(trH[p1]*trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p1][p2]*kk[p3][p3])/T(4)
-(trH[p3]*T(3)*g[a][b]*HH[p1][p2]*kk[p1][p2]*kk[p3][p3])
-(trH[p1]*T(3)*g[a][b]*HH[p2][p3]*kk[p1][p2]*kk[p3][p3])
+(trH[p1]*trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p2][p2]*kk[p3][p3])/T(2)
-((trH[p3]*T(9)*g[a][b]*HH[p1][p2]*kk[p2][p2]*kk[p3][p3])/T(2))
-((trH[p2]*T(3)*g[a][b]*HH[p1][p3]*kk[p2][p2]*kk[p3][p3])/T(2))
-((trH[p1]*T(3)*g[a][b]*HH[p2][p3]*kk[p2][p2]*kk[p3][p3])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term03(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p1]*T(9)*g[a][b]*HH[p2][p3]*kk[p2][p3]*kk[p3][p3]
-(pow2(kk[p2][p3])*trH[p2]*trH[p3]*T(3)*H[p1][a][b])
+pow2(kk[p2][p3])*T(3)*HH[p2][p3]*H[p1][a][b]
-(trH[p2]*trH[p3]*T(3)*kk[p1][p2]*kk[p1][p3]*H[p1][a][b])
-(trH[p2]*trH[p3]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p1][a][b])
+T(3)*HH[p2][p3]*kk[p1][p2]*kk[p2][p3]*H[p1][a][b]
-(T(18)*HH[p2][p3]*kk[p2][p2]*kk[p2][p3]*H[p1][a][b])
+trH[p2]*trH[p3]*T(3)*kk[p2][p2]*kk[p3][p3]*H[p1][a][b]
-(T(3)*HH[p2][p3]*kk[p2][p2]*kk[p3][p3]*H[p1][a][b])
+T(18)*HH[p2][p3]*kk[p2][p3]*kk[p3][p3]*H[p1][a][b]
+(pow2(kk[p2][p3])*trH[p1]*trH[p3]*T(3)*H[p2][a][b])/T(2)
-(pow2(kk[p2][p3])*T(3)*HH[p1][p3]*H[p2][a][b])
+trH[p1]*trH[p3]*T(3)*kk[p1][p2]*kk[p1][p3]*H[p2][a][b]
+T(6)*HH[p1][p3]*kk[p1][p2]*kk[p1][p3]*H[p2][a][b]
+trH[p1]*trH[p3]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p2][a][b]
+(T(3)*HH[p1][p3]*kk[p1][p2]*kk[p2][p3]*H[p2][a][b])/T(2)
-(trH[p1]*trH[p3]*T(6)*kk[p2][p2]*kk[p2][p3]*H[p2][a][b])
+T(12)*HH[p1][p3]*kk[p2][p2]*kk[p2][p3]*H[p2][a][b]
-((trH[p1]*trH[p3]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p2][a][b])/T(2))
+T(3)*HH[p1][p3]*kk[p1][p2]*kk[p3][p3]*H[p2][a][b]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term04(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((trH[p1]*trH[p3]*T(3)*kk[p2][p2]*kk[p3][p3]*H[p2][a][b])/T(2))
+T(3)*HH[p1][p3]*kk[p2][p2]*kk[p3][p3]*H[p2][a][b]
+(pow2(kk[p2][p3])*trH[p1]*trH[p2]*T(3)*H[p3][a][b])/T(2)
-(pow2(kk[p2][p3])*T(3)*HH[p1][p2]*H[p3][a][b])
-((trH[p1]*trH[p2]*T(3)*kk[p1][p1]*kk[p2][p3]*H[p3][a][b])/T(2))
-(T(9)*HH[p1][p2]*kk[p1][p2]*kk[p2][p3]*H[p3][a][b])
-((T(9)*HH[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p3][a][b])/T(2))
-((trH[p1]*trH[p2]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p3][a][b])/T(2))
+(T(9)*HH[p1][p2]*kk[p1][p2]*kk[p3][p3]*H[p3][a][b])/T(2)
-((trH[p1]*trH[p2]*T(3)*kk[p2][p2]*kk[p3][p3]*H[p3][a][b])/T(2))
+T(3)*HH[p1][p2]*kk[p2][p2]*kk[p3][p3]*H[p3][a][b]
+trH[p1]*trH[p2]*T(6)*kk[p2][p3]*kk[p3][p3]*H[p3][a][b]
-(T(12)*HH[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p3][a][b])
+T(6)*k[p1][a]*k[p2][b]*kk[p1][p3]*HHH[p1][p2][p3]
-(T(15)*k[p1][a]*k[p1][b]*kk[p2][p3]*HHH[p1][p2][p3])
-((T(3)*k[p1][b]*k[p2][a]*kk[p2][p3]*HHH[p1][p2][p3])/T(2))
-(T(18)*k[p1][a]*k[p2][b]*kk[p2][p3]*HHH[p1][p2][p3])
+T(12)*g[a][b]*kk[p1][p1]*kk[p2][p3]*HHH[p1][p2][p3]
+T(24)*g[a][b]*kk[p1][p2]*kk[p2][p3]*HHH[p1][p2][p3]
+T(6)*g[a][b]*kk[p1][p2]*kk[p3][p3]*HHH[p1][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term05(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(T(39)*k[p1][a]*k[p2][b]*kk[p1][p3]*HHH[p1][p3][p2])/T(2)
+T(12)*g[a][b]*kk[p1][p2]*kk[p1][p3]*HHH[p1][p3][p2]
+(T(27)*k[p1][a]*k[p1][b]*kk[p2][p3]*HHH[p1][p3][p2])/T(2)
+T(3)*k[p1][a]*k[p2][b]*kk[p3][p3]*HHH[p1][p3][p2]
-((T(33)*k[p1][a]*k[p2][b]*kk[p1][p3]*HHH[p2][p1][p3])/T(2))
-(T(6)*k[p1][a]*k[p1][b]*kk[p2][p3]*HHH[p2][p1][p3])
-(T(6)*g[a][b]*kk[p1][p1]*kk[p2][p3]*HHH[p2][p1][p3])
-(T(3)*g[a][b]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p1][p3])
+(T(3)*k[p1][a]*k[p2][b]*kk[p3][p3]*HHH[p2][p1][p3])/T(2)
-(pow2(kk[p2][p3])*T(42)*g[a][b]*HHH[p2][p3][p1])
-(T(12)*k[p2][a]*k[p3][b]*kk[p2][p2]*HHH[p2][p3][p1])
-(T(3)*k[p1][b]*k[p2][a]*kk[p2][p3]*HHH[p2][p3][p1])
+T(18)*k[p1][a]*k[p2][b]*kk[p2][p3]*HHH[p2][p3][p1]
+T(6)*k[p2][a]*k[p2][b]*kk[p2][p3]*HHH[p2][p3][p1]
+T(33)*k[p2][b]*k[p3][a]*kk[p2][p3]*HHH[p2][p3][p1]
+T(39)*k[p2][a]*k[p3][b]*kk[p2][p3]*HHH[p2][p3][p1]
+T(12)*k[p3][a]*k[p3][b]*kk[p2][p3]*HHH[p2][p3][p1]
+T(12)*g[a][b]*kk[p1][p2]*kk[p2][p3]*HHH[p2][p3][p1]
+T(36)*g[a][b]*kk[p2][p2]*kk[p2][p3]*HHH[p2][p3][p1]
+T(3)*k[p1][a]*k[p2][b]*kk[p3][p3]*HHH[p2][p3][p1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term06(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(6)*k[p2][a]*k[p3][b]*kk[p3][p3]*HHH[p2][p3][p1])
-(T(15)*g[a][b]*kk[p1][p2]*kk[p3][p3]*HHH[p2][p3][p1])
-(T(6)*k[p1][a]*k[p1][b]*kk[p2][p3]*HHH[p3][p1][p2])
+T(3)*k[p1][a]*k[p2][b]*kk[p2][p3]*HHH[p3][p1][p2]
+T(9)*g[a][b]*kk[p1][p2]*kk[p3][p3]*HHH[p3][p1][p2]
+pow2(kk[p2][p3])*T(36)*g[a][b]*HHH[p3][p2][p1]
+T(3)*k[p1][a]*k[p2][b]*kk[p1][p3]*HHH[p3][p2][p1]
+(T(15)*k[p1][a]*k[p1][b]*kk[p2][p3]*HHH[p3][p2][p1])/T(2)
+(T(3)*k[p1][b]*k[p2][a]*kk[p2][p3]*HHH[p3][p2][p1])/T(2)
+(T(15)*k[p1][a]*k[p2][b]*kk[p2][p3]*HHH[p3][p2][p1])/T(2)
-(T(6)*k[p2][a]*k[p2][b]*kk[p2][p3]*HHH[p3][p2][p1])
-(T(6)*k[p1][b]*k[p3][a]*kk[p2][p3]*HHH[p3][p2][p1])
-(T(3)*k[p2][b]*k[p3][a]*kk[p2][p3]*HHH[p3][p2][p1])
-((T(15)*k[p1][a]*k[p3][b]*kk[p2][p3]*HHH[p3][p2][p1])/T(2))
-(T(57)*k[p2][a]*k[p3][b]*kk[p2][p3]*HHH[p3][p2][p1])
-(T(12)*k[p3][a]*k[p3][b]*kk[p2][p3]*HHH[p3][p2][p1])
-(T(12)*g[a][b]*kk[p1][p2]*kk[p2][p3]*HHH[p3][p2][p1])
-(T(36)*g[a][b]*kk[p1][p3]*kk[p2][p3]*HHH[p3][p2][p1])
-((T(21)*k[p1][a]*k[p2][b]*kk[p3][p3]*HHH[p3][p2][p1])/T(2))
-(T(12)*k[p2][a]*k[p2][b]*kk[p3][p3]*HHH[p3][p2][p1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term07(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(12)*k[p2][b]*k[p3][a]*kk[p3][p3]*HHH[p3][p2][p1]
+T(6)*k[p2][a]*k[p3][b]*kk[p3][p3]*HHH[p3][p2][p1]
+T(6)*g[a][b]*kk[p2][p2]*kk[p3][p3]*HHH[p3][p2][p1]
-(T(36)*g[a][b]*kk[p2][p3]*kk[p3][p3]*HHH[p3][p2][p1])
+trH[p2]*trH[p3]*T(3)*k[p1][b]*kk[p1][p3]*Hk[p1][p2][a]
-(T(6)*HH[p2][p3]*k[p1][b]*kk[p1][p3]*Hk[p1][p2][a])
-(T(9)*HH[p2][p3]*k[p1][b]*kk[p2][p3]*Hk[p1][p2][a])
+(trH[p2]*trH[p3]*T(3)*k[p1][b]*kk[p3][p3]*Hk[p1][p2][a])/T(2)
-(T(3)*HH[p2][p3]*k[p1][b]*kk[p3][p3]*Hk[p1][p2][a])
+trH[p2]*trH[p3]*T(6)*k[p1][a]*kk[p1][p3]*Hk[p1][p2][b]
-(T(3)*HH[p2][p3]*k[p1][a]*kk[p1][p3]*Hk[p1][p2][b])
-(T(9)*HH[p2][p3]*k[p1][a]*kk[p2][p3]*Hk[p1][p2][b])
+(trH[p2]*trH[p3]*T(3)*k[p1][a]*kk[p3][p3]*Hk[p1][p2][b])/T(2)
-(T(3)*HH[p2][p3]*k[p1][a]*kk[p3][p3]*Hk[p1][p2][b])
+T(6)*HH[p2][p3]*k[p1][b]*kk[p1][p2]*Hk[p1][p3][a]
+(T(3)*HH[p2][p3]*k[p2][b]*kk[p1][p2]*Hk[p1][p3][a])/T(2)
-(T(3)*HH[p2][p3]*k[p3][b]*kk[p1][p2]*Hk[p1][p3][a])
+(trH[p2]*trH[p3]*T(3)*k[p1][b]*kk[p2][p2]*Hk[p1][p3][a])/T(2)
+T(9)*HH[p2][p3]*k[p1][b]*kk[p2][p3]*Hk[p1][p3][a]
-(trH[p2]*trH[p3]*T(3)*k[p1][a]*kk[p1][p2]*Hk[p1][p3][b])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term08(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(3)*HH[p2][p3]*k[p1][a]*kk[p1][p2]*Hk[p1][p3][b]
+T(3)*HH[p2][p3]*k[p3][a]*kk[p1][p2]*Hk[p1][p3][b]
+(trH[p2]*trH[p3]*T(3)*k[p1][a]*kk[p2][p2]*Hk[p1][p3][b])/T(2)
+T(9)*HH[p2][p3]*k[p1][a]*kk[p2][p3]*Hk[p1][p3][b]
+T(15)*HH[p1][p3]*k[p1][b]*kk[p2][p3]*Hk[p2][p1][a]
-(trH[p1]*trH[p3]*T(3)*k[p2][b]*kk[p2][p3]*Hk[p2][p1][a])
-((T(3)*HH[p1][p3]*k[p2][b]*kk[p2][p3]*Hk[p2][p1][a])/T(2))
-(trH[p3]*T(3)*kk[p1][p3]*Hk[p1][p2][b]*Hk[p2][p1][a])
-(trH[p3]*T(3)*kk[p2][p3]*Hk[p1][p2][b]*Hk[p2][p1][a])
-(trH[p3]*T(3)*kk[p3][p3]*Hk[p1][p2][b]*Hk[p2][p1][a])
-(trH[p3]*T(6)*kk[p2][p3]*Hk[p1][p3][b]*Hk[p2][p1][a])
+T(9)*HH[p1][p3]*k[p1][a]*kk[p2][p3]*Hk[p2][p1][b]
-(trH[p1]*trH[p3]*T(3)*k[p2][a]*kk[p2][p3]*Hk[p2][p1][b])
-((T(3)*HH[p1][p3]*k[p2][a]*kk[p2][p3]*Hk[p2][p1][b])/T(2))
-(trH[p3]*T(3)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p1][b])
-(trH[p3]*T(3)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p1][b])
-(T(6)*HH[p1][p3]*k[p1][b]*kk[p2][p3]*Hk[p2][p2][a])
+trH[p1]*trH[p3]*T(3)*k[p2][b]*kk[p2][p3]*Hk[p2][p2][a]
-(T(6)*HH[p1][p3]*k[p2][b]*kk[p2][p3]*Hk[p2][p2][a])
-(trH[p1]*trH[p3]*T(3)*k[p3][b]*kk[p2][p3]*Hk[p2][p2][a])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term09(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(6)*HH[p1][p3]*k[p3][b]*kk[p2][p3]*Hk[p2][p2][a]
+(trH[p1]*trH[p3]*T(3)*k[p2][b]*kk[p3][p3]*Hk[p2][p2][a])/T(2)
-(T(3)*HH[p1][p3]*k[p2][b]*kk[p3][p3]*Hk[p2][p2][a])
-(T(3)*HH[p1][p3]*k[p1][a]*kk[p2][p3]*Hk[p2][p2][b])
+trH[p1]*trH[p3]*T(3)*k[p2][a]*kk[p2][p3]*Hk[p2][p2][b]
-(T(6)*HH[p1][p3]*k[p2][a]*kk[p2][p3]*Hk[p2][p2][b])
+(trH[p1]*trH[p3]*T(3)*k[p2][a]*kk[p3][p3]*Hk[p2][p2][b])/T(2)
-(T(3)*HH[p1][p3]*k[p2][a]*kk[p3][p3]*Hk[p2][p2][b])
-(trH[p1]*trH[p3]*T(6)*k[p1][b]*kk[p1][p2]*Hk[p2][p3][a])
+T(12)*HH[p1][p3]*k[p1][b]*kk[p1][p2]*Hk[p2][p3][a]
+T(6)*HH[p1][p3]*k[p1][b]*kk[p2][p2]*Hk[p2][p3][a]
+trH[p1]*trH[p3]*T(3)*k[p2][b]*kk[p2][p2]*Hk[p2][p3][a]
-(T(6)*HH[p1][p3]*k[p2][b]*kk[p2][p2]*Hk[p2][p3][a])
+trH[p1]*trH[p3]*T(3)*k[p3][b]*kk[p2][p2]*Hk[p2][p3][a]
-(T(6)*HH[p1][p3]*k[p3][b]*kk[p2][p2]*Hk[p2][p3][a])
+T(12)*HH[p1][p3]*k[p1][b]*kk[p2][p3]*Hk[p2][p3][a]
-((trH[p1]*trH[p3]*T(3)*k[p2][b]*kk[p2][p3]*Hk[p2][p3][a])/T(2))
+T(3)*HH[p1][p3]*k[p2][b]*kk[p2][p3]*Hk[p2][p3][a]
-(trH[p1]*trH[p3]*T(3)*k[p3][b]*kk[p2][p3]*Hk[p2][p3][a])
+T(6)*HH[p1][p3]*k[p3][b]*kk[p2][p3]*Hk[p2][p3][a]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term010(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p3]*T(6)*kk[p1][p2]*Hk[p1][p2][b]*Hk[p2][p3][a]
-(trH[p3]*T(6)*kk[p1][p2]*Hk[p1][p3][b]*Hk[p2][p3][a])
-(trH[p1]*trH[p3]*T(6)*k[p1][a]*kk[p1][p2]*Hk[p2][p3][b])
+(T(27)*HH[p1][p3]*k[p1][a]*kk[p1][p2]*Hk[p2][p3][b])/T(2)
+T(3)*HH[p1][p3]*k[p1][a]*kk[p2][p2]*Hk[p2][p3][b]
+trH[p1]*trH[p3]*T(3)*k[p2][a]*kk[p2][p2]*Hk[p2][p3][b]
-(T(6)*HH[p1][p3]*k[p2][a]*kk[p2][p2]*Hk[p2][p3][b])
+T(6)*HH[p1][p3]*k[p1][a]*kk[p2][p3]*Hk[p2][p3][b]
-((trH[p1]*trH[p3]*T(3)*k[p2][a]*kk[p2][p3]*Hk[p2][p3][b])/T(2))
+T(3)*HH[p1][p3]*k[p2][a]*kk[p2][p3]*Hk[p2][p3][b]
-(trH[p1]*trH[p3]*T(3)*k[p3][a]*kk[p2][p3]*Hk[p2][p3][b])
+T(6)*HH[p1][p3]*k[p3][a]*kk[p2][p3]*Hk[p2][p3][b]
+trH[p3]*T(3)*kk[p1][p2]*Hk[p1][p2][a]*Hk[p2][p3][b]
+trH[p3]*T(6)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p3][b]
+trH[p3]*T(6)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p3][b]
+trH[p1]*trH[p2]*T(3)*k[p1][b]*kk[p2][p3]*Hk[p3][p1][a]
-(T(15)*HH[p1][p2]*k[p1][b]*kk[p2][p3]*Hk[p3][p1][a])
-(trH[p1]*trH[p2]*T(3)*k[p2][b]*kk[p2][p3]*Hk[p3][p1][a])
+(T(9)*HH[p1][p2]*k[p2][b]*kk[p2][p3]*Hk[p3][p1][a])/T(2)
+(T(9)*HH[p1][p2]*k[p3][b]*kk[p2][p3]*Hk[p3][p1][a])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term011(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p2]*T(3)*kk[p1][p3]*Hk[p1][p2][b]*Hk[p3][p1][a])
+(trH[p2]*T(15)*kk[p2][p3]*Hk[p1][p2][b]*Hk[p3][p1][a])/T(2)
-(trH[p2]*T(6)*kk[p3][p3]*Hk[p1][p2][b]*Hk[p3][p1][a])
+trH[p2]*T(3)*kk[p1][p2]*Hk[p1][p3][b]*Hk[p3][p1][a]
-(trH[p1]*T(3)*kk[p2][p3]*Hk[p2][p1][b]*Hk[p3][p1][a])
-(trH[p1]*T(3)*kk[p2][p3]*Hk[p2][p2][b]*Hk[p3][p1][a])
+trH[p1]*T(3)*kk[p2][p2]*Hk[p2][p3][b]*Hk[p3][p1][a]
+trH[p1]*trH[p2]*T(3)*k[p1][a]*kk[p2][p3]*Hk[p3][p1][b]
-(T(15)*HH[p1][p2]*k[p1][a]*kk[p2][p3]*Hk[p3][p1][b])
-(T(3)*HH[p1][p2]*k[p2][a]*kk[p2][p3]*Hk[p3][p1][b])
+(T(9)*HH[p1][p2]*k[p3][a]*kk[p2][p3]*Hk[p3][p1][b])/T(2)
-(trH[p2]*T(6)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p3][p1][b])
-(trH[p2]*T(6)*kk[p3][p3]*Hk[p1][p2][a]*Hk[p3][p1][b])
+trH[p2]*T(6)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p3][p1][b]
+trH[p2]*T(3)*kk[p2][p3]*Hk[p1][p3][a]*Hk[p3][p1][b]
+trH[p1]*trH[p2]*T(6)*k[p1][b]*kk[p1][p3]*Hk[p3][p2][a]
-(T(15)*HH[p1][p2]*k[p1][b]*kk[p1][p3]*Hk[p3][p2][a])
-(T(12)*HH[p1][p2]*k[p1][b]*kk[p2][p3]*Hk[p3][p2][a])
+trH[p1]*trH[p2]*T(3)*k[p2][b]*kk[p2][p3]*Hk[p3][p2][a]
-(T(6)*HH[p1][p2]*k[p2][b]*kk[p2][p3]*Hk[p3][p2][a])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term012(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((trH[p1]*trH[p2]*T(3)*k[p3][b]*kk[p2][p3]*Hk[p3][p2][a])/T(2))
+T(3)*HH[p1][p2]*k[p3][b]*kk[p2][p3]*Hk[p3][p2][a]
-(T(9)*HH[p1][p2]*k[p1][b]*kk[p3][p3]*Hk[p3][p2][a])
-(trH[p1]*trH[p2]*T(3)*k[p3][b]*kk[p3][p3]*Hk[p3][p2][a])
+T(6)*HH[p1][p2]*k[p3][b]*kk[p3][p3]*Hk[p3][p2][a]
+(trH[p2]*T(9)*kk[p1][p2]*Hk[p1][p3][b]*Hk[p3][p2][a])/T(2)
-(trH[p1]*T(3)*kk[p1][p3]*Hk[p2][p1][b]*Hk[p3][p2][a])
+trH[p1]*T(3)*kk[p2][p3]*Hk[p2][p1][b]*Hk[p3][p2][a]
+trH[p1]*T(3)*kk[p3][p3]*Hk[p2][p1][b]*Hk[p3][p2][a]
+trH[p1]*T(3)*kk[p3][p3]*Hk[p2][p2][b]*Hk[p3][p2][a]
-(trH[p1]*T(3)*kk[p1][p1]*Hk[p2][p3][b]*Hk[p3][p2][a])
-(trH[p1]*T(3)*kk[p2][p2]*Hk[p2][p3][b]*Hk[p3][p2][a])
+trH[p1]*T(3)*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p2][a]
+(trH[p1]*T(3)*kk[p3][p3]*Hk[p2][p3][b]*Hk[p3][p2][a])/T(2)
+trH[p1]*trH[p2]*T(3)*k[p1][a]*kk[p1][p3]*Hk[p3][p2][b]
-((T(15)*HH[p1][p2]*k[p1][a]*kk[p1][p3]*Hk[p3][p2][b])/T(2))
-(T(6)*HH[p1][p2]*k[p1][a]*kk[p2][p3]*Hk[p3][p2][b])
+trH[p1]*trH[p2]*T(3)*k[p2][a]*kk[p2][p3]*Hk[p3][p2][b]
-(T(6)*HH[p1][p2]*k[p2][a]*kk[p2][p3]*Hk[p3][p2][b])
-((trH[p1]*trH[p2]*T(3)*k[p3][a]*kk[p2][p3]*Hk[p3][p2][b])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term013(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(3)*HH[p1][p2]*k[p3][a]*kk[p2][p3]*Hk[p3][p2][b]
-(T(6)*HH[p1][p2]*k[p1][a]*kk[p3][p3]*Hk[p3][p2][b])
+trH[p1]*trH[p2]*T(3)*k[p2][a]*kk[p3][p3]*Hk[p3][p2][b]
-(T(6)*HH[p1][p2]*k[p2][a]*kk[p3][p3]*Hk[p3][p2][b])
-(trH[p1]*trH[p2]*T(3)*k[p3][a]*kk[p3][p3]*Hk[p3][p2][b])
+T(6)*HH[p1][p2]*k[p3][a]*kk[p3][p3]*Hk[p3][p2][b]
-((trH[p2]*T(3)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p3][p2][b])/T(2))
-(trH[p2]*T(3)*kk[p1][p3]*Hk[p1][p3][a]*Hk[p3][p2][b])
-((trH[p1]*T(3)*kk[p1][p3]*Hk[p2][p1][a]*Hk[p3][p2][b])/T(2))
-((trH[p1]*T(3)*kk[p2][p3]*Hk[p2][p2][a]*Hk[p3][p2][b])/T(2))
-(trH[p1]*T(6)*kk[p3][p3]*Hk[p2][p2][a]*Hk[p3][p2][b])
-((trH[p1]*T(3)*kk[p2][p2]*Hk[p2][p3][a]*Hk[p3][p2][b])/T(2))
+trH[p1]*T(3)*kk[p3][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]
+(trH[p1]*trH[p2]*T(3)*k[p3][b]*kk[p1][p2]*Hk[p3][p3][a])/T(2)
-((T(9)*HH[p1][p2]*k[p3][b]*kk[p1][p2]*Hk[p3][p3][a])/T(2))
+(trH[p1]*trH[p2]*T(3)*k[p3][b]*kk[p2][p2]*Hk[p3][p3][a])/T(2)
-(T(3)*HH[p1][p2]*k[p3][b]*kk[p2][p2]*Hk[p3][p3][a])
+T(9)*HH[p1][p2]*k[p1][b]*kk[p2][p3]*Hk[p3][p3][a]
-(trH[p1]*trH[p2]*T(3)*k[p3][b]*kk[p2][p3]*Hk[p3][p3][a])
+T(6)*HH[p1][p2]*k[p3][b]*kk[p2][p3]*Hk[p3][p3][a]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term014(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p2]*T(6)*kk[p1][p3]*Hk[p1][p2][b]*Hk[p3][p3][a]
-(trH[p1]*T(3)*kk[p2][p3]*Hk[p2][p1][b]*Hk[p3][p3][a])
-(trH[p1]*T(3)*kk[p2][p3]*Hk[p2][p2][b]*Hk[p3][p3][a])
-(trH[p1]*T(3)*kk[p1][p2]*Hk[p2][p3][b]*Hk[p3][p3][a])
+trH[p1]*T(3)*kk[p2][p2]*Hk[p2][p3][b]*Hk[p3][p3][a]
+(trH[p1]*T(3)*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p3][a])/T(2)
+(trH[p1]*trH[p2]*T(3)*k[p3][a]*kk[p1][p2]*Hk[p3][p3][b])/T(2)
-((T(9)*HH[p1][p2]*k[p3][a]*kk[p1][p2]*Hk[p3][p3][b])/T(2))
+(trH[p1]*trH[p2]*T(3)*k[p3][a]*kk[p2][p2]*Hk[p3][p3][b])/T(2)
-(T(3)*HH[p1][p2]*k[p3][a]*kk[p2][p2]*Hk[p3][p3][b])
+T(6)*HH[p1][p2]*k[p1][a]*kk[p2][p3]*Hk[p3][p3][b]
-(trH[p1]*trH[p2]*T(3)*k[p2][a]*kk[p2][p3]*Hk[p3][p3][b])
+T(6)*HH[p1][p2]*k[p2][a]*kk[p2][p3]*Hk[p3][p3][b]
-(trH[p1]*trH[p2]*T(3)*k[p3][a]*kk[p2][p3]*Hk[p3][p3][b])
+T(6)*HH[p1][p2]*k[p3][a]*kk[p2][p3]*Hk[p3][p3][b]
+trH[p2]*T(6)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p3][p3][b]
+trH[p2]*T(3)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p3][p3][b]
+trH[p1]*T(6)*kk[p2][p3]*Hk[p2][p2][a]*Hk[p3][p3][b]
-(trH[p1]*T(6)*kk[p1][p2]*Hk[p2][p3][a]*Hk[p3][p3][b])
-(trH[p1]*T(6)*kk[p2][p2]*Hk[p2][p3][a]*Hk[p3][p3][b])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term015(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p2]*trH[p3]*T(6)*g[a][b]*kk[p1][p3]*Hkk[p1][p1][p2])
+T(12)*g[a][b]*HH[p2][p3]*kk[p2][p3]*Hkk[p1][p1][p2]
-((trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p3][p3]*Hkk[p1][p1][p2])/T(2))
+trH[p3]*T(6)*kk[p1][p3]*H[p2][a][b]*Hkk[p1][p1][p2]
+trH[p3]*T(6)*kk[p2][p3]*H[p2][a][b]*Hkk[p1][p1][p2]
-(trH[p2]*T(9)*kk[p2][p3]*H[p3][a][b]*Hkk[p1][p1][p2])
+trH[p2]*T(3)*kk[p3][p3]*H[p3][a][b]*Hkk[p1][p1][p2]
-(trH[p3]*T(6)*k[p2][b]*Hk[p2][p3][a]*Hkk[p1][p1][p2])
-(trH[p3]*T(6)*k[p2][a]*Hk[p2][p3][b]*Hkk[p1][p1][p2])
-(trH[p2]*T(3)*k[p3][b]*Hk[p3][p3][a]*Hkk[p1][p1][p2])
-(T(6)*Hk[p2][p3][b]*Hk[p3][p3][a]*Hkk[p1][p1][p2])
-(trH[p2]*T(3)*k[p3][a]*Hk[p3][p3][b]*Hkk[p1][p1][p2])
-(T(6)*HH[p2][p3]*k[p1][a]*k[p2][b]*Hkk[p1][p1][p3])
+trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p1][p2]*Hkk[p1][p1][p3]
-((trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p2][p2]*Hkk[p1][p1][p3])/T(2))
+trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p2][p3]*Hkk[p1][p1][p3]
-(T(18)*g[a][b]*HH[p2][p3]*kk[p2][p3]*Hkk[p1][p1][p3])
-(trH[p3]*T(3)*kk[p1][p2]*H[p2][a][b]*Hkk[p1][p1][p3])
+trH[p3]*T(6)*kk[p2][p3]*H[p2][a][b]*Hkk[p1][p1][p3]
-(trH[p2]*T(6)*k[p2][b]*Hk[p3][p2][a]*Hkk[p1][p1][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term016(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(T(9)*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p1][p3])/T(2)
+T(6)*Hk[p2][p2][b]*Hk[p3][p2][a]*Hkk[p1][p1][p3]
-((T(3)*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p1][p3])/T(2))
-(T(6)*HH[p2][p3]*k[p2][a]*k[p3][b]*Hkk[p1][p2][p2])
-(T(6)*g[a][b]*HH[p2][p3]*kk[p2][p3]*Hkk[p1][p2][p2])
-(trH[p2]*trH[p3]*T(6)*g[a][b]*kk[p3][p3]*Hkk[p1][p2][p2])
+T(6)*g[a][b]*HH[p2][p3]*kk[p3][p3]*Hkk[p1][p2][p2]
+trH[p3]*T(6)*kk[p3][p3]*H[p2][a][b]*Hkk[p1][p2][p2]
+trH[p2]*T(6)*kk[p3][p3]*H[p3][a][b]*Hkk[p1][p2][p2]
-(trH[p3]*T(6)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p2])
+trH[p3]*T(3)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p2][p2]
-(T(6)*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p2][p2])
-(T(6)*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p2][p2])
-(trH[p2]*T(6)*k[p3][b]*Hk[p3][p3][a]*Hkk[p1][p2][p2])
+T(6)*Hk[p2][p3][b]*Hk[p3][p3][a]*Hkk[p1][p2][p2]
-(trH[p2]*T(6)*k[p3][a]*Hk[p3][p3][b]*Hkk[p1][p2][p2])
+T(6)*Hk[p2][p3][a]*Hk[p3][p3][b]*Hkk[p1][p2][p2]
-(T(3)*HH[p2][p3]*k[p1][a]*k[p1][b]*Hkk[p1][p2][p3])
-(trH[p2]*trH[p3]*T(6)*k[p1][a]*k[p2][b]*Hkk[p1][p2][p3])
+(T(27)*HH[p2][p3]*k[p1][a]*k[p2][b]*Hkk[p1][p2][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term017(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(3)*HH[p2][p3]*k[p1][b]*k[p3][a]*Hkk[p1][p2][p3]
+trH[p2]*trH[p3]*T(3)*k[p1][a]*k[p3][b]*Hkk[p1][p2][p3]
-((T(15)*HH[p2][p3]*k[p1][a]*k[p3][b]*Hkk[p1][p2][p3])/T(2))
-(trH[p2]*trH[p3]*T(6)*k[p2][a]*k[p3][b]*Hkk[p1][p2][p3])
-(T(30)*HH[p2][p3]*k[p2][a]*k[p3][b]*Hkk[p1][p2][p3])
+trH[p2]*trH[p3]*T(6)*k[p3][a]*k[p3][b]*Hkk[p1][p2][p3]
-(T(6)*HH[p2][p3]*k[p3][a]*k[p3][b]*Hkk[p1][p2][p3])
+trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p1][p1]*Hkk[p1][p2][p3]
+T(9)*g[a][b]*HH[p2][p3]*kk[p1][p1]*Hkk[p1][p2][p3]
+trH[p2]*trH[p3]*T(3)*g[a][b]*kk[p1][p2]*Hkk[p1][p2][p3]
-(T(21)*g[a][b]*HH[p2][p3]*kk[p1][p2]*Hkk[p1][p2][p3])
+trH[p2]*trH[p3]*T(6)*g[a][b]*kk[p1][p3]*Hkk[p1][p2][p3]
+T(30)*g[a][b]*HH[p2][p3]*kk[p2][p3]*Hkk[p1][p2][p3]
-((trH[p3]*T(9)*kk[p1][p2]*H[p2][a][b]*Hkk[p1][p2][p3])/T(2))
-(trH[p3]*T(12)*kk[p1][p3]*H[p2][a][b]*Hkk[p1][p2][p3])
+trH[p3]*T(6)*kk[p2][p3]*H[p2][a][b]*Hkk[p1][p2][p3]
-((trH[p2]*T(3)*kk[p1][p2]*H[p3][a][b]*Hkk[p1][p2][p3])/T(2))
+trH[p2]*T(3)*kk[p2][p3]*H[p3][a][b]*Hkk[p1][p2][p3]
+trH[p2]*T(6)*kk[p3][p3]*H[p3][a][b]*Hkk[p1][p2][p3]
+trH[p3]*T(3)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term018(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p3]*T(9)*k[p3][b]*Hk[p2][p1][a]*Hkk[p1][p2][p3]
-(trH[p3]*T(6)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p2][p3])
+trH[p3]*T(6)*k[p2][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]
+(trH[p3]*T(3)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3])/T(2)
+trH[p3]*T(6)*k[p2][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3]
-(trH[p2]*T(3)*k[p1][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3])
-((T(3)*Hk[p2][p3][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3])/T(2))
+trH[p2]*T(3)*k[p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]
+T(6)*Hk[p2][p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]
-(T(6)*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])
+(trH[p2]*T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p2][p3])/T(2)
+T(3)*Hk[p2][p2][b]*Hk[p3][p2][a]*Hkk[p1][p2][p3]
+T(9)*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p2][p3]
+(trH[p2]*T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3])/T(2)
+T(9)*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]
+trH[p2]*T(3)*k[p1][b]*Hk[p3][p3][a]*Hkk[p1][p2][p3]
-(trH[p2]*T(6)*k[p3][b]*Hk[p3][p3][a]*Hkk[p1][p2][p3])
+T(9)*Hk[p2][p1][b]*Hk[p3][p3][a]*Hkk[p1][p2][p3]
+trH[p2]*T(3)*k[p1][a]*Hk[p3][p3][b]*Hkk[p1][p2][p3]
-(trH[p2]*T(12)*k[p3][a]*Hk[p3][p3][b]*Hkk[p1][p2][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term019(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(12)*Hk[p2][p2][a]*Hk[p3][p3][b]*Hkk[p1][p2][p3])
+trH[p3]*T(3)*kk[p1][p2]*H[p2][a][b]*Hkk[p1][p3][p1]
-(trH[p2]*T(6)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p1])
+T(3)*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p1]
-(trH[p2]*T(6)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p1])
+T(6)*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p1]
-(T(3)*HH[p2][p3]*k[p1][a]*k[p2][b]*Hkk[p1][p3][p2])
-(trH[p2]*trH[p3]*T(6)*k[p2][a]*k[p2][b]*Hkk[p1][p3][p2])
+T(6)*HH[p2][p3]*k[p2][a]*k[p2][b]*Hkk[p1][p3][p2]
+T(15)*HH[p2][p3]*k[p2][b]*k[p3][a]*Hkk[p1][p3][p2]
+T(21)*HH[p2][p3]*k[p2][a]*k[p3][b]*Hkk[p1][p3][p2]
+T(36)*g[a][b]*HH[p2][p3]*kk[p1][p2]*Hkk[p1][p3][p2]
+trH[p2]*trH[p3]*T(6)*g[a][b]*kk[p2][p3]*Hkk[p1][p3][p2]
-(T(36)*g[a][b]*HH[p2][p3]*kk[p2][p3]*Hkk[p1][p3][p2])
+trH[p3]*T(12)*kk[p1][p2]*H[p2][a][b]*Hkk[p1][p3][p2]
-(trH[p3]*T(3)*kk[p2][p3]*H[p2][a][b]*Hkk[p1][p3][p2])
+trH[p2]*T(6)*kk[p1][p2]*H[p3][a][b]*Hkk[p1][p3][p2]
-(trH[p2]*T(18)*kk[p2][p3]*H[p3][a][b]*Hkk[p1][p3][p2])
-(trH[p2]*T(6)*kk[p3][p3]*H[p3][a][b]*Hkk[p1][p3][p2])
-(trH[p3]*T(6)*k[p1][b]*Hk[p2][p2][a]*Hkk[p1][p3][p2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term020(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p3]*T(6)*k[p2][b]*Hk[p2][p2][a]*Hkk[p1][p3][p2]
-(trH[p3]*T(6)*k[p1][a]*Hk[p2][p2][b]*Hkk[p1][p3][p2])
+trH[p3]*T(6)*k[p2][a]*Hk[p2][p2][b]*Hkk[p1][p3][p2]
-((trH[p2]*T(3)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p2])/T(2))
-((T(3)*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p2])/T(2))
-(T(15)*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p3][p2])
-(trH[p2]*T(3)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p2])
-(T(9)*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p3][p2])
+trH[p2]*T(6)*k[p1][b]*Hk[p3][p3][a]*Hkk[p1][p3][p2]
+trH[p2]*T(6)*k[p2][b]*Hk[p3][p3][a]*Hkk[p1][p3][p2]
-(T(12)*Hk[p2][p1][b]*Hk[p3][p3][a]*Hkk[p1][p3][p2])
+T(6)*Hk[p2][p2][b]*Hk[p3][p3][a]*Hkk[p1][p3][p2]
+trH[p2]*T(6)*k[p1][a]*Hk[p3][p3][b]*Hkk[p1][p3][p2]
+trH[p2]*T(6)*k[p2][a]*Hk[p3][p3][b]*Hkk[p1][p3][p2]
+trH[p2]*T(6)*k[p3][a]*Hk[p3][p3][b]*Hkk[p1][p3][p2]
-(T(12)*Hk[p2][p1][a]*Hk[p3][p3][b]*Hkk[p1][p3][p2])
-(T(3)*Hk[p2][p3][a]*Hk[p3][p3][b]*Hkk[p1][p3][p2])
+trH[p2]*trH[p3]*T(6)*k[p2][a]*k[p2][b]*Hkk[p1][p3][p3]
-(T(6)*HH[p2][p3]*k[p2][a]*k[p2][b]*Hkk[p1][p3][p3])
+T(6)*HH[p2][p3]*k[p2][b]*k[p3][a]*Hkk[p1][p3][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term021(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p2]*trH[p3]*T(6)*g[a][b]*kk[p1][p2]*Hkk[p1][p3][p3])
-(T(6)*g[a][b]*HH[p2][p3]*kk[p1][p2]*Hkk[p1][p3][p3])
+T(6)*g[a][b]*HH[p2][p3]*kk[p2][p3]*Hkk[p1][p3][p3]
+(trH[p3]*T(15)*kk[p1][p2]*H[p2][a][b]*Hkk[p1][p3][p3])/T(2)
+(trH[p2]*T(3)*kk[p1][p2]*H[p3][a][b]*Hkk[p1][p3][p3])/T(2)
-((T(15)*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3])/T(2))
+T(6)*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3]
-(trH[p2]*T(3)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3])
-(trH[p2]*T(6)*k[p2][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3])
+T(6)*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]
+trH[p1]*trH[p3]*T(9)*g[a][b]*kk[p2][p3]*Hkk[p2][p1][p1]
-(T(21)*g[a][b]*HH[p1][p3]*kk[p2][p3]*Hkk[p2][p1][p1])
-((trH[p3]*T(3)*kk[p2][p3]*H[p1][a][b]*Hkk[p2][p1][p1])/T(2))
-((trH[p1]*T(9)*kk[p2][p3]*H[p3][a][b]*Hkk[p2][p1][p1])/T(2))
-((T(3)*Hk[p1][p3][b]*Hk[p3][p2][a]*Hkk[p2][p1][p1])/T(2))
+trH[p3]*T(6)*g[a][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p1]
-(T(6)*H[p3][a][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p1])
-(trH[p1]*trH[p3]*T(3)*g[a][b]*kk[p2][p3]*Hkk[p2][p1][p2])
+T(21)*g[a][b]*HH[p1][p3]*kk[p2][p3]*Hkk[p2][p1][p2]
-((trH[p1]*trH[p3]*T(3)*g[a][b]*kk[p3][p3]*Hkk[p2][p1][p2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term022(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(6)*g[a][b]*HH[p1][p3]*kk[p3][p3]*Hkk[p2][p1][p2]
+trH[p3]*T(6)*kk[p2][p3]*H[p1][a][b]*Hkk[p2][p1][p2]
+trH[p3]*T(3)*kk[p3][p3]*H[p1][a][b]*Hkk[p2][p1][p2]
+trH[p1]*T(3)*kk[p2][p3]*H[p3][a][b]*Hkk[p2][p1][p2]
+trH[p1]*T(3)*kk[p3][p3]*H[p3][a][b]*Hkk[p2][p1][p2]
+(T(3)*Hk[p1][p3][b]*Hk[p3][p2][a]*Hkk[p2][p1][p2])/T(2)
-((T(9)*Hk[p1][p3][a]*Hk[p3][p2][b]*Hkk[p2][p1][p2])/T(2))
-(trH[p1]*T(3)*k[p3][b]*Hk[p3][p3][a]*Hkk[p2][p1][p2])
+T(12)*Hk[p1][p3][b]*Hk[p3][p3][a]*Hkk[p2][p1][p2]
-(trH[p1]*T(3)*k[p3][a]*Hk[p3][p3][b]*Hkk[p2][p1][p2])
+T(6)*Hk[p1][p3][a]*Hk[p3][p3][b]*Hkk[p2][p1][p2]
+trH[p3]*T(15)*g[a][b]*Hkk[p1][p3][p2]*Hkk[p2][p1][p2]
-(trH[p1]*trH[p3]*T(6)*k[p1][a]*k[p2][b]*Hkk[p2][p1][p3])
+(T(27)*HH[p1][p3]*k[p1][a]*k[p2][b]*Hkk[p2][p1][p3])/T(2)
+trH[p1]*trH[p3]*T(3)*g[a][b]*kk[p1][p2]*Hkk[p2][p1][p3]
-(T(21)*g[a][b]*HH[p1][p3]*kk[p1][p2]*Hkk[p2][p1][p3])
-(T(15)*g[a][b]*HH[p1][p3]*kk[p2][p2]*Hkk[p2][p1][p3])
-(T(36)*g[a][b]*HH[p1][p3]*kk[p2][p3]*Hkk[p2][p1][p3])
+(trH[p3]*T(3)*kk[p1][p2]*H[p1][a][b]*Hkk[p2][p1][p3])/T(2)
-((trH[p1]*T(3)*kk[p1][p2]*H[p3][a][b]*Hkk[p2][p1][p3])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term023(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p1]*T(3)*kk[p2][p2]*H[p3][a][b]*Hkk[p2][p1][p3])
-(trH[p1]*T(9)*kk[p2][p3]*H[p3][a][b]*Hkk[p2][p1][p3])
-((trH[p3]*T(3)*k[p1][b]*Hk[p1][p2][a]*Hkk[p2][p1][p3])/T(2))
+trH[p3]*T(3)*k[p2][b]*Hk[p1][p2][a]*Hkk[p2][p1][p3]
-(trH[p3]*T(3)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p1][p3])
+(trH[p3]*T(9)*k[p1][a]*Hk[p1][p2][b]*Hkk[p2][p1][p3])/T(2)
+trH[p3]*T(3)*k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p1][p3]
+T(3)*Hk[p1][p2][b]*Hk[p3][p1][a]*Hkk[p2][p1][p3]
-(T(3)*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p1][p3])
+trH[p1]*T(12)*k[p1][b]*Hk[p3][p2][a]*Hkk[p2][p1][p3]
+trH[p1]*T(12)*k[p1][a]*Hk[p3][p2][b]*Hkk[p2][p1][p3]
-(T(3)*Hk[p1][p2][b]*Hk[p3][p3][a]*Hkk[p2][p1][p3])
+T(3)*H[p3][a][b]*Hkk[p1][p2][p1]*Hkk[p2][p1][p3]
-(trH[p3]*T(6)*g[a][b]*Hkk[p1][p2][p2]*Hkk[p2][p1][p3])
-(trH[p3]*T(6)*g[a][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p3])
+(T(3)*H[p3][a][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p3])/T(2)
-(trH[p3]*T(6)*g[a][b]*Hkk[p1][p3][p2]*Hkk[p2][p1][p3])
+trH[p3]*T(6)*k[p1][b]*Hk[p1][p3][a]*Hkk[p2][p2][p1]
+trH[p3]*T(6)*k[p1][a]*Hk[p1][p3][b]*Hkk[p2][p2][p1]
-((trH[p1]*trH[p3]*T(3)*g[a][b]*kk[p3][p3]*Hkk[p2][p2][p2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term024(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p3]*T(3)*kk[p3][p3]*H[p1][a][b]*Hkk[p2][p2][p2])
-((trH[p1]*T(3)*kk[p2][p3]*H[p3][a][b]*Hkk[p2][p2][p2])/T(2))
+trH[p1]*trH[p3]*T(3)*k[p1][a]*k[p1][b]*Hkk[p2][p2][p3]
-(T(6)*HH[p1][p3]*k[p1][a]*k[p1][b]*Hkk[p2][p2][p3])
+T(6)*HH[p1][p3]*k[p1][b]*k[p2][a]*Hkk[p2][p2][p3]
-(T(3)*HH[p1][p3]*k[p1][a]*k[p2][b]*Hkk[p2][p2][p3])
-(trH[p1]*trH[p3]*T(3)*k[p2][a]*k[p2][b]*Hkk[p2][p2][p3])
+T(6)*HH[p1][p3]*k[p2][a]*k[p2][b]*Hkk[p2][p2][p3]
-(T(12)*HH[p1][p3]*k[p1][b]*k[p3][a]*Hkk[p2][p2][p3])
-(T(12)*HH[p1][p3]*k[p1][a]*k[p3][b]*Hkk[p2][p2][p3])
+trH[p1]*trH[p3]*T(9)*k[p2][a]*k[p3][b]*Hkk[p2][p2][p3]
-(T(18)*HH[p1][p3]*k[p2][a]*k[p3][b]*Hkk[p2][p2][p3])
+trH[p1]*trH[p3]*T(6)*k[p3][a]*k[p3][b]*Hkk[p2][p2][p3]
-(T(12)*HH[p1][p3]*k[p3][a]*k[p3][b]*Hkk[p2][p2][p3])
+T(6)*g[a][b]*HH[p1][p3]*kk[p1][p1]*Hkk[p2][p2][p3]
-(T(9)*g[a][b]*HH[p1][p3]*kk[p1][p2]*Hkk[p2][p2][p3])
-(trH[p1]*trH[p3]*T(3)*g[a][b]*kk[p2][p2]*Hkk[p2][p2][p3])
+T(6)*g[a][b]*HH[p1][p3]*kk[p2][p2]*Hkk[p2][p2][p3]
-(T(12)*g[a][b]*HH[p1][p3]*kk[p2][p3]*Hkk[p2][p2][p3])
+trH[p1]*trH[p3]*T(3)*g[a][b]*kk[p3][p3]*Hkk[p2][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term025(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(6)*g[a][b]*HH[p1][p3]*kk[p3][p3]*Hkk[p2][p2][p3])
-(trH[p3]*T(6)*kk[p2][p2]*H[p1][a][b]*Hkk[p2][p2][p3])
+trH[p3]*T(6)*kk[p3][p3]*H[p1][a][b]*Hkk[p2][p2][p3]
-(trH[p1]*T(3)*kk[p1][p2]*H[p3][a][b]*Hkk[p2][p2][p3])
-(trH[p1]*T(6)*kk[p1][p3]*H[p3][a][b]*Hkk[p2][p2][p3])
+(trH[p1]*T(9)*kk[p2][p2]*H[p3][a][b]*Hkk[p2][p2][p3])/T(2)
-(trH[p1]*T(3)*kk[p2][p3]*H[p3][a][b]*Hkk[p2][p2][p3])
+trH[p3]*T(3)*k[p1][b]*Hk[p1][p2][a]*Hkk[p2][p2][p3]
+trH[p3]*T(3)*k[p1][a]*Hk[p1][p2][b]*Hkk[p2][p2][p3]
+trH[p3]*T(3)*k[p1][b]*Hk[p1][p3][a]*Hkk[p2][p2][p3]
+trH[p3]*T(3)*k[p1][a]*Hk[p1][p3][b]*Hkk[p2][p2][p3]
+trH[p1]*T(12)*k[p1][b]*Hk[p3][p1][a]*Hkk[p2][p2][p3]
+trH[p1]*T(3)*k[p2][b]*Hk[p3][p1][a]*Hkk[p2][p2][p3]
+trH[p1]*T(6)*k[p3][b]*Hk[p3][p1][a]*Hkk[p2][p2][p3]
+trH[p1]*T(6)*k[p1][a]*Hk[p3][p1][b]*Hkk[p2][p2][p3]
+trH[p1]*T(6)*k[p3][a]*Hk[p3][p1][b]*Hkk[p2][p2][p3]
+trH[p1]*T(6)*k[p2][b]*Hk[p3][p2][a]*Hkk[p2][p2][p3]
+(trH[p1]*T(3)*k[p3][b]*Hk[p3][p2][a]*Hkk[p2][p2][p3])/T(2)
+(trH[p1]*T(3)*k[p3][a]*Hk[p3][p2][b]*Hkk[p2][p2][p3])/T(2)
+trH[p1]*T(3)*k[p2][b]*Hk[p3][p3][a]*Hkk[p2][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term026(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p1]*T(6)*k[p3][a]*Hk[p3][p3][b]*Hkk[p2][p2][p3])
-(trH[p3]*T(6)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p2][p2][p3])
-(trH[p3]*T(6)*g[a][b]*Hkk[p1][p1][p3]*Hkk[p2][p2][p3])
+trH[p3]*T(6)*g[a][b]*Hkk[p1][p2][p2]*Hkk[p2][p2][p3]
-(T(6)*H[p3][a][b]*Hkk[p1][p2][p2]*Hkk[p2][p2][p3])
+trH[p3]*T(18)*g[a][b]*Hkk[p1][p2][p3]*Hkk[p2][p2][p3]
-(T(9)*H[p3][a][b]*Hkk[p1][p2][p3]*Hkk[p2][p2][p3])
+T(12)*H[p3][a][b]*Hkk[p1][p3][p2]*Hkk[p2][p2][p3]
+trH[p3]*T(12)*g[a][b]*Hkk[p1][p3][p3]*Hkk[p2][p2][p3]
-(T(12)*H[p3][a][b]*Hkk[p1][p3][p3]*Hkk[p2][p2][p3])
+trH[p1]*trH[p3]*T(3)*k[p1][a]*k[p2][b]*Hkk[p2][p3][p1]
-((T(27)*HH[p1][p3]*k[p1][a]*k[p2][b]*Hkk[p2][p3][p1])/T(2))
-(trH[p3]*T(6)*k[p1][b]*Hk[p1][p2][a]*Hkk[p2][p3][p1])
-(trH[p3]*T(12)*k[p1][a]*Hk[p1][p2][b]*Hkk[p2][p3][p1])
-(trH[p1]*T(18)*k[p1][b]*Hk[p3][p2][a]*Hkk[p2][p3][p1])
+T(6)*Hk[p1][p1][b]*Hk[p3][p2][a]*Hkk[p2][p3][p1]
-(trH[p1]*T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p2][p3][p1])
-(trH[p1]*trH[p3]*T(3)*k[p2][a]*k[p2][b]*Hkk[p2][p3][p2])
+T(6)*HH[p1][p3]*k[p2][a]*k[p2][b]*Hkk[p2][p3][p2]
+trH[p1]*trH[p3]*T(3)*g[a][b]*kk[p2][p2]*Hkk[p2][p3][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term027(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(6)*g[a][b]*HH[p1][p3]*kk[p2][p2]*Hkk[p2][p3][p2])
+trH[p3]*T(6)*kk[p2][p2]*H[p1][a][b]*Hkk[p2][p3][p2]
-((trH[p1]*T(3)*kk[p2][p2]*H[p3][a][b]*Hkk[p2][p3][p2])/T(2))
-(trH[p3]*T(3)*k[p1][b]*Hk[p1][p2][a]*Hkk[p2][p3][p2])
-(trH[p3]*T(3)*k[p1][a]*Hk[p1][p2][b]*Hkk[p2][p3][p2])
+T(6)*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p2]
-(trH[p1]*T(3)*k[p2][b]*Hk[p3][p2][a]*Hkk[p2][p3][p2])
-((trH[p1]*T(3)*k[p2][a]*Hk[p3][p2][b]*Hkk[p2][p3][p2])/T(2))
-(trH[p1]*T(6)*k[p2][b]*Hk[p3][p3][a]*Hkk[p2][p3][p2])
-(trH[p1]*T(6)*k[p2][a]*Hk[p3][p3][b]*Hkk[p2][p3][p2])
-(trH[p3]*T(6)*g[a][b]*Hkk[p1][p2][p2]*Hkk[p2][p3][p2])
+T(3)*H[p3][a][b]*Hkk[p1][p2][p2]*Hkk[p2][p3][p2]
+trH[p1]*trH[p3]*T(3)*k[p2][a]*k[p2][b]*Hkk[p2][p3][p3]
-(T(6)*HH[p1][p3]*k[p2][a]*k[p2][b]*Hkk[p2][p3][p3])
-(trH[p1]*trH[p3]*T(3)*k[p2][b]*k[p3][a]*Hkk[p2][p3][p3])
+T(6)*HH[p1][p3]*k[p2][b]*k[p3][a]*Hkk[p2][p3][p3]
+T(12)*g[a][b]*HH[p1][p3]*kk[p1][p2]*Hkk[p2][p3][p3]
-(trH[p1]*trH[p3]*T(6)*g[a][b]*kk[p2][p2]*Hkk[p2][p3][p3])
+T(12)*g[a][b]*HH[p1][p3]*kk[p2][p2]*Hkk[p2][p3][p3]
+trH[p1]*trH[p3]*T(3)*g[a][b]*kk[p2][p3]*Hkk[p2][p3][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term028(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(6)*g[a][b]*HH[p1][p3]*kk[p2][p3]*Hkk[p2][p3][p3])
-(trH[p3]*T(12)*kk[p2][p2]*H[p1][a][b]*Hkk[p2][p3][p3])
+trH[p3]*T(6)*kk[p2][p3]*H[p1][a][b]*Hkk[p2][p3][p3]
-(trH[p1]*T(3)*kk[p1][p2]*H[p3][a][b]*Hkk[p2][p3][p3])
+(trH[p1]*T(3)*kk[p2][p2]*H[p3][a][b]*Hkk[p2][p3][p3])/T(2)
-(trH[p1]*T(6)*kk[p2][p3]*H[p3][a][b]*Hkk[p2][p3][p3])
-((trH[p3]*T(15)*k[p1][b]*Hk[p1][p2][a]*Hkk[p2][p3][p3])/T(2))
-((trH[p3]*T(15)*k[p1][a]*Hk[p1][p2][b]*Hkk[p2][p3][p3])/T(2))
+(T(15)*Hk[p1][p2][b]*Hk[p3][p1][a]*Hkk[p2][p3][p3])/T(2)
+(T(3)*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p3])/T(2)
-(trH[p1]*T(6)*k[p2][b]*Hk[p3][p2][a]*Hkk[p2][p3][p3])
-((trH[p1]*T(9)*k[p2][a]*Hk[p3][p2][b]*Hkk[p2][p3][p3])/T(2))
+trH[p1]*T(9)*k[p3][a]*Hk[p3][p2][b]*Hkk[p2][p3][p3]
+trH[p3]*T(12)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p2][p3][p3]
+T(3)*H[p3][a][b]*Hkk[p1][p1][p2]*Hkk[p2][p3][p3]
+trH[p3]*T(6)*g[a][b]*Hkk[p1][p2][p2]*Hkk[p2][p3][p3]
-(T(6)*H[p3][a][b]*Hkk[p1][p2][p2]*Hkk[p2][p3][p3])
+T(3)*H[p3][a][b]*Hkk[p1][p3][p2]*Hkk[p2][p3][p3]
-(trH[p1]*trH[p2]*T(6)*g[a][b]*kk[p2][p3]*Hkk[p3][p1][p1])
+T(18)*g[a][b]*HH[p1][p2]*kk[p2][p3]*Hkk[p3][p1][p1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term029(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p2]*T(6)*kk[p2][p3]*H[p1][a][b]*Hkk[p3][p1][p1]
-(trH[p2]*T(12)*g[a][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p1])
+T(9)*H[p2][a][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p1]
+T(3)*H[p1][a][b]*Hkk[p2][p2][p3]*Hkk[p3][p1][p1]
-(trH[p1]*trH[p2]*T(6)*g[a][b]*kk[p1][p3]*Hkk[p3][p1][p2])
+T(36)*g[a][b]*HH[p1][p2]*kk[p2][p3]*Hkk[p3][p1][p2]
-(trH[p1]*trH[p2]*T(3)*g[a][b]*kk[p3][p3]*Hkk[p3][p1][p2])
+T(9)*g[a][b]*HH[p1][p2]*kk[p3][p3]*Hkk[p3][p1][p2]
-(trH[p2]*T(6)*kk[p1][p3]*H[p1][a][b]*Hkk[p3][p1][p2])
-(trH[p2]*T(3)*kk[p2][p3]*H[p1][a][b]*Hkk[p3][p1][p2])
+trH[p2]*T(3)*kk[p3][p3]*H[p1][a][b]*Hkk[p3][p1][p2]
+trH[p1]*T(6)*kk[p1][p3]*H[p2][a][b]*Hkk[p3][p1][p2]
+trH[p2]*T(6)*k[p1][b]*Hk[p1][p3][a]*Hkk[p3][p1][p2]
-(trH[p2]*T(3)*k[p3][b]*Hk[p1][p3][a]*Hkk[p3][p1][p2])
+trH[p2]*T(6)*k[p1][a]*Hk[p1][p3][b]*Hkk[p3][p1][p2]
+T(3)*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p1][p2]
-((trH[p1]*T(3)*k[p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p2])/T(2))
-((T(9)*Hk[p1][p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p2])/T(2))
-(T(6)*Hk[p1][p3][b]*Hk[p2][p3][a]*Hkk[p3][p1][p2])
-((trH[p1]*T(3)*k[p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term030(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((T(3)*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p2])/T(2))
-(trH[p2]*T(9)*g[a][b]*Hkk[p1][p1][p3]*Hkk[p3][p1][p2])
-(T(6)*H[p2][a][b]*Hkk[p1][p1][p3]*Hkk[p3][p1][p2])
-(T(3)*H[p2][a][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p2])
+trH[p2]*T(12)*g[a][b]*Hkk[p1][p3][p1]*Hkk[p3][p1][p2]
-(T(9)*H[p2][a][b]*Hkk[p1][p3][p1]*Hkk[p3][p1][p2])
+trH[p2]*T(27)*g[a][b]*Hkk[p1][p3][p2]*Hkk[p3][p1][p2]
+(T(15)*H[p2][a][b]*Hkk[p1][p3][p2]*Hkk[p3][p1][p2])/T(2)
+trH[p2]*T(12)*g[a][b]*Hkk[p1][p3][p3]*Hkk[p3][p1][p2]
-(T(3)*H[p2][a][b]*Hkk[p1][p3][p3]*Hkk[p3][p1][p2])
-(trH[p1]*T(6)*g[a][b]*Hkk[p2][p1][p3]*Hkk[p3][p1][p2])
+T(3)*H[p1][a][b]*Hkk[p2][p2][p3]*Hkk[p3][p1][p2]
-(trH[p1]*T(6)*g[a][b]*Hkk[p2][p3][p1]*Hkk[p3][p1][p2])
+T(12)*H[p1][a][b]*Hkk[p2][p3][p1]*Hkk[p3][p1][p2]
+trH[p1]*trH[p2]*T(6)*g[a][b]*kk[p2][p3]*Hkk[p3][p1][p3]
-(T(9)*g[a][b]*HH[p1][p2]*kk[p2][p3]*Hkk[p3][p1][p3])
-(trH[p2]*T(9)*kk[p2][p3]*H[p1][a][b]*Hkk[p3][p1][p3])
+trH[p2]*T(6)*k[p3][b]*Hk[p1][p2][a]*Hkk[p3][p1][p3]
+trH[p2]*T(6)*k[p3][a]*Hk[p1][p2][b]*Hkk[p3][p1][p3]
-(T(3)*Hk[p1][p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term031(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(3)*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p3])
-(trH[p2]*T(9)*g[a][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3])
+T(6)*H[p2][a][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3]
-(trH[p2]*T(6)*g[a][b]*Hkk[p1][p3][p2]*Hkk[p3][p1][p3])
+trH[p2]*T(6)*k[p1][b]*Hk[p1][p3][a]*Hkk[p3][p2][p1]
+trH[p2]*T(6)*k[p1][a]*Hk[p1][p3][b]*Hkk[p3][p2][p1]
+T(6)*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p2][p1]
+trH[p1]*T(6)*k[p1][b]*Hk[p2][p3][a]*Hkk[p3][p2][p1]
+trH[p1]*T(3)*k[p1][a]*Hk[p2][p3][b]*Hkk[p3][p2][p1]
-(trH[p2]*T(12)*g[a][b]*Hkk[p1][p1][p3]*Hkk[p3][p2][p1])
+T(6)*H[p2][a][b]*Hkk[p1][p1][p3]*Hkk[p3][p2][p1]
-(trH[p1]*T(9)*g[a][b]*Hkk[p2][p1][p3]*Hkk[p3][p2][p1])
-(T(12)*H[p1][a][b]*Hkk[p2][p1][p3]*Hkk[p3][p2][p1])
+trH[p1]*trH[p2]*T(3)*k[p2][a]*k[p3][b]*Hkk[p3][p2][p2]
-(T(6)*HH[p1][p2]*k[p2][a]*k[p3][b]*Hkk[p3][p2][p2])
-(trH[p1]*trH[p2]*T(3)*g[a][b]*kk[p2][p3]*Hkk[p3][p2][p2])
+T(6)*g[a][b]*HH[p1][p2]*kk[p2][p3]*Hkk[p3][p2][p2]
+trH[p1]*trH[p2]*T(3)*g[a][b]*kk[p3][p3]*Hkk[p3][p2][p2]
-(T(6)*g[a][b]*HH[p1][p2]*kk[p3][p3]*Hkk[p3][p2][p2])
-(trH[p2]*T(6)*kk[p2][p3]*H[p1][a][b]*Hkk[p3][p2][p2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term032(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p2]*T(6)*kk[p3][p3]*H[p1][a][b]*Hkk[p3][p2][p2]
+trH[p1]*T(6)*kk[p2][p3]*H[p2][a][b]*Hkk[p3][p2][p2]
+(trH[p1]*T(3)*kk[p3][p3]*H[p2][a][b]*Hkk[p3][p2][p2])/T(2)
+(trH[p2]*T(9)*k[p1][b]*Hk[p1][p3][a]*Hkk[p3][p2][p2])/T(2)
+(trH[p2]*T(9)*k[p1][a]*Hk[p1][p3][b]*Hkk[p3][p2][p2])/T(2)
+T(3)*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p2][p2]
+T(3)*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p2][p2]
+trH[p1]*T(3)*k[p3][b]*Hk[p2][p3][a]*Hkk[p3][p2][p2]
-(trH[p1]*T(9)*k[p2][a]*Hk[p2][p3][b]*Hkk[p3][p2][p2])
+(trH[p1]*T(3)*k[p3][a]*Hk[p2][p3][b]*Hkk[p3][p2][p2])/T(2)
-(trH[p2]*T(18)*g[a][b]*Hkk[p1][p1][p3]*Hkk[p3][p2][p2])
-(T(6)*H[p2][a][b]*Hkk[p1][p1][p3]*Hkk[p3][p2][p2])
-(T(3)*H[p2][a][b]*Hkk[p1][p2][p3]*Hkk[p3][p2][p2])
-(trH[p1]*T(12)*g[a][b]*Hkk[p2][p2][p3]*Hkk[p3][p2][p2])
-(T(24)*H[p1][a][b]*Hkk[p2][p2][p3]*Hkk[p3][p2][p2])
+trH[p1]*T(6)*g[a][b]*Hkk[p2][p3][p2]*Hkk[p3][p2][p2]
+T(12)*H[p1][a][b]*Hkk[p2][p3][p2]*Hkk[p3][p2][p2]
+(trH[p1]*T(3)*g[a][b]*Hkk[p2][p3][p3]*Hkk[p3][p2][p2])/T(2)
+T(3)*H[p1][a][b]*Hkk[p2][p3][p3]*Hkk[p3][p2][p2]
+trH[p1]*trH[p2]*T(3)*k[p1][a]*k[p1][b]*Hkk[p3][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term033(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(3)*HH[p1][p2]*k[p1][b]*k[p3][a]*Hkk[p3][p2][p3]
-(trH[p1]*trH[p2]*T(6)*k[p2][b]*k[p3][a]*Hkk[p3][p2][p3])
+T(12)*HH[p1][p2]*k[p2][b]*k[p3][a]*Hkk[p3][p2][p3]
+T(6)*HH[p1][p2]*k[p1][a]*k[p3][b]*Hkk[p3][p2][p3]
-(trH[p1]*trH[p2]*T(3)*k[p2][a]*k[p3][b]*Hkk[p3][p2][p3])
+T(6)*HH[p1][p2]*k[p2][a]*k[p3][b]*Hkk[p3][p2][p3]
+trH[p1]*trH[p2]*T(6)*k[p3][a]*k[p3][b]*Hkk[p3][p2][p3]
-(T(12)*HH[p1][p2]*k[p3][a]*k[p3][b]*Hkk[p3][p2][p3])
-((trH[p1]*trH[p2]*T(3)*g[a][b]*kk[p1][p1]*Hkk[p3][p2][p3])/T(2))
-(T(6)*g[a][b]*HH[p1][p2]*kk[p1][p1]*Hkk[p3][p2][p3])
-(T(9)*g[a][b]*HH[p1][p2]*kk[p1][p3]*Hkk[p3][p2][p3])
+trH[p1]*trH[p2]*T(6)*g[a][b]*kk[p2][p3]*Hkk[p3][p2][p3]
+T(6)*g[a][b]*HH[p1][p2]*kk[p2][p3]*Hkk[p3][p2][p3]
-(trH[p2]*T(3)*kk[p1][p3]*H[p1][a][b]*Hkk[p3][p2][p3])
+trH[p2]*T(12)*kk[p2][p3]*H[p1][a][b]*Hkk[p3][p2][p3]
+trH[p1]*T(3)*kk[p1][p1]*H[p2][a][b]*Hkk[p3][p2][p3]
-(trH[p1]*T(9)*kk[p2][p3]*H[p2][a][b]*Hkk[p3][p2][p3])
-((trH[p1]*T(3)*kk[p3][p3]*H[p2][a][b]*Hkk[p3][p2][p3])/T(2))
-(trH[p1]*T(12)*k[p1][b]*Hk[p2][p1][a]*Hkk[p3][p2][p3])
-(T(6)*Hk[p1][p1][b]*Hk[p2][p1][a]*Hkk[p3][p2][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term034(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(3)*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p2][p3])
-(trH[p1]*T(12)*k[p1][a]*Hk[p2][p1][b]*Hkk[p3][p2][p3])
+trH[p1]*T(3)*k[p3][a]*Hk[p2][p1][b]*Hkk[p3][p2][p3]
-(T(12)*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p2][p3])
+trH[p1]*T(6)*k[p3][b]*Hk[p2][p2][a]*Hkk[p3][p2][p3]
+trH[p1]*T(9)*k[p3][a]*Hk[p2][p2][b]*Hkk[p3][p2][p3]
-((trH[p1]*T(9)*k[p2][b]*Hk[p2][p3][a]*Hkk[p3][p2][p3])/T(2))
-(trH[p1]*T(3)*k[p3][b]*Hk[p2][p3][a]*Hkk[p3][p2][p3])
+(trH[p1]*T(15)*k[p2][a]*Hk[p2][p3][b]*Hkk[p3][p2][p3])/T(2)
+(trH[p1]*T(3)*k[p3][a]*Hk[p2][p3][b]*Hkk[p3][p2][p3])/T(2)
-(trH[p2]*T(6)*g[a][b]*Hkk[p1][p1][p3]*Hkk[p3][p2][p3])
+T(6)*H[p2][a][b]*Hkk[p1][p1][p3]*Hkk[p3][p2][p3]
+T(21)*H[p2][a][b]*Hkk[p1][p2][p3]*Hkk[p3][p2][p3]
-(trH[p2]*T(12)*g[a][b]*Hkk[p1][p3][p2]*Hkk[p3][p2][p3])
-(T(3)*H[p2][a][b]*Hkk[p1][p3][p2]*Hkk[p3][p2][p3])
+T(3)*H[p2][a][b]*Hkk[p1][p3][p3]*Hkk[p3][p2][p3]
+trH[p1]*T(12)*g[a][b]*Hkk[p2][p1][p1]*Hkk[p3][p2][p3]
-(T(6)*H[p1][a][b]*Hkk[p2][p1][p1]*Hkk[p3][p2][p3])
+trH[p1]*T(6)*g[a][b]*Hkk[p2][p2][p3]*Hkk[p3][p2][p3]
+T(12)*H[p1][a][b]*Hkk[p2][p2][p3]*Hkk[p3][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term035(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p1]*T(3)*g[a][b]*Hkk[p2][p3][p1]*Hkk[p3][p2][p3]
+T(3)*H[p1][a][b]*Hkk[p2][p3][p1]*Hkk[p3][p2][p3]
-(trH[p1]*T(3)*g[a][b]*Hkk[p2][p3][p2]*Hkk[p3][p2][p3])
-(T(6)*H[p1][a][b]*Hkk[p2][p3][p2]*Hkk[p3][p2][p3])
+trH[p1]*T(6)*g[a][b]*Hkk[p2][p3][p3]*Hkk[p3][p2][p3]
+T(12)*H[p1][a][b]*Hkk[p2][p3][p3]*Hkk[p3][p2][p3]
+T(12)*HH[p1][p2]*k[p1][a]*k[p2][b]*Hkk[p3][p3][p1]
-(trH[p1]*trH[p2]*T(3)*g[a][b]*kk[p1][p2]*Hkk[p3][p3][p1])
+T(6)*g[a][b]*HH[p1][p2]*kk[p1][p2]*Hkk[p3][p3][p1]
+trH[p1]*T(3)*kk[p1][p2]*H[p2][a][b]*Hkk[p3][p3][p1]
-(trH[p2]*T(6)*k[p1][b]*Hk[p1][p2][a]*Hkk[p3][p3][p1])
-(trH[p2]*T(6)*k[p1][a]*Hk[p1][p2][b]*Hkk[p3][p3][p1])
-(T(6)*Hk[p1][p2][a]*Hk[p2][p1][b]*Hkk[p3][p3][p1])
+trH[p2]*T(12)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p3][p3][p1]
-(T(6)*H[p2][a][b]*Hkk[p1][p1][p2]*Hkk[p3][p3][p1])
+T(12)*HH[p1][p2]*k[p1][a]*k[p2][b]*Hkk[p3][p3][p2]
-(trH[p1]*trH[p2]*T(6)*k[p2][a]*k[p2][b]*Hkk[p3][p3][p2])
+T(12)*HH[p1][p2]*k[p2][a]*k[p2][b]*Hkk[p3][p3][p2]
-(trH[p1]*trH[p2]*T(6)*k[p2][b]*k[p3][a]*Hkk[p3][p3][p2])
+T(12)*HH[p1][p2]*k[p2][b]*k[p3][a]*Hkk[p3][p3][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term036(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p1]*trH[p2]*T(3)*g[a][b]*kk[p2][p2]*Hkk[p3][p3][p2])
+T(6)*g[a][b]*HH[p1][p2]*kk[p2][p2]*Hkk[p3][p3][p2]
-(T(6)*g[a][b]*HH[p1][p2]*kk[p2][p3]*Hkk[p3][p3][p2])
-(trH[p2]*T(6)*kk[p2][p2]*H[p1][a][b]*Hkk[p3][p3][p2])
+trH[p1]*T(6)*kk[p2][p3]*H[p2][a][b]*Hkk[p3][p3][p2]
-((trH[p1]*T(3)*kk[p3][p3]*H[p2][a][b]*Hkk[p3][p3][p2])/T(2))
-(trH[p2]*T(3)*k[p1][b]*Hk[p1][p2][a]*Hkk[p3][p3][p2])
-(trH[p2]*T(3)*k[p1][a]*Hk[p1][p2][b]*Hkk[p3][p3][p2])
+T(6)*Hk[p1][p2][b]*Hk[p2][p1][a]*Hkk[p3][p3][p2]
+trH[p1]*T(6)*k[p2][a]*Hk[p2][p2][b]*Hkk[p3][p3][p2]
+trH[p1]*T(6)*k[p2][b]*Hk[p2][p3][a]*Hkk[p3][p3][p2]
-(trH[p1]*T(6)*k[p2][a]*Hk[p2][p3][b]*Hkk[p3][p3][p2])
+trH[p2]*T(6)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p3][p3][p2]
-(trH[p2]*T(12)*g[a][b]*Hkk[p1][p2][p2]*Hkk[p3][p3][p2])
+T(12)*H[p2][a][b]*Hkk[p1][p2][p2]*Hkk[p3][p3][p2]
-(T(15)*H[p2][a][b]*Hkk[p1][p2][p3]*Hkk[p3][p3][p2])
-(trH[p2]*T(18)*g[a][b]*Hkk[p1][p3][p2]*Hkk[p3][p3][p2])
+T(6)*H[p2][a][b]*Hkk[p1][p3][p2]*Hkk[p3][p3][p2]
+trH[p1]*T(6)*g[a][b]*Hkk[p2][p2][p2]*Hkk[p3][p3][p2]
+T(12)*H[p1][a][b]*Hkk[p2][p2][p2]*Hkk[p3][p3][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term037(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p1]*T(6)*g[a][b]*Hkk[p2][p2][p3]*Hkk[p3][p3][p2])
-(T(12)*H[p1][a][b]*Hkk[p2][p2][p3]*Hkk[p3][p3][p2])
-(T(3)*HH[p1][p2]*k[p1][a]*k[p2][b]*Hkk[p3][p3][p3])
+trH[p1]*trH[p2]*T(3)*k[p2][a]*k[p2][b]*Hkk[p3][p3][p3]
-(T(6)*HH[p1][p2]*k[p2][a]*k[p2][b]*Hkk[p3][p3][p3])
-((trH[p1]*trH[p2]*T(3)*g[a][b]*kk[p1][p2]*Hkk[p3][p3][p3])/T(2))
+(T(9)*g[a][b]*HH[p1][p2]*kk[p1][p2]*Hkk[p3][p3][p3])/T(2)
-((trH[p1]*trH[p2]*T(3)*g[a][b]*kk[p2][p2]*Hkk[p3][p3][p3])/T(2))
+T(6)*g[a][b]*HH[p1][p2]*kk[p2][p2]*Hkk[p3][p3][p3]
-(trH[p2]*T(3)*kk[p2][p2]*H[p1][a][b]*Hkk[p3][p3][p3])
+trH[p1]*T(3)*kk[p1][p2]*H[p2][a][b]*Hkk[p3][p3][p3]
+trH[p1]*T(3)*kk[p2][p2]*H[p2][a][b]*Hkk[p3][p3][p3]
+(trH[p1]*T(3)*kk[p2][p3]*H[p2][a][b]*Hkk[p3][p3][p3])/T(2)
-(trH[p2]*T(3)*k[p1][b]*Hk[p1][p2][a]*Hkk[p3][p3][p3])
-(trH[p2]*T(3)*k[p1][a]*Hk[p1][p2][b]*Hkk[p3][p3][p3])
+T(6)*Hk[p1][p2][b]*Hk[p2][p1][a]*Hkk[p3][p3][p3]
-(trH[p1]*T(3)*k[p2][b]*Hk[p2][p2][a]*Hkk[p3][p3][p3])
-(trH[p1]*T(3)*k[p2][a]*Hk[p2][p2][b]*Hkk[p3][p3][p3])
+trH[p2]*T(3)*g[a][b]*Hkk[p1][p1][p2]*Hkk[p3][p3][p3]
+trH[p2]*T(6)*g[a][b]*Hkk[p1][p2][p2]*Hkk[p3][p3][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term038(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(6)*H[p2][a][b]*Hkk[p1][p2][p2]*Hkk[p3][p3][p3])
+trH[p1]*T(3)*g[a][b]*Hkk[p2][p1][p2]*Hkk[p3][p3][p3]
-(T(6)*H[p1][a][b]*Hkk[p2][p1][p2]*Hkk[p3][p3][p3])
+(trH[p1]*T(3)*g[a][b]*Hkk[p2][p2][p2]*Hkk[p3][p3][p3])/T(2)
+T(3)*H[p1][a][b]*Hkk[p2][p2][p2]*Hkk[p3][p3][p3]
-(trH[p1]*T(6)*g[a][b]*Hkk[p2][p2][p3]*Hkk[p3][p3][p3])
-(T(12)*H[p1][a][b]*Hkk[p2][p2][p3]*Hkk[p3][p3][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term11(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*trH[p3]*T(3)*kk[p1][p2]*kk[p1][p3]*H[p1][b][i1]*H[p2][a][i1]
-(metric[i1]*trH[p3]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p1][b][i1]*H[p2][a][i1])
+metric[i1]*trH[p3]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p1][b][i1]*H[p2][a][i1]
+metric[i1]*trH[p3]*T(3)*kk[p1][p2]*kk[p1][p3]*H[p1][a][i1]*H[p2][b][i1]
-((metric[i1]*trH[p3]*T(3)*kk[p1][p3]*kk[p2][p3]*H[p1][a][i1]*H[p2][b][i1])/T(2))
-(metric[i1]*trH[p2]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p1][b][i1]*H[p3][a][i1])
-(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(3)*H[p2][b][i1]*H[p3][a][i1])
+metric[i1]*trH[p1]*T(3)*kk[p1][p1]*kk[p2][p3]*H[p2][b][i1]*H[p3][a][i1]
-(metric[i1]*trH[p1]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p2][b][i1]*H[p3][a][i1])
+metric[i1]*trH[p1]*T(3)*kk[p2][p2]*kk[p2][p3]*H[p2][b][i1]*H[p3][a][i1]
+metric[i1]*trH[p1]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p2][b][i1]*H[p3][a][i1]
-(metric[i1]*trH[p1]*T(3)*kk[p2][p2]*kk[p3][p3]*H[p2][b][i1]*H[p3][a][i1])
-(metric[i1]*trH[p1]*T(3)*kk[p2][p3]*kk[p3][p3]*H[p2][b][i1]*H[p3][a][i1])
+metric[i1]*trH[p2]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p1][a][i1]*H[p3][b][i1]
-(metric[i1]*trH[p2]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p1][a][i1]*H[p3][b][i1])
-((metric[i1]*trH[p1]*T(3)*kk[p1][p2]*kk[p1][p3]*H[p2][a][i1]*H[p3][b][i1])/T(2))
+metric[i1]*trH[p1]*T(3)*kk[p2][p2]*kk[p2][p3]*H[p2][a][i1]*H[p3][b][i1]
+metric[i1]*trH[p1]*T(6)*kk[p1][p2]*kk[p3][p3]*H[p2][a][i1]*H[p3][b][i1]
+metric[i1]*trH[p1]*T(6)*kk[p2][p2]*kk[p3][p3]*H[p2][a][i1]*H[p3][b][i1]
-(metric[i1]*trH[p1]*T(3)*kk[p2][p3]*kk[p3][p3]*H[p2][a][i1]*H[p3][b][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term12(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*trH[p2]*T(6)*k[p1][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p1][i1]
+metric[i1]*trH[p2]*T(6)*k[p2][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p1][i1]
+metric[i1]*trH[p2]*T(6)*k[p1][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p1][i1]
+metric[i1]*trH[p3]*T(6)*k[p3][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p2][i1]
-(metric[i1]*trH[p3]*T(6)*k[p2][b]*kk[p3][p3]*H[p2][a][i1]*Hk[p1][p2][i1])
+metric[i1]*trH[p3]*T(3)*k[p1][a]*kk[p1][p3]*H[p2][b][i1]*Hk[p1][p2][i1]
-(metric[i1]*trH[p3]*T(3)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p2][i1])
-(metric[i1]*trH[p3]*T(6)*k[p2][a]*kk[p3][p3]*H[p2][b][i1]*Hk[p1][p2][i1])
-((metric[i1]*trH[p2]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p2][i1])/T(2))
+metric[i1]*trH[p2]*T(15)*k[p3][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p2][i1]
-(metric[i1]*trH[p2]*T(9)*k[p1][b]*kk[p3][p3]*H[p3][a][i1]*Hk[p1][p2][i1])
-(metric[i1]*trH[p2]*T(6)*k[p2][b]*kk[p3][p3]*H[p3][a][i1]*Hk[p1][p2][i1])
+metric[i1]*trH[p2]*T(6)*k[p3][b]*kk[p3][p3]*H[p3][a][i1]*Hk[p1][p2][i1]
+metric[i1]*trH[p2]*T(3)*k[p1][a]*kk[p1][p3]*H[p3][b][i1]*Hk[p1][p2][i1]
-(metric[i1]*trH[p2]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p2][i1])
+metric[i1]*trH[p2]*T(15)*k[p3][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p2][i1]
-(metric[i1]*trH[p2]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][b][i1]*Hk[p1][p2][i1])
-(metric[i1]*trH[p2]*T(6)*k[p2][a]*kk[p3][p3]*H[p3][b][i1]*Hk[p1][p2][i1])
+metric[i1]*trH[p2]*T(6)*k[p3][a]*kk[p3][p3]*H[p3][b][i1]*Hk[p1][p2][i1]
-(metric[i1]*trH[p3]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term13(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*trH[p3]*T(15)*k[p2][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][i1])/T(2))
+metric[i1]*trH[p3]*T(3)*k[p3][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][i1]
+metric[i1]*trH[p3]*T(6)*k[p1][b]*kk[p2][p2]*H[p2][a][i1]*Hk[p1][p3][i1]
-(metric[i1]*trH[p3]*T(6)*k[p2][b]*kk[p2][p2]*H[p2][a][i1]*Hk[p1][p3][i1])
+(metric[i1]*trH[p3]*T(3)*k[p1][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p3][i1])/T(2)
-(metric[i1]*trH[p3]*T(9)*k[p2][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p3][i1])
-(metric[i1]*trH[p3]*T(6)*k[p1][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p3][i1])
-((metric[i1]*trH[p3]*T(15)*k[p2][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p3][i1])/T(2))
+metric[i1]*trH[p3]*T(6)*k[p1][a]*kk[p2][p2]*H[p2][b][i1]*Hk[p1][p3][i1]
-(metric[i1]*trH[p3]*T(6)*k[p2][a]*kk[p2][p2]*H[p2][b][i1]*Hk[p1][p3][i1])
+metric[i1]*trH[p3]*T(6)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p3][i1]
-(metric[i1]*trH[p3]*T(9)*k[p2][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p3][i1])
-((metric[i1]*trH[p2]*T(3)*k[p3][b]*kk[p1][p2]*H[p3][a][i1]*Hk[p1][p3][i1])/T(2))
-(metric[i1]*trH[p2]*T(3)*k[p1][a]*kk[p1][p2]*H[p3][b][i1]*Hk[p1][p3][i1])
-((metric[i1]*trH[p2]*T(3)*k[p3][a]*kk[p1][p2]*H[p3][b][i1]*Hk[p1][p3][i1])/T(2))
+metric[i1]*trH[p2]*T(3)*k[p1][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p3][i1]
+metric[i1]*trH[p2]*T(6)*k[p2][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p3][i1]
+metric[i1]*T(12)*kk[p3][p3]*H[p3][b][i1]*Hk[p1][p2][i1]*Hk[p2][p1][a]
-(metric[i1]*T(3)*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p1][i1]*Hk[p2][p1][b])
+(metric[i1]*T(3)*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p2][i1]*Hk[p2][p1][b])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term14(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(3)*kk[p3][p3]*H[p3][a][i1]*Hk[p1][p2][i1]*Hk[p2][p1][b]
+(metric[i1]*T(15)*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p3][i1]*Hk[p2][p1][b])/T(2)
+(metric[i1]*trH[p3]*T(3)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p1][i1])/T(2)
+(metric[i1]*trH[p3]*T(3)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p1][i1])/T(2)
-(metric[i1]*trH[p1]*T(18)*k[p1][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p2][p1][i1])
+metric[i1]*trH[p1]*T(3)*k[p2][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p2][p1][i1]
+metric[i1]*trH[p1]*T(9)*k[p3][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p2][p1][i1]
-(metric[i1]*trH[p1]*T(15)*k[p1][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p2][p1][i1])
+metric[i1]*trH[p1]*T(9)*k[p3][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p2][p1][i1]
-(metric[i1]*T(6)*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p1][b]*Hk[p2][p1][i1])
-(metric[i1]*trH[p3]*T(12)*g[a][b]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p2][p1][i1])
+metric[i1]*T(12)*kk[p2][p3]*H[p3][a][b]*Hk[p1][p1][i1]*Hk[p2][p1][i1]
-(metric[i1]*T(6)*kk[p1][p3]*H[p3][b][i1]*Hk[p1][p2][a]*Hk[p2][p1][i1])
+metric[i1]*T(6)*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p2][a]*Hk[p2][p1][i1]
-(metric[i1]*T(6)*kk[p1][p3]*H[p3][a][i1]*Hk[p1][p2][b]*Hk[p2][p1][i1])
+metric[i1]*T(3)*kk[p3][p3]*H[p3][a][i1]*Hk[p1][p2][b]*Hk[p2][p1][i1]
+metric[i1]*trH[p3]*T(6)*g[a][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p1][i1]
+(metric[i1]*trH[p3]*T(3)*g[a][b]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p1][i1])/T(2)
+metric[i1]*T(6)*kk[p1][p3]*H[p3][a][b]*Hk[p1][p2][i1]*Hk[p2][p1][i1]
-(metric[i1]*T(3)*kk[p3][p3]*H[p3][a][b]*Hk[p1][p2][i1]*Hk[p2][p1][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term15(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(6)*kk[p1][p2]*H[p3][b][i1]*Hk[p1][p3][a]*Hk[p2][p1][i1]
-((metric[i1]*T(3)*kk[p2][p2]*H[p3][b][i1]*Hk[p1][p3][a]*Hk[p2][p1][i1])/T(2))
+metric[i1]*T(6)*kk[p1][p2]*H[p3][a][i1]*Hk[p1][p3][b]*Hk[p2][p1][i1]
-((metric[i1]*T(3)*kk[p2][p2]*H[p3][a][i1]*Hk[p1][p3][b]*Hk[p2][p1][i1])/T(2))
-(metric[i1]*trH[p3]*T(12)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1])
-(metric[i1]*trH[p3]*T(15)*g[a][b]*kk[p2][p2]*Hk[p1][p3][i1]*Hk[p2][p1][i1])
-(metric[i1]*trH[p3]*T(21)*g[a][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][i1])
-(metric[i1]*T(6)*kk[p1][p2]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1])
+metric[i1]*T(3)*kk[p2][p2]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1]
-(metric[i1]*T(6)*kk[p2][p3]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1])
-(metric[i1]*T(3)*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p2][i1]*Hk[p2][p2][a])
+metric[i1]*T(12)*kk[p3][p3]*H[p3][b][i1]*Hk[p1][p2][i1]*Hk[p2][p2][a]
-(metric[i1]*T(6)*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p2][i1]*Hk[p2][p2][b])
-(metric[i1]*T(6)*kk[p3][p3]*H[p3][a][i1]*Hk[p1][p2][i1]*Hk[p2][p2][b])
-(metric[i1]*trH[p3]*T(6)*k[p1][b]*kk[p1][p3]*H[p1][a][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p3]*T(12)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p3]*T(3)*k[p1][b]*kk[p3][p3]*H[p1][a][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p3]*T(6)*k[p1][a]*kk[p1][p3]*H[p1][b][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p3]*T(12)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p3]*T(3)*k[p1][a]*kk[p3][p3]*H[p1][b][i1]*Hk[p2][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term16(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*trH[p1]*T(12)*k[p1][b]*kk[p1][p3]*H[p3][a][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p1]*T(9)*k[p2][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p2][p2][i1])
+(metric[i1]*trH[p1]*T(3)*k[p3][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p2][p2][i1])/T(2)
+metric[i1]*trH[p1]*T(3)*k[p2][b]*kk[p3][p3]*H[p3][a][i1]*Hk[p2][p2][i1]
-(metric[i1]*trH[p1]*T(6)*k[p1][a]*kk[p1][p3]*H[p3][b][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p1]*T(3)*k[p2][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p2][p2][i1])
+(metric[i1]*trH[p1]*T(3)*k[p3][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p2][p2][i1])/T(2)
+metric[i1]*trH[p1]*T(6)*k[p2][a]*kk[p3][p3]*H[p3][b][i1]*Hk[p2][p2][i1]
+metric[i1]*trH[p1]*T(6)*k[p3][a]*kk[p3][p3]*H[p3][b][i1]*Hk[p2][p2][i1]
+metric[i1]*trH[p3]*T(18)*g[a][b]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p2][p2][i1]
+metric[i1]*trH[p3]*T(3)*g[a][b]*kk[p3][p3]*Hk[p1][p1][i1]*Hk[p2][p2][i1]
-(metric[i1]*T(6)*kk[p2][p3]*H[p3][a][b]*Hk[p1][p1][i1]*Hk[p2][p2][i1])
-(metric[i1]*T(6)*kk[p3][p3]*H[p3][a][b]*Hk[p1][p1][i1]*Hk[p2][p2][i1])
+metric[i1]*trH[p3]*T(9)*g[a][b]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p2][i1]
+metric[i1]*T(3)*kk[p2][p3]*H[p3][a][b]*Hk[p1][p2][i1]*Hk[p2][p2][i1]
-(metric[i1]*T(6)*kk[p3][p3]*H[p3][a][b]*Hk[p1][p2][i1]*Hk[p2][p2][i1])
+(metric[i1]*T(15)*kk[p1][p2]*H[p3][b][i1]*Hk[p1][p3][a]*Hk[p2][p2][i1])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*H[p3][a][i1]*Hk[p1][p3][b]*Hk[p2][p2][i1])/T(2)
+metric[i1]*trH[p3]*T(6)*k[p1][a]*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]
+metric[i1]*trH[p3]*T(6)*k[p1][b]*k[p2][a]*Hk[p1][p3][i1]*Hk[p2][p2][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term17(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*trH[p3]*T(6)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p3]*T(12)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1])
-(metric[i1]*trH[p3]*T(9)*g[a][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p2][i1])
+metric[i1]*trH[p3]*T(6)*g[a][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]
-(metric[i1]*trH[p3]*T(18)*g[a][b]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1])
-(metric[i1]*T(12)*kk[p1][p2]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1])
+metric[i1]*T(3)*kk[p2][p3]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]
+metric[i1]*T(12)*kk[p3][p3]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]
+metric[i1]*T(9)*kk[p2][p2]*H[p3][b][i1]*Hk[p1][p2][i1]*Hk[p2][p3][a]
+(metric[i1]*T(3)*kk[p1][p2]*H[p3][b][i1]*Hk[p1][p3][i1]*Hk[p2][p3][a])/T(2)
+(metric[i1]*T(3)*kk[p1][p2]*H[p3][a][i1]*Hk[p1][p2][i1]*Hk[p2][p3][b])/T(2)
+metric[i1]*T(12)*kk[p2][p2]*H[p3][a][i1]*Hk[p1][p2][i1]*Hk[p2][p3][b]
-(metric[i1]*T(6)*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p2][i1]*Hk[p2][p3][b])
+metric[i1]*T(9)*kk[p1][p2]*H[p3][a][i1]*Hk[p1][p3][i1]*Hk[p2][p3][b]
-(metric[i1]*T(3)*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p3][i1]*Hk[p2][p3][b])
+metric[i1]*trH[p3]*T(6)*k[p1][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p3][i1]
+metric[i1]*trH[p3]*T(6)*k[p2][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p3][i1]
-(metric[i1]*trH[p3]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p3][i1])
+(metric[i1]*trH[p3]*T(15)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p3][i1])/T(2)
+metric[i1]*trH[p3]*T(6)*k[p1][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p3][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term18(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*trH[p3]*T(3)*k[p2][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p3][i1]
+(metric[i1]*trH[p3]*T(15)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p3][i1])/T(2)
+metric[i1]*trH[p1]*T(3)*k[p3][b]*kk[p1][p2]*H[p3][a][i1]*Hk[p2][p3][i1]
-((metric[i1]*trH[p1]*T(3)*k[p3][b]*kk[p2][p2]*H[p3][a][i1]*Hk[p2][p3][i1])/T(2))
+metric[i1]*trH[p1]*T(6)*k[p2][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p2][p3][i1]
+metric[i1]*trH[p1]*T(6)*k[p3][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p2][p3][i1]
+metric[i1]*trH[p1]*T(3)*k[p3][a]*kk[p1][p2]*H[p3][b][i1]*Hk[p2][p3][i1]
-((metric[i1]*trH[p1]*T(3)*k[p3][a]*kk[p2][p2]*H[p3][b][i1]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*trH[p1]*T(9)*k[p2][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p2][p3][i1])/T(2)
-(metric[i1]*trH[p1]*T(3)*k[p3][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p2][p3][i1])
-(metric[i1]*trH[p3]*T(6)*k[p1][a]*k[p2][b]*Hk[p1][p1][i1]*Hk[p2][p3][i1])
-(metric[i1]*trH[p3]*T(6)*g[a][b]*kk[p2][p2]*Hk[p1][p1][i1]*Hk[p2][p3][i1])
-(metric[i1]*trH[p3]*T(18)*g[a][b]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p2][p3][i1])
-(metric[i1]*T(3)*kk[p2][p3]*H[p3][a][b]*Hk[p1][p1][i1]*Hk[p2][p3][i1])
-((metric[i1]*T(3)*kk[p1][p3]*H[p3][b][i1]*Hk[p1][p2][a]*Hk[p2][p3][i1])/T(2))
-((metric[i1]*T(15)*kk[p1][p3]*H[p3][a][i1]*Hk[p1][p2][b]*Hk[p2][p3][i1])/T(2))
+metric[i1]*trH[p3]*T(15)*k[p1][a]*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]
+metric[i1]*trH[p3]*T(12)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]
+metric[i1]*trH[p3]*T(3)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]
-(metric[i1]*trH[p3]*T(12)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term19(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*trH[p3]*T(18)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1])
-(metric[i1]*trH[p3]*T(12)*k[p3][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1])
-(metric[i1]*trH[p3]*T(6)*g[a][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1])
-(metric[i1]*trH[p3]*T(12)*g[a][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1])
-(metric[i1]*T(9)*kk[p1][p2]*H[p3][a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1])
-(metric[i1]*T(3)*kk[p2][p2]*H[p3][a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1])
+metric[i1]*T(30)*kk[p2][p3]*H[p3][a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]
-(metric[i1]*T(9)*kk[p1][p2]*H[p3][a][i1]*Hk[p1][p3][b]*Hk[p2][p3][i1])
-(metric[i1]*trH[p3]*T(3)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1])
-(metric[i1]*trH[p3]*T(12)*k[p2][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1])
+metric[i1]*trH[p3]*T(12)*k[p2][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]
+metric[i1]*trH[p3]*T(12)*g[a][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][i1]
+metric[i1]*trH[p3]*T(12)*g[a][b]*kk[p2][p2]*Hk[p1][p3][i1]*Hk[p2][p3][i1]
+(metric[i1]*T(21)*kk[p1][p2]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1])/T(2)
-(metric[i1]*T(3)*kk[p2][p2]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1])
+metric[i1]*T(3)*kk[p2][p3]*H[p3][a][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]
-(metric[i1]*T(6)*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p1][i1]*Hk[p3][p1][a])
+(metric[i1]*T(3)*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hk[p3][p1][a])/T(2)
+(metric[i1]*T(3)*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p1][i1]*Hk[p3][p1][a])/T(2)
+metric[i1]*T(3)*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p2][i1]*Hk[p3][p1][a]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term110(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(12)*kk[p3][p3]*H[p1][b][i1]*Hk[p2][p2][i1]*Hk[p3][p1][a]
-(metric[i1]*T(12)*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p1][a])
-(metric[i1]*T(3)*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p3][i1]*Hk[p3][p1][a])
-(metric[i1]*T(6)*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][a])
+metric[i1]*T(6)*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][a]
-(metric[i1]*T(6)*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p1][b])
+metric[i1]*T(6)*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p1][b]
+metric[i1]*T(3)*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p1][i1]*Hk[p3][p1][b]
+metric[i1]*T(6)*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p2][i1]*Hk[p3][p1][b]
+metric[i1]*T(12)*kk[p3][p3]*H[p1][a][i1]*Hk[p2][p2][i1]*Hk[p3][p1][b]
-(metric[i1]*T(3)*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])
+(metric[i1]*T(15)*k[p1][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(2)
+metric[i1]*T(6)*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b]
-(metric[i1]*trH[p2]*T(6)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p3][p1][i1])
-(metric[i1]*trH[p2]*T(6)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p3][p1][i1])
+metric[i1]*trH[p1]*T(18)*k[p1][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p3][p1][i1]
+(metric[i1]*trH[p1]*T(3)*k[p2][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p3][p1][i1])/T(2)
+metric[i1]*trH[p1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p3][p1][i1]
+(metric[i1]*trH[p1]*T(3)*k[p2][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p3][p1][i1])/T(2)
+metric[i1]*trH[p2]*T(6)*g[a][b]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p3][p1][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term111(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(3)*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p2][a]*Hk[p3][p1][i1])/T(2))
+metric[i1]*T(3)*kk[p3][p3]*H[p2][b][i1]*Hk[p1][p2][a]*Hk[p3][p1][i1]
+(metric[i1]*T(3)*kk[p1][p3]*H[p2][a][i1]*Hk[p1][p2][b]*Hk[p3][p1][i1])/T(2)
+metric[i1]*T(6)*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p2][b]*Hk[p3][p1][i1]
+metric[i1]*T(3)*kk[p3][p3]*H[p2][a][i1]*Hk[p1][p2][b]*Hk[p3][p1][i1]
+metric[i1]*trH[p2]*T(12)*g[a][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p3][p1][i1]
+metric[i1]*trH[p2]*T(12)*g[a][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p3][p1][i1]
+metric[i1]*trH[p2]*T(15)*g[a][b]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p3][p1][i1]
-(metric[i1]*T(9)*kk[p1][p3]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p1][i1])
-(metric[i1]*T(6)*kk[p2][p3]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p1][i1])
-(metric[i1]*T(6)*kk[p3][p3]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p1][i1])
+metric[i1]*T(6)*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p3][a]*Hk[p3][p1][i1]
-(metric[i1]*T(3)*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][b]*Hk[p3][p1][i1])
+metric[i1]*T(6)*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p3][b]*Hk[p3][p1][i1]
+metric[i1]*trH[p2]*T(6)*g[a][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p3][p1][i1]
-(metric[i1]*trH[p2]*T(12)*g[a][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p3][p1][i1])
-(metric[i1]*T(6)*kk[p1][p2]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p1][i1])
+metric[i1]*T(3)*kk[p2][p3]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p1][i1]
-(metric[i1]*trH[p1]*T(6)*g[a][b]*kk[p2][p3]*Hk[p2][p1][i1]*Hk[p3][p1][i1])
-((metric[i1]*T(33)*kk[p2][p3]*H[p1][a][b]*Hk[p2][p1][i1]*Hk[p3][p1][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term112(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(3)*kk[p2][p3]*H[p1][a][b]*Hk[p2][p2][i1]*Hk[p3][p1][i1]
-(metric[i1]*T(12)*k[p1][b]*Hk[p1][p3][a]*Hk[p2][p2][i1]*Hk[p3][p1][i1])
-(metric[i1]*T(3)*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p1][i1])
-(metric[i1]*T(3)*k[p1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p1][i1])
+(metric[i1]*T(9)*kk[p1][p2]*H[p1][a][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*T(15)*k[p1][b]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1])/T(2)
-((metric[i1]*T(3)*k[p3][b]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*Hk[p1][p2][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*T(15)*k[p3][a]*Hk[p1][p2][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1])/T(2))
+(metric[i1]*T(3)*kk[p1][p3]*H[p2][b][i1]*Hk[p1][p1][i1]*Hk[p3][p2][a])/T(2)
-(metric[i1]*T(6)*kk[p3][p3]*H[p2][b][i1]*Hk[p1][p1][i1]*Hk[p3][p2][a])
+metric[i1]*T(6)*kk[p3][p3]*H[p2][b][i1]*Hk[p1][p2][i1]*Hk[p3][p2][a]
+(metric[i1]*T(3)*kk[p1][p1]*H[p2][b][i1]*Hk[p1][p3][i1]*Hk[p3][p2][a])/T(2)
-((metric[i1]*T(3)*kk[p1][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hk[p3][p2][a])/T(2))
-(metric[i1]*T(6)*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hk[p3][p2][a])
-(metric[i1]*T(9)*kk[p3][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hk[p3][p2][a])
+(metric[i1]*T(3)*kk[p1][p3]*H[p1][b][i1]*Hk[p2][p1][i1]*Hk[p3][p2][a])/T(2)
-((metric[i1]*T(9)*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][a])/T(2))
+(metric[i1]*T(3)*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][a])/T(2)
+(metric[i1]*T(15)*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][a])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term113(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(6)*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][a])
-(metric[i1]*T(6)*kk[p1][p1]*H[p1][b][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
-(metric[i1]*T(6)*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
+metric[i1]*T(12)*k[p1][b]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a]
+metric[i1]*T(6)*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a]
+metric[i1]*T(6)*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a]
-(metric[i1]*T(6)*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
+(metric[i1]*T(51)*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])/T(2)
+metric[i1]*T(6)*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a]
-((metric[i1]*T(3)*kk[p1][p3]*H[p2][a][i1]*Hk[p1][p1][i1]*Hk[p3][p2][b])/T(2))
+metric[i1]*T(3)*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p2][i1]*Hk[p3][p2][b]
+metric[i1]*T(6)*kk[p3][p3]*H[p2][a][i1]*Hk[p1][p2][i1]*Hk[p3][p2][b]
+(metric[i1]*T(3)*kk[p1][p1]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p2][b])/T(2)
-(metric[i1]*T(3)*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*T(6)*kk[p1][p3]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*T(12)*kk[p3][p3]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p2][b])
+metric[i1]*T(9)*k[p1][a]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][b]
+(metric[i1]*T(15)*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][b])/T(2)
+metric[i1]*T(3)*k[p1][a]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][b]
+metric[i1]*T(18)*k[p1][a]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term114(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(3)*k[p1][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b]
+metric[i1]*T(6)*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b]
-(metric[i1]*T(6)*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-((metric[i1]*T(9)*k[p1][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])/T(2))
+metric[i1]*T(9)*k[p2][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b]
-(metric[i1]*T(18)*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*trH[p2]*T(6)*k[p1][b]*kk[p1][p3]*H[p1][a][i1]*Hk[p3][p2][i1])
-((metric[i1]*trH[p2]*T(3)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*trH[p2]*T(6)*k[p1][a]*kk[p1][p3]*H[p1][b][i1]*Hk[p3][p2][i1])
-((metric[i1]*trH[p2]*T(3)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*trH[p1]*T(6)*k[p2][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p3][p2][i1])
-(metric[i1]*trH[p1]*T(3)*k[p3][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p3][p2][i1])
-((metric[i1]*trH[p1]*T(3)*k[p2][b]*kk[p3][p3]*H[p2][a][i1]*Hk[p3][p2][i1])/T(2))
+metric[i1]*trH[p1]*T(3)*k[p2][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p3][p2][i1]
-((metric[i1]*trH[p1]*T(3)*k[p3][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*trH[p1]*T(3)*k[p2][a]*kk[p3][p3]*H[p2][b][i1]*Hk[p3][p2][i1])/T(2))
+metric[i1]*trH[p2]*T(15)*g[a][b]*kk[p1][p3]*Hk[p1][p1][i1]*Hk[p3][p2][i1]
+metric[i1]*trH[p2]*T(18)*g[a][b]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p3][p2][i1]
+metric[i1]*trH[p2]*T(12)*g[a][b]*kk[p3][p3]*Hk[p1][p1][i1]*Hk[p3][p2][i1]
-(metric[i1]*T(6)*kk[p1][p3]*H[p2][a][b]*Hk[p1][p1][i1]*Hk[p3][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term115(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(3)*kk[p2][p3]*H[p2][a][b]*Hk[p1][p1][i1]*Hk[p3][p2][i1]
-(metric[i1]*T(6)*kk[p3][p3]*H[p2][a][b]*Hk[p1][p1][i1]*Hk[p3][p2][i1])
-(metric[i1]*trH[p2]*T(12)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(3)*kk[p2][p3]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*kk[p3][p3]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(3)*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p3][a]*Hk[p3][p2][i1])
-(metric[i1]*T(3)*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][b]*Hk[p3][p2][i1])
-(metric[i1]*trH[p2]*T(21)*k[p1][a]*k[p1][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1])
+metric[i1]*trH[p2]*T(12)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
+metric[i1]*trH[p2]*T(12)*k[p2][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
+metric[i1]*trH[p2]*T(24)*k[p2][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
-(metric[i1]*trH[p2]*T(3)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1])
+metric[i1]*trH[p2]*T(6)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
+metric[i1]*trH[p2]*T(9)*g[a][b]*kk[p1][p1]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
+metric[i1]*trH[p2]*T(15)*g[a][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
-(metric[i1]*trH[p2]*T(12)*g[a][b]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(3)*kk[p1][p1]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1])
+(metric[i1]*T(9)*kk[p1][p2]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1])/T(2)
+metric[i1]*T(24)*kk[p1][p3]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
-(metric[i1]*T(18)*kk[p2][p3]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term116(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(3)*kk[p3][p3]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
-((metric[i1]*T(3)*kk[p1][p3]*H[p1][b][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*T(15)*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*T(33)*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2)
-(metric[i1]*T(6)*kk[p1][p3]*H[p1][a][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
-(metric[i1]*T(6)*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
-((metric[i1]*T(3)*k[p1][a]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*T(15)*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*trH[p1]*T(3)*g[a][b]*kk[p3][p3]*Hk[p2][p1][i1]*Hk[p3][p2][i1])
+metric[i1]*T(3)*kk[p1][p3]*H[p1][a][b]*Hk[p2][p1][i1]*Hk[p3][p2][i1]
-(metric[i1]*T(15)*k[p1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(6)*k[p3][b]*Hk[p1][p3][a]*Hk[p2][p1][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(3)*k[p1][a]*Hk[p1][p3][b]*Hk[p2][p1][i1]*Hk[p3][p2][i1])
+metric[i1]*T(12)*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p2][a]*Hk[p3][p2][i1]
-(metric[i1]*T(12)*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p2][a]*Hk[p3][p2][i1])
-(metric[i1]*T(12)*k[p2][a]*Hk[p1][p3][i1]*Hk[p2][p2][b]*Hk[p3][p2][i1])
-(metric[i1]*trH[p1]*T(6)*k[p2][a]*k[p3][b]*Hk[p2][p2][i1]*Hk[p3][p2][i1])
+metric[i1]*trH[p1]*T(12)*g[a][b]*kk[p2][p3]*Hk[p2][p2][i1]*Hk[p3][p2][i1]
-(metric[i1]*trH[p1]*T(6)*g[a][b]*kk[p3][p3]*Hk[p2][p2][i1]*Hk[p3][p2][i1])
+metric[i1]*T(24)*kk[p2][p3]*H[p1][a][b]*Hk[p2][p2][i1]*Hk[p3][p2][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term117(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(12)*kk[p3][p3]*H[p1][a][b]*Hk[p2][p2][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(12)*k[p1][b]*Hk[p1][p3][a]*Hk[p2][p2][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(12)*k[p1][a]*Hk[p1][p3][b]*Hk[p2][p2][i1]*Hk[p3][p2][i1])
+(metric[i1]*T(3)*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])/T(2)
-(metric[i1]*T(6)*k[p1][b]*Hk[p1][p1][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
-((metric[i1]*T(45)*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])/T(2))
+metric[i1]*T(6)*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1]
-(metric[i1]*T(6)*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
-(metric[i1]*T(6)*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
+(metric[i1]*T(9)*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])/T(2)
-(metric[i1]*T(6)*k[p1][a]*Hk[p1][p1][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])
+(metric[i1]*T(15)*k[p1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])/T(2)
+metric[i1]*T(18)*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1]
+metric[i1]*T(3)*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1]
-(metric[i1]*T(3)*k[p1][a]*Hk[p1][p3][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])
-(metric[i1]*T(6)*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])
-(metric[i1]*trH[p1]*T(3)*k[p1][a]*k[p1][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
+metric[i1]*trH[p1]*T(9)*k[p2][a]*k[p2][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]
+metric[i1]*trH[p1]*T(6)*k[p2][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p2][i1]
-(metric[i1]*trH[p1]*T(3)*k[p2][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term118(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*trH[p1]*T(9)*k[p3][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
+(metric[i1]*trH[p1]*T(3)*g[a][b]*kk[p1][p1]*Hk[p2][p3][i1]*Hk[p3][p2][i1])/T(2)
+metric[i1]*trH[p1]*T(6)*g[a][b]*kk[p2][p2]*Hk[p2][p3][i1]*Hk[p3][p2][i1]
-(metric[i1]*trH[p1]*T(3)*g[a][b]*kk[p2][p3]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
-(metric[i1]*trH[p1]*T(6)*g[a][b]*kk[p3][p3]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(3)*kk[p1][p2]*H[p1][a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
+metric[i1]*T(12)*kk[p2][p2]*H[p1][a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]
-(metric[i1]*T(6)*kk[p2][p3]*H[p1][a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(12)*kk[p3][p3]*H[p1][a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
+metric[i1]*T(3)*k[p1][b]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p2][i1]
+metric[i1]*T(3)*k[p1][a]*Hk[p1][p2][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]
-(metric[i1]*T(3)*k[p1][b]*Hk[p1][p3][a]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
-(metric[i1]*T(3)*k[p1][a]*Hk[p1][p3][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1])
+metric[i1]*T(6)*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p1][i1]*Hk[p3][p3][a]
-(metric[i1]*T(6)*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p2][i1]*Hk[p3][p3][a])
+metric[i1]*T(15)*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p3][i1]*Hk[p3][p3][a]
-(metric[i1]*T(6)*kk[p2][p2]*H[p2][b][i1]*Hk[p1][p3][i1]*Hk[p3][p3][a])
+metric[i1]*T(3)*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hk[p3][p3][a]
+metric[i1]*T(3)*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p1][i1]*Hk[p3][p3][a]
-(metric[i1]*T(12)*kk[p1][p3]*H[p1][b][i1]*Hk[p2][p2][i1]*Hk[p3][p3][a])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term119(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(6)*k[p3][b]*Hk[p1][p1][i1]*Hk[p2][p2][i1]*Hk[p3][p3][a]
+metric[i1]*T(6)*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p2][i1]*Hk[p3][p3][a]
-(metric[i1]*T(18)*k[p1][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p3][a])
+metric[i1]*T(6)*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p3][a]
-(metric[i1]*T(12)*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p3][a])
-(metric[i1]*T(9)*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p3][i1]*Hk[p3][p3][a])
-(metric[i1]*T(12)*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][a])
-(metric[i1]*T(6)*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][a])
+metric[i1]*T(12)*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][a]
-(metric[i1]*T(6)*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p2][i1]*Hk[p3][p3][b])
+metric[i1]*T(12)*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p3][b]
+metric[i1]*T(12)*kk[p2][p2]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p3][b]
+metric[i1]*T(6)*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p3][i1]*Hk[p3][p3][b]
+metric[i1]*T(3)*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p1][i1]*Hk[p3][p3][b]
-(metric[i1]*T(12)*kk[p1][p3]*H[p1][a][i1]*Hk[p2][p2][i1]*Hk[p3][p3][b])
+metric[i1]*T(6)*k[p3][a]*Hk[p1][p1][i1]*Hk[p2][p2][i1]*Hk[p3][p3][b]
+metric[i1]*T(6)*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p2][i1]*Hk[p3][p3][b]
-(metric[i1]*T(12)*k[p1][a]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p3][b])
+metric[i1]*T(12)*k[p2][a]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p3][b]
-(metric[i1]*T(6)*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][b])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term120(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(12)*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][b]
+metric[i1]*trH[p2]*T(6)*k[p1][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p2]*T(3)*k[p3][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p3][p3][i1])
-(metric[i1]*trH[p2]*T(3)*k[p1][b]*kk[p2][p2]*H[p1][a][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p2]*T(12)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p2]*T(6)*k[p1][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p2]*T(3)*k[p1][a]*kk[p2][p2]*H[p1][b][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p2]*T(12)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p1]*T(12)*k[p1][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p1]*T(6)*k[p3][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p3][p3][i1])
-(metric[i1]*trH[p1]*T(6)*k[p3][b]*kk[p2][p2]*H[p2][a][i1]*Hk[p3][p3][i1])
+(metric[i1]*trH[p1]*T(3)*k[p2][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p3][p3][i1])/T(2)
+metric[i1]*trH[p1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p1]*T(12)*k[p1][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p3][p3][i1])
-(metric[i1]*trH[p1]*T(6)*k[p2][a]*kk[p2][p2]*H[p2][b][i1]*Hk[p3][p3][i1])
-(metric[i1]*trH[p1]*T(9)*k[p3][a]*kk[p2][p2]*H[p2][b][i1]*Hk[p3][p3][i1])
+(metric[i1]*trH[p1]*T(3)*k[p2][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p3][p3][i1])/T(2)
+metric[i1]*trH[p1]*T(3)*k[p3][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p3][p3][i1]
+metric[i1]*T(6)*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p1][b]*Hk[p3][p3][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term121(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*trH[p2]*T(6)*g[a][b]*kk[p1][p2]*Hk[p1][p1][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p2]*T(3)*g[a][b]*kk[p2][p2]*Hk[p1][p1][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p2]*T(12)*g[a][b]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p3][p3][i1])
+metric[i1]*T(6)*kk[p2][p3]*H[p2][a][b]*Hk[p1][p1][i1]*Hk[p3][p3][i1]
-(metric[i1]*T(9)*kk[p1][p3]*H[p2][b][i1]*Hk[p1][p2][a]*Hk[p3][p3][i1])
-(metric[i1]*T(6)*kk[p1][p1]*H[p2][a][i1]*Hk[p1][p2][b]*Hk[p3][p3][i1])
-(metric[i1]*T(12)*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p2][b]*Hk[p3][p3][i1])
-(metric[i1]*T(12)*kk[p1][p3]*H[p2][a][i1]*Hk[p1][p2][b]*Hk[p3][p3][i1])
-(metric[i1]*trH[p2]*T(6)*k[p1][a]*k[p1][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p2]*T(12)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p2]*T(3)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p2]*T(18)*k[p2][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p2]*T(3)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p2]*T(6)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p2]*T(6)*g[a][b]*kk[p1][p1]*Hk[p1][p2][i1]*Hk[p3][p3][i1])
-(metric[i1]*trH[p2]*T(15)*g[a][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p2]*T(18)*g[a][b]*kk[p2][p2]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p2]*T(18)*g[a][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p3][p3][i1])
+metric[i1]*T(6)*kk[p1][p1]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*T(6)*kk[p1][p2]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term122(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(21)*kk[p1][p3]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
-(metric[i1]*T(12)*kk[p2][p2]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1])
+metric[i1]*T(9)*kk[p2][p3]*H[p2][a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
-(metric[i1]*T(3)*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p3][a]*Hk[p3][p3][i1])
+metric[i1]*T(15)*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][b]*Hk[p3][p3][i1]
-(metric[i1]*trH[p2]*T(12)*k[p2][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p2]*T(6)*g[a][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p2]*T(3)*g[a][b]*kk[p2][p2]*Hk[p1][p3][i1]*Hk[p3][p3][i1]
-(metric[i1]*T(18)*kk[p1][p2]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p3][i1])
-(metric[i1]*T(6)*kk[p2][p2]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p3][i1])
-(metric[i1]*T(3)*kk[p2][p3]*H[p2][a][b]*Hk[p1][p3][i1]*Hk[p3][p3][i1])
-(metric[i1]*T(6)*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1])
+metric[i1]*T(12)*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1]
-(metric[i1]*T(12)*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1])
+metric[i1]*T(12)*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p1][b]*Hk[p3][p3][i1]
+metric[i1]*T(6)*k[p1][a]*Hk[p1][p2][i1]*Hk[p2][p1][b]*Hk[p3][p3][i1]
-(metric[i1]*T(21)*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p1][b]*Hk[p3][p3][i1])
+metric[i1]*trH[p1]*T(6)*k[p1][a]*k[p2][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p1]*T(12)*g[a][b]*kk[p1][p2]*Hk[p2][p1][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p1]*T(3)*g[a][b]*kk[p2][p3]*Hk[p2][p1][i1]*Hk[p3][p3][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term123(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(6)*kk[p1][p2]*H[p1][a][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]
+metric[i1]*T(6)*k[p1][b]*Hk[p1][p2][a]*Hk[p2][p1][i1]*Hk[p3][p3][i1]
-(metric[i1]*T(6)*k[p1][a]*Hk[p1][p2][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1])
-(metric[i1]*T(6)*k[p2][a]*Hk[p1][p2][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1])
+metric[i1]*T(3)*k[p3][a]*Hk[p1][p2][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]
+metric[i1]*T(12)*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p2][a]*Hk[p3][p3][i1]
+metric[i1]*T(12)*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p2][a]*Hk[p3][p3][i1]
-(metric[i1]*T(12)*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p2][a]*Hk[p3][p3][i1])
+metric[i1]*T(6)*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p2][a]*Hk[p3][p3][i1]
+metric[i1]*T(6)*k[p1][a]*Hk[p1][p2][i1]*Hk[p2][p2][b]*Hk[p3][p3][i1]
-(metric[i1]*T(18)*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p2][b]*Hk[p3][p3][i1])
+metric[i1]*T(6)*k[p2][a]*Hk[p1][p3][i1]*Hk[p2][p2][b]*Hk[p3][p3][i1]
-(metric[i1]*trH[p1]*T(6)*k[p1][a]*k[p1][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p1]*T(6)*k[p2][a]*k[p2][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p1]*T(3)*k[p2][b]*k[p3][a]*Hk[p2][p2][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p1]*T(6)*k[p3][a]*k[p3][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1])
-(metric[i1]*trH[p1]*T(6)*g[a][b]*kk[p2][p2]*Hk[p2][p2][i1]*Hk[p3][p3][i1])
-(metric[i1]*trH[p1]*T(3)*g[a][b]*kk[p2][p3]*Hk[p2][p2][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p1]*T(6)*g[a][b]*kk[p3][p3]*Hk[p2][p2][i1]*Hk[p3][p3][i1]
-(metric[i1]*T(12)*kk[p2][p2]*H[p1][a][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term124(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(6)*kk[p2][p3]*H[p1][a][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1])
+metric[i1]*T(12)*kk[p3][p3]*H[p1][a][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*T(12)*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1]
+metric[i1]*T(3)*k[p1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1]
-(metric[i1]*T(6)*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])
-(metric[i1]*T(9)*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])
-(metric[i1]*T(6)*k[p1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])
-(metric[i1]*T(6)*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])
-(metric[i1]*trH[p1]*T(6)*k[p2][a]*k[p2][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p1]*T(6)*k[p2][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p1]*T(6)*g[a][b]*kk[p1][p2]*Hk[p2][p3][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p1]*T(12)*g[a][b]*kk[p2][p2]*Hk[p2][p3][i1]*Hk[p3][p3][i1]
-(metric[i1]*trH[p1]*T(12)*g[a][b]*kk[p2][p3]*Hk[p2][p3][i1]*Hk[p3][p3][i1])
+metric[i1]*T(24)*kk[p2][p2]*H[p1][a][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]
-(metric[i1]*T(24)*kk[p2][p3]*H[p1][a][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1])
+metric[i1]*T(18)*k[p1][b]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p3][i1]
+metric[i1]*T(18)*k[p1][a]*Hk[p1][p2][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]
+metric[i1]*T(6)*kk[p3][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p1][p2]
+(metric[i1]*T(3)*kk[p1][p3]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p1][p2])/T(2)
-(metric[i1]*T(3)*k[p3][b]*H[p3][a][i1]*Hk[p2][p3][i1]*Hkk[p1][p1][p2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term125(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(3)*k[p3][a]*H[p3][b][i1]*Hk[p2][p3][i1]*Hkk[p1][p1][p2])
-(metric[i1]*T(12)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p1][p2])
-(metric[i1]*T(6)*k[p1][b]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p1][p2])
+metric[i1]*T(6)*k[p3][a]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p1][p2]
+metric[i1]*T(18)*g[a][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p1][p2]
+metric[i1]*T(12)*g[a][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p1][p2]
-(metric[i1]*T(12)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p1][p2])
-(metric[i1]*T(6)*kk[p2][p2]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p1][p3])
+(metric[i1]*T(3)*kk[p1][p2]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p1][p3])/T(2)
-(metric[i1]*T(6)*k[p2][b]*H[p3][a][i1]*Hk[p2][p2][i1]*Hkk[p1][p1][p3])
+metric[i1]*T(6)*k[p2][b]*H[p2][a][i1]*Hk[p3][p2][i1]*Hkk[p1][p1][p3]
+metric[i1]*T(6)*k[p2][a]*H[p2][b][i1]*Hk[p3][p2][i1]*Hkk[p1][p1][p3]
+metric[i1]*T(24)*g[a][b]*Hk[p2][p2][i1]*Hk[p3][p2][i1]*Hkk[p1][p1][p3]
+metric[i1]*T(24)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p1][p3]
+metric[i1]*T(6)*kk[p2][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p2][p2]
-(metric[i1]*T(6)*kk[p3][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p2][p2])
+metric[i1]*T(3)*kk[p2][p3]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p2][p2]
-(metric[i1]*T(6)*kk[p3][p3]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p2][p2])
+metric[i1]*T(6)*k[p3][b]*H[p3][a][i1]*Hk[p2][p3][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(6)*k[p3][a]*H[p3][b][i1]*Hk[p2][p3][i1]*Hkk[p1][p2][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term126(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(12)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p2]
-(metric[i1]*T(12)*k[p1][b]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p2])
+metric[i1]*T(18)*k[p3][b]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(18)*k[p3][a]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(12)*g[a][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(24)*g[a][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p2]
-(metric[i1]*T(12)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p2])
+metric[i1]*T(3)*kk[p1][p2]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(3)*kk[p1][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p2][p3])/T(2)
-(metric[i1]*T(3)*kk[p2][p2]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(9)*kk[p2][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(6)*kk[p3][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(3)*kk[p1][p1]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p2][p3])/T(2))
+metric[i1]*T(3)*kk[p1][p2]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p2][p3]
+metric[i1]*T(6)*kk[p1][p3]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(9)*kk[p2][p3]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(12)*k[p1][b]*H[p3][a][i1]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(3)*k[p3][b]*H[p3][a][i1]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(2))
+(metric[i1]*T(3)*k[p1][a]*H[p3][b][i1]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(2)
-((metric[i1]*T(3)*k[p3][a]*H[p3][b][i1]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term127(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*k[p1][b]*H[p3][a][i1]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])/T(2)
-(metric[i1]*T(3)*k[p2][b]*H[p3][a][i1]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])
+(metric[i1]*T(15)*k[p1][a]*H[p3][b][i1]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])/T(2)
-((metric[i1]*T(21)*k[p1][b]*H[p3][a][i1]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])/T(2))
-(metric[i1]*T(12)*k[p2][b]*H[p3][a][i1]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])
+metric[i1]*T(6)*k[p3][b]*H[p3][a][i1]*Hk[p2][p3][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(6)*k[p1][a]*H[p3][b][i1]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(6)*k[p2][a]*H[p3][b][i1]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])
+metric[i1]*T(12)*k[p3][a]*H[p3][b][i1]*Hk[p2][p3][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(39)*k[p1][b]*H[p2][a][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2)
-((metric[i1]*T(9)*k[p2][b]*H[p2][a][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2))
+metric[i1]*T(6)*k[p3][b]*H[p2][a][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(9)*k[p1][a]*H[p2][b][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2)
-((metric[i1]*T(9)*k[p2][a]*H[p2][b][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2))
+(metric[i1]*T(3)*k[p3][a]*H[p2][b][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(12)*g[a][b]*Hk[p2][p1][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(3)*k[p1][b]*H[p2][a][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])
+metric[i1]*T(3)*k[p2][b]*H[p2][a][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(3)*k[p1][a]*H[p2][b][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])
+metric[i1]*T(3)*k[p2][a]*H[p2][b][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term128(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(6)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(6)*k[p1][b]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])
+metric[i1]*T(6)*k[p3][b]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(3)*k[p1][a]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(12)*k[p2][a]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])
+metric[i1]*T(6)*k[p3][a]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
+metric[i1]*T(9)*g[a][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(18)*g[a][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(6)*kk[p1][p2]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p3][p1])
+metric[i1]*T(12)*k[p1][b]*H[p3][a][i1]*Hk[p2][p2][i1]*Hkk[p1][p3][p1]
+metric[i1]*T(12)*k[p1][a]*H[p3][b][i1]*Hk[p2][p2][i1]*Hkk[p1][p3][p1]
-((metric[i1]*T(3)*kk[p1][p2]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p3][p2])/T(2))
+metric[i1]*T(21)*kk[p2][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p3][p2]
+metric[i1]*T(6)*kk[p3][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p3][p2]
+metric[i1]*T(9)*kk[p2][p3]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p3][p2]
+metric[i1]*T(3)*kk[p3][p3]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p3][p2]
+(metric[i1]*T(21)*k[p1][b]*H[p3][a][i1]*Hk[p2][p2][i1]*Hkk[p1][p3][p2])/T(2)
+(metric[i1]*T(3)*k[p1][a]*H[p3][b][i1]*Hk[p2][p2][i1]*Hkk[p1][p3][p2])/T(2)
-((metric[i1]*T(15)*k[p1][b]*H[p3][a][i1]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])/T(2))
+metric[i1]*T(6)*k[p2][b]*H[p3][a][i1]*Hk[p2][p3][i1]*Hkk[p1][p3][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term129(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(3)*k[p3][b]*H[p3][a][i1]*Hk[p2][p3][i1]*Hkk[p1][p3][p2]
-((metric[i1]*T(3)*k[p1][a]*H[p3][b][i1]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])/T(2))
-(metric[i1]*T(3)*k[p3][a]*H[p3][b][i1]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])
-(metric[i1]*T(24)*g[a][b]*Hk[p2][p2][i1]*Hk[p3][p1][i1]*Hkk[p1][p3][p2])
+metric[i1]*T(12)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p3][p2]
+metric[i1]*T(18)*k[p1][b]*H[p2][a][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2]
-(metric[i1]*T(12)*k[p2][b]*H[p2][a][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(15)*k[p1][a]*H[p2][b][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])/T(2)
-(metric[i1]*T(12)*k[p2][a]*H[p2][b][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])
+metric[i1]*T(12)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2]
-(metric[i1]*T(12)*k[p1][b]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])
-(metric[i1]*T(6)*k[p2][b]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])
-(metric[i1]*T(3)*k[p3][b]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])
-(metric[i1]*T(9)*k[p1][a]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])
+metric[i1]*T(6)*k[p2][a]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2]
-(metric[i1]*T(6)*k[p3][a]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])
+metric[i1]*T(21)*g[a][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2]
+metric[i1]*T(24)*g[a][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2]
-(metric[i1]*T(9)*kk[p1][p2]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p3][p3])
-(metric[i1]*T(3)*kk[p2][p3]*H[p2][b][i1]*H[p3][a][i1]*Hkk[p1][p3][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term130(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(3)*kk[p1][p2]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p3][p3])/T(2))
-(metric[i1]*T(6)*kk[p2][p3]*H[p2][a][i1]*H[p3][b][i1]*Hkk[p1][p3][p3])
+metric[i1]*T(12)*k[p1][b]*H[p3][a][i1]*Hk[p2][p2][i1]*Hkk[p1][p3][p3]
-(metric[i1]*T(12)*k[p2][b]*H[p3][a][i1]*Hk[p2][p2][i1]*Hkk[p1][p3][p3])
-(metric[i1]*T(12)*k[p2][a]*H[p3][b][i1]*Hk[p2][p2][i1]*Hkk[p1][p3][p3])
-(metric[i1]*T(12)*g[a][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p3])
-(metric[i1]*T(24)*g[a][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p3])
-((metric[i1]*T(3)*kk[p2][p3]*H[p1][b][i1]*H[p3][a][i1]*Hkk[p2][p1][p1])/T(2))
-(metric[i1]*T(3)*kk[p2][p3]*H[p1][a][i1]*H[p3][b][i1]*Hkk[p2][p1][p1])
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p1])
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p1])
-(metric[i1]*T(12)*kk[p3][p3]*H[p1][b][i1]*H[p3][a][i1]*Hkk[p2][p1][p2])
-(metric[i1]*T(6)*kk[p2][p3]*H[p1][a][i1]*H[p3][b][i1]*Hkk[p2][p1][p2])
-(metric[i1]*T(6)*kk[p3][p3]*H[p1][a][i1]*H[p3][b][i1]*Hkk[p2][p1][p2])
-(metric[i1]*T(24)*g[a][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])
+metric[i1]*T(18)*k[p3][b]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p2]
+metric[i1]*T(12)*k[p3][a]*H[p1][b][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p2]
-(metric[i1]*T(30)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p2])
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p3][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p2])
-(metric[i1]*T(3)*kk[p1][p2]*H[p1][b][i1]*H[p3][a][i1]*Hkk[p2][p1][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term131(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(3)*kk[p1][p2]*H[p1][a][i1]*H[p3][b][i1]*Hkk[p2][p1][p3]
-(metric[i1]*T(9)*k[p1][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])
-(metric[i1]*T(3)*k[p1][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])
+metric[i1]*T(6)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p1][i1]*Hkk[p2][p1][p3]
+metric[i1]*T(12)*k[p1][b]*H[p1][a][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3]
+(metric[i1]*T(3)*k[p2][b]*H[p1][a][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2)
+metric[i1]*T(18)*k[p1][a]*H[p1][b][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3]
-((metric[i1]*T(3)*k[p2][a]*H[p1][b][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2))
+metric[i1]*T(12)*g[a][b]*Hk[p1][p1][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3]
+metric[i1]*T(9)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3]
-(metric[i1]*T(6)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])
-(metric[i1]*T(12)*k[p1][b]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p1])
-(metric[i1]*T(12)*k[p1][a]*H[p1][b][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p1])
-(metric[i1]*T(24)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p2])
-(metric[i1]*T(6)*kk[p1][p2]*H[p1][b][i1]*H[p3][a][i1]*Hkk[p2][p2][p3])
-(metric[i1]*T(12)*k[p1][b]*H[p3][a][i1]*Hk[p1][p1][i1]*Hkk[p2][p2][p3])
-(metric[i1]*T(12)*k[p1][a]*H[p3][b][i1]*Hk[p1][p1][i1]*Hkk[p2][p2][p3])
-((metric[i1]*T(33)*k[p1][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p2][p3])/T(2))
-(metric[i1]*T(12)*k[p2][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p2][p3])
-(metric[i1]*T(3)*k[p3][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p2][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term132(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(3)*k[p1][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p2][p3])/T(2))
-(metric[i1]*T(3)*k[p3][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p2][p3])
-(metric[i1]*T(12)*k[p1][b]*H[p3][a][i1]*Hk[p1][p3][i1]*Hkk[p2][p2][p3])
+metric[i1]*T(12)*k[p3][b]*H[p3][a][i1]*Hk[p1][p3][i1]*Hkk[p2][p2][p3]
+metric[i1]*T(12)*k[p3][a]*H[p3][b][i1]*Hk[p1][p3][i1]*Hkk[p2][p2][p3]
-(metric[i1]*T(3)*k[p1][b]*H[p1][a][i1]*Hk[p3][p1][i1]*Hkk[p2][p2][p3])
-(metric[i1]*T(3)*k[p1][a]*H[p1][b][i1]*Hk[p3][p1][i1]*Hkk[p2][p2][p3])
+metric[i1]*T(24)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p1][i1]*Hkk[p2][p2][p3]
-(metric[i1]*T(3)*k[p1][b]*H[p1][a][i1]*Hk[p3][p2][i1]*Hkk[p2][p2][p3])
-(metric[i1]*T(3)*k[p1][a]*H[p1][b][i1]*Hk[p3][p2][i1]*Hkk[p2][p2][p3])
+metric[i1]*T(24)*g[a][b]*Hk[p1][p1][i1]*Hk[p3][p2][i1]*Hkk[p2][p2][p3]
+metric[i1]*T(48)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p2][p3]
-(metric[i1]*T(6)*k[p1][b]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p3])
-(metric[i1]*T(6)*k[p1][a]*H[p1][b][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p3])
+metric[i1]*T(12)*g[a][b]*Hk[p1][p1][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p3]
+metric[i1]*T(18)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p3]
+metric[i1]*T(12)*k[p1][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p1]
-((metric[i1]*T(3)*k[p1][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p1])/T(2))
-(metric[i1]*T(24)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p1][i1]*Hkk[p2][p3][p1])
+metric[i1]*T(12)*k[p1][b]*H[p1][a][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term133(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(6)*k[p1][a]*H[p1][b][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p1])
-(metric[i1]*T(6)*g[a][b]*Hk[p1][p1][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p1])
-(metric[i1]*T(3)*kk[p1][p2]*H[p1][b][i1]*H[p3][a][i1]*Hkk[p2][p3][p2])
-(metric[i1]*T(6)*kk[p1][p2]*H[p1][a][i1]*H[p3][b][i1]*Hkk[p2][p3][p2])
-(metric[i1]*T(3)*k[p1][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])
-(metric[i1]*T(6)*k[p1][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])
-(metric[i1]*T(3)*k[p2][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])
+metric[i1]*T(12)*k[p2][b]*H[p3][a][i1]*Hk[p1][p3][i1]*Hkk[p2][p3][p2]
+metric[i1]*T(12)*k[p2][a]*H[p3][b][i1]*Hk[p1][p3][i1]*Hkk[p2][p3][p2]
+metric[i1]*T(6)*k[p1][b]*H[p1][a][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p2]
+metric[i1]*T(6)*k[p1][a]*H[p1][b][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p2]
-(metric[i1]*T(24)*g[a][b]*Hk[p1][p1][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p2])
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p2])
-(metric[i1]*T(30)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p2])
+metric[i1]*T(9)*kk[p1][p2]*H[p1][b][i1]*H[p3][a][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(18)*k[p1][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(6)*k[p2][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p3]
-(metric[i1]*T(12)*k[p3][b]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
+(metric[i1]*T(15)*k[p1][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])/T(2)
+metric[i1]*T(6)*k[p2][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term134(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(12)*k[p3][a]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p2][i1]*Hk[p3][p1][i1]*Hkk[p2][p3][p3])
+metric[i1]*T(3)*k[p1][b]*H[p1][a][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(3)*k[p1][a]*H[p1][b][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3]
-(metric[i1]*T(24)*g[a][b]*Hk[p1][p1][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3])
+metric[i1]*T(12)*g[a][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(18)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p1]
-((metric[i1]*T(3)*kk[p1][p3]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p1][p2])/T(2))
-(metric[i1]*T(3)*kk[p2][p3]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p1][p2])
-(metric[i1]*T(3)*kk[p3][p3]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p1][p2])
+metric[i1]*T(3)*kk[p2][p3]*H[p1][a][i1]*H[p2][b][i1]*Hkk[p3][p1][p2]
+(metric[i1]*T(3)*kk[p3][p3]*H[p1][a][i1]*H[p2][b][i1]*Hkk[p3][p1][p2])/T(2)
-((metric[i1]*T(33)*k[p1][b]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2))
-(metric[i1]*T(6)*k[p3][b]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])
-((metric[i1]*T(3)*k[p1][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(3)*k[p3][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2))
+metric[i1]*T(24)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hkk[p3][p1][p2]
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])
-(metric[i1]*T(12)*k[p1][b]*H[p1][a][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])
+(metric[i1]*T(9)*k[p2][b]*H[p1][a][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term135(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(6)*k[p1][a]*H[p1][b][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])
+(metric[i1]*T(3)*k[p2][a]*H[p1][b][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2)
+metric[i1]*T(18)*g[a][b]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2]
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])
-(metric[i1]*T(24)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])
+metric[i1]*T(3)*kk[p2][p3]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p1][p3]
+(metric[i1]*T(9)*kk[p2][p3]*H[p1][a][i1]*H[p2][b][i1]*Hkk[p3][p1][p3])/T(2)
-(metric[i1]*T(12)*k[p3][b]*H[p1][a][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p3])
-(metric[i1]*T(12)*k[p3][a]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p3])
+metric[i1]*T(12)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p3]
+metric[i1]*T(9)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3]
+(metric[i1]*T(3)*kk[p2][p3]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p2][p1])/T(2)
-(metric[i1]*T(12)*k[p1][b]*H[p1][a][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])
-(metric[i1]*T(6)*k[p1][a]*H[p1][b][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])
-(metric[i1]*T(15)*k[p1][b]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p2])
+metric[i1]*T(12)*k[p2][b]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p2]
-((metric[i1]*T(9)*k[p1][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p2])/T(2))
+metric[i1]*T(12)*k[p2][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p2]
-(metric[i1]*T(3)*k[p1][b]*H[p1][a][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p2])
-(metric[i1]*T(3)*k[p1][a]*H[p1][b][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term136(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(24)*g[a][b]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p2]
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p2])
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p2])
+metric[i1]*T(6)*kk[p1][p1]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p2][p3]
+metric[i1]*T(12)*kk[p1][p3]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p2][p3]
+(metric[i1]*T(21)*kk[p1][p3]*H[p1][a][i1]*H[p2][b][i1]*Hkk[p3][p2][p3])/T(2)
+metric[i1]*T(6)*k[p1][b]*H[p2][a][i1]*Hk[p1][p1][i1]*Hkk[p3][p2][p3]
-(metric[i1]*T(6)*k[p3][a]*H[p2][b][i1]*Hk[p1][p1][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(12)*k[p3][b]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(18)*k[p3][a]*H[p2][b][i1]*Hk[p1][p2][i1]*Hkk[p3][p2][p3])
+(metric[i1]*T(45)*k[p1][b]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(2)
+metric[i1]*T(9)*k[p2][b]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3]
+metric[i1]*T(12)*k[p3][b]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3]
+(metric[i1]*T(15)*k[p1][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(2)
-(metric[i1]*T(15)*k[p2][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])
+metric[i1]*T(3)*k[p3][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3]
+metric[i1]*T(6)*k[p1][b]*H[p1][a][i1]*Hk[p2][p1][i1]*Hkk[p3][p2][p3]
+metric[i1]*T(6)*k[p1][a]*H[p1][b][i1]*Hk[p2][p1][i1]*Hkk[p3][p2][p3]
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p1][i1]*Hk[p2][p1][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(9)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hkk[p3][p2][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term137(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(9)*k[p1][b]*H[p1][a][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(9)*k[p1][a]*H[p1][b][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])
+metric[i1]*T(12)*g[a][b]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3]
-(metric[i1]*T(30)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(36)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(6)*kk[p1][p2]*H[p1][a][i1]*H[p2][b][i1]*Hkk[p3][p3][p1])
+metric[i1]*T(12)*k[p1][b]*H[p1][a][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p1]
+metric[i1]*T(12)*k[p1][a]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p1]
+metric[i1]*T(6)*kk[p1][p2]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p3][p2]
+metric[i1]*T(12)*k[p1][b]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p3][p2]
-(metric[i1]*T(12)*k[p2][b]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p3][p2])
-(metric[i1]*T(6)*k[p3][b]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p3][p2])
-(metric[i1]*T(12)*k[p2][a]*H[p2][b][i1]*Hk[p1][p2][i1]*Hkk[p3][p3][p2])
-(metric[i1]*T(12)*k[p2][b]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p3][p2])
+metric[i1]*T(3)*k[p1][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p3][p2]
+metric[i1]*T(12)*k[p2][a]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p3][p2]
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p1][i1]*Hkk[p3][p3][p2])
+metric[i1]*T(6)*k[p1][b]*H[p1][a][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p2]
+metric[i1]*T(6)*k[p1][a]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p2]
-(metric[i1]*T(24)*g[a][b]*Hk[p1][p1][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term138(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(24)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p2]
+metric[i1]*T(42)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p3][p2]
-(metric[i1]*T(6)*kk[p1][p2]*H[p1][b][i1]*H[p2][a][i1]*Hkk[p3][p3][p3])
+metric[i1]*T(6)*k[p2][b]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p3][p3]
+metric[i1]*T(6)*k[p2][a]*H[p2][b][i1]*Hk[p1][p2][i1]*Hkk[p3][p3][p3]
-(metric[i1]*T(3)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p1][i1]*Hkk[p3][p3][p3])
+metric[i1]*T(6)*k[p1][b]*H[p1][a][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p3]
+metric[i1]*T(6)*k[p1][a]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p3]
-(metric[i1]*T(6)*g[a][b]*Hk[p1][p1][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p3])
-(metric[i1]*T(12)*g[a][b]*Hk[p1][p2][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p3])
+metric[i1]*T(24)*g[a][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term21(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(6)*H[p1][i1][i2]*H[p2][b][i2]*H[p3][a][i1]
-(metric[i1]*metric[i2]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*H[p3][a][i1])
-(metric[i1]*metric[i2]*T(6)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*H[p3][a][i1])
-(metric[i1]*metric[i2]*T(12)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i2]*H[p3][a][i1])
+metric[i1]*metric[i2]*T(6)*kk[p2][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i2]*H[p3][a][i1]
+metric[i1]*metric[i2]*T(6)*kk[p1][p1]*kk[p2][p3]*H[p1][b][i2]*H[p2][i1][i2]*H[p3][a][i1]
+metric[i1]*metric[i2]*T(6)*kk[p1][p2]*kk[p2][p3]*H[p1][b][i2]*H[p2][i1][i2]*H[p3][a][i1]
+metric[i1]*metric[i2]*T(3)*kk[p1][p3]*kk[p2][p3]*H[p1][b][i2]*H[p2][i1][i2]*H[p3][a][i1]
-(metric[i1]*metric[i2]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i1]*H[p3][a][i2])
+metric[i1]*metric[i2]*T(6)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i1]*H[p3][a][i2]
+metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][b][i1]*H[p2][i1][i2]*H[p3][a][i2]
-(metric[i1]*metric[i2]*T(12)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*H[p3][b][i1])
-(metric[i1]*metric[i2]*T(12)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][a][i2]*H[p3][b][i1])
-(metric[i1]*metric[i2]*T(6)*kk[p1][p2]*kk[p2][p3]*H[p1][a][i2]*H[p2][i1][i2]*H[p3][b][i1])
+metric[i1]*metric[i2]*T(3)*kk[p1][p3]*kk[p2][p3]*H[p1][a][i2]*H[p2][i1][i2]*H[p3][b][i1]
+metric[i1]*metric[i2]*T(6)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i1]*H[p3][b][i2]
-(metric[i1]*metric[i2]*T(12)*kk[p2][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][a][i1]*H[p3][b][i2])
+metric[i1]*metric[i2]*T(6)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][a][i1]*H[p3][b][i2]
+(metric[i1]*metric[i2]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p1][b][i2]*H[p2][a][i1]*H[p3][i1][i2])/T(2)
-(metric[i1]*metric[i2]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p1][b][i2]*H[p2][a][i1]*H[p3][i1][i2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term22(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(3)*kk[p1][p2]*kk[p1][p3]*H[p1][b][i1]*H[p2][a][i2]*H[p3][i1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][b][i1]*H[p2][a][i2]*H[p3][i1][i2])/T(2)
-(metric[i1]*metric[i2]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p1][b][i1]*H[p2][a][i2]*H[p3][i1][i2])
-(metric[i1]*metric[i2]*T(6)*kk[p1][p2]*kk[p1][p3]*H[p1][a][i2]*H[p2][b][i1]*H[p3][i1][i2])
+(metric[i1]*metric[i2]*T(3)*kk[p1][p2]*kk[p2][p3]*H[p1][a][i1]*H[p2][b][i2]*H[p3][i1][i2])/T(2)
+metric[i1]*metric[i2]*T(3)*kk[p1][p2]*kk[p3][p3]*H[p1][a][i1]*H[p2][b][i2]*H[p3][i1][i2]
+metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p1][i1]
+metric[i1]*metric[i2]*T(12)*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*H[p3][b][i2]*Hk[p1][p1][i1]
-(metric[i1]*metric[i2]*T(3)*k[p2][b]*kk[p2][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p1][i1])
-(metric[i1]*metric[i2]*T(3)*k[p2][a]*kk[p2][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p1][i1])
+metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i1]*Hk[p1][p1][i2]
-(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i1]*Hk[p1][p1][i2])
+metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p3]*H[p2][i1][i2]*H[p3][b][i1]*Hk[p1][p1][i2]
-(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*H[p3][b][i1]*Hk[p1][p1][i2])
-(metric[i1]*metric[i2]*T(18)*k[p1][b]*kk[p2][p3]*H[p2][a][i1]*H[p3][i1][i2]*Hk[p1][p1][i2])
-(metric[i1]*metric[i2]*T(18)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*H[p3][i1][i2]*Hk[p1][p1][i2])
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p2][i1]
-(metric[i1]*metric[i2]*T(24)*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p2][i1])
+metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p3][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p2][i1]
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p3][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p2][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term23(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p3][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p2][i1])
+metric[i1]*metric[i2]*T(3)*k[p2][a]*kk[p2][p3]*H[p2][i1][i2]*H[p3][b][i2]*Hk[p1][p2][i1]
-(metric[i1]*metric[i2]*T(24)*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*H[p3][b][i2]*Hk[p1][p2][i1])
+metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p3][p3]*H[p2][i1][i2]*H[p3][b][i2]*Hk[p1][p2][i1]
-(metric[i1]*metric[i2]*T(12)*k[p3][a]*kk[p3][p3]*H[p2][i1][i2]*H[p3][b][i2]*Hk[p1][p2][i1])
+metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p1][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p2][i1]
+(metric[i1]*metric[i2]*T(33)*k[p1][b]*kk[p2][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p2][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p2][i1])
-(metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p2][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p2][i1])
+metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p3][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p2][i1]
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p3][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p2][i1])
-(metric[i1]*metric[i2]*T(6)*k[p3][a]*kk[p2][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p2][i1])
+metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p3][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p2][i1]
+metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p3][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p2][i1]
+metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p1][p3]*H[p2][i1][i2]*H[p3][a][i1]*Hk[p1][p2][i2]
+metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i1]*Hk[p1][p2][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][i2]*H[p3][b][i1]*Hk[p1][p2][i2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term24(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p2][p3]*H[p2][a][i1]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(3)*k[p3][b]*kk[p1][p2]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(18)*k[p1][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p3][i1])
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p3][i1]
+metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p3][i1]
-(metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p3][p3]*H[p2][i1][i2]*H[p3][a][i2]*Hk[p1][p3][i1])
+(metric[i1]*metric[i2]*T(3)*k[p3][a]*kk[p1][p2]*H[p2][i1][i2]*H[p3][b][i2]*Hk[p1][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p2][i1][i2]*H[p3][b][i2]*Hk[p1][p3][i1])
+metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*H[p3][b][i2]*Hk[p1][p3][i1]
-((metric[i1]*metric[i2]*T(15)*k[p1][b]*kk[p1][p2]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2))
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p2][p2]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p3][i1])
+metric[i1]*metric[i2]*T(12)*k[p2][b]*kk[p2][p2]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p3][i1]
+metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p2][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p3][i1]
+metric[i1]*metric[i2]*T(18)*k[p2][b]*kk[p2][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p3][i1]
-(metric[i1]*metric[i2]*T(6)*k[p3][b]*kk[p2][p3]*H[p2][a][i2]*H[p3][i1][i2]*Hk[p1][p3][i1])
-((metric[i1]*metric[i2]*T(15)*k[p1][a]*kk[p1][p2]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(3)*k[p3][a]*kk[p1][p2]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term25(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(12)*k[p2][a]*kk[p2][p2]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p3][i1]
+metric[i1]*metric[i2]*T(18)*k[p2][a]*kk[p2][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p3][i1]
-(metric[i1]*metric[i2]*T(3)*k[p3][a]*kk[p2][p3]*H[p2][b][i2]*H[p3][i1][i2]*Hk[p1][p3][i1])
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*H[p2][i1][i2]*H[p3][a][i1]*Hk[p1][p3][i2])/T(2))
+metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p2][p3]*H[p2][i1][i2]*H[p3][a][i1]*Hk[p1][p3][i2]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][i2]*H[p3][b][i1]*Hk[p1][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p1][p2]*H[p2][i1][i2]*H[p3][b][i1]*Hk[p1][p3][i2])
+metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p2][i1][i2]*H[p3][b][i1]*Hk[p1][p3][i2]
+(metric[i1]*metric[i2]*T(3)*k[p3][b]*kk[p1][p2]*H[p2][a][i1]*H[p3][i1][i2]*Hk[p1][p3][i2])/T(2)
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p2][p3]*H[p2][a][i1]*H[p3][i1][i2]*Hk[p1][p3][i2])
+metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p1][p2]*H[p2][b][i1]*H[p3][i1][i2]*Hk[p1][p3][i2]
+(metric[i1]*metric[i2]*T(15)*k[p3][a]*kk[p1][p2]*H[p2][b][i1]*H[p3][i1][i2]*Hk[p1][p3][i2])/T(2)
+metric[i1]*metric[i2]*T(6)*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p1][i1]
+(metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p1][i1])/T(2)
+metric[i1]*metric[i2]*T(6)*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p1][i1]
+(metric[i1]*metric[i2]*T(21)*k[p1][b]*kk[p2][p3]*H[p1][a][i2]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(2)
-((metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p1][b][i2]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(3)*k[p2][a]*kk[p2][p3]*H[p1][b][i2]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(2))
+metric[i1]*metric[i2]*T(36)*g[a][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p1][i2]*Hk[p2][p1][i1]
+metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term26(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p2][b]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(2)
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1]
+metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1]
+(metric[i1]*metric[i2]*T(39)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(15)*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(15)*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(3)*k[p2][b]*kk[p2][p3]*H[p1][a][i1]*H[p3][i1][i2]*Hk[p2][p1][i2])/T(2)
+metric[i1]*metric[i2]*T(6)*k[p3][b]*kk[p2][p3]*H[p1][a][i1]*H[p3][i1][i2]*Hk[p2][p1][i2]
+metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p1][i2]
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p1][i2])/T(2)
+metric[i1]*metric[i2]*T(6)*k[p3][a]*kk[p2][p3]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p1][i2]
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p1][i1]*Hk[p2][p1][i2]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2])
-(metric[i1]*metric[i2]*T(9)*g[a][b]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2])
+(metric[i1]*metric[i2]*T(21)*k[p1][a]*k[p2][b]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(2)
+metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2]
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p2][i1])
-(metric[i1]*metric[i2]*T(3)*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term27(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p3][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p2][i1]
+metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p3][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p2][i1]
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p2][i1])
-(metric[i1]*metric[i2]*T(3)*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p2][i1])
-(metric[i1]*metric[i2]*T(12)*k[p3][a]*kk[p3][p3]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p2][i1])
+metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p1][p3]*H[p1][a][i2]*H[p3][i1][i2]*Hk[p2][p2][i1]
-(metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p3][p3]*H[p1][a][i2]*H[p3][i1][i2]*Hk[p2][p2][i1])
-(metric[i1]*metric[i2]*T(12)*k[p1][a]*kk[p1][p3]*H[p1][b][i2]*H[p3][i1][i2]*Hk[p2][p2][i1])
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p3][p3]*H[p1][b][i2]*H[p3][i1][i2]*Hk[p2][p2][i1])
+metric[i1]*metric[i2]*T(18)*k[p1][a]*k[p1][b]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p2][i1]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p1]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p2][i1])
+metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p1][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p2][i2]
+(metric[i1]*metric[i2]*T(39)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p2][i2])/T(2)
+metric[i1]*metric[i2]*T(24)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p2][i2]
+metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p3][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p2][i2]
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p3][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p2][i2])
-(metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p3][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p2][i2])
+(metric[i1]*metric[i2]*T(15)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p2][i2])/T(2)
+metric[i1]*metric[i2]*T(12)*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p2][i2]
+metric[i1]*metric[i2]*T(12)*k[p1][a]*kk[p3][p3]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p2][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term28(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(12)*k[p2][a]*kk[p3][p3]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p2][i2])
+metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*H[p3][i1][i2]*Hk[p2][p2][i2]
+metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p3][p3]*H[p1][a][i1]*H[p3][i1][i2]*Hk[p2][p2][i2]
+metric[i1]*metric[i2]*T(12)*k[p1][a]*kk[p1][p3]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p2][i2]
+metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p2][i2]
+metric[i1]*metric[i2]*T(12)*k[p1][a]*kk[p3][p3]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p2][i2]
-(metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p1][i1]*Hk[p2][p2][i2])
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p1][i1]*Hk[p2][p2][i2])
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p3][b]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p2][i2]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p2][i2])
+metric[i1]*metric[i2]*T(6)*k[p1][a]*k[p1][b]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p2][i2]
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*k[p2][a]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p2][i2])
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*k[p3][a]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p2][i2])
-(metric[i1]*metric[i2]*T(12)*k[p2][b]*k[p3][a]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p2][i2])
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p3][b]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p2][i2]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p2][i2])
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p2][i2]
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p3][i1])/T(2))
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p2]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p3][i1]
+metric[i1]*metric[i2]*T(3)*k[p3][b]*kk[p2][p2]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p3][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term29(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(15)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p3][i1])/T(2))
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p3][i1])
+metric[i1]*metric[i2]*T(6)*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p3][i1]
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p3][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p3][i1]
+metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p3][p3]*H[p1][i1][i2]*H[p3][a][i2]*Hk[p2][p3][i1]
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p3][i1])/T(2))
+metric[i1]*metric[i2]*T(3)*k[p3][a]*kk[p2][p2]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p3][i1]
+(metric[i1]*metric[i2]*T(15)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(15)*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p3][i1])
+metric[i1]*metric[i2]*T(6)*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i2]*Hk[p2][p3][i1]
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p1][p2]*H[p1][a][i2]*H[p3][i1][i2]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(6)*k[p3][b]*kk[p1][p2]*H[p1][a][i2]*H[p3][i1][i2]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p2][p3]*H[p1][a][i2]*H[p3][i1][i2]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p3]*H[p1][b][i2]*H[p3][i1][i2]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p1][b]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])
+(metric[i1]*metric[i2]*T(21)*k[p1][a]*k[p2][b]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(3)*k[p1][b]*k[p3][a]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p1][p2]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term210(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(3)*k[p2][b]*kk[p1][p2]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(15)*k[p3][b]*kk[p1][p2]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p2][p2]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p2]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(18)*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p3][p3]*H[p1][i1][i2]*H[p3][a][i1]*Hk[p2][p3][i2])
+(metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p1][p2]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(15)*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p2]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p3][i2])
+metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*H[p3][b][i1]*Hk[p2][p3][i2]
-(metric[i1]*metric[i2]*T(15)*k[p2][b]*kk[p1][p2]*H[p1][a][i1]*H[p3][i1][i2]*Hk[p2][p3][i2])
+metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p2][p2]*H[p1][a][i1]*H[p3][i1][i2]*Hk[p2][p3][i2]
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*H[p3][i1][i2]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p1][p2]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p3][i2])
+metric[i1]*metric[i2]*T(3)*k[p3][a]*kk[p1][p2]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p3][i2]
+metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p2]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p3][i2]
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*H[p3][i1][i2]*Hk[p2][p3][i2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term211(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p3][i1][i2]*Hk[p1][p1][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p1][b]*k[p2][a]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(15)*k[p1][a]*k[p2][b]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(18)*k[p2][b]*k[p3][a]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
-((metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p3][b]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*k[p3][b]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
+metric[i1]*metric[i2]*T(12)*k[p3][a]*k[p3][b]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2]
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2]
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2]
-(metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p2]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
+metric[i1]*metric[i2]*T(30)*g[a][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2]
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p2][b]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p3][i2]
-(metric[i1]*metric[i2]*T(12)*k[p2][b]*k[p3][a]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(12)*k[p3][a]*k[p3][b]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p3][i2])
+metric[i1]*metric[i2]*T(12)*k[p3][a]*k[p3][b]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p3][i2]
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p1][i1]
+metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p1][i1]
+metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p2][p3]*H[p1][a][i2]*H[p2][i1][i2]*Hk[p3][p1][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term212(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p3]*H[p1][b][i2]*H[p2][i1][i2]*Hk[p3][p1][i1]
+(metric[i1]*metric[i2]*T(3)*k[p2][a]*kk[p2][p3]*H[p1][b][i2]*H[p2][i1][i2]*Hk[p3][p1][i1])/T(2)
-(metric[i1]*metric[i2]*T(30)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p1][i2]*Hk[p3][p1][i1])
-(metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p1][i1])
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])
-(metric[i1]*metric[i2]*T(15)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p1][i1])
-(metric[i1]*metric[i2]*T(36)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p1][i1])
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p1][i1])
+(metric[i1]*metric[i2]*T(33)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p1][i1])/T(2)
+metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p1][i1]
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p1][i1]
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i1]*Hk[p3][p1][i2])
-(metric[i1]*metric[i2]*T(12)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p1][i2])
-((metric[i1]*metric[i2]*T(3)*k[p2][b]*kk[p2][p3]*H[p1][a][i1]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(2))
+metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p1][i2]
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(2))
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p1][i2])
-(metric[i1]*metric[i2]*T(9)*g[a][b]*kk[p3][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p1][i2])
+metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term213(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2]
+metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p1][i2]
-((metric[i1]*metric[i2]*T(33)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p1][i2])/T(2))
+metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p1][i2]
+(metric[i1]*metric[i2]*T(21)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p2][i1]
+metric[i1]*metric[i2]*T(6)*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p2][i1]
-((metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p3][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*metric[i2]*T(3)*k[p2][b]*kk[p3][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p2][i1])
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p2][i1]
-((metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*metric[i2]*T(3)*k[p2][a]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p1][p3]*H[p1][a][i2]*H[p2][i1][i2]*Hk[p3][p2][i1]
-(metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p3][p3]*H[p1][a][i2]*H[p2][i1][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(12)*k[p1][a]*kk[p1][p3]*H[p1][b][i2]*H[p2][i1][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p3][p3]*H[p1][b][i2]*H[p2][i1][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p3][p3]*H[p2][i1][i2]*Hk[p1][p1][i2]*Hk[p3][p2][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term214(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(12)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p2][i1]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p1]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(21)*g[a][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(9)*g[a][b]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p2][i1]
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p2][i1]
-(metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p2][i1]
+metric[i1]*metric[i2]*T(12)*k[p1][a]*k[p1][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
+metric[i1]*metric[i2]*T(3)*k[p1][b]*k[p2][a]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(2)
-(metric[i1]*metric[i2]*T(18)*k[p2][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(15)*k[p2][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
+metric[i1]*metric[i2]*T(6)*k[p3][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i1]*Hk[p3][p2][i2])
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i1]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p3][p3]*H[p1][i1][i2]*H[p2][a][i1]*Hk[p3][p2][i2]
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p1][p3]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p2][i2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term215(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(6)*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p2][i2])
+metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(15)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*H[p2][i1][i2]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p3][p3]*H[p1][a][i1]*H[p2][i1][i2]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(12)*k[p1][a]*kk[p1][p3]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(15)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p3][p3]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p2][i2]
-(metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p1][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(18)*g[a][b]*kk[p3][p3]*H[p2][i1][i2]*Hk[p1][p1][i1]*Hk[p3][p2][i2])
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p3][b]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p1][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2]
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])/T(2))
-(metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*k[p3][a]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])
-((metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p3][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])/T(2))
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p3][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2]
-(metric[i1]*metric[i2]*T(18)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])
+metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p3][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p2][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term216(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*k[p1][b]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])
+(metric[i1]*metric[i2]*T(3)*k[p1][b]*k[p2][a]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p3][a]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(2))
-(metric[i1]*metric[i2]*T(3)*k[p2][b]*k[p3][a]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])
+metric[i1]*metric[i2]*T(12)*k[p3][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2]
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p1][p2]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p1][p2]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p3][i1]
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p2][p2]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p3][i1])
+(metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p1][p2]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1]
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p1][p2]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(12)*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term217(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p2]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(12)*k[p2][a]*kk[p2][p2]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1]
+(metric[i1]*metric[i2]*T(15)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1])/T(2)
+metric[i1]*metric[i2]*T(3)*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1]
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*kk[p1][p2]*H[p1][a][i2]*H[p2][i1][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p1][p2]*H[p1][a][i2]*H[p2][i1][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(3)*k[p1][b]*kk[p2][p3]*H[p1][a][i2]*H[p2][i1][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(3)*k[p1][a]*kk[p2][p3]*H[p1][b][i2]*H[p2][i1][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(12)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p1][i2]*Hk[p3][p3][i1]
-(metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p1][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(12)*k[p1][a]*k[p1][b]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p3][i1])
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(6)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(30)*g[a][b]*kk[p2][p2]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p3][i1]
-(metric[i1]*metric[i2]*T(9)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*k[p2][a]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(6)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1]
-(metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term218(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*k[p3][a]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*k[p3][a]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(12)*k[p1][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(12)*k[p3][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(18)*g[a][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1]
+(metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2)
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(6)*k[p2][b]*k[p3][a]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(6)*k[p2][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(15)*g[a][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*T(12)*k[p3][b]*kk[p2][p2]*H[p1][i1][i2]*H[p2][a][i1]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(18)*k[p1][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i1]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(18)*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(3)*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(18)*k[p3][a]*kk[p2][p2]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p3][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term219(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(12)*k[p1][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p2][b]*kk[p1][p2]*H[p1][a][i1]*H[p2][i1][i2]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p2][p2]*H[p1][a][i1]*H[p2][i1][i2]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p1][b]*kk[p2][p3]*H[p1][a][i1]*H[p2][i1][i2]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*kk[p1][p2]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p2]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p1][a]*kk[p2][p3]*H[p1][b][i1]*H[p2][i1][i2]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p1][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p2][i1][i2]*Hk[p1][p1][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(18)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p1][i1]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(12)*k[p1][b]*k[p2][a]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(6)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(12)*k[p1][b]*k[p3][a]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(18)*k[p2][b]*k[p3][a]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(6)*k[p2][a]*k[p3][b]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p1][p1]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term220(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(9)*g[a][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2]
+(metric[i1]*metric[i2]*T(15)*k[p1][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(2)
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p2][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(12)*k[p2][b]*k[p3][a]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(12)*k[p3][a]*k[p3][b]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(18)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(48)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(6)*k[p1][b]*k[p3][a]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(12)*k[p2][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(6)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p3][i2]
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p3][i2])
+(metric[i1]*metric[i2]*T(3)*k[p1][a]*k[p2][b]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2)
-(metric[i1]*metric[i2]*T(18)*k[p2][b]*k[p3][a]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(6)*k[p2][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(12)*k[p3][a]*k[p3][b]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_GB_GGGG_Term221(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(33)*g[a][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(12)*g[a][b]*kk[p2][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])
+metric[i1]*metric[i2]*T(24)*g[a][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2]
;
      return theentry;
}

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CurrentRule_GB_GGGG(const std::vector<momentumD<T, D> const*>& momlist,
                                      const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {

    // incoming momenta
    std::vector<momD<T, D>> k;
    k.emplace_back(-(*(momlist[0])));
    k.emplace_back(-(*(momlist[1])));
    k.emplace_back(-(*(momlist[2])));

    // the input gravitons
    std::vector<Tensor<T, D, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[0]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[2]->get_current()));

    // metric
    Tensor<T, D, 2> hout(0), g(0);
    const std::vector<T>& metric(hout.get_metric_extended());
    for (size_t ii = 0; ii < D; ii++) { g[ii][ii] = T(1)/metric[ii]; }

    // tensor contractions
    std::vector<std::vector<Tensor<T, D, 1>>> Hk;
    for (size_t p1 = 0; p1 < 3; p1++) {
        std::vector<Tensor<T, D, 1>> fill_Hk;
        for (size_t p2 = 0; p2 < 3; p2++) { fill_Hk.emplace_back(index_contraction<0>(k[p2], H[p1])); }
        Hk.emplace_back(fill_Hk);
    }

    // tensor contractions
    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array3D Hkk(3, array2D(3, array1D(3, T(0))));
    array3D HHH(3, array2D(3, array1D(3, T(0))));
    array2D HH(3, array1D(3, T(0)));
    array2D kk(3, array1D(3, T(0)));
    array1D trH(3, T(0));

    for (size_t p1 = 0; p1 < 3; p1++) {
        for (size_t ii = 0; ii < D; ii++) { trH[p1] += H[p1][ii][ii] * metric[ii]; }

        for (size_t p2 = 0; p2 < 3; p2++) {
            for (size_t ii = 0; ii < D; ii++) {
                kk[p1][p2] += k[p1][ii] * k[p2][ii] * metric[ii];
                for (size_t jj = 0; jj < D; jj++) { HH[p1][p2] += H[p1][ii][jj] * H[p2][ii][jj] * metric[ii] * metric[jj]; }
            }
            for (size_t p3 = 0; p3 < 3; p3++) {
                for (size_t ii = 0; ii < D; ii++) {
                    Hkk[p1][p2][p3] += Hk[p1][p2][ii] * k[p3][ii] * metric[ii];

                    for (size_t jj = 0; jj < D; jj++) {
                        for (size_t kk = 0; kk < D; kk++) {
                            HHH[p1][p2][p3] += H[p1][ii][jj] * H[p2][jj][kk] * H[p3][kk][ii] * metric[ii] * metric[jj] * metric[kk];
                        }
                    }
                }
            }
        }
    }

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 1, 0}, {2, 0, 1}};

    for (size_t a = 0; a < D; a++) {
        for (size_t b = 0; b < D; b++) {

            T theentry(0);

            for (size_t p = 0; p < 6; p++) {
                size_t p1 = perm[p][0];
                size_t p2 = perm[p][1];
                size_t p3 = perm[p][2];

                for (size_t i1 = 0; i1 < D; i1++) {

                theentry += CurrentRule_GB_GGGG_Term11<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term12<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term13<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term14<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term15<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term16<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term17<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term18<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term19<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term110<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);

				theentry += CurrentRule_GB_GGGG_Term111<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term112<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term113<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term114<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term115<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term116<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term117<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term118<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term119<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term120<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);

				theentry += CurrentRule_GB_GGGG_Term121<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term122<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term123<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term124<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term125<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term126<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term127<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term128<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term129<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term130<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);

				theentry += CurrentRule_GB_GGGG_Term131<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term132<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term133<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term134<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term135<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term136<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term137<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term138<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);


                }
                for (size_t i1 = 0; i1 < D; i1++) {
                    for (size_t i2 = 0; i2 < D; i2++) {

                theentry += CurrentRule_GB_GGGG_Term21<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term22<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term23<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term24<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term25<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term26<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term27<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term28<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term29<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term210<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);

				theentry += CurrentRule_GB_GGGG_Term211<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term212<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term213<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term214<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term215<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term216<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term217<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term218<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term219<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term220<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);

				theentry += CurrentRule_GB_GGGG_Term221<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);

                 }
                }


                theentry += CurrentRule_GB_GGGG_Term01<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term02<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term03<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term04<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term05<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term06<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term07<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term08<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term09<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term010<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);

				theentry += CurrentRule_GB_GGGG_Term011<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term012<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term013<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term014<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term015<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term016<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term017<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term018<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term019<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term020<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);

				theentry += CurrentRule_GB_GGGG_Term021<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term022<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term023<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term024<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term025<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term026<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term027<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term028<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term029<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term030<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);

				theentry += CurrentRule_GB_GGGG_Term031<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term032<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term033<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term034<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term035<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term036<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term037<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_GB_GGGG_Term038<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);





            }

            hout[a][b] = theentry;
        }
    }

    // factor of I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
    // hout*= T(0,1);
    // hout *= T(-1) / T(6);
    // Lagrangian =  - 2 sqrt(g) RicciScalar
    // hout *= T(1) / T(6) ;
    hout *= T(1) / T(3);

    return v_t_to_v_extended_t<T, Tcur>(hout.vectorize());
}
/* end Gauss Bonnet */

/* start R3 */

template <typename T, size_t D>
   T CurrentRule_R3_GGG_Term01(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
-(pow2(kk[p1][p2])*T(6)*Hk[p1][p2][b]*Hk[p2][p1][a])
-(T(6)*kk[p1][p1]*kk[p1][p2]*Hk[p1][p2][b]*Hk[p2][p1][a])
-(T(6)*kk[p1][p1]*kk[p1][p2]*Hk[p1][p2][a]*Hk[p2][p1][b])
-(T(6)*kk[p1][p1]*kk[p2][p2]*Hk[p1][p2][a]*Hk[p2][p1][b])
+T(3)*k[p1][b]*kk[p1][p2]*Hk[p2][p1][a]*Hkk[p1][p1][p2]
+T(3)*k[p1][a]*kk[p1][p2]*Hk[p2][p1][b]*Hkk[p1][p1][p2]
+T(6)*k[p1][a]*kk[p2][p2]*Hk[p2][p1][b]*Hkk[p1][p1][p2]
+T(3)*k[p1][b]*kk[p1][p2]*Hk[p2][p1][a]*Hkk[p1][p2][p1]
+T(3)*k[p1][a]*kk[p1][p2]*Hk[p2][p1][b]*Hkk[p1][p2][p1]
+T(6)*k[p1][b]*kk[p1][p2]*Hk[p2][p1][a]*Hkk[p1][p2][p2]
+T(6)*k[p1][b]*kk[p2][p2]*Hk[p2][p1][a]*Hkk[p1][p2][p2]
+T(6)*k[p1][a]*kk[p2][p2]*Hk[p2][p1][b]*Hkk[p1][p2][p2]
+T(6)*k[p2][a]*kk[p1][p2]*Hk[p1][p2][b]*Hkk[p2][p1][p1]
-(T(6)*k[p1][b]*k[p2][a]*Hkk[p1][p2][p2]*Hkk[p2][p1][p1])
+T(3)*k[p2][b]*kk[p1][p1]*Hk[p1][p2][a]*Hkk[p2][p1][p2]
-(T(3)*k[p1][a]*k[p2][b]*Hkk[p1][p1][p2]*Hkk[p2][p1][p2])
-(T(3)*k[p1][b]*k[p2][a]*Hkk[p1][p2][p2]*Hkk[p2][p1][p2])
-(T(3)*k[p1][a]*k[p2][b]*Hkk[p1][p2][p2]*Hkk[p2][p1][p2])
+T(3)*k[p2][b]*kk[p1][p1]*Hk[p1][p2][a]*Hkk[p2][p2][p1]
-(T(3)*k[p1][a]*k[p2][b]*Hkk[p1][p1][p2]*Hkk[p2][p2][p1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGG_Term02(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
-(T(3)*k[p1][b]*k[p2][a]*Hkk[p1][p2][p2]*Hkk[p2][p2][p1])
-(T(3)*k[p1][a]*k[p2][b]*Hkk[p1][p2][p2]*Hkk[p2][p2][p1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGG_Term11(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2){
        T theentry(0);
        theentry =
   
metric[i1]*pow2(kk[p1][p2])*T(6)*kk[p1][p1]*H[p1][i1][b]*H[p2][i1][a]
+metric[i1]*T(6)*pow3(kk[p1][p2])*H[p1][i1][b]*H[p2][i1][a]
+metric[i1]*pow2(kk[p1][p2])*T(6)*kk[p1][p1]*H[p1][i1][a]*H[p2][i1][b]
+metric[i1]*T(6)*kk[p1][p1]*kk[p1][p2]*kk[p2][p2]*H[p1][i1][a]*H[p2][i1][b]
-(metric[i1]*pow2(kk[p1][p2])*T(6)*k[p1][b]*H[p2][i1][a]*Hk[p1][p1][i1])
-(metric[i1]*pow2(kk[p1][p2])*T(6)*k[p1][a]*H[p2][i1][b]*Hk[p1][p1][i1])
-(metric[i1]*T(6)*k[p1][a]*kk[p1][p2]*kk[p2][p2]*H[p2][i1][b]*Hk[p1][p1][i1])
-(metric[i1]*pow2(kk[p1][p2])*T(6)*k[p1][b]*H[p2][i1][a]*Hk[p1][p2][i1])
-(metric[i1]*T(6)*k[p1][b]*kk[p1][p2]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p2][i1])
-(metric[i1]*T(6)*k[p1][a]*kk[p1][p2]*kk[p2][p2]*H[p2][i1][b]*Hk[p1][p2][i1])
-(metric[i1]*pow2(kk[p1][p2])*T(6)*k[p2][a]*H[p1][i1][b]*Hk[p2][p1][i1])
+metric[i1]*T(6)*k[p1][b]*k[p2][a]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p1][i1]
-(metric[i1]*T(6)*k[p2][b]*kk[p1][p1]*kk[p1][p2]*H[p1][i1][a]*Hk[p2][p2][i1])
+metric[i1]*T(6)*k[p1][a]*k[p2][b]*kk[p1][p2]*Hk[p1][p1][i1]*Hk[p2][p2][i1]
+metric[i1]*T(6)*k[p1][b]*k[p2][a]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p2][i1]
+metric[i1]*T(6)*k[p1][a]*k[p2][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p2][i1]
;
      return theentry;
}

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CurrentRule_R3_GGG(const std::vector<momentumD<T, D> const*>& momlist,
                                     const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {
    // incoming momenta
    std::vector<momD<T, D>> k;
    k.emplace_back(-(*(momlist[0])));
    k.emplace_back(-(*(momlist[1])));

    // the input gravitons
    std::vector<Tensor<T, D, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[0]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[1]->get_current()));

    // metric
    Tensor<T, D, 2> hout(0), g(0);
    const std::vector<T>& metric(hout.get_metric_extended());

    for (size_t ii = 0; ii < D; ii++) { g[ii][ii] = T(1)/metric[ii]; }

    // tensors contractions
    std::vector<std::vector<Tensor<T, D, 1>>> Hk;
    for (size_t p1 = 0; p1 < 2; p1++) {
        std::vector<Tensor<T, D, 1>> fill_Hk;
        for (size_t p2 = 0; p2 < 2; p2++) { fill_Hk.emplace_back(index_contraction<0>(k[p2], H[p1])); }
        Hk.emplace_back(fill_Hk);
    }

    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array3D Hkk(2, array2D(2, array1D(2, T(0))));
    array2D HH(2, array1D(2, T(0)));
    array2D kk(2, array1D(2, T(0)));
    array1D trH(2, T(0));

    for (size_t p1 = 0; p1 < 2; p1++) {

        for (size_t ii = 0; ii < D; ii++) { 
				trH[p1] += H[p1][ii][ii] * metric[ii]; 
		}

        for (size_t p2 = 0; p2 < 2; p2++) {
            for (size_t ii = 0; ii < D; ii++) {
                kk[p1][p2] += k[p1][ii] * k[p2][ii] * metric[ii];
                for (size_t jj = 0; jj < D; jj++) { HH[p1][p2] += H[p1][ii][jj] * H[p2][ii][jj] * metric[ii] * metric[jj]; }
            }
            for (size_t p3 = 0; p3 < 2; p3++) {
                for (size_t ii = 0; ii < D; ii++) { Hkk[p1][p2][p3] += Hk[p1][p2][ii] * k[p3][ii] * metric[ii]; }
            }
        }
    }

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1}, {1, 0}};

    for (size_t a = 0; a < D; a++) {
        for (size_t b = 0; b < D; b++) {

            T theentry(0);

            for (size_t ip = 0; ip < 2; ip++) {
                size_t p1 = perm[ip][0];
                size_t p2 = perm[ip][1];

                for (size_t i1 = 0; i1 < D; i1++) {
            	 theentry += CurrentRule_R3_GGG_Term11<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2);
                        ;
                }

                theentry += CurrentRule_R3_GGG_Term01<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);
                theentry += CurrentRule_R3_GGG_Term02<T,D>(Hkk, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2);

                    ;
            }
            hout[a][b] = theentry;
        }
    }

    // factor of I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
    // hout*= T(0,1);
    // hout *= T(1) / T(2);

    return v_t_to_v_extended_t<T, Tcur>(hout.vectorize());
}

// begin terms

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term01(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((trH[p3]*T(9)*HH[p1][p2]*k[p1][b]*k[p2][a]*kk[p1][p3]*kk[p2][p3])/T(4))
+(trH[p3]*T(9)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p2][p3])/T(4)
+pow2(kk[p2][p3])*T(9)*k[p1][a]*k[p2][b]*HHH[p1][p2][p3]
-((T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*HHH[p1][p2][p3])/T(2))
+(T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*HHH[p1][p2][p3])/T(2)
-((T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*kk[p2][p3]*HHH[p1][p2][p3])/T(2))
+(T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*kk[p2][p3]*HHH[p1][p2][p3])/T(4)
-((T(27)*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p2][p3]*HHH[p1][p2][p3])/T(2))
-((T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*kk[p2][p3]*HHH[p1][p2][p3])/T(4))
-((T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*kk[p2][p3]*HHH[p1][p2][p3])/T(4))
+(T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*kk[p3][p3]*HHH[p1][p2][p3])/T(2)
+(T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*kk[p3][p3]*HHH[p1][p2][p3])/T(2)
+(T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p3][p3]*HHH[p1][p2][p3])/T(2)
+T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*kk[p3][p3]*HHH[p1][p2][p3]
+pow2(kk[p2][p3])*T(9)*k[p1][b]*k[p3][a]*HHH[p1][p3][p2]
+(T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*HHH[p1][p3][p2])/T(2)
+(T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*kk[p2][p3]*HHH[p1][p3][p2])/T(2)
-((T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*kk[p2][p3]*HHH[p1][p3][p2])/T(4))
+(T(9)*k[p2][b]*k[p3][a]*kk[p1][p3]*kk[p2][p3]*HHH[p1][p3][p2])/T(2)
-((T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*kk[p2][p3]*HHH[p1][p3][p2])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term02(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*kk[p3][p3]*HHH[p1][p3][p2])/T(2))
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*kk[p3][p3]*HHH[p1][p3][p2])/T(2))
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p3][p3]*HHH[p1][p3][p2])/T(2))
+(T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*kk[p1][p3]*HHH[p2][p1][p3])/T(2)
+(T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*kk[p1][p3]*HHH[p2][p1][p3])/T(2)
+(T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*HHH[p2][p1][p3])/T(4)
-((T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*HHH[p2][p1][p3])/T(2))
+(T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*kk[p2][p3]*HHH[p2][p1][p3])/T(4)
+(T(27)*k[p1][b]*k[p2][a]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p1][p3])/T(4)
+(T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p1][p3])/T(4)
+(T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p1][p3])/T(2)
+(T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p1][p3])/T(2)
+T(3)*g[a][b]*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p1][p3]
-((T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*kk[p3][p3]*HHH[p2][p1][p3])/T(2))
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*kk[p3][p3]*HHH[p2][p1][p3])/T(2))
+T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p3][p3]*HHH[p2][p1][p3]
-((T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*kk[p3][p3]*HHH[p2][p1][p3])/T(4))
+(T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*kk[p3][p3]*HHH[p2][p1][p3])/T(4)
-((T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*kk[p1][p3]*HHH[p2][p3][p1])/T(2))
-((T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*kk[p1][p3]*HHH[p2][p3][p1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term03(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*kk[p2][p3]*HHH[p2][p3][p1])/T(2)
-((T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*HHH[p2][p3][p1])/T(4))
-((T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*kk[p2][p3]*HHH[p2][p3][p1])/T(4))
+(T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p3][p1])/T(4)
-(T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p3][p1])
-((T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*kk[p2][p3]*HHH[p2][p3][p1])/T(2))
+(T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*kk[p3][p3]*HHH[p2][p3][p1])/T(2)
+(T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*kk[p3][p3]*HHH[p2][p3][p1])/T(2)
+(T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*kk[p3][p3]*HHH[p2][p3][p1])/T(4)
-((T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*kk[p3][p3]*HHH[p2][p3][p1])/T(4))
-((T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*kk[p2][p3]*HHH[p3][p1][p2])/T(2))
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*kk[p2][p3]*HHH[p3][p1][p2])/T(2))
+(T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*HHH[p3][p1][p2])/T(4)
+(T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*kk[p2][p3]*HHH[p3][p1][p2])/T(4)
+(T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*kk[p2][p3]*HHH[p3][p1][p2])/T(4)
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p2][p3]*HHH[p3][p1][p2])/T(4))
-((T(9)*k[p2][b]*k[p3][a]*kk[p1][p3]*kk[p2][p3]*HHH[p3][p1][p2])/T(2))
-((T(9)*k[p1][b]*k[p3][a]*kk[p2][p2]*kk[p2][p3]*HHH[p3][p1][p2])/T(4))
-((T(9)*k[p1][a]*k[p3][b]*kk[p2][p2]*kk[p2][p3]*HHH[p3][p1][p2])/T(4))
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*kk[p3][p3]*HHH[p3][p1][p2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term04(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*kk[p2][p3]*HHH[p3][p2][p1])/T(2))
-((T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*HHH[p3][p2][p1])/T(4))
-((T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*kk[p2][p3]*HHH[p3][p2][p1])/T(4))
+(T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*kk[p2][p3]*HHH[p3][p2][p1])/T(4)
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*kk[p2][p3]*HHH[p3][p2][p1])/T(4))
+(T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*kk[p2][p3]*HHH[p3][p2][p1])/T(2)
+(T(9)*k[p1][b]*k[p3][a]*kk[p2][p2]*kk[p2][p3]*HHH[p3][p2][p1])/T(4)
+(T(9)*k[p1][a]*k[p3][b]*kk[p2][p2]*kk[p2][p3]*HHH[p3][p2][p1])/T(4)
-((T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*kk[p3][p3]*HHH[p3][p2][p1])/T(2))
-(T(9)*HH[p1][p3]*k[p1][b]*kk[p1][p2]*kk[p2][p3]*Hk[p2][p1][a])
-(T(9)*HH[p1][p3]*k[p1][b]*kk[p2][p2]*kk[p2][p3]*Hk[p2][p1][a])
-(pow2(kk[p2][p3])*T(9)*HH[p1][p3]*k[p3][a]*Hk[p2][p1][b])
-(T(9)*HH[p1][p3]*k[p1][a]*kk[p1][p2]*kk[p2][p3]*Hk[p2][p1][b])
+pow2(kk[p1][p2])*T(9)*HH[p1][p3]*k[p1][b]*Hk[p2][p3][a]
+T(9)*HH[p1][p3]*k[p1][b]*kk[p1][p2]*kk[p2][p2]*Hk[p2][p3][a]
+pow2(kk[p1][p2])*T(9)*HH[p1][p3]*k[p1][a]*Hk[p2][p3][b]
+T(9)*HH[p1][p3]*k[p3][a]*kk[p1][p2]*kk[p2][p3]*Hk[p2][p3][b]
-(pow2(kk[p2][p3])*T(9)*HH[p1][p2]*k[p2][b]*Hk[p3][p1][a])
+T(9)*HH[p1][p2]*k[p1][b]*kk[p2][p3]*kk[p3][p3]*Hk[p3][p1][a]
-((trH[p2]*T(9)*kk[p1][p2]*kk[p2][p3]*Hk[p1][p3][b]*Hk[p3][p1][a])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term05(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(trH[p2]*T(9)*kk[p1][p2]*kk[p3][p3]*Hk[p1][p3][b]*Hk[p3][p1][a])
-((trH[p2]*T(9)*kk[p1][p2]*kk[p2][p3]*Hk[p1][p3][a]*Hk[p3][p1][b])/T(2))
-(trH[p2]*T(9)*kk[p1][p2]*kk[p3][p3]*Hk[p1][p3][a]*Hk[p3][p1][b])
+T(9)*HH[p1][p2]*k[p2][b]*kk[p1][p3]*kk[p2][p3]*Hk[p3][p2][a]
-(T(9)*HH[p1][p2]*k[p1][b]*kk[p1][p3]*kk[p3][p3]*Hk[p3][p2][a])
-(trH[p1]*T(9)*kk[p1][p2]*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p2][a])
-((trH[p1]*T(9)*kk[p2][p2]*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p2][a])/T(2))
-((trH[p1]*T(9)*kk[p2][p3]*kk[p3][p3]*Hk[p2][p3][b]*Hk[p3][p2][a])/T(2))
-(pow2(kk[p2][p3])*trH[p1]*T(9)*Hk[p2][p3][a]*Hk[p3][p2][b])
-(trH[p1]*T(9)*kk[p1][p2]*kk[p2][p3]*Hk[p2][p3][a]*Hk[p3][p2][b])
-((trH[p1]*T(9)*kk[p2][p2]*kk[p2][p3]*Hk[p2][p3][a]*Hk[p3][p2][b])/T(2))
-(trH[p1]*T(9)*kk[p2][p2]*kk[p3][p3]*Hk[p2][p3][a]*Hk[p3][p2][b])
-((trH[p1]*T(9)*kk[p2][p3]*kk[p3][p3]*Hk[p2][p3][a]*Hk[p3][p2][b])/T(2))
+T(9)*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p1][p2]
+T(9)*kk[p3][p3]*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p1][p2]
+T(9)*kk[p2][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p1][p2]
+T(9)*kk[p3][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p1][p2]
-((T(9)*kk[p3][p3]*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p1][p3])/T(2))
-((T(9)*kk[p1][p2]*Hk[p2][p3][b]*Hk[p3][p1][a]*Hkk[p1][p2][p1])/T(4))
-((T(9)*kk[p1][p2]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p1])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term06(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(9)*HH[p2][p3]*k[p1][a]*k[p2][b]*kk[p1][p3]*Hkk[p1][p2][p2]
-((T(9)*kk[p1][p2]*Hk[p2][p3][b]*Hk[p3][p1][a]*Hkk[p1][p2][p2])/T(2))
-((T(9)*kk[p2][p2]*Hk[p2][p3][b]*Hk[p3][p1][a]*Hkk[p1][p2][p2])/T(4))
-((T(27)*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p1][a]*Hkk[p1][p2][p2])/T(4))
-((T(9)*kk[p2][p2]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p2])/T(4))
+(T(9)*kk[p2][p3]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p2])/T(4)
+T(9)*kk[p3][p3]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p2]
+T(9)*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p2][p2]
+T(9)*kk[p2][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p2][p2]
+T(18)*kk[p3][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p2][p2]
+(T(9)*HH[p2][p3]*k[p1][b]*k[p2][a]*kk[p1][p2]*Hkk[p1][p2][p3])/T(2)
-(T(9)*HH[p2][p3]*k[p1][a]*k[p2][b]*kk[p1][p2]*Hkk[p1][p2][p3])
+(T(27)*HH[p2][p3]*k[p1][a]*k[p3][b]*kk[p1][p3]*Hkk[p1][p2][p3])/T(2)
+(trH[p3]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p2][p1][a]*Hkk[p1][p2][p3])/T(4)
+(trH[p3]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p2][p1][b]*Hkk[p1][p2][p3])/T(4)
+(trH[p2]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p3][p1][a]*Hkk[p1][p2][p3])/T(4)
+(trH[p2]*T(9)*k[p1][b]*kk[p3][p3]*Hk[p3][p1][a]*Hkk[p1][p2][p3])/T(2)
-(T(9)*kk[p1][p2]*Hk[p2][p1][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3])
-((T(9)*kk[p2][p3]*Hk[p2][p1][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3])/T(2))
-((T(9)*kk[p3][p3]*Hk[p2][p1][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term07(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(9)*kk[p1][p2]*Hk[p2][p3][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3])
+T(9)*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3]
+(T(9)*kk[p3][p3]*Hk[p2][p3][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3])/T(2)
+(trH[p2]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p3][p1][b]*Hkk[p1][p2][p3])/T(4)
+(trH[p2]*T(9)*k[p1][a]*kk[p3][p3]*Hk[p3][p1][b]*Hkk[p1][p2][p3])/T(2)
-(T(9)*kk[p1][p2]*Hk[p2][p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])
-(T(9)*kk[p2][p2]*Hk[p2][p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])
-((T(9)*kk[p2][p3]*Hk[p2][p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])/T(2))
-(T(9)*kk[p3][p3]*Hk[p2][p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])
-(T(9)*kk[p1][p2]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])
+T(9)*kk[p1][p3]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]
-(T(9)*kk[p2][p2]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])
+(T(9)*kk[p2][p3]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])/T(2)
+(T(9)*kk[p3][p3]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3])/T(2)
-(T(9)*kk[p2][p3]*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p2][p3])
+T(18)*kk[p3][p3]*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p2][p3]
-(T(9)*kk[p2][p3]*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3])
-((T(9)*kk[p3][p3]*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3])/T(2))
+T(9)*kk[p1][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]
+T(36)*kk[p2][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term08(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(18)*kk[p3][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]
-((T(9)*HH[p2][p3]*k[p1][b]*k[p2][a]*kk[p1][p2]*Hkk[p1][p3][p2])/T(2))
-((T(9)*HH[p2][p3]*k[p1][a]*k[p3][b]*kk[p1][p3]*Hkk[p1][p3][p2])/T(2))
+(trH[p3]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p2][p1][a]*Hkk[p1][p3][p2])/T(4)
+(trH[p3]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p2][p1][b]*Hkk[p1][p3][p2])/T(4)
+(trH[p2]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p3][p1][a]*Hkk[p1][p3][p2])/T(4)
+(trH[p2]*T(9)*k[p1][b]*kk[p3][p3]*Hk[p3][p1][a]*Hkk[p1][p3][p2])/T(2)
-((T(9)*kk[p2][p3]*Hk[p2][p1][b]*Hk[p3][p1][a]*Hkk[p1][p3][p2])/T(2))
-((T(9)*kk[p3][p3]*Hk[p2][p1][b]*Hk[p3][p1][a]*Hkk[p1][p3][p2])/T(2))
+(trH[p2]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p3][p1][b]*Hkk[p1][p3][p2])/T(4)
+(trH[p2]*T(9)*k[p1][a]*kk[p3][p3]*Hk[p3][p1][b]*Hkk[p1][p3][p2])/T(2)
-((T(9)*kk[p2][p3]*Hk[p2][p1][a]*Hk[p3][p1][b]*Hkk[p1][p3][p2])/T(2))
-(T(9)*kk[p3][p3]*Hk[p2][p1][a]*Hk[p3][p1][b]*Hkk[p1][p3][p2])
+(T(9)*kk[p2][p3]*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p2])/T(2)
-((T(9)*kk[p2][p3]*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p2])/T(2))
-((T(9)*kk[p3][p3]*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p2])/T(2))
-(T(9)*HH[p2][p3]*k[p1][a]*k[p3][b]*kk[p1][p2]*Hkk[p1][p3][p3])
+trH[p2]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p3][p1][a]*Hkk[p1][p3][p3]
+trH[p2]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p3][p1][b]*Hkk[p1][p3][p3]
+T(9)*kk[p1][p2]*Hk[p2][p3][a]*Hk[p3][p1][b]*Hkk[p1][p3][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term09(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(9)*kk[p1][p2]*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3]
-(T(9)*kk[p2][p3]*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3])
-((T(9)*kk[p3][p3]*Hk[p2][p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3])/T(4))
+T(9)*kk[p1][p2]*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3]
+T(9)*kk[p2][p3]*Hk[p2][p3][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3]
+T(9)*kk[p1][p2]*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]
+(T(9)*kk[p2][p2]*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3])/T(2)
-((T(9)*kk[p3][p3]*Hk[p2][p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3])/T(4))
+T(9)*kk[p1][p2]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]
+T(18)*kk[p2][p2]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]
+T(9)*kk[p2][p3]*Hk[p2][p3][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]
+T(9)*kk[p2][p3]*Hk[p1][p3][b]*Hk[p3][p1][a]*Hkk[p2][p1][p1]
+T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p1][p1]
+T(9)*kk[p2][p3]*Hk[p1][p3][a]*Hk[p3][p1][b]*Hkk[p2][p1][p1]
-(T(9)*kk[p3][p3]*Hk[p1][p3][b]*Hk[p3][p2][a]*Hkk[p2][p1][p1])
-(T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p3][p2][b]*Hkk[p2][p1][p1])
-((T(9)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p3][p2][b]*Hkk[p2][p1][p1])/T(2))
-(T(9)*kk[p1][p3]*Hk[p1][p3][a]*Hk[p3][p2][b]*Hkk[p2][p1][p1])
+T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p1]
+(T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p2]*Hkk[p2][p1][p1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term010(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]*Hkk[p2][p1][p1]
+(T(9)*HH[p1][p3]*k[p1][b]*k[p2][a]*kk[p2][p3]*Hkk[p2][p1][p2])/T(2)
+(T(9)*kk[p2][p3]*Hk[p1][p3][b]*Hk[p3][p1][a]*Hkk[p2][p1][p2])/T(2)
+(T(9)*kk[p2][p3]*Hk[p1][p3][a]*Hk[p3][p1][b]*Hkk[p2][p1][p2])/T(2)
+T(9)*kk[p3][p3]*Hk[p1][p3][a]*Hk[p3][p1][b]*Hkk[p2][p1][p2]
+(T(9)*kk[p2][p3]*Hk[p1][p3][b]*Hk[p3][p2][a]*Hkk[p2][p1][p2])/T(2)
+(T(9)*kk[p2][p3]*Hk[p1][p3][a]*Hk[p3][p2][b]*Hkk[p2][p1][p2])/T(2)
+T(9)*kk[p3][p3]*Hk[p1][p3][a]*Hk[p3][p2][b]*Hkk[p2][p1][p2]
+(T(9)*k[p2][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p2])/T(2)
+(T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p2][p3]*Hkk[p2][p1][p2])/T(4)
+(T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p2])/T(4)
-((T(9)*k[p2][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]*Hkk[p2][p1][p2])/T(2))
-((T(9)*HH[p1][p3]*k[p1][b]*k[p2][a]*kk[p2][p3]*Hkk[p2][p1][p3])/T(4))
-((T(27)*HH[p1][p3]*k[p1][a]*k[p2][b]*kk[p2][p3]*Hkk[p2][p1][p3])/T(4))
+(T(9)*kk[p2][p3]*Hk[p1][p2][b]*Hk[p3][p1][a]*Hkk[p2][p1][p3])/T(2)
+(T(9)*kk[p3][p3]*Hk[p1][p2][b]*Hk[p3][p1][a]*Hkk[p2][p1][p3])/T(2)
+(T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p1][p3])/T(2)
+(T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p1][p3])/T(2)
+(T(9)*kk[p3][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p1][p3])/T(2)
-(T(9)*kk[p2][p3]*Hk[p1][p3][a]*Hk[p3][p1][b]*Hkk[p2][p1][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term011(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p3][p2][a]*Hkk[p2][p1][p3]
-((T(9)*kk[p1][p3]*Hk[p1][p2][b]*Hk[p3][p2][a]*Hkk[p2][p1][p3])/T(2))
-(T(9)*kk[p2][p3]*Hk[p1][p2][b]*Hk[p3][p2][a]*Hkk[p2][p1][p3])
-((T(9)*kk[p1][p3]*Hk[p1][p3][b]*Hk[p3][p2][a]*Hkk[p2][p1][p3])/T(2))
-(T(9)*kk[p2][p3]*Hk[p1][p3][b]*Hk[p3][p2][a]*Hkk[p2][p1][p3])
+(T(9)*kk[p3][p3]*Hk[p1][p3][b]*Hk[p3][p2][a]*Hkk[p2][p1][p3])/T(4)
+trH[p1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p3][p2][b]*Hkk[p2][p1][p3]
-((T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p3][p2][b]*Hkk[p2][p1][p3])/T(2))
-(T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p3][p2][b]*Hkk[p2][p1][p3])
-((T(9)*kk[p1][p3]*Hk[p1][p3][a]*Hk[p3][p2][b]*Hkk[p2][p1][p3])/T(2))
+(T(9)*kk[p3][p3]*Hk[p1][p3][a]*Hk[p3][p2][b]*Hkk[p2][p1][p3])/T(4)
+(T(9)*k[p2][b]*Hk[p3][p1][a]*Hkk[p1][p2][p2]*Hkk[p2][p1][p3])/T(2)
+T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p2][p2]*Hkk[p2][p1][p3]
+T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p2]*Hkk[p2][p1][p3]
-((T(9)*k[p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p3])/T(2))
+T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p2][p3]*Hkk[p2][p1][p3]
+T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p3]
-((trH[p3]*T(9)*k[p1][a]*k[p2][b]*Hkk[p1][p3][p2]*Hkk[p2][p1][p3])/T(4))
+T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p2]*Hkk[p2][p1][p3]
+T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p2]*Hkk[p2][p1][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term012(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3]*Hkk[p2][p1][p3]
+(T(9)*HH[p1][p3]*k[p1][b]*k[p2][a]*kk[p2][p3]*Hkk[p2][p2][p1])/T(2)
+(T(9)*kk[p2][p3]*Hk[p1][p3][b]*Hk[p3][p1][a]*Hkk[p2][p2][p1])/T(2)
+(T(9)*kk[p2][p3]*Hk[p1][p3][a]*Hk[p3][p1][b]*Hkk[p2][p2][p1])/T(2)
+(T(9)*k[p2][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]*Hkk[p2][p2][p1])/T(2)
-(T(9)*HH[p1][p3]*k[p1][b]*k[p2][a]*kk[p1][p2]*Hkk[p2][p2][p3])
+(trH[p1]*T(9)*k[p2][a]*kk[p3][p3]*Hk[p3][p2][b]*Hkk[p2][p2][p3])/T(2)
+(T(9)*k[p2][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]*Hkk[p2][p2][p3])/T(2)
-(T(9)*k[p1][b]*Hk[p3][p1][a]*Hkk[p1][p3][p3]*Hkk[p2][p2][p3])
-(T(9)*k[p1][a]*Hk[p3][p1][b]*Hkk[p1][p3][p3]*Hkk[p2][p2][p3])
-(T(9)*k[p2][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]*Hkk[p2][p2][p3])
+(T(9)*HH[p1][p3]*k[p1][b]*k[p2][a]*kk[p2][p3]*Hkk[p2][p3][p1])/T(4)
+(T(27)*HH[p1][p3]*k[p1][a]*k[p2][b]*kk[p2][p3]*Hkk[p2][p3][p1])/T(4)
-((T(9)*kk[p3][p3]*Hk[p1][p2][b]*Hk[p3][p1][a]*Hkk[p2][p3][p1])/T(2))
-((T(9)*kk[p3][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p1])/T(2))
-((trH[p3]*T(9)*k[p1][a]*k[p2][b]*Hkk[p1][p2][p3]*Hkk[p2][p3][p1])/T(2))
+(T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p2][p3]*Hkk[p2][p3][p1])/T(4)
+(T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]*Hkk[p2][p3][p1])/T(4)
-((trH[p3]*T(9)*k[p1][a]*k[p2][b]*Hkk[p1][p3][p2]*Hkk[p2][p3][p1])/T(4))
+(T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p2]*Hkk[p2][p3][p1])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term013(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p2]*Hkk[p2][p3][p1])/T(4)
+T(9)*k[p1][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3]*Hkk[p2][p3][p1]
+T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]*Hkk[p2][p3][p1]
-((T(9)*kk[p2][p3]*Hk[p1][p2][b]*Hk[p3][p1][a]*Hkk[p2][p3][p2])/T(4))
-((T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p2])/T(4))
-((T(9)*kk[p3][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p2])/T(2))
+(trH[p1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p3][p2][a]*Hkk[p2][p3][p2])/T(2)
+(trH[p1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p3][p2][b]*Hkk[p2][p3][p2])/T(2)
+(trH[p1]*T(9)*k[p2][a]*kk[p3][p3]*Hk[p3][p2][b]*Hkk[p2][p3][p2])/T(2)
+(T(9)*k[p2][b]*Hk[p3][p1][a]*Hkk[p1][p2][p2]*Hkk[p2][p3][p2])/T(4)
+(T(9)*k[p2][a]*Hk[p3][p1][b]*Hkk[p1][p2][p2]*Hkk[p2][p3][p2])/T(4)
+(T(9)*k[p2][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]*Hkk[p2][p3][p2])/T(2)
-(T(9)*k[p2][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]*Hkk[p2][p3][p2])
+(T(9)*kk[p1][p3]*Hk[p1][p2][b]*Hk[p3][p1][a]*Hkk[p2][p3][p3])/T(2)
-((T(9)*kk[p3][p3]*Hk[p1][p2][b]*Hk[p3][p1][a]*Hkk[p2][p3][p3])/T(2))
+(T(9)*kk[p1][p1]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p3])/T(2)
-((T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p3])/T(2))
-((T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p3])/T(2))
-((T(9)*kk[p3][p3]*Hk[p1][p2][a]*Hk[p3][p1][b]*Hkk[p2][p3][p3])/T(2))
-(T(9)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p3][p1][b]*Hkk[p2][p3][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term014(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p1]*T(9)*k[p2][b]*kk[p3][p3]*Hk[p3][p2][a]*Hkk[p2][p3][p3]
+trH[p1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p3][p2][b]*Hkk[p2][p3][p3]
+trH[p1]*T(9)*k[p2][a]*kk[p3][p3]*Hk[p3][p2][b]*Hkk[p2][p3][p3]
-(T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p3][p2][b]*Hkk[p2][p3][p3])
-((T(9)*k[p1][a]*Hk[p3][p1][b]*Hkk[p1][p1][p2]*Hkk[p2][p3][p3])/T(2))
-(T(9)*k[p1][b]*Hk[p3][p1][a]*Hkk[p1][p2][p2]*Hkk[p2][p3][p3])
-(T(9)*k[p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p2]*Hkk[p2][p3][p3])
-(T(9)*k[p1][b]*Hk[p3][p1][a]*Hkk[p1][p2][p3]*Hkk[p2][p3][p3])
+(T(9)*k[p1][a]*Hk[p3][p1][b]*Hkk[p1][p2][p3]*Hkk[p2][p3][p3])/T(2)
-(T(9)*k[p1][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]*Hkk[p2][p3][p3])
-(T(18)*k[p2][a]*Hk[p3][p2][b]*Hkk[p1][p2][p3]*Hkk[p2][p3][p3])
-((T(27)*k[p1][b]*Hk[p3][p1][a]*Hkk[p1][p3][p2]*Hkk[p2][p3][p3])/T(4))
-((T(9)*k[p1][a]*Hk[p3][p1][b]*Hkk[p1][p3][p2]*Hkk[p2][p3][p3])/T(4))
-(T(18)*k[p2][b]*Hk[p3][p2][a]*Hkk[p1][p3][p3]*Hkk[p2][p3][p3])
-(T(18)*k[p2][a]*Hk[p3][p2][b]*Hkk[p1][p3][p3]*Hkk[p2][p3][p3])
-(T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p1][b]*Hkk[p3][p1][p1])
+(T(45)*kk[p1][p2]*Hk[p1][p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p1])/T(4)
+T(9)*kk[p2][p2]*Hk[p1][p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p1]
+(T(45)*kk[p1][p2]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p1])/T(4)
-((T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term015(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(T(9)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p1][p1])
+(T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p1])/T(2)
-(T(9)*k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p2][p3]*Hkk[p3][p1][p1])
+T(9)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p3][p3]*Hkk[p3][p1][p1]
+(T(9)*HH[p1][p2]*k[p1][b]*k[p3][a]*kk[p2][p3]*Hkk[p3][p1][p2])/T(2)
-((T(9)*HH[p1][p2]*k[p2][b]*k[p3][a]*kk[p2][p3]*Hkk[p3][p1][p2])/T(2))
+(T(9)*HH[p1][p2]*k[p1][a]*k[p3][b]*kk[p2][p3]*Hkk[p3][p1][p2])/T(2)
-((T(9)*HH[p1][p2]*k[p1][b]*k[p2][a]*kk[p3][p3]*Hkk[p3][p1][p2])/T(4))
-((T(9)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p3][p3]*Hkk[p3][p1][p2])/T(2))
+T(9)*kk[p2][p3]*Hk[p1][p2][b]*Hk[p2][p1][a]*Hkk[p3][p1][p2]
+(T(9)*kk[p3][p3]*Hk[p1][p2][b]*Hk[p2][p1][a]*Hkk[p3][p1][p2])/T(2)
+T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p1][b]*Hkk[p3][p1][p2]
+T(18)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p1][b]*Hkk[p3][p1][p2]
+(T(9)*kk[p3][p3]*Hk[p1][p2][a]*Hk[p2][p1][b]*Hkk[p3][p1][p2])/T(2)
+(T(9)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p1][p2])/T(2)
+T(9)*kk[p1][p3]*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p1][p2]
+(T(9)*kk[p2][p2]*Hk[p1][p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p2])/T(4)
-((T(9)*kk[p1][p2]*Hk[p1][p3][b]*Hk[p2][p3][a]*Hkk[p3][p1][p2])/T(2))
+(T(9)*kk[p1][p2]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p2])/T(2)
+(T(9)*kk[p2][p2]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p2])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term016(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p2])/T(2))
-(T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p2][p2]*Hkk[p3][p1][p2])
-(T(9)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p2])
+(T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p1][p2])/T(2)
-(T(9)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p1][p2])
+(T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p2])/T(2)
-((T(9)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p3][p2]*Hkk[p3][p1][p2])/T(2))
-(T(9)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p3][p3]*Hkk[p3][p1][p2])
-((T(27)*k[p2][b]*Hk[p1][p2][a]*Hkk[p2][p1][p3]*Hkk[p3][p1][p2])/T(2))
-((T(9)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p1][p3]*Hkk[p3][p1][p2])/T(2))
-(T(9)*k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p1][p3]*Hkk[p3][p1][p2])
-((T(9)*k[p3][a]*Hk[p1][p2][b]*Hkk[p2][p1][p3]*Hkk[p3][p1][p2])/T(2))
-((T(3)*g[a][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p3]*Hkk[p3][p1][p2])/T(2))
-((T(9)*k[p2][b]*Hk[p1][p2][a]*Hkk[p2][p3][p2]*Hkk[p3][p1][p2])/T(4))
-((T(9)*k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p3][p2]*Hkk[p3][p1][p2])/T(4))
+T(9)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p3][p3]*Hkk[p3][p1][p2]
+(T(9)*HH[p1][p2]*k[p1][b]*k[p2][a]*kk[p2][p3]*Hkk[p3][p1][p3])/T(4)
-(T(9)*HH[p1][p2]*k[p1][b]*k[p3][a]*kk[p2][p3]*Hkk[p3][p1][p3])
+trH[p2]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][a]*Hkk[p3][p1][p3]
+(trH[p2]*T(9)*k[p3][a]*kk[p1][p2]*Hk[p1][p3][b]*Hkk[p3][p1][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term017(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(9)*kk[p1][p2]*Hk[p1][p2][b]*Hk[p2][p1][a]*Hkk[p3][p1][p3]
+T(9)*kk[p2][p2]*Hk[p1][p2][b]*Hk[p2][p1][a]*Hkk[p3][p1][p3]
-((T(9)*kk[p2][p3]*Hk[p1][p2][b]*Hk[p2][p1][a]*Hkk[p3][p1][p3])/T(2))
+T(9)*kk[p2][p3]*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p1][p3]
+T(9)*kk[p1][p2]*Hk[p1][p2][a]*Hk[p2][p1][b]*Hkk[p3][p1][p3]
-((T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p1][b]*Hkk[p3][p1][p3])/T(2))
+T(9)*kk[p2][p3]*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p1][p3]
+T(9)*kk[p1][p2]*Hk[p1][p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p3]
-((T(9)*kk[p1][p3]*Hk[p1][p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p3])/T(2))
+T(9)*kk[p2][p2]*Hk[p1][p2][b]*Hk[p2][p3][a]*Hkk[p3][p1][p3]
-((T(9)*kk[p1][p1]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p3])/T(2))
+T(9)*kk[p1][p2]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p3]
-((T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p1][p3])/T(2))
+(T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p1][p2]*Hkk[p3][p1][p3])/T(2)
+T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p2][p2]*Hkk[p3][p1][p3]
-(T(9)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p2]*Hkk[p3][p1][p3])
+T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p2][p2]*Hkk[p3][p1][p3]
-((trH[p2]*T(9)*k[p1][b]*k[p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3])/T(2))
-((trH[p2]*T(9)*k[p1][a]*k[p3][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3])/T(2))
+T(9)*k[p3][b]*Hk[p2][p1][a]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term018(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(T(9)*k[p3][a]*Hk[p2][p1][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3])/T(2)
+T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3]
-((T(9)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3])/T(2))
+T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3]
-((T(9)*k[p3][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3]*Hkk[p3][p1][p3])/T(2))
-((trH[p2]*T(9)*k[p1][a]*k[p3][b]*Hkk[p1][p3][p2]*Hkk[p3][p1][p3])/T(2))
+T(9)*k[p3][b]*Hk[p2][p1][a]*Hkk[p1][p3][p2]*Hkk[p3][p1][p3]
+(T(27)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p3][p2]*Hkk[p3][p1][p3])/T(4)
+(T(27)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p3][p2]*Hkk[p3][p1][p3])/T(4)
-((T(9)*k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p1][p2]*Hkk[p3][p1][p3])/T(2))
-(T(9)*k[p3][b]*Hk[p1][p3][a]*Hkk[p2][p1][p2]*Hkk[p3][p1][p3])
-((T(9)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p1][p3]*Hkk[p3][p1][p3])/T(2))
-((T(9)*k[p3][a]*Hk[p1][p2][b]*Hkk[p2][p1][p3]*Hkk[p3][p1][p3])/T(2))
-((T(9)*k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p2][p1]*Hkk[p3][p1][p3])/T(2))
-((T(9)*k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p2][p3]*Hkk[p3][p1][p3])/T(2))
+(T(9)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p3][p1]*Hkk[p3][p1][p3])/T(2)
+(T(9)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p3][p2]*Hkk[p3][p1][p3])/T(2)
-((T(9)*k[p2][a]*Hk[p1][p2][b]*Hkk[p2][p3][p2]*Hkk[p3][p1][p3])/T(2))
+(T(9)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p3][p3]*Hkk[p3][p1][p3])/T(2)
+(T(9)*k[p3][a]*Hk[p1][p2][b]*Hkk[p2][p3][p3]*Hkk[p3][p1][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term019(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((T(9)*HH[p1][p2]*k[p1][b]*k[p3][a]*kk[p2][p3]*Hkk[p3][p2][p1])/T(2))
+(T(9)*HH[p1][p2]*k[p2][b]*k[p3][a]*kk[p2][p3]*Hkk[p3][p2][p1])/T(2)
-((T(9)*HH[p1][p2]*k[p1][a]*k[p3][b]*kk[p2][p3]*Hkk[p3][p2][p1])/T(2))
+(T(9)*HH[p1][p2]*k[p1][b]*k[p2][a]*kk[p3][p3]*Hkk[p3][p2][p1])/T(2)
+(T(9)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p3][p3]*Hkk[p3][p2][p1])/T(4)
-((T(9)*kk[p2][p3]*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p2][p1])/T(2))
-(T(9)*kk[p3][p3]*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p2][p1])
-((T(9)*kk[p2][p3]*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p2][p1])/T(2))
-(T(9)*kk[p3][p3]*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p2][p1])
-((T(9)*kk[p1][p2]*Hk[p1][p3][b]*Hk[p2][p3][a]*Hkk[p3][p2][p1])/T(2))
-((T(9)*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p3][b]*Hkk[p3][p2][p1])/T(2))
+(T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p1])/T(4)
-((T(9)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p1])/T(2))
+(T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3]*Hkk[p3][p2][p1])/T(4)
+(T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p3][p2]*Hkk[p3][p2][p1])/T(4)
+(T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p3][p2]*Hkk[p3][p2][p1])/T(4)
-(trH[p2]*T(9)*k[p1][b]*k[p3][a]*Hkk[p1][p3][p3]*Hkk[p3][p2][p1])
-(trH[p2]*T(9)*k[p1][a]*k[p3][b]*Hkk[p1][p3][p3]*Hkk[p3][p2][p1])
+T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p3][p3]*Hkk[p3][p2][p1]
+T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p3][p3]*Hkk[p3][p2][p1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term020(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
T(9)*k[p3][b]*Hk[p1][p3][a]*Hkk[p2][p1][p3]*Hkk[p3][p2][p1]
+T(9)*k[p3][a]*Hk[p1][p3][b]*Hkk[p2][p1][p3]*Hkk[p3][p2][p1]
-((T(3)*g[a][b]*Hkk[p1][p2][p3]*Hkk[p2][p1][p3]*Hkk[p3][p2][p1])/T(2))
+(T(9)*k[p3][b]*Hk[p1][p2][a]*Hkk[p2][p3][p3]*Hkk[p3][p2][p1])/T(2)
+trH[p1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p2][p3][a]*Hkk[p3][p2][p2]
-((T(9)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p2])/T(4))
-((T(63)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p2][p3]*Hkk[p3][p2][p2])/T(4))
-(T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p2])
-(T(18)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p2])
-((T(9)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p3][p2]*Hkk[p3][p2][p2])/T(2))
-(trH[p1]*T(9)*k[p2][a]*k[p3][b]*Hkk[p2][p3][p3]*Hkk[p3][p2][p2])
-((T(9)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p1][p3]*Hkk[p3][p2][p3])/T(4))
+T(9)*HH[p1][p2]*k[p1][b]*k[p3][a]*kk[p1][p3]*Hkk[p3][p2][p3]
+(T(9)*kk[p1][p3]*Hk[p1][p2][b]*Hk[p2][p1][a]*Hkk[p3][p2][p3])/T(2)
-(T(9)*kk[p1][p2]*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p2][p3])
+(T(9)*kk[p1][p3]*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p2][p3])/T(2)
-((T(9)*kk[p2][p2]*Hk[p1][p3][b]*Hk[p2][p1][a]*Hkk[p3][p2][p3])/T(2))
+(T(9)*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p1][b]*Hkk[p3][p2][p3])/T(2)
-(T(9)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p2][p3])
+(T(9)*kk[p1][p3]*Hk[p1][p3][a]*Hk[p2][p1][b]*Hkk[p3][p2][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term021(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
trH[p1]*T(9)*k[p3][b]*kk[p2][p2]*Hk[p2][p3][a]*Hkk[p3][p2][p3]
+(trH[p1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p2][p3][a]*Hkk[p3][p2][p3])/T(2)
-(T(9)*kk[p1][p2]*Hk[p1][p3][b]*Hk[p2][p3][a]*Hkk[p3][p2][p3])
+(trH[p1]*T(9)*k[p3][a]*kk[p2][p3]*Hk[p2][p3][b]*Hkk[p3][p2][p3])/T(2)
-(T(9)*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p3][b]*Hkk[p3][p2][p3])
-(T(9)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p1][p2]*Hkk[p3][p2][p3])
-((T(9)*k[p3][a]*Hk[p2][p3][b]*Hkk[p1][p1][p2]*Hkk[p3][p2][p3])/T(2))
+(T(9)*k[p3][b]*Hk[p2][p1][a]*Hkk[p1][p1][p3]*Hkk[p3][p2][p3])/T(2)
-(T(9)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p2][p2]*Hkk[p3][p2][p3])
-(T(9)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p2][p2]*Hkk[p3][p2][p3])
-(T(18)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p2]*Hkk[p3][p2][p3])
-((T(27)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p3])/T(4))
+(T(9)*k[p3][b]*Hk[p2][p1][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p3])/T(2)
-((T(27)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p2][p3]*Hkk[p3][p2][p3])/T(4))
-(T(9)*k[p1][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p3])
-(T(18)*k[p3][b]*Hk[p2][p3][a]*Hkk[p1][p2][p3]*Hkk[p3][p2][p3])
-(T(9)*k[p1][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3]*Hkk[p3][p2][p3])
-(T(18)*k[p3][a]*Hk[p2][p3][b]*Hkk[p1][p2][p3]*Hkk[p3][p2][p3])
-((T(27)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p3][p2]*Hkk[p3][p2][p3])/T(4))
+(T(9)*k[p3][b]*Hk[p2][p1][a]*Hkk[p1][p3][p2]*Hkk[p3][p2][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term022(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((T(27)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p3][p2]*Hkk[p3][p2][p3])/T(4))
-(T(9)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p3][p3]*Hkk[p3][p2][p3])
+(T(9)*k[p3][b]*Hk[p2][p1][a]*Hkk[p1][p3][p3]*Hkk[p3][p2][p3])/T(4)
-(T(9)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p3][p3]*Hkk[p3][p2][p3])
+(T(9)*k[p3][a]*Hk[p2][p1][b]*Hkk[p1][p3][p3]*Hkk[p3][p2][p3])/T(4)
+T(9)*k[p3][a]*Hk[p1][p3][b]*Hkk[p2][p1][p1]*Hkk[p3][p2][p3]
-(T(9)*k[p3][b]*Hk[p1][p3][a]*Hkk[p2][p1][p2]*Hkk[p3][p2][p3])
+(T(9)*k[p2][a]*Hk[p1][p3][b]*Hkk[p2][p1][p2]*Hkk[p3][p2][p3])/T(2)
-((T(9)*k[p3][b]*Hk[p1][p3][a]*Hkk[p2][p1][p3]*Hkk[p3][p2][p3])/T(4))
-((T(9)*k[p3][a]*Hk[p1][p3][b]*Hkk[p2][p1][p3]*Hkk[p3][p2][p3])/T(4))
-((trH[p1]*T(9)*k[p2][a]*k[p3][b]*Hkk[p2][p2][p3]*Hkk[p3][p2][p3])/T(2))
-((trH[p1]*T(9)*k[p2][a]*k[p3][b]*Hkk[p2][p3][p2]*Hkk[p3][p2][p3])/T(2))
-(trH[p1]*T(9)*k[p2][b]*k[p3][a]*Hkk[p2][p3][p3]*Hkk[p3][p2][p3])
-(trH[p1]*T(9)*k[p2][a]*k[p3][b]*Hkk[p2][p3][p3]*Hkk[p3][p2][p3])
-((T(9)*HH[p1][p2]*k[p1][a]*k[p2][b]*kk[p2][p3]*Hkk[p3][p3][p1])/T(4))
+(trH[p2]*T(9)*k[p3][a]*kk[p1][p2]*Hk[p1][p3][b]*Hkk[p3][p3][p1])/T(2)
-((trH[p2]*T(9)*k[p1][b]*k[p3][a]*Hkk[p1][p3][p2]*Hkk[p3][p3][p1])/T(2))
+(T(9)*k[p3][a]*Hk[p2][p1][b]*Hkk[p1][p3][p2]*Hkk[p3][p3][p1])/T(2)
+(T(9)*k[p3][a]*Hk[p1][p2][b]*Hkk[p2][p3][p1]*Hkk[p3][p3][p1])/T(2)
+(T(9)*HH[p1][p2]*k[p1][b]*k[p2][a]*kk[p1][p3]*Hkk[p3][p3][p2])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term023(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((T(9)*k[p3][a]*Hk[p2][p3][b]*Hkk[p1][p1][p2]*Hkk[p3][p3][p2])/T(2))
-((T(9)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p2][p3]*Hkk[p3][p3][p2])/T(4))
-((T(9)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p2][p3]*Hkk[p3][p3][p2])/T(4))
-((T(9)*k[p1][b]*Hk[p2][p1][a]*Hkk[p1][p3][p2]*Hkk[p3][p3][p2])/T(4))
-((T(9)*k[p1][a]*Hk[p2][p1][b]*Hkk[p1][p3][p2]*Hkk[p3][p3][p2])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term11(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*trH[p2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a])/T(2)
+metric[i1]*trH[p2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][b]*H[p3][i1][a]
+metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*kk[p1][p2]*H[p2][i1][b]*H[p3][i1][a]
+(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*kk[p2][p2]*H[p2][i1][b]*H[p3][i1][a])/T(2)
+(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][a])/T(2)
+(metric[i1]*trH[p2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b])/T(2)
+metric[i1]*trH[p2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][b]
+metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*kk[p1][p2]*H[p2][i1][a]*H[p3][i1][b]
+(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*kk[p2][p2]*H[p2][i1][a]*H[p3][i1][b])/T(2)
+metric[i1]*trH[p1]*T(9)*pow3(kk[p2][p3]) *H[p2][i1][a]*H[p3][i1][b]
+(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b])/T(2)
+metric[i1]*trH[p1]*T(9)*kk[p2][p2]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]
-((metric[i1]*trH[p2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1])/T(2))
-(metric[i1]*trH[p2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1])
-((metric[i1]*trH[p2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1])/T(2))
-(metric[i1]*trH[p2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1])
-((metric[i1]*trH[p3]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1])/T(2))
-((metric[i1]*trH[p3]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1])/T(2))
-(metric[i1]*trH[p2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][i1])
-(metric[i1]*trH[p2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term12(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p1][i1]*Hk[p2][p1][a])/T(4))
+(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p1][i1]*Hk[p2][p1][a])/T(2)
+(metric[i1]*pow2(kk[p2][p3])*T(27)*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p1][a])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p1][a]
+metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p1][a]
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p1][a])/T(4))
+metric[i1]*T(18)*kk[p1][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p1][a]
+metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p1][a]
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hk[p2][p1][a])
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hk[p2][p1][a])/T(2))
+(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hk[p2][p1][a])/T(4)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p1][i1]*Hk[p2][p1][b])/T(4))
+(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p1][b])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p1][b]
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p1][b])/T(2))
+metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p1][b]
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p1][b])/T(4))
+metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p1][b]
+metric[i1]*pow2(kk[p2][p3])*T(9)*H[p3][i1][a]*Hk[p1][p3][i1]*Hk[p2][p1][b]
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][i1]*Hk[p2][p1][b])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term13(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p3][i1]*Hk[p2][p1][b])/T(4)
-(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*k[p2][b]*H[p3][i1][a]*Hk[p2][p1][i1])
-(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*k[p2][a]*H[p3][i1][b]*Hk[p2][p1][i1])
+metric[i1]*pow2(kk[p2][p3])*T(9)*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p1][i1]
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p1][i1])/T(2))
+metric[i1]*pow2(kk[p2][p3])*T(9)*H[p3][i1][a]*Hk[p1][p2][b]*Hk[p2][p1][i1]
+metric[i1]*pow2(kk[p1][p2])*T(9)*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1]
+(metric[i1]*T(9)*kk[p1][p1]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1])/T(4)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p1]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1]
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1]
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1]
+metric[i1]*T(9)*kk[p2][p2]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p1][i1]
+metric[i1]*pow2(kk[p2][p3])*T(9)*H[p3][i1][a]*Hk[p1][p3][b]*Hk[p2][p1][i1]
+(metric[i1]*T(9)*kk[p1][p1]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p3][b]*Hk[p2][p1][i1])/T(4)
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][b]*Hk[p2][p1][i1]
+metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][b]*Hk[p2][p1][i1]
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][b]*Hk[p2][p1][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term14(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*k[p2][b]*H[p3][i1][a]*Hk[p2][p2][i1])/T(2))
-((metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*k[p2][a]*H[p3][i1][b]*Hk[p2][p2][i1])/T(2))
-(metric[i1]*trH[p1]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p2][i1])
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p2][i1])/T(4)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p2][i1])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][b]*Hk[p2][p2][i1])/T(4)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p2][i1])/T(2))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p2][i1])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][b]*Hk[p2][p2][i1])/T(2))
+(metric[i1]*pow2(kk[p1][p2])*T(9)*H[p3][i1][b]*Hk[p1][p1][i1]*Hk[p2][p3][a])/T(4)
+metric[i1]*pow2(kk[p1][p3])*T(9)*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]
+metric[i1]*pow2(kk[p2][p3])*T(18)*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]
+(metric[i1]*T(9)*kk[p1][p1]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a])/T(4)
+metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a])/T(4)
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a])
+metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]
+(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a])/T(2)
+metric[i1]*T(18)*kk[p2][p2]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term15(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hk[p2][p3][a]
+metric[i1]*pow2(kk[p1][p2])*T(9)*H[p3][i1][b]*Hk[p1][p3][i1]*Hk[p2][p3][a]
-(metric[i1]*T(18)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hk[p2][p3][a])
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p3][i1][b]*Hk[p1][p3][i1]*Hk[p2][p3][a])/T(2)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hk[p2][p3][a])/T(2))
+(metric[i1]*pow2(kk[p1][p2])*T(9)*H[p3][i1][a]*Hk[p1][p1][i1]*Hk[p2][p3][b])/T(4)
+metric[i1]*pow2(kk[p1][p2])*T(9)*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]
+(metric[i1]*T(9)*kk[p1][p1]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b])/T(4)
+metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b])/T(4)
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]
+metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]
+metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]
+(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b])/T(2)
+metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hk[p2][p3][b]
+metric[i1]*pow2(kk[p1][p2])*T(9)*H[p3][i1][a]*Hk[p1][p3][i1]*Hk[p2][p3][b]
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][i1]*Hk[p2][p3][b])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p3][i1]*Hk[p2][p3][b])/T(2))
-(metric[i1]*trH[p1]*T(9)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p3][i1][a]*Hk[p2][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term16(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*k[p2][a]*H[p3][i1][b]*Hk[p2][p3][i1])
-(metric[i1]*trH[p1]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p3][i1])
+(metric[i1]*pow2(kk[p1][p3])*T(9)*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p3][i1])/T(2)
-((metric[i1]*T(9)*kk[p1][p1]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*T(27)*kk[p1][p3]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][a]*Hk[p2][p3][i1])/T(2)
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p3][i1][a]*Hk[p1][p2][b]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][b]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*T(9)*kk[p1][p1]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p3][i1])/T(2)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p3][i1])/T(2))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p3][a]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p3][i1][a]*Hk[p1][p3][b]*Hk[p2][p3][i1])/T(2)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p3][b]*Hk[p2][p3][i1])/T(2))
+metric[i1]*trH[p3]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][i1]
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p1][i1]*Hk[p3][p1][a])/T(4)
+(metric[i1]*pow2(kk[p2][p3])*T(27)*H[p2][i1][b]*Hk[p1][p2][i1]*Hk[p3][p1][a])/T(4)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hk[p3][p1][a])/T(2)
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hk[p3][p1][a])/T(4)
+metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p1][a]
-(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p1][a])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term17(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(18)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p1][a]
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p1][a]
-((metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p1][a])/T(4))
-(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p1][i1]*Hk[p3][p1][a])
-((metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p1][i1]*Hk[p3][p1][a])/T(2))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p1][a])
+(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p1][i1][b]*Hk[p2][p2][i1]*Hk[p3][p1][a])/T(4)
-((metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p2][i1]*Hk[p3][p1][a])/T(4))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p1][a])
-(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p1][a])
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p1][a])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p1][a])/T(2)
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p1][a])/T(4)
+(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p1][a])/T(4)
+(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hk[p3][p1][a])/T(4)
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][a])
+(metric[i1]*T(27)*k[p2][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][a])/T(4)
-(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][a])
+(metric[i1]*T(27)*k[p1][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p1][a])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p1][i1]*Hk[p3][p1][b])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term18(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p1][i1]*Hk[p3][p1][b])/T(2))
-((metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][a]*Hk[p1][p2][i1]*Hk[p3][p1][b])/T(4))
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hk[p3][p1][b])/T(4)
-(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hk[p3][p1][b])
+metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p1][b]
-((metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p1][b])/T(2))
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p1][b]
+metric[i1]*T(18)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p1][b]
+metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p1][b]
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p1][b]
-((metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p1][b])/T(4))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][i1]*Hk[p3][p1][b])
-(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][i1]*Hk[p3][p1][b])
+(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p1][i1][a]*Hk[p2][p2][i1]*Hk[p3][p1][b])/T(4)
+(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p2][i1]*Hk[p3][p1][b])/T(2)
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p2][i1]*Hk[p3][p1][b])/T(4))
-(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p1][b])
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p1][b])
-(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p1][b])
-(metric[i1]*T(18)*k[p1][a]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p1][b])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term19(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*pow2(kk[p2][p3])*T(9)*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p1][b]
+metric[i1]*T(9)*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p1][b]
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(2)
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(2))
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(4)
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p1][b]
+(metric[i1]*T(9)*kk[p2][p2]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(2)
+(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(4)
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(4))
-(metric[i1]*T(18)*k[p1][a]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])
-(metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(2))
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p1][b])/T(2))
+metric[i1]*pow2(kk[p1][p3])*T(9)*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1]
+metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1]
+(metric[i1]*T(27)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p1]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term110(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(27)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*T(9)*kk[p1][p1]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1]
+(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p1][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p1][i1]
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p1][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p1][i1]
+(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p1][i1])/T(2)
+metric[i1]*T(9)*kk[p2][p2]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p1][i1]
-((metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][b]*Hk[p1][p3][a]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p3][a]*Hk[p3][p1][i1])/T(2))
+metric[i1]*T(9)*kk[p1][p1]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][a]*Hk[p3][p1][i1]
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][a]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*T(27)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][b]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*T(45)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p1][i1])/T(4)
+metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p1][i1]
+(metric[i1]*T(45)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p1][i1])/T(4)
-(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p1][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term111(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p1][b]*Hk[p3][p1][i1]
+metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p1][i1]
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p1][i1]*Hk[p3][p1][i1])/T(2))
-(metric[i1]*T(18)*k[p2][b]*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p1][i1]*Hk[p3][p1][i1])
-((metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p1][i1]*Hk[p3][p1][i1])/T(2))
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p1][p3][a]*Hk[p2][p1][i1]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p3][b]*Hk[p2][p1][i1]*Hk[p3][p1][i1])/T(2)
-(metric[i1]*T(18)*k[p3][a]*kk[p2][p3]*Hk[p1][p3][b]*Hk[p2][p1][i1]*Hk[p3][p1][i1])
-((metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p2][i1]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p2][i1]*Hk[p3][p1][i1])/T(4))
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p2][b]*Hk[p2][p2][i1]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*Hk[p1][p2][b]*Hk[p2][p2][i1]*Hk[p3][p1][i1])/T(4))
-(metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*Hk[p1][p2][b]*Hk[p2][p2][i1]*Hk[p3][p1][i1])
-((metric[i1]*pow2(kk[p1][p2])*T(45)*H[p1][i1][b]*Hk[p2][p3][a]*Hk[p3][p1][i1])/T(4))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p1][i1][b]*Hk[p2][p3][a]*Hk[p3][p1][i1])
-((metric[i1]*T(45)*k[p1][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p1][i1])/T(4))
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p1][i1])
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p1][i1])
+metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p1][i1]
-((metric[i1]*pow2(kk[p1][p2])*T(45)*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p1][i1])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term112(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p1][i1])/T(2)
-((metric[i1]*T(45)*k[p1][a]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p1][i1])/T(4))
-(metric[i1]*T(18)*k[p3][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p1][i1])
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][b]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p1]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*T(27)*k[p3][b]*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1])/T(2))
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1])
-((metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*Hk[p1][p2][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1])/T(2))
+metric[i1]*T(18)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1]
-((metric[i1]*T(27)*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hk[p3][p2][a])/T(4))
+(metric[i1]*T(27)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p2][a])/T(2)
+metric[i1]*T(18)*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p2][a]
+metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p2][a]
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p2][a]
+(metric[i1]*T(27)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p2][a])/T(4)
+metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hk[p3][p2][a]
+metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p1][i1]*Hk[p3][p2][a]
-(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][a])
+metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][a]
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p2][i1]*Hk[p3][p2][a])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term113(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p2][i1]*Hk[p3][p2][a])/T(4))
-((metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][a])/T(4))
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][a])/T(2))
-(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][a])
+(metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p2][a])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p2][a])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p2][a]
+metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p2][a]
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hk[p3][p2][a])/T(4))
-(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
-(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
-((metric[i1]*T(27)*k[p1][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])/T(2))
-(metric[i1]*T(18)*k[p1][b]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
+metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a]
-((metric[i1]*T(27)*k[p1][b]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])/T(4))
-(metric[i1]*T(36)*k[p2][b]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][a])
+(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p1][i1]*Hk[p3][p2][b])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hk[p3][p2][b])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term114(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hk[p3][p2][b]
+metric[i1]*pow2(kk[p2][p3])*T(18)*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b]
-(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b])
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b])/T(2)
+metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b]
+(metric[i1]*T(9)*kk[p1][p1]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b])/T(2)
+metric[i1]*T(18)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b]
+(metric[i1]*T(27)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b])/T(4)
+metric[i1]*T(18)*kk[p2][p2]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b]
+metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hk[p3][p2][b]
+metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][a]*Hk[p2][p1][i1]*Hk[p3][p2][b]
+(metric[i1]*T(27)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p1][i1]*Hk[p3][p2][b])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p1][i1]*Hk[p3][p2][b])/T(2))
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][b])
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][b])
+metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hk[p3][p2][b]
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p2][i1]*Hk[p3][p2][b])/T(4))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p2][i1]*Hk[p3][p2][b])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p2][i1]*Hk[p3][p2][b])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term115(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][b])/T(4))
+metric[i1]*T(9)*k[p2][a]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][b]
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][b])/T(2))
-(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][b])
-(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][b])
-(metric[i1]*T(18)*k[p2][a]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hk[p3][p2][b])
+(metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p2][b])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p2][b])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p2][b]
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p2][b])/T(2))
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p2][b]
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hk[p3][p2][b])/T(4))
-(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*T(18)*k[p2][a]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-((metric[i1]*T(27)*k[p1][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])/T(2))
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term116(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(36)*k[p2][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-((metric[i1]*T(27)*k[p1][a]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])/T(4))
-(metric[i1]*T(36)*k[p2][a]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hk[p3][p2][b])
-(metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*k[p3][b]*H[p2][i1][a]*Hk[p3][p2][i1])
+metric[i1]*trH[p2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
+metric[i1]*trH[p2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p3][p2][i1]
+metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p2][i1]
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p2][i1]
+(metric[i1]*T(27)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*T(9)*kk[p2][p2]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*T(99)*k[p1][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*T(18)*k[p1][b]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p2]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term117(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p2][i1])
+metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p2][i1]
+metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p2][i1]
+metric[i1]*T(9)*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p2][i1]
+metric[i1]*T(18)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p2][i1]
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p2][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p2][i1]
+(metric[i1]*T(27)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p2][i1])/T(2)
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
-((metric[i1]*T(63)*k[p1][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*T(18)*k[p1][a]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
+metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1]
-(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p2][i1])
+metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p3][a]*Hk[p3][p2][i1]
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p1][i1][b]*Hk[p2][p3][a]*Hk[p3][p2][i1])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term118(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p3][a]*Hk[p3][p2][i1]
+metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1]
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])/T(4))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
-(metric[i1]*T(36)*k[p3][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
+metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1]
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
-(metric[i1]*T(18)*k[p3][b]*kk[p2][p2]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p2][i1])
-((metric[i1]*pow2(kk[p1][p2])*T(9)*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p2][i1])/T(4))
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p2][i1]
+metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p2][i1]
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p2]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])/T(4))
+metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1]
+metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term119(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*Hk[p1][p3][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])
-(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p3][b]*Hk[p3][p2][i1])
+metric[i1]*trH[p1]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*Hk[p2][p3][i1]*Hk[p3][p2][i1]
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*trH[p2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*Hk[p3][p3][i1])
-(metric[i1]*trH[p2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*Hk[p3][p3][i1])
-((metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*k[p3][b]*H[p2][i1][a]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*trH[p1]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p3][i1])
-((metric[i1]*pow2(kk[p2][p3])*trH[p1]*T(9)*k[p3][a]*H[p2][i1][b]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*pow2(kk[p1][p3])*T(9)*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*T(9)*kk[p1][p1]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p3][i1])/T(2)
-(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p2][a]*Hk[p3][p3][i1])
+(metric[i1]*pow2(kk[p1][p3])*T(9)*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p3][i1])/T(2)
-(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p3][i1])
-(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p2][b]*Hk[p3][p3][i1])
+metric[i1]*trH[p2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][b]*Hk[p1][p3][a]*Hk[p3][p3][i1]
-((metric[i1]*T(9)*kk[p1][p1]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][a]*Hk[p3][p3][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term120(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p3][a]*Hk[p3][p3][i1])/T(2))
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][a]*Hk[p3][p3][i1]
+metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][a]*Hk[p1][p3][b]*Hk[p3][p3][i1]
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p3][b]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p3][b]*Hk[p3][p3][i1])/T(2)
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][b]*Hk[p3][p3][i1]
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p3][i1]
-((metric[i1]*T(27)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][a]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p1][p1][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1])
-(metric[i1]*T(18)*k[p3][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1])
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1])
+metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1]
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1])
+metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1]
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][a]*Hk[p3][p3][i1])/T(4))
+metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p3][i1]
-((metric[i1]*T(27)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][b]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p1][b]*Hk[p3][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term121(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p1][b]*Hk[p3][p3][i1])
+metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p1][b]*Hk[p3][p3][i1]
+metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p3][i1]
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*Hk[p1][p3][i1]*Hk[p2][p1][b]*Hk[p3][p3][i1])/T(4))
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p1][i1]*Hk[p3][p3][i1])
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p2]*Hk[p1][p3][a]*Hk[p2][p1][i1]*Hk[p3][p3][i1])
+metric[i1]*trH[p1]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*Hk[p2][p2][i1]*Hk[p3][p3][i1]
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p2][i1]*Hk[p3][p3][i1])/T(2))
+metric[i1]*T(9)*k[p2][a]*kk[p1][p3]*Hk[p1][p2][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]
+metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p2][i1]*Hk[p3][p3][i1]
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*Hk[p1][p3][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*pow2(kk[p1][p2])*T(9)*H[p1][i1][b]*Hk[p2][p3][a]*Hk[p3][p3][i1])
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p3][a]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p1][i1][b]*Hk[p2][p3][a]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*T(18)*k[p1][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])
-((metric[i1]*T(27)*k[p1][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])/T(4))
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])
-(metric[i1]*T(18)*k[p3][b]*kk[p2][p2]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term122(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])
-((metric[i1]*T(27)*k[p1][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])/T(4))
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][a]*Hk[p3][p3][i1])/T(2)
-(metric[i1]*pow2(kk[p1][p2])*T(9)*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p3][i1])
+(metric[i1]*T(9)*kk[p1][p1]*kk[p1][p2]*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p3][b]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*Hk[p1][p1][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*T(18)*k[p1][a]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])
-(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])
-((metric[i1]*T(27)*k[p1][a]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])/T(4))
-((metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*Hk[p1][p2][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])
-((metric[i1]*T(27)*k[p1][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])/T(4))
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][b]*Hk[p3][p3][i1])/T(2)
+metric[i1]*trH[p1]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*Hk[p2][p3][i1]*Hk[p3][p3][i1]
+metric[i1]*trH[p1]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*Hk[p2][p3][i1]*Hk[p3][p3][i1]
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*Hk[p1][p2][a]*Hk[p2][p3][i1]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*Hk[p1][p2][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term123(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*Hk[p1][p3][a]*Hk[p2][p3][i1]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*Hk[p1][p3][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1])/T(2)
-(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p1][p2])
-(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p1][p2])
-(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p1][p2])
-(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p1][p2])
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p1][p2])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p1][p2])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p1][p2])/T(2))
+(metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p1][p2])/T(2)
+metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p1][p2]
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p1][p2])/T(2))
+metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p1][p2]
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p1])/T(2))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p2][b][i1]*H[p3][i1][a]*Hkk[p1][p2][p2])/T(4))
-(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p2])
+metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p2]
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p2][a][i1]*H[p3][i1][b]*Hkk[p1][p2][p2])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term124(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p2])
-(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p2])
-(metric[i1]*T(18)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p2])
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p2][p2])
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p2][p2])/T(2)
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p2][p2])
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p3][i1][a]*Hk[p2][p2][i1]*Hkk[p1][p2][p2])/T(4)
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p2][p2])/T(4)
+metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p2][p2]
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p2])/T(4)
+(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p2])/T(4)
-((metric[i1]*T(45)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p2])/T(2))
-(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p2])
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p2])/T(2))
-((metric[i1]*T(27)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p2])/T(2))
-(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p2])
+metric[i1]*T(27)*k[p1][b]*k[p2][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term125(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p2]
-((metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p3][p2][i1]*Hkk[p1][p2][p2])/T(4))
-((metric[i1]*T(45)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p3][p2][i1]*Hkk[p1][p2][p2])/T(4))
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p2])
+metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(18)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p2]
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p2][p2])
+metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p2]
+metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p2]
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p2]*H[p2][i1][a]*H[p3][b][i1]*Hkk[p1][p2][p3])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p3])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p3])/T(4)
-(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p3])
+metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p3]
-(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p3])
-((metric[i1]*T(27)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p3])/T(4))
-(metric[i1]*T(36)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p2][p3])
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p2][a][i1]*H[p3][i1][b]*Hkk[p1][p2][p3])/T(2))
-(metric[i1]*pow2(kk[p1][p3])*T(9)*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])
-(metric[i1]*pow2(kk[p2][p3])*T(72)*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term126(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p2]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])/T(2)
+(metric[i1]*T(27)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])/T(4)
-((metric[i1]*T(27)*kk[p1][p3]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])/T(2))
+metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3]
-(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])
-((metric[i1]*T(27)*kk[p1][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])/T(4))
-(metric[i1]*T(36)*kk[p2][p2]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])
-(metric[i1]*T(36)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p2][p3])
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(4))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(2)
-((metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(2))
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p2]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(4))
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p2][p3])/T(2))
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p3]*H[p3][b][i1]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term127(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])/T(4)
-(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])/T(2))
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])/T(4)
-(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])
+(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(36)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(27)*k[p1][b]*kk[p1][p3]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])/T(4)
+(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(36)*k[p2][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p2][p3]
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])/T(4))
+(metric[i1]*T(27)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(54)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(36)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][a][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2)
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
+metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term128(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(18)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
+metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2))
-(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(18)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
-(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
+(metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p1][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2)
+(metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p1][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(3)*g[a][b]*kk[p2][p3]*Hk[p2][p1][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
+metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p2][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
+metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p2][p2][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
+metric[i1]*T(18)*k[p1][a]*k[p2][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
+metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3]
-(metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])/T(2))
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])/T(2)
+(metric[i1]*T(27)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term129(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(54)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p2][i1]*Hkk[p1][p2][p3]
+metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p2][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p1][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])/T(4)
+(metric[i1]*T(63)*k[p1][a]*k[p2][b]*Hk[p2][p1][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])/T(4)
-((metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])/T(2))
-(metric[i1]*T(36)*k[p2][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p2][p3])
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
-((metric[i1]*T(27)*k[p1][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(4))
+(metric[i1]*T(27)*k[p3][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(4)
+(metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(36)*k[p3][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(45)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(4)
+metric[i1]*T(36)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
-((metric[i1]*T(27)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(4))
+(metric[i1]*T(27)*k[p3][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(4)
+(metric[i1]*T(45)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term130(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(36)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2)
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3]
+(metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2)
-((metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2))
-(metric[i1]*T(36)*k[p2][a]*k[p3][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2))
-((metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2))
-(metric[i1]*T(36)*k[p2][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])/T(2))
-(metric[i1]*T(36)*k[p2][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p2][p3])
-((metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p3][p1])/T(4))
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p1])/T(4))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p3][p1])/T(2))
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p3][p1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term131(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p2][b][i1]*H[p3][i1][a]*Hkk[p1][p3][p2])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p3][p2])/T(2))
-((metric[i1]*T(45)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p3][p2])/T(4))
-(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p3][p2])
-(metric[i1]*T(18)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p3][p2])
+metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p3][p2]
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p2][a][i1]*H[p3][i1][b]*Hkk[p1][p3][p2])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][a][i1]*H[p3][i1][b]*Hkk[p1][p3][p2])/T(2)
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p2][a][i1]*H[p3][i1][b]*Hkk[p1][p3][p2])/T(4))
+metric[i1]*pow2(kk[p2][p3])*T(18)*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p2]
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p2])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p2])/T(4)
-(metric[i1]*T(18)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p2])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p2])/T(2))
+metric[i1]*T(18)*kk[p2][p2]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p2]
+metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p2]
-(metric[i1]*T(18)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p3][p2])
-((metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p3][p2])/T(2))
-(metric[i1]*T(18)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p2])
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term132(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p2])
-((metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p2])/T(2))
+(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p2][i1]*Hkk[p1][p3][p2])/T(4)
+metric[i1]*T(18)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p2][i1]*Hkk[p1][p3][p2]
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p3][p2])/T(4)
+metric[i1]*T(18)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p3][p2]
+(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p3][p2])/T(2)
-(metric[i1]*T(18)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p3][p2])
-(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])/T(4)
+(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])/T(2)
-(metric[i1]*T(18)*k[p2][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(45)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])/T(4)
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])/T(2)
-(metric[i1]*T(18)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])/T(2)
-(metric[i1]*T(18)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][a][i1]*Hk[p3][p1][i1]*Hkk[p1][p3][p2])/T(2)
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p3][p2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term133(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(27)*k[p1][b]*kk[p3][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p3][p2])/T(4))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p3][p2])/T(2))
+metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p3][p2]
-((metric[i1]*T(27)*k[p1][a]*kk[p3][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p3][p2])/T(4))
+(metric[i1]*T(27)*k[p1][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p3][p2])/T(4)
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p1][i1]*Hkk[p1][p3][p2])/T(4))
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])/T(2)
+(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])/T(2)
-(metric[i1]*T(18)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p1][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])/T(2)
-((metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2])/T(2))
+metric[i1]*T(18)*k[p2][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p2]
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2))
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][a][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(4)
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*H[p2][b][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(4)
+(metric[i1]*T(27)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2)
-((metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(4))
+(metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term134(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*T(18)*k[p3][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(4)
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(27)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(4))
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(4)
-(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2)
+metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2]
+(metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2)
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2]
+(metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2)
-((metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2))
+metric[i1]*T(18)*k[p2][a]*k[p3][b]*Hk[p2][p2][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2]
-((metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2))
-((metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2))
+metric[i1]*T(18)*k[p2][b]*k[p3][a]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2]
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2])/T(2))
+metric[i1]*T(18)*k[p2][a]*k[p3][b]*Hk[p2][p3][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term135(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][b]*H[p3][a][i1]*Hkk[p1][p3][p3])/T(2))
-((metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][a]*H[p3][b][i1]*Hkk[p1][p3][p3])/T(2))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p2][i1][a]*H[p3][b][i1]*Hkk[p1][p3][p3])/T(2))
-(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p3][p3])
-((metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p3][p3])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][a]*Hkk[p1][p3][p3])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][a][i1]*H[p3][i1][b]*Hkk[p1][p3][p3])/T(2)
-(metric[i1]*pow2(kk[p2][p3])*T(9)*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p3])
-((metric[i1]*pow2(kk[p1][p2])*T(9)*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p3])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p3])/T(2)
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p3])
-(metric[i1]*T(18)*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p3])
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][b]*Hkk[p1][p3][p3])/T(2)
+(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p3][a][i1]*Hk[p2][p1][i1]*Hkk[p1][p3][p3])/T(2)
+(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p3][b][i1]*Hk[p2][p1][i1]*Hkk[p1][p3][p3])/T(2)
-((metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p3][p3])/T(2))
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p1][i1]*Hkk[p1][p3][p3])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p3])/T(2))
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p3])/T(2))
-((metric[i1]*T(27)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p1][i1]*Hkk[p1][p3][p3])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term136(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p3][b][i1]*Hk[p2][p2][i1]*Hkk[p1][p3][p3])/T(2)
+metric[i1]*T(18)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p2][i1]*Hkk[p1][p3][p3]
-((metric[i1]*T(27)*k[p1][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p3][p3])/T(4))
+metric[i1]*T(18)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p2][p3][i1]*Hkk[p1][p3][p3]
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p3])/T(4)
+metric[i1]*T(18)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p2][p3][i1]*Hkk[p1][p3][p3]
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p3][p1][i1]*Hkk[p1][p3][p3])/T(2))
-((metric[i1]*T(27)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p3][p3])/T(2))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p3][p1][i1]*Hkk[p1][p3][p3])
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p3][p3])/T(2)
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p3][p1][i1]*Hkk[p1][p3][p3])
+metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p2][p2][i1]*Hk[p3][p1][i1]*Hkk[p1][p3][p3]
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p2][i1]*Hk[p3][p1][i1]*Hkk[p1][p3][p3]
-(metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p2][p1][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p3])
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p2][p1][i1]*Hk[p3][p2][i1]*Hkk[p1][p3][p3]
+(metric[i1]*T(27)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p3])/T(4)
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p3][p3][i1]*Hkk[p1][p3][p3])/T(2))
+(metric[i1]*T(27)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p3][p3])/T(4)
-((metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p3][p3][i1]*Hkk[p1][p3][p3])/T(2))
+metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p2][p1][i1]*Hk[p3][p3][i1]*Hkk[p1][p3][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term137(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p3][b][i1]*Hkk[p2][p1][p1])/T(2)
-(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p1][p1])
+metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p1][p1]
-((metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p1][p1])/T(2))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p1])/T(2))
+(metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p1])/T(2)
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p1][p1])/T(2)
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p1][p1])
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p3][i1]*Hkk[p2][p1][p1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p1][p1])/T(2))
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p1][p1])
-((metric[i1]*T(27)*k[p2][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p1][p1])/T(2))
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p1]
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p1])/T(2)
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p1][p1])/T(2))
-(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p3][p3][i1]*Hkk[p2][p1][p1])
-((metric[i1]*pow2(kk[p2][p3])*T(9)*H[p1][b][i1]*H[p3][i1][a]*Hkk[p2][p1][p2])/T(4))
-((metric[i1]*pow2(kk[p2][p3])*T(9)*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p1][p2])/T(4))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p1][p2])/T(2))
-((metric[i1]*pow2(kk[p2][p3])*T(9)*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p1][p2])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term138(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p1][p2])/T(2))
-((metric[i1]*pow2(kk[p2][p3])*T(9)*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p2])/T(4))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p2])/T(2))
-(metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p2])
-((metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p2])/T(2))
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p1][p2])/T(2)
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p3][p1][i1]*Hkk[p2][p1][p2])/T(2))
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(4)
-((metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(2))
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(4))
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(2))
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(4))
-((metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p1][i1][b]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(2))
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p2])/T(2)
+metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p1][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term139(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p1][p2])/T(2)
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p3][p3][i1]*Hkk[p2][p1][p2])/T(2))
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p2])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p3][a][i1]*Hkk[p2][p1][p3])/T(4)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p3][b][i1]*Hkk[p2][p1][p3])/T(2))
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p3][b][i1]*Hkk[p2][p1][p3])/T(4)
-((metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][b][i1]*H[p3][i1][a]*Hkk[p2][p1][p3])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p1][p3])/T(4))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p1][p3])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*kk[p2][p3]*kk[p3][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p1][p3])/T(4))
-(metric[i1]*T(9)*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p3])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p3])/T(2))
+(metric[i1]*T(45)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p3])/T(4)
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p3])/T(4))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p3])
-((metric[i1]*T(9)*kk[p2][p2]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p1][i1]*Hkk[p2][p1][p3])/T(4))
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p1][i1]*Hkk[p2][p1][p3])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term140(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p1][i1]*Hkk[p2][p1][p3]
-(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])/T(4))
+(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])/T(2))
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])/T(4)
+(metric[i1]*T(27)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])/T(2)
+(metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p1][p3])/T(2)
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][a][i1]*Hk[p1][p3][i1]*Hkk[p2][p1][p3])/T(4))
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][b][i1]*Hk[p1][p3][i1]*Hkk[p2][p1][p3])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][b][i1]*Hk[p1][p3][i1]*Hkk[p2][p1][p3])/T(4))
-((metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p3][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p1][p3])/T(2))
+metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p1][p3]
+(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p1][i1]*Hkk[p2][p1][p3])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p3][p1][i1]*Hkk[p2][p1][p3])/T(4)
+(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p3][p1][i1]*Hkk[p2][p1][p3])/T(2)
+(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term141(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(27)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(27)*k[p3][a]*kk[p1][p3]*H[p1][i1][b]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2))
+metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p3][p2][i1]*Hkk[p2][p1][p3]
+metric[i1]*T(27)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3]
+(metric[i1]*T(45)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2)
+(metric[i1]*T(27)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2)
+(metric[i1]*T(27)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2)
+metric[i1]*T(3)*g[a][b]*kk[p1][p3]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3]
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(4)
+(metric[i1]*T(63)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(4)
+metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3]
+(metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p1][p3])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(4)
+(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(4)
+(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2)
+metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p1][p3]
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p2]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2)
+(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2)
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term142(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2))
+(metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(4)
-((metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(27)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p3][p3][i1]*Hkk[p2][p1][p3])/T(2))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p2][p1])/T(2))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p2][p1])/T(2))
-((metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p3][p1][i1]*Hkk[p2][p2][p1])/T(2))
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p2][p1])/T(2)
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p1])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p2][p3])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p2][p3])/T(2))
-(metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p2][p3])
+metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p3][i1][a]*Hk[p1][p3][i1]*Hkk[p2][p2][p3]
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p2][p3])/T(2))
+metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p2][p3]
+metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p3][p1][i1]*Hkk[p2][p2][p3]
+metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p1][i1]*Hkk[p2][p2][p3]
+metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term143(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p2][p3])/T(2))
+(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p3][p3][i1]*Hkk[p2][p2][p3])/T(2)
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p3])/T(2)
+(metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p3])/T(2)
+metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p2][p3]
-((metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][b][i1]*H[p3][i1][a]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][b][i1]*H[p3][i1][a]*Hkk[p2][p3][p1])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p3][p1])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p3][p1])/T(2)
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p3][p1])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p3][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p3][p1])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p3][p1])/T(2)
+metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p1]
-((metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p1])/T(4))
+(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p1])/T(2)
+metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p1]
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p1])/T(4)
+(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term144(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p1])/T(2)
+(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p3][i1]*Hkk[p2][p3][p1])/T(2)
-((metric[i1]*T(27)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p3][i1]*Hkk[p2][p3][p1])/T(4))
+(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p3][p1])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p3][p1])/T(4))
+(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p3][p1][i1]*Hkk[p2][p3][p1])/T(2)
-((metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p3][p1])/T(2))
+metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p1]
-((metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p1])/T(4))
+(metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p1])/T(4)
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p1])/T(2))
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p2]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2)
-((metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(4))
-((metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term145(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p1])/T(2))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][b][i1]*H[p3][i1][a]*Hkk[p2][p3][p2])/T(4)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p3][p2])/T(4)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p3][p2])/T(4)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p3][p2])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p3][p2])/T(4)
-((metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])/T(4))
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])/T(2))
-(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])/T(4))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])/T(2))
-(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])
-((metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])/T(2))
-(metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p2])
+(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p2][i1]*Hkk[p2][p3][p2])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p3][p2][i1]*Hkk[p2][p3][p2])/T(4)
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p2])/T(4)
+(metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p2])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term146(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p2]
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p2])/T(2))
+(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p2])/T(2)
+(metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p2])/T(2)
+metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p2]
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*H[p3][a][i1]*Hkk[p2][p3][p3])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p3][b][i1]*Hkk[p2][p3][p3])/T(4))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][b][i1]*H[p3][i1][a]*Hkk[p2][p3][p3])/T(2)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*H[p3][i1][a]*Hkk[p2][p3][p3])/T(4))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p3][p3])/T(2)
+(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][a][i1]*H[p3][i1][b]*Hkk[p2][p3][p3])/T(2)
-((metric[i1]*T(9)*kk[p1][p1]*kk[p1][p2]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p3][p3])/T(2))
+(metric[i1]*T(45)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p3][i1][b]*Hkk[p2][p3][p3])/T(4)
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p1][i1]*Hkk[p2][p3][p3])/T(2)
-((metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p3][a][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])/T(4))
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][b][i1]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])/T(4))
+metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p3]
-(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
-(metric[i1]*T(18)*k[p2][b]*kk[p3][p3]*H[p3][i1][a]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term147(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p3]
-(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
-(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
-(metric[i1]*T(18)*k[p2][a]*kk[p2][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
-(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
-(metric[i1]*T(18)*k[p2][a]*kk[p3][p3]*H[p3][i1][b]*Hk[p1][p2][i1]*Hkk[p2][p3][p3])
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p3][a][i1]*Hk[p1][p3][i1]*Hkk[p2][p3][p3])/T(2)
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p3][b][i1]*Hk[p1][p3][i1]*Hkk[p2][p3][p3])/T(2)
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p3][i1][a]*Hk[p1][p3][i1]*Hkk[p2][p3][p3])/T(4)
-((metric[i1]*T(27)*k[p1][a]*kk[p1][p2]*H[p3][i1][b]*Hk[p1][p3][i1]*Hkk[p2][p3][p3])/T(4))
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p3][p1][i1]*Hkk[p2][p3][p3])/T(2))
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p3][p1][i1]*Hkk[p2][p3][p3])/T(2))
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p1][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(18)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(36)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(18)*k[p2][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(18)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p3][p2][i1]*Hkk[p2][p3][p3]
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p3])/T(2))
-((metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p3])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term148(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(18)*k[p2][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p3]
+metric[i1]*T(18)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p3][p3][i1]*Hkk[p2][p3][p3]
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p2][b][i1]*Hkk[p3][p1][p1])/T(2)
-((metric[i1]*T(45)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p1])/T(4))
-(metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p1])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p1])/T(4))
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p1])/T(2)
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p1])/T(2))
+metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p2][i1]*Hkk[p3][p1][p1]
-((metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p1])/T(4))
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p1][p1])/T(4))
-(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p1][p1])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][a][i1]*Hkk[p3][p1][p2])/T(4))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][b]*H[p2][a][i1]*Hkk[p3][p1][p2])/T(4))
-(metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][a]*H[p2][b][i1]*Hkk[p3][p1][p2])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][b][i1]*Hkk[p3][p1][p2])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term149(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*H[p2][b][i1]*Hkk[p3][p1][p2])/T(4))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][b][i1]*H[p2][i1][a]*Hkk[p3][p1][p2])/T(4))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p2])
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p2])/T(4))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p2])
-((metric[i1]*T(9)*kk[p2][p2]*kk[p3][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][a][i1]*H[p2][i1][b]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][a][i1]*H[p2][i1][b]*Hkk[p3][p1][p2])/T(4))
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(27)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(9)*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(45)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p2])/T(4))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p2])
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p1][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p2][b][i1]*Hk[p1][p2][i1]*Hkk[p3][p1][p2])/T(2)
+metric[i1]*T(18)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p1][p2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term150(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p1][p2])/T(4))
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p1][p2])/T(2)
+metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p1][p2]
+(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
+metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p2]
+(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
+metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p2]
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2)
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p2])/T(2))
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p1][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p1][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p1][i1]*Hkk[p3][p1][p2])/T(2)
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hkk[p3][p1][p2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term151(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p1][i1][b]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])/T(2)
-((metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p2])/T(2))
+metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p2]
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(4)
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2)
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])
+metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p1][p2]
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(4)
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2))
-(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])
-(metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])
-((metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term152(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(27)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2))
-((metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2))
+(metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(3)*g[a][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p2])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p2][a][i1]*Hkk[p3][p1][p3])/T(4)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p2][b][i1]*Hkk[p3][p1][p3])/T(4)
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][b][i1]*H[p2][i1][a]*Hkk[p3][p1][p3])/T(2))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][b][i1]*H[p2][i1][a]*Hkk[p3][p1][p3])/T(2))
-(metric[i1]*pow2(kk[p1][p2])*T(9)*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p3])
-(metric[i1]*T(9)*kk[p1][p2]*kk[p2][p2]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p3])
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p3])/T(4)
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p1][p3])/T(2))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][a][i1]*H[p2][i1][b]*Hkk[p3][p1][p3])/T(2))
-(metric[i1]*pow2(kk[p1][p2])*T(9)*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p3])
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p1][p3])/T(4)
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p1][i1]*Hkk[p3][p1][p3])/T(2)
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p2][i1]*Hkk[p3][p1][p3])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term153(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p1][p3])/T(2))
+metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p1][p3]
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p1][p3])/T(2))
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p1][p3])
-((metric[i1]*T(27)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p1][p3])/T(2))
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p1][p3])/T(4)
-((metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p3])/T(2))
-((metric[i1]*T(27)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p3])/T(2))
+(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p1][p3])/T(4)
+metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p1][i1]*Hkk[p3][p1][p3]
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p3])/T(2)
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p2][i1]*Hkk[p3][p1][p3])/T(2))
+metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p2][p2][i1]*Hkk[p3][p1][p3]
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p2][i1]*Hkk[p3][p1][p3])/T(2)
+(metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p3])/T(2)
+metric[i1]*T(18)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p1][p3]
+(metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][a][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])/T(2)
+(metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][b][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term154(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p2][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])/T(2))
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p2]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])/T(4))
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p2]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])/T(2))
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])/T(4))
+metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3]
+metric[i1]*T(9)*k[p1][a]*k[p2][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3]
+(metric[i1]*T(27)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3])/T(2)
+metric[i1]*T(18)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3]
+metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3]
+metric[i1]*T(18)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p1][p3]
-((metric[i1]*T(9)*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*H[p2][b][i1]*Hkk[p3][p2][p1])/T(2))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][b][i1]*H[p2][i1][a]*Hkk[p3][p2][p1])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][b][i1]*H[p2][i1][a]*Hkk[p3][p2][p1])/T(2))
-((metric[i1]*T(9)*kk[p2][p2]*kk[p3][p3]*H[p1][b][i1]*H[p2][i1][a]*Hkk[p3][p2][p1])/T(2))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p2][p1])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p2][p1])/T(4))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p2][p1])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][a][i1]*H[p2][i1][b]*Hkk[p3][p2][p1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term155(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*kk[p2][p2]*kk[p2][p3]*H[p1][a][i1]*H[p2][i1][b]*Hkk[p3][p2][p1])/T(4))
-((metric[i1]*T(9)*kk[p1][p2]*kk[p3][p3]*H[p1][a][i1]*H[p2][i1][b]*Hkk[p3][p2][p1])/T(2))
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p2][p1])/T(2))
-(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p2][p1])
-((metric[i1]*T(9)*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p2][p1])/T(4))
-((metric[i1]*T(9)*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p2][p1])/T(4))
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][b][i1]*Hk[p1][p1][i1]*Hkk[p3][p2][p1])/T(2)
+(metric[i1]*T(45)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p2][p1])/T(4)
+(metric[i1]*T(9)*k[p1][b]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p2][p1])/T(2)
+(metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p2][p1])/T(4)
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p2][p1])/T(2))
+(metric[i1]*T(9)*k[p1][a]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p2][p1])/T(2)
+metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p1]
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p1])/T(2)
+metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p1]
+(metric[i1]*T(9)*k[p3][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p1])/T(2)
+metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p1]
+(metric[i1]*T(27)*k[p1][b]*kk[p3][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p1])/T(2)
+metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p1]
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term156(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p1])/T(2)
+metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p1]
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p1])/T(2))
+(metric[i1]*T(27)*k[p1][a]*kk[p3][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p1])/T(2)
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p1][i1]*Hkk[p3][p2][p1])/T(2)
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p1][i1]*Hkk[p3][p2][p1])/T(2)
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hkk[p3][p2][p1])/T(2))
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][a][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p1])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p2][p3]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p1])/T(4)
+(metric[i1]*T(9)*k[p2][a]*kk[p3][p3]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p1])/T(2)
-(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p1])
-((metric[i1]*T(9)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p1])/T(2))
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(4)
+(metric[i1]*T(9)*k[p2][b]*kk[p2][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(2)
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(4)
-((metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(2))
-(metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])
-((metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(2))
-((metric[i1]*T(27)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term157(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(27)*k[p1][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(2))
-((metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(2))
+(metric[i1]*T(3)*g[a][b]*kk[p1][p2]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p1])/T(2)
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p2][p2])/T(4)
+(metric[i1]*T(45)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p2][p2])/T(4)
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p2])/T(4)
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p2])/T(2))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p2])
-(metric[i1]*T(18)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p2])
+(metric[i1]*T(45)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p2])/T(4)
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p2][p2])/T(2)
+metric[i1]*T(18)*k[p1][b]*k[p2][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p2]
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p2]
+metric[i1]*T(36)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p2]
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][b]*H[p2][a][i1]*Hkk[p3][p2][p3])/T(4))
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][a]*H[p2][b][i1]*Hkk[p3][p2][p3])/T(4))
+(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][b][i1]*H[p2][i1][a]*Hkk[p3][p2][p3])/T(2)
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p2]*H[p1][b][i1]*H[p2][i1][a]*Hkk[p3][p2][p3])/T(2)
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p2][p3])/T(4))
+(metric[i1]*T(9)*kk[p1][p3]*kk[p2][p2]*H[p1][i1][b]*H[p2][i1][a]*Hkk[p3][p2][p3])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term158(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(9)*kk[p1][p2]*kk[p1][p3]*H[p1][a][i1]*H[p2][i1][b]*Hkk[p3][p2][p3])/T(2)
-((metric[i1]*pow2(kk[p1][p3])*T(9)*H[p1][i1][a]*H[p2][i1][b]*Hkk[p3][p2][p3])/T(4))
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p1][i1]*Hkk[p3][p2][p3])/T(2))
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p2][a][i1]*Hk[p1][p2][i1]*Hkk[p3][p2][p3])/T(2)
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][b][i1]*Hk[p1][p2][i1]*Hkk[p3][p2][p3])/T(2)
+metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p2][p3]
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p2][p3])/T(2)
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p2][i1]*Hkk[p3][p2][p3])
+metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p2][p3]
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p2][i1]*Hkk[p3][p2][p3])/T(2)
-((metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(2))
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p3]*H[p2][a][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(4)
-((metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(2))
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p3]*H[p2][b][i1]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(4)
-((metric[i1]*T(9)*k[p3][b]*kk[p1][p1]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(2))
-(metric[i1]*T(18)*k[p3][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])
+(metric[i1]*T(27)*k[p1][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(4)
-((metric[i1]*T(27)*k[p3][b]*kk[p1][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(4))
-(metric[i1]*T(9)*k[p1][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(18)*k[p3][b]*kk[p2][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term159(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*T(9)*k[p1][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(2))
-(metric[i1]*T(9)*k[p3][b]*kk[p2][p3]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])
+(metric[i1]*T(27)*k[p1][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(4)
-((metric[i1]*T(27)*k[p3][a]*kk[p1][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(4))
-((metric[i1]*T(9)*k[p1][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])/T(2))
-(metric[i1]*T(9)*k[p3][a]*kk[p2][p3]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p1][i1]*Hkk[p3][p2][p3])
-(metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p1][i1]*Hkk[p3][p2][p3])
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p3]*H[p1][b][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p3])/T(2))
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p2][i1]*Hkk[p3][p2][p3])/T(2)
-((metric[i1]*T(9)*k[p2][a]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p2][i1]*Hkk[p3][p2][p3])/T(2))
+metric[i1]*T(9)*k[p1][b]*k[p2][a]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p3]
+metric[i1]*T(9)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p3]
+metric[i1]*T(18)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p2][p3]
-(metric[i1]*T(9)*k[p3][b]*kk[p1][p2]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])
+(metric[i1]*T(9)*k[p3][b]*kk[p1][p3]*H[p1][i1][a]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])/T(4)
+(metric[i1]*T(9)*k[p3][a]*kk[p1][p3]*H[p1][i1][b]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])/T(4)
+metric[i1]*T(18)*k[p2][a]*k[p3][b]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3]
+metric[i1]*T(18)*k[p1][a]*k[p2][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term160(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*T(27)*k[p1][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])/T(4)
+metric[i1]*T(36)*k[p2][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3]
+(metric[i1]*T(27)*k[p1][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3])/T(4)
+metric[i1]*T(36)*k[p2][a]*k[p3][b]*Hk[p1][p3][i1]*Hk[p2][p3][i1]*Hkk[p3][p2][p3]
-((metric[i1]*T(9)*k[p3][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p3][p1])/T(2))
+(metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p3][i1]*Hk[p2][p2][i1]*Hkk[p3][p3][p1])/T(2)
-((metric[i1]*T(9)*k[p1][b]*k[p3][a]*Hk[p1][p2][i1]*Hk[p2][p3][i1]*Hkk[p3][p3][p1])/T(2))
+(metric[i1]*T(9)*k[p1][b]*kk[p1][p2]*H[p2][i1][a]*Hk[p1][p3][i1]*Hkk[p3][p3][p2])/T(2)
+(metric[i1]*T(9)*k[p1][a]*kk[p1][p2]*H[p2][i1][b]*Hk[p1][p3][i1]*Hkk[p3][p3][p2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term21(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][b]*H[p2][i1][i2]*H[p3][i1][a])/T(4))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][b]*H[p2][i1][i2]*H[p3][i1][a])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i1][i2]*H[p2][i2][b]*H[p3][i1][a])/T(2))
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p3]*H[p1][i1][i2]*H[p2][i2][b]*H[p3][i1][a])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p2][p2]*H[p1][i1][i2]*H[p2][i2][b]*H[p3][i1][a])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][b]*H[p3][i1][a])/T(2))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][b]*H[p3][i1][a])
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][b]*H[p3][i1][a])/T(2)
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][b]*H[p3][i1][a])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i2][i1]*H[p2][i2][b]*H[p3][i1][a])/T(4))
-(metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p1][p3]*H[p1][i2][i1]*H[p2][i2][b]*H[p3][i1][a])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*kk[p2][p2]*H[p1][i2][i1]*H[p2][i2][b]*H[p3][i1][a])/T(2))
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p3][p3]*H[p1][i2][i1]*H[p2][i2][b]*H[p3][i1][a])/T(2)
-((metric[i1]*metric[i2]*T(9)*kk[p1][p3]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i2][b]*H[p3][i1][a])/T(4))
-((metric[i1]*metric[i2]*pow2(kk[p1][p3])*T(9)*kk[p2][p3]*H[p1][i2][b]*H[p2][i2][i1]*H[p3][i1][a])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][a]*H[p2][i1][i2]*H[p3][i1][b])/T(4))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][i2]*H[p3][i1][b])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][a]*H[p2][i1][i2]*H[p3][i1][b])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p2][p2]*H[p1][i1][i2]*H[p2][i2][a]*H[p3][i1][b])/T(2))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*H[p3][i1][b])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term22(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(9)*pow3(kk[p2][p3])  *H[p1][i1][i2]*H[p2][i2][a]*H[p3][i1][b]
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][a]*H[p3][i1][b])/T(2)
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][a]*H[p3][i1][b])/T(2))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][a]*H[p3][i1][b])
+metric[i1]*metric[i2]*T(9)*kk[p2][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][a]*H[p3][i1][b]
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i2][i1]*H[p2][i2][a]*H[p3][i1][b])/T(4))
-(metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p1][p3]*H[p1][i2][i1]*H[p2][i2][a]*H[p3][i1][b])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*kk[p2][p2]*H[p1][i2][i1]*H[p2][i2][a]*H[p3][i1][b])/T(2))
+metric[i1]*metric[i2]*T(9)*pow3(kk[p2][p3])*H[p1][i2][i1]*H[p2][i2][a]*H[p3][i1][b]
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p3][p3]*H[p1][i2][i1]*H[p2][i2][a]*H[p3][i1][b])/T(2)
+(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i2][a]*H[p3][i1][b])/T(2)
-((metric[i1]*metric[i2]*T(9)*kk[p1][p3]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i2][a]*H[p3][i1][b])/T(4))
+metric[i1]*metric[i2]*T(9)*kk[p2][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i2][a]*H[p3][i1][b]
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p3]*H[p1][i2][a]*H[p2][i2][i1]*H[p3][i1][b])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i2][a]*H[p2][i2][i1]*H[p3][i1][b])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p3]*kk[p2][p2]*kk[p3][p3]*H[p1][i2][a]*H[p2][i2][i1]*H[p3][i1][b])/T(2))
-(metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p1][p3]*H[p1][i2][b]*H[p2][i1][a]*H[p3][i1][i2])
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p2][p3]*H[p1][i2][b]*H[p2][i1][a]*H[p3][i1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i2][b]*H[p2][i1][a]*H[p3][i1][i2])/T(4))
-((metric[i1]*metric[i2]*pow2(kk[p1][p3])*T(9)*kk[p1][p2]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i1][i2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term23(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i1][i2])/T(2))
-(metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p1][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i1][i2])
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p2][i2][a]*H[p3][i1][i2])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p3][p3]*H[p1][i1][b]*H[p2][i2][a]*H[p3][i1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p2]*kk[p3][p3]*H[p1][i1][b]*H[p2][i2][a]*H[p3][i1][i2])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p2][p3]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i1][i2])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p3][p3]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i1][i2])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(45)*kk[p1][p2]*H[p1][i1][i2]*H[p2][i1][b]*H[p3][i2][a])/T(4))
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p2][p2]*H[p1][i1][i2]*H[p2][i1][b]*H[p3][i2][a])/T(2)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][b]*H[p3][i2][a])/T(2))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][b]*H[p3][i2][a])
-((metric[i1]*metric[i2]*T(9)*kk[p1][p3]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][b]*H[p3][i2][a])/T(4))
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i2][i1]*H[p2][i1][b]*H[p3][i2][a])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p2][p2]*H[p1][i2][i1]*H[p2][i1][b]*H[p3][i2][a])/T(2)
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*H[p3][i2][a])/T(4))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*H[p3][i2][a])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term24(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][b]*H[p3][i2][a])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p1][p3])*T(9)*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][i2]*H[p3][i2][a])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][i2]*H[p3][i2][a])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p3]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][i2]*H[p3][i2][a])/T(4))
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i1][b]*H[p2][i2][i1]*H[p3][i2][a])
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p3]*H[p1][i1][b]*H[p2][i2][i1]*H[p3][i2][a])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i1][i2]*H[p2][i1][a]*H[p3][i2][b])/T(4))
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p2][p2]*H[p1][i1][i2]*H[p2][i1][a]*H[p3][i2][b])/T(2)
-(metric[i1]*metric[i2]*T(9)*pow3(kk[p2][p3])*H[p1][i1][i2]*H[p2][i1][a]*H[p3][i2][b])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][a]*H[p3][i2][b])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][a]*H[p3][i2][b])/T(2))
-((metric[i1]*metric[i2]*T(27)*kk[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][a]*H[p3][i2][b])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p3]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][a]*H[p3][i2][b])/T(4))
-(metric[i1]*metric[i2]*T(9)*kk[p2][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][a]*H[p3][i2][b])
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p2][p2]*H[p1][i2][i1]*H[p2][i1][a]*H[p3][i2][b])/T(2)
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*H[p3][i2][b])/T(4))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*H[p3][i2][b])
-(metric[i1]*metric[i2]*T(27)*pow3(kk[p2][p3])*H[p1][i2][i1]*H[p2][i1][a]*H[p3][i2][b])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][a]*H[p3][i2][b])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][a]*H[p3][i2][b])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term25(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(27)*kk[p2][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][a]*H[p3][i2][b])
-(metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][i2]*H[p3][i2][b])
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][i2]*H[p3][i2][b])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p3]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][i2]*H[p3][i2][b])/T(4))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*H[p2][i1][i2]*H[p3][i2][b])/T(2))
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i1][a]*H[p2][i2][i1]*H[p3][i2][b])
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p1][p3]*H[p1][i1][a]*H[p2][i2][i1]*H[p3][i2][b])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][a]*H[p2][i2][i1]*H[p3][i2][b])/T(2))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][b]*H[p2][i1][a]*H[p3][i2][i1])
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][b]*H[p2][i1][a]*H[p3][i2][i1])/T(4))
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p3][p3]*H[p1][i2][b]*H[p2][i1][a]*H[p3][i2][i1])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i2][i1])/T(4))
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p3][p3]*H[p1][i2][a]*H[p2][i1][b]*H[p3][i2][i1])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p2][p3]*H[p1][i1][b]*H[p2][i2][a]*H[p3][i2][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][b]*H[p2][i2][a]*H[p3][i2][i1])/T(4))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][b]*H[p2][i2][a]*H[p3][i2][i1])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*kk[p1][p2]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i2][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term26(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*kk[p2][p3]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i2][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p1]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i2][i1])/T(4))
-(metric[i1]*metric[i2]*T(9)*kk[p1][p2]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][a]*H[p2][i2][b]*H[p3][i2][i1])
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p1][i1])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p1][i1]
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p1][i1]
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p1][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p1][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p1][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p1][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p1][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p1][i2])/T(2)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p2]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1])
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term27(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(4))
+metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p3][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p1][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(27)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p2][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p2][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(4))
+metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p2][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p2][i1])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p2][i1]
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p2]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p2][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p2][i1])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term28(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p2][i1])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p1][p3]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p2]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p2][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][a]*H[p3][i2][i1]*Hk[p1][p2][i1])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][a]*H[p3][i2][i1]*Hk[p1][p2][i1]
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][a]*H[p3][i2][i1]*Hk[p1][p2][i1])
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p2][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p2][i1]
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p2][i1])
+(metric[i1]*metric[i2]*T(27)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p2][i2])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p2][i2])
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p2][i2]
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p2][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term29(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p2][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p2][i2])
+(metric[i1]*metric[i2]*T(27)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p2][i2])/T(2)
+metric[i1]*metric[i2]*T(18)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p2][i2]
+metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p2][i2]
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p2][i2]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p2][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p2][i2])
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p2][i2])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(18)*k[p3][b]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p2][i2]
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p2]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p2][i2]
+(metric[i1]*metric[i2]*T(45)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(4))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term210(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p2][i2])/T(4))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2]
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2]
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p3][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p1][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term211(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p2][i2])/T(4))
+(metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*k[p3][b]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p2]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][a]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*k[p3][a]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][b]*H[p3][i1][i2]*Hk[p1][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1])/T(4))
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1]
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][a]*Hk[p1][p3][i1])
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p3][i1])
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p2]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term212(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p3][i1])
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i2][a]*Hk[p1][p3][i1]
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(27)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i2][b]*Hk[p1][p3][i1])
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p2]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p3][i1])
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p3][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i2][b]*Hk[p1][p3][i1]
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][b]*H[p2][i2][a]*H[p3][i2][i1]*Hk[p1][p3][i1]
+metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][a]*H[p3][i2][i1]*Hk[p1][p3][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term213(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(18)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i2][a]*H[p3][i2][i1]*Hk[p1][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][a]*H[p3][i2][i1]*Hk[p1][p3][i1])/T(4)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][b]*H[p3][i2][i1]*Hk[p1][p3][i1])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p3][i2])
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p3][i2]
-(metric[i1]*metric[i2]*T(18)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p3][i2])/T(2))
+(metric[i1]*metric[i2]*T(27)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][a]*Hk[p1][p3][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p3][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p3][i2])/T(4))
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p3][i2]
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p3][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][a]*Hk[p1][p3][i2])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p3][i2]
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p3][i2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term214(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(18)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p3][i2]
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p3][i2])
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p3][i2])
+(metric[i1]*metric[i2]*T(27)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i1][i2]*H[p3][i1][b]*Hk[p1][p3][i2])/T(2)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p2][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p2][i2][i1]*H[p3][i1][b]*Hk[p1][p3][i2])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*k[p1][b]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p3][i2]
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p2]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][a]*H[p3][i1][i2]*Hk[p1][p3][i2])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*k[p1][a]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p3][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term215(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p1][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p3][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p2][i1][b]*H[p3][i1][i2]*Hk[p1][p3][i2])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*k[p3][b]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p3][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][a]*H[p3][i2][i1]*Hk[p1][p3][i2]
+(metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*k[p3][a]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p3][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p2][i1][b]*H[p3][i2][i1]*Hk[p1][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][a][i2]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p1][b][i2]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(4)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p1][i2][a]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][a]*H[p3][i1][i2]*Hk[p2][p1][i1])
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][a]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i2][a]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(4))
+metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p3][i1][i2]*Hk[p2][p1][i1]
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p1][i2][b]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][b]*H[p3][i1][i2]*Hk[p2][p1][i1])
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][b]*H[p3][i1][i2]*Hk[p2][p1][i1])/T(4))
+metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][b]*H[p3][i1][i2]*Hk[p2][p1][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term216(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p1][i1])/T(2)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p1][i1])/T(4))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p1][i1])/T(2))
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p1][i1])/T(2)
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p1][i1])
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p1][i1]
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p1][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i2][a]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][a]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][a]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][b]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][b]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][b]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][b]*H[p3][i2][i1]*Hk[p2][p1][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p1][i2]*Hk[p2][p1][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term217(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p1][i2]*Hk[p2][p1][i1])/T(2)
+(metric[i1]*metric[i2]*T(45)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(4)
+(metric[i1]*metric[i2]*T(45)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(4)
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(4)
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(4))
-((metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p3]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p1][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p1][i1])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1]
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term218(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i2]*Hk[p2][p1][i1]
-((metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p2]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i2]*Hk[p2][p1][i1])/T(2))
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p1][i2])/T(4)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*k[p2][b]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p1][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p1][i2]
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p1][i2])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p1][i2])/T(2))
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p1][i2])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p1][i2]
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p1][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p3][i1][i2]*Hk[p2][p1][i2]
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][i2]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][i2]*Hk[p2][p1][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*H[p3][i1][i2]*Hk[p2][p1][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*H[p3][i1][i2]*Hk[p2][p1][i2]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term219(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][i2]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][i2]*Hk[p2][p1][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][b]*H[p3][i1][i2]*Hk[p2][p1][i2])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p1][i1][a]*H[p3][i2][i1]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*H[p3][i2][i1]*Hk[p2][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*H[p3][i2][i1]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i2][i1]*Hk[p2][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i2][i1]*Hk[p2][p1][i2])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p1][i1][b]*H[p3][i2][i1]*Hk[p2][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i2][i1]*Hk[p2][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i2][i1]*Hk[p2][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p1][i1]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p1][i1]*Hk[p2][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term220(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p3]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p3][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p1][i2])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2])
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2]
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2])
+(metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p2]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p1][i2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term221(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][a][i2]*H[p3][i1][i2]*Hk[p2][p2][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][b][i2]*H[p3][i1][i2]*Hk[p2][p2][i1])/T(4))
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p2][i1])/T(4)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p2][i1])/T(2))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p2][i1])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p2][i1])/T(4)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p2][i1]
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p2][i1]
+metric[i1]*metric[i2]*T(27)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p3][i2][i1]*Hk[p2][p2][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][b]*H[p3][i2][i1]*Hk[p2][p2][i1])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p2][i1])
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term222(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p3][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p2][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p2][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p3][i2][i1]*Hk[p1][p3][i2]*Hk[p2][p2][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p3][i2][i1]*Hk[p1][p3][i2]*Hk[p2][p2][i1])
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p2][i2])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p2][i2])/T(4)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*k[p2][b]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p2][i2])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p2][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p2][i2]
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p2][i2])
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p2][i2])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term223(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*k[p2][a]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p2][i2])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p2][i2])
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p3][i1][i2]*Hk[p2][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i1][i2]*Hk[p2][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][b]*H[p3][i1][i2]*Hk[p2][p2][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p2][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p2][i2])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p2][i2])
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p2][i2])
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p2][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p2][i2])/T(2))
-(metric[i1]*metric[i2]*T(18)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p2][i2])
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][a]*H[p3][i1][i2]*Hk[p2][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][b]*H[p3][i1][i2]*Hk[p2][p3][i1])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][b]*H[p3][i1][i2]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term224(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(27)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1])
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(27)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][a]*Hk[p2][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p1]*kk[p1][p2]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p3][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(63)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i2][a]*Hk[p2][p3][i1])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1]
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(27)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term225(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(27)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i2][b]*Hk[p2][p3][i1])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*k[p2][a]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p3][i1]
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p1]*kk[p1][p2]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p3][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(63)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i2][b]*Hk[p2][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(4)
-((metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p2]*kk[p1][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(18)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i2]*Hk[p2][p3][i1])
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p3][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term226(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p3][i2][i1]*Hk[p1][p2][i2]*Hk[p2][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i2]*Hk[p2][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p3][i2]*Hk[p2][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p3][i2])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p3][i2])
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p3][i2]
-((metric[i1]*metric[i2]*T(27)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i1][a]*Hk[p2][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p3][i2])/T(4))
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p3][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(27)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p3][i2])/T(4)
-((metric[i1]*metric[i2]*T(27)*k[p2][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][a]*Hk[p2][p3][i2])/T(2))
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p3][i2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term227(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p3][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p3][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p3][i2]
-((metric[i1]*metric[i2]*T(27)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p3][i1][b]*Hk[p2][p3][i2])/T(2))
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2]
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][a]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(27)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2])/T(4)
-((metric[i1]*metric[i2]*T(27)*k[p2][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p3][i1][b]*Hk[p2][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p3][i1][i2]*Hk[p2][p3][i2])
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p1]*kk[p1][p2]*H[p1][i1][a]*H[p3][i2][i1]*Hk[p2][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p3][i2][i1]*Hk[p2][p3][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*H[p3][i2][i1]*Hk[p2][p3][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p3][i2][i1]*Hk[p2][p3][i2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term228(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p1][i1]*Hk[p2][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(18)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(18)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p3][i1][i2]*Hk[p1][p2][i1]*Hk[p2][p3][i2])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p3][i2])
+(metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p2]*kk[p1][p3]*H[p3][i2][i1]*Hk[p1][p2][i1]*Hk[p2][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p3][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p3][i1][i2]*Hk[p1][p3][i1]*Hk[p2][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p3][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p3][i2])/T(4))
-(metric[i1]*metric[i2]*T(18)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p3][i2])
-(metric[i1]*metric[i2]*T(18)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p3][i2][i1]*Hk[p1][p3][i1]*Hk[p2][p3][i2])
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p1][a][i2]*H[p2][i1][i2]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p1][b][i2]*H[p2][i1][i2]*Hk[p3][p1][i1])/T(2)
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p2][b]*H[p1][i2][a]*H[p2][i1][i2]*Hk[p3][p1][i1])
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][a]*H[p2][i1][i2]*Hk[p3][p1][i1]
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term229(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][i2]*Hk[p3][p1][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][i2]*Hk[p3][p1][i1]
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][i2]*Hk[p3][p1][i1])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][b]*H[p2][i1][i2]*Hk[p3][p1][i1]
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][b]*H[p2][i1][i2]*Hk[p3][p1][i1])/T(4)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p1][i1])/T(4))
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p1][i1])/T(4))
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p1][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p1][i1])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p2][i2][i1]*Hk[p3][p1][i1]
+metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][b]*H[p2][i2][i1]*Hk[p3][p1][i1]
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][b]*H[p2][i2][i1]*Hk[p3][p1][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p1][i2]*Hk[p3][p1][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p1][i2]*Hk[p3][p1][i1])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p1][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p1][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p1][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p3][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p1][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term230(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p1][i1])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(4))
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(4))
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(4))
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(2)
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p1][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i2]*Hk[p3][p1][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p1][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p1][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p1][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p1][i1])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p1][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p1][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term231(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p1][i1])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p1][i2]
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p1][i2]
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p1][i2])/T(4)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p1][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p1][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][i2]*Hk[p3][p1][i2])
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][a]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][i2]*Hk[p3][p1][i2])
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][b]*H[p2][i1][i2]*Hk[p3][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i1][a]*H[p2][i2][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i2][i1]*Hk[p3][p1][i2])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term232(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p1][i1][b]*H[p2][i2][i1]*Hk[p3][p1][i2]
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*H[p2][i2][i1]*Hk[p3][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][b]*H[p2][i2][i1]*Hk[p3][p1][i2])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p1][i1]*Hk[p3][p1][i2]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p1][i1]*Hk[p3][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p3][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p1][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term233(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p1][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(4))
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(4)
-(metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p2]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p1][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p1][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p1][i2])
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p1][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p1][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p1][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p1][i2])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term234(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p1][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p2][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p2][i1])/T(4))
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1]
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][b]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p2][i1])/T(4)
-(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][b]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p2][i1])
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term235(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p2]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(18)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p2][i1]
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p2][i1])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p2][i1]
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][a]*H[p2][i2][i1]*Hk[p3][p2][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term236(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p2][i1]
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p2][i1]
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p2][i1]
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p2][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p2][i1])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p2][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i2]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i2]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term237(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i2]*Hk[p3][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
+(metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(4))
-((metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term238(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(4)
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1])
+metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p2][i1]
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][b]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p2][i2]
+(metric[i1]*metric[i2]*T(27)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p2][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p2][i2])/T(4)
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2]
+metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*k[p3][b]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2]
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p2]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term239(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(27)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(27)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p2][i2])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p2][i2])
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p2][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p2][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p2][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2]
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term240(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p2][p3]*kk[p3][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p2][i2])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p2][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p2][i2])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p2][i2])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p2][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p2][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p2][i2]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i1]*Hk[p3][p2][i2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term241(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i1]*Hk[p3][p2][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i1]*Hk[p3][p2][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i1]*Hk[p3][p2][i2])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p3][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p2][i2])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term242(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p2][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(2)
-((metric[i1]*metric[i2]*T(3)*g[a][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][i2])
-(metric[i1]*metric[i2]*T(27)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][i2])
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p2][b]*kk[p3][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p2][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][a][i2]*Hk[p3][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][b][i2]*Hk[p3][p3][i1])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p1][p3]*H[p1][a][i2]*H[p2][i1][i2]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][b][i2]*H[p2][i1][i2]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][a]*H[p2][i1][i2]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p3][i1])/T(4)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][b]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p3][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term243(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p3][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p3][i1])/T(2)
-(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][a]*Hk[p3][p3][i1])
+metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*k[p1][b]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p3][i1]
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][b]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p2]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p3][i1])/T(4)
-(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][a]*Hk[p3][p3][i1])
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p3][i1])/T(4)
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p3][i1])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i2][b]*Hk[p3][p3][i1])/T(2)
+metric[i1]*metric[i2]*pow2(kk[p1][p2])*T(9)*k[p1][a]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p3][i1]
-((metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i2][b]*Hk[p3][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][a]*H[p2][i2][i1]*Hk[p3][p3][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term244(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p3]*kk[p2][p2]*H[p1][i2][a]*H[p2][i2][i1]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(18)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p2][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p3][i1])
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p3][i1])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p3][i1]
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i2]*Hk[p3][p3][i1])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(18)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1]
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1]
+metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1]
-((metric[i1]*metric[i2]*T(27)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term245(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(45)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p2]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(4)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(4)
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i2]*Hk[p3][p3][i1])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i2]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i2]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i2]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i2]*Hk[p3][p3][i1]
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p2][i2]*Hk[p3][p3][i1])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term246(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i2]*Hk[p3][p3][i1])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i2]*Hk[p3][p3][i1]
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(4)
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(4))
+(metric[i1]*metric[i2]*T(27)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2)
+(metric[i1]*metric[i2]*T(27)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p2]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(4))
+(metric[i1]*metric[i2]*T(27)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2)
-((metric[i1]*metric[i2]*T(27)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(4))
+(metric[i1]*metric[i2]*T(27)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i2]*Hk[p3][p3][i1])/T(2)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term247(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][b]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p2]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p1]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2))
+(metric[i1]*metric[i2]*T(27)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p3][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p3][i2])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][a]*Hk[p3][p3][i2]
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][b]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p3][i2])/T(4)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*k[p3][b]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p3][i2])/T(2)
+metric[i1]*metric[i2]*T(27)*k[p3][b]*kk[p2][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][a]*Hk[p3][p3][i2]
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p3][a]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p3][i2])/T(2))
+metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p3][i2]
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p3][i2])/T(4))
+(metric[i1]*metric[i2]*T(9)*k[p3][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i1][i2]*H[p2][i1][b]*Hk[p3][p3][i2])/T(4)
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term248(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(9)*k[p1][a]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p3][i2])/T(4)
+(metric[i1]*metric[i2]*pow2(kk[p2][p3])*T(27)*k[p3][a]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p2]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*kk[p1][p3]*kk[p2][p3]*H[p1][i2][i1]*H[p2][i1][b]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p2][i1][i2]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][a]*H[p2][i1][i2]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*kk[p1][p2]*kk[p1][p3]*H[p1][i1][b]*H[p2][i1][i2]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p3][b]*kk[p1][p2]*kk[p2][p3]*H[p1][i1][a]*H[p2][i2][i1]*Hk[p3][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p3]*H[p2][i1][i2]*Hk[p1][p2][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p2][i1]*Hk[p3][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term249(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(27)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p2]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(4)
+metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i1][i2]*Hk[p1][p3][i1]*Hk[p3][p3][i2]
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p3][i2])
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(4)
-((metric[i1]*metric[i2]*T(45)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p3][i2])/T(4))
-(metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p3][i2])
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p2][i2][i1]*Hk[p1][p3][i1]*Hk[p3][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p3][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p1][i1]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p1][i1]*Hk[p3][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p2][i1]*Hk[p3][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p3][i2])/T(2))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term250(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p3][i2])
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p3][i2])/T(2))
-(metric[i1]*metric[i2]*T(27)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p2][i1]*Hk[p3][p3][i2])
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2)
+metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(9)*k[p2][b]*k[p3][a]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2]
+metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2]
+(metric[i1]*metric[i2]*T(9)*k[p2][a]*k[p3][b]*kk[p1][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p2]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(27)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i1][i2]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
+(metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2)
+(metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2)
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p3][a]*kk[p1][p2]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p1][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(9)*k[p1][b]*k[p2][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(4))
-((metric[i1]*metric[i2]*T(9)*k[p1][a]*k[p2][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(4))
;
      return theentry;
}

template <typename T, size_t D>
   T CurrentRule_R3_GGGG_Term251(
       const std::vector<std::vector<std::vector<T>>>& Hkk,
       const std::vector<std::vector<std::vector<T>>>& HHH,
       const std::vector<std::vector<T>>& HH,
       const std::vector<std::vector<Tensor<T, D, 1>>>& Hk,
       const std::vector<std::vector<T>>& kk,
       const Tensor<T, D, 2>& g,
       const std::vector<T>& trH,
       const std::vector<Tensor<T, D, 2>>& H,
       const std::vector<T>& metric,
       const std::vector<momD<T, D>>& k,
       size_t a, size_t b,size_t i1, size_t i2, 
       size_t p1, size_t p2, size_t p3){
        T theentry(0);
        theentry =
   
-((metric[i1]*metric[i2]*T(63)*k[p2][b]*k[p3][a]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
-((metric[i1]*metric[i2]*T(63)*k[p2][a]*k[p3][b]*kk[p2][p3]*H[p1][i2][i1]*Hk[p2][p3][i1]*Hk[p3][p3][i2])/T(2))
;
      return theentry;
}



// end terms

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CurrentRule_R3_GGGG(const std::vector<momentumD<T, D> const*>& momlist,
                                      const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {

    // incoming momenta
    std::vector<momD<T, D>> k;
    k.emplace_back(-(*(momlist[0])));
    k.emplace_back(-(*(momlist[1])));
    k.emplace_back(-(*(momlist[2])));

    // the input gravitons
    std::vector<Tensor<T, D, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[0]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[2]->get_current()));

    // metric
    Tensor<T, D, 2> hout(0), g(0);
    const std::vector<T>& metric(hout.get_metric_extended());
    for (size_t ii = 0; ii < D; ii++) { g[ii][ii] = T(1)/metric[ii]; }

    // tensor contractions
    std::vector<std::vector<Tensor<T, D, 1>>> Hk;
    for (size_t p1 = 0; p1 < 3; p1++) {
        std::vector<Tensor<T, D, 1>> fill_Hk;
        for (size_t p2 = 0; p2 < 3; p2++) { fill_Hk.emplace_back(index_contraction<0>(k[p2], H[p1])); }
        Hk.emplace_back(fill_Hk);
    }

    // tensor contractions
    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array3D Hkk(3, array2D(3, array1D(3, T(0))));
    array3D HHH(3, array2D(3, array1D(3, T(0))));
    array2D HH(3, array1D(3, T(0)));
    array2D kk(3, array1D(3, T(0)));
    array1D trH(3, T(0));

    for (size_t p1 = 0; p1 < 3; p1++) {
        for (size_t ii = 0; ii < D; ii++) { trH[p1] += H[p1][ii][ii] * metric[ii]; }

        for (size_t p2 = 0; p2 < 3; p2++) {
            for (size_t ii = 0; ii < D; ii++) {
                kk[p1][p2] += k[p1][ii] * k[p2][ii] * metric[ii];
                for (size_t jj = 0; jj < D; jj++) { HH[p1][p2] += H[p1][ii][jj] * H[p2][ii][jj] * metric[ii] * metric[jj]; }
            }
            for (size_t p3 = 0; p3 < 3; p3++) {
                for (size_t ii = 0; ii < D; ii++) {
                    Hkk[p1][p2][p3] += Hk[p1][p2][ii] * k[p3][ii] * metric[ii];

                    for (size_t jj = 0; jj < D; jj++) {
                        for (size_t kk = 0; kk < D; kk++) {
                            HHH[p1][p2][p3] += H[p1][ii][jj] * H[p2][jj][kk] * H[p3][kk][ii] * metric[ii] * metric[jj] * metric[kk];
                        }
                    }
                }
            }
        }
    }

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 1, 0}, {2, 0, 1}};

    for (size_t a = 0; a < D; a++) {
        for (size_t b = 0; b < D; b++) {

            T theentry(0);

            for (size_t p = 0; p < 6; p++) {
                size_t p1 = perm[p][0];
                size_t p2 = perm[p][1];
                size_t p3 = perm[p][2];

                for (size_t i1 = 0; i1 < D; i1++) {

                theentry += CurrentRule_R3_GGGG_Term11<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term12<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term13<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term14<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term15<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term16<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term17<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term18<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term19<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term110<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);

				theentry += CurrentRule_R3_GGGG_Term111<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term112<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term113<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term114<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term115<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term116<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term117<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term118<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term119<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term120<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);

				theentry += CurrentRule_R3_GGGG_Term121<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term122<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term123<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term124<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term125<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term126<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term127<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term128<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term129<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term130<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);

				theentry += CurrentRule_R3_GGGG_Term131<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term132<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term133<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term134<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term135<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term136<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term137<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term138<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term139<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term140<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);



				theentry += CurrentRule_R3_GGGG_Term141<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term142<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term143<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term144<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term145<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term146<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term147<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term148<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term149<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term150<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);

				theentry += CurrentRule_R3_GGGG_Term151<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term152<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term153<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term154<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term155<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term156<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term157<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term158<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term159<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term160<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, p1, p2, p3);



                }
                for (size_t i1 = 0; i1 < D; i1++) {
                    for (size_t i2 = 0; i2 < D; i2++) {

                theentry += CurrentRule_R3_GGGG_Term21<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term22<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term23<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term24<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term25<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term26<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term27<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term28<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term29<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term210<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);

				theentry += CurrentRule_R3_GGGG_Term211<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term212<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term213<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term214<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term215<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term216<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term217<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term218<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term219<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term220<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);


				theentry += CurrentRule_R3_GGGG_Term221<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term222<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term223<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term224<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term225<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term226<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term227<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term228<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term229<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term230<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);


				theentry += CurrentRule_R3_GGGG_Term231<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term232<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term233<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term234<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term235<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term236<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term237<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term238<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term239<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term240<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);


				theentry += CurrentRule_R3_GGGG_Term241<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term242<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term243<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term244<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term245<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term246<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term247<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term248<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term249<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				theentry += CurrentRule_R3_GGGG_Term250<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);
				
				theentry += CurrentRule_R3_GGGG_Term251<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, i1, i2, p1, p2, p3);


                 }
                }


                theentry += CurrentRule_R3_GGGG_Term01<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term02<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term03<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term04<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term05<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term06<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term07<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term08<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term09<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term010<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);

				theentry += CurrentRule_R3_GGGG_Term011<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term012<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term013<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term014<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term015<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term016<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term017<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term018<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term019<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term020<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);

				theentry += CurrentRule_R3_GGGG_Term021<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term022<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);
                theentry += CurrentRule_R3_GGGG_Term023<T,D>(Hkk, HHH, HH, Hk, kk, g, trH, H, metric, k, a, b, p1, p2, p3);





            }

            hout[a][b] = theentry;
        }
    }

    // factor of I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
    // hout*= T(0,1);
    //hout *= T(-1) / T(6);
	// Lagrangian =  - 2 sqrt(g) RicciScalar 
    // hout *= T(1) / T(6) ;
    hout *= T(1) / T(3) ;

    return v_t_to_v_extended_t<T, Tcur>(hout.vectorize());
}



/* end R3 */

#endif

} // namespace BG
} // namespace Caravel

namespace Caravel { namespace BG {

#define _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                     \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CurrentRule_EH_GGG<T, Current_Type_Selector<T>::Tcur, D>(                                         \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&);

#define _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                    \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CurrentRule_EH_GGGG<T, Current_Type_Selector<T>::Tcur, D>(                                        \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&);

#define _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                   \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CurrentRule_EH_GGGGG<T, Current_Type_Selector<T>::Tcur, D>(                                       \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&);

#define _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                          \
    _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                         \
    _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                         \
    _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                         \
    _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                         \
    _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                         \
    _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                         \
    _INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#define _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                         \
    _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                        \
    _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                        \
    _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                        \
    _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                        \
    _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                        \
    _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                        \
    _INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#define _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                        \
    _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                       \
    _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                       \
    _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                       \
    _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                       \
    _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                       \
    _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                       \
    _INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#define _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                     \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CurrentRule_GB_GGG<T, Current_Type_Selector<T>::Tcur, D>(                                         \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&);

#define _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                    \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CurrentRule_GB_GGGG<T, Current_Type_Selector<T>::Tcur, D>(                                        \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&);

#define _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(KEY,T) \
    _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS_D(KEY,T,4) \
    _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS_D(KEY,T,5) \
    _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS_D(KEY,T,6) \
    _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS_D(KEY,T,7) \
    _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS_D(KEY,T,8) \
    _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS_D(KEY,T,9) \
    _INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS_D(KEY,T,10) \

#define _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(KEY,T) \
    _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,4) \
    _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,5) \
    _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,6) \
    _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,7) \
    _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,8) \
    _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,9) \
    _INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,10) \

#define _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS_D(KEY,T,D) \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CurrentRule_R3_GGG<T, Current_Type_Selector<T>::Tcur, D>(const std::vector<momentumD<T, D> const*>&,const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&); \

#define _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,D) \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CurrentRule_R3_GGGG<T, Current_Type_Selector<T>::Tcur, D>(const std::vector<momentumD<T, D> const*>&,const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&); \

#define _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(KEY,T) \
    _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS_D(KEY,T,4) \
    _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS_D(KEY,T,5) \
    _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS_D(KEY,T,6) \
    _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS_D(KEY,T,7) \
    _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS_D(KEY,T,8) \
    _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS_D(KEY,T,9) \
    _INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS_D(KEY,T,10) \

#define _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(KEY,T) \
    _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,4) \
    _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,5) \
    _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,6) \
    _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,7) \
    _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,8) \
    _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,9) \
    _INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS_D(KEY,T,10) \

#ifdef INCLUDE_EH_GRAVITY

_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,R)
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,C)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,R)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,C)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,R)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,C)

#ifdef HIGH_PRECISION
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,RHP)
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,CHP)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,RHP)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,CHP)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,RHP)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,CHP)
#endif

#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,RVHP)
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,CVHP)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,RVHP)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,CVHP)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,RVHP)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,CVHP)
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,RGMP)
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,CGMP)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,RGMP)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,CGMP)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,RGMP)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,CGMP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,F32)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,F32)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_EH_GGG_GRAVITY_FUNCTIONS(extern,BigRat)
_INSTANTIATE_EH_GGGG_GRAVITY_FUNCTIONS(extern,BigRat)
_INSTANTIATE_EH_GGGGG_GRAVITY_FUNCTIONS(extern,BigRat)
#endif
#endif

#endif

#ifdef INCLUDE_GB_GRAVITY

_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,R)
_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,C)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,R)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,C)

#ifdef HIGH_PRECISION
_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,RHP)
_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,CHP)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,RHP)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,CHP)
#endif

#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,RVHP)
_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,CVHP)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,RVHP)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,CVHP)
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,RGMP)
_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,CGMP)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,RGMP)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,CGMP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_GB_GGG_GRAVITY_FUNCTIONS(extern,F32)
_INSTANTIATE_GB_GGGG_GRAVITY_FUNCTIONS(extern,F32)
#endif

_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,R)
_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,C)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,R)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,C)

#ifdef HIGH_PRECISION
_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,RHP)
_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,CHP)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,RHP)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,CHP)
#endif

#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,RVHP)
_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,CVHP)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,RVHP)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,CVHP)
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,RGMP)
_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,CGMP)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,RGMP)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,CGMP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_R3_GGG_GRAVITY_FUNCTIONS(extern,F32)
_INSTANTIATE_R3_GGGG_GRAVITY_FUNCTIONS(extern,F32)
#endif

#endif



}}
