#include "Core/Utilities.h"
#include "Core/extended_t.h"
#include "Core/momD_conf.h"
#include "Core/type_traits_extra.h"
#include "Tensor.hpp"
#include <map>

namespace Caravel{ namespace BG{


// NOTICE: Since we do not use symetrized vertices, the Propagators and the external states have to be explicity symmetrized


/**
 * Graviton state in 4D with negative helicity (minus-minus)
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_tensorboson_mm(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
 	// the corresponding polarization vector
 	// NOTICE: normalization is being inherited from externalWF_vectorboson_m, this includes a factored out 1/sqrt(2) which
 	//	should be reintroduced with Builder_fixed_Ds::get_overall_normalization_info(.) (TODO!)
 	std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_m<T,Tcur,D>(momconf,lmom,refmom)));

 	// this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
 	Tensor<T,D,2> hmm;

 	// fill it
 	for(size_t mu=0;mu<D;mu++)
 		hmm[mu][mu]=epsm[mu]*epsm[mu];
 	for(size_t mu=0;mu<D;mu++)
 		for(size_t nu=mu+1;nu<D;nu++){
 			T emuenu(epsm[mu]*epsm[nu]);
 			hmm[mu][nu]=emuenu;
 			hmm[nu][mu]=emuenu;
 		}
 //std::cout<<"h--= "<<hmm<<std::endl;

 	// transform to canonical and return
 	return v_t_to_v_extended_t<T,Tcur>(hmm.vectorize());
}

 /**
  * Graviton state in 4D with positive helicity (plus-plus)
  */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_tensorboson_pp(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
 	// the corresponding polarization vector
 	// NOTICE: normalization is being inherited from externalWF_vectorboson_m, this includes a factored out 1/sqrt(2) which
 	//	should be reintroduced with Builder_fixed_Ds::get_overall_normalization_info(.) (TODO!)
 	std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_p<T,Tcur,D>(momconf,lmom,refmom)));

 	// this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
 	Tensor<T,D,2> hpp;

 	// fill it
 	for(size_t mu=0;mu<D;mu++)
 		hpp[mu][mu]=epsp[mu]*epsp[mu];
 	for(size_t mu=0;mu<D;mu++)
 		for(size_t nu=mu+1;nu<D;nu++){
 			T emuenu(epsp[mu]*epsp[nu]);
 			hpp[mu][nu]=emuenu;
 			hpp[nu][mu]=emuenu;
 		}
 //std::cout<<"h++= "<<hpp<<std::endl;

 	// transform to canonical and return
 	return v_t_to_v_extended_t<T,Tcur>(hpp.vectorize());
}

 /**
  * Graviton DUMMY state: this function is temporary and should be removed with proper D-dim implementations
  */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_tensorboson_dummy(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
    std::cout<<"ERROR: using dummy tensor external wave function!"<<std::endl;
    return std::vector<Tcur>({Tcur(0)});
}


 // TODO use that polarization tensors have zero blocks

 // Notice: pp and mm Polarizations are identical to D dimensional ones

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_mmD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    // the corresponding polarization vector
    // NOTICE: normalization is being inherited from externalWF_vectorboson_m, this includes a factored out 1/sqrt(2) which
    //	should be reintroduced with Builder_fixed_Ds::get_overall_normalization_info(.) (TODO!)
    std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hmm;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hmm[mu][mu]=epsm[mu]*epsm[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsm[mu]*epsm[nu]);
    		hmm[mu][nu]=emuenu;
    		hmm[nu][mu]=emuenu;
    	}

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hmm.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_ppD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    // the corresponding polarization vector
    // NOTICE: normalization is being inherited from externalWF_vectorboson_m, this includes a factored out 1/sqrt(2) which
    //	should be reintroduced with Builder_fixed_Ds::get_overall_normalization_info(.) (TODO!)
    std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hpp;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hpp[mu][mu]=epsp[mu]*epsp[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsp[mu]*epsp[nu]);
    		hpp[mu][nu]=emuenu;
    		hpp[nu][mu]=emuenu;
    	}

    hpp*=T(-2);

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hpp.vectorize());
}

template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_tensorboson_mmcD(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& refmom) {
    std::vector<Tcur> out = externalWF_tensorboson_ppD<T, Tcur, Ds>(momconf, lmom, refmom);
    for (size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii] / T(-2);
    return out;
}

template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_tensorboson_ppcD(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& refmom) {
    std::vector<Tcur> out = externalWF_tensorboson_mmD<T, Tcur, Ds>(momconf, lmom, refmom);
    for (size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii] / T(-2);
    return out;
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_msD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_sD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hmI;

    // fill it
    for(size_t mu=0;mu<Ds;mu++) hmI[mu][mu]=T(2)*epsm[mu]*epsI[mu];

    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsm[mu]*epsI[nu]+epsm[nu]*epsI[mu]);
    		hmI[mu][nu]=emuenu;
    		hmI[nu][mu]=emuenu;
    	}

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hmI.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_mtD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hmI;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hmI[mu][mu]=T(2)*epsm[mu]*epsI[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsm[mu]*epsI[nu]+epsm[nu]*epsI[mu]);
    		hmI[mu][nu]=emuenu;
    		hmI[nu][mu]=emuenu;
    	}

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hmI.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_psD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_sD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hpI;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hpI[mu][mu]=T(2)*epsp[mu]*epsI[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsp[mu]*epsI[nu]+epsp[nu]*epsI[mu]);
    		hpI[mu][nu]=emuenu;
    		hpI[nu][mu]=emuenu;
    	}

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hpI.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_ptD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hpI;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hpI[mu][mu]=T(2)*epsp[mu]*epsI[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsp[mu]*epsI[nu]+epsp[nu]*epsI[mu]);
    		hpI[mu][nu]=emuenu;
    		hpI[nu][mu]=emuenu;
    	}

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hpI.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_stD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_sD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsJ(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hIJ;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hIJ[mu][mu]=T(2)*epsI[mu]*epsJ[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsI[mu]*epsJ[nu]+epsJ[nu]*epsI[mu]);
    		hIJ[mu][nu]=emuenu;
    		hIJ[nu][mu]=emuenu;
    	}

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hIJ.vectorize());
}



template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_ssD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
  static_assert(Ds>=5,"Dimensional mismatch");

  momD<T,Ds> ml(-momconf.p(lmom));
  momD_conf<T,Ds> mconf_aux;

  auto mlmom = mconf_aux.insert(ml);

  size_t ref(0);
  if(refmom != 0){
    ref = mconf_aux.insert(momconf.p(refmom));
  }

  std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
  std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));
  std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_sD<T,Tcur,Ds>(momconf,lmom,refmom)));

  std::vector<T> epsmc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  std::vector<T> epspc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  std::vector<T> epsIc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_scD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));

  // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
  Tensor<T,Ds,2> hII;

  // fill it
  for(size_t mu=0;mu<Ds;mu++)
   hII[mu][mu]=epsI[mu]*epsIc[mu]-T(1)/T(2)*(epsp[mu]*epspc[mu]+epsp[mu]*epspc[mu]);
  for(size_t mu=0;mu<Ds;mu++)
   for(size_t nu=mu+1;nu<Ds;nu++){
     T emuenu(epsI[mu]*epsIc[nu]-T(1)/T(2)*(epsp[nu]*epspc[mu]+epsp[mu]*epspc[nu]));
     hII[mu][nu]=emuenu;
     hII[nu][mu]=emuenu;
   }

  // transform to canonical and return
  return v_t_to_v_extended_t<T,Tcur>(hII.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_ttD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
  static_assert(Ds>=5,"Dimensional mismatch");

  momD<T,Ds> ml(-momconf.p(lmom));
  momD_conf<T,Ds> mconf_aux;

  auto mlmom = mconf_aux.insert(ml);
  size_t ref(0);
  if(refmom != 0){
    ref = mconf_aux.insert(momconf.p(refmom));
  }


  std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
  std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));
  std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_sD<T,Tcur,Ds>(momconf,lmom,refmom)));
  std::vector<T> epsJ(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tD<T,Tcur,Ds>(momconf,lmom,refmom)));

  std::vector<T> epsmc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  std::vector<T> epspc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  std::vector<T> epsIc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_scD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  std::vector<T> epsJc(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
  Tensor<T,Ds,2> hII;

  // fill it
  for(size_t mu=0;mu<Ds;mu++)
    hII[mu][mu]=T(3)/T(2)*epsJ[mu]*epsJc[mu]-T(1)/T(2)*epsI[mu]*epsIc[mu]-T(1)/T(2)*(epsm[mu]*epsmc[mu]+epsp[mu]*epspc[mu]);
  for(size_t mu=0;mu<Ds;mu++)
    for(size_t nu=mu+1;nu<Ds;nu++){
      T emuenu(T(3)/T(2)*epsJ[mu]*epsJc[nu]-T(1)/T(2)*epsI[mu]*epsIc[nu]-T(1)/T(2)*(epsm[mu]*epsmc[nu]+epsp[mu]*epspc[nu]));
      hII[mu][nu]=emuenu;
      hII[nu][mu]=emuenu;
    }

  // transform to canonical and return
  return v_t_to_v_extended_t<T,Tcur>(hII.vectorize());
}

// NOTICE: All conjugate states are scaled by a factor of 2 due to the overall factor of 2 in the cut numerator

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_mscD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_scD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hmI;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hmI[mu][mu]=T(2)*epsm[mu]*epsI[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsm[mu]*epsI[nu]+epsm[nu]*epsI[mu]);
    		hmI[mu][nu]=emuenu;
    		hmI[nu][mu]=emuenu;
    	}
    hmI *=T(-1);

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hmI.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_mtcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hmI;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hmI[mu][mu]=T(2)*epsm[mu]*epsI[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsm[mu]*epsI[nu]+epsm[nu]*epsI[mu]);
    		hmI[mu][nu]=emuenu;
    		hmI[nu][mu]=emuenu;
    	}
    hmI *=T(-1);

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hmI.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_pscD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_scD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hpI;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hpI[mu][mu]=T(2)*epsp[mu]*epsI[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsp[mu]*epsI[nu]+epsp[nu]*epsI[mu]);
    		hpI[mu][nu]=emuenu;
    		hpI[nu][mu]=emuenu;
    	}

    hpI *=T(-1);

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hpI.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_ptcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hpI;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hpI[mu][mu]=T(2)*epsp[mu]*epsI[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsp[mu]*epsI[nu]+epsp[nu]*epsI[mu]);
    		hpI[mu][nu]=emuenu;
    		hpI[nu][mu]=emuenu;
    	}
      hpI *=T(-1);

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hpI.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_stcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_scD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsJ(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,Ds>(momconf,lmom,refmom)));

    // this step can be skipped for efficiency (constructing directly the vector to output), but we leave it here for ease of reading
    Tensor<T,Ds,2> hIJ;

    // fill it
    for(size_t mu=0;mu<Ds;mu++)
    	hIJ[mu][mu]=T(2)*epsI[mu]*epsJ[mu];
    for(size_t mu=0;mu<Ds;mu++)
    	for(size_t nu=mu+1;nu<Ds;nu++){
    		T emuenu(epsI[mu]*epsJ[nu]+epsJ[nu]*epsI[mu]);
    		hIJ[mu][nu]=emuenu;
    		hIJ[nu][mu]=emuenu;
    	}

    hIJ *= T(-1)/T(4);

    // transform to canonical and return
    return v_t_to_v_extended_t<T,Tcur>(hIJ.vectorize());
}

#if 0
template <typename T, typename Tcur, size_t Ds> std::enable_if_t< !is_complex<T>::value,std::vector<Tcur>> externalWF_tensorboson_mscD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    const momD<T,Ds>& l(momconf.p(lmom));
    Tensor<Tcur,Ds,2> hmI(externalWF_tensorboson_psD<T,Tcur,Ds>(momconf,lmom,refmom));
    hmI *=T(1)/(compute_Dsm4_prod(l,l));
    return hmI.vectorize();
}


template <typename T, typename Tcur, size_t Ds> std::enable_if_t< is_complex<T>::value,std::vector<Tcur>>  externalWF_tensorboson_mscD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    const momD<T,Ds>& l(momconf.p(lmom));
    Tensor<Tcur,Ds,2> hmI(externalWF_tensorboson_psD<T,Tcur,Ds>(momconf,lmom,refmom));
    hmI *=T(-1)/(compute_Dsm4_prod(l,l));
    return hmI.vectorize();
}


template <typename T, typename Tcur, size_t Ds> std::enable_if_t<!is_complex<T>::value,std::vector<Tcur>>  externalWF_tensorboson_mtcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    const momD<T,Ds>& l(momconf.p(lmom));
    Tensor<Tcur,Ds,2> hmI(externalWF_tensorboson_ptD<T,Tcur,Ds>(momconf,lmom,refmom));
    hmI *=T(-1)/(compute_Dsm4_prod(l,l));
    if(typeid(T)==typeid(F32)){hmI*=T(1)/(EpsilonBasis<T>::squares[0]*EpsilonBasis<T>::squares[1]);}
    return hmI.vectorize();
}

template <typename T, typename Tcur, size_t Ds> std::enable_if_t<is_complex<T>::value,std::vector<Tcur>> externalWF_tensorboson_mtcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    const momD<T,Ds>& l(momconf.p(lmom));
    Tensor<Tcur,Ds,2> hmI(externalWF_tensorboson_ptD<T,Tcur,Ds>(momconf,lmom,refmom));
    hmI *=T(-1)/(compute_Dsm4_prod(l,l));
    return hmI.vectorize();
}

template <typename T, typename Tcur, size_t Ds> std::enable_if_t<!is_complex<T>::value,std::vector<Tcur>> externalWF_tensorboson_pscD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    const momD<T,Ds>& l(momconf.p(lmom));
    Tensor<Tcur,Ds,2> hpI(externalWF_tensorboson_msD<T,Tcur,Ds>(momconf,lmom,refmom));
    hpI *=T(1)/(compute_Dsm4_prod(l,l));
    //if(typeid(T)==typeid(F32)){hpI*=T(1)/EpsilonBasis<T>::squares[1];cout << "ssss"<<endl;}
    return hpI.vectorize();
}

template <typename T, typename Tcur, size_t Ds> std::enable_if_t<is_complex<T>::value,std::vector<Tcur>> externalWF_tensorboson_pscD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    const momD<T,Ds>& l(momconf.p(lmom));
   	Tensor<Tcur,Ds,2> hpI(externalWF_tensorboson_msD<T,Tcur,Ds>(momconf,lmom,refmom));
   	hpI *=T(-1)/(compute_Dsm4_prod(l,l));
   	return hpI.vectorize();
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_ptcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    const momD<T,Ds>& l(momconf.p(lmom));
    Tensor<Tcur,Ds,2> hpI(externalWF_tensorboson_mtD<T,Tcur,Ds>(momconf,lmom,refmom));
    hpI *=T(-1)/(compute_Dsm4_prod(l,l));
    if(typeid(T)==typeid(F32)){hpI*=T(1)/(EpsilonBasis<T>::squares[0]*EpsilonBasis<T>::squares[1]);}
    return hpI.vectorize();
}

#endif

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_sscD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
   static_assert(Ds>=5,"Dimensional mismatch");
   Tensor<T,Ds,2> hII(v_extended_t_to_v_t<T,Tcur>(externalWF_tensorboson_ssD<T,Tcur,Ds>(momconf,lmom,refmom)));
   hII *= T(-4)/T(3);

   return v_t_to_v_extended_t<T,Tcur>(hII.vectorize());
}

// template <typename T, typename Tcur, size_t Ds> std::enable_if_t<!is_complex<T>::value,std::vector<Tcur>> externalWF_tensorboson_stcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
//     static_assert(Ds>=5,"Dimensional mismatch");
//     Tensor<Tcur,Ds,2> hIJ(externalWF_tensorboson_stD<T,Tcur,Ds>(momconf,lmom,refmom));
//     hIJ *= T(1)/T(4);
//     if(typeid(T)==typeid(F32)){hIJ*=EpsilonBasis<T>::squares[0]*EpsilonBasis<T>::squares[1];}
//     return hIJ.vectorize();
// }
//
// template <typename T, typename Tcur, size_t Ds> std::enable_if_t<is_complex<T>::value,std::vector<Tcur>> externalWF_tensorboson_stcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
//     static_assert(Ds>=5,"Dimensional mismatch");
//     Tensor<Tcur,Ds,2> hIJ(externalWF_tensorboson_stD<T,Tcur,Ds>(momconf,lmom,refmom));
//  	  hIJ *= T(-1)/T(4);
//  	  return hIJ.vectorize();
// }

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_tensorboson_ttcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
 	Tensor<T,Ds,2> hII(v_extended_t_to_v_t<T,Tcur>(externalWF_tensorboson_ttD<T,Tcur,Ds>(momconf,lmom,refmom)));
 	hII *= T(-2)/T(3);

 	return v_t_to_v_extended_t<T,Tcur>(hII.vectorize());
}

template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind> std::vector<Tcur> externalWF_tensorboson_mID(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
        static_assert(Ds>=5,"Dimensional mismatch");

 		if(Dsm4Ind == 3){
 			return externalWF_tensorboson_msD<T,Tcur,Ds>(momconf,lmom,refmom);
 		}
 		if(Dsm4Ind == 4){
 			return externalWF_tensorboson_mtD<T,Tcur,Ds>(momconf,lmom,refmom);
 		}

 		std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
 		Tensor<T,Ds,2> hmI(0);

 		for(size_t mu=0;mu<Ds;mu++){
 			hmI[Dsm4Ind+1][mu]=epsm[mu];
 			hmI[mu][Dsm4Ind+1]=epsm[mu];
 		}

 		return v_t_to_v_extended_t<T,Tcur>(hmI.vectorize());
}

template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind> std::vector<Tcur> externalWF_tensorboson_pID(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    if(Dsm4Ind == 3){
   	  return externalWF_tensorboson_psD<T,Tcur,Ds>(momconf,lmom,refmom);
 		}
 		if(Dsm4Ind == 4){
 			return externalWF_tensorboson_ptD<T,Tcur,Ds>(momconf,lmom,refmom);
 		}

 		std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));

 		Tensor<T,Ds,2> hpI(0);

 		for(size_t mu=0;mu<Ds;mu++){
 			hpI[Dsm4Ind+1][mu]=epsp[mu];
 			hpI[mu][Dsm4Ind+1]=epsp[mu];
 		}

 		return v_t_to_v_extended_t<T,Tcur>(hpI.vectorize());
 }



template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind> std::vector<Tcur> externalWF_tensorboson_IID(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    int ind(Dsm4Ind);

    if(Dsm4Ind==3){
      return  externalWF_tensorboson_ssD<T,Tcur,Ds>(momconf,lmom,refmom);
    }
    if(Dsm4Ind==4){
      return  externalWF_tensorboson_ttD<T,Tcur,Ds>(momconf,lmom,refmom);
    }

    Tensor<T,Ds,2> hII(0);

    momD<T,Ds> ml(-momconf.p(lmom));
    momD_conf<T,Ds> mconf_aux;

    auto mlmom = mconf_aux.insert(ml);
    size_t ref(0);
    if(refmom != 0){
      ref = mconf_aux.insert(momconf.p(refmom));
    }


      const std::vector<T>& metric(hII.get_metric_extended());

    std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_sD<T,Tcur,Ds>(momconf,lmom,refmom)));
    std::vector<T> epsJ(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tD<T,Tcur,Ds>(momconf,lmom,refmom)));

    std::vector<T> epsmc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
    std::vector<T> epspc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
    std::vector<T> epsIc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_scD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
    std::vector<T> epsJc(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));

    if (Dsm4Ind>=5) {
      for(size_t mu=0;mu<Ds;mu++){
        hII[mu][mu]=epsI[mu]*epsIc[mu]+epsJ[mu]*epsJc[mu]+(epsm[mu]*epsmc[mu]+epsp[mu]*epspc[mu]);
      }
      for(size_t mu=0;mu<Ds;mu++){
        for(size_t nu=mu+1;nu<Ds;nu++){
          T emuenu(epsI[mu]*epsIc[nu]+epsJ[mu]*epsJc[nu]+(epsm[mu]*epsmc[nu]+epsp[mu]*epspc[nu]));
          hII[mu][nu]=emuenu;
          hII[nu][mu]=emuenu;
        }
      }
      for (size_t ii = 0; ii < Dsm4Ind-5; ii++) {
        hII[6+ii][6+ii] -= T(1)/metric[6+ii];
      }

      hII *= T(1)/(T(ind)-T(1));

      hII[Dsm4Ind+1][Dsm4Ind+1] += T(1)/metric[Dsm4Ind+1];
    }
    return v_t_to_v_extended_t<T,Tcur>(hII.vectorize());
}


template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind1,unsigned Dsm4Ind2> std::vector<Tcur> externalWF_tensorboson_IJD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
        static_assert(Ds>=5,"Dimensional mismatch");
 		if(Dsm4Ind1==Dsm4Ind2){
 			return externalWF_tensorboson_IID<T,Tcur,Ds,Dsm4Ind1>(momconf,lmom,refmom);
 		}

 		auto minDsm4Ind =std::min(Dsm4Ind1,Dsm4Ind2);
 		auto maxDsm4Ind =std::max(Dsm4Ind1,Dsm4Ind2);

 		Tensor<T,Ds,2> hIJ(0);

 		if(minDsm4Ind > 4){
 			hIJ[Dsm4Ind1+1][Dsm4Ind2+1]=T(1);
 			hIJ[Dsm4Ind2+1][Dsm4Ind1+1]=T(1);
 			return v_t_to_v_extended_t<T,Tcur>(hIJ.vectorize());
 		}
 		std::vector<T> epsI(Ds,T(0));

 		if(maxDsm4Ind > 4){
 			if(minDsm4Ind == 3){
 				epsI = v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_sD<T,Tcur,Ds>(momconf,lmom,refmom));
 			}
 			if(minDsm4Ind == 4){
 				epsI = v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tD<T,Tcur,Ds>(momconf,lmom,refmom));
 			}
 			for(size_t mu=0;mu<Ds;mu++){
 				hIJ[maxDsm4Ind+1][mu]=epsI[mu];
 				hIJ[mu][maxDsm4Ind+1]=epsI[mu];
 			}
 			return v_t_to_v_extended_t<T,Tcur>(hIJ.vectorize());
 		}

 		return externalWF_tensorboson_stD<T,Tcur,Ds>(momconf,lmom,refmom);
}

template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind> std::vector<Tcur> externalWF_tensorboson_mIcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
        static_assert(Ds>=5,"Dimensional mismatch");
 		if(Dsm4Ind == 3){
 			auto out = externalWF_tensorboson_mscD<T,Tcur,Ds>(momconf,lmom,refmom);
			for(size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii] / T(-2);
			return out;
 		}
 		if(Dsm4Ind == 4){
 			auto out = externalWF_tensorboson_mtcD<T,Tcur,Ds>(momconf,lmom,refmom);
			for(size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii] / T(-2);
			return out;
 		}

 		std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));

 		Tensor<T,Ds,2> hmI(0);
 		const std::vector<T>& metric(hmI.get_metric_extended());

 		for(size_t mu=0;mu<Ds;mu++){
 			hmI[Dsm4Ind+1][mu]=epsm[mu];
 			hmI[mu][Dsm4Ind+1]=epsm[mu];
 		}

 		hmI *= metric[Dsm4Ind+1];
                hmI /= T(-2);

 		return v_t_to_v_extended_t<T,Tcur>(hmI.vectorize());
}

template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind> std::vector<Tcur> externalWF_tensorboson_pIcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=5,"Dimensional mismatch");
    if(Dsm4Ind == 3){
    	auto out = externalWF_tensorboson_pscD<T,Tcur,Ds>(momconf,lmom,refmom);
	for(size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii]/T(-2);
	return out;
    }
    if(Dsm4Ind == 4){
    	auto out = externalWF_tensorboson_ptcD<T,Tcur,Ds>(momconf,lmom,refmom);
	for(size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii]/T(-2);
	return out;
    }

    std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));

    Tensor<T,Ds,2> hpI(0);
    const std::vector<T>& metric(hpI.get_metric_extended());

    for(size_t mu=0;mu<Ds;mu++){
    	hpI[Dsm4Ind+1][mu]=epsp[mu];
    	hpI[mu][Dsm4Ind+1]=epsp[mu];
    }
    hpI *= metric[Dsm4Ind+1];
    hpI /= T(-2);

    return v_t_to_v_extended_t<T,Tcur>(hpI.vectorize());
}


template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind> std::vector<Tcur> externalWF_tensorboson_IIcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
  static_assert(Ds>=5,"Dimensional mismatch");
  int ind(Dsm4Ind);

  if(Dsm4Ind==3){
    return  externalWF_tensorboson_sscD<T,Tcur,Ds>(momconf,lmom,refmom);
  }
  if(Dsm4Ind==4){
    return  externalWF_tensorboson_ttcD<T,Tcur,Ds>(momconf,lmom,refmom);
  }

  Tensor<T,Ds,2> hII(0);

  momD<T,Ds> ml(-momconf.p(lmom));
  momD_conf<T,Ds> mconf_aux;

  auto mlmom = mconf_aux.insert(ml);
  size_t ref(0);
  if(refmom != 0){
    ref = mconf_aux.insert(momconf.p(refmom));
  }

  const std::vector<T>& metric(hII.get_metric_extended());

  std::vector<T> epsm(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(momconf,lmom,refmom)));
  std::vector<T> epsp(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(momconf,lmom,refmom)));
  std::vector<T> epsI(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_sD<T,Tcur,Ds>(momconf,lmom,refmom)));
  std::vector<T> epsJ(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tD<T,Tcur,Ds>(momconf,lmom,refmom)));


  std::vector<T> epsmc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_pD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  std::vector<T> epspc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_mD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  std::vector<T> epsIc(v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_scD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));
  std::vector<T> epsJc(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,Ds>(mconf_aux,mlmom,ref)));

  if (Dsm4Ind>=5) {
    for(size_t mu=0;mu<Ds;mu++){
      hII[mu][mu]=epsI[mu]*epsIc[mu]+epsJ[mu]*epsJc[mu]+(epsm[mu]*epsmc[mu]+epsp[mu]*epspc[mu]);
    }
    for(size_t mu=0;mu<Ds;mu++){
      for(size_t nu=mu+1;nu<Ds;nu++){
        T emuenu(epsI[mu]*epsIc[nu]+epsJ[mu]*epsJc[nu]+(epsm[mu]*epsmc[nu]+epsp[mu]*epspc[nu]));
        hII[mu][nu]=emuenu;
        hII[nu][mu]=emuenu;
      }
    }
    for (size_t ii = 0; ii < Dsm4Ind-5; ii++) {
      hII[6+ii][6+ii] -= T(1)/metric[6+ii];
    }

    hII *= T(1)/(T(ind)-T(1));

    hII[Dsm4Ind+1][Dsm4Ind+1] += T(1)*metric[Dsm4Ind+1];
  }

  // hII*= T(ind)/(T(ind)+T(1));
    hII *= T(-2)*(T(ind)-T(1))/T(ind);
    //hII *= T(2);

    return v_t_to_v_extended_t<T,Tcur>(hII.vectorize());
}



template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind1,unsigned Dsm4Ind2> std::vector<Tcur> externalWF_tensorboson_IJcD(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
  static_assert(Ds>=5,"Dimensional mismatch");
  if(Dsm4Ind1==Dsm4Ind2){
    auto out = externalWF_tensorboson_IIcD<T,Tcur,Ds,Dsm4Ind1>(momconf,lmom,refmom);
    for(size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii]/T(-2);
    return out;
  }

  auto minDsm4Ind =std::min(Dsm4Ind1,Dsm4Ind2);
  auto maxDsm4Ind =std::max(Dsm4Ind1,Dsm4Ind2);

  Tensor<T,Ds,2> hIJ(0);
  const std::vector<T>& metric(hIJ.get_metric_extended());

  if(minDsm4Ind > 4){
    hIJ[Dsm4Ind1+1][Dsm4Ind2+1]=T(1);
    hIJ[Dsm4Ind2+1][Dsm4Ind1+1]=T(1);

    hIJ *= T(-1)*metric[Dsm4Ind1+1]*metric[Dsm4Ind2+1];
    hIJ /= T(-2);

    return v_t_to_v_extended_t<T,Tcur>(hIJ.vectorize());
  }
  std::vector<T> epsI(Ds,T(0));

  momD<T,Ds> ml(-momconf.p(lmom));
  momD_conf<T,Ds> mconf_aux;

  auto mlmom = mconf_aux.insert(ml);
  size_t ref(0);
  if(refmom != 0){
    ref = mconf_aux.insert(momconf.p(refmom));
  }

  if(maxDsm4Ind > 4){
    if(minDsm4Ind == 3){
      epsI = v_extended_t_to_v_t<T,Tcur>(externalWF_vectorboson_scD<T,Tcur,Ds>(mconf_aux,mlmom,ref));
    }
    if(minDsm4Ind == 4){
      epsI = v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,Ds>(mconf_aux,mlmom,ref));
    }
    for(size_t mu=0;mu<Ds;mu++){
      hIJ[maxDsm4Ind+1][mu]=epsI[mu];
      hIJ[mu][maxDsm4Ind+1]=epsI[mu];
    }

    hIJ*= T(-1)*metric[maxDsm4Ind+1];
    hIJ /= T(-2);

    return v_t_to_v_extended_t<T,Tcur>(hIJ.vectorize());
  }

  auto out = externalWF_tensorboson_stcD<T, Tcur, Ds>(momconf, lmom, refmom);
  for (size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii] / T(-2);
  return out; 
}

// Ward state for Einstein-Hilbert Gravity
template <typename T, typename Tcur, size_t D>
std::vector<Tcur> externalWF_tensorboson_ward(const momD_conf<T, D>& momconf, const size_t& lmom, const size_t& refmom) {
    // NOTICE: Here we only use that eps_{munu}-> p_{mu}q_{nu}+q_{mu}p_{nu}-(p*q)eta_{munu} for the ward identity. In principla any vecor could be chosen
    // instead of q.
    Tensor<T, D, 2> hward(0);
    const momD<T, D>& l(momconf.p(lmom));
    momD<T, D> q;
    if (refmom > 0)
        q = momconf.p(refmom);
    else
        q = l.get_parity_conjugated();

    // fill it
    for (size_t mu = 0; mu < D; mu++) hward[mu][mu] = T(2) * l.pi(mu) * q.pi(mu);

    for (size_t mu = 0; mu < D; mu++)
        for (size_t nu = mu + 1; nu < D; nu++) {
            T emuenu(l.pi(mu) * q.pi(nu) + q.pi(mu) * l.pi(nu));
            hward[mu][nu] = emuenu;
            hward[nu][mu] = emuenu;
        }

    return v_t_to_v_extended_t<T, Tcur>(hward.vectorize());
}

// Ward state for Cubic formulation of Gravity
template <typename T, typename Tcur, size_t D>
std::vector<Tcur> externalWF_tensorboson_cubic_ward(const momD_conf<T, D>& momconf, const size_t& lmom, const size_t& refmom) {
    // NOTICE: Here we only use that eps_{munu}-> p_{mu}q_{nu}+q_{mu}p_{nu}-(p*q)eta_{munu} for the ward identity. In principla any vecor could be chosen
    // instead of q.
    Tensor<T, D, 2> hward(0);
    const momD<T, D>& l(momconf.p(lmom));
    momD<T, D> q;
    if (refmom > 0)
        q = momconf.p(refmom);
    else
        q = l.get_parity_conjugated();

    // fill it
    for (size_t mu = 0; mu < D; mu++) hward[mu][mu] = T(2) * l.pi(mu) * q.pi(mu);

    T ldotq = l * q;
    const std::vector<T>& metric(hward.get_metric_extended());
    for (size_t mu = 0; mu < D; mu++) hward[mu][mu] -= ldotq / metric[mu];

    for (size_t mu = 0; mu < D; mu++)
        for (size_t nu = mu + 1; nu < D; nu++) {
            T emuenu(l.pi(mu) * q.pi(nu) + q.pi(mu) * l.pi(nu));
            hward[mu][nu] = emuenu;
            hward[nu][mu] = emuenu;
        }

    return v_t_to_v_extended_t<T, Tcur>(hward.vectorize());
}

 // template <typename T, typename Tcur, size_t D> std::vector<Tcur> transversal_Projector(const momD<T,D>& mom){
 //
 // 	Tensor<Tcur,D,2> proj2(0);
 //
 // 	const std::vector<T>& metric(proj2.get_metric());
 // 	T muSq = compute_Dsm4_prod(mom,mom);
 //
 // 	for (size_t ii = 0; ii < D; ii++) {
 // 		if(ii != 4 && ii != 5)
 // 		{
 // 				proj2[ii][ii] += metric[ii];
 // 		}
 // 	}
 //
 // 	for (size_t ii = 0; ii < 4; ii++) {
 // 		for (size_t jj = 0; jj < 4; jj++) {
 // 			proj2[ii][jj]+=T(1)/muSq*(mom.pi(ii)*mom.pi(jj));
 // 		}
 // 	}
 // 	if(D >= 6){
 // 		proj2[4][4]-=T(1)/muSq*(mom.pi(5)*mom.pi(5));
 // 		proj2[4][5]-=T(1)/muSq*(mom.pi(5)*mom.pi(4));
 // 		proj2[5][4]-=T(1)/muSq*(mom.pi(5)*mom.pi(4));
 // 		proj2[5][5]-=T(1)/muSq*(mom.pi(4)*mom.pi(4));
 // 	}
 // 	return proj2.vectorize();
 // }
//
// template <typename T, typename Tcur, size_t D> std::enable_if_t<!is_complex<T>::value,std::vector<Tcur>>transversal_Projector(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
//
//  	Tensor<Tcur,D,2> proj(0);
//  	const momD<T,D>& l(momconf.p(lmom));
//
//  	const std::vector<T>& metric(proj.get_metric_extended());
//  	T muSq = compute_Dsm4_prod(l,l);
//
//  	for (size_t ii = 0; ii < D; ii++) {
//  		if(ii != 4 && ii != 5)
//  		{
//  				proj[ii][ii]+=T(1)/metric[ii];
//  		}
//  	}
//
//     T factor(1);
//
//     if(typeid(T)==typeid(F32)){
//         factor =T(-1);
//     }
//
//  	for (size_t ii = 0; ii < 4; ii++) {
//  		for (size_t jj = 0; jj < 4; jj++) {
//  			proj[ii][jj]+=T(1)/muSq*l.pi(ii)*l.pi(jj);
//  		}
//  	}
//
//   if (D >= 6) {
//     momD<T,D> ml(-momconf.p(lmom));
//     momD_conf<T,D> mconf_aux;
//
//     auto mlmom = mconf_aux.insert(ml);
//
//     size_t ref(0);
//     if(refmom != 0){
//       ref = mconf_aux.insert(momconf.p(refmom));
//     }
//
// 		std::vector<Tcur> epsJ(ForGravitons_externalWF_vectorboson_tD<T,Tcur,D>(momconf,lmom,refmom));
// 		std::vector<Tcur> epsJc(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,D>(mconf_aux,mlmom,ref));
// 		for (size_t ii = 0; ii < D; ii++) {
// 	 		for (size_t jj = 0; jj < D; jj++){
// 	 			proj[ii][jj]-=factor*epsJ[ii]*epsJc[jj];
// 	 		}
// 	 	}
// 	}
//
//  	return proj.vectorize();
// }

template <typename T, typename Tcur, size_t D>  std::vector<Tcur> transversal_Projector(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){

  Tensor<T,D,2> proj(0);
  const momD<T,D>& l(momconf.p(lmom));

  const std::vector<T>& metric(proj.get_metric_extended());
  T muSq = compute_Dsm4_prod(l,l);

  for (size_t ii = 0; ii < D; ii++) {
    if(ii != 4 && ii != 5)
    {
        proj[ii][ii]+=T(1)/metric[ii];
    }
  }

  for (size_t ii = 0; ii < 4; ii++) {
    for (size_t jj = 0; jj < 4; jj++) {
      proj[ii][jj]+=T(1)/muSq*l.pi(ii)*l.pi(jj);
    }
  }

  if (D >= 6) {
    momD<T,D> ml(-momconf.p(lmom));
    momD_conf<T,D> mconf_aux;

    auto mlmom = mconf_aux.insert(ml);

    size_t ref(0);
    if(refmom != 0){
      ref = mconf_aux.insert(momconf.p(refmom));
    }
    std::vector<T> epsJ(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tD<T,Tcur,D>(momconf,lmom,refmom)));
    std::vector<T> epsJc(v_extended_t_to_v_t<T,Tcur>(ForGravitons_externalWF_vectorboson_tcD<T,Tcur,D>(mconf_aux,mlmom,ref)));
    for (size_t ii = 0; ii < D; ii++) {
      for (size_t jj = 0; jj < D; jj++){
        proj[ii][jj]-=epsJ[ii]*epsJc[jj];
      }
    }
  }

  return v_t_to_v_extended_t<T,Tcur>(proj.vectorize());
}

template <typename T, typename Tcur, size_t D> std::vector<Tcur> transversal_Projector(const momD<T,D>& mom){
  momD_conf<T,D> mconf;

  std::vector<T> rmom(D,T(0));
  ASSIGN_lightcone_ref_1(&rmom[0]);
  auto mref = momD<T,D>(rmom);

  auto imom = mconf.insert(mom);
  auto ref = mconf.insert(mref);

  return transversal_Projector<T,Tcur,D>(mconf,imom,ref);
}

template <typename T, typename Tcur, size_t D> std::vector<Tcur> transversal_traceless_Projector(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
  Tensor<T,D,2> proj1(v_extended_t_to_v_t<T,Tcur>(transversal_Projector<T,Tcur,D>(momconf,lmom,refmom)));
  Tensor<T,D,4> proj2(0);
  int dim(D);
  for (size_t ii = 0; ii < D; ii++) {
    for (size_t jj = 0; jj < D; jj++) {
      for (size_t kk = 0; kk < D; kk++) {
        for (size_t ll = 0; ll < D; ll++) {
          proj2[ii][jj][kk][ll] = T(1)/T(2)*(proj1[ii][kk]*proj1[jj][ll]+proj1[ii][ll]*proj1[jj][kk])-T(1)/(T(dim)-T(2))*proj1[ii][jj]*proj1[kk][ll];
        }
      }
    }
  }
  return v_t_to_v_extended_t<T,Tcur>(proj2.vectorize());
}



 template <typename T, typename Tcur, size_t D> std::vector<Tcur> transversal_Projector2(const momD<T,D>& mom){
 	Tensor<T,D,2> proj(0);

 	const std::vector<T>& metric(proj.get_metric_extended());
 	T momSq(0);
 	for (size_t ii = 0; ii < D; ii++) {
 		momSq += mom.pi(ii)*mom.pi(ii);
 	}

 	for (size_t ii = 0; ii < D; ii++) {
 				proj[ii][ii] += T(1)/metric[ii];
 	}

 	for (size_t ii = 0; ii < D; ii++) {
 		for (size_t jj = 0; jj < D; jj++) {
 			proj[ii][jj]-=T(1)/momSq*(mom.pi(ii)*mom.pi(jj)*metric[jj]+mom.pi(ii)*mom.pi(jj)*metric[ii]);
 		}
 	}
 	return v_t_to_v_extended_t<T,Tcur>(proj.vectorize());
}

template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_tensorboson_trace(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
 	return transversal_Projector<T,Tcur,D>(momconf,lmom,refmom);
}

template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_tensorboson_tracec(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	int dim(D);
	Tensor<T,D,2> htrace(v_extended_t_to_v_t<T,Tcur>(transversal_Projector<T,Tcur,D>(momconf,lmom,refmom)));

	htrace *=(T(dim)-T(3))/(T(dim)-T(2));
	//TODO only temporary
	htrace *=T(-2);
	return v_t_to_v_extended_t<T,Tcur>(htrace.vectorize());
}


/**
 * Just to avoid having to add a operator< for SingleState for using std::map below
 */
class CompareSingleState {
	public:
		bool operator()(const SingleState& s1,const SingleState& s2) const{
			return s1.get_name() < s2.get_name();
		}
};


 /**
  * A struct, which contains tensor states.
  *
  * States of correct type and correct dimensionality are collected by constructor.
  * The structure is supposed to be used a static instant.
  */
 template <typename T, size_t Ds> class tensor_states{
     static_assert(Ds>=4,"Dimensional mismatch");
     public:
         typedef typename Current_Type_Selector<T>::Tcur Tcur;

     private:
         typedef typename std::map<SingleState,ExternalCurrent<T,Tcur,Ds>,CompareSingleState> MT;
         MT states{};

         template <size_t N> using int_ = Caravel::_utilities_private::int_<N>;
         std::function<ExternalCurrent<T,Tcur,Ds>(SingleState)> construct_external_current = [](SingleState s) -> ExternalCurrent<T,Tcur,Ds>{
           if (s.get_name() == "hWard") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_ward<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "hCubicWard") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_cubic_ward<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "Ph4TEST") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_dummy<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "Phc4TEST") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_dummy<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "hmm") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_mm<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "hpp") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_pp<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "hmmD") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_mmD<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "hppD") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_ppD<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "hmmcD") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_mmcD<T,Tcur,Ds>(mc,i,r);
              };
           else if (s.get_name() == "hppcD") return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_ppcD<T,Tcur,Ds>(mc,i,r);
              };
/*
           else if (s.get_name() == "hmID" && Ds > 4) return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_mID<T,Tcur,Ds,4>(mc,i,r);
              };
           else if (s.get_name() == "hpID" && Ds > 4) return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_pID<T,Tcur,Ds,4>(mc,i,r);
              };
           else if (s.get_name() == "hIJD" && Ds > 4) return[](const momD_conf<T,Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc){
                return externalWF_tensorboson_IJD<T,Tcur,Ds,4,4>(mc,i,r);
              };
*/
           else
             throw std::logic_error("Internal inconsistency in tensor_states() constructor");
         };

     public:
         //! for floating types (we keep this structure, but for now both constructors are the same)
         template <typename TT=T> tensor_states(typename std::enable_if<!is_exact<TT>::value>::type* = nullptr){
             states = {
                 {SingleState("hWard"), construct_external_current(SingleState("hWard"))},
                 // compute with D dimensional states
                 //{SingleState("hmID"), construct_external_current(SingleState("hmID"))},
                 //{SingleState("hpID"), construct_external_current(SingleState("hpID"))},
                 //{SingleState("hIJD"), construct_external_current(SingleState("hIJD"))},
             };

             add_normal(int_<Ds>{});
             add_projective(int_<Ds>{});
         }

         //! for integral types (we keep this structure, but for now both constructors are the same)
         template <typename TT=T> tensor_states(typename std::enable_if<is_exact<TT>::value>::type* = nullptr){
             states = {
                 {SingleState("hWard"), construct_external_current(SingleState("hWard"))},
                 // compute with D dimensional states
                 //{SingleState("hmID"), construct_external_current(SingleState("hmID"))},
                 //{SingleState("hpID"), construct_external_current(SingleState("hpID"))},
                 //{SingleState("hIJD"), construct_external_current(SingleState("hIJD"))},
             };

             add_normal(int_<Ds>{});
             add_projective(int_<Ds>{});
         }

         //! Floating point reals states are not used so far

         const MT& get() const {return states;}

     private:
 	// this specialization is adding the unique dummy state, in accordance with what's defined in add_tensor_boson_states
 	// TODO: fix for proper cases!
         inline void add_projective(int_<4>){
             states.emplace(SingleState("Ph4TEST"), construct_external_current(SingleState("Ph4TEST")) );
             states.emplace(SingleState("Phc4TEST"), construct_external_current(SingleState("Phc4TEST")) );
         }
 	// this specialization is adding the unique dummy state, in accordance with what's defined in add_tensor_boson_states
 	// TODO: fix for proper cases!
         template <size_t Pos> inline void add_projective(int_<Pos>){
             states.emplace(SingleState("Ph"+std::to_string(Pos)+"TEST"),
                            [](const momD_conf<T,Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc){ 
                              return externalWF_tensorboson_dummy<T,Tcur,Ds>(mc,l,r);}
 		);
             states.emplace(SingleState("Phc"+std::to_string(Pos)+"TEST"),
                            [](const momD_conf<T,Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc){ 
                              return externalWF_tensorboson_dummy<T,Tcur,Ds>(mc,l,r);}
 		);
 	    // reduce dimension by 1 (it stops at 4 dim)
             add_projective(int_<Pos-1>{});
         }
 	// specialization for the 4-D graviton states
         inline void add_normal(int_<4>){
             states.emplace(SingleState("hmm"), construct_external_current(SingleState("hmm")) );
             states.emplace(SingleState("hpp"), construct_external_current(SingleState("hpp")) );
         }
 	  inline void add_normal(int_<5>){
             states.emplace(SingleState("hmmD"), construct_external_current(SingleState("hmmD")) );
             states.emplace(SingleState("hmmcD"), construct_external_current(SingleState("hmmcD")) );
             states.emplace(SingleState("hppD"), construct_external_current(SingleState("hppD")) );
             states.emplace(SingleState("hppcD"), construct_external_current(SingleState("hppcD")) );
      	     //states.emplace(SingleState("htr"), externalWF_tensorboson_trace<T,Tcur,Ds>);
	     //states.emplace(SingleState("htrc"), externalWF_tensorboson_tracec<T,Tcur,Ds>);
             add_normal_IJ(int_<5>{},int_<5>{});
 	     add_normal(int_<4>{});
 	  }
 	// this specialization is adding the unique dummy state, in accordance with what's defined in add_tensor_boson_states

 	template <size_t Pos> inline void add_normal_IJ(int_<4>,int_<Pos>){
 		states.emplace(SingleState("hm"+std::to_string(Pos-2)+"D"),[](const momD_conf<T,Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc){
 			  return externalWF_tensorboson_mID<T,Tcur,Ds,Pos-2>(mc,l,r);
                        }
 		);
 		states.emplace(SingleState("hp"+std::to_string(Pos-2)+"D"),[](const momD_conf<T,Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc){
 			 return externalWF_tensorboson_pID<T,Tcur,Ds,Pos-2>(mc,l,r);
                       }
 		);
 		states.emplace(SingleState("hm"+std::to_string(Pos-2)+"cD"),[](const momD_conf<T,Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc){
			 return externalWF_tensorboson_mIcD<T,Tcur,Ds,Pos-2>(mc,l,r);
                       }
 		);
 		states.emplace(SingleState("hp"+std::to_string(Pos-2)+"cD"),[](const momD_conf<T,Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc){
			 return externalWF_tensorboson_pIcD<T,Tcur,Ds,Pos-2>(mc,l,r);
                       }
 		);
 	}

 	template <size_t Pos1,size_t Pos2> inline void add_normal_IJ(int_<Pos1>,int_<Pos2>){
 		states.emplace(SingleState("h"+std::to_string(Pos1-2)+std::to_string(Pos2-2)+"D"),[](const momD_conf<T,Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc){
 			 return externalWF_tensorboson_IJD<T,Tcur,Ds,Pos1-2,Pos2-2>(mc,l,r);
                       }
 		);
 		states.emplace(SingleState("h"+std::to_string(Pos1-2)+std::to_string(Pos2-2)+"cD"),[](const momD_conf<T,Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc){
			 return externalWF_tensorboson_IJcD<T,Tcur,Ds,Pos1-2,Pos2-2>(mc,l,r);
                       }
 		);
 		add_normal_IJ(int_<Pos1-1>{},int_<Pos2>{});
 	}

 	// TODO: fix for proper cases!
         template <size_t Pos> inline void add_normal(int_<Pos>){
 		add_normal_IJ(int_<Pos>{},int_<Pos>{});
 	    // reduce dimension by 1 (it stops at 4 dim)
             add_normal(int_<Pos-1>{});
         }

 };


// For graviton propagator
template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> Graviton_Propagator(const std::vector<Tcur>& in,T& prop,const momentumD<T,Ds>& mom, const BuilderContainer<T>& bc){
	// operate on the numerator
	// We choose for now to write operations very clearly, but those might need to be modified for efficiency!

	// the incoming tensor
	Tensor<T,Ds,2> hin(v_extended_t_to_v_t<T,Tcur>(in));
	// the transposed tensor
	Tensor<T,Ds,2> hin_trans(swap_indices<0,1>(hin));
	// the trace
	T trace(trace_rank2(hin));
	// NOTICE: potential problem for finite field evaluations by using the metric!
	// NOTICE: we don't make it static const to avoid troubles when changing number field (maybe better solution later!)
	// the metric tensor
	Tensor<T,Ds,2> mtrace_eta(0);
	// careful when changing finite fields
	const std::vector<T>& metric(mtrace_eta.get_metric_extended());
	for(size_t ii=0;ii<Ds;ii++)
		mtrace_eta[ii][ii]=-trace*T(1)/metric[ii];

	// the out
	Tensor<T,Ds,2> hout(hin+hin_trans+mtrace_eta);

	// NOTICE: an 'I' has been factored out
	// TODO: IT MUST be reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
	prop=T(-1)/(mom*mom);

	// multiply current
	hout*=(prop);
	// factor of I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
	//hout*=(T(0,1));

	return v_t_to_v_extended_t<T,Tcur>(hout.vectorize());
}

// For Einstein-Hilbert gravity we use the de Donder graviton propagator
template <typename T, typename Tcur, size_t Ds, size_t is_cut>
std::vector<Tcur> deDonder_Graviton_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& mom, const BuilderContainer<T>& bc) {
    // operate on the numerator
    // We choose for now to write operations very clearly, but those might need to be modified for efficiency!

    int dims(Ds);

    // the incoming tensor
    Tensor<T, Ds, 2> hin(v_extended_t_to_v_t<T, Tcur>(in));
    // the transposed tensor
    Tensor<T, Ds, 2> hin_trans(swap_indices<0, 1>(hin));
    // the trace
    T trace(trace_rank2(hin));

    // NOTICE: potential problem for finite field evaluations by using the metric!
    // NOTICE: we don't make it static const to avoid troubles when changing number field (maybe better solution later!)
    // the metric tensor
    Tensor<T, Ds, 2> mtrace_eta(0);
    // careful when changing finite fields
    const std::vector<T>& metric(mtrace_eta.get_metric_extended());
    for (size_t ii = 0; ii < Ds; ii++) mtrace_eta[ii][ii] = -trace * T(2) / metric[ii] / (T(dims) - T(2));

    // the out
    Tensor<T, Ds, 2> hout(hin + hin_trans + mtrace_eta);

    // NOTICE: an 'I' has been factored out
    // TODO: IT MUST be reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
    if constexpr (is_cut == 1)
        prop = T(1);
    else
        prop = T(1) / (mom * mom);

    // multiply current
    hout *= (prop);
    hout /= (T(2));
    // factor of I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
    // hout*=(T(0,1));

    return v_t_to_v_extended_t<T, Tcur>(hout.vectorize());
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> Cut_Graviton_Propagator(const std::vector<Tcur>& in,T& prop,const momentumD<T,Ds>& mom, const BuilderContainer<T>& bc){
	// operate on the numerator

	int dims(Ds);

	// the incoming tensor
	Tensor<T,Ds,2> proj_vector(v_extended_t_to_v_t<T,Tcur>(transversal_Projector<T,Tcur,Ds>(mom)));
	Tensor<T,Ds,2> hin(v_extended_t_to_v_t<T,Tcur>(in));
	Tensor<T,Ds,2> hout(0);
	const std::vector<T>& metric(hout.get_metric_extended());

	T h_transversaltrace(IP_rank2(proj_vector,hin));

	for (size_t ii = 0; ii < Ds; ii++) {
		for (size_t jj = 0; jj < Ds; jj++) {
			for (size_t kk = 0; kk < Ds; kk++) {
				for (size_t ll = 0; ll < Ds; ll++) {
					//TODO explicit symmetrization should not be required
					hout[ii][jj] += T(1)/T(2)*metric[kk]*metric[ll]*(proj_vector[ii][kk]*proj_vector[jj][ll]+proj_vector[jj][kk]*proj_vector[ii][ll])*(hin[kk][ll]);
				}
			}
		}
	}

	//hout -= T(1)/T(2)*Projh*proj2;
	hout -= T(1)/(T(dims)-T(2))*h_transversaltrace*proj_vector;
	//TODO introduce this factor in builder
	hout *= T(-2);

	// factor of I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
	//hout*=(T(0,1));


  // momD_conf<T,Ds> mconf;
  //
  // std::vector<T> rmom(Ds,T(0));
  // ASSIGN_lightcone_ref_2(&rmom[0]);
  // auto mref = momD<T,Ds>(rmom);
  //
  // auto imom = mconf.insert(mom);
  // auto ref = mconf.insert(mref);
  //Tensor<T,Ds,2> hWard(externalWF_tensorboson_ward<T,Tcur,Ds>(mconf,imom,ref));
//  std::cout <<"in ward-id:"<< IP_rank2(hWard,hin) << '\n';
//  std::cout <<"out ward-id:"<< IP_rank2(hWard,hout) << '\n';
//Tensor<T,Ds,1> ldoth(index_contraction<0>(mom,hout));
//std::cout << "transversality" << ldoth << '\n';
//std::cout << "trace" <<trace_rank2(hout) << '\n';
//std::cout <<"out ward-id:"<< IP_rank2(hWard,hout) << '\n';

	return v_t_to_v_extended_t<T,Tcur>(hout.vectorize());
}

/**
 * Constraction of graviton currents h1^{\mu\nu} h2_{\mu\nu}
 */
template <unsigned Ds, typename T, typename Tcur> T contract_GG(const std::vector<Tcur>& h1, const std::vector<Tcur>& h2, const BuilderContainer<T>& bc) {
    // We use this auxiliary constant rank two tensor to get access to traverse vector.
    // Maybe better done some other way, but this secures some efficiency
    // (no need to construct the h1- and h2-associated tensors, and then no call to Tensor*Tensor product)
    static thread_local const Tensor<T, Ds, 2> haux(0);
    // grab set of indices from the given class
    const std::vector<std::vector<size_t>>& traverse(haux.get_traverse());
    const std::vector<T>& metric(haux.get_metric_extended());

    T toret(0);
    for (size_t ii = 0; ii < traverse.size(); ii++)
        // sum_{mu,nu} g^{mumu} g^{nunu} h1_{munu} h2_{munu}
        // get_e0 as extended_t irrelevant
        toret += metric[traverse[ii][0]] * metric[traverse[ii][1]] * get_e0(h1[ii]) * get_e0(h2[ii]);

    return toret;
}
}}
