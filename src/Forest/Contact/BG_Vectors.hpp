#include "Tensor.hpp"

namespace Caravel {
namespace BG {

/**
 * A struct, which contains vector states.
 *
 * States of correct type and correct dimensionality are collected by constructor.
 * The structure is supposed to be used a static instant.
 */
template <typename T, size_t Ds> class vector_states {
    static_assert(Ds >= 4, "Dimensional mismatch");

  public:
    typedef typename Current_Type_Selector<T>::Tcur Tcur;

  private:
    typedef typename std::map<SingleState, ExternalCurrent<T, Tcur, Ds>> MT;
    MT states;

    template <size_t N> using int_ = Caravel::_utilities_private::int_<N>;
    std::function<ExternalCurrent<T, Tcur, Ds>(SingleState)> construct_external_current = [](SingleState s) -> ExternalCurrent<T, Tcur, Ds> {
        if (s.get_name() == "Ward")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_Ward<T, Tcur, Ds>(mc, i, r);
            };
        else if (s.get_name() == "m")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_m<T, Tcur, Ds>(mc, i, r);
            };
        else if (s.get_name() == "p")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_p<T, Tcur, Ds>(mc, i, r);
            };
        else if (s.get_name() == "mD")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_mD<T, Tcur, Ds>(mc, i, r);
            };
        else if (s.get_name() == "pD")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_pD<T, Tcur, Ds>(mc, i, r);
            };
        else if (s.get_name() == "sD")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_sD<T, Tcur, Ds>(mc, i, r);
            };
        else if (s.get_name() == "scD")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_scD<T, Tcur, Ds>(mc, i, r);
            };
        else if (s.get_name() == "tD")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_tD<T, Tcur, Ds>(mc, i, r);
            };
        else if (s.get_name() == "tcD")
            return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                return externalWF_vectorboson_tcD<T, Tcur, Ds>(mc, i, r);
            };
        else
            throw std::logic_error("Internal inconsistency in vector_states() constructor");
    };

  public:
    //! for floating types
    template <typename TT = T> vector_states(typename std::enable_if<!is_exact<TT>::value>::type* = nullptr) {
        states = {
            {SingleState("Ward"), construct_external_current(SingleState("Ward"))},
        };

        add_normal(int_<Ds>{});
        add_projective(int_<1>{});
    }

    //! for integral types
    template <typename TT = T> vector_states(typename std::enable_if<is_exact<TT>::value>::type* = nullptr) {
        states = {
            {SingleState("m"), construct_external_current(SingleState("m"))},
            {SingleState("p"), construct_external_current(SingleState("p"))},
            {SingleState("Ward"), construct_external_current(SingleState("Ward"))},
        };

        add_normal(int_<Ds>{});
        add_projective(int_<1>{});
    }

    //! Floating point reals states are not used so far

    const MT& get() const { return states; }

  private:
    inline void add_projective(int_<Ds>) {
        states.emplace(SingleState("mu" + std::to_string(Ds)), [](const momD_conf<T, Ds>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
            return externalWF_vectorboson_mui<T, Tcur, Ds, Ds - 1>(m, l, r);
        });
        states.emplace(SingleState("cont" + std::to_string(Ds)), [](const momD_conf<T, Ds>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
            return externalWF_vectorboson_conti<T, Tcur, Ds, Ds - 1>(m, l, r);
        });
    }
    template <size_t Pos> inline void add_projective(int_<Pos>) {
        states.emplace(SingleState("mu" + std::to_string(Pos)), [](const momD_conf<T, Ds>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
            return externalWF_vectorboson_mui<T, Tcur, Ds, Pos - 1>(m, l, r);
        });
        states.emplace(SingleState("cont" + std::to_string(Pos)),
                       [](const momD_conf<T, Ds>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                           return externalWF_vectorboson_conti<T, Tcur, Ds, Pos - 1>(m, l, r);
                       });
        add_projective(int_<Pos + 1>{});
    }
    inline void add_normal(int_<4>) {
        states.emplace(SingleState("m"), construct_external_current(SingleState("m")));
        states.emplace(SingleState("p"), construct_external_current(SingleState("p")));
    }
    inline void add_normal(int_<5>) {
        states.emplace(SingleState("mD"), construct_external_current(SingleState("mD")));
        states.emplace(SingleState("pD"), construct_external_current(SingleState("pD")));
        states.emplace(SingleState("sD"), construct_external_current(SingleState("sD")));
        states.emplace(SingleState("scD"), construct_external_current(SingleState("scD")));
        add_normal(int_<4>{});
    }
    inline void add_normal(int_<6>) {
        states.emplace(SingleState("tD"), construct_external_current(SingleState("tD")));
        states.emplace(SingleState("tcD"), construct_external_current(SingleState("tcD")));
        add_normal(int_<5>{});
    }
    template <size_t Pos> inline void add_normal(int_<Pos>) {
        states.emplace(SingleState("uD" + std::to_string(Pos - 2)),
                       [](const momD_conf<T, Ds>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                           return externalWF_vectorboson_trivial_Ds<T, Tcur, Ds, Pos - 2>(m, l, r);
                       });
        states.emplace(SingleState("ucD" + std::to_string(Pos - 2)),
                       [](const momD_conf<T, Ds>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                           return externalWF_vectorboson_trivial_conjugated_Ds<T, Tcur, Ds, Pos - 2>(m, l, r);
                       });
        add_normal(int_<Pos - 1>{});
    }
};

// Implementation of Propagator
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> Vector_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& mom, const BuilderContainer<T>& bc) {
    // massless propagator
    // NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)

    prop = T(1) / (mom * mom);
    // include negative from numerator of propagator -g^{\mu\nu}
    T tomultiply = T(-1) * prop;

    // multiply curent
    std::vector<Tcur> out(in);
    for (auto& it : out) { it *= tomultiply; }

    return out;
}

template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> Cut_Vector_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& mom, const BuilderContainer<T>& bc) {
    // cut massless propagator
    prop = T(1);

    // multiply curent by -1 (from the Feynman-gauge completeness relation -g^{\mu\nu})
    std::vector<Tcur> out(in);
    for (auto& it : out) { it *= T(-1); }
    return out;
}

template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> Unit_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& mom, const BuilderContainer<T>& bc) {
    // just 1
    prop = T(1);
    // return the same input current
    return in;
}

// For auxiliary tensor in QCD we never include propagator (except directly in the CurrentRule as in Eq. (3.5) of hep-ph/0607057)
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> Trivial_Tensor_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& mom, const BuilderContainer<T>& bc) {
    // just 1
    // prop=T(1);
    // doesn't operate on current
    return in;
}


/******************************************************************************
 * MASSIVE VECTOR BOSONS
 ******************************************************************************/
template <typename T, typename Tcur, size_t Ds, PropagatorType ptype>
std::vector<Tcur> massive_Vector_Propagator(const std::vector<Tcur>& in, T& prop, const momD<T, Ds>& mom, const size_t& midx, const BuilderContainer<T>& bc) {
    // massive propagator
    // NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
    const auto& msq = bc.mass[midx] * bc.mass[midx];

    if constexpr (ptype == PropagatorType::propagator) prop = T(1) / (mom * mom - msq);
    if constexpr (ptype == PropagatorType::cut) prop = T(1);

    // Use this in Gravity models
    T pJ = Tensor<T, Ds, 1>(mom) * Tensor<T, Ds, 1>(v_extended_t_to_v_t<T,Tcur>(in));
    // Use this in QCD models
    //for (size_t mu = 0; mu < Ds; mu++) pJ += mom[mu] * T(get_metric_signature<T>(mu)) * in[mu];

    // outgoing current
    std::vector<Tcur> out(Ds, Tcur(0));
    for (size_t mu = 0; mu < Ds; mu++) out[mu] = prop * (T(-1) * in[mu] + pJ / msq * mom[mu]);

    return out;
}

/**
 * External state for positive polarization
 * @param mc The momentum configuration
 * @param i the momentum index of the massive momentum
 * @param ref the momentum index of the massless reference vector
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_massive_vector_p(const momD_conf<T, Ds>& mc, const size_t& i, const size_t& ref, const size_t& midx,
                                              const BuilderContainer<T>& bc) {
    assert(midx > 0);
    using namespace Massive_vector_states; 
    const T& mass = bc.mass[midx];
    momentumD<T, Ds> q;
    switch (ref) {
        case 0: q = clifford_algebra::get_helper_momD<T, Ds>(1); break;
        default: q = mc[ref];
    }

    std::vector<Tcur> out = externalWF_p<T, Tcur, Ds>(mc[i], q, mass);
    return out;
}

/**
 * External state for negative polarization
 * @param mc The momentum configuration
 * @param i the momentum index of the massive momentum
 * @param ref the momentum index of the massless reference vector
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_massive_vector_m(const momD_conf<T, Ds>& mc, const size_t& i, const size_t& ref, const size_t& midx,
                                              const BuilderContainer<T>& bc) {
    assert(midx > 0);
    using namespace Massive_vector_states; 
    const T& mass = bc.mass[midx];
    momentumD<T, Ds> q;
    switch (ref) {
        case 0: q = clifford_algebra::get_helper_momD<T, Ds>(1); break;
        default: q = mc[ref];
    }

    std::vector<Tcur> out = externalWF_m<T, Tcur, Ds>(mc[i], q, mass);
    return out;
}

/**
 * External state for longitudinal polarization
 * @param mc The momentum configuration
 * @param i the momentum index of the massive momentum
 * @param ref the momentum index of the massless reference vector
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_massive_vector_L(const momD_conf<T, Ds>& mc, const size_t& i, const size_t& ref, const size_t& midx,
                                              const BuilderContainer<T>& bc) {
    assert(midx > 0);
    using namespace Massive_vector_states; 
    const T& mass = bc.mass[midx];
    momentumD<T, Ds> q;
    switch (ref) {
        case 0: q = clifford_algebra::get_helper_momD<T, Ds>(1); break;
        default: q = mc[ref];
    }

    std::vector<Tcur> out = externalWF_L<T, Tcur, Ds>(mc[i], q, mass);
    return out;
}

/**
 * A struct, which contains massive vector states.
 *
 * States of correct type and correct dimensionality are collected by constructor.
 * The structure is supposed to be used a static instant.
 */
template <typename T, size_t Ds> class massive_vector_states {
    static_assert(Ds >= 4, "Dimensional mismatch");

  public:
    typedef typename Current_Type_Selector<T>::Tcur Tcur;

  private:
    typedef typename std::map<SingleState, ExternalCurrent<T, Tcur, Ds>> MT;
    MT states;
    template <size_t N> using int_ = Caravel::_utilities_private::int_<N>;

  public:
    massive_vector_states(size_t midx) {
        auto construct_state = [midx](SingleState s) -> ExternalCurrent<T, Tcur, Ds> {
            // External states
            if (s.get_name() == "Ward")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_Ward<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "m")
                return [midx](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_massive_vector_m<T, Tcur, Ds>(mc, i, r, midx, bc);
                };
            else if (s.get_name() == "p")
                return [midx](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_massive_vector_p<T, Tcur, Ds>(mc, i, r, midx, bc);
                };
            else if (s.get_name() == "L")
                return [midx](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_massive_vector_L<T, Tcur, Ds>(mc, i, r, midx, bc);
                };

            // Cut states
            if (s.get_name() == "mD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_mD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "mcD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_pD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "pD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_pD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "pcD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_mD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "sD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_sD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "scD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_scD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "vD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return Massive_vector_states::WF_massive_vectorboson_extraD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "vcD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return Massive_vector_states::WF_massive_vectorboson_extracD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "tD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_tD<T, Tcur, Ds>(mc, i, r);
                };
            else if (s.get_name() == "tcD")
                return [](const momD_conf<T, Ds>& mc, const size_t& i, const size_t& r, const BuilderContainer<T>& bc) {
                    return externalWF_vectorboson_tcD<T, Tcur, Ds>(mc, i, r);
                };

            // You should never reach here
            throw std::logic_error("Unknown Helicity state: " + s.get_name() + " for Ds = " + std::to_string(Ds));
        }; // construct_state

        // 4D states
        states = {
            {SingleState("p"), construct_state(SingleState("p"))},
            {SingleState("m"), construct_state(SingleState("m"))},
            {SingleState("L"), construct_state(SingleState("L"))},
            {SingleState("Ward"), construct_state(SingleState("Ward"))},
        };

        /* Adding cut-states:
         * For massive vector boson there are Ds-1 independent polarization vectors.
         * For Ds = 5 (with 5-dim loop momentum) ep = { t1, t2, t3, t4 }
         * For Ds = 6 (with 6-dim loop momentum) ep = { t1, t2, t3, t4, t5 }
         * For Ds > 6 (but still 6-dim loop momenta) we can fill up the remaining space with trivial states
         * e.g. for Ds = 8,  ep = { t1, t2, t3, t4, t5 | uD6, uD7 }
         */
        // add non-trivial cut states
        if constexpr (Ds > 4) {
            states.emplace(SingleState("mD"), construct_state(SingleState("mD")));
            states.emplace(SingleState("mcD"), construct_state(SingleState("mcD")));
            states.emplace(SingleState("pD"), construct_state(SingleState("pD")));
            states.emplace(SingleState("pcD"), construct_state(SingleState("pcD")));
            states.emplace(SingleState("sD"), construct_state(SingleState("sD")));
            states.emplace(SingleState("scD"), construct_state(SingleState("scD")));
            states.emplace(SingleState("vD"), construct_state(SingleState("vD")));
            states.emplace(SingleState("vcD"), construct_state(SingleState("vcD")));
        }
        if constexpr (Ds > 5) {
            states.emplace(SingleState("tD"), construct_state(SingleState("tD")));
            states.emplace(SingleState("tcD"), construct_state(SingleState("tcD")));
        }

        // add trivial cut states
        add_trivial(int_<Ds>{});
    }

    const MT& get() const { return states; }

  private:
    inline void add_trivial(int_<4>) {}
    inline void add_trivial(int_<5>) {}
    inline void add_trivial(int_<6>) {}
    template <size_t Pos> inline void add_trivial(int_<Pos>) {
        states.emplace(SingleState("uD" + std::to_string(Pos - 1)),
                       [](const momD_conf<T, Ds>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                           return externalWF_vectorboson_trivial_Ds<T, Tcur, Ds, Pos - 2>(m, l, r);
                       });
        states.emplace(SingleState("ucD" + std::to_string(Pos - 1)),
                       [](const momD_conf<T, Ds>& m, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                           return externalWF_vectorboson_trivial_conjugated_Ds<T, Tcur, Ds, Pos - 2>(m, l, r);
                       });
        add_trivial(int_<Pos - 1>{});
    }
};
} // namespace BG
} // namespace Caravel
