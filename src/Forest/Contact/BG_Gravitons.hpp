
namespace Caravel{ namespace BG{

// For auxiliary rank-3 tensor for CubicGravity
template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> CubicTensor_Propagator(const std::vector<Tcur>& in,T& prop,const momentumD<T,Ds>& mom, const BuilderContainer<T>& bc){

	//TODO Should this be Ds or D?
	int dims(Ds);

	// the incoming auxiliary tensor
	Tensor<T,Ds,3> Ain(v_extended_t_to_v_t<T,Tcur>(in));

	Tensor<T,Ds,3> ATrans(0);
	Tensor<T,Ds,1> ATrace(trace_indices(1,2,Ain));
	Tensor<T,Ds,3> ATraceeta(0);
	// the metric tensor
	const std::vector<T>& metric(ATraceeta.get_metric_extended());

	Tensor<T,Ds,3> Aout(0);

	// Symmetrize incoming tensor (necessary because vertices are not explicitly symmetrized)
	Ain = T(1)/T(2)*(Ain+swap_indices<1,2>(Ain));

	for(size_t ii=0;ii<Ds;ii++){
			for(size_t jj=0;jj<Ds;jj++){
				for(size_t kk=0;kk<Ds;kk++){
						Aout[ii][jj][kk] = Ain[jj][kk][ii]+Ain[kk][jj][ii]-Ain[ii][jj][kk];
				}
			}
	}

	for(size_t ii=0;ii<Ds;ii++){
			for(size_t jj=0;jj<Ds;jj++){
					ATraceeta[ii][jj][jj]=T(1)/metric[jj]*ATrace[ii];
			}
	}

	Aout += T(1)/(T(dims)-T(2))*ATraceeta;
	Aout *= T(1)/T(2);
	// factor of -I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
	//Aout *= (T(0,-1));

	return v_t_to_v_extended_t<T,Tcur>(Aout.vectorize());
}



template <typename T, typename Tcur, size_t D> std::vector<Tcur> CurrentRule_GGA_A(const std::vector<momentumD<T,D> const*>& momlist,
		const std::vector<CurrentWorker_Template_Base<T,D> const*>& currentlist, const BuilderContainer<T>& bc){

	int dim(D);

	// incoming momenta
	const momD<T,D> p1(-(*(momlist[0])));
	const momD<T,D> p2(-(*(momlist[1])));
	const momD<T,D> p3(-p1-p2);

	// the metric tensor
	Tensor<T,D,2> eta(0);
	const std::vector<T>& metric(eta.get_metric_extended());
	for(size_t ii=0;ii<D;ii++)	eta[ii][ii]=T(1)/metric[ii];

	// the input gravitons
	Tensor<T,D,2> h1(v_extended_t_to_v_t<T,Tcur>(currentlist[0]->get_current()));
	Tensor<T,D,2> h2(v_extended_t_to_v_t<T,Tcur>(currentlist[1]->get_current()));

	Tensor<T,D,3> Aout(0);
	Tensor<T,D,1> V(0);

	T traceh1(trace_rank2(h1));
	T traceh2(trace_rank2(h2));

	Tensor<T,D,1> p1_h2(index_contraction<0>(p1,h2));
	Tensor<T,D,1> p2_h1(index_contraction<0>(p2,h1));
	Tensor<T,D,2> h1_h2(index_contraction<0,1>(h1,h2));
	//Tensor<T,D,2> h2_h1(index_contraction<0,1>(h2,h1));


	V += T(-1)/(T(dim)-T(2))*(p1_h2*traceh1+p2_h1*traceh2);

	Aout += concatenate_tensors(eta,V);


	Aout += T(-1)*swap_indices<0,1>(concatenate_tensors(p1,h1_h2));
	Aout += T(-1)*swap_indices<0,2>(concatenate_tensors(p2,h1_h2));

 	Aout += concatenate_tensors(h1,p1_h2);
	Aout += concatenate_tensors(h2,p2_h1);

	Aout += T(-1)*concatenate_tensors(p3,h1_h2);

	Aout += T(1)/(T(dim)-T(2))*(traceh2*swap_indices<0,1>(concatenate_tensors(p2,h1)));
	Aout += T(1)/(T(dim)-T(2))*(traceh1*swap_indices<0,1>(concatenate_tensors(p1,h2)));

	return v_t_to_v_extended_t<T,Tcur>(Aout.vectorize());
}



/**
 * To produce a graviton current according to equation (32) considering the hhA (GGA) vertex
 *
 * For now we do not factor out any constant factors, but this should be considered later to get proper normalization
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> CurrentRule_GGA_G(size_t i_G,size_t i_A,const std::vector<momentumD<T,D> const*>& momlist,
		const std::vector<CurrentWorker_Template_Base<T,D> const*>& currentlist, const BuilderContainer<T>& bc){

	int dim(D);

	// the metric tensor
	Tensor<T,D,2> eta(0);
	const std::vector<T>& metric(eta.get_metric_extended());
	for(size_t ii=0;ii<D;ii++)	eta[ii][ii]=T(1)/metric[ii];

	// incoming momenta
	const momD<T,D> p_G(-(*(momlist[i_G])));
	const momD<T,D> p_A(-(*(momlist[i_A])));
	const momD<T,D> p3(-p_G-p_A);

	// the input cubictensor
	Tensor<T,D,3> A(v_extended_t_to_v_t<T,Tcur>(currentlist[i_A]->get_current()));
	// the input graviton
	Tensor<T,D,2> hin(v_extended_t_to_v_t<T,Tcur>(currentlist[i_G]->get_current()));
	Tensor<T,D,2> hout(0);

	Tensor<T,D,1> A_trace(trace_indices(0,1,A));
	Tensor<T,D,1> Ah(0);
	Tensor<T,D,2> scal_eta(0);
	T h_trace(trace_rank2(hin));
  T scal(0);

	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
				for(size_t kk=0;kk<D;kk++){
					Ah[ii]+=metric[kk]*metric[jj]*A[jj][ii][kk]*hin[jj][kk];
				}
		}
	}

	scal += T(1)/(T(dim)-T(2))*trace_rank2(concatenate_tensors(p3,Ah-index_contraction<0,0>(A_trace,hin)));

	for(size_t ii=0;ii<D;ii++){
		scal_eta[ii][ii]=scal*T(1)/metric[ii];
	}

	hout += T(-1)*index_contraction<0>(p_G,index_contraction<0,0>(A,hin));
	hout += T(-1)*index_contraction<0>(p_A,index_contraction<2,1>(A,hin));
	hout += T(-1)*index_contraction<1>(p3,index_contraction<2,1>(A,hin));
	hout += index_contraction<2>(p3,index_contraction<2,1>(A,hin));

	hout += T(1)/(T(dim)-T(2))*h_trace*index_contraction<2>(p_G,A);

	hout += concatenate_tensors(p_G,Ah);

	hout += T(-1)/(T(dim)-T(2))*h_trace*concatenate_tensors(p_G,A_trace);

	hout += scal_eta;

	return v_t_to_v_extended_t<T,Tcur>(hout.vectorize());
}

/**
 * To produce a graviton current according to equation (32) considering the hhh (GGG) vertex
 *
 * For now we do not factor out any constant factors, but this should be considered later to get proper normalization
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> CurrentRule_GGG(const std::vector<momentumD<T,D> const*>& momlist,
		const std::vector<CurrentWorker_Template_Base<T,D> const*>& currentlist, const BuilderContainer<T>& bc){

	int dim(D);

	// incoming momenta
	const momD<T,D> p1(-(*(momlist[0])));
	const momD<T,D> p2(-(*(momlist[1])));
	const momD<T,D> p3(-p1-p2);

	// the input gravitons
	Tensor<T,D,2> h1(v_extended_t_to_v_t<T,Tcur>(currentlist[0]->get_current()));
	Tensor<T,D,2> h2(v_extended_t_to_v_t<T,Tcur>(currentlist[1]->get_current()));

	T traceh1(trace_rank2(h1));
	T traceh2(trace_rank2(h2));
	T scal(0);

	Tensor<T,D,1> p1_h2(index_contraction<0>(p1,h2));
	Tensor<T,D,1> p2_h1(index_contraction<0>(p2,h1));
	Tensor<T,D,2> h1_h2(index_contraction<0,1>(h1,h2));
	//Tensor<T,D,2> h2_h1(index_contraction<0,1>(h2,h1));
	Tensor<T,D,2> p1_p2(0);
	Tensor<T,D,2> scal_eta(0);

	const std::vector<T>& metric(scal_eta.get_metric_extended());

	for (size_t ii = 0; ii < D; ii++) {
		for (size_t jj = 0; jj < D; jj++) {
					p1_p2[ii][jj]=p1.pi(ii)*p2.pi(jj);
		}
	}



	// NOTICE Manifest symmetry under exchange 1<->2

	Tensor<T,D,2> hout(0);

	scal += T(1)/(T(2)*(T(dim)-T(2)))*trace_rank2(concatenate_tensors(p3,p1_h2))*traceh1;
	scal += T(1)/(T(2)*(T(dim)-T(2)))*trace_rank2(concatenate_tensors(p3,p2_h1))*traceh2;

	scal += T(1)/(T(2)*(T(dim)-T(2)))*trace_rank2(h1_h2)*(p3*p3);
	 //
	for(size_t ii=0;ii<D;ii++){
		scal_eta[ii][ii]=scal*T(1)/metric[ii];
	}
	hout += scal_eta;

	hout += T(1)/(T(2)*(T(dim)-T(2)))*h2*traceh1*(p1*p1);
	hout += T(1)/(T(2)*(T(dim)-T(2)))*h1*traceh2*(p2*p2);

	hout += T(-1)*h1_h2*(p1*p1);
	hout += h1_h2*(p3*p2);

	hout += T(-1)*concatenate_tensors(p1_h2,p2_h1);

	hout += T(1)/(T(2)*(T(dim)-T(2)))*traceh1*traceh2*p1_p2;
	hout += T(-1)/T(2)*trace_rank2(h1_h2)*p1_p2;

	hout += T(-1)*concatenate_tensors(p1,index_contraction<0>(p3,h1_h2));
	hout += T(-1)*concatenate_tensors(p2,index_contraction<1>(p3,h1_h2));

	hout += T(-1)/T(2)*h1*trace_rank2(concatenate_tensors(p3,p1_h2));
	hout += T(-1)/T(2)*h2*trace_rank2(concatenate_tensors(p3,p2_h1));

	// factor of I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
	//hout*= T(0,1);

 	return v_t_to_v_extended_t<T,Tcur>(hout.vectorize());
}

/**
 * To produce a graviton current according to equation (32) considering the hAA (GAA) vertex
 *
 * For now we do not factor out any constant factors, but this should be considered later to get proper normalization
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> CurrentRule_GAA_G(const std::vector<momentumD<T,D> const*>& momlist,
	const std::vector<CurrentWorker_Template_Base<T,D> const*>& currentlist, const BuilderContainer<T>& bc){

	int dim(D);

	// the input auxiliary tensors
	Tensor<T,D,3> A1(v_extended_t_to_v_t<T,Tcur>(currentlist[0]->get_current()));
	Tensor<T,D,3> A2(v_extended_t_to_v_t<T,Tcur>(currentlist[1]->get_current()));

	Tensor<T,D,1> A1_trace(trace_indices(0,1,A1));
	Tensor<T,D,1> A2_trace(trace_indices(0,1,A2));
	Tensor<T,D,2> A1_A2_contr(0);
	Tensor<T,D,2> hout(0);

	// the metric tensor
	const std::vector<T>& metric(hout.get_metric_extended());

	for(size_t ii=0;ii<D;ii++){
		for(size_t jj=0;jj<D;jj++){
				for(size_t kk=0;kk<D;kk++){
					for(size_t ll=0;ll<D;ll++){
					A1_A2_contr[ii][jj]+=metric[kk]*metric[ll]*A1[kk][ii][ll]*A2[ll][jj][kk];
				}
			}
		}
	}

	hout += T(-2)/(T(dim)-T(1))*concatenate_tensors(A1_trace,A2_trace);
	hout += T(2)*A1_A2_contr;

	// factor of -I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
	hout *= T(-1);

	return v_t_to_v_extended_t<T,Tcur>(hout.vectorize());
}

/**
 * To produce an auxiliary rank-3 tensor current according to equation (32) considering the hAA (GAA) vertex
 *
 * For now we do not factor out any constant factors, but this should be considered later to get proper normalization
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> CurrentRule_GAA_A(size_t i_G,size_t i_A,const std::vector<momentumD<T,D> const*>& momlist,
		const std::vector<CurrentWorker_Template_Base<T,D> const*>& currentlist, const BuilderContainer<T>& bc){

	int dim(D);

	// the input cubictensor
	Tensor<T,D,3> A(v_extended_t_to_v_t<T,Tcur>(currentlist[i_A]->get_current()));
	// the input graviton
	Tensor<T,D,2> hin(v_extended_t_to_v_t<T,Tcur>(currentlist[i_G]->get_current()));

	Tensor<T,D,1> A_trace(trace_indices(0,1,A));

	// the metric tensor
	Tensor<T,D,2> eta(0);
	const std::vector<T>& metric(eta.get_metric_extended());
	for(size_t ii=0;ii<D;ii++)	eta[ii][ii]=T(1)/metric[ii];

	Tensor<T,D,3> Aout(0);

	Aout += T(2)*swap_indices<0,1>(index_contraction<2,0>(A,hin));
	Aout += T(-2)/(T(dim)-T(1))*concatenate_tensors(eta,index_contraction<0,0>(A_trace,hin));

	// factor of -I factor out and handled by Builder_fixed_Ds::get_overall_normalization_info(.)
	Aout *= T(-1);

	return v_t_to_v_extended_t<T,Tcur>(Aout.vectorize());
}


}}
