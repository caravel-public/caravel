/**
 * @file Tensor.hpp
 *
 * @date 20.6.2018
 *
 * This is a templated implementation of utilities to define generic rank tensors. Initially we focus on readibility and
 * flexibility. Considerable improvement required for improving efficiency
*/

#pragma once

#include <array>
#include <iostream>
#include <functional>
#include <type_traits>
#include <vector>

#include "Core/Utilities.h"
#include "Core/momD.h"
#include "Core/type_traits_extra.h"

namespace Caravel {
namespace BG {

// forward declaration
template <class T,size_t D,size_t Rank> class Tensor;

template <class T,size_t D> std::ostream& operator<<(std::ostream&,Tensor<T,D,1>&);

template <class T,size_t D> std::enable_if_t<is_floating_point<T>::value,void> ASSIGN_METRIC_EXTENDED(std::vector<T> &metric){
	for (size_t ii = 0; ii < D; ii++) {
		metric[ii]=T(get_metric_signature<T>(ii));
	}
}

template <class T,size_t D> std::enable_if_t<!is_floating_point<T>::value,void> ASSIGN_METRIC_EXTENDED(std::vector<T> &metric){
	for (size_t ii = 0; ii < D; ii++) {
		metric[ii]=T(get_metric_signature<T>(ii));
	}
	if(D>=5){
		// it can be zero while no D-dim momenta produced
		if(EpsilonBasis<T>::squares[0]!=T(0)){
			metric[4]*=EpsilonBasis<T>::squares[0];
		}
	}
	if(D>=6){
		// it can be zero while no D-dim momenta produced
		if(EpsilonBasis<T>::squares[1]!=T(0)){
			metric[5]*=EpsilonBasis<T>::squares[1];
		}
	}
}

// 1-D specialization
template <class T,size_t D> class Tensor<T,D,1> {
	private:
		std::array<T,D> container;
		static const std::vector<std::vector<size_t>> traverse;
	public:
		Tensor() = default;
		Tensor(int i) {
			container.fill(T(i)); 
		}
		Tensor(const momD<T,D>& p) {
			for(size_t mu=0;mu<D;mu++) container[mu]=p.pi(mu); 
		}
		std::vector<T> get_metric_extended() const { std::vector<T> metric_extended(D); ASSIGN_METRIC_EXTENDED<T,D>(metric_extended); return metric_extended; }
		const std::vector<std::vector<size_t>>& get_traverse() const { return traverse; }

		friend std::ostream& operator<<<>(std::ostream&,Tensor<T,D,1>&);
		T& operator[](size_t i) {return container[i];}
		const T& operator[](size_t i) const {return container[i];}
		/**
		 * Given a vector of size_t we return the corresponding entry in the Tensor
		 */
		T entry(const std::vector<size_t>& ijs) const {
			return container[ijs.back()];
		}
		/**
		 * Maps the value passed into the corresponding entry specified by the vector
		 */
		void set_entry(const T& val,const std::vector<size_t>& ijs){
			container[ijs.back()]=val;
		}
		Tensor<T,D,1>& operator+=(const Tensor<T,D,1>& t){
			std::transform(&container[0],&container[0]+D,&t.container[0],&container[0],std::plus<T>());
			return *this;
		}
		Tensor<T,D,1>& operator-=(const Tensor<T,D,1>& t){
			std::transform(&container[0],&container[0]+D,&t.container[0],&container[0],std::minus<T>());
			return *this;
		}
		Tensor<T,D,1>& operator*=(const T& t){
			std::transform(&container[0],&container[0]+D,&container[0],std::bind(std::multiplies<T>(), std::placeholders::_1, t));
			return *this;
		}
		Tensor<T,D,1>& operator/=(const T& t){
			std::transform(&container[0],&container[0]+D,&container[0],std::bind(std::divides<T>(), std::placeholders::_1, t));
			return *this;
		}
};

template <class T,size_t D> std::ostream& operator<<(std::ostream& o,Tensor<T,D,1>& t){
	o<<"{"<<t.container[0];
	for(size_t ii=1;ii<D;ii++)
		o<<","<<t.container[ii];
	o<<"}";
	return o;
}

template <size_t D> std::vector<std::vector<size_t>> fill_traverse_1(){
	std::vector<std::vector<size_t>> toret;
	for (size_t ii = 0; ii < D; ii++)
		toret.push_back(std::vector<size_t>({ii}));
	return toret;
}

template <class T,size_t D> const std::vector<std::vector<size_t>> Tensor<T,D,1>::traverse = fill_traverse_1<D>();


#define _USE_EXPLICIT_RANK_2_AND_3 1

#if _USE_EXPLICIT_RANK_2_AND_3
template <class T,size_t D> std::ostream& operator<<(std::ostream&,Tensor<T,D,2>&);

// 2-D specialization
template <class T,size_t D> class Tensor<T,D,2> {
	private:
		T container[D][D];
		static const std::vector<std::vector<size_t>> traverse;
	public:
		Tensor() = default;
		Tensor(int i) {
			for(size_t ii=0;ii<D;ii++) std::fill(container[ii],container[ii]+D,T(i)); 
		}
		Tensor(const std::vector<T>& in) {
			map_vector(in);
		}
		std::vector<T> get_metric_extended() const { std::vector<T> metric_extended(D); ASSIGN_METRIC_EXTENDED<T,D>(metric_extended); return metric_extended; }
		const std::vector<std::vector<size_t>>& get_traverse() const { return traverse; }

		friend std::ostream& operator<<<>(std::ostream&,Tensor<T,D,2>&);
		T* operator[](size_t i) {return container[i];}
		const T* operator[](size_t i) const {return container[i];}
		/**
		 * Given a vector of size_t we return the corresponding entry in the Tensor
		 */
		T entry(const std::vector<size_t>& ijs) const {
			return container[ijs[0]][ijs[1]];
		}
		/**
		 * Maps the value passed into the corresponding entry specified by the vector
		 */
		void set_entry(const T& val,const std::vector<size_t>& ijs){
			container[ijs[0]][ijs[1]]=val;
		}
		/**
		 * This method maps the full content of the tensor into a vector in a canonical way
		 */
		std::vector<T> vectorize() const {
			static const size_t size(D*D);
			std::vector<T> toret(size);
			size_t counter(0);
			for(size_t ii=0;ii<D;ii++){
				std::copy(container[ii],container[ii]+D,&toret[counter]);
				counter+=D;
			}
			return toret;
		}
		/**
		 * This method helps filling the contents of this Tensor with the information from the input vector
		 * consistently with vectorize()
		 */
		void map_vector(const std::vector<T>& in) {
			size_t counter(0);
			for(size_t ii=0;ii<D;ii++){
				std::copy(&in[counter],&in[counter]+D,container[ii]);
				counter+=D;
			}
		}
		Tensor<T,D,2>& operator+=(const Tensor<T,D,2>& t){
			for(size_t ii=0;ii<D;ii++) 
				std::transform(container[ii],container[ii]+D,t.container[ii],container[ii],std::plus<T>());
			return *this;
		}
		Tensor<T,D,2>& operator-=(const Tensor<T,D,2>& t){
			for(size_t ii=0;ii<D;ii++) 
				std::transform(container[ii],container[ii]+D,t.container[ii],container[ii],std::minus<T>());
			return *this;
		}
		Tensor<T,D,2>& operator*=(const T& t){
			for(size_t ii=0;ii<D;ii++) 
				std::transform(container[ii],container[ii]+D,container[ii],std::bind(std::multiplies<T>(), std::placeholders::_1, t));
			return *this;
		}
		Tensor<T,D,2>& operator/=(const T& t){
			for(size_t ii=0;ii<D;ii++) 
				std::transform(container[ii],container[ii]+D,container[ii],std::bind(std::divides<T>(), std::placeholders::_1, t));
			return *this;
		}
};

template <class T,size_t D> std::ostream& operator<<(std::ostream& o,Tensor<T,D,2>& t){
	o<<"{";
	for(size_t ii=0;ii<D;ii++){
		o<<"{"<<t.container[ii][0];
		for(size_t jj=1;jj<D;jj++)
			o<<","<<t.container[ii][jj];
		o<<"}";
		if(ii+1==D)
			o<<"\n";
		else
			o<<",\n";
	}
	o<<"}";
	return o;
}

template <size_t D> std::vector<std::vector<size_t>> fill_traverse_2(){
	std::vector<std::vector<size_t>> toret;
	for (size_t ii = 0; ii < D; ii++)
		for (size_t jj = 0; jj < D; jj++)
			toret.push_back(std::vector<size_t>({ii,jj}));
	return toret;
}

template <class T,size_t D> const std::vector<std::vector<size_t>> Tensor<T,D,2>::traverse = fill_traverse_2<D>();

template <class T,size_t D> std::ostream& operator<<(std::ostream&,Tensor<T,D,3>&);

// 3-D specialization
template <class T,size_t D> class Tensor<T,D,3> {
	private:
		T container[D][D][D];
		static const std::vector<std::vector<size_t>> traverse;
	public:
		Tensor() = default;
		Tensor(int i) {
			for(size_t ii=0;ii<D;ii++) for(size_t jj=0;jj<D;jj++) std::fill(container[ii][jj],container[ii][jj]+D,T(i)); 
		}
		Tensor(const std::vector<T>& in) {
			map_vector(in);
		}
		std::vector<T> get_metric_extended() const { std::vector<T> metric_extended(D); ASSIGN_METRIC_EXTENDED<T,D>(metric_extended); return metric_extended; }
		const std::vector<std::vector<size_t>>& get_traverse() const { return traverse; }

		friend std::ostream& operator<<<>(std::ostream&,Tensor<T,D,3>&);
		auto operator[](size_t i) {return container[i];}
		auto operator[](size_t i) const {return container[i];}
		/**
		 * Given a vector of size_t we return the corresponding entry in the Tensor
		 */
		T entry(const std::vector<size_t>& ijs) const {
			return container[ijs[0]][ijs[1]][ijs[2]];
		}
		/**
		 * Maps the value passed into the corresponding entry specified by the vector
		 */
		void set_entry(const T& val,const std::vector<size_t>& ijs){
			container[ijs[0]][ijs[1]][ijs[2]]=val;
		}
		/**
		 * This method maps the full content of the tensor into a vector in a canonical way
		 */
		std::vector<T> vectorize() const {
			static const size_t size(D*D*D);
			std::vector<T> toret(size);
			size_t counter(0);
			for(size_t ii=0;ii<D;ii++)
				for(size_t jj=0;jj<D;jj++){
					std::copy(container[ii][jj],container[ii][jj]+D,&toret[counter]);
					counter+=D;
				}
			return toret;
		}
		/**
		 * This method helps filling the contents of this Tensor with the information from the input vector
		 * consistently with vectorize()
		 */
		void map_vector(const std::vector<T>& in) {
			size_t counter(0);
			for(size_t ii=0;ii<D;ii++)
				for(size_t jj=0;jj<D;jj++){
					std::copy(&in[counter],&in[counter]+D,container[ii][jj]);
					counter+=D;
				}
		}
		Tensor<T,D,3>& operator+=(const Tensor<T,D,3>& t){
			for(size_t ii=0;ii<D;ii++) 
				for(size_t jj=0;jj<D;jj++) 
					std::transform(container[ii][jj],container[ii][jj]+D,t.container[ii][jj],container[ii][jj],std::plus<T>());
			return *this;
		}
		Tensor<T,D,3>& operator-=(const Tensor<T,D,3>& t){
			for(size_t ii=0;ii<D;ii++) 
				for(size_t jj=0;jj<D;jj++) 
					std::transform(container[ii][jj],container[ii][jj]+D,t.container[ii][jj],container[ii][jj],std::minus<T>());
			return *this;
		}
		Tensor<T,D,3>& operator*=(const T& t){
			for(size_t ii=0;ii<D;ii++) 
				for(size_t jj=0;jj<D;jj++) 
					std::transform(container[ii][jj],container[ii][jj]+D,container[ii][jj],std::bind(std::multiplies<T>(), std::placeholders::_1, t));
			return *this;
		}
		Tensor<T,D,3>& operator/=(const T& t){
			for(size_t ii=0;ii<D;ii++) 
				for(size_t jj=0;jj<D;jj++) 
					std::transform(container[ii][jj],container[ii][jj]+D,container[ii][jj],std::bind(std::divides<T>(), std::placeholders::_1, t));
			return *this;
		}
};

template <class T,size_t D> std::ostream& operator<<(std::ostream& o,Tensor<T,D,3>& t){
	o<<"{";
	for(size_t ii=0;ii<D;ii++){
	    o<<"{";
	    for(size_t jj=0;jj<D;jj++){
		o<<"{"<<t.container[ii][jj][0];
		for(size_t kk=1;kk<D;kk++)
			o<<","<<t.container[ii][jj][kk];
		o<<"}";
		if(jj+1==D)
			o<<"\n";
		else
			o<<",\n";
	    }
	    o<<"}";
	    if(ii+1==D)
		o<<"\n";
	    else
		o<<",\n";
	}
	o<<"}";
	return o;
}

template <size_t D> std::vector<std::vector<size_t>> fill_traverse_3(){
	std::vector<std::vector<size_t>> toret;
	for (size_t ii = 0; ii < D; ii++)
		for (size_t jj = 0; jj < D; jj++)
			for (size_t kk = 0; kk < D; kk++)
				toret.push_back(std::vector<size_t>({ii,jj,kk}));
	return toret;
}

template <class T,size_t D> const std::vector<std::vector<size_t>> Tensor<T,D,3>::traverse = fill_traverse_3<D>();

#endif

template <size_t Pow> size_t small_power(size_t);

template <> inline size_t small_power<1>(size_t D) { return D; }

template <size_t Pow> size_t small_power(size_t D) { return D*small_power<Pow-1>(D); }

template <class T,size_t D,size_t Rank> std::ostream& operator<<(std::ostream&,Tensor<T,D,Rank>&);

template <size_t D,size_t Rank,size_t Pos> std::enable_if_t<(Pos>Rank),void> fill_traverse_tensor(std::vector<std::vector<size_t>>& traverse){
}

template <size_t D,size_t Rank,size_t Pos> std::enable_if_t<(Pos<=Rank),void> fill_traverse_tensor(std::vector<std::vector<size_t>>& traverse){
	for(size_t ii=0;ii<traverse.size();ii++){
		auto& tofill(traverse[ii]);
		size_t base(1);
		for(size_t jj=0;jj<Rank-Pos;jj++)
			base*=D;
		tofill[Pos-1]= (ii/base)%D;
	}
	if(Pos<Rank)
		fill_traverse_tensor<D,Rank,Pos+1>(traverse);
}

template <class T,size_t D,size_t Rank> class Tensor {
	private:
		std::array<Tensor<T,D,Rank-1>,D> container;
		static thread_local std::vector<std::vector<size_t>> traverse;
		static thread_local bool traverse_defined;
	public:
		Tensor() { if(!traverse_defined){ map_to_vectorize_get_metric(); traverse_defined=true; } }
		Tensor(const std::vector<T>& in) {
			if(!traverse_defined){ map_to_vectorize_get_metric(); traverse_defined=true; }
			map_vector(in);
		}
		Tensor(int i) {
			if(!traverse_defined){ map_to_vectorize_get_metric(); traverse_defined=true; }
			container.fill(Tensor<T,D,Rank-1>(i));
		}
		void map_to_vectorize_get_metric(){
			// this must be improved for efficiency! Scrambled data can have a big impact on performance, particularly for large D!
			static const size_t size(small_power<Rank>(D));
			traverse=std::vector<std::vector<size_t>>(size,std::vector<size_t>(Rank));
			fill_traverse_tensor<D,Rank,1>(traverse);
#if 0
std::cout<<"The result: ";
for(auto& a:traverse){
  for(auto& i:a)
    std::cout<<i<<" ";
  std::cout<<std::endl;
}
#endif
		}
		Tensor<T,D,Rank-1>& operator[](size_t i) {return container[i];}
		const std::vector<std::vector<size_t>>& get_traverse() const { return traverse; }
		std::vector<T> get_metric_extended() const { std::vector<T> metric_extended(D); ASSIGN_METRIC_EXTENDED<T,D>(metric_extended); return metric_extended; }
		const Tensor<T,D,Rank-1>& operator[](size_t i) const {return container[i];}
		friend std::ostream& operator<<<>(std::ostream&,Tensor<T,D,Rank>&);
		/**
		 * Given a vector of size_t we return the corresponding entry in the Tensor
		 */
		T entry(const std::vector<size_t>& ijs) const {
			size_t le(ijs[0]);
			std::vector<size_t> lijs(ijs.size()-1);
			std::copy(ijs.begin()+1,ijs.end(),lijs.begin());
			return container[le].entry(lijs);
		}
		/**
		 * Maps the value passed into the corresponding entry specified by the vector
		 */
		void set_entry(const T& val,const std::vector<size_t>& ijs){
			size_t le(ijs[0]);
			std::vector<size_t> lijs(ijs.size()-1);
			std::copy(ijs.begin()+1,ijs.end(),lijs.begin());
			container[le].set_entry(val,lijs);
		}
		/**
		 * This method maps the full content of the tensor into a vector in a canonical way
		 */
		std::vector<T> vectorize() const {
			static const size_t size(small_power<Rank>(D));
			std::vector<T> toret(size);
			for(size_t ii=0;ii<size;ii++)
				toret[ii]=entry(traverse[ii]);
			return toret;
		}
		/**
		 * This method helps filling the contents of this Tensor with the information from the input vector
		 * consistently with vectorize()
		 */
		void map_vector(const std::vector<T>& in) {
			for(size_t ii=0;ii<in.size();ii++)
				set_entry(in[ii],traverse[ii]);
		}
		Tensor<T,D,Rank>& operator+=(const Tensor<T,D,Rank>& t){
			for(size_t ii=0;ii<D;ii++) container[ii]+=t.container[ii];
			return *this;
		}
		Tensor<T,D,Rank>& operator-=(const Tensor<T,D,Rank>& t){
			for(size_t ii=0;ii<D;ii++) container[ii]-=t.container[ii];
			return *this;
		}
		Tensor<T,D,Rank>& operator*=(const T& t){
			for(size_t ii=0;ii<D;ii++) container[ii]*=t;
			return *this;
		}
		Tensor<T,D,Rank>& operator/=(const T& t){
			for(size_t ii=0;ii<D;ii++) container[ii]/=t;
			return *this;
		}
};

template <class T,size_t D,size_t Rank> thread_local std::vector<std::vector<size_t>> Tensor<T,D,Rank>::traverse = std::vector<std::vector<size_t>>();
template <class T,size_t D,size_t Rank> thread_local bool Tensor<T,D,Rank>::traverse_defined = false;


template <class T,size_t D,size_t Rank> std::ostream& operator<<(std::ostream& o,Tensor<T,D,Rank>& t){
	o<<"{";
	for(size_t ii=0;ii<D;ii++){
		o<<t[ii];
		if(ii+1!=D)
			o<<",\n";
		else
			o<<"\n";
	}
	o<<"}";
	return o;
}

// basic operations
template <class T,size_t D,size_t Rank> inline Tensor<T,D,Rank> operator+(Tensor<T,D,Rank> t1,const Tensor<T,D,Rank>& t2){
	return t1+=t2;
}
template <class T,size_t D,size_t Rank> inline Tensor<T,D,Rank> operator-(Tensor<T,D,Rank> t1,const Tensor<T,D,Rank>& t2){
	return t1-=t2;
}
template <class T,size_t D,size_t Rank> inline Tensor<T,D,Rank> operator-(Tensor<T,D,Rank> t1){
	return t1*=T(-1);
}
template <class T,size_t D,size_t Rank> inline Tensor<T,D,Rank> operator*(const T& c,Tensor<T,D,Rank> t1){
	return t1*=c;
}
template <class T,size_t D,size_t Rank> inline Tensor<T,D,Rank> operator*(Tensor<T,D,Rank> t1,const T& c){
	return t1*=c;
}
template <class T,size_t D,size_t Rank> inline Tensor<T,D,Rank> operator/(Tensor<T,D,Rank> t1,const T& c){
	return t1/=c;
}
// This represents a full contraction of two same-rank tensors
template <class T,size_t D,size_t Rank> T operator*(const Tensor<T,D,Rank>& t1,const Tensor<T,D,Rank>& t2){
	const std::vector<T>& metric(t1.get_metric_extended());
	const std::vector<std::vector<size_t>>& traverse(t1.get_traverse());
	T toret(0);

	for(auto& indices:traverse){
		T metricprods(1);
		for(auto& i:indices)
			metricprods*=metric[i];
		// sum_{mu1, ... , muRank} g^{mu1,mu1} ... g^{muRank,muRank} t1_{mu1...muRank} t2_{mu1...muRank}
		toret+=metricprods*t1.entry(indices)*t2.entry(indices);
	}

	return toret;
}

/**
 * This functions computes h^{\mu}_{\mu} for a given rank 2 tensor
 */

template <class T,size_t D> T trace_rank2(const Tensor<T,D,2>& h){
	const std::vector<T>& metric(h.get_metric_extended());
	T toret(0);
	for(size_t ii=0;ii<D;ii++)
		toret+=h[ii][ii]*metric[ii];
	return toret;
}



// TODO create function that contracts two indices of a tensor

// template <class T,size_t D,size_t Rank> std::enable_if_t<(Rank>=2),Tensor<T,D,Rank-2>> trace_indices(size_t mu,size_t nu,const Tensor<T,D,Rank>& t){
// 	Tensor<T,D,Rank-2> tout(0);
// 	const std::vector<T>& metric(tout.get_metric());
//
// 	for (size_t ii = 0; ii < D; ii++) {
// 		 tout+= t[ii][ii];
// 	}
// 	return tout;
// }

template <class T,size_t D> T IP_rank2(const Tensor<T,D,2>& h1,const Tensor<T,D,2>& h2){
	const std::vector<T>& metric(h1.get_metric_extended());
	T toret(0);
	for(size_t ii=0;ii<D;ii++){
		for (size_t jj = 0; jj < D; jj++) {
		toret+=h1[ii][jj]*h2[ii][jj]*metric[ii]*metric[jj];
		}
	}
	return toret;
}


template <class T,size_t D,size_t Rank> std::enable_if_t<(Rank>=2),Tensor<T,D,Rank-2>> trace_indices(size_t i,size_t j,
		const Tensor<T,D,Rank>& t){
	Tensor<T,D,Rank-2> tout(0);
	// CAREFUL: Tensor::metric might need update when field change!
	const std::vector<T>& metric(tout.get_metric_extended());
	// grab set of indices from the given class
	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());
	// indices to contract (mu<nu)
	size_t mu(i);
	size_t nu(j);
	if(i>j){
		mu=j;
		nu=i;
	}
	else if(i==j){
		std::cout<<"ERROR: can't trace over same index! exit 1"<<std::endl;
		exit(1);
	}

	for(auto& indicesRm2:traverse){
	 	T theentry(0);
	 	std::vector<size_t> indicesR(indicesRm2);
		// insert 0 in mu
		indicesR.insert(indicesR.begin()+mu,0);
		// insert 0 in nu
		indicesR.insert(indicesR.begin()+nu,0);

	 	for(size_t rho=0;rho<D;rho++){
	 		indicesR[mu]=rho;
	 		indicesR[nu]=rho;
	 		theentry+=metric[rho]*t.entry(indicesR);
	 	}
	 	tout.set_entry(theentry,indicesRm2);
	}

	return tout;
}




template <size_t mu,size_t nu,class T,size_t D,size_t Rank> std::vector<std::vector<size_t>> get_indices_for_swap(){
	Tensor<T,D,Rank> hout;
	// grab set of indices from the given class

	const std::vector<std::vector<size_t>>& traverse(hout.get_traverse());
	std::vector<std::vector<size_t>> toret;

	for(size_t ii=0;ii<traverse.size();ii++){
		const std::vector<size_t>& indices(traverse[ii]);
		std::vector<size_t> swap(indices);
		swap[mu]=indices[nu];
		swap[nu]=indices[mu];
		toret.push_back(swap);
	}
	return toret;
}


/**
 * This function swaps two indices of a given tensor
 */
template <size_t mu,size_t nu,class T,size_t D,size_t Rank> std::enable_if_t<(Rank>=2),Tensor<T,D,Rank>> swap_indices(const Tensor<T,D,Rank>& h){
	Tensor<T,D,Rank> hout;
	// grab set of indices from the given class

	const std::vector<std::vector<size_t>>& traverse(h.get_traverse());
	static const std::vector<std::vector<size_t>> swap = get_indices_for_swap<mu,nu,T,D,Rank>();

	for(size_t ii=0;ii<traverse.size();ii++){
		const std::vector<size_t>& indices(traverse[ii]);
		const std::vector<size_t>& lswap(swap[ii]);

		// hout...[mu]...[nu]=hin...[nu]...[mu]
		hout.set_entry(h.entry(indices),lswap);
	}
	return hout;
}

template <size_t i,class T,size_t D,size_t Rank1,size_t Rank2,size_t iStart,size_t iEnd> std::vector<std::vector<std::vector<size_t>>> get_indices_for_contraction(){
	Tensor<T,D,Rank1+Rank2-2> tout;
	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());

	std::vector<std::vector<std::vector<size_t>>> toret;
	for(size_t ii=0;ii<traverse.size();ii++){
		toret.push_back(std::vector<std::vector<size_t>>());
		std::vector<std::vector<size_t>>& collect(toret.back());

		std::vector<size_t> lindices1;
		lindices1.insert(lindices1.begin(),traverse[ii].begin()+iStart,traverse[ii].begin()+iEnd);
		lindices1.insert(lindices1.begin()+i,0);

		for(size_t mu=0;mu<D;mu++){
			lindices1[i]=mu;
			collect.push_back(lindices1);
		}
	}
	return toret;
}

// all complex cases, i>0 then g_{i,i} = -1
template <class T,size_t i> std::enable_if_t<is_complex<T>::value,void> CONTRACTION_HANDLE(T& value,const T& product){	value-=product; }

// non complex cases and floating point, i>0 && i odd, then g_{i,i} = -1
template <class T,size_t i> std::enable_if_t<!is_complex<T>::value && is_floating_point<T>::value && (i%2)==1,void> CONTRACTION_HANDLE(T& value,const T& product){	value-=product; }

// non complex cases and floating point, i>0 && i even, then g_{i,i} = +1
template <class T,size_t i> std::enable_if_t<!is_complex<T>::value && is_floating_point<T>::value && (i%2)==0,void> CONTRACTION_HANDLE(T& value,const T& product){	value+=product; }

// non complex cases and integer, i>0 && i odd && i!= 5, then g_{i,i} = -1
template <class T,size_t i> std::enable_if_t<!is_complex<T>::value && is_exact<T>::value && (i%2)==1 && i!=5,void> CONTRACTION_HANDLE(T& value,const T& product){	value-=product; }

// non complex cases and integer, i==5, then g_{5,5} = -1 * EpsilonBasis<T>::squares[1]
template <class T,size_t i> std::enable_if_t<!is_complex<T>::value && is_exact<T>::value && i==5,void> CONTRACTION_HANDLE(T& value,const T& product){	value-=product*EpsilonBasis<T>::squares[1]; }

// non complex cases and integer, i>0 && i even && i!= 4, then g_{i,i} = +1
template <class T,size_t i> std::enable_if_t<!is_complex<T>::value && is_exact<T>::value && (i%2)==0 && i!=4,void> CONTRACTION_HANDLE(T& value,const T& product){	value+=product; }

// non complex cases and integer, i==4, then g_{4,4} = +1 * EpsilonBasis<T>::squares[0]
template <class T,size_t i> std::enable_if_t<!is_complex<T>::value && is_exact<T>::value && i==4,void> CONTRACTION_HANDLE(T& value,const T& product){	value+=product*EpsilonBasis<T>::squares[0]; }

// Case i==0 always add (irrespective of class T)
template <class T,size_t D,size_t i,size_t Rank1,size_t Rank2> std::enable_if_t<(i==0),void> unroll_contraction_tensors(
	T& value,const Tensor<T,D,Rank1>& t1,const std::vector<std::vector<size_t>>& lindices1,
	         const Tensor<T,D,Rank2>& t2,const std::vector<std::vector<size_t>>& lindices2){
	value+=t1.entry(lindices1[0])*t2.entry(lindices2[0]);
	unroll_contraction_tensors<T,D,1>(value,t1,lindices1,t2,lindices2);
}

// Generic 0<i<D
template <class T,size_t D,size_t i,size_t Rank1,size_t Rank2> std::enable_if_t<(i>0&&i<D),void> unroll_contraction_tensors(
	T& value,const Tensor<T,D,Rank1>& t1,const std::vector<std::vector<size_t>>& lindices1,
	         const Tensor<T,D,Rank2>& t2,const std::vector<std::vector<size_t>>& lindices2){
	// delegate instruction to this function
	CONTRACTION_HANDLE<T,i>(value,t1.entry(lindices1[i])*t2.entry(lindices2[i]));
	unroll_contraction_tensors<T,D,i+1>(value,t1,lindices1,t2,lindices2);
}
// Once we reach i==D we stop (irrespective of class T)
template <class T,size_t D,size_t i,size_t Rank1,size_t Rank2> std::enable_if_t<(i==D),void> unroll_contraction_tensors(
	T& value,const Tensor<T,D,Rank1>& t1,const std::vector<std::vector<size_t>>& lindices1,
	         const Tensor<T,D,Rank2>& t2,const std::vector<std::vector<size_t>>& lindices2){
}

// Case i==0 always add (irrespective of class T)
template <class T,size_t D,size_t i,size_t Rank> std::enable_if_t<(i==0),void> unroll_contraction_mom_tensor(
	T& value,const momD<T,D>& p,const Tensor<T,D,Rank>& t,const std::vector<std::vector<size_t>>& lindices){
	value+=p.pi(0)*t.entry(lindices[0]);
	unroll_contraction_mom_tensor<T,D,1>(value,p,t,lindices);
}

// Generic 0<i<D
template <class T,size_t D,size_t i,size_t Rank> std::enable_if_t<(i>0&&i<D),void> unroll_contraction_mom_tensor(
	T& value,const momD<T,D>& p,const Tensor<T,D,Rank>& t,const std::vector<std::vector<size_t>>& lindices){
	// delegate instruction to this function
	CONTRACTION_HANDLE<T,i>(value,p.pi(i)*t.entry(lindices[i]));
	unroll_contraction_mom_tensor<T,D,i+1>(value,p,t,lindices);
}
// Once we reach i==D we stop (irrespective of class T)
template <class T,size_t D,size_t i,size_t Rank> std::enable_if_t<(i==D),void> unroll_contraction_mom_tensor(
	T& value,const momD<T,D>& p,const Tensor<T,D,Rank>& t,const std::vector<std::vector<size_t>>& lindices){
}

/**
 * Index contraction of two tensors:
 *
 * Tout(mu1,...,mui-1,mui+1,...,muRank1,nu1,...,nuj-1,nuj+1,...,nuRank2)=T1(mu1,...,mui-1,mu,mui+1,...,muRank1)T2(nu1,...,nuj-1,mu,nuj+1,...,nuRank2)
 */
template <size_t i, size_t j,class T,size_t D,size_t Rank1,size_t Rank2> std::enable_if_t<(Rank1+Rank2>2),Tensor<T,D,Rank1+Rank2-2>> index_contraction(
		const Tensor<T,D,Rank1>& t1,const Tensor<T,D,Rank2>& t2){
	Tensor<T,D,Rank1+Rank2-2> tout;
	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());
	static const std::vector<std::vector<std::vector<size_t>>> indices1 = get_indices_for_contraction<i,T,D,Rank1,Rank2,0,Rank1-1>();
	static const std::vector<std::vector<std::vector<size_t>>> indices2 = get_indices_for_contraction<j,T,D,Rank1,Rank2,Rank1-1,Rank1+Rank2-2>();

	for(size_t ii=0;ii<traverse.size();ii++){
		T theentry(0);

		const std::vector<std::vector<size_t>>& lindices1(indices1[ii]);
		const std::vector<std::vector<size_t>>& lindices2(indices2[ii]);

		unroll_contraction_tensors<T,D,0>(theentry,t1,lindices1,t2,lindices2);
		tout.set_entry(theentry,traverse[ii]);
	}

	return tout;
}




/**
 * Overloaded case to handle a momD<T,D> directly
 */
template <size_t i,class T,size_t D,size_t Rank> std::enable_if_t<(Rank>=2),Tensor<T,D,Rank-1>> index_contraction(
		const momD<T,D>& p,const Tensor<T,D,Rank>& t){
	Tensor<T,D,Rank-1> tout;

	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());
	static const std::vector<std::vector<std::vector<size_t>>> indicest = get_indices_for_contraction<i,T,D,Rank,1,0,Rank-1>();

	for(size_t ii=0;ii<traverse.size();ii++){
		T theentry(0);

		const std::vector<std::vector<size_t>>& lindicest(indicest[ii]);
		unroll_contraction_mom_tensor<T,D,0>(theentry,p,t,lindicest);
		tout.set_entry(theentry,traverse[ii]);
	}

	return tout;
}

template <class T,size_t D,size_t Rank1,size_t Rank2,size_t iStart,size_t iEnd> std::vector<std::vector<size_t>> get_indices_for_concatenate(){
	Tensor<T,D,Rank1+Rank2> tout;
	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());

	std::vector<std::vector<size_t>> toret;
	for(size_t ii=0;ii<traverse.size();ii++){
		const std::vector<size_t>& indices(traverse[ii]);

		toret.push_back(std::vector<size_t>());
		std::vector<size_t>& collect(toret.back());

		collect.insert(collect.begin(),indices.begin()+iStart,indices.begin()+iEnd);
	}
	return toret;
}


/**
 * Defines a tensor product of two tensors:
 *
 * Tout(mu1,...,muRank1,nu1,...,nuRank2)=T1(mu1,...,muRank1)T2(nu1,...,nuRank2)
 */
template <class T,size_t D,size_t Rank1,size_t Rank2> Tensor<T,D,Rank1+Rank2> concatenate_tensors(const Tensor<T,D,Rank1>& t1,const Tensor<T,D,Rank2>& t2){
	Tensor<T,D,Rank1+Rank2> tout;
	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());
	static const std::vector<std::vector<size_t>> indices1 = get_indices_for_concatenate<T,D,Rank1,Rank2,0,Rank1>();
	static const std::vector<std::vector<size_t>> indices2 = get_indices_for_concatenate<T,D,Rank1,Rank2,Rank1,Rank1+Rank2>();

	for(size_t ii=0;ii<traverse.size();ii++){
		const std::vector<size_t>& lindices1(indices1[ii]);
		const std::vector<size_t>& lindices2(indices2[ii]);
		tout.set_entry(t1.entry(lindices1)*t2.entry(lindices2),traverse[ii]);
	}

	return tout;
}

/**
 * Overloaded version to allow to pass a momentum
 */
template <class T,size_t D,size_t Rank> Tensor<T,D,Rank+1> concatenate_tensors(const momD<T,D>& p,const Tensor<T,D,Rank>& tin){
	Tensor<T,D,Rank+1> tout;
	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());
	static const std::vector<std::vector<size_t>> indicesin = get_indices_for_concatenate<T,D,1,Rank,1,Rank+1>();

	for(size_t ii=0;ii<traverse.size();ii++){
		const std::vector<size_t>& indices(traverse[ii]);

		const std::vector<size_t>& lindicesin(indicesin[ii]);

		tout.set_entry(p.pi(indices[0])*tin.entry(lindicesin),indices);
	}

	return tout;
}

// TODO: build a better version for generic rank cases with a template parameter pack
// T012 = Ti Tjk (i, j and k assumed always a permutation of 0, 1 and 2)
template <class T,size_t D> Tensor<T,D,3> permute_1_2_to_3(size_t i,size_t j,size_t k,const Tensor<T,D,1>& t1,const Tensor<T,D,2>& t2){
	Tensor<T,D,3> tout;
	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());

	for(auto& indices:traverse){
		std::vector<size_t> indicesin { indices[j], indices[k] };
		tout.set_entry(t1[indices[i]]*t2.entry(indicesin),indices);
	}

	return tout;
}
template <class T,size_t D> Tensor<T,D,3> permute_1_2_to_3(size_t i,size_t j,size_t k,const Tensor<T,D,2>& t1,const Tensor<T,D,1>& t2){
	Tensor<T,D,3> tout;
	const std::vector<std::vector<size_t>>& traverse(tout.get_traverse());

	for(auto& indices:traverse){
		std::vector<size_t> indicesin { indices[i], indices[j] };
		tout.set_entry(t1.entry(indicesin)*t2[indices[k]],indices);
	}

	return tout;
}


}
}
