#include "Core/spinor/clifford_algebra.h"
#include "Core/spinor/dirac_algebra.h"
#include "Core/spinor/DSpinor.h"
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Forest/Builder.h"
#include <map>
#include "Core/settings.h"
#include "Core/Utilities.h"

namespace Caravel{ namespace BG{

template <class T, typename Tcur, size_t Ds, repr RE, int i_f, int j_p, int sign = -1>
std::vector<Tcur> CombineRule_pff_f(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc) {
    const auto& in = currentlist[i_f]->get_current();
    const auto& l = currentlist[j_p]->get_current();

    using namespace Caravel::clifford_algebra;

    auto res = multiply_sparse_smatrix_Weyl<mult_from::R, RE, Ds>(l, in);

    if (sign != 1) {
        for (auto& it : res) { _multeq_by(it, T(sign)); }
    }

    return res;
}

template <class T, typename Tcur, size_t Ds, repr RE, int i_fb, int j_p, int sign = -1>
std::vector<Tcur> CombineRule_pff_fb(const std::vector<momentumD<T, Ds> const*>& momlist,
                                     const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc) {
    const auto& l = currentlist[j_p]->get_current();
    const auto& in = currentlist[i_fb]->get_current();

    using namespace Caravel::clifford_algebra;

    auto res = multiply_sparse_smatrix_Weyl<mult_from::L, RE, Ds>(l, in);

    if (sign != 1) {
        for (auto& it : res) { _multeq_by(it, T(sign)); }
    }

    return res;
}

namespace detail{
template <size_t Ds, repr RE, typename Tcur, typename Cur> inline std::enable_if_t<RE == repr::Lat, std::vector<Tcur>> _sp_contract(const Cur& c1, const Cur& c2) {
    return clifford_algebra::LvAB_Ds<Ds>(c1, c2);
}
template <size_t Ds, repr RE, typename Tcur, typename Cur> inline std::enable_if_t<RE == repr::La, std::vector<Tcur>> _sp_contract(const Cur& c1, const Cur& c2) {
    return clifford_algebra::LvBA_Ds<Ds>(c1, c2);
}
}

template <class T, typename Tcur, size_t Ds, repr RE, int i_fb, int j_f, int sign = -1>
std::vector<Tcur> CombineRule_pff_p(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc) {
    const auto& in1 = currentlist[j_f]->get_current();
    const auto& in2 = currentlist[i_fb]->get_current();

    auto res = detail::_sp_contract<Ds, RE, Tcur>(in1, in2);

    for (auto& it : res) { _multeq_by(it, T(sign * 2)); }

    return res;
}


/*
 * scalar vertices
 */

template <typename T, typename Tcur, size_t Ds, repr RE, int c, size_t qpos> std::vector<Tcur> CombineRule_sqq_q(const std::vector<momentumD<T,Ds> const*>& momlist,const std::vector<CurrentWorker_Template_Base<T,Ds> const*>& currentlist, const BuilderContainer<T>& bc){
    auto ret = currentlist[qpos]->get_current();

    Tcur fac = (currentlist[(qpos+1)%2]->get_current())[0];

    constexpr int sign = c * clifford_algebra::_internal::get_sign_from_repr(RE);
    if(sign!=1){
        _multeq_by(fac,static_cast<T>(sign));
    }

    for(auto& it : ret)
        it = it*fac;

    return ret;
}

template <typename T, typename Tcur, size_t Ds, repr RE, int c, size_t LeftPos = 1>
std::vector<Tcur> CombineRule_sqq_s(const std::vector<momentumD<T, Ds> const*>& momlist,
                                                                       const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc) {
    const auto& in1 = currentlist[LeftPos]->get_current();
    const auto& in2 = currentlist[(LeftPos+1)%2]->get_current();

    using sp1 = typename clifford_algebra::sp_type<RE,Tcur>::type;
    using sp2 = typename clifford_algebra::sp_type<flip(RE),Tcur>::type;

    std::vector<Tcur> res(1,sp1{in1[0],in1[1]}*sp1{in2[0],in2[1]} + sp2{in1[2],in1[3]}*sp2{in2[2],in2[3]});

    constexpr int sign = c * clifford_algebra::_internal::get_sign_from_repr(RE);
    if(sign!=1){
        _multeq_by(res[0],static_cast<T>(sign));
    }

    return res;
}



/*******************/


/**
 * We assume all particles outgoing. For massless fermions this means that 
 * there are only two different degrees of freedom. They can be either identify by
 * quark and antiquark or by helicity. 
 * Quark has negative helicity and antiquark has positive helicity.
 *
 * For Weyl spinors in Ds dims we always use the state with Dtt_index=1. We do not need anything else.
 */

//! Extern outgoing massless quark
template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_fermion_qm(const momD_conf<T, Ds>& momconf, const size_t& lmom, size_t ind = 1) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    std::vector<Tcur> ret(clifford_algebra::two_to_the(Ds / 2 - 1), Tcur(0));
    ret.shrink_to_fit();

    assert(ind <= ret.size() / 2);

    auto l = la<T>(momconf[lmom]);

    ret[2 * ind - 2] = l[0];
    ret[2 * ind - 1] = l[1];

    return ret;
}

//! Extern outgoing massless antiquark
template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_fermion_qbp(const momD_conf<T,Ds>& momconf,const size_t& lmom, size_t ind = 1){
    static_assert(Ds>=4,"Dimensional mismatch");
    std::vector<Tcur> ret(clifford_algebra::two_to_the(Ds/2-1),Tcur(0));
    ret.shrink_to_fit();

    assert(ind <= ret.size()/2);

    auto l = lat<T>(momconf[lmom]);

    ret[2*ind-2] = l[0];
    ret[2*ind-1] = l[1];

    return ret;
}

//! Cut outgoing massless quark with Dtt_index
template <typename T, typename Tcur, size_t Ds, unsigned Dtt_index, repr RE = repr::La> inline std::vector<Tcur> cut_WF_q(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    //_MESSAGE("cut_WF_q is called with Ds=",Ds,", Dtt_index=",Dtt_index);
    // I assume all outgoing momenta, so this minus sign probably because of it
    return Caravel::clifford_algebra::cut_WF_q<RE,Dtt_index,Tcur>(momconf[lmom]);
}
//! Cut outgoing massless quark with Dtt_index
template <typename T, typename Tcur, size_t Ds, unsigned Dtt_index, repr RE = repr::Lat> inline std::vector<Tcur> cut_WF_qb(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    //_MESSAGE("cut_WF_qb is called with Ds=",Ds,", Dtt_index=",Dtt_index);
    // I assume all outgoing momenta, so this minus sign probably because of it
    return Caravel::clifford_algebra::cut_WF_qb<RE,Dtt_index,Tcur>(momconf[lmom]);
}


/**
    * Momentum direction is flowing in the vertex here and we get fermion when the diractoin of the line is the same.
    * Thus we do the map [a|s  -->  <b|
    */
template <typename T, typename Tcur, size_t Ds, repr RE = repr::Lat> std::vector<Tcur> Fermion_Propagator(const std::vector<Tcur>& in,T& prop,const momentumD<T,Ds>& l, const BuilderContainer<T>& bc){
    using namespace Caravel::clifford_algebra;
    // massless propagator
    // NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
    prop=T(1)/(l*l);

    //la<T> out( lat<T>(in[0],in[1]) * smatrix<T>(mom) );

    auto res = multiply_sparse_smatrix_Weyl<mult_from::R,RE,Ds>(to_extended_t<std::array<Tcur,Ds>,Ds>(l.get_components()),in);
    for(auto& it: res){_multeq_by(it,prop);}

    //out *= prop;

    return res;
}

template <typename T, typename Tcur, size_t Ds, repr RE = repr::Lat> std::vector<Tcur> Cut_Fermion_Propagator(const std::vector<Tcur>& in,T& prop,const momentumD<T,Ds>& l, const BuilderContainer<T>& bc){
    using namespace Caravel::clifford_algebra;
    // massless propagator
    // NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
    prop=T(1);

    //la<T> out( lat<T>(in[0],in[1]) * smatrix<T>(mom) );
    auto res = multiply_sparse_smatrix_Weyl<mult_from::R,RE,Ds>(to_extended_t<std::array<Tcur,Ds>,Ds>(l.get_components()),in);

    return res;
}

/**
    * Momentum direction is flowing in the vertex here and we get antifermion when the directoin of the line is the opposite.
    * Thus we do the map s|a> -->  |b]
    */
template <typename T, typename Tcur, size_t Ds, repr RE = repr::La> std::vector<Tcur> AntiFermion_Propagator(const std::vector<Tcur>& in,T& prop,const momentumD<T,Ds>& l, const BuilderContainer<T>& bc){
    using namespace Caravel::clifford_algebra;
    // massless propagator
    // NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
    // additional minus from mismatch between direction of the line and the momentum
    prop=T(1)/(l*l);

    //lat<T> out(  smatrix<T>(mom) * la<T>(in[0],in[1]));
    auto res = multiply_sparse_smatrix_Weyl<mult_from::L,RE,Ds>(to_extended_t<std::array<Tcur,Ds>,Ds>(l.get_components()),in);
    for(auto& it: res){_multeq_by(it,-prop);}

    return res;
}
template <typename T, typename Tcur, size_t Ds, repr RE = repr::La> std::vector<Tcur> Cut_AntiFermion_Propagator(const std::vector<Tcur>& in,T& prop,const momentumD<T,Ds>& l, const BuilderContainer<T>& bc){
    using namespace Caravel::clifford_algebra;
    // massless propagator
    // NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
    // additional minus from mismatch between direction of the line and the momentum
    prop=T(1);

    auto res = multiply_sparse_smatrix_Weyl<mult_from::L,RE,Ds>(to_extended_t<std::array<Tcur,Ds>,Ds>(l.get_components()),in);
    for(auto& it: res){_multeq_by(it,T(-1));}

    return res;
}

/*******************/

// namespace __internal{
//// These are superseded by general Ds-dim spinors. Might be still used later for specialization to Ds=4 (optimisations).

/**
 * Constraction SM|*] --> |*>
 */
// template <class T> vector<Tcur> contract_SM_B(const std::vector<Tcur>& in1,const std::vector<Tcur>& in2, int sign = 1){
// lat<T> l(in2[0],in2[1]);
// l*=T(sign);
// smatrix<T> sl(momentumD<T,4>(in1[0],in1[1],in1[2],in1[3]));

// return (sl*l).get_vector();
//}

/**
 * Constraction <*|SM --> [*|
 */
// template <class T> vector<Tcur> contract_A_SM(const std::vector<Tcur>& in1,const std::vector<Tcur>& in2, int sign = 1){
// la<T> l(in1[0],in1[1]);
// l*=T(sign);
// smatrix<T> sl(momentumD<T,4>(in2[0],in2[1],in2[2],in2[3]));

// return (l*sl).get_vector();
//}

/**
 * Constraction <*|s^\mu|*] --> v^\mu
 */
// template <class T> vector<Tcur> get_vector_AB(const std::vector<Tcur>& in1,const std::vector<Tcur>& in2, int sign = 1){
// la<T> l1(in1[0],in1[1]);
// lat<T> l2(in2[0],in2[1]);

// auto t = LvAB(l1,l2);

// t*=T(sign*=2);

// return t.get_vector();
//}
////! Extern outgoing massless quark
// template <class T,size_t D> std::vector<Tcur> externalWF_fermion_4D_qm(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
// static_assert(D==4,"Dimensional mismatch");
// return (la<T>(momconf[lmom])).get_vector();
//}

////! Extern outgoing massless antiquark
// template <class T,size_t D> std::vector<Tcur> externalWF_fermion_4D_qbp(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
// static_assert(D==4,"Dimensional mismatch");
// return (lat<T>(momconf[lmom])).get_vector();
//}
//}


/**
 * Constraction with [*,*] in the first entry
 * @param in1 final current
 * @param in2 external state
 */
template <unsigned Ds, typename T, typename Tcur,repr RE = repr::Lat> T contract_BB(const std::vector<Tcur>& b1,const std::vector<Tcur>& b2, const BuilderContainer<T>& bc){
    using namespace Caravel::clifford_algebra;
    static constexpr unsigned Dt =two_to_the(Ds/2-1);

    Tcur res(0);

    //return lat<T>(in1[0],in1[1])*lat<T>(in2[0],in2[1]);
    _internal::unroll_sp_prod<RE,Dt>(b1,b2,res,_internal::int_<Dt/2>{});

    return get_e0(res);
}

/**
 * Constraction with <*,*> in the first entry
 * @param in1 final current
 * @param in2 external state
 */
template <unsigned Ds, typename T, typename Tcur, repr RE = repr::La> T contract_AA(const std::vector<Tcur>& b1,const std::vector<Tcur>& b2, const BuilderContainer<T>& bc){
    using namespace Caravel::clifford_algebra;
    static constexpr unsigned Dt =two_to_the(Ds/2-1);

    Tcur res(0);

    //return la<T>(in2[0],in2[1])*la<T>(in1[0],in1[1]);
    _internal::unroll_sp_prod<RE,Dt>(b2,b1,res,_internal::int_<Dt/2>{});

    return get_e0(res);
}

/**
 * A struct, which contains fermion states.
 */
template <typename T, size_t Ds> class fermion_states {
  public:
    typedef typename Current_Type_Selector<T>::Tcur Tcur;

  private:
    static constexpr unsigned Dt = Caravel::clifford_algebra::two_to_the(Ds / 2 - 1);
    static_assert(Ds >= 4 && Ds % 2 == 0, "Dimensional mismatch");

    typedef typename std::map<SingleState, ExternalCurrent<T, Tcur, Ds>> MT;
    MT states;

  public:
    fermion_states() {

        auto construct_weyl_state = [](SingleState s) -> ExternalCurrent<T, Tcur, Ds> {
            // FIXME: this should not be necessary
            // Catch Dtt indices that are outside of the allowed range
            if (static_cast<size_t>(s.get_dtt_index()) > clifford_algebra::two_to_the(Ds / 2 - 2)) {
                return [](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                    std::vector<Tcur> ret(clifford_algebra::two_to_the(Ds / 2 - 1), Tcur(0));
                    ret.shrink_to_fit();
                    return ret;
                };
            }

            // Return the proper function for each polarization
            if (s.get_name() == "qm")
                return [i = s.get_dtt_index()](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                    return BG::externalWF_fermion_qm<T, Tcur, Ds>(mc, l, i);
                };
            else if (s.get_name() == "qbp")
                return [i = s.get_dtt_index()](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                    return BG::externalWF_fermion_qbp<T, Tcur, Ds>(mc, l, i);
                };
            else
                throw std::logic_error("Internal inconsistency in construct_weyl_state");
        };

        states = {{SingleState("qm"), construct_weyl_state(SingleState("qm", 1))}, {SingleState("qbp"), construct_weyl_state(SingleState("qbp", 1))}};

        // FIXME: later should add only states which make sense
        if (Ds > 4) {
            for (int i = 1; i <= 8; ++i) {
                states.emplace(SingleState("qm", i), construct_weyl_state(SingleState("qm", i)));
                states.emplace(SingleState("qbp", i), construct_weyl_state(SingleState("qbp", i)));
            }
        }
        unroll_add_cut_states(_utilities_private::int_<Dt / 2>{});

        DEBUG_MESSAGE("Fermion states for Ds=", Ds, ":");
        DEBUG(for (auto& it : states) { _MESSAGE(std::get<0>(it)); });
    }

    const MT& get() const { return states; }

  private:
    inline void unroll_add_cut_states(_utilities_private::int_<0>) {}
    template <size_t Pos, USE_IF(Pos != 0)> inline void unroll_add_cut_states(_utilities_private::int_<Pos>) {
        using namespace Caravel::clifford_algebra;
        using clifford_algebra::_internal::repr_of_first_spinor;
        states.emplace(q_CUT_STATE_PREFIX + std::to_string(Pos), [](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r,
                                                                    const BuilderContainer<T>& bc) { return BG::cut_WF_q<T, Tcur, Ds, Pos>(mc, l, r); });
        states.emplace(q_CUT_STATE_PREFIX + "b" + std::to_string(Pos), [](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r,
                                                                          const BuilderContainer<T>& bc) { return BG::cut_WF_qb<T, Tcur, Ds, Pos>(mc, l, r); });
        if (Ds > 4) {
            states.emplace(SingleState(q_CUT_STATE_PREFIX + std::to_string(Pos), SingleState::rLa),
                           [](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                               return BG::cut_WF_q<T, Tcur, Ds, Pos, repr::La>(mc, l, r);
                           });
            states.emplace(SingleState(q_CUT_STATE_PREFIX + "b" + std::to_string(Pos), SingleState::rLat),
                           [](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                               return BG::cut_WF_qb<T, Tcur, Ds, Pos, repr::Lat>(mc, l, r);
                           });
            states.emplace(SingleState(q_CUT_STATE_PREFIX + std::to_string(Pos), SingleState::rLat),
                           [](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                               return BG::cut_WF_q<T, Tcur, Ds, Pos, repr::Lat>(mc, l, r);
                           });
            states.emplace(SingleState(q_CUT_STATE_PREFIX + "b" + std::to_string(Pos), SingleState::rLa),
                           [](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                               return BG::cut_WF_qb<T, Tcur, Ds, Pos, repr::La>(mc, l, r);
                           });
        }
        unroll_add_cut_states(_utilities_private::int_<Pos - 1>{});
    }
};
}
} // namespace BG
