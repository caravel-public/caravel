/**
 * @file vectorboson_utilities.hpp
 *
 * @date 26.7.2017
 *
 * @brief Few utilities to handle algebra of CurrentContainer's associated to vector boson currents
 *
*/
#ifndef VECTORBOSON_UTILITIES_HPP_
#define VECTORBOSON_UTILITIES_HPP_

#include <iostream>
#include <numeric>
#include <complex>

#include "Core/Debug.h"
#include "Core/extended_t.h"
#include "Core/type_traits_extra.h"


namespace Caravel {

template <size_t D,typename T> inline T contract_vector_currents_from_0_to(const std::vector<T>& in1,const std::vector<T>& in2){
    T res(in1.front()*in2.front());
    _momD_internal::scalar_product_impl<D,1>(in1.begin(),in2.begin(),res);
    return res;
}

// This function takes a current j and a current jp and performs the operation j+=factor*jp;
template <class T, typename Tcur> inline void add_vector_current_with_prefactor(std::vector<Tcur>& target,const T& factor,const std::vector<Tcur>& toadd){
	std::transform(target.begin(),target.end(),toadd.begin(),target.begin(),[&factor](auto& in,auto& add){ return in+factor*add; });
}


// This function takes a current j and a momentum m and performs the operation j+=factor*m;
template <class T, typename Tcur, typename Tfact, size_t D> inline void add_momentum_with_prefactor(std::vector<Tcur>& target,const Tfact& factor,const momD<T,D>& toadd){
    for (size_t i = 0; i < 4; ++i) target[i] += factor * toadd[i];
    if constexpr (D >= 5) {
        auto mapped_mom = to_extended_t<std::array<Tcur, D>, D>(toadd.get_components());
        target[4] += factor * mapped_mom[4];
        if constexpr (D >= 6) target[5] += factor * mapped_mom[5];
    }
    if constexpr (D >= 7)
        for (size_t i = 6; i < D; ++i) target[i] += factor * toadd[i];
}


template <typename T, typename Tcur, size_t D> std::enable_if_t<is_extended<Tcur>::value,Tcur> contract_vector_current_with_momentum_from_0_to(const std::vector<Tcur>& j,const momD<T,D>& m){
    auto mapped_mom = to_extended_t<std::array<Tcur, D>, D>(m.get_components());

    Tcur res(j[0]*m[0]);

    for (unsigned i = 1; i < 4; i++) res += (T(get_metric_signature<T>(i)) * m[i]) * j[i];
    if (D >= 5) res += j[4] * T(get_metric_signature<T>(4)) * mapped_mom[4];
    if (D >= 6) res += j[5] * T(get_metric_signature<T>(5)) * mapped_mom[5];
    for (unsigned i = 6; i < D; i++) res += (T(get_metric_signature<T>(i)) * m[i]) * j[i];

    return res;
}

template <typename T, typename Tcur, size_t D> std::enable_if_t<!is_extended<Tcur>::value && std::is_same<T,Tcur>::value,Tcur> contract_vector_current_with_momentum_from_0_to(const std::vector<Tcur>& j,const momD<T,D>& m){
    Tcur res(j[0]*m[0]);
    _momD_internal::scalar_product_impl<D,1>(j.cbegin(),m.begin(),res);
    return res;
}


template <class T, typename Tcur> inline void multiply_current_by_factor(std::vector<Tcur>& j,const T& factor){
    for(auto& it: j) it*=factor;
}

}

#endif // VECTORBOSON_UTILITIES_HPP_ 
