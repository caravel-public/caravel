#include "Core/Utilities.h"
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/settings.h"
#include "Core/spinor/DSpinor.h"
#include "Core/spinor/clifford_algebra.h"
#include "Core/spinor/dirac_algebra.h"
#include "Forest/Builder.h"
#include <map>

namespace Caravel {
namespace BG {

/**
 * External: outgoing Dirac fermion Ubar(p,-)
 *
 * @param momconf The momentum configuration
 * @param lmom the index of the massive momentum within the momconf
 * @param ref the index of the massless reference momentum within the momconf
 * @param midx the mass index in the global particle_mass_container
 * @param ind the chosen index in the Dtt space
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_dirac_fermion_Qm(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& ref, const size_t& midx, size_t ind,
                                              const BuilderContainer<T>& bc) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    using namespace clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    std::vector<Tcur> out(Dt, Tcur(0));
    std::vector<T> s(Dt, T(0));

    if (midx == 0) { s = DSpinor_Ubarm<T>(momconf[lmom], ind); }
    else {
        const T& mass = bc.mass[midx];
        momentumD<T, Ds> q;
        switch (ref) {
            case 0: q = get_helper_momD<T, Ds>(1); break;
            default: q = momconf[ref];
        }
        s = DSpinor_Ubarm<T>(momconf[lmom], q, mass, ind);
    }

    // automated casting T -> Tcur if Tcur is extended type
    for (unsigned n = 0; n < Dt; n++) out[n] = s[n];

    return out;
}

/**
 * External: outgoing Dirac fermion Ubar(p,+)
 *
 * @param momconf The momentum configuration
 * @param lmom the index of the massive momentum within the momconf
 * @param ref the index of the massless reference momentum within the momconf
 * @param midx the mass index in the global particle_mass_container
 * @param ind the chosen index in the Dtt space
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_dirac_fermion_Qp(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& ref, const size_t& midx, size_t ind,
                                              const BuilderContainer<T>& bc) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    using namespace clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    std::vector<Tcur> out(Dt, Tcur(0));
    std::vector<T> s(Dt, T(0));

    if (midx == 0) { s = DSpinor_Ubarp<T>(momconf[lmom], ind); }
    else {
        const T& mass = bc.mass[midx];
        momentumD<T, Ds> q;
        switch (ref) {
            case 0: q = get_helper_momD<T, Ds>(1); break;
            default: q = momconf[ref];
        }
        s = DSpinor_Ubarp<T>(momconf[lmom], q, mass, ind);
    }

    // automated casting T -> Tcur if Tcur is extended type
    for (unsigned n = 0; n < Dt; n++) out[n] = s[n];

    return out;
}

/**
 * External: outgoing Dirac anti-fermion V(p,-)
 *
 * @param momconf The momentum configuration
 * @param lmom the index of the massive momentum within the momconf
 * @param ref the index of the massless reference momentum within the momconf
 * @param midx the mass index in the global particle_mass_container
 * @param ind the chosen index in the Dtt space
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_dirac_fermion_Qbm(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& ref, const size_t& midx, size_t ind,
                                               const BuilderContainer<T>& bc) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    using namespace clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    std::vector<Tcur> out(Dt, Tcur(0));
    std::vector<T> s(Dt, T(0));

    if (midx == 0) { s = DSpinor_Vm<T>(momconf[lmom], ind); }
    else {
        const T& mass = bc.mass[midx];
        momentumD<T, Ds> q;
        switch (ref) {
            case 0: q = get_helper_momD<T, Ds>(1); break;
            default: q = momconf[ref];
        }
        s = DSpinor_Vm<T>(momconf[lmom], q, mass, ind);
    }

    // automated casting T -> Tcur if Tcur is extended type
    for (unsigned n = 0; n < Dt; n++) out[n] = s[n];

    return out;
}

/**
 * External: outgoing Dirac anti-fermion V(p,+)
 *
 * @param momconf The momentum configuration
 * @param lmom the index of the massive momentum within the momconf
 * @param ref the index of the massless reference momentum within the momconf
 * @param midx the mass index in the global particle_mass_container
 * @param ind the chosen index in the Dtt space
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> externalWF_dirac_fermion_Qbp(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& ref, const size_t& midx, size_t ind,
                                               const BuilderContainer<T>& bc) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    using namespace clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    std::vector<Tcur> out(Dt, Tcur(0));
    std::vector<T> s(Dt, T(0));

    if (midx == 0) { s = DSpinor_Vp<T>(momconf[lmom], ind); }
    else {
        const T& mass = bc.mass[midx];
        momentumD<T, Ds> q;
        switch (ref) {
            case 0: q = get_helper_momD<T, Ds>(1); break;
            default: q = momconf[ref];
        }
        s = DSpinor_Vp<T>(momconf[lmom], q, mass, ind);
    }

    // automated casting T -> Tcur if Tcur is extended type
    for (unsigned n = 0; n < Dt; n++) out[n] = s[n];

    return out;
}

/**
 * External: outgoing cut DIRAC fermion Ubar(p,+)
 *
 * @param momconf The momentum configuration
 * @param lmom the index of the massive momentum within the momconf
 * @param ref the index of the massless reference momentum within the momconf
 * @param midx the mass index in the global particle_mass_container
 * @param ind the chosen index in the Dtt space
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> cutWF_dirac_fermion_Qp(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& ref, const size_t& midx, size_t ind,
                                         const BuilderContainer<T>& bc) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    using namespace clifford_algebra;
    const T& mass = bc.mass[midx];
    return DSpinor_cut_Qp<Tcur>(momconf[lmom], mass, ind);
}
/**
 * External: outgoing cut DIRAC fermion Ubar(p,-)
 *
 * @param momconf The momentum configuration
 * @param lmom the index of the massive momentum within the momconf
 * @param ref the index of the massless reference momentum within the momconf
 * @param midx the mass index in the global particle_mass_container
 * @param ind the chosen index in the Dtt space
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> cutWF_dirac_fermion_Qm(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& ref, const size_t& midx, size_t ind,
                                         const BuilderContainer<T>& bc) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    const T& mass = bc.mass[midx];
    return clifford_algebra::DSpinor_cut_Qm<Tcur>(momconf[lmom], mass, ind);
}
/**
 * External: outgoing cut DIRAC anti-fermion V(p,+)
 *
 * @param momconf The momentum configuration
 * @param lmom the index of the massive momentum within the momconf
 * @param ref the index of the massless reference momentum within the momconf
 * @param midx the mass index in the global particle_mass_container
 * @param ind the chosen index in the Dtt space
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> cutWF_dirac_fermion_Qbp(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& ref, const size_t& midx, size_t ind,
                                          const BuilderContainer<T>& bc) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    const T& mass = bc.mass[midx];
    return clifford_algebra::DSpinor_cut_Qbp<Tcur>(momconf[lmom], mass, ind);
}
/**
 * External: outgoing cut DIRAC anti-fermion V(p,-)
 *
 * @param momconf The momentum configuration
 * @param lmom the index of the massive momentum within the momconf
 * @param ref the index of the massless reference momentum within the momconf
 * @param midx the mass index in the global particle_mass_container
 * @param ind the chosen index in the Dtt space
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> cutWF_dirac_fermion_Qbm(const momD_conf<T, Ds>& momconf, const size_t& lmom, const size_t& ref, const size_t& midx, size_t ind,
                                          const BuilderContainer<T>& bc) {
    static_assert(Ds >= 4, "Dimensional mismatch");
    const T& mass = bc.mass[midx];
    return clifford_algebra::DSpinor_cut_Qbm<Tcur>(momconf[lmom], mass, ind);
}

/**
 * Dirac Fermion propagator: <Psi|(P_slash + m)|/(p^2-m^2)
 * Note: outgoing fermions are of type <Psi|
 * NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> Dirac_Fermion_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& l, const size_t& midx, const BuilderContainer<T>& bc) {
    using namespace Caravel::clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    const T& mass = bc.mass[midx];
    prop = T(1) / (l * l - mass * mass);

    std::vector<Tcur> out = multiply_sparse_DGamma<mult_from::L, Tcur>(l.get_components(), in);
    for (unsigned i = 0; i < Dt; i++) {
        out[i] += mass * in[i];
        out[i] *= prop;
    }
    return out;
}
/**
 * Dirac Fermion cut-propagator: <Psi|(P_slash + m)
 * Note: outgoing fermions are of type <Psi|
 * NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> Cut_Dirac_Fermion_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& l, const size_t& midx,
                                               const BuilderContainer<T>& bc) {
    using namespace Caravel::clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    const T& mass = bc.mass[midx];
    prop = T(1);

    std::vector<Tcur> out = multiply_sparse_DGamma<mult_from::L, Tcur>(l.get_components(), in);
    for (unsigned i = 0; i < Dt; i++) out[i] += mass * in[i];

    return out;
}
/**
 * Dirac Anti-Fermion propagator: (-P_slash + m)|Psi>/(p^2-m^2)
 * Note: outgoing anti-fermions are of type |Psi>
 * NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> Dirac_AntiFermion_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& l, const size_t& midx,
                                               const BuilderContainer<T>& bc) {
    using namespace Caravel::clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    const T& mass = bc.mass[midx];
    prop = T(1) / (l * l - mass * mass);

    std::vector<Tcur> out = multiply_sparse_DGamma<mult_from::R, Tcur>((-l).get_components(), in);
    for (unsigned i = 0; i < Dt; i++) {
        out[i] += mass * in[i];
        out[i] *= prop;
    }
    return out;
}
/**
 * Dirac Anti-Fermion cut-propagator: (-P_slash + m)|Psi>
 * Note: outgoing anti-fermions are of type |Psi>
 * NOTICE: an 'I' has been factored out and reincluded in with Builder_fixed_Ds::get_overall_normalization_info(.)
 */
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> Cut_Dirac_AntiFermion_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& l, const size_t& midx,
                                                   const BuilderContainer<T>& bc) {
    using namespace Caravel::clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    const T& mass = bc.mass[midx];

    prop = T(1);
    std::vector<Tcur> out = multiply_sparse_DGamma<mult_from::R, Tcur>((-l).get_components(), in);
    for (unsigned i = 0; i < Dt; i++) out[i] += mass * in[i];

    return out;
}
/**
 * Dirac contraction rules
 */
template <unsigned Ds, typename T, typename Tcur> T Dirac_contract(const std::vector<Tcur>& b1, const std::vector<Tcur>& b2, const BuilderContainer<T>& bc) {
    using namespace clifford_algebra;
    constexpr unsigned Dt = two_to_the(Ds / 2);
#if DEBUG_ON
    if (b1.size() != Dt) _WARNING("Incoming fermion current b1 has ", b1.size(), " dimension, should have ", Dt);
    if (b2.size() != Dt) _WARNING("Incoming fermion current b2 has ", b2.size(), " dimension, should have ", Dt);
#endif

    Tcur out = Tcur(0);
    for (unsigned i = 0; i < Dt; i++) out += b1[i] * b2[i];

    return get_e0(out);
}

/**
 * This vertex function combines the gttL and gttR vertices
 * gttL -> sign = 1, gttR -> sign = -1
 * mult_from::R -> |Psi'> = A_slash |Psi>
 * mult_from::L -> <Psi'| = <Psi| A_slash
 * i_f -> index of the fermion current
 * j_g -> index of the gluon current
 **/
template <class T, typename Tcur, size_t Ds, clifford_algebra::mult_from MF, int i_f, int j_g, int sign = -1>
std::vector<Tcur> CombineRule_gtt_t(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc) {

    using namespace Caravel::clifford_algebra;
    const auto& top = currentlist[i_f]->get_current();
    const auto& glu = currentlist[j_g]->get_current();

#if DEBUG_ON
    if (top.size() != two_to_the(Ds / 2)) _WARNING("Incoming (anti)fermion current has ", top.size(), " dimension, should have ", two_to_the(Ds / 2));
    if (glu.size() != Ds) _WARNING("Incoming gluon current has ", glu.size(), " dimension, should have ", Ds);
#endif

    // [gamma^mu]_ij *g_{munu} * glu^{nu} * top_j
    std::vector<Tcur> out = multiply_sparse_DGamma<MF, Tcur, Ds>(glu, top);
    if (sign != 1) {
        for (auto& it : out) { _multeq_by(it, T(sign)); }
    }
    return out;
}
/**
 * This vertex function combines the gttL and gttR vertices <Psi_1|g_mu|Psi_2>
 * gttL -> sign = 1, gttR -> sign = -1
 * i_f -> index of the fermion current
 * j_af -> index of the anti_fermion current
 **/
template <class T, typename Tcur, size_t Ds, int i_f, int j_af, int sign = -1>
std::vector<Tcur> CombineRule_gtt_g(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc) {
    using namespace Caravel::clifford_algebra;
    const auto& top = currentlist[i_f]->get_current();      // <Psi|
    const auto& antitop = currentlist[j_af]->get_current(); // |Psi>

    std::vector<Tcur> out = vector_from_DSpinor<Ds>(antitop, top);
    if (sign != 1) {
        for (auto& it : out) { _multeq_by(it, T(sign)); }
    }

#if DEBUG_ON
    constexpr unsigned Dt = two_to_the(Ds / 2);
    if (top.size() != Dt) _WARNING("Incoming fermion current has ", top.size(), " dimension, should have ", Dt);
    if (antitop.size() != Dt) _WARNING("Incoming antifermion current has ", antitop.size(), " dimension, should have ", Dt);
    if (out.size() != Ds) _WARNING("Outgoing vector current has ", out.size(), " dimension, should have ", Ds);
#endif

    return out;
}
/**
 * This vertex function computes (fermion,scalar) -> fermion and (antifermion,scalar) -> antifermion
 * i_f -> index of the fermion current
 * j_s -> index of the scalar current
 **/
template <class T, typename Tcur, size_t Ds, int i_f, int j_s>
std::vector<Tcur> CombineRule_SFF_F(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                    const size_t midx) {
    // get the currents
    const auto& fermion = currentlist[i_f]->get_current();
    const auto& scalar = currentlist[j_s]->get_current();
    const T& mass = bc.mass[midx];

    std::vector<Tcur> out = fermion;
    for (unsigned n = 0; n < out.size(); n++) out[n] = out[n] * scalar[0] * mass;

    return out;
}

/**
 * Container for Dirac fermion states
 */
template <typename T, size_t Ds> class dirac_fermion_states {
  public:
    typedef typename Current_Type_Selector<T>::Tcur Tcur;

  private:
    static_assert(Ds >= 4 && Ds % 2 == 0, "Dimensional mismatch");

    typedef typename std::map<SingleState, ExternalCurrent<T, Tcur, Ds>> MT;
    MT states;

  public:
    dirac_fermion_states(unsigned midx) {
        auto construct_dirac_state = [midx](SingleState s) -> ExternalCurrent<T, Tcur, Ds> {
            unsigned Dtt = Caravel::clifford_algebra::two_to_the(Ds / 2 - 2);

            // FIXME: this should not be necessary
            // Catch Dtt indices that are outside of the allowed range
            if (static_cast<size_t>(s.get_dtt_index()) > Dtt) {
                return [](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                    std::vector<Tcur> ret(clifford_algebra::two_to_the(Ds / 2), Tcur(0));
                    ret.shrink_to_fit();
                    return ret;
                };
            }

            // Return the proper function for each polarization
            // in tree calculations the Dtt_index is set to zero
            //unsigned Dtt_index = std::max(1, s.get_dtt_index());
            unsigned Dtt_index = s.get_dtt_index();
            if (s.get_name() == "Qm")
                return [midx, Dtt_index](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                    return BG::externalWF_dirac_fermion_Qm<T, Tcur, Ds>(mc, l, r, midx, Dtt_index, bc);
                };
            else if (s.get_name() == "Qp")
                return [midx, Dtt_index](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                    return BG::externalWF_dirac_fermion_Qp<T, Tcur, Ds>(mc, l, r, midx, Dtt_index, bc);
                };
            else if (s.get_name() == "Qbm")
                return [midx, Dtt_index](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                    return BG::externalWF_dirac_fermion_Qbm<T, Tcur, Ds>(mc, l, r, midx, Dtt_index, bc);
                };
            else if (s.get_name() == "Qbp")
                return [midx, Dtt_index](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                    return BG::externalWF_dirac_fermion_Qbp<T, Tcur, Ds>(mc, l, r, midx, Dtt_index, bc);
                };

            // Cut states
            for (unsigned i = 1; i <= Dtt; i++) {
                if (s.get_name() == Q_CUT_STATE_PREFIX + "p" + std::to_string(i))
                    return [midx, i](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                        return BG::cutWF_dirac_fermion_Qp<T, Tcur, Ds>(mc, l, r, midx, i, bc);
                    };
                else if (s.get_name() == Q_CUT_STATE_PREFIX + "bp" + std::to_string(i))
                    return [midx, i](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                        return BG::cutWF_dirac_fermion_Qbp<T, Tcur, Ds>(mc, l, r, midx, i, bc);
                    };
                else if (s.get_name() == Q_CUT_STATE_PREFIX + "m" + std::to_string(i))
                    return [midx, i](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                        return BG::cutWF_dirac_fermion_Qm<T, Tcur, Ds>(mc, l, r, midx, i, bc);
                    };
                else if (s.get_name() == Q_CUT_STATE_PREFIX + "bm" + std::to_string(i))
                    return [midx, i](const momD_conf<T, Ds>& mc, const size_t& l, const size_t& r, const BuilderContainer<T>& bc) {
                        return BG::cutWF_dirac_fermion_Qbm<T, Tcur, Ds>(mc, l, r, midx, i, bc);
                    };
            }

            // You should never reach here
            throw std::logic_error("dirac_fermion_states: Unknown external state!");
        }; // construct_dirac_state

        // 4D states
        states = {{SingleState("Qm"), construct_dirac_state(SingleState("Qm", 1))},
                  {SingleState("Qp"), construct_dirac_state(SingleState("Qp", 1))},
                  {SingleState("Qbm"), construct_dirac_state(SingleState("Qbm", 1))},
                  {SingleState("Qbp"), construct_dirac_state(SingleState("Qbp", 1))}};

        if (Ds > 4) {
            unsigned Dtt = Caravel::clifford_algebra::two_to_the(Ds / 2 - 2);

            // add external states for Ds dimensions
            // FIXME: later should add only states which make sense
            for (unsigned i = 1; i <= 8; i++) {
                states.emplace(SingleState("Qm", i), construct_dirac_state(SingleState("Qm", i)));
                states.emplace(SingleState("Qp", i), construct_dirac_state(SingleState("Qp", i)));
                states.emplace(SingleState("Qbm", i), construct_dirac_state(SingleState("Qbm", i)));
                states.emplace(SingleState("Qbp", i), construct_dirac_state(SingleState("Qbp", i)));
            }

            // add cut states for Ds dimensions
            for (unsigned i = 1; i <= Dtt; i++) {
                std::string cutQm = Q_CUT_STATE_PREFIX + "m" + std::to_string(i);
                std::string cutQp = Q_CUT_STATE_PREFIX + "p" + std::to_string(i);
                std::string cutQbm = Q_CUT_STATE_PREFIX + "bm" + std::to_string(i);
                std::string cutQbp = Q_CUT_STATE_PREFIX + "bp" + std::to_string(i);
                states.emplace(SingleState(cutQm), construct_dirac_state(SingleState(cutQm, i)));
                states.emplace(SingleState(cutQp), construct_dirac_state(SingleState(cutQp, i)));
                states.emplace(SingleState(cutQbm), construct_dirac_state(SingleState(cutQbm, i)));
                states.emplace(SingleState(cutQbp), construct_dirac_state(SingleState(cutQbp, i)));
            }
        }
    }
    const MT& get() const { return states; }
};

} // namespace BG
} // namespace Caravel
