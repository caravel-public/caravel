/**
 * @file States_Model_Projective.hpp
 *
 * @date 1.6.2017
 *
 * @brief Template functions for handling internal vector states with settings::general::cut_states::projective
 *
 * Here we define the internal states used when setting settings::general::cut_states::projective
 *
*/

#include <iostream>

#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/Debug.h"

#include "vectorboson_utilities.hpp"


namespace Caravel {

namespace{

// If finite field employed, normalize by corresponding square
template <size_t i,typename T> inline std::enable_if_t<is_exact<T>::value && (i==4 or i==5)> Epsilon_normalize(std::vector<T>& in){ in[i]/=EpsilonBasis<T>::squares[i-4]; }

// otherwise, do nothing
template <size_t i,typename T> inline std::enable_if_t<is_exact<T>::value && (i<4 or i>5)> Epsilon_normalize(std::vector<T>& in){}
template <size_t i,typename T> inline std::enable_if_t<!is_exact<T>::value> Epsilon_normalize(std::vector<T>& in){}

}

// polarization state for gluons in ModelGluonProjective
template <typename T, typename Tcur, size_t D, size_t i>
inline std::vector<Tcur> externalWF_vectorboson_mui(const momD_conf<T, D>& momconf, const size_t& lmom, const size_t& refmom) {
    std::vector<T> result(D,T(0));
    result[i] = T(1);
    return to_extended_t<std::vector<Tcur>,D>(result);
}

template <typename T, typename Tcur, size_t D,size_t i> inline std::vector<Tcur> externalWF_vectorboson_conti(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(i<D,"Index out of bounds");

    std::vector<T> result(D,T(0));

    result[i] = T(-get_metric_signature<i,T>());
    Epsilon_normalize<i>(result);

    std::vector<T> rmom(D,T(0));
    if(refmom==0){
        ASSIGN_lightcone_ref_1(&rmom[0]);
    }
    else{
        rmom=momconf.p(refmom).get_vector();
    }
    T norm(contract_vector_current_with_momentum_from_0_to(rmom,momconf.p(lmom)));
    // T_is_zero defined in States_Vector_Boson.hpp
    if(T_is_zero(norm)){
        rmom=std::vector<T>(D,T(0));
        ASSIGN_lightcone_ref_2(&rmom[0]);
        norm=contract_vector_current_with_momentum_from_0_to(rmom,momconf.p(lmom));
    }
    T inversenorm(T(1)/norm);

    add_vector_current_with_prefactor(result, momconf.p(lmom).pi(i)*inversenorm, rmom);

    add_momentum_with_prefactor(result,rmom[i]*inversenorm,momconf.p(lmom));

    return to_extended_t<std::vector<Tcur>,D>(result);
}

}
