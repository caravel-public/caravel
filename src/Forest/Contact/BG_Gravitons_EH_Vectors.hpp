#include "Core/extended_t.h"
#include "Tensor.hpp"

namespace Caravel {
namespace BG {

/**
 * Vertex function that combines a vector and a graviton into a vector
 * iv -> index of vector current
 * igr -> index of graviton current
 * Note: Caravel removes a factor of 2*I*kappa from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_VVG_V(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                    const size_t& midx, size_t igr, size_t iv) {

    // incoming Graviton
    Tensor<T, Ds, 2> H = v_extended_t_to_v_t<T, Tcur>(currentlist[igr]->get_current());
    const T trH = trace_rank2(H);

    // Incoming vectors
    const std::vector<T> c1 = v_extended_t_to_v_t<T, Tcur>(currentlist[iv]->get_current());
    const momD<T, Ds> J(c1);

    // momenta
    const auto& p1 = (*(momlist[iv]));
    const auto& p3 = (*(momlist[igr]));
    const momD<T, Ds> p2 = -(p1 + p3);
    const auto& msq = bc.mass[midx] * bc.mass[midx];

    Tensor<T, Ds, 1> Hp1 = index_contraction<1>(p1, H), HJ = index_contraction<1>(J, H);
    const T p2_H_J = Tensor<T, Ds, 1>(p2) * HJ;
    const T p2_H_p1 = Tensor<T, Ds, 1>(p2) * Hp1;
    const T A = T(2) * p2_H_J - trH * (p2 * J);
    const T B = T(2) * (p2 * J);
    const T C = T(-2) * ((p1 * p2) + msq);
    const T D = trH * ((p1 * p2) + msq) - T(2) * p2_H_p1;

    std::vector<Tcur> Jout(Ds, Tcur(0));
    for (size_t mu = 0; mu < Ds; mu++) {
        Jout[mu] = A * p1[mu] + B * Hp1[mu] + C * HJ[mu] + D * J[mu];
        Jout[mu] /= T(2); // kappa
    }

    return Jout;
}

/**
 * Vertex function that combines two vectors into a graviton
 * Note: Caravel removes a factor of 2*I*kappa from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_VVG_G(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                    const size_t& midx) {

    // Incoming vectors
    const std::vector<T> V1 = v_extended_t_to_v_t<T, Tcur>(currentlist[0]->get_current());
    const std::vector<T> V2 = v_extended_t_to_v_t<T, Tcur>(currentlist[1]->get_current());
    const momD<T, Ds> J1(V1), J2(V2);

    // incoming momenta
    const auto& p1 = (*(momlist[0]));
    const auto& p2 = (*(momlist[1]));
    const auto& msq = bc.mass[midx] * bc.mass[midx];

    // metric eta^{munu}
    Tensor<T, Ds, 2> eta(0);
    const std::vector<T> metric(eta.get_metric_extended());
    for (size_t mu = 0; mu < Ds; mu++) eta[mu][mu] = T(1) / metric[mu];

    Tensor<T, Ds, 2> T1(0), T2(0), T3(0), T4(0);
    for (size_t mu = 0; mu < Ds; mu++) {
        for (size_t nu = mu; nu < Ds; nu++) {
            T1[mu][nu] = J1[mu] * J2[nu] + J1[nu] * J2[mu];
            T1[nu][mu] = T1[mu][nu];
            T2[mu][nu] = p1[mu] * p2[nu] + p1[nu] * p2[mu];
            T2[nu][mu] = T2[mu][nu];
            T3[mu][nu] = p1[mu] * J2[nu] + p1[nu] * J2[mu];
            T3[nu][mu] = T3[mu][nu];
            T4[mu][nu] = p2[mu] * J1[nu] + p2[nu] * J1[mu];
            T4[nu][mu] = T4[mu][nu];
        }
    }

    const T c1 = -((p1 * p2) + msq);
    const T c2 = ((p1 * p2) + msq) * (J1 * J2) - (p1 * J2) * (p2 * J1);
    const T c3 = -(J1 * J2);
    const T c4 = (p2 * J1);
    const T c5 = (p1 * J2);

    Tensor<T, Ds, 2> V = (c1 * T1 + c2 * eta + c3 * T2 + c4 * T3 + c5 * T4) / T(2);

    return v_t_to_v_extended_t<T, Tcur>(V.vectorize());
}

/**
 * Vertex function that combines two vectors and a graviton into a graviton
 * iv1 -> index of first vector current
 * iv2 -> index of second vector current
 * igr  -> index of graviton current
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_VVGG_G(const std::vector<momentumD<T, Ds> const*>& momlist,
                                     const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                     const size_t& midx, size_t iv1, size_t iv2, size_t igr) {

    // Incoming vector and gravitons
    std::vector<Tensor<T, Ds, 1>> V;
    V.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[iv1]->get_current()));
    V.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[iv2]->get_current()));
    std::vector<Tensor<T, Ds, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr]->get_current()));
    const size_t p3 = 0;

    // incoming momenta
    std::vector<momD<T, Ds>> k;
    k.emplace_back(-(*(momlist[iv1])));
    k.emplace_back(-(*(momlist[iv2])));
    k.emplace_back(-(*(momlist[igr])));
    const T mSQ = bc.mass[midx] * bc.mass[midx];

    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;

    array1D trH(1, trace_rank2(H[p3]));
    array2D kk(2, array1D(2, T(0)));
    array2D Vk(2, array1D(2, T(0)));
    array2D VV(2, array1D(2, T(0)));
    array3D kHk(2, array2D(1, array1D(2, T(0))));
    array3D VHk(2, array2D(1, array1D(2, T(0))));
    array3D VHV(2, array2D(1, array1D(2, T(0))));

    using T1array1D = std::vector<Tensor<T, Ds, 1>>;
    using T1array2D = std::vector<std::vector<Tensor<T, Ds, 1>>>;
    T1array2D Hk(1, T1array1D(2, Tensor<T, Ds, 1>(0)));
    T1array2D HV(1, T1array1D(2, Tensor<T, Ds, 1>(0)));

    // build Tensors
    for (size_t ii = 0; ii < 2; ii++) {
        Hk[p3][ii] = index_contraction<1>(k[ii], H[p3]);
        HV[p3][ii] = index_contraction<1, 0>(H[p3], V[ii]);
    }

    // build scalars
    for (size_t ii = 0; ii < 2; ii++) {
        for (size_t jj = 0; jj < 2; jj++) {
            kk[ii][jj] = k[ii] * k[jj];
            Vk[ii][jj] = V[ii] * Tensor<T, Ds, 1>(k[jj]);
            VV[ii][jj] = V[ii] * V[jj];
            kHk[ii][p3][jj] = Tensor<T, Ds, 1>(k[ii]) * Hk[p3][jj];
            VHk[ii][p3][jj] = V[ii] * Hk[p3][jj];
            VHV[ii][p3][jj] = V[ii] * HV[p3][jj];
        }
    }

    // Graviton tensor
    Tensor<T, Ds, 2> Hout(0), g(0);
    const std::vector<T>& metric(g.get_metric_extended());
    for (size_t ii = 0; ii < Ds; ii++) g[ii][ii] = T(1) / metric[ii];

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1}, {1, 0}};

    for (size_t a = 0; a < Ds; a++) {
        for (size_t b = a; b < Ds; b++) {
            T theentry = T(0);
            for (size_t ip = 0; ip < 2; ip++) {
                size_t p1 = perm[ip][0], p2 = perm[ip][1];

                theentry += (trH[p3] * (-(kk[p1][p2] * V[p1][b] * V[p2][a]) - mSQ * V[p1][a] * V[p2][b] +
                                        (k[p1][b] * V[p2][a] + k[p1][a] * V[p2][b]) * Vk[p1][p2] - k[p1][a] * k[p2][b] * VV[p1][p2])) /
                                T(4) +
                            g[a][b] * ((trH[p3] * (-(Vk[p1][p2] * Vk[p2][p1]) + (mSQ + kk[p1][p2]) * VV[p1][p2])) / T(8) +
                                       (-(VV[p1][p2] * kHk[p1][p3][p2]) + Vk[p2][p1] * VHk[p1][p3][p2] + Vk[p1][p2] * VHk[p2][p3][p1] -
                                        (mSQ + kk[p1][p2]) * VHV[p1][p3][p2]) /
                                           T(4)) +
                            (-(VV[p1][p2] * ((mSQ + kk[p1][p2]) * H[p3][a][b] - T(2) * (k[p1][b] * Hk[p3][p2][a] + k[p1][a] * Hk[p3][p2][b]))) +
                             Vk[p1][p2] * (Vk[p2][p1] * H[p3][a][b] - T(2) * (k[p1][b] * HV[p3][p2][a] + k[p1][a] * HV[p3][p2][b])) +
                             T(2) * (kk[p1][p2] * V[p2][a] * HV[p3][p1][b] + V[p1][a] * (-(Vk[p2][p1] * Hk[p3][p2][b]) + mSQ * HV[p3][p2][b]) +
                                     V[p1][b] * (-(Vk[p2][p1] * Hk[p3][p2][a]) + (mSQ + kk[p1][p2]) * HV[p3][p2][a] + V[p2][a] * kHk[p1][p3][p2]) -
                                     (k[p1][b] * V[p2][a] + k[p1][a] * V[p2][b]) * VHk[p1][p3][p2] + k[p1][a] * k[p2][b] * VHV[p2][p3][p1])) /
                                T(4);
            }
            Hout[a][b] = theentry * T(-1);
            Hout[b][a] = Hout[a][b];
        }
    }

    return v_t_to_v_extended_t<T, Tcur>(Hout.vectorize());
}

/**
 * Vertex function that combines two gravitons and a vector into a vector
 * iv  -> index of vector current
 * igr1 -> index of first graviton current
 * igr2 -> index of second graviton current
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_VVGG_V(const std::vector<momentumD<T, Ds> const*>& momlist,
                                     const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                     const size_t& midx, size_t iv, size_t igr1, size_t igr2) {

    // Incoming vector and gravitons
    std::vector<Tensor<T, Ds, 1>> V;
    V.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[iv]->get_current()));
    std::vector<Tensor<T, Ds, 2>> H;
    H.emplace_back(std::vector<T>(Ds * Ds, T(0))); // necessary to get indices right
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr2]->get_current()));

    // incoming momenta
    std::vector<momD<T, Ds>> k;
    k.emplace_back(-(*(momlist[iv])));
    k.emplace_back(-(*(momlist[igr1])));
    k.emplace_back(-(*(momlist[igr2])));
    const T mSQ = bc.mass[midx] * bc.mass[midx];

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1, 2}, {0, 2, 1}};

    // build Tensors
    using T1array1D = std::vector<Tensor<T, Ds, 1>>;
    using T1array2D = std::vector<std::vector<Tensor<T, Ds, 1>>>;
    using T1array3D = std::vector<std::vector<std::vector<Tensor<T, Ds, 1>>>>;
    using T2array1D = std::vector<Tensor<T, Ds, 2>>;
    using T2array2D = std::vector<std::vector<Tensor<T, Ds, 2>>>;

    T2array2D HH(3, T2array1D(3, Tensor<T, Ds, 2>(0)));
    T1array2D Hk(3, T1array1D(3, Tensor<T, Ds, 1>(0)));
    T1array2D HV(3, T1array1D(3, Tensor<T, Ds, 1>(0)));
    T1array3D HHk(3, T1array2D(3, T1array1D(3, Tensor<T, Ds, 1>(0))));
    T1array3D HHV(3, T1array2D(3, T1array1D(3, Tensor<T, Ds, 1>(0))));
    for (size_t ip = 0; ip < 2; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2];
        HH[p2][p3] = index_contraction<1, 0>(H[p2], H[p3]);
        HV[p2][p1] = index_contraction<1, 0>(H[p2], V[p1]);
        HHV[p2][p3][p1] = index_contraction<1, 0>(HH[p2][p3], V[p1]);
        for (size_t ii = 0; ii < 3; ii++) {
            Hk[p2][ii] = index_contraction<1>(k[ii], H[p2]);
            HHk[p2][p3][ii] = index_contraction<1>(k[ii], HH[p2][p3]);
        }
    }

    // build Scalars
    using array4D = std::vector<std::vector<std::vector<std::vector<T>>>>;
    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array1D trH(3, T(0));
    array2D trHH(3, array1D(3, T(0)));
    array2D kk(3, array1D(3, T(0)));
    array2D Vk(3, array1D(3, T(0)));
    array3D kHk(3, array2D(3, array1D(3, T(0))));
    array3D VHk(3, array2D(3, array1D(3, T(0))));
    array4D VHHk(3, array3D(3, array2D(3, array1D(3, T(0)))));
    array4D kHHk(3, array3D(3, array2D(3, array1D(3, T(0)))));

    for (size_t ip = 0; ip < 2; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2];
        trH[p2] = trace_rank2(H[p2]);
        trHH[p2][p3] = trace_rank2(HH[p2][p3]);
        for (size_t ii = 0; ii < 3; ii++) {
            VHHk[p1][p2][p3][ii] = V[p1] * HHk[p2][p3][ii];
            VHk[p1][p2][ii] = V[p1] * Hk[p2][ii];
            Vk[p1][ii] = V[p1] * Tensor<T, Ds, 1>(k[ii]);
            for (size_t jj = 0; jj < 3; jj++) {
                kHHk[ii][p2][p3][jj] = Tensor<T, Ds, 1>(k[ii]) * HHk[p2][p3][jj];
                kHk[ii][p2][jj] = Tensor<T, Ds, 1>(k[ii]) * Hk[p2][jj];
                kk[ii][jj] = k[ii] * k[jj];
            }
        }
    }

    std::vector<Tcur> Jout(Ds, T(0));
    for (size_t a = 0; a < Ds; a++) {
        T theentry = T(0);
        for (size_t ip = 0; ip < 2; ip++) {
            size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2];

            theentry += (trHH[p3][p2] * ((-mSQ + kk[p1][p1]) * V[p1][a] - k[p1][a] * Vk[p1][p1])) / T(2) +
                        trHH[p2][p3] * (kk[p1][p3] * V[p1][a] - k[p1][a] * Vk[p1][p3]) - T(2) * HV[p2][p1][a] * (kHk[p1][p3][p1] + kHk[p1][p3][p3]) -
                        HV[p3][p1][a] * (kHk[p1][p2][p3] + kHk[p3][p2][p1]) + T(2) * Hk[p3][p1][a] * VHk[p1][p2][p3] +
                        trH[p3] * (-((Vk[p1][p1] + Vk[p1][p3]) * Hk[p2][p1][a]) + (-mSQ + kk[p1][p1] + kk[p1][p3]) * HV[p2][p1][a] +
                                   V[p1][a] * (kHk[p1][p2][p1] + kHk[p1][p2][p3]) - k[p1][a] * (VHk[p1][p2][p1] + VHk[p1][p2][p3])) +
                        T(2) * Hk[p2][p1][a] * (VHk[p1][p3][p1] + VHk[p1][p3][p3]) +
                        trH[p2] * ((trH[p3] * ((mSQ - kk[p1][p1] - T(2) * kk[p1][p3]) * V[p1][a] + k[p1][a] * (Vk[p1][p1] + T(2) * Vk[p1][p3]))) / T(4) -
                                   Vk[p1][p3] * Hk[p3][p1][a] + kk[p1][p3] * HV[p3][p1][a] + V[p1][a] * kHk[p1][p3][p3] - k[p1][a] * VHk[p1][p3][p3]) +
                        T(2) * (Vk[p1][p1] + Vk[p1][p3]) * HHk[p2][p3][p1][a] + T(2) * Vk[p1][p3] * HHk[p3][p2][p1][a] +
                        T(2) * (mSQ - kk[p1][p1] - kk[p1][p3]) * HHV[p2][p3][p1][a] - T(2) * kk[p1][p3] * HHV[p3][p2][p1][a] -
                        V[p1][a] * (T(2) * kHHk[p1][p2][p3][p1] + T(2) * kHHk[p1][p2][p3][p3] + kHHk[p1][p3][p2][p3] + kHHk[p3][p2][p3][p1]) +
                        T(2) * k[p1][a] * (VHHk[p1][p2][p3][p3] + VHHk[p1][p3][p2][p1] + VHHk[p1][p3][p2][p3]);
        }
        Jout[a] = theentry / T(-2);
    }

    return Jout;
}

/**
 * Vertex function that combines three gravitons and a vector into a vector
 * iv  -> index of vector current
 * igr1 -> index of first graviton current
 * igr2 -> index of second graviton current
 * igr3 -> index of third graviton current
 * Note: Caravel removes a factor of (2*I*kappa)^3 from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_VVGGG_V(const std::vector<momentumD<T, Ds> const*>& momlist,
                                      const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                      const size_t& midx, size_t iv, size_t igr1, size_t igr2, size_t igr3) {

    // Incoming vector and gravitons
    std::vector<Tensor<T, Ds, 1>> V;
    V.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[iv]->get_current()));
    std::vector<Tensor<T, Ds, 2>> H;
    H.emplace_back(std::vector<T>(Ds * Ds, T(0))); // necessary to get indices right
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr2]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr3]->get_current()));

    // incoming momenta
    std::vector<momD<T, Ds>> k;
    k.emplace_back(-(*(momlist[iv])));
    k.emplace_back(-(*(momlist[igr1])));
    k.emplace_back(-(*(momlist[igr2])));
    k.emplace_back(-(*(momlist[igr3])));
    const T mSQ = bc.mass[midx] * bc.mass[midx];

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1}};

    // build Tensors
    using T1array1D = std::vector<Tensor<T, Ds, 1>>;
    using T1array2D = std::vector<std::vector<Tensor<T, Ds, 1>>>;
    using T1array3D = std::vector<std::vector<std::vector<Tensor<T, Ds, 1>>>>;
    using T1array4D = std::vector<std::vector<std::vector<std::vector<Tensor<T, Ds, 1>>>>>;
    using T2array1D = std::vector<Tensor<T, Ds, 2>>;
    using T2array2D = std::vector<std::vector<Tensor<T, Ds, 2>>>;
    using T2array3D = std::vector<std::vector<std::vector<Tensor<T, Ds, 2>>>>;
    T2array2D HH(4, T2array1D(4, Tensor<T, Ds, 2>(0)));
    T2array3D HHH(4, T2array2D(4, T2array1D(4, Tensor<T, Ds, 2>(0))));
    T1array2D Hk(4, T1array1D(4, Tensor<T, Ds, 1>(0)));
    T1array2D HV(4, T1array1D(4, Tensor<T, Ds, 1>(0)));
    T1array3D HHV(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0))));
    T1array3D HHk(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0))));
    T1array4D HHHk(4, T1array3D(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0)))));
    T1array4D HHHV(4, T1array3D(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0)))));

    for (size_t ip = 0; ip < 6; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];
        HH[p2][p3] = index_contraction<1, 0>(H[p2], H[p3]);
        HHV[p2][p3][p1] = index_contraction<1, 0>(HH[p2][p3], V[p1]);
        HHH[p2][p3][p4] = index_contraction<1, 0>(HH[p2][p3], H[p4]);
        HV[p2][p1] = index_contraction<1, 0>(H[p2], V[p1]);
        HHHV[p2][p3][p4][p1] = index_contraction<1, 0>(HHH[p2][p3][p4], V[p1]);
        for (size_t ii = 0; ii < 4; ii++) {
            Hk[p2][ii] = index_contraction<1>(k[ii], H[p2]);
            HHk[p2][p3][ii] = index_contraction<1>(k[ii], HH[p2][p3]);
            HHHk[p2][p3][p4][ii] = index_contraction<1>(k[ii], HHH[p2][p3][p4]);
        }
    }

    // build Scalars
    using array5D = std::vector<std::vector<std::vector<std::vector<std::vector<T>>>>>;
    using array4D = std::vector<std::vector<std::vector<std::vector<T>>>>;
    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array1D trH(4, T(0));
    array2D trHH(4, array1D(4, T(0)));
    array3D trHHH(4, array2D(4, array1D(4, T(0))));
    array2D Vk(4, array1D(4, T(0)));
    array2D kk(4, array1D(4, T(0)));
    array3D kHk(4, array2D(4, array1D(4, T(0))));
    array3D VHk(4, array2D(4, array1D(4, T(0))));
    array4D kHHk(4, array3D(4, array2D(4, array1D(4, T(0)))));
    array4D VHHk(4, array3D(4, array2D(4, array1D(4, T(0)))));
    array5D kHHHk(4, array4D(4, array3D(4, array2D(4, array1D(4, T(0))))));
    array5D VHHHk(4, array4D(4, array3D(4, array2D(4, array1D(4, T(0))))));

    for (size_t ip = 0; ip < 6; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];
        trH[p2] = trace_rank2(H[p2]);
        trHH[p2][p3] = trace_rank2(HH[p2][p3]);
        trHHH[p2][p3][p4] = trace_rank2(HHH[p2][p3][p4]);
        for (size_t ii = 0; ii < 4; ii++) {
            Tensor<T, Ds, 1> ki = Tensor<T, Ds, 1>(k[ii]);
            VHk[p1][p2][ii] = V[p1] * Hk[p2][ii];
            Vk[p1][ii] = ki * V[p1];
            VHHk[p1][p2][p3][ii] = V[p1] * HHk[p2][p3][ii];
            VHHHk[p1][p2][p3][p4][ii] = V[p1] * HHHk[p2][p3][p4][ii];
            for (size_t jj = 0; jj < 4; jj++) {
                kHk[ii][p2][jj] = ki * Hk[p2][jj];
                kHHk[ii][p2][p3][jj] = ki * HHk[p2][p3][jj];
                kHHHk[ii][p2][p3][p4][jj] = ki * HHHk[p2][p3][p4][jj];
            }
        }
    }

    for (size_t ii = 0; ii < 4; ii++) {
        for (size_t jj = 0; jj < 4; jj++) { kk[ii][jj] = k[ii] * k[jj]; }
    }

    std::vector<T> Jout(Ds, T(0));
    for (size_t a = 0; a < Ds; a++) {
        T theentry = T(0);
        for (size_t ip = 0; ip < 6; ip++) {
            size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];

            theentry +=
                trHH[p3][p4] * (T(3) * Vk[p1][p4] * Hk[p2][p1][a] - T(3) * kk[p1][p4] * HV[p2][p1][a]) +
                (T(3) * trHH[p4][p3] *
                 (Vk[p1][p1] * Hk[p2][p1][a] + (mSQ - kk[p1][p1]) * HV[p2][p1][a] - V[p1][a] * kHk[p1][p2][p1] + k[p1][a] * VHk[p1][p2][p1])) /
                    T(2) +
                trHH[p2][p4] * (-(T(3) * V[p1][a] * kHk[p1][p3][p4]) + T(3) * k[p1][a] * VHk[p1][p3][p4]) +
                (T(3) * trHH[p3][p2] * (Vk[p1][p4] * Hk[p4][p1][a] - kk[p1][p4] * HV[p4][p1][a] - V[p1][a] * kHk[p1][p4][p4] + k[p1][a] * VHk[p1][p4][p4])) /
                    T(2) +
                trH[p4] * (-((T(3) * trHH[p3][p2] * ((mSQ - kk[p1][p1] - kk[p1][p4]) * V[p1][a] + k[p1][a] * (Vk[p1][p1] + Vk[p1][p4]))) / T(4)) -
                           T(3) * (HV[p2][p1][a] * (kHk[p1][p3][p1] + kHk[p1][p3][p4]) - Hk[p2][p1][a] * (VHk[p1][p3][p1] + VHk[p1][p3][p4]) -
                                   Vk[p1][p4] * HHk[p2][p3][p1][a] + kk[p1][p4] * HHV[p2][p3][p1][a] + V[p1][a] * kHHk[p1][p2][p3][p4] -
                                   k[p1][a] * VHHk[p1][p2][p3][p4])) +
                trH[p3] *
                    ((T(3) * trHH[p2][p4] * (kk[p1][p4] * V[p1][a] - k[p1][a] * Vk[p1][p4])) / T(2) -
                     (T(3) * trH[p4] *
                      ((Vk[p1][p1] + T(2) * Vk[p1][p4]) * Hk[p2][p1][a] + (mSQ - kk[p1][p1] - T(2) * kk[p1][p4]) * HV[p2][p1][a] - V[p1][a] * kHk[p1][p2][p1] +
                       k[p1][a] * VHk[p1][p2][p1])) /
                         T(4) -
                     (T(3) * (T(2) * HV[p2][p1][a] * kHk[p1][p4][p4] + HV[p4][p1][a] * kHk[p4][p2][p1] - Hk[p4][p1][a] * VHk[p1][p2][p4] -
                              T(2) * Hk[p2][p1][a] * VHk[p1][p4][p4] - T(2) * (Vk[p1][p1] + Vk[p1][p4]) * HHk[p2][p4][p1][a] - Vk[p1][p4] * HHk[p4][p2][p1][a] +
                              T(2) * (-mSQ + kk[p1][p1] + kk[p1][p4]) * HHV[p2][p4][p1][a] + kk[p1][p4] * HHV[p4][p2][p1][a] +
                              V[p1][a] * (T(2) * kHHk[p1][p2][p4][p1] + kHHk[p1][p4][p2][p4] + kHHk[p4][p2][p4][p1]) +
                              k[p4][a] * (-VHHk[p1][p2][p4][p1] + VHHk[p1][p4][p2][p1]) - T(2) * k[p1][a] * (VHHk[p1][p4][p2][p1] + VHHk[p1][p4][p2][p4]))) /
                         T(2)) +
                trH[p2] *
                    ((T(3) * trH[p4] * (V[p1][a] * kHk[p1][p3][p4] - k[p1][a] * VHk[p1][p3][p4])) / T(2) +
                     trH[p3] *
                         ((trH[p4] * ((mSQ - kk[p1][p1] - T(3) * kk[p1][p4]) * V[p1][a] + k[p1][a] * (Vk[p1][p1] + T(3) * Vk[p1][p4]))) / T(8) -
                          (T(3) * (Vk[p1][p4] * Hk[p4][p1][a] - kk[p1][p4] * HV[p4][p1][a] - V[p1][a] * kHk[p1][p4][p4] + k[p1][a] * VHk[p1][p4][p4])) / T(4)) +
                     (T(3) * (-(HV[p4][p1][a] * kHk[p1][p3][p4]) + Hk[p4][p1][a] * VHk[p1][p3][p4] + Vk[p1][p4] * HHk[p4][p3][p1][a] -
                              kk[p1][p4] * HHV[p4][p3][p1][a] - T(2) * V[p1][a] * kHHk[p1][p3][p4][p4] + T(2) * k[p1][a] * VHHk[p1][p3][p4][p4] +
                              k[p4][a] * (-VHHk[p1][p3][p4][p1] + VHHk[p1][p4][p3][p1]))) /
                         T(2)) +
                T(6) * (-mSQ + kk[p1][p1] + kk[p1][p4]) * HHHV[p2][p3][p4][p1][a] +
                T(3) * kk[p1][p4] * (T(2) * HHHV[p2][p4][p3][p1][a] + HHHV[p4][p2][p3][p1][a] + HHHV[p4][p3][p2][p1][a]) +
                V[p1][a] * ((mSQ - kk[p1][p1] - T(3) * kk[p1][p4]) * trHHH[p2][p3][p4] +
                            T(3) * (T(2) * kHHHk[p1][p2][p3][p4][p1] + T(2) * kHHHk[p1][p2][p3][p4][p4] + kHHHk[p1][p3][p4][p2][p4] +
                                    kHHHk[p4][p2][p3][p4][p1] + kHHHk[p4][p2][p4][p3][p1] + kHHHk[p4][p3][p2][p4][p1])) +
                (T(3) *
                 (-(T(4) * VHk[p1][p4][p4] * HHk[p2][p3][p1][a]) - T(4) * (VHk[p1][p3][p1] + VHk[p1][p3][p4]) * HHk[p2][p4][p1][a] -
                  T(2) * VHk[p1][p3][p4] * HHk[p4][p2][p1][a] - T(2) * VHk[p1][p2][p4] * HHk[p4][p3][p1][a] + T(4) * kHk[p1][p4][p4] * HHV[p2][p3][p1][a] +
                  T(2) * (T(2) * kHk[p1][p3][p1] + kHk[p1][p3][p4] + kHk[p4][p3][p1]) * HHV[p2][p4][p1][a] + T(2) * kHk[p1][p3][p4] * HHV[p4][p2][p1][a] +
                  T(2) * kHk[p4][p2][p1] * HHV[p4][p3][p1][a] + T(2) * HV[p4][p1][a] * (kHHk[p1][p2][p3][p4] + kHHk[p4][p2][p3][p1]) +
                  T(2) * HV[p2][p1][a] * (T(2) * kHHk[p1][p3][p4][p1] + T(2) * kHHk[p1][p3][p4][p4] + kHHk[p1][p4][p3][p4] + kHHk[p4][p3][p4][p1]) -
                  T(2) * Hk[p4][p1][a] * (VHHk[p1][p2][p3][p4] + VHHk[p1][p3][p2][p4]) - Hk[p2][p4][a] * (VHHk[p1][p3][p4][p3] + VHHk[p1][p4][p3][p3]) +
                  Hk[p2][p3][a] * (VHHk[p1][p3][p4][p4] + VHHk[p1][p4][p3][p4]) -
                  T(4) * Hk[p2][p1][a] * (VHHk[p1][p3][p4][p4] + VHHk[p1][p4][p3][p1] + VHHk[p1][p4][p3][p4]) +
                  T(2) * k[p4][a] *
                      (VHHHk[p1][p2][p3][p4][p1] - VHHHk[p1][p2][p4][p3][p1] - VHHHk[p1][p3][p2][p4][p1] + VHHHk[p1][p3][p4][p2][p1] +
                       VHHHk[p1][p4][p2][p3][p1]))) /
                    T(2) -
                T(3) * (T(2) * Vk[p1][p1] * HHHk[p2][p3][p4][p1][a] +
                        Vk[p1][p4] * (T(2) * HHHk[p2][p3][p4][p1][a] + T(2) * HHHk[p2][p4][p3][p1][a] + HHHk[p4][p2][p3][p1][a] + HHHk[p4][p3][p2][p1][a]) +
                        k[p4][a] * VHHHk[p1][p4][p3][p2][p1]) +
                k[p1][a] * ((Vk[p1][p1] + T(3) * Vk[p1][p4]) * trHHH[p2][p3][p4] -
                            T(3) * (T(2) * VHHHk[p1][p2][p3][p4][p4] + T(2) * VHHHk[p1][p3][p4][p2][p4] + VHHHk[p1][p4][p2][p3][p4] +
                                    T(2) * VHHHk[p1][p4][p3][p2][p1] + VHHHk[p1][p4][p3][p2][p4]));
        }
        Jout[a] = theentry / T(6);
    }

    return v_t_to_v_extended_t<T,Tcur>(Jout);
}

/**
 * Vertex function that combines two gravitons and two vector into a graviton
 * iv1  -> index of first vector current
 * iv2  -> index of second vector current
 * igr1 -> index of first graviton current
 * igr2 -> index of second graviton current
 * Note: Caravel removes a factor of (2*I*kappa)^3 from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_VVGGG_G(const std::vector<momentumD<T, Ds> const*>& momlist,
                                      const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                      const size_t& midx, size_t iv1, size_t iv2, size_t igr1, size_t igr2) {

    // Incoming vector and gravitons
    std::vector<Tensor<T, Ds, 1>> V;
    V.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[iv1]->get_current()));
    V.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[iv2]->get_current()));
    std::vector<Tensor<T, Ds, 2>> H;
    H.emplace_back(std::vector<T>(Ds * Ds, T(0))); // necessary to get indices right
    H.emplace_back(std::vector<T>(Ds * Ds, T(0))); // necessary to get indices right
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr2]->get_current()));

    // incoming momenta
    std::vector<momD<T, Ds>> k;
    k.emplace_back(-(*(momlist[iv1])));
    k.emplace_back(-(*(momlist[iv2])));
    k.emplace_back(-(*(momlist[igr1])));
    k.emplace_back(-(*(momlist[igr2])));
    const T mSQ = bc.mass[midx] * bc.mass[midx];

    // permutations
    static const std::vector<std::vector<size_t>> permV = {{0, 1}, {1, 0}};
    static const std::vector<std::vector<size_t>> permH = {{2, 3}, {3, 2}};

    // build Tensors
    using T1array1D = std::vector<Tensor<T, Ds, 1>>;
    using T1array2D = std::vector<std::vector<Tensor<T, Ds, 1>>>;
    using T1array3D = std::vector<std::vector<std::vector<Tensor<T, Ds, 1>>>>;
    using T2array1D = std::vector<Tensor<T, Ds, 2>>;
    using T2array2D = std::vector<std::vector<Tensor<T, Ds, 2>>>;
    T2array2D HH(4, T2array1D(4, Tensor<T, Ds, 2>(0)));
    T1array2D Hk(4, T1array1D(4, Tensor<T, Ds, 1>(0)));
    T1array2D HV(4, T1array1D(4, Tensor<T, Ds, 1>(0)));
    T1array3D HHk(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0))));
    T1array3D HHV(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0))));

    for (size_t ipH = 0; ipH < 2; ipH++) {
        size_t p3 = permH[ipH][0], p4 = permH[ipH][1];
        HH[p3][p4] = index_contraction<1, 0>(H[p3], H[p4]);
        for (size_t ipV = 0; ipV < 2; ipV++) {
            size_t p1 = permV[ipV][0];
            HV[p3][p1] = index_contraction<1, 0>(H[p3], V[p1]);
            HHV[p3][p4][p1] = index_contraction<1, 0>(HH[p3][p4], V[p1]);
        }
        for (size_t ii = 0; ii < 4; ii++) {
            Hk[p3][ii] = index_contraction<1>(k[ii], H[p3]);
            HHk[p3][p4][ii] = index_contraction<1>(k[ii], HH[p3][p4]);
        }
    }

    // build Scalars
    using array4D = std::vector<std::vector<std::vector<std::vector<T>>>>;
    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array1D trH(4, T(0));
    array2D trHH(4, array1D(4, T(0)));
    array2D kk(4, array1D(4, T(0)));
    array2D Vk(4, array1D(4, T(0)));
    array2D VV(4, array1D(4, T(0)));
    array3D kHk(4, array2D(4, array1D(4, T(0))));
    array3D VHk(4, array2D(4, array1D(4, T(0))));
    array3D VHV(4, array2D(4, array1D(4, T(0))));
    array4D kHHk(4, array3D(4, array2D(4, array1D(4, T(0)))));
    array4D VHHk(4, array3D(4, array2D(4, array1D(4, T(0)))));
    array4D VHHV(4, array3D(4, array2D(4, array1D(4, T(0)))));

    for (size_t ipH = 0; ipH < 2; ipH++) {
        size_t p3 = permH[ipH][0], p4 = permH[ipH][1];
        trH[p3] = trace_rank2(H[p3]);
        trHH[p3][p4] = trace_rank2(HH[p3][p4]);
        for (size_t ii = 0; ii < 4; ii++) {
            Tensor<T, Ds, 1> ki = Tensor<T, Ds, 1>(k[ii]);
            for (size_t jj = 0; jj < 4; jj++) {
                kk[ii][jj] = k[ii] * k[jj];
                kHk[ii][p3][jj] = ki * Hk[p3][jj];
                kHHk[ii][p3][p4][jj] = ki * HHk[p3][p4][jj];
            }
        }
        for (size_t ipV = 0; ipV < 2; ipV++) {
            size_t p1 = permV[ipV][0], p2 = permV[ipV][1];
            VV[p1][p2] = V[p1] * V[p2];
            VHV[p1][p3][p2] = V[p1] * HV[p3][p2];
            for (size_t ii = 0; ii < 4; ii++) {
                Vk[p1][ii] = Tensor<T, Ds, 1>(k[ii]) * V[p1];
                VHk[p1][p3][ii] = V[p1] * Hk[p3][ii];
                VHHk[p1][p3][p4][ii] = V[p1] * HHk[p3][p4][ii];
                VHHV[p1][p3][p4][p2] = V[p1] * HHV[p3][p4][p2];
            }
        }
    }

    // Graviton tensor
    Tensor<T, Ds, 2> Hout(0), g(0);
    const std::vector<T>& metric(g.get_metric_extended());
    for (size_t ii = 0; ii < Ds; ii++) g[ii][ii] = T(1) / metric[ii];

    for (size_t a = 0; a < Ds; a++) {
        for (size_t b = a; b < Ds; b++) {
            T theentry = T(0);
            for (size_t ipV = 0; ipV < 2; ipV++) {
                size_t p1 = permV[ipV][0], p2 = permV[ipV][1];
                for (size_t ipH = 0; ipH < 2; ipH++) {
                    size_t p3 = permH[ipH][0], p4 = permH[ipH][1];

                    theentry +=
                        (trH[p3] * trH[p4] *
                         (-(kk[p1][p2] * V[p1][b] * V[p2][a]) - mSQ * V[p1][a] * V[p2][b] + (k[p1][b] * V[p2][a] + k[p1][a] * V[p2][b]) * Vk[p1][p2] -
                          k[p1][a] * k[p2][b] * VV[p1][p2])) /
                            T(8) +
                        (trHH[p4][p3] * (kk[p1][p2] * V[p1][b] * V[p2][a] + mSQ * V[p1][a] * V[p2][b] -
                                         (k[p1][b] * V[p2][a] + k[p1][a] * V[p2][b]) * Vk[p1][p2] + k[p1][a] * k[p2][b] * VV[p1][p2])) /
                            T(4) +
                        (trH[p4] * (-(VV[p1][p2] * ((mSQ + kk[p1][p2]) * H[p3][a][b] - T(2) * (k[p1][b] * Hk[p3][p2][a] + k[p1][a] * Hk[p3][p2][b]))) +
                                    Vk[p1][p2] * (Vk[p2][p1] * H[p3][a][b] - T(2) * (k[p1][b] * HV[p3][p2][a] + k[p1][a] * HV[p3][p2][b])) +
                                    T(2) * (kk[p1][p2] * V[p2][a] * HV[p3][p1][b] + V[p1][a] * (-(Vk[p2][p1] * Hk[p3][p2][b]) + mSQ * HV[p3][p2][b]) +
                                            V[p1][b] * (-(Vk[p2][p1] * Hk[p3][p2][a]) + (mSQ + kk[p1][p2]) * HV[p3][p2][a] + V[p2][a] * kHk[p1][p3][p2]) -
                                            (k[p1][b] * V[p2][a] + k[p1][a] * V[p2][b]) * VHk[p1][p3][p2] + k[p1][a] * k[p2][b] * VHV[p2][p3][p1]))) /
                            T(4) +
                        g[a][b] * ((trHH[p3][p4] * Vk[p1][p3] * Vk[p2][p4]) / T(8) +
                                   (trHH[p4][p3] * (Vk[p1][p2] * Vk[p2][p1] - Vk[p1][p3] * Vk[p2][p4] - (mSQ + kk[p1][p2]) * VV[p1][p2])) / T(8) +
                                   trH[p3] * ((trH[p4] * (-(Vk[p1][p2] * Vk[p2][p1]) + (mSQ + kk[p1][p2]) * VV[p1][p2])) / T(16) -
                                              (VV[p1][p2] * kHk[p1][p4][p2]) / T(4)) +
                                   (trH[p4] * (Vk[p2][p1] * VHk[p1][p3][p2] + Vk[p1][p2] * VHk[p2][p3][p1] - (mSQ + kk[p1][p2]) * VHV[p1][p3][p2])) / T(4) +
                                   (-(VHk[p1][p4][p2] * VHk[p2][p3][p1]) + kHk[p1][p4][p2] * VHV[p1][p3][p2] + VV[p1][p2] * kHHk[p1][p3][p4][p2] -
                                    Vk[p2][p1] * VHHk[p1][p3][p4][p2] - Vk[p1][p2] * VHHk[p2][p4][p3][p1] + (mSQ + kk[p1][p2]) * VHHV[p1][p3][p4][p2]) /
                                       T(2)) +
                        (Vk[p1][p4] * Hk[p4][p3][a] * HV[p3][p2][b] + T(4) * HV[p3][p2][a] * (Vk[p1][p2] * Hk[p4][p1][b] - kk[p1][p2] * HV[p4][p1][b]) -
                         Vk[p1][p3] * Hk[p3][p4][a] * HV[p4][p2][b] - mSQ * T(4) * HV[p3][p1][a] * HV[p4][p2][b] + kk[p3][p4] * HV[p3][p1][a] * HV[p4][p2][b] -
                         T(4) * (V[p2][a] * HV[p3][p1][b] + V[p1][b] * HV[p3][p2][a]) * kHk[p1][p4][p2] -
                         HV[p4][p1][a] * (kk[p3][p4] * HV[p3][p2][b] + T(2) * V[p2][b] * (kHk[p2][p3][p4] - kHk[p4][p3][p2])) -
                         T(2) * Vk[p2][p1] * H[p3][a][b] * VHk[p1][p4][p2] +
                         T(4) * (V[p2][b] * Hk[p3][p1][a] + V[p2][a] * Hk[p3][p1][b] + k[p1][b] * HV[p3][p2][a] + k[p1][a] * HV[p3][p2][b]) * VHk[p1][p4][p2] +
                         mSQ * T(2) * H[p3][a][b] * VHV[p1][p4][p2] + T(2) * kk[p1][p2] * H[p3][a][b] * VHV[p1][p4][p2] -
                         T(4) * k[p1][b] * Hk[p3][p2][a] * VHV[p1][p4][p2] - T(4) * k[p1][a] * Hk[p3][p2][b] * VHV[p2][p4][p1] +
                         T(4) * Vk[p2][p1] * (V[p1][b] * HHk[p3][p4][p2][a] + V[p1][a] * HHk[p3][p4][p2][b]) +
                         T(2) * VV[p1][p2] *
                             (-(T(2) * Hk[p3][p1][a] * Hk[p4][p2][b]) + H[p3][a][b] * kHk[p1][p4][p2] + (mSQ + kk[p1][p2]) * HH[p3][p4][a][b] -
                              T(2) * (k[p1][b] * HHk[p3][p4][p2][a] + k[p1][a] * HHk[p3][p4][p2][b])) -
                         T(4) * kk[p1][p2] * V[p2][a] * HHV[p3][p4][p1][b] - mSQ * T(4) * V[p1][b] * HHV[p3][p4][p2][a] -
                         T(4) * kk[p1][p2] * V[p1][b] * HHV[p3][p4][p2][a] - mSQ * T(4) * V[p1][a] * HHV[p3][p4][p2][b] +
                         Vk[p1][p2] * (T(4) * Hk[p3][p1][a] * HV[p4][p2][b] - T(2) * H[p3][a][b] * VHk[p2][p4][p1] - T(2) * Vk[p2][p1] * HH[p3][p4][a][b] +
                                       T(4) * k[p1][b] * HHV[p3][p4][p2][a] + T(4) * k[p1][a] * HHV[p3][p4][p2][b]) -
                         T(4) * V[p1][b] * V[p2][a] * kHHk[p1][p3][p4][p2] + T(4) * k[p1][b] * V[p2][a] * VHHk[p1][p3][p4][p2] +
                         T(4) * k[p1][a] * V[p2][b] * VHHk[p1][p3][p4][p2] - T(2) * k[p2][a] * k[p4][b] * VHHV[p1][p4][p3][p2] +
                         T(2) * (-(T(2) * k[p1][a] * k[p2][b]) + k[p2][a] * k[p4][b]) * VHHV[p2][p3][p4][p1]) /
                            T(4);
                }
            }
            Hout[a][b] = theentry / T(2);
            Hout[b][a] = Hout[a][b];
        }
    }

    return v_t_to_v_extended_t<T, Tcur>(Hout.vectorize());
}

#define _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                     \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_VVG_V<T, Current_Type_Selector<T>::Tcur>(                                             \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t);                                                                                                                                       \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_VVG_G<T, Current_Type_Selector<T>::Tcur, D>(                                          \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&);

#define _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                    \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_VVGG_G<T, Current_Type_Selector<T>::Tcur, D>(                                         \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t, size_t);                                                                                                                               \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_VVGG_V<T, Current_Type_Selector<T>::Tcur, D>(                                         \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t, size_t);

#define _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                   \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_VVGGG_V<T, Current_Type_Selector<T>::Tcur, D>(                                        \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t, size_t, size_t);                                                                                                                       \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_VVGGG_G<T, Current_Type_Selector<T>::Tcur, D>(                                        \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t, size_t, size_t);

#define _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                          \
    _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                         \
    _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                         \
    _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                         \
    _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                         \
    _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                         \
    _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                         \
    _INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#define _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                         \
    _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                        \
    _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                        \
    _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                        \
    _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                        \
    _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                        \
    _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                        \
    _INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#define _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                        \
    _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                       \
    _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                       \
    _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                       \
    _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                       \
    _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                       \
    _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                       \
    _INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#ifdef INCLUDE_EH_GRAVITY
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, R)
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, C)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, R)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, C)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, R)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, C)

#ifdef HIGH_PRECISON
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, RHP)
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, CHP)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, RHP)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, CHP)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, RHP)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, CHP)
#endif

#ifdef VERY_HIGH_PRECISON
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, RVHP)
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, CVHP)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, RVHP)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, CVHP)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, RVHP)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, CVHP)
#endif

#ifdef USE_GMP
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, RGMP)
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, CGMP)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, RGMP)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, CGMP)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, RGMP)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, CGMP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, F32)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, F32)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_EH_VVG_GRAVITY_FUNCTIONS(extern, BigRat)
_INSTANTIATE_EH_VVGG_GRAVITY_FUNCTIONS(extern, BigRat)
_INSTANTIATE_EH_VVGGG_GRAVITY_FUNCTIONS(extern, BigRat)
#endif
#endif
#endif
} // namespace BG
} // namespace Caravel
