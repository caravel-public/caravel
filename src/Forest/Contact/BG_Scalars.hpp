namespace Caravel {
namespace BG {

template <typename T, typename Tcur, size_t Ds, PropagatorType ptype>
std::vector<Tcur> Scalar_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& mom, const size_t& midx, const BuilderContainer<T>& bc) {
    const T& msq = bc.mass[midx] * bc.mass[midx];

    if constexpr (ptype == PropagatorType::propagator) prop = T(1) / (mom * mom - msq);
    if constexpr (ptype == PropagatorType::cut) prop = T(1);
    return std::vector<Tcur>({prop * in.front()});
}

// DS SCALAR
// TODO: Why do ds_scalars have this weird sign?!
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> ds_Scalar_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& mom, const BuilderContainer<T>& bc) {
    prop = T(1) / (mom * mom);
    return std::vector<Tcur>({-prop * in.front()});
}

template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> ds_Scalar_Cut_Propagator(const std::vector<Tcur>& in, T& prop, const momentumD<T, Ds>& mom, const BuilderContainer<T>& bc) {
    prop = T(1);
    return std::vector<Tcur>({-in.front()});
}

} // namespace BG
} // namespace Caravel
