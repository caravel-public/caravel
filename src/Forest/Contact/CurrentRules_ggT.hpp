/**
 * @file CurrentRules_ggT.hpp
 *
 * @date 14.5.2018
 *
 * @brief Implementation of the ggT (auxiliary tensor for QCD)
 *
*/

#include <iostream>

#include "Forest/Builder.h"
#include "Core/Debug.h"

#include "vectorboson_utilities.hpp"

namespace Caravel {

template <typename Tcur,size_t D> void contract_tensor_with_vector(std::vector<Tcur>& out,const std::vector<Tcur>& tensor,const std::vector<Tcur>& in){
	for(size_t ii=0;ii<D;ii++)
		// we use inner_product (instead of corresponding metric) because tensor has been prepared accordingly for efficiency
		out[ii]=std::inner_product(tensor.begin()+ii*D,tensor.begin()+ii*D+D,in.begin(),Tcur(0));
}

// This rule gives a vector current corresponding to the '0' entry in the ggT vertex with: g(0)g(1)T(2)
// We factor out of this rule an 'I*g' (such that considering the accompanying CombineCurrent_CurrentRule_ggT_2, the normalizations are in accordance with computations with 4-gluon vertices)
template <typename T, typename Tcur, size_t D> std::vector<Tcur> CombineCurrent_CurrentRule_ggT_0(const std::vector<momentumD<T,D> const*>& momlist,const std::vector<CurrentWorker_Template_Base<T,D> const*>& currentlist, const BuilderContainer<T>& bc){
	// allocating memory for a vector current
	std::vector<Tcur> result(D);

	// the vector input current
	const std::vector<Tcur>& J1(currentlist[0]->get_current());
	// the tensor input current
	const std::vector<Tcur>& Taux(currentlist[1]->get_current());

	// result^\mu = - Taux^{\mu\nu} J1_\nu
	// MINUS sign missing
	contract_tensor_with_vector<Tcur,D>(result,Taux,J1);
	// flip sign
	std::transform(result.begin(),result.end(),result.begin(),[](Tcur c){return -c;});

//std::cout<<"ggT_0 combination rule: result= "<<result<<std::endl;
	return result;
}
// REMEMBER TO ADD YOUR CASE TO get_combine_rule(CurrentBasic*)

// This rule gives a vector current corresponding to the '1' entry in the ggT vertex with: g(0)g(1)T(2)
// We factor out of this rule an 'I*g' (such that considering the accompanying CombineCurrent_CurrentRule_ggT_2, the normalizations are in accordance with computations with 4-gluon vertices)
template <typename T, typename Tcur, size_t D> std::vector<Tcur> CombineCurrent_CurrentRule_ggT_1(const std::vector<momentumD<T,D> const*>& momlist,const std::vector<CurrentWorker_Template_Base<T,D> const*>& currentlist, const BuilderContainer<T>& bc){
	// allocating memory for a vector current
	std::vector<Tcur> result(D);

	// the tensor input current
	const std::vector<Tcur>& Taux(currentlist[0]->get_current());
	// the vector input current
	const std::vector<Tcur>& J0(currentlist[1]->get_current());

	// result^\mu = Taux^{\mu\nu} J0_\nu
	contract_tensor_with_vector<Tcur,D>(result,Taux,J0);

//std::cout<<"ggT_1 combination rule: result= "<<result<<std::endl;
	return result;
}
// REMEMBER TO ADD YOUR CASE TO get_combine_rule(CurrentBasic*)

// This rule gives a tensor current corresponding to the '2' entry in the ggT vertex with: g(0)g(1)T(2)
// We factor out of this rule an 'I*I*g/2' (one 'I' for the intrinsically included propagator, which is reincluded by Builder_fixed_Ds::get_overall_normalization_info(.)) 
// 	(such that considering the accompanying CombineCurrent_CurrentRule_ggT_{0,1}, the normalizations are in accordance with computations with 4-gluon vertices)
template <typename T, typename Tcur, size_t D> std::vector<Tcur> CombineCurrent_CurrentRule_ggT_2(const std::vector<momentumD<T,D> const*>& momlist,const std::vector<CurrentWorker_Template_Base<T,D> const*>& currentlist, const BuilderContainer<T>& bc){
	// allocating memory for a tensor current (though for auxiliary QCD tensor we only really use D*(D-1)/2)
	std::vector<Tcur> result(D*D,Tcur(0));

	// the vector input current
	const std::vector<Tcur>& J0(currentlist[0]->get_current());
	// the vector input current
	const std::vector<Tcur>& J1(currentlist[1]->get_current());

	// now we construct the tensor in full, all D^2 entries, this just to facilitate operations in CombineCurrent_CurrentRule_ggT_{0,1}
	for(size_t mu=0;mu<D;mu++){
		for(size_t nu=mu+1;nu<D;nu++){
			Tcur theentry(J0[mu]*J1[nu]-J0[nu]*J1[mu]);
			// Non-local assignments, can be expensive
			result[D*mu+nu]=T(get_metric_signature<T>(nu))*theentry;
			result[D*nu+mu]=-T(get_metric_signature<T>(mu))*theentry;
		}
	}

//std::cout<<"ggT_2 combination rule: result= "<<result<<std::endl;
	return result;
}
// REMEMBER TO ADD YOUR CASE TO get_combine_rule(CurrentBasic*)


}
