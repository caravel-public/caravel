namespace Caravel {
namespace BG {

/**
 * Function to transform vector of T's to vector of Tcur's
 */
template <typename T,typename Tcur> std::enable_if_t<is_extended<Tcur>::value,std::vector<Tcur>> v_t_to_v_extended_t(const std::vector<T>& in){
	std::vector<Tcur> out;
	for(auto& i:in) out.push_back(Tcur(i));
	return out;
}

template <typename T,typename Tcur> std::enable_if_t<!is_extended<Tcur>::value,std::vector<T>> v_t_to_v_extended_t(const std::vector<T>& in){
	return in;
}

/**
 * Function to transform vector of Tcur's to vector of T's, assuming all Tcur info (but scalar entry) is irrelevant
 */
template <typename T,typename Tcur> std::enable_if_t<is_extended<Tcur>::value,std::vector<T>> v_extended_t_to_v_t(const std::vector<Tcur>& in){
	std::vector<T> out;
	for(auto& i:in) out.push_back(get_e0(i));
	return out;
}

template <typename T,typename Tcur> std::enable_if_t<!is_extended<Tcur>::value,std::vector<T>> v_extended_t_to_v_t(const std::vector<Tcur>& in){
	return in;
}

} // namespace BG
} // namespace Caravel

