/**
 * @file States_Vector_Boson.hpp
 *
 * @date 12.6.2017
 *
 * @brief Contains templated code for D-dim vector boson states
 *
 * Here we define all states employed in YM and SM_color_ordered regarding vector bosons in D-dims
 * when setting settings::general::cut_states::states
 *
*/

#include <iostream>
#include <algorithm>
#include <utility>
#include <functional>

#include "Core/spinor/spinor.h"
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/Debug.h"
#include "Core/type_traits_extra.h"

#define _LARGE_SQRT_FACTOR 1000000
#define _LARGE_INV_MU 10000000

#include "vectorboson_utilities.hpp"

namespace Caravel {

// catching non-complex (exact) zeros
template <class F> std::enable_if_t<is_exact<F>::value,bool> T_is_zero(const F& in){
	return (in==F(0));
}

// catching complex (numerical) zeros
template <class T> std::enable_if_t<!is_exact<T>::value,bool> T_is_zero(const T& in){
	using std::abs;
	return (abs(in)<abs(T(1)/T(_LARGE_SQRT_FACTOR)));
}

// catching complex (exact) zeros
template <class CF> std::enable_if_t<is_exact<CF>::value && is_complex<CF>::value,bool> vectorbosonWF_X_Y_match(const CF& inX,const CF& inY){
	return (inX==CF(0,1)*inY);
}

// catching complex (numerical) zeros
template <class T> std::enable_if_t<(!is_exact<T>::value) && is_complex<T>::value,bool> vectorbosonWF_X_Y_match(const T& inX,const T& inY){
	return (abs(inX-T(0,1)*inY)<abs(T(1)/T(_LARGE_SQRT_FACTOR)));
}

// Use BH old phase convention -- never for exact
template <class F> std::enable_if_t<is_exact<F>::value,bool> USE_OLD_PHASE_CONVENTION(const F& in){
	return false;
}

// Use BH old phase convention -- never for exact
template <class T>  std::enable_if_t<!is_exact<T>::value,bool> USE_OLD_PHASE_CONVENTION(const T& in){
	return (in<T(0));
}

// 'avoid_phase_change' used when building a full set of D-dimensional polarization vectors, we then bypass any little group reweighting done for (external) 4-D momenta (important because of ep_pm(-l) <---> ep_mp(l) identification)
template <typename T, typename Tcur, size_t D, bool avoid_phase_change = false> std::enable_if_t<is_complex<T>::value,std::vector<Tcur>> externalWF_vectorboson_m_momentum_default(const momD<T,D>& imom){
	std::vector<Tcur> carray(D,Tcur(0));
	T E(imom.pi(0)); T X(imom.pi(1)); T Y(imom.pi(2)); T Z(imom.pi(3));
	if(vectorbosonWF_X_Y_match(X,Y)){
		DEBUG_MESSAGE("vectorbosonWF_X_Y_match(X,Y) got true in externalWF_vectorboson_m_momentum_default(.) with mom= ",imom," --- returned special case");
		// here there are explicit phase convention choices!
		if(T_is_zero(X)){
			// E==Z (this phase is well defined w.r.t. formulae below)
			if(T_is_zero(E-Z)){
				carray[1]=T(-1);
				carray[2]=T(0,-1);
			}
			// E==-Z (this phase is conventional)
			else{
				carray[1]=T(1);
				carray[2]=T(0,-1);
                                
                                // return as no more (phase) changes required in this conventional case
                                return carray;
			}
		}
		else{
			// E==Z && X==I*Y && X!=0 (this phase is well defined w.r.t. formulae below)
			if(T_is_zero(E-Z)){
				carray[1]=X*X/(E*E)-T(1);
				carray[2]=T(0,-1)*(X*X/(E*E)+T(1));
				carray[3]=T(2)*X/E;
			}
			// E==-Z (this phase is conventional)
			else{
				carray[1]=T(1);
				carray[2]=T(0,-1);
                                
                                // return as no more (phase) changes required in this conventional case
                                return carray;
			}
		}
	}
	else{
		// a 1/sqrt(2) has been factored out
		T factor(T(1)/E/(-X+T(0,1)*Y));
		//carray[0]=0;
		carray[1]=factor*(T(0,1)*E*Y-X*Z);
		carray[2]=factor*(T(0,-1)*E*X-Y*Z);
		carray[3]=factor*(E*E-Z*Z);
	}
#if _OLD_PHASE_CONVENTION
#else
	if(USE_OLD_PHASE_CONVENTION(std::real(imom.pi(0))))
		multiply_current_by_factor(carray,T(-1));
#endif
	return carray;
}

// 'avoid_phase_change' used when building a full set of D-dimensional polarization vectors, we then bypass any little group reweighting done for (external) 4-D momenta (important because of ep_pm(-l) <---> ep_mp(l) identification)
template <typename T, typename Tcur, size_t D, bool avoid_phase_change = false> std::enable_if_t<!is_complex<T>::value,std::vector<Tcur>> externalWF_vectorboson_m_momentum_default(const momD<T,D>& imom){
	std::vector<Tcur> carray(D,Tcur(0));
	T E(imom.pi(0)); T X(imom.pi(1)); T Y(imom.pi(2)); T Z(imom.pi(3));
	if(T_is_zero(X+Y)){
		DEBUG_MESSAGE("exact vectorbosonWF_X_Y_match(X,Y) got true in externalWF_vectorboson_m_momentum_default(.) with mom= ",imom," --- returned special case");
		// here there are explicit phase convention choices!
		if(T_is_zero(X)){
			// E==Z (this phase is well defined w.r.t. formulae below)
			if(T_is_zero(E-Z)){
				carray[1]=T(-1);
				carray[2]=T(-1);
			}
			// E==-Z (this phase is conventional)
			else{
				carray[1]=T(1);
				carray[2]=T(-1);
                                
                                // return as no more (phase) changes required in this conventional case
                                return carray;
			}
		}
		else{
			// E==Z && X==I*Y && X!=0 (this phase is well defined w.r.t. formulae below)
			if(T_is_zero(E-Z)){
				carray[1]=X*X/(E*E)-T(1);
				carray[2]=T(-1)*(X*X/(E*E)+T(1));
				carray[3]=T(2)*X/E;
			}
			// E==-Z (this phase is conventional)
			else{
				carray[1]=T(1);
				carray[2]=T(-1);
                                
                                // return as no more (phase) changes required in this conventional case
                                return carray;
			}
		}
	}
	else{
		// a 1/sqrt(2) has been factored out
		T factor(T(-1)/E/(X+Y));
		//carray[0]=0;
		carray[1]=factor*(-E*Y-X*Z);
		carray[2]=factor*(-E*X-Y*Z);
		carray[3]=factor*(E*E-Z*Z);
	}

        // Adjust little group scaling
        if constexpr (!avoid_phase_change) multiply_current_by_factor(carray, E + Z);

        return carray;
}

// 'avoid_phase_change' used when building a full set of D-dimensional polarization vectors, we then bypass any little group reweighting done for (external) 4-D momenta (important because of ep_pm(-l) <---> ep_mp(l) identification)
template <typename T, typename Tcur, size_t D, bool avoid_phase_change = false> std::enable_if_t<is_complex<T>::value,std::vector<Tcur>> externalWF_vectorboson_m_momenta(const momD<T,D>& imom,const momD<T,D>& ref){
	std::vector<Tcur> carray(D,Tcur(0));
	T E(imom.pi(0)); T X(imom.pi(1)); T Y(imom.pi(2)); T Z(imom.pi(3));
	T rE(ref.pi(0)); T rX(ref.pi(1)); T rY(ref.pi(2)); T rZ(ref.pi(3));
	// a (1/sqrt(2))^-1 has been factored out
	T factor(T(0,1)*(rY*E-Y*rE-Y*rZ+rY*Z)+(-rX*E+X*rE+X*rZ-rX*Z));
	// this special case needs proper handling: E=-Z && X=-I*Y (consistent treatment with externalWF_vectorboson_p_momenta(.))
	if(T_is_zero(E+Z)&&T_is_zero(X+T(0,1)*Y)){
		T invfactor(T(1)/(rE+rZ));
		carray[0]=invfactor*(-(T(0,1)*E*rY-rX*E-X*rE-X*rZ));
		carray[1]=invfactor*(-T(0,1)*X*rY+E*rE+E*rZ+X*rX);
		carray[2]=invfactor*(-(T(0,1)*(T(0,1)*X*rY+E*rE+E*rZ-X*rX)));
		carray[3]=invfactor*(T(0,1)*E*rY-rX*E+X*rE+X*rZ);
	}
	// this special case needs proper handling: E=-Z && X=I*Y (consistent treatment with externalWF_vectorboson_p_momenta(.))
	else if(T_is_zero(E+Z)&&T_is_zero(X-T(0,1)*Y)){
		T invfactor(T(1)/(T(0,1)*X*rY+rE*E+rZ*E-X*rX));
		carray[0]=invfactor*(-(T(0,1)*rY-rX));
		carray[1]=invfactor*(rE+rZ);
		carray[2]=invfactor*(-(T(0,1)*(rE+rZ)));
		carray[3]=invfactor*(T(0,1)*rY-rX);
	}
	else if(T_is_zero(factor)){
		std::cout<<"ERROR: division by zero in complex externalWF_vectorboson_m_momenta(.)"<<std::endl;
		std::cout<<"mom: "<<imom<<" ref: "<<ref<<std::endl;
		exit(1);
		// forward call to default
		return externalWF_vectorboson_m_momentum_default<T,Tcur,D>(imom);
	}
	else{
		T invfactor(T(-1)/factor);
		carray[0]=invfactor*(T(0,1)*(-X*rY+Y*rX)+rE*E+rZ*E+X*rX+Y*rY+rE*Z+rZ*Z);
		carray[1]=-invfactor*(T(0,1)*(rY*E-Y*rE-Y*rZ+rY*Z)-rX*E-X*rE-X*rZ-rX*Z);
		carray[2]=invfactor*(rY*E+Y*rE+Y*rZ+rY*Z+T(0,-1)*(-rX*E+X*rE+X*rZ-rX*Z));
		carray[3]=invfactor*(T(0,1)*(X*rY-Y*rX)+rE*E+rZ*E-X*rX-Y*rY+rE*Z+rZ*Z);
	}
#if _OLD_PHASE_CONVENTION
#else
	if(USE_OLD_PHASE_CONVENTION(std::real(imom.pi(0))))
		multiply_current_by_factor(carray,T(-1));
#endif
	return carray;
}

// 'avoid_phase_change' used when building a full set of D-dimensional polarization vectors, we then bypass any little group reweighting done for (external) 4-D momenta (important because of ep_pm(-l) <---> ep_mp(l) identification)
template <typename T, typename Tcur, size_t D, bool avoid_phase_change = false> std::enable_if_t<!is_complex<T>::value,std::vector<Tcur>> externalWF_vectorboson_m_momenta(const momD<T,D>& imom,const momD<T,D>& ref){
	std::vector<Tcur> carray(D,Tcur(0));
	T E(imom.pi(0)); T X(imom.pi(1)); T Y(imom.pi(2)); T Z(imom.pi(3));
	T rE(ref.pi(0)); T rX(ref.pi(1)); T rY(ref.pi(2)); T rZ(ref.pi(3));
	// a (1/sqrt(2))^-1 has been factored out
	T factor(-(rY*E-Y*rE-Y*rZ+rY*Z)+(-rX*E+X*rE+X*rZ-rX*Z));
	// this special case needs proper handling: E=-Z && X=Y (consistent treatment with externalWF_vectorboson_p_momenta(.))
	if(T_is_zero(E+Z)&&T_is_zero(X-Y)){
		T invfactor(T(1)/(rE+rZ));
		carray[0]=invfactor*(E*rX+E*rY+X*rE+X*rZ);
		carray[1]=invfactor*(E*rE+E*rZ+X*rX+X*rY);
		carray[2]=invfactor*(-(E*rE+E*rZ-X*rX-X*rY));
		carray[3]=invfactor*(-(E*rX+E*rY-X*rE-X*rZ));
	}
	// this special case needs proper handling: E=-Z && X=-Y (consistent treatment with externalWF_vectorboson_p_momenta(.))
	else if(T_is_zero(E+Z)&&T_is_zero(X+Y)){
		T invfactor(T(1)/(E*rE+E*rZ-X*rX-X*rY));
		carray[0]=invfactor*(rX+rY);
		carray[1]=invfactor*(rE+rZ);
		carray[2]=invfactor*(-(rE+rZ));
		carray[3]=invfactor*(-(rX+rY));
	}
	else if(T_is_zero(factor)){
		std::cout<<"ERROR: division by zero in real exact externalWF_vectorboson_m_momenta(.)"<<std::endl;
		std::cout<<"mom: "<<imom<<" ref: "<<ref<<std::endl;
		exit(1);
		// forward call to default
		return externalWF_vectorboson_m_momentum_default<T,Tcur,D>(imom);
	}
	else{
		T invfactor(T(-1)/factor);
		carray[0]=invfactor*(-(-X*rY+Y*rX)+rE*E+rZ*E+X*rX-Y*rY+rE*Z+rZ*Z);
		carray[1]=-invfactor*(-(rY*E-Y*rE-Y*rZ+rY*Z)-rX*E-X*rE-X*rZ-rX*Z);
		carray[2]=invfactor*(rY*E+Y*rE+Y*rZ+rY*Z-(-rX*E+X*rE+X*rZ-rX*Z));
		carray[3]=invfactor*(-(X*rY-Y*rX)+rE*E+rZ*E-X*rX+Y*rY+rE*Z+rZ*Z);
	}
        // Adjust little group scaling
        if constexpr (!avoid_phase_change) multiply_current_by_factor(carray, E + Z);

        return carray;
}

// 'avoid_phase_change' used when building a full set of D-dimensional polarization vectors, we then bypass any little group reweighting done for (external) 4-D momenta (important because of ep_pm(-l) <---> ep_mp(l) identification)
template <typename T, typename Tcur, size_t D, bool avoid_phase_change = false> std::enable_if_t<is_complex<T>::value,std::vector<Tcur>> externalWF_vectorboson_p_momentum_default(const momD<T,D>& imom){
	std::vector<Tcur> carray(D,Tcur(0));
	T E(imom.pi(0)); T X(imom.pi(1)); T Y(imom.pi(2)); T Z(imom.pi(3));
	// NOTICE minus sign!
	if(vectorbosonWF_X_Y_match(X,-Y)){
		DEBUG_MESSAGE("vectorbosonWF_X_Y_match(X,Y) got true in externalWF_vectorboson_p_momentum_default(.) with mom= ",imom," --- returned special case");
		// here there are explicit phase convention choices!
		if(T_is_zero(X)){
			// E==Z (this phase is well defined w.r.t. formulae below)
			if(T_is_zero(E-Z)){
				carray[1]=T(1);
				carray[2]=T(0,-1);
			}
			// E==-Z (this phase is conventional)
			else{
				carray[1]=T(-1);
				carray[2]=T(0,-1);
                                
                                // return as no more (phase) changes required in this conventional case
                                return carray;
			}
		}
		else{
			// E==Z && X==I*Y && X!=0 (this phase is well defined w.r.t. formulae below)
			if(T_is_zero(E-Z)){
				carray[1]=-X*X/(E*E)+T(1);
				carray[2]=T(0,-1)*(X*X/(E*E)+T(1));
				carray[3]=T(-2)*X/E;
			}
			// E==-Z (this phase is conventional)
			else{
				carray[1]=T(-1);
				carray[2]=T(0,-1);
                                
                                // return as no more (phase) changes required in this conventional case
                                return carray;
			}
		}
	}
	else{
		// a 1/sqrt(2) has been factored out
		T factor(T(-1)/E/(X+T(0,1)*Y));
		//carray[0]=0;
		carray[1]=factor*(T(0,-1)*E*Y-X*Z);
		carray[2]=factor*(T(0,1)*E*X-Y*Z);
		carray[3]=factor*(E*E-Z*Z);
	}
#if _OLD_PHASE_CONVENTION
#else
	// NOTICE minus sign!
	if(USE_OLD_PHASE_CONVENTION(std::real(imom.pi(0))))
		multiply_current_by_factor(carray,T(-1));
#endif
	return carray;
}

// 'avoid_phase_change' used when building a full set of D-dimensional polarization vectors, we then bypass any little group reweighting done for (external) 4-D momenta (important because of ep_pm(-l) <---> ep_mp(l) identification)
template <typename T, typename Tcur, size_t D, bool avoid_phase_change = false> std::enable_if_t<(!is_complex<T>::value),std::vector<Tcur>> externalWF_vectorboson_p_momentum_default(const momD<T,D>& imom){
	std::vector<Tcur> carray(D,Tcur(0));
	T E(imom.pi(0)); T X(imom.pi(1)); T Y(imom.pi(2)); T Z(imom.pi(3));
	// NOTICE minus sign!
	if(T_is_zero(X-Y)){
		DEBUG_MESSAGE("exact vectorbosonWF_X_Y_match(X,Y) got true in externalWF_vectorboson_p_momentum_default(.) with mom= ",imom," --- returned special case");
		// here there are explicit phase convention choices!
		if(T_is_zero(X)){
			// E==Z (this phase is well defined w.r.t. formulae below)
			if(T_is_zero(E-Z)){
				carray[1]=T(1);
				carray[2]=T(-1);
			}
			// E==-Z (this phase is conventional)
			else{
				carray[1]=T(-1);
				carray[2]=T(-1);
                                
                                // return as no more (phase) changes required in this conventional case
                                return carray;
			}
		}
		else{
			// E==Z && X==I*Y && X!=0 (this phase is well defined w.r.t. formulae below)
			if(T_is_zero(E-Z)){
				carray[1]=-X*X/(E*E)+T(1);
				carray[2]=T(-1)*(X*X/(E*E)+T(1));
				carray[3]=T(-2)*X/E;
			}
			// E==-Z (this phase is conventional)
			else{
				carray[1]=T(-1);
				carray[2]=T(-1);
                                
                                // return as no more (phase) changes required in this conventional case
                                return carray;
			}
		}
	}
	else{
		// a 1/sqrt(2) has been factored out
		T factor(T(1)/E/(-X+Y));
		//carray[0]=0;
		carray[1]=factor*(E*Y-X*Z);
		carray[2]=factor*(E*X-Y*Z);
		carray[3]=factor*(E*E-Z*Z);
	}
        // Adjust little group scaling
        if constexpr (!avoid_phase_change) multiply_current_by_factor(carray, T(1) / (E + Z));

        return carray;
}

// 'avoid_phase_change' used when building a full set of D-dimensional polarization vectors, we then bypass any little group reweighting done for (external) 4-D momenta (important because of ep_pm(-l) <---> ep_mp(l) identification)
template <typename T, typename Tcur, size_t D, bool avoid_phase_change = false> std::enable_if_t<is_complex<T>::value,std::vector<Tcur>> externalWF_vectorboson_p_momenta(const momD<T,D>& imom,const momD<T,D>& ref){
	std::vector<Tcur> carray(D,Tcur(0));
	T E(imom.pi(0)); T X(imom.pi(1)); T Y(imom.pi(2)); T Z(imom.pi(3));
	T rE(ref.pi(0)); T rX(ref.pi(1)); T rY(ref.pi(2)); T rZ(ref.pi(3));
	// a (1/sqrt(2))^-1 has been factored out
	T factor(T(0,1)*(rY*E-Y*rE-Y*rZ+rY*Z)+rX*E-X*rE-X*rZ+rX*Z);
	// this special case needs proper handling: E=-Z && X=-I*Y (consistent treatment with externalWF_vectorboson_m_momenta(.))
	if(T_is_zero(E+Z)&&T_is_zero(X+T(0,1)*Y)){
		T invfactor(T(1)/(-T(0,1)*X*rY+rE*E+rZ*E-X*rX));
		carray[0]=invfactor*(-(T(0,1)*rY+rX));
		carray[1]=invfactor*(-(rE+rZ));
		carray[2]=invfactor*(-(T(0,1)*(rE+rZ)));
		carray[3]=invfactor*(T(0,1)*rY+rX);
	}
	// this special case needs proper handling: E=-Z && X=I*Y (consistent treatment with externalWF_vectorboson_m_momenta(.))
	else if(T_is_zero(E+Z)&&T_is_zero(X-T(0,1)*Y)){
		T invfactor(T(1)/(rE+rZ));
		carray[0]=invfactor*(-(T(0,1)*E*rY+rX*E+X*rE+X*rZ));
		carray[1]=invfactor*(-(T(0,1)*X*rY+E*rE+E*rZ+X*rX));
		carray[2]=invfactor*(-(T(0,1)*(-T(0,1)*X*rY+E*rE+E*rZ-X*rX)));
		carray[3]=invfactor*(T(0,1)*E*rY+rX*E-X*rE-X*rZ);
	}
	else if(T_is_zero(factor)){
		std::cout<<"ERROR: division by zero in complex externalWF_vectorboson_p_momenta(.)"<<std::endl;
		std::cout<<"mom: "<<imom<<" ref: "<<ref<<std::endl;
		exit(1);
		// forward call to default
		return externalWF_vectorboson_p_momentum_default<T,Tcur,D>(imom);
	}
	else{
		T invfactor(T(1)/factor);
		carray[0]=invfactor*(T(0,1)*(X*rY-Y*rX)+rE*E+rZ*E+X*rX+Y*rY+rE*Z+rZ*Z);
		carray[1]=invfactor*(T(0,1)*(rY*E-Y*rE-Y*rZ+rY*Z)+rX*E+X*rE+X*rZ+rX*Z);
		carray[2]=invfactor*(rY*E+Y*rE+Y*rZ+rY*Z+T(0,-1)*(rX*E-X*rE-X*rZ+rX*Z));
		carray[3]=invfactor*(T(0,1)*(-X*rY+Y*rX)+rE*E+rZ*E-X*rX-Y*rY+rE*Z+rZ*Z);
	}
#if _OLD_PHASE_CONVENTION
#else
	// NOTICE minus sign!
	if(USE_OLD_PHASE_CONVENTION(std::real(imom.pi(0))))
		multiply_current_by_factor(carray,T(-1));
#endif
	return carray;
}

// 'avoid_phase_change' used when building a full set of D-dimensional polarization vectors, we then bypass any little group reweighting done for (external) 4-D momenta (important because of ep_pm(-l) <---> ep_mp(l) identification)
template <typename T, typename Tcur, size_t D, bool avoid_phase_change = false> std::enable_if_t<(!is_complex<T>::value),std::vector<Tcur>> externalWF_vectorboson_p_momenta(const momD<T,D>& imom,const momD<T,D>& ref){
	std::vector<Tcur> carray(D,Tcur(0));
	T E(imom.pi(0)); T X(imom.pi(1)); T Y(imom.pi(2)); T Z(imom.pi(3));
	T rE(ref.pi(0)); T rX(ref.pi(1)); T rY(ref.pi(2)); T rZ(ref.pi(3));
	// a (1/sqrt(2))^-1 has been factored out
	T factor(-(rY*E-Y*rE-Y*rZ+rY*Z)+rX*E-X*rE-X*rZ+rX*Z);
	// this special case needs proper handling: E=-Z && X=Y (consistent treatment with externalWF_vectorboson_m_momenta(.))
	if(T_is_zero(E+Z)&&T_is_zero(X-Y)){
		T invfactor(T(1)/(E*rE+E*rZ-X*rX+X*rY));
		carray[0]=invfactor*(-(rX-rY));
		carray[1]=invfactor*(-(rE+rZ));
		carray[2]=invfactor*(-(rE+rZ));
		carray[3]=invfactor*(rX-rY);
	}
	// this special case needs proper handling: E=-Z && X=-Y (consistent treatment with externalWF_vectorboson_m_momenta(.))
	else if(T_is_zero(E+Z)&&T_is_zero(X+Y)){
		T invfactor(T(1)/(rE+rZ));
		carray[0]=invfactor*(-(E*rX-E*rY+X*rE+X*rZ));
		carray[1]=invfactor*(-(E*rE+E*rZ+X*rX-X*rY));
		carray[2]=invfactor*(-(E*rE+E*rZ-X*rX+X*rY));
		carray[3]=invfactor*(E*rX-E*rY-X*rE-X*rZ);
	}
	else if(T_is_zero(factor)){
		std::cout<<"ERROR: division by zero in real exact externalWF_vectorboson_p_momenta(.)"<<std::endl;
		std::cout<<"mom: "<<imom<<" ref: "<<ref<<std::endl;
		exit(1);
		// forward call to default
		return externalWF_vectorboson_p_momentum_default<T,Tcur,D>(imom);
	}
	else{
		T invfactor(T(1)/factor);
		carray[0]=invfactor*(-(X*rY-Y*rX)+rE*E+rZ*E+X*rX-Y*rY+rE*Z+rZ*Z);
		carray[1]=invfactor*(-(rY*E-Y*rE-Y*rZ+rY*Z)+rX*E+X*rE+X*rZ+rX*Z);
		carray[2]=invfactor*(rY*E+Y*rE+Y*rZ+rY*Z-(rX*E-X*rE-X*rZ+rX*Z));
		carray[3]=invfactor*(-(-X*rY+Y*rX)+rE*E+rZ*E-X*rX+Y*rY+rE*Z+rZ*Z);
	}
        // Adjust little group scaling
        if constexpr (!avoid_phase_change) multiply_current_by_factor(carray, T(1) / (E + Z));

        return carray;
}


// Polarization state for gluon in 4D with minus helicity
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_m(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	if(refmom==0){
		return externalWF_vectorboson_m_momentum_default<T,Tcur,D>(momconf.p(lmom));
	}
	else{
		return externalWF_vectorboson_m_momenta<T,Tcur,D>(momconf.p(lmom),momconf.p(refmom));
	}
}
// REMEMBER TO ADD YOUR CASE TO get_external_rule(.)

// Polarization state for gluon in 4D with plus helicity
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_p(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	std::vector<Tcur> carray(D,T(0));
	if(refmom==0){
		return externalWF_vectorboson_p_momentum_default<T,Tcur,D>(momconf.p(lmom));
	}
	else{
		return externalWF_vectorboson_p_momenta<T,Tcur,D>(momconf.p(lmom),momconf.p(refmom));
	}
}
// REMEMBER TO ADD YOUR CASE TO get_external_rule(.)

template <typename T> inline std::enable_if_t<!is_complex<T>::value> ASSIGN_lightcone_ref_1(T* n_array){
	// on-shell mom in +-+- signature
	n_array[0]=T(-16);
	n_array[1]=T(-33);
	n_array[2]=T(63);
	n_array[3]=T(56);
}
template <typename T> inline std::enable_if_t<is_complex<T>::value> ASSIGN_lightcone_ref_1(T* n_array){
	// just a Pythagorean quad
	n_array[0]=T(-13);
	n_array[1]=T(-12);
	n_array[2]=T(3);
	n_array[3]=T(4);
}

template <typename T> inline std::enable_if_t<!is_complex<T>::value> ASSIGN_lightcone_ref_2(T* n_array){
	// on-shell mom in +-+- signature
	n_array[0]=T(84);
	n_array[1]=T(-133);
	n_array[2]=T(-187);
	n_array[3]=T(156);
}
template <typename T> inline std::enable_if_t<is_complex<T>::value> ASSIGN_lightcone_ref_2(T* n_array){
	// just a Pythagorean quad
	n_array[0]=T(11);
	n_array[1]=T(2);
	n_array[2]=T(-6);
	n_array[3]=T(-9);
}


/**
 * Given a 5/6-D momentum, producess a flatted (light-like) 4-D vector according to:
 *
 * lflat4D = l4D-musqr/T(2)/(l4D*reference)*reference
 *
 * where l4D are the (massive) 4-D components of the input momentum and reference is a 4-D light-like momentum.
 * If refmom==0 then we chose from two choices of reference momentum
 */
template <typename T, size_t D> momD<T,D> momentum_4D_flat(const momD_conf<T,D>& momconf,momD<T,D>& ref,const size_t& lmom,const size_t& refmom){
	const momD<T,D>& l(momconf.p(lmom));
	// (l4D)^2
	T mm4dsqr = l*l - compute_Dsm4_prod(l,l);

	std::vector<T> lflat4D(l.get_vector(4));
	std::vector<T> rCmom4D(4);
	T to_normalize_a(0);
	// default reference extracted from ASSIGN_lightcone_ref_?
	if(refmom==0){
		// OLD choice
		//T p12(momconf.p(lmom).pi(1)*momconf.p(lmom).pi(1));
		//T p22(momconf.p(lmom).pi(2)*momconf.p(lmom).pi(2));
		//T p32(momconf.p(lmom).pi(3)*momconf.p(lmom).pi(3));
		//rCmom4D[0]=sqrt(p12+p22+p32);
		//rCmom4D[1]=sqrt(p12);
		//rCmom4D[2]=sqrt(p22);
		//rCmom4D[3]=sqrt(p32);

		ASSIGN_lightcone_ref_1(&rCmom4D[0]);
		// check if normalization factor is zero
		to_normalize_a=T(2)*contract_vector_currents_from_0_to<4>(lflat4D,rCmom4D);
		if(T_is_zero(to_normalize_a)){
			ASSIGN_lightcone_ref_2(&rCmom4D[0]);
			// compute normalization factor
			to_normalize_a=T(2)*contract_vector_currents_from_0_to<4>(lflat4D,rCmom4D);
		}
	}
	else{
		rCmom4D[0]=momconf.p(refmom).pi(0);
		rCmom4D[1]=momconf.p(refmom).pi(1);
		rCmom4D[2]=momconf.p(refmom).pi(2);
		rCmom4D[3]=momconf.p(refmom).pi(3);
		// compute normalization factor
		to_normalize_a=T(2)*contract_vector_currents_from_0_to<4>(lflat4D,rCmom4D);
	}
	// lflat4D = lflat4D-musqr/T(2)/(lflat4D*rCmom4D)*rCmom4D;
	add_vector_current_with_prefactor(lflat4D,-mm4dsqr/to_normalize_a,rCmom4D);

	// assign ref
	for(size_t ii=4;ii<D;ii++)	rCmom4D.push_back(T(0));
	ref=momD<T,D>(rCmom4D);

	// return the flatted momentum
	for(size_t ii=4;ii<D;ii++)	lflat4D.push_back(T(0));
	return momD<T,D>(lflat4D);
}

/**
 * mD polarization state for gluon in D dims. NOTICE: we include a normalization factor -1/2 instead of 1/sqrt(2) (compensating
 * the missing factor in the (correlated) pD state)
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_mD(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	momD<T,D> ref;
	momD<T,D> lflat(momentum_4D_flat<T,D>(momconf,ref,lmom,refmom));

	std::vector<Tcur> toret;
	toret=externalWF_vectorboson_m_momenta<T,Tcur,D,true>(lflat,ref);
#if 0
std::cout<<"(m) lflat: "<<lflat<<std::endl;
std::cout<<"(m) ref: "<<ref<<std::endl;
const momD<T,D>& l(momconf.p(lmom));
std::vector<Tcur> lTcur(l.get_vector(D));
std::cout<<"(m)  toret.l: "<<contract_vector_currents_from_0_to<D>(toret,lTcur)<<std::endl;
#endif
	multiply_current_by_factor(toret,T(1)/T(2));
	return toret;
}
// REMEMBER TO ADD YOUR CASE TO get_external_rule(.)

template <typename Tcur> std::enable_if_t<is_complex<Tcur>::value> FLIP_SIGN_CURRENT(std::vector<Tcur>& c){
	// multiply by -1
        typedef typename infer_unext_type<Tcur>::type TE;
	multiply_current_by_factor(c,TE(-1));
}

template <typename Tcur> std::enable_if_t<!is_complex<Tcur>::value> FLIP_SIGN_CURRENT(std::vector<Tcur>& c){
	// do nothing
}

/**
 * pD polarization state for gluon in D dims. NOTICE: we include a normalization factor 1 instead of 1/sqrt(2) (compensating
 * the squared factor in the (correlated) mD state)
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_pD(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	momD<T,D> ref;
	momD<T,D> lflat(momentum_4D_flat<T,D>(momconf,ref,lmom,refmom));

	std::vector<Tcur> toret;
	toret=externalWF_vectorboson_p_momenta<T,Tcur,D,true>(lflat,ref);
#if 0
std::cout<<"(p) lflat: "<<lflat<<std::endl;
std::cout<<"(p) ref: "<<ref<<std::endl;
const momD<T,D>& l(momconf.p(lmom));
std::vector<Tcur> lTcur(l.get_vector(D));
std::cout<<"(p)  toret.l: "<<contract_vector_currents_from_0_to<D>(toret,lTcur)<<std::endl;
#endif
	// flip sign for floating point cases
	// as a result of the identification:
	//	epsilon_+^\mu(l) ( epsilon_+^\mu(l) )^* + epsilon_-^\mu(l) ( epsilon_-^\mu(l) )^*
	// with:
	//	epsilon_+^\mu(l) epsilon_-^\mu(-l) + epsilon_-^\mu(l) epsilon_+^\mu(-l)
	// NOTICE the change in the momentum sign (l) ---> (-l) (which brings a phase convention issue (_OLD_PHASE_CONVENTION)
	FLIP_SIGN_CURRENT(toret);
	return toret;
}
// REMEMBER TO ADD YOUR CASE TO get_external_rule(.)

/**
 * sD polarization state for gluon in D dims. NOTICE: we include a normalization factor 1 instead of m4D^2 (compensating
 * the 1/m4D^2 squared factor in the (correlated) scD state)
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_sD(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	momD<T,D> ref;
	momD<T,D> lflat(momentum_4D_flat<T,D>(momconf,ref,lmom,refmom));

	std::vector<T> rTcur(D);
	std::vector<T> result(D);

        // here convetrsion T --> Tcut happens
        for(unsigned i=0; i<D; i++){ rTcur[i] = ref[i]; }
        for(unsigned i=0; i<D; i++){ result[i] = lflat[i]; }

	// result = T(0,1)*(lflat/mu-mu/to_normalize*rCmom4D);
	T to_normalize(T(2)*(lflat*ref));
	const momD<T,D>& l(momconf.p(lmom));
	// (l4D)^2
	T mm4dsqr = l*l - compute_Dsm4_prod(l,l);
	add_vector_current_with_prefactor(result,-mm4dsqr/to_normalize,rTcur);
	// factoring out I:		multiply_current_by_factor(result,T(0,1));

        return to_extended_t<std::vector<Tcur>,D>(result);
}
// REMEMBER TO ADD YOUR CASE TO get_external_rule(.)

/**
 * scD polarization state for gluon in D dims. NOTICE: we include a normalization factor 1/m4D^2 (compensating
 * the 1 factor in the (correlated) sD state)
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_scD(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	momD<T,D> ref;
	momD<T,D> lflat(momentum_4D_flat<T,D>(momconf,ref,lmom,refmom));

	std::vector<T> rTcur(D);
	std::vector<T> result(D);

        // here convetrsion T --> Tcut happens
        for(unsigned i=0; i<D; i++){ rTcur[i] = ref[i]; }
        for(unsigned i=0; i<D; i++){ result[i] = lflat[i]; }

	// result = T(0,1)*(lflat/mu-mu/to_normalize*rCmom4D);
	T to_normalize(T(2)*(lflat*ref));
	const momD<T,D>& l(momconf.p(lmom));
	// (l4D)^2
	T mm4dsqr = l*l - compute_Dsm4_prod(l,l);
	add_vector_current_with_prefactor(result,-mm4dsqr/to_normalize,rTcur);
	// factoring out I
	multiply_current_by_factor(result,T(-1)/mm4dsqr);

        return to_extended_t<std::vector<Tcur>,D>(result);
}
// REMEMBER TO ADD YOUR CASE TO get_external_rule(.)

// for non-exact complex types we assume metric +-----
template <typename T, typename Tcur> inline std::enable_if_t<is_complex<T>::value && !is_exact<T>::value> ASSIGN_tStates(Tcur* n_array,const T& x,const T& y){
	n_array[0]=y;
	n_array[1]=-x;
}

// for non-exact real types we assume metric +-+-+-
template <typename T, typename Tcur> inline std::enable_if_t<!is_complex<T>::value && !is_exact<T>::value> ASSIGN_tStates(Tcur* n_array,const T& x,const T& y){
	n_array[0]=y;
	n_array[1]=x;
}

// for exact complex types we assume metric +-----
template <typename T, typename Tcur> inline std::enable_if_t<is_complex<T>::value && is_exact<T>::value> ASSIGN_tStates(Tcur* n_array,const T& x,const T& y){
	n_array[0]=y*EpsilonBasis<T>::squares[1];
	n_array[1]=-x*EpsilonBasis<T>::squares[0];
}

// for exact real types we assume metric +-+-+-
template <typename T, typename Tcur> inline std::enable_if_t<!is_complex<T>::value && is_exact<T>::value> ASSIGN_tStates(Tcur* n_array,const T& x,const T& y){
	n_array[0]=y*EpsilonBasis<T>::squares[1];
	n_array[1]=x*EpsilonBasis<T>::squares[0];
}

// Simply normalize non-exact real types, assume metric -+-+-
template <typename T, typename Tcur> inline std::enable_if_t<!is_complex<T>::value && !is_exact<T>::value> NORMALIZE_EXACT_tcStates(Tcur* n_array,const T& mmusqr){
	T to_norm(mmusqr);
	n_array[0]/=-to_norm;
	n_array[1]/=-to_norm;
}

// Simply normalize non-exact complex types, assume metric +-----
template <typename T, typename Tcur> inline std::enable_if_t<is_complex<T>::value && !is_exact<T>::value> NORMALIZE_EXACT_tcStates(Tcur* n_array,const T& mmusqr){
	T to_norm(mmusqr);
	n_array[0]/=to_norm;
	n_array[1]/=to_norm;
}

// Properly normalization for exact complex types we assume metric +-----
template <typename T, typename Tcur> inline std::enable_if_t<is_complex<T>::value && is_exact<T>::value> NORMALIZE_EXACT_tcStates(Tcur* n_array,const T& mmusqr){
	T to_norm(-get_e0(n_array[0])*get_e0(n_array[0])*EpsilonBasis<T>::squares[0]-get_e0(n_array[1])*get_e0(n_array[1])*EpsilonBasis<T>::squares[1]);

	n_array[0]/=to_norm;
	n_array[1]/=to_norm;
}

// Properly normalization for exact real types we assume metric +-+-+-
template <typename T, typename Tcur> inline std::enable_if_t<!is_complex<T>::value && is_exact<T>::value> NORMALIZE_EXACT_tcStates(Tcur* n_array,const T& mmusqr){
	T to_norm(get_e0(n_array[0])*get_e0(n_array[0])*EpsilonBasis<T>::squares[0]-get_e0(n_array[1])*get_e0(n_array[1])*EpsilonBasis<T>::squares[1]);
	n_array[0]/=to_norm;
	n_array[1]/=to_norm;
}

/**
 * tD polarization state for gluon in D dims. NOTICE: we include a normalization factor 1 instead of mu (compensating
 * the 1/mu^2 squared factor in the (correlated) tcD state)
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_tD(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	const momD<T,D>& l(momconf.p(lmom));
	T mmusqr = compute_Dsm4_prod(l,l);

	std::vector<T> result(D,T(0));
	// assign components according to +----- metric (complex) or +-+-+- (real)
        if constexpr (D > 5) { ASSIGN_tStates(&result[4], l.pi(4), l.pi(5)); }
        else {
                std::cerr << "ERROR: called 'tD' states in D=" << D << " dimensions " << std::endl;
                std::exit(1);
        }

        if(T_is_zero(mmusqr)){
		DEBUG_MESSAGE("TOO SMALL mu in tD state!! returned 5th component polarization vector");
		result[4]=T(0);
		// factoring out I
                if constexpr (D > 5) { result[5] = T(1); }
        }
#if 0
std::cout<<"(t) l: "<<l<<std::endl;
std::cout<<"(t) result: "<<result<<std::endl;
std::vector<Tcur> lTcur(l.get_vector(D));
std::cout<<"-1: "<<T(-1)<<std::endl;
std::cout<<"-mu^2: "<<mmusqr<<std::endl;
std::cout<<"(t)  result.l: "<<contract_vector_currents_from_0_to<D>(result,lTcur)<<std::endl;
std::cout<<"(t)   e^2: "<<contract_vector_currents_from_0_to<D>(result,result)<<std::endl;
#endif
	// factoring out I: else{	multiply_current_by_factor(result,T(0,1)); }
        return to_extended_t<std::vector<Tcur>,D>(result);
}
// REMEMBER TO ADD YOUR CASE TO get_external_rule(.)

/**
 * tcD polarization state for gluon in D dims. NOTICE: we include a normalization factor 1/mu^2 (compensating
 * the 1 factor in the (correlated) tD state)
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_tcD(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	const momD<T,D>& l(momconf.p(lmom));
	T mmusqr = compute_Dsm4_prod(l,l);

	std::vector<T> result(D,T(0));
	// assign components according to +----- metric (complex) or +-+-+- (real)
        if constexpr (D > 5) { ASSIGN_tStates(&result[4], l.pi(4), l.pi(5)); }
        else {
                std::cerr << "ERROR: called 'tcD' states in D=" << D << " dimensions " << std::endl;
                std::exit(1);
        }

        if(T_is_zero(mmusqr)){
		DEBUG_MESSAGE("TOO SMALL mu in tcD state!! returned 5th component polarization vector");
		result[4]=T(0);
		// factoring out I
                if constexpr (D > 5) { result[5] = T(1); }
        }
	else{
		// factoring out I
		NORMALIZE_EXACT_tcStates(&result[4],mmusqr);
	}
#if 0
std::cout<<"(tc) l: "<<l<<std::endl;
std::cout<<"(tc) result: "<<result<<std::endl;
std::cout<<"(tc) result final: "<<result<<std::endl;
std::vector<Tcur> lTcur(l.get_vector(D));
std::cout<<"-1: "<<T(-1)<<std::endl;
std::cout<<"-mu^2: "<<mmusqr<<std::endl;
std::cout<<"(tc) result.l: "<<contract_vector_currents_from_0_to<D>(result,lTcur)<<std::endl;
std::cout<<"(tc)  e^2: "<<contract_vector_currents_from_0_to<D>(result,result)<<std::endl;
#endif
        return to_extended_t<std::vector<Tcur>,D>(result);
}

#if INCLUDE_CUBIC_GRAVITY || INCLUDE_EH_GRAVITY
/**
 * A duplicate of externalWF_vectorboson_tD for usage in graviton build up of states,
 * which avoids using 'to_extended_t' function
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> ForGravitons_externalWF_vectorboson_tD(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	const momD<T,D>& l(momconf.p(lmom));
	T mmusqr = compute_Dsm4_prod(l,l);

	std::vector<T> result(D,T(0));
	// assign components according to +----- metric (complex) or +-+-+- (real)
	if constexpr (D > 5) { ASSIGN_tStates(&result[4],l.pi(4),l.pi(5)); }
        else {
                std::cerr << "ERROR: called 'ForGravitons_externalWF_vectorboson_tD' states in D=" << D << " dimensions " << std::endl;
                std::exit(1);
        }

	if(T_is_zero(mmusqr)){
		DEBUG_MESSAGE("TOO SMALL mu in tD state!! returned 5th component polarization vector");
		result[4]=T(0);
		// factoring out I
		if constexpr (D > 5) { result[5]=T(1); }
	}

	std::vector<Tcur> resultTcur(D,Tcur(0));
        for(size_t ii = 0; ii < D; ii++ )  resultTcur[ii] = Tcur(result[ii]);

	// factoring out I: else{	multiply_current_by_factor(result,T(0,1)); }
        return resultTcur;
}

/**
 * A duplicate of externalWF_vectorboson_tcD for usage in graviton build up of states,
 * which avoids using 'to_extended_t' function
 */
template <typename T, typename Tcur, size_t D> std::vector<Tcur> ForGravitons_externalWF_vectorboson_tcD(const momD_conf<T,D>& momconf,const size_t& lmom,const size_t& refmom){
	const momD<T,D>& l(momconf.p(lmom));
	T mmusqr = compute_Dsm4_prod(l,l);

	std::vector<T> result(D,T(0));
	// assign components according to +----- metric (complex) or +-+-+- (real)
	if constexpr (D > 5) { ASSIGN_tStates(&result[4],l.pi(4),l.pi(5)); }
        else {
                std::cerr << "ERROR: called 'ForGravitons_externalWF_vectorboson_tcD' states in D=" << D << " dimensions " << std::endl;
                std::exit(1);
        }

	if(T_is_zero(mmusqr)){
		DEBUG_MESSAGE("TOO SMALL mu in tcD state!! returned 5th component polarization vector");
		result[4]=T(0);
		// factoring out I
		if constexpr (D > 5) { result[5]=T(1); }
	}
	else{
		// factoring out I
		NORMALIZE_EXACT_tcStates(&result[4],mmusqr);
	}

	std::vector<Tcur> resultTcur(D,Tcur(0));
        for(size_t ii = 0; ii < D; ii++ )  resultTcur[ii] = Tcur(result[ii]);

        return resultTcur;
}
#endif

//! Generic trivial polarization state (just i in the position Dsm4Ind)
template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind=5> std::vector<Tcur> externalWF_vectorboson_trivial_Ds(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=7, "Dimensional mismatch");
    static_assert(Dsm4Ind>=5 && Dsm4Ind<=Ds-2, "Dimensional mismatch");
    std::vector<Tcur> result(Ds,T(0));
    // factoring out I
    result[Dsm4Ind+1]=T(1);
    return result;
}

//! Conjugated of generic trivial polarization state (just i in the position Dsm4Ind)
template <typename T, typename Tcur, size_t Ds,unsigned Dsm4Ind=5> std::vector<Tcur> externalWF_vectorboson_trivial_conjugated_Ds(const momD_conf<T,Ds>& momconf,const size_t& lmom,const size_t& refmom){
    static_assert(Ds>=7, "Dimensional mismatch");
    static_assert(Dsm4Ind>=5 && Dsm4Ind<=Ds-2, "Dimensional mismatch");
    std::vector<Tcur> result(Ds,T(0));
    // factoring out I
    result[Dsm4Ind+1]=T(-get_metric_signature<T>(Dsm4Ind+1));
    return result;
}


// Return the momentum itself as polarization vector
template <typename T, typename Tcur, size_t D> std::vector<Tcur> externalWF_vectorboson_Ward(const momD_conf<T,D>& momconf,const size_t& lmom, const size_t& refmom){
	std::vector<Tcur> toret(D);
	for(size_t ii=0;ii<D;ii++)	toret[ii]=momconf.p(lmom).pi(ii);
	return toret;
}

}
