/**
 * @file CurrentRules_ggg_gggg.hpp
 *
 * @date 12.6.2016
 *
 * @brief Implementation of the ggg and gggg gluon Feynman rules
 *
 * Factor of i/sqrt{2} factor out with the coupling gs
 * ALSO: an 'I' removed to be consisted with overall factor from propagators and 4-pt vertices included in Builder_fixed_Ds::get_overall_normalization(.)
*/

#include <iostream>
#include <numeric>
#include <complex>

#include "Forest/Builder.h"
#include "Core/Debug.h"

#include "Core/typedefs.h"

#include "vectorboson_utilities.hpp"

namespace Caravel {
namespace BG{

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CombineCurrent_CurrentRule_ggg(const std::vector<momentumD<T, D> const*>& momlist,
                                                 const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {
    std::vector<Tcur> result(D, Tcur(0));

    const auto& jCURR1 = currentlist[0]->get_current();
    const auto& jCURR2 = currentlist[1]->get_current();
    const auto& qMOM1 = *(momlist[0]);
    const auto& qMOM2 = *(momlist[1]);

    momentumD<T, D> qMOM12(qMOM1 + qMOM2);

    Tcur j1dotj2(contract_vector_currents_from_0_to<D>(jCURR1, jCURR2));
    Tcur k2_2k1_dot_j2(contract_vector_current_with_momentum_from_0_to(jCURR2, qMOM1 + qMOM12));
    Tcur k1_2k2_dot_j1(contract_vector_current_with_momentum_from_0_to(jCURR1, qMOM2 + qMOM12));

    // result+= +(jCURR1*jCURR2) * (qMOM2 - qMOM1)
    add_momentum_with_prefactor(result, j1dotj2, qMOM2 - qMOM1);
    // result+= +((qMOM1+qMOM12)*jCURR2)*jCURR1
    add_vector_current_with_prefactor(result, k2_2k1_dot_j2, jCURR1);
    // result+= -((qMOM2+qMOM12)*jCURR1)*jCURR2
    add_vector_current_with_prefactor(result, -k1_2k2_dot_j1, jCURR2);

    // std::cout<<"Disect: jCURR1*jCURR2= "<<jCURR1*jCURR2<<" qMOM1*jCURR2= "<<qMOM1*jCURR2<<" qMOM2*jCURR2= "<<qMOM2*jCURR2<<std::endl;
    // std::cout<<"Disect: qMOM1*jCURR1= "<<qMOM1*jCURR1<<" qMOM2*jCURR1= "<<qMOM2*jCURR1<<std::endl;
    // std::cout<<"jCURR1: "<<jCURR1<<" , jCURR2"<<jCURR2<<std::endl;
    // std::cout<<"qMOM1: "<<qMOM1<<" , qMOM2"<<qMOM2<<std::endl;
    return result;
}

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CombineCurrent_CurrentRule_gggg(const std::vector<momentumD<T, D> const*>& momlist,
                                                  const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {
    std::vector<Tcur> result(D, Tcur(0));

    const auto& jCURR1 = currentlist[0]->get_current();
    const auto& jCURR2 = currentlist[1]->get_current();
    const auto& jCURR3 = currentlist[2]->get_current();

    Tcur j1dotj2(contract_vector_currents_from_0_to<D>(jCURR1, jCURR2));
    Tcur j1dotj3(contract_vector_currents_from_0_to<D>(jCURR1, jCURR3));
    Tcur j2dotj3(contract_vector_currents_from_0_to<D>(jCURR2, jCURR3));

    // result+=  (T(-2)*(jCURR1*jCURR3))*jCURR2
    add_vector_current_with_prefactor(result, T(-2) * j1dotj3, jCURR2);
    // result+= +(jCURR1*jCURR2)*jCURR3
    add_vector_current_with_prefactor(result, j1dotj2, jCURR3);
    // result+= +(jCURR2*jCURR3)*jCURR1
    add_vector_current_with_prefactor(result, j2dotj3, jCURR1);

    // std::cout<<"jCURR1: "<<jCURR1<<" , jCURR2"<<jCURR2<<" , jCURR3"<<jCURR3<<std::endl;
    // std::cout<<"gggg combination rule: result= "<<result<<std::endl;
    return result;
}



// Rules with scalars

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CombineCurrent_CurrentRule_gss_g(const std::vector<momentumD<T, D> const*>& momlist,
                                                   const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {
    std::vector<Tcur> result(D, Tcur(0));

    const auto& jCURR1 = currentlist[0]->get_current();
    const auto& jCURR2 = currentlist[1]->get_current();
    const auto& qMOM1 = *(momlist[0]);
    const auto& qMOM2 = *(momlist[1]);

    Tcur j1dotj2(jCURR1.front() * jCURR2.front());

    add_momentum_with_prefactor(result, j1dotj2, qMOM2 - qMOM1);

    return result;
}

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CombineCurrent_CurrentRule_ssg(const std::vector<momentumD<T, D> const*>& momlist,
                                                   const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {
    std::vector<Tcur> result(1);

    const auto& jCURR1 = currentlist[0]->get_current();
    const auto& jCURR2 = currentlist[1]->get_current();
    const auto& qMOM1 = *(momlist[0]);
    const auto& qMOM2 = *(momlist[1]);

    assert(jCURR1.size()==1);

    momentumD<T, D> qMOM12(qMOM1 + qMOM2);

    Tcur k2_2k1_dot_j2(contract_vector_current_with_momentum_from_0_to(jCURR2, qMOM1 + qMOM12));

    result[0] = k2_2k1_dot_j2 * jCURR1.front();

    return result;
}

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> CombineCurrent_CurrentRule_sgs(const std::vector<momentumD<T, D> const*>& momlist,
                                                   const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {
    std::vector<Tcur> result(1);

    const auto& jCURR1 = currentlist[0]->get_current();
    const auto& jCURR2 = currentlist[1]->get_current();
    const auto& qMOM1 = *(momlist[0]);
    const auto& qMOM2 = *(momlist[1]);

    assert(jCURR2.size()==1);

    momentumD<T, D> qMOM12(qMOM1 + qMOM2);

    Tcur k1_2k2_dot_j1(contract_vector_current_with_momentum_from_0_to(jCURR1, qMOM2 + qMOM12));

    result[0] = -k1_2k2_dot_j1 * jCURR2.front();

    return result;
}

template <typename T, typename Tcur, size_t D, int factor,size_t g_pos, size_t s1_pos, size_t s2_pos >
std::vector<Tcur> CombineCurrent_CurrentRule_ggss_g(const std::vector<momentumD<T, D> const*>& momlist,
                                                   const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {
    static_assert(s1_pos != s2_pos,"Inconsistent indies!");
    static_assert(g_pos != s2_pos,"Inconsistent indies!");
    static_assert(g_pos != s1_pos,"Inconsistent indies!");

    std::vector<Tcur> result = currentlist[g_pos]->get_current();

    Tcur f = currentlist[s1_pos]->get_current().front()*currentlist[s2_pos]->get_current().front();
    _multeq_by(f,static_cast<T>(factor));

    for(auto& it: result) it=it*f;

    return result;
}

template <typename T, typename Tcur, size_t D, int factor, size_t s_pos, size_t g1_pos, size_t g2_pos >
std::vector<Tcur> CombineCurrent_CurrentRule_ggss_s(const std::vector<momentumD<T, D> const*>& momlist,
                                                   const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {
    static_assert(s_pos != g1_pos,"Inconsistent indies!");
    static_assert(s_pos != g2_pos,"Inconsistent indies!");
    static_assert(g1_pos != g2_pos,"Inconsistent indies!");

    Tcur result = currentlist[s_pos]->get_current().front();

    result = result * contract_vector_currents_from_0_to<D>(currentlist[g1_pos]->get_current(),currentlist[g2_pos]->get_current());

    _multeq_by(result,static_cast<T>(factor));
    return std::vector<Tcur>(1,result);
}

template <typename T, typename Tcur, size_t D, int factor>
std::vector<Tcur> CombineCurrent_CurrentRule_ssss(const std::vector<momentumD<T, D> const*>& momlist,
                                                   const std::vector<CurrentWorker_Template_Base<T, D> const*>& currentlist, const BuilderContainer<T>& bc) {

    Tcur result = currentlist[0]->get_current().front() * currentlist[1]->get_current().front() * currentlist[2]->get_current().front();

    _multeq_by(result,static_cast<T>(factor));

    return std::vector<Tcur>(1,result);
}

}
}
