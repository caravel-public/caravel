#include "Tensor.hpp"

namespace Caravel {
namespace BG {

/**
 * Vertex function that combines a scalar and a graviton into a scalar
 * |Phi_1> = G_{munu}V^{munu}|Phi_2>
 * <Phi_1| = <Phi_2|G_{munu}V^{munu}
 * isc -> index of scalar current
 * igr -> index of graviton current
 * Note: Caravel removes a factor of 2*I*kappa from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_SSG_S(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                    const size_t& midx, size_t igr, size_t isc) {
    // Incoming Graviton H^{munu} and scalar
    const Tensor<T, Ds, 2>& H = v_extended_t_to_v_t<T, Tcur>(currentlist[igr]->get_current());
    const std::vector<Tcur>& sc = currentlist[isc]->get_current();
    const T traceH = trace_rank2(H);

    // Scalar momenta (p^mu) and mass
    const auto& p1 = (*(momlist[isc]));
    const momD<T, Ds> p2 = -(p1 + (*(momlist[igr])));
    const auto& msq = bc.mass[midx] * bc.mass[midx];

    // Tensor: P^{munu} = (p1^mu*p2^nu + p2^mu*p1^mu)
    Tensor<T, Ds, 2> P(0);
    for (size_t mu = 0; mu < Ds; mu++)
        for (size_t nu = 0; nu < Ds; nu++) P[mu][nu] = p1[mu] * p2[nu] + p2[mu] * p1[nu];

    // Vertex: H^{munu}V_{munu} with V_{munu} from Phys. Rev. 67 084033
    Tcur v = (trace_rank2(index_contraction<0, 0>(H, P)) - traceH * (msq + (p1 * p2))) / T(4);

    // H^{munu}*V_{munu}|Phi> (extra factor of 2 to match KLT relation)
    std::vector<Tcur> out = sc;
    for (size_t ii = 0; ii < out.size(); ii++) out[ii] = out[ii] * T(2) * v;

    return out;
}

/**
 * Vertex function that combines two scalars into a graviton
 * G^{munu} = <Phi_1|V^{munu}|Phi_2>
 * Note: Caravel removes a factor of 2*I*kappa from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_SSG_G(const std::vector<momentumD<T, Ds> const*>& momlist,
                                    const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                    const size_t& midx) {
    // Incoming scalars
    const std::vector<T>& sc1 = v_extended_t_to_v_t<T,Tcur>(currentlist[0]->get_current());
    const std::vector<T>& sc2 = v_extended_t_to_v_t<T,Tcur>(currentlist[1]->get_current());

    // Scalar momenta and mass
    const auto& p1 = (*(momlist[0]));
    const auto& p2 = (*(momlist[1]));
    const auto& msq = bc.mass[midx] * bc.mass[midx];

    // Tensor: P^{munu} = (p1^mu*p2^nu + p2^mu*p1^mu)
    // and metric eta^{munu}
    Tensor<T, Ds, 2> P(0), eta(0);
    const std::vector<T> metric(P.get_metric_extended());
    for (size_t mu = 0; mu < Ds; mu++) {
        eta[mu][mu] = T(1) / metric[mu];
        for (size_t nu = 0; nu < Ds; nu++) P[mu][nu] = p1[mu] * p2[nu] + p2[mu] * p1[nu];
    }

    // Graviton tensor: V^{munu} from Phys. Rev. 67 084033
    Tensor<T, Ds, 2> V = (P - eta * (msq + (p1 * p2))) / T(4);

    // <Phi_1|Phi_2> * V^{munu}
    for (size_t mu = 0; mu < Ds; mu++) {
        for (size_t nu = 0; nu < Ds; nu++) V[mu][nu] *= T(2) * sc1[0] * sc2[0];
    }

    return v_t_to_v_extended_t<T, Tcur>(V.vectorize());
}

/**
 * Vertex function that combines two scalars and a graviton into a graviton
 * G^{munu} = <Phi_1|H^{albe}V^{munu}_{albe}|Phi_2>
 * isc1 -> index of first scalar current
 * isc2 -> index of second scalar current
 * igr  -> index of graviton current
 * Note: Caravel removes a factor of (2*I*kappa)^2 from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_SSGG_G(const std::vector<momentumD<T, Ds> const*>& momlist,
                                     const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                     const size_t& midx, size_t isc1, size_t isc2, size_t igr) {

    // Incoming scalars
    std::vector<T> S;
    S.push_back(get_e0((currentlist[isc1]->get_current())[0]));
    S.push_back(get_e0((currentlist[isc2]->get_current())[0]));

    // the input gravitons
    std::vector<Tensor<T, Ds, 2>> H;
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr]->get_current()));
    size_t p3 = 0; // graviton

    // incoming momenta
    std::vector<momD<T, Ds>> k;
    k.emplace_back(-(*(momlist[isc1])));
    k.emplace_back(-(*(momlist[isc2])));

    const auto& mSQ = bc.mass[midx] * bc.mass[midx];

    // metric
    Tensor<T, Ds, 2> Hout(0), g(0);
    const std::vector<T>& metric(Hout.get_metric_extended());

    for (size_t ii = 0; ii < Ds; ii++) { g[ii][ii] = T(1) / metric[ii]; }

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1}, {1, 0}};

    // Build Tensors
    std::vector<std::vector<Tensor<T, Ds, 1>>> Hk(2, std::vector<Tensor<T, Ds, 1>>(2, Tensor<T, Ds, 1>(0)));
    for (size_t jj = 0; jj < 2; jj++) Hk[p3][jj] = index_contraction<1>(k[jj], H[p3]);

    // Build scalars
    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array1D trH = {trace_rank2(H[p3])};
    array2D kk(2, array1D(2, T(0)));
    array3D kHk(2, array2D(2, array1D(2, T(0))));

    for (size_t ii = 0; ii < 2; ii++) {
        Tensor<T, Ds, 1> ki = Tensor<T, Ds, 1>(k[ii]);
        for (size_t jj = 0; jj < 2; jj++) {
            kk[ii][jj] = k[ii] * k[jj];
            kHk[ii][p3][jj] = ki * Hk[p3][jj];
        }
    }

    for (size_t a = 0; a < Ds; a++) {
        for (size_t b = a; b < Ds; b++) {
            T theentry = T(0);
            for (size_t ip = 0; ip < 2; ip++) {
                size_t p1 = perm[ip][0], p2 = perm[ip][1];

                theentry += (S[p1] * S[p2] * trH[p3] * k[p1][a] * k[p2][b]) / T(4) +
                            (S[p1] * S[p2] * ((mSQ + kk[p1][p2]) * H[p3][a][b] - T(2) * (k[p1][b] * Hk[p3][p2][a] + k[p1][a] * Hk[p3][p2][b]))) / T(4) +
                            g[a][b] * ((S[p1] * S[p2] * trH[p3] * (-mSQ - kk[p1][p2])) / T(8) + (S[p1] * S[p2] * kHk[p1][p3][p2]) / T(4));
            }
            Hout[a][b] = theentry * T(-1);
            Hout[b][a] = Hout[a][b];
        }
    }

    return v_t_to_v_extended_t<T, Tcur>(Hout.vectorize());
}
/**
 * Vertex function that combines two gravitons and a scalar into a scalar
 * |Phi_1>  = H2^{munu}H1^{albe}V_{munualbe}|Phi_2>
 * isc  -> index of scalar current
 * igr1 -> index of first graviton current
 * igr2 -> index of second graviton current
 * Note: Caravel removes a factor of (2*I*kappa)^2 from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_SSGG_S(const std::vector<momentumD<T, Ds> const*>& momlist,
                                     const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                     const size_t& midx, size_t isc, size_t igr1, size_t igr2) {
    // Incoming scalar and gravitons
    const std::vector<Tcur>& sc1 = currentlist[isc]->get_current();
    const Tensor<T, Ds, 2>& H1 = v_extended_t_to_v_t<T, Tcur>(currentlist[igr1]->get_current());
    const Tensor<T, Ds, 2>& H2 = v_extended_t_to_v_t<T, Tcur>(currentlist[igr2]->get_current());

    // momenta
    const auto& p1 = (*(momlist[isc]));
    const auto& p3 = (*(momlist[igr1]));
    const auto& p4 = (*(momlist[igr2]));
    const momD<T, Ds> p2 = -(p1 + p3 + p4);
    const auto& msq = bc.mass[midx] * bc.mass[midx];

    // Aux. Tensors and traces
    Tensor<T, Ds, 2> Q(0);
    for (size_t mu = 0; mu < Ds; mu++) {
        for (size_t nu = 0; nu < Ds; nu++) Q[mu][nu] = -(p1[mu] * p2[nu] + p2[mu] * p1[nu]);
    }

    const T trH1 = trace_rank2(H1);
    const T trH2 = trace_rank2(H2);
    const T trH12 = trace_rank2(index_contraction<1, 0>(H1, H2));
    const T trQH2 = trace_rank2(index_contraction<1, 0>(Q, H2));
    const T trH1Q = trace_rank2(index_contraction<1, 0>(H1, Q));
    const T trH1QH2 = trace_rank2(index_contraction<1, 0>(H1, index_contraction<1, 0>(Q, H2)));

    Tcur out = ((msq + (p1 * p2)) * (T(2) * trH12 - trH1 * trH2) + T(4) * trH1QH2 - trH1 * trQH2 - trH1Q * trH2) / T(4);

    // extra factor of 1/4 because caravel excludes (2*I*Kappa)^2 from vertex
    out = out * sc1[0] / T(4); //  (1/2)^2   from coupling
    //(extra factor of 4 to match KLT relation)
    out = out * T(-4);

    return std::vector<Tcur>{out};
}

/**
 * Vertex function that combines three gravitons and a scalar into a scalar
 * isc  -> index of scalar current
 * igr1 -> index of first graviton current
 * igr2 -> index of second graviton current
 * igr3 -> index of third graviton current
 * Note: Caravel removes a factor of (2*I*kappa)^3 from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_SSGGG_S(const std::vector<momentumD<T, Ds> const*>& momlist,
                                      const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                      const size_t& midx, size_t isc, size_t igr1, size_t igr2, size_t igr3) {

    // Incoming scalars
    std::vector<T> S;
    S.emplace_back(get_e0((currentlist[isc]->get_current())[0]));

    // the input gravitons
    std::vector<Tensor<T, Ds, 2>> H;
    H.emplace_back(std::vector<T>(Ds * Ds, T(0))); // necessary to get indices right
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr2]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr3]->get_current()));

    // incoming momenta
    std::vector<momD<T, Ds>> k;
    k.emplace_back(-(*(momlist[isc])));
    k.emplace_back(-(*(momlist[igr1])));
    k.emplace_back(-(*(momlist[igr2])));
    k.emplace_back(-(*(momlist[igr3])));
    const auto& mSQ = bc.mass[midx] * bc.mass[midx];

    // permutations
    static const std::vector<std::vector<size_t>> perm = {{0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1}};

    // build Tensors
    using T1array1D = std::vector<Tensor<T, Ds, 1>>;
    using T1array2D = std::vector<std::vector<Tensor<T, Ds, 1>>>;
    using T1array3D = std::vector<std::vector<std::vector<Tensor<T, Ds, 1>>>>;
    using T1array4D = std::vector<std::vector<std::vector<std::vector<Tensor<T, Ds, 1>>>>>;
    using T2array1D = std::vector<Tensor<T, Ds, 2>>;
    using T2array2D = std::vector<std::vector<Tensor<T, Ds, 2>>>;
    using T2array3D = std::vector<std::vector<std::vector<Tensor<T, Ds, 2>>>>;
    T2array2D HH(4, T2array1D(4, Tensor<T, Ds, 2>(0)));
    T2array3D HHH(4, T2array2D(4, T2array1D(4, Tensor<T, Ds, 2>(0))));
    T1array2D Hk(4, T1array1D(4, Tensor<T, Ds, 1>(0)));
    T1array3D HHk(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0))));
    T1array4D HHHk(4, T1array3D(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0)))));

    for (size_t ip = 0; ip < 6; ip++) {
        size_t p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];
        HH[p2][p3] = index_contraction<1, 0>(H[p2], H[p3]);
        HHH[p2][p3][p4] = index_contraction<1, 0>(HH[p2][p3], H[p4]);
        for (size_t ii = 0; ii < 4; ii++) {
            Hk[p2][ii] = index_contraction<1>(k[ii], H[p2]);
            HHk[p2][p3][ii] = index_contraction<1>(k[ii], HH[p2][p3]);
            HHHk[p2][p3][p4][ii] = index_contraction<1>(k[ii], HHH[p2][p3][p4]);
        }
    }

    // build Scalars
    using array5D = std::vector<std::vector<std::vector<std::vector<std::vector<T>>>>>;
    using array4D = std::vector<std::vector<std::vector<std::vector<T>>>>;
    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array2D = std::vector<std::vector<T>>;
    using array1D = std::vector<T>;
    array5D kHHHk(4, array4D(4, array3D(4, array2D(4, array1D(4, T(0))))));
    array4D kHHk(4, array3D(4, array2D(4, array1D(4, T(0)))));
    array3D kHk(4, array2D(4, array1D(4, T(0))));
    array2D kk(4, array1D(4, T(0)));
    array3D trHHH(4, array2D(4, array1D(4, T(0))));
    array2D trHH(4, array1D(4, T(0)));
    array1D trH(4, T(0));

    for (size_t ip = 0; ip < 6; ip++) {
        size_t p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];
        trH[p2] = trace_rank2(H[p2]);
        trHH[p2][p3] = trace_rank2(HH[p2][p3]);
        trHHH[p2][p3][p4] = trace_rank2(HHH[p2][p3][p4]);
        for (size_t ii = 0; ii < 4; ii++) {
            Tensor<T, Ds, 1> ki = Tensor<T, Ds, 1>(k[ii]);
            for (size_t jj = 0; jj < 4; jj++) {
                kk[ii][jj] = k[ii] * k[jj];
                kHk[ii][p2][jj] = ki * Hk[p2][jj];
                kHHk[ii][p2][p3][jj] = ki * HHk[p2][p3][jj];
                kHHHk[ii][p2][p3][p4][jj] = ki * HHHk[p2][p3][p4][jj];
            }
        }
    }

    T out = T(0);
    for (size_t ip = 0; ip < 6; ip++) {
        size_t p1 = perm[ip][0], p2 = perm[ip][1], p3 = perm[ip][2], p4 = perm[ip][3];

        out += (S[p1] * T(3) * trHH[p4][p3] * kHk[p1][p2][p1]) / T(2) + S[p1] * T(3) * trHH[p2][p4] * kHk[p1][p3][p4] +
               (S[p1] * T(3) * trHH[p3][p2] * kHk[p1][p4][p4]) / T(2) +
               trH[p4] * ((S[p1] * T(3) * (mSQ - kk[p1][p1] - kk[p1][p4]) * trHH[p3][p2]) / T(4) + S[p1] * T(3) * kHHk[p1][p2][p3][p4]) +
               trH[p2] * (-((S[p1] * T(3) * trH[p4] * kHk[p1][p3][p4]) / T(2)) +
                          trH[p3] * ((S[p1] * trH[p4] * (-mSQ + kk[p1][p1] + T(3) * kk[p1][p4])) / T(8) - (S[p1] * T(3) * kHk[p1][p4][p4]) / T(4)) +
                          S[p1] * T(3) * kHHk[p1][p3][p4][p4]) +
               trH[p3] * (-((S[p1] * T(3) * kk[p1][p4] * trHH[p2][p4]) / T(2)) - (S[p1] * T(3) * trH[p4] * kHk[p1][p2][p1]) / T(4) +
                          (S[p1] * T(3) * (T(2) * kHHk[p1][p2][p4][p1] + kHHk[p1][p4][p2][p4] + kHHk[p4][p2][p4][p1])) / T(2)) +
               S[p1] * ((-mSQ + kk[p1][p1] + T(3) * kk[p1][p4]) * trHHH[p2][p3][p4] -
                        T(3) * (T(2) * kHHHk[p1][p2][p3][p4][p1] + T(2) * kHHHk[p1][p2][p3][p4][p4] + kHHHk[p1][p3][p4][p2][p4] + kHHHk[p4][p2][p3][p4][p1] +
                                kHHHk[p4][p2][p4][p3][p1] + kHHHk[p4][p3][p2][p4][p1]));
    }

    out = out / T(6);

    return std::vector<Tcur>{out};
}

/**
 * Vertex function that combines two gravitons and two scalar into a graviton
 * isc1  -> index of first scalar current
 * isc2  -> index of second scalar current
 * igr1 -> index of first graviton current
 * igr2 -> index of second graviton current
 * Note: Caravel removes a factor of (2*I*kappa)^3 from the Vertex rule
 **/
template <typename T, typename Tcur, size_t Ds>
std::vector<Tcur> CombineRule_SSGGG_G(const std::vector<momentumD<T, Ds> const*>& momlist,
                                      const std::vector<CurrentWorker_Template_Base<T, Ds> const*>& currentlist, const BuilderContainer<T>& bc,
                                      const size_t& midx, size_t isc1, size_t isc2, size_t igr1, size_t igr2) {

    // Incoming scalars
    std::vector<T> S;
    S.emplace_back(get_e0((currentlist[isc1]->get_current())[0]));
    S.emplace_back(get_e0((currentlist[isc2]->get_current())[0]));

    // the input gravitons
    std::vector<Tensor<T, Ds, 2>> H;
    H.emplace_back(std::vector<T>(Ds * Ds, T(0))); // necessary to get indices right
    H.emplace_back(std::vector<T>(Ds * Ds, T(0))); // necessary to get indices right
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr1]->get_current()));
    H.emplace_back(v_extended_t_to_v_t<T, Tcur>(currentlist[igr2]->get_current()));

    // incoming momenta
    std::vector<momD<T, Ds>> k;
    k.emplace_back(-(*(momlist[isc1])));
    k.emplace_back(-(*(momlist[isc2])));
    k.emplace_back(-(*(momlist[igr1])));
    k.emplace_back(-(*(momlist[igr2])));
    const auto& mSQ = bc.mass[midx] * bc.mass[midx];

    static const std::vector<std::vector<size_t>> perm = {{2, 3}, {3, 2}};
    static const std::vector<std::vector<size_t>> permS = {{0, 1}, {1, 0}};

    // build Tensors
    using T1array1D = std::vector<Tensor<T, Ds, 1>>;
    using T1array2D = std::vector<std::vector<Tensor<T, Ds, 1>>>;
    using T1array3D = std::vector<std::vector<std::vector<Tensor<T, Ds, 1>>>>;
    using T2array1D = std::vector<Tensor<T, Ds, 2>>;
    using T2array2D = std::vector<std::vector<Tensor<T, Ds, 2>>>;
    T2array2D HH(4, T2array1D(4, Tensor<T, Ds, 2>(0)));
    T1array2D Hk(4, T1array1D(4, Tensor<T, Ds, 1>(0)));
    T1array3D HHk(4, T1array2D(4, T1array1D(4, Tensor<T, Ds, 1>(0))));

    for (size_t ip = 0; ip < 2; ip++) {
        size_t p3 = perm[ip][0], p4 = perm[ip][1];
        HH[p3][p4] = index_contraction<1, 0>(H[p3], H[p4]);
        for (size_t ii = 0; ii < 4; ii++) {
            Hk[p3][ii] = index_contraction<1>(k[ii], H[p3]);
            HHk[p3][p4][ii] = index_contraction<1>(k[ii], HH[p3][p4]);
        }
    }

    // build scalars
    using array1D = std::vector<T>;
    using array2D = std::vector<std::vector<T>>;
    using array3D = std::vector<std::vector<std::vector<T>>>;
    using array4D = std::vector<std::vector<std::vector<std::vector<T>>>>;
    array1D trH(4, T(0));
    array2D trHH(4, array1D(4, T(0)));
    array2D kk(4, array1D(4, T(0)));
    array3D kHk(4, array2D(4, array1D(4, T(0))));
    array4D kHHk(4, array3D(4, array2D(4, array1D(4, T(0)))));

    for (size_t ip = 0; ip < 2; ip++) {
        size_t p3 = perm[ip][0], p4 = perm[ip][1];
        trH[p3] = trace_rank2(H[p3]);
        trHH[p3][p4] = trace_rank2(HH[p3][p4]);
        for (size_t ii = 0; ii < 4; ii++) {
            Tensor<T, Ds, 1> ki = Tensor<T, Ds, 1>(k[ii]);
            for (size_t jj = 0; jj < 4; jj++) {
                kHk[ii][p3][jj] = ki * Hk[p3][jj];
                kHHk[ii][p3][p4][jj] = ki * HHk[p3][p4][jj];
                kk[ii][jj] = k[ii] * k[jj];
            }
        }
    }

    Tensor<T, Ds, 2> Hout(0), g(0);
    const std::vector<T>& metric(g.get_metric_extended());
    for (size_t ii = 0; ii < Ds; ii++) g[ii][ii] = T(1) / metric[ii];

    for (size_t a = 0; a < Ds; a++) {
        for (size_t b = a; b < Ds; b++) {
            T theentry = T(0);
            for (size_t ip = 0; ip < 2; ip++) {
                for (size_t ipS = 0; ipS < 2; ipS++) {
                    size_t p1 = permS[ipS][0], p2 = permS[ipS][1], p3 = perm[ip][0], p4 = perm[ip][1];

                    theentry +=
                        (S[p1] * S[p2] * trH[p3] * trH[p4] * k[p1][a] * k[p2][b]) / T(8) - (S[p1] * S[p2] * k[p1][a] * k[p2][b] * trHH[p4][p3]) / T(4) +
                        (S[p1] * S[p2] * trH[p4] * ((mSQ + kk[p1][p2]) * H[p3][a][b] - T(2) * (k[p1][b] * Hk[p3][p2][a] + k[p1][a] * Hk[p3][p2][b]))) / T(4) +
                        S[p1] * S[p2] *
                            (Hk[p3][p1][a] * Hk[p4][p2][b] - (H[p3][a][b] * kHk[p1][p4][p2]) / T(2) - ((mSQ + kk[p1][p2]) * HH[p3][p4][a][b]) / T(2) +
                             k[p1][b] * HHk[p3][p4][p2][a] + k[p1][a] * HHk[p3][p4][p2][b]) +
                        g[a][b] *
                            ((S[p1] * S[p2] * trH[p3] * trH[p4] * (-mSQ - kk[p1][p2])) / T(16) + (S[p1] * S[p2] * (mSQ + kk[p1][p2]) * trHH[p4][p3]) / T(8) +
                             (S[p1] * S[p2] * trH[p4] * kHk[p1][p3][p2]) / T(4) - (S[p1] * S[p2] * kHHk[p1][p3][p4][p2]) / T(2));
                }
            }
            Hout[a][b] = theentry / T(2);
            Hout[b][a] = Hout[a][b];
        }
    }

    return v_t_to_v_extended_t<T, Tcur>(Hout.vectorize());
}

#define _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                     \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_SSG_S<T, Current_Type_Selector<T>::Tcur, D>(                                          \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t);                                                                                                                                       \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_SSG_G<T, Current_Type_Selector<T>::Tcur, D>(                                          \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&);

#define _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                    \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_SSGG_G<T, Current_Type_Selector<T>::Tcur, D>(                                         \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t, size_t);                                                                                                                               \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_SSGG_S<T, Current_Type_Selector<T>::Tcur, D>(                                         \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t, size_t);

#define _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS_D(KEY, T, D)                                                                                                   \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_SSGGG_S<T, Current_Type_Selector<T>::Tcur, D>(                                        \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t, size_t, size_t);                                                                                                                       \
    KEY template std::vector<Current_Type_Selector<T>::Tcur> CombineRule_SSGGG_G<T, Current_Type_Selector<T>::Tcur, D>(                                        \
        const std::vector<momentumD<T, D> const*>&, const std::vector<CurrentWorker_Template_Base<T, D> const*>&, const BuilderContainer<T>&, const size_t&,   \
        size_t, size_t, size_t, size_t);

#define _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                          \
    _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                         \
    _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                         \
    _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                         \
    _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                         \
    _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                         \
    _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                         \
    _INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#define _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                          \
    _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                         \
    _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                         \
    _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                         \
    _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                         \
    _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                         \
    _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                         \
    _INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#define _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(KEY, T)                                                                                                          \
    _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS_D(KEY, T, 4)                                                                                                         \
    _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS_D(KEY, T, 5)                                                                                                         \
    _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS_D(KEY, T, 6)                                                                                                         \
    _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS_D(KEY, T, 7)                                                                                                         \
    _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS_D(KEY, T, 8)                                                                                                         \
    _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS_D(KEY, T, 9)                                                                                                         \
    _INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS_D(KEY, T, 10)

#ifdef INCLUDE_EH_GRAVITY
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, R)
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, C)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, R)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, C)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, R)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, C)

#ifdef HIGH_PRECISON
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, RHP)
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, CHP)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, RHP)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, CHP)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, RHP)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, CHP)
#endif

#ifdef VERY_HIGH_PRECISON
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, RVHP)
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, CVHP)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, RVHP)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, CVHP)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, RVHP)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, CVHP)
#endif

#ifdef USE_GMP
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, RGMP)
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, CGMP)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, RGMP)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, CGMP)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, RGMP)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, CGMP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, F32)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, F32)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_EH_SSG_GRAVITY_FUNCTIONS(extern, BigRat)
_INSTANTIATE_EH_SSGG_GRAVITY_FUNCTIONS(extern, BigRat)
_INSTANTIATE_EH_SSGGG_GRAVITY_FUNCTIONS(extern, BigRat)
#endif
#endif

#endif
} // namespace BG
} // namespace Caravel
