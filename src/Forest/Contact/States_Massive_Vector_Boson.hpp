#include "Core/Debug.h"
#include "Core/momD.h"
#include "Core/momD_conf.h"
#include "Core/type_traits_extra.h"

namespace Caravel {
namespace Massive_vector_states {
/*******************************************************************************
 * EXTERNAL POLARIZATION VECTORS
 ******************************************************************************/
/**
 * Longitudinal Polarization vector for massive Vector boson
 * k - massive momentum (confined to 4D)
 * r - massless reference momenta (4D)
 * mass - k^2 = mass^2
 */
template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_L(const momD<T, Ds>& k, const momD<T, Ds>& r, const T& mass) {
    static_assert(Ds >= 4, "Massive Polarization vectors only implemented for Ds >= 4!");

    // Lightcone decomposition
    const momD<T, Ds> n = mass * mass / (T(2) * T(k * r)) * r;
    const momD<T, Ds> kf = k - n;

    // Longitudinal polarization
    std::vector<T> pL = (n - kf).get_vector();
    std::vector<Tcur> out(Ds, Tcur(0));
    for (size_t ii = 0; ii < pL.size(); ii++) out[ii] = pL[ii] / mass;

    return out;
}

/**
 * Positive Polarization vectors for massive vector bosons
 * k - massive momentum (confined to 4D)
 * r - massless reference momentum (4D)
 * mass - k^2 = mass^2
 */
template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_p(const momD<T, Ds>& k, const momD<T, Ds>& r, const T& mass) {
    static_assert(Ds >= 4, "Massive Polarization vectors only implemented for Ds >= 4!");

    // Lightcone decomposition
    const momD<T, Ds> q = mass * mass / (T(2) * T(k * r)) * r;
    const momD<T, Ds> kf = k - q;
    return externalWF_vectorboson_p_momenta<T, Tcur, Ds>(kf, q);
}

/**
 * Negative Polarization vectors for massive vector bosons
 * k - massive momentum (confined to 4D)
 * r - massless reference momentum (4D)
 * mass - k^2 = mass^2
 */
template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_m(const momD<T, Ds>& k, const momD<T, Ds>& r, const T& mass) {
    static_assert(Ds >= 4, "Massive Polarization vectors only implemented for Ds >= 4!");

    // Lightcone decomposition
    const momD<T, Ds> q = mass * mass / (T(2) * T(k * r)) * r;
    const momD<T, Ds> kf = k - q;
    return externalWF_vectorboson_m_momenta<T, Tcur, Ds>(kf, q);
}

// Conjugated states
template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_conj_L(const momD<T, Ds>& k, const momD<T, Ds>& r, const T& mass) {
    static_assert(Ds >= 4, "Massive Polarization vectors only implemented for Ds >= 4!");
    return externalWF_L<T, Tcur, Ds>(k, r, mass);
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_conj_m(const momD<T, Ds>& k, const momD<T, Ds>& r, const T& mass) {
    static_assert(Ds >= 4, "Massive Polarization vectors only implemented for Ds >= 4!");
    return externalWF_p<T, Tcur, Ds>(k, r, mass);
}

template <typename T, typename Tcur, size_t Ds> std::vector<Tcur> externalWF_conj_p(const momD<T, Ds>& k, const momD<T, Ds>& r, const T& mass) {
    static_assert(Ds >= 4, "Massive Polarization vectors only implemented for Ds >= 4!");
    return externalWF_m<T, Tcur, Ds>(k, r, mass);
}

/**
 * 'Extra' vector polarization state for a massive vector boson w.r.t. direct extension
 * of the (D-2) massless polarization states.
 *
 * Normalization factor pushed to the 'extracD' state
 */
template <typename T, typename Tcur, size_t D>
std::vector<Tcur> WF_massive_vectorboson_extraD(const momD_conf<T, D>& momconf, const size_t& lmom, const size_t& refmom) {
    const momD<T,D>& l(momconf.p(lmom));
    // mu^2
    auto mmusqr = compute_Dsm4_prod(l,l);
    // (l4D)^2
    auto mm4dsqr = l*l - mmusqr;

    std::vector<T> result(D);
    for(size_t ii = 0; ii < 4; ++ii) result[ii] = T(-1)/mm4dsqr * l.pi(ii);
    for(size_t ii = 4; ii < D; ++ii) result[ii] = T(1)/mmusqr * l.pi(ii);

    return to_extended_t<std::vector<Tcur>,D>(result);
}

template <typename T, typename Tcur, size_t D>
std::vector<Tcur> WF_massive_vectorboson_extracD(const momD_conf<T, D>& momconf, const size_t& lmom, const size_t& refmom) {
    const momD<T,D>& l(momconf.p(lmom));
    // mu^2
    auto mmusqr = compute_Dsm4_prod(l,l);
    // (l4D)^2
    auto mm4dsqr = l*l - mmusqr;

    std::vector<T> result(D);
    for(size_t ii = 0; ii < 4; ++ii) result[ii] = T(-1)/mm4dsqr * l.pi(ii);
    for(size_t ii = 4; ii < D; ++ii) result[ii] = T(1)/mmusqr * l.pi(ii);

    auto normalize = mmusqr * mm4dsqr / (mmusqr + mm4dsqr);
    for(size_t ii = 0; ii < D; ++ii) result[ii] *= normalize;

    return to_extended_t<std::vector<Tcur>,D>(result);
}

} // namespace Massive_vector_states
} // namespace Caravel
