/**
 * @file Coupling.cpp
 *
 * @date 9.6.2017
 *
 * @brief Specializations of Coupling::get_value()
 *
*/

#include <string>
#include <ostream>
#include <complex>

#include "Coupling.h"
#include "Core/typedefs.h"
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif

#ifdef INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

namespace Caravel {

template <> C Coupling::get_value<C>() const { return C(value_num_r,value_num_i)/C(value_den_r,value_den_i); }
#ifdef HIGH_PRECISION
template <> CHP Coupling::get_value<CHP>() const { return CHP(value_num_r,value_num_i)/CHP(value_den_r,value_den_i); }
#endif
#ifdef VERY_HIGH_PRECISION
template <> CVHP Coupling::get_value<CVHP>() const { return CVHP(value_num_r,value_num_i)/CVHP(value_den_r,value_den_i); }
#endif
#if INSTANTIATE_GMP
template <> CGMP Coupling::get_value<CGMP>() const { return CGMP(value_num_r,value_num_i)/CGMP(value_den_r,value_den_i); }
#endif

std::ostream& operator<<(std::ostream& s, const Coupling& c){
    s << c.get_name() << ":" << "I^" << c.get_powers_of_I();
    return s;
}

}
