namespace Caravel {

template <class T, size_t D, size_t Ds> void Builder_fixed_Ds<T, D, Ds, typename std::enable_if_t<(Ds >= D)>>::normalization_report() const {
    // report is only necessary for real evaluations (complex always include all factors)
    if constexpr (is_complex<T>::value) return;

    DEBUG(
        // trees
        for (auto& n
             : tree_normalization_info) {
            auto& factors = n.second.get_imaginary_normalization_factors();
            if (factors.size() > 0) {
                std::cout << "Tree # " << n.first << " contains imaginary factor(s) not included in this real evaluation: " << factors << std::endl;
            }
        }

        // cuts
        for (auto& vn
             : cut_normalization_info) {
            size_t counter(0);
            for (auto& n : vn.second) {
                ++counter;
                auto& factors = n.get_imaginary_normalization_factors();
                if (factors.size() > 0) {
                    std::cout << "Cut # " << vn.first << "(" << counter << ") contains imaginary factor(s) not included in this real evaluation: " << factors
                              << std::endl;
                }
            }
        });
}
}
