#include "Forest/Forest.h"
#include "Core/typedefs.h"

namespace Caravel {

template <class T,size_t D> Builder_Ds_base<T,D>::~Builder_Ds_base(){ }

// deprecated constructor
template <class T,size_t D> Builder<T,D>::Builder(Forest* forest,const momD_conf<T,4>& emom): Builder(forest,emom.size()) {
    set_p(emom);
}

template <class T,size_t D> Builder<T,D>::Builder(Forest* forest,size_t e_mult): external_momenta(e_mult) {
	dimensions=forest->get_added_dimensions();
	std::sort(dimensions.begin(),dimensions.end());

	// check which Builder's we need for each dimension
	std::vector<bool> builder_ds_needed;
	//size_t n_of_builder_ds(0);
	for(auto& dim:dimensions){
//std::cout<<"Dim: "<<dim<<" size: "<<forest->get_total_number_of_treelike_currents(dim)<<std::endl;
		if(forest->get_total_number_of_treelike_currents(dim)>0){
			//n_of_builder_ds++;
			builder_ds_needed.push_back(true);
		}
		else
			builder_ds_needed.push_back(false);
	}

	// to store the default builder associated to the given cut
	std::unordered_map<int,size_t> cut_default_builder;

//std::cout<<"Builder REQUESTS: "<<n_of_builder_ds<<std::endl;
	size_t counter_added_builders(0);
	for(size_t ii=0;ii<dimensions.size();ii++){
	  if(builder_ds_needed[ii]){
		// we will map all calls to the last dim builder -- generic case will be coded in later
		bool got_builder(false);
		switch(dimensions[ii]){
			case 4: {
					if constexpr (D==4){
						builder_ds_container.push_back(new Builder_fixed_Ds<T,D,4>(forest,e_mult));
						counter_added_builders++;
						got_builder=true;
					}
					break;
				}
			case 5: {
					if constexpr (D==5){
						builder_ds_container.push_back(new Builder_fixed_Ds<T,D,5>(forest,e_mult));
						counter_added_builders++;
						got_builder=true;
					}
					break;
				}
			case 6: {
					if constexpr (D==5||D==6){
						builder_ds_container.push_back(new Builder_fixed_Ds<T,D,6>(forest,e_mult));
						counter_added_builders++;
						got_builder=true;
					}
					break;
				}
			case 7: {
					if constexpr (D==5||D==6){
						builder_ds_container.push_back(new Builder_fixed_Ds<T,D,7>(forest,e_mult));
						counter_added_builders++;
						got_builder=true;
					}
					break;
				}
			case 8: {
					if constexpr (D==5||D==6){
						builder_ds_container.push_back(new Builder_fixed_Ds<T,D,8>(forest,e_mult));
						counter_added_builders++;
						got_builder=true;
					}
					break;
				}
			case 9: {
					if constexpr (D==5||D==6){
						builder_ds_container.push_back(new Builder_fixed_Ds<T,D,9>(forest,e_mult));
						counter_added_builders++;
						got_builder=true;
          				}
					break;
				}
			case 10: {
					if constexpr (D==5||D==6){
						builder_ds_container.push_back(new Builder_fixed_Ds<T,D,10>(forest,e_mult));
						counter_added_builders++;
						got_builder=true;
					}
					break;
				}
/*
			case 11: {
  					if constexpr (D==5||D==6){
  						builder_ds_container.push_back(new Builder_fixed_Ds<T,D,11>(forest,e_mult));
  						counter_added_builders++;
  						got_builder=true;
  					}
  					break;
  				}
*/
			//case 12: {
  			// 		if constexpr (D==5||D==6){
  			// 			builder_ds_container.push_back(new Builder_fixed_Ds<T,D,12>(forest,e_mult));
  			// 			counter_added_builders++;
  			// 			got_builder=true;
  			// 		}
  			// 		break;
  			// 	}
			default: {
					break;
				}
		}
		if(!got_builder){
			std::cout<<"ERROR!! Builder<T,D> can't deal with Ds= "<<dimensions[ii]<<" (all dims: "<<dimensions<<")"<<std::endl;
			return;
		}

		// Check which cuts should be handled the dimension dimensions[ii]
		for(size_t jj=0;jj<forest->get_size_of_cut_register();jj++){
			int cut_index(forest->get_current_index_for_cut(int(jj+1)));
			Current* Top_cut_current(std::get<1>(forest->all_processes[cut_index-1]));
			const std::vector<std::vector<Current*>> all_currents_Ds(forest->get_all_currents(dimensions[ii]));
			if(all_currents_Ds.size()>0){
				// is this top current handled in dimensions[ii]?
				if(std::find(all_currents_Ds.back().begin(),all_currents_Ds.back().end(),Top_cut_current)!=all_currents_Ds.back().end()){
					if(cut_builderDs_map_for_compute.find(jj+1)==cut_builderDs_map_for_compute.end()){
						DEBUG_MESSAGE("Assigned the (cut_i,Ds,builder): (",jj+1,",",dimensions[ii],",",counter_added_builders-1,")")
						cut_builderDs_map_for_compute[jj+1]=std::vector<size_t>({counter_added_builders-1});
					}
					else{
						std::vector<size_t>& cut_builders(cut_builderDs_map_for_compute.at(jj+1));
						cut_builders.push_back(counter_added_builders-1);
						DEBUG_MESSAGE("Added a dimension for (cut_i,Ds,builder): (",jj+1,",",dimensions[ii],",",counter_added_builders-1,")")
					}
					// complete info for calls
					if(cut_builderDs_map_for_get.find(std::make_pair(jj+1,dimensions[ii]))==cut_builderDs_map_for_get.end()){
						DEBUG_MESSAGE("For getter assigned the (pair<cut_i,Ds>,builder): (pair<",jj+1,",",dimensions[ii],">,",counter_added_builders-1,")")
						cut_builderDs_map_for_get[std::make_pair(jj+1,dimensions[ii])]=counter_added_builders-1;
						// because dimensions[ii] increases monotonically, in the end this will store the default builder for Ds=0
						cut_default_builder[jj+1]=counter_added_builders-1;
#ifndef BUILDERS_FOR_EACH_Ds
						// Make sure that lower-dimensional cases are covered when BUILDERS_FOR_EACH_Ds not defined
						for(size_t kk=0;kk<ii;kk++){
							// sanity check
							if(cut_builderDs_map_for_get.find(std::make_pair(jj+1,dimensions[kk]))==cut_builderDs_map_for_get.end()){
								cut_builderDs_map_for_get[std::make_pair(jj+1,dimensions[kk])]=counter_added_builders-1;
							}
							else{
								DEBUG_MESSAGE("CAREFUL: with BUILDERS_FOR_EACH_Ds not defined found a lower-dim case covered! Existing: pair<",
									jj+1,",",dimensions[kk],"> and case: pair<",jj+1,",",dimensions[ii],">, this is for top current: ",
									Top_cut_current->get_name())
							}
						}
#endif
					}
					else{
						std::cout<<"ERROR: For getter tried to reassign the (pair<cut_i,Ds>,builder): (pair<"<<jj+1<<","<<dimensions[ii]<<">,"
							<<counter_added_builders-1<<") with the existing builder: "<<cut_builderDs_map_for_get.at(std::make_pair(jj+1,dimensions[ii]))<<std::endl;
					}
				}
			}
		}

	  }
	}

        for(const auto& it : cut_builderDs_map_for_get){
            cut_dimensions[it.first.first].insert(it.first.second);
        }

	// take care of defining default (Ds=0) builder
	for(size_t jj=0;jj<forest->get_size_of_cut_register();jj++){
		cut_builderDs_map_for_get[std::make_pair(jj+1,0)]=cut_default_builder[jj+1];
	}

	// check if something was produced!
	if(counter_added_builders==0){
		std::cout<<"ERROR, Builder constructor failed to produce any tree/cut!"<<std::endl;
	}

        // make sure bcontainer is initialized
        update_builder_container();
}

template <class T, size_t D> void Builder<T, D>::compute() {
    // trees need to be computed for all builders
    for (auto& b : builder_ds_container) {
        // we perform the combined operations in Builder compute():
        b->update_builder_container();
        b->compute();
    }
}

template <class T,size_t D> void Builder<T,D>::contract(){
	// we contract() only for min dim builder
	builder_ds_container[0]->contract();
}

template <class T,size_t D> void Builder<T,D>::compute_cut(int i){
	for(auto& jj:cut_builderDs_map_for_compute.at(i))
		// map to correct Builder(s)
		builder_ds_container[jj]->compute_cut(i);
}

template <class T,size_t D> void Builder<T,D>::contract_cut(int i){
	for(auto& jj:cut_builderDs_map_for_compute.at(i))
		// map to correct Builder(s)
		builder_ds_container[jj]->contract_cut(i);
}

template <class T, size_t D> std::set<size_t> Builder<T, D>::get_cut_dimensions(int i) const { return cut_dimensions.at(i); }

template <class T,size_t D> T Builder<T,D>::get_tree(int i) const {
	// we use always the min dim for trees
	return builder_ds_container[0]->get_tree(i);
}

template <class T,size_t D> T Builder<T,D>::get_cut(int cut_i,size_t dim) const {
#ifdef DEBUG_ON
	// sanity check
	if(cut_builderDs_map_for_get.find(std::make_pair(cut_i,dim))==cut_builderDs_map_for_get.end()){
		std::cout<<"ERROR: cut_"<<cut_i<<" called for Ds: "<<dim<<" which has not been processed! Builder::get_cut(.) returned 0"<<std::endl;
		return T(0);
	}
#endif
	// we use always the last dim for trees
	return builder_ds_container[cut_builderDs_map_for_get.at(std::make_pair(cut_i,dim))]->get_cut(cut_i,dim);
}

template <class T,size_t D> std::vector<T> Builder<T,D>::get_cut(int cut_i) const {
    std::vector<T> results;
    for(const auto& dim : cut_dimensions.at(cut_i)){
        results.push_back(builder_ds_container[cut_builderDs_map_for_get.at(std::make_pair(cut_i,dim))]->get_cut(cut_i,dim));
    }
    return results;
}

template <class T,size_t D> const momD_conf<T,D>& Builder<T,D>::get_phys_momD_conf() const {
	return builder_ds_container[0]->get_phys_momD_conf();
}

template <class T,size_t D> const momD_conf<T,D>& Builder<T,D>::get_loop_momD_conf(
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		int cutindex
#endif
	) const {
	return builder_ds_container[0]->get_loop_momD_conf(
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
		cutindex
#endif
	);
}

template <class T,size_t D> size_t Builder<T,D>::get_phys_size() const {
	return builder_ds_container[0]->get_phys_size();
}

template <class T,size_t D> void Builder<T,D>::set_p(const momD<T,4>& m,size_t n){
	if(n>0&&n<=external_momenta.n())
		external_momenta.set_p(m,n);
	for(auto& b:builder_ds_container){
		b->set_p(m,n);
	}
}

template<class T, size_t D> void Builder<T,D>::set_p(const momD_conf<T,4>& conf){
    for (size_t i = 1; i <= external_momenta.n(); i++){
      set_p(conf.p(i), i);
    }
}

template <class T,size_t D> void Builder<T,D>::set_loop(const momD<T,D>& m,size_t n
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
			, int cutindex
#endif
		){
	for(auto& b:builder_ds_container){
		b->set_loop(m,n
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, cutindex
#endif
			);
	}
}

template <class T,size_t D> void Builder<T,D>::set_loop(const OnShellPoint<T,D>& osp
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
			, int cutindex
#endif
		){
	for(auto& b:builder_ds_container){
		b->set_loop(osp
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
				, cutindex
#endif
			);
	}
}

template <class T,size_t D> void Builder<T,D>::insert_extra_4D_mom(const momD<T,4>& m,size_t n){
	for(auto& b:builder_ds_container){
		b->insert_extra_4D_mom(m,n);
	}
}

template <class T, size_t D> void Builder<T, D>::update_builder_container() {
    for (auto& b : builder_ds_container) b->update_builder_container();
}

template <class T, size_t D> std::vector<size_t> Builder<T,D>::get_state_dimensions() const{
  return dimensions;
}

template <class T,size_t D> void Builder<T,D>::update_normalizations(){
	for(auto& b:builder_ds_container){
		b->update_normalizations();
	}
}

template <class T,size_t D> Builder<T,D>::~Builder(){
	DEBUG_MESSAGE("Deleting Builder");
	for(auto& b:builder_ds_container){
		delete b;
	}
	builder_ds_container.clear();
}


// CurrentContainer

template <class T> CurrentContainer<T>::CurrentContainer(): size(0), spin(ParticleStatistics::undefined) {}

template <class T> CurrentContainer<T>::CurrentContainer(ParticleStatistics in_spin,ParticleFlavor f, unsigned Dim): spin(in_spin) {
    // What follows is a table for the dimensions of the containers employed.
    // So far, only spin and dimension considered! Later we can specify information related to the state of the (external) current

    using ps = ParticleStatistics;

    switch(spin){
        case ps::scalar:
            size =1;
            break;
        case ps::fermion:
        case ps::antifermion:
            if(Dim%2!=0||Dim==0){
                _WARNING("Tried to initialize (anti)fermion CurrentContainer in Dim= ",Dim," which isn't even! Produced empty container!");
                size=0;
            }
            else{
                //! This is for Weyl spinors! ParticleStatistics is not enough to distinguish from Dirac spinors, should be done later.
		// Dim==4 condition to make it consistent with corresponding choice in class fermion_states (in BG_Fermion.hpp) and in get_combine_rule (in Model.hpp)
                if (!is_dirac(f))
                    size = std::pow(2, Dim / 2 - 1);
                else
                    size = std::pow(2, Dim / 2);
            }
            break;
        case ps::vector:
            size = Dim;
            break;
	case ps::tensor:
	    // maximum size (but for example in the case of T auxiliary tensors for QCD, only Dim*(Dim-1)/2 is necessary
	    size = Dim*Dim;
	    break;
	case ps::cubictensor:
	    size = Dim*Dim*Dim;
	    break;
        default:
            _WARNING("Careful! Initializing current with undefined spin-statistics! ",spin);
            size = 0;
    }

    container=std::vector<T>(size,T(0));
    container.shrink_to_fit();
}

template <class T> void CurrentContainer<T>::set(const std::vector<T>& in) {
#ifdef DEBUG_ON
    // sanity check (only in debug mode)
    if(in.size()!=size){
        _WARNING_R("In CurrentContainer::set(in): vector in has size: ",in.size()," while *this container has size: ",size);
    }
    //std::copy(in.begin(),in.end(),container.begin());
#endif
    //std::copy(in.begin(),in.begin()+size,container.begin());
    container = in;
}

template <class T> void CurrentContainer<T>::add(const std::vector<T>& in) {
#ifdef DEBUG_ON
    // sanity check (only in debug mode)
    if(in.size()!=size){
        std::cout<<"ERROR in CurrentContainer::add(in): vector in has size: "<<in.size()<<" while *this container has size: "<<size<<std::endl;
	if(size>in.size()){
		std::transform(container.begin(),container.begin()+in.size(),in.begin(),container.begin(),std::plus<T>());
		return;
	}
    }
#endif
    std::transform(container.begin(),container.end(),in.begin(),container.begin(),std::plus<T>());
}

template <class T> void CurrentContainer<T>::scalar_multiply(const T& in) {
    using namespace std::placeholders;
    std::transform(container.begin(),container.end(),container.begin(),std::bind(std::multiplies<T>(),in, _1));
}

template <class T> std::ostream& operator<<(std::ostream& o, const CurrentContainer<T>& cc){
    using ps = ParticleStatistics;

    switch(cc.spin){
        case ps::scalar:
            o << "(s)";
            break;
        case ps::fermion:
            o << "(f)";
            break;
        case ps::antifermion:
            o << "(b)";
            break;
        case ps::vector:
            o << "(v)";
            break;
        case ps::tensor:
            o << "(T)";
            break;
        case ps::cubictensor:
            o << "(A)";
            break;
        default:
            o << "(UNDEF)";
            break;
    }

    o<<"<";
    if(cc.size>0)	o<<cc.container[0];
    for(size_t ii=1;ii<cc.size;ii++)	o<<","<<cc.container[ii];

    return o<<">";
}


template <class R> void EntryNormalization::set_normalization_factor(std::complex<R>& getnorm) {
    getnorm = std::complex<R>(1);
    for (auto& e : real_normalization_factors) getnorm *= pow(std::complex<R>(e.num) / std::complex<R>(e.den), e.exponent);
    for (auto& e : imaginary_normalization_factors) getnorm *= pow(std::complex<R>(0, e.num) / std::complex<R>(e.den), e.exponent);
}

#ifdef INSTANTIATE_RATIONAL
// FIXME: This should not be necessary
template <class R, class RR> void EntryNormalization::set_normalization_factor(R& getnorm, typename std::enable_if<is_rational<RR>::value>::type*) {
    getnorm = BigRat(1);
    for (auto& e : real_normalization_factors) getnorm *= R(pow(R(e.num) / R(e.den), uint64_t(e.exponent)));
    DEBUG(if (imaginary_normalization_factors.size() > 0) {
        _WARNING("WARNING: imaginary normalization factor skipped for real field calculation!");
        _WARNING("\t real factors: ", real_normalization_factors);
        _WARNING("\t imag factors: ", imaginary_normalization_factors);
    })
}
#endif

template <class R, class RR> void EntryNormalization::set_normalization_factor(R& getnorm, typename std::enable_if<!is_rational<RR>::value>::type*) {
    using std::pow;
    getnorm = R(1);
    for (auto& e : real_normalization_factors) getnorm *= R(pow(R(e.num) / R(e.den), e.exponent));
    DEBUG(if (imaginary_normalization_factors.size() > 0) {
        _WARNING("WARNING: imaginary normalization factor skipped for real field calculation!");
        _WARNING("\t real factors: ", real_normalization_factors);
        _WARNING("\t imag factors: ", imaginary_normalization_factors);
    })
}


// Declare instantiations in accordance with instantiation definitions in Builder.cpp

#define _BUILDER_NEW(KEY,T) \
    KEY template class Builder<T, 4>; \
    KEY template class Builder<T, 5>; \
    KEY template class Builder<T, 6>;

_BUILDER_NEW(extern,R)
_BUILDER_NEW(extern,C)

#ifdef HIGH_PRECISION
_BUILDER_NEW(extern,RHP)
_BUILDER_NEW(extern,CHP)
#endif

#ifdef VERY_HIGH_PRECISION
_BUILDER_NEW(extern,RVHP)
_BUILDER_NEW(extern,CVHP)
#endif

#ifdef INSTANTIATE_GMP
_BUILDER_NEW(extern,RGMP)
_BUILDER_NEW(extern,CGMP)
#endif

#ifdef USE_FINITE_FIELDS
_BUILDER_NEW(extern,F32)
#ifdef INSTANTIATE_RATIONAL
_BUILDER_NEW(extern,BigRat)
#endif
#endif

}
