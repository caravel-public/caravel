#include "Forest/cardinality.h"
#include "Core/spinor/clifford_algebra.h"
#include "Forest/Builder.h"

namespace Caravel{
template <> std::unique_ptr<cardinality_update_manager<F32>> cardinality_update_manager<F32>::instance = nullptr;
template <> std::mutex  cardinality_update_manager<F32>::b_access{};
template <> std::once_flag cardinality_update_manager<F32>::initInstanceFlag{};

template <typename T> void cardinality_update_manager<T>::init() {
    instance = std::unique_ptr<cardinality_update_manager<T>>(new cardinality_update_manager<T>()); 
}

template <typename T> cardinality_update_manager<T>* cardinality_update_manager<T>::get_instance() {
    std::call_once(initInstanceFlag,init);
    return instance.get();
}

template <typename T> void cardinality_update_manager<T>::add_builder(Builder_normalizations_base* b){
    std::lock_guard<std::mutex> lock(b_access);
    builders.insert(b);
}
template <typename T> void cardinality_update_manager<T>::remove_builder(Builder_normalizations_base* b){
    std::lock_guard<std::mutex> lock(b_access);
    builders.erase(b);
}

template <typename T> void cardinality_update_manager<T>::set_cardinality(uint64_t c) {
    static std::mutex m;
    std::lock_guard<std::mutex> lock(m);

    T::set_p(c);

    using clifford_algebra::sparse_smatrix;
    using clifford_algebra::sparse_DGamma;
    
    sparse_smatrix<6,T,true>::update_values();
    sparse_smatrix<6,T,false>::update_values();
    sparse_smatrix<8,T,true>::update_values();
    sparse_smatrix<8,T,false>::update_values();
    sparse_smatrix<10,T,true>::update_values();
    sparse_smatrix<10,T,false>::update_values();

    sparse_DGamma<4,T,true>::update_values();
    sparse_DGamma<4,T,false>::update_values();
    sparse_DGamma<6,T,true>::update_values();
    sparse_DGamma<6,T,false>::update_values();
    sparse_DGamma<8,T,true>::update_values();
    sparse_DGamma<8,T,false>::update_values();
    sparse_DGamma<10,T,true>::update_values();
    sparse_DGamma<10,T,false>::update_values();
    for(auto it: builders) it->update_normalizations();
}

  template <typename T> uint64_t cardinality_update_manager<T>::get_current_cardinality() { return T::get_p(); }

template class cardinality_update_manager<F32>;


}
