/**
 * @file Forest.h 
 *
 * @date 24.11.2015 original source
 *
 * @brief Header file for abstract generic off-shell tree/multi-loop recursions
 *
 * All classes associated to class Forest are defined here. Forest aims to construct generic 
 * solutions to off-shell recursion relations for computing trees and multi-loop cuts
 *
 * Implementations are provided in the files currents.cpp, Forest_utils.cpp and BG_basic.cpp
 *
*/
#ifndef FOREST_H_
#define FOREST_H_

#include <vector>
#include <string>
#include <tuple>
#include <utility>
#include <unordered_set>
#include <stdexcept>

#include "Coupling.h"
#include "Core/Particle.h"
#include "Core/cutTopology.h"
#include "Core/MathInterface.h"

#include "PhaseSpace/PhaseSpace.h"

// if not defined: includes all states appearing at Ds, such that cuts can be build from sums on a single Builder_fixed_Ds instances
// if defined:     includes all states appearing strictly up to Ds, such that cuts should be assembled from several Builder_fixed_Ds instances 
// #define BUILDERS_FOR_EACH_Ds is now set by the meson file in the main directory

// Option only considered when openMP is enabled
#ifdef USE_OMP
// 0: Sequential computation: 	No information tracked of possible parallel memory-sharing multithreading (memory efficient and faster single thread)
// 1: Multi-cut parallel:	Forest and Builder keep track of distinct loop particles to allow "omp parallel for" pragmas (memory intensive, probably with slow down of single threads)
#define _BUILDER_THREAD_SAFE 0
#endif

namespace Caravel {
class Current;
}

namespace std {
template <> struct hash<Caravel::Current*> { size_t operator()(Caravel::Current* c) const; };
} // namespace std

namespace Caravel {

// Dummy declarations for classes from Model.h
class CurrentRule; 
class Model;
class Current;


class CurrentLeaves;
CurrentLeaves operator*(const CurrentLeaves&,const CurrentLeaves&);
bool operator==(const CurrentLeaves&,const CurrentLeaves&);

struct InternalIndexVertex {
    bool needs_color_handling{false};
    int index_left{0};
    int index_right{0};
    std::vector<int> all_colored_internal;
    std::vector<int> all_colorless_internal;

    /**
     * Check is *this is in the default state (given by the default contructor)
     */
    bool is_trivial();

    InternalIndexVertex() = default;
};

bool operator==(const InternalIndexVertex&,const InternalIndexVertex&);
std::ostream& operator<<(std::ostream&, const InternalIndexVertex&);

class LeavesContainer {
		friend bool operator==(const LeavesContainer&,const LeavesContainer&);
		std::vector<Current*>colored;
		std::set<std::vector<Current*> >colorless;
                std::vector<std::vector<Current*>> sorted_currents;  /**< this members facilitates corresponding operator== */
	public:
		LeavesContainer();
		LeavesContainer(Current*);
		const std::vector<Current*>&get_colored() const;
		const std::set<std::vector<Current*> >&get_colorless() const;
                const std::vector<std::vector<Current*>>& get_sorted_currents() const;
		void add(const LeavesContainer&,bool);
		void clear();
		size_t size()const;
		std::string get_short_name() const;
		void show() const;
		class iterator {
			std::set<std::vector<Current*> >::iterator itNcs;
			std::set<std::vector<Current*> >::iterator itNce;
			std::vector<Current*>::const_iterator itNcv;
			std::vector<Current*>::const_iterator itCv;
			public:
			iterator(const std::set<std::vector<Current*> >::iterator&,const std::set<std::vector<Current*> >::iterator&,const std::vector<Current*>::const_iterator&,const std::vector<Current*>::const_iterator&);
				iterator operator++();
					bool operator!=(const iterator&)const;
			Current* operator*() const;
		};
		iterator begin()const;
		iterator end()const;
};

/**
 * Defines the leaves of a given current, keeping the information of potential dropped propagators in a given current
 */
class CurrentLeaves {
		friend bool operator==(const CurrentLeaves&,const CurrentLeaves&);
		friend std::hash<Current>;
		LeavesContainer open_leaves;
		std::vector<LeavesContainer> closed_leaves;
		std::vector<Current*> all_leaves;
		std::vector<size_t> mom_indices;
		bool prop_dropped;
	public:
		CurrentLeaves();
		CurrentLeaves(Current*,size_t);
		/**
		 * Method to instruct that current has a dropped propagator
		 */
		void drop_propagator();
		bool leaves_overlap(const CurrentLeaves&) const;
		const std::vector<size_t>& get_all_momenta_indices() const;
		const std::vector<Current*>& get_all_leaves() const;
		const LeavesContainer& get_open_leaves() const;
		void add(const CurrentLeaves&,bool);
		/**
		 * as inherited from Printable
		 */
		std::string get_short_name() const;
		void show() const;
		/**
		 * Tells if propagator has been dropped for *this
		 */
		bool propagator_dropped() const;
};

/**
 * Defines the internal indices of a given current, keeping the information of dropped propagators
 */
class CurrentIndices {
		friend bool operator==(const CurrentIndices&,const CurrentIndices&);
		friend CurrentIndices operator*(const CurrentIndices&,const CurrentIndices&);
                friend std::hash<Current>;
		bool prop_dropped{false};
		int dynamical_internal_index{0};	/**< An index_index that is dynamically assigned to associated Current, not tracked in operator==, returned by  */
		std::vector<int> internal_indices_parents;	/**< Internal indices in parent Currents */
		InternalIndexVertex internal_indices_open{};	/**< Tracks internal_index'es assigned to the open leaves */
		std::vector<InternalIndexVertex> internal_indices_closed;	/**< Tracks assignements of internal_index'es for vertices that have been closed */
	public:
		CurrentIndices() = default;
		CurrentIndices(const std::vector<Current*>&);
		/**
		 * Method to instruct that current has a dropped propagator, and to pass information on 'internal_index' assignement for the corresponding Vertex
		 */
		void drop_propagator_and_pass_internal_indices(const ColorHandledProcess&);
		/**
		 * Returns string describing internal_index information of *this
		 */
		std::string get_index_info() const;
		/**
		 * Tells if propagator has been dropped for *this
		 */
		bool propagator_dropped() const;
		/**
		 * Assigns dynamical_internal_index with the int passed
		 */
		void set_dynamically_assigned_internal_index(int);
		/**
		 * Gives information of internal_index information of the parent currents of *this.
		 * This information can be used to assign proper CurrentRule
		 *
		 * @param size_t representing the given parent of associated current
		 */
		int get_assigned_internal_index(size_t) const;
		int get_dynamically_assigned_internal_index() const;
		/**
		 * With the information passed, sets internal_indices_open
		 */
		void set_internal_indices_open(const InternalIndexVertex&);
		/**
		 * Method for Forest::build_offshell_currents to check matching of indices for currents
		 */
		static bool internal_indices_match(const CurrentIndices&, const CurrentIndices&);
		/**
		 * This method erases information on internal_indices_parents, meant for when *this represents
		 * the current indices of a CurrentBuild
		 */
		void reset_internal_indices_parents();
                void set_internal_indices_parents(const std::vector<int>& s) {internal_indices_parents = s;}
};

std::ostream& operator<<(std::ostream&, const CurrentIndices&);


class CorrelatedState {
		friend std::ostream& operator<<(std::ostream& s,const CorrelatedState& cs) {return s<<"{"<<cs.first()<<","<<cs.second()<<"}";};
		friend bool operator==(const CorrelatedState& a,const CorrelatedState& b) {return a.state==b.state;};
		std::pair<SingleState,SingleState> state;
	public:
		CorrelatedState() = default;
		CorrelatedState(const std::pair<SingleState,SingleState>& p): state(p) {};
		CorrelatedState(std::pair<SingleState,SingleState>&& p): state(std::move(p)) {};
		CorrelatedState& operator=(const std::pair<SingleState,SingleState>& p) { state=p; return *this;};
		CorrelatedState(const SingleState& a,const SingleState& b): state(a,b) {};
		CorrelatedState flip() {return CorrelatedState(this->state.second,this->state.first);};
		const SingleState& first() const {return state.first;};
		const SingleState& second() const {return state.second;};
};

class PhysicalStateContainer;

/**
 * Information on a given physical state associated with a Current. It holds information on dimensions for which the state has been added and whether or not it is present
 */
class PhysicalState /* : public Printable */ {
	public:
		std::vector<size_t> dims;	/**< Contains all dimensions tried that matched this state */
		std::vector<Particle*> leaves;	/**< Holds the pointers to all Particle's with default states of which we hold the states */
		std::vector<Particle*> correlated_complement_leaves;	/**< Holds the pointers to all Particle's with default states that complete a fully correlated state */
		std::vector<SingleState> state_entries;	/**< Vector of strings that define the state, one for each entry of the leaves */
		std::vector<SingleState> correlated_complement_state_entries;	/**< Vector of strings that define the states for members in correlated_complement_leaves */
		PhysicalState() = default;
		PhysicalState permutation(const std::vector<size_t>&) const;	/**< Produces a new physical state which is a permutation of *this */
		PhysicalState prune(const std::vector<Particle*>&) const;	/**< Produces a new physical state with a subset of the leaves in *this */
		bool extra_dim(const PhysicalState&);	/**< Checks if parameter matches *this up to dimensions. If true, adds to *this the missing dimensions */
		friend bool operator==(const PhysicalState&,const PhysicalState&);
		SingleState SingleState_of_particle_state(Particle*) const;	/**< Given a Particle*, returns if present corresponding SingleState */
		bool matches_daughter(const PhysicalState&) const;	/**< Checks that this->leaves are contained in the ones of param, and then checks state match */
		size_t get_index_correlated_state(const PhysicalStateContainer&) const;	/**< Returns the index of the PhysicalState in the container that complements *this -- Otherwise, returns zero and error message */
		bool contains_one_of_these_dims(const std::vector<size_t>&) const;	/**< Returns true if at least one of the dimensions passed is contained in this->dims, false otherwise */
		PhysicalState dimension_filter(const std::vector<size_t>&) const;	/**< Returns a copy of *this in which elements in this->dims not contained in the vector passed, are dropped */

		/**
		 * as inherited from Printable
		 */
		std::string get_short_name() const;
		void show() const;
};

std::ostream& operator<<(std::ostream&, const Caravel::PhysicalState&);

struct dim_counter {
	size_t dim;	/**< Dimension associated with this counter */
	size_t states_counter;	/**< Number of states for dim */
	size_t states_strictly_contributing_up_to_Ds;	/**< Number of states appearing with Ds'<=dim and not with Ds''>dim */
};

/**
 * Holds all information on physical states added to a given Current. Gives access to them through several dimensional strategies.
 */
class PhysicalStateContainer {
                friend std::ostream& operator<<(std::ostream&, const Caravel::PhysicalStateContainer&);
		Current* parent_current;	/**< Pointer to associated current */
		bool leaves_set;	/**< To track if internal defaulted leaves of the associated current have been set */
		std::vector<Particle*> default_leaves;	/**< Holds the pointers to all Particle's with default states in associated Current */
		std::vector<dim_counter> dim_tracker;	/**< Contains all dimensions added to *this and corresponding counters */
		std::vector<PhysicalState> all_states;	/**< All states contained */
	public:
		PhysicalStateContainer(Current*);
		size_t size() const;	/**< Returns the total number of states contained in all dimensions included, avoiding double counting */
		size_t size(size_t) const;	/**< Returns the total number of states container in a given dimension passed as argument */
		void set_leaves();	/**< Takes care of retrieving default leaves from parent_current */
		/** Passes and processes information to be contained in all_states, dims and states_counter 
		 *
		 * @param vector of PhysicalState's to add to *this
		 * @param int that if negative, implies that all states are to be processed. If positive, it fits the maximum amount of dimensions considered for each added state
		 */
		void set_default_states(const std::vector<PhysicalState>&,int);	
		const std::vector<Particle*>& get_default_state_particles() const;	/**< Gives access to Particle* with default states contained in *this */
		const PhysicalState& operator[] (size_t i) const {return all_states[i];};	/**< Gives access to individual PhysicalState's contained */
		const PhysicalState& operator[] (int i) const {return all_states[i];};	/**< Gives access to individual PhysicalState's contained */
		const PhysicalState& get_state(size_t,size_t) const;	/**< Gives access to the ith PhysicalState for a given dimension */
		dim_counter get_dim_counter(size_t) const;	/**< For a given dimension, it returns the associated dim_counter */
};

class Forest;

/**
 * This is the basic building block of the forest of trees
 * the currents
 */
class Current {
		friend class CurrentLeaves;
		friend class Forest;
		friend void recursive_current_construction(const size_t&,size_t,std::vector<size_t>&,const size_t&,CurrentRule*,const std::vector<ParticleType>&,const std::vector<Particle>&,
					const std::vector<size_t>&,std::vector<Current *>&,const std::vector<std::vector<Current*>>&,const std::vector<Coupling>&,const cutTopology::couplings_t&,
					bool,const InternalIndexVertex&);
	protected:
		Particle particle;	/**< This member encapsulates the particle information associated with *this  */
		bool default_particles_processed;	/**< Keeps information of whether defaulted particles have been processed by calling this->set_default_state_particles() */
		PhysicalStateContainer physical_states;	/**< Keeps info for all states associated with this current */
		int maximal_number_of_dimensions_accepted;	/**< When negative, all requested dimensions processed. If positive, only up to *this values (sorted) of the dimensions are considered */
		size_t default_momD_conf;	/**< In case multiple momD_conf's are handle by Builder, here we store the corresponding one */
		CurrentIndices internal_indices;	/**< This container handles all information associated to internal indices used to build *this */
	public:	
		const Particle& get_current_particle() const { return particle; };	/**< Access member particle */
		std::string get_index_info();
	protected:
		void set_internal_indices_open(const InternalIndexVertex& chp){ internal_indices.set_internal_indices_open(chp); }	
	public:
		/**
		 * Allows to set the internal index of the particle representing the current. In case
		 * the current has dropped propagator, it extends the setting to the CurrentLeaves.
		 * Also in the case of a CurrentExternal (current with a single leaf)
		 */
		void set_internal_index(int set){ internal_indices.set_dynamically_assigned_internal_index(set); }	
		CurrentIndices get_current_indices() const { return internal_indices; };
                int get_internal_index() const {return internal_indices.get_dynamically_assigned_internal_index();}
	public:	
		Current(): physical_states(this), maximal_number_of_dimensions_accepted(-1), default_momD_conf(0) {};
		/**
		 * Allows to set the maximal number of dimensions that default states will be resolved in
		 */
		void set_maximal_number_of_dimensions_accepted(int in) {maximal_number_of_dimensions_accepted=in;};
		/**
		 * Gets access to spin-statistics associated to this current
		 */
		virtual ParticleStatistics get_statistics() const {return particle.get_statistics();};
		// for accessing states info
		void set_default_state_particles();
		bool get_default_particles_processed() const;
		void set_default_states(const std::vector<PhysicalState>&);
		const std::vector<Particle*>& get_default_state_particles() const;
		virtual const PhysicalStateContainer& get_physical_states() const;
		std::string get_long_name();
		// purely virtual destructor
		virtual ~Current() = 0;
		virtual std::vector<Current*> * get_parents() = 0;
		virtual std::vector<Current*> * get_daughters() = 0;
		/**
		 * Current type is a ParticleType which is always conected to the particle member
		 */
		ParticleType get_current_type() const { return particle.get_type(); };
		/**
		 * Anti-Current type is a ParticleType which is always conected to the particle member
		 */
		ParticleType get_current_anti_type() const { return particle.get_anti_type(); };
		virtual std::string get_name();
		virtual std::string get_coupling_string() const = 0;
		virtual void add_parent(Current *) = 0;
		virtual void add_daughter(Current *) = 0;
		// how many leaves has this current
		virtual size_t get_n_leaves() const = 0;
		virtual const CurrentLeaves& get_leaves() const = 0;
		virtual CurrentLeaves& get_leaves() = 0;
		/**
		 * Virtual method to instruct that current has a dropped propagator, and to pass information on 'internal_index' assignement for the corresponding Vertex
		 */
		virtual void drop_propagator_and_pass_internal_indices(const ColorHandledProcess&);
		/**
		 * Virtual method to get 1-Current to the left of *this
		 *
		 * @return Current* corresponding to 1-Current to the left of *this (for exm in amp <12345|, for the (234) Current we get pointer to "1")
		 */
		virtual Current * left_color_adjacent() = 0;
		/**
		 * Virtual method to get 1-Current to the right of *this
		 *
		 * @return Current* corresponding to 1-Current to the right of *this (for exm in amp <12345|, for the (234) Current we get pointer to "5")
		 */
		virtual Current * right_color_adjacent() = 0;
		/**
		 * Virtual method to get internal 1-Current to the left of *this
		 *
		 * @return Current* corresponding to internal 1-Current to the left of *this (for exm in amp <12345|, for the (234) Current we get pointer to "2")
		 */
		virtual Current * internal_left_color() = 0;
		/**
		 * Virtual method to get internal 1-Current to the right of *this
		 *
		 * @return Current* corresponding to 1-Current to the right of *this (for exm in amp <12345|, for the (234) Current with get pointer to "5")
		 */
		virtual Current * internal_right_color() = 0;
		virtual void set_left_color_adjacent(Current *) = 0;
		virtual void set_right_color_adjacent(Current *) = 0;
		/**
		 * Returns if current is colored or not as stored in particle member
		 */
		bool is_colored() const { return particle.is_colored(); };
		// Reference to associated CurrentRule
		CurrentRule * rule;
		// Given a coupling, in returns the associated power to this current
		virtual int get_power(const Coupling&) const = 0;
		// in case a flip of leaves has been performed
		virtual bool was_flipped() const = 0;
		virtual void set_flipped() = 0;
		/**
		 * Virtual Method that allows easily compare a Current with a particle pointer
		 */
		virtual bool is_associated_to_particle(Particle*) const {return false;};
		/**
		 * Virtual Method that returns nullptr, unless it is a 1-leaf Current
		 */
		virtual Particle* get_unique_particle_leaf() const {return nullptr;};
		/**
		 * Virtual method that receives a vector of powers of couplings and returns true if it matches the current structure
		 */
		virtual bool check_couplings(const cutTopology::couplings_t&) const = 0;
		/**
		 * Method to define default momD_conf (necessary if Builder is to perform multithreading-safe calculations)
		 */
		void define_default_momDconf(size_t);
		size_t get_default_momDconf() const {return default_momD_conf;};

                /**
                 * An object which builds up information about couplings of scalars to quark lines.
                 * Maps internal_index (represents a spinor chain) to the list of coupled scalars to it.
                 * Used to match to a requested SelectionRule.
                 */
                std::map<int,std::vector<int>> qqs_powers;
                /**
                 * Stored qqs_powers for each closed cut vertex which this current contains.
                 */
                std::vector<decltype(qqs_powers)> closed_qqs_powers;
};

using CurrentPartitionEntries = std::vector<std::vector<size_t>>;

/**
 * Class for computing, storing and delivering generic integer partitions: given n, how can be written in terms of m integers (n>=0,m>=0)
 */
class CurrentPartitions {
		size_t maximum_n;
		std::vector<size_t> maximum_m_for_each_n;
		std::vector<std::vector<CurrentPartitionEntries>> store;
	public:
		CurrentPartitions();
		/**
		 * For a given pair of size_t's n,m, returns all possible ways n can be written in terms of m positive integers
		 */
		const CurrentPartitionEntries& get_partitions(size_t,size_t);
};

/**
 * Class for organizing information on internal states added to the Forest
 */
class DefaultStateParticleContainer {
		std::vector<Particle*> particles;	/**< Particle* (not owned) to each of the particles contained in this */
		std::vector<Current*> level_1_currents;	/**< Current* (not owned) corresponding to each particle added */
		std::vector<std::vector<std::vector<Current*>>> current_array;	/**< Array of currents containing the given default currents */
		std::vector<std::vector<Current*>> all_default_currents;	/**< Single array that contains all currents added to current_array, avoiding double counting */
		std::vector<std::vector<SingleState>> states;	/**< Corresponding states added to each entry */
	public:
		DefaultStateParticleContainer() = default;
		/**
		 * Returns number of entries on *this
		 */
		size_t size() const;
		/**
		 * If Current* is already included, do nothing. If not, add an entry to *this
		 */
		void add_entry(Current*);
		/**
		 * Method that adds entry to corresponding current_array
		 *
		 * @param Current* corresponding to level-1 current
		 * @param Current* to be added to corresponding current_array
		 */
		void add_current_to_entry(Current*,Current*);
		/**
		 * Giving access to Particle*'s in *this
		 */
		const Particle* get_particle(size_t) const;
		/**
		 * To manipulate corresponding vector of states
		 */
		std::vector<SingleState>& get_vector_of_states(size_t);
		/**
		 * To manipulate corresponding array of currents
		 */
		std::vector<std::vector<Current*>>& get_array_of_currents(size_t);
		/**
		 * To access the list of all currents that contain at least one defaulted state
		 */
		const std::vector<std::vector<Current*>>& get_all_defaulted_currents() const;
};

/**
 * Data structure to hold information on correlated states for multiple dimensions for a single pair of correlated particles
 */
class SinglePairMultiDimCorrelatedStates /* : public Printable */ {
		std::vector<size_t> dimensions;	/**< Keeps track of each dimension added to this */
		std::vector<std::vector<CorrelatedState>> states_by_dimension;	/**< For each dimension, it keeps a vector of all correlated states */
		std::vector<std::vector<SingleState>> first_particle_states_by_dimension;	/**< For each dimension, it keeps a vector of states associated with first_particle */
		std::vector<std::vector<SingleState>> second_particle_states_by_dimension;	/**< For each dimension, it keeps a vector of states associated with second_particle */
		std::vector<CorrelatedState> all_states;	/**< A master container of all correlated states added for all dimensions, avoiding double counting */
		std::vector<std::vector<size_t>> dimensions_in_all_states;	/**< Tracks for convenience all dimensions included on each entry in all_states */
		std::vector<SingleState> first_particle_all_states;	/**< For all dimensions, it keeps a vector of states associated with first_particle */
		std::vector<std::vector<size_t>> dimensions_in_first_particle_all_states;	/**< Tracks for convenience all dimensions included on each entry in first_particle_all_states */
		std::vector<SingleState> second_particle_all_states;	/**< For all dimensions, it keeps a vector of states associated with second_particle */
		std::vector<std::vector<size_t>> dimensions_in_second_particle_all_states;	/**< Tracks for convenience all dimensions included on each entry in second_particle_all_states */
	public:
		Particle* first_particle;	/**< Pointer to first particle in the correlated pair */
		Particle* second_particle;	/**< Pointer to second particle in the correlated pair */
		SinglePairMultiDimCorrelatedStates();
		/**
		 * Passes a correlated state associated to *this
		 */
		void add_correlated_state(size_t,const CorrelatedState&);
		/**
		 * Gives access to all_states
		 */
		const std::vector<CorrelatedState>& get_all_states() const;
		/**
		 * Gives access to all dimensions included in each entry of all_states
		 */
		const std::vector<std::vector<size_t>>& get_dimensions_in_all_states() const;

		/**
		 * as inherited from Printable
		 */
		std::string get_short_name() const;
		void show() const;
};

/**
 * Data structure to hold information on correlated states for multiple dimensions for a set of internal particles
 */
class MultiDimCorrelatedStates {
		std::vector<size_t> dimensions;	/**< Keeps track of each dimension added to this */
		std::vector<SinglePairMultiDimCorrelatedStates> states_per_pair;	/**< For each correlated pair included, we keep full information on correlated states */
	public:
		MultiDimCorrelatedStates() = default;
		size_t size() const {return states_per_pair.size();};	/**< Gives access to the # of states in states_per_pair */
		const SinglePairMultiDimCorrelatedStates& operator[] (size_t i) const {return states_per_pair[i];};	/**< Gives access to individual SinglePairMultiDimCorrelatedStates's contained */
		const SinglePairMultiDimCorrelatedStates& operator[] (int i) const {return states_per_pair[i];};	/**< Gives access to individual SinglePairMultiDimCorrelatedStates's contained */
		/**
		 * Passes a correlated stated associated
		 */
		void add_correlated_states(size_t,Particle*,Particle*,const std::vector<CorrelatedState>&);
};

struct physical_state_struct {
	std::vector<Particle*> particles;
	std::vector<PhysicalState> states;
};

struct state_counter_per_dim {
	size_t dim;
	std::vector<size_t> number_of_toplevel_cut_currents_for_this_dim;	/**< one entry in the vector for each cut */
	std::vector<size_t> number_of_toplevel_cut_currents_strictly_up_to_this_dim;	/**< one entry in the vector for each cut */
	size_t total_number_of_toplevel_cut_currents_for_this_dim;
	size_t total_number_of_toplevel_cut_currents_strictly_up_to_this_dim;

	std::vector<size_t> number_of_alllevels_cut_currents_for_this_dim;	/**< one entry in the vector for each cut */
	std::vector<size_t> number_of_alllevels_cut_currents_strictly_up_to_this_dim;	/**< one entry in the vector for each cut */
	size_t total_number_of_alllevels_cut_currents_for_this_dim;
	size_t total_number_of_alllevels_cut_currents_strictly_up_to_this_dim;

	std::vector<size_t> number_of_treelike_currents_per_level_for_this_dim;	/**< one entry in the vector for level */
	std::vector<size_t> number_of_treelike_currents_per_level_strictly_up_to_this_dim;	/**< one entry in the vector for level */
	size_t total_number_of_treelike_currents_for_this_dim;	
	size_t total_number_of_treelike_currents_strictly_up_to_this_dim;	

	std::vector<std::vector<Current*>> all_necessary_currents_for_this_dim;	/**< Array of current pointers associated with all currents needed for this dim as defined by top currents */
	std::vector<std::vector<Current*>> all_necessary_currents_strictly_up_to_this_dim;	/**< Array of current pointers associated with all currents needed up to this dim as defined by top currents */
};

/**
 * Friend class of Forest. Encapsulates all functionalities and data structures necessary for proper handling of multidimensional state assignments for Forest's currents. It
 * organizes all the states associated to all internal particles included in a given Forest, with useful multi-dimensional information that is requested by Builder when transforming 
 * a Forest.
 *
 * Includes the tracker of all cuts (forest->add_cut(.)) added to the Forest, as those are the ones that contain internal default-state currents.
 */
class DefaultStateManager {
		Forest * forest;	/**< Forest* to owner of *this */

		// Tracking all cuts added to forest
		std::vector<int> cut_register_index;	/**< Stores for each add_cut(.) the index obtained in the corresponding call to add_cutTopology */
		std::vector<size_t> cut_register_refmom;	/**< Stores for each add_cut(.) the reference momentum passed */

		MultiDimCorrelatedStates correlated_states;	/**<  keep information of correlated states for all particles hold in forest->generated_loop_particles */
		std::vector<std::vector<physical_state_struct>> all_state_combinations;	/**< Stores all produced state combinations requested, ordered by number of leaves (from one and on) */
		std::vector<size_t> number_tree_currents_per_level;	/**<  Tracks total number of currents per level */
		size_t total_number_of_tree_currents;	/**< Number of upper-level currents (trees) without any unresolved state */
		std::vector<state_counter_per_dim> dimensions_internal_states;	/**< Here we keep a sorted copy of the dimensions defined with define_internal_states(.) for internal states. The struct 'state_counter_per_dim' contains all data necessary to count states, and also an associated array of currents */

		std::vector<std::vector<int>> register_of_loop_particle_combinations;	/**< In case of need of thread-safe calculation, we track here distinct combinations of loop particles */
		std::vector<size_t> map_cut_to_loop_combination;	/**< For each entry in cut_register_index, we store corresponding entry in register_of_loop_particle_combinations */
	public:
		DefaultStateManager(Forest*);
		/**
		 * Registers a new cut added to forest
		 *
		 * @param int coming from the output of the corresponding forest->add_cutTopology(.)
		 * @param size_t representing the requested reference momentum for the internal loop momenta
		 * @param vector of integers representing the loop particles employed in the cut registered
		 */
		int register_cut(int,size_t,std::vector<int>);
		/**
		 * Returns number of cuts registered in *this
		 */
		size_t get_size_of_cut_register() const;
		/**
		 * Returns the reference momentum associated with the cut @param
		 */
		size_t get_ref_mom_for_cut(int) const;
		/**
		 * Returns the index with the current in *forest associated with the cut @param
		 */
		int get_current_index_for_cut(int) const;
		/**
		 * Most important method for DefaultStateManager. Given a vector of dimensions and then proceeds to produce all necessary states and pass information to all (default-state) currents
		 * in *forest
		 *
		 * @param std::vector of size_t's representing each dimension to be considered. It will be stored in a sorted way in dimensions_internal_states
		 */
		void define_internal_states(const std::vector<size_t>&);
		/**
		 * Method to get all dimensions added in define_internal_states(.)
		 */
		std::vector<size_t> get_added_dimensions() const;
		/**
		 * Takes a Current* and based on states contained defines its physical states in all dimensions
		 */
		void add_states_to_current(Current*);
		/**
		 * For a given (ordered) vector of Particle*, it checks if states are stored or if not produces, stores and returns it
		 */
		const std::vector<PhysicalState>& get_states_associated_with_default_particles(const std::vector<Particle*>&);
		/**
		 * Computes physical_state_struct associated with @param and states contained in *this
		 */
		physical_state_struct assemble_new_physical_state_struct(const std::vector<Particle*>&);
	private:
		/**
		 * Produces the cartesian product of all states in the input vector, where the entries point to the correlated pairs in correlated_states, ordered as stored (pairs like {first,second}X{first,second}X...)
		 */
		std::vector<PhysicalState> cartesian_product_of_states(const std::vector<size_t>&) const;
	public:
		/**
		 * Returns the total number of tree currents (not associated to any "default" state) present in *forest
		 */
		size_t get_total_number_of_tree_currents() const;
		/**
		 * Strategy for counting the total number of tree-like currents (with "default" entries counting with corresponding multiplicity) present in *forest
		 * Links to a private method
		 *
		 * @param size_t associated with dimension of request
		 */
		size_t get_total_number_of_treelike_currents(size_t) const;
		/**
		 * Strategy, returns the total number of tree-like currents (with "default" entries counting with corresponding multiplicity) present in *forest
		 * for a given dimension.
		 * Links to a private method
		 *
		 * @param size_t associated with dimension of request
		 * @param size_t representing current level for the request
		 */
		size_t get_treelike_currents_per_level(size_t,size_t) const;
		/**
		 * Strategy for counting total number of currents in Forest for a given cut. Links to a private method
		 *
		 * @param size_t representing the dimension (Ds) considered
		 * @param size_t to specify cut
		 */
		size_t get_count_of_states_for_cut(size_t,size_t) const;
		/**
		 * Counts total number of currents in Forest for a given cut including all levels of default currents. 
		 * Request forwarded to states_manager (notice that there a strategy of counting is defined!).
		 *
		 * @param size_t representing the dimension (Ds) considered
		 * @param size_t to specify cut
		 */
		size_t get_count_of_all_level_states_for_cut(size_t,size_t) const;
		/**
		 * Strategy  for counting total number of currents for all cuts in Forest in a given dimension. 
		 * Links to a private method
		 *
		 * @param size_t representing the dimension (Ds) considered
		 */
		size_t get_total_number_of_cut_currents(size_t) const;
		/**
		 * Given a Ds dimension, returns the array of all necessary currents contained in parent Forest
		 */
		const std::vector<std::vector<Current*>>& get_all_currents(size_t) const;
	private:
		/**
		 * Returns the total number of tree-like currents (with "default" entries counting with corresponding multiplicity) present in *forest
		 * for a given dimension
		 *
		 * @param size_t associated with dimension of request
		 */
		size_t get_total_number_of_treelike_currents_dim(size_t) const;
		/**
		 * Returns the total number of tree-like currents (with "default" entries counting with corresponding multiplicity) present in *forest
		 * considering all states strictly up to for a given dimension
		 *
		 * @param size_t associated with dimension of request
		 */
		size_t get_total_number_of_treelike_currents_up_to_dim(size_t) const;
		/**
		 * Returns the total number of tree-like currents (with "default" entries counting with corresponding multiplicity) present in *forest
		 * for a given dimension
		 *
		 * @param size_t associated with dimension of request
		 * @param size_t representing current level for the request
		 */
		size_t get_number_of_treelike_currents_per_level_dim(size_t,size_t) const;
		/**
		 * Returns the total number of tree-like currents (with "default" entries counting with corresponding multiplicity) present in *forest
		 * considering all states strictly up to for a given dimension
		 *
		 * @param size_t associated with dimension of request
		 */
		size_t get_number_of_treelike_currents_per_level_up_to_dim(size_t,size_t) const;
		/**
		 * Returns the total number of cut currents present in *forest for the specified cut at a given dimension
		 *
		 * @param size_t associated with dimension of request
		 * @param size_t associated with cut, as labeled by entry in forest->all_processes
		 */
		size_t get_number_of_toplevel_cut_currents_dim(size_t,size_t) const;
		/**
		 * Returns the total number of cut currents present in *forest for the specified cut including all levels of defaulted currents at a given dimension
		 *
		 * @param size_t associated with dimension of request
		 * @param size_t associated with cut, as labeled by entry in forest->all_processes
		 */
		size_t get_number_of_alllevels_cut_currents_dim(size_t,size_t) const;
		/**
		 * Returns the total number of cut currents present in *forest for a given dimension
		 *
		 * @param size_t associated with dimension of request
		 */
		size_t get_total_number_of_toplevel_cut_currents_dim(size_t) const;
		/**
		 * Returns the total number of cut currents present in *forest for the specified cut, considering states strictly appearing only up to the given dimension
		 *
		 * @param size_t associated with dimension of request
		 * @param size_t associated with cut, as labeled by entry in forest->all_processes
		 */
		size_t get_number_of_toplevel_cut_currents_up_to_dim(size_t,size_t) const;
		/**
		 * Returns the total number of cut currents present in *forest for the specified cut including all levels of defaulted currents, 
		 * considering states strictly appearing only up to the given dimension
		 *
		 * @param size_t associated with dimension of request
		 * @param size_t associated with cut, as labeled by entry in forest->all_processes
		 */
		size_t get_number_of_alllevels_cut_currents_up_to_dim(size_t,size_t) const;
		/**
		 * Returns the total number of cut currents present in *forest, considering states strictly appearing only up to the given dimension
		 *
		 * @param size_t associated with dimension of request
		 */
		size_t get_total_number_of_toplevel_cut_currents_up_to_dim(size_t) const;
	public:
		/**
		 * This method loops over all cuts in *forest and updates the state counting which is contained in the member dimensions_internal_states
		 */
		void update_counting_all_dims_and_currents();
		/**
		 * This method returns the number of independent momentum configurations that Builder could be required to run in a multithreading-safe way
		 *	
		 * @return the maximum between 1 and register_of_loop_particle_combinations.size()
		 */
		size_t number_of_momDconf_copies() const;
		/**
		 * For a registered cut, returns corresponding default momD_conf
		 *
		 * @return corresponding entry in map_cut_to_loop_combination
		 */
		size_t corresponding_momDconf_for_cut(int) const;
};

namespace detail {
/**
 * A helper class to implement the fall-back equality check for unordered_set of currents.
 */
struct CurrentEquality {
    Model* m; /**< The Model needs to be stored to know which couplings to check */
    CurrentEquality(Model* sm) : m(sm) {}
    bool operator()(Current* c1, Current* c2) const;
};
} // namespace detail

/**
 * Class that constructs off-shell currents and their tree structure from the Feynman rules defined in given Model.
 *
 * The user can sequentially add different (color ordered, helicity, partition) trees or 1- or 2-loop cuts and Forest should take
 * care of matching to existing currents or creating new ones as required.
 *
 * Forest works at an 'abstract' level, using only topological information and have no access to kinematic tools. For this, look
 * into the Builder class template.
 *
 */
class Forest {
    public:
                using CurrentsSetType = std::unordered_set<Current*,std::hash<Current*>,detail::CurrentEquality>;
		friend class DefaultStateManager;
    private:
                std::vector<CurrentsSetType> all_currents_set; /**< To store pointers to all currents */
		std::vector<std::vector<Current*>> all_tree_currents;	/**<  To store pointers to all tree-level currents */
		std::vector<std::vector<int>> all_powers;	/**<  Corresponding powers */
		Model * model;	/**< Cloned copy of the Model passed to the constructor. Owned by *this */
		// handle information of cuts added

		std::vector<std::tuple<Particle*,Particle*>> generated_loop_particles;	/**<  keep loop Particles* (owned) (all physical states) created for each loop_particle, and a Particle copy */
		DefaultStateParticleContainer default_state_particles; /**< DefaultStateParticleContainer keeping info of all "default" states in *this */
		DefaultStateManager states_manager;	/**< DefaultStateManager encapsulating algorithms and containing all information on default states as extracted from the define_internal_states_and_close_forest(.) */
		bool forest_closed;	/**<  To track if forest has been closed (no more undefined states to be added) */
		bool minimal_states_in_currents;	/**< A boolean to track if Forest should close undefined states in a minimal way. For example, at two loops, only two Ds's are kept for non-bowtie cuts and only three for bowties */
#ifdef DS_BY_PARTICLE_CONTENT
                bool enable_Ds_decomposition{true};
#else
                bool enable_Ds_decomposition{false};
#endif
		std::vector<std::vector<int>> join_cuts_calls;		/**<  Tracks cut mergers */

		std::vector<size_t> loop_count_present;		/**<  to keep track of which type of currents (0: tree, 1: 1-loop, 2: 2-loop, etc) have been added to the forest */
	public:
		/**
		 * Constructor of Forest, starting from an empty Forest, and trees and cuts can be added sequentially with add_process(.) and add_cutTopology(.)
		 *
		 * @param Model pointer to the corresponding model to use. It is cloned and a copy kept internally. The user can destroy the pointer at any stage
		 * @param bool defaulted to true. It makes that the Forest construct minimal set of states for each contained current when this->define_internal_states_and_close_forest(.) is called. If false, all requested states explicitly added
		 */
		Forest(Model*,bool = true);
		Forest(const Model&,bool = true);

		Forest(const Forest&) = delete;
		// cutTopology (successfully) added, paired with corresponding upper level current and external current to contract
				// last entry vector keeps all currents (level by level) that contain associated loop particles (empty if tree computation)
		std::vector<std::tuple<cutTopology,Current*,Current*,std::vector<std::vector<Current*>>>> all_processes;
		size_t get_size_of_cut_register() const;
		size_t get_ref_mom_for_cut(int) const;
		int get_current_index_for_cut(int) const;
		/**
		 * Given a Ds dimension, returns the array of all necessary currents. Call forwarded to states_manager. Default value, returns all
		 */
		const std::vector<std::vector<Current*>> get_all_currents(size_t = 0) const;
		const std::vector<std::vector<Current*>>& get_all_tree_currents() const;
		/**
		 * Add a given tree-level process with information on couplings and their corresponding powers 
		 *
		 * @param Process of external particles, assuming colored ones as ordered
		 * @param Powers of the couplings referring to the given model
		 * @return Integer that indexes the trees added to the forest, can be used with method get_tree() later on with Builder
		 */
		int add_process(Process,const cutTopology::couplings_t&, const SelectionRule& = {});

                /**
                 * Adds a tree-level process given only the external particles,
                 * attempting to make a sane interpretation of the external couplings.
                 */
                int add_process(Process);

                [[deprecated("The coupling information should be passed as a map to not rely on the order!")]] int add_process(Process, std::vector<int>);
                void set_minimal_states_in_currents(bool s) { minimal_states_in_currents = s; }

              private:
		/**
		 * Main internal method for adding processes and cuts to the Forest. It is 'private' as it depends on internal information, as it use loop particles in generated_loop_particles
		 *
		 * @param vector of tuple's describing: 1) int: location of loop in global Process; 2) int: location of minus loop in global Process; 3) int: location of p_{loop} in momD_conf; 4) int: location of -p_{loop} in momD_conf
		 * @param cutTopology that specifies the tree/cut to be added
		 * @param vector of pair's associated to the loop,-loop location in generated_loop_particles with same order as first parameter
		 * @param bool to properly handle color ordering in cuts associated to planar hierarchies (relevant for 2 loops (and beyond))
		 * @return Integer that indexes the trees/cuts added to the forest
		 */
		int add_cutTopology(std::vector< std::tuple<int,int,int,int>>,cutTopology&,std::vector<int>,bool switch_to_planar_standard=false);
	public:
		/**
		 * Forwards call to model to return list of couplings
		 */
		const std::vector<Coupling>& get_model_couplings() const;
		/**
		 * Main method to add a generic (tree, 1- or 2-loop) cut to the Forest.
		 *
		 * @param vector of tuples describing: 1) int: location (in the ordered list of all particles of the cut) of loop in global Process; 2) int: location of minus loop in global Process; 3) int: location of p_{loop} in momD_conf; 4) int: location of -p_{loop} in momD_conf
		 * @param cutTopology that describes the cut (trivial for trees and with structure for 1- and 2-loop cuts)
		 * @param size_t defaulted (to zero) unsigned integer for using as reference vector of internal particles
		 * @param bool to handle non-trivial 2-loop cut color ordering in planar hierarchies (defaulted to false)
		 */
		int add_cut(std::vector< std::tuple<int,int,int,int>>&,cutTopology&,size_t = 0,bool switch_to_planar_standard=false);
		/**
		 * Returns the number of trees (no "default" states present) that have been added to the current with add_process(). Request forwarded to states_manager.
		 */
		size_t get_total_number_of_tree_currents() const;
		/**
		 * Returns the total number of tree-like entries, including trees and cuts added to the Forest in a given dimension. Request forwarded to states_manager.
		 *
		 * @param size_t representing the dimension (Ds) considered
		 */
		size_t get_total_number_of_treelike_currents(size_t) const;
		/**
		 * Returns the number of tree-like currents in the Forest (with trees and cut resolved) per level. Request forwarded to states_manager
		 *
		 * @param size_t representing the dimension (Ds) considered
		 * @param size_t for the level requested
		 */
		size_t get_treelike_currents_per_level(size_t,size_t) const;
		/**
		 * Counts total number of currents in Forest for a given cut. Request forwarded to states_manager (notice that there a strategy of counting is defined!).
		 *
		 * @param size_t representing the dimension (Ds) considered
		 * @param size_t to specify cut
		 */
		size_t get_count_of_states_for_cut(size_t,size_t) const;
		/**
		 * Counts total number of currents in Forest for a given cut including all levels of default currents. 
		 * Request forwarded to states_manager (notice that there a strategy of counting is defined!).
		 *
		 * @param size_t representing the dimension (Ds) considered
		 * @param size_t to specify cut
		 */
		size_t get_count_of_all_level_states_for_cut(size_t,size_t) const;
		/**
		 * Counts total number of currents for all cuts in Forest in a given dimension. 
		 * Request forwarded to states_manager (notice that there a strategy of counting is defined!).
		 *
		 * @param size_t representing the dimension (Ds) considered
		 */
		size_t get_total_number_of_cut_currents(size_t) const;
		/**
		 * This method effectively closes the Forest, not allowing any other add_process() or add_cut(). The vector of size_t's passed
		 * specifies the dimensions in which default states present in the stored currents will be defined. It takes care of defining in 
		 * a nested way the different states (that is, if Ds1>Ds2, only new states in Ds1 are considered and the rest mapped into existing ones).
		 */
		void define_internal_states_and_close_forest(const std::vector<size_t>&);
		/**
		 * Method to get all dimensions added in define_internal_states_and_close_forest(.)
		 */
		std::vector<size_t> get_added_dimensions() const;
		/**
		 * This method can be used to merge currents between cuts added by add_cut()
		 *	for example, it can help to no over compute cuts that overlap considerably, like for example the boxes <nl1m|2p|3m|4p| & <nl1m|2p|3m|4m|
		 */
		void join_cuts(std::vector<int>);
		/**
		 * This method returns the number of independent momentum configurations that Builder could be required to run in a multithreading-safe way
		 */
		size_t number_of_momDconf_copies() const;
		/**
		 * For a registered cut, returns corresponding default momD_conf
		 */
		size_t corresponding_momDconf_for_cut(int) const;
		std::vector<size_t> get_loop_count_present();
                const Model& get_model() const;
		~Forest();
	private:
		/**
		 * Private method to locate if a loop particle has been stored in generated_loop_particles, or to create it and store it otherwise
		 *
		 * @param Particle pointer corresponding to loop particle and loop antiparticle
		 * @param int location of loop in global Process
		 * @param int location of minus loop in global Process
		 * @param int location of p_{loop} in momD_conf
		 * @param int location of -p_{loop} in momD_conf
		 * @return int related to entry in generated_loop_particles that corresponds to given loop particle
		 */
		int location_loop_momenta(std::pair<Particle*,Particle*>,int,int,int,int);
		/**
		 * Takes care of cleaning up global containers and of copy to them new information
		 *
		 * @param vector of tuple's which contains: 1) int: location of loop in global Process; 2) int: location of minus loop in global Process; 3) int: location of p_{loop} in momD_conf; 4) int: location of -p_{loop} in momD_conf
		 * @param cutTopology to be processed
		 * @param vector of int's with information of location in generated_loop_particles of passed loop particles
		 * @param bool to tag if configuration is planar for two-loop cuts
		 */
		void store_variables(std::vector<std::tuple<int,int,int,int>>,cutTopology&,std::vector<int>,bool);
		/**
		 * Method to recursively build currents associated with current global variables. Goes sequentially over each vertex of cutTopology
		 */
		void build_offshell_currents();
		// LOCAL VERTEX operations
		/**
		 * Method to process the addition of currents related to a given vertex on global_topology
		 *
		 * @param size_t that labels the vertex to process
		 * @param bool that tells if this is the last vertex
		 */
		void offshell_currents_from_vertex(size_t,bool);
		/**
		 * Performs necessary steps to end addition of currents checks on local currents produced by build_offshell_currents()
                 * @param number of Ds powers
		 */
		void finish_add_cutTopology(unsigned);
                /**
                 * Checks if the current is an onshell current that should be dropped
                 * @param Current* the current to be checked
                 */
                bool is_onshell(Current*) const;
                /**
                  * Performs momentum conservation for ext. and internal momenta
                  * @param vector<size_t> vector of momentum indices
                  * @param size_t the number of external momenta
                  */
                std::vector<size_t> mom_conservation(const std::vector<size_t>&, size_t) const;

         
		
		// NOW GLOBAL VARIABLES DEFINED!
		int location_new_forest_entry;	/**< int that specifies location of last added cutTopology in all_processes */
		bool global_handle_request;	/**< bool to specify if request is valid */
		bool global_switch_to_planar;	/**< bool to store if cut should be handled in a planar way */
		bool global_is_factorizable;		/**< bool to define if we handle two-loop factorizable cut */
		size_t global_vertex_locate_l1;	/**< size_t to identify vertex location of loop particle 1 in a two-loop cut */
		size_t global_vertex_locate_l2;	/**< size_t to identify vertex location of loop particle 2 in a two-loop cut */
		size_t global_index_in_vertex_locate_l1;	/**< size_t to identify location within containing vertex of loop particle 1 in a two-loop cut */
		size_t global_index_in_vertex_locate_l2;	/**< size_t to identify location within containing vertex of loop particle 2 in a two-loop cut */
		size_t global_loop_level;	/**< size_t to identify if global_topology is a tree (0) or one- (1) or two-loop (2) cut  */
		std::vector<int> global_location_generated_loop_momentum;	/**< Here we store location in generated_loop_particles of employed loop momenta as passed to add_cutTopology */
		static thread_local CurrentPartitions partitions;	/**< Static variable that contains all partitions necessary for handling generic Feynman rules */
		std::vector<Current*> global_level_1_currents_default;	/**< Keeps a register of level 1 currents which contain associated loop momenta */
		
		cutTopology global_topology;	/**< cutTopology which specifies the last topology added to the Forest, with corresponding replacement for internal loop momenta particles */
		Current * global_start_current;	/**< Current* that characterizes the 'left' input current for the next vertex to be processed in global_topology */
		Current * global_close_current;	/**< Current* that characterizes the 'right' final current for contracting and getting trees/cuts */
		std::vector<std::vector<Current*>> global_currents_with_loop_particles;	/**< array containing level-by-level the currents that depend on default loop particles */
		InternalIndexVertex global_internal_index_vertex;	/**< Tracks full information of internal indices in a given vertex in global_topology */
};

std::ostream& operator<<(std::ostream&, Forest&);
void print_all_currents(Forest& forest); 

void explore_current_inheritance(Forest& forest);

/// \enum PropagatorType
/// Enumerates the types of propagators that a current can have associated. A factor of I is always assumed factored out
WISE_ENUM_CLASS(PropagatorType,
	/// full propagator (scalar 1/(p^2-m^2); fermion (\slash p + m)/(p^2-m^2); massless vector -1/(p^2))
	(propagator , 0),
	/// a cut propagator  (scalar 1; fermion 1; massless vector -1)
	(cut , 1),
	/// identity (commonly used for final contraction of top currents in BG)
	(identity , 2)
)

/**
 * This is the basic building block of the forest of trees
 * the currents (composed of several sub currents)
 */
class CurrentBuild : public Current {
            friend std::hash<CurrentBuild*>;
	protected:
		std::vector<Current*> parents;
		std::vector<Current*> daughters;
		CurrentLeaves leaves;
		std::string name;
		std::string pure_name;
		std::string cstring;
		size_t size;
		Current * Lcurrent;
		Current * Rcurrent;
		Current * InternalLcurrent;
		Current * InternalRcurrent;
		// To keep coupling content --- we might later store this more efficiently
		std::vector<Coupling> couplings;
		std::vector<int> powers;
	public:
		PropagatorType includepropagator;	/**< To define if *this needs to include */
		bool wasflipped;
		std::vector<Current*> basicblocks;
		/**
		 * Default constructor
		 */
		CurrentBuild();
		/**
		 * General constructor
		 *
		 * @param vector of pointers of Currents, representing the set of subcurrents of *this
		 * @param corresponding vector of couplings
		 * @param PropagatorType to hold information about type of propagator
		 * @param ColorHandledProcess from which possible non-trivial routing information (for fermions) will be handled out
		 */
		CurrentBuild(const std::vector<Current *>&,const std::vector<Coupling>&,PropagatorType,const ColorHandledProcess&);
		~CurrentBuild();
		/**
		 * This method returns the type of scalar propagator that is included in *this current
		 */
		PropagatorType include_propagator() const;
	// As inherited from Current:
		std::vector<Current*> * get_parents() override;
		std::vector<Current*> * get_daughters() override;
		// I take implementation from the base class
		// later it might make more sense to have a string storing this name (finally too many levels of recursion)
		std::string get_name() override;
		std::string get_coupling_string() const override;
		void add_parent(Current *) override;
		void add_daughter(Current *) override;
		// how many leaves has this current
		size_t get_n_leaves() const override;
		const CurrentLeaves& get_leaves() const override;
		CurrentLeaves& get_leaves() override;
		void drop_propagator_and_pass_internal_indices(const ColorHandledProcess&) override;
		Current * left_color_adjacent() override;
		Current * right_color_adjacent() override;
		Current * internal_left_color() override;
		Current * internal_right_color() override;
		void set_left_color_adjacent(Current *) override;
		void set_right_color_adjacent(Current *) override;
		// Given a coupling, in returns the associated power to this current
		int get_power(const Coupling&) const override;
		// in case a flip of leaves has been performed
		bool was_flipped() const override;
		void set_flipped() override;
		bool check_couplings(const cutTopology::couplings_t&) const override;

};

/**
 * This is the basic building block for getting currents in the Forest
 * the sub currents
 */
class CurrentBasic : public Current {
	protected:
		std::vector<Current*> parents;
		std::vector<Current*> daughters;
		Current* parent_CurrentBuild;	/**< A pointer to the associated CurrentBuild that contains all subcurrents including *this */
		CurrentLeaves leaves;
		std::string name;
		std::string cstring;
		size_t size;
		Current * Lcurrent;
		Current * Rcurrent;
		Current * InternalLcurrent;
		Current * InternalRcurrent;
	public:
		int * indices;	/**< Shift for when evaluating reference CurrentRule (rule) --- Negative in case image needed */
	protected:
		// To keep coupling content
		std::vector<Coupling> couplings;
		std::vector<int> powers;
		bool wasflipped;
	public:
		CurrentBasic();
		/**
		 * Constructor without explicit information on contract type, color and spin-statistics (which remain set by default constructors)
		 *
		 * @param Particle for type of the current
		 * @param list of parent currents
		 * @param the CurrentRule employed for building *this
		 * @param vector to map into indices[] array
		 * @param vector of Coupling's to be copied into member 'couplings'
		 */
		CurrentBasic(const Particle&,std::vector<Current *>,CurrentRule *,std::vector<size_t>,const std::vector<Coupling>&);
		/**
		 * A method to set parent_CurrentBuild for *this
		 *
		 * @param Current* that will be used to set parent_CurrentBuild (will check if it has been set before!)
		 */
		void set_parent_CurrentBuild(Current *);
		~CurrentBasic();
	// As inherited from Current:
		const PhysicalStateContainer& get_physical_states() const override;
		std::vector<Current*> * get_parents() override;
		std::vector<Current*> * get_daughters() override;
		// I take implementation from the base class
		// later it might make more sense to have a string storing this name (finally too many levels of recursion)
		std::string get_name() override;
		std::string get_coupling_string() const override;
		void add_parent(Current *) override;
		void add_daughter(Current *) override;
		// how many leaves has this current
		size_t get_n_leaves() const override;
		const CurrentLeaves& get_leaves() const override;
		CurrentLeaves& get_leaves() override;
		void drop_propagator_and_pass_internal_indices(const ColorHandledProcess&) override;
		Current * left_color_adjacent() override;
		Current * right_color_adjacent() override;
		Current * internal_left_color() override;
		Current * internal_right_color() override;
		void set_left_color_adjacent(Current *) override;
		void set_right_color_adjacent(Current *) override;
		// Reference to associated CurrentRule
		CurrentRule * rule;
		// Given a coupling, in returns the associated power to this current
		int get_power(const Coupling&) const override;
		// in case a flip of leaves has been performed
		bool was_flipped() const override;
		void set_flipped() override;
		bool check_couplings(const cutTopology::couplings_t&) const override;

		int * get_indices();
		/**
		 * This method will give information on internal indices stored in leaves. If open, will warn, if closed, will give index associated to given parent
		 *
		 * @param size_t representing the parent that is associated to the call
		 */
		int get_assigned_internal_index(size_t) const;

		void set_internal_indices_parents(const std::vector<int>&);
};

/**
 * Currents corresponding to external particles
 */
class CurrentExternal : public Current {
	protected:
		std::vector<Current*> daughters;
		CurrentLeaves leaf;
		Current * Lcurrent;
		Current * Rcurrent;
	public:
		CurrentExternal(Particle *);
		Particle * psource;
		~CurrentExternal();
	// As inherited from Current:
		std::vector<Current*> * get_parents() override;
		std::vector<Current*> * get_daughters() override;
		// uses base class implementation
		std::string get_name() override;
		std::string get_coupling_string() const override;
		void add_parent(Current *) override;
		void add_daughter(Current *) override;
		// how many leaves has this current
		size_t get_n_leaves() const override;
		const CurrentLeaves& get_leaves() const override;
		CurrentLeaves& get_leaves() override;
		void drop_propagator_and_pass_internal_indices(const ColorHandledProcess&) override;
		Current * left_color_adjacent() override;
		Current * right_color_adjacent() override;
		Current * internal_left_color() override;
		Current * internal_right_color() override;
		void set_left_color_adjacent(Current *) override;
		void set_right_color_adjacent(Current *) override;
		Particle * get_source();
		// Reference to associated CurrentRule
		CurrentRule * rule;
		// Given a coupling, in returns the associated power to this current
		int get_power(const Coupling&) const override;
		// in case a flip of leaves has been performed
		bool was_flipped() const override;
		void set_flipped() override;
		/**
		 * Method that allows easily compare a Current with a particle pointer
		 */
		bool is_associated_to_particle(Particle*p) const override;
		/**
		 * Virtual Method that returns nullptr, unless it is a 1-leaf Current
		 */
		Particle* get_unique_particle_leaf() const override;
		bool check_couplings(const cutTopology::couplings_t&) const override;
};

std::ostream& operator<< (std::ostream&, const Current&);

}
#endif	// FOREST_H_
