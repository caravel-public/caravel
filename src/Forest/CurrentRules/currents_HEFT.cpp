#include "currents_HEFT.h"
#include "Core/Debug.h"

namespace Caravel {
// Implementations for CurrentRule_Hgg
CurrentRule_Hgg::CurrentRule_Hgg() {
    DEBUG_MESSAGE("Constructed a Hgg color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle H("", ParticleType("H"), SingleState("NONMOD"), 0, 0);
    Particle g1("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g2("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{H, g1, g2};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("Hgg");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(2);
    this->set_coupling(g, gpower);
}

CurrentRule_Hgg::~CurrentRule_Hgg() {}

// Implementations for CurrentRule_Hggg
CurrentRule_Hggg::CurrentRule_Hggg() {
    DEBUG_MESSAGE("Constructed a Hggg color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle H("", ParticleType("H"), SingleState("NONMOD"), 0, 0);
    Particle g1("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g2("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g3("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{H, g1, g2, g3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("Hggg");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(3);
    this->set_coupling(g, gpower);
}

CurrentRule_Hggg::~CurrentRule_Hggg() {}

// Implementations for CurrentRule_Hgggg
CurrentRule_Hgggg::CurrentRule_Hgggg() {
    DEBUG_MESSAGE("Constructed a Hgggg color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle H("", ParticleType("H"), SingleState("NONMOD"), 0, 0);
    Particle g1("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g2("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g3("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g4("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{H, g1, g2, g3, g4};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("Hgggg");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(4);
    this->set_coupling(g, gpower);
}

CurrentRule_Hgggg::~CurrentRule_Hgggg() {}

} // namespace Caravel
