#include "Forest/CurrentRule.h"

namespace Caravel {
/**
 * Derived CurrentRule class for ggg color ordered rule
 */
class CurrentRule_ggg : public CurrentRule {
  public:
    CurrentRule_ggg();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_ggg* clone() const { return new CurrentRule_ggg(*this); };
    ~CurrentRule_ggg();
};

/**
 * Derived CurrentRule class for gggg color ordered rule
 */
class CurrentRule_gggg : public CurrentRule {
  public:
    CurrentRule_gggg();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_gggg* clone() const { return new CurrentRule_gggg(*this); };
    ~CurrentRule_gggg();
};

/**
 * Derived CurrentRule class for ggT color ordered rule
 */
class CurrentRule_ggT : public CurrentRule {
  public:
    CurrentRule_ggT();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_ggT* clone() const { return new CurrentRule_ggT(*this); };
    ~CurrentRule_ggT();
};

/******
 * Rules with Ds-scalars
 */

#define generate_new_current(name)                                                                                                                             \
    class CurrentRule_##name : public CurrentRule {                                                                                                            \
      public:                                                                                                                                                  \
        CurrentRule_##name();                                                                                                                                  \
        CurrentRule_##name* clone() const final { return new CurrentRule_##name(*this); };                                                                     \
    }

#define generate_new_current_2(name)                                                                                                                           \
    class CurrentRule_##name : public CurrentRule {                                                                                                            \
      public:                                                                                                                                                  \
        CurrentRule_##name();                                                                                                                                  \
        CurrentRule_##name* clone() const final { return new CurrentRule_##name(*this); };                                                                     \
        virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;                                  \
    }

#define generate_new_current_3(name)                                                                                                                           \
    class CurrentRule_##name : public CurrentRule {                                                                                                            \
      public:                                                                                                                                                  \
        CurrentRule_##name();                                                                                                                                  \
        CurrentRule_##name* clone() const final { return new CurrentRule_##name(*this); };                                                                     \
        virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;                                  \
        virtual void update_qqs_couplings(CurrentBasic*, int mode = 0) const final;                                                                            \
    }

#define generate_new_current_4(name)                                                                                                                           \
    class CurrentRule_##name : public CurrentRule {                                                                                                            \
      public:                                                                                                                                                  \
        CurrentRule_##name();                                                                                                                                  \
        CurrentRule_##name* clone() const final { return new CurrentRule_##name(*this); };                                                                     \
        virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;                                  \
        virtual bool selection_rule_relevant() const final { return true; }                                                                                    \
        virtual void update_qqs_couplings(CurrentBasic*, int mode = 0) const final;                                                                            \
    }

generate_new_current_3(gqqL);
generate_new_current_3(gqqR);
generate_new_current_2(pqq);
generate_new_current_2(pll);

generate_new_current_3(Wp_ubar_d);
generate_new_current_3(Wm_u_dbar);

generate_new_current_2(Wm_ebar_ve);
generate_new_current_2(Wp_e_vebar);

#undef generate_new_current
#undef generate_new_current_2
#undef generate_new_current_3
#undef generate_new_current_4

/**
 * Derived CurrentRule class for photon-QQ rule
 */
class CurrentRulePUU : public CurrentRule {
  public:
    CurrentRulePUU();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRulePUU* clone() const { return new CurrentRulePUU(*this); };
    ~CurrentRulePUU();
};

/**
 * Derived CurrentRule class for 3-pt photon rule (just a fake to test)
 */
class CurrentRulePPP : public CurrentRule {
  public:
    CurrentRulePPP();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRulePPP* clone() const { return new CurrentRulePPP(*this); };
    ~CurrentRulePPP();
};

/**
 * Derived CurrentRule class for Htt color ordered rule
 */
class CurrentRule_Htt : public CurrentRule {
  public:
    CurrentRule_Htt();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_Htt* clone() const { return new CurrentRule_Htt(*this); };
    ~CurrentRule_Htt();
};

/**
 * Derived CurrentRule class for gttL color ordered rule
 */
class CurrentRule_gttL : public CurrentRule {
  public:
    CurrentRule_gttL();
    CurrentRule_gttL* clone() const { return new CurrentRule_gttL(*this); };
    ~CurrentRule_gttL();
    virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;
};
/**
 * Derived CurrentRule class for gttR color ordered rule
 */
class CurrentRule_gttR : public CurrentRule {
  public:
    CurrentRule_gttR();
    CurrentRule_gttR* clone() const { return new CurrentRule_gttR(*this); };
    ~CurrentRule_gttR();
    virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;
};
/**
 * Derived CurrentRule class for Mass renormalisation insertion
 */
class CurrentRule_Zm : public CurrentRule {
  public:
    CurrentRule_Zm();
    CurrentRule_Zm* clone() const { return new CurrentRule_Zm(*this); };
    ~CurrentRule_Zm();
};

} // namespace Caravel
