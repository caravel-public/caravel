#include "currents_Gravity.h"
#include "Core/Debug.h"

namespace Caravel {

// Implementations for CurrentRule_GGG
CurrentRule_GGG::CurrentRule_GGG() {
    DEBUG_MESSAGE("Constructed a GGG vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, G3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("GGG");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling K("K");
    int Kpower(1);
    this->set_coupling(K, Kpower);
}

CurrentRule_GGG::~CurrentRule_GGG() {}

// Implementations for CurrentRule_Cubic_GGG
CurrentRule_Cubic_GGG::CurrentRule_Cubic_GGG() {
    DEBUG_MESSAGE("Constructed a GGG vertex for CubicGravity");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, G3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("Cubic_GGG");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling K("K");
    int Kpower(1);
    this->set_coupling(K, Kpower);
}

CurrentRule_Cubic_GGG::~CurrentRule_Cubic_GGG() {}

// Implementations for CurrentRule_GGGG
CurrentRule_GGGG::CurrentRule_GGGG() {
    DEBUG_MESSAGE("Constructed a GGGG vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G4("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, G3, G4};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("GGGG");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling K("K");
    int Kpower(2);
    this->set_coupling(K, Kpower);
}

CurrentRule_GGGG::~CurrentRule_GGGG() {}

// Implementations for CurrentRule_GGGGG
CurrentRule_GGGGG::CurrentRule_GGGGG() {
    DEBUG_MESSAGE("Constructed a GGGGG vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G4("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G5("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, G3, G4, G5};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("GGGGG");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling K("K");
    int Kpower(3);
    this->set_coupling(K, Kpower);
}

CurrentRule_GGGGG::~CurrentRule_GGGGG() {}

// Implementations for CurrentRule_GB_GGG
CurrentRule_GB_GGG::CurrentRule_GB_GGG() {
    DEBUG_MESSAGE("Constructed a GB_GGG vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, G3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("GB_GGG");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define couplings and corresponding powers
    Coupling K("K");
    int Kpower(3);
    this->set_coupling(K, Kpower);

    Coupling c("c");
    int cpower(1);
    this->set_coupling(c, cpower);
}

CurrentRule_GB_GGG::~CurrentRule_GB_GGG() {}

// Implementations for CurrentRule_GB_GGGG
CurrentRule_GB_GGGG::CurrentRule_GB_GGGG() {
    DEBUG_MESSAGE("Constructed a GB_GGGG vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G4("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, G3, G4};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("GB_GGGG");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling K("K");
    int Kpower(4);
    this->set_coupling(K, Kpower);

    Coupling c("c");
    int cpower(1);
    this->set_coupling(c, cpower);
}

CurrentRule_GB_GGGG::~CurrentRule_GB_GGGG() {}

// Implementations for CurrentRule_R3_GGG
CurrentRule_R3_GGG::CurrentRule_R3_GGG() {
    DEBUG_MESSAGE("Constructed a R3_GGG vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, G3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("R3_GGG");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define couplings and corresponding powers
    Coupling K("K");
    int Kpower(5);
    this->set_coupling(K, Kpower);

    Coupling c("cR3");
    int cpower(1);
    this->set_coupling(c, cpower);
}

CurrentRule_R3_GGG::~CurrentRule_R3_GGG() {}

// Implementations for CurrentRule_R3_GGGG
CurrentRule_R3_GGGG::CurrentRule_R3_GGGG() {
    DEBUG_MESSAGE("Constructed a R3_GGGG vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G4("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, G3, G4};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("R3_GGGG");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling K("K");
    int Kpower(6);
    this->set_coupling(K, Kpower);

    Coupling c("cR3");
    int cpower(1);
    this->set_coupling(c, cpower);
}

CurrentRule_R3_GGGG::~CurrentRule_R3_GGGG() {}

// Implementations for CurrentRule_GGA
CurrentRule_GGA::CurrentRule_GGA() {
    DEBUG_MESSAGE("Constructed a GGA vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle A1("", ParticleType("A"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, G2, A1};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("GGA");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling K("K");
    int Kpower(1);
    this->set_coupling(K, Kpower);
}

CurrentRule_GGA::~CurrentRule_GGA() {}

// Implementations for CurrentRule_GAA
CurrentRule_GAA::CurrentRule_GAA() {
    DEBUG_MESSAGE("Constructed a GAA vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle A1("", ParticleType("A"), SingleState("NONMOD"), 0, 0);
    Particle A2("", ParticleType("A"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{G1, A1, A2};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("GAA");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling K("K");
    int Kpower(1);
    this->set_coupling(K, Kpower);
}

CurrentRule_GAA::~CurrentRule_GAA() {}

// Implementations for CurrentRule_ssG1
CurrentRule_ssG1::CurrentRule_ssG1() {
    DEBUG_MESSAGE("Constructed a ssG1 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle s1("", ParticleType("Phi1"), SingleState("NONMOD"), 0, 0);
    Particle s2("", ParticleType("Phi1"), SingleState("NONMOD"), 0, 0);
    Particle G("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{s1, s2, G};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ssG1");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(1);
    this->set_coupling(coup, cpower);
}

CurrentRule_ssG1::~CurrentRule_ssG1() {}

// Implementations for CurrentRule_ssG2
CurrentRule_ssG2::CurrentRule_ssG2() {
    DEBUG_MESSAGE("Constructed a ssG2 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle s1("", ParticleType("Phi2"), SingleState("NONMOD"), 0, 0);
    Particle s2("", ParticleType("Phi2"), SingleState("NONMOD"), 0, 0);
    Particle G("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{s1, s2, G};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ssG2");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(1);
    this->set_coupling(coup, cpower);
}

CurrentRule_ssG2::~CurrentRule_ssG2() {}

// Implementations for CurrentRule_ssGG1
CurrentRule_ssGG1::CurrentRule_ssGG1() {
    DEBUG_MESSAGE("Constructed a ssGG1 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle s1("", ParticleType("Phi1"), SingleState("NONMOD"), 0, 0);
    Particle s2("", ParticleType("Phi1"), SingleState("NONMOD"), 0, 0);
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{s1, s2, G1, G2};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ssGG1");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(2);
    this->set_coupling(coup, cpower);
}

CurrentRule_ssGG1::~CurrentRule_ssGG1() {}

// Implementations for CurrentRule_ssGG2
CurrentRule_ssGG2::CurrentRule_ssGG2() {
    DEBUG_MESSAGE("Constructed a ssGG2 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle s1("", ParticleType("Phi2"), SingleState("NONMOD"), 0, 0);
    Particle s2("", ParticleType("Phi2"), SingleState("NONMOD"), 0, 0);
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{s1, s2, G1, G2};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ssGG2");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(2);
    this->set_coupling(coup, cpower);
}

CurrentRule_ssGG2::~CurrentRule_ssGG2() {}

// Implementations for CurrentRule_ssGGG1
CurrentRule_ssGGG1::CurrentRule_ssGGG1() {
    DEBUG_MESSAGE("Constructed a ssGGG1 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle s1("", ParticleType("Phi1"), SingleState("NONMOD"), 0, 0);
    Particle s2("", ParticleType("Phi1"), SingleState("NONMOD"), 0, 0);
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{s1, s2, G1, G2, G3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ssGGG1");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(3);
    this->set_coupling(coup, cpower);
}

CurrentRule_ssGGG1::~CurrentRule_ssGGG1() {}

// Implementations for CurrentRule_ssGGG2
CurrentRule_ssGGG2::CurrentRule_ssGGG2() {
    DEBUG_MESSAGE("Constructed a ssGGG2 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle s1("", ParticleType("Phi2"), SingleState("NONMOD"), 0, 0);
    Particle s2("", ParticleType("Phi2"), SingleState("NONMOD"), 0, 0);
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{s1, s2, G1, G2, G3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ssGGG2");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(3);
    this->set_coupling(coup, cpower);
}

CurrentRule_ssGGG2::~CurrentRule_ssGGG2() {}

// Implementations for CurrentRule_VVG1
CurrentRule_VVG1::CurrentRule_VVG1() {
    DEBUG_MESSAGE("Constructed a VVG1 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle V1("", ParticleType("V1"), SingleState("NONMOD"), 0, 0);
    Particle V2("", ParticleType("V1"), SingleState("NONMOD"), 0, 0);
    Particle G("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{V1, V2, G};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("VVG1");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(1);
    this->set_coupling(coup, cpower);
}

CurrentRule_VVG1::~CurrentRule_VVG1() {}

// Implementations for CurrentRule_VVG2
CurrentRule_VVG2::CurrentRule_VVG2() {
    DEBUG_MESSAGE("Constructed a VVG2 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle V1("", ParticleType("V2"), SingleState("NONMOD"), 0, 0);
    Particle V2("", ParticleType("V2"), SingleState("NONMOD"), 0, 0);
    Particle G("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{V1, V2, G};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("VVG2");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(1);
    this->set_coupling(coup, cpower);
}

CurrentRule_VVG2::~CurrentRule_VVG2() {}

// Implementations for CurrentRule_VVGG1
CurrentRule_VVGG1::CurrentRule_VVGG1() {
    DEBUG_MESSAGE("Constructed a VVGG1 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle V1("", ParticleType("V1"), SingleState("NONMOD"), 0, 0);
    Particle V2("", ParticleType("V1"), SingleState("NONMOD"), 0, 0);
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{V1, V2, G1, G2};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("VVGG1");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(2);
    this->set_coupling(coup, cpower);
}

CurrentRule_VVGG1::~CurrentRule_VVGG1() {}

// Implementations for CurrentRule_VVGG2
CurrentRule_VVGG2::CurrentRule_VVGG2() {
    DEBUG_MESSAGE("Constructed a VVGG2 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle V1("", ParticleType("V2"), SingleState("NONMOD"), 0, 0);
    Particle V2("", ParticleType("V2"), SingleState("NONMOD"), 0, 0);
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{V1, V2, G1, G2};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("VVGG2");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(2);
    this->set_coupling(coup, cpower);
}

CurrentRule_VVGG2::~CurrentRule_VVGG2() {}

// Implementations for CurrentRule_VVGGG1
CurrentRule_VVGGG1::CurrentRule_VVGGG1() {
    DEBUG_MESSAGE("Constructed a VVGGG1 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle V1("", ParticleType("V1"), SingleState("NONMOD"), 0, 0);
    Particle V2("", ParticleType("V1"), SingleState("NONMOD"), 0, 0);
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{V1, V2, G1, G2, G3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("VVGGG1");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(3);
    this->set_coupling(coup, cpower);
}

CurrentRule_VVGGG1::~CurrentRule_VVGGG1() {}

// Implementations for CurrentRule_VVGGG2
CurrentRule_VVGGG2::CurrentRule_VVGGG2() {
    DEBUG_MESSAGE("Constructed a VVGGG2 vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle V1("", ParticleType("V2"), SingleState("NONMOD"), 0, 0);
    Particle V2("", ParticleType("V2"), SingleState("NONMOD"), 0, 0);
    Particle G1("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G2("", ParticleType("G"), SingleState("NONMOD"), 0, 0);
    Particle G3("", ParticleType("G"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{V1, V2, G1, G2, G3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("VVGGG2");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling coup("K");
    int cpower(3);
    this->set_coupling(coup, cpower);
}

CurrentRule_VVGGG2::~CurrentRule_VVGGG2() {}
} // namespace Caravel
