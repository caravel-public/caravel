#include "Forest/CurrentRule.h"

namespace Caravel {

/******
 * Rules with Ds-scalars
 */

#define generate_new_current(name)                                                                                                                             \
    class CurrentRule_##name : public CurrentRule {                                                                                                            \
      public:                                                                                                                                                  \
        CurrentRule_##name();                                                                                                                                  \
        CurrentRule_##name* clone() const final { return new CurrentRule_##name(*this); };                                                                     \
    }

#define generate_new_current_2(name)                                                                                                                           \
    class CurrentRule_##name : public CurrentRule {                                                                                                            \
      public:                                                                                                                                                  \
        CurrentRule_##name();                                                                                                                                  \
        CurrentRule_##name* clone() const final { return new CurrentRule_##name(*this); };                                                                     \
        virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;                                  \
    }

#define generate_new_current_3(name)                                                                                                                           \
    class CurrentRule_##name : public CurrentRule {                                                                                                            \
      public:                                                                                                                                                  \
        CurrentRule_##name();                                                                                                                                  \
        CurrentRule_##name* clone() const final { return new CurrentRule_##name(*this); };                                                                     \
        virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;                                  \
        virtual void update_qqs_couplings(CurrentBasic*, int mode = 0) const final;                                                                            \
    }

#define generate_new_current_4(name)                                                                                                                           \
    class CurrentRule_##name : public CurrentRule {                                                                                                            \
      public:                                                                                                                                                  \
        CurrentRule_##name();                                                                                                                                  \
        CurrentRule_##name* clone() const final { return new CurrentRule_##name(*this); };                                                                     \
        virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;                                  \
        virtual bool selection_rule_relevant() const final { return true; }                                                                                    \
        virtual void update_qqs_couplings(CurrentBasic*, int mode = 0) const final;                                                                            \
    }

generate_new_current_3(gss);
generate_new_current_3(ggss);
generate_new_current_3(gsgs);

generate_new_current_4(sqqL);
generate_new_current_4(sqqR);

#undef generate_new_current
#undef generate_new_current_2
#undef generate_new_current_3
#undef generate_new_current_4

class CurrentRule_ssss : public CurrentRule {
  public:
    CurrentRule_ssss();
    CurrentRule_ssss* clone() const final { return new CurrentRule_ssss(*this); };
    virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>& perm) const final;
    virtual void update_qqs_couplings(CurrentBasic*, int mode = 0) const final;
    virtual int n_of_modes() const final { return 3; }
};

} // namespace Caravel
