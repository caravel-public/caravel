#include "Forest/CurrentRule.h"

namespace Caravel {
/**
 * Derived CurrentRule class for Hgg color ordered rule
 */
class CurrentRule_Hgg : public CurrentRule {
  public:
    CurrentRule_Hgg();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_Hgg* clone() const { return new CurrentRule_Hgg(*this); };
    ~CurrentRule_Hgg();
};

/**
 * Derived CurrentRule class for Hggg color ordered rule
 */
class CurrentRule_Hggg : public CurrentRule {
  public:
    CurrentRule_Hggg();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_Hggg* clone() const { return new CurrentRule_Hggg(*this); };
    ~CurrentRule_Hggg();
};

/**
 * Derived CurrentRule class for Hgggg color ordered rule
 */
class CurrentRule_Hgggg : public CurrentRule {
  public:
    CurrentRule_Hgggg();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_Hgggg* clone() const { return new CurrentRule_Hgggg(*this); };
    ~CurrentRule_Hgggg();
};
} // namespace Caravel
