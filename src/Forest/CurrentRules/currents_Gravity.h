#include "Forest/CurrentRule.h"

namespace Caravel {
/**
 * Derived CurrentRule class for GGG rule
 */
class CurrentRule_GGG : public CurrentRule {
  public:
    CurrentRule_GGG();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_GGG* clone() const { return new CurrentRule_GGG(*this); };
    ~CurrentRule_GGG();
};

/**
 * Derived CurrentRule class for Cubic_GGG rule in CubicGravity
 */
class CurrentRule_Cubic_GGG : public CurrentRule {
  public:
    CurrentRule_Cubic_GGG();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_Cubic_GGG* clone() const { return new CurrentRule_Cubic_GGG(*this); };
    ~CurrentRule_Cubic_GGG();
};

/**
 * Derived CurrentRule class for GGGG rule
 */
class CurrentRule_GGGG : public CurrentRule {
  public:
    CurrentRule_GGGG();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_GGGG* clone() const { return new CurrentRule_GGGG(*this); };
    ~CurrentRule_GGGG();
};

/**
 * Derived CurrentRule class for GGGGG rule
 */
class CurrentRule_GGGGG : public CurrentRule {
  public:
    CurrentRule_GGGGG();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_GGGGG* clone() const { return new CurrentRule_GGGGG(*this); };
    ~CurrentRule_GGGGG();
};

/**
 * Derived CurrentRule class for GB_GGG rule
 */
class CurrentRule_GB_GGG : public CurrentRule {
  public:
    CurrentRule_GB_GGG();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_GB_GGG* clone() const { return new CurrentRule_GB_GGG(*this); };
    ~CurrentRule_GB_GGG();
};

/**
 * Derived CurrentRule class for GB_GGGG rule
 */
class CurrentRule_GB_GGGG : public CurrentRule {
  public:
    CurrentRule_GB_GGGG();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_GB_GGGG* clone() const { return new CurrentRule_GB_GGGG(*this); };
    ~CurrentRule_GB_GGGG();
};

/**
 * Derived CurrentRule class for R3_GGG rule
 */
class CurrentRule_R3_GGG : public CurrentRule {
  public:
    CurrentRule_R3_GGG();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_R3_GGG* clone() const { return new CurrentRule_R3_GGG(*this); };
    ~CurrentRule_R3_GGG();
};

/**
 * Derived CurrentRule class for R3_GGGG rule
 */
class CurrentRule_R3_GGGG : public CurrentRule {
  public:
    CurrentRule_R3_GGGG();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_R3_GGGG* clone() const { return new CurrentRule_R3_GGGG(*this); };
    ~CurrentRule_R3_GGGG();
};

/**
 * Derived CurrentRule class for GGA rule, meant for the model CubicGravity
 */
class CurrentRule_GGA : public CurrentRule {
  public:
    CurrentRule_GGA();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_GGA* clone() const { return new CurrentRule_GGA(*this); };
    ~CurrentRule_GGA();
};

/**
 * Derived CurrentRule class for GAA rule, meant for the model CubicGravity
 */
class CurrentRule_GAA : public CurrentRule {
  public:
    CurrentRule_GAA();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_GAA* clone() const { return new CurrentRule_GAA(*this); };
    ~CurrentRule_GAA();
};

/**
 * Derived CurrentRule class for ssG1 rule
 */
class CurrentRule_ssG1 : public CurrentRule {
  public:
    CurrentRule_ssG1();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_ssG1* clone() const { return new CurrentRule_ssG1(*this); };
    ~CurrentRule_ssG1();
};

/**
 * Derived CurrentRule class for ssG2 rule
 */
class CurrentRule_ssG2 : public CurrentRule {
  public:
    CurrentRule_ssG2();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_ssG2* clone() const { return new CurrentRule_ssG2(*this); };
    ~CurrentRule_ssG2();
};

/**
 * Derived CurrentRule class for ssGG1 rule
 */
class CurrentRule_ssGG1 : public CurrentRule {
  public:
    CurrentRule_ssGG1();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_ssGG1* clone() const { return new CurrentRule_ssGG1(*this); };
    ~CurrentRule_ssGG1();
};

/**
 * Derived CurrentRule class for ssGG2 rule
 */
class CurrentRule_ssGG2 : public CurrentRule {
  public:
    CurrentRule_ssGG2();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_ssGG2* clone() const { return new CurrentRule_ssGG2(*this); };
    ~CurrentRule_ssGG2();
};

/**
 * Derived CurrentRule class for ssGGG1 rule
 */
class CurrentRule_ssGGG1 : public CurrentRule {
  public:
    CurrentRule_ssGGG1();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_ssGGG1* clone() const { return new CurrentRule_ssGGG1(*this); };
    ~CurrentRule_ssGGG1();
};

/**
 * Derived CurrentRule class for ssGGG2 rule
 */
class CurrentRule_ssGGG2 : public CurrentRule {
  public:
    CurrentRule_ssGGG2();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_ssGGG2* clone() const { return new CurrentRule_ssGGG2(*this); };
    ~CurrentRule_ssGGG2();
};

/**
 * Derived CurrentRule class for VVG1 rule
 */
class CurrentRule_VVG1 : public CurrentRule {
  public:
    CurrentRule_VVG1();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_VVG1* clone() const { return new CurrentRule_VVG1(*this); };
    ~CurrentRule_VVG1();
};

/**
 * Derived CurrentRule class for VVG2 rule
 */
class CurrentRule_VVG2 : public CurrentRule {
  public:
    CurrentRule_VVG2();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_VVG2* clone() const { return new CurrentRule_VVG2(*this); };
    ~CurrentRule_VVG2();
};

/**
 * Derived CurrentRule class for VVGG1 rule
 */
class CurrentRule_VVGG1 : public CurrentRule {
  public:
    CurrentRule_VVGG1();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_VVGG1* clone() const { return new CurrentRule_VVGG1(*this); };
    ~CurrentRule_VVGG1();
};

/**
 * Derived CurrentRule class for VVGG2 rule
 */
class CurrentRule_VVGG2 : public CurrentRule {
  public:
    CurrentRule_VVGG2();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_VVGG2* clone() const { return new CurrentRule_VVGG2(*this); };
    ~CurrentRule_VVGG2();
};

/**
 * Derived CurrentRule class for VVGGG1 rule
 */
class CurrentRule_VVGGG1 : public CurrentRule {
  public:
    CurrentRule_VVGGG1();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_VVGGG1* clone() const { return new CurrentRule_VVGGG1(*this); };
    ~CurrentRule_VVGGG1();
};

/**
 * Derived CurrentRule class for VVGGG2 rule
 */
class CurrentRule_VVGGG2 : public CurrentRule {
  public:
    CurrentRule_VVGGG2();
    // As inherited from CurrentRule:
    // virtual copy parttern
    CurrentRule_VVGGG2* clone() const { return new CurrentRule_VVGGG2(*this); };
    ~CurrentRule_VVGGG2();
};
} // namespace Caravel
