#include "currents_SM.h"
#include "Core/Debug.h"
#include "current_helper.h"

namespace Caravel {

// Implementations for CurrentRule_ggg
CurrentRule_ggg::CurrentRule_ggg() {
    DEBUG_MESSAGE("Constructed a ggg color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle g1("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g2("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g3("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{g1, g2, g3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ggg");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(1);
    this->set_coupling(g, gpower);
}

CurrentRule_ggg::~CurrentRule_ggg() {}

// Implementations for CurrentRule_ggT
CurrentRule_ggT::CurrentRule_ggT() {
    DEBUG_MESSAGE("Constructed a ggT color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle g1("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g2("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle T("", ParticleType("T"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{g1, g2, T};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ggT");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(1);
    this->set_coupling(g, gpower);
}

CurrentRule_ggT::~CurrentRule_ggT() {}

// Implementations for CurrentRule_gggg
CurrentRule_gggg::CurrentRule_gggg() {
    DEBUG_MESSAGE("Constructed a gggg color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle g1("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g2("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g3("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g4("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{g1, g2, g3, g4};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("gggg");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(2);
    this->set_coupling(g, gpower);
}

CurrentRule_gggg::~CurrentRule_gggg() {}

// Implementations for CurrentRule_gqqL
CurrentRule_gqqL::CurrentRule_gqqL() {
    DEBUG_MESSAGE("Constructed a gqqL color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle qb("", ParticleType("qb"), SingleState("NONMOD"), 0, 0);
    Particle q("", ParticleType("q"), SingleState("NONMOD"), 0, 0);
    Particle glue("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{qb, q, glue};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("gqqL");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(1);
    this->set_coupling(g, gpower);
}

// Implementations for CurrentRule_gqqR
CurrentRule_gqqR::CurrentRule_gqqR() {
    DEBUG_MESSAGE("Constructed a gqqR color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle q("", ParticleType("q"), SingleState("NONMOD"), 0, 0);
    Particle qb("", ParticleType("qb"), SingleState("NONMOD"), 0, 0);
    Particle glue("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{q, qb, glue};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("gqqR");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(1);
    this->set_coupling(g, gpower);
}

// Implementations for CurrentRule_pqq
CurrentRule_pqq::CurrentRule_pqq() {
    DEBUG_MESSAGE("Constructed a pqq vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle qb("", ParticleType("qb"), SingleState("NONMOD"), 0, 0);
    Particle q("", ParticleType("q"), SingleState("NONMOD"), 0, 0);
    Particle photon("", ParticleType("photon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{qb, q, photon};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("pqq");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling g("gw");
    int gpower(1);
    this->set_coupling(g, gpower);
}

// Implementations for CurrentRule_pll
CurrentRule_pll::CurrentRule_pll() {
    DEBUG_MESSAGE("Constructed a pll vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle lb("", ParticleType("lb"), SingleState("NONMOD"), 0, 0);
    Particle l("", ParticleType("l"), SingleState("NONMOD"), 0, 0);
    Particle photon("", ParticleType("photon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{lb, l, photon};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("pll");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling g("gw");
    int gpower(1);
    this->set_coupling(g, gpower);
}

// Implementations for CurrentRulePUU
CurrentRulePUU::CurrentRulePUU() {
    DEBUG_MESSAGE("Constructed a PUU vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle ub("", ParticleType(ParticleStatistics::antifermion, ParticleFlavor::ub, ParticleMass(), true), SingleState("NONMOD"), 0, 0);
    Particle u("", ParticleType(ParticleStatistics::fermion, ParticleFlavor::u, ParticleMass(), true), SingleState("NONMOD"), 0, 0);
    Particle photon("", ParticleType("photon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{ub, u, photon};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("PUU");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling g("gw");
    int gpower(1);
    this->set_coupling(g, gpower);
}

CurrentRulePUU::~CurrentRulePUU() {}

// Implementations for CurrentRulePPP
CurrentRulePPP::CurrentRulePPP() {
    DEBUG_MESSAGE("Constructed a PPP vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle p1("", ParticleType("photon"), SingleState("NONMOD"), 0, 0);
    Particle p2("", ParticleType("photon"), SingleState("NONMOD"), 0, 0);
    Particle p3("", ParticleType("photon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{p1, p2, p3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("PPP");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling g("gw");
    int gpower(1);
    this->set_coupling(g, gpower);
}

// Implementations for CurrentRule_gttL
CurrentRule_gttL::CurrentRule_gttL() {
    DEBUG_MESSAGE("Constructed a gttL color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at
    Particle tb("", ParticleType("tb"), SingleState("NONMOD"), 0, 0);
    Particle t("", ParticleType("t"), SingleState("NONMOD"), 0, 0);
    Particle glue("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{tb, t, glue};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("gttL");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(1);
    this->set_coupling(g, gpower);
}

CurrentRule_gttL::~CurrentRule_gttL() {}

// Implementations for CurrentRule_gttR
CurrentRule_gttR::CurrentRule_gttR() {
    DEBUG_MESSAGE("Constructed a gttR color ordered vertex");

    // no name added, no state, no momentum of reference momentum pointed at
    Particle t("", ParticleType("t"), SingleState("NONMOD"), 0, 0);
    Particle tb("", ParticleType("tb"), SingleState("NONMOD"), 0, 0);
    Particle glue("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{t, tb, glue};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("gttR");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    int gpower(1);
    this->set_coupling(g, gpower);
}

CurrentRule_gttR::~CurrentRule_gttR() {}

// Implementations for CurrentRule_Htt
CurrentRule_Htt::CurrentRule_Htt() {
    DEBUG_MESSAGE("Constructed a Htt vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle H("", ParticleType("H"), SingleState("NONMOD"), 0, 0);
    Particle t("", ParticleType("t"), SingleState("NONMOD"), 0, 0);
    Particle tb("", ParticleType("tb"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{H, t, tb};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("Htt");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling g("yt");
    int gpower(1);
    this->set_coupling(g, gpower);
}

CurrentRule_Htt::~CurrentRule_Htt() {}

// Implementations for CurrentRule_Zm
CurrentRule_Zm::CurrentRule_Zm() {
    DEBUG_MESSAGE("Constructed a mass renormalisation insertion vertex");

    // no name added, no state, no momentum of reference momentum pointed at, mass set to zero
    Particle R("", ParticleType(ParticleStatistics::scalar, ParticleFlavor::renormalon, ParticleMass{}, false), SingleState("NONMOD"), 0, 0);
    Particle t("", ParticleType("t"), SingleState("NONMOD"), 0, 0);
    Particle tb("", ParticleType("tb"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{R, t, tb};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("Zm");

    // Define if color ordered vertex
    this->set_colorordered(false);

    // Now define coupling and corresponding power
    Coupling g("zm");
    int gpower(1);
    this->set_coupling(g, gpower);
}

CurrentRule_Zm::~CurrentRule_Zm() {}

CurrentRulePPP::~CurrentRulePPP() {}
CurrentRule_Wp_ubar_d::CurrentRule_Wp_ubar_d() {
    Particle ubar("", ParticleType("ub"), SingleState("NONMOD"), 0, 0);
    Particle d("", ParticleType("d"), SingleState("NONMOD"), 0, 0);
    Particle Wp("", ParticleType("Wp"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{Wp, ubar, d};

    this->fill(to_fill);
    this->set_name("Wp_ubar_d");
    this->set_colorordered(false);
    this->set_coupling(Coupling{"gw"}, 1);
}

bool CurrentRule_Wp_ubar_d::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqg_template(out, other, false);
}
void CurrentRule_Wp_ubar_d::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::qq_g_or_s(c); }

CurrentRule_Wm_u_dbar::CurrentRule_Wm_u_dbar() {
    Particle dbar("", ParticleType("db"), SingleState("NONMOD"), 0, 0);
    Particle u("", ParticleType("u"), SingleState("NONMOD"), 0, 0);
    Particle Wm("", ParticleType("Wm"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{Wm, dbar, u};

    this->fill(to_fill);
    this->set_name("Wm_u_dbar");
    this->set_colorordered(false);
    this->set_coupling(Coupling{"gw"}, 1);
}

bool CurrentRule_Wm_u_dbar::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqg_template(out, other, false);
}
void CurrentRule_Wm_u_dbar::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::qq_g_or_s(c); }

CurrentRule_Wm_ebar_ve::CurrentRule_Wm_ebar_ve() {
    Particle ebar("", ParticleType("eb"), SingleState("NONMOD"), 0, 0);
    Particle ve("", ParticleType("ve"), SingleState("NONMOD"), 0, 0);
    Particle Wm("", ParticleType("Wm"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{Wm, ve, ebar};

    this->fill(to_fill);
    this->set_name("Wm_ebar_ve");
    this->set_colorordered(false);
    this->set_coupling(Coupling{"gw"}, 1);
}
bool CurrentRule_Wm_ebar_ve::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqg_template(out, other, false);
}

CurrentRule_Wp_e_vebar::CurrentRule_Wp_e_vebar() {
    Particle vebar("", ParticleType("veb"), SingleState("NONMOD"), 0, 0);
    Particle e("", ParticleType("e"), SingleState("NONMOD"), 0, 0);
    Particle Wp("", ParticleType("Wp"), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{Wp, vebar, e};

    this->fill(to_fill);
    this->set_name("Wp_e_vebar");
    this->set_colorordered(false);
    this->set_coupling(Coupling{"gw"}, 1);
}
bool CurrentRule_Wp_e_vebar::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqg_template(out, other, false);
}

void CurrentRule_gqqL::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::qq_g_or_s(c); }
void CurrentRule_gqqR::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::qq_g_or_s(c); }
bool CurrentRule_gqqL::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqg_template(out, other);
}
bool CurrentRule_gqqR::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqg_template(out, other);
}
bool CurrentRule_pqq::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::check_qed_charge(out, other) && current_helper::qqg_template(out, other);
}
bool CurrentRule_pll::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::check_qed_charge(out, other) && current_helper::qqg_template(out, other);
}
bool CurrentRule_gttL::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqg_template(out,other);
}
bool CurrentRule_gttR::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqg_template(out,other);
}
} // namespace Caravel
