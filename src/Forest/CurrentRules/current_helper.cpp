#include "Forest/CurrentRules/current_helper.h"
#include "Core/Debug.h"
#include "Forest/CurrentRule.h"
#include "Forest/Forest.h"
#include <cassert>

namespace Caravel {
namespace current_helper {

bool qqg_template(Particle& out, const std::vector<Current*>& other, bool check_same_flavor) {
    if (out.get_statistics() == ParticleStatistics::vector) {
        if (check_same_flavor && other.at(0)->get_current_type().get_flavor() != other.at(1)->get_current_type().get_anti_flavor()) return false;
        if (other.at(0)->get_internal_index() != other.at(1)->get_internal_index()) return false;

        // Check representations for Weyl fermions
        if (!is_dirac(other.at(0)->get_current_type().get_flavor()) and !is_dirac(other.at(1)->get_current_type().get_flavor())) {
            auto s1 = other.at(0)->get_current_particle().external_state();
            auto s2 = other.at(1)->get_current_particle().external_state();
            if (s1.get_representation() != flip(s2.get_representation())) {
                DEBUG_MESSAGE("Incompatible representation in a vertex!");
                return false;
            }
        }
    }
    else {
        const Current* p = nullptr;
        for (auto it : other) {
            if (it->get_statistics() == ParticleStatistics::antifermion or it->get_statistics() == ParticleStatistics::fermion) p = it;
        }
        if (p == nullptr) throw std::runtime_error("Unexpected particle content while qqg_template matching.");
        if (check_same_flavor) { out.set_flavor(p->get_current_type().get_flavor()); }
        out.set_internal_index(p->get_internal_index());
        out.set_dtt_index(p->get_current_particle().external_state().get_representation());
    }

    return true;
}

bool check_qed_charge(Particle& out, const std::vector<Current*>& other) {
    assert(other.size() == 2);
    if (out.get_statistics() == ParticleStatistics::vector) {
        std::set<decltype(QED_charge(ParticleFlavor::e))> charges;
        for (auto p : other) {
            auto charge = QED_charge(p->get_current_type().get_flavor());
            if (charge.first == 0) return false;
            charges.insert(charge);
        }
        if (charges.size() != 1) return false;
    }
    else {
        const Current* p = nullptr;
        for (auto it : other) {
            if (it->get_statistics() == ParticleStatistics::antifermion or it->get_statistics() == ParticleStatistics::fermion) p = it;
        }
        if (p == nullptr) throw std::runtime_error("Unexpected particle content in check_qed_charge.");
        if (QED_charge(out.get_flavor()) != QED_charge(p->get_current_type().get_flavor())) return false;
    }

    return true;
}

bool qqs_template(Particle& out, const std::vector<Current*>& other) {
    if (out.get_flavor() == ParticleFlavor::ds_scalar) {
        if (other.at(0)->get_current_type().get_flavor() != other.at(1)->get_current_type().get_anti_flavor()) return false;
        if (other.at(0)->get_internal_index() != other.at(1)->get_internal_index()) return false;
        auto s1 = other.at(0)->get_current_particle().external_state();
        auto s2 = other.at(1)->get_current_particle().external_state();
        if (s1.get_representation() != s2.get_representation()) {
            DEBUG_MESSAGE("Incompatible representation in a vertex!");
            return false;
        }
        // we set the scalar index which represents a scalar spawned from a quark line,
        // later there will be an attempt to contract it with something to assign
        // qqs_powers
        out.set_internal_index(current_helper::_default_scalar_contract_index);
    }
    else {
        const Current* p = nullptr;
        for (auto it : other) {
            if (it->get_statistics() == ParticleStatistics::antifermion or it->get_statistics() == ParticleStatistics::fermion) p = it;
        }
        if (p == nullptr) throw std::runtime_error("Unexpected particle content while qqs_template matching.");
        out.set_flavor(p->get_current_type().get_flavor());
        out.set_internal_index(p->get_internal_index());
        out.set_dtt_index(flip(p->get_current_particle().external_state().get_representation()));
    }

    return true;
}

bool ss_template(Particle& out, const std::vector<Current*>& other) {
    std::vector<const Current*> found_scalars;
    for (auto it : other)
        if (it->get_statistics() == ParticleStatistics::scalar) { found_scalars.push_back(it); }

    if (found_scalars.size() == 2) {
        assert(out.get_statistics() == ParticleStatistics::vector);
        // we default scalar index matches anything, so we skip the check if one of scalars have it
        if (found_scalars.at(0)->get_internal_index() == current_helper::_default_scalar_contract_index or
            found_scalars.at(1)->get_internal_index() == current_helper::_default_scalar_contract_index)
            return true;
        if (found_scalars.at(0)->get_internal_index() != found_scalars.at(1)->get_internal_index()) return false;
    }
    else if (found_scalars.size() == 1) {
        assert(out.get_statistics() == ParticleStatistics::scalar);
        out.set_internal_index(found_scalars.front()->get_internal_index());
    }
    else {
        throw std::runtime_error("Unexpected particle content in ggss vertex");
    }
    return true;
}

namespace update_qqs_couplings {

void generic(CurrentBasic* cur) {
    using std::vector;

    for (auto c : *cur->get_parents()) {
        // count only contribution from curents which are not closed
        if (!c->get_leaves().propagator_dropped()) {
            for (auto it : c->qqs_powers) {
#if DEBUG_ON
                if (!cur->qqs_powers[it.first].empty()) {
                    _WARNING_R("Violated expectation of zero intersection of internal indices of parents!");
                    exit(1);
                }
#endif
                cur->qqs_powers[it.first] = it.second;
            }
        }
    }
}

/**
 * Finds uncontracted scalars in qqs_powers of a current
 * Returns pointers
 */
template <typename Tqqs> std::pair<int, int*> find_uncontracted_scalars(Tqqs& qqs_powers) {
    std::map<int, std::vector<int*>> locations;

    int N = 0;

    for (auto& line : qqs_powers) {
        for (auto& ind : line.second) {
            if (ind == current_helper::_default_scalar_contract_index) {
                locations[line.first].push_back(&ind);
                ++N;
            }
        }
    }

    if (N > 1) {
        _WARNING_R("Found more than one uncontracted scalar!)");
        std::exit(1);
    }

    return {locations.begin()->first, locations.begin()->second.front()};
}

void qq_g_or_s(CurrentBasic* cur) {
    int q_pos = -1;
    int qb_pos = -1;

    assert(cur->rule->get_rule_multiplicity() == 3);

    const auto& v = *cur->get_parents();
    const auto& p = cur->get_current_particle();

    for (size_t n = 0; n < v.size(); ++n) {
        const auto& fl = v[n]->get_current_type().get_flavor();
        if (in_container<ParticleFlavor::qb>(fl) and !is_dirac(fl))
            qb_pos = n;
        else if (in_container<ParticleFlavor::q>(fl) and !is_dirac(fl))
            q_pos = n;
    }

    auto& qqs_powers = cur->qqs_powers;

    assert(qqs_powers.empty());

    DEBUG_PRINT(cur->get_name());
    DEBUG_PRINT(v[0]->get_name());
    DEBUG_PRINT(v[1]->get_name());
    if (!v[0]->get_leaves().propagator_dropped()) { DEBUG_PRINT(v[0]->qqs_powers); }
    if (!v[1]->get_leaves().propagator_dropped()) { DEBUG_PRINT(v[1]->qqs_powers); }

    for (size_t n = 0; n < v.size(); ++n) {
        const auto& fl = v[n]->get_current_type().get_flavor();
        if (in_container<ParticleFlavor::qb>(fl) and !is_dirac(fl))
            qb_pos = n;
        else if (in_container<ParticleFlavor::q>(fl) and !is_dirac(fl))
            q_pos = n;
    }

    auto add_scalar_vertex = [&qqs_powers](Current* q, Current* s) {
        if (s->get_internal_index() == current_helper::_default_scalar_contract_index) {
            auto un_s = find_uncontracted_scalars(qqs_powers);
            *un_s.second = current_helper::_default_scalar_contract_index + q->get_internal_index();
            qqs_powers[q->get_internal_index()].push_back(current_helper::_default_scalar_contract_index + un_s.first);
        }
        else {
            qqs_powers[q->get_internal_index()].push_back(s->get_internal_index());
        }
    };

    if (q_pos >= 0 and qb_pos >= 0) {
        if (!v[qb_pos]->get_leaves().propagator_dropped())
            for (auto it : v[qb_pos]->qqs_powers) { qqs_powers[it.first].insert(qqs_powers[it.first].end(), it.second.begin(), it.second.end()); }
        if (p.get_flavor() == ParticleFlavor::ds_scalar) {
            qqs_powers[v[qb_pos]->get_internal_index()].push_back(current_helper::_default_scalar_contract_index);
        }
        if (!v[q_pos]->get_leaves().propagator_dropped())
            for (auto it : v[q_pos]->qqs_powers) { qqs_powers[it.first].insert(qqs_powers[it.first].end(), it.second.begin(), it.second.end()); }
    }

    else if (q_pos >= 0 and qb_pos == -1) {
        int s_pos = (q_pos + 1) % 2;

        // from the scalar current add trivially
        if (!v[s_pos]->get_leaves().propagator_dropped()) { qqs_powers = v[s_pos]->qqs_powers; }

        if (v[s_pos]->get_current_type().get_flavor() == ParticleFlavor::ds_scalar) { add_scalar_vertex(v[q_pos], v[s_pos]); }
        if (!v[q_pos]->get_leaves().propagator_dropped())
            for (auto it : v[q_pos]->qqs_powers) { qqs_powers[it.first].insert(qqs_powers[it.first].end(), it.second.begin(), it.second.end()); }
    }

    else if (q_pos == -1 and qb_pos >= 0) {
        int s_pos = (qb_pos + 1) % 2;

        // from the scalar current add trivially
        if (!v[s_pos]->get_leaves().propagator_dropped()) { qqs_powers = v[s_pos]->qqs_powers; }

        if (!v[qb_pos]->get_leaves().propagator_dropped())
            for (auto it : v[qb_pos]->qqs_powers) { qqs_powers[it.first].insert(qqs_powers[it.first].end(), it.second.begin(), it.second.end()); }

        if (v[s_pos]->get_current_type().get_flavor() == ParticleFlavor::ds_scalar) { add_scalar_vertex(v[qb_pos], v[s_pos]); }
    }

    DEBUG_PRINT(qqs_powers);
}

void ss(CurrentBasic* cur) {

    // first we propagate trivially like for generic vertices
    current_helper::update_qqs_couplings::generic(cur);

    auto& qqs_powers = cur->qqs_powers;
    const auto& out = cur->get_current_particle();

    std::vector<Current*> found_scalars;
    for (auto it : *cur->get_parents())
        if (it->get_statistics() == ParticleStatistics::scalar) { found_scalars.push_back(it); }

    // if we merge two scalars and we one of them came from a quark line we need to contract
    // the index of the quark line
    if (found_scalars.size() == 2) {
        assert(out.get_statistics() == ParticleStatistics::vector);
        int s1ind = found_scalars.at(0)->get_internal_index();
        int s2ind = found_scalars.at(1)->get_internal_index();
        // at this point indices should have been assigned and the only case when we don't have
        // a strict equality is when scalars emmited from quark lines are here
        if (s1ind != s2ind) {
            std::vector<int*> locations;
            for (auto& line : qqs_powers) {
                for (auto& ind : line.second) {
                    if (ind == current_helper::_default_scalar_contract_index) locations.push_back(&ind);
                }
            }

            if (locations.size() > 1) {
                _WARNING_R("Found an ambiguity in possible constraction of indices!)");
                std::exit(1);
            }

            if (s1ind == current_helper::_default_scalar_contract_index) { *locations.front() = s2ind; }
            else if (s2ind == current_helper::_default_scalar_contract_index) {
                *locations.front() = s1ind;
            }
#ifdef DEBUG_ON
            else {
                _WARNING_R("Internal inconcistency!");
                std::exit(1);
            }
#endif
        }
        else if (s1ind == current_helper::_default_scalar_contract_index && s2ind == current_helper::_default_scalar_contract_index) {
            std::map<int, std::vector<int*>> locations;
            for (auto& line : qqs_powers) {
                for (auto& ind : line.second) {
                    if (ind == current_helper::_default_scalar_contract_index) locations[line.first].push_back(&ind);
                }
            }

            if (!(locations.size() == 2 && locations.begin()->second.size() == 1 && std::next(locations.begin())->second.size() == 1)) {
                _WARNING_R("Unexpected structure encountered when two quark lines are connected by a scalar line.");
                std::exit(1);
            }

            auto l1 = locations.begin();
            auto l2 = std::next(locations.begin());

            *l1->second.front() = current_helper::_default_scalar_contract_index + l2->first;
            *l2->second.front() = current_helper::_default_scalar_contract_index + l1->first;
        }
    }
}

} // namespace update_qqs_couplings
} // namespace current_helper
} // namespace Caravel
