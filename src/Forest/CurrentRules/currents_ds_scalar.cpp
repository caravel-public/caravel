#include "currents_ds_scalar.h"
#include "Core/Debug.h"
#include "current_helper.h"
#include <cassert>

namespace Caravel {

CurrentRule_gss::CurrentRule_gss() {
    ParticleType sc = ParticleType(ParticleStatistics::scalar, ParticleFlavor::ds_scalar, ParticleMass{}, true);

    Particle g1("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle g2("", sc, SingleState("NONMOD"), 0, 0);
    Particle g3("", sc, SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{g1, g2, g3};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("gss");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    this->set_coupling(g, 1);
}

CurrentRule_ggss::CurrentRule_ggss() {
    Particle gl("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle sc("", ParticleType(ParticleStatistics::scalar, ParticleFlavor::ds_scalar, ParticleMass{}, true), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{gl, gl, sc, sc};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ggss");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    this->set_coupling(g, 2);
}

CurrentRule_gsgs::CurrentRule_gsgs() {
    Particle gl("", ParticleType("gluon"), SingleState("NONMOD"), 0, 0);
    Particle sc("", ParticleType(ParticleStatistics::scalar, ParticleFlavor::ds_scalar, ParticleMass{}, true), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{gl, sc, gl, sc};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("gsgs");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    this->set_coupling(g, 2);
}

CurrentRule_ssss::CurrentRule_ssss() {
    Particle sc("", ParticleType(ParticleStatistics::scalar, ParticleFlavor::ds_scalar, ParticleMass{}, true), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{sc, sc, sc, sc};

    // fill the containers (from base class)
    this->fill(to_fill);

    // Give a name to the CurrentRule
    this->set_name("ssss");

    // Define if color ordered vertex
    this->set_colorordered(true);

    // Now define coupling and corresponding power
    Coupling g("gs");
    this->set_coupling(g, 2);
}

CurrentRule_sqqL::CurrentRule_sqqL() {
    DEBUG_MESSAGE("Constructed a sqqL color ordered vertex");

    Particle qb("", ParticleType("qb"), SingleState("NONMOD"), 0, 0);
    Particle q("", ParticleType("q"), SingleState("NONMOD"), 0, 0);
    Particle sc("", ParticleType(ParticleStatistics::scalar, ParticleFlavor::ds_scalar, ParticleMass{}, true), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{qb, q, sc};

    this->fill(to_fill);
    this->set_name("sqqL");
    this->set_colorordered(true);
    Coupling g("gs");
    this->set_coupling(g, 1);
}

CurrentRule_sqqR::CurrentRule_sqqR() {
    DEBUG_MESSAGE("Constructed a sqqR color ordered vertex");

    Particle qb("", ParticleType("qb"), SingleState("NONMOD"), 0, 0);
    Particle q("", ParticleType("q"), SingleState("NONMOD"), 0, 0);
    Particle sc("", ParticleType(ParticleStatistics::scalar, ParticleFlavor::ds_scalar, ParticleMass{}, true), SingleState("NONMOD"), 0, 0);

    std::vector<Particle> to_fill{q, qb, sc};

    this->fill(to_fill);
    this->set_name("sqqR");
    this->set_colorordered(true);
    Coupling g("gs");
    this->set_coupling(g, 1);
}

void CurrentRule::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::generic(c); }
void CurrentRule_sqqL::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::qq_g_or_s(c); }
void CurrentRule_sqqR::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::qq_g_or_s(c); }
void CurrentRule_gss::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::ss(c); }
void CurrentRule_ggss::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::ss(c); }
void CurrentRule_gsgs::update_qqs_couplings(CurrentBasic* c, int) const { current_helper::update_qqs_couplings::ss(c); }
bool CurrentRule_sqqL::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqs_template(out, other);
}
bool CurrentRule_sqqR::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::qqs_template(out, other);
}

bool CurrentRule_gss::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::ss_template(out, other);
}
bool CurrentRule_ggss::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::ss_template(out, other);
}
bool CurrentRule_gsgs::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>&) const {
    return current_helper::ss_template(out, other);
}

// here we interpret the second argument perm as a prescription of how direct the flow of indices
bool CurrentRule_ssss::propagate_indices(Particle& out, const std::vector<Current*>& other, const std::vector<size_t>& perm) const {
    // we try to match generically
    if (perm.empty()) {
        // trivial internal indices are not allowed in 4s vertex since we have to know the routing
        for (auto it : other) { assert(it->get_internal_index() != 0); }
        std::vector<int> found_non_matching;
        for (auto p : other) {
            assert(p->get_statistics() == ParticleStatistics::scalar);
            bool matched = false;
            for (auto it : other) {
                if (p != it and p->get_internal_index() == it->get_internal_index()) {
                    matched = true;
                    break;
                }
            }
            if (!matched) found_non_matching.push_back(p->get_internal_index());
        }

        switch (found_non_matching.size()) {
            case 0: _WARNING_R("4 scalar vertex with no internal indices assigned!"); __FALLTHROUGH;
            case 1: out.set_internal_index(found_non_matching.front()); return true;
            default: return false;
        }
    }
    // we match according to the provided scheme
    else {
        auto match = [&](Current* c1, Current* c2) {
            if (c1->get_internal_index() == current_helper::_default_scalar_contract_index or
                c2->get_internal_index() == current_helper::_default_scalar_contract_index)
                return true;
            if (c1->get_internal_index() == c2->get_internal_index()) return true;
            DEBUG_MESSAGE("Indices in rule ssss didn't match ");
            return false;
        };

        switch (perm.front()) {
            case 1: // adjacent with the current particle to the left
                if (!match(other.at(1), other.at(2))) return false;
                out.set_internal_index(other.at(0)->get_internal_index());
                break;
            case 2: // adjacent with the current particle to the right
                if (!match(other.at(0), other.at(1))) return false;
                out.set_internal_index(other.at(2)->get_internal_index());
                break;
            case 3: // crossed
                if (!match(other.at(0), other.at(2))) return false;
                out.set_internal_index(other.at(1)->get_internal_index());
                break;
            default: _WARNING_R("Internal inconcistency"); std::exit(1);
        }
        return true;
    }

    return false;
}

void CurrentRule_ssss::update_qqs_couplings(CurrentBasic* cur, int mode) const {
    // first we propagate trivially like for generic vertices
    current_helper::update_qqs_couplings::generic(cur);

    size_t ndefaults = 0;
    auto& v = *cur->get_parents();
    for (auto it : v)
        if (it->get_internal_index() == current_helper::_default_scalar_contract_index) ++ndefaults;

    // nothing else we need to do
    if (ndefaults == 0) return;

    if (ndefaults > 0 && mode == 0) {
        _WARNING_R("Not implemented!");
        std::exit(1);
    }

    auto& qqs_powers = cur->qqs_powers;
    const auto& out = cur->get_current_particle();

    using qline = decltype(cur->qqs_powers);

    auto combine_qlines = [](qline qline1, int scind1, qline qline2, int scind2) -> qline {
        qline result;

        for (auto&& c : {qline1, qline2}) {
            // count only contribution from curents which are not closed
            for (auto it : c) {
                if (result.count(it.first) > 0) {
                    _WARNING_R("Quark lines should not intersect here!\n\t", qline1, qline2);
                    std::exit(1);
                }
                result[it.first] = it.second;
            }
        }

        // asked to just combine
        if (scind1 == -1 and scind2 == -1) return result;

        // now look for possible contractions between lines
        std::vector<std::pair<int*, int>> locations;

        for (auto& ql : result) {
            for (auto& it : ql.second) {
                if (it == current_helper::_default_scalar_contract_index) locations.emplace_back(&it, ql.first);
            }
        }

        switch (locations.size()) {
            case 0: break;
            case 1:
                if (scind1 == current_helper::_default_scalar_contract_index) { *locations[0].first = scind2; }
                else if (scind2 == current_helper::_default_scalar_contract_index) {
                    *locations[0].first = scind1;
                }
                else {
                    _WARNING_R("Inconsistency in combining quark lines found!\n\t", qline1, scind1, qline2, scind2);
                    std::exit(1);
                }
                break;
            case 2:
                (*locations[0].first) += locations[1].second;
                (*locations[1].first) += locations[0].second;
                break;
            default: _WARNING_R("Inconsistency in combining quark lines found!\n\t", qline1, scind1, qline2, scind2); std::exit(1);
        }

        return result;
    };

    std::vector<int> parent_indices{cur->get_assigned_internal_index(0), cur->get_assigned_internal_index(1), cur->get_assigned_internal_index(2)};

    auto contract_pair = [&parent_indices](int i1, int i2) {
        int ind1 = parent_indices.at(i1);
        int ind2 = parent_indices.at(i2);
        if (ind1 == current_helper::_default_scalar_contract_index)
            parent_indices[i1] = ind2;
        else if (ind2 == current_helper::_default_scalar_contract_index)
            parent_indices[i2] = ind1;
    };

    std::array<int, 3> scheme;

    switch (mode) {
        case 1: scheme = {0, 1, 2}; break;
        case 2: scheme = {2, 0, 1}; break;
        case 3: scheme = {1, 0, 2}; break;
        default: std::exit(1);
    }

    auto ql1 = combine_qlines({}, out.get_internal_index(), v.at(scheme[0])->qqs_powers, v.at(scheme[0])->get_internal_index());
    auto ql2 =
        combine_qlines(v.at(scheme[1])->qqs_powers, v.at(scheme[1])->get_internal_index(), v.at(scheme[2])->qqs_powers, v.at(scheme[2])->get_internal_index());

    qqs_powers = combine_qlines(ql1, -1, ql2, -1);
    contract_pair(scheme[1], scheme[2]);

    cur->set_internal_indices_parents(parent_indices);
}

} // namespace Caravel
