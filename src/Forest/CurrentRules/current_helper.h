#include "Core/Particle.h"
#include "Forest/Forest.h"
#include <vector>

namespace Caravel {
namespace current_helper {
inline constexpr int _default_scalar_contract_index = 1000;
bool qqg_template(Particle& out, const std::vector<Current*>& other, bool check_same_flavor = true);
bool check_qed_charge(Particle& out, const std::vector<Current*>& other);
bool qqs_template(Particle& out, const std::vector<Current*>& other);
bool ss_template(Particle& out, const std::vector<Current*>& other);

namespace update_qqs_couplings {
void generic(CurrentBasic* cur);
template <typename Tqqs> std::pair<int, int*> find_uncontracted_scalars(Tqqs& qqs_powers);
void qq_g_or_s(CurrentBasic* cur);
void ss(CurrentBasic* cur);
} // namespace update_qqs_couplings
} // namespace current_helper
} // namespace Caravel
