/**
 * @file Model.cpp
 *
 * @date 26.2.2016
 *
 * @brief Implementation of abstract Model functions
 *
 */

#include "Forest/Model.h"
#include "Core/Debug.h"
#include "Core/Utilities.h"
#include "Core/settings.h"
#include <iostream>

namespace Caravel {

void ParticleStates::add_state(const ParticleType& p, size_t Dim, const std::vector<std::pair<SingleState, SingleState>>& pstates) {
    size_t p_entry(0);
    size_t dim_entry(0);
    // new particle?
    if (std::find(types_of_particles.begin(), types_of_particles.end(), p) == types_of_particles.end()) {
        DEBUG_MESSAGE("Particle type: ", p, " is new to *this in ParticleStates::add_state(.)");
        p_entry = types_of_particles.size();
        dim_entry = 0;
        // add corresponding entries to *this
        types_of_particles.push_back(p);
        types_of_antiparticles.push_back(p.get_anti_type());
        dimensions.push_back(std::vector<size_t>());
        dimensions.back().push_back(Dim);
        // add array for particle
        states.push_back(std::vector<std::vector<std::pair<SingleState, SingleState>>>());
        // and vector for Dim
        states.back().push_back(std::vector<std::pair<SingleState, SingleState>>());
    }
    else {
        p_entry = std::find(types_of_particles.begin(), types_of_particles.end(), p) - types_of_particles.begin();
        // find if Dim is new
        if (std::find(dimensions[p_entry].begin(), dimensions[p_entry].end(), Dim) == dimensions[p_entry].end()) {
            dim_entry = dimensions[p_entry].size();
            dimensions[p_entry].push_back(Dim);
            states[p_entry].push_back(std::vector<std::pair<SingleState, SingleState>>());
        }
        else {
            dim_entry = std::find(dimensions[p_entry].begin(), dimensions[p_entry].end(), Dim) - dimensions[p_entry].begin();
        }
    }
    // add states that are not present
    for (size_t ii = 0; ii < pstates.size(); ii++) {
        if (std::find(states[p_entry][dim_entry].begin(), states[p_entry][dim_entry].end(), pstates[ii]) == states[p_entry][dim_entry].end()) {
            states[p_entry][dim_entry].push_back(pstates[ii]);
        }
    }
}

std::vector<std::pair<SingleState, SingleState>> ParticleStates::get_states(const ParticleType& name, size_t Dim) const {
    std::vector<std::pair<SingleState, SingleState>> toret;
    size_t p_entry(0);
    size_t dim_entry(0);
    auto comp = [&name](const ParticleType& p) { return almost_equal(p, name); };

    if (std::find_if(types_of_particles.begin(), types_of_particles.end(), comp) != types_of_particles.end()) {
        p_entry = std::find_if(types_of_particles.begin(), types_of_particles.end(), comp) - types_of_particles.begin();
        if (std::find(dimensions[p_entry].begin(), dimensions[p_entry].end(), Dim) != dimensions[p_entry].end()) {
            dim_entry = std::find(dimensions[p_entry].begin(), dimensions[p_entry].end(), Dim) - dimensions[p_entry].begin();
            toret = states[p_entry][dim_entry];
        }
        else {
            std::cerr << "ERROR: requested states for " << name << " in dimension: " << Dim << " when only: " << dimensions[p_entry] << " dimensions included"
                      << std::endl;
            std::cerr << "review your Model for dimensions in 'add_vector_boson_states', 'add_fermionic_states', 'add_tensor_boson_states', etc! " << std::endl;
            std::exit(1);
        }
    }
    else if (std::find_if(types_of_antiparticles.begin(), types_of_antiparticles.end(), comp) != types_of_antiparticles.end()) {
        p_entry = std::find_if(types_of_antiparticles.begin(), types_of_antiparticles.end(), comp) - types_of_antiparticles.begin();
        if (std::find(dimensions[p_entry].begin(), dimensions[p_entry].end(), Dim) != dimensions[p_entry].end()) {
            dim_entry = std::find(dimensions[p_entry].begin(), dimensions[p_entry].end(), Dim) - dimensions[p_entry].begin();
            toret = states[p_entry][dim_entry];
            std::transform(toret.begin(), toret.end(), toret.begin(), [](std::pair<SingleState, SingleState> e) { return std::make_pair(e.second, e.first); });
        }
        else {
            std::cerr << "ERROR: requested states for " << name << " in dimension: " << Dim << " when only: " << dimensions[p_entry] << " dimensions included"
                      << std::endl;
            std::cerr << "review your Model for dimensions in 'add_vector_boson_states', 'add_fermionic_states', 'add_tensor_boson_states', etc! " << std::endl;
            std::exit(1);
        }
    }
    else {
        std::cerr << "ERROR: requested states for ParticleType " << name << " which isn't included in your Model" << std::endl;
        std::exit(1);
    }
#if 0
std::cout<<"When asking for particle: "<<name<<" in Dim: "<<Dim<<" ParticleStates::get_states(.) returned the following "<<toret.size()<<" correlated pairs: "<<std::endl;
for(auto& pp:toret)	std::cout<<pp<<" ";
std::cout<<std::endl;
#endif
    return toret;
}

std::vector<std::pair<SingleState, SingleState>> Model::get_states(const ParticleType& PT, size_t Dim) const { return particle_states.get_states(PT, Dim); }

Model::Model(const Model& in) : v_couplings(in.v_couplings), particle_states(in.particle_states), name(in.name) {
    for (auto cr : v_rules) delete cr;
    v_rules.clear();
    for (auto cr : in.v_rules) v_rules.push_back(cr->clone());
}

Model& Model::operator=(const Model& in) {
    v_couplings = in.v_couplings;
    particle_states = in.particle_states;
    name = in.name;

    for (auto cr : v_rules) delete cr;
    v_rules.clear();
    for (auto cr : in.v_rules) v_rules.push_back(cr->clone());

    return *this;
}

Model::~Model() {
    // v_couplings.clear();
    for (std::vector<CurrentRule*>::iterator it = v_rules.begin(); it != v_rules.end(); it++) delete *it;
    // v_rules.clear();
}

const std::vector<Coupling>& Model::v_of_couplings() const { return v_couplings; }

const std::vector<CurrentRule*>& Model::v_of_rules() const { return v_rules; }

Model* model_from_name(std::string name) {
#define _add_entry_with_name(n)                                                                                                                                \
    if (name == #n) return new n{};

    _add_entry_with_name(SM_color_ordered);
    _add_entry_with_name(YM);
    _add_entry_with_name(CubicGravity);
    _add_entry_with_name(EHGravity);
    _add_entry_with_name(YMwTensor);
#undef _add_entry_with_name
    throw std::runtime_error("Unknown model " + name + " in model_from_name");
}

namespace Model_Helper {
// explicit specialization for Ds=4 || VECTOR BOSONS
template <> std::vector<std::pair<SingleState, SingleState>> add_vector_boson_states_impl<4>() {
    std::vector<std::pair<SingleState, SingleState>> states;

    states.emplace_back(SingleState("m"), SingleState("p"));
    states.emplace_back(SingleState("p"), SingleState("m"));

    return states;
}
// explicit specialization for Ds=4 || TENSORS
template <> std::vector<std::pair<SingleState, SingleState>> add_tensor_boson_states_impl<4>() {
    std::vector<std::pair<SingleState, SingleState>> states;

    states.emplace_back(SingleState("hmm"), SingleState("hpp"));
    states.emplace_back(SingleState("hpp"), SingleState("hmm"));
    states.emplace_back(SingleState("hWard"), SingleState("hWard"));
    states.emplace_back(SingleState("hmID"), SingleState("hmID"));
    states.emplace_back(SingleState("hpID"), SingleState("hpID"));
    states.emplace_back(SingleState("hIID"), SingleState("hIID"));
    return states;
}
} // namespace Model_Helper
} // namespace Caravel
