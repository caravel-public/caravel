/**
 * @file Forest_utils.cpp 
 *
 * @date 5.5.2017
 *
 * @brief Implementing several classes used as members in Forest
 *
 * Keeping accessory utilities for the off-shell recursions
 *
*/

#include <iostream>
#include <algorithm>

#include "Forest/Forest.h"
#include "Core/Debug.h"
#include "Forest/Model.h"

namespace Caravel {

CurrentPartitions::CurrentPartitions(): maximum_n(0){
}

// Given a multiplicity n, and a number m of integers, returns a list of how n can be written in terms of m integers k (with 0<k<=max)
CurrentPartitionEntries partitions_of_currents(size_t n,size_t m,size_t max){
	std::vector<std::vector<size_t> > v2return;
	if(m>n)	return v2return;
	if(m==n){
		std::vector<size_t> toadd;
		for(size_t k=0;k<n;k++)	toadd.push_back(1);
		v2return.push_back(toadd);
		return v2return;
	};
	if(m==1){
		std::vector<size_t> toadd;
		toadd.push_back(n);
		v2return.push_back(toadd);
		return v2return;
	};
	for(size_t k=n-1;k>1 && k*m>=n;k--){
	  if(k<=max){
		std::vector<std::vector<size_t> > lv2return;
		lv2return=partitions_of_currents(n-k,m-1,k);
		std::vector<size_t> toadd;
		for(size_t l=0;l<lv2return.size();l++){
			toadd=lv2return[l];
			toadd.push_back(k);
			v2return.push_back(toadd);
		};
	  };
	};
	return v2return;
}

CurrentPartitionEntries partitions_of_currents(size_t n,size_t m){
	return partitions_of_currents(n,m,n);
}

const CurrentPartitionEntries& CurrentPartitions::get_partitions(size_t n,size_t m){
	if(n>maximum_n){
		// add space in the array store
		for(size_t ii=maximum_n;ii<n;ii++){
			store.push_back(std::vector<CurrentPartitionEntries>());
			maximum_m_for_each_n.push_back(0);
		}
		maximum_n=store.size();
//std::cout<<"When receiving: CurrentPartitions::get_partitions("<<n<<","<<m<<"), we end up with a store with: "<<maximum_n<<" entries, as well as maximum_m_for_each_n.size()= "<<maximum_m_for_each_n.size()<<std::endl;
		// and now fill the corresponding 
		for(size_t ii=0;ii<m;ii++){
			DEBUG_MESSAGE("Computing partitions with n,m = ",n,",",ii+1);
			store[maximum_n-1].push_back(partitions_of_currents(n,ii+1));
		}
		maximum_m_for_each_n[maximum_n-1]=m;
	}
	else if(m>maximum_m_for_each_n[n-1]){
		// fill array
		for(size_t ii=store[n-1].size();ii<m;ii++){
			DEBUG_MESSAGE("Computing partitions with n,m = ",n,",",ii+1);
			store[n-1].push_back(partitions_of_currents(n,ii+1));
		}
//std::cout<<"For level: "<<n<<" we filled up up to "<<store[n-1].size()<<" entries, with the request m= "<<m<<std::endl;
		maximum_m_for_each_n[n-1]=m;
	}
#if 0
// Look at partitions:
std::cout<<"Final return for n= "<<n<<" and m= "<<m<<std::endl;
for(size_t tt=0;tt<store[n-1][m-1].size();tt++){
	std::cout<<"n= "<<n<<", m= "<<m<<" partition "<<tt<<": [ ";
	for(size_t pp=0;pp<store[n-1][m-1][tt].size();pp++){
		std::cout<<store[n-1][m-1][tt][pp]<<" ";
	};
	std::cout<<"]"<<std::endl;
};
#endif
	DEBUG_MESSAGE("Returning partitions with n,m = ",n,",",m);
	return store[n-1][m-1];
}

std::string get_internal_index_vertex_string(const InternalIndexVertex& iiv){
    std::string toret;
    iiv.needs_color_handling ? (toret+="true") : (toret+="false");
    toret+="("+std::to_string(iiv.index_left)+")";
    toret+="{";
    for(size_t jj=0;jj<iiv.all_colored_internal.size();jj++){
        toret+=std::to_string(iiv.all_colored_internal[jj]);
        if(jj+1!=iiv.all_colored_internal.size())
            toret+=",";
    }
    toret+="}{";
    for(size_t jj=0;jj<iiv.all_colorless_internal.size();jj++){
        toret+=std::to_string(iiv.all_colorless_internal[jj]);
        if(jj+1!=iiv.all_colorless_internal.size())
            toret+=",";
    }
    toret+="}("+std::to_string(iiv.index_right)+")";
    return toret;
}

std::ostream& operator<<(std::ostream& s, const InternalIndexVertex& iiv){
	s<<get_internal_index_vertex_string(iiv);
	return s;
}

LeavesContainer::LeavesContainer(){}

LeavesContainer::LeavesContainer(Current*c){
	if(c->is_colored())
		colored.push_back(c);
	else
		colorless.insert(std::vector<Current*>{c});
        sorted_currents.push_back({c});
}

void LeavesContainer::clear(){
	colored.clear();
	colorless.clear();
        sorted_currents.clear();
}

size_t LeavesContainer::size()const{
    size_t toret=colored.size();
    for(auto&ncv:colorless)
        toret+=ncv.size();
    return toret;
}

bool operator==(const LeavesContainer& a,const LeavesContainer& b){
    return a.get_sorted_currents() == b.get_sorted_currents();
}

const std::vector<Current*>& LeavesContainer::get_colored() const {
  return colored;
}

const std::set<std::vector<Current*> >& LeavesContainer::get_colorless() const {
  return colorless;
}

const std::vector<std::vector<Current*>>& LeavesContainer::get_sorted_currents() const {
    return sorted_currents;
}

void LeavesContainer::add(const LeavesContainer&in,bool is_colored) {
	colorless.insert(in.get_colorless().begin(),in.get_colorless().end());
	if(is_colored)
		colored.insert(colored.end(),in.get_colored().begin(),in.get_colored().end());
	else if(in.get_colored().size())
		colorless.insert(in.get_colored());
        sorted_currents.clear();
        for (auto& v : colorless) sorted_currents.push_back(v);
        sorted_currents.push_back(colored);
        std::sort(sorted_currents.begin(), sorted_currents.end());
}

std::string LeavesContainer::get_short_name() const {
	std::string toret;
	if(colorless.size()>0) {
		toret+="{";
		for(auto ncvIt=colorless.begin();ncvIt!=colorless.end();++ncvIt) {
			for(auto&nc:*ncvIt) {
				std::stringstream ss;
				ss << nc->get_current_particle();
				toret+= ss.str();
			}
			toret+=",";
		}
		toret+="}";
	}
	if(colored.size()>0)
		for(auto&c:colored) {
			std::stringstream ss;
			ss << c->get_current_particle();
			toret+= ss.str();
		}
	return toret;
}

void LeavesContainer::show() const {
	std::cout<<"Leaves container: "<<this->get_short_name()<<std::endl;
}

LeavesContainer::iterator::iterator(const std::set<std::vector<Current*> >::iterator&inItNcs,const std::set<std::vector<Current*> >::iterator&inItNce,const std::vector<Current*>::const_iterator&inItNcv,const std::vector<Current*>::const_iterator&inItCv):itNcs(inItNcs),itNce(inItNce),itNcv(inItNcv),itCv(inItCv){
}

LeavesContainer::iterator LeavesContainer::iterator::operator++(){
	if(itNcs!=itNce) {
		itNcv++;
		if(itNcv==itNcs->end()) {
			itNcs++;
			if(itNcs!=itNce)
				itNcv=itNcs->begin();
		}
	}
	else
		itCv++;
	return *this;
}

bool LeavesContainer::iterator::operator!=(const LeavesContainer::iterator&other)const{
	if(itNcs!=itNce)
		return itNcs!=other.itNcs||itNcv!=other.itNcv;
	else
		return itCv!=other.itCv;
}

Current* LeavesContainer::iterator::operator*()const{
	if(itNcs!=itNce)
		return *itNcv;
	else
		return *itCv;
}

LeavesContainer::iterator LeavesContainer::begin()const{
	return LeavesContainer::iterator(colorless.begin(),colorless.end(),colorless.begin()->begin(),colored.begin());
}

LeavesContainer::iterator LeavesContainer::end()const{
		return LeavesContainer::iterator(colorless.end(),colorless.end(),std::vector<Current*>::iterator(),colored.end());
}

CurrentLeaves::CurrentLeaves(): prop_dropped(false) {
}

CurrentLeaves::CurrentLeaves(Current*c,size_t mom_index): prop_dropped(false) {
	open_leaves=LeavesContainer(c);
	all_leaves.push_back(c);
	mom_indices.push_back(mom_index);
}

void CurrentLeaves::drop_propagator(){
	if(prop_dropped)
		return;
	if(open_leaves.size()==0){
		std::cout<<"Trying to drop props with zero open leaves! -- Did nothing"<<std::endl;
		return;
	}
	closed_leaves.push_back(open_leaves);
	open_leaves.clear();
	prop_dropped=true;
}

bool CurrentLeaves::propagator_dropped() const { return prop_dropped; }
bool CurrentIndices::propagator_dropped() const { return prop_dropped; }

void CurrentLeaves::add(const CurrentLeaves&in,bool is_colored) {
  if(in.closed_leaves.size()>0&&closed_leaves.size()>0){
		std::cout<<"Trying to multiply two CurrentLeaves both with propagator drops! -- did nothing!!"<<std::endl;
		return;
	}
	if(in.closed_leaves.size()>0)
		closed_leaves=in.closed_leaves;
	open_leaves.add(in.get_open_leaves(),is_colored);
	all_leaves.insert(all_leaves.end(),in.get_all_leaves().begin(),in.get_all_leaves().end());
	std::sort(all_leaves.begin(),all_leaves.end());
	mom_indices.insert(mom_indices.end(),in.get_all_momenta_indices().begin(),in.get_all_momenta_indices().end());
}

bool operator==(const InternalIndexVertex& i1,const InternalIndexVertex& i2){
//std::cout<<"TRY: "<<i1<<" "<<i2<<std::endl;
	bool tr(i1.needs_color_handling==i2.needs_color_handling &&
		i1.index_left==i2.index_left &&
		i1.index_right==i2.index_right &&
		i1.all_colored_internal==i2.all_colored_internal &&
		i1.all_colorless_internal==i2.all_colorless_internal
	    );
//std::cout<<"	return: "<<(tr?"true":"false")<<std::endl;
	return tr;
}

bool operator==(const CurrentLeaves& a,const CurrentLeaves& b){
    if(a.prop_dropped==b.prop_dropped)
        if(a.open_leaves==b.open_leaves)
            if(a.closed_leaves==b.closed_leaves)
                return true;
    return false;
}

bool CurrentLeaves::leaves_overlap(const CurrentLeaves& in) const {
	for(size_t ii=0;ii<all_leaves.size();ii++){
		if(std::find(in.all_leaves.begin(),in.all_leaves.end(),all_leaves[ii])!=in.all_leaves.end())
			return true;
	}
	return false;
}

const std::vector<Current*>& CurrentLeaves::get_all_leaves() const {
	return all_leaves;
}

const LeavesContainer& CurrentLeaves::get_open_leaves() const {
	return open_leaves;
}

const std::vector<size_t>& CurrentLeaves::get_all_momenta_indices() const {
	return mom_indices;
}

std::string CurrentLeaves::get_short_name() const {
	std::string toret("< ");
	for(size_t ii=0;ii<closed_leaves.size();ii++)
		toret+=closed_leaves[ii].get_short_name()+" || ";
	toret+=open_leaves.get_short_name();
	if(open_leaves.size()>0)
		toret+="--";
	return toret;
}

void CurrentLeaves::show() const {
	std::cout<<"Leaves: "<<this->get_short_name()<<std::endl;
}

CurrentIndices::CurrentIndices(const std::vector<Current*>& vc) {
    // fill information of internal indices of parents
    for(size_t ii=0;ii<vc.size();ii++)
        internal_indices_parents.push_back(vc[ii]->get_current_indices().get_dynamically_assigned_internal_index());

    // fill information on internal_indices_open
    bool non_trivial_internal_indices_open(false);
    InternalIndexVertex base;
    for(size_t ii=0;ii<vc.size();ii++){
        if(!(vc[ii]->get_current_indices().internal_indices_open.is_trivial())){
            non_trivial_internal_indices_open=true;
            base=vc[ii]->get_current_indices().internal_indices_open;
            break;
	}
    }
    if(non_trivial_internal_indices_open){
        // check consistency between different parents
        for(size_t ii=0;ii<vc.size();ii++){
            bool equal_to_trivial(vc[ii]->get_current_indices().internal_indices_open.is_trivial());
            bool equal_to_base(vc[ii]->get_current_indices().internal_indices_open==base);
            if((!equal_to_trivial)&&(!equal_to_base)){
                std::cout<<"ERROR: inconsistent assigment of current indices between parents: "<<base<<" "<<vc[ii]->get_current_indices().internal_indices_open<<std::endl;
                exit(101);
            }
        }
        internal_indices_open=base;
    }

    // fill information on internal_indices_closed
    // a single parent should have closed indices!
    int has_closed(-1);
    for(size_t ii=0;ii<vc.size();ii++){
        if(vc[ii]->get_current_indices().internal_indices_closed.size()>0){
            if(has_closed>-1){
                std::cout<<"ERROR: two parents with closed indices!"<<std::endl;
                std::cout<<"	"<<has_closed<<": "<<*(vc[has_closed])<<std::endl;
                std::cout<<"	"<<ii<<": "<<*(vc[ii])<<std::endl;
                exit(1);
            }
            has_closed=int(ii);
	}
    }
    if(has_closed>-1)
        internal_indices_closed=vc[has_closed]->get_current_indices().internal_indices_closed;
}

std::string CurrentIndices::get_index_info() const {
	std::string toret("internal_index: open: {");
	toret+=get_internal_index_vertex_string(internal_indices_open);
	toret+="} closed: {";
	for(size_t ii=0;ii<internal_indices_closed.size();ii++){
		toret+=get_internal_index_vertex_string(internal_indices_closed[ii]);
		if(ii+1!=internal_indices_closed.size())
			toret+="|";
	}
	toret+="} parents indices: {";
	for(size_t ii=0;ii<internal_indices_parents.size();ii++){
		toret+=std::to_string(internal_indices_parents[ii]);
		if(ii+1!=internal_indices_parents.size())
			toret+=",";
	}
	toret+="}";
	return toret;
}

void CurrentIndices::set_dynamically_assigned_internal_index(int i) {
    dynamical_internal_index=i;
}

int CurrentIndices::get_assigned_internal_index(size_t i) const {
    if(i>=internal_indices_parents.size()){
        std::cout<<"ERROR: CurrentIndices::get_assigned_internal_index(.) called for parent "<<i<<" when there is only "<<internal_indices_parents.size()<<std::endl;
        exit(101);
    }
    return internal_indices_parents[i];
}

int CurrentIndices::get_dynamically_assigned_internal_index() const {
    return dynamical_internal_index;
}

void CurrentIndices::set_internal_indices_open(const InternalIndexVertex& chp){
    internal_indices_open=chp;
}

bool CurrentIndices::internal_indices_match(const CurrentIndices& a,const CurrentIndices& b){
    // no check is performed on dynamical_internal_index or internal_indices_parents
    if(a.prop_dropped==b.prop_dropped)
        if(a.internal_indices_closed==b.internal_indices_closed)
            if(a.internal_indices_open==b.internal_indices_open)
                return true;
    return false;
}

void CurrentIndices::drop_propagator_and_pass_internal_indices(const ColorHandledProcess& chp){
	if(prop_dropped)
		return;
	prop_dropped=true;
	dynamical_internal_index=0;
	internal_indices_open=InternalIndexVertex{};
	internal_indices_closed.push_back({chp.needs_internal_index_handling(),chp.get_internal_index_left(),chp.get_internal_index_right(),chp.get_all_internal_index().first,chp.get_all_internal_index().second});
}

void CurrentIndices::reset_internal_indices_parents(){
    internal_indices_parents.clear();
}

bool operator==(const CurrentIndices& a,const CurrentIndices& b){
    // no check is performed on dynamical_internal_index as that is only dynamical information
    if(a.prop_dropped==b.prop_dropped)
        if(a.internal_indices_closed==b.internal_indices_closed)
            if(a.internal_indices_parents==b.internal_indices_parents)
                if(a.internal_indices_open==b.internal_indices_open)
                    return true;
    return false;
}

std::ostream& operator<<(std::ostream& o, const CurrentIndices& is){
    o<<is.get_index_info();
    return o;
}

size_t DefaultStateParticleContainer::size() const {
	return particles.size();
}

void DefaultStateParticleContainer::add_entry(Current* c){
	if(c->get_n_leaves()!=1){
		std::cout<<"DefaultStateParticleContainer::add_entry(c="<<c->get_name()<<") should receive only currents with 1 leaf!! Did nothing"<<std::endl;
		return;
	}
	
	if(std::find(level_1_currents.begin(),level_1_currents.end(),c)==level_1_currents.end()){
		Particle* lp(c->get_unique_particle_leaf());
		if(lp==nullptr){
			std::cout<<"Added to DefaultStateParticleContainer::add_entry(c="<<c->get_name()<<") a case with nullptr particle!! Did nothing"<<std::endl;
			return;
		}
		if(lp->external_state().get_name()!="default"){
			std::cout<<"Added to DefaultStateParticleContainer::add_entry(c="<<c->get_name()<<") a case with particle with state "<<lp->external_state()<<" and it should be \"default\"!! Did nothing"<<std::endl;
			return;
		}
		particles.push_back(lp);
		level_1_currents.push_back(c);
		current_array.push_back(std::vector<std::vector<Current*>>());
			current_array.back().push_back(std::vector<Current*>());
			current_array.back().back().push_back(c);
		states.push_back(std::vector<SingleState>());
		if(all_default_currents.size()==0)
			all_default_currents.push_back(std::vector<Current*>());
		all_default_currents[0].push_back(c);
	}
}

void DefaultStateParticleContainer::add_current_to_entry(Current* c1,Current* c2) {
	if(c1->get_n_leaves()!=1){
		std::cout<<"DefaultStateParticleContainer::add_current_to_entry(c1="<<c1->get_name()<<",cn="<<c2->get_name()<<") should receive only c1 with 1 leaf!! Did nothing"<<std::endl;
		return;
	}
	if(std::find(level_1_currents.begin(),level_1_currents.end(),c1)!=level_1_currents.end()){
		size_t entry(std::find(level_1_currents.begin(),level_1_currents.end(),c1)-level_1_currents.begin());
		size_t levelc2(c2->get_n_leaves());
		
		std::vector<std::vector<Current*>>& thearray(current_array[entry]);
		while(thearray.size()<levelc2) {
			thearray.push_back(std::vector<Current*>());
		}
		// only if c2 isn't included, we add it
		if(std::find(thearray[levelc2-1].begin(),thearray[levelc2-1].end(),c2)==thearray[levelc2-1].end()){
			thearray[levelc2-1].push_back(c2);
		}
		// now add to array of all defaulted currents
		while(all_default_currents.size()<levelc2) {
			all_default_currents.push_back(std::vector<Current*>());
		}
		// only if c2 isn't included, we add it
		if(std::find(all_default_currents[levelc2-1].begin(),all_default_currents[levelc2-1].end(),c2)==all_default_currents[levelc2-1].end()){
			all_default_currents[levelc2-1].push_back(c2);
		}
	}
	else{
		std::cout<<"cn in DefaultStateParticleContainer::add_current_to_entry(c1="<<c1->get_name()<<",cn="<<c2->get_name()<<") does not contain c1!! Did nothing"<<std::endl;
		return;
	}
}

const Particle* DefaultStateParticleContainer::get_particle(size_t ii) const {
	return particles[ii];
}

std::vector<SingleState>& DefaultStateParticleContainer::get_vector_of_states(size_t ii){
	return states[ii];
}

std::vector<std::vector<Current*>>& DefaultStateParticleContainer::get_array_of_currents(size_t ii){
	return current_array[ii];
}

const std::vector<std::vector<Current*>>& DefaultStateParticleContainer::get_all_defaulted_currents() const {
	return all_default_currents;
}

SinglePairMultiDimCorrelatedStates::SinglePairMultiDimCorrelatedStates(): first_particle(nullptr), second_particle(nullptr) {
}

DefaultStateManager::DefaultStateManager(Forest* in): forest(in) {
}

int DefaultStateManager::register_cut(int pid,size_t refmom,std::vector<int> loop_particles) {
	cut_register_index.push_back(pid);
	cut_register_refmom.push_back(refmom);

	size_t entry(0);
	bool newconf(true);
	std::sort(loop_particles.begin(),loop_particles.end());
	for(size_t ww=0;ww<register_of_loop_particle_combinations.size();ww++){
		if(loop_particles==register_of_loop_particle_combinations[ww]){
			entry=ww;
			newconf=false;
			break;
		}
	}
	if(newconf){
		entry=register_of_loop_particle_combinations.size();
		register_of_loop_particle_combinations.push_back(loop_particles);
	}
	map_cut_to_loop_combination.push_back(entry);

	// define for each loop-momentum current the corresponding momD_conf entry
	const std::vector<std::vector<Current*>>& lcurrents(std::get<3>((forest->all_processes)[pid-1]));
	for(auto& levels:lcurrents)
		for(auto& current:levels)
			current->define_default_momDconf(entry);

	return int(cut_register_index.size());
}

size_t DefaultStateManager::get_size_of_cut_register() const {
	return cut_register_index.size();
}

size_t DefaultStateManager::get_ref_mom_for_cut(int cut_i) const {
	if(cut_i>0&&cut_i<=int(cut_register_refmom.size()))
		return cut_register_refmom[cut_i-1];
	else{
		std::cout<<"DefaultStateManager::get_ref_mom_for_cut(cut_i="<<cut_i<<") as invalid input! Should be between 1 and "<<cut_register_refmom.size()<<" -- Returned zero"<<std::endl;
		return 0;
	}
}

int DefaultStateManager::get_current_index_for_cut(int cut_i) const {
	if(cut_i>0&&cut_i<=int(cut_register_index.size()))
		return cut_register_index[cut_i-1];
	else{
		std::cout<<"DefaultStateManager::get_current_index_for_cut(cut_i="<<cut_i<<") as invalid input! Should be between 1 and "<<cut_register_index.size()<<" -- Returned zero"<<std::endl;
		return 0;
	}
}

template <class T> void check_if_include(std::vector<T>& v,std::vector<std::vector<size_t>>& aDim,const size_t& Dim,const T& s){
	// check if we need s in v
	if(std::find(v.begin(),v.end(),s)==v.end()){
		// add it
		v.push_back(s);
		// and add corresponding entry in aDim
		aDim.push_back(std::vector<size_t>({Dim}));
	}
	// Check if aDim needs update
	else{
		size_t index_all_states(std::find(v.begin(),v.end(),s)-v.begin());
		// and add if necessary corresponding entry in aDim
		if(std::find(aDim[index_all_states].begin(),aDim[index_all_states].end(),Dim)==aDim[index_all_states].end()){
			aDim[index_all_states].push_back(Dim);
			std::sort(aDim[index_all_states].begin(),aDim[index_all_states].end());
		}
	}
}

void SinglePairMultiDimCorrelatedStates::add_correlated_state(size_t Dim,const CorrelatedState& state){
	// identify the index for the Dim passed
	size_t index_Dim(0);
	if(std::find(dimensions.begin(),dimensions.end(),Dim)==dimensions.end()){
		index_Dim=dimensions.size();
		dimensions.push_back(Dim);
		states_by_dimension.push_back(std::vector<CorrelatedState>());
		first_particle_states_by_dimension.push_back(std::vector<SingleState>());
		second_particle_states_by_dimension.push_back(std::vector<SingleState>());
	}
	else{
		index_Dim=std::find(dimensions.begin(),dimensions.end(),Dim)-dimensions.begin();
	}
	// define all local variables to work with
	std::vector<CorrelatedState>& states_Dim(states_by_dimension[index_Dim]);
	std::vector<SingleState>& first_states_Dim(first_particle_states_by_dimension[index_Dim]);
	std::vector<SingleState>& second_states_Dim(second_particle_states_by_dimension[index_Dim]);
	// now check if it is new state
	if(std::find(states_Dim.begin(),states_Dim.end(),state)==states_Dim.end()){
		// add it
		states_Dim.push_back(state);
		// now the individual states (the check for null is just for a future case in which I handle uncorrelated default states)
		if(state.first()==SingleState("NULLSTATE")||state.second()==SingleState("NULLSTATE"))
			std::cout<<"CAREFUL: SinglePairMultiDimCorrelatedStates::add_correlated_state("<<Dim<<","<<state<<") should have no NULLSTATE!!"<<std::endl;
		if(std::find(first_states_Dim.begin(),first_states_Dim.end(),state.first())==first_states_Dim.end())
			first_states_Dim.push_back(state.first());
		if(std::find(second_states_Dim.begin(),second_states_Dim.end(),state.second())==second_states_Dim.end())
			second_states_Dim.push_back(state.second());
		// check if we need this in all_states
		check_if_include(all_states,dimensions_in_all_states,Dim,state);
		// check if we need this in first_particle_all_states
		if(state.first()!=SingleState("NULLSTATE"))
			check_if_include(first_particle_all_states,dimensions_in_first_particle_all_states,Dim,state.first());
		// check if we need this in second_particle_all_states
		if(state.second()!=SingleState("NULLSTATE"))
			check_if_include(second_particle_all_states,dimensions_in_second_particle_all_states,Dim,state.second());
	}
}

const std::vector<CorrelatedState>& SinglePairMultiDimCorrelatedStates::get_all_states() const {
	return all_states;
}

const std::vector<std::vector<size_t>>& SinglePairMultiDimCorrelatedStates::get_dimensions_in_all_states() const {
	return dimensions_in_all_states;
}

std::string SinglePairMultiDimCorrelatedStates::get_short_name() const {
	std::string pair_name("(");
	if(first_particle==nullptr)	pair_name+="null,";	else pair_name+=first_particle->get_name()+",";
	if(second_particle==nullptr)	pair_name+="null";	else pair_name+=second_particle->get_name()+")";
	if(all_states.size()==0)
		return "empty correlated state for "+pair_name;
	std::string sts("{"+all_states[0].first().get_name()+","+all_states[0].second().get_name()+"}");
	for(size_t ii=1;ii<all_states.size();ii++)
		sts+=",{"+all_states[ii].first().get_name()+","+all_states[ii].second().get_name()+"}";
	return pair_name+" with states: "+sts;
}

std::string get_dimensions_string(const std::vector<size_t>& dimensions){
	std::string toret("");
	if(dimensions.size()>0){
		toret+=std::to_string(dimensions[0]);
		for(size_t jj=1;jj<dimensions.size();jj++)
			toret+=","+std::to_string(dimensions[jj]);
	}
	return toret;
}

void SinglePairMultiDimCorrelatedStates::show() const {
	std::string first_name("");
	std::string second_name("");
	if(first_particle==nullptr)	first_name+="null";	else first_name+=first_particle->get_name();
	if(second_particle==nullptr)	second_name+="null";	else second_name+=second_particle->get_name();
	std::string pair_name("("+first_name+","+second_name+")");
	if(all_states.size()==0){
		std::cout<<"empty correlated state for "<<pair_name<<std::endl;
		return;
	}

	std::cout<<"Correlated physical states for pair "<<pair_name<<":"<<std::endl;
	for(size_t ii=0;ii<all_states.size();ii++){
		std::string stringstate("{"+all_states[ii].first().get_name()+","+all_states[ii].second().get_name()+"}[Ds=");
		stringstate+=get_dimensions_string(dimensions_in_all_states[ii]);
		stringstate+="]";
		std::cout<<"	"<<ii<<": "<<stringstate<<std::endl;
	}
	std::cout<<"And individual states for first particle: "<<first_name<<std::endl;
	for(size_t ii=0;ii<first_particle_all_states.size();ii++){
		std::string stringstate("{"+first_particle_all_states[ii].get_name()+"}[Ds=");
		stringstate+=get_dimensions_string(dimensions_in_first_particle_all_states[ii]);
		stringstate+="]";
		std::cout<<"	"<<ii<<": "<<stringstate<<std::endl;
	}
	std::cout<<"And individual states for second particle: "<<second_name<<std::endl;
	for(size_t ii=0;ii<second_particle_all_states.size();ii++){
		std::string stringstate("{"+second_particle_all_states[ii].get_name()+"}[Ds=");
		stringstate+=get_dimensions_string(dimensions_in_second_particle_all_states[ii]);
		stringstate+="]";
		std::cout<<"	"<<ii<<": "<<stringstate<<std::endl;
	}
}

void MultiDimCorrelatedStates::add_correlated_states(size_t Dim,Particle* p1,Particle* p2,const std::vector<CorrelatedState>& states){
	bool included(false);
	bool flipped(false);
	size_t entry(0);
	// find if pair (p1,p2) (or oposite (p2,p1)) has already been added to *this
	for(size_t ii=0;ii<states_per_pair.size();ii++){
		if(states_per_pair[ii].first_particle==p1&&states_per_pair[ii].second_particle==p2){
			included=true;
			entry=ii;
			break;
		}
		else if(states_per_pair[ii].first_particle==p2&&states_per_pair[ii].second_particle==p1){
			included=true;
			flipped=true;
			entry=ii;
			break;
		}
	}
	if(!included){
		entry=states_per_pair.size();
		// add new entry
		states_per_pair.push_back(SinglePairMultiDimCorrelatedStates());
		// and set its correlated pair
		states_per_pair.back().first_particle=p1;
		states_per_pair.back().second_particle=p2;
	}

	// now add all states
	for(auto local_state:states){
		if(flipped)
			states_per_pair[entry].add_correlated_state(Dim,local_state.flip());
		else
			states_per_pair[entry].add_correlated_state(Dim,local_state);
	}

#if 0
std::cout<<"##############"<<std::endl;
std::cout<<"Showing now correlated states for entry: "<<entry<<" in "<<Dim<<" dimensions"<<std::endl;
states_per_pair[entry].show();
std::cout<<"##############"<<std::endl;
#endif
}

/**
 * Map states defined by Model to states used by Forest
 */
std::vector<CorrelatedState> get_CorrelatedStates_from_Model(std::vector<std::pair<SingleState,SingleState>>&& in){
    std::vector<CorrelatedState> toret;
    for(auto&& it:in) toret.push_back(std::move(it));
    return toret;
}

void DefaultStateManager::define_internal_states(const std::vector<size_t>& vDim){
	std::vector<size_t> local_dimensions;
	for(auto Dim:vDim){
		if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[Dim](const state_counter_per_dim& a) {return Dim==a.dim;})==dimensions_internal_states.end()){
			// a dimension not included so far
			local_dimensions.push_back(Dim);
		}
	}
	std::sort(local_dimensions.begin(),local_dimensions.end());
	DEBUG_MESSAGE("DefaultStateManager::define_internal_states(.) will add the following dimensions: ",local_dimensions," to the Forest's states_manager");

	// store information on dimensions added
	for(auto Dim:local_dimensions)
		dimensions_internal_states.push_back({Dim,std::vector<size_t>(),std::vector<size_t>(),0,0,
					std::vector<size_t>(),std::vector<size_t>(),0,0,
					std::vector<size_t>(),std::vector<size_t>(),0,0,
					std::vector<std::vector<Current*>>(),std::vector<std::vector<Current*>>()});
	std::sort(dimensions_internal_states.begin(),dimensions_internal_states.end(),[](const state_counter_per_dim& a,const state_counter_per_dim& b) 
			{return a.dim<b.dim;});

        // loop over all new dimensions
        for(auto Dim: local_dimensions){
            // loop over each pair of internal particles stored in Forest
            DEBUG_PRINT(Dim);
            for(auto& local_pair: forest->generated_loop_particles){
                Particle* first_particle(std::get<0>(local_pair));
                Particle* second_particle(std::get<1>(local_pair));

                DEBUG_PRINT(local_pair);
                DEBUG_PRINT(*first_particle);
                DEBUG_PRINT(*second_particle);
                DEBUG_MESSAGE("states: ",get_CorrelatedStates_from_Model(forest->model->get_states(first_particle->get_type(),Dim)));

                correlated_states.add_correlated_states(Dim,first_particle,second_particle,
                        get_CorrelatedStates_from_Model(forest->model->get_states(first_particle->get_type(),Dim)));
            }
	}
}

std::vector<size_t> DefaultStateManager::get_added_dimensions() const {
	std::vector<size_t> toret(dimensions_internal_states.size());
	std::transform(dimensions_internal_states.begin(),dimensions_internal_states.end(),toret.begin(),[](const state_counter_per_dim& s) {return s.dim;});
	return toret;
}

void DefaultStateManager::add_states_to_current(Current * current){
//std::cout<<"TREATING the current: "<<current->get_name()<<std::endl;
	// first make sure current hasn't been processed, and if not, set is as processed
	if(current->get_default_particles_processed()){
		std::cout<<"Current: "<<current->get_long_name()<<" already processed for internal states"<<std::endl;
		return;
	}
	// ask the current to find its Particle* leaves with default states
	current->set_default_state_particles();
	// and retrieve them
	const std::vector<Particle*>& default_particles(current->get_default_state_particles());
	// and find all states corresponding to it
	const std::vector<PhysicalState>& corresponding_states(this->get_states_associated_with_default_particles(default_particles));
	// and pass the info to current
	current->set_default_states(corresponding_states);
}

physical_state_struct get_permuted_set_of_states(std::vector<size_t> permutation,const physical_state_struct& physstate){
#if 0
std::cout<<"permuting for new physical state for the default particles: ";
for(auto p:physstate.particles)
	std::cout<<p->get_name()<<" ";
std::cout<<" with indices: "<<permutation<<std::endl;
#endif
	physical_state_struct toret({std::vector<Particle*>(),std::vector<PhysicalState>()});
	for(size_t ii=0;ii<permutation.size();ii++){
		toret.particles.push_back(physstate.particles[permutation[ii]]);
	}
	for(size_t ii=0;ii<physstate.states.size();ii++){
		toret.states.push_back(physstate.states[ii].permutation(permutation));
	}
	return toret;
}

const std::vector<PhysicalState>& DefaultStateManager::get_states_associated_with_default_particles(const std::vector<Particle*>& v){
#if 0
std::cout<<"AND vector of particles: ";
for(auto p:v)	std::cout<<p->get_name()<<" ";
std::cout<<std::endl;
#endif
	size_t n_particles(v.size());
	if(n_particles==0){
		std::cout<<"ERROR DefaultStateManager::get_states_associated_with_default_particles(.) should not receive empty vector!! -- Returned the first array entry in all_state_combinations"<<std::endl;
		// create space for v.size() configurations
		while(all_state_combinations.size()<1)
			all_state_combinations.push_back(std::vector<physical_state_struct>());
		while(all_state_combinations[0].size()<1)
			all_state_combinations[0].push_back({std::vector<Particle*>(),std::vector<PhysicalState>()});
		return all_state_combinations[0][0].states;
	}
	// create space for v.size() configurations
	while(all_state_combinations.size()<n_particles)
		all_state_combinations.push_back(std::vector<physical_state_struct>());
//std::cout<<"#parts: "<<n_particles<<" entries in all_state_combinations: "<<all_state_combinations.size()<<std::endl;
	
	// search if configuration has been produced
	size_t entry(0);
	if(std::find_if(all_state_combinations[n_particles-1].begin(),all_state_combinations[n_particles-1].end(),[v](const physical_state_struct& s) {return s.particles==v;})!=all_state_combinations[n_particles-1].end()){
		entry=std::find_if(all_state_combinations[n_particles-1].begin(),all_state_combinations[n_particles-1].end(),[v](const physical_state_struct& s) {return s.particles==v;})-all_state_combinations[n_particles-1].begin();
//std::cout<<"REUSING set of states"<<std::endl;
	}
	// exact configuration not encountered
	else{
		// see if a permutation of v has been included (in such case, it is much cheaper to apply the given permutation)
		std::vector<std::pair<size_t,Particle*>> particles_permuted;
		for(size_t ii=0;ii<v.size();ii++)
			particles_permuted.push_back(std::make_pair(ii,v[ii]));
		bool permutation_exists(false);
		if(n_particles>1){
			do {
				// loop over every entry with n leaves
				for(size_t kk=0;kk<all_state_combinations[n_particles-1].size();kk++){
					bool matches(true);
					for(size_t ii=0;ii<all_state_combinations[n_particles-1][kk].particles.size();ii++){
						if(all_state_combinations[n_particles-1][kk].particles[ii]!=particles_permuted[ii].second){
							matches=false;
							// break the ii loop
							break;
						}
					}
					if(matches){
						permutation_exists=true;
						entry=kk;
						// break the permutation loop
						break;
					}
				}
				if(permutation_exists){
					break;
				}
			} while( std::next_permutation(particles_permuted.begin(),particles_permuted.end(),
					[](const std::pair<size_t,Particle*>& a, const std::pair<size_t,Particle*>& b) {return a.first<b.first;}));
		}
		// we permute or compute a new set of states
		if(permutation_exists){
			std::vector<size_t> permutation;
			for(size_t ii=0;ii<particles_permuted.size();ii++)
				permutation.push_back(particles_permuted[ii].first);
//std::cout<<"The permutation found is: "<<permutation<<std::endl;
			physical_state_struct states_permuted(get_permuted_set_of_states(permutation,all_state_combinations[n_particles-1][entry]));
			// now fix entry for the return statement
			entry=all_state_combinations[n_particles-1].size();
			// and store the permutation
			all_state_combinations[n_particles-1].push_back(states_permuted);
//std::cout<<"PERMUTING set of states"<<std::endl;
		}
		else{
			// we need to compute a new entry
			physical_state_struct new_set_of_states(this->assemble_new_physical_state_struct(v));
			// now fix entry for the return statement
			entry=all_state_combinations[n_particles-1].size();
			// and store the permutation
//std::cout<<"PUSHED back: "<<entry<<" pointer "<<&all_state_combinations[n_particles-1]<<std::endl;
			all_state_combinations[n_particles-1].push_back(new_set_of_states);
		}
	}

	return all_state_combinations[n_particles-1][entry].states;
}

physical_state_struct DefaultStateManager::assemble_new_physical_state_struct(const std::vector<Particle*>& v){
#if 0
std::cout<<"assembling a new physical state for the default particles: ";
for(auto p:v)
	std::cout<<p->get_name()<<" ";
std::cout<<std::endl;
#endif
	// find which particles in correlated_states (and in forest->generated_loop_particles) the particles in v correspond to
	std::vector<Particle*> v_prune(v);
	// holds all particles in correlated pairs associated with v
	std::vector<Particle*> v_all_correlated;
	std::vector<size_t> indices_correlated;
	while(v_prune.size()>0){
		Particle* local_particle(v_prune.back());
		bool match(false);
		bool match_first(true);
		for(size_t ii=0;ii<correlated_states.size();ii++){
			if(local_particle==correlated_states[ii].first_particle){
				match=true;
				match_first=true;
				indices_correlated.push_back(ii);
				break;
			}
			else if(local_particle==correlated_states[ii].second_particle){
				match=true;
				match_first=false;
				indices_correlated.push_back(ii);
				break;
			}
		}
		if(!match){
			std::cout<<"DefaultStateManager::assemble_new_physical_state_struct(.) received a vector containing Particle: "<<local_particle->get_name()<<" which is not contained in correlated_states!! -- Returned empty PhysicalState!!"<<std::endl;
			return {v,std::vector<PhysicalState>()};
		}
		// drop the back particle
		v_prune.pop_back();
		Particle* match_local_particle;
		if(match_first)
			match_local_particle=correlated_states[indices_correlated.back()].second_particle;
		else
			match_local_particle=correlated_states[indices_correlated.back()].first_particle;
		bool pair_included(true);
		if(std::find(v_prune.begin(),v_prune.end(),match_local_particle)==v_prune.end())
			pair_included=false;
		if(pair_included){
			// insert in all correlated, in proper order
			v_all_correlated.insert(v_all_correlated.begin(),local_particle);
			v_all_correlated.insert(v_all_correlated.begin(),match_local_particle);
			// erase match_local_particle from v_prune
			v_prune.erase(std::find(v_prune.begin(),v_prune.end(),match_local_particle));
		}
		else{
			// insert in all correlated, in proper order
			v_all_correlated.insert(v_all_correlated.begin(),match_local_particle);
			v_all_correlated.insert(v_all_correlated.begin(),local_particle);
		}	
	}
#if 0
std::cout<<"From an input ";
for(auto p:v)	std::cout<<p->get_name()<<" ";
std::cout<<std::endl<<"Got a list of all correlated particles: ";
for(auto p:v_all_correlated)	std::cout<<p->get_name()<<" ";
std::cout<<std::endl;
#endif

	// get the states associated with v_all_correlated and use them to obtain the needed ones from pruning
	std::vector<PhysicalState> states_v_all_correlated;

	// make the cartesian product of the states in v_all_correlated
	
	// check first if we have it stored
	if(v!=v_all_correlated)
		states_v_all_correlated=this->get_states_associated_with_default_particles(v_all_correlated);
	// compute it!
	else{
		// proper order for construction
		v_all_correlated.clear();
		for(size_t ii=0;ii<indices_correlated.size();ii++){
			v_all_correlated.push_back(correlated_states[indices_correlated[ii]].first_particle);
			v_all_correlated.push_back(correlated_states[indices_correlated[ii]].second_particle);
		}
		// fill all possible PhysicalState's related to the pairs added.
//std::cout<<"compute!"<<std::endl;
		states_v_all_correlated=this->cartesian_product_of_states(indices_correlated);
		return {v_all_correlated,states_v_all_correlated};		
	}	

	std::vector<PhysicalState> thestates;
	// check each state in states_v_all_correlated
	for(size_t ii=0;ii<states_v_all_correlated.size();ii++){
		PhysicalState lpstate(states_v_all_correlated[ii].prune(v));
		// check if it is a new state
		if(std::find(thestates.begin(),thestates.end(),lpstate)==thestates.end()){
			// check if there is a state that only differs by dimension
			bool differ_by_extra_dim(false);
			for(auto existing:thestates){
				// extra_dim, in case true, add dimension to 'existing'
				if(existing.extra_dim(lpstate)){
					differ_by_extra_dim=true;
					break;
				}
			}
			// if is new, push back!
			if(!differ_by_extra_dim)
				thestates.push_back(lpstate);
		}
	}
	
	return {v,thestates};
}

/**
 * Simple function to fill 'step' indices for a cartesian product of 'depth' elements, with 'all_sizes[depth-step]' entries
 */
void get_the_cartesian_product_indices(const size_t& depth,size_t step,const std::vector<size_t>& all_sizes,std::vector<std::vector<size_t>>& fill,std::vector<size_t> local_indices){
	// we finished the recursion!
	if(step==0){
		fill.push_back(local_indices);
		return;
	}
	local_indices.push_back(0);
	for(size_t ii=0;ii<all_sizes[depth-step];ii++){
		local_indices.back()=ii;
		get_the_cartesian_product_indices(depth,step-1,all_sizes,fill,local_indices);
	}
}

using all_states_given_pair = std::vector<CorrelatedState>;
using all_dimensions_given_pair = std::vector<std::vector<size_t>>;

/**
 * This function checks the dimensionalities for each entry of a cartesian product of CorrelatedState's, and returns a vector of all commonly allowed states
 */
std::vector<size_t> get_matching_dimensions(const std::vector<size_t>& entries,const std::vector<all_dimensions_given_pair>& all_pair_dimensions){
	std::vector<size_t> toret;
	// loop over each correlated pair
	for(size_t ii=0;ii<all_pair_dimensions.size();ii++){
		const std::vector<size_t>& local_dims(all_pair_dimensions[ii][entries[ii]]);
		for(auto d:local_dims){
			// and add every dimension without repeating it
			if(std::find(toret.begin(),toret.end(),d)==toret.end())
				toret.push_back(d);
		}
	}
	// sort
	std::sort(toret.begin(),toret.end());
	// now filter toret such that only includes dimension that are common to every correlated-pair state considered
	for(size_t ii=toret.size();ii>0;ii--){
		bool all_included(true);
		for(size_t jj=0;jj<all_pair_dimensions.size();jj++){
			const std::vector<size_t>& local_dims(all_pair_dimensions[jj][entries[jj]]);
			if(std::find(local_dims.begin(),local_dims.end(),toret[ii-1])==local_dims.end()){
				all_included=false;
				break;
			}
		}
		if(!all_included){
			// then erase the dimension
			toret.erase(toret.begin()+ii-1);
		}
	}
#if 0
std::cout<<"Out of the input dimensions:"<<std::endl;
for(size_t jj=0;jj<all_pair_dimensions.size();jj++){
	const std::vector<size_t>& local_dims(all_pair_dimensions[jj][entries[jj]]);
	std::cout<<"	"<<jj<<": "<<local_dims<<std::endl;
}
std::cout<<"we retained: "<<toret<<std::endl;
#endif
	return toret;
}

/**
 * Utility to produce PhysicalState from a given cartesian product
 */
PhysicalState get_PhysicalState_from_cartesian_product_of_correlated_states(const std::vector<size_t>& dimensions,const std::vector<Particle*>& all_particles,const std::vector<size_t>& entries,
		const std::vector<all_states_given_pair>& all_pair_states){
	PhysicalState toret;
	toret.dims=dimensions;
	toret.leaves=all_particles;
	std::vector<SingleState>& local_states(toret.state_entries);
	for(size_t ii=0;ii<entries.size();ii++){
		local_states.push_back(all_pair_states[ii][entries[ii]].first());
		local_states.push_back(all_pair_states[ii][entries[ii]].second());
	}
#if 0
std::cout<<"A NEW PhysicalState: "<<std::endl;
toret.show();
#endif
	return toret;
}

std::vector<PhysicalState> DefaultStateManager::cartesian_product_of_states(const std::vector<size_t>& indices) const {
	std::vector<PhysicalState> toret;
	size_t depth(indices.size());
	// to store all involved CorrelatedState's and dimensions
	std::vector<Particle*> all_particles(2*depth);
	std::vector<all_states_given_pair> all_pair_states(depth);
	std::vector<all_dimensions_given_pair> all_pair_dimensions(depth);
	// track size of each set of states
	std::vector<size_t> all_sizes(depth);
	//size_t total_dimension_cartesian_product(1);
	// retrieve information
	for(size_t ii=0;ii<depth;ii++){
		all_particles[2*ii]=correlated_states[indices[ii]].first_particle;
		all_particles[2*ii+1]=correlated_states[indices[ii]].second_particle;
		all_pair_states[ii]=correlated_states[indices[ii]].get_all_states();
		all_pair_dimensions[ii]=correlated_states[indices[ii]].get_dimensions_in_all_states();
		all_sizes[ii]=all_pair_states[ii].size();
		//total_dimension_cartesian_product*=all_sizes[ii];
	}
	// the list of all indices for the cartesian product
	std::vector<std::vector<size_t>> cartesian_product_indices;
	std::vector<size_t> local_indices;
	// a recursive function for filling the entries
	get_the_cartesian_product_indices(depth,depth,all_sizes,cartesian_product_indices,local_indices);
#if 0
std::cout<<"All indices for cartesian product: "<<std::endl;
for(size_t ii=0;ii<cartesian_product_indices.size();ii++)
	std::cout<<"	"<<ii<<": "<<cartesian_product_indices[ii]<<std::endl;
#endif

	// now loop over all potential cartesian product states
	for(auto entries:cartesian_product_indices){
		// get allowed dimensions for this state
		std::vector<size_t> new_dimensions(get_matching_dimensions(entries,all_pair_dimensions));

		// if dimensions remain, we add cartesian product
		if(new_dimensions.size()>0){
			toret.push_back(get_PhysicalState_from_cartesian_product_of_correlated_states(new_dimensions,all_particles,entries,all_pair_states));
		}
	}

	return toret;
}

size_t DefaultStateManager::get_total_number_of_tree_currents() const{ 
	return total_number_of_tree_currents; 
}

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// STRATEGIES for Forest for counting D-dim states
/////////////////////////////////////////////////
/////////////////////////////////////////////////

#ifdef BUILDERS_FOR_EACH_Ds
// CHOICE: for dim
size_t DefaultStateManager::get_total_number_of_treelike_currents(size_t Dim) const { return this->get_total_number_of_treelike_currents_dim(Dim); }
size_t DefaultStateManager::get_treelike_currents_per_level(size_t Dim,size_t level) const { return this->get_number_of_treelike_currents_per_level_dim(Dim,level); }
size_t DefaultStateManager::get_count_of_states_for_cut(size_t Dim,size_t cut_i) const { return this->get_number_of_toplevel_cut_currents_dim(Dim,cut_i); }
size_t DefaultStateManager::get_count_of_all_level_states_for_cut(size_t Dim,size_t cut_i) const { return this->get_number_of_alllevels_cut_currents_dim(Dim,cut_i); }
size_t DefaultStateManager::get_total_number_of_cut_currents(size_t Dim) const { return this->get_total_number_of_toplevel_cut_currents_dim(Dim); }
#else
// CHOICE: up to dim
size_t DefaultStateManager::get_total_number_of_treelike_currents(size_t Dim) const { return this->get_total_number_of_treelike_currents_up_to_dim(Dim); }
size_t DefaultStateManager::get_treelike_currents_per_level(size_t Dim,size_t level) const { return this->get_number_of_treelike_currents_per_level_up_to_dim(Dim,level); }
size_t DefaultStateManager::get_count_of_states_for_cut(size_t Dim,size_t cut_i) const { return this->get_number_of_toplevel_cut_currents_up_to_dim(Dim,cut_i); }
size_t DefaultStateManager::get_count_of_all_level_states_for_cut(size_t Dim,size_t cut_i) const { return this->get_number_of_alllevels_cut_currents_up_to_dim(Dim,cut_i); }
size_t DefaultStateManager::get_total_number_of_cut_currents(size_t Dim) const { return this->get_total_number_of_toplevel_cut_currents_up_to_dim(Dim); }
#endif

/////////////////////////////////////////////////
/////////////////////////////////////////////////

const std::vector<std::vector<Current*>>& DefaultStateManager::get_all_currents(size_t d) const {
	auto entry(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
		[d](const state_counter_per_dim& a){return d==a.dim;}));
	if(entry==dimensions_internal_states.end()){
		std::cout<<"ERROR, dim Ds: "<<d<<" was passed to DefaultStateManager::get_all_currents(.) and was not added to the parent Forest!!"<<std::endl;
                std::exit(1);
	}
	else{
		size_t stentry(entry-dimensions_internal_states.begin());
#ifdef BUILDERS_FOR_EACH_Ds
//std::cout<<"Dim: "<<d<<" stentry "<<stentry<<" size: "<<dimensions_internal_states[stentry].all_necessary_currents_for_this_dim.size()<<std::endl;
		return dimensions_internal_states[stentry].all_necessary_currents_for_this_dim;
#else
//std::cout<<"Dim: "<<d<<" stentry "<<stentry<<" size: "<<dimensions_internal_states[stentry].all_necessary_currents_strictly_up_to_this_dim.size()<<std::endl;
		return dimensions_internal_states[stentry].all_necessary_currents_strictly_up_to_this_dim;
#endif
	}
}

size_t DefaultStateManager::get_total_number_of_treelike_currents_dim(size_t dim) const{
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		return dimensions_internal_states[index].total_number_of_treelike_currents_for_this_dim;
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_total_number_of_treelike_currents_up_to_dim(size_t dim) const{
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		return dimensions_internal_states[index].total_number_of_treelike_currents_strictly_up_to_this_dim;
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_number_of_treelike_currents_per_level_dim(size_t dim,size_t i) const {
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		// check if level exist 
		if(i<dimensions_internal_states[index].number_of_treelike_currents_per_level_for_this_dim.size()+1){
			return dimensions_internal_states[index].number_of_treelike_currents_per_level_for_this_dim[i-1];
		}
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_number_of_treelike_currents_per_level_up_to_dim(size_t dim,size_t i) const {
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		// check if level exist 
		if(i<dimensions_internal_states[index].number_of_treelike_currents_per_level_strictly_up_to_this_dim.size()+1){
			return dimensions_internal_states[index].number_of_treelike_currents_per_level_strictly_up_to_this_dim[i-1];
		}
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_number_of_toplevel_cut_currents_dim(size_t dim,size_t cut_i) const {
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		// check if cut appears
		if(cut_i<dimensions_internal_states[index].number_of_toplevel_cut_currents_for_this_dim.size()+1){
			return dimensions_internal_states[index].number_of_toplevel_cut_currents_for_this_dim[cut_i-1];
		}
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_number_of_alllevels_cut_currents_dim(size_t dim,size_t cut_i) const {
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		// check if cut appears
		if(cut_i<dimensions_internal_states[index].number_of_alllevels_cut_currents_for_this_dim.size()+1){
			return dimensions_internal_states[index].number_of_alllevels_cut_currents_for_this_dim[cut_i-1];
		}
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_total_number_of_toplevel_cut_currents_dim(size_t dim) const {
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		return dimensions_internal_states[index].total_number_of_toplevel_cut_currents_for_this_dim;
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_number_of_toplevel_cut_currents_up_to_dim(size_t dim,size_t cut_i) const {
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		// check if cut appears
		if(cut_i<dimensions_internal_states[index].number_of_toplevel_cut_currents_strictly_up_to_this_dim.size()+1){
			return dimensions_internal_states[index].number_of_toplevel_cut_currents_strictly_up_to_this_dim[cut_i-1];
		}
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_number_of_alllevels_cut_currents_up_to_dim(size_t dim,size_t cut_i) const {
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		// check if cut appears
		if(cut_i<dimensions_internal_states[index].number_of_alllevels_cut_currents_strictly_up_to_this_dim.size()+1){
			return dimensions_internal_states[index].number_of_alllevels_cut_currents_strictly_up_to_this_dim[cut_i-1];
		}
	}
	// otherwise, zero
	return 0;
}

size_t DefaultStateManager::get_total_number_of_toplevel_cut_currents_up_to_dim(size_t dim) const {
	// check dimension appears
	if(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),
				[dim](const state_counter_per_dim& a) {return dim==a.dim;})!=dimensions_internal_states.end()){
		size_t index(std::find_if(dimensions_internal_states.begin(),dimensions_internal_states.end(),[dim](const state_counter_per_dim& a) {return dim==a.dim;})
			-dimensions_internal_states.begin());
		return dimensions_internal_states[index].total_number_of_toplevel_cut_currents_strictly_up_to_this_dim;
	}
	// otherwise, zero
	return 0;
}

void add_all_necessary_currents(std::vector<std::vector<Current*>>& toproc,const std::vector<std::vector<Current*>>& in){
	for(size_t ii=0;ii<in.size();ii++){
		// resize if needed
		while(toproc.size()<ii+1)
			toproc.push_back(std::vector<Current*>());
		for(size_t jj=0;jj<in[ii].size();jj++){
			// check if current is new
			if(std::find(toproc[ii].begin(),toproc[ii].end(),in[ii][jj])==toproc[ii].end()){
				// and in such case, push back
				toproc[ii].push_back(in[ii][jj]);
			}
		}
	}
}

void DefaultStateManager::update_counting_all_dims_and_currents(){
	// First, take care of filling arrays of current pointers
	bool tree_included_for(false);
	bool tree_included_up(false);
	for(size_t ll=0;ll<dimensions_internal_states.size();ll++){
		state_counter_per_dim& scpd(dimensions_internal_states[ll]);
		std::vector<std::vector<Current*>>& local_array_for(scpd.all_necessary_currents_for_this_dim);
		std::vector<std::vector<Current*>>& local_array_up(scpd.all_necessary_currents_strictly_up_to_this_dim);
		size_t local_dim(scpd.dim);
		local_array_for.clear();
		local_array_up.clear();
		// we loop over all top level currents, and see if it contributes to this dim
		for(auto tp:forest->all_processes){
			Current* cTop(std::get<1>(tp));
			// checks if it has undefined states
			if(cTop->get_default_particles_processed()){
				const PhysicalStateContainer& pc(cTop->get_physical_states());
				// is this state associated with dimension local_dim
				for(size_t ii=0;ii<pc.size();ii++){
					if(std::find(pc[ii].dims.begin(),pc[ii].dims.end(),local_dim)!=pc[ii].dims.end()){
						// we found a state that appears in local_dim, we add currents and break
						add_all_necessary_currents(local_array_for,std::get<3>(tp));
						break;
					}
				}

				for(size_t ii=0;ii<pc.size();ii++){
					// we have a state associated strictly up to local_dim, we add currents and break
					if(pc[ii].dims.back()==local_dim){
						add_all_necessary_currents(local_array_up,std::get<3>(tp));
						break;
					}
				}
			}
		}
		// if local_array is not empty, we must add tree currents
		if(local_array_for.size()>0){
			add_all_necessary_currents(local_array_for,forest->get_all_tree_currents());
			tree_included_for=true;
		}
		if(local_array_up.size()>0){
			add_all_necessary_currents(local_array_up,forest->get_all_tree_currents());
			tree_included_up=true;
		}
	}

	// check if tree level information was included for dim
	if(!tree_included_for){
		if(dimensions_internal_states.size()>0){
			DEBUG_MESSAGE("Adding tree currents to compute pure-tree-level amplitudes, only in smallest Ds in for strictly Ds");
			state_counter_per_dim& scpd(dimensions_internal_states[0]);
			std::vector<std::vector<Current*>>& local_array_for(scpd.all_necessary_currents_for_this_dim);
			local_array_for.clear();

			add_all_necessary_currents(local_array_for,forest->get_all_tree_currents());
		}
		else{
			std::cout<<"No currents were added to the Forest's (for exactly Ds) state manager by DefaultStateManager::update_counting_all_dims_and_currents()"<<std::endl;
		}
	}
	// check if tree level information was included up to dim
	if(!tree_included_up){
		if(dimensions_internal_states.size()>0){
			DEBUG_MESSAGE("Adding tree currents to compute pure-tree-level amplitudes, only in smallest Ds up to strictly Ds");
			state_counter_per_dim& scpd(dimensions_internal_states[0]);
			std::vector<std::vector<Current*>>& local_array_up(scpd.all_necessary_currents_strictly_up_to_this_dim);
			local_array_up.clear();

			add_all_necessary_currents(local_array_up,forest->get_all_tree_currents());
		}
		else{
			std::cout<<"No currents were added to the Forest's (up to strictly Ds) state manager by DefaultStateManager::update_counting_all_dims_and_currents()"<<std::endl;
		}
	}


	// fill first tree-level information
	const std::vector<std::vector<Current*>>& all_tree_currents(forest->get_all_tree_currents());
	number_tree_currents_per_level.clear();
	total_number_of_tree_currents=0;
	for(size_t ii=0;ii<all_tree_currents.size();ii++){
		number_tree_currents_per_level.push_back(all_tree_currents[ii].size());
		total_number_of_tree_currents+=all_tree_currents[ii].size();
	}
	
	for(size_t ii=0;ii<dimensions_internal_states.size();ii++){
		state_counter_per_dim& state_counter(dimensions_internal_states[ii]);
		// clear existing data
		state_counter.number_of_toplevel_cut_currents_for_this_dim.clear();
		state_counter.number_of_toplevel_cut_currents_strictly_up_to_this_dim.clear();	
		state_counter.total_number_of_toplevel_cut_currents_for_this_dim=0;
		state_counter.total_number_of_toplevel_cut_currents_strictly_up_to_this_dim=0;

		state_counter.number_of_alllevels_cut_currents_for_this_dim.clear();
		state_counter.number_of_alllevels_cut_currents_strictly_up_to_this_dim.clear();	
		state_counter.total_number_of_alllevels_cut_currents_for_this_dim=0;
		state_counter.total_number_of_alllevels_cut_currents_strictly_up_to_this_dim=0;

		state_counter.number_of_treelike_currents_per_level_for_this_dim.clear();
		state_counter.number_of_treelike_currents_per_level_strictly_up_to_this_dim.clear();
		state_counter.total_number_of_treelike_currents_for_this_dim=0;	
		state_counter.total_number_of_treelike_currents_strictly_up_to_this_dim=0;

		// a loop over all entries in cut_register_index mapped through forest->all_processes for the number_of_currents information

		// fill information on cut states
		for(size_t jj=0;jj<cut_register_index.size();jj++){
			Current* c(std::get<1>(forest->all_processes[cut_register_index[jj]-1]));
			dim_counter counter(c->get_physical_states().get_dim_counter(state_counter.dim));
			// fill
			state_counter.number_of_toplevel_cut_currents_for_this_dim.push_back(counter.states_counter);
			// notice that we don't write counter.states_strictly_contributing_up_to_Ds but counter.states_counter, as everything in local Ds is necessary
			state_counter.number_of_toplevel_cut_currents_strictly_up_to_this_dim.push_back(counter.states_counter);	
			state_counter.total_number_of_toplevel_cut_currents_for_this_dim+=counter.states_counter;
			// notice that we don't write counter.states_strictly_contributing_up_to_Ds but counter.states_counter, as everything in local Ds is necessary
			state_counter.total_number_of_toplevel_cut_currents_strictly_up_to_this_dim+=counter.states_counter;

			// and fill info for all levels
			size_t all_counter_for_this_dim(0);
			size_t all_counter_strictly_up_to_this_dim(0);
			const std::vector<std::vector<Current*>>& c_all(std::get<3>(forest->all_processes[cut_register_index[jj]-1]));
			for(size_t kk=0;kk<c_all.size();kk++){
				for(size_t ll=0;ll<c_all[kk].size();ll++){
					Current* c_local(c_all[kk][ll]);
					dim_counter counter_local(c_local->get_physical_states().get_dim_counter(state_counter.dim));
					all_counter_for_this_dim+=counter_local.states_counter;
					// the associated maximal cut 'c' has states, we should count his daughter current 'c_local' without considering its appearance in larger dimensions
					all_counter_strictly_up_to_this_dim+=counter_local.states_counter;
				}
			}
			state_counter.number_of_alllevels_cut_currents_for_this_dim.push_back(all_counter_for_this_dim);
			state_counter.number_of_alllevels_cut_currents_strictly_up_to_this_dim.push_back(all_counter_strictly_up_to_this_dim);	
			state_counter.total_number_of_alllevels_cut_currents_for_this_dim+=all_counter_for_this_dim;
			state_counter.total_number_of_alllevels_cut_currents_strictly_up_to_this_dim+=all_counter_strictly_up_to_this_dim;
		}

		// initialize
		const std::vector<std::vector<Current*>> all_currents(forest->get_all_currents());
		for(size_t jj=0;jj<all_currents.size();jj++){
			state_counter.number_of_treelike_currents_per_level_for_this_dim.push_back(0);
			state_counter.number_of_treelike_currents_per_level_strictly_up_to_this_dim.push_back(0);
		}

		std::vector<std::vector<Current*>>& all_currents_for(state_counter.all_necessary_currents_for_this_dim);

		//a loop over all entries in all_currents for the treelike information for exactly this dim
		for(size_t jj=0;jj<all_currents_for.size();jj++){
			for(size_t kk=0;kk<all_currents_for[jj].size();kk++){
				// current with default states?
				if(all_currents_for[jj][kk]->get_default_particles_processed()){
					Current* c(all_currents_for[jj][kk]);
					dim_counter counter(c->get_physical_states().get_dim_counter(state_counter.dim));
					state_counter.number_of_treelike_currents_per_level_for_this_dim[jj]+=counter.states_counter;	
				}
				// single state
				else{
					state_counter.number_of_treelike_currents_per_level_for_this_dim[jj]++;	
				}
			}
			state_counter.total_number_of_treelike_currents_for_this_dim+=state_counter.number_of_treelike_currents_per_level_for_this_dim[jj];	
		}

		const std::vector<std::vector<Current*>>& all_currents_up(state_counter.all_necessary_currents_strictly_up_to_this_dim);

		//a loop over all entries in all_currents for the treelike information for up to this dim
		for(size_t jj=0;jj<all_currents_up.size();jj++){
			for(size_t kk=0;kk<all_currents_up[jj].size();kk++){
				// current with default states?
				if(all_currents_up[jj][kk]->get_default_particles_processed()){
					Current* c(all_currents_up[jj][kk]);
					dim_counter counter(c->get_physical_states().get_dim_counter(state_counter.dim));
					// we should count all contributions that appear with the local Ds
					state_counter.number_of_treelike_currents_per_level_strictly_up_to_this_dim[jj]+=counter.states_counter;
//std::cout<<"	dim: "<<state_counter.dim<<" c: "<<c->get_name()<<" strictly: "<<counter.states_strictly_contributing_up_to_Ds<<" other: "<<counter.states_counter<<" level: "<<jj<<" max: "<<all_currents_up.size()<<std::endl;
				}
				// single state
				else{
					state_counter.number_of_treelike_currents_per_level_strictly_up_to_this_dim[jj]++;
				}
			}
			state_counter.total_number_of_treelike_currents_strictly_up_to_this_dim+=state_counter.number_of_treelike_currents_per_level_strictly_up_to_this_dim[jj];
		}


	}

#if 0
// for debugging, print all info
std::cout<<"####################"<<std::endl;
std::cout<<"Displaying full information of state counting in DefaultStateManager"<<std::endl;
std::cout<<"####################"<<std::endl;
std::cout<<"The number of tree currents per level is: "<<number_tree_currents_per_level<<std::endl;
std::cout<<"For a total of: "<<total_number_of_tree_currents<<std::endl;
for(size_t Dim=0;Dim<dimensions_internal_states.size();Dim++){
std::cout<<"####################"<<std::endl;
std::cout<<"In exactly "<<dimensions_internal_states[Dim].dim<<" dimensions we have: "<<std::endl;
for(size_t cut=0;cut<dimensions_internal_states[Dim].number_of_toplevel_cut_currents_for_this_dim.size();cut++){
	Current* c(std::get<1>(forest->all_processes[cut_register_index[cut]-1]));
	std::cout<<"For cut_"<<cut<<"="<<c->get_name()<<" we have # of top-level states: "<<dimensions_internal_states[Dim].number_of_toplevel_cut_currents_for_this_dim[cut]
		<<" and # of all-levels states: "<<dimensions_internal_states[Dim].number_of_alllevels_cut_currents_for_this_dim[cut]<<std::endl;
}
std::cout<<"For a total count of currents: "<<dimensions_internal_states[Dim].total_number_of_toplevel_cut_currents_for_this_dim<<
		" "<<dimensions_internal_states[Dim].total_number_of_alllevels_cut_currents_for_this_dim<<std::endl;
std::cout<<"####################"<<std::endl;
std::cout<<"Inclusively up to strictly "<<dimensions_internal_states[Dim].dim<<" dimensions we have: "<<std::endl;
for(size_t cut=0;cut<dimensions_internal_states[Dim].number_of_toplevel_cut_currents_strictly_up_to_this_dim.size();cut++){
	Current* c(std::get<1>(forest->all_processes[cut_register_index[cut]-1]));
	std::cout<<"For cut_"<<cut<<"="<<c->get_name()<<" we have # of states: "<<dimensions_internal_states[Dim].number_of_toplevel_cut_currents_strictly_up_to_this_dim[cut]
		<<" and # of all-levels states: "<<dimensions_internal_states[Dim].number_of_alllevels_cut_currents_strictly_up_to_this_dim[cut]<<std::endl;
}
std::cout<<"For a total count of currents: "<<dimensions_internal_states[Dim].total_number_of_toplevel_cut_currents_strictly_up_to_this_dim<<
		" "<<dimensions_internal_states[Dim].total_number_of_alllevels_cut_currents_strictly_up_to_this_dim<<std::endl;
std::cout<<"####################"<<std::endl;
}
for(size_t Dim=0;Dim<dimensions_internal_states.size();Dim++){
std::cout<<"####################"<<std::endl;
std::cout<<"In exactly "<<dimensions_internal_states[Dim].dim<<" dimensions we have: "<<std::endl;
for(size_t level=0;level<dimensions_internal_states[Dim].number_of_treelike_currents_per_level_for_this_dim.size();level++){
	std::cout<<"For level_"<<level<<" we have # of tree-like currents: "<<dimensions_internal_states[Dim].number_of_treelike_currents_per_level_for_this_dim[level]<<std::endl;
}
std::cout<<"For a total count of currents: "<<dimensions_internal_states[Dim].total_number_of_treelike_currents_for_this_dim<<std::endl;
std::cout<<"####################"<<std::endl;
std::cout<<"Inclusively up to strictly "<<dimensions_internal_states[Dim].dim<<" dimensions we have: "<<std::endl;
for(size_t level=0;level<dimensions_internal_states[Dim].number_of_treelike_currents_per_level_strictly_up_to_this_dim.size();level++){
	std::cout<<"For level_"<<level<<" we have # of tree-like currents: "<<dimensions_internal_states[Dim].number_of_treelike_currents_per_level_strictly_up_to_this_dim[level]<<std::endl;
}
std::cout<<"For a total count of currents: "<<dimensions_internal_states[Dim].total_number_of_treelike_currents_strictly_up_to_this_dim<<std::endl;
std::cout<<"####################"<<std::endl;
}
std::cout<<"####################"<<std::endl;
#endif
}

size_t DefaultStateManager::number_of_momDconf_copies() const {
	if(register_of_loop_particle_combinations.size()==0)
		return 1;
	else
		return register_of_loop_particle_combinations.size();
}

size_t DefaultStateManager::corresponding_momDconf_for_cut(int i) const {
	if(i<1||i>int(map_cut_to_loop_combination.size())){
		std::cout<<"ERROR in DefaultStateManager::corresponding_momDconf_for_cut("<<i<<"): argument should be between 1 and "<<map_cut_to_loop_combination.size()<<" -- returned 0!"<<std::endl;
		return 0;
	}
	else{
		return map_cut_to_loop_combination[i-1];
	}
}


// Utility to count all currents associated with a given vector of vector of currents
void complete_current_couting(size_t& total,std::vector<size_t>& number_per_level,
			const std::vector<std::vector<Current*> >& lcurrents){
	total=0;
	for(size_t ii=0;ii<lcurrents.size();ii++){
		size_t lnumber(0);
		for(size_t jj=0;jj<lcurrents[ii].size();jj++){
			Current* lc(lcurrents[ii][jj]);
			if(lc->get_default_particles_processed()){
				lnumber+=(lc->get_physical_states()).size();
			}
			else{
				lnumber+=1;
			}
		}
		number_per_level.push_back(lnumber);
		total+=lnumber;
	}
}

}
