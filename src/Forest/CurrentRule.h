/**
 * @file CurrentRule.h
 *
 * @date 15.4.2020
 *
 * @brief Header file for abstract CurrentRule definition
 */
#pragma once

#include "Core/Particle.h"
#include "Coupling.h"
#include <map>
#include <string>
#include <utility>
#include <vector>

namespace Caravel {

class Current;
class CurrentBasic;

/**
 * Abstract factory for producing current rules (Feynman rules)
 */
class CurrentRule {
  private:
    bool colororder;               /**< Info about color ordering of this rule */
    std::string name;              /**< Name the rule */
    int multiplicity;              /**< Stores size of content */
    std::vector<Particle> content; /**< The main container of *this, an ordered list of particles. All type_name_list, anti_type_name_list, color_info and
                                      spin_list filled accordingly by method fill(.) */
    std::vector<ParticleType> type_name_list;      /**< An ordered list of the names of the currents that interact on this CurrentRule */
    std::vector<ParticleType> anti_type_name_list; /**< An ordered list of the names of the currents that contract with the currents in type_name_list */
    std::vector<bool> color_info;                  /**< Keep info on color properties of each current interacting in this CurrentRule */
    std::vector<ParticleStatistics> spin_list;     /**< Following order in type_name_list, it keeps information on spin of the interacting currents */
    /**
     * Maps a string (the coupling name) into a Coupling and an int, the latter representing the power of the coupling
     */
    using coupling_content = std::map<std::string, std::pair<Coupling, int>>;
    coupling_content gs; /**< Couplings associated to *this */
  protected:
    /**
     * Main method for derived classes to fill particle content of the CurrentRule. To be executed by derived constructor.
     */
    void fill(const std::vector<Particle>&);
    /**
     * Defines name. To be executed by derived constructor.
     */
    void set_name(std::string);
    /**
     * Defines if color ordered. To be executed by derived constructor.
     */
    void set_colorordered(bool);
    /**
     * Passes information about coupling of *this. To be executed by derived constructor.
     *
     * @param the coupling to be stored
     * @param int associated to the power of the coupling
     */
    void set_coupling(const Coupling&, int);

  public:
    CurrentRule() = default;
    /**
     * Boolean specifying whether the current rule is color ordered.
     *
     * @return True if rule is color ordered, else False
     */
    bool is_color_ordered() const;
    /**
     * Find if the propagation of indices is possible.
     * If yes it propagates necessary information like flavor, internal index, etc.
     * IMPORTANT: Assumes that everything else was a correct match already, no check in this
     * regard are performed.
     *
     * @param particle representing the current
     * @param parent currents
     * @param permutation of the particles in the rule to match
     */
    virtual bool propagate_indices(Particle&, const std::vector<Current*>&, const std::vector<size_t>&) const { return true; }
    /**
     * Used to identify the rule for selection rules of qqs coupling.
     * Basically just triggers on sqq vertex.
     */
    virtual bool selection_rule_relevant() const { return false; }
    /**
     * This implements the logic of propagating information about sqq couplings
     * by *this rule. Essentially this does an analytic
     * kinematics-independent part of the off-shell-recursion.
     */
    virtual void update_qqs_couplings(CurrentBasic*, int mode = 0) const;
    /**
     * Gives the number of different ways internal indices can be assigned in this rule.
     */
    virtual int n_of_modes() const { return 1; }
    /**
     * Returns the name of a current rule.
     * Example 'ggg' for 3-gluon vertex.
     *
     * @return Name of the current rule
     */
    std::string get_name() const;
    /**
     * Specifies how many currents interact in the vertex.
     * Example: 3-gluon vertex: 3
     *
     * @return The number of currents that can interact with vertex.
     */
    int get_rule_multiplicity() const;
    /**
     * Returns the power of the provided coupling in the CurrentRule.
     * Example: SM strong coupling 'gs', 3-gluon Vertex: 1, 4-gluon Vertex: 2
     *
     * @param	Coupling whose power is asked for
     * @return  Power of the provided coupling in the CurrentRule
     */
    int get_power(const Coupling&) const;
    /**
     * Explore the particle content of this CurrentRule
     */
    const std::vector<Particle>& get_particle_content() const;
    /**
     * Access the information on the type of currents that build *this CurrentRule
     */
    const std::vector<ParticleType>& get_current_type_list() const;
    /**
     * Access the information on the type of currents that can contract with each of the currents that build *this CurrentRule
     */
    const std::vector<ParticleType>& get_contract_type_list() const;
    /**
     * Method that returns a const vector of bool's to describe if each entry in the CurrentRule is or not colored
     */
    const std::vector<bool>& get_color_info_list() const;
    /**
     * Method that returns a const vector of ParticleStatistics to describe the spin of each interacting current in *this
     */
    const std::vector<ParticleStatistics>& get_statistics_list() const;
    /**
     * Virtual copy pattern to clone a CurrentRule
     */
    virtual CurrentRule* clone() const = 0;
    virtual ~CurrentRule();
};

std::ostream& operator<<(std::ostream&, const CurrentRule&);
} // namespace Caravel
