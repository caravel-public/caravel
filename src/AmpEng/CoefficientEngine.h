/**
 * @file CoefficientEngine.h
 *
 * @date 6/22/2021
 *
 * @brief A coefficient provider based on graph hierarchy subtraction
 *
*/
#pragma once

#include <cassert>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "Core/Debug.h"
#include "Core/CaravelInput.h"
#include "Core/Utilities.h"
#include "Core/settings.h"
#include "Core/momD_conf.h"
#include "Forest/Model.h"
#include "FunctionalReconstruction/DenseRational.h"
#include "Graph/IntegralGraph.h"

#include "AmpEng/CutEquation.h"
#include "AmpEng/Evaluator.h"

// for is_vanishing_tree(.)
#include "AmplitudeCoefficients/IntegrandHierarchy.h"

namespace Caravel {
namespace AmpEng {

/**
 * A parent diagram is specified by a series of external and internal particles,
 * together with corresponding coupling information
 *
 * We store the information across several containers (std::vector) // TODO: better data structure!
 */
struct ParentDiagrams {
    using Ext = std::vector<std::vector<Particle>>;
    using Int = std::vector<Particle>;
    using couplings_t = std::map<std::string, int>;
    using Coups = std::vector<couplings_t>;
    std::vector<Ext> external; /**< In each 'Ext', each entry represents a set of external particles in a corner of the 1-loop parent (counterclockwise) */
    std::vector<Int> internal; /**< In each 'Int', each entry represents the loop particle going towards corresponding corner in 'external' (counterclockwise) */
    std::vector<Coups> coups;  /**< In each 'Coups', each entry represents the couplings included on the corresponding corner in 'external' (counterclockwise) */
    couplings_t tree_couplings; /**< Couplings included in corresponding tree-level amplitude (if the map is empty, we assume no tree) */
};

ParentDiagrams get_parent_diagrams(const std::shared_ptr<Model>&, const PartialAmplitudeInput&);

/**
 * Main interface of subtraction engine tools to produce integrand coefficients
 */
template <typename T> class CoefficientEngine {
    using couplings_t = std::map<std::string, int>;
    // TODO: generic handling of D template parameter (should not be set as CoefficientEngine template parameter),
    //       so far this is dedicate to 1-loop cases
    std::shared_ptr<HierarchyEngineBase<T>> hierarchy;
    Evaluator<T> handler;
    std::string warmup_filename{"warmup_file.info"}; /**< Filename for warmup */
    ParentDiagrams parentdiags;
    settings::IntegrandHierarchy::norm_type normalization{settings::IntegrandHierarchy::amplitude_normalization}; /**< Picks default value for normalization */
    std::function<T(const momD_conf<T, 4>&)> norm; /**< Computes normalization factor for coefficients */
    T norm_value;                                  /**< Stores last normalization value */
    size_t Nf{0};                                  /**< How many light-quark loops */
    size_t Nh{0};                                  /**< How many heavy-quark loops */
  public:
    CoefficientEngine() = default;
    /**
     * Employs a PartialAmplitudeInput to build a 1-loop amplitude
     *
     * @param model to be employed in calculation
     * @param the partial amplitude which will produce corresponding parent diagrams
     * @param bool to control if external currents are always recomputed when computing cuts
     */
    CoefficientEngine(const std::shared_ptr<Model>& m, const PartialAmplitudeInput& pa, bool recompute = false)
        : CoefficientEngine(m, get_parent_diagrams(m, pa), pa.get_nf_powers(), pa.get_nh_powers(), recompute) {}
    /**
     * Main constructor for 1-loop amplitudes from parent diagrams information
     */
    CoefficientEngine(const std::shared_ptr<Model>&, const ParentDiagrams&, size_t nf = 0, size_t nh = 0, bool recompute = false);
    bool has_warmup() const { return handler.has_warmup(); }
    void warmup_run(const momD_conf<T, 4>& mc) {
        handler.reduce(mc, *hierarchy);
        hierarchy->clear_cut_cache();
    }
    /**
     * Employs a Genealogy for construction (key input for HierarchyEngineBase construction)
     *
     * @param Genealogy
     * @param model to be employed in calculation
     * @param vector of Ds dimensions (defaulted to 6)
     * @param couplings for tree-level amplitude (defaulted to empty)
     * @param bool to control if external currents are always recomputed when computing cuts
     */
    CoefficientEngine(const Genealogy& g, const std::shared_ptr<Model>& m, const std::vector<size_t>& = {6}, const couplings_t& = {}, bool recompute = false);
    bool reload_warmup() { return handler.reload_warmup(); }
    /**
     * Get access to information about integrals associated to coefficients returned by 'compute(.)'
     */
    std::vector<lGraph::IntegralGraph> get_integral_graphs() const { return hierarchy->get_integral_graphs(); }
    /**
     * Points to method compute_tHV
     */
    std::vector<DenseRational<T>> compute(const momD_conf<T, 4>&);
    /**
     * Computes amplitude in a given PSP and returns coefficients.
     *
     * Full Ds results are mapped to FDH scheme [Ds = 4]. 
     */
    std::vector<DenseRational<T>> compute_FDH(const momD_conf<T, 4>&);
    /**
     * Computes amplitude in a given PSP and returns coefficients.
     *
     * Full Ds results are mapped to tHV scheme [Ds = 4-2ep]. 
     */
    std::vector<DenseRational<T>> compute_tHV(const momD_conf<T, 4>&);
    /**
     * Computes amplitude in a given PSP and returns coefficients.
     *
     * Full Ds results are returned
     */
    std::vector<DenseRational<DenseRational<T>, T>> compute_full_Ds(const momD_conf<T, 4>&);
    /**
     * Related to 'compute_full_Ds' but returns for each master integral coefficient (entry in first std::vector) a vector of 
     * DenseRational is return (with each entry corresponding to the function returned by get_Ds_functions())
     */
    std::vector<std::vector<DenseRational<T>>> compute_coefficients_full_Ds(const momD_conf<T, 4>&);
    std::vector<DenseRational<T>> get_Ds_functions() const { return handler.get_Ds_functions(); }
    /**
     * Compute normalization only
     */
    T get_normalization(const momD_conf<T, 4>& mc) const { return norm(mc); }
    const auto& get_warmup_filename() const { return warmup_filename; }
    T get_tree(const momD_conf<T, 4>& mc) { return hierarchy->get_tree(mc); }
    void set_Ds_function_basis(const std::vector<DenseRational<T>>& vfs) { handler.set_Ds_function_basis(vfs); }

    /**
     * Computes coefficients of single diagram in hierarchy
     *
     * @param Pair that specifies coefficient targeted: {level of hierarchy, diagram}
     * @param Momentum configuration
     * @return Array of coefficient / integrals
     */
    std::vector<std::pair<DenseRational<DenseRational<T>, T>, lGraph::IntegralGraph>> compute_coefficient_full_Ds(const std::pair<size_t, size_t>&,
                                                                                                                  const momD_conf<T, 4>&);

    /**
     * Forwards request to HierarchyEngine::eval_diagram
     * 
     * @param Pair to chose diagram in the hierarchy
     * @param Momentum configuration
     * @param T representing epsilon
     */
    void reduce_diagram(const std::pair<size_t, size_t>& p, const momD_conf<T, 4>& mc, const T& eps) { hierarchy->eval_diagram(mc, eps, p); } 
    

    /**
     * Forwards requests to hierarchy (that is to the instance of HierarchyEngineBase)
     */
    void set_cut_equation(bool, const std::pair<size_t, size_t>&);
    void build_subtractions();

  private:
    void build_warmup_name(const std::shared_ptr<Model>& m, const std::vector<std::vector<Particle>>&, size_t nLoops,
                           size_t Nf, size_t Nh); /**< Standardize warmup naming */
};

} // namespace AmpEng
} // namespace Caravel

#include "CoefficientEngine.hpp"
