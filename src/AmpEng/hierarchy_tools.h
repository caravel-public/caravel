/**
 * @file hierarchy_tools.h
 *
 * @date 15.5.2019
 *
 * @brief Tools for building hierarchies in AmpEng. Started from contents earlier in cutF.{h,cpp}
 *
*/
#ifndef HIERARCHY_TOOLS_H_
#define HIERARCHY_TOOLS_H_

#include <vector>
#include <cstddef>

#include "AmpEng/cutF.h"

namespace Caravel {

namespace AmpEng{

/**
 * A function to quickly produce 1-loop parent diagrams (not cuts, as it can represent hexagons, heptagons, etc),
 * with only the following particle content:
 *
 * 1) n "gluon" particles
 *
 * 2) n "gluon" particles and 1 "q" particle and to its inmediate right one "qb" particle
 *
 * Assumes leading-color configuration
 *
 * @param std::vector containing pointers to the particles
 */
cutF get_simple_1loop_parent_diagram(std::vector<Particle*>);

/**
 * Specify the 'Internal' and 'External' particles to build a 'parent' diagram
 *
 *   Internal flavor read from right to left (i.e. quark 'q' means --<--, while anti-quark 'qb' means -->--)
 *   while External flavor is always outgoing
 *
 *   EXAMPLE: for left-turner 2q2g process
 *
 *     gL   qL     gL      gL
 *     ______________________________
 *	 |	|	|	|
 *     {q(1)} {qb(2)}  {g(3)}  {g(4)}
 *
 *	External is  { { &q1m } , { &Q2p } , { &g3m } , { &g4p } }
 *	Internal is  { &gL  , &qL  , &gL  , &gL  }
 *      couplings is { {{"gs", 1}} , {{"gs", 1}} , {{"gs", 1}} , {{"gs", 1}} }
 *
 * NOTICE: external particle in the cut will be build using pointers taken from 'reference' vector
*/
cutF get_single_1L_parent(const std::vector<std::vector<Particle>>& External, const std::vector<Particle>& Internal,
                          const std::vector<std::map<std::string, int>>& couplings, std::vector<std::vector<Particle>>& reference);

/**
 * Tool to produce parent diagrams from vectors of 'External', 'Internal' and 'coupling' containers, 
 * using the get_single_1L_parent(.) function avoid
*/
std::vector<cutF> get_1loop_parents(std::vector<std::vector<std::vector<Particle>>>& Externals, const std::vector<std::vector<Particle>>& Internals,
                          const std::vector<std::vector<std::map<std::string, int>>>& couplings);

/**
 * A function to quickly produce 2-loop maximal cuts with only the following particle content:
 *
 * 1) n "gluon" particles
 *
 * 2) n "gluon" particles and 1 "q" particle and to its inmediate right one "qb" particle
 *
 * Assumes leading-color configuration (always with a central rung containing a "gluon")
 *
 * @param std::vector containing pointers to the particles
 * @param integer describing the # of particles in the "left" rung
 */
cutF get_simple_2loop_maximal_cut(std::vector<Particle*>,int);

/**
 * A function to quickly produce all 1-loop parent diagrams (not cuts, as it can represent hexagons, heptagons, etc),
 * with only the following particle content:
 *
 * 1) n "gluon" particles and a (q,qb) pair contiguos together with m photons
 *
 * Assumes leading-color configuration
 *
 * @param std::vector containing pointers to the colored particles (the last ones being (q,qb)
 * @param std::vector containing pointers to the photons
 */
std::vector<cutF> get_simple_1loop_2qng_photons_parent_diagram(std::vector<Particle*>,std::vector<Particle*>);

/**
 * A function to quickly produce 2-loop 'maximal' diagrams, with only the following particle content:
 *
 * 1) n "gluon" particles and a quark q and antiquark qb which sandwich the gluons (q,g,g...,qb) together with m photons
 *
 * Assumes leading-color configuration
 *
 * @param A reference to a vector of cutF's where maximals will be added
 * @param std::vector containing pointers to the colored particles (always as (q,g1,...,gn,qb))
 * @param std::vector containing pointers to the photons
 * @param integer describing the # of particles in the "left" rung
 */
void simple_add_2loop_2qng_photons_parent_diagram(std::vector<cutF>&,std::vector<Particle*>,std::vector<Particle*>,size_t);

/**
 * A function to quickly produce all 1-loop parent diagrams (not cuts, as it can represent hexagons, heptagons, etc),
 * with only the following particle content:
 *
 * 1) n "gluon" particles and a (q,qb) pair contiguos together with m pairs of (l,lb) leptons
 *
 * Assumes leading-color configuration
 *
 * @param std::vector containing pointers to the colored particles (the last ones being (q,qb)
 * @param std::vector containing pairs of pointers to the pairs of leptons
 */
std::vector<cutF> get_simple_1loop_2qng_leptons_parent_diagram(std::vector<Particle*>,std::vector<std::pair<Particle*,Particle*>>);

/**
 * A function to quickly produce 2-loop 'maximal' diagrams, with only the following particle content:
 *
 * 1) n "gluon" particles and a quark q and antiquark qb which sandwich the gluons (q,g,g...,qb) together with m pairs of (l,lb) leptons
 *
 * Assumes leading-color configuration
 *
 * @param A reference to a vector of cutF's where maximals will be added
 * @param std::vector containing pointers to the colored particles (always as (q,g1,...,gn,qb))
 * @param std::vector containing pairs of pointers to the pairs of leptons
 * @param integer describing the # of particles in the "left" rung
 */
void simple_add_2loop_2qng_leptons_parent_diagram(std::vector<cutF>&,std::vector<Particle*>,std::vector<std::pair<Particle*,Particle*>>,size_t);

/**
 * A function to quickly produce 2-loop 'maximal' diagrams, with only the following particle content:
 *
 * 1) n "gluon" external particles with a quark line in the left loop (nf term)
 *
 * Assumes leading-color configuration
 *
 * @param A reference to a vector of cutF's where maximals will be added
 * @param std::vector containing pointers to the colored particles (always as (g1,...,gn))
 * @param integer describing the # of particles in the "left" rung
 */
void simple_add_2loop_ng_nf_parent_diagrams(std::vector<cutF>&,std::vector<Particle*>,size_t);

using couplings_t = std::map<std::string,int>;
/**
 * A function to assemble a maximal 2-loop cut with only 3-pt vertices. It builds a 3-rung cut where 
 * the information passed is tracked:
 *
 * 1) i (first int argument) particles down the "left" rung
 *
 * 2) j (second int argument) particles up the "right" rung
 *
 * 3) the left-over particles down the "central" rung (with couplings on top and bottom vertices passed in the central rung)
 *
 * This can be seen as a "6" routing of the 2-loop diagram (as opposed for example to the S-tree routing).
 *
 * @param std::vector containing pointers to the external particles
 * @param std::vector containing the internal particles
 * @param integer describing the # of particles in the "left" rung
 * @param integer describing the # of particles in the "right" rung
 8 @param vector containing corresponding coupling information for each vertex in the cutF
 */
cutF get_2loop_maximal_cut(std::vector<Particle*>,const std::vector<Particle>&,int,int,const std::vector<couplings_t>&);


}

}
#endif	// HIERARCHY_TOOLS_H_
