#include "AmpEng/hierarchy_tools.h"

namespace Caravel {
namespace AmpEng {

ParentDiagrams get_parent_diagrams(const std::shared_ptr<Model>& model, const PartialAmplitudeInput& pa) {
    std::vector<std::vector<Particle>> entryExternal;
    std::vector<Particle> entryInternal;
    std::vector<couplings_t> entry_cps_parent;
    couplings_t entry_cps_tree;
    Caravel::detail::construct_ordered_1L_parent_diagram(pa, entryExternal, entryInternal, entry_cps_parent, entry_cps_tree);

    ParentDiagrams pdiags = { {entryExternal}, {entryInternal}, {entry_cps_parent}, entry_cps_tree };
    return pdiags;
}

template <typename T>
CoefficientEngine<T>::CoefficientEngine(const std::shared_ptr<Model>& model, const ParentDiagrams& diagrams, size_t nf, size_t nh, bool recompute)
    : parentdiags(diagrams), Nf(nf), Nh(nh) {
    auto& External = parentdiags.external;
    auto& Internal = parentdiags.internal;
    auto& coups = parentdiags.coups;
    auto theParents = get_1loop_parents(External, Internal, coups); 
    auto maximals = CutHierarchy::get_maximal_cuts(theParents);

    auto diagramhierarchy = CutHierarchy(maximals);
    DEBUG(
        diagramhierarchy.print_all_cuts(); 
        diagramhierarchy.explore_cut_inheritance();
    );

    // FIXME for now, hard-coded Ds values
    std::vector<size_t> Dss = { 6, 8 };
    hierarchy = std::make_shared<HierarchyEngine<T, 5>>(diagramhierarchy, model, recompute, Dss, parentdiags.tree_couplings);
    DEBUG(hierarchy->get_generations().explore(););
    size_t loops(0);
    for(auto& [member, loc]: hierarchy->get_generations().get_locations()) {
        loops = member.get_graph().get_n_loops();
        break;
    }
    build_warmup_name(model, External.front(), loops, Nf, Nh);
    assert(Dss.size() > 0);
    handler = Evaluator<T>(warmup_filename, Dss.size() - 1);

    // guess normalization type if automatic
    using namespace settings::IntegrandHierarchy;
    std::vector<Particle> particles;
    for (auto& it : parentdiags.external[0]) {
        for (auto p : it) { particles.push_back(std::move(p)); }
    }
    {
        if (amplitude_normalization == norm_type::automatic) {
            if (is_vanishing_tree(PartialAmplitudeInput(particles))) { normalization = norm_type::spinor_weight; }
            else {
                normalization = norm_type::tree;
            }
        }
    }
    if(normalization == norm_type::tree)
        norm = hierarchy->get_tree_evaluator();
    else if(normalization == norm_type::spinor_weight)
        norm = construct_normalizer<T>(particles);
    // no normalization
    else
        norm = [](const momD_conf<T, 4>& mc) { return T(1); };
}

template <typename T> CoefficientEngine<T>::CoefficientEngine(const Genealogy& g, const std::shared_ptr<Model>& model, const std::vector<size_t>& Dss, const couplings_t& treecoups, bool recompute) {

    if (g.get_n_loops() == 1)
        hierarchy = std::make_shared<HierarchyEngine<T, 5>>(g, model, recompute, Dss, treecoups);
    else if (g.get_n_loops() == 2)
        hierarchy = std::make_shared<HierarchyEngine<T, 6>>(g, model, recompute, Dss, treecoups);
    else {
        std::cerr << "ERROR: only 1- and 2-loop calculations enabled!" << std::endl;
        std::exit(1);
    }

    DEBUG(hierarchy->get_generations().explore(););

    size_t loops(0);
    for(auto& [member, loc]: hierarchy->get_generations().get_locations()) {
        loops = member.get_graph().get_n_loops();
        break;
    }
    std::vector<std::vector<Particle>> External;
    for(auto [ii, pp]: g.get_graph_cut_map().external) {
        External.push_back({*pp});
    }
    build_warmup_name(model, External, loops, Nf, Nh);
    assert(Dss.size() > 0);
    handler = Evaluator<T>(warmup_filename, Dss.size() - 1);

    using namespace settings::IntegrandHierarchy;
    // guess normalization type if automatic
    if (amplitude_normalization == norm_type::automatic) {
        std::cerr << "ERROR: non automatic normalization coded yet in CoefficientEngine construction" << std::endl;
        std::exit(1);
    }
    using namespace settings::IntegrandHierarchy;
    std::vector<Particle> particles;
    for(auto [ii, pp]: g.get_graph_cut_map().external) {
        particles.push_back(*pp);
    }
    if(normalization == norm_type::tree)
        norm = hierarchy->get_tree_evaluator();
    else if(normalization == norm_type::spinor_weight)
        norm = construct_normalizer<T>(particles);
    // no normalization
    else
        norm = [](const momD_conf<T, 4>& mc) { return T(1); };
}


template <typename T>
void CoefficientEngine<T>::build_warmup_name(const std::shared_ptr<Model>& m, const std::vector<std::vector<Particle>>& particles, size_t nLoops, size_t Nf, size_t Nh) {
    warmup_filename = "warmup_";
    warmup_filename += std::to_string(nLoops) + "L_";
    for (auto vps : particles) {
        if (vps.size() > 1) warmup_filename += "[";
        for (auto ps : vps) warmup_filename += ps.get_type().to_string() + "<" + ps.external_state().get_name() + ">";
        if (vps.size() > 1) warmup_filename += "]";
    }
    if (Nf > 0) warmup_filename += ".Nf^" + std::to_string(Nf);
    if (Nh > 0) warmup_filename += ".Nh^" + std::to_string(Nh);
    if (nLoops == 1) {
        if (settings::general::one_loop_master_basis != settings::general::OneLoopMasterBasis::GKM) {
            warmup_filename += ".";
            warmup_filename += wise_enum::to_string(settings::general::one_loop_master_basis.value);
        }
    }
    else {
        warmup_filename += ".";
        warmup_filename += wise_enum::to_string(settings::general::numerator_decomposition.value);
    }
    warmup_filename += ".info";
    DEBUG_MESSAGE("Warmup filename set to: " + warmup_filename);
}

template <typename T>
std::vector<DenseRational<T>> CoefficientEngine<T>::compute(const momD_conf<T, 4>& mc) {
    return compute_tHV(mc);
}

template <typename T>
std::vector<DenseRational<T>> CoefficientEngine<T>::compute_FDH(const momD_conf<T, 4>& mc) {
    if (!has_warmup()) {
        _WARNING("Performing warmup run on the phase space point for \"compute\". Note that special simplifications can occur if the point is too simple.");
        warmup_run(mc);
    }
    auto result = handler.reduce_FDH(mc, *hierarchy);
    hierarchy->clear_cut_cache();
    norm_value = norm(mc);
    auto inv = T(1) / norm_value;
    for(auto& r: result) r = DenseRational<T>(inv*r.numerator, r.denominator); 
    return result;
}

template <typename T>
std::vector<DenseRational<T>> CoefficientEngine<T>::compute_tHV(const momD_conf<T, 4>& mc) {
    if (!has_warmup()) {
        _WARNING("Performing warmup run on the phase space point for \"compute\". Note that special simplifications can occur if the point is too simple.");
        warmup_run(mc);
    }
    auto result = handler.reduce_tHV(mc, *hierarchy);
    hierarchy->clear_cut_cache();
    norm_value = norm(mc);
    auto inv = T(1) / norm_value;
    for(auto& r: result) r = DenseRational<T>(inv*r.numerator, r.denominator); 
    return result;
}

template <typename T>
std::vector<DenseRational<DenseRational<T>, T>> CoefficientEngine<T>::compute_full_Ds(const momD_conf<T, 4>& mc) {
    if (!has_warmup()) {
        _WARNING("Performing warmup run on the phase space point for \"compute\". Note that special simplifications can occur if the point is too simple.");
        warmup_run(mc);
    }
    auto result = handler.reduce_with_Ds(mc, *hierarchy);
    hierarchy->clear_cut_cache();
    norm_value = norm(mc);
    auto inv = T(1) / norm_value;
    for (auto& r : result) {
        auto& lnumerator = r.numerator;
        for (auto& c : lnumerator.coefficients) c = DenseRational<T>(inv * c.numerator, c.denominator);
    }
    return result;
}

template <typename T>
std::vector<std::vector<DenseRational<T>>> CoefficientEngine<T>::compute_coefficients_full_Ds(const momD_conf<T, 4>& mc) {
    if (!has_warmup()) {
        _WARNING("Performing warmup run on the phase space point for \"compute\". Note that special simplifications can occur if the point is too simple.");
        warmup_run(mc);
    }
    auto result = handler.reduce_Ds_function_coefficients(mc, *hierarchy);
    hierarchy->clear_cut_cache();
    norm_value = norm(mc);
    auto inv = T(1) / norm_value;
    for (auto& mi_coeff : result) {
        for(auto& Ds_coeff: mi_coeff) Ds_coeff = DenseRational<T>(inv * Ds_coeff.numerator, Ds_coeff.denominator);
    }
    return result;
}

template <typename T>
std::vector<std::pair<DenseRational<DenseRational<T>, T>, lGraph::IntegralGraph>>
CoefficientEngine<T>::compute_coefficient_full_Ds(const std::pair<size_t, size_t>& diag, const momD_conf<T, 4>& mc) {
    if (!has_warmup()) {
        std::cerr << "ERROR: can't run compute_coefficient_full_Ds(.) without warmup!" << std::endl;
        std::exit(1);
    }
    auto result = handler.reduce_diagram_with_Ds(mc, *hierarchy, diag);
    hierarchy->clear_cut_cache();
    norm_value = norm(mc);
    auto inv = T(1) / norm_value;
    for (auto& r : result) {
        auto& lnumerator = r.numerator;
        for (auto& c : lnumerator.coefficients) c = DenseRational<T>(inv * c.numerator, c.denominator);
    }
    auto integrals = hierarchy->get_diagram_integral_graphs(diag);
    assert(result.size() == integrals.size());

    std::vector<std::pair<DenseRational<DenseRational<T>, T>, lGraph::IntegralGraph>> toret;
    for (size_t ii = 0; ii < result.size(); ii++) toret.push_back(std::make_pair(result[ii], integrals[ii]));
    return toret;
}

template <typename T>
void CoefficientEngine<T>::set_cut_equation(bool b, const std::pair<size_t, size_t>& diag) {
    hierarchy->set_cut_equation(b, diag);
}

template <typename T>
void CoefficientEngine<T>::build_subtractions() {
    hierarchy->build_subtractions();
}

#ifdef USE_FINITE_FIELDS 
extern template class CoefficientEngine<F32>; 

extern template class CoefficientEngine<C>; 
#ifdef HIGH_PRECISION 
extern template class CoefficientEngine<CHP>; 
#endif // HIGH_PRECISION
#ifdef VERY_HIGH_PRECISION
extern template class CoefficientEngine<CVHP>; 
#endif // VERY_HIGH_PRECISION

#endif // USE_FINITE_FIELDS


} // namespace AmpEng
} // namespace Caravel
