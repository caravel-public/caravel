#include "AmpEng/Parenthood.h"

namespace Caravel {
namespace AmpEng {

using namespace lGraph;

#ifdef USE_FINITE_FIELDS
momD_conf<F32, 4> get_random_external_psp_with_masses(const Member& m) {
    auto legs = m.get_xgraph().get_legs();
    std::vector<size_t> masses;
    for (auto& l : legs)
        if (l.is_on_shell())
            masses.push_back(l.get_mass_index());
        else {
            std::cerr << "ERROR: asking for a PSP without on-shell condition. The leg: " << l << " and the diagram: " << std::endl;
            m.get_xgraph().show(std::cerr);
            std::exit(1);
        }
    size_t n = legs.size();
    return Caravel::ps_parameterizations::random_rational_mom_conf<F32>(n, masses);
}

momD_conf<F32, 4> get_random_external_psp(const Member& m) {
    auto legs = m.get_xgraph().get_legs();
    // check if massive
    for (auto& l : legs)
        if (l.is_on_shell())
            if (l.get_mass_index() != 0) return get_random_external_psp_with_masses(m);
    size_t n = legs.size();
    return Caravel::ps_parameterizations::random_rational_mom_conf<F32>(n);
}

OnShellPoint<F32, 6> get_random_internal_psp_mapped(const Member& parent, const Member& daughter, const Registro& reg, const momD_conf<F32, 4>& momc4D) {
    EpsilonBasis<F32>::_make_sure_squares_are_initialized();
    using lGraphKin::GraphKin;
    using lGraph::lGraphKin::MomentumRoutingEvaluator;
    OnShellPoint<F32, 6> toret;
    auto vardims = parent.get_xgraph().n_OS_variables_with_special_routing();
    std::vector<std::vector<F32>> vars;
    for (size_t ii = 0; ii < vardims.size(); ii++) {
        vars.push_back(std::vector<F32>());
        for (size_t jj = 0; jj < vardims[ii]; jj++) vars.back().push_back(F32(std::rand() / 42));
    }
    if (parent.get_xgraph().get_n_loops() == 1) {
        momD_conf<F32, 5> momc(momc4D);
        GraphKin<F32, 5> graphkin(parent.get_xgraph());
        graphkin.change_point(momc);
        auto get_momenta = surd_parameterization<F32>(parent.get_xgraph(), graphkin);
        auto resP = get_momenta(vars);
        // check on-shellness
        auto OnS_checker = lGraph::lGraphKin::LoopOnShellChecker<F32, 5>(parent.get_xgraph());
        if (OnS_checker(resP, momc4D) != 0) {
            std::cerr << "ERROR: on-shell parent momenta check in get_random_internal_psp_mapped(.) failed for the graph: " << parent.get_xgraph() << std::endl;
            std::exit(1);
        }
        auto PtoDmap = MomentumRoutingEvaluator<F32, 5>(TransformOSMapMomRouting(reg.lD_from_lP[0]));
        auto res = PtoDmap(resP, momc);
        toret.push_back(momD<F32, 6>(res));
    }
    else {
        momD_conf<F32, 6> momc(momc4D);
        GraphKin<F32, 6> graphkin(parent.get_xgraph());
        graphkin.change_point(momc);
        auto get_momenta = surd_parameterization<F32>(parent.get_xgraph(), graphkin);
        auto resP = get_momenta(vars);
        // check on-shellness
        auto OnS_checker = lGraph::lGraphKin::LoopOnShellChecker<F32, 6>(parent.get_xgraph());
        if (OnS_checker(resP, momc4D) != 0) {
            std::cerr << "ERROR: on-shell parent momenta check in get_random_internal_psp_mapped(.) failed for the graph: " << parent.get_xgraph() << std::endl;
            std::exit(1);
        }
        auto PtoDmap1 = MomentumRoutingEvaluator<F32, 6>(TransformOSMapMomRouting(reg.lD_from_lP[0]));
        auto PtoDmap2 = MomentumRoutingEvaluator<F32, 6>(TransformOSMapMomRouting(reg.lD_from_lP[1]));
        auto res1 = PtoDmap1(resP, momc);
        auto res2 = PtoDmap2(resP, momc);
        toret.push_back(momD<F32, 6>(res1));
        toret.push_back(momD<F32, 6>(res2));
    }
    return toret;
}

Builder<F32, 6> get_daughter_builder(const Member& daughter, xGraph& xg4b, const std::string& smodel, const GraphCutMap& gcm) {
    size_t n = daughter.get_xgraph().get_legs().size();
    std::shared_ptr<Model> model2use{model_from_name(smodel)};
    Forest forest(*model2use);
    forest.set_minimal_states_in_currents(false);
    if (daughter.has_cut()) {
        auto lcut = daughter.get_cut();
        lcut.add_to_forest(forest, 0, false);
    }
    else {
        std::vector<LoopMomLocator> loop_particle_container;
        auto ctopology = _misc::getcutTopology(daughter.get_xgraph(), xg4b, loop_particle_container, gcm);
        std::vector<std::tuple<int, int, int, int>> loop_parts_out;
        for (size_t i = 0; i < loop_particle_container.size(); ++i)
            loop_parts_out.push_back(std::make_tuple(loop_particle_container[i].looploc, loop_particle_container[i].mlooploc,
                                                     loop_particle_container[i].looploc_momconf, loop_particle_container[i].mlooploc_momconf));
        forest.add_cut(loop_parts_out, ctopology);
    }
    // FIXME: don't hard code dimension
    forest.define_internal_states_and_close_forest({8});
    return Builder<F32, 6>(&forest, n);
}
#endif

bool perform_extra_check_spurious_fact_channel(const Member& parent, const Member& daughter, const Registro& reg) {
    // FIXME: better check of non-colored case
    if(daughter.get_cut().get_short_name() != "" && daughter.get_cut().get_short_name() != " ") {
        using xGtype = Graph<Node, Connection<Strand>>;
        // generic massless bubble
        static const xGraph bubble = xGtype("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]]]]");
        if(daughter.get_xgraph().get_topology() == bubble.get_topology())
            // flipped propagators imply color-structure change
            if(reg.lP_from_lD[0].loop_sign[0] < 0) return true;
    }
    return false;
}

/**
 * Checks 1-parent factorization channel by exploring the daughter's cut (on a Forest/Builder calculation) on the parent's on-shell phase space
 */
bool check_spurious_fact_channel(const Member& parent, const Member& daughter, const Registro& reg, const std::string& smodel, const GraphCutMap& gcm) {
#ifdef USE_FINITE_FIELDS
    auto momc = get_random_external_psp(parent);
    auto momenta = get_random_internal_psp_mapped(parent, daughter, reg, momc);
    auto builder = get_daughter_builder(daughter, daughter.xg_routing, smodel, gcm);
    using lGraph::lGraphKin::LoopOnShellChecker;
    auto OnS_checker = LoopOnShellChecker<F32, 6>(daughter.get_xgraph());
    if (OnS_checker(momenta, momc) != 0) {
        std::cerr << "ERROR: on-shell momenta check in check_spurious_fact_channel(.) failed for the graph: " << daughter.get_xgraph() << std::endl;
        std::exit(1);
    }
    auto mommapper = OnShellStrategies::OnShellPointMap<F32, 6>(daughter.get_xgraph(), daughter.xg_routing);
    builder.set_p(momc);
    builder.set_loop(mommapper(momenta, momc));
    builder.compute();
    try {
        // we only add cut related to daughter (possibly 'joined' if fermions involved)
        builder.compute_cut(1);
    }
    catch (const Caravel::DivisionByZeroException& ee) {
        DEBUG(
            std::cout << "CAUGHT: division-by-zero exception, assuming it is an ok fact channel! " << std::endl;
        );
        return perform_extra_check_spurious_fact_channel(parent, daughter, reg);
    }
    DEBUG(
        std::cout << "No division by zero, assuming it isn't a fact channel! " << std::endl;
        _PRINT(parent.get_xgraph());
        _PRINT(daughter.get_xgraph());
        _PRINT(reg);
    );
    return true;
#else
    _WARNING("No check performed on check_spurious_fact_channel(.). Returned false");
    return false;
#endif
}

std::ostream& operator<<(std::ostream& o, const LoopMomLocator& l) {
    o << "{p: " << l.p <<", loc: " <<l.looploc<<", -loc: "<<l.mlooploc << ", loc_momconf: " << l.looploc_momconf << ", -loc_momconf: " << l.mlooploc_momconf <<"}";

    return o;
}

namespace _misc {

Particle _produce_a_particle_from_mass_index(size_t massindex, const GraphCutMap& gcm, const std::string& name, const SingleState& ss, size_t momindex) {
    bool found(false);
    ParticleType ptype;
    // massindex should math an entry of gcm.internal
    for(auto& [index, lpt]: gcm.internal) {
        if (lpt.get_mass_index() == massindex) {
            ptype = lpt;
            found = true;
            break;
        }
    }
    if (!found) {
        std::cerr << "ERROR: _produce_a_particle_from_mass_index(.) did not find ParticleType with massindex = " << massindex << " in passed GraphCutMap" << std::endl;
        std::exit(1);
    }
    return Particle(ptype.to_string() + name, ptype, ss, momindex);
}

Particle* _get_particle_pointer_from_mom_index(const lGraph::Leg& l, const GraphCutMap& gcm) {
    std::shared_ptr<Particle> tr;
    for(auto& [index, pp]: gcm.external) {
        // as indices in graph are mapped, we check which Particle in gcm matches
        if(l.get_momentum_index() == pp->mom_index()) {
            tr = pp;
            break;
        }
    }
    if(!tr) {
        std::cerr<<"ERROR: _misc::_get_particle_pointer_from_mom_index(.) failed to find "<< l <<" in graph-cut map"<<std::endl;
        std::exit(1);
    }
    return tr.get();
}

Particle* _get_particle_pointer(const lGraph::Leg& l) {
    static std::mutex access;
    std::lock_guard<std::mutex> lock(access);
    static std::unordered_map<std::pair<size_t, std::pair<std::string, std::string>>, std::shared_ptr<Particle>> pcontainer;
    auto key = std::make_pair(l.get_momentum_index(), l.ptype);
    auto it = pcontainer.find(key);
    if (it != pcontainer.end()) {
        // sanity check
        auto& part = it->second;
        if (l.ptype.first != part->get_type().to_string()) {
            std::cerr << "ERROR: _get_particle_pointer(leg) tried to match to particle " << *part << " when leg has " << l.ptype.first << std::endl;
            std::exit(1);
        }
        if (l.ptype.second != part->external_state().get_name()) {
            std::cerr << "ERROR: _get_particle_pointer(leg) tried to match to particle with state " << *part << " when leg has state " << l.ptype.second
                      << std::endl;
            std::exit(1);
        }
        return part.get();
    }
    pcontainer[key] = std::make_shared<Particle>(l.ptype.first + "_mom" + std::to_string(l.get_momentum_index()), ParticleType(l.ptype.first),
                                                                    SingleState(l.ptype.second), l.get_momentum_index());
    return pcontainer.at(key).get();
}

std::vector<Particle*> _get_vertex_particle_pointers_from_mom_indices(const std::vector<lGraph::Leg>& ls, const GraphCutMap& gcm) {
    std::vector<Particle*> tret;
    for(auto l: ls)    tret.push_back(_get_particle_pointer_from_mom_index(l, gcm));
    return tret;
}

std::vector<Particle*> _get_vertex_particle_pointers(const std::vector<lGraph::Leg>& ls) {
    std::vector<Particle*> tret;
    for (auto l : ls) tret.push_back(_get_particle_pointer(l));
    return tret;
}

bool ptype_is_less_than(const ParticleType& pt1, const ParticleType& pt2) {
    // here we just give an ordering to priority of loop opening
    static const std::unordered_map<std::string, size_t> compare_map = {
        {"Phi1", 0}, 
        {"Phi2", 0}, 
        {"cPhi1", 0}, 
        {"cbPhi1", 0}, 
        {"Phi2", 0}, 
        {"cPhi2", 0}, 
        {"cbPhi2", 0}, 
        {"photon", 1}, 
        {"V1", 2}, 
        {"V2", 2}, 
        {"cV1", 2}, 
        {"cbV1", 2}, 
        {"G", 3},
        {"M", 4},
    };
    auto it1 = compare_map.find(pt1.to_string());
    auto it2 = compare_map.find(pt2.to_string());
    if (it1 == compare_map.end() || it2 == compare_map.end()) {
        std::cerr << "ERROR: ptype_is_less_than(.) did not find one of the particle types: " << pt1 << ", " << pt2 << std::endl;
        std::exit(1);
    }
    return it1->second < it2->second;
}

size_t get_1loop_optimal_shift(const xGraph& xg) {
    assert(xg.get_n_loops() == 1);
    size_t shift(0);
    const auto& strand = xg.get_base_graph().get_connection(0, 0)[0];
    auto ptype = ParticleType(strand.get_links()[0].ptype.particle);
    for (size_t kk = 1; kk < strand.get_links().size(); kk++) {
        auto lptype = ParticleType(strand.get_links()[kk].ptype.particle);
        if (ptype_is_less_than(lptype, ptype)) {
            shift = kk;
            ptype = lptype;
        }
    }
    return shift;
}

size_t get_1loop_optimal_shift(const xGraph& xg, const GraphCutMap& gcm) {
    assert(xg.get_n_loops() == 1);
    size_t shift(0);
    const auto& strand = xg.get_base_graph().get_connection(0, 0)[0];
    auto ptype = gcm.internal.at(strand.get_links()[0].get_mass_index());
    for (size_t kk = 1; kk < strand.get_links().size(); kk++) {
        auto lptype = gcm.internal.at(strand.get_links()[kk].get_mass_index());
        if (ptype_is_less_than(lptype, ptype)) {
            shift = kk;
            ptype = lptype;
        }
    }
    return shift;
}

cutTopology getcutTopology1L(const xGraph& xg, std::vector<LoopMomLocator>& lml) {
    if (!xg.fields_defined()) {
        std::cerr << "ERROR: called getcutTopology1L(xg,vLm) without fields defined!" << std::endl;
        std::exit(1);
    }
    auto shift = get_1loop_optimal_shift(xg);
    cutTopology toret;
    auto& joint = xg.get_base_graph().get_nodes()[0];
    auto& strand = xg.get_base_graph().get_connection_map().at({0, 0})[0];
    size_t nparticles = xg.get_n_legs();

    std::vector<std::vector<lGraph::Leg>> thelegs;
    using couplings_t = std::map<std::string, int>;
    std::vector<couplings_t> thecoups;
    for (auto b : strand.get_beads()) {
        thelegs.push_back(b.get_legs());
        thecoups.push_back(b.coups);
    }
    thelegs.push_back(joint.get_legs());
    thecoups.push_back(joint.coups);
    auto thelinks = strand.get_links();
    assert(thelinks.size() == thelegs.size());

    // rotate to proper opening of the loop
    std::rotate(thelegs.begin(), thelegs.begin() + shift, thelegs.end());
    std::rotate(thelinks.begin(), thelinks.begin() + shift, thelinks.end());
    std::rotate(thecoups.begin(), thecoups.begin() + shift, thecoups.end());

    // initial info
    auto testp = Particle(thelinks[0].ptype.particle + "L", ParticleType(thelinks[0].ptype.particle), SingleState("default"), 0);
    auto leftp = Particle("n" + testp.get_name(), testp.get_anti_type(), testp.external_state(), 0, 0);
    lml.push_back({leftp, int(nparticles + 1), 0, int(nparticles + 1), int(nparticles + 2)});
    auto rightp = testp;
    rightp.set_momentum_index(0);

    // traverse containers
    for (size_t ll = 0; ll < thelinks.size(); ll++) {
        auto& nextlink = thelinks[(ll + 1) % thelinks.size()];
        auto& legs = thelegs[ll];
        auto vertexps = _get_vertex_particle_pointers(legs);
        leftp = Particle("n" + rightp.get_name(), rightp.get_anti_type(), rightp.external_state(), 0, 0);
        rightp = Particle(nextlink.ptype.particle + "L", ParticleType(nextlink.ptype.particle), SingleState("default"), 0);
        // Particle
        toret.add_vertex(leftp, vertexps, rightp, thecoups[ll]);
    }

    return toret;
}

cutTopology getcutTopology1L(const xGraph& xg, std::vector<LoopMomLocator>& lml,  const GraphCutMap& gcm) {
    if (xg.fields_defined()) return getcutTopology1L(xg, lml);
    auto shift = get_1loop_optimal_shift(xg, gcm);
    cutTopology toret;
    auto& joint = xg.get_base_graph().get_nodes()[0];
    auto& strand = xg.get_base_graph().get_connection_map().at({0, 0})[0];
    size_t nparticles = xg.get_n_legs();

    std::vector<std::vector<lGraph::Leg>> thelegs;
    for(auto b: strand.get_beads()) thelegs.push_back(b.get_legs());
    thelegs.push_back(joint.get_legs());
    auto thelinks = strand.get_links();
    assert(thelinks.size() == thelegs.size());

    // rotate to proper opening of the loop
    std::rotate(thelegs.begin(), thelegs.begin() + shift, thelegs.end());
    std::rotate(thelinks.begin(), thelinks.begin() + shift, thelinks.end());

    // initial info
    auto testp = _produce_a_particle_from_mass_index(thelinks[0].get_mass_index(), gcm,  "L",  SingleState("default"));
    lml.push_back({testp, int(nparticles+1), 0, int(nparticles+1), int(nparticles+2) });
    auto leftp = testp;
    auto rightp = testp; 
    rightp.set_momentum_index(0);

    // traverse containers
    for(size_t ll = 0; ll < thelinks.size(); ll++){
        auto& nextlink = thelinks[(ll+1)%thelinks.size()];
        auto& legs = thelegs[ll];
        auto vertexps = _get_vertex_particle_pointers_from_mom_indices(legs, gcm);
        leftp = Particle("n"+rightp.get_name(), rightp.get_anti_type(), rightp.external_state(), 0, 0);
        rightp = _produce_a_particle_from_mass_index(nextlink.get_mass_index(), gcm,  "L",  SingleState("default"));
        //Particle
        toret.add_vertex(leftp, vertexps, rightp, { {gcm.coup, vertexps.size()} });
    }

    return toret;
}

WISE_ENUM_CLASS(FlowDirection, down, up)

struct StreeEntry {
    std::pair<size_t, size_t> connection;
    size_t strand;
    FlowDirection direction;
};

struct StreeStruct {
    std::vector<StreeEntry> path;
};

bool choice_is_less_than(const std::pair<StreeEntry, StreeEntry>& c1, const std::pair<StreeEntry, StreeEntry>& c2, const xGraph& xg) {
    std::vector<ParticleType> v1, v2;
    {
        const auto& strand1 = xg.get_base_graph().get_connection_map().at(c1.first.connection)[c1.first.strand];
        const auto& strand2 = xg.get_base_graph().get_connection_map().at(c1.second.connection)[c1.second.strand];
        v1.push_back(ParticleType(strand1.get_links().front().ptype.particle));
        v1.push_back(ParticleType(strand2.get_links().back().ptype.particle));
    }
    {
        const auto& strand1 = xg.get_base_graph().get_connection_map().at(c2.first.connection)[c2.first.strand];
        const auto& strand2 = xg.get_base_graph().get_connection_map().at(c2.second.connection)[c2.second.strand];
        v2.push_back(ParticleType(strand1.get_links().front().ptype.particle));
        v2.push_back(ParticleType(strand2.get_links().back().ptype.particle));
    }
    std::sort(v1.begin(), v1.end(), ptype_is_less_than);
    std::sort(v2.begin(), v2.end(), ptype_is_less_than);
    return std::lexicographical_compare(v1.begin(), v1.end(), v2.begin(), v2.end(), ptype_is_less_than);
}

bool choice_is_less_than(const std::pair<StreeEntry, StreeEntry>& c1, const std::pair<StreeEntry, StreeEntry>& c2, const xGraph& xg, const GraphCutMap& gcm) {
    std::vector<ParticleType> v1, v2;
    {
        const auto& strand1 = xg.get_base_graph().get_connection_map().at(c1.first.connection)[c1.first.strand];
        const auto& strand2 = xg.get_base_graph().get_connection_map().at(c1.second.connection)[c1.second.strand];
        v1.push_back({gcm.internal.at(strand1.get_links().front().get_mass_index())});
        v1.push_back({gcm.internal.at(strand2.get_links().back().get_mass_index())});
    }
    {
        const auto& strand1 = xg.get_base_graph().get_connection_map().at(c2.first.connection)[c2.first.strand];
        const auto& strand2 = xg.get_base_graph().get_connection_map().at(c2.second.connection)[c2.second.strand];
        v2.push_back({gcm.internal.at(strand1.get_links().front().get_mass_index())});
        v2.push_back({gcm.internal.at(strand2.get_links().back().get_mass_index())});
    }
    std::sort(v1.begin(), v1.end(), ptype_is_less_than);
    std::sort(v2.begin(), v2.end(), ptype_is_less_than);
    return std::lexicographical_compare(v1.begin(), v1.end(), v2.begin(), v2.end(), ptype_is_less_than);
}

StreeStruct get_2loop_optimal_Stree(const xGraph& xg) {
    static std::unordered_map<Graph<Node, Connection<Strand>>, StreeStruct> store;
    auto it = store.find(xg.get_base_graph());
    if (it != store.end()) return it->second;

    assert(xg.get_n_loops() == 2);
    StreeStruct toret;
    if (xg.get_base_graph().is_factorizable()) {
        // resolve first loop
        const auto& fstrand = xg.get_base_graph().get_connection(0, 0)[0];
        auto iptype = ParticleType(fstrand.get_links().front().ptype.particle);
        auto fptype = ParticleType(fstrand.get_links().back().ptype.particle);
        if (ptype_is_less_than(iptype, fptype)) { toret.path.push_back({std::make_pair(0, 0), size_t(0), FlowDirection::down}); }
        else { toret.path.push_back({std::make_pair(0, 0), size_t(0), FlowDirection::up}); }
        // resolve second loop
        const auto& sstrand = xg.get_base_graph().get_connection(0, 0)[1];
        iptype = ParticleType(sstrand.get_links().front().ptype.particle);
        fptype = ParticleType(sstrand.get_links().back().ptype.particle);
        if (ptype_is_less_than(iptype, fptype)) { toret.path.push_back({std::make_pair(0, 0), size_t(1), FlowDirection::up}); }
        else { toret.path.push_back({std::make_pair(0, 0), size_t(1), FlowDirection::down}); }
    }
    else {
        // keep six possible combinations (each pair: {initiating Stree, closing Stree})
        static const std::vector<std::pair<StreeEntry, StreeEntry>> choices = {
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 0, FlowDirection::down}, {std::make_pair(0, 1), 1, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 0, FlowDirection::down}, {std::make_pair(0, 1), 2, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 1, FlowDirection::down}, {std::make_pair(0, 1), 0, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 1, FlowDirection::down}, {std::make_pair(0, 1), 2, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 2, FlowDirection::down}, {std::make_pair(0, 1), 0, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 2, FlowDirection::down}, {std::make_pair(0, 1), 1, FlowDirection::up}),
        };
        auto min = choices[0];
        for (size_t kk = 1; kk < 6; kk++) {
            if (choice_is_less_than(choices[kk], min, xg)) min = choices[kk];
        }
        size_t strandsum(0);
        strandsum += min.first.strand;
        strandsum += min.second.strand;
        toret.path.push_back({min.first.connection, min.first.strand, FlowDirection::down});
        switch (strandsum) {
            case 1: toret.path.push_back({std::make_pair(0, 1), 2, FlowDirection::up}); break;
            case 3: toret.path.push_back({std::make_pair(0, 1), 0, FlowDirection::up}); break;
            case 2: toret.path.push_back({std::make_pair(0, 1), 1, FlowDirection::up}); break;
        }
        toret.path.push_back({min.second.connection, min.second.strand, FlowDirection::down});
    }
    store[xg.get_base_graph()] = toret;
    return toret;
}


StreeStruct get_2loop_optimal_Stree(const xGraph& xg, const GraphCutMap& gcm) {
    static std::unordered_map<Graph<Node, Connection<Strand>>, StreeStruct> store;
    auto it = store.find(xg.get_base_graph());
    if (it != store.end()) return it->second;

    assert(xg.get_n_loops() == 2);
    StreeStruct toret;
    if (xg.get_base_graph().is_factorizable()) {
        // resolve first loop
        const auto& fstrand = xg.get_base_graph().get_connection(0, 0)[0];
        auto iptype = gcm.internal.at(fstrand.get_links().front().get_mass_index());
        auto fptype = gcm.internal.at(fstrand.get_links().back().get_mass_index());
        if (ptype_is_less_than(iptype, fptype)) { toret.path.push_back({std::make_pair(0, 0), size_t(0), FlowDirection::down}); }
        else { toret.path.push_back({std::make_pair(0, 0), size_t(0), FlowDirection::up}); }
        // resolve second loop
        const auto& sstrand = xg.get_base_graph().get_connection(0, 0)[1];
        iptype = gcm.internal.at(sstrand.get_links().front().get_mass_index());
        fptype = gcm.internal.at(sstrand.get_links().back().get_mass_index());
        if (ptype_is_less_than(iptype, fptype)) { toret.path.push_back({std::make_pair(0, 0), size_t(1), FlowDirection::up}); }
        else { toret.path.push_back({std::make_pair(0, 0), size_t(1), FlowDirection::down}); }
    }
    else {
        // keep six possible combinations (each pair: {initiating Stree, closing Stree})
        static const std::vector<std::pair<StreeEntry, StreeEntry>> choices = {
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 0, FlowDirection::down}, {std::make_pair(0, 1), 1, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 0, FlowDirection::down}, {std::make_pair(0, 1), 2, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 1, FlowDirection::down}, {std::make_pair(0, 1), 0, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 1, FlowDirection::down}, {std::make_pair(0, 1), 2, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 2, FlowDirection::down}, {std::make_pair(0, 1), 0, FlowDirection::up}), 
            std::make_pair<StreeEntry, StreeEntry>({std::make_pair(0, 1), 2, FlowDirection::down}, {std::make_pair(0, 1), 1, FlowDirection::up}),
        };
        auto min = choices[0];
        for (size_t kk = 1; kk < 6; kk++) {
            if (choice_is_less_than(choices[kk], min, xg, gcm)) min = choices[kk];
        }
        size_t strandsum(0);
        strandsum += min.first.strand;
        strandsum += min.second.strand;
        toret.path.push_back({min.first.connection, min.first.strand, FlowDirection::down});
        switch (strandsum) {
            case 1: toret.path.push_back({std::make_pair(0, 1), 2, FlowDirection::up}); break;
            case 3: toret.path.push_back({std::make_pair(0, 1), 0, FlowDirection::up}); break;
            case 2: toret.path.push_back({std::make_pair(0, 1), 1, FlowDirection::up}); break;
        }
        toret.path.push_back({min.second.connection, min.second.strand, FlowDirection::down});
    }
    store[xg.get_base_graph()] = toret;
    return toret;
}


void _process_strand_for_cutTopology(cutTopology& ct, const lGraph::Strand& strand, int& location, Particle& finalright,
                                     bool specialp, bool special_leftright, size_t position_special,
                                     const Particle& particle_special) {
    auto testp = Particle(strand.get_link(0).ptype.particle + "L", ParticleType(strand.get_link(0).ptype.particle),  SingleState("default"));
    // irrelevant what we set here
    auto leftp = testp;
    auto rightp = testp;
    // traverse strand
    for (size_t ll = 0; ll < strand.size(); ll++) {
        auto& nextlink = strand.get_link(ll + 1);
        auto& bead = strand.get_bead(ll);
        auto vertexps = _get_vertex_particle_pointers(bead.get_legs());
        leftp = Particle("n" + rightp.get_name(), rightp.get_anti_type(), rightp.external_state(), 0, 0);
        rightp = Particle(nextlink.ptype.particle + "L", ParticleType(nextlink.ptype.particle),  SingleState("default"));
        if(specialp) {
            if(ll == position_special - 1) {
                // true: left
                if(special_leftright) {
                    leftp = Particle("n" + particle_special.get_name(), particle_special.get_anti_type(), particle_special.external_state(), 0, 0);
                }
                else {
                    rightp = Particle("n" + particle_special.get_name(), particle_special.get_anti_type(), particle_special.external_state(), 0, 0);
                }
            }
        }

        // Particle
        ct.add_vertex(leftp, vertexps, rightp, bead.coups);
        location += int(vertexps.size());
    }
    finalright = Particle("n" + rightp.get_name(), rightp.get_anti_type(), rightp.external_state(), 0, 0);
}

void _process_strand_for_cutTopology(cutTopology& ct, const lGraph::Strand& strand, int& location, Particle& finalright,
                                     const GraphCutMap& gcm, bool specialp, bool special_leftright, size_t position_special,
                                     const Particle& particle_special) {
    auto testp = _produce_a_particle_from_mass_index(strand.get_link(0).get_mass_index(), gcm,  "L",  SingleState("default"));
    // irrelevant what we set here
    auto leftp = testp;
    auto rightp = testp;
    // traverse strand
    for (size_t ll = 0; ll < strand.size(); ll++) {
        auto& nextlink = strand.get_link(ll + 1);
        auto& bead = strand.get_bead(ll);
        auto vertexps = _get_vertex_particle_pointers_from_mom_indices(bead.get_legs(), gcm);
        leftp = Particle("n" + rightp.get_name(), rightp.get_anti_type(), rightp.external_state(), 0, 0);
        rightp = _produce_a_particle_from_mass_index(nextlink.get_mass_index(), gcm,  "L",  SingleState("default"));
        if(specialp) {
            if(ll == position_special - 1) {
                // true: left
                if(special_leftright) {
                    leftp = Particle("n" + particle_special.get_name(), particle_special.get_anti_type(), particle_special.external_state(), 0, 0);
                }
                else {
                    rightp = Particle("n" + particle_special.get_name(), particle_special.get_anti_type(), particle_special.external_state(), 0, 0);
                }
            }
        }

        // Particle
        ct.add_vertex(leftp, vertexps, rightp, {{gcm.coup, vertexps.size()}});
        location += int(vertexps.size());
    }
    finalright = Particle("n" + rightp.get_name(), rightp.get_anti_type(), rightp.external_state(), 0, 0);
}

cutTopology getcutTopology2L(const xGraph& xg, std::vector<LoopMomLocator>& lml) {
    if (!xg.fields_defined()) {
        std::cerr << "ERROR: called getcutTopology2L(xg,vLm) without fields defined!" << std::endl;
        std::exit(1);
    }
    // the function will choose an optimal S-tree choice according to the hierarchy defined in 'ptype_is_less_than'
    auto Streechoice = get_2loop_optimal_Stree(xg);

    cutTopology toret;
    size_t nparticles = xg.get_n_legs();
    auto& nodes = xg.get_base_graph().get_nodes();
    int location(0);
    // initial strand to traverse
    auto conn = xg.get_base_graph().get_connection_map().at(Streechoice.path.front().connection);
    auto istrand = conn[Streechoice.path.front().strand];
    if (Streechoice.path.front().direction == FlowDirection::up) istrand.reverse();

    auto testp1 = Particle(istrand.get_link(0).ptype.particle + "L1", ParticleType(istrand.get_link(0).ptype.particle), SingleState("default"));
    auto testp2 = testp1;

    // final strand to traverse (we use the same 'conn' as it doesn't change a two loops)
    auto fstrand = conn[Streechoice.path.back().strand];
    if (Streechoice.path.back().direction == FlowDirection::up) fstrand.reverse();

    testp2 = Particle(fstrand.get_links().back().ptype.antiparticle + "L2", ParticleType(fstrand.get_links().back().ptype.antiparticle), SingleState("default"));

    Particle finalright;
    _process_strand_for_cutTopology(toret, istrand, location, finalright, true, true, 1, testp1);

    lml = std::vector<LoopMomLocator>(2);
    lGraph::Node node = nodes[0];
    // not factorized
    if (nodes.size() > 1) { node = nodes[1]; }
    location += node.size();
    // second loop particle
    int loop2location = location + 1;
    lml[1] = {testp2, loop2location, int(nparticles + 3), int(nparticles + 3), int(nparticles + 4)};
    ++location;
    auto vertexps = _get_vertex_particle_pointers(node.get_legs());
    vertexps.push_back(&(lml[1].p));
    // factorized
    if (nodes.size() == 1) {
        int loop1location = location + 1;
        ++location;
        lml[0] = {testp1, loop1location, 0, int(nparticles + 1), int(nparticles + 2)};
        vertexps.push_back(&(lml[0].p));
    }
    auto leftp = Particle("n" + finalright.get_name(), finalright.get_type(), finalright.external_state(), 0, 0);
    // in this case, the 'add_vertex' that follows is first vertex in toret
    if (istrand.size() == 0) leftp = Particle("n" + testp1.get_name(), testp1.get_anti_type(), testp1.external_state(), 0, 0);

    // only non-factorized graph
    for (size_t ss = 1; ss + 1 < Streechoice.path.size(); ss++) {
        // central strand to traverse (we use the same 'conn' as it doesn't change a two loops)
        auto cstrand = conn[Streechoice.path[ss].strand];
        // always needs reverse
        cstrand.reverse();
        auto rightp = Particle(cstrand.get_link(0).ptype.particle + "L", ParticleType(cstrand.get_link(0).ptype.particle), SingleState("default"));
        toret.add_vertex(leftp, vertexps, rightp, nodes[1].coups);
        _process_strand_for_cutTopology(toret, cstrand, location, finalright, false, false, 0, testp1);

        // add node
        location += nodes[0].size();
        vertexps = _get_vertex_particle_pointers(nodes[0].get_legs());
        int loop1location = location + 1;
        ++location;
        lml[0] = {testp1, loop1location, 0, int(nparticles + 1), int(nparticles + 2)};
        vertexps.push_back(&(lml[0].p));

        if (cstrand.size() == 0)
            leftp = Particle("n" + rightp.get_name(), rightp.get_anti_type(), rightp.external_state(), 0, 0);
        else
            leftp = Particle("n" + finalright.get_name(), finalright.get_anti_type(), finalright.external_state(), 0, 0);
    }

    auto rightp = Particle(fstrand.get_link(0).ptype.particle + "L", ParticleType(fstrand.get_link(0).ptype.particle), SingleState("default"));
    // in this case, the following 'add_vertex' is the last vertex in toret
    if (fstrand.size() == 0) rightp = Particle("n" + testp2.get_name(), testp2.get_anti_type(), testp2.external_state(), 0, 0);

    toret.add_vertex(leftp, vertexps, rightp, nodes[0].coups);

    _process_strand_for_cutTopology(toret, fstrand, location, finalright, true, false, fstrand.size(), testp2);

    return toret;
}

cutTopology getcutTopology2L(const xGraph& xg, std::vector<LoopMomLocator>& lml, const GraphCutMap& gcm) {
    if (xg.fields_defined()) return getcutTopology2L(xg, lml);
    // the function will choose an optimal S-tree choice according to the hierarchy defined in 'ptype_is_less_than'
    auto Streechoice = get_2loop_optimal_Stree(xg, gcm);

    cutTopology toret;
    size_t nparticles = xg.get_n_legs();
    auto& nodes = xg.get_base_graph().get_nodes();
    int location(0);
    // initial strand to traverse
    auto conn = xg.get_base_graph().get_connection_map().at(Streechoice.path.front().connection);
    auto istrand = conn[Streechoice.path.front().strand];
    if (Streechoice.path.front().direction == FlowDirection::up) istrand.reverse();

    auto testp1 = _produce_a_particle_from_mass_index(istrand.get_link(0).get_mass_index(), gcm, "L1", SingleState("default"));
    auto testp2 = testp1;

    // final strand to traverse (we use the same 'conn' as it doesn't change a two loops)
    auto fstrand = conn[Streechoice.path.back().strand];
    if (Streechoice.path.back().direction == FlowDirection::up) fstrand.reverse();

    testp2 = _produce_a_particle_from_mass_index(fstrand.get_links().back().get_mass_index(), gcm, "L2", SingleState("default"));

    Particle finalright;
    _process_strand_for_cutTopology(toret, istrand, location, finalright, gcm, true, true, 1, testp1);

    lml = std::vector<LoopMomLocator>(2);
    lGraph::Node node = nodes[0];
    // not factorized
    if (nodes.size() > 1) { node = nodes[1]; }
    location += node.size();
    // second loop particle
    int loop2location = location + 1;
    lml[1] = {testp2, loop2location, int(nparticles + 3), int(nparticles + 3), int(nparticles + 4)};
    ++location;
    auto vertexps = _get_vertex_particle_pointers_from_mom_indices(node.get_legs(), gcm);
    vertexps.push_back(&(lml[1].p));
    // factorized
    if (nodes.size() == 1) {
        int loop1location = location + 1;
        ++location;
        lml[0] = {testp1, loop1location, 0, int(nparticles + 1), int(nparticles + 2)};
        vertexps.push_back(&(lml[0].p));
    }
    auto leftp = Particle("n" + finalright.get_name(), finalright.get_anti_type(), finalright.external_state(), 0, 0);
    // in this case, the 'add_vertex' that follows is first vertex in toret
    if (istrand.size() == 0) leftp = Particle("n" + testp1.get_name(), testp1.get_anti_type(), testp1.external_state(), 0, 0);

    // only non-factorized graph
    for (size_t ss = 1; ss + 1 < Streechoice.path.size(); ss++) {
        // central strand to traverse (we use the same 'conn' as it doesn't change a two loops)
        auto cstrand = conn[Streechoice.path[ss].strand];
        // always needs reverse
        cstrand.reverse();
        auto rightp = _produce_a_particle_from_mass_index(cstrand.get_link(0).get_mass_index(), gcm, "L", SingleState("default"));
        toret.add_vertex(leftp, vertexps, rightp, {{gcm.coup, vertexps.size()}});
        _process_strand_for_cutTopology(toret, cstrand, location, finalright, gcm, false, false, 0, testp1);

        // add node
        location += nodes[0].size();
        vertexps = _get_vertex_particle_pointers_from_mom_indices(nodes[0].get_legs(), gcm);
        int loop1location = location + 1;
        ++location;
        lml[0] = {testp1, loop1location, 0, int(nparticles + 1), int(nparticles + 2)};
        vertexps.push_back(&(lml[0].p));

        if (cstrand.size() == 0)
            leftp = Particle("n" + rightp.get_name(), rightp.get_anti_type(), rightp.external_state(), 0, 0);
        else
            leftp = Particle("n" + finalright.get_name(), finalright.get_anti_type(), finalright.external_state(), 0, 0);
    }

    auto rightp = _produce_a_particle_from_mass_index(fstrand.get_link(0).get_mass_index(), gcm, "L", SingleState("default"));
    // in this case, the following 'add_vertex' is the last vertex in toret
    if (fstrand.size() == 0) rightp = Particle("n" + testp2.get_name(), testp2.get_anti_type(), testp2.external_state(), 0, 0);

    toret.add_vertex(leftp, vertexps, rightp, {{gcm.coup, vertexps.size()}});

    _process_strand_for_cutTopology(toret, fstrand, location, finalright, gcm, true, false, fstrand.size(), testp2);

    return toret;
}

// FIXME: this is temporary hack where an xGraph is mapped to a 'cut'
cutTopology getcutTopology(const xGraph& xg, xGraph& xgrouting, std::vector<LoopMomLocator>& lml, const GraphCutMap& gcm) {
    xgrouting = _misc::map_xgraph_mom_to_stree(xg, gcm);
    if (xg.get_n_loops() == 1)
        return getcutTopology1L(xg, lml, gcm);
    else if (xg.get_n_loops() == 2) { return getcutTopology2L(xg, lml, gcm); }
    else {
        std::cerr << "ERROR: getcutTopology(.) xGraph --> cutTopology conversion so far only for 1- a 2-loop diagrams" << std::endl;
        std::exit(1);
    }
}

xGraph map_xgraph_mom_to_stree(const xGraph& xg, const GraphCutMap& gcm) {
    if (xg.get_n_loops() == 1) {
        auto shift = get_1loop_optimal_shift(xg, gcm);
        // switches the routing to top link in positive direction
        auto xg4b = xg;
        xg4b.reset_momentum_routing(true, shift);
        xg4b.trace_momentum_routing();
        return xg4b;
    }
    if (xg.get_n_loops() != 2) {
        std::cerr << "ERROR: map_xgraph_mom_to_stree(.) not completed for " << xg.get_n_loops() << "-loop diagrams" << std::endl;
        std::exit(1);
    }

    // the function will choose an optimal S-tree choice according to the hierarchy defined in 'ptype_is_less_than'
    auto Streechoice = get_2loop_optimal_Stree(xg, gcm);

    std::map<std::pair<size_t, size_t>, lGraph::Connection<lGraph::Strand>> newconnections = xg.get_base_graph().get_connection_map();
    for (auto& [p, conn] : newconnections) {
        for (size_t ss = 0; ss < conn.get_edges().size(); ss++) {
            Strand& strand = conn[ss];
            strand.drop_routings();
            for (auto& l : strand.get_links()) l.drop_momentum_label();
        }
    }

    // 'front' strand has l1 (+1 in first link if 'down', -1 in last link if 'up')
    Strand& strandl1 = newconnections.at(Streechoice.path.front().connection)[Streechoice.path.front().strand];
    if(Streechoice.path.front().direction == FlowDirection::down) {
        strandl1.get_links().front().set_momentum_label(1);
    }
    else {
        strandl1.get_links().back().set_momentum_label(-1);
    }
    // 'back' strand has l2 (-2 in last link if 'down', +2 in first link if 'up')
    Strand& strandl2 = newconnections.at(Streechoice.path.back().connection)[Streechoice.path.back().strand];
    if (Streechoice.path.back().direction == FlowDirection::down) { strandl2.get_links().back().set_momentum_label(-2); }
    else { strandl2.get_links().front().set_momentum_label(2); }

    return xGraph(lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>(xg.get_base_graph().get_nodes(), newconnections));
}

} // namespace _misc

} // namespace AmpEng
} // namespace Caravel

