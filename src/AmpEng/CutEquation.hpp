#include <future>
#include "Core/InversionStrategies.h"
#include "Core/settings.h"
#include "FunctionSpace/IBPs/IBPImport.hpp"
#include "FunctionSpace/MasterIntegrands/Import.hpp"

namespace Caravel {
namespace AmpEng {

#define DEBUG_COEFFICIENTS

namespace detail {

template <typename T> bool is_zero(T x) {
    if constexpr (is_floating_point_v<T>) {
        using TR = remove_complex_t<T>;
        using std::fabs;
        static const TR eps = 1e-6; // it's just a random cutoff, we keep it uniform over precision as well
        if (fabs(x) > eps)
            return false;
        else
            return true;
    }
    else if constexpr (is_exact_v<T>) {
        return x == T{};
    }
    else if constexpr (is_iterable_v<T>) {
        for (const auto& it : x) {
            if (!::Caravel::AmpEng::detail::is_zero(it)) return false;
        }
        return true;
    }
    throw std::runtime_error("Not implemented");
}

} // namespace detail

template <typename T, size_t D> FactChannel<T, D>::FactChannel(const Registro& r) {
    using MomEvaluator = std::function<momD<T, D>(const OnShellPoint<T, D>&, const momD_conf<T, D>&)>;
    std::vector<MomEvaluator> lPi;
    for(auto& l: r.lP_from_lD) {
        auto mr = TransformOSMapMomRouting(l);
        lPi.emplace_back(lGraph::lGraphKin::MomentumRoutingEvaluator<T, D>(mr));
    }
    lPfromlD = [lPi{std::move(lPi)}](const OnShellPoint<T, D>& os, const momD_conf<T, D>& mc) {
                     std::vector<momD<T, D>> tr;
                     for(auto& lpi: lPi) tr.push_back(lpi(os, mc));
                     return tr;
                 };
    std::vector<PropagatorProduct<T, D>> each;
    for(auto& p: r.propagators) {
        auto getmom = lGraph::lGraphKin::MomentumRoutingEvaluator<T, D>(p.mom);
        each.emplace_back(
                [getmom{std::move(getmom)}, mindex=p.mass] (const OnShellPoint<T, D>& os, const GraphKin<T, D>& gK) {
                    auto mom = getmom(os, gK.get_mom_conf());
                    auto mom2 = mom*mom;
                    return mom2
                        - gK.get_masses()[mindex] * gK.get_masses()[mindex]
                    ;
                }
            );
    }
    propagatorprod = [each{std::move(each)}](const OnShellPoint<T, D>& os, const GraphKin<T, D>& gK) {
                T prod(1);
                for(auto& e: each) prod *= e(os, gK);
                return prod;
            };
}

template <typename T, size_t D> OnShellPoint<T, D> FactChannel<T, D>::lP(const OnShellPoint<T, D>& lD, const GraphKin<T, D>& gK) const {
    return lPfromlD(lD, gK.get_mom_conf());
}

template <typename T, size_t D> T FactChannel<T, D>::operator()(const OnShellPoint<T, D>& lP, const GraphKin<T, D>& gK) const {
    return propagatorprod(lP, gK);
}

template <typename T, size_t D> CutEquationComponent<T, D>::CutEquationComponent(const ArbolEntry& ae, size_t size): parent(ae.parent), Size(size) {
    for(auto& registro: ae.parenthood.get_relationships())
        individual.emplace_back( FactChannel<T, D>(registro) );
}

template <typename T, size_t D> CoeffType<T> CutEquationComponent<T, D>::operator()(const OnShellPoint<T, D>& lD, const GraphKin<T, D>& gK, const T& ep, const Diagram<T, D>& parentintegrand) const {
    CoeffType<T> value(Size);
    for(auto& channel: individual) {
        auto lP = channel.lP(lD, gK);
        value += 
            parentintegrand(lP, ep) *
            // FIXME: include exception handling in case of division by zero!
            T(1) / channel(lP, gK);
    }
    return value;
}

template <typename T, size_t D> CutEquationContributor<T, D>::CutEquationContributor(const ArbolEntry& ae): parent(ae.parent) {
    for(auto& registro: ae.parenthood.get_relationships())
        individual.emplace_back( FactChannel<T, D>(registro) );
}

template <typename T, size_t D> std::vector<T> CutEquationContributor<T, D>::operator()(const OnShellPoint<T, D>& lD, const GraphKin<T, D>& gK, const T& ep, const Diagram<T, D>& parentintegrand) const {
    std::vector<T> funcs(parentintegrand.basis_dim, T(0));
    for(auto& channel: individual) {
        auto lP = channel.lP(lD, gK);
        auto lfuncs = parentintegrand.eval_functions(lP, ep);
        auto propsfact = T(1) / channel(lP, gK);
        for(size_t ii = 0; ii < parentintegrand.basis_dim; ++ii) funcs[ii] += lfuncs[ii] * propsfact;
    }
    return funcs;
}

template <typename T, size_t D>
CutEquationComponents<T, D>::CutEquationComponents(std::vector<std::pair<size_t, size_t>>& resolver, const Arbol& a, size_t size) : Size(size) {
    for (auto& level : a.parents) {
        individual.push_back(std::vector<CutEquationComponent<T, D>>());
        auto& rhs = individual.back();
        needed.push_back(std::vector<CutEquationContributor<T, D>>());
        auto& lhs = needed.back();
        for (auto& arbolentry : level) {
            // do not include non-representative subleading pole diagrams
            if (Diagram<T, D>::omitted_diagrams.at(arbolentry.parent)) continue;

            auto found = std::find(resolver.begin(), resolver.end(), arbolentry.parent);
            if (found == resolver.end()) { rhs.emplace_back(arbolentry, Size); }
            else { lhs.emplace_back(arbolentry); }
        }
    }
}

template <typename T, size_t D> CoeffType<T> CutEquationComponents<T, D>::operator()(const OnShellPoint<T, D>& lD, const GraphKin<T, D>& gK, const T& ep, const std::vector<std::vector<Diagram<T, D>>>& hierarchy) const {
    CoeffType<T> value(Size);
    for (size_t level = 0; level < individual.size(); ++level)
        for (size_t nparent = 0; nparent < individual[level].size(); ++nparent) {
            auto& parentlocation = individual[level][nparent].parent;
            value += individual[level][nparent](lD, gK, ep, hierarchy[parentlocation.first][parentlocation.second]);
        }
    return value;
}

template <typename T, size_t D>
std::vector<T> CutEquationComponents<T, D>::asc_functions(const OnShellPoint<T, D>& lD, const GraphKin<T, D>& gK, const T& ep,
                                                      const std::vector<std::vector<Diagram<T, D>>>& hierarchy) const {
    std::vector<T> toret;
    for (size_t level = 0; level < needed.size(); ++level)
        for (size_t nparent = 0; nparent < needed[level].size(); ++nparent) {
            auto& parentlocation = needed[level][nparent].parent;
            auto funcs = needed[level][nparent](lD, gK, ep, hierarchy[parentlocation.first][parentlocation.second]);
            toret.insert(toret.end(), funcs.begin(), funcs.end());
        }
    return toret;
}

template <typename T, size_t D>
Diagram<T, D>::Diagram(const xGraph& xg, const Arbol& a, const GraphCutMap& gcm, bool recompute, const std::vector<size_t>& Ds_vals, const std::pair<size_t, size_t>& pp, std::vector<std::pair<size_t,size_t>>& resolver, bool rep, const SubleadPoleTerm& sing) : xgraph(xg), hier_locate(pp), arbol(a), graphkin(GraphKin<T, D>(xgraph)), always_recompute_external(recompute), Dss(Ds_vals), checker(LoopOnShellChecker<T, D>(xgraph)), representative_diag(rep), singularity(sing) {
    std::sort(Dss.begin(), Dss.end());
    ctopology = _misc::getcutTopology(xgraph, xgraphrouting, loop_particle_container, gcm);
    checkerBuilder = LoopOnShellChecker<T, D>(xgraphrouting);

    // handle subleading pole information
    if (!representative_diag)
        enable_onshell_integrand = false;
    else if (singularity != SubleadPoleTerm::leading)
        enable_onshell_integrand = false;

    // Number of OS variables
    n_OS_vars = xgraph.n_OS_variables_with_special_routing();

    DEBUG(
        std::cout<<"Diagram constructor: "<<std::endl;
        xg.show(); 
        std::cout << "cTopology: " << ctopology << std::endl;
        using Caravel::operator<<;
        std::cout << "cTopology particles: " << ctopology.get_particles_vertices() << std::endl; 
        std::cout << "loop_particle_container: " << loop_particle_container << std::endl;
        std::cout << "n_OS_vars: " << n_OS_vars << std::endl;
    );
    build_subtraction(resolver, a);
    check_factor();
}

template <typename T, size_t D>
Diagram<T, D>::Diagram(const xGraph& xg, const Arbol& a, cutF& cF, bool recompute, const std::vector<size_t>& Ds_vals, const std::pair<size_t, size_t>& pp, std::vector<std::pair<size_t,size_t>>& resolver, bool rep, const SubleadPoleTerm& sing) : xgraph(xg), xgraphrouting(cF.get_graph()), hier_locate(pp), arbol(a), graphkin(GraphKin<T, D>(xgraph)), always_recompute_external(recompute), temp_reference(&cF), Dss(Ds_vals), checker(LoopOnShellChecker<T, D>(xgraph)), checkerBuilder(LoopOnShellChecker<T, D>(xgraphrouting)), representative_diag(rep), singularity(sing) {
    std::sort(Dss.begin(), Dss.end());
    auto lcF = cF;
    ctopology = lcF.get_cutTopology_copy();
    auto nparts = cF.get_n_particles();

    // handle subleading pole information
    if (!representative_diag)
        enable_onshell_integrand = false;
    else if (singularity != SubleadPoleTerm::leading)
        enable_onshell_integrand = false;

    // FIXME: this is just temporary for 1-loop, anyhow!
    loop_particle_container.push_back(LoopMomLocator{Particle(ctopology.get_right()->get_name(), ctopology.get_right()->get_type(), SingleState("default") ), int(nparts+1), int(0), int(nparts+1), int(nparts+2)} );

    // Number of OS variables
    n_OS_vars = xgraph.n_OS_variables_with_special_routing();

    DEBUG(
        std::cout<<"Diagram constructor: "<<std::endl;
        xg.show(); 
        std::cout << "cTopology: " << ctopology << std::endl; 
        using Caravel::operator<<;
        std::cout << "cTopology particles: " << ctopology.get_particles_vertices() << std::endl; 
        std::cout << "loop_particle_container: " << loop_particle_container << std::endl;
        std::cout << "n_OS_vars: " << n_OS_vars << std::endl;
    );
    build_subtraction(resolver, a);
    check_factor();
}


template <typename T, size_t D> void Diagram<T, D>::add_location_index(int i) {
    if (i < 0) {
        std::cerr << "ERROR: Diagram::add_location_index(" << i << ") received a negative argument" << std::endl;
        std::exit(1);
    }
    locations.push_back(i);
}

template <typename T, size_t D> void Diagram<T, D>::add_to_forest(std::unique_ptr<Forest>& forest, size_t _max_Ds) {
    if(temp_reference != nullptr) {
        temp_reference->add_to_forest(*(forest.get()), 0, false, _max_Ds);
        locations = (temp_reference->get_forest_positions()).forest_ids;
        return;
    }
    std::vector<std::tuple<int, int, int, int>> loop_parts_out;
    for (size_t i = 0; i < loop_particle_container.size(); ++i)
        loop_parts_out.push_back(std::make_tuple(loop_particle_container[i].looploc, loop_particle_container[i].mlooploc,
                                                 loop_particle_container[i].looploc_momconf, loop_particle_container[i].mlooploc_momconf));
    locations = { forest->add_cut(loop_parts_out, ctopology) };
}

template <typename T, size_t D> CoeffType<T> Diagram<T, D>::compute_onshell_integrand(const OnShellPoint<T, D>& momenta, std::shared_ptr<Builder<T, D>>& builder) {
    if(!enable_onshell_integrand) {
        std::cerr << "ERROR: call to compute_onshell_integrand(.) which isn't enabled." << std::endl;
        xgraph.show(std::cerr);
        std::exit(1);
    }
    auto it = result_cache.find(momenta);
    if(it != result_cache.end())
        return it->second;

    builder->set_loop(momenta);
    if (always_recompute_external) builder->compute();
    builder->compute_cut(locations[0]);
    // FIXME: consider all Ds contributions in get_cut call
    CoeffType<T> toret(Dss.size());
    for(int k = 0; k < int(locations.size()); ++k) {
        builder->contract_cut(locations[k]);
        toret += CoeffType<T>(builder->get_cut(locations[k]));
    } 

    result_cache[momenta] = toret;

    return toret;
}

template <typename T, size_t D> void Diagram<T, D>::assemble(const BasisTruncation& truncation) {
    // produce 'functions'
    update_functions(truncation);

    get_momenta = surd_parameterization<T>(xgraph, graphkin);

    // produce 'OSmom4builder'
    produce_OS_mapper_for_builder();
}

template <typename T, size_t D>
void Diagram<T, D>::compute_coefficients(const momD_conf<T, D>& externalmomenta, const T& epsilon, std::shared_ptr<Builder<T, D>>& builder,
                         std::vector<std::vector<Diagram<T, D>>>& hierarchy) {
    size_t basis_dim_total(basis_dim);
    for (const auto& level : subtraction.needed)
        for (const auto& diag : level) {
            auto& p = diag.parent;
            basis_dim_total += hierarchy[p.first][p.second].basis_dim;
        }
    std::vector<CoeffType<T>> result;

    // Update GraphKin
    graphkin.change_point(externalmomenta);

    for (size_t t = 0; t <= 2; t++) {
        try {
            std::vector<T> A(basis_dim_total * basis_dim_total, T(0));
            std::vector<CoeffType<T>> lhs(basis_dim_total, CoeffType<T>(Dss.size()));

            for (size_t n = 0; n < basis_dim_total; n++) {
                // Generate random variables
                std::vector<std::vector<T>> vars = get_variables(n);

                // Get on-shell loop momenta
                auto loop_moms = get_momenta(vars);
                auto builder_loop_moms = OSmom4builder(loop_moms, externalmomenta);

#ifdef DEBUG_COEFFICIENTS
                auto c = checker(loop_moms, externalmomenta);
                auto cB = checkerBuilder(builder_loop_moms, externalmomenta);
                if(c != 0) {
                    std::cerr << "ERROR: on-shell momenta check in Diagram::compute_coefficients(.) failed for the direct graph: " << xgraph << std::endl;
                    std::exit(1);
                }
                if(cB != 0) {
                    std::cerr << "ERROR: on-shell momenta check in Diagram::compute_coefficients(.) failed for routing-mapped the graph: " << xgraphrouting << std::endl;
                    std::exit(1);
                }
#endif

                // Compute the cut
                auto cut = compute_onshell_integrand(builder_loop_moms, builder);

                // Compute subtraction
                auto sub = subtraction(loop_moms, graphkin, epsilon, hierarchy);

                // Compute function basis
                auto basis_elements = eval_functions(loop_moms, epsilon);

                // Compute parent's function basis
                auto asc_basis_elements = subtraction.asc_functions(loop_moms, graphkin, epsilon, hierarchy);

#if 0
                DEBUG(
                    using Caravel::operator<<;
                    std::cout << "For sample " << n << "/" << basis_dim_total << " with ep = " << epsilon << std::endl;
                    std::cout << "cut = " << cut << " and subtraction = " << sub << std::endl;
                    std::cout << "functions = " << basis_elements << std::endl;
                    std::cout << "ascendant functions = " << asc_basis_elements << std::endl;
                    std::cout << std::endl;
                );
#endif

                // Assemble linear equation system: lhs = A*c, where c is a vector of unknown coefficients
                lhs[n] = cut - sub;
                assert(basis_elements.size() + asc_basis_elements.size() == basis_dim_total);
                for (size_t k = 0; k < basis_elements.size(); k++) A[n + k * basis_dim_total] = basis_elements[k];
                for (size_t k = basis_elements.size(); k < basis_dim_total; k++) A[n + k * basis_dim_total] = asc_basis_elements[k - basis_elements.size()];
            }

            // Solve: lhs = A*c for c
            if (basis_dim_total > 0) result = plu_solver(std::move(A), std::move(lhs));
#if 0
            DEBUG(
                std::cout << "coefficients: " << result << std::endl;
            );
#endif
        }
        catch (const DivisionByZeroException&) {
            clear_cache();
            std::cout << "Caught DivisionByZeroException while fitting: attempt #" << t + 1 << std::endl;
            continue;
        }
        break;
    }

    DEBUG(
        size_t non_zero = 0;
        for (auto x : result)
            if (x != CoeffType<T>(Dss.size())) non_zero++;
        std::cout << "basis_dim_total/non-zero = " << basis_dim_total << " / " << non_zero << "\t" << ctopology << std::endl;
    );

    // N = N test
    if (settings::general::do_n_eq_n_test == settings::answer::yes) {
        // Generate random variables
        std::vector<std::vector<T>> vars = get_variables(basis_dim_total);

        // Get on-shell loop momenta
        OnShellPoint<T, D> loop_moms = get_momenta(vars);
        OnShellPoint<T, D> builder_loop_moms = OSmom4builder(loop_moms, externalmomenta);

#ifdef DEBUG_COEFFICIENTS
        auto c = checker(loop_moms, externalmomenta);
        auto cB = checkerBuilder(builder_loop_moms, externalmomenta);
        if (c != 0) {
            std::cerr << "ERROR: on-shell momenta check in Diagram::compute_coefficients(.) failed for the direct graph: " << xgraph << std::endl;
            std::exit(1);
        }
        if (cB != 0) {
            std::cerr << "ERROR: on-shell momenta check in Diagram::compute_coefficients(.) failed for routing-mapped the graph: " << xgraphrouting << std::endl;
            std::exit(1);
        }
#endif

        // Compute lhs
        auto cut = compute_onshell_integrand(builder_loop_moms, builder);
        auto sub = subtraction(loop_moms, graphkin, epsilon, hierarchy);
        auto lhs = cut - sub;

        // Compute contraction
        auto basis_elements = eval_functions(loop_moms, epsilon);
        auto asc_basis_elements = subtraction.asc_functions(loop_moms, graphkin, epsilon, hierarchy);
        // compute rhs
        CoeffType<T> rhs(Dss.size());
        for (size_t ii = 0; ii < basis_elements.size(); ii++) rhs += result[ii] * basis_elements[ii];
        for (size_t ii = 0; ii < asc_basis_elements.size(); ii++) rhs += result[basis_elements.size() + ii] * asc_basis_elements[ii];

        if (!detail::is_zero(lhs - rhs)) {
            _MESSAGE("N = N test failed! ", lhs, " != ", rhs);
	    xgraph.show(std::cerr);
            std::exit(1);
        }
    }

    set_coefficients(epsilon, result);
    for (const auto& level : subtraction.needed) {
        for (const auto& diag : level) {
            auto& p = diag.parent;
            hierarchy[p.first][p.second].set_coefficients(epsilon, result);
        }
    }
    assert(result.size() == 0);
}

template <typename T, size_t D> void Diagram<T, D>::set_coefficients(const T& epsilon, std::vector<CoeffType<T>>& result) {
    // sanity check
    auto cfs = coeffs.find(epsilon);
    if(cfs != coeffs.end()) {
        std::cerr << "ERROR: trying to Diagram::set_coefficients(.) when already set!" << std::endl;
        std::exit(1);
    }
    assert(result.size() >= functions.size());
    std::vector<CoeffType<T>> copy;
    copy.insert(copy.begin(), result.begin(), result.begin() + basis_dim);
    result.erase(result.begin(), result.begin() + basis_dim);
    coeffs[epsilon] = copy;

    if (needs_warmup) {
        needs_warmup = false;
        basis_truncation.clear();
        std::vector<CoeffType<T>> result_trimmed;
        // Find BasisTruncation and non zero coefficients
        for (size_t ii = 0; ii < copy.size(); ii++) {
            if (copy[ii] == CoeffType<T>(Dss.size()))
                basis_truncation.push_back(ii);
            else
                result_trimmed.push_back(copy[ii]);
        }
        update_functions(basis_truncation);
        coeffs[epsilon] = result_trimmed;
    }
}

template <typename T, size_t D> CoeffType<T> Diagram<T, D>::operator()(const OnShellPoint<T, D>& lD, const T& ep) const {
    // Get pre-computed coefficients
    auto cfs = coeffs.find(ep);
    if (cfs == coeffs.end()) {
        std::cout << "ERROR: Diagram - coefficients should be known by now!" << std::endl;
        std::exit(1);
    }
    const auto& c = cfs->second;

    // Compute FunctionBasis
    auto basis_elements = eval_functions(lD, ep);

    // Assemble value 
    CoeffType<T> value(Dss.size());
    for (size_t ii = 0; ii < basis_dim; ii++) value += c[ii] * basis_elements[ii];

    return value;
}

template <typename T, size_t D> std::vector<T> Diagram<T, D>::eval_functions(const OnShellPoint<T, D>& lD, const T& ep) const {
    // Compute FunctionBasis
    const std::vector<T> d_vals = {T(4) - T(2) * ep};
    std::vector<std::vector<T>> basis_elements = functions(lD, d_vals);

    std::vector<T> toret(basis_dim);
    for (size_t ii = 0; ii < basis_dim; ii++) toret[ii] = basis_elements[ii][0];

    return toret;
}

// FIXME: make more flexible choices by using 'settings'
template <typename T, size_t D> void Diagram<T, D>::update_functions(const BasisTruncation& truncation) {
    static std::mutex access;
    std::lock_guard<std::mutex> lock(access);
    static bool loaded_1L = false;
    static bool loaded_2L = false;

    if (xgraph.get_n_loops() == 1) {
        if (!loaded_1L) {
            FunctionSpace::SurfaceTerms::Other::load();
            loaded_1L = true;
        }
        functions = FunctionSpace::build_topology_master_surface_basis(xgraph, graphkin, truncation);
    }
    else if (xgraph.get_n_loops() == 2) {
        if (settings::general::numerator_decomposition.value == settings::general::IntegralBasis::master_surface) {
            if (!loaded_2L) {
                FunctionSpace::SurfaceTerms::MassiveGravity::load();
                FunctionSpace::SurfaceTerms::Other::load();
                FunctionSpace::SurfaceTerms::Factorizable::load();
                FunctionSpace::MasterIntegrands::MassiveGravity::load();
                loaded_2L = true;
            }
            functions = FunctionSpace::build_topology_master_surface_basis(xgraph, graphkin, truncation);
        }
        else if (settings::general::numerator_decomposition.value == settings::general::IntegralBasis::tensors)
            functions = FunctionSpace::build_topology_tensor_basis(xgraph, graphkin, truncation);
        else if (settings::general::numerator_decomposition.value == settings::general::IntegralBasis::scattering_plane_tensors) {
            if (!loaded_2L) {
                FunctionSpace::SurfaceTerms::Factorizable::load();
                loaded_2L = true;
            }
            functions = FunctionSpace::build_topology_scattering_plane_tensor_basis(xgraph, graphkin, truncation);
        }
        else {
            std::cerr << "ERROR: got a 'numerator_decomposition' " << wise_enum::to_string(settings::general::numerator_decomposition.value) << " is not implemented!" << std::endl;
            std::exit(1);
        }
    }
    else {
        std::cerr << "ERROR: trying to construct integrand parametrization for a " << xgraph.get_n_loops() << "-loop Diagram, needs implementation!"
                  << std::endl;
        std::exit(1);
    }

    // Reset stored coefficients
    coeffs.clear();

    // Keep track of global information
    basis_dim = functions.size();
    basis_truncation = truncation;
    if( basis_truncation.size() != 0 ) needs_warmup = false; // FIXME: More sensible condition?

}

template <typename T, size_t D> void Diagram<T, D>::construct_integral_graphs() {
    graphs.clear();

    if (xgraph.get_n_loops() == 1) {
        // FIXME: unify with 2-loop inclusion of external map
        // FIXME: furthermore, FunctionBasis should contain the 'to_representative_kinematics_map' info,
        //        such that coefficients providers would not need to access the holder maps
        const auto& pmaster_data = FunctionSpace::master_term_data_holder.find(xgraph.get_topology());
        // FIXME! add topology name for standard 1-loop parametrization
        //std::string topology_name = std::get<std::string>(pmaster_data->second);
        std::string topology_name = "fffff";
        _PRINT(topology_name);
        for (size_t ii = 0; ii < functions.get_n_masters(); ii++) { 
            if (functions.get_integral_insertion(ii) != IntegralInsertion::undefined) {
                graphs.emplace_back(functions.get_integral_insertion(ii), xgraph);
            }
            else {
                graphs.emplace_back(xgraph, functions.get_integral_insertion_string(ii), topology_name);
            }
            if (pmaster_data != FunctionSpace::master_term_data_holder.end()) {
                const auto& repr_graph = std::get<lGraph::xGraph>(pmaster_data->second);
                graphs.back().to_representative_kinematics_map = lGraph::lGraphKin::abstract_external_momentum_mapper(xgraph, repr_graph);
            }
        }
    }
    else if (xgraph.get_n_loops() == 2) {
        if (settings::general::numerator_decomposition.value == settings::general::IntegralBasis::master_surface) {
            // Master information
            const auto& master_data = FunctionSpace::master_term_data_holder.at(xgraph.get_topology());
            std::vector<std::string> names = std::get<decltype(names)>(master_data);
            std::string topology_name = std::get<std::string>(master_data);

            // add info about how this integral is mapped to a representative master,
            // only if the representative graph was provided
            const auto& repr_graph = std::get<lGraph::xGraph>(master_data);
            decltype(graphs.back().to_representative_kinematics_map) storemapinfo;
            bool addinfo{false};
            if (repr_graph.get_base_graph() != decltype(repr_graph.get_base_graph()){}) {
                addinfo = true;
                storemapinfo = lGraph::lGraphKin::abstract_external_momentum_mapper(xgraph, repr_graph);
            }

            for (size_t ii = 0; ii < functions.get_n_masters(); ii++) {
                graphs.emplace_back(xgraph, names.at(ii), topology_name);
                if (addinfo) graphs.back().to_representative_kinematics_map = storemapinfo;
            }
        }
        else if (settings::general::numerator_decomposition.value == settings::general::IntegralBasis::scattering_plane_tensors) {
            for (size_t ii = 0; ii < functions.get_n_masters(); ii++) {
                // no topology_name included
                if (xgraph.get_base_graph().is_factorizable()) std::cout << "WARNING: careful with naming of insertions for factorizable diagrams!!" << std::endl;
                graphs.emplace_back(xgraph.get_special_routing(), functions.get_integral_insertion_string(ii), "");
            }
        }
    }
}

template <typename T, size_t D> std::vector<std::vector<T>> Diagram<T, D>::get_variables(size_t i) {
    auto it = variable_values.find(i);
    if (it != variable_values.end()) return it->second;

    std::vector<std::vector<T>> vars(n_OS_vars.size(), std::vector<T>());

    for (size_t ii = 0; ii < n_OS_vars.size(); ii++)
        for (size_t jj = 0; jj < n_OS_vars[ii]; jj++) vars[ii].push_back(random_scaled_number<T>(T(1)));

    variable_values[i] = vars;
    return vars;
}

template <typename T, size_t D> void Diagram<T, D>::clear_cache() {
    variable_values.clear();
    result_cache.clear();
    coeffs.clear();
}

template <typename T, size_t D> void Diagram<T, D>::check_factor() {
    // basic check of bubble symmetry factor
    using namespace lGraph;
    using xGtype = Graph<Node, Connection<Strand>>;
    static const xGraph bubble = xGtype("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]]]]");
    if(xgraph.get_topology() == bubble.get_topology()){
        // check if it's colorless
        if(!ctopology.get_left()->is_colored() && !ctopology.get_vertex(0).get_left().is_colored()) {
            factor = T(1) / T(2);
            DEBUG(
                std::cout << "Added a 1/2 factor for bubble diagram " << ctopology << std::endl;
            );
        }
    }
}

template <typename T, size_t D> void Diagram<T, D>::produce_OS_mapper_for_builder() {
    OSmom4builder = OnShellStrategies::OnShellPointMap<T, D>(xgraph, xgraphrouting);
}

template <typename T, size_t D> void Diagram<T, D>::build_subtraction(std::vector<std::pair<size_t,size_t>>& resolver, const Arbol& a) {
    if(enable_onshell_integrand) {
        auto found = std::find(resolver.begin(), resolver.end(), std::make_pair(get_hier_location().first, get_hier_location().second));
        // sanity check 
        assert(found != resolver.end());
        resolver.erase(found);
        subtraction = CutEquationComponents<T, D>(resolver, a, Dss.size());
    }
    else {
        subtraction = CutEquationComponents<T, D>();
    }
}

template <typename T, size_t D> void Diagram<T, D>::build_subtraction(std::vector<std::pair<size_t,size_t>>& resolver) {
    build_subtraction(resolver, arbol);
}

template <typename T, size_t D> std::vector<CoeffType<T>> Diagram<T, D>::get_master_coeffs(const T& ep) const {
    // Get pre-computed coefficients
    auto cfs = coeffs.find(ep);
    if (cfs == coeffs.end()) {
        std::cout << "ERROR: Diagram - coefficients should be known by now!" << std::endl;
        std::exit(1);
    }
    auto lcoeffs = cfs->second;

    // Fill with explicit zeroes that have been dropped by the warmup
    if (basis_truncation.size() != 0)
        for (auto k : basis_truncation) lcoeffs.insert(lcoeffs.begin() + k, CoeffType<T>(Dss.size()));

    // keep only masters
    lcoeffs.erase(lcoeffs.begin() + functions.get_n_masters(), lcoeffs.end());

    // include overall factors
    for (auto& c : lcoeffs) c *= factor;

    return lcoeffs;
}

template <typename T, size_t D> void Diagram<T, D>::set_cut_equation(bool b) {
    enable_onshell_integrand = b;
}

template <typename T, size_t D> bool Diagram<T, D>::has_cut_equation() const {
    return enable_onshell_integrand;
}

template <typename T, size_t D> std::pair<size_t, size_t> Diagram<T, D>::get_hier_location() const {
    return hier_locate;
}

template <typename T, size_t D>
thread_local std::unordered_map<std::pair<size_t, size_t>, bool> Diagram<T, D>::omitted_diagrams = std::unordered_map<std::pair<size_t, size_t>, bool>{};

template <typename T, size_t D>
HierarchyEngine<T, D>::HierarchyEngine(const Genealogy& g, const std::shared_ptr<Model>& model, bool recompute, const std::vector<size_t>& Ds_vals, const couplings_t& coupin)
    : forest(std::make_unique<Forest>(*model)), generations(g), always_recompute_tree(recompute), Dss(Ds_vals), treecoups(coupin) {
    std::sort(Dss.begin(), Dss.end());

    DEBUG(
        std::cout<<"particle map in HierarchyEngine: (pointers first) "<<generations.get_graph_cut_map().external<<std::endl;
        for(auto [index, p]: generations.get_graph_cut_map().external) std::cout<<*p<<" ";
        std::cout<<std::endl;
    );
    // fill 'nparticles'
    auto& samplediag = (generations.get_propagator_hierarchy().begin()->second)[0];
    nparticles = samplediag.get_n_legs();
    // fill 'hierarchy'
    size_t level(0);
    Diagram<T, D>::omitted_diagrams.clear();
    // traverse from maximal to minimal (backwards in generations.get_propagator_hierarchy())
    for(auto iter = generations.get_propagator_hierarchy().rbegin(); iter != generations.get_propagator_hierarchy().rend(); ++iter) {
        auto& diagrams = iter->second;
        hierarchy.push_back(std::vector<Diagram<T, D>>());
        auto& lcontainer = hierarchy.back();
        size_t diag_index(0);
        for(auto& xg: diagrams) {
            DEBUG( std::cout<<"level: "<<level<<" diag_index: "<<diag_index<<std::endl; );
            auto& arbol = generations.get_generations().at(level).at(diag_index);
            resolver.push_back(std::make_pair(level, diag_index));
            auto member = Member(xg);
            auto representative = generations.get_info_representative(member);
            auto subleading = generations.get_info_subleading(member);
            Diagram<T, D>::omitted_diagrams[std::make_pair(level, diag_index)] = (!representative);
            lcontainer.push_back(Diagram<T, D>(xg, arbol, generations.get_graph_cut_map(), always_recompute_tree, Dss, std::make_pair(level, diag_index), resolver, representative, subleading));
            ++diag_index;
        }
        ++level;
    }
    Diagram<T, D>::omitted_diagrams.clear();

    assemble();
}

template <typename T, size_t D>
HierarchyEngine<T, D>::HierarchyEngine(const CutHierarchy& h, const std::shared_ptr<Model>& model, bool recompute, const std::vector<size_t>& Ds_vals, const couplings_t& coupin)
    : forest(std::make_unique<Forest>(*model)), generations(h), always_recompute_tree(recompute), Dss(Ds_vals), treecoups(coupin) {
    std::sort(Dss.begin(), Dss.end());
    // fill 'nparticles'
    auto& samplediag = (generations.get_propagator_hierarchy().begin()->second)[0];
    nparticles = samplediag.get_n_legs();
    // fill 'hierarchy'
    size_t level(0);
    // traverse from maximal to minimal
    auto cutFhierarchy = h.get_hierarchy();

    Diagram<T, D>::omitted_diagrams.clear();
    for(auto& clevel: cutFhierarchy) {
        hierarchy.push_back(std::vector<Diagram<T, D>>());
        auto& lcontainer = hierarchy.back();
        size_t diag_index(0);
        for(auto& cF: clevel) {
            DEBUG_MESSAGE("level: ", level, " diag_index: ", diag_index);
            // find location in generations
            auto& xg = cF.get_graph();
            auto member = Member(cF);
            auto pair = generations.get_locations().at(member);
            auto& arbol = generations.get_generations().at(pair.first).at(pair.second);
            resolver.push_back(std::make_pair(level, diag_index));
            auto representative = generations.get_info_representative(member);
            auto subleading = generations.get_info_subleading(member);
            Diagram<T, D>::omitted_diagrams[std::make_pair(level, diag_index)] = (!representative);
            lcontainer.push_back(Diagram<T, D>(xg, arbol, cF, always_recompute_tree, Dss, std::make_pair(level, diag_index), resolver, representative, subleading));
            ++diag_index;
        }
        ++level;
    }
    Diagram<T, D>::omitted_diagrams.clear();

    assemble();
}

template <typename T, size_t D> void HierarchyEngine<T, D>::assemble() {
    truncate_basis(std::vector<std::vector<BasisTruncation>>());
    forest->set_minimal_states_in_currents(false);

    // grow forest
    for (size_t level = 0; level < hierarchy.size(); ++level)
        for (size_t ndiag = 0; ndiag < hierarchy[level].size(); ++ndiag) hierarchy[level][ndiag].add_to_forest(forest, Dss.back());
    // get the tree
    if(treecoups.size() > 0) {
        std::vector<Particle*> parts;
        auto& pparticles = hierarchy[0][0].ctopology.get_particles_vertices();
        for(auto& p: pparticles) {
            static const SingleState internal("default");
            if (!(p->external_state() == internal))
                parts.push_back(p);
        }
        Process treep(parts);
        tree = forest->add_process(treep, treecoups);
    }
        
    // close forest
    // FIXME: deal properly with Ds values!
    forest->define_internal_states_and_close_forest(Dss);

    // Create farm of builders
    std::vector<std::future<std::shared_ptr<Builder<T, D>>>> futures;
    auto make_builder = [f = forest.get(), N = nparticles]() { return std::make_shared<Builder<T, D>>(f, N); };
    //for (size_t ii = 0; ii < std::thread::hardware_concurrency(); ii++) futures.push_back(std::async(std::launch::async, make_builder));
    for (size_t ii = 0; ii < 1; ii++) futures.push_back(std::async(std::launch::async, make_builder));
    for (auto& it : futures) builders.push_back(it.get());

    EpsilonBasis<T>::_make_sure_squares_are_initialized();
}

template <typename T, size_t D> void HierarchyEngine<T, D>::truncate_basis(const std::vector<std::vector<BasisTruncation>>& arraytruncation) {
    // sanity checks in input truncation
    bool usetruncation(false);
    bool error(false);
    if (arraytruncation.size() != 0 && hierarchy.size() != arraytruncation.size()) error = true;
    if (!error && arraytruncation.size() > 0) {
        usetruncation = true;
        for (size_t level = 0; level < arraytruncation.size(); level++) {
            size_t level_representative(0);
            for (auto& diagram : hierarchy[level]) {
                if (diagram.representative_diag) ++level_representative;
            }
            if (level_representative != arraytruncation[level].size()) {
                error = true;
                break;
            }
        }
    }
    if (error) {
        std::cerr << "ERROR: HierarchyEngine::truncate_basis(.) called with truncation array of wrong size!" << std::endl;
        std::exit(1);
    }

    for (size_t level = 0; level < hierarchy.size(); ++level) {
        size_t arraycounter(0);
        for (size_t ndiag = 0; ndiag < hierarchy[level].size(); ndiag++) {
            auto& diagram = hierarchy[level][ndiag];
            if (diagram.representative_diag) {
                if (usetruncation) { diagram.assemble(arraytruncation[level][arraycounter++]); }
                else {
                    hierarchy[level][ndiag].assemble();
                    hierarchy[level][ndiag].construct_integral_graphs();
                }
            }
        }
    }
}

template <typename T, size_t D> void HierarchyEngine<T, D>::eval(const momD_conf<T, 4>& externalmomenta, const T& epsilon) {

    builders[0]->set_p(externalmomenta);
    builders[0]->compute();
    builders[0]->contract();
    for (auto& level : hierarchy) {
        for (auto& diagram : level) {
            DEBUG_MESSAGE("Computing integrand in level: ", diagram.get_hier_location().first, " # ", diagram.get_hier_location().second);
            if(!diagram.enable_onshell_integrand) {
                DEBUG_MESSAGE("Continuing");
                // make sure that corresponding graphkin is evaluated
                auto lpair = diagram.get_hier_location();
                auto& dd = hierarchy[lpair.first][lpair.second];
                dd.graphkin.change_point(externalmomenta);
                continue;
            }
            diagram.compute_coefficients(externalmomenta, epsilon, builders[0], hierarchy);
        }
    }
}

template <typename T, size_t D> void HierarchyEngine<T, D>::eval_diagram(const momD_conf<T, 4>& externalmomenta, const T& epsilon, const std::pair<size_t, size_t>& diag) {
    builders[0]->set_p(externalmomenta);
    builders[0]->compute();
    builders[0]->contract();
    // change point in all unresolved parents
    for(size_t level = hierarchy[diag.first][diag.second].subtraction.needed.size(); level > 0; --level) {
        auto& allnparents = hierarchy[diag.first][diag.second].subtraction.needed[level-1];
        for(size_t parent = 0; parent < allnparents.size(); ++parent ) {
            auto& location = allnparents[parent].parent;
            auto& diagram = hierarchy[location.first][location.second];
            DEBUG_MESSAGE("Skips integrand in level: ", location.first, " # ", location.second);
            diagram.graphkin.change_point(externalmomenta);
        }
    }
    // evaluate all parents
    for(size_t level = hierarchy[diag.first][diag.second].subtraction.individual.size(); level > 0; --level) {
        auto& allnparents = hierarchy[diag.first][diag.second].subtraction.individual[level-1];
        for(size_t parent = 0; parent < allnparents.size(); ++parent ) {
            auto& location = allnparents[parent].parent;
            auto& diagram = hierarchy[location.first][location.second];
            DEBUG_MESSAGE("Computing integrand in level: ", location.first, " # ", location.second);
            if(!diagram.enable_onshell_integrand) {
                DEBUG_MESSAGE("Continuing");
                continue;
            }
            diagram.compute_coefficients(externalmomenta, epsilon, builders[0], hierarchy);
        }
    }
    // evaluate diagram
    DEBUG_MESSAGE("Computing integrand in level: ", diag.first, " # ", diag.second);
    auto& diagram = hierarchy[diag.first][diag.second];
    diagram.compute_coefficients(externalmomenta, epsilon, builders[0], hierarchy);
}

template <typename T, size_t D> std::vector<std::vector<std::vector<CoeffType<T>>>> HierarchyEngine<T, D>::get_master_coeffs(const T& epsilon) const {
    std::vector<std::vector<std::vector<CoeffType<T>>>> result;

    for (auto& level : hierarchy) {
        std::vector<std::vector<CoeffType<T>>> res_lvl;
        for (auto& diagram : level) {
            if (diagram.representative_diag) {
                res_lvl.push_back(diagram.get_master_coeffs(epsilon));
            }
        }
        result.push_back(res_lvl);
    }

    return result;
}

template <typename T, size_t D>
std::vector<CoeffType<T>> HierarchyEngine<T, D>::get_diagram_master_coeffs(const T& epsilon, const std::pair<size_t, size_t>& diag) const {
    return hierarchy.at(diag.first).at(diag.second).get_master_coeffs(epsilon);
}

template <typename T, size_t D> std::vector<lGraph::IntegralGraph> HierarchyEngine<T, D>::get_integral_graphs() const {
    std::vector<lGraph::IntegralGraph> result;

    for (auto& level : hierarchy) {
        for (auto& diagram : level) {
            if (diagram.representative_diag) {
                std::vector<lGraph::IntegralGraph> dia_gr = diagram.get_integral_graphs();
                result.insert(std::end(result), std::begin(dia_gr), std::end(dia_gr));
            }
        }
    }

    return result;
}

template <typename T, size_t D> std::vector<lGraph::IntegralGraph> HierarchyEngine<T, D>::get_diagram_integral_graphs(const std::pair<size_t, size_t>& diag) const {
    return hierarchy.at(diag.first).at(diag.second).get_integral_graphs();
}

template <typename T, size_t D> FunctionSpace::HierarchyBasisTruncation HierarchyEngine<T, D>::get_basis_truncation() const {
    HierarchyBasisTruncation trunc;

    for (auto& level : hierarchy) {
        std::vector<BasisTruncation> trunc_lvl;
        for (auto& diagram : level) {
            if (diagram.representative_diag) {
                trunc_lvl.push_back(diagram.get_basis_truncation());
            }
        }
        trunc.push_back(trunc_lvl);
    }

    return trunc;
}

template <typename T, size_t D> void HierarchyEngine<T, D>::clear_cut_cache() {
    for (auto& level : hierarchy)
        for (auto& diagram : level)
            diagram.clear_cache();
}

template <typename T, size_t D> std::function<T(const momD_conf<T, 4>&)> HierarchyEngine<T, D>::get_tree_evaluator() const {
    return [b = builders.at(0), treeind = tree](const momD_conf<T, 4>& mc) { return b->get_tree(treeind); };
}

template <typename T, size_t D> T HierarchyEngine<T, D>::get_tree(const momD_conf<T, 4>& mc) {
    builders[0]->compute();
    builders[0]->contract();
    return builders[0]->get_tree(tree);
}

template <typename T, size_t D> size_t HierarchyEngine<T, D>::n_masters_up_to(const std::pair<size_t, size_t>& diag) const {
    size_t toret(0);
    for(size_t level = 0; level <= diag.first; level++) {
        size_t lastd = hierarchy.at(level).size();
        if(level == diag.first) lastd = diag.second;
        for(size_t d = 0; d < lastd; d++) toret += hierarchy.at(level).at(d).get_n_masters();
    }
    return toret;
}

template <typename T, size_t D> void HierarchyEngine<T, D>::set_cut_equation(bool b, const std::pair<size_t, size_t>& diag) {
    hierarchy.at(diag.first).at(diag.second).set_cut_equation(b);
}

template <typename T, size_t D> void HierarchyEngine<T, D>::build_subtractions() {
    resolver.clear();
    Diagram<T, D>::omitted_diagrams.clear();
    for(size_t level = 0; level < hierarchy.size(); level++)
        for(size_t diag = 0; diag < hierarchy[level].size(); diag++) {
            resolver.push_back(std::make_pair(level, diag));
            Diagram<T, D>::omitted_diagrams[std::make_pair(level, diag)] = (!hierarchy[level][diag].representative_diag);
        }
    for (auto& level : hierarchy)
        for (auto& d : level) d.build_subtraction(resolver);
    Diagram<T, D>::omitted_diagrams.clear();
}

} // namespace AmpEng
} // namespace Caravel
