/**
 * @file Parenthood.h
 *
 * @date 1.2.2021
 *
 * @brief General tools for producing genealogy relations in AmpEng Hierarchy
 *
*/
#pragma once

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include "wise_enum.h"
#include "Core/Debug.h"
#include "Core/settings.h"
#include "Graph/Graph.h"
#include "Graph/Utilities.h"
#include "AmpEng/Diagrammatica.h"
#include "OnShellStrategies/osm.h"

#include "AmpEng/cutF.h"
#include "AmpEng/Utilities.h"

namespace Caravel {
namespace AmpEng {

class Registro {
  public:
    std::vector<lGraph::Propagator> propagators;    /**< Propagators in ancestor - descendant, written as function of loop mom from parent, canonicalized */
    std::vector<OnShellStrategies::info_OS_map> lP_from_lD;    /**< Map for loop mom of ancestor from loop mom from descendant */
    std::vector<OnShellStrategies::info_OS_map> lD_from_lP;    /**< Map for loop mom of descendant from loop mom from ancestor */
    Registro() = default;
    Registro(const std::vector<lGraph::Propagator>& props, const std::vector<OnShellStrategies::info_OS_map>& lPfromlD, const std::vector<OnShellStrategies::info_OS_map>& lDfromlP);

  private:
    void canonicalize(); /**< Takes care of writing 'propagators' in a unique way */
};

std::ostream& operator<<(std::ostream&, const Registro&);
bool operator==(const Registro&, const Registro&);

/**
 * A register of the relationship between two xGraphs 
 */
class Parenthood {
    lGraph::xGraph ancestor;
    lGraph::xGraph descendant;
    std::vector<Registro> relationships;    /**< Store all factorization channels that connect ancestor and descendant */
  public:
    Parenthood() = default;
    /**
     * 1-parent relation contructor (will throw if not 1-parent related)
     *
     * @param xGraph representing the parent
     * @param xGraph representing the daughter
     * @param vector (defaulted to empty) containing relationships
     */
    Parenthood(const lGraph::xGraph& p, const lGraph::xGraph& d, const std::vector<Registro>& = {});
    const auto& get_ancestor() const { return ancestor; }
    const auto& get_descendant() const { return descendant; }
    void add_relationship(const Registro& r) { relationships.push_back(r); }
    const auto& get_relationships() const { return relationships; }
    auto& get_relationships() { return relationships; }
  private:
};

struct ArbolEntry {
    std::pair<size_t, size_t> parent; /**< The diagram member of the hierarchy, written as a pair (meant to be in sync with Genealogy.locations)  */
    Parenthood parenthood;
};

struct Arbol {
    Topology descendant;
    std::vector<std::vector<ArbolEntry>> parents; /**< Array [n-th level parent, i-th parent in the level] of parents */
};

WISE_ENUM_CLASS(SubleadPoleTerm,
    (leading, 0),   		/**< The leading term (with intrinsic cut associated) */
    (subleading, 1),		/**< One power of inverse propagator down the leading term */
    (subsubleading, 2),		/**< Two powers of inverse propagator down the leading term */
    (subsubsubleading, 3)	/**< Three powers of inverse propagator down the leading term */
)

struct SingleSubleadingPoleTracker {
    lGraph::Propagator rho;
    SubleadPoleTerm depth;
};

struct SubleadingPoleTracker {
    bool representative{true};  /**< For a given combination of 'singularities' (which are not all 'SubleadPoleTerm::leading') this is the representative kept in the subtraction */
    std::vector<SingleSubleadingPoleTracker> singularities;
};

/**
 * The class to represent a given entry (the "member") in a Genealogy
 *
 * Instances of this class abstract the key used for Genealogy maps. Basic object is an 'xGraph', 
 * but we add extra structure which can account for more physical information associated to
 * the given entry of the Genealogy (e.g. color decomposition, coupling structure, different
 * particle content, etc)
 */
class Member {
    friend std::ostream& operator<<(std::ostream&,const Member&);
    lGraph::xGraph xgraph;
    size_t key{0};  /**< although we keep a lot of identifying info for *this, we store a hash as main identifier */
    bool hascut{false};
    std::string name{""};
    cutF cut;
    SubleadingPoleTracker subleading_poles;
  public:
    Member() = default;
    explicit Member(const lGraph::xGraph&);
    explicit Member(const cutF&);
    const auto& get_xgraph() const { return xgraph; }
    const auto& get_graph() const { return xgraph.get_base_graph(); }
    const auto& get_key() const { return key; }
    bool has_cut() const { return hascut; }
    const auto& get_cut() const { return cut; }
    mutable lGraph::xGraph xg_routing;
    const SubleadingPoleTracker& get_subleading_poles() const { return subleading_poles; }
    SubleadingPoleTracker& get_subleading_poles() { return subleading_poles; }
};

bool operator==(const Member& m1, const Member& m2);
std::ostream& operator<<(std::ostream&, const Member&);

} // namespace AmpEng
} // namespace Caravel

// Extend standard hash function for Member instances
namespace std {
template <> struct hash<Caravel::AmpEng::Member> { size_t operator()(const Caravel::AmpEng::Member&) const; };
} // namespace std

namespace Caravel {
namespace AmpEng {

/**
 * Genealogical information for a Hierarchy
 */
class Genealogy : public Hierarchy {
    std::unordered_map<Member, std::map<size_t, std::unordered_map<Member, Parenthood>>>
        book; /**< parent --> { level --> { daughter, Parenthood } }, where 'level' in an integer n where parent-daughter is an n-parent relationship */
    std::unordered_map<Member, std::map<size_t, std::unordered_map<Member, Parenthood>>>
        portal; /**< Redundant with 'book', but helpful for traversing: daughter --> { level --> { parent, Parenthood } }, where 'level' is an integer n where
                   parent-daughter is an n-parent relationship */
    std::map<size_t, std::map<size_t, Arbol>> generations;               /**< Redundant container { level ---> {diagram_index ---> {Member, Parenthood} } } */
    std::unordered_map<Member, std::pair<size_t, size_t>> locations;     /**< Redundant container { diagram ---> {level, diagram_index} } */
    std::unordered_map<std::pair<size_t, size_t>, Member> inv_locations; /**< Redundant container { {level, diagram_index} ---> diagram } */
    std::unordered_map<Topology, std::vector<Member>> multiplets;        /**< Information about members that share the same topologies */
    //bool unique_insertion;                                               /**< Temporary handle to filter non-color-ordered fact channels */
    std::string model;                                                   /**< Store model for 1-parent factorization-channel check */
    GraphCutMap particlemap;                                             /**< Store info of map between Graph instances and cutTopology instances */
  public:
    Genealogy() = default;
    /**
     * Will construct parenthood information out of a given Hierarchy
     *
     * Defaulted boolean argument employed to set 'unique_insertion' in *this (and corresponding in base class)
     */
    Genealogy(const Hierarchy&, const GraphCutMap&, bool = true, const std::string& = "SM_color_ordered");
    /**
     * Map a cutF-based hierarchy to a Genealogy
     */
    Genealogy(const AmpEng::CutHierarchy&, const std::string& = "SM_color_ordered");
    /**
     * Registers a parent-daughter relationship in *this
     *
     * @param Member representing parent
     * @param Member representing daughter
     * @param registers relationship
     */
    void add_relationship(const Member& p, const Member& d, const Registro& r);
    /**
     * Registers a daughter-parent relationship in *this
     *
     * @param xGraph representing daughter
     * @param xGraph representing parent
     * @param vector of registers for relationships
     */
    void add_inverse_relationship(const Member& d, const Member& p, const std::vector<Registro>& vr);
    const auto& get_book() const { return book; }
    const auto& get_portal() const { return portal; }
    const auto& get_generations() const { return generations; }
    const auto& get_locations() const { return locations; }
    const auto& get_inv_locations() const { return inv_locations; }
    const auto& get_multiplets() const { return multiplets; }
    auto& get_registro(const Member& parent, const Member& daughter) { 
        size_t level = parent.get_xgraph().get_n_edges() - daughter.get_xgraph().get_n_edges(); 
        return book.at(parent).at(level).at(daughter); 
    }
    /**
     * Allows to explore information contained in *this
     */
    void explore() const;

    // from Hierarchy inheritance
    void filter(const std::function<bool(const lGraph::xGraph&)>& ff, bool print_filtered = false) { Hierarchy::filter(ff, print_filtered); }
    size_t size() const { return Hierarchy::size(); }
    size_t topology_size() const { return Hierarchy::topology_size(); }
    int get_n_loops() const { return Hierarchy::get_n_loops(); }
    const std::vector<std::vector<lGraph::xGraph>>& get_xgraphs(size_t i) const { return Hierarchy::get_xgraphs(i); }
    const std::vector<std::vector<Topology>>& get_topologies(size_t i) const { return Hierarchy::get_topologies(i); }
    const std::map<size_t, std::vector<lGraph::xGraph>>& get_propagator_hierarchy() const { return Hierarchy::get_propagator_hierarchy(); }
    const std::map<size_t, std::vector<Topology>>& get_topology_propagator_hierarchy() const { return Hierarchy::get_topology_propagator_hierarchy(); }
    std::vector<lGraph::xGraph> get_all_xgraphs() const { return Hierarchy::get_all_xgraphs(); }
    const auto& get_graph_cut_map() const { return particlemap; }

    /**
     * FIXME: This method next to be extended to more general cases. Now working at two loops for leading singularities which are maximal diagrams
     *        It updates corresponding book entry, and all other related entries in containers
     *
     * @param: The Member to be updated
     * @param: SubleadPoleTerm
     * @param: bool that defines 'representative' in SingleSubleadingPoleTracker
     * @param: Propagator
     */
    void define_subleading_pole_in_book(const Member&, const SubleadPoleTerm&, bool, const lGraph::Propagator&);

    /**
     * Updates all Members in containers from corresponding keys in book
     */
    void update_containers_from_book();

    /**
     * Returns whether the Member argument has been marked as representative of a subleading-pole family
     */
    bool get_info_representative(const Member&) const;
    /**
     * FIXME: crude info on subleading poles (works only at two loops) --- should be replaced later by more general method
     */
    SubleadPoleTerm get_info_subleading(const Member&) const;
    const SubleadingPoleTracker& get_subleading_poles(const Member&) const;

    /**
     * Temporary method to handle 2-loop subleading pole contributions for 3-level (maximal, next-to-, and next-to-next-to maximal) hierarchies
     */
    void TMP_handle_subleading_pole_contributions();

    ~Genealogy();
  private:
    void register_1_parenthood(const Member&, const Member&);
    void register_all_n_gt_1_parenthoods();  /**< Picks all 1-parenthoods registered and by composition complets all n-parenthoods (n>1) */
    void produce_1p_momentum_maps_and_register(const Member& parent, const lGraph::xGraph& pinched, const Member& daughter, const lGraph::Propagator&, size_t = 0);
    void check_1p_register(const Member& parent, const Member& daughter);
    void generate_portal();
    void fill_extra_containers();
    void fill_extra_containers(const AmpEng::CutHierarchy&);
    void fill_multiplets();
};

} // namespace AmpEng
} // namespace Caravel

