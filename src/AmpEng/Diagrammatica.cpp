#include "AmpEng/Diagrammatica.h"
#include "AmpEng/Utilities.h"

namespace Caravel {
namespace AmpEng {

using namespace lGraph;

// an (ordered) container of the legs (in an std::set<size_t>, where size_t is the index of a given Leg) attached to each vertex in an edge
using EdgeLegs = std::vector<std::set<size_t>>;

// a pair to order the distribution of a set of legs:
//     first: an ordered list (std::vector) of the legs (in an std::set<size_t>, where size_t is the index of a given Leg) that go into a given node
//     second: an ordered list (std::vector) of the distribution (EdgeLegs) of legs in a given edge
using LegDistribution = std::pair<std::vector<std::set<size_t>>,std::vector<EdgeLegs>>;

Vacua::Vacua(int i): n_loops(i) {
    using namespace lGraph;
    if(i<=0){
        std::cerr<<"ERROR: Vacua constructor received zero or a negative number of loops: "<<i<<std::endl;
        std::exit(99999999);
    }
    // just avoid people going way out or range of applicability (this limit could be increased if made more efficient!)
    if(i>5){
        std::cerr<<"ERROR: hard limit set to 5 loops in Vacua constructor! Received Vacua("<<i<<")"<<std::endl;
        std::exit(i);
    }
    // add the starting point, the 'flower' diagram with one node
    Graph<int, Connection<int>> lgraph(std::vector<int>{0}, Connection<int>(std::vector<int>(i,0)));
    graphs[1] = std::set<Graph<int, Connection<int>>>{lgraph};
    // 1-loop case is done (special, because we force the 1-loop graph to have a node)
    if(i==1)
        return;
    // maximum number of nodes possible
    int maxn = 2*(i-1);
    for(int ii=2;ii<=maxn;ii++){
        // create locations
        graphs[ii]=std::set<Graph<int, Connection<int>>>();
        for(const auto& parent:graphs.at(ii-1))
            include_daughters(parent,ii);
    }
}

std::set<Graph<int, Connection<int>>> get_all_opened_graphs(int node,const Graph<int, Connection<int>>& p){
    using namespace lGraph;
    std::set<Graph<int, Connection<int>>> toret;

//std::cout<<"PROCESSING: "<<node<<std::endl;
//p.show_adjacency();

    // the new node set
    std::vector<int> nodes(int(p.get_n_nodes()+1),0);

    // retrieve the connection map
    const auto& connections = p.get_connection_map();

    size_t selfconn(0);
    if(connections.at(std::pair<size_t,size_t>{node,node}).size()>0)
        selfconn=connections.at(std::pair<size_t,size_t>{node,node}).size();

    size_t independentconn(0);
    std::vector<std::pair<size_t,size_t>> relevant_connections;

    for (auto conn : connections)
        // check conn contains 'node'
        if (int(conn.first.first) == node || int(conn.first.second) == node)
            // check it's a non-empty connection
            if (conn.second.size() > 0)
                // no duplicates, self connections treated independently
                if (conn.first.first < conn.first.second) relevant_connections.push_back(conn.first);

    for(auto& pair:relevant_connections)
        independentconn+=connections.at(pair).size();

    // we keep here the potential links to transfer
    //		each entry in the vector is a pair representing an edge to transfer
    //		with the pair:
    //			first: 	0 or 1 if selfconnection (0 "incoming", 1 "outgoing"), i+2 where i is the location in 'relevant_connections'
    //			second:	the node to which the edge is connected
    std::vector<std::pair<size_t,size_t>> edges2transfer;
    for(size_t ii=0;ii<selfconn;ii++){
        edges2transfer.push_back(std::make_pair(0,size_t(node)));
        edges2transfer.push_back(std::make_pair(1,size_t(node)));
    }
    for(size_t ii=0;ii<relevant_connections.size();ii++){
        const auto& lconn = connections.at(relevant_connections[ii]);
        if(relevant_connections[ii].first==size_t(node)){
            for(size_t jj=0;jj<lconn.size();jj++)
                edges2transfer.push_back(std::make_pair(ii+2,relevant_connections[ii].second));
        }
        else{
            for(size_t jj=0;jj<lconn.size();jj++)
                edges2transfer.push_back(std::make_pair(ii+2,relevant_connections[ii].first));
        }
    }
//std::cout<<"edges2transfer: "<<edges2transfer<<std::endl;

    // now we loop over all possible edge transfers to the 'new' node
    for(size_t ntransfer=2;ntransfer+1<2*selfconn+independentconn;ntransfer++){
        //std::pair<size_t,size_t> pick = {ntransfer, edges2transfer.size()};
        const auto& transfers = get_all_subsets(ntransfer, edges2transfer.size());

        // check all possible new graphs
        for(const auto& transfer:transfers){
            // local copy of the connection map
            auto lconnections = connections;

            // add all new connections

            // self connects and connection to 'node'
            {
                size_t countin(0);
                size_t countout(0);
                for(const auto& e:transfer){
                    const auto& theedge = edges2transfer[e];
                    if(theedge.first==0)
                        countin++;
                    else if(theedge.first==1)
                        countout++;
                }
                size_t movedpairs(0);
                size_t movedsingles(0);
                if(countin>countout){
                    movedpairs=countout;
                    movedsingles=countin-countout;
                }
                else{
                    movedpairs=countin;
                    movedsingles=countout-countin;
                }
                // we avoid single-connection new node
                if(movedsingles==0&&(countin+countout)==transfer.size())
                    continue;

                // handle self connect transfers
                for(size_t ii=0;ii<movedpairs;ii++)
                    lconnections[std::pair<size_t,size_t>{node,node}].erase(0);
                lconnections[std::make_pair(p.get_n_nodes(),p.get_n_nodes())]=Connection<int>(std::vector<int>(movedpairs,0));

                // handle the rest
                for(size_t ii=0;ii<movedsingles;ii++)
                    lconnections[std::pair<size_t,size_t>{node,node}].erase(0);
                lconnections[std::make_pair(size_t(node), p.get_n_nodes())] = Connection<int>(std::vector<int>(movedsingles + 1, 0));
            }

            // the rest of new connections
            {
                for(const auto& e:transfer){
                    const auto& theedge = edges2transfer[e];
                    if(theedge.first==0||theedge.first==1)
                        continue;
                    // drop existing connections
                    std::pair<size_t,size_t> exist = { size_t(node), theedge.second };
                    if(size_t(node)>theedge.second)
                        exist = { theedge.second, size_t(node) };
                    lconnections[exist].erase(0);
                    // create new ones
                    std::pair<size_t,size_t> create = { theedge.second, p.get_n_nodes() };
                    size_t nedges(0);
                    if(lconnections.find(create)==lconnections.end()){
                        nedges=1;
                    }
                    else{
                        nedges = lconnections.at(create).size()+1;
                    }
                    lconnections[create]=Connection<int>(std::vector<int>(nedges, 0));
                }
            }

            // check that node is not isolated
            size_t isolated(0);
            for(size_t ii=0;ii<p.get_n_nodes()+1;ii++){
                if(ii==size_t(node))
                    continue;
                std::pair<size_t,size_t> key = {ii,size_t(node)};
                if(ii>size_t(node))
                    key={size_t(node),ii};
                isolated+=lconnections.at(key).size();
            }
            if(isolated<2)
                continue;

            // clean up empty connections
            for (auto it = lconnections.begin(); it != lconnections.end();) {
                if (it->second.size() == 0)
                    it = lconnections.erase(it);
                else
                    it++;
            }
            Graph<int, Connection<int>> lgraph(nodes,lconnections);
            // sanity check
            if(lgraph.get_n_loops()!=p.get_n_loops()){
                std::cerr<<"ERROR: get_all_opened_graphs(.) produced a "<<lgraph.get_n_loops()<<"-loop graph when the input was a "<<p.get_n_loops()<<"-loop graph!"<<std::endl;
                std::exit(64518);
            }
            toret.insert(lgraph);
#if 0
            auto test = toret.insert(lgraph);
            if(test.second){
                std::cout<<"INSERTED THE FOLLOWING GRAPH!"<<std::endl;
                lgraph.show_adjacency();
            }
#endif
        }
    }

//std::cout << "FOUND: " << relevant_connections << " (self,independent): (" << selfconn << "," << independentconn << ")" << std::endl;
//std::cout << "BORROW: " << edges2transfer << std::endl;

    return toret;
}

void Vacua::include_daughters(const Graph<int, Connection<int>>& p,int n){
    using namespace lGraph;
    int nnodes(p.get_n_nodes());
    // sanity check
    if(nnodes+1!=n){
        std::cerr<<"ERROR: Vacua::include_daughters(.) received parent with "<<nnodes<<" to fill the level with "<<n<<std::endl;
        std::exit(131313);
    }
    auto& toinsert = graphs.at(n);
    // treat each node in p independently
    for(int ii=0;ii<nnodes;ii++){
        auto trials = get_all_opened_graphs(ii,p);
        // TODO: in C++17 a merge would be more efficient
        for(auto d:trials)
            toinsert.insert(d);
    }
}

void assign_loop_momentum(size_t& loop, Strand& strand){
    int theloop=int(loop);
    size_t link(0);
    if(loop%2==0){
        theloop=-theloop;
        link=strand.size();
    }
    strand.set_momentum_label(link,theloop);
    loop++;
}

bool check_node_has_dof(const size_t& node, const std::map<std::pair<size_t,size_t>, Connection<Strand>>& map){
    // not counting self connections
    size_t attachments(0);
    size_t assignments(0);
    for(const auto& conn:map){
        if(conn.first.first==conn.first.second)
            continue;
        // connection related to node
        if(conn.first.first==node||conn.first.second==node){
            attachments+=conn.second.size();
            for(auto s:conn.second.get_edges()){
                if(s.has_link_with_mom_label()){
                    assignments++;
                }
            }
        }
    }
    return assignments+1<attachments;
}

std::vector<std::pair<size_t, size_t>> extract_loop_momenta_assignment(const size_t& inloop, const std::map<std::pair<size_t, size_t>, Connection<Strand>>& inmap,
                                                                       const sGraph& d,const std::vector<Node>& nodes) {
    using namespace lGraph;
    using Caravel::operator<<;
    std::vector<std::pair<size_t,size_t>> single_strands;
    for (auto& conn : inmap) {
        // is not tadpole
        if (conn.first.first != conn.first.second) {
            if(conn.second.size() == 1)
                single_strands.push_back(conn.first);
        }
    }
    // just in case necessary, we also add connections with size two or larger
    for (auto& conn : inmap) {
        // is not tadpole
        if (conn.first.first != conn.first.second) {
            if(conn.second.size() > 1)
                single_strands.push_back(conn.first);
        }
    }

    if(single_strands.size()<d.get_n_loops()+1-inloop){
        std::cerr<<"ERROR: extract_loop_momenta_assignment(.) found only "<<single_strands<<" as possible single-strand connections, and we need at least "<<d.get_n_loops()+1-inloop<<std::endl;
        std::exit(1);
    }

    std::vector<std::pair<size_t,size_t>> toret;
    std::set<std::vector<std::pair<size_t,size_t>>> tried;

    bool success(false);
    // we attempt all combinations and break in first success TODO: better generic choices?
    do {
        size_t loop = inloop;
        std::map<std::pair<size_t, size_t>, Connection<Strand>> outmap = inmap;

        toret.clear();
        for(size_t ii=0;ii<d.get_n_loops()+1-inloop;ii++){
            toret.push_back(single_strands[ii]);
            assign_loop_momentum(loop, outmap.at(single_strands[ii])[0]);
        }
        auto ltest = tried.insert(toret);
        if(!ltest.second)
            continue;
        try{ xGraph test(Graph<Node, Connection<Strand>>(nodes,outmap)); }
        catch(...) {
            continue;
        }
        success=true;
        break;

    } while(std::next_permutation(single_strands.begin(),single_strands.end()));

    if(!success){
        std::cerr<<"ERROR: extract_loop_momenta_assignment(.) failed with loop: "<<inloop<<" graph: "<<std::endl;
        d.show();
        std::cout<<"	and input connection map: "<<inmap<<std::endl;
        std::exit(1);
    }

    return toret;
}

xGraph construct_massless_xGraph_from_sGraph(const sGraph& d, const std::vector<Leg>& legs){
    using namespace lGraph;
    // first assemble nodes
    std::vector<Node> lnodes;
    for(auto n:d.get_nodes()){
        std::vector<Leg> llegs;
        for(auto i:n)  llegs.push_back(legs[i]);
        lnodes.push_back(Node(llegs));
    }
    // now corresponding map
    const auto& inmap = d.get_connection_map();
    std::map<std::pair<size_t,size_t>,Connection<Strand>> outmap;
    for(const auto& conn:inmap){
        std::vector<Strand> lstrands;
        for(auto vec:conn.second){
            std::vector<Bead> lbeads;
            for(auto iv:vec){
                std::vector<Leg> llegs;
                for(auto i:iv) llegs.push_back(legs[i]);
                lbeads.push_back(Bead(llegs));
            }
            // massless links
            // TODO: handle generic mass assignments
            std::vector<lGraph::Link> llinks(lbeads.size()+1,lGraph::Link(0));
            lstrands.push_back(Strand(lbeads,llinks));
        }
        outmap[conn.first]=Connection<Strand>(lstrands);
    }
    // just some quick standard labeling of loop momenta
    size_t loop(1);
    // this procedure should work in general
    {
        // first fill tadpoles and connections with 2 or more strands
        for (auto& conn : outmap) {
            // tadpole(s), all strands assigned a loop momentum
            if(conn.first.first==conn.first.second){
                for(size_t ii=conn.second.size();ii>0;ii--){
                    assign_loop_momentum(loop,conn.second[ii-1]);
                }
            }
            // cases with sure 'internal' momenta, all but one assigned a loop momentum
            else if(conn.second.size()>1){
                for(size_t ii=conn.second.size();ii>1;ii--){
                    assign_loop_momentum(loop,conn.second[ii-1]);
                }
            }
        }
        if (int(loop) - 1 < d.get_n_loops()) {
            // loop momentum assignment can be extracted only from vacuum information (which we handle through the adjacency matrix)
            static thread_local std::unordered_map<std::vector<std::vector<size_t>>,std::vector<std::pair<size_t,size_t>>> loop_momenta_assignments;
            if(loop_momenta_assignments.find(d.get_adjacency())==loop_momenta_assignments.end())
                loop_momenta_assignments[d.get_adjacency()]=extract_loop_momenta_assignment(loop,outmap,d,lnodes);
            for (const auto& key : loop_momenta_assignments.at(d.get_adjacency())) {
                auto& conn = outmap.at(key);
                assign_loop_momentum(loop, conn[0]);
            }
        }
    }
    xGraph toret(Graph<Node, Connection<Strand>>(lnodes,outmap));
    return toret;
}

Walkers::Walkers(int n,const std::vector<size_t>& inlegs): n_legs(inlegs.size()), legs_masses(inlegs), vacuum_graphs(n) {
    n_loops = n;
    using namespace lGraph;
    std::vector<size_t> lindices;
    // the +1 is to get the standard of momentum indices in corresponding xGraphs
    for (size_t ii = 0; ii < legs_masses.size(); ii++) lindices.push_back(ii + 1);
    // store momentum labels for sorting legs_masses
    std::sort(lindices.begin(), lindices.end(), [&](const size_t& j, const size_t& k) { return legs_masses[j - 1] < legs_masses[k - 1]; });
    // now sort legs_masses
    std::sort(legs_masses.begin(),legs_masses.end());
    // fill legs container
    for(size_t ii=0;ii<legs_masses.size();ii++)    legs.push_back(Leg(true,legs_masses[ii],lindices[ii]));
    std::cout<<"Starting Walker construction for "<<n<<"-loop diagrams with legs: "<<legs<<std::endl;

    // we loop over all vacuum graphs (from the ones with 1 node till the max), add legs and store new sGraphs
    for(int ii=1;ii<=int(vacuum_graphs.get_all_vacuum_graphs().size());ii++){
        const auto& vacua_ii = vacuum_graphs.get_vacuum_graphs(ii);
        for(const auto& d:vacua_ii){
            process_vacuum_graph(ii,d);
        }
        std::cout<<"sGraphs added with "<<ii<<" nodes and "<<n_legs<<" legs ["<<graphs.at(size_t(ii)).size()<<"]"<<std::endl;
#if 0
        for(const auto& d:graphs.at(size_t(ii))){
            d.show();
        }
        std::cout<<std::endl;
#endif
    }
    // use set for a sorted-unique container
    std::unordered_map<size_t,std::vector<std::set<xGraph,xComp>>> lxgraphs;
    for (const auto& ds : graphs) {
        for (const auto& d : ds.second) {
            xGraph lgraph = construct_massless_xGraph_from_sGraph(d, legs);
            size_t nlegs = lgraph.get_n_topology_legs();
            if (lxgraphs.find(ds.first) == lxgraphs.end()) lxgraphs[ds.first] = std::vector<std::set<xGraph, xComp>>();
            if(nlegs+1>lxgraphs.at(ds.first).size())
                for(size_t ii=lxgraphs.at(ds.first).size();ii<nlegs+1;ii++)
                    lxgraphs.at(ds.first).push_back(std::set<xGraph, xComp>());
            lxgraphs.at(ds.first)[nlegs].insert(lgraph);
        }
    }
    // now fill the xGraph containers
    for (const auto& nc: lxgraphs) {
        xgraphs[nc.first] = std::vector<std::vector<xGraph>>();
        auto& array = xgraphs.at(nc.first);
        for(const auto& sxgs: nc.second)
            array.push_back( std::vector<xGraph>( sxgs.begin(), sxgs.end() ) );
    }
    // now fill the topologies
    update_containers();
    // show
    show_graph_counting();
}

Hierarchy::~Hierarchy() {}
Walkers::~Walkers() {}
Pincher::~Pincher() {}

Hierarchy::Hierarchy(const AmpEng::CutHierarchy& h, bool unique) : unique_insertion(unique) {
    auto inhierarchy = h.get_hierarchy();
    for (auto& level : inhierarchy) {
        for (auto& cut : level) {
            auto& xg = cut.get_graph();
            // we let the hierarchy to insert same xgraphs several times
            insert_xgraph(xg);
        }
    }
    update_containers();
}

Hierarchy::Hierarchy(const std::vector<lGraph::xGraph>& h, bool unique) : unique_insertion(unique) {
    assert(h.size() > 0);
    n_loops = h[0].get_n_loops();
    for (auto& xg : h) insert_xgraph(xg);
    update_containers();
}

namespace detail {

std::map<size_t, size_t> index_getter(const std::map<size_t, ParticleType>& internal) {
    std::map<size_t, size_t> toret;
    std::set<size_t> in, out;
    for(auto [i, pt]: internal) {
        toret[i] = pt.get_mass_index();
        in.insert(i);
        out.insert(pt.get_mass_index());
    }
    // sanity check (1-1 mapping)
    assert(in.size() == out.size());
    return toret;
}

} // namespace detail

Hierarchy::Hierarchy(const Hierarchy& in, const GraphCutMap& gcm, bool unique): unique_insertion(unique) {
    for (auto& xg: in.get_all_xgraphs())
        insert_xgraph(lGraph::replace_mass_indices(xg.get_base_graph(), detail::index_getter(gcm.internal)));
    update_containers();
    for(auto& [p, level]: hier_prop)
        if(level.size()>0) {
            n_loops = level[0].get_n_loops();
            break;
        }
}

void Hierarchy::insert_xgraph(const xGraph& in) {
    auto n = in.get_base_graph().get_nodes().size();
    auto pointer = xgraphs.find(n);
    if (pointer == xgraphs.end()) xgraphs[n] = std::vector<std::vector<lGraph::xGraph>>();
    auto& array = xgraphs.at(n);
    auto nlegs = in.get_n_topology_legs();
    while (array.size() <= nlegs) array.push_back(std::vector<lGraph::xGraph>());
    auto& container = array[nlegs];
    if (unique_insertion) {
        auto isthere = std::find_if(container.begin(), container.end(), [in](const xGraph& lxg) { return in.get_base_graph() == lxg.get_base_graph(); });
        if (isthere != container.end()) {
            std::cerr << "ERROR: while in unique insertion mode, called Hierarchy::insert_xgraph(.) when input xgraph already added! The xGraph: " << std::endl;
            in.show(std::cerr);
            std::exit(1);
        }
    }
    container.push_back(in);
}

void Hierarchy::update_containers(){
    // use set for a sorted-unique container
    std::unordered_map<size_t,std::vector<std::set<Topology>>> ltopologies;
    for(const auto& vsxg:xgraphs){
        if(ltopologies.find(vsxg.first)==ltopologies.end())
            ltopologies[vsxg.first]=std::vector<std::set<Topology>>(vsxg.second.size());
        for(size_t ll=0;ll<vsxg.second.size();ll++){
            for(auto& xg:vsxg.second[ll])
                ltopologies.at(vsxg.first)[ll].insert(xg.get_topology());
        }
    }
    // now fill topologies container
    topologies.clear();
    for (const auto& vsxg: ltopologies) {
        topologies[vsxg.first] = std::vector<std::vector<Topology>>();
        auto& array = topologies.at(vsxg.first);
        for (const auto& sxg: vsxg.second)
            array.push_back( std::vector<Topology>( sxg.begin(), sxg.end() ) );
    }

    hier_prop.clear();
    for(const auto& axg: xgraphs)
        for(const auto& vxg: axg.second)
            for(auto& xg: vxg){
                size_t nprops = xg.get_n_edges();
                if(hier_prop.find(nprops) == hier_prop.end())
                    hier_prop[nprops] = std::vector<xGraph>();
                hier_prop.at(nprops).push_back(xg);
            }

    topo_hier_prop.clear();
    for(const auto& atp: topologies)
        for(const auto& vtp: atp.second)
            for(auto& tp: vtp){
                size_t nprops(0);
                for(const auto& conn: tp.get_connection_map()) 
                    for(const auto& strand: conn.second)    nprops+= strand.size()+1; // +1 because size() returns # of beads
                if(topo_hier_prop.find(nprops) == topo_hier_prop.end())
                    topo_hier_prop[nprops] = std::vector<Topology>();
                topo_hier_prop.at(nprops).push_back(tp);
            }
}

void Hierarchy::show_graph_counting() const {
    std::cout << "xGraph counting: " << std::endl;
    size_t cc(0);
    for (auto& array : xgraphs) {
        size_t slevel(0);
        for (auto & set : array.second) {
            std::cout << "	with " << array.first << " nodes and " << slevel << " points we have: " << set.size() << " graphs!" << std::endl;
            cc += set.size();
            ++slevel;
        }
    }
    std::cout << "	for a total " << cc << " graphs!" << std::endl;

    std::cout << "topologies counting: " << std::endl;
    cc=0;
    for (auto& array : topologies) {
        size_t slevel(0);
        for (auto & set : array.second) {
            std::cout << "	with " << array.first << " nodes and " << slevel << " points we have: " << set.size() << " graphs!" << std::endl;
            cc += set.size();
            ++slevel;
        }
    }
    std::cout << "	for a total " << cc << " topologies!" << std::endl;
}

void Hierarchy::filter(const std::function<bool(const xGraph&)>& test, bool print_filtered){
    size_t counter(0);
    for(auto& vsxg:xgraphs){
        for(auto& sxg:vsxg.second){
            for(auto it = sxg.begin(); it != sxg.end(); ){
                if(test(*it)){
                    if (print_filtered) {
                        std::cout << "Filtering the xGraph: " << std::endl;
                        it->show();
                    }
                    it = sxg.erase(it);
                    counter++;
                }
                else
                    ++it;
            }
        }
    }
    std::cout<<"# of xGraphs filtered: "<<counter<<std::endl;
    update_containers();
    show_graph_counting();
}

size_t Hierarchy::size() const {
    size_t toret(0);
    for(auto& vsxg:xgraphs)
        for(auto& sxg:vsxg.second)
            toret+=sxg.size();
    return toret;
}

size_t Hierarchy::topology_size() const {
    size_t toret(0);
    for(auto& vsxg:topologies)
        for(auto& sxg:vsxg.second)
            toret+=sxg.size();
    return toret;
}

void recursive_assemble_simple_partitions(const size_t& step,const size_t& max,const size_t& accumulate,const size_t& total,const std::vector<size_t>& incollect,std::set<std::vector<size_t>>& assignments){
    if(step+1==max){
        auto collect = incollect;
        collect.push_back(total-accumulate);
        assignments.insert(collect);
        return;
    }
    else if(accumulate==total){
        auto collect = incollect;
        for(size_t ii=step;ii<max;ii++)    collect.push_back(0);
        assignments.insert(collect);
        return;
    }
    else{
        for(size_t local=0;local<=total-accumulate;local++){
            auto collect = incollect;
            collect.push_back(local);
            recursive_assemble_simple_partitions(step+1,max,accumulate+local,total,collect,assignments);
        }
    }
}

void fill_container_of_partitions(const size_t& entry,const size_t& start,const std::vector<std::set<size_t>>& p,const std::vector<size_t>& partition,
                                    const size_t& divisions,const std::vector<size_t>& indices,std::set<std::vector<std::set<size_t>>>& collect){
    if(entry==divisions){
        collect.insert(p);
        return;
    }
    else{
        const size_t& total = partition[entry];
        std::vector<std::set<size_t>> plocal = p;
        plocal.push_back(std::set<size_t>{});
        for(size_t ii=start;ii<start+total;ii++)
            plocal.back().insert(indices[ii]);
        fill_container_of_partitions(entry+1,start+total,plocal,partition,divisions,indices,collect);
    }
}

std::set<std::vector<std::set<size_t>>> detailed_assemble_simple_sorted_partitions(const std::vector<size_t>& partition){
    std::set<std::vector<std::set<size_t>>> toret;
    size_t divisions=partition.size();
    size_t nlegs(0);
    for(auto i:partition)    nlegs+=i;
    std::vector<size_t> indices(nlegs);
    for(size_t ii=0;ii<nlegs;ii++)    indices[ii]=ii;

    // simple nested procedure for filling containers
    // we keep first element fixed
    if(indices.size()>1){
        do {
            fill_container_of_partitions(0,0,std::vector<std::set<size_t>>{},partition,divisions,indices,toret);
        } while(std::next_permutation(indices.begin(),indices.end()));
    }
    else{
        fill_container_of_partitions(0,0,std::vector<std::set<size_t>>{},partition,divisions,indices,toret);
    }

    return toret;
}

std::set<std::vector<std::set<size_t>>> detailed_assemble_simple_partitions(const std::vector<size_t>& partition){
    std::set<std::vector<std::set<size_t>>> toret;
    std::vector<size_t> indices(partition.size());
    for(size_t ii=0;ii<partition.size();ii++)    indices[ii]=ii;
    std::vector<size_t> lpartition = partition;

    // store lpartition sort
    std::sort(indices.begin(),indices.end(),[&](const size_t& j,const size_t& k) { return lpartition[j]<lpartition[k]; } );
    // now sort
    std::sort(lpartition.begin(),lpartition.end());

    static thread_local std::unordered_map<std::vector<size_t>,std::set<std::vector<std::set<size_t>>>> sorted_leg_distribution;
    if(sorted_leg_distribution.find(lpartition)==sorted_leg_distribution.end())
        sorted_leg_distribution[lpartition]=detailed_assemble_simple_sorted_partitions(lpartition);
    for(const auto& distribution:sorted_leg_distribution.at(lpartition)){
        std::vector<std::set<size_t>> local(distribution.size());
        // unsort
        for(size_t ii=0;ii<distribution.size();ii++)
            local[indices[ii]]=distribution[ii];
        toret.insert(local);
    }

    return toret;
}

std::vector<std::vector<std::set<size_t>>> assemble_simple_partitions(size_t nlegs,size_t total,int nloops){
    std::set<std::vector<std::set<size_t>>> toret;

    std::set<std::vector<size_t>> assignments;
    for(size_t first=0;first<=nlegs;first++){
        std::vector<size_t> nassignemnt = {first};
        recursive_assemble_simple_partitions(1,total,first,nlegs,nassignemnt,assignments);
    }
#if 0
std::cout<<"TEST: assignments= "<<assignments<<" size: "<<assignments.size()<<std::endl;
for(const auto& v:assignments){ if(v.size()!=total) std::cout<<"ERROR not right number of entries! "<<v.size()<<" vs. "<<total<<std::endl;
  size_t sum(0); for(auto i:v) sum+=i; if(sum!=nlegs) std::cout<<"ERROR not right sum! "<<v<<std::endl; }
#endif

    static thread_local std::unordered_map<std::vector<size_t>,std::set<std::vector<std::set<size_t>>>> container_of_leg_distribution;
    // traverse all assignments
    for(const auto& part:assignments){
        if(container_of_leg_distribution.find(part)==container_of_leg_distribution.end())
            container_of_leg_distribution[part]=detailed_assemble_simple_partitions(part);
        // TODO: in C++17 a merge would be more efficient
        for(const auto& local:container_of_leg_distribution.at(part))
            toret.insert(local);
    }

    std::vector<std::vector<std::set<size_t>>> vtoret;
    for(const auto& local:toret)
        vtoret.push_back(local);
    // A hard-wired filter to avoid 1-loop graphs with legs and nothing attached to the node (a requirement for 1-loop xGraphs)
    if(nloops==1&&nlegs>0){
        for(size_t ii=vtoret.size();ii>0;ii--){
            // the zeroth entry corresponds to the node
            if(vtoret[ii-1][0].size()==0)
                vtoret.erase(vtoret.begin()+ii-1);
        }
    }
//std::cout<<"assemble_simple_partitions("<<nlegs<<","<<total<<")[size:"<<vtoret.size()<<"]: zero_comp: "<<vtoret[0]<<" tenth_comp: "<<vtoret<<std::endl;
    return vtoret;
}

void recursive_add_splitts(const size_t& step,const size_t& nentries,const size_t& start,const EdgeLegs& filling,const std::vector<size_t>& indices,std::set<EdgeLegs>& toret){
    // recursion finished
    if(step==nentries){
        // check that only results with proper number of entries remain
        if(filling.size()!=nentries)
            return;

        // check that all indices are included
        size_t totalentries(0);
        for(const auto& f:filling)    totalentries+=f.size();
        if(totalentries!=indices.size())
            return;

        toret.insert(filling);
        return;
    }
    else {
        for (size_t index = start; index < indices.size(); index++) {
            EdgeLegs local = filling;
            local.push_back(std::set<size_t>{});
            
            local.back().insert(indices.begin()+start, indices.begin() + index + 1);
            recursive_add_splitts(step+1, nentries, index+1, local, indices, toret);
        }
    }
}

std::set<EdgeLegs> index_splitter(const std::vector<size_t>& indices){
    std::set<EdgeLegs> toret;
    for(size_t ii=1;ii<=indices.size();ii++){
        std::vector<size_t> local = indices;
        do {
            recursive_add_splitts(0,ii,0,EdgeLegs{},local,toret);
        } while(std::next_permutation(local.begin(),local.end()));
    }
//std::cout<<"SHOW: input "<<indices<<" out: "<<toret<<std::endl; 
    return toret;
}

std::vector<EdgeLegs> splitting_indices_in_strand(const std::set<size_t>& in){
    std::vector<size_t> indices;
    for(size_t ii=0;ii<in.size();ii++)    indices.push_back(ii);
    std::vector<size_t> input;
    for(auto ii:in)    input.push_back(ii);

    static thread_local std::unordered_map<std::vector<size_t>,std::set<EdgeLegs>> simplified_strand_collector;
    if(simplified_strand_collector.find(indices)==simplified_strand_collector.end())
        simplified_strand_collector[indices]=index_splitter(indices);
    std::vector<EdgeLegs> toret;
    if(simplified_strand_collector.at(indices).size()==0)
        toret.push_back({});
    for(const auto& term:simplified_strand_collector.at(indices)){
        EdgeLegs include;
        for(const auto& s:term){
            std::set<size_t> local;
            for(const auto& ii:s)    local.insert(input[ii]);
            include.push_back(local);
        }
        toret.push_back(include);
    }
#if 0
for(const auto& el:toret){
  std::set<size_t> test;
  for(auto s:el)  for(auto ii:s)  test.insert(ii);
  if(test!=in){
      std::cerr<<"BROKE!! "<<test<<" "<<in<<std::endl;
      std::exit(45);
  }
}
#endif
    return toret;
}

void recursive_addition_leg_distributions(const size_t& step,const std::vector<EdgeLegs>& filling,const std::vector<std::set<size_t>>& nodes_content,
                                           const std::vector<std::vector<EdgeLegs>>& info_for_edges,std::vector<LegDistribution>& container){
    if(step==info_for_edges.size()){
#if 0
std::set<size_t> all;
for(const auto& el:filling){
    for(const auto& s:el)  for(auto ii:s){  
       auto ttt=all.insert(ii); 
       if(!ttt.second){
           std::cerr<<"ERROR!!!: "<<all<<" "<<filling<<std::endl;
           std::exit(1234);
       }
    }
}
#endif
        container.push_back({nodes_content,filling});
        return;
    }
    else{
        for(size_t ii=0;ii<info_for_edges[step].size();ii++){
            auto cfilling = filling;
            cfilling.push_back(info_for_edges[step][ii]);
            recursive_addition_leg_distributions(step+1,cfilling,nodes_content,info_for_edges,container);
        }
    }
}

void add_corresponding_leg_distributions(const std::vector<std::set<size_t>>& nodes_content,const std::vector<std::vector<EdgeLegs>>& info_for_edges,std::vector<LegDistribution>& container){
    recursive_addition_leg_distributions(0,std::vector<EdgeLegs>{},nodes_content,info_for_edges,container);
}

std::vector<LegDistribution> assemble_partitions(size_t nlegs,int nnodes,size_t nedges,int nloops){
    if(nnodes<1){
        std::cerr<<"ERROR: assemble_partitions(.) received "<<nnodes<<" as number of nodes, but it should be 1 or greater!"<<std::endl;
        std::exit(17);
    }

    size_t all = size_t(nnodes)+nedges;

    // first, a simple distribution of nlegs in all
    static thread_local std::unordered_map<std::pair<size_t,size_t>,std::vector<std::vector<std::set<size_t>>>> generic_partition;
    if(generic_partition.find({nlegs,all})==generic_partition.end())
        generic_partition[{nlegs,all}]=assemble_simple_partitions(nlegs,all,nloops);

    std::vector<LegDistribution> toret;

    static thread_local std::unordered_map<std::vector<size_t>,std::vector<EdgeLegs>> strand_collector;
//size_t counter(0);
    for(const auto& generic:generic_partition.at({nlegs,all})){
//counter++;
        std::vector<std::set<size_t>> first;
        first.insert(first.begin(),generic.begin(),generic.begin()+nnodes);
        std::vector<std::set<size_t>> to_construct_second;
        to_construct_second.insert(to_construct_second.begin(),generic.begin()+nnodes,generic.end());
        std::vector<std::vector<EdgeLegs>> helper;
        for(size_t ii=0;ii<to_construct_second.size();ii++){
            std::vector<size_t> local; for(const auto& jj:to_construct_second[ii]) local.push_back(jj);
            if(strand_collector.find(local)==strand_collector.end())
                strand_collector[local]=splitting_indices_in_strand(to_construct_second[ii]);
            helper.push_back(strand_collector.at(local));
        }
        add_corresponding_leg_distributions(first,helper,toret);
    }

//std::cout<<"assemble_partitions("<<nlegs<<","<<nnodes<<","<<nedges<<"): size: "<<toret.size()<<" first: " << toret[0] <<" back: " << toret.back() <<" counter: "<<counter<<std::endl;
    return toret;
}

void Walkers::process_vacuum_graph(int n, const Graph<int, Connection<int>>& p){
    using namespace lGraph;
    // sanity check
    if(n!=int(p.get_n_nodes())){
        std::cerr<<"ERROR: Walkers::process_vacuum_graph(.) got inconsistent number of nodes: "<<n<<" and graph: "<<std::endl;
        p.show_adjacency();
        std::exit(1);
    }
    // compute partitions of n_legs in n nodes and number of edges, and for each generate all diagrams
    //     key: a tuple containing { number of legs, number of nodes, numbers of edges }
    static thread_local std::unordered_map<std::tuple<size_t,int,size_t>,std::vector<LegDistribution>> leg_partitions;
    // check if we need to assemble partition
    if(leg_partitions.find({n_legs, n, p.get_n_edges()})==leg_partitions.end())
        leg_partitions[{n_legs, n, p.get_n_edges()}]=assemble_partitions(n_legs, n, p.get_n_edges(), p.get_n_loops());

    for (const auto& ld : leg_partitions.at({n_legs, n, p.get_n_edges()})) {

        // construct local graph
        std::vector<std::vector<size_t>> nodes;
        for (const auto& n : ld.first) {
            nodes.push_back(std::vector<size_t>());
            for (const auto& i : n) nodes.back().push_back(i);
        }
        std::map<std::pair<size_t, size_t>, Connection<std::vector<std::vector<size_t>>>> map_conn;
        // this defines the traverse procedure
        size_t counter(0);
        for (const auto& conn : p.get_connection_map()) {
            std::vector<std::vector<std::vector<size_t>>> toconst;
            for (size_t ii = 0; ii < conn.second.size(); ii++) {
                toconst.push_back(std::vector<std::vector<size_t>>());
                for (const auto& b : (ld.second)[counter]) {
                    toconst.back().push_back(std::vector<size_t>());
                    for (const auto& i : b) toconst.back().back().push_back(i);
                }
                counter++;
            }
            map_conn[conn.first] = Connection<std::vector<std::vector<size_t>>>(toconst);
        }
        sGraph lgraph(nodes, map_conn);
        if(graphs.find(nodes.size())==graphs.end())
            graphs[nodes.size()]=std::set<sGraph>();
        graphs.at(nodes.size()).insert(lgraph);
#if 0
        auto test = graphs.at(nodes.size()).insert(lgraph);
        if(test.second){
            std::cout << "Added new diagram: " << std::endl;
            lgraph.show();
        }
#endif
    }
}

Walkers::Walkers(int n,size_t l): Walkers(n,std::vector<size_t>(l,0)) {}

std::vector<xGraph> Hierarchy::get_all_xgraphs() const {
    std::vector<xGraph> toret;

    for(const auto& vsxgs:xgraphs)
        for(const auto& sxgs:vsxgs.second)
            for(const auto& xg:sxgs)
                toret.push_back(xg);

    return toret;
}

namespace _misc {
bool matching_xgraphs(int n, const std::vector<Leg>& legs, const xGraph& compare) {
    if( n != compare.get_n_loops() )
        return false;
    if( legs.size() != compare.get_legs().size() )
        return false;
    auto legscopy = legs;
    for(auto& l : compare.get_legs()) {
        if( std::find(legscopy.begin(), legscopy.end(),l) == legscopy.end() )
            return false;
        legscopy.erase(std::remove(legscopy.begin(), legscopy.end(), l), legscopy.end());
    }
    return true;
}
}

Pincher::Pincher(const std::vector<xGraph>& in, size_t depth) {
    // perform some sanity checks
    if( in.size() == 0 ){
        std::cerr<<"ERROR: using Pincher(const std::vector<xGraph>&) with an empty vector!"<<std::endl;
        std::exit(1);
    }
    n_loops = in[0].get_n_loops();
    legs = in[0].get_legs();
    n_legs = legs.size();
    for(const auto& l:legs)
        legs_masses.push_back( l.get_mass_index() );
    for(size_t ii = 1; ii<in.size(); ii++) {
        if( !_misc::matching_xgraphs(n_loops, legs, in[ii]) ) {
            std::cerr<<"ERROR: Pincher constructor received the following two xGraph's which do not match!"<<std::endl;
            in[0].show();
            in[ii].show();
            std::exit(1);
        }
    }

    // fill Hierarchy::xgraphs container
    std::unordered_map<size_t,std::vector<std::set<xGraph,xComp>>> lxgraphs;
    for( const auto& diag : in )
        process_input_graph(diag, lxgraphs, depth);

    // now fill the xGraph containers
    for (const auto& nc: lxgraphs) {
        xgraphs[nc.first] = std::vector<std::vector<xGraph>>();
        auto& array = xgraphs.at(nc.first);
        for(const auto& sxgs: nc.second)
            array.push_back( std::vector<xGraph>( sxgs.begin(), sxgs.end() ) );
    }

    // now fill the topologies
    update_containers();
    // show
    show_graph_counting();
}

namespace _misc {

bool attempt_insert( const xGraph& xg, std::unordered_map<size_t,std::vector<std::set<xGraph,xComp>>>& container){
    if (container.find(xg.get_n_nodes()) == container.end()) container[xg.get_n_nodes()] = std::vector<std::set<xGraph, xComp>>();
    if(xg.get_n_topology_legs()+1>container.at(xg.get_n_nodes()).size())
        for(size_t ii=container.at(xg.get_n_nodes()).size();ii<xg.get_n_topology_legs()+1;ii++)
            container.at(xg.get_n_nodes()).push_back(std::set<xGraph, xComp>());
    auto itry = container.at(xg.get_n_nodes())[xg.get_n_topology_legs()].insert(xg);

    return itry.second;
}

void insert_pinches(const int& nloops, const std::vector<xGraph>& topinch, std::unordered_map<size_t,std::vector<std::set<xGraph,xComp>>>& container, size_t step, size_t depth) {
    if ( topinch.size() == 0 )
        return;
    if ( depth > 0 && step > depth )
        return;

    std::vector<xGraph> added_diags;

    for(const auto& diagram : topinch) {
        auto links = diagram.list_links();
        for(const auto& link : links){
            auto newxg = diagram.pinch( link );
            if ( newxg.get_n_loops() != nloops )
                continue;
            auto itry = attempt_insert( newxg, container );
            if ( itry )
                added_diags.push_back( newxg );
        }
    }

    if ( added_diags.size() != 0 )
        insert_pinches(nloops, added_diags, container, ++step, depth);
}

}

void Pincher::process_input_graph(const xGraph& xg, std::unordered_map<size_t,std::vector<std::set<xGraph,xComp>>>& container, size_t depth) {
    std::vector<xGraph> topinch;

    // try to insert xg in container
    auto itry = _misc::attempt_insert( xg, container );

    if( itry )
        topinch.push_back(xg);

    // pinch all new diagrams (recursive)
    _misc::insert_pinches(xg.get_n_loops(), topinch, container, 1, depth);
}

} // namespace AmpEng
} // namespace Caravel
