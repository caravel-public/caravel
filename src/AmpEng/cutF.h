/**
 * @file cutF.h
 *
 * @date 24.7.2016
 *
 * @brief Header file for abstract objects used in constructing planar (subtraction) hierarchies
 *
 * Header file for all classes associated to cutF and their hierarchy. Implementations are included in cutF.cpp
 *
*/
#pragma once

#include <vector>
#include <cstddef>

#include "AmpEng/cstructure.h"
#include "Forest/Forest.h"
#include "Graph/Graph.h"

namespace Caravel {

// Forward declarations
template <class T,size_t D,size_t Ds,typename> class Builder_fixed_Ds;
template <class T,size_t D> class Builder;
class Forest;

namespace AmpEng{


class CStructure;
class CutHierarchy;
class cutF_min;
class cutF;

/**
 * This class will help to organize parenthood relations between a given cut and a n-daughter
 */
class parenthood {
		friend class cutF;
		friend class CutHierarchy;
		friend class subtraction;
		size_t parenthood_level;	/**< Integer to how many levels relate the parent and daughter cuts (e.g. 1 if it is parent-daughter, 2 if it is granparent-daughter, etc */
		size_t locate;	/**< Store the location of the parent in the hierarchy */
		std::pair<size_t,size_t> parent;	/**< Location of parent in associated cut_hierarchy (this duplicates info, but cleaner for now) */
		std::pair<size_t,size_t> daughter;	/**< Location of daughter in associated cut_hierarchy (this duplicates info, but cleaner for now) */
		std::vector<std::vector<size_t> > paths;	/**< Keeps inequivalent paths for parenthood (maybe this info don't need to be stored) */
		std::vector<std::pair<size_t,size_t> > one_parents;	/**< Defines the 1-parents associated to each inequivalent path in paths. first: location 1-parent; second: location of (parenthood_level-1) parent of 1-parent */
		std::vector<size_t> locate_subtraction_in_container_subs;	/**< Keeps location of corresponding subtraction in associated CStructure.container_subs */
		size_t one_parent_received;		/**< to keep info on passed (to constructor) one_p values (in cases this doubles information, but we keep it for simplicity) */
		size_t one_parent_nth_parent_received;	/**< to keep info on passed (to constructor) one_p values (in cases this doubles information, but we keep it for simplicity) */
		/**
		 * Each entry in this static vector represents:
		 *
		 *	first: an (i,j) pair with:
		 *
		 *		i: # of props in parent cut
		 *
		 *		j: # of props that have to be drop to reach daughter
		 *
		 *	second: a vector of vectors containing each inequivalent inheritance path
		 */
		static std::vector<std::pair<std::pair<size_t,size_t>,std::vector<std::vector<size_t> > > > inequivalent_paths_stored;
	public:
		/**
		 * Default constructor
		 */
		parenthood();
		/**
		 * Main constructor
		 *
		 * @param size_t representing the level of the daughter
		 * @param size_t representing the location in the given hierarchy level of the daughter
		 * @param pointer to the daughter cutF
		 * @param size_t representing the level of the parent
		 * @param size_t representing the location in the given hierarchy level of the parent
		 * @param pointer to the parent cutF
		 * @param pointer to the CutHierarchy containing the associated hierarchy
		 * @param size_t representing the corresponding 1-parent received
		 * @param size_t representing the nth-parent of the one parent received	(if there are other possible entries in one_parents, all will be filled by constructor)
		 */
		parenthood(size_t,size_t,cutF*,size_t,size_t,cutF*,CutHierarchy*,size_t,size_t);
                const auto& get_parent_location() const { return parent; }
                const auto& get_daughter_location() const { return daughter; }
	private:
		std::vector<std::pair<size_t,size_t> > get_inequivalent_paths(CutHierarchy*,cutF*,cutF*);	/**< given a CutHierarchy and a daughter and parent cut, it returns inequivalente paths in the format needed to fill the one_parents member vector of *this */
		/**
		 * Given # of props in parent and # of props needed to drop to reach daughter, this method checks if inequivalent paths were computed and added to
		 * 	the static inequivalent_paths_stored, and if not, computes them and add them
		 *
		 * @param size_t to represent the number of propagators present in the considered parent
		 * @param size_t to represent the number of propagator that we want to drop
		 */
		std::vector<std::vector<size_t> > get_paths(size_t,size_t);
};

namespace detail{
    struct ForestPostitionsContainer{
        std::vector<int> forest_ids; /**< only gluons, Ds-independend, vector for external states */
        std::vector<std::vector<int>> forest_ids_ds; /** (Ds-6) coefficients starting from power 1, inside: vector for external states */
    };
}

/**
 * Class for multi-loop cuts
 */
class cutF {
		friend class parenthood;
		friend class CutHierarchy;
		friend class cutF_min;
		friend class subtraction;
		friend bool operator==(const cutF&,const cutF&);
		friend std::ostream& operator<<(std::ostream&,const cutF&);
		template <class T,size_t D> friend class Builder;
		template <class T,size_t D,size_t Ds,typename> friend class Builder_fixed_Ds;
		friend void map_loop_mom_3rungs(std::pair<size_t,int>*,cutF*,cutF*,size_t,size_t,cutF*);
		friend std::pair<std::vector<int>,std::vector<int> > daughter_to_parent_loop_rule(cutF*,int,std::pair<size_t,int>,size_t);
		friend std::pair<size_t,int> get_propagator_location_of_lD_in_parent(const std::pair<std::vector<Particle*>,std::vector<Particle*>>&,size_t,size_t,cutF*);

		static size_t cut_counter;	/**< A static member to count number of existing cuts */

		RungF * cut_rungs;	/**< to store defining rungs, which always have empty top & bottom vertices. Corresponding info stored in the member array vertex_end[2]. General two loop case: 0 is left (maximum count of vertices); 1 is central (minimum count of vertices); 2 is right (next-to-max count) */
		size_t rung_count;	/**< Counts the number of rungs that *this has */
		Vertex vertex_end[2];	/**< potential vertices on RungF joints (0 is entry top, 1 is bottom). 1-loop cuts: unused */

		Process cut_process;	/**< process for cut */
                detail::ForestPostitionsContainer forest_positions;		/**< locations of cuts in a given Forest */
		Process tree_process;	/**< TEMPORARY (not the right place to store trees) */
		int tree_locate;	/**< Tracks location of associated tree in Forest */

		int cut_hierarchy_level;	/**< To store level in hierarchy associated to a given CutHierarchy */
		int cut_kin_locate;	/**< To store location of kinematic info in CutHierarchy */

		std::vector<std::string> info;	/**< Contains printable info of cut */
                void construct_info(); /**< Will build info array for *this */
		std::string name;	/**< Short string naming the cut (e.g. penta-box) */
		std::string name_id;	/**< Short id for the cut specifying the particle content of the cut rung by rung (e.g. "([{g1m}[1,0],{g2p}[1,0],{g3p}[1,0]],[],[{g5p}[1,0],{g4m}[1,0]])")  */

		CStructure * structure;	/**< reference to associated CStructure */
		bool owns_struct;	/**< Defines whether the member pointer 'structure' is owned by *this */

		size_t n_particles;	/**< external particle multiplicity */
		int * loop_mom_conf_location;	/**< loop momenta location in mom_conf */
		std::vector< std::tuple<Particle,int,int,int,int > > loop_particle_container;	/**< Vector of tuples each one containing: Particle, l1 location ,  ml1 location ,  l1 mom conf index , ml1 mom conf index */

		std::vector<std::vector<parenthood> > parent_cuts;	/**< vector of vectors of size_t's that is associated to parents. Will be filled by CutHierarchy. Order from parents, to parents of parents, etc */

		bool accepts_direct_fit;	/**< To identify if this cut has associated a product of trees (false for diagrams associated to subleading poles) */

		bool constructed_cutTopology{false};	/**< Track whether cutTopology has already been constructed or not */
		cutTopology ctopology;	/**< Contains the topological info of the cut which is passed to Forest::add_cut(.) */
		lGraph::xGraph graph;		/**< The xGraph associated to *this */
		void construct_graph();
		/**
		 * Contains the loop particles
		 * The format is l1, ml1, [l2, ml2] where the last two entries only
		 * appear for a two loop cut.
		 */
		std::vector<Particle> loop_parts;
	public:
		/**
		 * Default constructor (returns an 'empty' cut, with no rungs)
		 */
		cutF();
		/**
		 * Constructor from vector of rungs
		 *
		 * @param an std::vector of RungF's properly ordered
		 * @param a boolean to explicitly say if associated CStructure should be constructed (and then the pointer member 'structure' should be owned by *this)
		 */
		cutF(std::vector<RungF>,bool = true);
		/**
		 * Constructor for 1-loop cuts
		 *
		 * @param RungF corresponding to the 1-loop cut (assumed that vertices top and bottom are empty)
		 * @param a boolean to explicitly say if associated CStructure should be constructed (and then the pointer member 'structure' should be owned by *this)
		 */
		cutF(RungF,bool = true);
		/**
		 * Copy constructor
		 */
		cutF(const cutF&);
		/**
		 * Assignment operator
		 */
		cutF& operator=(const cutF&);
		/**
		 * Method to print to the standard output a representation of *this
		 */
		void show() const;
		/**
		 * Returns a short name for *this
		 */
		std::string get_short_name() const;
		/**
		 * Method to explicitly construct assocaited CStructure (will drop an error message if *this already owns the given pointer)
		 */
		void construct_local_cstructure();
		/**
		 * A method to add *this to a given Fores
		 *
		 * @param Forest reference
		 * @param size_t representing non-default reference vector for loop particles (defaulted to 0, that is, no reference)
                 * @param bool (defaulted to true) to control switch_to_planar_standard in forest.add_cut(.) call
                 * @param size_t representing the maximum Ds to be used in this calculation (defaults to 0, meaning Ds = 8 for 1-loop calculations and Ds = 10 for 2-loop)
		 */
		void add_to_forest(Forest&,size_t = 0, bool = true, size_t _max_Ds = 0);
		/**
		 * Contruct cut by one size_t steps counterclockwise rotation among rungs specified by input vector (CAREFUL! This is only for testing!)
		 *
		 *	it moves along top and bottom vertices in two-loop cuts
		 *
		 *	it DOES NOT rotate/change links in corresponding rungs
		 */
		cutF counterclockwise_rotate(size_t = 1,std::vector<size_t> = std::vector<size_t>());
		/**
		 * An enhanced operator== to check for 1-loop cuts considering cyclic permutations (rotation)
		 */
		bool check_1loop_equal(const cutF&) const;
		/**
		 * Method to construct a rotated 1-loop cut equivalent to *this
		 */
		cutF rotate_1loop(int) const;
		/**
		 * Counts number of propagators in cut
		 */
		size_t get_prop_count() const;
		/**
		 * Produces a daughter from a given propagator pinch
		 */
		cutF pinch(size_t) const;
		/**
		 * Produces a n-level daughter by dropping the specified set of propagators in the vector
		 */
		cutF multi_pinch(std::vector<size_t>) const;
		/**
		 * Method to check if *this contains a scaleless sub integral
		 */
		bool has_scaleless_int() const;
		/**
		 * In case we want to check if there is a bubble insertion in the diagram (like: --O-- )
		 */
		bool has_sub_bubble() const;
		/**
		 * To check if *this is "semi simple" in Harald's terminology
		 */
		bool is_semi_simple() const;
		/**
		 * Checks if *this and passed cutF are related by beeing equivalent double prop cuts
		 */
		bool is_doubled_prop_related(const cutF&) const;
		/**
		 * Destructor
		 */
		~cutF();
                /**
                 * This method gives (const ref) access to the member 'ctopology', which was created ONLY if *this was added to a Forest.
                 * FIXME: currently memory unsafe!
                 */
		const cutTopology& get_cutTopology() const;
                const lGraph::xGraph& get_graph() const;
                /**
                 * Produces a cutTopology associated to *this, and returns it. For internal (loop particles) references are used to Particles contained in 'loop_parts'
                 */
                cutTopology get_cutTopology_copy();
		int get_cut_locate() const;
		bool is_one_loop() const;
		bool is_two_loop() const;
                const auto& get_loop_particle_container() const { return loop_particle_container; }
                const auto& get_n_particles() const { return n_particles; }
                /**
                 * A method that for unambiguous cases will assign fermion line numbers to colored fermions in *this
                 */
                void trace_fermion_lines();
                bool has_massive_propagator() const;
                /**
                 * Gives the location of loop momenta in associated cutTopology of *this
                 *
                 * @return an int pair {l1, l2} describing position of loop-mom 1 (l1) and loop-mom 2 (l2)
                 */
                std::pair<int, int> get_loop_mom_location() const;
                const auto& get_forest_positions() const { return forest_positions; }
                const auto& get_parenthoods() const { return parent_cuts; }
	private:
		/**
		 * This method is used to fill the loop-particle container member loop_parts (and clear in the process loop_particle_container and set to false constructed_cutTopology)
		 *
		 * @param String that can be attached to the conventional name of the loop particles (usually empty, unless thread-safe calculation needed)
		 */
		void update_loop_momentum(std::string);
                /**
                 * Will search in cut_rungs if pointer is included and ask for its fermion_line_index
                 */
                fermion_line_index get_fermion_line_index(Particle*) const;
                /**
                 * Will search in cut_rungs if pointer is included and ask for its internal_index
                 */
                int get_internal_index(Particle*) const;
                /**
                 * Will deliver the fermion_line_index in the corresponding link, where the index runs from 0 to structure->partition.size() (inclusive)
                 */
                fermion_line_index get_link_fermion_line_index(size_t) const;
};


// Minimal 'exposed' class containing cutF info for cut_kin_engine
class cutF_min {
	public:
		size_t loop_count;
		// integer to track location of cut in associated Forest
		int cut_locate;
		// integer to track location of associated tree in Forest
		int tree_locate;

		// to store location of kinematic info in CutHierarchy
		int cut_hierarchy_level;
		int cut_kin_locate;

		// contains printable info of cut
		std::string name;

		// reference to associated CStructure
		std::vector<size_t> mom_indices;
		std::vector<size_t> partition;

		size_t n_particles;

		// vector of vectors of size_t's that is associated to parents. Will be filled by CutHierarchy
		// order from parents, to parents of parents, etc
		std::vector<std::vector<parenthood> > parent_cuts;
#if 0
		// info for subtraction of parents, they match order in parent_cuts
		std::vector<std::vector<subtraction> > container_subs;
#endif
		// to identify if this cut will be fitted
		bool accepts_direct_fit;

		size_t location_loop_1;
		size_t location_loop_2;

		cutF_min() = default;
		cutF_min(const cutF&);
};


bool operator==(const cutF&,const cutF&);

// a class container for multiple cuts organized according to inheritance
// it also holds kinematic information of the contained cuts (that is associated CStructure's and given map)
/**
 * This class generates a hierarchy of cuts from parent cuts.
 *
 * It provides methods to print all the cuts to the standard output and explore
 * the different cuts in the hierarchy.
 *
 * Furthermore the information of the cut hierarchy stored in this class is
 * used to built a so-called Forest which constructs the necessary currents
 * that are used to build the cuts and trees at an abstract level. The
 * contraction and the computation of the cuts using kinematic and numerical
 * input happens in the builder.
 */
class CutHierarchy {
		friend class parenthood;
	protected:
		/// Maximal cuts stored
		std::vector<std::vector<cutF> > cut_hierarchy;
		/// Number of loops of contained cuts
		size_t n_loops;
		/// Number of propagators in maximal cuts
		size_t n_max_props;
		/// Contains associated CStructures to object in hierarchy, organized by level in hierarchy
		std::vector<std::vector<CStructure> > structures;
		/// Corresponding cutF to CStructure map
		std::vector<std::vector<int> > map_cutindex_to_cstructure;
	public:
		CutHierarchy() = default;
		/**
		 * Built a CutHierarchy with a set of maximal cuts.
		 * @param  ivcF	Vector containing the maximal cuts from which the hierarchy
		 * 		   		will be constructed.
                 * @param size_t to define the depth of pinching to produce hierarchy (default to 0, meaning maximum depth allowed)
                 * @param std::vector<size_t> to filter bottom of hierarchy
		 */
		CutHierarchy(const std::vector<cutF>&, size_t = 0, std::vector<size_t> = {});

		/**
		 * Prints all the cuts in the hierarchy.
		 */
		void print_all_cuts() const;

		/**
		 * Allows to explore the different cuts in the hierarchy more closely.
		 *
		 * This is done interactively and the user can choose a specific cut
		 * in a specific level of the hierarchy to contemplate more closely.
		 */
		void explore_cut_inheritance() const;

		/**
		 * Return the complete hierarchy.
		 *
		 * @return Contains a vector with all the cuts per level in the hierarchy.
		 */
		const std::vector<std::vector<cutF> >& get_hierarchy() const;
                /**
                 * Produces a cutTopology associated to the given level and cut in the CutHierarchy, and returns it. For internal (loop particles) references are used to Particles contained in 'loop_parts'
                 *
                 * @param size_t for level
                 * @param size_t for cut
                 */
                cutTopology get_cutTopology_copy(size_t,size_t);

		/**
		 * Add all the (directly fittable) cutF's to the Forest.
		 *
		 * The Forest constructs the abstract currents that are needed to build
		 * trees and cuts.
		 * @param   f			A reference to the Forest that shall be grown for the
		 * 						hierarchy contained in *this.
		 * @param 	ref 		size_t representing a reference momentum for the loop particle
                 * @param bool (defaulted to true) to control switch_to_planar_standard in forest.add_cut(.) call
                 * @param size_t representing the maximum Ds to be used in this calculation (defaults to 0, meaning Ds = 8 for 1-loop calculations and Ds = 10 for 2-loop)
		 */
		void grow_forest(Forest&,size_t = 0,bool = true, size_t _max_Ds = 0);

		// utility to build maximal cuts from a set of parent cuts
		/**
		 * Obtain a set of maximal cuts from the parent cuts.
		 *
		 * For multiplicities up to 5 external particles the maximal cuts
		 * correspond the parent cuts at 1 Loop. For higher multiplicities
		 * the cutF's will be pinched until they correspond to pentagon
		 * topologies. Similarly at the 2-Loop level.
		 * @param   ivcF  The parent cuts.
		 * @return        The maximal cuts obtained from the parents by
		 * 				  pinching propagators.
		 */
		static std::vector<cutF> get_maximal_cuts(const std::vector<cutF>&);
                /**
                 * Given a level and cut in the hierarchy, it returns the corresponding cut_locate of the cutF
                 */
		int get_cut_locate(size_t,size_t) const;
	private:
		// this method takes back of cut hierarchy and if possible fills a new level of parents
		void fill_level_parents();
		// this method takes the back of the cut_hierarchy, checks for which 2-loop cuts are related by beeing doubled-propagator cuts, and returns corresponding pairs of indices
		/**
		 * Return the location of the cuts in the hierarchy that have doubled propagators.
		 *
		 * These cuts are not directly fittable and need to be treated separately. C.f.: <br>
		 * <a href="https://arxiv.org/abs/1703.05255">arXiv:1703.05255</a>  <br>
		 * <a href="https://arxiv.org/abs/1703.05273">arXiv:1703.05273</a>  <br>
		 *
		 * @return A vector with pairs of indices denoting the level and the location in this
		 * 		   level in the hierarchy of the doubled-propagator cuts.
		 */
		std::vector<std::pair<size_t,size_t> > get_doubled_prop_related_cuts();
		/// this method takes a vector of size_t pairs, corresponding to next-to back of cut_hierarchy, removes inheritance related to each "second" size_t from back
		/// 	DO NOTHING for 1-loop
		void remove_unresolved_1parents_from_hierarchy(const std::vector<std::pair<size_t,size_t> >&);
};

}



}
