/*
 * hierarchy_tools.cpp
 *
 * First created on 15.5.2019
 *
 * Implementation of routines for aiding building hierarchies
 *
*/

#include <iostream>
#include <algorithm>
#include <math.h>
#include <set>
#include <cassert>

#include "Core/Utilities.h"
#include "Core/Debug.h"
#include "Forest/Forest.h"
#include "AmpEng/hierarchy_tools.h"

namespace Caravel {
namespace AmpEng{

using couplings_t = std::map<std::string,int>;


couplings_t add_couplings(couplings_t ca,const couplings_t& cb);

// A tool for get_simple_maximal_cut(.) (always massless!)
Particle get_given_particle(std::string iString,Particle* pIn,Particle* pPrevious){
	if(pIn->get_flavor()==ParticleFlavor::gluon)
		return Particle(iString,ParticleType("gluon"),SingleState("default"),0,0);
	else
		return Particle(iString,pPrevious->get_type(),SingleState("default"),0,0);
}

cutF get_simple_1loop_parent_diagram(std::vector<Particle*> particles){
	// few checks on the particles passed:
	if(particles.size()<4){
		std::cout<<"ERROR: get_simple_1loop_parent_diagram(.) expected at least four particles -- returned empty cutF"<<std::endl;
		return cutF();
	}

	// the should be all "gluon" or a contigous ("q","qb") pair
	for(size_t ii=0;ii<particles.size();ii++){
		// if it is not a gluon
		if(particles[ii]->get_flavor()!=ParticleFlavor::gluon||particles[ii]->get_anti_flavor()!=ParticleFlavor::gluon){
			// check that it is a ("q","qb") pair (properly ordered)
			size_t next((ii+1)%particles.size());
			size_t previous(ii+particles.size());
			previous=(previous-1)%particles.size();
			if(particles[ii]->get_flavor()==ParticleFlavor::q&&particles[next]->get_flavor()!=ParticleFlavor::qb){
				std::cout<<"Found a pair of non gluons: ("<<particles[ii]->get_type()<<","<<particles[next]->get_type()
						<<") and it can only be (\"q\",\"qb\") -- returned empty cutF"<<std::endl;
				return cutF();
			}
			else if(particles[ii]->get_flavor()==ParticleFlavor::qb&&particles[previous]->get_flavor()!=ParticleFlavor::q){
				std::cout<<"Found an (inverted) pair of non gluons: ("<<particles[previous]->get_type()<<","
						<<particles[ii]->get_type()
						<<") and it can only be (\"q\",\"qb\") -- returned empty cutF"<<std::endl;
				return cutF();
			}
		}
	}

	// now build the cutF

	std::vector<std::tuple<Link,Vertex> > to_construct_rung;

	Vertex NoCharge; 

	size_t n(particles.size());
	size_t nm1(n-1);

	// Construct left rung: always particles go "up" in the rung
	for(int ii=0;ii<int(n);ii++){
		int previous((ii+nm1)%n);
		// pIi: particle Internal i
		Particle plink(get_given_particle("pI"+std::to_string(ii),particles[ii],particles[previous]));
		// Assuming always gs
		Vertex lV(particles[ii],{{"gs",1}});
		if(ii==0)
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
		else
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
	}
	int ifinal(nm1);
	Particle pfinal(get_given_particle("pI"+std::to_string(n),particles[0],particles[ifinal]));
	Link finallinkL(pfinal,link_end,1);
	RungF rung1loop(1,to_construct_rung,finallinkL,2, NoCharge, NoCharge);

        cutF toret(std::vector<RungF>( {rung1loop} ));
        toret.trace_fermion_lines();
        return toret;
}

cutF get_simple_2loop_maximal_cut(std::vector<Particle*> particles,int nLeft){
	// few checks on the particles passed:
	if(particles.size()<4){
		std::cout<<"ERROR: get_simple_2loop_maximal_cut(.) expected at least four particles -- returned empty cutF"<<std::endl;
		return cutF();
	}

	// the should be all "gluon" or a contigous ("q","qb") pair
	for(size_t ii=0;ii<particles.size();ii++){
		// if it is not a gluon
		if(particles[ii]->get_flavor()!=ParticleFlavor::gluon||particles[ii]->get_anti_flavor()!=ParticleFlavor::gluon){
			// check that it is a ("q","qb") pair (properly ordered)
			size_t next((ii+1)%particles.size());
			size_t previous(ii+particles.size());
			previous=(previous-1)%particles.size();
			if(particles[ii]->get_flavor()==ParticleFlavor::q&&particles[next]->get_flavor()!=ParticleFlavor::qb){
				std::cout<<"Found a pair of non gluons: ("<<particles[ii]->get_type()<<","<<particles[next]->get_type()
						<<") and it can only be (\"q\",\"qb\") -- returned empty cutF"<<std::endl;
				return cutF();
			}
			else if(particles[ii]->get_flavor()==ParticleFlavor::qb&&particles[previous]->get_flavor()!=ParticleFlavor::q){
				std::cout<<"Found an (inverted) pair of non gluons: ("<<particles[previous]->get_type()<<","
						<<particles[ii]->get_type()
						<<") and it can only be (\"q\",\"qb\") -- returned empty cutF"<<std::endl;
				return cutF();
			}
		}
	}

	// now build the cutF

	std::vector<std::tuple<Link,Vertex> > to_construct_rung;

	// Start with central rung (always a gluon)
	Particle centralg("Gcentral",ParticleType("gluon"), SingleState("default"),0,0);
	to_construct_rung = std::vector<std::tuple<Link, Vertex> >();
	Link centrallink(centralg,link_end,1);
	// We let the central rung carry the couplings on the top and bottom vertices
	Vertex topCharge({{"gs",1}}); 
	Vertex bottomCharge({{"gs",1}});
	RungF rungCenter(1,to_construct_rung,centrallink,2, topCharge, bottomCharge);

	// clean up
	to_construct_rung.clear();

	Vertex NoCharge; 

	size_t n(particles.size());
	size_t nm1(n-1);

	// Construct left rung: always particles go "up" in the rung
	for(int ii=0;ii<nLeft;ii++){
		int previous((ii+nm1)%n);
		// pLIi: particle Left Internal i
		Particle plink(get_given_particle("pLI"+std::to_string(ii),particles[ii],particles[previous]));
		// Assuming always gs
		Vertex lV(particles[ii],{{"gs",1}});
		if(ii==0)
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
		else
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
	}
	int iLfinal((nLeft+nm1)%n);
	Particle pLfinal(get_given_particle("pLI"+std::to_string(nLeft),particles[nLeft%n],particles[iLfinal]));
	Link finallinkL(pLfinal,link_end,1);
	RungF rungLeft(1,to_construct_rung,finallinkL,2, NoCharge, NoCharge);

	to_construct_rung.clear();

	// Construct right rung: always particles go "up" in the rung
	for(int ii=nm1;ii>=nLeft;ii--){
		int previous((ii+1)%n);
		// pRIi: particle Reft Internal i
		Particle plink(get_given_particle("pRI"+std::to_string(ii),particles[ii],particles[previous]));
		// Assuming always gs
		Vertex lV(particles[ii],{{"gs",1}});
		if(ii==int(nm1))
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
		else
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
	}
	if(nLeft<int(n)){
		int iRfinal((nLeft)%n);
		int iRfinalb((nLeft+nm1)%n);
		Particle pRfinal(get_given_particle("pRI"+std::to_string(nLeft),particles[iRfinalb],particles[iRfinal]));
		Link finallinkR(pRfinal,link_end,1);
		RungF rungRight(1,to_construct_rung,finallinkR,2, NoCharge, NoCharge);

		return cutF(std::vector<RungF>( {rungLeft, rungCenter, rungRight} ));
	}
	else{
		Particle pRfinal(get_given_particle("pRI0",particles[nm1],particles[0]));
		Link finallinkR(pRfinal,link_end,1);
		RungF rungRight(1,to_construct_rung,finallinkR,2, NoCharge, NoCharge);

		return cutF(std::vector<RungF>( {rungLeft, rungCenter, rungRight} ));
	}
}

std::vector<cutF> get_simple_1loop_2qng_photons_parent_diagram(std::vector<Particle*> colored,std::vector<Particle*> photons){
	// few checks on the particles passed:
	if(colored.size()+photons.size()<4){
		std::cout<<"ERROR: get_simple_1loop_2qng_photons_parent_diagram(.) expected at least four particles -- returned empty vector of cutF"<<std::endl;
		return std::vector<cutF>();
	}
	if(colored.size()<2){
		std::cout<<"ERROR: get_simple_1loop_2qng_photons_parent_diagram(.) should have at least two colored particles (the corresponding (q,qb) pair) -- returned empty vector of cutF"<<std::endl;
		return std::vector<cutF>();
	}

	// Check that colored is as expected (all gluons except last (q,qb) pair
	size_t ncolored(colored.size());
	if(colored[ncolored-2]->get_flavor()!=ParticleFlavor::q||colored[ncolored-1]->get_flavor()!=ParticleFlavor::qb){
		std::cout<<"ERROR: get_simple_1loop_2qng_photons_parent_diagram(.) last two particles should be (q,qb) pair -- received: ("
				<<colored[ncolored-2]->get_type()<<","<<colored[ncolored-1]->get_type()<<") -- returned empty vector of cutF"<<std::endl;
		return std::vector<cutF>();
	}
	
	// check that photons are all photons
	for(size_t ii=0;ii<photons.size();ii++){
		if(photons[ii]->get_flavor()!=ParticleFlavor::photon||photons[ii]->get_anti_flavor()!=ParticleFlavor::photon){
			std::cout<<"ERROR: get_simple_1loop_2qng_photons_parent_diagram(.) in photon container found: ("
					<<photons[ii]->get_type()<<") -- returned empty vector of cutF"<<std::endl;
			return std::vector<cutF>();
		}
	}

	std::vector<cutF> toret;

	do {
		// now build the cutF

		std::vector<std::tuple<Link,Vertex> > to_construct_rung;

		Vertex NoCharge; 

		// Construct rung up to finding quark (part of only gluons)
		for(int ii=0;ii<int(ncolored-1);ii++){
			// pIi: particle Internal i
			Particle plink("pI"+std::to_string(ii),ParticleType("gluon"),SingleState("default"),0,0);
			// Assuming always gs
			Vertex lV(colored[ii],{{"gs",1}});
			if(ii==0)
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
			else
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
		}

		// now attach the photons in the given order
		for(size_t ii=0;ii<photons.size();ii++){
			// always q
			Particle plink("pI"+std::to_string(ii+ncolored-1),ParticleType("q"),SingleState("default"),0,0);
			// Assuming always gw
			Vertex lV(photons[ii],{{"gs",1}});
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+ncolored),lV));
		}

		// Treat qb
		Particle plink("pI"+std::to_string(photons.size()+ncolored-1),ParticleType("q"),SingleState("default"),0,0);
		// Assuming always gs
		Vertex lV(colored.back(),{{"gs",1}});
		to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,photons.size()+ncolored),lV));

		// always a gluon
		Particle pfinal("pI"+std::to_string(photons.size()+ncolored),ParticleType("gluon"),SingleState("default"),0,0);
		Link finallinkL(pfinal,link_end,1);
		RungF rung1loop(1,to_construct_rung,finallinkL,2, NoCharge, NoCharge);

		toret.push_back(cutF(std::vector<RungF>( {rung1loop} )));
	} while (std::next_permutation(photons.begin(),photons.end()));

	return toret;
}

void simple_add_2loop_2qng_photons_parent_diagram(std::vector<cutF>& maximals,std::vector<Particle*> colored,std::vector<Particle*> photons,size_t nl){
	// sanity checks
	if(nl<=0){
		std::cout<<"ERROR: integer in simple_add_2loop_2qng_photons_parent_diagram(.) should be greater than 0, but received "<<nl<<" --- Did nothing!"<<std::endl;
		return;
	}

	// few checks on the particles passed:
	if(colored.size()+photons.size()<4){
		std::cout<<"ERROR: simple_add_2loop_2qng_photons_parent_diagram(.) expected at least four particles -- Did nothing!"<<std::endl;
		return;
	}
	if(colored.size()+photons.size()<nl){
		std::cout<<"ERROR: simple_add_2loop_2qng_photons_parent_diagram(.) received less particles ("<<colored.size()+photons.size()
				<<") than what is requested for the left rung ("<<nl<<") -- Did nothing!"<<std::endl;
		return;
	}
	if(colored.size()<2){
		std::cout<<"ERROR: simple_add_2loop_2qng_photons_parent_diagram(.) should have at least two colored particles (the corresponding (q,qb) pair) -- Did nothing!"<<std::endl;
		return;
	}
	// q should be first and qb last
	if(colored[0]->get_flavor()!=ParticleFlavor::q||colored[0]->get_anti_flavor()!=ParticleFlavor::qb){
		std::cout<<"ERROR: simple_add_2loop_2qng_photons_parent_diagram(.) should received first colored particle a \"q\" but received: ("
				<<colored[0]->get_type()<<","<<colored[0]->get_type()<<") -- Did nothing!"<<std::endl;
		return;
	}
	if(colored.back()->get_flavor()!=ParticleFlavor::qb||colored.back()->get_anti_flavor()!=ParticleFlavor::q){
		std::cout<<"ERROR: simple_add_2loop_2qng_photons_parent_diagram(.) should received last colored particle a \"qb\" but received: ("
				<<colored.back()->get_type()<<","<<colored.back()->get_type()<<") -- Did nothing!"<<std::endl;
		return;
	}

	// check that photons are all photons
	for(size_t ii=0;ii<photons.size();ii++){
		if(photons[ii]->get_flavor()!=ParticleFlavor::photon||photons[ii]->get_anti_flavor()!=ParticleFlavor::photon){
			std::cout<<"ERROR: simple_add_2loop_2qng_photons_parent_diagram(.) in photon container found: ("
					<<photons[ii]->get_type()<<") -- Did nothing!"<<std::endl;
			return;
		}
	}

	std::vector<cutF> lmaximals;

	size_t n(colored.size()+photons.size());

	std::vector<std::tuple<Link,Vertex> > to_construct_rung;
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	globally needed
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Start with central rung (always a gluon)
	Particle centralg("Gcentral",ParticleType("gluon"),SingleState("default"),0,0);
	to_construct_rung = std::vector<std::tuple<Link, Vertex> >();
	Link centrallink(centralg,link_end,1);
	// We let the central rung carry the couplings on the top and bottom vertices
	Vertex topCharge({{"gs",1}}); 
	Vertex bottomCharge({{"gs",1}});
	RungF rungCenter(1,to_construct_rung,centrallink,2, topCharge, bottomCharge);

	Vertex NoCharge; 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	

	// create corresponding maximals with all permutations of the photon orderings
	do{
	    // a container for all particles
	    std::vector<Particle*> lall(photons);
	    lall.insert(lall.end(),colored.begin(),colored.end());

	    for(size_t ii=0;ii<lall.size();ii++){
		// we loop over all possible combinations of the particles in the left
		
		std::vector<Particle*> leftps;
		for(size_t jj=ii;jj<ii+nl;jj++)
			leftps.push_back(lall[jj%n]);
		std::vector<Particle*> rightps;	
		for(size_t jj=ii+nl;jj<ii+n;jj++)
			rightps.push_back(lall[jj%n]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// now build the cutF
	
		// clean up
		to_construct_rung.clear();
	
		size_t pcounting(0);
	
		// Construct left rung: always particles go "up" in the rung
		for(auto& lp:leftps){
			Particle plink;
			std::vector<int> lc({1,0});
			if(lp->get_flavor()==ParticleFlavor::q)
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("qb"),SingleState("default"),0,0);
			else if(lp->get_flavor()==ParticleFlavor::photon){
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("qb"),SingleState("default"),0,0);
				lc={0,1};
			}
			else if(lp->get_flavor()==ParticleFlavor::qb||lp->get_flavor()==ParticleFlavor::gluon)
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
			else
				std::cout<<"ERROR: lp: "<<lp->get_type()<<std::endl;
			std::map<std::string,int> mc; mc["gs"]=lc[0]; mc["gw"]=lc[1];
			Vertex lV(lp,mc);
			if(pcounting==0)
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,pcounting+1),lV));
			else
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,pcounting+1),lV));
			pcounting++;
		}
		Particle pLfinal;
		if(leftps.back()->get_flavor()==ParticleFlavor::q||leftps.back()->get_flavor()==ParticleFlavor::gluon)
			pLfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
		else if(leftps.back()->get_flavor()==ParticleFlavor::photon||leftps.back()->get_flavor()==ParticleFlavor::qb)
			pLfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("qb"),SingleState("default"),0,0);
		else
			std::cout<<"ERROR: leftps.back(): "<<leftps.back()->get_type()<<std::endl;
		pcounting++;
		Link finallinkL(pLfinal,link_end,1);
		RungF rungLeft(1,to_construct_rung,finallinkL,2, NoCharge, NoCharge);
	
		to_construct_rung.clear();
	
		// Construct right rung: always particles go "up" in the rung
		for(size_t ii=rightps.size();ii>0;ii--){
			Particle plink;
			std::vector<int> lc({1,0});
			if(rightps[ii-1]->get_flavor()==ParticleFlavor::q||rightps[ii-1]->get_flavor()==ParticleFlavor::gluon)
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
			else if(rightps[ii-1]->get_flavor()==ParticleFlavor::photon){
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("q"),SingleState("default"),0,0);
				lc={0,1};
			}
			else if(rightps[ii-1]->get_flavor()==ParticleFlavor::qb)
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("q"),SingleState("default"),0,0);
			else
				std::cout<<"ERROR: right lp: "<<rightps[ii-1]->get_type()<<std::endl;
			std::map<std::string,int> mc; mc["gs"]=lc[0]; mc["gw"]=lc[1];
			Vertex lV(rightps[ii-1],mc);
			if(ii==rightps.size())
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
			else
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
		}
		Particle pRfinal;
		if(nl<n){
			if(rightps[0]->get_flavor()==ParticleFlavor::qb||rightps[0]->get_flavor()==ParticleFlavor::gluon)
				pRfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
			else if(rightps[0]->get_flavor()==ParticleFlavor::photon||rightps[0]->get_flavor()==ParticleFlavor::q)
				pRfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("q"),SingleState("default"),0,0);
			else
				std::cout<<"ERROR: rightps[0]: "<<rightps[0]->get_type()<<std::endl;
		}
		else{
			if(leftps.back()->get_flavor()==ParticleFlavor::q||leftps.back()->get_flavor()==ParticleFlavor::gluon)
				pRfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
			else if(leftps.back()->get_flavor()==ParticleFlavor::photon||leftps.back()->get_flavor()==ParticleFlavor::qb)
				pRfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("q"),SingleState("default"),0,0);
			else
				std::cout<<"ERROR: leftps.back(): "<<leftps.back()->get_type()<<std::endl;
		}
		Link finallinkR(pRfinal,link_end,1);
		RungF rungRight(1,to_construct_rung,finallinkR,2, NoCharge, NoCharge);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		cutF lcut(std::vector<RungF>( {rungLeft, rungCenter, rungRight} ));

		// before adding the cutF to lmaximals, check that it doesn't coincide with an existing one
		bool add(true);
		for(size_t ii=0;ii<lmaximals.size();ii++){
			if(lcut==lmaximals[ii]){
				add=false;
				break;
			}
		}
		if(add)
			lmaximals.push_back(lcut);
	    }
	} while (std::next_permutation(photons.begin(),photons.end()));

	maximals.insert(maximals.end(),lmaximals.begin(),lmaximals.end());
}

std::vector<cutF> get_simple_1loop_2qng_leptons_parent_diagram(std::vector<Particle*> colored,std::vector<std::pair<Particle*,Particle*>> leptons){
	// few checks on the particles passed:
	if(colored.size()+leptons.size()<3||leptons.size()==0){
		std::cout<<"ERROR: get_simple_1loop_2qng_leptons_parent_diagram(.) expected at least four particles (with at least a lepton pair) -- returned empty vector of cutF"<<std::endl;
		return std::vector<cutF>();
	}
	if(colored.size()<2){
		std::cout<<"ERROR: get_simple_1loop_2qng_leptons_parent_diagram(.) should have at least two colored particles (the corresponding (q,qb) pair) -- returned empty vector of cutF"<<std::endl;
		return std::vector<cutF>();
	}

	// Check that colored is as expected (all gluons except last (q,qb) pair)
	size_t ncolored(colored.size());
	if(colored[ncolored-2]->get_flavor()!=ParticleFlavor::q||colored[ncolored-1]->get_flavor()!=ParticleFlavor::qb){
		std::cout<<"ERROR: get_simple_1loop_2qng_leptons_parent_diagram(.) last two particles should be (q,qb) pair -- received: ("
				<<colored[ncolored-2]->get_type()<<","<<colored[ncolored-1]->get_type()<<") -- returned empty vector of cutF"<<std::endl;
		return std::vector<cutF>();
	}
	
	// check that leptons are all lepton pairs
	for(size_t ii=0;ii<leptons.size();ii++){
		if(
			// it is not a (l,lb) pair
			(leptons[ii].first->get_flavor()!=ParticleFlavor::l||leptons[ii].first->get_anti_flavor()!=ParticleFlavor::lb)||
			(leptons[ii].second->get_flavor()!=ParticleFlavor::lb||leptons[ii].second->get_anti_flavor()!=ParticleFlavor::l)
		  ){
			if(
				// it is not a (lb,l) pair
				(leptons[ii].first->get_flavor()!=ParticleFlavor::lb||leptons[ii].first->get_anti_flavor()!=ParticleFlavor::l)||
				(leptons[ii].second->get_flavor()!=ParticleFlavor::l||leptons[ii].second->get_anti_flavor()!=ParticleFlavor::lb)
			  ){
				std::cout<<"ERROR: get_simple_1loop_2qng_leptons_parent_diagram(.) in lepton pair container found: ("
						<<leptons[ii].first->get_type()<<","<<leptons[ii].second->get_type()<<") -- returned empty vector of cutF"<<std::endl;
				return std::vector<cutF>();
			}
		}
	}

	std::vector<cutF> toret;

	do {
		// now build the cutF

		std::vector<std::tuple<Link,Vertex> > to_construct_rung;

		Vertex NoCharge; 

		// Construct rung up to finding quark (part of only gluons)
		for(int ii=0;ii<int(ncolored-1);ii++){
			// pIi: particle Internal i
			Particle plink("pI"+std::to_string(ii),ParticleType("gluon"),SingleState("default"),0,0);
			// Assuming always gs
			Vertex lV(colored[ii],{{"gs",1}});
			if(ii==0)
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
			else
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
		}

		// now attach the lepton pairs in the given order
		for(size_t ii=0;ii<leptons.size();ii++){
			// always q
			Particle plink("pI"+std::to_string(ii+ncolored-1),ParticleType("q"),SingleState("default"),0,0);
			// Assuming always gw
			Vertex lV(std::vector<Particle*>({leptons[ii].first,leptons[ii].second}),{{"gw",2}});
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+ncolored),lV));
		}

		// Treat qb
		Particle plink("pI"+std::to_string(leptons.size()+ncolored-1),ParticleType("q"),SingleState("default"),0,0);
		// Assuming always gs
		Vertex lV(colored.back(),{{"gs",1}});
		to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,leptons.size()+ncolored),lV));

		// always a gluon
		Particle pfinal("pI"+std::to_string(leptons.size()+ncolored),ParticleType("gluon"),SingleState("default"),0,0);
		Link finallinkL(pfinal,link_end,1);
		RungF rung1loop(1,to_construct_rung,finallinkL,2, NoCharge, NoCharge);

		toret.push_back(cutF(std::vector<RungF>( {rung1loop} )));
	} while (std::next_permutation(leptons.begin(),leptons.end()));

	return toret;
}

void simple_add_2loop_2qng_leptons_parent_diagram(std::vector<cutF>& maximals,std::vector<Particle*> colored,std::vector<std::pair<Particle*,Particle*>> leptons,size_t nl){
	// sanity checks
	if(nl<=0){
		std::cout<<"ERROR: integer in simple_add_2loop_2qng_leptons_parent_diagram(.) should be greater than 0, but received "<<nl<<" --- Did nothing!"<<std::endl;
		return;
	}

	// few checks on the particles passed:
	if(colored.size()+leptons.size()<3||leptons.size()==0){
		std::cout<<"ERROR: simple_add_2loop_2qng_leptons_parent_diagram(.) expected at least four particles (with at least a lepton pair) -- Did nothing!"<<std::endl;
		return;
	}
	if(colored.size()+leptons.size()<nl){
		std::cout<<"ERROR: simple_add_2loop_2qng_leptons_parent_diagram(.) received less particles(/lepton pairs) ("<<colored.size()+leptons.size()
				<<") than what is requested for the left rung ("<<nl<<") -- Did nothing!"<<std::endl;
		return;
	}
	if(colored.size()<2){
		std::cout<<"ERROR: simple_add_2loop_2qng_leptons_parent_diagram(.) should have at least two colored particles (the corresponding (q,qb) pair) -- Did nothing!"<<std::endl;
		return;
	}
	// q should be first and qb last
	if(colored[0]->get_flavor()!=ParticleFlavor::q||colored[0]->get_anti_flavor()!=ParticleFlavor::qb){
		std::cout<<"ERROR: simple_add_2loop_2qng_leptons_parent_diagram(.) should received first colored particle a \"q\" but received: ("
				<<colored[0]->get_type()<<","<<colored[0]->get_type()<<") -- Did nothing!"<<std::endl;
		return;
	}
	if(colored.back()->get_flavor()!=ParticleFlavor::qb||colored.back()->get_anti_flavor()!=ParticleFlavor::q){
		std::cout<<"ERROR: simple_add_2loop_2qng_leptons_parent_diagram(.) should received last colored particle a \"qb\" but received: ("
				<<colored.back()->get_type()<<","<<colored.back()->get_type()<<") -- Did nothing!"<<std::endl;
		return;
	}

	// check that leptons are all lepton pairs
	for(size_t ii=0;ii<leptons.size();ii++){
		if(
			// it is not a (l,lb) pair
			(leptons[ii].first->get_flavor()!=ParticleFlavor::l||leptons[ii].first->get_anti_flavor()!=ParticleFlavor::lb)||
			(leptons[ii].second->get_flavor()!=ParticleFlavor::lb||leptons[ii].second->get_anti_flavor()!=ParticleFlavor::l)
		  ){
			if(
				// it is not a (lb,l) pair
				(leptons[ii].first->get_flavor()!=ParticleFlavor::lb||leptons[ii].first->get_anti_flavor()!=ParticleFlavor::l)||
				(leptons[ii].second->get_flavor()!=ParticleFlavor::l||leptons[ii].second->get_anti_flavor()!=ParticleFlavor::lb)
			  ){
				std::cout<<"ERROR: get_simple_1loop_2qng_leptons_parent_diagram(.) in lepton pair container found: ("
						<<leptons[ii].first->get_type()<<","<<leptons[ii].second->get_type()<<") -- Did nothing!"<<std::endl;
				return;
			}
		}
	}


	std::vector<cutF> lmaximals;

	size_t n(colored.size()+leptons.size());

	std::vector<std::tuple<Link,Vertex> > to_construct_rung;
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	globally needed
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Start with central rung (always a gluon)
	Particle centralg("Gcentral",ParticleType("gluon"),SingleState("default"),0,0);
	to_construct_rung = std::vector<std::tuple<Link, Vertex> >();
	Link centrallink(centralg,link_end,1);
	// We let the central rung carry the couplings on the top and bottom vertices
	Vertex topCharge({{"gs",1}}); 
	Vertex bottomCharge({{"gs",1}});
	RungF rungCenter(1,to_construct_rung,centrallink,2, topCharge, bottomCharge);

	Vertex NoCharge; 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	

	// create corresponding maximals with all permutations of the photon orderings
	do{
	    // a container for all particles
	    std::vector<Particle*> lall;
	    for(size_t kk=0;kk<leptons.size();kk++)
		lall.push_back(leptons[kk].first);
	    lall.insert(lall.end(),colored.begin(),colored.end());

	    for(size_t ii=0;ii<lall.size();ii++){
		// we loop over all possible combinations of the particles in the left
		
		std::vector<Particle*> leftps;
		for(size_t jj=ii;jj<ii+nl;jj++)
			leftps.push_back(lall[jj%n]);
		std::vector<Particle*> rightps;	
		for(size_t jj=ii+nl;jj<ii+n;jj++)
			rightps.push_back(lall[jj%n]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// now build the cutF
	
		// clean up
		to_construct_rung.clear();
	
		size_t pcounting(0);

		// to track the current lepton pair analized
		size_t currentlepton(0);
	
		// Construct left rung: always particles go "up" in the rung
		for(auto& lp:leftps){
			Particle plink;
			std::vector<int> lc({1,0});
			if(lp->get_flavor()==ParticleFlavor::q)
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("qb"),SingleState("default"),0,0);
			else if(lp->get_flavor()==ParticleFlavor::l||lp->get_flavor()==ParticleFlavor::lb){
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("qb"),SingleState("default"),0,0);
				lc={0,2};
			}
			else if(lp->get_flavor()==ParticleFlavor::qb||lp->get_flavor()==ParticleFlavor::gluon)
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
			else
				std::cout<<"ERROR: lp: "<<lp->get_type()<<std::endl;
			Vertex lV;
			std::map<std::string,int> mc; mc["gs"]=lc[0]; mc["gw"]=lc[1];
			if(lp->get_flavor()==ParticleFlavor::l||lp->get_flavor()==ParticleFlavor::lb){
				lV=Vertex(std::vector<Particle*>({lp,leptons[currentlepton].second}),mc);
				currentlepton++;
			}
			else
				lV=Vertex(lp,mc);
			if(pcounting==0)
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,pcounting+1),lV));
			else
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,pcounting+1),lV));
			pcounting++;
		}
		Particle pLfinal;
		if(leftps.back()->get_flavor()==ParticleFlavor::q||leftps.back()->get_flavor()==ParticleFlavor::gluon)
			pLfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
		else if(leftps.back()->get_flavor()==ParticleFlavor::l||leftps.back()->get_flavor()==ParticleFlavor::lb||leftps.back()->get_flavor()==ParticleFlavor::qb)
			pLfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("qb"),SingleState("default"),0,0);
		else
			std::cout<<"ERROR: leftps.back(): "<<leftps.back()->get_type()<<std::endl;
		pcounting++;
		Link finallinkL(pLfinal,link_end,1);
		RungF rungLeft(1,to_construct_rung,finallinkL,2, NoCharge, NoCharge);
	
		to_construct_rung.clear();
	
		// Construct right rung: always particles go "up" in the rung
		for(size_t ii=rightps.size();ii>0;ii--){
			Particle plink;
			std::vector<int> lc({1,0});
			if(rightps[ii-1]->get_flavor()==ParticleFlavor::q||rightps[ii-1]->get_flavor()==ParticleFlavor::gluon)
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
			else if(rightps[ii-1]->get_flavor()==ParticleFlavor::l||rightps[ii-1]->get_flavor()==ParticleFlavor::lb){
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("q"),SingleState("default"),0,0);
				lc={0,2};
			}
			else if(rightps[ii-1]->get_flavor()==ParticleFlavor::qb)
				plink=Particle("pLI"+std::to_string(pcounting),ParticleType("q"),SingleState("default"),0,0);
			else
				std::cout<<"ERROR: right lp: "<<rightps[ii-1]->get_type()<<std::endl;
			std::map<std::string,int> mc; mc["gs"]=lc[0]; mc["gw"]=lc[1];
			Vertex lV;
			if(rightps[ii-1]->get_flavor()==ParticleFlavor::l||rightps[ii-1]->get_flavor()==ParticleFlavor::lb){
				lV=Vertex(std::vector<Particle*>({rightps[ii-1],leptons[currentlepton].second}),mc);
				currentlepton++;
			}
			else
				lV=Vertex(rightps[ii-1],mc);
			if(ii==rightps.size())
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
			else
				to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
		}
		Particle pRfinal;
		if(nl<n){
			if(rightps[0]->get_flavor()==ParticleFlavor::qb||rightps[0]->get_flavor()==ParticleFlavor::gluon)
				pRfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
			else if(rightps[0]->get_flavor()==ParticleFlavor::l||rightps[0]->get_flavor()==ParticleFlavor::lb||rightps[0]->get_flavor()==ParticleFlavor::q)
				pRfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("q"),SingleState("default"),0,0);
			else
				std::cout<<"ERROR: rightps[0]: "<<rightps[0]->get_type()<<std::endl;
		}
		else{
			if(leftps.back()->get_flavor()==ParticleFlavor::q||leftps.back()->get_flavor()==ParticleFlavor::gluon)
				pRfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("gluon"),SingleState("default"),0,0);
			else if(leftps.back()->get_flavor()==ParticleFlavor::l||leftps.back()->get_flavor()==ParticleFlavor::lb||leftps.back()->get_flavor()==ParticleFlavor::qb)
				pRfinal=Particle("pLI"+std::to_string(pcounting),ParticleType("q"),SingleState("default"),0,0);
			else
				std::cout<<"ERROR: leftps.back(): "<<leftps.back()->get_type()<<std::endl;
		}
		Link finallinkR(pRfinal,link_end,1);
		RungF rungRight(1,to_construct_rung,finallinkR,2, NoCharge, NoCharge);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		cutF lcut(std::vector<RungF>( {rungLeft, rungCenter, rungRight} ));

		// before adding the cutF to lmaximals, check that it doesn't coincide with an existing one
		bool add(true);
		for(size_t ii=0;ii<lmaximals.size();ii++){
			if(lcut==lmaximals[ii]){
				add=false;
				break;
			}
		}
		if(add)
			lmaximals.push_back(lcut);
	    }
	} while (std::next_permutation(leptons.begin(),leptons.end()));

	maximals.insert(maximals.end(),lmaximals.begin(),lmaximals.end());
}

void simple_add_2loop_ng_nf_parent_diagrams(std::vector<cutF>& maximals,std::vector<Particle*> particles,size_t nLeft){
	// few checks on the particles passed:
	if(particles.size()<4){
		std::cout<<"ERROR: simple_add_2loop_ng_nf_parent_diagrams(.) expected at least four external particles -- Did nothing"<<std::endl;
		return;
	}

	// all particles should be gluons
	for(size_t ii=0;ii<particles.size();ii++){
		// if it is not a gluon
		if(particles[ii]->get_flavor()!=ParticleFlavor::gluon||particles[ii]->get_anti_flavor()!=ParticleFlavor::gluon){
			std::cout<<"In simple_add_2loop_ng_nf_parent_diagrams(.) found a non-gluon particle: ("<<particles[ii]->get_type()<<") --- Did nothing"<<std::endl;
			return;
		}
	}

	// now build the cutF

	std::vector<std::tuple<Link,Vertex> > to_construct_rung;

	// Start with central rung (always a q (from bottom to top))
	Particle centralq("Qcentral",ParticleType("q"),SingleState("default"),0,0);
	to_construct_rung = std::vector<std::tuple<Link, Vertex> >();
	Link centrallink(centralq,link_end,1);
	// We let the central rung carry the couplings on the top and bottom vertices
	Vertex topCharge({{"gs",1}}); 
	Vertex bottomCharge({{"gs",1}});
	RungF rungCenter(1,to_construct_rung,centrallink,2, topCharge, bottomCharge);

	Vertex NoCharge; 

	size_t n(particles.size());
	size_t nm1(n-1);

	std::vector<Particle*> rotate_particles(particles);
	
	// consider each rotation
	for(size_t rr=0;rr<n;rr++){
	    // don't rotate if it is first iteration
	    if(rr>0)
		std::rotate(rotate_particles.begin(),rotate_particles.begin()+1,rotate_particles.end());

	    // clean up
	    to_construct_rung.clear();

	    // Construct left rung: always a "qb" go "up" in the rung
	    for(int ii=0;ii<int(nLeft);ii++){
		// pLIi: particle Left Internal i
		Particle plink("pLI"+std::to_string(ii),ParticleType("qb"),SingleState("default"),0,0);
		// Assuming always gs
		Vertex lV(rotate_particles[ii],{{"gs",1}});
		if(ii==0)
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
		else
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
	    }
	    Particle pLfinal("pLI"+std::to_string(nLeft),ParticleType("qb"),SingleState("default"),0,0);
	    Link finallinkL(pLfinal,link_end,1);
	    RungF rungLeft(1,to_construct_rung,finallinkL,2, NoCharge, NoCharge);

	    to_construct_rung.clear();

	    // Construct right rung: always a "gluon" in the loop
	    for(int ii=nm1;ii>=int(nLeft);ii--){
		// pRIi: particle Reft Internal i
		Particle plink("pLI"+std::to_string(ii),ParticleType("gluon"),SingleState("default"),0,0);
		// Assuming always gs
		Vertex lV(rotate_particles[ii],{{"gs",1}});
		if(ii==int(nm1))
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_start,ii+1),lV));
		else
			to_construct_rung.push_back(std::make_tuple(Link(plink,link_internal,ii+1),lV));
	    }
	    Particle pRfinal("pLI"+std::to_string(nLeft),ParticleType("gluon"),SingleState("default"),0,0);
	    Link finallinkR(pRfinal,link_end,1);
	    RungF rungRight(1,to_construct_rung,finallinkR,2, NoCharge, NoCharge);

//cutF local(std::vector<RungF>( {rungLeft, rungCenter, rungRight} ));
//local.show();
	    maximals.push_back( cutF(std::vector<RungF>( {rungLeft, rungCenter, rungRight} )) );
	}
}


void assign_fermion_indices(const std::tuple<Link, Vertex>& tp,int& femion_internal,std::vector<std::pair<int,int>>& link_fermion_indices,std::vector<int>& external_fermion_indices){
    const Link& lL(std::get<0>(tp));
    const Vertex& lV(std::get<1>(tp));
    // only non-trivial if single particle
    if(lV.get_multiplicity()==1){
	const Particle& lP(*(lV.get_particles()[0]));
	if(lP.get_statistics()==ParticleStatistics::fermion||lP.get_statistics()==ParticleStatistics::antifermion){
	    // need to determine if fermion goes up or down
	    bool goes_up(false);
	    const Particle& up(lL.get_particle());
	    if(up.get_statistics()==ParticleStatistics::fermion||up.get_statistics()==ParticleStatistics::antifermion)
		goes_up=true;
	    if(goes_up){
	        link_fermion_indices.push_back(std::make_pair(femion_internal,0));
		external_fermion_indices.push_back(femion_internal);
		femion_internal++;
		return;
	    }
	    else{
	        link_fermion_indices.push_back(std::make_pair(0,femion_internal));
		external_fermion_indices.push_back(femion_internal);
		femion_internal++;
		return;
	    }
	}
    }

    // all conditions fail
    link_fermion_indices.push_back(std::make_pair(0,0));
    external_fermion_indices.push_back(0);
}

Particle* search_pointer_for_particle(const Particle& p, std::vector<std::vector<Particle>>& container){
    for(auto& vp: container){
        for(auto& lp: vp){
            if(lp == p){
                return &lp;
            }
        }
    }
    std::cerr<<"ERROR: search_pointer_for_particle(.) could not find particle "<<p<<" in container: "<<container<<std::endl;
    std::exit(1);
}

cutF get_single_1L_parent(const std::vector<std::vector<Particle>>& External, const std::vector<Particle>& Internal,
                          const std::vector<std::map<std::string, int>>& couplings, std::vector<std::vector<Particle>>& reference) {
    size_t nvertices = External.size();
    assert(nvertices > 0);
    assert( Internal.size() == nvertices);
    assert( couplings.size() == nvertices );
    assert( reference.size() == nvertices );

    // container of references
    std::vector<std::vector<Particle*>> pExternal;
    for (auto& vp : External) {
        pExternal.push_back(std::vector<Particle*>());
        for (auto& p : vp) { pExternal.back().push_back(search_pointer_for_particle(p, reference)); }
    }

    // vertices to be used to construct 'parent' one-loop diagram
    std::vector<Vertex> vertices;
    for (size_t ii = 0; ii < nvertices; ii++) { vertices.push_back(Vertex(pExternal[ii], couplings[ii])); }

    // to construct "by hand" the n-point parent diagram
    //	in the future, we should build the parent diagrams either from BG itself or from data files
    std::vector<std::tuple<Link, Vertex>> to_construct;
    to_construct.push_back(std::make_tuple(Link(Internal[0], link_start, 1), vertices[0]));
    for (size_t ii = 1; ii < nvertices; ii++) to_construct.push_back(std::make_tuple(Link(Internal[ii], link_internal, ii + 1), vertices[ii]));
    Link finallink(Internal[0], link_end, 1);

    // now construct the corresponding rung
    RungF r_Parent(1, to_construct, finallink, 1, Vertex(), Vertex());
    // r_Parent.show();

    // std::cout<<"####################################"<<std::endl;
    // std::cout<<"SHOWING Parent_cut: "<<std::endl;
    // std::cout<<"####################################"<<std::endl;
    // and then, the parent "cut"
    cutF Parent_cut(r_Parent);
    Parent_cut.trace_fermion_lines();
    // Parent_cut.show();

    return Parent_cut;
}

// FIXME: this logic is a legacy from older code, and it should be fixed!
bool check_if_photon_symmetrize(const std::vector<std::vector<Particle>>& External, std::vector<std::map<std::string, int>>& each_cps_parent) {
    bool toret(false);
    for (auto& cp : each_cps_parent)
        if (cp["gw"]) {
            toret = true;
            break;
        }
    // we found a gw coupling, check that all external colorless particles are photons
    if (toret) {
        for (auto& vps : External) {
            bool check(false);
            for (auto& p : vps) {
                if (!p.is_colored()) {
                    if (p.get_flavor() != ParticleFlavor::photon) {
                        check = true;
                        break;
                    }
                }
            }
            if (check) {
                toret = false;
                break;
            }
        }
    }
    return toret;
}

std::vector<cutF> get_1loop_parents(std::vector<std::vector<std::vector<Particle>>>& Externals, const std::vector<std::vector<Particle>>& Internals,
                          const std::vector<std::vector<std::map<std::string, int>>>& couplings){

    // same number of parents' information
    assert(Externals.size() == Internals.size());
    assert(Externals.size() == couplings.size());
    for(size_t ii=0; ii<Externals.size(); ii++){
        // check matching Externals/Internals information contained for each of the parents
        assert(Externals[ii].size() == Internals[ii].size());
        assert(Externals[ii].size() == couplings[ii].size());
    }
    auto first_parent_size = Externals[0].size();
    assert( first_parent_size > 0 );
    for(size_t ii=1; ii<Externals.size(); ii++)
        // check that all parents have same amount of external legs
        assert(first_parent_size == Externals[ii].size());

    std::vector<cutF> theParents;

    // a loop over each parent
    for (size_t pp = 0; pp < Externals.size(); pp++) {
        auto& External = Externals[pp];
        auto& Internal = Internals[pp];
        auto each_cps_parent = couplings[pp];

        bool photonSymmetrize = check_if_photon_symmetrize(External, each_cps_parent);
        if (photonSymmetrize) {
            std::vector<size_t> colored, colorless, putBefore;
            for (size_t i = 0; i < External.size(); ++i)
                if (External[i][0].is_colored()) {
                    colored.push_back(i);
                    if (in_container<ParticleFlavor::q>(Internal[i % Internal.size()].get_flavor()) ||
                        in_container<ParticleFlavor::qb>(Internal[i % Internal.size()].get_flavor()))
                        putBefore.push_back(i);
                }
                else
                    colorless.push_back(i);

            for (size_t i = 0; i < std::pow(putBefore.size(), colorless.size()); ++i) {
                std::vector<size_t> sums(putBefore.size(), 0);
                for (size_t buf = i, j = 0; j < colorless.size(); buf /= putBefore.size(), ++j) sums[buf % putBefore.size()]++;
                size_t iPerm = 0;
                std::vector<Particle> partCombsIn;
                do {
                    std::vector<size_t> partCombs;
                    size_t kLast = 0;
                    for (size_t j = 0; j < colored.size(); ++j) {
                        auto pbIt = std::find(putBefore.begin(), putBefore.end(), colored[j]);
                        size_t k;
                        for (k = kLast; k <= (pbIt == putBefore.end() ? 0 : sums[pbIt - putBefore.begin()]) + kLast; ++k) {
                            if (pbIt != putBefore.end() && k < sums[pbIt - putBefore.begin()] + kLast)
                                partCombs.push_back(colorless[k]);
                            else
                                partCombs.push_back(colored[j]);
                            if (!iPerm)
                                partCombsIn.push_back(Particle(std::string(wise_enum::to_string(Internal[colored[j]].get_flavor())),
                                                               Internal[colored[j]].get_type(), SingleState("default"), partCombs.size(), 0));
                        }
                        kLast = k - 1;
                    }
                    ++iPerm;

                    std::vector<std::vector<Particle>> lExternal;
                    std::vector<std::map<std::string, int>> lcouplings;
                    for (size_t ii = 0; ii < External.size(); ii++){
                        lExternal.push_back(External[partCombs[ii]]);
                        lcouplings.push_back(each_cps_parent[partCombs[ii]]);
                    }
                    
                    theParents.push_back( get_single_1L_parent(lExternal, partCombsIn, lcouplings, Externals[0]) );
                } while (std::next_permutation(colorless.begin(), colorless.end()));
            }
        }
        else {

            // add this permutation
            theParents.push_back( get_single_1L_parent(External, Internal, each_cps_parent, Externals[0] ) );

            // a quick check to see if this is a gravity computation
            bool allgravitons(true);
            for (auto& vpE : External) {
                for (auto& pE : vpE) {
                    if (pE.get_type() != ParticleType("G")) {
                        allgravitons = false;
                        break;
                    }
                }
                if (!allgravitons) break;
            }
            for (auto& pI : Internal) {
                if (pI.get_type() != ParticleType("G")) {
                    allgravitons = false;
                    break;
                }
            }
            if (allgravitons) {
                std::vector<size_t> indices;
                for (size_t tt = 1; tt < first_parent_size; tt++) indices.push_back(tt);
                size_t counter(0);
                // all particles involved are gravitons, now we symmetrize
                do {
                    counter++;
                    if (counter == 1)
                        // skip first (identity) permutation
                        continue;

                    std::vector<std::vector<Particle>> lExternal;
                    lExternal.push_back(External[0]);
                    for (size_t ii = 0; ii < indices.size(); ii++)
                        lExternal.push_back(External[indices[ii]]);
                    
                    // add this permutation
                    theParents.push_back( get_single_1L_parent(lExternal, Internal, each_cps_parent, Externals[0]) );
                } while (std::next_permutation(indices.begin(), indices.end()));
            }
        }
    }

    // delete possible duplicates
    std::vector<cutF> final_parents;
    for(auto p : theParents){
        if ( std::find(final_parents.begin(),final_parents.end(),p) == final_parents.end()) {
            final_parents.push_back(std::move(p));
        }
    }

    return final_parents;
}



cutF get_2loop_maximal_cut(std::vector<Particle*> external, const std::vector<Particle>& internal, int nleft, int nright,
                           const std::vector<couplings_t>& couplings) {
    // few sanity checks
    if(external.size()<4){
        std::cerr<<"ERROR: get_2loop_maximal_cut(.) is requiring at least 4 external particles!"<<std::endl;
        std::exit(1);
    }
    if (external.size() + 3 != internal.size()) {
        std::cerr << "ERROR: get_2loop_maximal_cut(.) received " << external.size() << " external particles and " << internal.size()
                  << " internal ones when it should be " << external.size() + 3 << std::endl;
        std::exit(1);
    }
    if (external.size() +2 != couplings.size()) {
        std::cerr << "ERROR: get_2loop_maximal_cut(.) received " << external.size() << " external particles and " << couplings.size()
                  << " couplings when it should be " << external.size() + 2 << std::endl;
        std::exit(1);
    }
    if(int(external.size())<nleft+nright){
        std::cerr << "ERROR: get_2loop_maximal_cut(.) received " << external.size() << " external particles with a request of nleft= " << nleft
                  << " and nright= " << nright << std::endl;
        std::exit(1);
    }
    int ncentral=int(external.size())-nleft-nright;
    if(nleft<nright||nright<ncentral){
        std::cerr << "ERROR: get_2loop_maximal_cut(.) requires standard organization of particles in rungs, with nleft>=nright>=ncenter, received (" << nleft
                  << "," << nright<< "," <<ncentral<<")"<< std::endl;
        std::exit(1);
    }

    // few helpful variables
    std::vector<std::tuple<Link,Vertex>> to_construct_rung;
    Vertex NoCharge;
    int counter(0);

    // construct left rung
    for (int ii = 0; ii < nleft; ii++) {
        counter++;
        const Particle& plink(internal[ii]);
        Vertex lV(external[ii], couplings[ii]);
        if (ii == 0)
            to_construct_rung.push_back(std::make_tuple(Link(plink, link_start, counter), lV));
        else
            to_construct_rung.push_back(std::make_tuple(Link(plink, link_internal, counter), lV));
    }
    const Particle& pLfinal(internal[counter]);
    counter++;
    Link finallinkL(pLfinal,link_end,counter);

    // build left rung
    RungF rungLeft(1, to_construct_rung, finallinkL, 2, NoCharge, NoCharge);

    // construct right rung
    to_construct_rung.clear();
    for(int ii=nleft+nright-1;ii>=nleft;ii--){
        counter++;
        const Particle& plink(internal[ii+2]);
        Vertex lV(external[ii], couplings[ii]);
        if (ii == nleft+nright-1)
            to_construct_rung.push_back(std::make_tuple(Link(plink, link_start, counter), lV));
        else
            to_construct_rung.push_back(std::make_tuple(Link(plink, link_internal, counter), lV));
    }
    const Particle& pRfinal(internal[nleft+1]);
    counter++;
    Link finallinkR(pRfinal,link_end,counter);

    // build left rung
    RungF rungRight(1, to_construct_rung, finallinkR, 2, NoCharge, NoCharge);

    // construct central rung
    to_construct_rung.clear();
    for(int ii=nleft+nright;ii<nleft+nright+ncentral;ii++){
        counter++;
        const Particle& plink(internal[ii+2]);
        Vertex lV(external[ii], couplings[ii+1]);
        if (ii == nleft+nright)
            to_construct_rung.push_back(std::make_tuple(Link(plink, link_start, counter), lV));
        else
            to_construct_rung.push_back(std::make_tuple(Link(plink, link_internal, counter), lV));
    }
    const Particle& pCfinal(internal[nleft+nright+ncentral+2]);
    counter++;
    Link finallinkC(pCfinal,link_end,counter);

    Vertex topCharge(couplings[nleft+nright]);
    Vertex bottomCharge(couplings[nleft+nright+ncentral+1]);

    // build left rung
    RungF rungCenter(1, to_construct_rung, finallinkC, 2, topCharge, bottomCharge);

    return cutF(std::vector<RungF>( {rungLeft, rungCenter, rungRight} ));
}
}
}
