/**
 * @file CutEquation.h
 *
 * @date 3/29/2021
 *
 * @brief Tools for solving cut equations
 *
*/
#pragma once

#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "Core/Debug.h"
#include "Core/momD_conf.h"
#include "Core/cutTopology.h"
#include "Core/Vec.h"
#include "Core/type_traits_extra.h"
#include "Graph/Graph.h"
#include "Graph/GraphKin.h"
#include "Graph/IntegralGraph.h"
#include "Graph/Utilities.h"
#include "OnShellStrategies/OnShellStrategies.h"
#include "OnShellStrategies/osm.h"
#include "FunctionSpace/FunctionSpace.h"
#include "Forest/Forest.h"
#include "Forest/Builder.h"
#include "AmplitudeCoefficients/NumericalTools.h"

#include "AmpEng/Diagrammatica.h"
#include "AmpEng/Parenthood.h"
#include "AmpEng/Utilities.h"

#include "AmpEng/cutF.h"

namespace Caravel {
namespace AmpEng {

using ::Caravel::FunctionSpace::FunctionBasis;
using ::Caravel::lGraph::xGraph;
using ::Caravel::lGraph::lGraphKin::GraphKin;
using ::Caravel::lGraph::lGraphKin::LoopOnShellChecker;

template <typename T> using CoeffType = Vec<T>;

template <typename T, size_t D> class HierarchyEngine;
template <typename T, size_t D> class Diagram;

template <typename T, size_t D> class FactChannel {
    friend class HierarchyEngine<T, D>;
    friend class Diagram<T, D>;
    template <typename Tl, size_t Dl> using OnShellMapper = std::function<OnShellPoint<Tl, Dl>(const OnShellPoint<Tl, Dl>&, const momD_conf<Tl, Dl>&)>;
    template <typename Tl, size_t Dl> using PropagatorProduct = std::function<Tl(const OnShellPoint<Tl, Dl>&, const GraphKin<Tl, Dl>&)>;
    
    OnShellMapper<T, D> lPfromlD; /**< Get parent loop momenta from daughter loop momentum */
    PropagatorProduct<T, D> propagatorprod; /**< Computes product of propagators */

  public:
    FactChannel() = default;
    FactChannel(const Registro&);
    /**
     * Produce parent momenta from daughter's
     */
    OnShellPoint<T, D> lP(const OnShellPoint<T, D>&, const GraphKin<T, D>&) const;
    /**
     * From an OnShellPoint (the parent's momenta) returns the product of dropped propagators of 
     * this factorization channel
     */
    T operator()(const OnShellPoint<T, D>&, const GraphKin<T, D>&) const;
};

template <typename T, size_t D> class CutEquationComponents;

/**
 * Represents rhs (integrand = cut - subtraction) cut equation component for specific ascendant of a descendant
 */
template <typename T, size_t D> class CutEquationComponent {
    friend class HierarchyEngine<T, D>;
    friend class Diagram<T, D>;
    friend class CutEquationComponents<T, D>;
    std::pair<size_t, size_t> parent; /**< Location in hierarchy of parent (level, n_diagram) */
    std::vector<FactChannel<T, D>> individual; /**< Each of the factorization channels between parent-daughter */

    size_t Size;

  public:
    CutEquationComponent() = default;
    CutEquationComponent(const ArbolEntry&, size_t size);
    /**
     * Sums over all factorization channels subtraction contributions from this parent
     *
     * @param loop momenta (from daughter) employed
     * @param kinematical configuration (external momenta, masses, etc)
     * @param epsilon value
     * @param diagram in hierarchy of the parent
     */
    CoeffType<T> operator()(const OnShellPoint<T, D>&, const GraphKin<T, D>&, const T&, const Diagram<T, D>&) const;
};

/**
 * Represents lhs (integrand = cut - subtraction) cut equation component for specific ascendant of a descendant
 */
template <typename T, size_t D> class CutEquationContributor {
    friend class HierarchyEngine<T, D>;
    friend class Diagram<T, D>;
    friend class CutEquationComponents<T, D>;
    std::pair<size_t, size_t> parent; /**< Location in hierarchy of parent (level, n_diagram) */
    std::vector<FactChannel<T, D>> individual; /**< Each of the factorization channels between parent-daughter */

  public:
    CutEquationContributor() = default;
    CutEquationContributor(const ArbolEntry&);
    /**
     * Evaluate all functions from this parent summed over all factorization channels
     *
     * @param loop momenta (from daughter) employed
     * @param kinematical configuration (external momenta, masses, etc)
     * @param epsilon value
     * @param diagram in hierarchy of the parent
     */
    std::vector<T> operator()(const OnShellPoint<T, D>&, const GraphKin<T, D>&, const T&, const Diagram<T, D>&) const;
};

template <typename T, size_t D> class CutEquationComponents {
    friend class HierarchyEngine<T, D>;
    friend class Diagram<T, D>;
    std::vector<std::vector<CutEquationComponent<T, D>>> individual; /**< Individual subtractions, one for each resolved parent */
    std::vector<std::vector<CutEquationContributor<T, D>>> needed; /**< Individual lhs components, one for each unresolved parent */

    size_t Size;

  public:
    CutEquationComponents() = default;
    CutEquationComponents(std::vector<std::pair<size_t,size_t>>&, const Arbol&, size_t size);
    /**
     * Sums over all (resolved) parents contributions from different factorization channels
     *
     * @param loop momenta (from daughter) employed
     * @param kinematical configuration (external momenta, masses, etc)
     * @param epsilon value
     * @param access to hierarchy to evaluate integrands of parents
     */
    CoeffType<T> operator()(const OnShellPoint<T, D>&, const GraphKin<T, D>&, const T&, const std::vector<std::vector<Diagram<T, D>>>&) const;
    /**
     * Collects all (unresolved) ascendant functions from different factorization channels
     *
     * @param loop momenta (from daughter) employed
     * @param kinematical configuration (external momenta, masses, etc)
     * @param epsilon value
     * @param access to hierarchy to evaluate integrands of parents
     */
    std::vector<T> asc_functions(const OnShellPoint<T, D>&, const GraphKin<T, D>&, const T&, const std::vector<std::vector<Diagram<T, D>>>&) const;
};

template <typename T, size_t D> class Diagram {
    friend class HierarchyEngine<T, D>;
    friend class CutEquationContributor<T, D>;
    template <typename Tl, size_t Dl> using OnShellMapper = std::function<OnShellPoint<Tl, Dl>(const OnShellPoint<Tl, Dl>&,const momD_conf<Tl, Dl>&)>;
    using BasisTruncation = std::vector<size_t>;
    using OnShellChecker = std::function<size_t(const OnShellPoint<T, D>&, const momD_conf<T, D>&)>;

    xGraph xgraph; /**< Graph representation of this Diagram */
    xGraph xgraphrouting; /**< Graph representation of this Diagram with momenta routing for builder */
    std::pair<size_t, size_t> hier_locate; /**< Location in associated hierarchy */
    Arbol arbol; /**< Stores generation information associated to *this */
    cutTopology ctopology; /**< Store this main input to communicate with a given Forest */
    std::vector<LoopMomLocator> loop_particle_container;	/**< Vector of loop particles contained in *this */
    std::vector<int> locations; /**< Represents the indices associated to this Diagram in Forest (default, empty vector, unassigned) */
    GraphKin<T, D> graphkin; /**< For communicating to functions */

    OnShellMomentumParameterization<T, D> get_momenta; /**< On-shell phase space parametrization as a functor with OnShellCoordinates<T> argument */
    OnShellMapper<T, D> OSmom4builder; /**< Takes OS momenta and transforms it to S-tree routing needed for Forest/Builder evaluation */
    FunctionBasis<T, D> functions; /**< Functions parametrizing integrand FIXME: make more flexible choice of parametrization using 'settings' */
    CutEquationComponents<T, D> subtraction; /**< Contains all information for evaluating ascendant integrands and building of cut equations */
    bool always_recompute_external;	/**< Tracks whether to always recompute external currents when computing coefficients */

    std::vector<size_t> n_OS_vars; /**< Total number of OS variables needed for sampling */
    std::unordered_map<T, std::vector<CoeffType<T>>> coeffs; /**< Stores the coefficients of the FunctionBasis for a given value of ep */
    size_t basis_dim{0}; /**< The dimension of the FunctionSpace */
    BasisTruncation basis_truncation; /**< Saved warmup information for Truncation of the Basis */
    bool needs_warmup{true}; 
    std::vector<lGraph::IntegralGraph> graphs; /**< The master integrals */

    cutF* temp_reference{nullptr}; /**< In case the diagram has associated a cutF instance, we keep it temporarily for accessing cutF::add_to_forest(.) */
    std::vector<size_t> Dss{}; /**< values of Ds to evaluate cuts */

    std::unordered_map<size_t, std::vector<std::vector<T>>> variable_values;   /**< Store variables employed to cover on-shell phase space */
    std::unordered_map<OnShellPoint<T, D>, CoeffType<T>> result_cache;   /**< For given loop-momenta, it stores computed value according to compute_onshell_integrand(.) */

    T factor{T(1)}; /**< Overall factor to be applied to diagram master coefficients (e.g. symmetry factor). OJO: this is only applied so far when calling get_master_coeffs(.), meaning we suppose factor is included "after integration" */
    OnShellChecker checker;
    OnShellChecker checkerBuilder;
    bool enable_onshell_integrand{true}; /**< Controls evaluation of on-shell integrand. A subleading pole diagram should have it set to 'false' */

    bool representative_diag; /**< It should only be 'false' for those sub^{n}-leading pole diagrams which are not used as representative integrand for corresponding family */
    // FIXME: this is temporary while we do some 2-loop tests (more structure should be added later)
    SubleadPoleTerm singularity; /**< FIXME: first crude tracking of singular terms associated with *this */
  public:
    Diagram() = default;
    /**
     * Diagram in a hierarchy
     *
     * @param associated xGraph
     * @param Receives parenthood information
     * @param Information of particles from (global) external particles
     * @param Whether to recompute external currents when evaluating on-shell integrand
     * @param vector of Ds values
     * @param pair with info of location in associated hierarchy
     * @param array to track resolved/unresolved diagram & parents in subtraction
     */
    Diagram(const lGraph::xGraph&, const Arbol&, const GraphCutMap& gcm, bool, const std::vector<size_t>&, const std::pair<size_t, size_t>&, std::vector<std::pair<size_t,size_t>>&, bool, const SubleadPoleTerm&);
    /**
     * Diagram in a hierarchy
     *
     * @param associated xGraph
     * @param Receives parenthood information
     * @param associated cutF
     * @param Whether to recompute external currents when evaluating on-shell integrand
     * @param vector of Ds values
     * @param pair with info of location in associated hierarchy
     * @param array to track resolved/unresolved diagram & parents in subtraction
     * @param bool to define the member 'representative_diag'
     * @param SubleadPoleTerm to define the member 'singularity'
     */
    Diagram(const lGraph::xGraph&, const Arbol&, cutF&, bool, const std::vector<size_t>& Ds_vals, const std::pair<size_t, size_t>&, std::vector<std::pair<size_t,size_t>>&, bool, const SubleadPoleTerm&);
    void add_location_index(int);
    void add_to_forest(std::unique_ptr<Forest>& forest, size_t _max_Ds);

    // TODO: not const because of caching (maybe return a constant reference after caching in place!)
    //std::vector<T> get_coefficients(const T& epsilon, const momD_conf<Tl, Dl>& external_momenta);

    // FIXME: not allowing multiple 'epsilon' values at once skips FunctionBasis corresponding optimization
    /**
     * Contraction of coefficients with functions (evaluated in its loop momenta) for the given
     * value of epsilon (this is the off-shell integrand)
     */
     CoeffType<T> operator()(const OnShellPoint<T, D>& momenta, const T& epsilon) const;
    /**
     * Allows evaluation of all functions in a given OnShellPoint and epsilon
     */
     std::vector<T> eval_functions(const OnShellPoint<T, D>& momenta, const T& epsilon) const;

    /**
     * For a given on-shell point (with corresponding external momenta implicit in builder) 
     * computes associated cut/integrand
     */
    CoeffType<T> compute_onshell_integrand(const OnShellPoint<T, D>&, std::shared_ptr<Builder<T, D>>&);

    /**
     * Samples on-shell phase space to fix all coefficients of this
     */
    void compute_coefficients(const momD_conf<T, D>&, const T&, std::shared_ptr<Builder<T, D>>&, std::vector<std::vector<Diagram<T, D>>>&);

    /**
     * Returns master integral coefficients
     *
     * @param epsilon value
     */
    std::vector<CoeffType<T>> get_master_coeffs(const T&) const;

    /**
     * Returns number of master coefficients
     */
    size_t get_n_masters() const { return functions.get_n_masters(); };

    /**
     * Returns IntegralGraphs for all masters
     */
    std::vector<lGraph::IntegralGraph> get_integral_graphs() const { return graphs; };
    /**
     * Returns the Basis Truncation
     */
    BasisTruncation get_basis_truncation() const { return basis_truncation; };

    /**
     * Sets corresponding cut equation. If boolean 'false' then cut equation emptied
     */
    void set_cut_equation(bool);
    bool has_cut_equation() const;
    std::pair<size_t, size_t> get_hier_location() const;

    static thread_local std::unordered_map<std::pair<size_t,size_t>, bool> omitted_diagrams; /**< Track diagrams that should be ommited in a given hierarchy TODO: better handling! */

    void set_coefficients(const T&, std::vector<CoeffType<T>>&);
  private:
    /**
     * Constructs 'functions', 'get_momenta', 'OSmom4builder, 'subtraction'
     *
     * @param lets pass a truncation (from a warmup) for using in method update_functions() (defaulted to empty)
     */
    void assemble(const BasisTruncation& truncation = BasisTruncation());
    void update_functions(const BasisTruncation& truncation = BasisTruncation());
    /**
     * Takes care of producing 'OSmom4builder'
     */
    void produce_OS_mapper_for_builder();
    void build_subtraction(std::vector<std::pair<size_t,size_t>>&, const Arbol&);
    void build_subtraction(std::vector<std::pair<size_t,size_t>>&);
    void construct_integral_graphs();
    /**
     * This method handles (access to) the map 'variable_values'
     */
    std::vector<std::vector<T>> get_variables(size_t);
    void clear_cache();
    void check_factor();
};

/**
 * (abstract) base class template for multi-loop (non D-dependent) HierarchyEngine
 */

template <typename T> class HierarchyEngineBase {
    using BasisTruncation = std::vector<size_t>;
    using HierarchyBasisTruncation = std::vector<std::vector<BasisTruncation>>;

  public:
    /**
     * Gives access to related Genealogy
     */
    virtual const Genealogy& get_generations() const = 0;

    /**
     * Evaluate hierarchy for given external momenta and epsilon value
     */
    virtual void eval(const momD_conf<T, 4>&, const T&) = 0;
    /**
     * Evaluates hierarchy for given diagram and all its parents
     */
    virtual void eval_diagram(const momD_conf<T, 4>&, const T&, const std::pair<size_t, size_t>&) = 0;
    /**
     * Returns master integral coefficients
     *
     * @param epsilon value
     */
    virtual std::vector<std::vector<std::vector<CoeffType<T>>>> get_master_coeffs(const T&) const = 0;
    /**
     * Returns master integral coefficients of a given diagram
     *
     * @param epsilon value
     * @param pair: {level in hierarchy, diagram}
     */
    virtual std::vector<CoeffType<T>> get_diagram_master_coeffs(const T&, const std::pair<size_t, size_t>&) const = 0;
    /**
     * Truncate the function basis using warmup info
     *
     * @param: array of BasisTruncation for passing warmup information to Diagrams (defaults to empty)
     */
    virtual void truncate_basis(const std::vector<std::vector<BasisTruncation>>& = std::vector<std::vector<BasisTruncation>>()) = 0;
    /**
     * Returns IntegralGraphs for all masters
     */
    virtual std::vector<lGraph::IntegralGraph> get_integral_graphs() const = 0;
    /**
     * Returns IntegralGraphs for all masters in a given entry in the Hierarchy
     */ 
    virtual std::vector<lGraph::IntegralGraph> get_diagram_integral_graphs(const std::pair<size_t, size_t>&) const = 0;
    /**
     * Returns the Basis Truncation
     */
    virtual HierarchyBasisTruncation get_basis_truncation() const = 0;

    /**
     * Returns vector of integers representing Ds values in which the underlying hierarchy is evaluated
     */
    virtual const std::vector<size_t>& get_Ds_values() const = 0;

    /**
     * Will erase all cache in  Diagrams in 'hierarchy' regarding their on-shell phase space and cut values
     */
    virtual void clear_cut_cache() = 0;
    virtual T get_tree(const momD_conf<T, 4>&) = 0;

    virtual std::function<T(const momD_conf<T, 4>&)> get_tree_evaluator() const = 0;

    /**
     * Given a diagram in the hierarchy (as a pair {level, diagram}) returns how many master coefficients are before it
     */
    virtual size_t n_masters_up_to(const std::pair<size_t, size_t>&) const = 0;

    /**
     * Sets corresponding cut equation of diagram ({level, diagram}). If boolean 'false' then cut equation emptied
     */
    virtual void set_cut_equation(bool, const std::pair<size_t, size_t>&) = 0;
    /**
     * Constructs members of hierarchy used for solving cut equations
     */
    virtual void build_subtractions() = 0;

    virtual ~HierarchyEngineBase() = default;
};

template <typename T, size_t D> class HierarchyEngine : public HierarchyEngineBase<T> {
    using BasisTruncation = std::vector<size_t>;
    using HierarchyBasisTruncation = std::vector<std::vector<BasisTruncation>>;
    using couplings_t = std::map<std::string, int>;

    std::unique_ptr<Forest> forest;
    std::vector<std::shared_ptr<Builder<T, D>>> builders; /**< Available copies of corresponding builder (for parallelization) */
    size_t nparticles;
    std::vector<std::vector<Diagram<T, D>>> hierarchy; /**< the diagrams in the hierarchy, as a 2-D array {level, diag_index} */
    Genealogy generations;
    bool always_recompute_tree;	/**< Tracks whether to always recompute external currents when computing coefficients */
    std::vector<size_t> Dss; /**< values of Ds to evaluate cuts */
    int tree{-1}; /**< Location of tree in forest */
    couplings_t treecoups;
    std::vector<std::pair<size_t, size_t>> resolver; /**< Keeps track of cut-equation built while executing 'build_subtractions()' */

  public:
    // FIXME: make this private and give controlled access
    momD_conf<T, D> externalmom; /**< Tracks momentum configuration of external particles */
    OnShellPoint<T, D> loopmom;

    HierarchyEngine() = default;
    HierarchyEngine(const Genealogy&, const std::shared_ptr<Model>&, bool = false, const std::vector<size_t>& Ds_vals = { 6 }, const couplings_t& = {});
    HierarchyEngine(const CutHierarchy&, const std::shared_ptr<Model>&, bool = false, const std::vector<size_t>& Ds_vals = { 6 }, const couplings_t& = {});

    const Genealogy& get_generations() const { return generations; }

    /**
     * Evaluate hierarchy for given external momenta and epsilon value
     */
    void eval(const momD_conf<T, 4>&, const T&);
    /**
     * Evaluates hierarchy for given diagram and all its parents
     */
    void eval_diagram(const momD_conf<T, 4>&, const T&, const std::pair<size_t, size_t>&);
    /**
     * Returns master integral coefficients
     *
     * @param epsilon value
     */
    std::vector<std::vector<std::vector<CoeffType<T>>>> get_master_coeffs(const T&) const;
    /**
     * Returns master integral coefficients of a given diagram
     *
     * @param epsilon value
     * @param pair: {level in hierarchy, diagram}
     */
    std::vector<CoeffType<T>> get_diagram_master_coeffs(const T&, const std::pair<size_t, size_t>&) const;
    /**
     * Truncate the function basis using warmup info
     *
     * @param: array of BasisTruncation for passing warmup information to Diagrams (defaults to empty)
     */
    void truncate_basis(const std::vector<std::vector<BasisTruncation>>& = std::vector<std::vector<BasisTruncation>>());
    /**
     * Returns IntegralGraphs for all masters
     */ 
    std::vector<lGraph::IntegralGraph> get_integral_graphs() const;
    /**
     * Returns IntegralGraphs for all masters in a given diagram in the Hierarchy (points to corresponding Diagram::get_integral_graphs())
     */ 
    std::vector<lGraph::IntegralGraph> get_diagram_integral_graphs(const std::pair<size_t, size_t>&) const;
    /**
     * Returns the Basis Truncation
     */
    HierarchyBasisTruncation get_basis_truncation() const;
 
    const std::vector<size_t>& get_Ds_values() const { return Dss; }

    /**
     * Will erase all cache in  Diagrams in 'hierarchy' regarding their on-shell phase space and cut values
     */
    void clear_cut_cache();

    /**
     * Return tree-level matrix element
     */
    T get_tree(const momD_conf<T, 4>&);

    std::function<T(const momD_conf<T, 4>&)> get_tree_evaluator() const;

    /**
     * Given a diagram in the hierarchy (as a pair {level, diagram}) returns how many master coefficients are before it
     */
    size_t n_masters_up_to(const std::pair<size_t, size_t>&) const;

    /**
     * Sets corresponding cut equation of diagram ({level, diagram}). If boolean 'false' then cut equation emptied
     */
    void set_cut_equation(bool, const std::pair<size_t, size_t>&);
    /**
     * Constructs members of hierarchy used for solving cut equations
     */
    void build_subtractions();

  private:
    /**
     * After construction, takes care of generating all kinematical functions needed for evaluation
     */
    void assemble();
};

} // namespace AmpEng
} // namespace Caravel

#include "CutEquation.hpp"
