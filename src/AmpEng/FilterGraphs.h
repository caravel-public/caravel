/**
 * @file FilterGraphs.h
 *
 * @date 5.7.2019
 *
 * @brief Helpful tools to filter graphs
*/
#pragma once

#include "Graph/Graph.h"

namespace Caravel {
namespace AmpEng {

bool target_drop_hexagons_and_above(const lGraph::xGraph& xg);

inline std::function<bool(const lGraph::xGraph&)> drop_hexagons_and_above = target_drop_hexagons_and_above;

bool target_scaleless_graphs_filter(const lGraph::xGraph& xg);

inline std::function<bool(const lGraph::xGraph&)> scaleless_graphs_filter = target_scaleless_graphs_filter;

std::function<bool(const lGraph::xGraph&)> nonplanar_ordering_filter(const std::vector<size_t>& ordering);

bool  target_four_massive_particle_filter(const lGraph::xGraph& xg);

/**
 * Search if there is any Node or Bead that has four or more massive particle attached, if so returns true
 */
inline std::function<bool(const lGraph::xGraph&)> four_massive_particle_filter = target_four_massive_particle_filter;

bool  target_four_massive_particle_with_skip_diagrams_filter(const lGraph::xGraph& xg);

/**
 * Same as four_massive_particle_filter but skipping needed diagrams
 */
inline std::function<bool(const lGraph::xGraph&)> four_massive_particle_with_skip_diagrams_filter = target_four_massive_particle_with_skip_diagrams_filter;

bool  target_at_least_two_matter_lines_filter(const lGraph::xGraph& xg);

/**
 * Search if there is at least one propagator with a mass m1!=0 and at least one propagator with mass m2!=0 (m2 can be m1)
 */
inline std::function<bool(const lGraph::xGraph&)> at_least_two_matter_lines_filter = target_at_least_two_matter_lines_filter;

bool  target_subloop_one_matter_line_filter(const lGraph::xGraph& xg);

/**
 * Search if for each sub loop of the graph there is at least one propagator with a mass m1!=0
 */
inline std::function<bool(const lGraph::xGraph&)> subloop_one_matter_line_filter = target_subloop_one_matter_line_filter;

bool  target_mushroom_filter(const lGraph::xGraph& xg);

/**
 * Filters two-loop diagrams in massive(m1)-massive(m2) scattering in which a massless link appears joining the same massive line
 */
inline std::function<bool(const lGraph::xGraph&)> mushroom_filter = target_mushroom_filter;

bool  target_weak_mushroom_filter(const lGraph::xGraph& xg);

/**
 * Targets needed 2-loop contributions for radiation contributions.
 * Checks 'mushroom_filter', if true and it is a 6- or 7-propagator graph, returns false,
   unless it is factorized and has a bubble. 
 * Furtheremore, if 'mushroom_filter'
 */
inline std::function<bool(const lGraph::xGraph&)> weak_mushroom_filter = target_weak_mushroom_filter;

} // namespace AmpEng
} // namespace Caravel
