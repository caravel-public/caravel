#include "AmpEng/FilterGraphs.h"

namespace Caravel {
namespace AmpEng {

bool target_drop_hexagons_and_above(const lGraph::xGraph& xg){
    // sanity check
    if(xg.get_n_loops()!=1){
        std::cerr<<"ERROR: drop_hexagons_and_above should be called only for 1-loop diagrams! Received:"<<std::endl;
        xg.show();
        std::exit(264);
    }
    // greater than four, as one leg is always attached to the unique node
    if(xg.get_base_graph().get_connection_map().at({0,0})[0].size()>4)
        return true;
    return false;
}

bool target_scaleless_graphs_filter(const lGraph::xGraph& xg){
    return xg.has_scaleless_bubble();
}

bool planar_rotation_check(const std::vector<size_t>& sizes,const std::vector<std::vector<size_t>>& tomatch,std::vector<size_t> lordering){
    for(size_t ii=0;ii<lordering.size();ii++){
        std::rotate(lordering.begin(),lordering.begin()+1,lordering.end());
        std::vector<std::vector<size_t>> matches;
        size_t entry(0);
        for(size_t jj=0;jj<sizes.size();jj++){
            matches.push_back(std::vector<size_t>());
            for(size_t kk=0;kk<sizes[jj];kk++){
                matches.back().push_back(lordering[entry]);
                entry++;
            }
            // sort
            std::sort(matches.back().begin(),matches.back().end());
        }
        if(matches==tomatch)
            return true;
    }

    return false;
}

bool planar_check_of_lists(const std::vector<std::vector<size_t>>& tomatch,const std::vector<size_t>& ordering){
    std::vector<size_t> sizes;
    for(size_t ii=0;ii<tomatch.size();ii++)
        sizes.push_back(tomatch[ii].size());

    auto lordering = ordering;

    if(planar_rotation_check(sizes,tomatch,lordering))
        return true;

    lordering.clear();
    for(size_t ii=ordering.size();ii>0;ii--)
        lordering.push_back(ordering[ii-1]);

    return planar_rotation_check(sizes,tomatch,lordering);
}

bool factorized_planar_rotation_check(size_t s1size,const std::vector<size_t>& sizes,const std::vector<std::vector<size_t>>& tomatch,std::vector<size_t> lordering){
    size_t nodesize(0);
    if(sizes.size()>0)
        nodesize=sizes[0];
    for(size_t ii=0;ii<lordering.size();ii++){
        std::rotate(lordering.begin(),lordering.begin()+1,lordering.end());
        // we choose a particular size from the from coming to the central node
        for (size_t startsize = 0; startsize <= nodesize; startsize++) {
            std::vector<std::vector<size_t>> matches;
            size_t entry(0);
            size_t entry2fill(0);
            for (size_t jj = 0; jj <= sizes.size(); jj++) {
                // first entry
                if (jj == 0) {
                    matches.push_back(std::vector<size_t>());
                    for (size_t kk = 0; kk < startsize; kk++) {
                        matches.back().push_back(lordering[entry]);
                        entry++;
                    }
                    entry2fill++;
                    // sort
                    std::sort(matches.back().begin(), matches.back().end());
                }
                else if (jj == s1size + 1) {
                    for (size_t kk = startsize; kk < nodesize; kk++) { 
                        matches.front().push_back(lordering[entry]); 
                        entry++;
                    }
                    // sort
                    std::sort(matches.front().begin(), matches.front().end());
                }
                else {
                    matches.push_back(std::vector<size_t>());
                    for (size_t kk = 0; kk < sizes[entry2fill]; kk++) {
                        matches.back().push_back(lordering[entry]);
                        entry++;
                    }
                    entry2fill++;
                    // sort
                    std::sort(matches.back().begin(), matches.back().end());
                }
            }
            if (matches == tomatch) return true;
        }
    }

    return false;
}

bool twoloop_factorized_planar_check_of_lists(size_t s1size,const std::vector<std::vector<size_t>>& tomatch,const std::vector<size_t>& ordering){
    std::vector<size_t> sizes;
    for(size_t ii=0;ii<tomatch.size();ii++)
        sizes.push_back(tomatch[ii].size());

    auto lordering = ordering;

    if(factorized_planar_rotation_check(s1size,sizes,tomatch,lordering))
        return true;

    lordering.clear();
    for(size_t ii=ordering.size();ii>0;ii--)
        lordering.push_back(ordering[ii-1]);

    return factorized_planar_rotation_check(s1size,sizes,tomatch,lordering);
}

std::function<bool(const lGraph::xGraph&)> nonplanar_ordering_filter(const std::vector<size_t>& ordering) {
    // sanity check
    size_t max = ordering.size();
    for(size_t ii=1;ii<max;ii++){
        if(std::find(ordering.begin(),ordering.end(),ii)==ordering.end()){
            std::cerr<<"ERROR: All consecutive numbers 1.."<<max<<" should appear in input of planar_ordering_filter(.), received: "<<ordering<<std::endl;
            std::exit(265);
        }
    }
    std::function<bool(const lGraph::xGraph&)> lambda = [ordering](const lGraph::xGraph& xg) {
        if(!xg.is_planar())
            return true;
        if(xg.get_n_loops()!=1&&xg.get_n_loops()!=2){
            std::cerr<<"ERROR: Only planar 1- and 2-loop graphs handled by planar_ordering_filter(.) so far! Received: "<<std::endl;
            xg.show();
            std::exit(266);
        }
        std::vector<std::vector<size_t>> totest;
        totest.push_back(xg.get_base_graph().get_nodes()[0].get_momentum_indices());
        // one loop
        if(xg.get_n_loops()==1){
            for(auto& b:(xg.get_base_graph().get_connection_map().at({0,0}).get_edges())[0].get_beads())
                totest.push_back(b.get_momentum_indices());
        }
        // two loops
        else{
            std::pair<size_t,size_t> istrand = {1, 2};
            std::pair<size_t,size_t> pick = {0, 1};
            if(xg.get_base_graph().is_factorizable()){
                istrand= {0, 1};
                pick = {0, 0};
            }
            else{
                size_t empty(0);
                for(size_t ii=0;ii<3;ii++){
                    if((xg.get_base_graph().get_connection_map().at(pick).get_edges())[ii].size()==0){
                        empty=ii;
                        break;
                    }
                }
                istrand = { (empty+1)%3, (empty+2)%3 };
            }
            for(auto& b:(xg.get_base_graph().get_connection_map().at(pick).get_edges())[istrand.first].get_beads())
                totest.push_back(b.get_momentum_indices());
            if(!xg.get_base_graph().is_factorizable())
                totest.push_back(xg.get_base_graph().get_nodes()[1].get_momentum_indices());
            std::vector<std::vector<size_t>> local;
            for(auto& b:(xg.get_base_graph().get_connection_map().at(pick).get_edges())[istrand.second].get_beads())
                local.push_back(b.get_momentum_indices());
            // traverse backwards
            for(size_t ii=local.size();ii>0;ii--)
                totest.push_back(local[ii-1]);
            // factorizable (2-loop) graphs need special treatment
            if(xg.get_base_graph().is_factorizable()){
                // size of first strand
                size_t s1 = (xg.get_base_graph().get_connection_map().at(pick).get_edges())[istrand.first].get_beads().size();
                return !twoloop_factorized_planar_check_of_lists(s1,totest,ordering); 
            }
        }
        // to filter non-planar orderings, we negate
        return !planar_check_of_lists(totest,ordering); 
    };
    return lambda;
}

bool target_four_massive_particle_filter(const lGraph::xGraph& xg){

    auto& nodes = xg.get_base_graph().get_nodes();
    auto& connections = xg.get_base_graph().get_connection_map();

    for (size_t ii = 0; ii < nodes.size(); ii++) {
        auto& n = nodes[ii];
        auto& legs = n.get_legs();
        size_t counter(0);
        // count all massive legs attached
        for (auto& l : legs)
            if (l.get_momentum_index() != 0) counter++;

        // count all massive links attached
        for (auto& conn : connections) {
            // counting 'from' connections
            if (conn.first.first == ii) {
                for (auto& strand : conn.second.get_edges()) {
                    if (strand.get_links().size() > 0)
                        if (strand.get_links().front().get_mass_index() != 0) counter++;
                }
            }
            // counting 'to' connections
            if (conn.first.second == ii) {
                for (auto& strand : conn.second.get_edges()) {
                    if (strand.get_links().size() > 0)
                        if (strand.get_links().back().get_mass_index() != 0) counter++;
                }
            }
        }

        if (counter > 3) return true;
    }

    for (auto& conn : connections) {
        for (auto& strand : conn.second.get_edges()) {
            for (size_t kk = 0; kk < strand.get_beads().size(); kk++) {
                auto& b = strand.get_beads()[kk];
                auto& legs = b.get_legs();
                size_t counter(0);

                // count massive legs
                for (auto& l : legs)
                    if (l.get_momentum_index() != 0) counter++;

                // count links masses attached
                if (strand.get_links()[kk].get_mass_index() != 0) counter++;
                if (strand.get_links()[kk + 1].get_mass_index() != 0) counter++;

                if (counter > 3) return true;
            }
        }
    }

    return false;
}

bool target_four_massive_particle_with_skip_diagrams_filter(const lGraph::xGraph& xg){
    static const lGraph::xGraph b(MathList("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[2], Link[0]]]]"));
    static const lGraph::xGraph t1(MathList("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[2,1], Link[0], Leg[3,0], Link[0]]]]"));
    static const lGraph::xGraph t2(MathList("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[2], Link[0], Leg[3,2], Link[2]]]]"));
    static const lGraph::xGraph t3(MathList("CaravelGraph[Nodes[Node[Leg[1,1]]], Connection[Strand[Link[LoopMomentum[1],1], Bead[Leg[2,2],Leg[3,2]], Link[1], Leg[4], Link[0]]]]"));
    static const lGraph::xGraph t4(MathList("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[3,0], Link[1], Leg[2,1], Link[0]]]]"));
    static const lGraph::xGraph d1(MathList("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[2,1], Link[0], Leg[3,0], Link[0], Leg[4,2], Link[2]]]]"));
    static const lGraph::xGraph d2(MathList("CaravelGraph[Nodes[Node[Leg[1,1]]], Connection[Strand[Link[LoopMomentum[1],1], Bead[Leg[2,2],Leg[3,2]], Link[1], Leg[4,1], Link[0], Leg[5,0], Link[0]]]]"));
    static const lGraph::xGraph d3(MathList("CaravelGraph[Nodes[Node[Leg[1,1]]], Connection[Strand[Link[LoopMomentum[1],1], Bead[Leg[2,2],Leg[3,2]], Link[1], Leg[5,0], Link[1], Leg[4,1], Link[0]]]]"));
    static const lGraph::xGraph d4(MathList("CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[LoopMomentum[1],1], Leg[3,0], Link[1], Leg[2,1], Link[0], Leg[4,2], Link[2]]]]"));
    if (xg.get_topology() == b.get_topology()
           || xg.get_topology() == t1.get_topology()
           || xg.get_topology() == t2.get_topology()
           || xg.get_topology() == t3.get_topology()
           || xg.get_topology() == t4.get_topology()
           || xg.get_topology() == d1.get_topology()
           || xg.get_topology() == d2.get_topology()
           || xg.get_topology() == d3.get_topology()
           || xg.get_topology() == d4.get_topology()
       )
        return false;
    return target_four_massive_particle_filter(xg);
}


bool target_at_least_two_matter_lines_filter(const lGraph::xGraph& xg) {

    auto propagators = xg.get_propagators();

    bool m1found(false);
    size_t m1(0);

    for (auto& rho : propagators) {
        if (rho.mass != 0) {
            m1 = rho.mass;
            m1found = true;
            break;
        }
    }

    if (!m1found) return true;

    bool filter(true);
    bool m1passed(false);

    for (auto& rho : propagators) {
        if (rho.mass != 0 && rho.mass != m1) {
            filter = false;
            break;
        }
        else if (rho.mass != 0) {
            if(!m1passed){
                m1passed = true;
                continue;
            }
            filter = false;
            break;
        }
    }

    return filter;
}

bool target_subloop_one_matter_line_filter(const lGraph::xGraph& xg) {

    auto& momenta = xg.get_internal_momenta();

    for (auto& mom : momenta) {
        auto receive = xg.get_one_loop_subdiagram(mom.n);
        auto& strand = std::get<lGraph::Strand>(receive);
        auto found(false);
        for (auto& link : strand.get_links()) {
            if (link.get_mass_index() != 0) {
                found = true;
                break;
            }
        }
        if (!found) return true;
    }

    return false;
}

bool target_mushroom_filter(const lGraph::xGraph& xg) {

    if (xg.get_n_loops() != 2) {
        static thread_local bool announce = true;
        if (announce) {
            std::cout << "WARNING: 'mushroom_filter' only working for 2-loop diagrams\n";
            announce = false;
        }
        return false;
    }
    if (!xg.has_massive_link()) return false;

    // traverse all links in xg and check for mushrooms
    auto& connections = xg.get_base_graph().get_connection_map();

    for (auto& conn : connections) {
        for (size_t strand_i = 0; strand_i < conn.second.get_edges().size(); strand_i++) {
            auto& strand = conn.second[strand_i];
            for (size_t link_j = 0; link_j < strand.get_n_links(); link_j++) {
                auto& link = strand.get_link(link_j);
                if (link.is_massless_propagator()) {
                    // Collect masses attached to each side of 'link'.
                    // If one non-zero mass appears on each side, we'll assume a mushroom!

                    // simplest case, an internal link
                    if (link_j > 0 and link_j + 1 < strand.get_n_links()) {
                        auto check = (strand.get_link(link_j - 1).get_mass_index() == strand.get_link(link_j + 1).get_mass_index());
                        if (check) return true;
                    }

                    // not a single link in the strand
                    else if (strand.get_n_links() > 1) {
                        // TODO: the following logic is purely 2-loopish
                        const size_t mass_target = strand.get_link((link_j == 0 ? 1 : link_j - 1)).get_mass_index();
                        if (mass_target == 0) continue;
                        for (size_t strand_k = 0; strand_k < conn.second.get_edges().size(); strand_k++) {
                            if (strand_k == strand_i) continue;
                            size_t mass_to_match(0);
                            if (xg.get_base_graph().is_factorizable()) {
                                mass_to_match = conn.second[strand_k].get_links().front().get_mass_index();
                                if (mass_to_match != mass_target) mass_to_match = conn.second[strand_k].get_links().back().get_mass_index();
                                if (mass_to_match != mass_target) {
                                    // check if single node has corresponding outgoing mass
                                    auto& node = xg.get_base_graph().get_nodes()[0];
                                    for (auto& leg : node.get_legs())
                                        if (leg.get_mass_index() == mass_target) {
                                            mass_to_match = leg.get_mass_index();
                                            break;
                                        }
                                }
                            }
                            else {
                                if (link_j == 0)
                                    mass_to_match = conn.second[strand_k].get_links().front().get_mass_index();
                                else
                                    mass_to_match = conn.second[strand_k].get_links().back().get_mass_index();
                            }
                            if (mass_target == mass_to_match) { return true; }
                        }
                    }

                    // single link in strand, we check if another strand has all links with the same mass
                    else {
                        for (size_t strand_k = 0; strand_k < conn.second.get_edges().size(); strand_k++) {
                            if (strand_k == strand_i) continue;
                            size_t mass_target = conn.second[strand_k].get_links().front().get_mass_index();
                            if (mass_target == 0) continue;
                            bool allequal(true);
                            for (size_t ll = 1; ll < conn.second[strand_k].get_links().size(); ll++) {
                                if (mass_target != conn.second[strand_k].get_links()[ll].get_mass_index()) {
                                    allequal = false;
                                    break;
                                }
                            }
                            if (allequal) return true;
                        }
                    }
                }
                else
                    continue;
            }
        }
    }

    return false;
}

bool target_weak_mushroom_filter(const lGraph::xGraph& xg) {
    // Few graphs to make sure are filtered
    static const lGraph::xGraph pb1(MathList("CaravelGraph[Nodes[Node[],Node[]],Connection[Strand[Link[LoopMomentum[2],0]],Strand[Link[1]],Strand[Link[1],Bead["
                                             "Leg[1,1]],Link[0],Bead[Leg[2,2],Leg[3,2]],Link[0],Bead[Leg[4,1]],Link[LoopMomentum[-1],1]]]]"));
    if (xg.get_topology() == pb1.get_topology()) return true;
    static const lGraph::xGraph bt1(MathList("CaravelGraph[Nodes[Node[],Node[]],Connection[Strand[Link[0],Bead[Leg[1,1],Leg[4,1]],Link[LoopMomentum[-1],0]],"
                                             "Strand[Link[2]],Strand[Link[LoopMomentum[2],2],Bead[Leg[2,2]],Link[0],Bead[Leg[3,2]],Link[2]]]]"));
    if (xg.get_topology() == bt1.get_topology()) return true;
    static const lGraph::xGraph bt2(MathList("CaravelGraph[Nodes[Node[],Node[]],Connection[Strand[Link[0],Bead[Leg[2,2],Leg[3,2]],Link[0],Bead[Leg[1,1]],Link["
                                             "LoopMomentum[-2],1]],Strand[Link[1]],Strand[Link[LoopMomentum[1],1],Bead[Leg[4,1]],Link[0]]]]"));
    if (xg.get_topology() == bt2.get_topology()) return true;
    static const lGraph::xGraph bb1(MathList("CaravelGraph[Nodes[Node[Leg[4,1]],Node[]],Connection[Strand[Link[0]],Strand[Link[0],Bead[Leg[2,2],Leg[3,2]],Link["
                                             "0],Bead[Leg[1,1]],Link[LoopMomentum[-2],1]],Strand[Link[LoopMomentum[1],1]]]]"));
    if (xg.get_topology() == bb1.get_topology()) return true;
    static const lGraph::xGraph sstt1(MathList("CaravelGraph[Nodes[Node[Leg[3,2]],Node[]],Connection[Strand[Link[LoopMomentum[2],0],Bead[Leg[1,1],Leg[4,1]],"
                                               "Link[0]],Strand[Link[0],Bead[Leg[2,2]],Link[2]],Strand[Link[LoopMomentum[-1],2]]]]"));
    if (xg.get_topology() == sstt1.get_topology()) return true;

    if (!target_mushroom_filter(xg))
        return false;
    if (xg.get_base_graph().is_factorizable())
        return true;
    return false;
}

} // namespace AmpEng
} // namespace Caravel
