#include "CoefficientEngine.h"

namespace Caravel {
namespace AmpEng {

#ifdef USE_FINITE_FIELDS
template class CoefficientEngine<F32>;

template class CoefficientEngine<C>; 
#ifdef HIGH_PRECISION 
template class CoefficientEngine<CHP>; 
#endif // HIGH_PRECISION
#ifdef VERY_HIGH_PRECISION
template class CoefficientEngine<CVHP>; 
#endif

#endif // USE_FINITE_FIELDS

} // namespace AmpEng
} // namespace Caravel

