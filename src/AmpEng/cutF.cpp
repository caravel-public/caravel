/*
 * cutF.cpp
 *
 * First created on 24.7.2016
 *
 * Implementation of routines for computing cuts
 *
*/

#include <iostream>
#include <algorithm>
#include <math.h>
#include <set>
#include <cassert>

#include "Core/Utilities.h"
#include "Core/Debug.h"
#include "Forest/Forest.h"
#include "AmpEng/cutF.h"

#include "AmplitudeCoefficients/partial_trace.h"


#define _DEBUG_EXTERNAL_PARTICLES_HACK

namespace Caravel {
namespace AmpEng{

using couplings_t = std::map<std::string,int>;


couplings_t add_couplings(couplings_t ca,const couplings_t& cb);

// default constructor
parenthood::parenthood(): parenthood_level(0), locate(0), parent(std::make_pair(0,0)), daughter(std::make_pair(0,0)), one_parent_received(0), one_parent_nth_parent_received(0) {
}

// general constructor
parenthood::parenthood(size_t d_level,size_t d_locate,cutF* daughtercF,size_t p_level,size_t p_locate,cutF* parentcF,CutHierarchy* cFm,size_t one_p,size_t associated_parent_one_p): locate(p_locate),
		parent(std::make_pair(p_level,p_locate)), daughter(std::make_pair(d_level,d_locate)), one_parent_received(one_p), one_parent_nth_parent_received(associated_parent_one_p) {
	if(d_level>p_level)
		parenthood_level=d_level-p_level;
	else{
		std::cout<<"parenthood::parenthood(.) received a lower level daughter than parent!! d: "<<d_level<<" vs. p: "<<p_level<<std::endl;
		parenthood_level=0;
	}
	// add 1-parent info
	if(parenthood_level<2)
							// 0 as for 1-parents it is irrelevant
		one_parents.push_back(std::make_pair(one_p,associated_parent_one_p));
	// 1-loop case
	else if(parentcF->rung_count==1)
		one_parents.push_back(std::make_pair(one_p,associated_parent_one_p));
	else if(parentcF->rung_count==0||parentcF->rung_count>3)
		std::cout<<"Error: parenthood::parenthood(.) received a non 1- or 2-loop parent cut!! -- Did nothing"<<std::endl;
	// in case we need to check for extra possible parenthood paths
	else{
		bool is_sunrise(false);
		// bow ties skipped
		if(daughtercF->rung_count==3){
			bool all_zero(true);
			for(size_t ii=0;ii<3;ii++){
				if(daughtercF->cut_rungs[ii].get_multiplicity()>0){
					all_zero=false;
					break;
				}
			}
			if(all_zero)
				is_sunrise=true;
		}
		// treat in full sunrises
		if(is_sunrise){
			DEBUG_MESSAGE("Possible multiple paths for parenthood of sunrise!!");
			one_parents=this->get_inequivalent_paths(cFm,daughtercF,parentcF);
		}
		// in general, all two-loop cuts should be checked. But for now, non-sunrise cuts assumed to have single inequivalent paths
		else{
			DEBUG_MESSAGE("PARENTHOOD only handling single inheritance path for all cuts but the sunrise");
			one_parents.push_back(std::make_pair(one_p,associated_parent_one_p));
		}
	}
}

std::vector<std::pair<size_t,size_t> > parenthood::get_inequivalent_paths(CutHierarchy* cFm,cutF* daughtercF,cutF* parentcF){
	// find out possible inequivalent paths
	std::vector<std::vector<size_t> > paths_between_p_and_d(this->get_paths(parentcF->get_prop_count(),parenthood_level));
#if 0
std::cout<<"IN CALL to get_paths: # props in parent: "<<parentcF->get_prop_count()<<" # of props to drop: "<<parenthood_level<<" size of returned possible paths: "<<paths_between_p_and_d.size()<<std::endl;
std::cout<<"ALL of them:"<<std::endl;
for(size_t ii=0;ii<paths_between_p_and_d.size();ii++)
	std::cout<<"	"<<ii<<": "<<paths_between_p_and_d[ii]<<std::endl;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// What follows is a rather heavy piece of code. It should scale pretty badly, but maybe enough for reaching relatively low multiplicity (up to 6?) 2-loop cut hierachies

	// check which paths of inheritance actually match parentcF with daughtercF
	std::vector<size_t> needed_paths;
	for(size_t ii=0;ii<paths_between_p_and_d.size();ii++){
		cutF local_daughter(parentcF->multi_pinch(paths_between_p_and_d[ii]));
#if 0
std::cout<<"The NEW local daughter is: "<<local_daughter.get_short_name()<<std::endl;
std::cout<<"This was produced from the parent: "<<parentcF->get_short_name()<<" by dropping the props: "<<paths_between_p_and_d[ii]<<std::endl;
#endif
		if(local_daughter==*daughtercF){
			needed_paths.push_back(ii);
#if 0
std::cout<<"FOUND that it matches daughter: "<<daughtercF->get_short_name()<<" !!"<<std::endl;
#endif
		}
	}
	if(needed_paths.size()==0){
		std::cout<<"ERROR!! Did not find any path connecting parent: "<<parentcF->get_short_name()<<" with daughter: "<<daughtercF->get_short_name()<<std::endl;
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::pair<size_t,size_t> > toret;

	// in this case, just take user input to define inheritance
	if(needed_paths.size()==1){
		toret.push_back(std::make_pair(one_parent_received,one_parent_nth_parent_received));
	}
	else{
		size_t props_in_d(daughtercF->get_prop_count());
		// there has to be at least one maximal
		size_t props_in_max(cFm->cut_hierarchy[0][0].get_prop_count());
		if(props_in_d+1>props_in_max)
			std::cout<<"ERROR in props_in_max-props_in_d"<<std::endl;
		size_t one_p_level(props_in_max-props_in_d-1);
//std::cout<<"one_p_level: "<<one_p_level<<std::endl;

		for(size_t ii=0;ii<needed_paths.size();ii++){
			std::vector<size_t> local_path_to_1parent(paths_between_p_and_d[needed_paths[ii]]);
			// drop last prop (just a choice)
			local_path_to_1parent.pop_back();
//std::cout<<"popped back: "<<local_path_to_1parent<<std::endl;
			cutF onepcF(parentcF->multi_pinch(local_path_to_1parent));
			int locate_one_p_incFm(-1);
			// find place in cFm where this is stored
			for(size_t jj=0;jj<cFm->cut_hierarchy[one_p_level].size();jj++){
				if(onepcF==cFm->cut_hierarchy[one_p_level][jj]){
					locate_one_p_incFm=int(jj);
					break;
				}
			}
			if(locate_one_p_incFm<0)
				std::cout<<"ERROR!! Did not find one_p in cFm !!"<<std::endl;
			// now see where locate_one_p_incFm is located among 1-parents of daughter
			int locate_one_p(-1);
			for(size_t jj=0;jj<cFm->cut_hierarchy[daughter.first][daughter.second].parent_cuts[0].size();jj++){
				if(cFm->cut_hierarchy[daughter.first][daughter.second].parent_cuts[0][jj].locate==size_t(locate_one_p_incFm)){
					locate_one_p=int(jj);
					break;
				}
			}
			if(locate_one_p<0)
				std::cout<<"ERROR!! Did not find p_of_one_p !!"<<std::endl;

			// now find which entry in parents of one_parent corresponds to locate (place in which parentcF is stored)
			int locate_p_of_one_p(-1);
			for(size_t jj=0;jj<cFm->cut_hierarchy[one_p_level][size_t(locate_one_p_incFm)].parent_cuts[parenthood_level-2].size();jj++){
				if(cFm->cut_hierarchy[one_p_level][size_t(locate_one_p_incFm)].parent_cuts[parenthood_level-2][jj].locate==locate){
					locate_p_of_one_p=int(jj);
					break;
				}
			}
			if(locate_p_of_one_p<0)
				std::cout<<"ERROR!! Did not find p_of_one_p !!"<<std::endl;
#if 0
std::cout<<"TEST: "<<locate_one_p<<" "<<locate_p_of_one_p<<std::endl;
std::cout<<"WITH this received: "<<one_parent_received<<" "<<one_parent_nth_parent_received<<std::endl;
#endif
			// finally, add to toret
			toret.push_back(std::make_pair(size_t(locate_one_p),size_t(locate_p_of_one_p)));
		}
	}

	return toret;
}

void recursive_fill(std::vector<std::vector<size_t> >& all,std::vector<size_t>& tofill,size_t local_entry,size_t position,size_t size,size_t total_props){
	// first fill corresponding entry
	tofill[position]=local_entry;
	// do we need to fill a new position?
	if(position+1<size){
		for(size_t ii=local_entry+1;ii<total_props-size+1+position+1;ii++){
			recursive_fill(all,tofill,ii,position+1,size,total_props);
		}
	}
	// and if we reach a complete path, add it to all
	else if(position+1==size){
#if 0
std::cout<<"A new path: "<<tofill<<std::endl;
#endif
		all.push_back(tofill);
	}
}

std::vector<std::vector<size_t> > compute_all_paths(size_t n_parent,size_t n_drop){
	std::vector<std::vector<size_t> > toret;
	for(size_t ii=0;ii<n_parent-n_drop+1;ii++){
		std::vector<size_t> tofill(n_drop);
		recursive_fill(toret,tofill,ii,0,n_drop,n_parent);
	}
	return toret;
}

std::vector<std::vector<size_t> > parenthood::get_paths(size_t n_props_in_parent,size_t n_props_to_drop){
	std::vector<std::vector<size_t> > toret;

	for(size_t ii=0;ii<inequivalent_paths_stored.size();ii++){
		if(n_props_in_parent==inequivalent_paths_stored[ii].first.first&&n_props_to_drop==inequivalent_paths_stored[ii].first.second)
			return inequivalent_paths_stored[ii].second;
	}

	// no corresponding entry in inequivalent_paths_stored was found, so we need to compute it, add it and return
	std::pair<size_t,size_t> tostore_first(std::make_pair(n_props_in_parent,n_props_to_drop));
	toret=compute_all_paths(n_props_in_parent,n_props_to_drop);

	inequivalent_paths_stored.push_back(std::make_pair(tostore_first,toret));

	return toret;
}

std::vector<std::pair<std::pair<size_t,size_t>,std::vector<std::vector<size_t> > > > parenthood::inequivalent_paths_stored = std::vector<std::pair<std::pair<size_t,size_t>,std::vector<std::vector<size_t> > > > ();

size_t cutF::cut_counter = 0;

// forward specific 1-loop constructor to general one
cutF::cutF(RungF r,bool b) : cutF(std::vector<RungF>{r},b) {
}

// Given a rung, returns topological info of partition
std::vector<size_t> get_rung_partition(RungF* r){
	std::vector<size_t> toreturn;
	const std::vector<Vertex>& lvertices(r->get_vertices());
	for(size_t ii=0;ii<lvertices.size();ii++)
		toreturn.push_back(lvertices[ii].get_multiplicity());
#if 0
std::cout<<"RungF PARTITTION return: ";
for(size_t ii=0;ii<toreturn.size();ii++)
	std::cout<<toreturn[ii]<<" ";
std::cout<<std::endl;
#endif
	return toreturn;
}

std::string ngon_name(size_t n){
	switch (n){
		case 0:	return "0-gon";
		case 1:	return "tadpole";
		case 2:	return "bubble";
		case 3:	return "triangle";
		case 4:	return "box";
		case 5:	return "pentagon";
		case 6:	return "hexagon";
		default:	return std::to_string(n)+"-gon";
	}
}

void order_decreasing(size_t& left_index,size_t& central_index,size_t& right_index,size_t vr0,size_t vr1,size_t vr2){
	// assume 0 first
	left_index=0;
	central_index=1;
	right_index=2;
	if(vr2>vr1){
		right_index=2;
		central_index=1;
	}
	// 1 should be first
	if(vr1>vr0&&vr1>=vr2){
		left_index=1;
		if(vr2>vr0){
			right_index=2;
			central_index=0;
		}
		else{
			right_index=0;
			central_index=2;
		}
	}
	// 2 should be first
	if(vr2>vr0&&vr2>=vr1){
		left_index=2;
		if(vr1>vr0){
			right_index=1;
			central_index=0;
		}
		else{
			right_index=0;
			central_index=1;
		}
	}
//std::cout<<"order_decreasing took: ("<<vr0<<","<<vr1<<","<<vr2<<") and returned: ("<<left_index<<","<<central_index<<","<<right_index<<")"<<std::endl;
}

// default constructor
cutF::cutF() : cut_rungs(nullptr), rung_count(0), tree_locate(0), cut_hierarchy_level(0), cut_kin_locate(0),
			name(""), name_id(""), structure(nullptr), owns_struct(false), n_particles(0), loop_mom_conf_location(nullptr),
			accepts_direct_fit(true), ctopology(cutTopology()) {
}

cutF::cutF(std::vector<RungF> vr,bool construct_cst) : cut_rungs(nullptr), rung_count(0), tree_locate(0), cut_hierarchy_level(0), cut_kin_locate(0),
			name(""), name_id(""), structure(nullptr), owns_struct(false), n_particles(0), loop_mom_conf_location(nullptr),
			accepts_direct_fit(true) {
	// check number of rungs passed, if one, forward to 1-loop constructor
	if(vr.size()<1||vr.size()>3){
		std::cout<<"Only handle 1- (1 rung) and 2-loop (2 or 3 rungs) cuts. Received: "<<vr.size()<<" rungs --- Do nothing "<<std::endl;
		return;
	}
	// one-loop constructor
	else if(vr.size()==1){
		DEBUG_MESSAGE("Constructing 1-loop cut");
		// no problem with rung ordering, copy existing one, EXPLICITLY droping top and bottom vertices
		cut_rungs=new RungF(vr[0],Vertex(),Vertex());
		vertex_end[0]=Vertex();
		vertex_end[1]=Vertex();
		rung_count=1;
	}
	// bowtie two-loop cut
	else if(vr.size()==2){
		DEBUG_MESSAGE("Constructing a bowtie 2-loop cut");
		cut_rungs=new RungF[2];
		// always left rung has maximum amount of vertices
		size_t left_index(0);
		size_t right_index(1);
		if(vr[1].get_multiplicity()>vr[0].get_multiplicity()){
			left_index=1;
			right_index=0;
		}
		// I drop explicit info on vertices for the local rungs
		// When adding the coupling powers we always take the size of the left rung which is not supposed to be empty
		*(cut_rungs)=RungF(vr[left_index],Vertex(),Vertex());
		*(cut_rungs+1)=RungF(vr[right_index],Vertex(),Vertex());
		rung_count=2;
		// here both ends of both rungs match, so labels are irrelevant (anyhow, maybe good to add a check?)
		vertex_end[0]=(vr[left_index].get_vertex_top())*(vr[right_index].get_vertex_top());
		vertex_end[1]=(vr[left_index].get_vertex_bottom())*(vr[right_index].get_vertex_bottom());
		// Fix color ordering in case of left-right flip, by performing a top-bottom flip
		if(left_index>right_index){
			*(cut_rungs)=(cut_rungs)->flip();
			*(cut_rungs+1)=(cut_rungs+1)->flip();
			// the flip of the vertices was necessary irrespective of the top-bottom flip (it is due to the left-bottom flip)
			Vertex temp(vertex_end[0].flip());
			vertex_end[0]=vertex_end[1].flip();
			vertex_end[1]=temp;
		}
	}
	// three-rung two-loop cut
	else{
		// check if a rung is a null rung
		int check(-1);
		int count_check(0);
		for(int ii=0;ii<3;ii++){
			if(vr[ii].is_null()){
				DEBUG_MESSAGE("Found a null rung (",ii,") in cutF constructor --- dropped it");
				check=ii;
				count_check++;
			}
		}
		if(count_check>1){
			DEBUG_MESSAGE("Passed two or more null rungs --- Do nothing ");
			return;
		}
		else if(check<0){
			// always left rung has maximum amount of vertices
			size_t left_index(0);
			size_t right_index(0);
			size_t central_index(0);
			// proper ordering
			order_decreasing(left_index,central_index,right_index,vr[0].get_multiplicity(),vr[1].get_multiplicity(),vr[2].get_multiplicity());
			if(central_index!=1)
				std::cout<<"DEALING with moving central rung in non-planar diagrams! EXPECT troubles!"<<std::endl;
			DEBUG_MESSAGE("Constructing 2-loop cut");
			cut_rungs=new RungF[3];
			// I drop explicit info on vertices for the local rungs
			// Always use the left rung to obtain the size of the coupling vector since it is not supposed to be empty
			*(cut_rungs)=RungF(vr[left_index],Vertex(),Vertex());
			*(cut_rungs+1)=RungF(vr[central_index],Vertex(),Vertex());
			*(cut_rungs+2)=RungF(vr[right_index],Vertex(),Vertex());
			rung_count=3;
			// here end orderings can be different!! OJO should check!!
			vertex_end[0]=((vr[left_index].get_vertex_top())*(vr[central_index].get_vertex_top()))*(vr[right_index].get_vertex_top());
			vertex_end[1]=((vr[left_index].get_vertex_bottom())*(vr[central_index].get_vertex_bottom()))*(vr[right_index].get_vertex_bottom());
			// if there is a left/right exchange, we need to flip vertices
			if(left_index>right_index){
				*(cut_rungs)=(cut_rungs)->flip();
				*(cut_rungs+1)=(cut_rungs+1)->flip();
				*(cut_rungs+2)=(cut_rungs+2)->flip();
				// the flip of the vertices was necessary irrespective of the top-bottom flip (it is due to the left-bottom flip)
				Vertex temp(vertex_end[0].flip());
				vertex_end[0]=vertex_end[1].flip();
				vertex_end[1]=temp;
			}
		}
		else{
			int track[2];
			if(check==0){		track[0]=1; track[1]=2; }
			else if(check==1){	track[0]=0; track[1]=2; }
			else{			track[0]=0; track[1]=1; }
			DEBUG_MESSAGE("Constructing a bowtie 2-loop cut");
			cut_rungs=new RungF[2];
			// always left rung has maximum amount of vertices
			size_t left_index(0);
			size_t right_index(1);
			if(vr[track[1]].get_multiplicity()>vr[track[0]].get_multiplicity()){
				left_index=1;
				right_index=0;
			}
			// Use left rung to determine coupling power vertex size since this rung is not supposed to be empty
			*(cut_rungs)=RungF(vr[track[left_index]],Vertex(),Vertex());
			*(cut_rungs+1)=RungF(vr[track[right_index]],Vertex(),Vertex());
			// as they are bow ties, fix endings:
			cut_rungs->match_top=1;
			cut_rungs->match_bottom=1;
			(cut_rungs+1)->match_top=1;
			(cut_rungs+1)->match_bottom=1;
			rung_count=2;
			// here both ends of both rungs match, so labels are irrelevant (anyhow, maybe good to add a check?)
			// given that a null rung should come in the middle, the following is a proper handling of the merge top/bottom vertices
			vertex_end[0]=(vr[0].get_vertex_top())*(vr[1].get_vertex_top())*(vr[2].get_vertex_top());
			vertex_end[1]=(vr[0].get_vertex_bottom())*(vr[1].get_vertex_bottom())*(vr[2].get_vertex_bottom());
			// Fix color ordering in case of left-right flip, by performing a top-bottom flip
			if(left_index>right_index){
				*(cut_rungs)=(cut_rungs)->flip();
				*(cut_rungs+1)=(cut_rungs+1)->flip();
				// the flip of the vertices was necessary irrespective of the top-bottom flip (it is due to the left-bottom flip)
				Vertex temp(vertex_end[0].flip());
				vertex_end[0]=vertex_end[1].flip();
				vertex_end[1]=temp;
			}
			// and update info
			cut_rungs->update_info();
			(cut_rungs+1)->update_info();
//std::cout<<"bottom vertex: "<<vertex_end[1].get_name()<<std::endl;
		}
	}


///////////////////////////////////////////////////////////////////////////////
	// build cutF info
///////////////////////////////////////////////////////////////////////////////
        construct_info();

///////////////////////////////////////////////////////////////////////////////
	// extract topological info
///////////////////////////////////////////////////////////////////////////////

	std::vector< std::vector<size_t> > rung_partitions;
	for(size_t ii=0;ii<rung_count;ii++)
		rung_partitions.push_back(get_rung_partition(cut_rungs+ii));
	std::vector<Particle*> to_construct_proc;
	std::vector<Particle*> to_construct_tree_proc;
	// 1 loop
	if(rung_count==1){
		const std::vector<Particle*>& lparts(cut_rungs->get_particles());
		// copy needed info for process
		to_construct_proc=lparts;
		to_construct_tree_proc=lparts;
	}
	// bowtie cut
	else if(rung_count==2){
		DEBUG_MESSAGE("CAREFUL!! Questions about proper handling of bowtie cuts!!");

		const std::vector<Particle*>& lparts(cut_rungs->get_particles());
		// start building process
		to_construct_proc=lparts;
		to_construct_tree_proc=lparts;

		// take care of bottom vertex
		const std::vector<Particle*>& lparts2(vertex_end[1].get_particles());
		for(size_t kk=0;kk<lparts2.size();kk++){
			to_construct_proc.push_back(lparts2[kk]);
			to_construct_tree_proc.push_back(lparts2[kk]);
		}

		// take care of top vertex
		const std::vector<Particle*>& lparts2b(vertex_end[0].get_particles());
		for(size_t kk=0;kk<lparts2b.size();kk++){
			to_construct_proc.push_back(lparts2b[kk]);
			to_construct_tree_proc.push_back(lparts2b[kk]);
		}

		// follow second rung
		const std::vector<Particle*>& lparts3((cut_rungs+1)->get_particles());
		for(size_t kk=0;kk<lparts3.size();kk++){
			to_construct_proc.push_back(lparts3[kk]);
			to_construct_tree_proc.push_back(lparts3[kk]);
		}
	}
	// general two loop
	else{
		const std::vector<Particle*>& lparts(cut_rungs->get_particles());
		// start building process
		to_construct_proc=lparts;
		to_construct_tree_proc=lparts;

		// now take care of bottom vertex
		const std::vector<Particle*>& lparts2(vertex_end[1].get_particles());
		for(size_t kk=0;kk<lparts2.size();kk++){
			to_construct_proc.push_back(lparts2[kk]);
			to_construct_tree_proc.push_back(lparts2[kk]);
		}

		// follow second rung from bottom to top
		const std::vector<Particle*>& lparts3((cut_rungs+1)->get_particles());
		for(int kk=int(lparts3.size())-1;kk>-1;kk--)	to_construct_proc.push_back(lparts3[kk]);

		// now take care of top vertex
		const std::vector<Particle*>& lparts4(vertex_end[0].get_particles());
		for(size_t kk=0;kk<lparts4.size();kk++)	to_construct_proc.push_back(lparts4[kk]);

		// follow third rung
		const std::vector<Particle*>& lparts5((cut_rungs+2)->get_particles());
		for(size_t kk=0;kk<lparts5.size();kk++)	to_construct_proc.push_back(lparts5[kk]);

		// arbitrary assigment for tree_process
		// after bottom vertex, now third rung
		for(int kk=lparts5.size()-1;kk>-1;kk--)	to_construct_tree_proc.push_back(lparts5[kk]);
		// now second rung
		for(int kk=lparts3.size()-1;kk>-1;kk--)	to_construct_tree_proc.push_back(lparts3[kk]);
		// now top vertex
		for(size_t kk=0;kk<lparts4.size();kk++)	to_construct_tree_proc.push_back(lparts4[kk]);
	}

        // do not use embedding prescription for trees (this doesn't work atm)
        //for(auto it: to_construct_tree_proc) it->set_dtt_index(0);

	cut_process=Process(to_construct_proc);
	tree_process=Process(to_construct_tree_proc);
#if 0
std::cout<<"Testing corresponding process: ";
for(size_t ii=0;ii<to_construct_proc.size();ii++)	std::cout<<to_construct_proc[ii]->get_name()<<" ";
std::cout<<std::endl;
std::cout<<"Testing corresponding \"TREE\" process: ";
for(size_t ii=0;ii<to_construct_tree_proc.size();ii++)	std::cout<<to_construct_tree_proc[ii]->get_name()<<" ";
std::cout<<std::endl;
#endif
	// as we know the external particles, let's allocate the loop_mom_conf_location array
	n_particles=cut_process.get_process().size();
	// size should be correct
	if(rung_count==1){
		loop_mom_conf_location=new int[2];
		for(size_t ii=0;ii<2;ii++)
			*(loop_mom_conf_location+ii)=n_particles+ii+1;
	}
	else if(rung_count>1 && rung_count<4){
		loop_mom_conf_location=new int[4];
		for(size_t ii=0;ii<4;ii++)
			*(loop_mom_conf_location+ii)=n_particles+ii+1;
	}
	else{
		std::cout<<"Now handling only 1- and 2-loop cuts: rung_count="<<rung_count<<" did not assign loop_mom_conf_location"<<std::endl;
	}

	// build name for cut
	if(rung_count==1){
		name=ngon_name(rung_partitions[0].size());
	}
	else if(rung_count==2){
		if(vertex_end[0].get_multiplicity()>0||vertex_end[1].get_multiplicity()>0)
			name="generic factorized ";
		else
			name="factorized ";
		name+=ngon_name(rung_partitions[0].size()+1)+"-";
		name+=ngon_name(rung_partitions[1].size()+1);
	}
	else if(rung_count==3){
		if(vertex_end[0].get_multiplicity()>0&&vertex_end[1].get_multiplicity()>0)
			name="generic ";
		else if(vertex_end[0].get_multiplicity()>0||vertex_end[1].get_multiplicity()>0)
			name="semi-simple ";
		name+=ngon_name(rung_partitions[0].size()+rung_partitions[1].size()+2)+"-";
		name+=ngon_name(rung_partitions[1].size()+rung_partitions[2].size()+2);
		if(rung_partitions[0].size()>0&&rung_partitions[1].size()>0&&rung_partitions[2].size()>0)
			name="non-planar "+name;
	}
	// build name_id
	name_id="(";
	for(size_t ii=0;ii<rung_count;ii++){
		name_id+=(cut_rungs+ii)->get_rung_name();
		if(ii+1<rung_count)
			name_id+=",";
	}
	name_id+=")";
	if(rung_count==2||rung_count==3){
		if(vertex_end[0].get_multiplicity()>0||vertex_end[1].get_multiplicity()>0)
			name_id+="-("+vertex_end[0].get_name()+","+vertex_end[1].get_name()+")";
	}


	// one new cut
	cut_counter++;

	// now construct default loop particles (we don't attach any extra string to the conventional name by default)
	this->update_loop_momentum("");

	construct_graph();

	// check if we need to construct local cstructure
	if(construct_cst){
		this->construct_local_cstructure();
	}
}

///////////////////////////////////////////////////////////////////////////////
	// build cutF info
///////////////////////////////////////////////////////////////////////////////
void cutF::construct_info() {
    std::vector<std::vector<std::string>> collect_rung_info;
    size_t length(0);
    for (size_t ii = 0; ii < rung_count; ii++) {
        collect_rung_info.push_back((cut_rungs + ii)->get_info());
        if (collect_rung_info[ii].size() > length) length = collect_rung_info[ii].size();
    }
    // normalize rung entries
    for (size_t ii = 0; ii < rung_count; ii++) {
        size_t llength((collect_rung_info[ii][0]).size());
        std::string ls("");
        for (size_t jj = 0; jj < llength; jj++) ls += " ";
        for (size_t jj = collect_rung_info[ii].size(); jj < length; jj++) collect_rung_info[ii].push_back(ls);
    }
    info.clear();
    // add first rung info
    for (size_t ii = 0; ii < length; ii++) info.push_back(collect_rung_info[0][ii]);
    // and now rest
    for (size_t ii = 0; ii < length; ii++) {
        for (size_t jj = 1; jj < rung_count; jj++) info[ii] += "  " + collect_rung_info[jj][ii];
    }
}

void cutF::construct_graph() {
    using namespace lGraph;
    // one loop
    if (rung_count == 1) {
        const RungF& rung = *cut_rungs;
        // start from back for easier xGraph construction (canonicalization will take care of choosing an ordering)
	const Vertex& vfirst = rung.get_vertices().back();
        std::vector<Leg> legs;
        for(auto& p:vfirst.get_particles()){
            legs.push_back( Leg(true, p->get_mass_index() , p->mom_index()) );
        }
        Node node(legs);
        std::vector<Bead> beads;
        std::vector<lGraph::Link> links;
        // assign loop-momentum rounting!
        links.emplace_back(LoopMomentum{1}, rung.get_links()[0].get_particle().get_mass_index());
        for (size_t ii = 0; ii + 1 < rung.get_vertices().size(); ii++) {
            links.emplace_back(rung.get_links()[ii + 1].get_particle().get_mass_index());
            const Vertex& lvfirst = rung.get_vertices()[ii];
            std::vector<Leg> llegs;
            for (auto& p : lvfirst.get_particles()) { llegs.push_back(Leg(true, p->get_mass_index(), p->mom_index())); }
            beads.push_back(Bead(llegs));
        }
        Strand strand(beads, links);
        std::map<std::pair<size_t, size_t>, Connection<Strand>> map;
        map[std::make_pair(0,0)]=Connection<Strand>({strand});
        Graph<Node, Connection<Strand>> toconst({node},map);
        graph = xGraph(toconst);
    }
    // two loops factorized
    else if (rung_count == 2) {
        std::vector<Leg> legs;
        for (size_t ii = 0; ii < 2; ii++) {
            // start from back for easier xGraph construction (canonicalization will take care of choosing an ordering)
            const Vertex& vfirst = vertex_end[ii];
            for (auto& p : vfirst.get_particles()) { legs.push_back(Leg(true, p->get_mass_index(), p->mom_index())); }
        }
        Node node(legs);
        std::vector<Strand> strands;
        for (size_t ii = 0; ii < 2; ii++) {
            const RungF& rung = cut_rungs[ii];
            std::vector<Bead> beads;
            std::vector<lGraph::Link> links;
            links.emplace_back(rung.get_links()[0].get_particle().get_mass_index());
            for (size_t ii = 0; ii < rung.get_vertices().size(); ii++) {
                links.emplace_back(rung.get_links()[ii + 1].get_particle().get_mass_index());
                const Vertex& lvfirst = rung.get_vertices()[ii];
                std::vector<Leg> llegs;
                for (auto& p : lvfirst.get_particles()) { llegs.push_back(Leg(true, p->get_mass_index(), p->mom_index())); }
                beads.push_back(Bead(llegs));
            }
            // assign loop-momentum rounting!
            if(ii==0)
                links.front().set_momentum_label(1);
            else
                links.back().set_momentum_label(-2);
            strands.push_back(Strand(beads, links));
        }
        std::map<std::pair<size_t, size_t>, Connection<Strand>> map;
        map[std::make_pair(0,0)]=Connection<Strand>(strands);
        Graph<Node, Connection<Strand>> toconst({node},map);
        graph = xGraph(toconst);
    }
    // two loops
    else if (rung_count == 3) {
        std::vector<Node> nodes;
        for (size_t ii = 0; ii < 2; ii++) {
        std::vector<Leg> legs;
            // start from back for easier xGraph construction (canonicalization will take care of choosing an ordering)
            const Vertex& vfirst = vertex_end[ii];
            for (auto& p : vfirst.get_particles()) { legs.push_back(Leg(true, p->get_mass_index(), p->mom_index())); }
            nodes.push_back(Node(legs));
        }
        std::vector<Strand> strands;
        for (size_t ii = 0; ii < 3; ii++) {
            const RungF& rung = cut_rungs[ii];
            std::vector<Bead> beads;
            std::vector<lGraph::Link> links;
            links.emplace_back(rung.get_links()[0].get_particle().get_mass_index());
            for (size_t ii = 0; ii < rung.get_vertices().size(); ii++) {
                links.emplace_back(rung.get_links()[ii + 1].get_particle().get_mass_index());
                const Vertex& lvfirst = rung.get_vertices()[ii];
                std::vector<Leg> llegs;
                for (auto& p : lvfirst.get_particles()) { llegs.push_back(Leg(true, p->get_mass_index(), p->mom_index())); }
                beads.push_back(Bead(llegs));
            }
            // assign loop-momentum rounting!
            if(ii==0)
                links.front().set_momentum_label(1);
            else if(ii==2)
                links.back().set_momentum_label(-2);
            strands.push_back(Strand(beads, links));
        }
        std::map<std::pair<size_t, size_t>, Connection<Strand>> map;
        map[std::make_pair(0,1)]=Connection<Strand>(strands);
        Graph<Node, Connection<Strand>> toconst(nodes,map);
        graph = xGraph(toconst);
    }
    else {
        std::cerr << "ERROR: cutF only works for 1- or 2-loop cuts!" << std::endl;
        std::exit(343);
    }
}

// copy constructor
cutF::cutF(const cutF& ic): rung_count(ic.rung_count), cut_process(ic.cut_process), forest_positions(ic.forest_positions), tree_process(ic.tree_process), tree_locate(ic.tree_locate),
		cut_hierarchy_level(ic.cut_hierarchy_level), cut_kin_locate(ic.cut_kin_locate), info(ic.info), name(ic.name), name_id(ic.name_id),
		owns_struct(ic.owns_struct), n_particles(ic.n_particles), loop_particle_container(ic.loop_particle_container),
		accepts_direct_fit(ic.accepts_direct_fit), ctopology(ic.ctopology), graph(ic.graph), loop_parts(ic.loop_parts) {
	// copy top and bottom vertices
	vertex_end[0]=ic.vertex_end[0];
	vertex_end[1]=ic.vertex_end[1];
	// copy parents
	parent_cuts=ic.parent_cuts;
	// copy rungs
	if(rung_count>0&&rung_count<4){
		if(rung_count==1)
			cut_rungs=new RungF;
		else
			cut_rungs=new RungF[rung_count];
		for(size_t ii=0;ii<rung_count;ii++)
			*(cut_rungs+ii)=ic.cut_rungs[ii];

		//----if(rung_count==1)
		//----	loop_particles=new Particle(*(ic.loop_particles));
		//----else
		//----	loop_particles=new Particle[2] { *(ic.loop_particles) , *(ic.loop_particles+1) };

		if(ic.loop_mom_conf_location==nullptr){
			loop_mom_conf_location=nullptr;
		}
		else if(rung_count==1){
			loop_mom_conf_location=new int[2];
			for(size_t ii=0;ii<2;ii++)
				*(loop_mom_conf_location+ii)=*(ic.loop_mom_conf_location+ii);
		}
		else{
			loop_mom_conf_location=new int[4];
			for(size_t ii=0;ii<4;ii++)
				*(loop_mom_conf_location+ii)=*(ic.loop_mom_conf_location+ii);
		}
	}
	else{
		cut_rungs=nullptr;
		//----loop_particles=nullptr;
		loop_mom_conf_location=nullptr;
	}

	if(owns_struct&&ic.structure!=nullptr){
		structure=new CStructure(*(ic.structure));
	}
	else{
		structure=ic.structure;
	}
}

// assignment operator
cutF& cutF::operator=(const cutF& ic){
	rung_count=ic.rung_count;
	cut_process=ic.cut_process;
	forest_positions=ic.forest_positions;
	tree_process=ic.tree_process;
	tree_locate=ic.tree_locate,
	cut_hierarchy_level=ic.cut_hierarchy_level;
	cut_kin_locate=ic.cut_kin_locate;
	info=ic.info;
	name=ic.name;
	name_id=ic.name_id;
	owns_struct=ic.owns_struct;
	n_particles=ic.n_particles;
	loop_particle_container=ic.loop_particle_container;
	accepts_direct_fit=ic.accepts_direct_fit;

	// copy top and bottom vertices
	vertex_end[0]=ic.vertex_end[0];
	vertex_end[1]=ic.vertex_end[1];

	// copy parents
	parent_cuts=ic.parent_cuts;

	// clear memory
	if(rung_count==1){
		delete cut_rungs;
	}
	else{
		delete [] cut_rungs;
	}

	if(loop_mom_conf_location!=nullptr)
		delete [] loop_mom_conf_location;


	// copy rungs
	if(rung_count>0&&rung_count<4){
		if(rung_count==1){
			cut_rungs=new RungF;
		}
		else{
			cut_rungs=new RungF[rung_count];
		}
		for(size_t ii=0;ii<rung_count;ii++)
			*(cut_rungs+ii)=ic.cut_rungs[ii];

		//----if(rung_count==1)
		//----	loop_particles=new Particle(*(ic.loop_particles));
		//----else
		//----	loop_particles=new Particle[2] { *(ic.loop_particles) , *(ic.loop_particles+1) };

		if(ic.loop_mom_conf_location==nullptr){
			loop_mom_conf_location=nullptr;
		}
		else if(rung_count==1){
			loop_mom_conf_location=new int[2];
			for(size_t ii=0;ii<2;ii++)
				*(loop_mom_conf_location+ii)=*(ic.loop_mom_conf_location+ii);
		}
		else{
			loop_mom_conf_location=new int[4];
			for(size_t ii=0;ii<4;ii++)
				*(loop_mom_conf_location+ii)=*(ic.loop_mom_conf_location+ii);
		}
	}
	else{
		cut_rungs=nullptr;
		//----loop_particles=nullptr;
		loop_mom_conf_location=nullptr;
	}

	if(owns_struct&&ic.structure!=nullptr){
		delete structure;
		structure=new CStructure(*(ic.structure));
	}
	else{
		structure=ic.structure;
	}

	// assign loop momentum vector
	loop_parts = ic.loop_parts;

	// assign cutTopology information
	ctopology = ic.ctopology;

	// assign graph
	graph=ic.graph;

	return *this;
}

void cutF::update_loop_momentum(std::string ls){
	// start by clearing loop_particle_container
	loop_particle_container.clear();

	// clear existing loop particles
	loop_parts.clear();

	// now build set of Particle to use
	if(rung_count==1){
		std::string lpname("l1");
		// attach extra string
		lpname+=ls;

		// ASSUMING for now always colored particles in the loop
				// notice order: picks first Link of RungF for building loop particle (with FUTURE switch of order "get_type()" and "contract_name()")
		//----loop_particles=new Particle(lpname,(cut_rungs->links)[0].get_particle_type(),(cut_rungs->links)[0].get_particle().contract_name(),(cut_rungs->links)[0].get_particle().is_colored(),SingleState("default"),0,0);
		// Fill the Loop particles
		// l1, ml1, l2, ml2
                auto p = (cut_rungs->links)[0].get_particle();
		loop_parts.push_back(Particle( p.get_anti_type().to_string()+lpname, p.get_anti_type(), SingleState("default",p.external_state().get_dtt_index()),0,0));
		loop_parts.push_back(Particle( "n"+p.get_type().to_string()+lpname, p.get_type(), SingleState("default",p.external_state().get_dtt_index()),0,0));
	}
	else if(rung_count==2){
		std::string lpname1("l1");
		// attach extra string
		lpname1+=ls;

		std::string lpname2("l2");
		// attach extra string
		lpname2+=ls;

		// ASSUMING for now always colored particles in the loop
				// notice order: picks first Link of RungF for building loop particle (with FUTURE switch of order "get_type()" and "contract_name()")
		//----Particle ll1(lpname1,((cut_rungs)->endlink).particle_type,((cut_rungs)->endlink).get_particle().contract_name(),((cut_rungs)->endlink).get_particle().is_colored(),SingleState("default"),0,0);
		// in case there is at least a vertex, define properly ll1
		//----if(((cut_rungs)->links).size()>0)
		//----	ll1=Particle(lpname1,((cut_rungs)->links)[0].particle_type,((cut_rungs)->links)[0].get_particle().contract_name(),((cut_rungs)->links)[0].get_particle().is_colored(),SingleState("default"),0,0);
		//----Particle ll2(lpname2,((cut_rungs+1)->endlink).particle_type,((cut_rungs+1)->endlink).get_particle().contract_name(),((cut_rungs+1)->endlink).get_particle().is_colored(),SingleState("default"),0,0);
		//----loop_particles=new Particle[2] { ll1 , ll2 };
		// Fill loop particles
		// First l1 and -l1
                if ((cut_rungs->links).size() > 0) {
                    auto p = (cut_rungs->links)[0].get_particle();
                    loop_parts.push_back(
                        Particle(p.get_anti_type().to_string() + lpname1, p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                    loop_parts.push_back(
                        Particle("n" + p.get_type().to_string() + lpname1, p.get_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                } else {
                    auto p = (cut_rungs->endlink).get_particle();
                    loop_parts.push_back(
                        Particle(p.get_anti_type().to_string() + lpname1, p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                    loop_parts.push_back(
                        Particle("n" + p.get_type().to_string() + lpname1, p.get_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                }
		// Then l2 and -l2
                auto p = ((cut_rungs+1)->endlink).get_particle();
		loop_parts.push_back(Particle(p.get_type().to_string()+lpname2, p.get_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
		loop_parts.push_back(Particle("n"+p.get_anti_type().to_string()+lpname2, p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()),0,0));
	}
	else if(rung_count==3){
		std::string lpname1("l1");
		// attach extra string
		lpname1+=ls;

		std::string lpname2("l2");
		// attach extra string
		lpname2+=ls;

		// ASSUMING for now always colored particles in the loop
				// notice order: picks first Link of RungF for building loop particle (with FUTURE switch of order "get_type()" and "contract_name()")
		//----Particle ll1(lpname1,((cut_rungs)->endlink).particle_type,((cut_rungs)->endlink).get_particle().contract_name(),((cut_rungs)->endlink).get_particle().is_colored(),SingleState("default"),0,0);
		// in case there is at least a vertex, define properly ll1
		//----if(((cut_rungs)->links).size()>0)
		//----	ll1=Particle(lpname1,((cut_rungs)->links)[0].particle_type,((cut_rungs)->links)[0].get_particle().contract_name(),((cut_rungs)->links)[0].get_particle().is_colored(),SingleState("default"),0,0);
		//----Particle ll2(lpname2,((cut_rungs+2)->endlink).particle_type,((cut_rungs+2)->endlink).get_particle().contract_name(),((cut_rungs+2)->endlink).get_particle().is_colored(),SingleState("default"),0,0);
		//----loop_particles=new Particle[2] { ll1 , ll2 };
		// Fill loop particles
		// First l1 and -l1
                if ((cut_rungs->links).size() > 0) {
                    auto p = (cut_rungs->links)[0].get_particle();
                    loop_parts.push_back(
                        Particle(p.get_anti_type().to_string() + lpname1, p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                    loop_parts.push_back(
                        Particle("n" + p.get_type().to_string() + lpname1, p.get_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                } else {
                    auto p = (cut_rungs->endlink).get_particle();
                    loop_parts.push_back(
                        Particle(p.get_anti_type().to_string() + lpname1, p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                    loop_parts.push_back(
                        Particle("n" + p.get_type().to_string() + lpname1, p.get_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                }
                // Then l2 and -l2
                auto p = ((cut_rungs + 2)->endlink).get_particle();
                loop_parts.push_back(
                    Particle(p.get_type().to_string() + lpname2, p.get_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                loop_parts.push_back(Particle("n" + p.get_anti_type().to_string() + lpname2, p.get_anti_type(),
                                              SingleState("default", p.external_state().get_dtt_index()), 0, 0));
        }
        //std::cout << "loop_parts.size() after constructor : " << loop_parts.size() << std::endl;
}

void build_info_for_cstructure(const size_t& rung_count,RungF* cut_rungs,Vertex* vertex_end,std::vector<size_t>& l_partition,int& l_n_particles,size_t* l_location_loops,
			std::vector<size_t>& l_mom_indices,const std::vector<Particle *>& process_particles){

	std::vector< std::vector<size_t> > rung_partitions;
	for(size_t ii=0;ii<rung_count;ii++)
		rung_partitions.push_back(get_rung_partition(cut_rungs+ii));
	// 1 loop
	if(rung_count==1){
		l_partition=rung_partitions[0];
		l_n_particles=0;
		for(size_t ii=0;ii<l_partition.size();ii++)	l_n_particles+=l_partition[ii];
		l_location_loops[0]=l_n_particles+1;
		l_location_loops[1]=0;
		// not relevant:
		l_location_loops[2]=0;
		l_location_loops[3]=0;
	}
	// bowtie cut
	else if(rung_count==2){
		DEBUG_MESSAGE("CAREFUL!! Questions about proper handling of bowtie cuts!!");

		size_t top_count(vertex_end[0].get_multiplicity());
		size_t bottom_count(vertex_end[1].get_multiplicity());
		l_n_particles=0;
		// follow first rung
		for(size_t ii=0;ii<rung_partitions[0].size();ii++){
			l_partition.push_back(rung_partitions[0][ii]);
			l_n_particles+=rung_partitions[0][ii];
		}

		// take care of vertex
		// -l1
		l_location_loops[1]=0;
		l_n_particles+=bottom_count;
		// l2
		l_location_loops[2]=l_n_particles+1;
		// l1
		l_location_loops[0]=l_n_particles+2;
		l_partition.push_back(bottom_count+top_count+2);
		l_n_particles+=top_count;

		// follow second rung
		for(size_t ii=0;ii<rung_partitions[1].size();ii++){
			l_partition.push_back(rung_partitions[1][ii]);
			l_n_particles+=rung_partitions[1][ii];
		}
		// -l2
		l_location_loops[3]=l_n_particles+3;
	}
	// general two loop
	else{
		size_t top_count(vertex_end[0].get_multiplicity());
		size_t bottom_count(vertex_end[1].get_multiplicity());
		l_n_particles=0;
		// follow first rung
		for(size_t ii=0;ii<rung_partitions[0].size();ii++){
			l_partition.push_back(rung_partitions[0][ii]);
			l_n_particles+=rung_partitions[0][ii];
		}

		// now take care of bottom vertex
		// -l1
		l_location_loops[1]=0;
		l_n_particles+=bottom_count;
		// l2
		l_location_loops[2]=l_n_particles+1;
		l_partition.push_back(bottom_count+1);

		// follow second rung from bottom to top
		for(int ii=int(rung_partitions[1].size())-1;ii>-1;ii--){
			l_partition.push_back(rung_partitions[1][ii]);
			l_n_particles+=rung_partitions[1][ii];
		}

		// now take care of top vertex
		// l1
		l_location_loops[0]=l_n_particles+2;
		l_n_particles+=top_count;
		l_partition.push_back(top_count+1);

		// follow third rung
		for(size_t ii=0;ii<rung_partitions[2].size();ii++){
			l_partition.push_back(rung_partitions[2][ii]);
			l_n_particles+=rung_partitions[2][ii];
		}
		// -l2
		l_location_loops[3]=l_n_particles+3;

	}

	for(size_t kk=0;kk<process_particles.size();kk++)	l_mom_indices.push_back(process_particles[kk]->mom_index());
}

void cutF::construct_local_cstructure(){
	if(!owns_struct){
		owns_struct=true;

		// for building structure
		std::vector<size_t> l_partition;
		int l_n_particles;
		size_t l_location_loops[4];
		std::vector<size_t> l_mom_indices;

		build_info_for_cstructure(rung_count,cut_rungs,vertex_end,l_partition,l_n_particles,l_location_loops,l_mom_indices,cut_process.get_process());

		// ready to construct structure
		structure=new CStructure(graph,rung_count,l_partition,l_n_particles,l_location_loops,l_mom_indices,accepts_direct_fit);
	}
	else{
		_WARNING("Already owns its cstructure --- construct_local_cstructure() did nothing!!");
	}
}

void cutF::show() const {
	std::cout<<name;
	if(rung_count>0&&rung_count<4)
		rung_count==1?std::cout<<" 1-loop":std::cout<<" 2-loop";
	std::cout<<" cut -- "<<name_id<<":"<<std::endl;
	// first attached top vertex
	std::cout<<"Top vertex: "<<vertex_end[0].get_name()<<std::endl;
	// now cut
	for(size_t ii=0;ii<info.size();ii++)
		std::cout<<info[ii]<<std::endl;
	// finally attached bottom vertex
	std::cout<<"Bottom vertex: "<<vertex_end[1].get_name()<<std::endl;
}

std::ostream& operator<<(std::ostream& s, const cutF& c){
	s<<c.name;
	if(c.rung_count>0&&c.rung_count<4)
		c.rung_count==1?s<<" 1-loop":s<<" 2-loop";
	s<<" cut -- "<<c.name_id<<":"<<std::endl;
	// first attached top vertex
	s<<"Top vertex: "<<c.vertex_end[0].get_name()<<std::endl;
	// now cut
	for(size_t ii=0;ii<c.info.size();ii++)
		s<<c.info[ii]<<std::endl;
	// finally attached bottom vertex
	s<<"Bottom vertex: "<<c.vertex_end[1].get_name();
        return s;
}
#if 0
void cutF::set_loop_mom_conf(const size_t& csi){
	// multiplicity of the process
	n_particles=csi;
	// size should be correct
	if(loop_mom_conf_location!=nullptr){
		_WARNING("Trying to re-assign loop location --- set_loop_mom_conf(.) did nothing");
	}
	else if(rung_count==1){
		loop_mom_conf_location=new int[2];
		for(size_t ii=0;ii<2;ii++)
			*(loop_mom_conf_location+ii)=csi+ii+1;
	}
	else if(rung_count>1 && rung_count<4){
		loop_mom_conf_location=new int[4];
		for(size_t ii=0;ii<4;ii++)
			*(loop_mom_conf_location+ii)=csi+ii+1;
	}
	else{
		std::cout<<"Now handling only 1- and 2-loop cuts: rung_count="<<rung_count<<" --- set_loop_mom_conf(i) Did nothing"<<std::endl;
		return;
	}
}
#endif

// optional argument is possible non-default reference vector
void cutF::add_to_forest(Forest& f, size_t rvec, bool switch_to_planar_standard, size_t _max_Ds) {
    // check that loop momenta location has been added properly
    if (loop_mom_conf_location == nullptr) {
        std::cout << "add_to_forest(...) had no access to loop_mom_conf_location --- Did nothing" << std::endl;
        return;
    }

    // check that structure is not a null pointer
    if (structure == nullptr) {
        std::cout << "add_to_forest(...) called without defining corresponding CStructure: maybe need to call construct_local_cstructure()? --- Did nothing"
                  << std::endl;
        return;
    }

    // in case loop particles need to be created
    if (loop_particle_container.size() == 0) {
        //----loop_particle_container.push_back(std::make_tuple(*loop_particles,*(structure->location_loops),*(structure->location_loops+1),
        //----			*(loop_mom_conf_location),*(loop_mom_conf_location+1)));
        loop_particle_container.push_back(std::make_tuple(loop_parts[0], *(structure->location_loops), *(structure->location_loops + 1),
                                                          *(loop_mom_conf_location), *(loop_mom_conf_location + 1)));
        // two particles!
        if (rung_count > 1)
            //----loop_particle_container.push_back(std::make_tuple(*(loop_particles+1),*(structure->location_loops+2),*(structure->location_loops+3),
            //----			*(loop_mom_conf_location+2),*(loop_mom_conf_location+3)));
            loop_particle_container.push_back(std::make_tuple(loop_parts[2], *(structure->location_loops + 2), *(structure->location_loops + 3),
                                                              *(loop_mom_conf_location + 2), *(loop_mom_conf_location + 3)));
    }

    if (tree_locate != 0 or !forest_positions.forest_ids.empty()) {
        _WARNING_R("Called add_to_forest() for a second time !");
        std::exit(1);
    }

    std::vector<std::tuple<int, int, int, int>> loop_parts_out;
    for (size_t i = 0; i < loop_particle_container.size(); ++i)
        loop_parts_out.push_back(std::make_tuple(std::get<1>(loop_particle_container[i]), std::get<2>(loop_particle_container[i]),
                                                 std::get<3>(loop_particle_container[i]), std::get<4>(loop_particle_container[i])));


    /*
     * Now adding to forest
     */
    {
        // Container for the different coupling powers per Vertex
        std::vector<couplings_t> vcp;
        // Container for the global coupling powers of the cut
        couplings_t cp;
        // Container for the internal particles that enter and exit vertices
        std::vector<Particle> parts;

        // Now step through the rungs and fill the vectors

        // Rung 1 -----------------------------------------------------------------
        if (rung_count > 0) {

            // Get the internal particles
            for (size_t i = 0; i < cut_rungs[0].get_links().size(); ++i) {

                // First particle is only entering
                if (i == 0) {
                    // -l1 enters the loop
                    Particle pml1(loop_parts[1]);

										//If -l1 already belongs to right loop
										if (i == cut_rungs[0].get_links().size() - 1)
										    pml1.set_internal_index(0);
										else
										    pml1.set_internal_index(cut_rungs[0].get_links()[0].get_down_internal_index());

                    parts.push_back(pml1);
                    // std::cout<<"-l1: "<<parts.back()<<std::endl;
                    // std::cout<<"	fermion index: i: "<<i<<" down: "<<cut_rungs[0].get_links()[0].get_down_internal_index()<<std::endl;
                }
                // Last particle is only exiting if there is only one rung,
                // otherwise it also enters the second rung
                else if (i == cut_rungs[0].get_links().size() - 1) {
                    if (rung_count == 1) {
                        // l1 exits the loop
                        parts.push_back(loop_parts[0]);
                        // std::cout<<"l1: "<<parts.back()<<std::endl;
                        // std::cout<<"	fermion index: i: "<<i<<" up: "<<cut_rungs[0].get_links()[i].get_up_internal_index()<<std::endl;
                    }
                    // If there is a second rung particle enters it
                    else {
                        auto p = cut_rungs[0].get_links()[i].get_particle();
                        parts.push_back(Particle("", p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                        // std::cout<<"First added (1): "<<parts.back()<<std::endl;
                        parts.push_back(p);
                        // std::cout<<"Second added (1): "<<parts.back()<<std::endl;
                    }
                }
                // Inside the rung a particle is exiting the previous and entering
                // the next vertex
                else {
                    Particle p2(cut_rungs[0].get_links()[i].get_particle());
                    Particle p1("", p2.get_anti_type(), SingleState("default", p2.external_state().get_dtt_index()), 0, 0);
                    p1.set_internal_index(cut_rungs[0].get_links()[i].get_up_internal_index());
                    p2.set_internal_index(cut_rungs[0].get_links()[i].get_down_internal_index());
                    parts.push_back(p1);
                    // std::cout<<"First added (2): "<<parts.back()<<std::endl;
                    // std::cout<<"	fermion index: i: "<<i<<" down: "<<cut_rungs[0].get_links()[i].get_down_internal_index()<<" up: "<<cut_rungs[0].get_links()[i].get_up_internal_index()<<std::endl;
                    parts.push_back(p2);
                    // std::cout<<"Second added (2): "<<parts.back()<<std::endl;
                }
            }

            // Add the coupling powers per vertex
            vcp = cut_rungs[0].get_coupling_powers_per_vertex();

            // Add global couplings
            cp = cut_rungs[0].get_coupling_powers();

        } // Rung 1

        // Rung 2 -----------------------------------------------------------------
        if (rung_count > 1) {
            if (rung_count == 3) {
                // Get the internal particles.
                // Due to the S-tree structure that is used to construct the
                // cutTopology, the second recond rung is stepped through from
                // bottom to top. This is why the role of particles and antiparticles
                // is exchanged.
                for (int i = cut_rungs[1].get_links().size() - 1; i > -1; --i) {
                    auto p = cut_rungs[1].get_links().at(i).get_particle();
                    parts.push_back(p);
                    parts.push_back(Particle("", p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                }
            }
            // If there are two rungs the second one is stepped through in correct order
            else if (rung_count == 2) {
                for (size_t i = 0; i < cut_rungs[1].get_links().size(); ++i) {

                    // Last particle is only exiting
                    if (i == cut_rungs[1].get_links().size() - 1) {
                        parts.push_back(loop_parts[3]);
                    } else {
                        auto p = cut_rungs[1].get_links().at(i).get_particle();
                        parts.push_back(Particle("", p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                        parts.push_back(p);
                    }
                }
            }

            // Deal with the coupling powers
            // Global coupling powers
            cp = add_couplings(cp, cut_rungs[1].get_coupling_powers());

            // If number of rungs == 3, first add bottom couplings, then
            // the vertex couplings of rung 2 and finally the top couplings
            if (rung_count == 3) {
                // Couplings per vertex. They also need to be inverted!!!
                // First add the bottom couplings (in S-tree, bottom comes first)
                vcp.push_back(vertex_end[1].get_coupling_powers());
                // Then add the other vertex's coupling powers in reversed order
                for (int i = cut_rungs[1].get_coupling_powers_per_vertex().size() - 1; i > -1; --i) {
                    vcp.push_back(cut_rungs[1].get_coupling_powers_per_vertex()[i]);
                }
                // Finally add the top vertex couplings
                vcp.push_back(vertex_end[0].get_coupling_powers());
            }
            // If number of rungs == 2, then first add combination of top and
            // bottom vertices and then add rung two couplings
            if (rung_count == 2) {
                vcp.push_back((vertex_end[1] * vertex_end[0]).get_coupling_powers());
                for (size_t i = 0; i < cut_rungs[1].get_coupling_powers_per_vertex().size(); ++i) {
                    vcp.push_back(cut_rungs[1].get_coupling_powers_per_vertex()[i]);
                }
            }
        } // Rung 2

        // Rung 3 -----------------------------------------------------------------
        if (rung_count == 3) {

            for (size_t i = 0; i < cut_rungs[2].get_links().size(); ++i) {

                // Last particle is only exiting
                if (i == cut_rungs[2].get_links().size() - 1) {
                    // -l2 is exiting the loop
                    parts.push_back(loop_parts[3]);
                } else {
                    auto p = cut_rungs[2].get_links().at(i).get_particle();
                    parts.push_back(Particle("", p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0));
                    parts.push_back(p);
                }
            }

            // Deal with the coupling powers
            // Global coupling powers
            cp = add_couplings(cp, cut_rungs[2].get_coupling_powers());

            // Coupling powers per vertex
            for (auto vertexcp : cut_rungs[2].get_coupling_powers_per_vertex()) { vcp.push_back(vertexcp); }
        }

        size_t n_loops = (rung_count > 1) ? 2 : 1;

#ifdef _DEBUG_EXTERNAL_PARTICLES_HACK
        static thread_local std::vector<Particle*> different_external_particles;
#endif

        // add information about (nf,nh) powers
        unsigned nfcount = 0, nhcount = 0;
        {
            std::set<ParticleFlavor> internal_flavours;
            for (const auto& it : parts) { internal_flavours.insert(it.get_flavor()); }
            if (internal_flavours.count(ParticleFlavor::q_nf1) or internal_flavours.count(ParticleFlavor::qb_nf1)) ++nfcount;
            if (internal_flavours.count(ParticleFlavor::q_nf2) or internal_flavours.count(ParticleFlavor::qb_nf2)) ++nfcount;
            if (internal_flavours.count(ParticleFlavor::Q_nf1) or internal_flavours.count(ParticleFlavor::Qb_nf1)) ++nhcount;
            if (internal_flavours.count(ParticleFlavor::Q_nf2) or internal_flavours.count(ParticleFlavor::Qb_nf2)) ++nhcount;
        }


        Caravel::partial_trace::full trace_full{6+2*unsigned(n_loops)};
        // check if a non-default (maxDs = 8 for 1-loop and maxDs = 10 for 2-loop) is requested
        if(_max_Ds != 0)
            // partial trace only works for even dimensions
            if(_max_Ds%2 == 0)
                trace_full = Caravel::partial_trace::full(_max_Ds);


        decltype(trace_full.get(0,0)) partial_trace_scheme;
        {

            size_t n_quarks = 0;
            for (auto p : cut_process.get_process()){
                if (p->external_state().is_external() and in_container<ParticleFlavor::q>(p->get_flavor()) ) ++n_quarks;
            }
            if(n_quarks>0){
                partial_trace_scheme = trace_full.get(n_quarks, nfcount+nhcount);
            }
        }


        auto add_cut = [this, &vcp, &n_loops, &parts,&f,&loop_parts_out,&partial_trace_scheme,&nfcount,&nhcount,rvec,&switch_to_planar_standard](int sch_i) -> int {
            // local copy of cut process
            std::vector<Particle> process;
            for(auto it : cut_process.get_process()) process.emplace_back(*it);


            // assign new indices for external particles
            if(!partial_trace_scheme.empty()){
                const auto& ptr_i = partial_trace_scheme.at(sch_i);
                for (size_t ii=0;ii<process.size();ii++){
                    auto& p = process[ii];
                    auto& pp = cut_process.get_process()[ii];
                    ParticleFlavor fl = p.get_flavor();
                    if(p.external_state().is_external() and (in_container<ParticleFlavor::q>(fl) or in_container<ParticleFlavor::qb>(fl))){
                        auto ind = get_fermion_line_index(pp);
			p.set_internal_index(get_internal_index(pp));
                        if(!ind.assigned){
                            _WARNING_R("Internal inconsistency! No fermion line index found!");
                            std::exit(1);
                        }
                        // indices ind.value here are 0-based, but the scheme expects 1-based, so we +1
                        p.set_dtt_index(ptr_i.first.at(ind.value+1));
                    }
                }
            }

            cutTopology ct;

            std::vector<Particle*> to_remove;

            // Now step through the S-tree and construct the cutTopology
            int startparticle(0);
            size_t locationcount(0);
            couplings_t lcouplings;
            size_t verts_in_partition(structure->partition.size());
            // if (n_loops == 2) verts_in_partition += 2;

            for (size_t ii = 0; ii < verts_in_partition; ++ii) {
                // Particles per Vertex on S-tree
                int nparts(structure->partition[ii]);
                Particle lp1(parts[2 * ii]);
                fermion_line_index lp1_findex = get_link_fermion_line_index(ii);
                Particle lp2(parts[2 * ii + 1]);
                fermion_line_index lp2_findex = get_link_fermion_line_index(ii+1);
                std::vector<Particle> toconst;
                std::vector<fermion_line_index> toconst_findex;

                // Step through all the particles contained in vertex ii
                for (int jj = startparticle; jj < startparticle + nparts; ++jj) {

                    // In two loop case external ends of rungs are connected to loop
                    // momenta in vertices on the rung
                    if (n_loops == 2) {
                        if (jj + 1 == std::get<1>(loop_particle_container[0])) {
                            //----toconst.push_back(&std::get<0>(loop_particle_container[0]));
                            toconst.push_back(loop_parts[0]);
                            toconst_findex.push_back(get_link_fermion_line_index(0));
                        } else if (jj + 1 == std::get<1>(loop_particle_container[1])) {
                            //----toconst.push_back(&std::get<0>(loop_particle_container[1]));
                            toconst.push_back(loop_parts[2]);
                            toconst_findex.push_back(get_link_fermion_line_index(structure->partition.size()));
                        } else {
                            toconst.push_back(process[locationcount]);
                            toconst_findex.push_back(get_fermion_line_index(cut_process.get_process()[locationcount]));
                            locationcount++;
                        }
                    }      // n_loops == 2
                    else { // 1 - loop case
                        toconst.push_back(process[locationcount]);
                        toconst_findex.push_back(get_fermion_line_index(cut_process.get_process()[locationcount]));
                        locationcount++;
                    }
                }
                startparticle += nparts;
                lcouplings = vcp.at(ii);

                // Here we assign correct representation to Weyl spinors, using the global fermion_line_index
                {
                    // which spinor chains should change representation compared to the standard assignment (q <--> repr::La; qb <--> repr::Lat)
                    std::vector<size_t> flip_representation_container;

                    for (size_t nn=0;nn<process.size();nn++) {
                        const auto& p = process[nn];
                        auto& pp = cut_process.get_process()[nn];
                        auto ind = p.get_flavor();
                        if (!is_dirac(ind) and in_container<ParticleFlavor::q>(ind) and (p.external_state().get_representation() != repr::La)){
                            auto findex = get_fermion_line_index(pp);
                            if(!findex.assigned){
                                _WARNING_R("Internal inconsistency! No fermion line index found in representation assignment!");
                                std::exit(1);
                            }
                            flip_representation_container.push_back(findex.value);
                        }
                        else if (!is_dirac(ind) and in_container<ParticleFlavor::qb>(ind) and p.external_state().get_representation() != repr::Lat){
                            auto findex = get_fermion_line_index(pp);
                            if(!findex.assigned){
                                _WARNING_R("Internal inconsistency! No fermion line index found in representation assignment!");
                                std::exit(1);
                            }
                            flip_representation_container.push_back(findex.value);
                        }
                    }

                    auto assign_representation = [&flip_representation_container](Particle& p,const fermion_line_index& pfi) {
                        auto flip_representation = [&flip_representation_container](const fermion_line_index& i) -> bool {
                            if(!i.assigned) return false;
                            if (std::find(flip_representation_container.begin(), flip_representation_container.end(), i.value) != flip_representation_container.end())
                                return true;
                            return false;
                        };

                        auto fl = p.get_flavor();
                        if (!p.external_state().is_external()) {
                            if (!is_dirac(fl) and in_container<ParticleFlavor::q>(fl)) {
                                if (flip_representation(pfi))
                                    p.set_dtt_index(repr::Lat);
                                else
                                    p.set_dtt_index(repr::La);
                            }
                            if (!is_dirac(fl) and in_container<ParticleFlavor::qb>(fl)) {
                                if (flip_representation(pfi))
                                    p.set_dtt_index(repr::La);
                                else
                                    p.set_dtt_index(repr::Lat);
                            }
                        }
                    };

                    assign_representation(lp1,lp1_findex);
                    assign_representation(lp2,lp2_findex);
                    for (size_t tt=0;tt<toconst.size();tt++) assign_representation(toconst[tt],toconst_findex[tt]);
                }

                std::vector<Particle*> to_insert;
// FIXME
// TERRIBLE HACK: new external particle each time to disable BG cache
// for external particles we create new ones
#ifdef _DEBUG_EXTERNAL_PARTICLES_HACK
                for (auto& it : toconst) {
                    if (it.external_state().is_external()) {
                        auto fp = std::find_if(different_external_particles.begin(), different_external_particles.end(), [&it](auto p) { return it == *p; });
                        // if doesn't exist yet create a new copy
                        if (fp == different_external_particles.end()) {
                            different_external_particles.push_back(new Particle(it));
                            to_insert.push_back(different_external_particles.back());
                        } else {
                            (*fp)->set_internal_index(it.get_internal_index());
                            to_insert.push_back(*fp);
                        }
                    }
                    else{
                        // for loop particles we pass pointers to temporary objects, but after they get added to forest they are not relevant
                        to_remove.push_back(new Particle(it));
                        to_insert.push_back(to_remove.back());
                    }
                }
#endif

                ct.add_vertex(lp1, to_insert, lp2, lcouplings);

            }

            ct.set_n_closed_light_fermion_loops(nfcount);
            ct.set_n_closed_heavy_fermion_loops(nhcount);

            // set number of embeddings for correct normalization by builder
            if (!partial_trace_scheme.empty()) {
                ct.set_n_external_embeddings(partial_trace_scheme.size());
                ct.set_n_copies(partial_trace_scheme.at(sch_i).second);
            }

            if (!constructed_cutTopology) {
                ctopology = ct;
                constructed_cutTopology = true;
            }
            else {
                ctopology.set_n_closed_light_fermion_loops(nfcount);
                ctopology.set_n_closed_heavy_fermion_loops(nhcount);
            }

            int pos =  f.add_cut(loop_parts_out, ct,rvec,switch_to_planar_standard);
            if (pos == 0) {
                _WARNING_R("Failed to add a cut to forest!");
                std::exit(1);
            }

            for(auto it : to_remove) delete it;

            return pos;
        };

        size_t iN = 1;
        if (!partial_trace_scheme.empty()) iN = partial_trace_scheme.size();

        // for each helicity component add gluon cuts
        for (size_t i = 0; i < iN; ++i) {
            forest_positions.forest_ids.push_back(add_cut(i));
        }

    }

    if (forest_positions.forest_ids.size() >= 2) { f.join_cuts(forest_positions.forest_ids); }

    DEBUG_MESSAGE("cut located by add_to_forest() with index: ", forest_positions.forest_ids);

}

fermion_line_index cutF::get_fermion_line_index(Particle* p) const {
    for(size_t r=0;r<rung_count;r++){
        if(cut_rungs[r].is_contained_inside(p)){
            return cut_rungs[r].get_fermion_line_index(p);
        }
    }
    for(size_t r=0;r<2;r++){
        if(vertex_end[r].is_contained(p)){
            return vertex_end[r].get_fermion_line_index(p);
        }
    }
    std::cerr<<"ERROR: particle pointer "<<p<<" was not found in any of the contained rungs!"<<std::endl;
    std::exit(1);
    return {false, 0};
}

int cutF::get_internal_index(Particle* p) const {
    for(size_t r=0;r<rung_count;r++){
        if(cut_rungs[r].is_contained_inside(p)){
            return cut_rungs[r].get_internal_index(p);
        }
    }
    for(size_t r=0;r<2;r++){
        if(vertex_end[r].is_contained(p)){
            return vertex_end[r].get_internal_index(p);
        }
    }
    std::cerr<<"ERROR: particle pointer "<<p<<" was not found in any of the contained rungs!"<<std::endl;
    std::exit(1);
    return 0;
}

fermion_line_index cutF::get_link_fermion_line_index(size_t ii) const {
    if(rung_count==1)
        return cut_rungs[0].get_link_fermion_line_index(ii);
    size_t max = structure->partition.size()+1;
    if(ii>max){
        std::cerr<<"ERROR: cutF::get_link_fermion_line_index("<<ii<<") called with too large argument, max: "<<max<<std::endl;
        std::exit(1);
    }
    // two loop case
    size_t upto = cut_rungs[0].get_multiplicity()+1;
    if(ii<upto)
        return cut_rungs[0].get_link_fermion_line_index(ii);
    size_t start = upto;
    upto+=cut_rungs[1].get_multiplicity()+1;
    if(ii<upto){
        if(rung_count>2)
            return cut_rungs[1].get_link_fermion_line_index(cut_rungs[1].get_multiplicity()-(ii-start));
        else
            return cut_rungs[1].get_link_fermion_line_index(ii-start);
    }
    start = upto;
    upto+=cut_rungs[2].get_multiplicity()+1;
    if(ii<upto&&rung_count>2)
        return cut_rungs[2].get_link_fermion_line_index(ii-start);
    std::cerr<<"ERROR: cutF::get_link_fermion_line_index("<<ii<<") came to the end, max: "<<max<<std::endl;
    std::exit(1);
    return {false, 0};
}

// construct new cutF from counterclockwise rotations
cutF cutF::counterclockwise_rotate(size_t steps,std::vector<size_t> iv){
	// can't rotate a cut without rungs
	if(rung_count==0 ||
		// 1-loop should have empty vector
		(rung_count==1&&iv.size()!=0) ||
		// Bow tie should have empty vector
		(rung_count==2&&iv.size()!=0) ||
		// General two loop wither empty vector or two components
		(rung_count==3&&iv.size()!=2&&iv.size()!=0) ||
		// no more than 2 loops
		rung_count>3
	  ){
		std::cout<<"cutF::counterclockwise_rotate() called with wrong parameters -- Did nothing"<<std::endl;
		return cutF();
	}
	// 1-loop case
	if(rung_count==1){
		// it is a 1-loop cut, so I assume no top or bottom vertex relevant
		std::vector<Particle*> new_order;
		new_order.insert(new_order.end(),(cut_rungs->get_particles()).begin(),(cut_rungs->get_particles()).end());
		// counter clockwise roration on particles is a simple rotate right steps
		std::rotate(new_order.rbegin(),new_order.rbegin()+steps,new_order.rend());
#if 0
std::cout<<"after rotation:"<<std::endl;
for(size_t ii=0;ii<new_order.size();ii++)
	std::cout<<new_order[ii]->get_name()<<" ";
std::cout<<std::endl;
#endif
		if(cut_rungs->get_multiplicity()==0){
			std::cout<<"Trying to rotate 1-loop cutF with  cutF::counterclockwise_rotate(.) -- makes no sense!"<<std::endl;
		}
		RungF modifiedRungF(cut_rungs->map_particles(new_order,Vertex(),Vertex()));
		return cutF(modifiedRungF);
	}
	// 2-loop case
	else{
		if(rung_count==2){
			std::cout<<"NO BOWTIE counterclockwise_rotate() implementation yet -- Returned default cutF"<<std::endl;
			return cutF();
		}
		else{

			// default value for general two loops in case empty iv
			if(iv.size()==0){
				// left rung
				iv.push_back(0);
				// right rung
				iv.push_back(2);
			}
			if(iv[0]>=iv[1]){
				std::cout<<"counterclockwise_rotate() should receive increasing rungs -- Returned default cutF"<<std::endl;
				return cutF();
			}
			// here I keep all particles in the order: (top vertex,1st rung,bottom vertex,2nd rung)
			// and rotate it accordingly
			std::vector<Particle*> new_order;
			new_order.insert(new_order.end(),vertex_end[0].get_particles().rbegin(),vertex_end[0].get_particles().rend());
			new_order.insert(new_order.end(),cut_rungs[iv[0]].get_particles().begin(),cut_rungs[iv[0]].get_particles().end());
			new_order.insert(new_order.end(),vertex_end[1].get_particles().begin(),vertex_end[1].get_particles().end());
			new_order.insert(new_order.end(),cut_rungs[iv[1]].get_particles().rbegin(),cut_rungs[iv[1]].get_particles().rend());
			// counter clockwise roration on particles is a simple rotate right steps
			std::rotate(new_order.rbegin(),new_order.rbegin()+steps,new_order.rend());
#if 0
std::cout<<"after rotation:"<<std::endl;
for(size_t ii=0;ii<new_order.size();ii++)
	std::cout<<new_order[ii]->get_name()<<" ";
std::cout<<std::endl;
#endif
			size_t current_particle(0);
			std::vector<Particle*> temp;
			temp.insert(temp.end(),new_order.begin()+current_particle,new_order.begin()+current_particle+(vertex_end[0].get_particles()).size());
			current_particle+=(vertex_end[0].get_particles()).size();
			// construct top vertex
			// Add couplings assuming only gluonic amplitudes
			Vertex top_vertex(temp, vertex_end[0].get_coupling_powers());
			temp.clear();
			temp.insert(temp.end(),new_order.begin()+current_particle,new_order.begin()+current_particle+(cut_rungs[iv[0]].get_particles()).size());
			current_particle+=(cut_rungs[iv[0]].get_particles()).size();
			// now new 1st rung
			RungF first_new_rung(cut_rungs[iv[0]].map_particles(temp,Vertex(),Vertex()));
			temp.clear();
			temp.insert(temp.end(),new_order.begin()+current_particle,new_order.begin()+current_particle+(vertex_end[1].get_particles()).size());
			current_particle+=(vertex_end[1].get_particles()).size();
			// construct bottom vertex
			// Add couplings assuming only gluonic amplitudes
			Vertex bottom_vertex(temp, vertex_end[1].get_coupling_powers());
			temp.clear();
			size_t reverse_order(new_order.size()-(current_particle+(cut_rungs[iv[1]].get_particles()).size()));
			temp.insert(temp.end(),new_order.rbegin()+reverse_order,new_order.rbegin()+reverse_order+(cut_rungs[iv[1]].get_particles()).size());
			current_particle+=(cut_rungs[iv[1]].get_particles()).size();
			// now new 1st rung with attached vertices
			RungF second_new_rung(cut_rungs[iv[1]].map_particles(temp,top_vertex,bottom_vertex));

			std::vector<RungF> new_rungs;
			if(iv[0]==0&&iv[1]==1){ new_rungs.push_back(first_new_rung); new_rungs.push_back(second_new_rung); new_rungs.push_back(cut_rungs[2]);}
			if(iv[0]==0&&iv[1]==2){ new_rungs.push_back(first_new_rung); new_rungs.push_back(cut_rungs[1]); new_rungs.push_back(second_new_rung);}
			if(iv[0]==1&&iv[1]==2){ new_rungs.push_back(cut_rungs[0]); new_rungs.push_back(first_new_rung); new_rungs.push_back(second_new_rung);}
			return cutF(new_rungs);
		}
	}
}

// == check for 1-loop cuts considering cyclic permutations (rotation)
bool cutF::check_1loop_equal(const cutF& ic) const {
	// first check both cuts are 1 loop
	if(this->rung_count!=1||ic.rung_count!=1)
		return false;
	// also, to be 1-loop, it is necessary that top and bottom links match (it is the same particle)
	if((this->cut_rungs->links).size()>0&&(ic.cut_rungs->links).size()>0){
		if(!((this->cut_rungs->links)[0]==this->cut_rungs->endlink))
			return false;
		if(!((ic.cut_rungs->links)[0]==ic.cut_rungs->endlink))
			return false;
	}
	else
		// there has to be first links!
		return false;
	// make sure there are particles
	if((this->cut_rungs->get_particles()).size()==0)
		return false;
	if((ic.cut_rungs->get_particles()).size()==0)
		return false;
	// particle count should match
	if((this->cut_rungs->get_particles()).size()!=(ic.cut_rungs->get_particles()).size())
		return false;

	// find out the needed rotation for a possible match
	Particle* first_part((ic.cut_rungs->get_particles())[0]);
	size_t steps(0);
	for(size_t ii=0;ii<this->cut_rungs->get_vertices().size();ii++){
		if(((this->cut_rungs->get_vertices())[ii].get_particles()).size()>0){
			if(first_part==((this->cut_rungs->get_vertices())[ii].get_particles())[0]){
				break;
			}
		}
		steps++;
	}
	// did not find it
	if(steps==this->cut_rungs->get_vertices().size())
		return false;
	// already aligned: compare rungs
	if(steps==0)
		return *(this->cut_rungs)==*ic.cut_rungs;
	// get the number of steps to rotate *this
	size_t torotate_steps(this->cut_rungs->get_vertices().size()-steps);
	// I construct then a rotated 1-loop cut and compare their rungs!
	cutF temp(this->rotate_1loop(torotate_steps));
//std::cout<<"Rotated cut! step: "<<torotate_steps<<"("<<steps<<") *this: "<<this->get_short_name()<<" in cut: "<<ic.get_short_name()<<std::endl;
//temp.show();
	return ic==temp;
}

cutF cutF::rotate_1loop(int in) const {
	return cutF(this->cut_rungs->rotate_rung(in));
}

size_t cutF::get_prop_count() const {
	if(rung_count==1){
		// # of vertices matches # of propagators in 1-loop cuts
		return cut_rungs->get_multiplicity();
	}
	// bow tie
	else if(rung_count==2){
		return cut_rungs->get_multiplicity()+(cut_rungs+1)->get_multiplicity()+2;
	}
	else if(rung_count==3){
		return cut_rungs->get_multiplicity()+(cut_rungs+1)->get_multiplicity()+(cut_rungs+2)->get_multiplicity()+3;
	}
	else{
		_WARNING("Only handling 1- and 2-loop cuts -- cutF::get_prop_count() returned 0");
		return 0;
	}
}

// tests organized in order of complexity
bool operator==(const cutF& c1,const cutF& c2){
	// number of rungs should match
	if(c1.rung_count!=c2.rung_count)
		return false;
	// only 1- and 2- loop covered
	if(c1.rung_count==0||c1.rung_count>3){
		std::cout<<"cut == cut returns false always if cuts are not either 1- or 2-loop!!"<<std::endl;
		return false;
	}
	// 1-loop case
	if(c1.rung_count==1){
		// check if unique rungs match
		if(*(c1.cut_rungs)==*(c2.cut_rungs))
			return true;
		else
			return c1.check_1loop_equal(c2);
	}
	// Bow-tie 2-loop case
	if(c1.rung_count==2){
#if 0
		// sizes related to c1
		size_t c1_nv1(c1.cut_rungs[0].get_vertices().size()); size_t c1_nv2(c1.cut_rungs[1].get_vertices().size());
		size_t c1_np1(c1.cut_rungs[0].get_particles().size()); size_t c1_np2(c1.cut_rungs[1].get_particles().size());
		// sizes related to c2
		size_t c2_nv1(c2.cut_rungs[0].get_vertices().size()); size_t c2_nv2(c2.cut_rungs[1].get_vertices().size());
		size_t c2_np1(c2.cut_rungs[0].get_particles().size()); size_t c2_np2(c2.cut_rungs[1].get_particles().size());
		// proper order counting match
		if(c1_nv1==c2_nv1&&c1_nv2==c2_nv2&&c1_np1==c2_np1&&c1_np2==c2_np2){
			// full alignement
			if(!(c1.vertex_end[0]==c2.vertex_end[0]))
				return false;
			if(!(c1.vertex_end[1]==c2.vertex_end[1]))
				return false;
			return *(c1.cut_rungs)==*(c2.cut_rungs)&&*(c1.cut_rungs+1)==*(c2.cut_rungs+1);
		}
		// flip order counting match
		else if(c1_nv1==c2_nv2&&c1_nv2==c2_nv1&&c1_np1==c2_np2&&c1_np2==c2_np1){
			std::cout<<"Possibility of match cutF::operator== in 2-loop bow tie -- not yet properly implemented"<<std::endl;
			return false;
		}
		else
			return false;
#endif
		// sizes related to c1
		size_t c1_nvL(c1.cut_rungs[0].get_vertices().size()); size_t c1_nvR(c1.cut_rungs[1].get_vertices().size());
		size_t c1_npL(c1.cut_rungs[0].get_particles().size()); size_t c1_npR(c1.cut_rungs[1].get_particles().size());
		// sizes related to c2
		size_t c2_nvL(c2.cut_rungs[0].get_vertices().size()); size_t c2_nvR(c2.cut_rungs[1].get_vertices().size());
		size_t c2_npL(c2.cut_rungs[0].get_particles().size()); size_t c2_npR(c2.cut_rungs[1].get_particles().size());
		// clear ordering, more vertices in left
		if(c1_nvL>c1_nvR){
			// check size matching
			if(c1_nvL!=c2_nvL||c1_nvR!=c2_nvR||c1_npL!=c2_npL||c1_npR!=c2_npR)
				return false;
			// direct match
			if( c1.vertex_end[0]==c2.vertex_end[0]&&
				c1.vertex_end[1]==c2.vertex_end[1]&&
				*(c1.cut_rungs)==*(c2.cut_rungs)&&
				*(c1.cut_rungs+1)==*(c2.cut_rungs+1)){
				return true;
			}
			// flips
			else{
				// needed vertices for flips
				//Vertex new_v_fromT1(c1.vertex_end[0].flip());
				//Vertex new_v_fromB1(c1.vertex_end[1].flip());
				RungF new_r_frmL(c1.cut_rungs->flip());
				RungF new_r_frmR((c1.cut_rungs+1)->flip());

				// \pi rotation (with flip around central rung for normal ordering)
				if( c1.vertex_end[0]==c2.vertex_end[1]&&
					c1.vertex_end[1]==c2.vertex_end[0]&&
					new_r_frmL==*(c2.cut_rungs)&&
					new_r_frmR==*(c2.cut_rungs+1)){
					return true;
				}
				return false;
			}
		}
		// same amount of vertices
		else if(c1_nvL==c1_nvR){
			if(c2_nvL!=c2_nvR||c2_nvL!=c1_nvL)
				return false;
			// direct match
			if( c1.vertex_end[0]==c2.vertex_end[0]&&
				c1.vertex_end[1]==c2.vertex_end[1]&&
				*(c1.cut_rungs)==*(c2.cut_rungs)&&
				*(c1.cut_rungs+1)==*(c2.cut_rungs+1)){
				return true;
			}
			// flips
			else{
				// needed vertices for flips
				Vertex new_v_fromT1(c1.vertex_end[0].flip());
				Vertex new_v_fromB1(c1.vertex_end[1].flip());
				// flip around central rung
				if( new_v_fromT1==c2.vertex_end[0]&&
					new_v_fromB1==c2.vertex_end[1]&&
					*(c1.cut_rungs+1)==*(c2.cut_rungs)&&
					*(c1.cut_rungs)==*(c2.cut_rungs+1)){
					return true;
				}
				RungF new_r_frmL(c1.cut_rungs->flip());
				RungF new_r_frmR((c1.cut_rungs+1)->flip());

				// \pi rotation
				if( new_v_fromT1==c2.vertex_end[1]&&
					new_v_fromB1==c2.vertex_end[0]&&
					new_r_frmR==*(c2.cut_rungs)&&
					new_r_frmL==*(c2.cut_rungs+1)){
					return true;
				}

				// top-bottom flip
				if( c1.vertex_end[0]==c2.vertex_end[1]&&
					c1.vertex_end[1]==c2.vertex_end[0]&&
					new_r_frmR==*(c2.cut_rungs+1)&&
					new_r_frmL==*(c2.cut_rungs)){
					return true;
				}

				return false;
			}
		}
		else{
			std::cout<<"Problem! Bow tie. First rung should have the largest number of vertices -- cutF::operator== returned false"<<std::endl;
			return false;
		}
	}
	// general 2-loop case
	if(c1.rung_count==3){
		size_t count_last_prop1(c1.cut_rungs[1].get_vertices().size());
		size_t count_last_prop2(c2.cut_rungs[1].get_vertices().size());
		// minimum amount of vertices in rungs should match
		if(count_last_prop1!=count_last_prop2)
			return false;
		// No planar case, central rung contains vertices
		if(count_last_prop1>0){
			std::cout<<"Careful! cutF::operator== in 2-loop non-planar case not implemented yet! Assumed false"<<std::endl;
			return false;
		}
		// check that the central rung on planar cases match
		if(!(*(c1.cut_rungs+1)==*(c2.cut_rungs+1)))
			return false;
		// follow checks for planar cases
		// sizes related to c1
		size_t c1_nvL(c1.cut_rungs[0].get_vertices().size()); size_t c1_nvR(c1.cut_rungs[2].get_vertices().size());
		size_t c1_npL(c1.cut_rungs[0].get_particles().size()); size_t c1_npR(c1.cut_rungs[2].get_particles().size());
		// sizes related to c2
		size_t c2_nvL(c2.cut_rungs[0].get_vertices().size()); size_t c2_nvR(c2.cut_rungs[2].get_vertices().size());
		size_t c2_npL(c2.cut_rungs[0].get_particles().size()); size_t c2_npR(c2.cut_rungs[2].get_particles().size());
		// clear ordering, more vertices in left
		if(c1_nvL>c1_nvR){
			// check size matching
			if(c1_nvL!=c2_nvL||c1_nvR!=c2_nvR||c1_npL!=c2_npL||c1_npR!=c2_npR)
				return false;
			// direct match
			if( c1.vertex_end[0]==c2.vertex_end[0]&&
				c1.vertex_end[1]==c2.vertex_end[1]&&
				*(c1.cut_rungs)==*(c2.cut_rungs)&&
				*(c1.cut_rungs+2)==*(c2.cut_rungs+2)){
				return true;
			}
			// flips
			else{
				// needed vertices for flips
				//Vertex new_v_fromT1(c1.vertex_end[0].flip());
				//Vertex new_v_fromB1(c1.vertex_end[1].flip());
				RungF new_r_frmL(c1.cut_rungs->flip());
				RungF new_r_frmR((c1.cut_rungs+2)->flip());
#if 0
std::cout<<"Review flipped Rungs:"<<std::endl;
std::cout<<"new_v_fromT1: "<<new_v_fromT1.get_name()<<std::endl;
std::cout<<"v2B: "<<c2.vertex_end[1].get_name()<<std::endl;
std::cout<<"new_v_fromB1: "<<new_v_fromB1.get_name()<<std::endl;
std::cout<<"v2T: "<<c2.vertex_end[0].get_name()<<std::endl;
new_r_frmL.show();
new_r_frmR.show();
std::cout<<"v1T == v2B: "<<((new_v_fromT1==c2.vertex_end[1])?"true":"false")<<std::endl;
std::cout<<"v1B == v2T: "<<((new_v_fromB1==c2.vertex_end[0])?"true":"false")<<std::endl;
std::cout<<"r1LF== r2L: "<<((new_r_frmL==*(c2.cut_rungs))?"true":"false")<<std::endl;
std::cout<<"r1RF== r2R: "<<((new_r_frmR==*(c2.cut_rungs+2))?"true":"false")<<std::endl;
#endif
				// \pi rotation (with flip around central rung for normal ordering)
				if( c1.vertex_end[0]==c2.vertex_end[1]&&
					c1.vertex_end[1]==c2.vertex_end[0]&&
					new_r_frmL==*(c2.cut_rungs)&&
					new_r_frmR==*(c2.cut_rungs+2)){
					return true;
				}
				return false;
			}
		}
		// same amount of vertices
		else if(c1_nvL==c1_nvR){
			if(c2_nvL!=c2_nvR||c2_nvL!=c1_nvL)
				return false;
			// direct match
			if( c1.vertex_end[0]==c2.vertex_end[0]&&
				c1.vertex_end[1]==c2.vertex_end[1]&&
				*(c1.cut_rungs)==*(c2.cut_rungs)&&
				*(c1.cut_rungs+2)==*(c2.cut_rungs+2)){
				return true;
			}
			// flips
			else{
				// needed vertices for flips
				Vertex new_v_fromT1(c1.vertex_end[0].flip());
				Vertex new_v_fromB1(c1.vertex_end[1].flip());
				// flip around central rung
				if( new_v_fromT1==c2.vertex_end[0]&&
					new_v_fromB1==c2.vertex_end[1]&&
					*(c1.cut_rungs+2)==*(c2.cut_rungs)&&
					*(c1.cut_rungs)==*(c2.cut_rungs+2)){
					return true;
				}
				RungF new_r_frmL(c1.cut_rungs->flip());
				RungF new_r_frmR((c1.cut_rungs+2)->flip());
#if 0
std::cout<<"Review flipped Rungs:"<<std::endl;
std::cout<<"new_v_fromT1: "<<new_v_fromT1.get_name()<<" new_v_fromB1: "<<new_v_fromB1.get_name()<<std::endl;
new_r_frmL.show();
new_r_frmR.show();
#endif
				// \pi rotation
				if( new_v_fromT1==c2.vertex_end[1]&&
					new_v_fromB1==c2.vertex_end[0]&&
					new_r_frmR==*(c2.cut_rungs)&&
					new_r_frmL==*(c2.cut_rungs+2)){
					return true;
				}

				// top-bottom flip
				if( c1.vertex_end[0]==c2.vertex_end[1]&&
					c1.vertex_end[1]==c2.vertex_end[0]&&
					new_r_frmR==*(c2.cut_rungs+2)&&
					new_r_frmL==*(c2.cut_rungs)){
					return true;
				}

				return false;
			}
		}
		else{
			std::cout<<"Problem! First rung should have the largest number of vertices -- cutF::operator== returned false"<<std::endl;
			return false;
		}
	}
	std::cout<<"Came to the end of cutF::operator== and no checks -- Returned false"<<std::endl;
	return false;
}

std::string cutF::get_short_name() const {
	return name+" "+name_id;
}

cutF cutF::pinch(size_t dropp) const {
	if(rung_count==0||rung_count>3){
		// nothing to do
		_WARNING("pinch() called on cutF not handled -- returned empty cutF");
		return cutF();
	}
	// check out of bound
	else if(dropp>=this->get_prop_count()){
		std::cout<<"ERROR: called cutF::pinch() with index "<<dropp<<" when # of props is "<<this->get_prop_count()<<" -- returned empty cutF"<<std::endl;
		return cutF();
	}
	// 1-loop cut
	else if(rung_count==1){
		return cutF(cut_rungs->one_loop_pinch(dropp));
	}
	// 2-loop bow tie
	else if(rung_count==2){
		if(dropp<cut_rungs->get_multiplicity()+1){
			// we are pinching first propagator in a left rung, treat separately
			if(dropp==0)
				return cutF(std::vector<RungF>({cut_rungs->pinch_top_reverse(vertex_end[0],vertex_end[1]),*(cut_rungs+1)}));
			else
				return cutF(std::vector<RungF>({cut_rungs->pinch(dropp,vertex_end[0],vertex_end[1]),*(cut_rungs+1)}));
		}
		else{
			size_t ptodrop(dropp-cut_rungs->get_multiplicity()-1);
			// we are pinching bottom propagator in a right rung, treat separately
			if(ptodrop==(cut_rungs+1)->get_multiplicity())
				return cutF(std::vector<RungF>({*cut_rungs,(cut_rungs+1)->pinch_bottom_reverse(vertex_end[0],vertex_end[1])}));
			else if(ptodrop==0)
				return cutF(std::vector<RungF>({*cut_rungs,(cut_rungs+1)->pinch_top_of_right_rung(vertex_end[0],vertex_end[1])}));
			else
				return cutF(std::vector<RungF>({*cut_rungs,(cut_rungs+1)->pinch(ptodrop,vertex_end[0],vertex_end[1])}));
		}
	}
	// other 2-loop
	else if(rung_count==3){
		if(dropp<cut_rungs->get_multiplicity()+1){
			// we are pinching first propagator in a left rung, treat separately
			if(dropp==0)
				return cutF(std::vector<RungF>({cut_rungs->pinch_top_reverse(vertex_end[0],vertex_end[1]),*(cut_rungs+1),*(cut_rungs+2)}));
			else
				return cutF(std::vector<RungF>({cut_rungs->pinch(dropp,vertex_end[0],vertex_end[1]),*(cut_rungs+1),*(cut_rungs+2)}));
		}
		else if(dropp<cut_rungs->get_multiplicity()+(cut_rungs+1)->get_multiplicity()+2){
			size_t ptodrop(dropp-cut_rungs->get_multiplicity()-1);
			if((cut_rungs+1)->get_multiplicity()>0&&(ptodrop==0||ptodrop==(cut_rungs+1)->get_multiplicity()))
				std::cout<<"PINCHING top or bottom central prop in non-planar case!! -- Doing default ordering!! "<<std::endl;
			return cutF(std::vector<RungF>({*cut_rungs,(cut_rungs+1)->pinch(ptodrop,vertex_end[0],vertex_end[1]),*(cut_rungs+2)}));
		}
		else{
			size_t ptodrop(dropp-cut_rungs->get_multiplicity()-(cut_rungs+1)->get_multiplicity()-2);
			// we are pinching bottom propagator in a right rung, treat separately
			if(ptodrop==(cut_rungs+2)->get_multiplicity())
				return cutF(std::vector<RungF>({*cut_rungs,*(cut_rungs+1),(cut_rungs+2)->pinch_bottom_reverse(vertex_end[0],vertex_end[1])}));
			else if(ptodrop==0)
				return cutF(std::vector<RungF>({*cut_rungs,*(cut_rungs+1),(cut_rungs+2)->pinch_top_of_right_rung(vertex_end[0],vertex_end[1])}));
			else
				return cutF(std::vector<RungF>({*cut_rungs,*(cut_rungs+1),(cut_rungs+2)->pinch(ptodrop,vertex_end[0],vertex_end[1])}));
		}
	}
	return cutF();
}

cutF cutF::multi_pinch(std::vector<size_t> drops) const {
	// we always sort copied argument
	std::sort(drops.begin(),drops.end());
	if(rung_count<1||rung_count>3){
		std::cout<<"cutF::multi_pinch() only works for 1- and 2-loop cuts. Returned empty cutF"<<std::endl;
		return cutF();
	}
	// 1-loop case can be easily achieved with nested pinchs
	else if(rung_count==1){
		cutF toret(*this);
		size_t pinchs_counter(0);
		for(size_t ii=0;ii<drops.size();ii++){
			toret=toret.pinch(drops[ii]-pinchs_counter);
			pinchs_counter++;
		}
		return toret;
	}

	// now 2-loop cuts

	// first find the vectors of propagator drops on each rung
	std::vector<std::vector<size_t> > drops_per_rung;

	size_t props1(this->cut_rungs->get_multiplicity()+1);
	std::vector<size_t> rung1_drops;
	for(size_t ii=0;ii<drops.size();ii++){
		if(drops[ii]<props1){
			rung1_drops.push_back(drops[ii]);
		}
		else{
			break;
		}
	}
	// store it in the global container
	drops_per_rung.push_back(rung1_drops);

	// parent is bow tie
	if(rung_count==2){
		std::vector<size_t> rung2_drops;
		for(size_t ii=0;ii<drops.size();ii++){
			if(drops[ii]>=props1){
				rung2_drops.push_back(drops[ii]-props1);
			}
		}
		// store it in the global container
		drops_per_rung.push_back(rung2_drops);

		// we can now construct both multi-pinched rungs
		std::vector<RungF> newrungs;
		for(size_t ii=0;ii<2;ii++)
			newrungs.push_back((this->cut_rungs+ii)->multi_pinch(drops_per_rung[ii]));
		// fix top and bottom vertices in cut by passing them to the first rung
		newrungs[0].vertex_end[0]=newrungs[0].vertex_end[0]*vertex_end[0];
		newrungs[0].vertex_end[1]=newrungs[0].vertex_end[1]*vertex_end[1];
		// whatever was accumulated on vertices of right rung, should be flipped
		newrungs[1].vertex_end[0]=newrungs[1].vertex_end[0].flip();
		newrungs[1].vertex_end[1]=newrungs[1].vertex_end[1].flip();

		// and retunr cut
		return cutF(newrungs);
	}
	// parent has three rungs
	else{
		size_t props2((this->cut_rungs+1)->get_multiplicity()+1+props1);
		std::vector<size_t> rung2_drops;
		for(size_t ii=0;ii<drops.size();ii++){
			if(drops[ii]>=props1&&drops[ii]<props2){
				// should be traced backward
				rung2_drops.push_back(props2-props1-1-(drops[ii]-props1));
			}
		}
		// store it in the global container
		drops_per_rung.push_back(rung2_drops);

		// now final rung
		std::vector<size_t> rung3_drops;
		for(size_t ii=0;ii<drops.size();ii++){
			if(drops[ii]>=props2){
				rung3_drops.push_back(drops[ii]-props2);
			}
		}
		// store it in the global container
		drops_per_rung.push_back(rung3_drops);

		// we can now construct both multi-pinched rungs
		std::vector<RungF> newrungs;
		for(size_t ii=0;ii<3;ii++)
			newrungs.push_back((this->cut_rungs+ii)->multi_pinch(drops_per_rung[ii]));
		// fix top and bottom vertices in cut by passing them to the first rung
		newrungs[0].vertex_end[0]=newrungs[0].vertex_end[0]*vertex_end[0];
		newrungs[0].vertex_end[1]=newrungs[0].vertex_end[1]*vertex_end[1];
		// whatever was accumulated on vertices of right rung, should be flipped
		newrungs[2].vertex_end[0]=newrungs[2].vertex_end[0].flip();
		newrungs[2].vertex_end[1]=newrungs[2].vertex_end[1].flip();

		// WARNING about possible non-planar cases
		if((this->cut_rungs+1)->get_multiplicity()>0)
			std::cout<<"WARNING: cutF::multi_pinch() not handling properly non-planar cases!! this: "<<this->get_short_name()<<std::endl;

		// and retunr cut
		return cutF(newrungs);
	}
}

bool cutF::has_scaleless_int() const {
	if(rung_count==0||rung_count>3){
		std::cout<<"Only handling 1- and 2-loop cuts! cutF::has_scaleless_int() returned false"<<std::endl;
		return false;
	}
	// 1-loop case
	else if(rung_count==1){
		if(cut_rungs->get_multiplicity()==1){
			if((cut_rungs->endlink).get_particle().is_massless()){
				return true;
			}
		}
		else if(cut_rungs->get_multiplicity()==2){
			if(cut_rungs->get_vertices()[0].get_multiplicity()==1){
				// check if the unique particle is massless and the corresponding propagators
				if(cut_rungs->get_vertices()[0].get_particles()[0]->is_massless()){
					if(cut_rungs->all_massless_internal_links()){
						return true;
					}
					else{
#if 0
						std::cout<<"Found massive bubble! Printed next!"<<std::endl;
						cut_rungs->show();
#endif
					}
				}
			}
			if(cut_rungs->get_vertices()[1].get_multiplicity()==1){
				// check if the unique particle is massless and the corresponding propagators
				if(cut_rungs->get_vertices()[1].get_particles()[0]->is_massless()){
					if(cut_rungs->all_massless_internal_links()){
						return true;
					}
					else{
#if 0
						std::cout<<"Found massive bubble! Printed next!"<<std::endl;
						cut_rungs->show();
#endif
					}
				}
			}
		}
		return false;
	}
	// bow tie
	else if(rung_count==2){
		// always second rung has minimum amount of vertices
		if((cut_rungs+1)->get_multiplicity()==0){
			if(((cut_rungs+1)->endlink).get_particle().is_massless()){
			return true;
		}
			else{
				std::cout<<"Found massive bubble!"<<std::endl;
			}
		}
		else if((cut_rungs+1)->get_multiplicity()==1){
			// it could be that first rung has a single vertex that is massless
			if(cut_rungs->get_multiplicity()==1){
				if(cut_rungs->get_vertices()[0].get_multiplicity()==1){
					if((cut_rungs->endlink).get_particle().is_massless()
						&&(cut_rungs->get_links())[0].get_particle().is_massless()
						&&cut_rungs->get_particles()[0]->is_massless()
					){
						return true;
					}
					else{
						std::cout<<"Found first rung in factorized 2-loop cut with a bubble with a single particle particle attached non massless"<<std::endl;
					}
				}
			}
			if((cut_rungs+1)->get_vertices()[0].get_multiplicity()>1){
				return false;
			}
			else{
				if(((cut_rungs+1)->endlink).get_particle().is_massless()
					&&((cut_rungs+1)->get_links())[0].get_particle().is_massless()
					&&(cut_rungs+1)->get_particles()[0]->is_massless()
				){
					return true;
				}
				else{
					std::cout<<"Found second rung in factorized 2-loop cut with a bubble with a single particle particle attached non massless"<<std::endl;
				}
			}
		}
	}
	// general 2-loop case
	else if(rung_count==3){
		// to have a scaleless with three rungs you need:
		// 	1) at least one rung without vertices
		//	2) no internal masses (ASSUMED HERE, not checked)
		//	3) either:
		//		3.a) Two vertices, with one with a single massless leg
		//		3.b) A single vertex
		// always second rung has minimum amount of vertices
		if((cut_rungs+1)->get_multiplicity()==0&&(cut_rungs+1)->all_massless_internal_links()){
			// counting total number of vertices in cut
			size_t n_of_vertices(0);
			// track if there is a 1-particle vertex
			bool has_1_part_vertex(false);
			bool the_single_particle_is_massless(false);
			// check top/bottom vertices
			if(vertex_end[0].get_particles().size()>0){
				n_of_vertices++;
				if(vertex_end[0].get_particles().size()==1){
					has_1_part_vertex=true;
					the_single_particle_is_massless=vertex_end[0].get_particles()[0]->is_massless();
				}
			}
			if(vertex_end[1].get_particles().size()>0){
				n_of_vertices++;
				if(vertex_end[1].get_particles().size()==1){
					has_1_part_vertex=true;
					the_single_particle_is_massless=vertex_end[1].get_particles()[0]->is_massless();
				}
			}
			if(!cut_rungs->all_massless_internal_links()){
				std::cout<<"Found first rung with massive link(s)!"<<std::endl;
				return false;
			}
			if(cut_rungs->get_multiplicity()>0){
				n_of_vertices+=cut_rungs->get_multiplicity();
				for(size_t ii=0;ii<cut_rungs->get_vertices().size();ii++){
					if(cut_rungs->get_vertices()[ii].get_particles().size()==1){
						has_1_part_vertex=true;
						the_single_particle_is_massless=cut_rungs->get_vertices()[ii].get_particles()[0]->is_massless();
						break;
					}
				}
			}
			if(!(cut_rungs+2)->all_massless_internal_links()){
				std::cout<<"Found third rung with massive link(s)!"<<std::endl;
				return false;
			}
			if((cut_rungs+2)->get_multiplicity()>0){
				n_of_vertices+=(cut_rungs+2)->get_multiplicity();
				for(size_t ii=0;ii<(cut_rungs+2)->get_vertices().size();ii++){
					if((cut_rungs+2)->get_vertices()[ii].get_particles().size()==1){
						has_1_part_vertex=true;
						the_single_particle_is_massless=(cut_rungs+2)->get_vertices()[ii].get_particles()[0]->is_massless();
						break;
					}
				}
			}

			// vacuum bubble ??
			if(n_of_vertices==0){
				return true;
			}
			else if(n_of_vertices==1){
				return true;
			}
			else if(n_of_vertices==2){
				// two vertices only and one leg has a single particle
				if(has_1_part_vertex){
					if(the_single_particle_is_massless){
					return true;
				}
			}
		}
	}
		else if(!(cut_rungs+1)->all_massless_internal_links()){
			std::cout<<"Found second rung with massive link(s)!"<<std::endl;
			return false;
		}
	}
	return false;
}

bool cutF::has_sub_bubble() const {
	if(rung_count==0||rung_count>3){
		std::cout<<"Only handling 1- and 2-loop cuts! cutF::has_sub_bubble() returned false"<<std::endl;
		return false;
	}
	// 1-loop case
	else if(rung_count==1){
		// sub bubbles only appear at two loops
		return false;
	}
	// bow tie
	else if(rung_count==2){
		// bow ties have no sub bubbles
		return false;
	}
	// general two loop case
	else if(rung_count==3){
		// if sub bubble, two rungs should have 0 vertices
		if((cut_rungs+1)->get_multiplicity()==0&&(cut_rungs+2)->get_multiplicity()==0)
			return true;
	}
	return false;
}

bool cutF::is_semi_simple() const {
	if(rung_count==0||rung_count>3){
		std::cout<<"Only handling 1- and 2-loop cuts! cutF::is_semi_simple() returned false"<<std::endl;
		return false;
	}
	// 1-loop case
	else if(rung_count==1){
		// always false
		return false;
	}
	// 2-loop cases
	else if(rung_count==2||rung_count==3){
		// bow ties have no sub bubbles
		if( (vertex_end[0].get_particles().size()==0&&vertex_end[1].get_particles().size()>0) ||
			(vertex_end[1].get_particles().size()==0&&vertex_end[0].get_particles().size()>0)
		  )
			return true;
	}
	return false;
}

bool cutF::is_doubled_prop_related(const cutF& ic) const {
#if 0
std::cout<<"cutF TESTING doubled prop: "<<this->get_short_name()<<" with "<<ic.get_short_name()<<std::endl;
#endif
	// only 2-loop 3-rung cuts can be double-prop related
	if(rung_count!=3){
		return false;
	}
	// sanity check, both cuts should be semi-simple and contain bubble
	if(! (this->has_sub_bubble()&&this->is_semi_simple()
		&& ic.has_sub_bubble()&&ic.is_semi_simple()) ){
		return false;
	}

	// both cuts passed are semi-simple and contain a bubble. If they have the same set of vertices (maybe "flipped"), then we have a match
	std::vector<Vertex> this_vertices;
	if(vertex_end[0].get_multiplicity()>0)
		this_vertices.push_back(vertex_end[0].flip());
	for(size_t ii=0;ii<cut_rungs->get_multiplicity();ii++)
		this_vertices.push_back(cut_rungs->vertices[ii]);
	if(vertex_end[1].get_multiplicity()>0)
		this_vertices.push_back(vertex_end[1]);

	// do the proper with passed cut
	std::vector<Vertex> passed_vertices;
	if(ic.vertex_end[0].get_multiplicity()>0)
		passed_vertices.push_back(ic.vertex_end[0].flip());
	for(size_t ii=0;ii<ic.cut_rungs->get_multiplicity();ii++)
		passed_vertices.push_back(ic.cut_rungs->vertices[ii]);
	if(ic.vertex_end[1].get_multiplicity()>0)
		passed_vertices.push_back(ic.vertex_end[1]);

	// we do not need to track couplings in the identification of related semi-simple bubble diagrams
	for(size_t ii=0;ii<this_vertices.size();ii++)
		this_vertices[ii].set_to_zero_all_couplings();
	// we do not need to track couplings in the identification of related semi-simple bubble diagrams
	for(size_t ii=0;ii<passed_vertices.size();ii++)
		passed_vertices[ii].set_to_zero_all_couplings();

	if(this_vertices==passed_vertices)
		return true;

	// in case we have a flipped relation
	std::vector<Vertex> this_flipped_vertices;
	if(vertex_end[1].get_multiplicity()>0)
		this_flipped_vertices.push_back(vertex_end[1].flip());
	for(size_t ii=cut_rungs->get_multiplicity();ii>0;ii--)
		this_flipped_vertices.push_back(cut_rungs->vertices[ii-1].flip());
	if(vertex_end[0].get_multiplicity()>0)
		this_flipped_vertices.push_back(vertex_end[0]);

	// we do not need to track couplings in the identification of related semi-simple bubble diagrams
	for(size_t ii=0;ii<this_flipped_vertices.size();ii++)
		this_flipped_vertices[ii].set_to_zero_all_couplings();

	if(this_flipped_vertices==passed_vertices)
		return true;

	return false;
}

cutF::~cutF() {
	if(rung_count==1)
		delete cut_rungs;
	else if(rung_count==2||rung_count==3)
		delete [] cut_rungs;

	if(owns_struct)
		delete structure;

	// now delete loop particles
	//----if(rung_count==1)
	//----	delete loop_particles;
	//----if(rung_count==2||rung_count==3)
	//----	delete [] loop_particles;

	if(loop_mom_conf_location!=nullptr)
		delete [] loop_mom_conf_location;
}

const cutTopology& cutF::get_cutTopology() const {
    return ctopology;
}

const lGraph::xGraph& cutF::get_graph() const {
    return graph;
}

bool cutF::is_one_loop() const {
    return rung_count==1;
}

bool cutF::is_two_loop() const {
    return rung_count==2||rung_count==3;
}

void cutF::trace_fermion_lines() {
    // implementation only valid for 1-loop case
    if(!is_one_loop()){
        std::cerr<<"ERROR: cutF::trace_fermion_lines() only works for 1-loop cases!"<<std::endl;
        show();
        std::exit(1);
    }

    bool ambiguous(false);

    std::vector<size_t> tightpair;
    size_t counter(0);
    for(const auto& v:cut_rungs->get_vertices()){
        counter++;
        if(v.get_multiplicity()>1){
            // check if there are several colored particles
            const auto& parts = v.get_particles();
            bool colorless(true);
            for(auto pp:parts){
                if(pp->is_colored()){
                    colorless=false;
                    break;
                }
            }
            if(!colorless){
                // check if it is a pair quark/antiquark
                if(v.get_multiplicity()==2){
                    if(parts[0]->get_type()==parts[1]->get_anti_type()){
                        tightpair.push_back(counter-1);
                        continue;
                    }
                }
                // check if it is a pair quark/antiquark and a lepton pair
                if(v.get_multiplicity()==4 && v.get_colored().size()==2){
                    tightpair.push_back(counter-1);
                    continue;
                }
                ambiguous=true;
                break;
            }
        }
    }

    if(ambiguous){
        std::cerr<<"ERROR: cutF::trace_fermion_lines() called for an ambiguous case! This call should be made only for parent diagrams"<<std::endl;
        show();
        std::exit(1);
    }

    int start(-1);
    auto links = cut_rungs->get_links();
    for(int ii=0;ii<int((cut_rungs->get_vertices()).size());ii++){
        const auto& thevertex = (cut_rungs->get_vertices())[ii];
        // only simple vertices considered
        if(thevertex.get_multiplicity()>1)
            continue;
        const auto& pp = thevertex.get_particles()[0];
        // it has to be a colored fermion
        if(!(pp->is_colored()))
            continue;
        if((pp->get_statistics()!=ParticleStatistics::fermion)&&(pp->get_statistics()!=ParticleStatistics::antifermion))
            continue;
        const auto& lp = links[ii+1].get_particle();
        // following link needs to be a fermion
        if((lp.get_statistics()!=ParticleStatistics::fermion)&&(lp.get_statistics()!=ParticleStatistics::antifermion))
            continue;
        start=ii;
        break;
    }

    auto is_fermion_or_antifermion = [](const Particle& p) { return p.get_statistics()==ParticleStatistics::fermion || p.get_statistics()==ParticleStatistics::antifermion; };

    // check if it is an nf contribution
    if(start<0){
        if(!is_fermion_or_antifermion(links[0].get_particle()))
            return;
        auto ptype = links[0].get_particle_type();
        bool allsame(true);
        for(auto l:links){
            if(l.get_particle_type()!=ptype){
                allsame=false;
                break;
            }
        }
        if(!allsame)
            return;
        else
            start=0;
    }

    // nothing to do
    if(start<0)
        return;

    int end = start+int((cut_rungs->get_vertices()).size());

    size_t flindex(0);

    // deal first with pairs outside of the loop
    for(auto i:tightpair){
        auto& thevertex = (cut_rungs->get_vertices())[i];
        // special treatment for case with pair quark/antiquark and a lepton pair
        if(thevertex.get_multiplicity()==4) {
            const auto& pp1 = thevertex.get_colored()[0];
            const auto& pp2 = thevertex.get_colored()[1];
            thevertex.set_fermion_line_index(pp1,flindex);
            thevertex.set_fermion_line_index(pp2,flindex);
            flindex++;
        }
        else {
            const auto& pp1 = thevertex.get_particles()[0];
            const auto& pp2 = thevertex.get_particles()[1];
            thevertex.set_fermion_line_index(pp1,flindex);
            thevertex.set_fermion_line_index(pp2,flindex);
            flindex++;
        }
    }

    // to find out if it was an nf
    int continue_assigned_in_loop(0);
    // now the rest
    for(int ii=start;ii<end;ii++){
        int vindex = ii%int((cut_rungs->get_vertices()).size());
        int lindex = (ii+1)%int((cut_rungs->get_vertices()).size());
        const auto& lbp = links[vindex].get_particle();
        const auto& lap = links[lindex].get_particle();
        // fermion line continues
        if(is_fermion_or_antifermion(lbp)&&is_fermion_or_antifermion(lap)){
            continue_assigned_in_loop++;
            (cut_rungs->links)[lindex].set_fermion_line_index(flindex);
            // in case we assigned top link, match endlink
            if(lindex==0)
                (cut_rungs->endlink).set_fermion_line_index(flindex);
        }
        // fermion line starts
        else if(is_fermion_or_antifermion(lap)){
            auto& thevertex = (cut_rungs->get_vertices())[vindex];
            const auto& pp = thevertex.get_particles()[0];
            thevertex.set_fermion_line_index(pp,flindex);
            (cut_rungs->links)[lindex].set_fermion_line_index(flindex);
            // in case we assigned top link, match endlink
            if(lindex==0)
                (cut_rungs->endlink).set_fermion_line_index(flindex);
        }
        // fermion line ends
        else if(is_fermion_or_antifermion(lbp)){
            auto& thevertex = (cut_rungs->get_vertices())[vindex];
            const auto& pp = thevertex.get_particles()[0];
            thevertex.set_fermion_line_index(pp,flindex);
            flindex++;
        }
        // not relevant
        else
            continue;
    }
    if(continue_assigned_in_loop==end-start)
        (cut_rungs->endlink).set_fermion_line_index(flindex);

    cut_rungs->update_info();
    construct_info();
}

bool cutF::has_massive_propagator() const {
    for(size_t ii=0; ii<rung_count; ii++)
        if(!((cut_rungs+ii)->all_massless_internal_links()))
            return true;
    return false;
}

std::pair<int, int> cutF::get_loop_mom_location() const {
    int l1(-1);
    int l2(-1);
    if(rung_count==1)
        l1 = int(n_particles) + 1;
    else if(rung_count==2 || rung_count==3){
        l2 = int( cut_rungs->get_particles().size() ) + 1;
        l2 += int( vertex_end[1].get_multiplicity() );
        l1 = l2 + 1;
        // if non planar, we need to add particles in central rung
        if(rung_count==3)
            l1 += int( (cut_rungs + 1)->get_particles().size() );
    }
    return {l1, l2};
}

void add_rung_vertices_to_cutTopology(cutTopology& ct, const RungF& prung, bool inverse, bool use_initial_particle = false, bool use_last_particle = false,
                                      Particle initialp = Particle(), Particle lastp = Particle()) {
    RungF rung(prung);
    if (inverse) rung = prung.flip();

    auto couplings_per_vertex = rung.get_coupling_powers_per_vertex();
    // "top" and "bottom" vertices are irrelevant here
    auto vertices = rung.get_vertices();
    auto links = rung.get_links();
    assert(vertices.size()+1==links.size());

    std::vector<Particle*> tconst;

    // inner vertices (the only ones considered in this function)
    for (size_t ii = 0; ii < vertices.size(); ii++) {
        Particle llL(links[ii].get_particle());
        tconst = vertices[ii].get_particles();
        auto p = links[ii + 1].get_particle();
        Particle llR("", p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0, 0);
        if(use_initial_particle && ii==0)
            llL = initialp;
        if(use_last_particle && ii+1==vertices.size())
            llR = lastp;
        ct.add_vertex(llL, tconst, llR, couplings_per_vertex[ii]);
    }
}

cutTopology cutF::get_cutTopology_copy() {
    cutTopology ct;

    // construct ct for a 1-loop cut
    if(is_one_loop()){
        auto couplings_per_vertex = cut_rungs[0].get_coupling_powers_per_vertex();
        // "top" and "bottom" vertices are irrelevant
        auto vertices = cut_rungs[0].get_vertices();
        // we should have at least a vertex
        assert(vertices.size()>0);
        auto links = cut_rungs[0].get_links();

        // treat first vertex
        Particle ml(loop_parts[1]);
        std::vector<Particle*> tconst = vertices[0].get_particles();
        auto p = links[1].get_particle();
        Particle lR("",p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0 , 0);
	ct.add_vertex(ml,tconst,lR,couplings_per_vertex[0]);

	// inner vertices
        for(size_t ii=1;ii+1<vertices.size();ii++){
            Particle llL(links[ii].get_particle());
            tconst = vertices[ii].get_particles();
            // the particle in the link passed the first vertex
            auto p = links[ii+1].get_particle();
            Particle llR("",p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0 , 0);
            ct.add_vertex(llL,tconst,llR,couplings_per_vertex[ii]);
        }

        // treat last vertex
        if(vertices.size()>1){
            Particle lL(links[vertices.size()-1].get_particle());
            tconst = vertices.back().get_particles();
            Particle l(loop_parts[0]);
            ct.add_vertex(lL,tconst,l,couplings_per_vertex.back());
        }
    }
    // construct ct for a 2-loop cut
    else if(is_two_loop()){
        // factorized two-loop cut
        if(rung_count==2){
            // rung 0
            add_rung_vertices_to_cutTopology(ct,cut_rungs[0],false,true,false,loop_parts[1]);

            // handle central vertex of cut
            Particle lL(cut_rungs[0].get_links().back().get_particle());
            // particles in the bottom vertex
            std::vector<Particle*> tconst = vertex_end[1].get_particles();
            // l2
            tconst.push_back(&(loop_parts[2]));
            // l1
            tconst.push_back(&(loop_parts[0]));
            // particles in the top vertex
            tconst.insert(tconst.end(),vertex_end[0].get_particles().begin(),vertex_end[0].get_particles().end());
            auto p = cut_rungs[1].get_links().front().get_particle();
            Particle lR("",p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0 , 0);
            // add correctly final right particle in this special case
            if(cut_rungs[1].get_multiplicity() == 0)
                lR = loop_parts[3];
            auto cps = add_couplings(vertex_end[0].get_coupling_powers(),vertex_end[1].get_coupling_powers());
            ct.add_vertex(lL,tconst,lR,cps);

            // rung 1
            add_rung_vertices_to_cutTopology(ct,cut_rungs[1],false,false,true,Particle(),loop_parts[3]);
        }
        // generic (three-rung) two-loop cut
        else{
            // rung 0
            add_rung_vertices_to_cutTopology(ct,cut_rungs[0],false,true,false,loop_parts[1]);

            // handle bottom vertex of cut
            Particle lL(cut_rungs[0].get_links().back().get_particle());
            // particles in the bottom vertex
            std::vector<Particle*> tconst = vertex_end[1].get_particles();
            // l2
            tconst.push_back(&(loop_parts[2]));
            auto lR = cut_rungs[1].get_links().back().get_particle();
            ct.add_vertex(lL,tconst,lR,vertex_end[1].get_coupling_powers());


            // rung 1 (we traverse it backwards)
            add_rung_vertices_to_cutTopology(ct,cut_rungs[1],true);

            // handle top vertex of cut
            auto p = cut_rungs[1].get_links().front().get_particle();
            Particle lL2("",p.get_anti_type(), SingleState("default", p.external_state().get_dtt_index()), 0 , 0);
            // l1
            tconst = { &(loop_parts[0]) };
            // particles in the top vertex
            tconst.insert(tconst.end(),vertex_end[0].get_particles().begin(),vertex_end[0].get_particles().end());
            auto p2 = cut_rungs[2].get_links().front().get_particle();
            Particle lR2("",p2.get_anti_type(), SingleState("default", p2.external_state().get_dtt_index()), 0 , 0);
            // add correctly final right particle in this special case
            if(cut_rungs[2].get_multiplicity() == 0)
                lR2 = loop_parts[3];
            ct.add_vertex(lL2,tconst,lR2,vertex_end[0].get_coupling_powers());


            // rung 2
            add_rung_vertices_to_cutTopology(ct,cut_rungs[2],false,false,true,Particle(),loop_parts[3]);
        }
    }

    return ct;
}

int cutF::get_cut_locate() const {
    if(!forest_positions.forest_ids.empty()) return forest_positions.forest_ids.front();
    else return 0;
}

cutF_min::cutF_min(const cutF& ic): loop_count(0), cut_locate(ic.get_cut_locate()), tree_locate(ic.tree_locate), cut_hierarchy_level(ic.cut_hierarchy_level),
	cut_kin_locate(ic.cut_kin_locate),name(ic.name+" "+ic.name_id), n_particles(ic.n_particles), parent_cuts(ic.parent_cuts), 
	accepts_direct_fit(ic.accepts_direct_fit), location_loop_1(0), location_loop_2(0) {
	if(ic.structure!=nullptr){
		mom_indices=ic.structure->mom_indices;
		partition=ic.structure->partition;
		location_loop_1=ic.structure->location_loops[0];
		location_loop_2=ic.structure->location_loops[2];
	}
	if(ic.rung_count==1)
		loop_count=1;
	else if(ic.rung_count==2||ic.rung_count==3)
		loop_count=2;
}

CutHierarchy::CutHierarchy(const std::vector<cutF>& ivcF, size_t depth, std::vector<size_t> filter): n_loops(0), n_max_props(0) {
	DEBUG_MESSAGE("CONSTRUCTED a CutHierarchy");
	if(ivcF.size()==0){
		std::cout<<"CutHierarchy constructed without any maximal cut -- Empty structure"<<std::endl;
		return;
	}
	// check that all passed cuts are for the same amount of loops
	if(ivcF[0].rung_count==1){
		n_loops=1;
	}
	else if(ivcF[0].rung_count>1&&ivcF[0].rung_count<4){
		n_loops=2;
	}

	// organize the passed cuts by the corresponding number of propagators (at two loops and beyond there can be maximal cuts with different amount of propagators)
	std::vector<size_t> propagator_count_in_maximal;
	for(size_t ii=0;ii<ivcF.size();ii++){
		size_t lcount(ivcF[ii].get_prop_count());
		if(std::find(propagator_count_in_maximal.begin(),propagator_count_in_maximal.end(),lcount)==propagator_count_in_maximal.end()){
			propagator_count_in_maximal.push_back(lcount);
			std::sort(propagator_count_in_maximal.begin(),propagator_count_in_maximal.end());
		}
	}
	DEBUG_MESSAGE("The maximals passed have the following set of number of propagators: ",propagator_count_in_maximal);

	size_t max_prop_count(propagator_count_in_maximal.back());
	size_t levels_to_add(max_prop_count-propagator_count_in_maximal[0]+1);
	for(size_t ii=0;ii<levels_to_add;ii++){
		// add a vector to store maximal cuts
		cut_hierarchy.push_back(std::vector<cutF>());
	}
	for(size_t ii=0;ii<ivcF.size();ii++){
		size_t lcount(ivcF[ii].get_prop_count());
		cut_hierarchy[max_prop_count-lcount].push_back(ivcF[ii]);
	}

	n_max_props=max_prop_count;

	bool check_consistent_prop_count(true);
	for(size_t ii=1;ii<ivcF.size();ii++){
		DEBUG_MESSAGE("CutHierarchy added maximal cut with ",ivcF[ii].get_prop_count()," props and first: ",n_max_props);
		if(n_loops==1&&ivcF[ii].rung_count!=1){
			std::cout<<"All maximal cuts should have same # of loops! # rungs "<<ivcF[ii].rung_count<<" for 1-loop case"<<std::endl;
			std::cout<<ivcF[ii].get_short_name()<<" vs. "<<ivcF[0].get_short_name()<<std::endl;
			check_consistent_prop_count=false;
		}
		else if(n_loops==2&&(ivcF[ii].rung_count<2||ivcF[ii].rung_count>3)){
			std::cout<<"All maximal cuts should have same # of loops! # rungs "<<ivcF[ii].rung_count<<" for 2-loop case"<<std::endl;
			std::cout<<ivcF[ii].get_short_name()<<" vs. "<<ivcF[0].get_short_name()<<std::endl;
			check_consistent_prop_count=false;
		}
	}

	// a bad 'maximal' cut passed
	if(!check_consistent_prop_count){
		std::cout<<"Stops lower cut generation in CutHierarchy, as a bad 'maximal' passed"<<std::endl;
		return;
	}

	size_t maximum_depth(0);
	if(n_loops==1){
		// minimum amount of propagators in (massless) one-loop cuts is 2
		maximum_depth=n_max_props-2;
	}
	if(n_loops==2){
		// minimum amount of propagators in (massless) two-loop cuts is 3
		maximum_depth=n_max_props-3;
	}

        // check if we can have massive tadpoles
        for(auto& maximal: ivcF){
            if(maximal.has_massive_propagator()){
                // then explore tadpoles!
                ++maximum_depth;
                break;
            }
        }
        if(depth != 0)
            maximum_depth=depth;

	std::vector<std::pair<size_t,size_t> > track_last_related_doubled_prop;
	for(size_t hh=1;hh<=maximum_depth;hh++){
	  // Generate all (next-to)^hh-maximal cuts!

	  if(cut_hierarchy.size()<hh+1)
	  	cut_hierarchy.push_back(std::vector<cutF>());
	  for(size_t ii=0;ii<cut_hierarchy[hh-1].size();ii++){
		for(size_t jj=0;jj+hh<n_max_props+1;jj++){
			// a potentially new cutF
			cutF lcut(cut_hierarchy[hh-1][ii].pinch(jj));
			// if cut has a sub bubble, and it is semi simple, then it is not directly fittable
			if(lcut.has_sub_bubble()&&lcut.is_semi_simple()){
				lcut.accepts_direct_fit=false;
				// if associated structure exists, also update its accepts_direct_fit
				if(lcut.structure!=nullptr)
					lcut.structure->accepts_direct_fit=false;
			}
			// in case of a repeated cut, we store info here
			size_t potential_equal(0);
			bool add_cutF(true);
			for(size_t kk=0;kk<cut_hierarchy[hh].size();kk++){
#if 0
std::cout<<"Checking cut identities"<<std::endl;
cut_hierarchy[hh][kk].show();
lcut.show();
#endif
				if(lcut==cut_hierarchy[hh][kk]){
					// it is not a new cut!
					add_cutF=false;
					potential_equal=kk;
#if 0
#ifdef DEBUG_ON
std::cout<<"EQUAL cuts identified! This one: ("<<ii<<" parent and "<<jj<<" pinch)"<<std::endl;
lcut.show();
std::cout<<"AND this one:"<<std::endl;
cut_hierarchy[hh][kk].show();
#endif
#endif
					break;
				}
			}
			if(add_cutF){
				// check if it contains a scaleless subint
				if(!lcut.has_scaleless_int()){
					// add parent info to lcut
					lcut.parent_cuts.push_back(std::vector<parenthood>(
							// daughter level, daughter locate, 	daughter, parent level, p locate, p,	cutFm,	one_parent (same as p),	0 is because it's irrelevant
							{parenthood(hh,cut_hierarchy[hh].size(),&lcut,hh-1,ii,&cut_hierarchy[hh-1][ii],this,ii,0)}
							));
					// add also corresponding subtraction
					if(lcut.structure!=nullptr){
						lcut.structure->container_subs.push_back(std::vector<subtraction>({subtraction(&cut_hierarchy[hh-1][ii],&lcut,jj)}));
						// and fill accordingly entry in parenthood
						size_t locate_in_container_subs(lcut.structure->container_subs.back().size()-1);
						lcut.parent_cuts.back().back().locate_subtraction_in_container_subs.push_back(locate_in_container_subs);
					}
					else
						std::cout<<"Could not add subtraction to associated CStructure of lcut!! nullptr"<<std::endl;
					// and copy to cutFmanger container
					cut_hierarchy[hh].push_back(lcut);
#if 0
#ifdef DEBUG_ON
std::cout<<"Added a new next-to-maximal cutF: ("<<ii<<" parent and "<<jj<<" pinch) out of: "<<cut_hierarchy[hh].size()<<std::endl;
cut_hierarchy[hh].back().show();
#endif
#endif
				}
#if 0
				else{
					add_cutF=false;
#ifdef DEBUG_ON
std::cout<<"Recognized a scaleless sub integral in the following cut:"<<std::endl;
lcut.show();
#endif
				}
#endif
			}
			// repeated cut
			else{
				// check if parent already included, and if not add it
				bool already_added_one_parent(false);
				for(size_t ff=0;ff<cut_hierarchy[hh][potential_equal].parent_cuts.back().size();ff++){
					if(ii==cut_hierarchy[hh][potential_equal].parent_cuts.back()[ff].locate){
						already_added_one_parent=true;
						break;
					}
				}
				if(!already_added_one_parent){
					// add parent info to lcut
					cut_hierarchy[hh][potential_equal].parent_cuts.back().push_back(
							// daughter level, daughter locate, 	daughter, parent level, p locate, p,	cutFm,	one_parent (same as p),	0 is because it's irrelevant
							parenthood(hh,potential_equal,&cut_hierarchy[hh][potential_equal],hh-1,ii,&cut_hierarchy[hh-1][ii],this,ii,0)
							);
					if(cut_hierarchy[hh][potential_equal].structure!=nullptr){
						// HERE it is assumed that a single subtraction is necessary for 1-parents
						cut_hierarchy[hh][potential_equal].structure->container_subs.back().push_back(
							subtraction(&cut_hierarchy[hh-1][ii],&cut_hierarchy[hh][potential_equal],jj,&lcut)
						);
						// and fill accordingly entry in parenthood
						size_t locate_in_container_subs(cut_hierarchy[hh][potential_equal].structure->container_subs.back().size()-1);
						cut_hierarchy[hh][potential_equal].parent_cuts.back().back().locate_subtraction_in_container_subs.push_back(locate_in_container_subs);
					}
					else{
						std::cout<<"Could not add subtraction to associated CStructure of "<<
							"cut_hierarchy[hh][potential_equal].structure!! nullptr"<<std::endl;
					}
				}
				else{
					std::cout<<"ERROR?! daughter cut: "<<lcut.get_short_name()<<" has been identified with: "<<std::endl;
					std::cout<<"	existing daughter: "<<cut_hierarchy[hh][potential_equal].get_short_name()<<std::endl;
					std::cout<<"	and already contained the 1-parent: "<<cut_hierarchy[hh-1][ii].get_short_name()<<std::endl;
					std::cout<<"	--- Did nothing"<<std::endl;
				}
			}
		}
	  }
	  // we have finished filling the hh-th level of (next-to)^hh-maximal cuts, including only 1-parents in corresponding cuts
	  // next we complete information with up to hh-parents (in parent_cuts)
	  this->fill_level_parents();

	  // employing the set of doubled-propagator cuts 1-parents, remove "second" in favor of first
	  this->remove_unresolved_1parents_from_hierarchy(track_last_related_doubled_prop);

	  // in current level find all related doubled-propagator cuts and store for the following cycle
	  track_last_related_doubled_prop=this->get_doubled_prop_related_cuts();
	}

        // filter bottom of hierarchy
        std::sort(filter.rbegin(), filter.rend());
        for(auto rr : filter) 
            cut_hierarchy.back().erase(cut_hierarchy.back().begin()+rr);


#if 0
std::cout<<"SWAPPING: 5,29 with 5,45"<<std::endl;
if(cut_hierarchy.size()>3)
std::iter_swap(cut_hierarchy[2].begin()+29,cut_hierarchy[2].begin()+45);
#endif

	// now that cut_hierarchy has been filled, we fill the arrays of CStructure's and corresponding map
	// FOR NOW I'm creating a CStructure for each cutF!!
	for(size_t ii=0;ii<cut_hierarchy.size();ii++){
		structures.push_back(std::vector<CStructure>(cut_hierarchy[ii].size()));
	}
	for(size_t ii=0;ii<cut_hierarchy.size();ii++){
		map_cutindex_to_cstructure.push_back(std::vector<int>());
		for(size_t jj=0;jj<cut_hierarchy[ii].size();jj++){

			// For now I just add the cut information
			// Later it will check if cutF.structure matches one on the vector structures
			map_cutindex_to_cstructure[ii].push_back(jj);
			// HERE! Before pushing back, check if it matches!!
			structures[ii][jj]=(*(cut_hierarchy[ii][jj].structure));
			// redefine 'structure' for cut_hierarchy[ii][jj]
#if 1
			delete cut_hierarchy[ii][jj].structure;
			cut_hierarchy[ii][jj].owns_struct=false;
			cut_hierarchy[ii][jj].structure=&structures[ii][jj];
#endif
			// assign level and locate indices in cut
			cut_hierarchy[ii][jj].cut_hierarchy_level=ii;
			cut_hierarchy[ii][jj].cut_kin_locate=jj;
// update loop particles to make calculation thread safe
#if defined(USE_OMP) && _BUILDER_THREAD_SAFE == 1
			cut_hierarchy[ii][jj].update_loop_momentum(std::to_string(jj));
//std::cout<<"Redefining loop particles: "<<ii<<" "<<jj<<std::endl;
#endif
			// for each term in cut_hierarchy[ii][jj].parent_cuts assign indices in corresponding structure.container_subs
			for(size_t kk=0;kk<cut_hierarchy[ii][jj].parent_cuts.size();kk++){
				for(size_t ll=0;ll<cut_hierarchy[ii][jj].parent_cuts[kk].size();ll++){
					// assign level and cstructure_locate indices in corresponding CStructure
//std::cout<<"ii,jj: "<<ii<<","<<jj<<" kk,ll: "<<kk<<","<<ll<<" parent_cuts[kk][ll]: "<<cut_hierarchy[ii][jj].parent_cuts[kk][ll]<<std::endl;
//std::cout<<"And: map_cutindex_to_cstructure[ii-kk-1][cut_hierarchy[ii][jj].parent_cuts[kk][ll]] "<<map_cutindex_to_cstructure[ii-kk-1][cut_hierarchy[ii][jj].parent_cuts[kk][ll]]<<std::endl;
					for(size_t mm=0;mm<cut_hierarchy[ii][jj].parent_cuts[kk][ll].locate_subtraction_in_container_subs.size();mm++){
						size_t local_index(cut_hierarchy[ii][jj].parent_cuts[kk][ll].locate_subtraction_in_container_subs[mm]);
						structures[ii][jj].container_subs[kk][local_index].parent_level=ii-kk-1;
						structures[ii][jj].container_subs[kk][local_index].parent_cstructure_locate=
							map_cutindex_to_cstructure[ii-kk-1][cut_hierarchy[ii][jj].parent_cuts[kk][ll].locate];
//std::cout<<"And B:  (cut_hierarchy[ii][jj].structure->container_subs)[kk][ll].parent_level "<<(cut_hierarchy[ii][jj].structure->container_subs)[kk][ll].parent_level<<" "<<(cut_hierarchy[ii][jj].structure->container_subs)[kk][ll].parent_cstructure_locate<<std::endl;
					}
				}
			}
		}
	}
#if 0
std::cout<<"Map from cut to cstructure:"<<std::endl;
for(size_t ii=0;ii<map_cutindex_to_cstructure.size();ii++)
	std::cout<<"i: "<<ii<<" "<<map_cutindex_to_cstructure[ii]<<std::endl;
#endif

DEBUG(
	std::cout<<"In the end we have: "<<cut_hierarchy[0].size()<<" maximal cuts and "<<std::endl;
	for(size_t ii=1;ii<cut_hierarchy.size();ii++)
		std::cout<<"	--- "<<cut_hierarchy[ii].size()<<" (next-to-)^"<<ii<<" maximal"<<std::endl;
        );
}

std::vector<cutF> CutHierarchy::get_maximal_cuts(const std::vector<cutF>& ivcF) {
	size_t l_n_loops(0);
	size_t l_n_max_props;
	if(ivcF.size()==0){
		std::cout<<"CutHierarchy::get_maximal_cuts(.) got empty vector -- Did nothing!!"<<std::endl;
		return std::vector<cutF>();
	}
	// check that all passed cuts are for the same amount of loops
	if(ivcF[0].rung_count==1){
		l_n_loops=1;
	}
	else if(ivcF[0].rung_count>1&&ivcF[0].rung_count<4){
		l_n_loops=2;
	}
	// check that all passed cuts have the same amount of propagators
	l_n_max_props=ivcF[0].get_prop_count();
	bool check_consistent_prop_count(true);
	for(size_t ii=1;ii<ivcF.size();ii++){
		if(l_n_loops==1&&ivcF[ii].rung_count!=1){
			std::cout<<"All maximal cuts should have same # of loops! # rungs "<<ivcF[ii].rung_count<<" for 1-loop case"<<std::endl;
			std::cout<<ivcF[ii].get_short_name()<<" vs. "<<ivcF[0].get_short_name()<<std::endl;
			check_consistent_prop_count=false;
		}
		else if(l_n_loops==2&&(ivcF[ii].rung_count<2||ivcF[ii].rung_count>3)){
			std::cout<<"All maximal cuts should have same # of loops! # rungs "<<ivcF[ii].rung_count<<" for 2-loop case"<<std::endl;
			std::cout<<ivcF[ii].get_short_name()<<" vs. "<<ivcF[0].get_short_name()<<std::endl;
			check_consistent_prop_count=false;
		}
		else if(l_n_max_props!=ivcF[ii].get_prop_count()){
			std::cout<<"All maximal cuts should have equal # of propagators! "<<ivcF[ii].get_prop_count()<<" vs. "<<l_n_max_props<<std::endl;
			std::cout<<ivcF[ii].get_short_name()<<" vs. "<<ivcF[0].get_short_name()<<std::endl;
			check_consistent_prop_count=false;
		}
	}
	// a bad 'maximal' cut passed
	if(!check_consistent_prop_count){
		std::cout<<"Incosistent set of parents passed to CutHierarchy::get_maximal_cuts(.) got empty vector -- Did nothing!!"<<std::endl;
		return std::vector<cutF>();
	}

	// 2-loop cuts not supported yet!!
	if(l_n_loops==2){
		std::cout<<"Not doing anything with 2-loop cuts in CutHierarchy::get_maximal_cuts(.) got empty vector -- Returned input vector!! Correct if multiplicity is less than 9"<<std::endl;
		return ivcF;
	}

	// for 1-loop, work only needed if l_n_max_props>5
	if(l_n_max_props<6&&l_n_loops==1)
		return ivcF;

	std::vector<cutF> toreturn(ivcF);

	size_t maximum_depth(0);
	if(l_n_loops==1){
		// 1-loop maximals are pentagons
		maximum_depth=l_n_max_props-5;
	}
	if(l_n_loops==2){
		// 2-loop maximals are 11-prop cuts with at most exagons on each side... this has to be better accounted for!!
		//maximum_depth=l_n_max_props-11;
		maximum_depth=0;
	}
	for(size_t hh=1;hh<=maximum_depth;hh++){
	  std::vector<cutF> toreturn_local;
	  for(size_t ii=0;ii<toreturn.size();ii++){
		for(size_t jj=0;jj+hh<l_n_max_props+1;jj++){
			// a potentially new cutF
			cutF lcut(toreturn[ii].pinch(jj));
			// if cut has a sub bubble, and it is semi simple, then it is not directly fittable
			if(lcut.has_sub_bubble()&&lcut.is_semi_simple()){
				lcut.accepts_direct_fit=false;
				// if associated structure exists, also update its accepts_direct_fit
				if(lcut.structure!=nullptr)
					lcut.structure->accepts_direct_fit=false;
			}
			// in case of a repeated cut, we store info here
			//size_t potential_equal(0);
			bool add_cutF(true);
			for(size_t kk=0;kk<toreturn_local.size();kk++){
				if(lcut==toreturn_local[kk]){
					// it is not a new cut!
					add_cutF=false;
					//potential_equal=kk;
					break;
				}
			}
			if(add_cutF){
				// check if it contains a scaleless subint
				if(!lcut.has_scaleless_int()){
					// and copy to cutFmanger container
					toreturn_local.push_back(lcut);
				}
			}
#if 0
			// repeated cut
			else{
			}
#endif
		}
	  }
	  toreturn=toreturn_local;
	}

	return toreturn;
}

int CutHierarchy::get_cut_locate(size_t ilevel,size_t icut) const {
    return cut_hierarchy[ilevel][icut].get_cut_locate();
}

// this method takes back of cut hierarchy and if possible fills a new level of parents
//	at the end it calls itself to keep going if necessary
void CutHierarchy::fill_level_parents(){
	size_t n_existing_levels(cut_hierarchy.size());
	if(n_existing_levels<=2){
		return;
	}
	size_t n_cuts_in_level(cut_hierarchy[n_existing_levels-1].size());
	if(n_cuts_in_level==0){
		return;
	}
	size_t current_depth(cut_hierarchy[n_existing_levels-1][0].parent_cuts.size());
	for(size_t ii=1;ii<n_cuts_in_level;ii++){
		if(cut_hierarchy[n_existing_levels-1][ii].parent_cuts.size()!=current_depth){
			std::cout<<"The first cut in level: "<<n_existing_levels<<" of cut_hierarchy has parent-depth "<<current_depth<<" while the "<<ii
				<<" has "<<cut_hierarchy[n_existing_levels-1][ii].parent_cuts.size()<<std::endl;
			std::cout<<cut_hierarchy[n_existing_levels-1][0].get_short_name()<<" vs. "<<cut_hierarchy[n_existing_levels-1][ii].get_short_name()<<std::endl;
			std::cout<<"CutHierarchy::fill_level_parents() did nothing!!"<<std::endl;
			return;
		}
	}
	if(current_depth==0){
		std::cout<<"Found that the cut "<<cut_hierarchy[n_existing_levels-1][0].get_short_name()<<" has no parents --- CutHierarchy::fill_level_parents() stop "<<std::endl;
		return;
	}

	// check if we already have maximum depth of parenthood
	if(current_depth+1>=n_existing_levels){
		DEBUG_MESSAGE("CutHierarchy::fill_level_parents() has reached maximum depth of parenthood ",current_depth," for hierarchy level ",n_existing_levels);
		return;
	}

	// all clear, we now fill a new level of parent-depth for level n_existing_levels of cut_hierarchy

	// we go one by one for each cutF
	for(size_t ii=0;ii<n_cuts_in_level;ii++){
		std::vector<size_t> to_add_new_level_parents;
		std::vector<std::pair<size_t,size_t> > corresponding_parent;

		cutF& localcutF(cut_hierarchy[n_existing_levels-1][ii]);
		std::vector<parenthood>& local_parent_cuts(localcutF.parent_cuts[0]);
		// check each 1-parent of cutF, search over all of its (current_depth-1)-parents for their corresponding 1-parents
		for(size_t jj=0;jj<local_parent_cuts.size();jj++){
			cutF& localparent(cut_hierarchy[n_existing_levels-2][local_parent_cuts[jj].locate]);
			if(localparent.parent_cuts.size()==0){
				DEBUG_MESSAGE("Found an nth-parent (",localparent.get_short_name(),") with no parents");
				continue;
			}

			std::vector<parenthood>& parent_1parents(localparent.parent_cuts[current_depth-1]);
			for(size_t kk=0;kk<parent_1parents.size();kk++){
				// check if we need to add it to to_add_new_level_parents
				if(std::find(to_add_new_level_parents.begin(),to_add_new_level_parents.end(),parent_1parents[kk].locate)==to_add_new_level_parents.end()){
					to_add_new_level_parents.push_back(parent_1parents[kk].locate);
					corresponding_parent.push_back(std::make_pair(jj,kk));
				}
			}
		}
//		std::cout<<"Will push into cut "<<localcutF.get_short_name()<<" its "<<current_depth+1<<" level of parenthood: "<<to_add_new_level_parents<<std::endl;

		std::vector<parenthood> to_add_new_level_parenthoods;
		if(localcutF.structure!=nullptr){
			(localcutF.structure->container_subs).push_back(std::vector<subtraction>());
			std::vector<subtraction>& local_container_subs((localcutF.structure->container_subs).back());
#if 0
std::cout<<"CutHierarchy::fill_level_parents() handling the parenthood of "<<localcutF.get_short_name()<<std::endl;
std::cout<<"  will add: "<<localcutF.parent_cuts.size()+1<<" level of parenthood:"<<std::endl;
#endif
			// and add the composition of the corresponding subtractions
			for(size_t ll=0;ll<to_add_new_level_parents.size();ll++){
#if 0
std::cout<<"	============"<<std::endl;
std::cout<<"	new parent # "<<ll<<" with path: one-parent: "<<corresponding_parent[ll].first<<" and its corresponding parent: "<<corresponding_parent[ll].second<<std::endl;
std::cout<<"	level of 1-parent: "<<n_existing_levels-2<<" using the 1-parent "<<local_parent_cuts[corresponding_parent[ll].first].locate<<std::endl;
std::cout<<"	the used 1-parent name: "<<cut_hierarchy[n_existing_levels-2][local_parent_cuts[corresponding_parent[ll].first].locate].get_short_name()<<std::endl;
std::cout<<"	level of new parent: "<<n_existing_levels-1-(current_depth+1)<<" with index "<<to_add_new_level_parents[ll]<<std::endl;
std::cout<<"	the new parent name: "<<cut_hierarchy[n_existing_levels-1-(current_depth+1)][to_add_new_level_parents[ll]].get_short_name()<<std::endl;
#endif

				// we are dealing with the current_depth+1 parents of localcutF
				// now, for the ll-th  find which 1-parent of localcutF has this cut as

				// First, build new parenthood
				to_add_new_level_parenthoods.push_back(
					// daughter level, daughter locate, 	daughter, parent level, p locate,
					parenthood(n_existing_levels-1,ii,&localcutF,n_existing_levels-1-(current_depth+1),to_add_new_level_parents[ll],
						// p,	cutFm,
						&cut_hierarchy[n_existing_levels-1-(current_depth+1)][to_add_new_level_parents[ll]],this,
						// one_parent,					and corresponding nth parent of one_parent
						corresponding_parent[ll].first,corresponding_parent[ll].second)
				);

				// and add corresponding subtraction for each inequivalent parenthood path, as contained in corresponding one_parents
				size_t max_amount_of_paths(to_add_new_level_parenthoods.back().one_parents.size());
				for(size_t oo=0;oo<max_amount_of_paths;oo++){
					const std::pair<size_t,size_t>& one_parent_info(to_add_new_level_parenthoods.back().one_parents[oo]);
#if 0
std::cout<<"	Adding inequivalent path: j: "<<oo<<" with: one_parent_j.first = "<<one_parent_info.first<<" one_parent_info.second "<<one_parent_info.second<<std::endl;
#endif
					const subtraction& parent_to_nthparent(
						((cut_hierarchy[n_existing_levels-2][local_parent_cuts[one_parent_info.first].locate].structure)->
							container_subs)[current_depth-1][one_parent_info.second]
					);
					const subtraction& local_to_parent(((localcutF.structure)->container_subs)[0][one_parent_info.first]);
#if 0
std::cout<<"THE LEFT: "<<parent_to_nthparent.get_info()<<" THE RIGHT: "<<local_to_parent.get_info()<<std::endl;
std::cout<<"FROM cut: "<<cut_hierarchy[n_existing_levels-2][local_parent_cuts[one_parent_info.first].locate].get_short_name()<<" AND "<<localcutF.get_short_name()<<std::endl;
#endif
					// compose the subtractions
					local_container_subs.push_back(parent_to_nthparent*local_to_parent);
					// check if constructed subtraction should own its own tensors
					if(max_amount_of_paths>1){
						local_container_subs.back().is_unique_cutF_subtraction=false;
					}
					// and fill accordingly entry in parenthood
					size_t locate_in_container_subs(local_container_subs.size()-1);
					to_add_new_level_parenthoods.back().locate_subtraction_in_container_subs.push_back(locate_in_container_subs);
				}
			}
		}
		else{
			std::cout<<"In method CutHierarchy::fill_level_parents(), received a cutF on the hierarchy with nullptr structure!! -- Did nothing"<<std::endl;
		}
		localcutF.parent_cuts.push_back(to_add_new_level_parenthoods);
	}
	// and we make it recursively until it stops
	this->fill_level_parents();
}

// this method takes the back of the cut_hierarchy, checks for which 2-loop cuts are related by beeing doubled-propagator cuts, and returns corresponding pairs of indices
std::vector<std::pair<size_t,size_t> > CutHierarchy::get_doubled_prop_related_cuts(){
	// only relevant for 2-loop cuts
	if(n_loops!=2)
		return std::vector<std::pair<size_t,size_t> >();
	if(cut_hierarchy.size()==0){
		std::cout<<"Called CutHierarchy::get_doubled_prop_related_cuts() with empty hierarchy!! Error??"<<std::endl;
		return std::vector<std::pair<size_t,size_t> >();
	}

	// first collect all semi-simple diagrams with a bubble loop
	std::vector<size_t> all_dp_cuts;
	for(size_t ii=0;ii<cut_hierarchy.back().size();ii++){
		const cutF& lcut(cut_hierarchy.back()[ii]);
		if(lcut.has_sub_bubble()&&lcut.is_semi_simple()){
			all_dp_cuts.push_back(ii);
		}
	}

	// now pair them, and store info to return
	std::vector<std::pair<size_t,size_t> > toreturn;
	while(all_dp_cuts.size()>0){
		size_t cutfirst(all_dp_cuts[0]);
		const cutF& lcut(cut_hierarchy.back()[cutfirst]);
		int thepair(-1);
		// find if any cut pairs with cutfirst
		for(size_t ii=1;ii<all_dp_cuts.size();ii++){
			const cutF& tocompare(cut_hierarchy.back()[all_dp_cuts[ii]]);
			if(lcut.is_doubled_prop_related(tocompare)){
				thepair=int(ii);
				break;
			}
		}

		// if pair found, add toreturn and drop both from all_dp_cuts
		if(thepair>-1){
			size_t pair(thepair);
			toreturn.push_back(std::make_pair(cutfirst,all_dp_cuts[pair]));
			all_dp_cuts.erase(all_dp_cuts.begin()+pair);
			all_dp_cuts.erase(all_dp_cuts.begin());
		}
		// no pair found, just remove cutfirst
		else{
			all_dp_cuts.erase(all_dp_cuts.begin());
		}
	}

#if 0
std::cout<<"Pairs found by CutHierarchy::get_doubled_prop_related_cuts():"<<std::endl;
for(size_t ii=0;ii<toreturn.size();ii++)
	std::cout<<"	"<<cut_hierarchy.back()[toreturn[ii].first].get_short_name()<<" with "<<cut_hierarchy.back()[toreturn[ii].second].get_short_name()<<" "<<toreturn[ii]<<std::endl;
#endif

	return toreturn;
}


// this method takes a vector of size_t pairs, corresponding to next-to back of cut_hierarchy, removes inheritance related to each "second" size_t from back
// 	DO NOTHING for 1-loop
void CutHierarchy::remove_unresolved_1parents_from_hierarchy(const std::vector<std::pair<size_t,size_t> >& iv){
	if(cut_hierarchy.size()==0)
		return;

	// The removal is carried out after fill_level_parents() has operated over cut_hierarchy.
	// This means that all needed inheritance information from 1-parents has been properly passed through to cut_hierarchy.back().
	// We review all 1-parents of cut_hierarchy.back(), and whenever a match happens with iv[ii].second, we remove it from corresponding parent_cuts

	for(size_t ii=0;ii<iv.size();ii++){
		size_t doubled_cut(iv[ii].first);
		for(size_t jj=0;jj<cut_hierarchy.back().size();jj++){
			if(cut_hierarchy.back()[jj].parent_cuts.size()>0){
				std::vector<parenthood>& one_parents(cut_hierarchy.back()[jj].parent_cuts[0]);
				size_t n_1ps(one_parents.size());
				// find if one of the 1-parents is doubled_cut
				for(size_t kk=0;kk<n_1ps;kk++){
					if(doubled_cut==one_parents[kk].locate){
#if 0
std::cout<<"FOR: "<<cut_hierarchy.back()[jj].get_short_name()<<std::endl;
std::cout<<"one_parent size: "<<one_parents.size()<<" to be removed the entry: "<<kk<<" locate_sub_in_container for parenthood: "<<one_parents[kk].locate_subtraction_in_container_subs<<std::endl;
std::cout<<"	size of associated container_sub: "<<cut_hierarchy.back()[jj].structure->container_subs[0].size()<<std::endl;
#endif
						one_parents.erase(one_parents.begin()+kk);
						// we need to erase corresponding entry in container_subs
						cut_hierarchy.back()[jj].structure->container_subs[0].erase(cut_hierarchy.back()[jj].structure->container_subs[0].begin()+kk);
						// and then fix (decrease by one) all remaining locate_subtraction_in_container_subs in the other 1-level parenthoods
						for(size_t ll=kk;ll+1<n_1ps;ll++){
							one_parents[ll].locate_subtraction_in_container_subs[0]--;
						}
						break;
					}
				}
			}
		}
#if 0
std::cout<<"EFFECTIVELY removed the cut("<<cut_hierarchy.size()-2<<","<<doubled_cut<<"): "<<cut_hierarchy[cut_hierarchy.size()-2][doubled_cut].get_short_name()<<" from the hierarchy tree"<<std::endl;
#endif
	}

}

void CutHierarchy::print_all_cuts() const {
	for(size_t ii=0;ii<cut_hierarchy.size();ii++){
		std::cout<<"############"<<std::endl;
		std::cout<<"Start printing cuts with "<<n_max_props-ii<<" propagators"<<std::endl;
		for(size_t jj=0;jj<cut_hierarchy[ii].size();jj++){
			std::cout<<"Cut "<<jj<<(cut_hierarchy[ii][jj].accepts_direct_fit?" (+) ":" (-) ")<<cut_hierarchy[ii][jj].get_short_name()<<std::endl;
		}
		std::cout<<"############"<<std::endl;
	}
}

void CutHierarchy::explore_cut_inheritance() const {
	while (true) {
		size_t desiredpropagators,desiredcut;
		std::cout<<"Please write # of propagators in cut to explore (\"0\" will take you out): ";
		std::cin>>desiredpropagators;
		if(desiredpropagators==0)	break;
		std::cout<<"Please write cut to explore: ";
		std::cin>>desiredcut;
		if(n_max_props-cut_hierarchy.size()<desiredpropagators && desiredpropagators<=n_max_props){
			size_t cut_hier_entry(n_max_props-desiredpropagators);
			const std::vector<cutF>& cuts_this_level(cut_hierarchy[cut_hier_entry]);
			if(desiredcut<cuts_this_level.size()){
				std::cout<<"============ \n";
				std::cout<<"Details for the cut "<<desiredcut<<" with "<<desiredpropagators<<" propagators:"<<std::endl;
				cuts_this_level[desiredcut].show();
				std::cout<<"============ \n";
				const std::vector<std::vector<parenthood> >& parenthoods(cuts_this_level[desiredcut].parent_cuts);
				if(parenthoods.size()==0){
					std::cout<<"has no parents"<<std::endl;
					std::cout<<"============ \n";
				}
				for(size_t ii=0;ii<parenthoods.size();ii++){
				  if(parenthoods[ii].size()>0){
					std::cout<<"has "<<ii+1<<"-parents: \n";
					std::cout<<"============ \n";
					for(size_t i=0;i<parenthoods[ii].size();i++){
						std::cout<<"Cut_"<<i<<" (# "<<parenthoods[ii][i].locate<<" with "<<desiredpropagators+ii+1
							<<" props"<<(cut_hierarchy[cut_hier_entry-ii-1][parenthoods[ii][i].locate].accepts_direct_fit?" (+)":" (-)")
							<<"): "<<cut_hierarchy[cut_hier_entry-ii-1][parenthoods[ii][i].locate].get_short_name()<<std::endl;
						for(size_t kk=0;kk<parenthoods[ii][i].locate_subtraction_in_container_subs.size();kk++){
							size_t local_index(parenthoods[ii][i].locate_subtraction_in_container_subs[kk]);
							std::cout<<"	--- sub info: "<<cuts_this_level[desiredcut].structure->container_subs[ii][local_index].get_info()<<std::endl;
						}
					}
				  }
				  else{
					std::cout<<"has no "<<ii+1<<"-parents \n";
				  }
				 std::cout<<"============ \n";
				}
				std::cout<<"It "<<(cuts_this_level[desiredcut].accepts_direct_fit?"accepts ":"does not accept ")<<"a direct fit"<<std::endl;
				std::cout<<"============ \n";
			}
			else{
				std::cout<<"WARNING: there is no cut: "<<desiredcut<<" with: "<<desiredpropagators<<" propagators\n";
			}
		}
		else{
			std::cout<<"WARNING: there is no level with "<<desiredpropagators<<" propagators\n";
		}
	}
}

const std::vector<std::vector<cutF> >& CutHierarchy::get_hierarchy() const {
	return cut_hierarchy;
}

cutTopology CutHierarchy::get_cutTopology_copy(size_t l,size_t c) {
	return cut_hierarchy[l][c].get_cutTopology_copy();
}

void CutHierarchy::grow_forest(Forest& f,size_t ref,bool switch_to_planar_standard, size_t _max_Ds){
	for(size_t ii=0;ii<cut_hierarchy.size();ii++){
		for(size_t jj=0;jj<cut_hierarchy[ii].size();jj++){
			// only add to forest directly fittable cuts (basically avoid double-prop semi-simple cuts)
			if(cut_hierarchy[ii][jj].accepts_direct_fit){
				cut_hierarchy[ii][jj].add_to_forest(f,ref,switch_to_planar_standard,_max_Ds);
			}
		}
	}
}

}
}
