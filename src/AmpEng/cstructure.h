/**
 * @file cstructure.h
 *
 * @date 24.7.2016
 *
 * @brief Header file for basic structures to build cutF's
 *
 * Each diagram in a hierarchy contains kinematic information that can be factor out (and reused as needed). Here we implement the associated cut structures
 * necessary for the hierarchies build in cutF.h
 *
*/
#ifndef CSTRUCTURE_H_
#define CSTRUCTURE_H_

#include <vector>
#include <map>
#include <cstddef>
#include <tuple>
#include <utility>
#include <unordered_map>

#include "Core/Particle.h"
#include "Core/CaravelException.h"
#include "Graph/Graph.h"

namespace Caravel {
namespace AmpEng{

// Forward declaration
class Vertex;

// the (non-commutative) operator for merging ordered vertices
/**
 * Non commutative merging operator for two vertices
 * @param First vertex of the merging
 * @param Second vertex involved in the merging
 * @param Vertex containing the merged content of the two input vertices
 */
Vertex operator*(Vertex,const Vertex&);
/**
 * Comparison operator for Vertices
 * They are compared for particle content and coupling powers.
 * @param First Vertex to be compared
 * @param Second Vertex to be compared
 * @returns True if the provided Vertices are equal, else false.
 */
bool operator==(const Vertex&,const Vertex&);

/**
 * Processes with fermions will get assigned an index, one for each (continuous) fermion line
 */
struct fermion_line_index {
    bool assigned{false};
    size_t value;
};

/**
 * Class for ordered vertices in cuts
 */
class Vertex {
    	using couplings_t = std::map<std::string,int>;
		friend bool operator==(const Vertex&,const Vertex&);
		std::vector<Particle*> content_colorless; /**< Colorless Particles contained in the vertex */
		std::vector<Particle*> content_colored; /**< Colored Particles contained in the vertex */
		std::vector<int> fermion_indices; 	/**< This vector can carry 'internal_index' (see the class Particle) of fermionic links. It is defaulted to {0}, and increased to {i,j,...} (i>0, j>0,...) only when necessary */
		std::vector<Particle*> content_all; /**< All Particles contained in the vertex */
		couplings_t coupling_powers; /**< Powers of the couplings involved in the Vertex */
                std::unordered_map<Particle*,fermion_line_index> fline_colorless; /**< Tracks fermion line of content_colorless */
                std::unordered_map<Particle*,fermion_line_index> fline_colored; /**< Tracks fermion line of content_colored */
	public:
		friend Vertex operator*(Vertex,const Vertex&);
		/**
		 * Empty vertex constructor
		 */
		Vertex() = default;
		/**
		 * Construct vertex by adding a particle
		 *
		 * @param 	Particle to be added.
		 * @param 	couplings_t containing the coupling powers involved in the Vertex
		 */
		Vertex(Particle*, const couplings_t& );
		/**
		 * Construct Vertex by providing a vector of particles it contains.
		 *
		 * @param 	vector containing the particles of the Vertex.
		 * @param 	coupling constants involved in the vertex.
		 * @param   internal indeces of the colored particles
		 */
		Vertex(std::vector<Particle*>,const couplings_t&,std::vector<int>);
		/**
		 * Contruct an empty vertex with  given coupling powers
		 *
		 * @param  couplings_t containing the powers of the couplings involved
		 *         in the vertex.
		 */
		Vertex(const couplings_t&);
		/**
		 * Construct Vertex by providing a vector of particles it contains.
		 *
		 * @param 	vector containing the particles of the Vertex.
		 * @param 	coupling constants involved in the vertex.
		 */
		Vertex(std::vector<Particle*>, const couplings_t&);
		/**
		 * Returns the name of a Vertex
		 *
		 * prints a set like "{p1,p2,...}["power of coupling1" "coupling2" ...]"
		 * @return A string describing the composition of the Vertex
		 * TODO: Maybe add coupling information to this.
		 */
		std::string get_name() const;
		/**
		 * Returns the number of particles per vertex
		 *
		 * @return The number of particles contained in the vertex
		 */
		size_t get_multiplicity() const;
		/**
		 * Access the different particles contained in the vertex
		 *
		 * @return A vector of pointers to particles contained in the vertex
		 */
		const std::vector<Particle*>& get_particles() const;
		/**
		 * Access the colored particles contained in the vertex
		 *
		 * @return A vector of pointers to colored particles contained in the vertex
		 */
		const std::vector<Particle*>& get_colored() const;
		/**
		 * Let's to set the 'internal_index' of a given particle
		 *
		 * @param the particle to be checked (bound check performed)
		 * @param the value
		 */
		void set_internal_index(size_t,int);
		/**
		 * @return a Vertex wich maps the passed particles
		 */
		Vertex map_particles(const std::vector<Particle*>&) const;
		/**
		 * Returns a vector with the particles in flipped order
		 *
		 * @return The vertex with paricles in reversed order.
		 */
		Vertex flip() const;
		/**
		 * Checks if passed particle pointer is contained in the Vertex
		 *
		 * @param   Pointer to particle whose occurence in the vertex is to be checked.
		 * @return  True if particle is contained in vertex, else false.
		 */
		bool is_contained(Particle*) const;
		/**
		 * Returns the powers of the couplings involved in the ordering
		 *
		 * @return 	Vector containing the powers of the couplings in the Vertex
		 */
		couplings_t get_coupling_powers() const;

		/**
		 * Returns the internal indices of the colored particles
		 *
		 * @return 	Vector containing the internal indices of the colored particles
		 */
		std::vector<int> get_internal_indices() const;
		/**
		 * Helper to set internal indices
		 */
		void set_internal_index(Particle*,int);
		/**
		 * Retrieve internal index
		 */
		int get_internal_index(Particle*) const;
		/**
		 * Set the coupling powers of the Vertex
		 * @param  coupling powers of the coupling involved in the vertex
		 */
		//void set_coupling_powers(const std::vector<int>&);

		/**
		 * A method to set to zero all couplings on *this
		 */
		void set_to_zero_all_couplings();
                /**
                 * Helper to set fermion line numbers
                 */
                void set_fermion_line_index(Particle*,size_t);
                /**
                 * Retrieve fermion line number information
                 */
                fermion_line_index get_fermion_line_index(Particle*) const;
};

std::ostream& operator<<(std::ostream&, const Vertex& );

enum link_type { link_start = 1, link_internal = 0, link_end = -1, link_null = -2 };

class cutF;

// class for linking Vertex's into RungFs
class Link {
		friend class cutF;
		friend bool operator==(const Link&,const Link&);
		link_type type;	/**< Type of the link, internal, end, start or null */
		int id;			/**< The id of the link */
		// Add particle, do not modify other attributes, compatibility
		Particle particle; /**< Particle the link is made of */
		std::vector<int> fermion_indices; 	/**< This vector can carry 'internal_index' (see the class Particle) of fermionic links. It is defaulted to {0}, and increased to {i,j} (i>0, j>0) only when necessary */
                fermion_line_index findex; /**< In case 'particle' needs a fermion-line assignment. The value is 'global' to a cutF, and so different to info stored in 'fermion_indices' */
	public:
		/**
		 * Default constructor for an empty link
		 */
		Link();
		/**
		 * Construct a link by providing a particle instance
		 *
		 * @param The particle the link is made of
		 * @param The type of the link (start, end, internal, null)
		 * @param The id of the link
		 */
		Link(Particle, link_type, int);
		/**
		 * Returns string with Link information (particle type and link id)
		 *
		 * @return String containing information on the link
		 */
		std::string get_info() const;
		/**
		 * Returns the particle type in the link
		 *
		 * @return The type of the link particle
		 */
		ParticleType get_particle_type() const;
		/**
		 * Returns the type of the link
		 *
		 * @return The type of the link
		 */
		link_type get_type() const;
		/**
		 * Check whether a link is null or not
		 *
		 * @return True if link_type is null, else false
		 */
		bool is_null() const;
		/**
		 * Returns the particle the link is made of.
		 *
		 * @return The particle the link is made of.
		 */
		Particle get_particle() const;
		/**
		 * Change the link type after construction (it resets fermion_indices)
		 *
		 * @param The new type of the link
		 */
		void set_link_type(link_type);
		/**
		 * Change the link type after construction with explicit input for fermion_indices
		 *
		 * @param The new type of the link
		 * @param input info for fermion_indices
		 */
		void set_link_type(link_type,const std::vector<int>&);
		/**
		 * Returns a new Link with a flipped directionality: id unchanged, Particle changed to anti-particle, 
		 * link_type unchanged if internal or null and end swapped with start
		 */
		Link flip() const;
		/**
		 * Method to set the UP 'internal_index' of a fermionic link
		 */
		void set_up_internal_index(int);
		/**
		 * Method to set the DOWN 'internal_index' of a fermionic link
		 */
		void set_down_internal_index(int);
		/**
		 * Method to retrive the UP 'internal_index' of a fermionic link
		 */
		int get_up_internal_index() const;
		/**
		 * Method to retive the DOWN 'internal_index' of a fermionic link
		 */
		int get_down_internal_index() const;
		bool is_fermion_link() const;
		bool is_antifermion_link() const;
                void set_fermion_line_index(size_t);
                fermion_line_index get_fermion_line_index() const;
};

bool operator==(const Link&,const Link&);
std::ostream& operator<<(std::ostream&, const Link& );

/**
 * Class for loop rungs
 *
 * A general one loop rung is composed of pairs of links and Vertices and
 * a link at the end:
 *
 * 			(Link_start - Vertex) (Link_internal - Vertex) ... (Link_end)
 *
 * A two-loop rung may also have Top and Bottom Vertices:
 *
 * 		(Vertex Top) (Link_start - Vertex) (Link_internal - Vertex) ... (Link_end) (Vertex bottom)
 *
 * The particle type included in the link is chosen to be the particle entering
 * the vertex. Its antiparticle is then the particle leaving the previous
 * vertex.
 */
class RungF {
    	using couplings_t = std::map<std::string,int>;
		friend class cutF;
		friend bool operator==(const RungF&,const RungF&);
		int match_top;		/**< Rungs with same match_top are joined on top */
		int match_bottom;	/**< Rungs with same match_bottom are joined on the bottom */
		Vertex vertex_end[2];	/**< These two vertices correspond to potential vertices on top and bottom RungF ends */
		std::vector<Vertex> vertices;	/**< Vertices in the RungF */
		std::vector<Link> links;	/**< Links in the RungF */
		Link endlink;			/**< The final link of the RungF */
		std::vector<std::string> info;	/**< A string container used to hold 'human-readble' information about *this */
		std::vector<Particle*> particles;	/**< Contains all pointers to particles in the rung (DO NOT include info on particles in vertex_end!!) with proper ordering (from top to bottom, following each vertex) */
		couplings_t coupling_powers;	/**< Global coupling powers of the RungF */
		std::vector<couplings_t> coupling_powers_per_vertex;	/**<  Coupling powers per Vertex inside rung */
		couplings_t coup_top; /**< Coupling powers for the top vertex */
		couplings_t coup_bot; /**< Coupling powers for the bottom vertex */

	public:
		/**
		 * Default constructor (empty rung)
		 */
		RungF() = default;
		/**
		 * Explicit constructor
		 *
		 * @param integer labeling top vertex
		 * @param std::vector with links and vertices (always 'going up')
		 * @param Link representing the end link
		 * @param integer labeling the bottom vertex (in 1-loop RunF's = to top integer label)
		 */
		RungF(int,std::vector<std::tuple<Link,Vertex> >,Link,int);
		/**
		 * Explicit constructor
		 *
		 * @param integer labeling top vertex
		 * @param std::vector with links and vertices (always 'going up')
		 * @param Link representing the end link
		 * @param integer labeling the bottom vertex (in 1-loop RunF's = to top integer label)
		 * @param Vertex representing the top vertex
		 * @param Vertex representing the bottom vertex
		 */
		RungF(int,std::vector<std::tuple<Link,Vertex> >,Link,int,Vertex,Vertex);
		/**
		 * A constructor that copies a rung, but modifies top and bottom vertices
		 *
		 * @param RungF to be copied
		 * @param Vertex top which will replace the one from first parameter
		 * @param Vertex bottom which will replace the one from first parameter
		 */
		RungF(const RungF&,const Vertex&,const Vertex&);
		/**
		 * Method that counts number of vertices in rung
		 */
		size_t get_multiplicity() const;
		/**
		 * Method that updates printable information stored in member 'info'
		 */
		void update_info();
		/**
		 * This method drops to the standard output human-readable information of *this
		 */
		void show();
		/**
		 * Returns information of *this contained in member 'info'
		 */
		std::vector<std::string > get_info();
		/**
		 * This method return a relatively small string with information of the rung (a "name" for *this)
		 */
		std::string get_rung_name();
		/**
		 * Pinch method with explicit vertices passed. There is a convention, in which the top vertex (Vertex_Top) and bottom vertex (Vertex_bottom) 
		 * passed are assumed to come to the right of the RungF. That is, if the zeroth propagator is pinched, the top vertex it Vertex_0 * Vertex_Top
		 * and if the last propagator is pinched, the bottom vertex is Vertex_last * Vertex_bottom
		 *
		 * @param size_t representing the propagator that is being pinched
		 * @param Vertex representing an external top vertex which will be used, considering it to be at the right of the RungF 
		 * @param Vertex representing an external bottom vertex which will be used, considering it to be at the right of the RungF 
		 */
		RungF pinch(size_t,Vertex,Vertex);
		/**
		 * Tool to perform several pinches in a row. It is based in RungF::pinch() and RungF::pinch_top_reverse() 
		 * (meant for two-loop cuts!)
		 */
		RungF multi_pinch(const std::vector<size_t>&);
		/** 
		 * Special pinch for 1-loop cuts
		 */
		RungF one_loop_pinch(size_t);
		/**
		 * Special pinch for two loops pinching zero and flipping corresponding vertex 
		 * (meant for 'left' rung in a two-loop cut)
		 */
		RungF pinch_top_reverse(Vertex,Vertex);
		/**
		 * Special pinch for two loops pinching last and flipping corresponding vertex
		 * (meant for 'right' rung in a two-loop cut)
		 */
		RungF pinch_bottom_reverse(Vertex,Vertex);
		/**
		 * Special pinch for a RungF in which the zeroth propagator is being drop. It is assumed that vertices passed are at the left of *this RungF,
		 * that is, unlike RungF::pinch(size_t,Vertex,Vertex), here the result as as top Vertex, Vertex_Top * Vertex_0 (with Vertex_Top the one passed as
		 * first parameter of the method)
		 *
		 * @param Vertex representing an external top vertex which will be used, considering it to be at the left of the RungF 
		 * @param Vertex representing an external bottom vertex which will be used, considering it to be at the left of the RungF 
		 */
		RungF pinch_top_of_right_rung(Vertex,Vertex);
		/**
		 * Method to get access to vertices in *this as in member 'vertices'
		 */
		const std::vector<Vertex>& get_vertices() const;
		std::vector<Vertex>& get_vertices();
		/**
		 * Method to get access to top vertex
		 */
		const Vertex& get_vertex_top();
		/**
		 * Method to get access to bottom vertex
		 */
		const Vertex& get_vertex_bottom();
		/**
		 * Method to check if *this is null (constructed with default RungF constructor or with a null endlink)
		 */
		bool is_null();
		/**
		 * Method to get access to particles in *this (as in member 'particles')
		 */
		const std::vector<Particle*>& get_particles() const;
		/**
		 * This method produces a copy of the rung replacing the corresponding passed particles with EMPTY top/bottom vertices
		 * except if explicitly passed
		 *
		 * NO CHANGE performed to links
		 */
		RungF map_particles(const std::vector<Particle*>&,const Vertex&,const Vertex&) const;
		/**
		 * Produces a 1-loop rotation to produce a new rung
		 *
		 * ASSUMES empty vertices
		 */
		RungF rotate_rung(size_t) const;
		/**
		 * Method that returns a rung which is a copy of *this but with a top/bottom flip
		 */
		RungF flip() const;
		/**
		 * Checks if passed particle pointer is contained inside (not in vertex_end's) the rung
		 */
		bool is_contained_inside(Particle*) const;
		/**
		 * Returns the links of the rung, including the endlink
		 * @return Vector containing the links in the rung.
		 */
		std::vector<Link> get_links() const;
                /**
                 * Returns a const ref to requested Link (with bound check)
                 */
                const Link& get_link(size_t) const;
		/**
		 * Returns the global powers of the couplings in the Rung
		 * @return [description]
		 */
		couplings_t get_coupling_powers() const;
		/**
		 * Returns the powers of the couplings in each Vertex
		 * @return The couplings powers per vertex
		 */
		std::vector<couplings_t> get_coupling_powers_per_vertex() const;
		/**
		 * Return the coupling powers of the top vertex
		 * @return coupling powers of the top vertex
		 */
		couplings_t get_top_coupling_powers() const;
		/**
		 * Return the coupling powers of the bottom vertex
		 * @return Coupling powers of the bottom vertex
		 */
		couplings_t get_bottom_coupling_powers() const;
		/**
		 * Returns true if all links in *this are massless. False otherwise
		 */
		bool all_massless_internal_links() const;
                /**
                 * Will search for the pointer in vertices and forward the call to corresponding container
                 */
                int get_internal_index(Particle*) const;
                /**
                 * Will search for the pointer in vertices and forward the call to corresponding container
                 */
                fermion_line_index get_fermion_line_index(Particle*) const;
                /**
                 * Will search for fermion_line_index in the corresponding link
                 */
                fermion_line_index get_link_fermion_line_index(size_t) const;
};

class mom_routing_and_mass;
mom_routing_and_mass operator+(const mom_routing_and_mass&,const mom_routing_and_mass&);
mom_routing_and_mass operator-(const mom_routing_and_mass&,const mom_routing_and_mass&);
std::ostream& operator<<(std::ostream&, RungF& );

// class for handling routing of momenta in RungF's and cutF's and mass information of propagators
class mom_routing_and_mass {
		friend mom_routing_and_mass operator+(const mom_routing_and_mass&,const mom_routing_and_mass&);
		friend mom_routing_and_mass operator-(const mom_routing_and_mass&,const mom_routing_and_mass&);

		int loop[2];
		std::vector<int> external;
		int multiplicity;
		size_t mass;
	public:
		mom_routing_and_mass();
		mom_routing_and_mass(int);
		mom_routing_and_mass(int,std::vector<int> iv);
		mom_routing_and_mass(int,int);
		mom_routing_and_mass(int,int,int);
		mom_routing_and_mass(int,int,int,std::vector<int>);
		// negative of instance
		mom_routing_and_mass operator-();
		// add external momenta contained in vector of Particle*
		mom_routing_and_mass operator+=(std::vector<Particle*>);
		// subtract external momenta contained in vector of Particle*
		mom_routing_and_mass operator-=(std::vector<Particle*>);
		// add momenta
		void add_momenta(int,int);
		// routing a RungF through its vertices
		void route_rung(RungF*);
		void route_rung(RungF*,size_t,size_t);
                void set_mass(size_t);
                size_t get_mass() const;
		// check mom conservation
		void mom_conservation();
		std::string get_info(std::string s="") const;
		// allows to substitute the given momentum into local one: mom_{local} + {external_{local}} = (mom_{passed}+{external_{passed}}) + external_{local}
		mom_routing_and_mass substitute(const mom_routing_and_mass&) const;
		// allows to substitute the given momentum into local one: mom_{local} + {external_{local}} = (mom_{passed}-{external_{passed}}) + external_{local}
		mom_routing_and_mass substitute_inverse(const mom_routing_and_mass&) const;
		// to get rule for grand-parent prop at 2-loops
		mom_routing_and_mass solve_prop_2loops(const mom_routing_and_mass&,const mom_routing_and_mass&) const;
		// 2-loop substitutions to get lP=lP(lD1a,lD1b) = lP(lD2a,lD2b)
		mom_routing_and_mass substitute(const mom_routing_and_mass&,const mom_routing_and_mass&) const;
	private:
		// check mom conservation
		void neg_mom_conservation();
};


class subtraction;
// the (non-commutative) operator for composing subtractions
subtraction operator*(const subtraction&,const subtraction&);

// This class is employed to build subtraction terms from parent cutF's
class subtraction {
		friend class CutHierarchy;
		friend subtraction operator*(const subtraction&,const subtraction&);

		// reference to associated cutF
		//cutF* parent_cutF;
		size_t nloops;
		int n_particles;
		size_t parent_level;
		size_t parent_cstructure_locate;
		// to identify dropped propagators (one pair identifies: <rung,propagator>)
			// propagator always goes from 1 to #props

		std::vector<std::pair<size_t,size_t> > dropped_props;
		// for each dropped propagator we store a momentum routing
		std::vector<mom_routing_and_mass> dropped_props_indices;
		// map to loop momenta from daughter (only one entry needed for 1-loop cuts)
			// identifies the propagator that the daughter produces as l1 and l2 (no l2 for 1-loop case)
			// to make the corresponding map
			// format: <rung,propagator> 	--> sign or propagator means top/bottom flip
			// propagator always goes from 1 to #props
		std::pair<size_t,int> loop_momenta_from_daughter[2];
		// a momentum routing for getting parent momenta from daughter
		mom_routing_and_mass loop_momenta_from_d[2];
		// in case there is a color related sign for this subtraction term
		int sign;
		// stores infor about if the parent cutF that is subtracted is unique, or several routings are performed
		bool is_unique_cutF_subtraction;
	public:
		subtraction();
		// constructor passes pointers to parent cutF and daughter cutF
		// index represents pinch used to produce daughter
		// -- extra defaulted cutF is passed in case directly pinched
		//    daughter cutF has been transformed into the passed daughter cutF
		subtraction(cutF*,cutF*,const size_t&,cutF* = nullptr);
		std::string get_info() const;
	private:
		// assign bow tie lP=lP(lDa,lDb) rules
		void assign_bow_tie(cutF*,size_t,size_t,bool,bool);
		// assign loop momenta rules for parents with 3-rung
		void assign_general(bool,cutF*);
};


bool operator==(const RungF&,const RungF&);

/**
 * This class holds purely kinematical information for cuts (for example, information shared among associated cuts with different external
 * helicities
 */
class CStructure {
		// exposing CStructure to these friends
		friend class cutF;
		friend class cutF_min;
		friend class CutHierarchy;

		size_t rung_count;	/**< 1 loop 1 rung ; 2 loop 2 or 3 rungs */
		std::vector<size_t> partition;	/**< Contains topological info of related S-tree. Each entry is the number of particles in corresponding vertex on partition */
		int n_particles;	/**< Number of external particles in the cut */
		/**
		 * Array for location of loop momenta (maximum # of loops considered is 2)
		 *
		 * 0th entry:  l1
		 *
		 * 1st entry: -l1
		 *
		 * 2nd entry:  l2
		 *
		 * 3rd entry: -l2
		 *
		 */
		size_t location_loops[4];
		std::vector<size_t> mom_indices;	/**< indices for tracing color ordering */

		// for now unused. Will be employed for linking to integrals
		std::vector<std::vector<int> > momenta_rungs;
		std::vector<std::vector<int> > link_masses;

		std::vector<std::vector<subtraction> > container_subs; /**< info for subtraction of parents, they match order in parent_cuts of associated cutF */
		bool accepts_direct_fit;	/**< keeps info on whether this cut has a doubled propagator */
	public:
                lGraph::xGraph graph;    /**< Keeps a copy of the xGraph associated to the cutF */
		CStructure();
		CStructure(const lGraph::xGraph&,size_t,std::vector<size_t>,int,size_t*,std::vector<size_t>,bool);
};

}
}
#endif	// CSTRUCTURE_H_
