#pragma once 

#include <cassert>
#include <utility>

#include "Core/typedefs.h"
#ifdef USE_FINITE_FIELDS
#include "Core/RationalReconstruction.h"
#endif

#include "Graph/IntegralGraph.h"
#include "AmpEng/Diagrammatica.h"
#include "AmpEng/FilterGraphs.h"
#include "AmpEng/Parenthood.h"
#include "AmpEng/CutEquation.h"
#include "AmplitudeCoefficients/SubtractionEngine.h" // <--- this is for 'load_warmups' function call
#include "FunctionalReconstruction/DenseRational.h"
#include "FunctionalReconstruction/ThieleFunction.h"
#include "Core/InversionStrategies.h"

namespace Caravel {
namespace AmpEng {

using BasisTruncation = std::vector<size_t>;
using HierarchyBasisTruncation = std::vector<std::vector<BasisTruncation>>;

template <typename T> class Evaluator {
  public:
    Evaluator() = default;
    /**
     * Constructs from a string (warmup filename) and an unsigned integer n (representing the monomials Ds^0, ... , Ds^n to be used in dimensional reconstruction)
     */
    Evaluator(const std::string&, size_t);
    /**
     * Constructs from a string (warmup filename) and a vector of functions parametrizing Ds dependence
     */
    Evaluator(const std::string&, const std::vector<DenseRational<T>>&);
    template <typename HierEngType> std::vector<DenseRational<T>> reduce(const momD_conf<T, 4>&, HierEngType&);
    template <typename HierEngType> std::vector<DenseRational<T>> reduce_diagram(const momD_conf<T, 4>&, HierEngType&, const std::pair<size_t, size_t>&);
    template <typename HierEngType> std::vector<DenseRational<DenseRational<T>, T>> reduce_with_Ds(const momD_conf<T, 4>&, HierEngType&);
    /**
     * Returns each master coefficient as an std::vector<DenseRational<T>>, where:
     *
     * std::vector has an entry for each coefficient of corresponding 'monomials' entry (i.e. each Ds function)
     * DenseRational<T> has the epsilon dependence
     */
    template <typename HierEngType> std::vector<std::vector<DenseRational<T>>> reduce_Ds_function_coefficients(const momD_conf<T, 4>&, HierEngType&);
    template <typename HierEngType> std::vector<DenseRational<DenseRational<T>, T>> reduce_diagram_with_Ds(const momD_conf<T, 4>&, HierEngType&, const std::pair<size_t, size_t>&);
    // FIXME: we don't need multiple methods for different Ds dim reg schemes (e.g. tHV, FDH), just add settings to control this and replace accordingly the
    // 'reduce' method above
    template <typename HierEngType> std::vector<DenseRational<T>> reduce_FDH(const momD_conf<T, 4>&, HierEngType&);
    template <typename HierEngType> std::vector<DenseRational<T>> reduce_tHV(const momD_conf<T, 4>&, HierEngType&);
    bool has_warmup() const { return !need_warmup; }
    bool reload_warmup();
    std::vector<std::shared_ptr<HierarchyWarmupInfo>> warmup_info;
    void set_Ds_function_basis(const std::vector<DenseRational<T>>& vfs) {
        assert(vfs.size() == monomials.size());
        monomials = vfs;
    }
    std::vector<DenseRational<T>> get_Ds_functions() const { return monomials; }

  private:
    /**
     * Helper function to methods 'reduce_with_Ds' and 'reduce_Ds_function_coefficients'
     */
    template <typename HierEngType> std::vector<DenseRational<Vec<T>, T>> reduce_internal_Ds(const momD_conf<T, 4>&, HierEngType&);
    std::string warmup_filename{"none"};
    bool need_warmup{true};
    bool firstcall{true};
    std::string ep_name = "ep";
    template <typename HierEngType> std::vector<DenseRational<T>> reduce_use_warmup(const momD_conf<T, 4>&, HierEngType&);
    template <typename HierEngType> std::vector<DenseRational<T>> reduce_gen_warmup(const momD_conf<T, 4>&, HierEngType&);
    std::vector<int> numerator_ranks;
    std::vector<DensePolynomial<T>> denominators;
    std::vector<DenseRational<T>> monomials;
    HierarchyBasisTruncation truncation;
    HierarchyBasisTruncation get_basis_truncation(const std::vector<std::vector<std::vector<T>>>&);
    HierarchyWarmupInfo get_warmup_info(const std::vector<std::vector<std::vector<T>>>&, std::vector<int>, std::vector<DensePolynomial<T>>);
};

/**
 * Produces functions for dimensional reconstruction of the form (Ds-2)^(-n), (Ds-2)^(-(n-1)), ... , (Ds-2)^(m-1), (Ds-2)^m
 */
template <typename T> std::vector<DenseRational<T>> powers_of_Dsm2(size_t n, size_t m);

} // namespace AmpEng

#ifndef USE_FINITE_FIELDS
// forward declaration
namespace RationalReconstruction {
// dummy declaration, should never be used without finite fields
template <typename T>
int rational_guess(const T&);
}
#endif

} // namespace Caravel

#include "Evaluator.hpp"
