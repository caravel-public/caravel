/**
 * @file Utilities.h
 *
 * @date 24.6.2022
 *
 * @brief Helpful tools for AmpEng
 *
*/
#pragma once

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "wise_enum.h"

#include "Core/Debug.h"
#include "Core/Particle.h"
#include "Graph/Graph.h"
#include "Graph/Utilities.h"
#include "Graph/GraphKin.h"
#include "AmpEng/Diagrammatica.h"
#include "OnShellStrategies/osm.h"
#include "OnShellStrategies/OnShellStrategies.h"
#include "Forest/Model.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "AmpEng/cutF.h"

namespace Caravel {
namespace AmpEng {

class Member;
class Registro;

/**
 * A data structure to aid cases where it is possible to map between Graph instances and cutTopology instances
 *
 * Use with caution
 */
struct GraphCutMap {
    std::map<size_t, std::shared_ptr<Particle>>
        external;                            /**< From an unsigned integer (the 'Leg' of corresponding Graph instance) to a (pointer to a) Particle */
    std::map<size_t, ParticleType> internal; /**< From an unsigned integer (the 'Link' of corresponding Graph instance) to a ParticleType */
    std::string coup;                        /**< For now, a string to identify coupling (n powers of each for each n+2 vertex) */
};

/**
 * This function uses a Forest/Builder evaluation of a 1-daughter cut in a 1-parent
 * on-shell phase space point, mapped through a given factorization channel (contained
 * in the Registro argument) to find if the corresponding vanishing propagator is 
 * present in the daughter cut
 *
 * Returns 'false' if a vanishing inverse propagator is encountered, 'true' otherwise
 */
bool check_spurious_fact_channel(const Member& parent, const Member& daughter, const Registro& reg, const std::string& smodel, const GraphCutMap& gcm);

struct LoopMomLocator {
    Particle p; /**< Represents the given particle in the loop */
    int looploc; /**< Where is the loop particle found in ordered list of particles in cutTopology */
    int mlooploc; /**< Where is the minus loop particle found in ordered list of particles in cutTopology */
    int looploc_momconf; /**< Index of loop momentum (of the particle) in a global momconf */
    int mlooploc_momconf;  /**< Index of minus loop momentum (of the particle) in a global momconf */
};

std::ostream& operator<<(std::ostream&, const LoopMomLocator&);

namespace _misc {

Particle _produce_a_particle_from_mass_index(size_t massindex, const GraphCutMap& gcm, const std::string& name, const SingleState& ss, size_t momindex = 0);

cutTopology getcutTopology(const lGraph::xGraph& xg, lGraph::xGraph& xgrout, std::vector<LoopMomLocator>& lml, const GraphCutMap& gcm);

lGraph::xGraph map_xgraph_mom_to_stree(const lGraph::xGraph& xg, const GraphCutMap& gcm);

}

} // namespace AmpEng
} // namespace Caravel

