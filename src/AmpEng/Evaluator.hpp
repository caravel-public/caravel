
namespace Caravel{
namespace AmpEng{

namespace _misc {

template <typename T>
std::vector<DenseRational<T>> get_Ds_monomials(size_t n) {
    std::vector<DenseRational<T>> toret;
    for (size_t ii = 0; ii <= n; ii++) {
        std::vector<T> num(ii + 1, T(0));
        num.back() = T(1);
        toret.push_back(DenseRational<T>(DensePolynomial<T>(num, "Ds"), DensePolynomial<T>(std::vector<T>{T{1}}, "Ds")));
    }
    return toret;
}

}

template <typename T> std::vector<DenseRational<T>> powers_of_Dsm2(size_t n, size_t m) {
    std::vector<DenseRational<T>> toret;
    DensePolynomial<T> Dsm2(std::vector<T>{T(-2), T(1)}, "Ds");
    DensePolynomial<T> one(std::vector<T>{T(1)}, "Ds");
    for (size_t ln = n; ln > 0; ln--) {
        auto Dsm2ln = Dsm2;
        for (size_t ii = 2; ii <= ln; ii++) Dsm2ln *= Dsm2;
        toret.push_back(DenseRational<T>(one, Dsm2ln));
    }
    toret.push_back(DenseRational<T>(one, one));
    for (size_t lm = 1; lm <= m; lm++) {
        auto Dsm2lm = Dsm2;
        for (size_t ii = 2; ii <= lm; ii++) Dsm2lm *= Dsm2;
        toret.push_back(DenseRational<T>(Dsm2lm, one));
    }
    return toret;
}

template <typename T> Evaluator<T>::Evaluator(const std::string& file_name, size_t n) : Evaluator(file_name, _misc::get_Ds_monomials<T>(n)) {}

template <typename T> Evaluator<T>::Evaluator(const std::string& file_name, const std::vector<DenseRational<T>>& im) : monomials(im) {
    // check for warmup
    warmup_filename = file_name;
    reload_warmup();
}

template <typename T> bool Evaluator<T>::reload_warmup() {
    std::ifstream warmup_file(warmup_filename);
    if (!warmup_file.is_open())
        std::cout << "No warmup file found!" << std::endl;
    else {
        std::cout << "Loading warmup file: " << warmup_filename << std::endl;
        warmup_info = { std::make_shared<HierarchyWarmupInfo>(load_warmups(warmup_filename)[0]) };
        truncation = std::get<0>(*warmup_info[0]);
        denominators = ::Caravel::detail::exact_polys_to_numeric<T>(std::get<1>(*warmup_info[0]));
        numerator_ranks = std::get<2>(*warmup_info[0]);
        need_warmup = false;
    }
    return true;
}

template <typename T> template <typename HierEngType> std::vector<DenseRational<T>> Evaluator<T>::reduce(const momD_conf<T, 4>& mc, HierEngType& hierarchy) {
    if (need_warmup)
        return reduce_gen_warmup(mc, hierarchy);
    else
        return reduce_use_warmup(mc, hierarchy);
}

template <typename T, typename Tin> std::vector<std::vector<std::vector<T>>> pick_first_Ds(const Tin& in) {
    std::vector<std::vector<std::vector<T>>> out;
    for(auto& l1: in) {
        out.push_back( std::vector<std::vector<T>>{} );
        for(auto& l2: l1) {
            out.back().push_back( std::vector<T>{} );
            for(auto& l3: l2) 
                out.back().back().push_back( l3[0] );
        }
    }
    return out;
}

template <typename T> template <typename HierEngType> std::vector<DenseRational<T>> Evaluator<T>::reduce_gen_warmup(const momD_conf<T, 4>& mc, HierEngType& hierarchy) {
    if constexpr (is_finite_field_v<T>) {
        std::mt19937 mt;
        std::function<T()> ep_gen = [mt]() mutable { return T(mt()); };

        // Find truncation of hierarchy
        T ep = ep_gen();
        hierarchy.eval(mc, ep);
        std::vector<std::vector<std::vector<T>>> cfs = pick_first_Ds<T>(hierarchy.get_master_coeffs(ep));
        truncation = hierarchy.get_basis_truncation();
        hierarchy.truncate_basis(truncation);

        // multiple evaluations are only nescessary if the coefficients have a d-dependence
        size_t Nep = settings::IntegrandHierarchy::n_cached_D_values_warmup;
        if (settings::general::numerator_decomposition == settings::general::IntegralBasis::tensors) Nep = 1;

        // precomute some values to speed things up
        std::unordered_map<T, std::vector<std::vector<std::vector<T>>>> table;
        for (size_t ii = 0; ii < Nep; ii++) {
            T ep = ep_gen();
            hierarchy.eval(mc, ep);
            table[ep] = pick_first_Ds<T>(hierarchy.get_master_coeffs(ep));
        }

        std::function<std::vector<T>(T)> cached_f = [&mc, &hierarchy, table = std::move(table)](T ep) {
            auto cached = table.find(ep);
            std::vector<std::vector<std::vector<T>>> res;
            if (cached != table.end())
                res = cached->second;
            else {
                hierarchy.eval(mc, ep);
                res = pick_first_Ds<T>(hierarchy.get_master_coeffs(ep));
            }

            // flatten output
            std::vector<T> res_flatten;
            for (size_t clevel = 0; clevel < res.size(); clevel++) {
                for (size_t diag = 0; diag < res[clevel].size(); diag++) {
                    for (auto s : res[clevel][diag]) res_flatten.push_back(s);
                }
            }
            return res_flatten;
        };

        VectorThieleFunction<T> reconstructed_function(cached_f, ep_gen, ep_name);
        std::vector<DenseRational<T>> result;
        for (auto& component : reconstructed_function.components) result.push_back(component.to_canonical());

        // Check if non-trivial GCD exists
        for (auto& f : result) {
            auto& num = f.numerator;
            auto& den = f.denominator;
            auto gcd = polynomial_GCD(num, den);
            if (!gcd.is_one()) {
                _WARNING("Non trivial GCD found: ", gcd);
                f.numerator = polynomial_divide(num, gcd).first;
                f.denominator = polynomial_divide(den, gcd).first;
            }
        }

        for (auto& f : result) {
            auto& num = f.numerator;
            auto& den = f.denominator;
            numerator_ranks.push_back(num.get_degree());
            denominators.push_back(den);
        }

        hierarchy.clear_cut_cache();

        // save warmup
        warmup_info = {std::make_shared<HierarchyWarmupInfo>(get_warmup_info(cfs, numerator_ranks, denominators))};
        write_warmup(warmup_filename, warmup_info);
        truncation = std::get<0>(*warmup_info[0]);
        denominators = ::Caravel::detail::exact_polys_to_numeric<T>(std::get<1>(*warmup_info[0]));
        numerator_ranks = std::get<2>(*warmup_info[0]);
        need_warmup = false;

        return result;
    }
    else {
        throw std::runtime_error("Evaluator can generate warmup only in a finite field!");
    }
}

template <typename T> template <typename HierEngType> std::vector<DenseRational<T>> Evaluator<T>::reduce_use_warmup(const momD_conf<T, 4>& mc, HierEngType& hierarchy) {
    if (firstcall) {
        hierarchy.truncate_basis(truncation);
        firstcall = false;
    }

    size_t max_rank = *std::max_element(numerator_ranks.begin(), numerator_ranks.end());
    std::function<T()> ep_gen = []() { return random_scaled_number<T>(T(1)); };
    std::vector<T> ep_vals;
    std::vector<std::vector<T>> master_coeffs;

    // Sample the function max_rank + 1 times
    for (size_t ii = 0; ii <= max_rank; ii++) {
        ep_vals.push_back(ep_gen());

        // compute the coefficents and multiply with known denominator functions
        hierarchy.eval(mc, ep_vals.at(ii));
        std::vector<std::vector<std::vector<T>>> cfs = pick_first_Ds<T>(hierarchy.get_master_coeffs(ep_vals.at(ii)));
        std::vector<T> coeffs;
        for (size_t clevel = 0; clevel < cfs.size(); clevel++) {
            for (size_t diag = 0; diag < cfs[clevel].size(); diag++) {
                for (auto s : cfs[clevel][diag]) coeffs.push_back(s);
            }
        }

        for (size_t jj = 0; jj < coeffs.size(); jj++) coeffs.at(jj) *= denominators.at(jj)(ep_vals.at(ii));
        master_coeffs.push_back(coeffs);
    }
    hierarchy.clear_cut_cache();

    // Fit a polynomial in ep to the sample values
    std::vector<DenseRational<T>> result;
    for (size_t ii = 0; ii < denominators.size(); ii++) {
        size_t num_rank = numerator_ranks.at(ii);
        std::vector<T> vals;
        for (size_t jj = 0; jj <= num_rank; jj++) vals.push_back(master_coeffs.at(jj).at(ii));
        DensePolynomial<T, T> master_numerator(vals, ep_vals, ep_name);
        DenseRational<T> coeff_function(master_numerator, denominators.at(ii));
        result.push_back(coeff_function);
    }

    return result;
}

// this is meant to become a constructor for DenseRational
template <typename T, typename Tvec> auto DimRec(const Tvec& vec, const std::vector<size_t>& Dss, const std::vector<DenseRational<T>>& functions) {
    assert(vec.size() > 0);
    // potentially vec[0].size() > Dss.size(), such that the extra data point can serve as tests of the following solution to the lineas system of equations
    assert(vec[0].numerator.coefficients[0].size() == Dss.size());
    assert(Dss.size() == functions.size());

    std::vector<T> A;
    for(auto& f: functions)
        for(auto& Ds: Dss)
            A.push_back(f(static_cast<T>(static_cast<int>(Ds))));
    
    std::vector<Vec<T>> b;
    for(size_t Di = 0; Di < Dss.size(); Di++) {
        std::vector<T> toconst;
        for(auto& integralcoeff: vec)
            for(auto& epcoeff: integralcoeff.numerator.coefficients)
                toconst.push_back(epcoeff[Di]);
        b.push_back(Vec<T>(toconst));
    }

    return plu_solver(std::move(A), std::move(b));
}

template <typename T, typename Tvec> std::vector<DenseRational<DenseRational<T>, T>> DimReconstruction(const Tvec& vec, const std::vector<size_t>& Dss, const std::vector<DenseRational<T>>& functions) {
    auto result = DimRec(vec, Dss, functions);

    std::vector<DenseRational<DenseRational<T>, T>> toret;

    std::string Dsname = functions[0].numerator.var_name;
    std::string var_name_ep = vec[0].numerator.var_name;

    size_t bcounter(0);
    for(auto& integralcoeff: vec) {
        std::vector<DenseRational<T>> toconstdp;
        for(auto& epcoeff: integralcoeff.numerator.coefficients) {
            (void)epcoeff; // FIXME: this suppresses unused variable warning, should the variable be actually used??
            DenseRational<T> local(Dsname);
            for(size_t Dsindex = 0; Dsindex < Dss.size(); Dsindex++) {
                auto& Dsfunctioncoeff = result[Dsindex];
                local += DenseRational<T>(Dsfunctioncoeff[bcounter] * functions[Dsindex].numerator, functions[Dsindex].denominator);
            }
            bcounter++;
            toconstdp.push_back(local);
        }
        DensePolynomial<DenseRational<T>, T> toconstdr(toconstdp, var_name_ep);
        toret.push_back( DenseRational<DenseRational<T>, T>(toconstdr, integralcoeff.denominator) );
    }

    return toret;
}

template <typename T, typename Tvec>
std::vector<std::vector<DenseRational<T>>> DimReconstructionCoeffs(const Tvec& vec, const std::vector<size_t>& Dss,
                                                                   const std::vector<DenseRational<T>>& functions) {
    auto result = DimRec(vec, Dss, functions);

    std::vector<std::vector<DenseRational<T>>> toret = std::vector<std::vector<DenseRational<T>>>(vec.size());
    for (auto& mi_coeff : toret) mi_coeff = std::vector<DenseRational<T>>(Dss.size());

    std::string var_name_ep = vec[0].numerator.var_name;

    for (size_t Dsindex = 0; Dsindex < Dss.size(); Dsindex++) {
        auto& Dsfunctioncoeff = result[Dsindex];
        size_t bcounter(0), mi_index(0);
        for (auto& integralcoeff : vec) {
            std::vector<T> num;
            for (auto& epcoeff : integralcoeff.numerator.coefficients) {
                (void)epcoeff; // FIXME: this suppresses unused variable warning, should the variable be actually used??
                num.push_back(Dsfunctioncoeff[bcounter++]);
            }
            DensePolynomial<T> numdp(num, var_name_ep);
            toret[mi_index][Dsindex] = DenseRational<T>(numdp, integralcoeff.denominator);
            mi_index++;
        }
    }

    return toret;
}

template <typename T>
template <typename HierEngType>
std::vector<DenseRational<DenseRational<T>, T>> Evaluator<T>::reduce_with_Ds(const momD_conf<T, 4>& mc, HierEngType& hierarchy) {
    auto result = reduce_internal_Ds(mc, hierarchy);
    return DimReconstruction<T>(result, hierarchy.get_Ds_values(), monomials);
}

template <typename T>
template <typename HierEngType>
std::vector<std::vector<DenseRational<T>>> Evaluator<T>::reduce_Ds_function_coefficients(const momD_conf<T, 4>& mc, HierEngType& hierarchy) {
    auto result = reduce_internal_Ds(mc, hierarchy);
    return DimReconstructionCoeffs<T>(result, hierarchy.get_Ds_values(), monomials);
}

template <typename T>
template <typename HierEngType>
std::vector<DenseRational<Vec<T>, T>> Evaluator<T>::reduce_internal_Ds(const momD_conf<T, 4>& mc, HierEngType& hierarchy) {
    // update denominators (in case of field change)
    denominators = ::Caravel::detail::exact_polys_to_numeric<T>(std::get<1>(*warmup_info[0]));

    if (firstcall) {
        if (need_warmup) {
            std::cerr << "ERROR: generate first a warmup file with reduce(...) function!" << std::endl;
            std::exit(1);
        }
        hierarchy.truncate_basis(truncation);
        firstcall = false;
    }

    //using coefftypeVec = Ratio<DensePolynomial<Vec<T>, T>, DensePolynomial<T>>;
    using coefftypeVec = Vec<T>;
    size_t max_rank = *std::max_element(numerator_ranks.begin(), numerator_ranks.end());
    std::function<T()> ep_gen = []() { return random_scaled_number<T>(T(1)); };
    std::vector<T> ep_vals;
    std::vector<std::vector<coefftypeVec>> master_coeffs;

    // Sample the function max_rank + 1 times
    for (size_t ii = 0; ii <= max_rank; ii++) {
        ep_vals.push_back(ep_gen());

        // compute the coefficents and multiply with known denominator functions
        hierarchy.eval(mc, ep_vals.at(ii));
        auto cfs = hierarchy.get_master_coeffs(ep_vals.at(ii));
        // flatten 3-D {level, diagram, master} array of master coefficients into 1-D vector
        auto coeffs = cfs[0][0];
        coeffs.clear();
        for (auto& level : cfs) {
            for (auto& diagram : level) {
                for (auto& master : diagram) coeffs.push_back(master);
            }
        }

        for (size_t jj = 0; jj < coeffs.size(); jj++) coeffs.at(jj) *= denominators.at(jj)(ep_vals.at(ii));
        master_coeffs.push_back(coeffs);
    }
    hierarchy.clear_cut_cache();

    // Fit a polynomial in ep to the sample values
    std::vector<DenseRational<Vec<T>, T>> result;
    for (size_t ii = 0; ii < denominators.size(); ii++) {
        size_t num_rank = numerator_ranks.at(ii);
        std::vector<coefftypeVec> vals;
        for (size_t jj = 0; jj <= num_rank; jj++) vals.push_back(master_coeffs.at(jj).at(ii));
        DensePolynomial<Vec<T>, T> master_numerator(vals, ep_vals, ep_name);
        DenseRational<Vec<T>, T> coeff_function(master_numerator, denominators.at(ii));
        result.push_back(coeff_function);
    }
    return result;
}

template <typename T> template <typename HierEngType>
std::vector<DenseRational<DenseRational<T>, T>> Evaluator<T>::reduce_diagram_with_Ds(const momD_conf<T, 4>& mc, HierEngType& hierarchy,
                                                                                        const std::pair<size_t, size_t>& diagram) {
    if (firstcall) {
        if (need_warmup) {
            std::cerr << "ERROR: generate first a warmup file with reduce(...) function!" << std::endl;
            std::exit(1);
        }
        hierarchy.truncate_basis(truncation);
        firstcall = false;
    }

    //using coefftypeVec = Ratio<DensePolynomial<Vec<T>, T>, DensePolynomial<T>>;
    using coefftypeVec = Vec<T>;
    size_t max_rank = *std::max_element(numerator_ranks.begin(), numerator_ranks.end());
    std::function<T()> ep_gen = []() { return random_scaled_number<T>(T(1)); };
    std::vector<T> ep_vals;
    std::vector<std::vector<coefftypeVec>> master_coeffs;

    // find location of first denominator
    size_t index_first_denom = hierarchy.n_masters_up_to(diagram);

    // Sample the function max_rank + 1 times
    for (size_t ii = 0; ii <= max_rank; ii++) {
        ep_vals.push_back(ep_gen());

        // compute the coefficents and multiply with known denominator functions
        hierarchy.eval_diagram(mc, ep_vals.at(ii), diagram);
        auto coeffs = hierarchy.get_diagram_master_coeffs(ep_vals.at(ii), diagram);

        for (size_t jj = 0; jj < coeffs.size(); jj++) coeffs.at(jj) *= denominators.at(index_first_denom + jj)(ep_vals.at(ii));
        master_coeffs.push_back(coeffs);
    }
    hierarchy.clear_cut_cache();

    // Fit a polynomial in ep to the sample values
    std::vector<DenseRational<Vec<T>, T>> result;
    for (size_t ii = 0; ii < master_coeffs.at(0).size(); ii++) {
        size_t num_rank = numerator_ranks.at(index_first_denom + ii);
        std::vector<coefftypeVec> vals;
        for (size_t jj = 0; jj <= num_rank; jj++) vals.push_back(master_coeffs.at(jj).at(ii));
        DensePolynomial<Vec<T>, T> master_numerator(vals, ep_vals, ep_name);
        DenseRational<Vec<T>, T> coeff_function(master_numerator, denominators.at(index_first_denom + ii));
        result.push_back(coeff_function);
    }

    return DimReconstruction<T>(result, hierarchy.get_Ds_values(), monomials);
}

template <typename T> template <typename HierEngType> std::vector<DenseRational<T>> Evaluator<T>::reduce_FDH(const momD_conf<T, 4>& mc, HierEngType& hierarchy) {
    auto fullDs = reduce_with_Ds(mc, hierarchy);
    std::vector<DenseRational<T>> toret;
    // Ds = 4
    DensePolynomial<T> Dsval({T(4)}, "ep");
    for(auto coeffDs: fullDs) {
        toret.push_back( substitute(Dsval, coeffDs) );
    }
    return toret;
}

template <typename T> template <typename HierEngType> std::vector<DenseRational<T>> Evaluator<T>::reduce_tHV(const momD_conf<T, 4>& mc, HierEngType& hierarchy) {
    auto fullDs = reduce_with_Ds(mc, hierarchy);
    std::vector<DenseRational<T>> toret;
    // Ds = 4 - 2*ep
    DensePolynomial<T> Dsval({T(4), T(-2)}, "ep");
    for(auto coeffDs: fullDs) {
        toret.push_back( substitute(Dsval, coeffDs) );
    }
    return toret;
}

// FIXME: Handle explicit diagram as optional parameter to regular reduce_use_warmup function?
template <typename T> template <typename HierEngType>
std::vector<DenseRational<T>> Evaluator<T>::reduce_diagram(const momD_conf<T, 4>& mc, HierEngType& hierarchy,
                                                              const std::pair<size_t, size_t>& diagram) {
    if (firstcall) {
        if (need_warmup) {
            std::cerr << "ERROR: generate first a warmup file with reduce(...) function!" << std::endl;
            std::exit(1);
        }
        hierarchy.truncate_basis(truncation);
        firstcall = false;
    }

    size_t max_rank = *std::max_element(numerator_ranks.begin(), numerator_ranks.end());
    std::function<T()> ep_gen = []() { return random_scaled_number<T>(T(1)); };
    std::vector<T> ep_vals;
    std::vector<std::vector<T>> master_coeffs;

    // Sample the function max_rank + 1 times
    for (size_t ii = 0; ii <= max_rank; ii++) {
        ep_vals.push_back(ep_gen());

        // compute the coefficents and multiply with known denominator functions
        hierarchy.eval_diagram(mc, ep_vals.at(ii), diagram);
        std::vector<std::vector<std::vector<T>>> cfs = pick_first_Ds<T>(hierarchy.get_diagram_master_coeffs(ep_vals.at(ii), diagram));
        std::vector<T> coeffs;
        for (size_t clevel = 0; clevel < cfs.size(); clevel++) {
            for (size_t diag = 0; diag < cfs[clevel].size(); diag++) {
                for (auto s : cfs[clevel][diag]) coeffs.push_back(s);
            }
        }

        for (size_t jj = 0; jj < coeffs.size(); jj++) coeffs.at(jj) *= denominators.at(jj)(ep_vals.at(ii));
        master_coeffs.push_back(coeffs);
    }
    hierarchy.clear_cut_cache();

    // Fit a polynomial in ep to the sample values
    std::vector<DenseRational<T>> result;
    for (size_t ii = 0; ii < denominators.size(); ii++) {
        size_t num_rank = numerator_ranks.at(ii);
        std::vector<T> vals;
        for (size_t jj = 0; jj <= num_rank; jj++) vals.push_back(master_coeffs.at(jj).at(ii));
        DensePolynomial<T, T> master_numerator(vals, ep_vals, ep_name);
        DenseRational<T> coeff_function(master_numerator, denominators.at(ii));
        result.push_back(coeff_function);
    }

    return result;
}

template <typename T> HierarchyBasisTruncation Evaluator<T>::get_basis_truncation(const std::vector<std::vector<std::vector<T>>>& coeffs) {
    HierarchyBasisTruncation basis_truncations;

    // Find vanishing coefficients
    for (size_t clevel = 0; clevel < coeffs.size(); clevel++) {
        std::vector<BasisTruncation> zero_lvl;
        for (size_t diag = 0; diag < coeffs[clevel].size(); diag++) {
            BasisTruncation zero;
            for (size_t jj = 0; jj < coeffs[clevel][diag].size(); jj++) {
                if (coeffs[clevel][diag][jj] == T(0)) zero.push_back(jj);
            }
            zero_lvl.push_back(zero);
        }
        basis_truncations.push_back(zero_lvl);
    }

    return basis_truncations;
}

template <typename T>
HierarchyWarmupInfo Evaluator<T>::get_warmup_info(const std::vector<std::vector<std::vector<T>>>& coeffs, std::vector<int> numerator_ranks,
                                                     std::vector<DensePolynomial<T>> denominators) {
    //    HierarchyBasisTruncation basis_truncations = get_basis_truncation(coeffs);

    if constexpr (is_finite_field_v<T>) {

        auto rational_guess_int = [](T& x) {
            auto result = RationalReconstruction::rational_guess(x);

            if (abs(result.num()) > 3500 or abs(result.den()) > 30) {
                _WARNING("WARNING: epsilon-dependent denominators were determined to include suspiciously large rational numbers ", result);
                _WARNING("This likely means that denominators are kinematically-dependent or something else went wrong, take care!");
            }
            return result;
        };

        std::vector<DensePolynomial<BigRat>> exact_denominators;
        for (auto& den : denominators) {
            std::vector<BigRat> exact_coeffs;
            for (auto& coeff : den.coefficients) { exact_coeffs.push_back(rational_guess_int(coeff)); }
            exact_denominators.push_back(exact_coeffs);
        }

        return std::make_tuple(truncation, exact_denominators, numerator_ranks);
    }
    else {
        throw std::runtime_error("Evaluator::get_warmup_info can run only in a finite field");
    }
}

}
}
