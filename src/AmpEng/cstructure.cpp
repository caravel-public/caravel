/**
 * @file cstructure.cpp
 *
 * @date 24.7.2016
 *
 * @brief Implementation of tools for building multi-loop (planar) hierarchies
 *
*/

#include <iostream>
#include <algorithm>
#include "AmpEng/cstructure.h"

#include "Core/Particle.h"
#include "Core/Debug.h"
#include "AmpEng/cutF.h"

#include "Core/Utilities.h"

namespace Caravel {
namespace AmpEng{

using couplings_t = std::map<std::string,int>;

// Empty vertex with given coupling powers
Vertex::Vertex(const couplings_t& cp) : Vertex(){
	coupling_powers = cp;
}

// one-particle vertex
Vertex::Vertex(Particle* p, const couplings_t& cp) {
	// copy particle to corresponding container
	if(p->is_colored()) {
		content_colored.push_back(p);
		fermion_indices.push_back(p->get_internal_index());
	} else
		// no need for sorting
		content_colorless.push_back(p);
	content_all.push_back(p);
	coupling_powers = cp;
        // no fermion line by default
        for(const auto& pp:content_colorless)  fline_colorless[pp]={false,0};
        for(const auto& pp:content_colored)  fline_colored[pp]={false,0};
}

// build directly the vertex
Vertex::Vertex(std::vector<Particle*> vp, const couplings_t& cp) : coupling_powers(cp){
	for(size_t ii=0;ii<vp.size();ii++){
		if(vp[ii]->is_colored()) {
			content_colored.push_back(vp[ii]);
			fermion_indices.push_back(vp[ii]->get_internal_index());
		} else
			// Will sort after loop
			content_colorless.push_back(vp[ii]);
	}
	// sort colorless particles
	std::sort(content_colorless.begin(),content_colorless.end());
	content_all=content_colorless;
	content_all.insert(content_all.end(),content_colored.begin(),content_colored.end());
        // no fermion line by default
        for(const auto& pp:content_colorless)  fline_colorless[pp]={false,0};
        for(const auto& pp:content_colored)  fline_colored[pp]={false,0};

}

// build directly the vertex
Vertex::Vertex(std::vector<Particle*> vp, const couplings_t& cp, std::vector<int> vind) : fermion_indices(vind), coupling_powers(cp){
	for(size_t ii=0;ii<vp.size();ii++){
		if(vp[ii]->is_colored())
			content_colored.push_back(vp[ii]);
		else	
			// Will sort after loop
			content_colorless.push_back(vp[ii]);
	}
	// sort colorless particles
	std::sort(content_colorless.begin(),content_colorless.end());
	content_all=content_colorless;
	content_all.insert(content_all.end(),content_colored.begin(),content_colored.end());
        // no fermion line by default
        for(const auto& pp:content_colorless)  fline_colorless[pp]={false,0};
        for(const auto& pp:content_colored)  fline_colored[pp]={false,0};
}

couplings_t add_couplings(couplings_t ca,const couplings_t& cb){
	// we add cb to ca
	for(auto c:cb){
		auto key = c.first;
		if(ca.find(key)==ca.end())
			ca[key]=c.second;
		else
			ca[key]+=c.second;
	}
	return ca;
}

// vertex product: merges vectors of particles in order and return
Vertex operator*(Vertex v1,const Vertex& v2){
	v1.content_colored.insert(v1.content_colored.end(),v2.content_colored.begin(),v2.content_colored.end());
	v1.content_colorless.insert(v1.content_colorless.end(),v2.content_colorless.begin(),v2.content_colorless.end());
	// and sort
	std::sort(v1.content_colorless.begin(),v1.content_colorless.end());
	v1.content_all=v1.content_colorless;
	v1.content_all.insert(v1.content_all.end(),v1.content_colored.begin(),v1.content_colored.end());
	// Now add the couplings
	v1.coupling_powers=add_couplings(v1.coupling_powers,v2.coupling_powers);

        for(const auto& fli:v2.fline_colorless)  v1.fline_colorless[fli.first]=fli.second;
        for(const auto& fli:v2.fline_colored)  v1.fline_colored[fli.first]=fli.second;

				v1.fermion_indices.insert(v1.fermion_indices.end(),v2.fermion_indices.begin(),v2.fermion_indices.end());

	return v1;
}

std::ostream& operator<<(std::ostream& s, const Vertex& p ){
    s << "{";
    for(const auto& it: p.get_particles()){
        s << *it;
    }
    s << "}";
    using Caravel::operator<<;
    s << "[" << p.get_coupling_powers() << "]";
    return s;
}

// prints a set like "{l1,l2,..}{p1,p2,...}" where the initial are colorless and the final colored
std::string Vertex::get_name() const {
	std::string r;
	if(content_colorless.size()>0){
		r+="<";
		r+=content_colorless[0]->get_name();
                if(fline_colorless.at(content_colorless[0]).assigned)
                    // report fermion line
                    r+="(fl:"+std::to_string(fline_colorless.at(content_colorless[0]).value)+")";
		for(size_t ii=1;ii<content_colorless.size();ii++){
			r+=","+content_colorless[ii]->get_name();
                        if(fline_colorless.at(content_colorless[ii]).assigned)
                            // report fermion line
                            r+="(fl:"+std::to_string(fline_colorless.at(content_colorless[ii]).value)+")";
                }
		r+=">";
	}
	if(content_colored.size()>0){
		r+="{";
		r+=content_colored[0]->get_name();
                if(fline_colored.at(content_colored[0]).assigned)
                    // report fermion line
                    r+="(fl:"+std::to_string(fline_colored.at(content_colored[0]).value)+")";
		for(size_t ii=1;ii<content_colored.size();ii++){
			r+=","+content_colored[ii]->get_name();
                        if(fline_colored.at(content_colored[ii]).assigned)
                            // report fermion line
                            r+="(fl:"+std::to_string(fline_colored.at(content_colored[ii]).value)+")";
                }
		r+="}";
	}
	// Add the information on the coupling powers to the string
	r += "[";
	size_t counter(0);
	for(auto c:coupling_powers){
		if(counter>0)	r += ",";
		r += c.first+"^"+std::to_string(c.second);
		counter++;
	}
	r += "]";
	return r;
}

// counts number of particles in vertex
size_t Vertex::get_multiplicity() const {
	return content_all.size();
}

// to access to content
const std::vector<Particle*>& Vertex::get_particles() const {
	return content_all;
}

// to access colored content
const std::vector<Particle*>& Vertex::get_colored() const {
	return content_colored;
}

void Vertex::set_internal_index(size_t particle,int index) {
	if(particle+1>content_colored.size()) {
		std::cout<<"ERROR: Vertex::set_internal_index(.) tried to access colored particle # "<<particle<<" when there is only: "<<content_all.size()<<" --- Did nothing! "<<std::endl;
		return;
	}
	fermion_indices[particle]=index;
}

// returns a Vertex wich maps the passed particles
Vertex Vertex::map_particles(const std::vector<Particle*>& ivp) const {
	std::cout << "Careful: Vertex::map_particles only works for gluons" << std::endl;
	if(content_all.size()!=ivp.size()){
		std::cout<<"Vertex::map_particles() has to receive proper sized vector -- Returned empty Vertex"<<std::endl;
		return Vertex();
	}
	return Vertex(ivp, this->coupling_powers, this->fermion_indices);
}

// returns a Vertex with flipped order of particles
Vertex Vertex::flip() const {
	std::vector<Particle*> ivp;
	ivp.insert(ivp.end(),content_all.rbegin(),content_all.rend());
	std::vector<int> inds;
	inds.insert(inds.end(),fermion_indices.rbegin(),fermion_indices.rend());
	return Vertex(ivp, this->get_coupling_powers(),inds);
}

bool Vertex::is_contained(Particle* p) const {
	return std::find(content_all.begin(),content_all.end(),p)!=content_all.end();
}

couplings_t Vertex::get_coupling_powers() const {
	return coupling_powers;
}

void Vertex::set_to_zero_all_couplings() {
	for(auto c:coupling_powers)
		coupling_powers[c.first]=0;
}

std::vector<int> Vertex::get_internal_indices() const {
	return fermion_indices;
}

void Vertex::set_internal_index(Particle* pp,int i){
	bool found=false;
	for(size_t j=0;j<content_colored.size();++j)
		if(content_colored[j]==pp) {
			found=true;
			fermion_indices[j]=i;
		}
	if(!found) {
		std::cerr<<"Tried to Vertex::set_internal_index(.) for particle pointer not in this->get_colored(): "<<pp<<std::endl;
		std::exit(1);
	}
}

int Vertex::get_internal_index(Particle* pp) const {
	if(std::find(content_colorless.begin(),content_colorless.end(),pp)!=content_colorless.end())
		return 0;
	for(size_t j=0;j<content_colored.size();++j)
		if(content_colored[j]==pp)
			return fermion_indices[j];
	std::cerr<<"Tried to Vertex::get_internal_index(.) for particle pointer not in *this: "<<pp<<std::endl;
	std::exit(1);
	return 0;
}

void Vertex::set_fermion_line_index(Particle* pp,size_t i){
    if(fline_colorless.find(pp)!=fline_colorless.end())
        fline_colorless[pp]={true, i};
    else if(fline_colored.find(pp)!=fline_colored.end())
        fline_colored[pp]={true, i};
    else{
        std::cerr<<"Tried to Vertex::set_fermion_line_index(.) for particle pointer not in *this: "<<pp<<std::endl;
        std::exit(1);
    }
}

fermion_line_index Vertex::get_fermion_line_index(Particle* pp) const {
    if(fline_colorless.find(pp)!=fline_colorless.end())
        return fline_colorless.at(pp);
    else if(fline_colored.find(pp)!=fline_colored.end())
        return fline_colored.at(pp);
   else{
        std::cerr<<"Tried to Vertex::get_fermion_line_index(.) for particle pointer not in *this: "<<pp<<std::endl;
        std::exit(1);
    }
}

bool operator==(const Vertex& v1,const Vertex& v2){
	// quick size check
	if(v1.content_colorless.size()!=v2.content_colorless.size())
		return false;
	if(v1.content_colored.size()!=v2.content_colored.size())
		return false;
	if(v1.coupling_powers.size() != v2.coupling_powers.size())
		return false;
	// colorless (assumed sorted) particle by particle check, of pointers!!
	for(size_t ii=0;ii<v1.content_colorless.size();ii++){
		if(v1.content_colorless[ii]!=v2.content_colorless[ii])
			return false;
	}
	// colored particle by particle check, of pointers!!
	for(size_t ii=0;ii<v1.content_colored.size();ii++){
		if(v1.content_colored[ii]!=v2.content_colored[ii]||v1.fermion_indices[ii]!=v2.fermion_indices[ii])
			return false;
	}
        if(!std::equal(v1.coupling_powers.begin(),v1.coupling_powers.end(),v2.coupling_powers.begin(), [] (auto a, auto b) { return (a.first == b.first) && (a.second == b.second); } ))
		return false;
	return true;
}

Link::Link(): type(link_null), id(0), fermion_indices({0}), findex({false, 0}) {}

Link::Link(Particle part, link_type lt, int i) :
	type(lt), id(i), particle(part), fermion_indices({0}) {
	if(lt==link_internal)
		fermion_indices={0,0};
        // no fermion line by default
        findex={false, 0};
}

Particle Link::get_particle() const {
	return particle;
}

void Link::set_link_type(link_type lt) {
	link_type old_type(type);
	std::vector<int> old_indices(fermion_indices);
	type = lt;
	if(lt==link_internal){
		std::cout<<"ERROR: not enough info to properly track fermion_indices in Link::set_link_type(.)! Set them to zero pair"<<std::endl;
		fermion_indices={0,0};
	}
	else if(lt==link_start){
		if(old_type==link_internal){
			fermion_indices={old_indices.back()};
		}
		else{
			std::cout<<"ERROR: not enough info to properly track fermion_indices in Link::set_link_type(.)! As 'link_start', set them to {0}"<<std::endl;
			fermion_indices={0};
		}
	}
	else if(lt==link_end){
		if(old_type==link_internal){
			fermion_indices={old_indices[0]};
		}
		else{
			std::cout<<"ERROR: not enough info to properly track fermion_indices in Link::set_link_type(.)! As 'link_end', set them to {0}"<<std::endl;
			fermion_indices={0};
		}
	}
	else
		fermion_indices={0};
}

void Link::set_link_type(link_type lt,const std::vector<int>& in) {
	type = lt;
	fermion_indices=in;
}

Link Link::flip() const {
	link_type trlt(link_null);
	if(type==link_null||type==link_internal)
		trlt=type;
	else if(type==link_end)
		trlt=link_start;
	else
		trlt=link_end;

	std::vector<int> lfermion_indices;
	for(size_t ii=fermion_indices.size();ii>0;ii--)
		lfermion_indices.push_back(fermion_indices[ii-1]);

	Link tret(
			Particle("a"+particle.get_name(),particle.get_anti_type(),
					particle.external_state(),particle.mom_index(),particle.ref_index()),
			trlt,
			id
		);
	tret.fermion_indices=lfermion_indices;
	return tret;
}

std::string Link::get_info() const {
	return "("+particle.get_type().to_string()+","+std::to_string(id)+(findex.assigned?"(fl:"+std::to_string(findex.value)+")":"")+")"+"(dtt:"+std::to_string(particle.external_state().get_dtt_index())+")"+"(repr:"+std::string(particle.external_state().get_representation()==repr::La?"La":(particle.external_state().get_representation()==repr::Lat?"Lat":"undefined"))+")";
}

ParticleType Link::get_particle_type() const {
	return particle.get_type();
}

link_type Link::get_type() const {
	return type;
}

bool Link::is_null() const {
	if(type==link_null)	return true;
	else			return false;
}

bool operator==(const Link& l1,const Link& l2){
	DEBUG_MESSAGE("Link operator== for now only checking particle.get_type() string!!");
	return l1.particle.get_type()==l2.particle.get_type();
}

std::ostream& operator<<(std::ostream& s, const Link& l ){
    s << l.get_info();
    return s;
}

void Link::set_up_internal_index(int f){
	if(type==link_end)
		fermion_indices={f};
	else if(type==link_start)
		std::cout<<"Link::set_up_internal_index("<<f<<") trying to set UP 'internal_index' in a 'link_start' -- Did nothing!!"<<std::endl;
	else if(type==link_null)
		std::cout<<"Link::set_up_internal_index("<<f<<") trying to set UP 'internal_index' in a 'link_null' -- Did nothing!!"<<std::endl;
	else
		fermion_indices[0]=f;
}

void Link::set_down_internal_index(int f){
	if(type==link_start)
		fermion_indices={f};
	else if(type==link_end)
		std::cout<<"Link::set_down_internal_index("<<f<<") trying to set UP 'internal_index' in a 'link_end' -- Did nothing!!"<<std::endl;
	else if(type==link_null)
		std::cout<<"Link::set_down_internal_index("<<f<<") trying to set UP 'internal_index' in a 'link_null' -- Did nothing!!"<<std::endl;
	else
		fermion_indices[1]=f;
}

int Link::get_up_internal_index() const {
	if(type==link_start){
		std::cout<<"Link::get_up_internal_index() trying to get UP 'internal_index' in a 'link_start' -- Returned -1!! "<<get_info()<<std::endl;
		return -1;
	}
	else if(type==link_end)
		return fermion_indices[0];
	else if(type==link_null){
		std::cout<<"Link::get_up_internal_index() trying to get UP 'internal_index' in a 'link_null' -- Returned -1!!"<<std::endl;
		return -1;
	}
	else
		return fermion_indices[0];
}

int Link::get_down_internal_index() const {
	if(type==link_end){
		std::cout<<"Link::get_down_internal_index() trying to get DOWN 'internal_index' in a 'link_end' -- Returned -1!!"<<std::endl;
		return -1;
	}
	else if(type==link_start)
		return fermion_indices[0];
	else if(type==link_null){
		std::cout<<"Link::get_down_internal_index() trying to get DOWN 'internal_index' in a 'link_null' -- Returned -1!!"<<std::endl;
		return -1;
	}
	else
		return fermion_indices[1];
}

bool Link::is_fermion_link() const {
	return particle.get_statistics()==ParticleStatistics::fermion;
}

bool Link::is_antifermion_link() const {
	return particle.get_statistics()==ParticleStatistics::antifermion;
}

void Link::set_fermion_line_index(size_t i){ findex={true, i}; }
fermion_line_index Link::get_fermion_line_index() const { return findex; }

// explicit constructor: vector with links and vertices, plus final link in rung
RungF::RungF(int itop,std::vector<std::tuple<Link,Vertex> > structure,Link finallink,int ibot): match_top(itop), match_bottom(ibot), endlink(finallink) {
	if(structure.size()>0){
		// Assign zero couplings to top and bottom vertices
		vertex_end[0]=Vertex();
		vertex_end[1]=Vertex();
	}

	for(size_t ii=0;ii<structure.size();ii++){
		links.push_back(std::get<0>(structure[ii]));
		Vertex& lvertex(std::get<1>(structure[ii]));
		if(lvertex.get_multiplicity()>0){
			vertices.push_back(lvertex);
		}
		else{
			std::cout<<"A RungF constructor was called with an empty internal Vertex --- exit 1"<<std::endl;
			exit(1);
		}
	}
	// construct particles
	for(size_t ii=0;ii<vertices.size();ii++){
		const std::vector<Particle*>& lparts(vertices[ii].get_particles());
		for(size_t jj=0;jj<lparts.size();jj++)
			particles.push_back(lparts[jj]);
	}
	// Count the coupling powers per vertex
	// Add the top vertex couplings
	for(auto& struc : structure){
		coupling_powers_per_vertex.push_back(std::get<1>(struc).get_coupling_powers());
	}
	for (auto cp : coupling_powers_per_vertex){
		coupling_powers=add_couplings(coupling_powers,cp);
	}
	this->update_info();
}

// add to previous constructor possibility of defining top and bottom vertices
RungF::RungF(int t,std::vector<std::tuple<Link,Vertex> > s,Link fl,int b,Vertex tV,Vertex bV): RungF(t,s,fl,b) {
	vertex_end[0]=tV;
	vertex_end[1]=bV;

	// Take care of the coupling powers
	// Add the top vertex
	coup_top = tV.get_coupling_powers();
	// Append the bottom vertex
	coup_bot = bV.get_coupling_powers();

	// Adjust the global coupling powers by including the top and bottom vertices powers
        coupling_powers=add_couplings(coupling_powers,coup_top);
        coupling_powers=add_couplings(coupling_powers,coup_bot);
}

// copy constructor that passes explicit values of top and bottom vertices
RungF::RungF(const RungF& ir,const Vertex& v1,const Vertex& v2): RungF(ir) {
	vertex_end[0]=v1;
	vertex_end[1]=v2;

	coup_top = v1.get_coupling_powers();
	coup_bot = v2.get_coupling_powers();

	// Adjust the global coupling powers by including the top and bottom vertices powers
        coupling_powers=add_couplings(coupling_powers,coup_top);
        coupling_powers=add_couplings(coupling_powers,coup_bot);
}

// function to normalize vector of string (modifies entries such that all are same length)
void normalize_string_vector(std::vector<std::string>& iv){
	size_t maxsize(0);
	// find max size
	for(size_t ii=0;ii<iv.size();ii++)
		if(iv[ii].size()>maxsize) maxsize=iv[ii].size();
	// normalize
	for(size_t ii=0;ii<iv.size();ii++){
		std::string add("");
		for(int jj=0;jj<int(maxsize)-int(iv[ii].size());jj++)
			add+=" ";
		iv[ii]+=add;
	}
}

// counts number of vertices in rung
size_t RungF::get_multiplicity() const {
	return vertices.size();
}

// updates information contained in info vector
void RungF::update_info(){
	info.clear();
	std::string lstring("");
	size_t nvertices(vertices.size());
	// case of single propagator
	if(nvertices==0){
		// null link between top and bottom
		if(endlink.get_type()==link_null){
			lstring="\\/ "+std::to_string(match_top);
			info.push_back(lstring);
			lstring="/\\ "+std::to_string(match_bottom);
			info.push_back(lstring);
		}
		else{
			lstring="-^- "+std::to_string(match_top);
			info.push_back(lstring);
			lstring=" |";
			info.push_back(lstring);
			lstring+=" "+endlink.get_info();
			info.push_back(lstring);
			lstring=" |";
			info.push_back(lstring);
			lstring="-^- "+std::to_string(match_bottom);
			info.push_back(lstring);
		}
	}
	// general case
	else{
		lstring="-^- "+std::to_string(match_top)+": "+links[0].get_info();
		info.push_back(lstring);
		std::vector<int> findices;
		int findex(links[0].get_down_internal_index());
		for(size_t ii=1;ii<nvertices;ii++){ 
			lstring=" |";
			if(findex!=0)
				lstring+="~"+std::to_string(findex);
			findex=links[ii].get_up_internal_index();
			info.push_back(lstring);
			lstring=" |";
			vertices[ii-1].get_multiplicity()==1?lstring+="--- ":lstring+="==< ";
				lstring+=vertices[ii-1].get_name();
			info.push_back(lstring);
			lstring=" |";
			if(findex!=0)
				lstring+="~"+std::to_string(findex);
			findex=links[ii].get_down_internal_index();
			info.push_back(lstring);
			lstring=" |"+links[ii].get_info();
			info.push_back(lstring);
		}
		lstring=" |";
		if(findex!=0)
			lstring+="~"+std::to_string(findex);
		findex=endlink.get_up_internal_index();
		info.push_back(lstring);
		lstring=" |";
		vertices[nvertices-1].get_multiplicity()==1?lstring+="--- ":lstring+="==< ";
			lstring+=vertices[nvertices-1].get_name();
		info.push_back(lstring);
		lstring=" |";
		if(findex!=0)
			lstring+="~"+std::to_string(findex);
		info.push_back(lstring);
		lstring="-^- "+std::to_string(match_bottom)+": "+endlink.get_info();
		info.push_back(lstring);
	}

	// now normalize info
	normalize_string_vector(info);
}

void RungF::show(){
	// show() always update info vector
	this->update_info();

	// now print
	// first attached top vertex
	if(vertex_end[0].get_multiplicity()>0){
		std::cout<<"Attached to "<<match_top<<" "<<vertex_end[0].get_name()<<std::endl;
	}
	// now rung
	for(size_t ii=0;ii<info.size();ii++)
		std::cout<<info[ii]<<std::endl;
	// finally attached bottom vertex
	if(vertex_end[1].get_multiplicity()>0){
		std::cout<<"Attached to "<<match_bottom<<" "<<vertex_end[1].get_name()<<std::endl;
	}
}

std::vector<std::string> RungF::get_info(){
	return info;
}

std::string RungF::get_rung_name(){
	std::string toreturn("[");
	for(size_t kk=0;kk+1<vertices.size();kk++){
		toreturn+=vertices[kk].get_name()+",";
	}
	if(vertices.size()>0){
		toreturn+=(vertices.back()).get_name();
	}
	toreturn+="]";
	return toreturn;
}

RungF RungF::pinch(size_t drop,Vertex vtop,Vertex vbottom){
	size_t nvertices(vertices.size());
	if(drop>nvertices){
		std::cout<<"pinch(k) called with k="<<drop<<" but rung has nvertices="<<nvertices<<" --- return empty rung"<<std::endl;
		return RungF();
	}
	// case of empty rung
	else if(endlink.get_type()==link_null){
		std::cout<<"pinch(k) called for an empty rung, no pinch possible --- return empty rung"<<std::endl;
		return RungF();
	}
	// Dropping link to top end
	else if(drop==0){
		// dropping single link (notice that endlink can not be a link_null)
		if(nvertices==0){
			return RungF(match_top,std::vector<std::tuple<Link,Vertex> >(),Link(),match_bottom,vtop,vbottom);
		}
		else{
			DEBUG_MESSAGE("CAREFUL for rungF pinches to top prop!");
			Vertex tVertex(vertices[0]*vtop);
			std::vector<std::tuple<Link,Vertex> > tobuild;
			for(size_t ii=1;ii<nvertices;ii++)
				tobuild.push_back(std::make_tuple(links[ii],vertices[ii]));
			return RungF(match_top,tobuild,endlink,match_bottom,tVertex,vbottom);
		}
	}
	// Dropping link to bottom end
	else if(drop==nvertices){
		DEBUG_MESSAGE("CAREFUL for rungF pinches to bottom prop!");
		Vertex bVertex(vertices.back()*vbottom);
		std::vector<std::tuple<Link,Vertex> > tobuild;
		for(size_t ii=0;ii+1<nvertices;ii++)
			tobuild.push_back(std::make_tuple(links[ii],vertices[ii]));
		return RungF(match_top,tobuild,links.back(),match_bottom,vtop,bVertex);
	}
	// general case
	else{
		int index_over_oldlink(links[drop-1].get_down_internal_index());
		int index_below_oldlink(0);
		if(drop+1==nvertices)
			index_below_oldlink=endlink.get_up_internal_index();
		else
			index_below_oldlink=links[drop+1].get_up_internal_index();
		int oldlink_below(links[drop].get_down_internal_index());
		int oldlink_above(links[drop].get_up_internal_index());
		int new_over_pinchedlink(0);
		int new_over_pinchedvertex(0);
		int new_below_pinchedlink(0);
		int new_below_pinchedvertex(0);
		int new_over_pinchedgluon(0);
		int new_below_pinchedgluon(0);
		if(links[drop].is_fermion_link()||links[drop].is_antifermion_link()){
			// is there a break in fermion index on top of the dropped link?
			bool BreakTop(index_over_oldlink!=oldlink_above);
			// is there a break in fermion index below the dropped link?
			bool BreakBottom(index_below_oldlink!=oldlink_below);
			if(BreakTop&&BreakBottom){
				new_over_pinchedlink=index_over_oldlink;
				new_below_pinchedlink=index_below_oldlink;
			}
			else if(BreakTop){
				new_over_pinchedlink=index_over_oldlink;
				new_below_pinchedlink=oldlink_above;
				new_below_pinchedvertex=oldlink_above;
			}
			else if(BreakBottom){
				new_over_pinchedlink=oldlink_below;
				new_over_pinchedvertex=oldlink_below;
				new_below_pinchedlink=index_below_oldlink;
			}
			else{
				new_over_pinchedlink=index_over_oldlink;
				new_below_pinchedlink=index_below_oldlink;
			}
		}
		// dropping a non-fermion link
		else{
			new_over_pinchedlink=index_over_oldlink;
			new_below_pinchedlink=index_below_oldlink;
                        // don't assign fermion indices when flavors around pich are different
                        bool equalflvs(true);
                        if(drop+1==nvertices)
                            equalflvs = (links[drop-1].get_particle().get_flavor() == endlink.get_particle().get_flavor());
                        else
                            equalflvs = (links[drop-1].get_particle().get_flavor() == links[drop+1].get_particle().get_flavor());
			if((equalflvs)&&(links[drop-1].get_fermion_line_index().assigned&&(drop+1==nvertices?endlink:links[drop+1]).get_fermion_line_index().assigned))
				if((links[drop-1].is_fermion_link()||links[drop-1].is_antifermion_link())&&((drop+1==nvertices?endlink:links[drop+1]).is_fermion_link()||(drop+1==nvertices?endlink:links[drop+1]).is_antifermion_link())&&links[drop-1].get_fermion_line_index().value==(drop+1==nvertices?endlink:links[drop+1]).get_fermion_line_index().value) {
					new_over_pinchedlink++;
					new_over_pinchedgluon++;
					new_below_pinchedlink+=2;
					new_below_pinchedgluon+=2;
				}
		}
		std::vector<std::tuple<Link,Vertex> > tobuild;
		for(size_t ii=0;ii+1<drop;ii++)
			tobuild.push_back(std::make_tuple(links[ii],vertices[ii]));
		// the pinched vertex
		Vertex fVertex(vertices[drop-1]*vertices[drop]);
		if(new_over_pinchedgluon) {
			for(size_t i=0;i<vertices[drop-1].get_colored().size();++i)
				if(vertices[drop-1].get_colored()[i]->get_anti_type()==links[drop-1].get_particle_type())
					fVertex.set_internal_index(vertices[drop-1].get_colored()[i],new_over_pinchedgluon);
			for(size_t i=0;i<vertices[drop].get_colored().size();++i)
			  if(vertices[drop].get_colored()[i]->get_type()==links[drop-1].get_particle_type())
			    fVertex.set_internal_index(vertices[drop].get_colored()[i],new_below_pinchedgluon);

		}
		if(new_over_pinchedvertex) {
			for(size_t i=0;i<vertices[drop-1].get_colored().size();++i)
				if(vertices[drop-1].get_colored()[i]->get_type()==links[drop].get_particle_type())
					fVertex.set_internal_index(vertices[drop-1].get_colored()[i],new_over_pinchedvertex);
		}
		if(new_below_pinchedvertex) {
			for(int i=vertices[drop].get_colored().size()-1;i>=0;--i)
				if(vertices[drop].get_colored()[size_t(i)]->get_type()==links[drop].get_particle_type())
					fVertex.set_internal_index(vertices[drop].get_colored()[size_t(i)],new_below_pinchedvertex);
		}
		tobuild.push_back(std::make_tuple(links[drop-1],fVertex));
		std::get<0>(tobuild.back()).set_down_internal_index(new_over_pinchedlink);
		bool bottomfixed(false);
		for(size_t ii=drop+1;ii<nvertices;ii++){
			tobuild.push_back(std::make_tuple(links[ii],vertices[ii]));
			if(ii==drop+1){
				std::get<0>(tobuild.back()).set_up_internal_index(new_below_pinchedlink);
				bottomfixed=true;
			}
		}
		Link theend(endlink);
		if(!bottomfixed)
			theend.set_up_internal_index(new_below_pinchedlink);
		return RungF(match_top,tobuild,theend,match_bottom,vtop,vbottom);
	}
}

RungF RungF::multi_pinch(const std::vector<size_t>& iv){
	RungF toret(*this);
	size_t count_pinches(0);
	std::vector<size_t> ivsorted(iv);
	// important that appearance is in order
	std::sort(ivsorted.begin(),ivsorted.end());
	for(size_t ii=0;ii<ivsorted.size();ii++){
		// in this case, when we pinch top link, take care of flipping top vertex
		if(ivsorted[ii]-count_pinches==0)
			toret=toret.pinch_top_reverse(toret.vertex_end[0],toret.vertex_end[1]);
		else
			toret=toret.pinch(ivsorted[ii]-count_pinches,toret.vertex_end[0],toret.vertex_end[1]);
		count_pinches++;
	}
	return toret;
}

RungF RungF::one_loop_pinch(size_t drop){
	size_t nvertices(vertices.size());
	// if drop is not an extreme, call general pinch
	if(drop>0&&drop<nvertices){
		return this->pinch(drop,Vertex(),Vertex());
	}
	// we assume that for 1-loop cutF's, no drop is ever performed on bottom (equivalent to drop zeroth prop)
	// also add all other wrong cases
	else if(drop>=nvertices||nvertices<=1){
		std::cout<<"RungF::one_loop_pinch() with "<<nvertices<<" propagators, received index: "<<drop<<" -- Returned empty RungF"<<std::endl;
                std::exit(1);
		return RungF();
	}
	// case of empty rung
	else if(endlink.get_type()==link_null){
		std::cout<<"pinch(k) called for an empty rung, no pinch possible --- return empty rung"<<std::endl;
                std::exit(1);
		return RungF();
	}
	// zeroth drop
	else{
		// nvertices is >= 2! So we will not square the 0th Vertex
		Vertex fVertex(vertices.back()*vertices[0]);
		std::vector<std::tuple<Link,Vertex> > tobuild;
		Link lTop(links.back());
		// single entry, as it is a link_start
		std::vector<int> lvs {0};
		int index_over_oldlink(links.back().get_down_internal_index());
		int index_below_oldlink(links[1].get_up_internal_index());
		int oldlink_below(links[0].get_down_internal_index());
		int oldlink_above(endlink.get_up_internal_index());
		int new_over_pinchedlink(0);
		int new_over_pinchedvertex(0);
		int new_below_pinchedlink(0);
		int new_below_pinchedvertex(0);
		int new_over_pinchedgluon(0);
		int new_below_pinchedgluon(0);
		if(links[0].is_fermion_link()||links[0].is_antifermion_link()){
			// is there a break in fermion index on top of the dropped link?
			bool BreakTop(index_over_oldlink!=oldlink_above);
			// is there a break in fermion index below the dropped link?
			bool BreakBottom(index_below_oldlink!=oldlink_below);
			if(BreakTop&&BreakBottom){
				new_over_pinchedlink=index_over_oldlink;
				new_below_pinchedlink=index_below_oldlink;
			}
			else if(BreakTop){
				new_over_pinchedlink=index_over_oldlink;
				new_below_pinchedlink=oldlink_above;
				new_below_pinchedvertex=oldlink_above;
			}
			else if(BreakBottom){
				new_over_pinchedlink=oldlink_below;
				new_over_pinchedvertex=oldlink_below;
				new_below_pinchedlink=index_below_oldlink;
			}
			else{
				new_over_pinchedlink=index_over_oldlink;
				new_below_pinchedlink=index_below_oldlink;
			}
		}
		// dropping a non-fermion link
		else{
			new_over_pinchedlink=index_over_oldlink;
			new_below_pinchedlink=index_below_oldlink;
                        // don't assign fermion indices when flavors around pinch are different
                        bool equalflvs = (links.back().get_particle().get_flavor() == links[1].get_particle().get_flavor());
			if((equalflvs)&&(links.back().get_fermion_line_index().assigned&&links[1].get_fermion_line_index().assigned))
				if((links.back().is_fermion_link()||links.back().is_antifermion_link())&&(links[1].is_fermion_link()||links[1].is_antifermion_link())&&links.back().get_fermion_line_index().value==links[1].get_fermion_line_index().value) {
					new_over_pinchedlink++;
					new_over_pinchedgluon++;
					new_below_pinchedlink+=2;
					new_below_pinchedgluon+=2;
				}
		}
		int new_linkstart(new_over_pinchedlink);
		if(new_over_pinchedgluon) {
			for(size_t i=0;i<vertices.back().get_colored().size();++i)
				if(vertices.back().get_colored()[i]->get_anti_type()==links.back().get_particle_type())
					fVertex.set_internal_index(vertices.back().get_colored()[i],new_over_pinchedgluon);
			for(size_t i=0;i<vertices[0].get_colored().size();++i)
				if(vertices[0].get_colored()[i]->get_type()==links.back().get_particle_type())
				  fVertex.set_internal_index(vertices[0].get_colored()[i],new_below_pinchedgluon);
		}
		if(new_over_pinchedvertex) {
			for(size_t i=0;i<vertices.back().get_colored().size();++i)
				if(vertices.back().get_colored()[i]->get_type()==endlink.get_particle_type())
					fVertex.set_internal_index(vertices.back().get_colored()[i],new_over_pinchedvertex);
		}
		if(new_below_pinchedvertex) {
			for(int i=vertices[0].get_colored().size()-1;i>=0;--i)
				if(vertices[0].get_colored()[size_t(i)]->get_type()==links[0].get_particle_type())
					fVertex.set_internal_index(vertices[0].get_colored()[size_t(i)],new_below_pinchedvertex);
		}
		int new_linkend(links.back().get_up_internal_index());
		lvs[0]=new_linkstart;
		lTop.set_link_type(link_start,lvs);
		tobuild.push_back(std::make_tuple(lTop,fVertex));
		for(size_t ii=1;ii+1<nvertices;ii++){
			tobuild.push_back(std::make_tuple(links[ii],vertices[ii]));
			if(ii==1)
				std::get<0>(tobuild.back()).set_up_internal_index(new_below_pinchedlink);
		}
		// single entry, as it is a link_end
		std::vector<int> lve {0};
		lve[0]=new_linkend;
		Link lend(links.back());
		lend.set_link_type(link_end,lve);
		return RungF(match_top,tobuild,lend,match_bottom);
	}
}

RungF RungF::pinch_top_reverse(Vertex vtop,Vertex vbottom){
	size_t nvertices(vertices.size());
	if(nvertices==0){
		return this->pinch(0,vtop,vbottom);
	}
	// case of empty rung
	else if(endlink.get_type()==link_null){
		std::cout<<"pinch_top_reverse(k) called for an empty rung, no pinch possible --- return empty rung"<<std::endl;
		return RungF();
	}
	// Dropping link to top end with flip
	else{
		Vertex tVertex(vertices[0].flip()*vtop);
		std::vector<std::tuple<Link,Vertex> > tobuild;
		for(size_t ii=1;ii<nvertices;ii++)
			tobuild.push_back(std::make_tuple(links[ii],vertices[ii]));
		return RungF(match_top,tobuild,endlink,match_bottom,tVertex,vbottom);
	}
}

RungF RungF::pinch_bottom_reverse(Vertex vtop,Vertex vbottom){
	size_t nvertices(vertices.size());
	if(nvertices==0){
		return this->pinch(0,vtop,vbottom);
	}
	// case of empty rung
	else if(endlink.get_type()==link_null){
		std::cout<<"pinch_top_reverse(k) called for an empty rung, no pinch possible --- return empty rung"<<std::endl;
		return RungF();
	}
	// Dropping link to bottom end (with flip)
	else{
		Vertex bVertex(vbottom*vertices.back().flip());
		std::vector<std::tuple<Link,Vertex> > tobuild;
		for(size_t ii=0;ii+1<nvertices;ii++)
			tobuild.push_back(std::make_tuple(links[ii],vertices[ii]));
		return RungF(match_top,tobuild,links.back(),match_bottom,vtop,bVertex);
	}
}

RungF RungF::pinch_top_of_right_rung(Vertex vtop,Vertex vbottom){
	size_t nvertices(vertices.size());
	if(nvertices==0){
		return this->pinch(0,vtop,vbottom);
	}
	// case of empty rung
	else if(endlink.get_type()==link_null){
		std::cout<<"pinch_top_reverse(k) called for an empty rung, no pinch possible --- return empty rung"<<std::endl;
		return RungF();
	}
	// Dropping link to bottom end (with flip)
	else{
		Vertex tVertex(vtop*vertices[0]);
		std::vector<std::tuple<Link,Vertex> > tobuild;
		for(size_t ii=1;ii<nvertices;ii++)
			tobuild.push_back(std::make_tuple(links[ii],vertices[ii]));
		return RungF(match_top,tobuild,endlink,match_bottom,tVertex,vbottom);
	}
}


const std::vector<Vertex>& RungF::get_vertices() const{
	return vertices;
}

std::vector<Vertex>& RungF::get_vertices(){
	return vertices;
}

const Vertex& RungF::get_vertex_top(){
	return vertex_end[0];
}

const Vertex& RungF::get_vertex_bottom(){
	return vertex_end[1];
}

bool RungF::is_null(){
	return endlink.is_null();
}

// to get access to particles
const std::vector<Particle*>& RungF::get_particles() const {
	return particles;
}

// function to allow mapping of a new particle set into a given RungF, with EMPTY top/bottom vertices
// except if explicitly passed
// NO CHANGE performed to links
RungF RungF::map_particles(const std::vector<Particle*>& iv,const Vertex& ivt,const Vertex& ivb) const {
	std::cout << "Careful: RungF::map_particles() only works for gluons" << std::endl;
	std::vector<Particle*> temp;
	size_t max_size(iv.size());
	size_t current_particle(0);

	std::vector<std::tuple<Link,Vertex> > toconstruct;
	// scan vertices
	for(size_t ii=0;ii<vertices.size();ii++){
		temp.clear();
		for(size_t jj=0;jj<vertices[ii].get_multiplicity();jj++){
			if(current_particle>=max_size){
				std::cout<<"Error not enough particles passed to RungF::map_particles() -- Returned empty rung"<<std::endl;
				return RungF();
			}
			temp.push_back(iv[current_particle]);
			current_particle++;
		}
		Vertex lvertex(temp, coupling_powers_per_vertex[ii]);
		toconstruct.push_back(std::make_tuple(links[ii],lvertex));
	}
	return RungF(match_top,toconstruct,endlink,match_bottom,ivt,ivb);
}

void handle_rotated_link(Link& llink,int nvertices,int& position,const std::vector<int>& fermion_indices){
	link_type toassign;
	std::vector<int> lv;
	if(position==0){
		toassign=link_start;
		lv.push_back(fermion_indices[position]);
		position++;
	}
	else if(position+1==2*nvertices){
		toassign=link_end;
		lv.push_back(fermion_indices[position]);
		position++;
	}
	else{
		toassign=link_internal;
		lv.push_back(fermion_indices[position]);
		position++;
		lv.push_back(fermion_indices[position]);
		position++;
	}
	llink.set_link_type(toassign,lv);
}

// a 1-loop rotation to produce a new rung
// ASSUMES empty vertices
RungF RungF::rotate_rung(size_t step) const {
	// in case step==0, returns a copy of this
	if(step==0){
		std::cout<<"Careful, asked for no rotation in RungF::rotate_rung() -- posible inf loop"<<std::endl;
		return *this;
	}
	if(step>=vertices.size()){
		std::cout<<"RungF::rotate_rung() received out of bounds size_t "<<step<<"/"<<vertices.size()<<" -- Returned RungF()"<<std::endl;
		return RungF();
	}
	
	// here we collect all fermion indices before rotating
	std::vector<int> fermion_indices;
	fermion_indices.push_back(links[0].get_down_internal_index());
	for(size_t ii=1;ii<vertices.size();ii++){
		fermion_indices.push_back(links[ii].get_up_internal_index());
		fermion_indices.push_back(links[ii].get_down_internal_index());
	}
	fermion_indices.push_back(endlink.get_up_internal_index());
	std::rotate(fermion_indices.rbegin(),fermion_indices.rbegin()+2*step,fermion_indices.rend());
	int position(0);

	std::vector<std::tuple<Link,Vertex> > toconst;
	for(size_t ii=vertices.size()-step;ii<vertices.size();ii++){
		toconst.push_back(std::make_tuple(links[ii],vertices[ii]));
		handle_rotated_link(std::get<0>(toconst.back()),vertices.size(),position,fermion_indices);
	}
	for(size_t ii=0;ii<vertices.size()-step;ii++){
		toconst.push_back(std::make_tuple(links[ii],vertices[ii]));
		handle_rotated_link(std::get<0>(toconst.back()),vertices.size(),position,fermion_indices);
	}
	Link llink(links[vertices.size()-step]);
	handle_rotated_link(llink,vertices.size(),position,fermion_indices);
	// returns rung without top/bottom vertices
	return RungF(1,toconst,llink,1);
}

RungF RungF::flip() const {
	if(vertices.size()==0){
		std::vector<std::tuple<Link,Vertex> > toconst;
		Link llink(endlink.flip());
		// returns rung without top/bottom vertices
		return RungF(match_bottom,toconst,llink,match_top,vertex_end[1],vertex_end[0]);
	}
	std::vector<std::tuple<Link,Vertex> > toconst;
	toconst.push_back(std::make_tuple(endlink.flip(),vertices.back().flip()));
	for(int ii=int(vertices.size())-2;ii>=0;ii--){
		toconst.push_back(std::make_tuple(links[ii+1].flip(),vertices[ii].flip()));
	}
	Link llink(links[0].flip());
	// returns rung without top/bottom vertices
	return RungF(match_bottom,toconst,llink,match_top,vertex_end[1],vertex_end[0]);
}

bool RungF::is_contained_inside(Particle* p) const {
	for(size_t ii=0;ii<vertices.size();ii++){
		if(vertices[ii].is_contained(p))
			return true;
	}
        if(vertex_end[0].is_contained(p)||vertex_end[1].is_contained(p))
	    return true;
	return false;
}

fermion_line_index RungF::get_fermion_line_index(Particle* p) const {
    if (!is_contained_inside(p)) {
        std::cerr << "ERROR: call to RungF::get_fermion_line_index(.) when particle isn't contained! (Notice, should fail in 2-loop cases!)" << std::endl;
        std::exit(1);
    }
    for (size_t ii = 0; ii < vertices.size(); ii++)
        if (vertices[ii].is_contained(p)) 
            return vertices[ii].get_fermion_line_index(p);
    for (size_t ii = 0; ii < 2; ii++)
        if (vertex_end[ii].is_contained(p)) 
            return vertex_end[ii].get_fermion_line_index(p);

    return {false, 0};
}

int RungF::get_internal_index(Particle* p) const {
    if (!is_contained_inside(p)) {
        std::cerr << "ERROR: call to RungF::get_fermion_line_index(.) when particle isn't contained! (Notice, should fail in 2-loop cases!)" << std::endl;
        std::exit(1);
    }
    for (size_t ii = 0; ii < vertices.size(); ii++)
        if (vertices[ii].is_contained(p))
            return vertices[ii].get_internal_index(p);
    for (size_t ii = 0; ii < 2; ii++)
        if (vertex_end[ii].is_contained(p))
            return vertex_end[ii].get_internal_index(p);

    return 0;
}

fermion_line_index RungF::get_link_fermion_line_index(size_t i) const {
    if (i>links.size()) {
        std::cerr << "ERROR: call to RungF::get_link_fermion_line_index("<<i<<") when link counting is: " <<links.size()<< std::endl;
        std::exit(1);
    }
    if(i==links.size())
        return endlink.get_fermion_line_index();
    else
        return links[i].get_fermion_line_index();
}

std::vector<Link> RungF::get_links() const {
	std::vector<Link> temp = links;
	temp.push_back(endlink);
	return temp;
}

const Link& RungF::get_link(size_t i) const {
    if(i>links.size()){
        std::cerr<<"ERROR: RungF::get_link("<<i<<") called when only there are "<<links.size()+1<<" links! Return 1"<<std::endl;
        std::exit(1);
    }
    else if(i==links.size())
        return endlink;
    return links[i];
}

couplings_t RungF::get_coupling_powers() const {
	return coupling_powers;
}

std::vector<couplings_t> RungF::get_coupling_powers_per_vertex() const {
	return coupling_powers_per_vertex;
}

couplings_t RungF::get_top_coupling_powers() const {
	return coup_top;
}

couplings_t RungF::get_bottom_coupling_powers() const {
	return coup_bot;
}

bool RungF::all_massless_internal_links() const {
	if(endlink.get_particle().is_massive())
		return false;
	for(size_t ii=0;ii<links.size();ii++)
		if(links[ii].get_particle().is_massive())
			return false;
	return true;
}


bool operator==(const RungF& r1,const RungF& r2){
	// make first simple/fast checks
	// if one loop or butterfly, check match
	if(r1.match_top==r1.match_bottom&&r2.match_top!=r2.match_bottom)
		return false;
	// same # of particles inside rungs
	if(r1.particles.size()!=r2.particles.size())
		return false;
	// same # of vertices
	if(r1.vertices.size()!=r2.vertices.size())
		return false;
	// check top/bottom vertices
	if(!(r1.vertex_end[0]==r2.vertex_end[0]))
		return false;
	if(!(r1.vertex_end[1]==r2.vertex_end[1]))
		return false;
	// check nature of all links
	for(size_t ii=0;ii<r1.vertices.size();ii++){
		if(!(r1.links[ii]==r2.links[ii]))
			return false;
	}
	// check all rest of vertices
	for(size_t ii=0;ii<r1.vertices.size();ii++){
		if(!(r1.vertices[ii]==r2.vertices[ii]))
			return false;
	}
	return true;
}

std::ostream& operator<<(std::ostream& s, RungF& r){
    using Caravel::operator<<;
    r.update_info();
    s << r.get_info();
    return s;
}

mom_routing_and_mass::mom_routing_and_mass(): multiplicity(0), mass(0) { loop[0]=0; loop[1]=0; }
mom_routing_and_mass::mom_routing_and_mass(int mult): multiplicity(mult), mass(0) { loop[0]=0; loop[1]=0; }
mom_routing_and_mass::mom_routing_and_mass(int mult,std::vector<int> iv): external(iv), multiplicity(mult), mass(0) { loop[0]=0; loop[1]=0; this->mom_conservation(); }
mom_routing_and_mass::mom_routing_and_mass(int mult,int loop1): multiplicity(mult), mass(0) { loop[0]=loop1; loop[1]=0; }
mom_routing_and_mass::mom_routing_and_mass(int mult,int loop1,int loop2): multiplicity(mult), mass(0) { loop[0]=loop1; loop[1]=loop2; }
mom_routing_and_mass::mom_routing_and_mass(int mult,int loop1,int loop2,std::vector<int> iv): external(iv), multiplicity(mult), mass(0) { loop[0]=loop1; loop[1]=loop2; this->mom_conservation(); }
// negative of instance
mom_routing_and_mass mom_routing_and_mass::operator-() {std::vector<int> ne(external); std::transform(ne.begin(),ne.end(),ne.begin(),std::negate<int>());
	return mom_routing_and_mass(multiplicity,-loop[0],-loop[1],ne);}
// add external momenta contained in vector of Particle*
mom_routing_and_mass mom_routing_and_mass::operator+=(std::vector<Particle*> vp) {for(size_t ii=0;ii<vp.size();ii++) external.push_back(vp[ii]->mom_index()); this->mom_conservation(); return *this; }
// subtract external momenta contained in vector of Particle*
mom_routing_and_mass mom_routing_and_mass::operator-=(std::vector<Particle*> vp) {for(size_t ii=0;ii<vp.size();ii++) external.push_back(-(vp[ii]->mom_index())); this->mom_conservation(); return *this; }
// add momenta
void mom_routing_and_mass::add_momenta(int l1,int l2) { loop[0]+=l1; loop[1]+=l2; }
// routing a RungF through its vertices
void mom_routing_and_mass::route_rung(RungF* rung) {	*this-=rung->get_particles(); }
void mom_routing_and_mass::route_rung(RungF* rung,size_t i,size_t j) { if(i<=j){ for(size_t kk=i;kk<j&&kk<rung->get_vertices().size();kk++) *this-=rung->get_vertices()[kk].get_particles(); }
	else { for(size_t kk=j;kk<i&&kk<rung->get_vertices().size();kk++) *this-=rung->get_vertices()[kk].get_particles(); }	}
void mom_routing_and_mass::set_mass(size_t i) { mass = i; }
size_t mom_routing_and_mass::get_mass() const { return mass; }
// check mom conservation
void mom_routing_and_mass::mom_conservation() {bool deletemom(true); for(int ii=1;ii<=multiplicity;ii++) if(std::find(external.begin(),external.end(),ii)==external.end()){ deletemom=false; break; }
	if(deletemom) {for(int ii=1;ii<=multiplicity;ii++){ auto it=std::find(external.begin(),external.end(),ii); if(it!=external.end()) external.erase(it); } }
	// do proper with negative values
	this->neg_mom_conservation();}

std::string printloop(int f,std::string s,int x) {
	if(f==0) return "";
	if(f==1) return "+l"+s+std::to_string(x);
	if(f==-1) return "-l"+s+std::to_string(x);
	if(f>0) return "+"+std::to_string(f)+"*l"+s+std::to_string(x);
	return std::to_string(f)+"*l"+s+std::to_string(x); }
std::string printmom(int f) { if(f==0) return "";
	else if(f>0) return "+"+std::to_string(f);
	else return std::to_string(f); }


std::string mom_routing_and_mass::get_info(std::string s) const {std::string toret(""); toret+=printloop(loop[0],s,1)+printloop(loop[1],s,2);
	for(size_t ii=0;ii<external.size();ii++) 
		toret+=printmom(external[ii]); 
	return toret; }
// check mom conservation
void mom_routing_and_mass::neg_mom_conservation() {
	bool deletemom(true); 
	for(int ii=-1;ii>=-multiplicity;ii--)
		if(std::find(external.begin(),external.end(),ii)==external.end()){ 
			deletemom=false; break; 
		}
	if(deletemom) {
		for(int ii=-1;ii>=-multiplicity;ii--){ 
			auto it=std::find(external.begin(),external.end(),ii); 
			if(it!=external.end()) external.erase(it);
		} 
	}
	// find out if we have a i-i = 0 situation
	for(int ii=1;ii<=multiplicity;ii++){
		if(std::find(external.begin(),external.end(),ii)!=external.end() && std::find(external.begin(),external.end(),-ii)!=external.end()){
			auto it=std::find(external.begin(),external.end(),ii);	if(it!=external.end()) external.erase(it);
			auto itn=std::find(external.begin(),external.end(),-ii);	if(itn!=external.end()) external.erase(itn);
		}
	}
}

// allows to substitute the given momentum into local one: mom_{local} + {external_{local}} = (mom_{passed}+{external_{passed}}) + external_{local}
mom_routing_and_mass mom_routing_and_mass::substitute(const mom_routing_and_mass& mom_passed) const {
	if(this->multiplicity!=mom_passed.multiplicity){
		std::cout<<"mom_routing_and_mass::substitute multiplicities should match!"<<std::endl;
		return mom_routing_and_mass();
	}
	if(this->loop[0]!=0&&this->loop[1]!=0){
		std::cout<<"mom_routing_and_mass::substitute works for a single momentum substitution!"<<std::endl;
		return mom_routing_and_mass();
	}
	int factor(0);
	if(this->loop[0]!=0){
		factor=this->loop[0];
	}
	else if(this->loop[1]!=0){
		factor=this->loop[1];
	}
	// gets common multiplicity, and loop[0] and loop[1] from mom_passed, with proper factor from *this
	// we pass along also this->external
	mom_routing_and_mass toret(this->multiplicity,factor*mom_passed.loop[0],factor*mom_passed.loop[1],this->external);

	// now, depending on factor, add/subtract or do nothing with mom_passed.external
	if(factor>0)
		toret.external.insert(toret.external.end(),mom_passed.external.begin(),mom_passed.external.end());
	else if(factor<0){
		for(size_t ii=0;ii<mom_passed.external.size();ii++)
			toret.external.push_back(-mom_passed.external[ii]);
	}

	// check mom conservation
	toret.mom_conservation();

//std::cout<<"substitute received: "<<this->get_info("L")<<" and "<<mom_passed.get_info("P")<<" and returned: "<<toret.get_info("P")<<std::endl;
	return toret;
}

// allows to substitute the given momentum into local one: mom_{local} + {external_{local}} = (mom_{passed}-{external_{passed}}) + external_{local}
mom_routing_and_mass mom_routing_and_mass::substitute_inverse(const mom_routing_and_mass& mom_passed) const{
	if(this->multiplicity!=mom_passed.multiplicity){
		std::cout<<"mom_routing_and_mass::substitute_inverse multiplicities should match!"<<std::endl;
		return mom_routing_and_mass();
	}
	if(this->loop[0]!=0&&this->loop[1]!=0){
		std::cout<<"mom_routing_and_mass::substitute_inverse works for a single momentum substitution!"<<std::endl;
		return mom_routing_and_mass();
	}
	int factor(0);
	if(this->loop[0]!=0){
		factor=this->loop[0];
	}
	else if(this->loop[1]!=0){
		factor=this->loop[1];
	}
	// gets common multiplicity, and loop[0] and loop[1] from mom_passed, with proper factor from *this
	// we pass along also this->external
	mom_routing_and_mass toret(this->multiplicity,factor*mom_passed.loop[0],factor*mom_passed.loop[1],this->external);
        toret.set_mass(this->get_mass());

	// now, depending on factor, add/subtract or do nothing with mom_passed.external
	if(factor<0)
		toret.external.insert(toret.external.end(),mom_passed.external.begin(),mom_passed.external.end());
	else if(factor>0){
		for(size_t ii=0;ii<mom_passed.external.size();ii++)
			toret.external.push_back(-mom_passed.external[ii]);
	}

	// check mom conservation
	toret.mom_conservation();

//std::cout<<"substitute_inverse received: "<<this->get_info("L")<<" and "<<mom_passed.get_info("P")<<" and returned: "<<toret.get_info("P")<<std::endl;
	return toret;
}

// to get rule for grand-parent prop at 2-loops
mom_routing_and_mass mom_routing_and_mass::solve_prop_2loops(const mom_routing_and_mass& lPa_from_D1,const mom_routing_and_mass& lPb_from_D1) const {
	if(this->multiplicity!=lPa_from_D1.multiplicity||this->multiplicity!=lPb_from_D1.multiplicity){
		std::cout<<"mom_routing_and_mass::solve_prop_2loops(.) multiplicities should match!"<<std::endl;
		return mom_routing_and_mass();
	}
	// by general conditions we assume one entry zero!
	// also, all other coefficients either -1, 0 or 1
	if(lPa_from_D1.loop[0]!=0&&lPa_from_D1.loop[1]!=0&&lPb_from_D1.loop[0]!=0&&lPb_from_D1.loop[1]!=0){
		std::cout<<"mom_routing_and_mass::solve_prop_2loops(.) should get AT LEAST one zero coeff!"<<std::endl;
		return mom_routing_and_mass();
	}

	mom_routing_and_mass lD1a_from_P(this->multiplicity);
	mom_routing_and_mass lD1b_from_P(this->multiplicity);

	if(lPa_from_D1.loop[0]==0){
		// now assign lD2a
		int c(lPb_from_D1.loop[0]);
		int bcd(lPa_from_D1.loop[1]*lPb_from_D1.loop[0]*lPb_from_D1.loop[1]);
		lD1a_from_P.loop[0]=-bcd;
		lD1a_from_P.loop[1]=c;
		// add external 2
		if(c>0){
			for(size_t ii=0;ii<lPb_from_D1.external.size();ii++)
				lD1a_from_P.external.push_back(-lPb_from_D1.external[ii]);
		}
		else if (c<0)
			lD1a_from_P.external=lPb_from_D1.external;
		// add external 1
		if(bcd>0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1a_from_P.external.push_back(lPa_from_D1.external[ii]);
		}
		else if (bcd<0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1a_from_P.external.push_back(-lPa_from_D1.external[ii]);
		}

		// now assign lD2b
		lD1b_from_P.loop[0]=lPa_from_D1.loop[1];
		if(lPa_from_D1.loop[1]>0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(-lPa_from_D1.external[ii]);
		}
		// it has to be -1 !
		else if(lPa_from_D1.loop[1]<0)
			lD1b_from_P.external=lPa_from_D1.external;
	}
	else if(lPa_from_D1.loop[1]==0){
		// now assign lD2b
		int d(lPb_from_D1.loop[1]);
		int acd(lPa_from_D1.loop[0]*lPb_from_D1.loop[0]*lPb_from_D1.loop[1]);
		lD1b_from_P.loop[0]=-acd;
		lD1b_from_P.loop[1]=d;
		// add external 2
		if(d>0){
			for(size_t ii=0;ii<lPb_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(-lPb_from_D1.external[ii]);
		}
		else if (d<0)
			lD1b_from_P.external=lPb_from_D1.external;
		// add external 1
		if(acd>0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(lPa_from_D1.external[ii]);
		}
		else if (acd<0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(-lPa_from_D1.external[ii]);
		}

		// now assign lD2a
		lD1a_from_P.loop[0]=lPa_from_D1.loop[0];
		if(lPa_from_D1.loop[0]>0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1a_from_P.external.push_back(-lPa_from_D1.external[ii]);
		}
		// it has to be -1 !
		else if(lPa_from_D1.loop[0]<0){
			lD1a_from_P.external=lPa_from_D1.external;
		}
	}
	else if(lPb_from_D1.loop[0]==0){
		// now assign lD2a
		int a(lPa_from_D1.loop[0]);
		int abd(lPa_from_D1.loop[0]*lPa_from_D1.loop[1]*lPb_from_D1.loop[1]);
		lD1a_from_P.loop[0]=-abd;
		lD1a_from_P.loop[1]=a;
		// add external 2
		if(abd<0){
			for(size_t ii=0;ii<lPb_from_D1.external.size();ii++)
				lD1a_from_P.external.push_back(-lPb_from_D1.external[ii]);
		}
		else if (abd>0)
			lD1a_from_P.external=lPb_from_D1.external;
		// add external 1
		if(a<0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1a_from_P.external.push_back(lPa_from_D1.external[ii]);
		}
		else if (a>0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1a_from_P.external.push_back(-lPa_from_D1.external[ii]);
		}

		// now assign lD2b
		lD1b_from_P.loop[0]=lPb_from_D1.loop[1];
		if(lPb_from_D1.loop[1]>0){
			for(size_t ii=0;ii<lPb_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(-lPb_from_D1.external[ii]);
		}
		// it has to be -1 !
		else if(lPb_from_D1.loop[1]<0)
			lD1b_from_P.external=lPb_from_D1.external;
	}
	else if(lPb_from_D1.loop[1]==0){
		// now assign lD2b
		int b(lPa_from_D1.loop[1]);
		int abc(lPa_from_D1.loop[0]*lPa_from_D1.loop[1]*lPb_from_D1.loop[0]);
		lD1b_from_P.loop[0]=b;
		lD1b_from_P.loop[1]=-abc;
		// add external 2
		if(abc>0){
			for(size_t ii=0;ii<lPb_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(lPb_from_D1.external[ii]);
		}
		else if (abc<0)
			for(size_t ii=0;ii<lPb_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(-lPb_from_D1.external[ii]);
		// add external 1
		if(b>0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(-lPa_from_D1.external[ii]);
		}
		else if (b<0){
			for(size_t ii=0;ii<lPa_from_D1.external.size();ii++)
				lD1b_from_P.external.push_back(lPa_from_D1.external[ii]);
		}

		// now assign lD2a
		lD1a_from_P.loop[0]=lPb_from_D1.loop[0];
		if(lPb_from_D1.loop[0]>0){
			for(size_t ii=0;ii<lPb_from_D1.external.size();ii++)
				lD1a_from_P.external.push_back(-lPb_from_D1.external[ii]);
		}
		// it has to be -1 !
		else if(lPb_from_D1.loop[0]<0){
			lD1a_from_P.external=lPb_from_D1.external;
		}
	}
	else{
		std::cout<<"Error in mom_routing_and_mass::solve_prop_2loops(.)"<<std::endl;
		return mom_routing_and_mass();
	}
	lD1a_from_P.mom_conservation();
	lD1b_from_P.mom_conservation();
#if 0
std::cout<<"Inversion of lP1 = "<<lPa_from_D1.get_info("D1")<<" ; lP2 = "<<lPb_from_D1.get_info("D1")<<std::endl;
std::cout<<"gives: lD11 = "<<lD1a_from_P.get_info("P")<<" ; lD12 = "<<lD1b_from_P.get_info("P")<<std::endl;
#endif

	mom_routing_and_mass toret(this->multiplicity,this->loop[0],this->loop[1],this->external);
        toret.set_mass(this->get_mass());

	toret=toret.substitute(lD1a_from_P,lD1b_from_P);

	toret.mom_conservation();
#if 0
std::cout<<"From rule: prop = "<<this->get_info("D1")<<" we get: "<<toret.get_info("P")<<std::endl;
#endif
	return toret;
}


// 2-loop substitutions to get lP=lP(lD1a,lD1b) = lP(lD2a,lD2b)
mom_routing_and_mass mom_routing_and_mass::substitute(const mom_routing_and_mass& lD1a_from_D2,const mom_routing_and_mass& lD1b_from_D2) const {
	if(this->multiplicity!=lD1a_from_D2.multiplicity||this->multiplicity!=lD1b_from_D2.multiplicity){
		std::cout<<"mom_routing_and_mass::substitute(.,.) multiplicities should match!"<<std::endl;
		return mom_routing_and_mass();
	}

	mom_routing_and_mass toret(this->multiplicity,this->external);
	// deal with lD1a
	if(this->loop[0]>0)
		toret=toret+lD1a_from_D2;
	else if(this->loop[0]<0)
		toret=toret-lD1a_from_D2;
	// deal with lD1b
	if(this->loop[1]>0)
		toret=toret+lD1b_from_D2;
	else if(this->loop[1]<0)
		toret=toret-lD1b_from_D2;

	toret.mom_conservation();
#if 0
std::cout<<"substitute(lD1a,lD1b) got: lP = "<<this->get_info("D1")<<" and: lD11= "<<lD1a_from_D2.get_info("D2")<<" and: lD12= "<<lD1b_from_D2.get_info("D2")<<std::endl;
std::cout<<"AND returned: lP = "<<toret.get_info("D2")<<std::endl;
#endif
	return toret;
}

mom_routing_and_mass operator+(const mom_routing_and_mass& m1,const mom_routing_and_mass& m2) {
	if(m1.multiplicity!=m2.multiplicity){ std::cout<<"Diff size mom_rout!\n"; return mom_routing_and_mass();}
	mom_routing_and_mass toret(m1.multiplicity);
	toret.loop[0]=m1.loop[0]+m2.loop[0];
	toret.loop[1]=m1.loop[1]+m2.loop[1];
	toret.external=m1.external;
	toret.external.insert(toret.external.end(),m2.external.begin(),m2.external.end());
	toret.mom_conservation();
	return toret;
}

mom_routing_and_mass operator-(const mom_routing_and_mass& m1,const mom_routing_and_mass& m2) {
	if(m1.multiplicity!=m2.multiplicity){ std::cout<<"Diff size mom_rout!\n"; return mom_routing_and_mass();}
	mom_routing_and_mass toret(m1.multiplicity);
	toret.loop[0]=m1.loop[0]-m2.loop[0];
	toret.loop[1]=m1.loop[1]-m2.loop[1];
	toret.external=m1.external;
	for(size_t ii=0;ii<m2.external.size();ii++)
		toret.external.push_back(-m2.external[ii]);
	toret.mom_conservation();
	return toret;
}

bool check_overlap(const std::vector<Particle*>& parent_particles,const std::vector<Particle*>& daughter_particles){
	// The rule is that if a single particle in parent_particles is in daughter_particles, then we return true
	if(parent_particles.size()==0)
		return true;
	// we can assume there is a particle in parent_particles
	Particle* pp(parent_particles[0]);
	if(std::find(daughter_particles.begin(),daughter_particles.end(),pp)==daughter_particles.end())
		return false;
	else
		return true;
}

std::pair<size_t,int> get_propagator_location_of_lD_in_parent(const std::pair<std::vector<Particle*>,std::vector<Particle*>>& prop,size_t rung_daughter,size_t corresponding_rung_parent,cutF* parent){
	std::pair<size_t,int> toret;
	RungF* lrung((parent->cut_rungs)+corresponding_rung_parent);
	std::vector<Particle*> starting;
	if(corresponding_rung_parent==0)
		starting=(parent->vertex_end[0]).get_particles();
	else
		starting=(parent->vertex_end[1]).get_particles();
	int theprop(0);
	bool assigned(false);
	// pair: <corresponding vertex, propagator counting>
	std::vector<std::pair<int,int>> indices;
	if(corresponding_rung_parent==0)
		for(int ii=0;ii<int(lrung->get_multiplicity());ii++)
			indices.push_back(std::make_pair(ii,ii+1));
	else
		for(int ii=int(lrung->get_multiplicity());ii>0;ii--)
			indices.push_back(std::make_pair(ii-1,ii+1));
	
	for(auto& ii_pair:indices){
		std::vector<Particle*> end(lrung->get_vertices()[ii_pair.first].get_particles());
		
		if(check_overlap(starting,prop.first)&&check_overlap(end,prop.second)){
			theprop=ii_pair.second;
			assigned=true;
			break;
		}
/*
No need: Assumed always same color ordering!
		else if(check_overlap(starting,prop.second)&&check_overlap(end,prop.first)){
			theprop=-(ii_pair.second);
			assigned=true;
			break;
		}
*/
		// no assignment, redefine starting
		starting=end;
	}
	// no assignment produced, check final propagator
	if(!assigned){
		std::vector<Particle*> end;
		if(corresponding_rung_parent==0)
			end=(parent->vertex_end[1]).get_particles();
		else
			end=(parent->vertex_end[0]).get_particles();
		if(check_overlap(starting,prop.first)&&check_overlap(end,prop.second)){
			// bottom
			if(corresponding_rung_parent==0){
				theprop=int(lrung->get_multiplicity())+1;
				assigned=true;
			}
			// top
			else{
				theprop=1;
				assigned=true;
			}
		}
/*
No need: Assumed always same color ordering!
		else if(check_overlap(starting,prop.second)&&check_overlap(end,prop.first)){
			// bottom
			if(corresponding_rung_parent==0){
				theprop=-(int(lrung->get_multiplicity())+1);
				assigned=true;
			}
			// top
			else{
				theprop=-1;
				assigned=true;
			}
		}
*/
	}

	// assign info
	toret.first=rung_daughter;
	toret.second=theprop;
	if(!assigned)
		std::cout<<"ERROR: get_propagator_location_in_parent(.) did not find corresponding propagator from parent cutF! --- returned <"<<corresponding_rung_parent<<","<<theprop<<">!"<<std::endl;
	return toret;
}

// general map, working with non-planar diagrams (untested though!)
void map_loop_mom_3rungs(std::pair<size_t,int>* loop_map,cutF* parent,cutF* daughter,size_t rung_pinch,size_t pinch,cutF* direct_pinch_daughter){
	// Top-bottom flips will always come with left-right flips to avoid reordering of color
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool daughter_is_bow_tie(false);
	if(daughter->rung_count==2)
		daughter_is_bow_tie=true;
	std::vector<size_t> map_to_daughter_rungs({3,3,3});
	// find to which parent rung it corresponds the first rung in daughter
	if((daughter->cut_rungs->get_particles()).size()==0){
		// all rungs have no particles, then we make full assignment

		// see if top/bottom vertices are aligned or not
		const std::vector<Particle*>& parent_top(parent->vertex_end[0].get_particles());
		const std::vector<Particle*>& parent_bottom(parent->vertex_end[1].get_particles());
		const std::vector<Particle*>& daughter_top(daughter->vertex_end[0].get_particles());
		const std::vector<Particle*>& daughter_bottom(daughter->vertex_end[1].get_particles());
		// in this case, perfect alignment (0,0 & 1,1 & 2,2)
		if(check_overlap(parent_top,daughter_top)&&check_overlap(parent_bottom,daughter_bottom)){
			map_to_daughter_rungs[0]=0;
			if(daughter_is_bow_tie){
				map_to_daughter_rungs[1]=1;
				// there is no third rung
				map_to_daughter_rungs[2]=3;
			}
			else{
				map_to_daughter_rungs[1]=1;
				map_to_daughter_rungs[2]=2;
			}
		}
		// in this case, perfect anti-alignment (0,2 & 1,1 & 2,0)
		else{
			map_to_daughter_rungs[0]=2;
			if(daughter_is_bow_tie){
				map_to_daughter_rungs[1]=0;
				// there is no third rung
				map_to_daughter_rungs[2]=3;
			}
			else{
				map_to_daughter_rungs[1]=1;
				map_to_daughter_rungs[2]=0;
			}
		}
	}
	else{
		// daughter must have particles in first rung
		Particle* toppart((daughter->cut_rungs->get_particles())[0]);
		size_t rung_in_parent(0);
		for(size_t ii=1;ii<3;ii++){
			// see if iith rung of parent contains both particles
			if((parent->cut_rungs+ii)->is_contained_inside(toppart)){
				rung_in_parent=ii;
				break;
			}
		}
		map_to_daughter_rungs[0]=rung_in_parent;

		// find now to which parent rung it corresponds the right rung in daughter

		// bow tie daughter
		if(daughter_is_bow_tie){
			// as pinch of 3-rung parent produces bow tie, assignment would only go to 0 or 2
			size_t assign(0);
			if(map_to_daughter_rungs[0]==0)
				assign=2;
			map_to_daughter_rungs[1]=assign;
			map_to_daughter_rungs[2]=3;
		}
		// no particles in daughter's right rung, which implies no particles in parent's (and daughter's) central rung
		else if(((daughter->cut_rungs+2)->get_particles()).size()==0){
			map_to_daughter_rungs[1]=1;
			size_t assign(0);
			if(map_to_daughter_rungs[0]==0)
				assign=2;
			map_to_daughter_rungs[2]=assign;
		}
		// simplest case
		else{
			// daughter must have particles in right rung
			Particle* lpart(((daughter->cut_rungs+2)->get_particles())[0]);
			size_t lrung_in_parent(0);
			for(size_t ii=1;ii<3;ii++){
				// see if iith rung of parent contains both particles
				if((parent->cut_rungs+ii)->is_contained_inside(lpart)){
					lrung_in_parent=ii;
					break;
				}
			}
			map_to_daughter_rungs[2]=lrung_in_parent;

			// set central rung by discart
			for(size_t ii=0;ii<3;ii++)
				if(map_to_daughter_rungs[0]!=ii&&map_to_daughter_rungs[2]!=ii){
					map_to_daughter_rungs[1]=ii;
					break;
				}
		}
	}

#if 0
std::cout<<"With parent: "<<parent->get_short_name()<<" and daughter: "<<daughter->get_short_name()<<std::endl;
std::cout<<"we get a map from daughter to parent's rungs:"<<std::endl;
for(size_t ii=0;ii<3;ii++) std::cout<<"d_"<<ii<<" corresponds to p_"<<map_to_daughter_rungs[ii]<<std::endl;
#endif

	// classifying the lD1 propagator ( pair: < Particles in top vertex , Particles in first rung vertex (or bottom vertex) >)
	std::pair<std::vector<Particle*>,std::vector<Particle*>> prop_lD1;
	prop_lD1.first=daughter->vertex_end[0].get_particles();
	if((daughter->cut_rungs)->get_multiplicity()>0){
		prop_lD1.second=(daughter->cut_rungs)->get_vertices()[0].get_particles();
	}
	else{
		prop_lD1.second=daughter->vertex_end[1].get_particles();
	}
	// we are assigning lP1
	if(map_to_daughter_rungs[0]==0)
		loop_map[0]=get_propagator_location_of_lD_in_parent(prop_lD1,0,map_to_daughter_rungs[0],parent);
	// we are assigning lP2
	else
		loop_map[1]=get_propagator_location_of_lD_in_parent(prop_lD1,0,map_to_daughter_rungs[0],parent);

	// classifying the lD2 propagator ( pair: < Particles in bottom vertex , Particles in last rung vertex (or top vertex) >)
	std::pair<std::vector<Particle*>,std::vector<Particle*>> prop_lD2;
	prop_lD2.first=daughter->vertex_end[1].get_particles();
	size_t right_rung(2);
	if(daughter_is_bow_tie){
		right_rung=1;
	}
	if(((daughter->cut_rungs)+right_rung)->get_multiplicity()>0){
		prop_lD2.second=(((daughter->cut_rungs)+right_rung)->get_vertices()).back().get_particles();
	}
	else{
		prop_lD2.second=daughter->vertex_end[0].get_particles();
	}
	// we are assigning lP1
	if(map_to_daughter_rungs[right_rung]==0)
		loop_map[0]=get_propagator_location_of_lD_in_parent(prop_lD2,right_rung,map_to_daughter_rungs[right_rung],parent);
	// we are assigning lP2
	else
		loop_map[1]=get_propagator_location_of_lD_in_parent(prop_lD2,right_rung,map_to_daughter_rungs[right_rung],parent);
}

std::pair<std::vector<int>,std::vector<int> > daughter_to_parent_loop_rule(cutF* parent,int loopcase,std::pair<size_t,int> loop_daughter,size_t picked_lD){
	std::vector<int> loop_track;
	std::vector<int> external_track;
	// we want lP1 -- assume that loop_daughter.first==0
	if(loopcase==1){
		// prepare loop_track

		// lD1 -> lP1
		if(picked_lD==0){
			// add first mom
			if(loop_daughter.second>0)
				loop_track.push_back(-1);
			else
				loop_track.push_back(1);
			// add second mom (does not contribute)
			loop_track.push_back(0);
			// pass info of direction (we reach lP1 'backwards')
			loop_track.push_back(-1);
		}
		// lD2 -> lP1
		else{
			// add first mom (does not contribute)
			loop_track.push_back(0);
			// add second mom
			if(loop_daughter.second>0)
				loop_track.push_back(-1);
			else
				loop_track.push_back(1);
			// pass info of direction (we reach lP1 'backwards')
			loop_track.push_back(-1);
		}

		// now prepare external_track

		// we go from vertex abs(loop_daughter.second)-2 till the zeroth
		for(size_t ii=abs(loop_daughter.second);ii>1;ii--){
			const std::vector<Particle*>& lparts((parent->cut_rungs->get_vertices())[ii-2].get_particles());
			for(size_t jj=0;jj<lparts.size();jj++){
				external_track.push_back(lparts[jj]->mom_index());
			}
		}
	}
	// we want lP2 -- assume that loop_daughter.first==2
	else if(loopcase==2){
		// prepare loop_track

		// lD1 -> lP2
		if(picked_lD==0){
			// add first mom
			if(loop_daughter.second>0)
				loop_track.push_back(-1);
			else
				loop_track.push_back(1);
			// add second mom (does not contribute)
			loop_track.push_back(0);
			// pass info of direction (we reach lP2 'backwards')
			loop_track.push_back(-1);
		}
		// lD2 -> lP2
		else{
			// add first mom (does not contribute)
			loop_track.push_back(0);
			// add second mom
			if(loop_daughter.second>0)
				loop_track.push_back(-1);
			else
				loop_track.push_back(1);
			// pass info of direction (we reach lP2 'backwards')
			loop_track.push_back(-1);
		}

		// now prepare external_track

		size_t to_pick_rung(parent->rung_count-1);
		size_t multiplicity((parent->cut_rungs+to_pick_rung)->get_multiplicity());
		// we go from vertex abs(loop_daughter.second)-1 till the last one
		for(size_t ii=abs(loop_daughter.second);ii<multiplicity+1;ii++){
			const std::vector<Particle*>& lparts(((parent->cut_rungs+to_pick_rung)->get_vertices())[ii-1].get_particles());
			for(size_t jj=0;jj<lparts.size();jj++){
				external_track.push_back(lparts[jj]->mom_index());
			}
		}
	}

	// quick check to see if all external particles were added
		// by mom conservation, we would drop them
	if(external_track.size()==parent->n_particles){
		DEBUG_MESSAGE("Dropping lD to lP: ",external_track);
		external_track.clear();
	}

	return std::make_pair(loop_track,external_track);
}

void get_index_daughter_to_parent_loop_rule(cutF* parent,std::pair<size_t,int>* loop_daughter,std::pair<std::vector<int>,std::vector<int> >* tofill){
	size_t to_lP_1(0);
	size_t to_lP_2(1);
	// lD1->lP1 && lD2->lP2
	if(loop_daughter[0].first==0&&loop_daughter[1].first==2){
		to_lP_1=0;
		to_lP_2=1;
	}
	// lD1->lP2 && lD2->lP1
	else if(loop_daughter[0].first==2&&loop_daughter[1].first==0){
		to_lP_1=1;
		to_lP_2=0;
	}
	else{
		std::cout<<"CASE in get_index_daughter_to_parent_loop_rule not covered!! Did nothing!!"<<std::endl;
		return;
	}
	// fill map for lP1
	tofill[0]=daughter_to_parent_loop_rule(parent,1,loop_daughter[to_lP_1],to_lP_1);
	// fill map for lP2
	tofill[1]=daughter_to_parent_loop_rule(parent,2,loop_daughter[to_lP_2],to_lP_2);
}

// default constructor
subtraction::subtraction(): nloops(0), n_particles(0), parent_level(0), parent_cstructure_locate(0), sign(0), is_unique_cutF_subtraction(true) {
	loop_momenta_from_d[0]=mom_routing_and_mass();
	loop_momenta_from_d[1]=mom_routing_and_mass();
}


// assign bow tie lP=lP(lDa,lDb) rules
void subtraction::assign_bow_tie(cutF* parent,size_t pinched_rung,size_t propagator_pinched,bool left_right_flip,bool top_bottom_flip){
	loop_momenta_from_d[0]=mom_routing_and_mass(n_particles);
	loop_momenta_from_d[1]=mom_routing_and_mass(n_particles);

	// we pinched first rung
	if(pinched_rung==0){
		// no flips
		if(!left_right_flip&&!top_bottom_flip){
			// lP2 is straghtforward
			loop_momenta_from_d[1].add_momenta(0,1);
			// lP1 includes lD1
			loop_momenta_from_d[0].add_momenta(1,0);
			// check if 0th prop has been dropped for l1
			if(propagator_pinched==0){
				// add particles in first vertex
				loop_momenta_from_d[0]+=(parent->cut_rungs->get_vertices())[0].get_particles();
			}
		}
		// both flips
		else if(left_right_flip&&top_bottom_flip){
			// lP2 is straghtforward
			loop_momenta_from_d[1].add_momenta(1,0);
			// lP1 includes lD2
			loop_momenta_from_d[0].add_momenta(0,1);
			// check if 0th prop has been dropped for l1
			if(propagator_pinched==0){
				// add particles in first vertex
				loop_momenta_from_d[0]+=(parent->cut_rungs->get_vertices())[0].get_particles();
			}
		}
		// left-right flip
		else if(left_right_flip&&!top_bottom_flip){
			// lP2 is straghtforward
			loop_momenta_from_d[1].add_momenta(-1,0);
			// and add full momenta in rung
			loop_momenta_from_d[1]+=(parent->cut_rungs+1)->get_particles();
			// lP1 includes lD2
			loop_momenta_from_d[0].add_momenta(0,1);
			int n_vertices((parent->cut_rungs->get_vertices()).size());
			int start_routing(n_vertices);
			// check if 0th prop has been dropped for l1
			if(propagator_pinched==size_t(n_vertices)){
				start_routing--;
			}
			// add particles around first rung
			loop_momenta_from_d[0].route_rung(parent->cut_rungs,size_t(start_routing),0);
			// and flip!
			loop_momenta_from_d[0]=-loop_momenta_from_d[0];
		}
		// top-bottom flip
		else{
			// lP2 is straghtforward
			loop_momenta_from_d[1].add_momenta(0,-1);
			// and add full momenta in rung
			loop_momenta_from_d[1]+=(parent->cut_rungs+1)->get_particles();
			// lP1 includes lD1
			loop_momenta_from_d[0].add_momenta(1,0);
			int n_vertices((parent->cut_rungs->get_vertices()).size());
			int start_routing(n_vertices);
			// check if 0th prop has been dropped for l1
			if(propagator_pinched==size_t(n_vertices)){
				start_routing--;
			}
			// add particles around first rung
			loop_momenta_from_d[0].route_rung(parent->cut_rungs,size_t(start_routing),0);
			// and flip!
			loop_momenta_from_d[0]=-loop_momenta_from_d[0];
		}
	}
	// we pinched second rung
	else{
		int n_vertices_second(((parent->cut_rungs+1)->get_vertices()).size());
		// no flips
		if(!left_right_flip&&!top_bottom_flip){
			// lP1 is straghtforward
			loop_momenta_from_d[0].add_momenta(1,0);
			// lP2 includes lD2
			loop_momenta_from_d[1].add_momenta(0,1);
			// check if nth prop has been dropped for l1
			if(propagator_pinched==size_t(n_vertices_second)){
				// add particles in last vertex
				loop_momenta_from_d[1]+=((parent->cut_rungs+1)->get_vertices()).back().get_particles();
			}
		}
		// both flips
		else if(left_right_flip&&top_bottom_flip){
			// lP1 is straghtforward
			loop_momenta_from_d[0].add_momenta(0,1);
			// lP2 includes lD1
			loop_momenta_from_d[1].add_momenta(1,0);
			// check if nth prop has been dropped for l1
			if(propagator_pinched==size_t(n_vertices_second)){
				// add particles in last vertex
				loop_momenta_from_d[1]+=((parent->cut_rungs+1)->get_vertices()).back().get_particles();
			}
		}
		// left-right flip
		else if(left_right_flip&&!top_bottom_flip){
			// lP1 is straghtforward
			loop_momenta_from_d[0].add_momenta(0,-1);
			// and add full momenta in rung
			loop_momenta_from_d[0]+=(parent->cut_rungs)->get_particles();
			// lP2 includes lD1
			loop_momenta_from_d[1].add_momenta(1,0);
			int start_routing(0);
			// check if 0th prop has been dropped for l1
			if(propagator_pinched==0){
				start_routing++;
			}
			// add particles around second rung
			loop_momenta_from_d[1].route_rung(parent->cut_rungs,size_t(start_routing),n_vertices_second);
			// and flip!
			loop_momenta_from_d[1]=-loop_momenta_from_d[0];
		}
		// top-bottom flip
		else{
			// lP1 is straghtforward
			loop_momenta_from_d[0].add_momenta(-1,0);
			// and add full momenta in rung
			loop_momenta_from_d[0]+=parent->cut_rungs->get_particles();
			// lP2 includes lD2
			loop_momenta_from_d[1].add_momenta(0,1);
			int start_routing(0);
			// check if 0th prop has been dropped for l1
			if(propagator_pinched==0){
				start_routing++;
			}
			// add particles around second rung
			loop_momenta_from_d[1].route_rung(parent->cut_rungs,size_t(start_routing),n_vertices_second);
			// and flip!
			loop_momenta_from_d[1]=-loop_momenta_from_d[0];
		}
	}
	// check mom cons
	loop_momenta_from_d[0].mom_conservation();
	loop_momenta_from_d[1].mom_conservation();
}


// assign loop momenta rules for parents with 3-rung
void subtraction::assign_general(bool daughter_is_bow_tie,cutF* parent){
	size_t right_rung(2);
	if(daughter_is_bow_tie)
		right_rung=1;

	// in case no loop_momenta_from_daughter[].first is central rung, assignments straight forward:
	loop_momenta_from_d[0]=mom_routing_and_mass(n_particles);
	loop_momenta_from_d[1]=mom_routing_and_mass(n_particles);
	if((loop_momenta_from_daughter[0].first!=1&&loop_momenta_from_daughter[1].first!=1)||daughter_is_bow_tie){
		// l1
		if(loop_momenta_from_daughter[0].first==0){
			loop_momenta_from_d[0].route_rung(parent->cut_rungs,abs(loop_momenta_from_daughter[0].second)-1,0);
			if(loop_momenta_from_daughter[0].second<0)
				loop_momenta_from_d[0].add_momenta(1,0);
			else
				loop_momenta_from_d[0].add_momenta(-1,0);
			loop_momenta_from_d[0]=-loop_momenta_from_d[0];
		}
		else{
			loop_momenta_from_d[0].route_rung(parent->cut_rungs,abs(loop_momenta_from_daughter[0].second)-1,0);
			if(loop_momenta_from_daughter[0].second<0)
				loop_momenta_from_d[0].add_momenta(0,1);
			else
				loop_momenta_from_d[0].add_momenta(0,-1);
			loop_momenta_from_d[0]=-loop_momenta_from_d[0];
		}
		// l2
		if(loop_momenta_from_daughter[1].first==0){
			int n_vertices(((parent->cut_rungs+right_rung)->get_vertices()).size());
			loop_momenta_from_d[1].route_rung(parent->cut_rungs+right_rung,abs(loop_momenta_from_daughter[1].second)-1,n_vertices);
			if(loop_momenta_from_daughter[1].second<0)
				loop_momenta_from_d[1].add_momenta(1,0);
			else
				loop_momenta_from_d[1].add_momenta(-1,0);
			loop_momenta_from_d[1]=-loop_momenta_from_d[1];
		}
		else{
			int n_vertices(((parent->cut_rungs+right_rung)->get_vertices()).size());
			loop_momenta_from_d[1].route_rung(parent->cut_rungs+right_rung,abs(loop_momenta_from_daughter[1].second)-1,n_vertices);
			if(loop_momenta_from_daughter[1].second<0)
				loop_momenta_from_d[1].add_momenta(0,1);
			else
				loop_momenta_from_d[1].add_momenta(0,-1);
			loop_momenta_from_d[1]=-loop_momenta_from_d[1];
		}
	}
	else{
		std::cout<<"ERROR: no rule written yet in subtraction::subtraction(.) for non-planar cases"<<std::endl;
	}
	// check mom cons
	loop_momenta_from_d[0].mom_conservation();
	loop_momenta_from_d[1].mom_conservation();
}

// subtraction directly constructed only for 1-level inheritance (parent & daughter)
// Higher levels constructed from composition (operator*)
// last cutF is optional, in case daughter is not a direct pinch
subtraction::subtraction(cutF* parent,cutF* daughter,const size_t& pinch,cutF* direct_pinch_daughter): n_particles(int(parent->n_particles)), parent_level(0), parent_cstructure_locate(0),
		sign(1), is_unique_cutF_subtraction(true) {
    using Caravel::operator<<;
#if 0
std::cout<<"CONSTRUCTING SUBTRACTION!!! pinch: "<<pinch<<std::endl;
std::cout<<"Parent: "<<parent->get_short_name()<<std::endl;
std::cout<<"Daughter: "<<daughter->get_short_name()<<std::endl;
if(direct_pinch_daughter!=nullptr)
	std::cout<<"Daughter directly pinched: "<<direct_pinch_daughter->get_short_name()<<std::endl;
#endif
	if(parent->rung_count<1||parent->rung_count>3){
		nloops=0;
		std::cout<<"Only subtraction of 1- or 2-loop cuts handled -- returned empty subtraction"<<std::endl;
		return;
	}
	// 1-loop case
	else if(parent->rung_count==1){
		nloops=1;
		// no transformation needed (direct pinch)
		if(direct_pinch_daughter==nullptr||1){
			size_t mPar=parent->get_graph().get_base_graph().get_connection(0,0).get_edges()[0].mom_label_link(),
				mDgh=daughter->get_graph().get_base_graph().get_connection(0,0).get_edges()[0].mom_label_link();
			std::vector<lGraph::Link> lPar=parent->get_graph().get_base_graph().get_connection(0,0).get_edges()[0].get_links(),
				lDgh=daughter->get_graph().get_base_graph().get_connection(0,0).get_edges()[0].get_links();
			int mlPar=lPar[mPar].get_momentum_label(),
				mlDgh=lDgh[mDgh].get_momentum_label();
			std::vector<std::vector<size_t> >bmPar,bmDgh;
			bmPar.push_back(parent->get_graph().get_base_graph().get_nodes()[0].get_momentum_indices());
			bmDgh.push_back(daughter->get_graph().get_base_graph().get_nodes()[0].get_momentum_indices());
			for(size_t i=0;i<parent->get_graph().get_base_graph().get_connection(0,0).get_edges()[0].size();++i)
				bmPar.push_back(parent->get_graph().get_base_graph().get_connection(0,0).get_edges()[0][i].get_momentum_indices());
			for(size_t i=0;i<daughter->get_graph().get_base_graph().get_connection(0,0).get_edges()[0].size();++i)
				bmDgh.push_back(daughter->get_graph().get_base_graph().get_connection(0,0).get_edges()[0][i].get_momentum_indices());
			bool found=false;
			std::vector<size_t> minusPar,minusDgh,plusPar,plusDgh,minusSol,plusSol;
			size_t lkPar=0,lkDgh=0;
			for(size_t i=0;i<bmPar.size()&&!found;++i) {
				size_t iStart=(mPar+(mlPar*i)+(mlPar<0?1:0)+bmPar.size())%bmPar.size();
				size_t iEnd=(iStart+mlPar+bmPar.size())%bmPar.size();
				for(size_t j=0;j<bmDgh.size()&&!found;++j) {
					size_t jStart=(mDgh+(mlDgh*j)+(mlDgh<0?1:0)+bmDgh.size())%bmDgh.size();
					size_t jEnd=(jStart+mlDgh+bmDgh.size())%bmDgh.size();
					for(size_t k=0;k<bmDgh[jStart].size()&&!found;++k)
						if(std::find(bmPar[iStart].begin(),bmPar[iStart].end(),bmDgh[jStart][k])!=bmPar[iStart].end())
							for(size_t l=0;l<bmDgh[jEnd].size()&&!found;++l)
								if(std::find(bmPar[iEnd].begin(),bmPar[iEnd].end(),bmDgh[jEnd][l])!=bmPar[iEnd].end()) {
									found=true;
									lkPar=i;
									lkDgh=j;
								}
				}
			}
			for(size_t i=0;i<lkPar;++i) {
				size_t iDir=(mPar+(mlPar*i)+bmPar.size()+(mlPar>0?1:0))%bmPar.size();
				minusPar.insert(minusPar.end(),bmPar[iDir].begin(),bmPar[iDir].end());
			}
			for(size_t i=0;i<lkDgh;++i) {
				size_t iDir=(mDgh+(mlDgh*i)+bmDgh.size()+(mlDgh>0?1:0))%bmDgh.size();
				minusDgh.insert(minusDgh.end(),bmDgh[iDir].begin(),bmDgh[iDir].end());
			}
			plusSol=plusDgh;
			minusSol=minusDgh;
			for(size_t i=0;i<minusPar.size();++i) {
				auto itMinusSol=std::find(minusSol.begin(),minusSol.end(),minusPar[i]);
				if(itMinusSol==minusSol.end())
					plusSol.push_back(minusPar[i]);
				else minusSol.erase(itMinusSol);
			}
			for(size_t i=0;i<plusPar.size();++i) {
				auto itPlusSol=std::find(plusSol.begin(),plusSol.end(),plusPar[i]);
				if(itPlusSol==plusSol.end())
					minusSol.push_back(plusPar[i]);
				else plusSol.erase(itPlusSol);
			}
			std::vector<size_t> no_partsM,no_partsP;
			for(int i=1;i<=n_particles;++i)
				if(std::find(minusSol.begin(),minusSol.end(),i)==minusSol.end())
					no_partsM.push_back(i);
			for(size_t i=0;i<plusSol.size();++i)
				if(std::find(no_partsM.begin(),no_partsM.end(),i)==no_partsM.end())
					no_partsM.push_back(plusSol[i]);
			for(int i=1;i<=n_particles;++i)
				if(std::find(plusSol.begin(),plusSol.end(),i)==plusSol.end())
					no_partsP.push_back(i);
			for(size_t i=0;i<minusSol.size();++i)
				if(std::find(no_partsP.begin(),no_partsP.end(),i)==no_partsP.end())
					no_partsP.push_back(minusSol[i]);
			if(no_partsM.size()<plusSol.size()+minusSol.size()&&no_partsM.size()<no_partsP.size()) {
				minusSol=std::vector<size_t>();
				plusSol=no_partsM;
			}
			if(no_partsP.size()<plusSol.size()+minusSol.size()) {
				plusSol=std::vector<size_t>();
				minusSol=no_partsP;
			}
			dropped_props.push_back(std::make_pair(0,pinch+1));
			// construct dropped_props_indices
			mom_routing_and_mass lmr(n_particles);
			lmr.add_momenta(1,0);
			// route the rung from 0 to pinch
			lmr.route_rung(parent->cut_rungs,0,pinch);
                        // set mass index in lmr
			lmr.set_mass( (parent->cut_rungs->get_link(pinch)).get_particle().get_mass_index() );
			dropped_props_indices.push_back(lmr);
			if(pinch==0){
				// bring up proper bottom prop, the last one
				loop_momenta_from_daughter[0]=std::make_pair(0,int(parent->get_prop_count()));
  			        loop_momenta_from_d[0]=mom_routing_and_mass(n_particles);
				// l1
				loop_momenta_from_d[0].add_momenta(1,0);
				// subtract particles in last vertex (of parent)
				loop_momenta_from_d[0]-=(parent->cut_rungs->get_vertices()).back().get_particles();

			}
			else{
				// perfectly aligned
				loop_momenta_from_daughter[0]=std::make_pair(0,1);
				loop_momenta_from_d[0]=mom_routing_and_mass(n_particles);
				// l1
				loop_momenta_from_d[0].add_momenta(1,0);

			}
///*********** MESS [
                        if (!parent->has_massive_propagator()) {
                            std::vector<int> extp;
                            for (auto& ps : plusSol) extp.push_back(ps);
                            for (auto& ms : minusSol) extp.push_back(-ms);
                            loop_momenta_from_d[0] = mom_routing_and_mass(n_particles, 1, 0, extp);
                        }
//************ MESS ]*/
			// useless
			loop_momenta_from_daughter[1]=std::make_pair(0,0);
			loop_momenta_from_d[1]=mom_routing_and_mass();
                }
		else{
			std::cout<<"ONE LOOP with non direct pinch?? -- Careful!! "<<std::endl;
			std::cout<<"parent: "<<parent->get_short_name()<<" daughter: "<<daughter->get_short_name()
				<<" related-daughter: "<<direct_pinch_daughter->get_short_name()<<std::endl;
		}
	}
	// bow tie case
	else if(parent->rung_count==2){
		nloops=2;
		sign=1;
		size_t pinched_rung(0);
		size_t propagator_pinched(pinch);
		if(pinch>parent->cut_rungs->get_multiplicity()){
			pinched_rung=1;
			propagator_pinched=pinch-parent->cut_rungs->get_multiplicity()-1;
		}
		dropped_props.push_back(std::make_pair(pinched_rung,propagator_pinched+1));
		bool left_right_flip(false);
		bool top_bottom_flip(false);
		// in a bowtie there has to always be at least a particle on each rung
		Particle* DlpartL(nullptr);
                if(daughter->cut_rungs->get_particles().size()>0)
                    DlpartL=daughter->cut_rungs->get_particles()[0];
		const std::vector<Particle*> Pvparts(parent->cut_rungs->get_particles());
		if(std::find(Pvparts.begin(),Pvparts.end(),DlpartL)==Pvparts.end()){
			left_right_flip=true;
		}
		// find which rung of daughter has at least two particles
		size_t rung_2parts(0);
		if(daughter->cut_rungs->get_particles().size()<2)
			rung_2parts=1;
		const std::vector<Particle*> D_2parts((daughter->cut_rungs+rung_2parts)->get_particles());
		// now check for top-bottom flip
		if(!left_right_flip){
                    if(D_2parts.size()>0 && (parent->cut_rungs+rung_2parts)->get_particles().size()>0)
			if(D_2parts[0]==(parent->cut_rungs+rung_2parts)->get_particles().back()
				|| D_2parts.back()==(parent->cut_rungs+rung_2parts)->get_particles()[0])
					top_bottom_flip=true;
		}
		// check is against 'other' rung
		else{
			size_t c_parent_rung(0);
			if(rung_2parts==0)
				c_parent_rung=1;
                    if(D_2parts.size()>0 && (parent->cut_rungs+c_parent_rung)->get_particles().size()>0)
			if(D_2parts[0]==(parent->cut_rungs+c_parent_rung)->get_particles().back()
				|| D_2parts.back()==(parent->cut_rungs+c_parent_rung)->get_particles()[0])
					top_bottom_flip=true;
		}

		// construct dropped_props_indices
		mom_routing_and_mass lmr(n_particles);
		if(pinched_rung==0){
			// l1
			lmr.add_momenta(1,0);
			lmr.route_rung(parent->cut_rungs,0,propagator_pinched);
                        // set mass index in lmr
			lmr.set_mass( (parent->cut_rungs->get_link(propagator_pinched)).get_particle().get_mass_index() );
		}
		else{
			// l2
			lmr.add_momenta(0,1);
			lmr.route_rung(parent->cut_rungs+1,propagator_pinched,((parent->cut_rungs+1)->get_vertices()).size());
                        // set mass index in lmr
			lmr.set_mass( ((parent->cut_rungs+1)->get_link(propagator_pinched)).get_particle().get_mass_index() );
		}
		lmr.mom_conservation();
		// route the rung from 0 to pinch
		dropped_props_indices.push_back(lmr);

//std::cout<<"TESTING: "<<dropped_props_indices.back().get_info("P")<<std::endl;

		// now assign relations between loop momenta
		this->assign_bow_tie(parent,pinched_rung,propagator_pinched,left_right_flip,top_bottom_flip);
//std::cout<<"Bow tie assignments: lP1 = "<<loop_momenta_from_d[0].get_info("D")<<" lP2 = "<<loop_momenta_from_d[1].get_info("D")<<std::endl;

	}
	// general 2-loop case
	else{
		nloops=2;
		// find which rung was pinched
		size_t pinched_rung(0);
		size_t propagator_pinched(pinch);
		if(pinch>parent->cut_rungs->get_multiplicity()){
			if(pinch>parent->cut_rungs->get_multiplicity()+(parent->cut_rungs+1)->get_multiplicity()+1){
				// pinch falls in right (third) rung
				pinched_rung=2;
				propagator_pinched=pinch-parent->cut_rungs->get_multiplicity()-(parent->cut_rungs+1)->get_multiplicity()-2;
			}
			else{
				pinched_rung=1;
				propagator_pinched=pinch-parent->cut_rungs->get_multiplicity()-1;
			}
		}
		dropped_props.push_back(std::make_pair(pinched_rung,propagator_pinched+1));

		// construct dropped_props_indices
		mom_routing_and_mass lmr(n_particles);
		if(pinched_rung==0){
			// l1
			lmr.add_momenta(1,0);
			lmr.route_rung(parent->cut_rungs,0,propagator_pinched);
                        // set mass index in lmr
			lmr.set_mass( (parent->cut_rungs->get_link(propagator_pinched)).get_particle().get_mass_index() );
		}
		else if(pinched_rung==2){
			// l2
			lmr.add_momenta(0,1);
			lmr.route_rung(parent->cut_rungs+2,propagator_pinched,((parent->cut_rungs+2)->get_vertices()).size());
                        // set mass index in lmr
			lmr.set_mass( ((parent->cut_rungs+2)->get_link(propagator_pinched)).get_particle().get_mass_index() );
		}
		// central rung
		else{
			// l1-l2
			lmr.add_momenta(1,-1);
			// full momenta in first rung
			lmr.route_rung(parent->cut_rungs);
			lmr.route_rung(parent->cut_rungs+1,propagator_pinched,((parent->cut_rungs+1)->get_vertices()).size());
                        // set mass index in lmr
			lmr.set_mass( ((parent->cut_rungs+1)->get_link(propagator_pinched)).get_particle().get_mass_index() );
		}
		lmr.mom_conservation();
		// route the rung from 0 to pinch
		dropped_props_indices.push_back(lmr);
#if 0
std::cout<<"General case prop dropped: "<<dropped_props_indices.back().get_info("P")<<std::endl;
#endif
		// delegate assignment of loop mom map to this function
		map_loop_mom_3rungs(loop_momenta_from_daughter,parent,daughter,pinched_rung,propagator_pinched,direct_pinch_daughter);
#if 0
std::cout<<"Obtained props corresponding to daughter's momenta: "<<loop_momenta_from_daughter[0]<<" and: "<<loop_momenta_from_daughter[1]<<std::endl;
#endif
		bool daughter_is_bow_tie(false);
		if(daughter->rung_count==2)
			daughter_is_bow_tie=true;

		// now assign relations between loop momenta
		this->assign_general(daughter_is_bow_tie,parent);
#if 0
std::cout<<"General 2-loop assignments: lP1 = "<<loop_momenta_from_d[0].get_info("D")<<" lP2 = "<<loop_momenta_from_d[1].get_info("D")<<std::endl;
#endif
	}
#if 0
std::cout<<"Dropped propagators:"<<std::endl;
for(size_t ii=0;ii<dropped_props.size();ii++)
	std::cout<<dropped_props[ii]<<std::endl;
std::cout<<"loop mom from daughter [0]: "<<loop_momenta_from_daughter[0]<<std::endl;
std::cout<<"loop mom from daughter [1]: "<<loop_momenta_from_daughter[1]<<std::endl;
#endif
}

subtraction operator*(const subtraction& is1,const subtraction& is2){
	// sanity checks
	if(is1.nloops!=is2.nloops){
		std::cout<<"Composing subtraction terms should be among same # of loops cuts! -- Returned empty subtraction"<<std::endl;
		return subtraction();
	}
	if(is1.nloops==0){
		std::cout<<"Trying to compose default constructed subtraction's -- returned empty subtraction"<<std::endl;
		return subtraction();
	}
	if(is1.nloops<1||is1.nloops>2){
		std::cout<<"Composition of subtractions working only for 1 and 2 loops!! --returned empty subtraction"<<std::endl;
		return subtraction();
	}
	// order of composition should be correct
	if(is1.dropped_props.size()<is2.dropped_props.size()){
//std::cout<<"drop left: "<<is1.dropped_props.size()<<" and right: "<<is2.dropped_props.size()<<std::endl;
		std::cout<<"Subtraction composition takes to the left always (strictly) older parent!! -- Returned empty subtraction"<<std::endl;
		return subtraction();
	}
	if(is2.dropped_props.size()!=1){
		std::cout<<"Composing subtractions always takes to the right a 1-level subtraction -- returned empty subtraction"<<std::endl;
		return subtraction();
	}
//std::cout<<"WILL compose subtractions!!"<<std::endl;
	subtraction toreturn;
	// assign # of loops
	toreturn.nloops=is1.nloops;

	// add all dropped propagators from is1
	for(size_t ii=0;ii<is1.dropped_props_indices.size();ii++){
		// this only filled properly when directly constructed
		toreturn.dropped_props.push_back(std::make_pair(0,0));
		toreturn.dropped_props_indices.push_back(is1.dropped_props_indices[ii]);
	}

	// 1-loop case
	if(toreturn.nloops==1){
		// add unique propagator from is2 with substitution of loop mom of Daughter1 in terms of loop from parent:
		// P -> D1 -> D2, then substitute in rule lD1=lD1(lP)
		toreturn.dropped_props.push_back(std::make_pair(0,0));
		// add propagator by substituting lD1 = lD1(lP)=lP-{external} (the inverse of the rule lP=lP(lD1)=lD1+{external}) into propagator (lD1+{external^\prime})
		toreturn.dropped_props_indices.push_back(is2.dropped_props_indices[0].substitute_inverse(is1.loop_momenta_from_d[0]));

		// and now lP rule in terms of lD2
		toreturn.loop_momenta_from_daughter[0]=std::make_pair(0,0);
		toreturn.loop_momenta_from_daughter[1]=std::make_pair(0,0);
		// give rule lP=lP(lD2) by composing: lP=lP(lD1)=lD1+{external}=lP(lD1(lD2))=(lD2+{external^\prime})+{external}
		toreturn.loop_momenta_from_d[0]=is1.loop_momenta_from_d[0].substitute(is2.loop_momenta_from_d[0]);
		toreturn.loop_momenta_from_d[1]=mom_routing_and_mass();
	}
	// 2-loop case
	else{
		// add unique propagator from is2 with substitution of loop moms of Daughter1 in terms of loop from parent:
		// P -> D1 -> D2, then substitute in rule lD1=lD1(lP)
		toreturn.dropped_props.push_back(std::make_pair(0,0));
		toreturn.dropped_props_indices.push_back(is2.dropped_props_indices[0].solve_prop_2loops(is1.loop_momenta_from_d[0],is1.loop_momenta_from_d[1]));

		// and now lP rule in terms of lD2
		toreturn.loop_momenta_from_daughter[0]=std::make_pair(0,0);
		toreturn.loop_momenta_from_daughter[1]=std::make_pair(0,0);
		toreturn.loop_momenta_from_d[0]=is1.loop_momenta_from_d[0].substitute(is2.loop_momenta_from_d[0],is2.loop_momenta_from_d[1]);
		toreturn.loop_momenta_from_d[1]=is1.loop_momenta_from_d[1].substitute(is2.loop_momenta_from_d[0],is2.loop_momenta_from_d[1]);

	}

	return toreturn;
}

#if 0
// copy constructor
subtraction::subtraction(const subtraction& is): nloops(is.nloops), parent_level(is.parent_level), parent_cstructure_locate(is.parent_cstructure_locate),
		dropped_props(is.dropped_props), dropped_props_indices(is.dropped_props_indices) {
	loop_momenta_from_daughter[0]=is.loop_momenta_from_daughter[0];
	loop_momenta_from_daughter[1]=is.loop_momenta_from_daughter[1];
	loop_momenta_from_d[0]=loop_momenta_from_d[0];
	loop_momenta_from_d[1]=loop_momenta_from_d[1];
}

// assignment operator
subtraction& subtraction::operator=(const subtraction& is){
	nloops=is.nloops;
	parent_level=is.parent_level;
	parent_cstructure_locate=is.parent_cstructure_locate;
	dropped_props=is.dropped_props;
	dropped_props_indices=is.dropped_props_indices;

	loop_momenta_from_daughter[0]=is.loop_momenta_from_daughter[0];
	loop_momenta_from_daughter[1]=is.loop_momenta_from_daughter[1];
	loop_momenta_from_d[0]=loop_momenta_from_d[0];
	loop_momenta_from_d[1]=loop_momenta_from_d[1];

	return *this;
}

subtraction::~subtraction(){
}
#endif

std::string subtraction::get_info() const {
	// default constructed
	if(nloops==0)
		return "subtraction default constructed";
#if 0
std::cout<<std::endl<<dropped_props[0].first<<","<<dropped_props[0].second<<" "
	<<loop_momenta_from_daughter[0].first<<","<<loop_momenta_from_daughter[0].second<<" "
	<<loop_momenta_from_daughter[1].first<<","<<loop_momenta_from_daughter[1].second<<std::endl;
#endif
#if 0
	std::string togiveinfo("[");
	if(sign==1)
		togiveinfo+="+";
	else
		togiveinfo+="-";
	togiveinfo+="] ";
#else
	std::string togiveinfo("");
#endif
	togiveinfo+="("+std::to_string(parent_level)+","+std::to_string(parent_cstructure_locate)+") dropped props: ";
	for(size_t ii=0;ii<dropped_props_indices.size();ii++){
		togiveinfo+="("+dropped_props_indices[ii].get_info("P")+")";
                if(dropped_props_indices[ii].get_mass()>0)
                    togiveinfo+="[mass=m_"+std::to_string(dropped_props_indices[ii].get_mass())+"]";
                togiveinfo+=" ";
	}
	std::string toreturn("");
	if(nloops==1){
		toreturn+="lP = ";
	}
	else{
		toreturn+="lP1 = ";
	}
	// 1-loop case
	if(nloops==1){
		toreturn+=loop_momenta_from_d[0].get_info("D");
	}
	// 2-loop case
	else{
		toreturn+=loop_momenta_from_d[0].get_info("D")+"; ";
		toreturn+="lP2 = "+loop_momenta_from_d[1].get_info("D");
	}

	return togiveinfo+"--- "+toreturn;
}

CStructure::CStructure(): rung_count(0), n_particles(0), accepts_direct_fit(false) {
	location_loops[0]=0;
	location_loops[1]=0;
	location_loops[2]=0;
	location_loops[3]=0;
}

CStructure::CStructure(const lGraph::xGraph& xG, size_t rc, std::vector<size_t> vp, int in, size_t* ll, std::vector<size_t> mi, bool ib)
    : rung_count(rc), partition(vp), n_particles(in), mom_indices(mi), accepts_direct_fit(ib), graph(xG) {
    *location_loops = *ll;
    *(location_loops + 1) = *(ll + 1);
    *(location_loops + 2) = *(ll + 2);
    *(location_loops + 3) = *(ll + 3);
#if 0
std::cout<<"assembled partition: ";
for(size_t ii=0;ii<partition.size();ii++)
	std::cout<<partition[ii]<<" ";
std::cout<<std::endl;
std::cout<<"loop mom location: l1 "<<location_loops[0]<<" -l1 "<<location_loops[1]<<" l2 "<<location_loops[2]<<" -l2 "<<location_loops[3]<<std::endl;
std::cout<<"n_particles: "<<n_particles<<std::endl;
std::cout<<"indices of momenta following the tree for the cut: ";
for(size_t ii=0;ii<mom_indices.size();ii++)
	std::cout<<mom_indices[ii]<<" ";
std::cout<<std::endl;
#endif

}

}
}
