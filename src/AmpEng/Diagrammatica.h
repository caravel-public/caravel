/**
 * @file Diagrammatica.h
 *
 * @date 5.6.2019
 *
 * @brief General tools for producing diagrams in AmpEng
 *
 * We develop here a series of tools helpful for constructing diagram/cut
 * hierarchies in AmpEng. We rely heavily on the Graph library.
 *
 * Check out http://inspirehep.net/record/380718 !
*/
#pragma once

#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <map>
#include <set>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Graph/Graph.h"

#include "AmpEng/cutF.h"

namespace Caravel {
namespace AmpEng {

/**
 * An instance of this class holds all information of connected vacuum graphs for a given number of loops 'n'.
 * Graphs contained:
 *
 * 1) are all produced from the n-loop 1-node (flower) graph
 * 
 * 2) can not have nodes with only two edges attached (with the trivial exception for the 1-loop case)
 */
class Vacua {
    int n_loops;    /**< The number of loops of the vacuum graphs held */
    std::unordered_map<int,std::set<lGraph::Graph<int, lGraph::Connection<int>>>> graphs;   /**< Container of vacuum graphs, ordered by number of nodes, and pointing to an std::set */
  public:
    Vacua() = default;
    /**
     * Main constructor for the class. Given an integer n, it'll construct all connected n-loop vacuum graphs
     */
    explicit Vacua(int);
    int get_n_loops() const { return n_loops; }
    const auto& get_all_vacuum_graphs() const { return graphs; }
    const auto& get_vacuum_graphs(int i) const { return graphs.at(i); }
    size_t size() const { size_t i(0); for(auto& s:graphs) i+=s.second.size(); return i; }
  private:
    /**
     * Main tool for construction. Given an input graph, tries all possible openings to include in graphs.at(n)
     */
    void include_daughters(const lGraph::Graph<int, lGraph::Connection<int>>&,int n);
};

// simple Graph
using sGraph = lGraph::Graph<std::vector<size_t>, lGraph::Connection<std::vector<std::vector<size_t>>>>;
// topology
using Topology = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;

struct xComp {
    bool operator()(const lGraph::xGraph& g1,const lGraph::xGraph& g2) const {
        return g1.get_base_graph()<g2.get_base_graph();
    }
};

struct GraphCutMap;

/**
 * Data structure to hold hierachical information of xGraphs (or other Graph classes)
 */
class Hierarchy {
  protected:
    int n_loops;    /**< The number of loops of the graphs held */
    std::unordered_map<size_t, std::set<sGraph>> graphs;   /**< Container of sGraphs, ordered by number of nodes, and pointing to an std::set */
    std::unordered_map<size_t, std::vector<std::vector<lGraph::xGraph>>> xgraphs; /**< Container of all graphs, mapped according to n nodes, to and std::vector<
                                                                                     std::vector >, where the lth vector entry has (in topology) l legs */
    // TODO: the following containers are added for simpler access. But memory-wise they could be avoided, replaced in terms of just maps
    std::unordered_map<size_t, std::vector<std::vector<Topology>>> topologies; /**< Container of topologically inequivalent graphs, mapped Topology */
    std::map<size_t, std::vector<lGraph::xGraph>> hier_prop;    /**< Hierarchical arrangement in the number of propagators */
    std::map<size_t, std::vector<Topology>> topo_hier_prop;    /**< Hierarchical arrangement of topologies in the number of propagators */
    bool unique_insertion{true};  /**< Wheter or not we insert only a unique copy for each equivalente xgraph inserted */
  public:
    Hierarchy() = default;
    Hierarchy(const AmpEng::CutHierarchy&, bool unique = true);
    Hierarchy(const std::vector<lGraph::xGraph>&, bool unique = true);
    Hierarchy(const Hierarchy&, const GraphCutMap&, bool unique = true);

    /**
     * Filters graphs in *this, according to the passed std::function
     */
    virtual void filter(const std::function<bool(const lGraph::xGraph&)>&, bool print_filtered = false);

    /**
     * Returns the size of all xgraphs contained
     */
    virtual size_t size() const;
    /**
     * Returns the size of all topologies contained
     */
    virtual size_t topology_size() const;
    virtual int get_n_loops() const { return n_loops; }
    virtual const decltype(xgraphs)::mapped_type& get_xgraphs(size_t i) const { return xgraphs.at(i); }
    virtual const decltype(topologies)::mapped_type& get_topologies(size_t i) const { return topologies.at(i); }
    virtual const decltype(hier_prop)& get_propagator_hierarchy() const { return hier_prop; }
    virtual const decltype(topo_hier_prop)& get_topology_propagator_hierarchy() const { return topo_hier_prop; }
    virtual std::vector<lGraph::xGraph> get_all_xgraphs() const;

    virtual ~Hierarchy();

  protected:
    virtual void update_containers();
    virtual void show_graph_counting() const;
    void insert_xgraph(const lGraph::xGraph&);
};

/**
 * An instance of this class holds all information of connected graphs with 'n' loops and a given set of external
 * legs specified according to their mass labels.
 *
 * Graphs contained:
 *
 * 1) are all produced starting from the corresponding vacuum graphs in Vacua(n)
 * 
 * 2) have un-specified edges (no assumption over possible "masses" on internal "propagators")
 */
class Walkers : public Hierarchy {
    size_t n_legs;    /**< The number of legs on the graphs held */
    std::vector<size_t> legs_masses;    /**< This is the main set of legs, where each entry corresponds to the associated mass label */
    std::vector<lGraph::Leg> legs;    /**< Container with all info for legs */
    Vacua vacuum_graphs;    /**< Could be removed and keep it only temporarily at construction time */
  public:
    Walkers() = default;
    /**
     * Main constructor for the class. It will construct all graphs with as many legs as the size
     * of the passed vector, considering the mass labels (unsigned integers) that it contains
     *
     * @param integer representing the number of loops
     * @param std::vector<size_t> specifying the mass labels for each of the external legs (ordering irrelevant, but positions will be taken as momentum indices)
     */
    explicit Walkers(int,const std::vector<size_t>&);
    /**
     * Quick constructor for graphs with n loops and m legs (all assumed massless, more precisely
     * with '0' mass label)
     *
     * @param integer representing the number of loops
     * @param unsigned integer representing the number of external legs (all will be assumed to have the same mass label, set to '0')
     */
    explicit Walkers(int,size_t);

    /**
     * Filters graphs in *this, according to the passed std::function
     */
    void filter(const std::function<bool(const lGraph::xGraph&)>& ff, bool print_filtered = false) { Hierarchy::filter(ff, print_filtered); }

    /**
     * Returns the size of all xgraphs contained
     */
    size_t size() const { return Hierarchy::size(); }
    /**
     * Returns the size of all topologies contained
     */
    size_t topology_size() const { return Hierarchy::topology_size(); }
    int get_n_loops() const { return Hierarchy::get_n_loops(); }
    const std::vector<std::vector<lGraph::xGraph>>& get_xgraphs(size_t i) const { return Hierarchy::get_xgraphs(i); }
    const std::vector<std::vector<Topology>>& get_topologies(size_t i) const { return Hierarchy::get_topologies(i); }
    const std::map<size_t, std::vector<lGraph::xGraph>>& get_propagator_hierarchy() const { return Hierarchy::get_propagator_hierarchy(); }
    const std::map<size_t, std::vector<Topology>>& get_topology_propagator_hierarchy() const { return Hierarchy::get_topology_propagator_hierarchy(); }
    std::vector<lGraph::xGraph> get_all_xgraphs() const { return Hierarchy::get_all_xgraphs(); }

    ~Walkers();
  private:
    /**
     * Primary tool for construction. Given an input vacuum graph, produces all possible graphs with legs attached to nodes and edges
     */
    void process_vacuum_graph(int n,const lGraph::Graph<int, lGraph::Connection<int>>&);
    void update_containers() { Hierarchy::update_containers(); }
    void show_graph_counting() const { Hierarchy::show_graph_counting(); }
};

/**
 * This class construct xGraph hierarchies from a list of xgraphs passed at construction time.
 * Basically tries all pinches of them recursively and stores unique instances of the xgraphs
 * produced.
 *
 * All xgraphs should be consistent, in the sense of same number of loops, same set of external
 * legs
 */
class Pincher : public Hierarchy {
    size_t n_legs;    /**< The number of legs on the graphs held */
    std::vector<size_t> legs_masses;    /**< This is the main set of legs, where each entry corresponds to the associated mass label */
    std::vector<lGraph::Leg> legs;    /**< Container with all info for legs */
  public:
    Pincher() = default;
    /**
     * Main constructor for the class. After few sanity checks, goes into a loop
     * to fill containers of graphs
     *
     * Unsigned integer argument controls the depth of the recursive pinching
     * procedure. Defaulted to 0, which means to go the deepest possible
     */
    explicit Pincher(const std::vector<lGraph::xGraph>&, size_t depth = 0);

    /**
     * Filters graphs in *this, according to the passed std::function
     */
    void filter(const std::function<bool(const lGraph::xGraph&)>& ff, bool print_filtered = false) { Hierarchy::filter(ff, print_filtered); }

    /**
     * Returns the size of all xgraphs contained
     */
    size_t size() const { return Hierarchy::size(); }
    /**
     * Returns the size of all topologies contained
     */
    size_t topology_size() const { return Hierarchy::topology_size(); }
    int get_n_loops() const { return Hierarchy::get_n_loops(); }
    const std::vector<std::vector<lGraph::xGraph>>& get_xgraphs(size_t i) const { return Hierarchy::get_xgraphs(i); }
    const std::vector<std::vector<Topology>>& get_topologies(size_t i) const { return Hierarchy::get_topologies(i); }
    const std::map<size_t, std::vector<lGraph::xGraph>>& get_propagator_hierarchy() const { return Hierarchy::get_propagator_hierarchy(); }
    const std::map<size_t, std::vector<Topology>>& get_topology_propagator_hierarchy() const { return Hierarchy::get_topology_propagator_hierarchy(); }
    std::vector<lGraph::xGraph> get_all_xgraphs() const { return Hierarchy::get_all_xgraphs(); }

    ~Pincher();
  private:
    /**
     * Primary tool for construction. Given an input vacuum graph, produces all possible graphs with legs attached to nodes and edges
     */
    void process_input_graph(const lGraph::xGraph&, std::unordered_map<size_t,std::vector<std::set<lGraph::xGraph,xComp>>>&, size_t depth);
    void update_containers() { Hierarchy::update_containers(); }
    void show_graph_counting() const { Hierarchy::show_graph_counting(); }
};

} // namespace AmpEng
} // namespace Caravel

