#include "AmpEng/Parenthood.h"

namespace Caravel {
namespace AmpEng {

using namespace lGraph;

Registro::Registro(const std::vector<lGraph::Propagator>& props, const std::vector<OnShellStrategies::info_OS_map>& lPfromlD,
                   const std::vector<OnShellStrategies::info_OS_map>& lDfromlP): propagators(props), lP_from_lD(lPfromlD), lD_from_lP(lDfromlP)  {
    canonicalize();
}

lGraph::Propagator canonical_propagator_representation(const lGraph::Propagator& p) {
    auto& loops = p.mom.get_internal();
    // decide to make leading loop momentum positive
    if(loops.plus_momenta.size() + loops.minus_momenta.size() == 0) 
        return p;
    else if (loops.plus_momenta.size() + loops.minus_momenta.size() == 1)
        if(loops.plus_momenta.size() == 1)
            return p;
        else
            return {-p.mom, p.mass};
    else if (loops.minus_momenta.size() == 0)
        return p;
    else if (loops.plus_momenta.size() == 0)
        return {-p.mom, p.mass};
    // both loops.plus_momenta.size() and loops.minus_momenta.size() have entries
    else {
        size_t leadplus = *(loops.plus_momenta.begin());
        size_t leadminus = *(loops.minus_momenta.begin());
        if(leadminus < leadplus)
            return {-p.mom, p.mass};
        else
            return p;
    }
}

void canonicalize_propagators(std::vector<lGraph::Propagator>& ps) {
    for(size_t i = 0; i < ps.size(); ++i)
        ps[i] = canonical_propagator_representation(ps[i]);
}

struct {
    bool operator()(const lGraph::Propagator& p, const lGraph::Propagator& r) {
        auto& ploops = p.mom.get_internal();
        auto& rloops = r.mom.get_internal();
        auto& pext = p.mom.get_external();
        auto& rext = r.mom.get_external();
        // mass
        if(p.mass < r.mass)
            return true;
        if(r.mass < p.mass)
            return false;
        // positive loop
        if (std::lexicographical_compare(ploops.plus_momenta.begin(), ploops.plus_momenta.end(), rloops.plus_momenta.begin(), rloops.plus_momenta.end()))
            return true;
        if (std::lexicographical_compare(rloops.plus_momenta.begin(), rloops.plus_momenta.end(), ploops.plus_momenta.begin(), ploops.plus_momenta.end()))
            return false;
        // negative loop
        if (std::lexicographical_compare(ploops.minus_momenta.begin(), ploops.minus_momenta.end(), rloops.minus_momenta.begin(), rloops.minus_momenta.end()))
            return true;
        if (std::lexicographical_compare(rloops.minus_momenta.begin(), rloops.minus_momenta.end(), ploops.minus_momenta.begin(), ploops.minus_momenta.end()))
            return false;
        // positive external
        if (std::lexicographical_compare(pext.plus_momenta.begin(), pext.plus_momenta.end(), rext.plus_momenta.begin(), rext.plus_momenta.end()))
            return true;
        if (std::lexicographical_compare(rext.plus_momenta.begin(), rext.plus_momenta.end(), pext.plus_momenta.begin(), pext.plus_momenta.end()))
            return false;
        // negative external
        if (std::lexicographical_compare(pext.minus_momenta.begin(), pext.minus_momenta.end(), rext.minus_momenta.begin(), rext.minus_momenta.end()))
            return true;
        if (std::lexicographical_compare(rext.minus_momenta.begin(), rext.minus_momenta.end(), pext.minus_momenta.begin(), pext.minus_momenta.end()))
            return false;
        return false;
    }
} compare_props;

void Registro::canonicalize() {
    canonicalize_propagators(propagators);
    std::sort(propagators.begin(), propagators.end(), compare_props);
}

std::ostream& operator<<(std::ostream& o, const Registro& r) {
    o << "Propagators(lP) = " << r.propagators << std::endl;
    o << "lP(lD): " << std::endl;
    print_vector_info_OS_map(r.lP_from_lD, o);
    o << "lD(lP): " << std::endl;
    print_vector_info_OS_map(r.lD_from_lP, o);
    return o;
}

bool operator==(const Registro& r1, const Registro& r2) {
    // will avoid checking inverse map, as it is redundant
    return r1.propagators == r2.propagators && r1.lP_from_lD == r2.lP_from_lD;
}

Parenthood::Parenthood(const xGraph& p, const xGraph& d, const std::vector<Registro>& r) : ancestor(p), descendant(d), relationships(r) {}

Member::Member(const lGraph::xGraph& t): xgraph(t), xg_routing(t) {
    std::hash<Topology> hash_fkt;
    key = hash_fkt(xgraph.get_base_graph());
}

Member::Member(const cutF& c): xgraph(c.get_graph()), hascut(true), name(c.get_short_name()), cut(c), xg_routing(c.get_graph()) {
    std::hash<Topology> hash_fkt;
    key = hash_fkt(xgraph.get_base_graph());
    Caravel::hash_combine(key, cut.get_short_name());
}

bool operator==(const Member& m1, const Member& m2) {
    return m1.get_graph() == m2.get_graph() && m1.get_key() == m2.get_key();
}

std::ostream& operator<<(std::ostream& o, const Member& m) {
    if (m.name != "") o << m.name << std::endl;
    o << m.get_xgraph();
    return o;
}

// we copy parent Hierarchy
Genealogy::Genealogy(const Hierarchy& in, const GraphCutMap& gcm, bool unique, const std::string& m) : Hierarchy(in, gcm, unique), /*unique_insertion(unique),*/ model(m), particlemap(gcm) {
    // sanity check, no index permutation in 'particlemap'
    bool checkpermutation(false);
    for (auto& [index, particle] : particlemap.external)
        if (index != particle->mom_index()) {
            checkpermutation = true;
            break;
        }
    if (checkpermutation) {
        std::cerr << "ERROR: while constructing a Genealogy instance, got a GraphCutMap with a permutation of momentum, and this is not implemented!"
                  << std::endl;
        std::cerr << "	the external map: {";
        for (auto& [index, particle] : particlemap.external) std::cerr << "(" << index << ", " << *particle << "),";
        std::cerr << "}" << std::endl;
        std::exit(1);
    }
    assert(hier_prop.size() > 0);
    size_t minimal = hier_prop.begin()->first;
    size_t maximal = hier_prop.rbegin()->first;

    assert(maximal >= minimal);

    // single-level case
    if(minimal == maximal) {
        for (auto& p : hier_prop[maximal]) {
            auto toadd = book.find(Member(p));
            // no daughters
            if (toadd == book.end()) book[Member(p)] = std::map<size_t, std::unordered_map<Member, Parenthood>>();
        }
    }

    // resolve 1-parenthood
    auto vval = Caravel::verbosity_settings.report_settings;
    Caravel::verbosity_settings.report_settings = false;
    for (size_t ll = maximal; ll > minimal; --ll)
        for (auto& p : hier_prop[ll])
            for (auto& d : hier_prop[ll - 1]) register_1_parenthood(Member(p), Member(d));
    Caravel::verbosity_settings.report_settings = vval;

    // fill up the rest of n-parenthood (n>=2)
    register_all_n_gt_1_parenthoods();
    // invert book
    generate_portal();
    // fill generations and locations
    fill_extra_containers();
    fill_multiplets();
    DEBUG(
        explore();
    );
}

Genealogy::Genealogy(const AmpEng::CutHierarchy& in, const std::string& m) : Hierarchy(in, false), /*unique_insertion(false),*/ model(m) {
    assert(hier_prop.size() > 0);
    size_t minimal = hier_prop.begin()->first;
    size_t maximal = hier_prop.rbegin()->first;

    assert(maximal >= minimal);
    auto& cut_hier = in.get_hierarchy();

    // construct 'particlemap'
    auto cutFhierarchy = in.get_hierarchy();
    auto particlepointers = cutFhierarchy[0][0].get_cutTopology_copy().get_particles_vertices();
    auto& external = particlemap.external;
    for (auto& pp : particlepointers) external[pp->mom_index()] = std::make_shared<Particle>(*pp);

    // single-level case
    if(minimal == maximal) {
        for (auto& c : cut_hier[0]) {
            auto toadd = book.find(Member(c));
            // no daughters
            if (toadd == book.end()) book[Member(c)] = std::map<size_t, std::unordered_map<Member, Parenthood>>();
        }
    }

    // register 1-parenthoods
    auto vval = Caravel::verbosity_settings.report_settings;
    Caravel::verbosity_settings.report_settings = false;
    for (size_t ll = 1; ll < cut_hier.size(); ll++)
        for (size_t d = 0; d < cut_hier[ll].size(); d++) {
            auto& daughter = cut_hier[ll][d];
            auto& parents = daughter.get_parenthoods();
            if (parents.size() > 0) {
                for (size_t p = 0; p < parents[0].size(); p++) {
                    auto& onep = parents[0][p];
                    auto& parent = cut_hier[onep.get_parent_location().first][onep.get_parent_location().second];
                    register_1_parenthood(Member(parent), Member(daughter));
                }
            }
        }
    Caravel::verbosity_settings.report_settings = vval;

    // fill up the rest of n-parenthood (n>=2)
    register_all_n_gt_1_parenthoods();
    // invert book
    generate_portal();
    // fill generations and locations
    fill_extra_containers(in);
    fill_multiplets();
    DEBUG(
        explore();
    );
}

void Genealogy::explore() const {
    using Caravel::operator<<;
    if (generations.size() == 0) {
        std::cout << "Empty Genealogy instance" << std::endl;
        return;
    }
    if ((generations.begin()->second).size() == 0) {
        std::cerr << "ERROR: got a Genealogy instance with empty maximal level!" << std::endl;
        std::exit(1);
    }
    auto& amaximal = (((generations.begin()->second).begin())->second).descendant;
    auto nloops = amaximal.get_n_loops();
    auto maxprops = get_n_of_links(amaximal);
    std::cout << "==========================" << std::endl;
    std::cout << "Genealogy contains " << generations.size() << " levels (for " << (nloops == 1 ? "1-loop" : std::to_string(nloops) + "-loops")
              << " maximals with " << maxprops << " propagators)" << std::endl;
    std::cout << "==========================" << std::endl;
    for (size_t level = 0; level < generations.size() ; level++) {
        auto& diagramsmap = generations.at(level);
        std::cout << "############" << std::endl;
        std::cout << "Now printing diagrams in level " << level << std::endl;
        for (size_t index = 0; index < diagramsmap.size(); index++) { 
            auto& diagram = diagramsmap.at(index);
            std::cout << "Diagram " << index << "/" << diagramsmap.size() << ": " << diagram.descendant.get_short_name() << std::endl; 
        }
        std::cout << "############" << std::endl;
    }
    while (true) {
        int idesiredlevel, idesireddiag;
        std::cout << "Please write level of diagrams to explore (or \"-1\" to quit): ";
        std::cin >> idesiredlevel;
        if (idesiredlevel < 0) break;
        std::cout << "Please write diagram number in level " << idesiredlevel << " to explore (or \"-1\" to quit): ";
        std::cin >> idesireddiag;
        if (idesireddiag < 0) break;
        size_t desiredlevel = size_t(idesiredlevel);
        size_t desireddiag = size_t(idesireddiag);
        if (generations.find(desiredlevel) == generations.end()) {
            std::cout << "WARNING: no level " << desiredlevel << " in hierarchy" << std::endl;
            continue;
        }
        auto& leveldiags = generations.at(desiredlevel);
        if (leveldiags.find(desireddiag) == leveldiags.end()) {
            std::cout << "WARNING: no diagram " << desireddiag << " in level " << desiredlevel << std::endl;
            continue;
        }
        auto& parentstree = leveldiags.at(desireddiag);
        auto& daughter = inv_locations.at(std::make_pair(desiredlevel, desireddiag));
        std::cout << "Details for diagram " << desireddiag << " in level " << desiredlevel << " of the hierarchy: " << std::endl;
        std::cout << daughter;
        std::cout << math_string(daughter.get_xgraph()) << std::endl;
        auto it = book.find(daughter);
        if (it != book.end()) {
            auto& subleadtracker = get_subleading_poles(daughter);
            if (subleadtracker.singularities.size() > 0) {
                std::cout << "============" << std::endl;
                std::cout << "Subleading pole: representative = " << (subleadtracker.representative ? "true" : "false")
                          << "; term: " << wise_enum::to_string(subleadtracker.singularities[0].depth) << std::endl;
            }
        }
        for (size_t level_nparent = 1; level_nparent <= parentstree.parents.size(); level_nparent++) {
            std::cout << "============" << std::endl;
            std::cout << "has " << level_nparent << "-parents: " << std::endl;
            std::cout << "============" << std::endl;
            auto& nparents = parentstree.parents[level_nparent-1];
            for (size_t pp = 0; pp < nparents.size(); pp++) {
                auto& parent = generations.at(nparents[pp].parent.first).at(nparents[pp].parent.second).descendant;
                auto& parenthood = nparents[pp].parenthood;
                std::cout << "Diagram " << pp << ": (in level " << nparents[pp].parent.first << " #  "<< nparents[pp].parent.second 
                          << "): " << parent.get_short_name() << std::endl;
                for (auto& relationship : parenthood.get_relationships()) {
                    std::cout << "	--- subtraction info: dropped props from parent: " << relationship.propagators << "; mappings: ";
                    compact_print_vector_info_OS_map(relationship.lP_from_lD, std::cout, "P", "D");
                    std::cout << std::endl;
                }
            }
        }
    }
}

void Genealogy::generate_portal() {
    if(portal.size()>0) {
        std::cout<<"WARNING: called Genealogy::generate_portal() when 'portal' isn't empty, did nothing..."<<std::endl;
        return;
    }
    for(auto& [parent, leveldaughtersmap]: book)
        for(auto& [level, daughtersmap]: leveldaughtersmap)
            for(auto& [daughter, parenthood]: daughtersmap) {
                add_inverse_relationship(daughter, parent, parenthood.get_relationships());
            }
}

void Genealogy::fill_extra_containers() {
    if (generations.size() > 0 || locations.size() > 0 || inv_locations.size() > 0) {
        std::cerr << "ERROR: Called Genealogy::fill_extra_containers() without empty containers!" << std::endl;
        std::exit(1);
    }
    size_t max_props(0);
    // guess this is safe...
    size_t min_props(10000);
    for(auto& [parent, leveldaughtersmap]: book) {
        auto props = get_n_of_links(parent.get_graph());
        if(max_props < props) max_props = props;
        if(min_props > props) min_props = props;
        // traverse also daughters
        for(auto& [level, daughtersmap]: leveldaughtersmap)
            for(auto& [daughter, parenthood]: daughtersmap) {
                auto dprops = get_n_of_links(daughter.get_graph());
                if(max_props < dprops) max_props = dprops;
                if(min_props > dprops) min_props = dprops;
            }
    }
    DEBUG(
        std::cout<<"Hierarchy has diagrams with minimum "<< min_props <<" and maximum "<< max_props << " propagators" <<std::endl;
    );
    // from maximal to minimal
    std::vector<size_t> levels(max_props-min_props+1, 0);
    for(auto& [level, diagrams]: hier_prop) {
        for(auto& diag: diagrams) {
            auto props = diag.get_n_edges();
            levels[max_props - props]++;
        }
    }
    size_t index(0);
    // fill "locations"
    for(auto iter = hier_prop.rbegin(); iter != hier_prop.rend(); ++iter) {
        auto& level = iter->first;
        auto& diagrams = iter->second;
        for(auto& diag: diagrams) {
            locations[Member(diag)] = {max_props-level, index};
            ++index;
        }
        index=0;
    }
    for(auto& e: locations)
        inv_locations[e.second] = e.first;
  
    // fill "generations"
    index = 0;
    for(auto iter = hier_prop.rbegin(); iter != hier_prop.rend(); ++iter) {
        auto level = max_props - iter->first;
        auto& diagrams = iter->second;
        if(generations.find(level) == generations.end())
            generations[level] = std::map<size_t, Arbol>();
        for(auto& diag: diagrams) {
            // sanity check
            if((generations.at(level)).find(index) != (generations.at(level)).end()) {
                std::cerr<<"ERROR: while filling 'generations' in Genealogy::fill_extra_containers() found a filled entry: {"<<level<<", "<<index<<"}"<< std::endl;
                diag.show(std::cerr);
                std::exit(1);
            }
            // check if no parents
            if(portal.find(Member(diag)) == portal.end()) {
                (generations.at(level))[index] = Arbol{diag.get_base_graph(), std::vector<std::vector<ArbolEntry>>()};
                index++;
                continue;
            }

            Arbol tconst;
            tconst.descendant = diag.get_base_graph();
            const auto& parents = portal.at(Member(diag));
            for(auto& [level_nparent, parenthoodmap]: parents) {
                tconst.parents.push_back(std::vector<ArbolEntry>());
                auto& nparents = tconst.parents.back();
                for(auto& [parent, parenthood]: parenthoodmap) {
                    nparents.push_back(ArbolEntry{locations.at(parent), parenthood});
                }
            }
            (generations.at(level))[index] = tconst;
            index++;
        }
        index=0;
    }
}

void Genealogy::fill_extra_containers(const AmpEng::CutHierarchy& in) {
    if (generations.size() > 0 || locations.size() > 0 || inv_locations.size() > 0) {
        std::cerr << "ERROR: Called Genealogy::fill_extra_containers() without empty containers!" << std::endl;
        std::exit(1);
    }
    auto& chierarchy = in.get_hierarchy();
    for (size_t level = 0; level < chierarchy.size(); level++) {
        for (size_t index = 0; index < chierarchy[level].size(); index++)
            locations[Member(chierarchy[level][index])] = std::make_pair(level, index);
    }
    for(auto& e: locations)
        inv_locations[e.second] = e.first;

    // fill "generations"
    for(size_t level = 0; level < chierarchy.size(); level++) {
        if(generations.find(level) == generations.end())
            generations[level] = std::map<size_t, Arbol>();
        for (size_t index = 0; index < chierarchy[level].size(); index++) {
            auto& cut = chierarchy[level][index];
            auto member = Member(cut);
            // sanity check
            if((generations.at(level)).find(index) != (generations.at(level)).end()) {
                std::cerr<<"ERROR: while filling 'generations' in Genealogy::fill_extra_containers(AmpEng::CutHierarchy&) found a filled entry: {"<<level<<", "<<index<<"}"<< std::endl;
                cut.get_graph().show(std::cerr);
                std::exit(1);
            }
            // check if no parents
            if(portal.find(member) == portal.end()) {
                (generations.at(level))[index] = Arbol{member.get_graph(), std::vector<std::vector<ArbolEntry>>()};
                continue;
            }

            Arbol tconst;
            tconst.descendant = member.get_graph();
            const auto& parents = portal.at(member);
            for(auto& [level_nparent, parenthoodmap]: parents) {
                tconst.parents.push_back(std::vector<ArbolEntry>());
                auto& nparents = tconst.parents.back();
                for(auto& [parent, parenthood]: parenthoodmap) {
                    nparents.push_back(ArbolEntry{locations.at(parent), parenthood});
                }
            }
            (generations.at(level))[index] = tconst;
        }
    }
}

void Genealogy::fill_multiplets() {
    if (multiplets.size() > 0) {
        std::cerr << "ERROR: Called Genealogy::fill_multiplets() without empty container!" << std::endl;
        std::exit(1);
    }
    // fill "multiplets"
    for(auto& member: locations) {
        auto& topo = member.first.get_graph();
        if(multiplets.find(topo) == multiplets.end()) multiplets[topo] = std::vector<Member>();
        multiplets.at(topo).push_back(member.first);
    }
}

void Genealogy::define_subleading_pole_in_book(const Member& m, const SubleadPoleTerm& spt, bool b, const lGraph::Propagator& p) {
    auto it = book.find(m);
    assert(it != book.end());
    auto key = it->first;
    auto map = it->second;
    key.get_subleading_poles().representative = b;
    key.get_subleading_poles().singularities = std::vector<SingleSubleadingPoleTracker>{{p, spt}};
    book.erase(it);
    book[key] = map;
}

void Genealogy::update_containers_from_book() {
    for(auto& [key, map]: book) {
        // update entry in 'portal'
        auto it1 = portal.find(key);
        if (it1 != portal.end()) {
            auto nhp = portal.extract(key);
            nhp.key() = key;
            portal.insert(std::move(nhp));
        }

        // update entry in 'locationsl'
        auto it2 = locations.find(key);
        if (it2 != locations.end()) {
            auto nhl = locations.extract(key);
            nhl.key() = key;
            locations.insert(std::move(nhl));
        }
    }
}

bool Genealogy::get_info_representative(const Member& m) const {
    if (m.get_xgraph().get_n_loops() == 1) { return true; }
    else if (m.get_xgraph().get_n_loops() == 2) {
        auto it = book.find(m);
        if (it == book.end()) {
            std::cerr << "ERROR: called Genealogy::get_info_representative(.) for a member that is not present in book!" << std::endl;
            //m.get_xgraph().show(std::cerr);
            // FIXME: for now let all minimal diagrams to have 'representative'
            return true;
            std::exit(1);
        }
        auto& key = it->first;
        return key.get_subleading_poles().representative;
    }
    std::cerr << "ERROR: called Genealogy::get_info_representative(.) for a " << m.get_xgraph().get_n_loops() << " entry" << std::endl;
    std::exit(1);
}

SubleadPoleTerm Genealogy::get_info_subleading(const Member& m) const {
    if (m.get_xgraph().get_n_loops() == 1) { return SubleadPoleTerm::leading; }
    else if (m.get_xgraph().get_n_loops() == 2) {
        auto it = book.find(m);
        if (it == book.end()) {
            std::cerr << "ERROR: called Genealogy::get_info_subleading(.) for a member that is not present in book!" << std::endl;
            //m.get_xgraph().show(std::cerr);
            // FIXME: for now let all minimal diagrams to be marked 'leading'
            return SubleadPoleTerm::leading;
            std::exit(1);
        }
        auto& key = it->first;
        assert(key.get_subleading_poles().singularities.size() < 2);
        if (key.get_subleading_poles().singularities.size() == 1)
            return key.get_subleading_poles().singularities[0].depth;
        else
            return SubleadPoleTerm::leading;
    }
    std::cerr << "ERROR: called Genealogy::get_info_subleading(.) for a " << m.get_xgraph().get_n_loops() << " entry" << std::endl;
    std::exit(1);
}

const SubleadingPoleTracker& Genealogy::get_subleading_poles(const Member& m) const {
    auto it = book.find(m);
    assert(it != book.end());
    return (it->first).get_subleading_poles();
}

namespace _misc {

struct PropCounter {
    lGraph::Propagator rho;
    size_t multiplicity; /**< Number of propagators in diagram (should be greater than 1 for subleading pole diagrams */
};

std::ostream& operator<<(std::ostream& o, const PropCounter& pc) {
    o << "{" << pc.multiplicity << ", " << pc.rho << "}";
    return o;
}

std::vector<PropCounter> find_repeated_props(const std::vector<lGraph::Propagator>& vps) {
    std::vector<PropCounter> toret;
    for (size_t ii = 0; ii < vps.size(); ii++) {
        auto& lp = vps[ii];
        bool newp(true);
        for (auto& pc : toret) {
            if (pc.rho == lp) {
                newp = false;
                break;
            }
        }
        if (!newp) continue;
        size_t mult(1);
        for (size_t jj = ii + 1; jj < vps.size(); jj++) {
            auto& op = vps[jj];
            if (lp == op) ++mult;
        }
        if (mult > 1) toret.push_back({lp, mult});
    }
    return toret;
}

struct MultiPoleDiagram {
    std::pair<size_t, size_t> member;
    std::vector<PropCounter> propagators;
};

} // namespace _misc

void Genealogy::TMP_handle_subleading_pole_contributions() {
    const auto& maximals = get_propagator_hierarchy().crbegin();

    // few sanity checks for specific case handled so far
    assert(get_propagator_hierarchy().size() == 3);
    assert((maximals->second).size() > 0);
    auto& exg = (maximals->second)[0];
    assert(exg.get_n_loops() == 2);

    std::vector<_misc::MultiPoleDiagram> maximals_multi_pole;
    for (auto& xg : maximals->second) {
        auto pps = xg.get_propagators();
        auto repeated_props = _misc::find_repeated_props(pps);
        if (repeated_props.size() > 0) {
            auto& lloc = get_locations().at(Member(xg));
            maximals_multi_pole.push_back({lloc, repeated_props});
        }
    }

    // FIXME: this is a simple two-loop implementation for maximal cases. Generalize for generic multi-loop cases!
    for (auto& mpd: maximals_multi_pole) {
        // FIXME: sanity 2-loop checks
        assert(mpd.propagators.size() == 1);
        assert(mpd.propagators[0].multiplicity == 2);
        auto& mem = get_inv_locations().at(mpd.member);
        auto& xg = mem.get_xgraph();
        std::vector<lGraph::AccessLink> topinch;
        for(auto& al: xg.list_links()) {
            if(xg.get_propagator(al) == mpd.propagators[0].rho)
                topinch.push_back(al);
        }
        assert(topinch.size() == 2);
        auto xgp1 = xg.pinch(topinch[0]);
        auto memdaught1 = Member(xgp1);
        auto xgp2 = xg.pinch(topinch[1]);
        auto memdaught2 = Member(xgp2);
        
        define_subleading_pole_in_book(mem, SubleadPoleTerm::leading, true, mpd.propagators[0].rho);
        define_subleading_pole_in_book(memdaught1, SubleadPoleTerm::subleading, true, mpd.propagators[0].rho);
        define_subleading_pole_in_book(memdaught2, SubleadPoleTerm::subleading, false, mpd.propagators[0].rho);
    }
    update_containers_from_book();
}

void Genealogy::register_all_n_gt_1_parenthoods() {
    using namespace OnShellStrategies;
    assert(hier_prop.size() > 0);
    size_t minimal = hier_prop.begin()->first;
    size_t maximal = hier_prop.rbegin()->first;

    assert(maximal >= minimal);

    for (size_t ll = maximal; ll > minimal + 1; --ll) {
        // traverse all level-ll diagrams
        std::vector<Member> ascendants;
        for (auto& b : book)
            // find all members in book with ll propagators
            if (b.first.get_xgraph().get_n_edges() == ll) ascendants.push_back(b.first);
        for (auto& asc : ascendants) {
            auto& alllevels = book.at(asc);
            for (size_t mm = ll - 1; mm > minimal; --mm) {
                // sanity check: (ll-mm+1)-daughters should be empty
                assert(alllevels.find(ll - mm + 1) == alllevels.end());
                // traverse all asc (ll-mm)-daughters
                auto pmiddles = alllevels.find(ll - mm);
                if (pmiddles == alllevels.end()) continue;
                auto& middles = alllevels.at(ll - mm);
                for (auto& pmid : middles) {
                    auto& mid = pmid.first;
                    auto& am_parenthood = pmid.second;
                    auto palldescendants = book.find(mid);
                    if (palldescendants == book.end()) continue;
                    auto& alldescendants = book.at(mid);
                    auto pdescendants = alldescendants.find(1);
                    if (pdescendants == alldescendants.end()) continue;
                    // now traverse all 1-daughters of mid
                    auto& descendants = alldescendants.at(1);
                    for (auto& pdesc : descendants) {
                        auto& desc = pdesc.first;
                        auto& md_parenthood = pdesc.second;
                        auto& am_relationships = am_parenthood.get_relationships();
                        auto& md_relationships = md_parenthood.get_relationships();
                        for (auto& am_relationship : am_relationships) {
                            for (auto& md_relationship : md_relationships) {
                                std::vector<AmpEng::Propagator> propagators = am_relationship.propagators;
                                auto& la_from_lm = am_relationship.lP_from_lD;
                                auto& lm_from_la = am_relationship.lD_from_lP;
                                std::vector<MomentumRouting> mrlm_from_la;
                                for (auto& m : lm_from_la) mrlm_from_la.push_back(TransformOSMapMomRouting(m));
                                auto& lm_from_ld = md_relationship.lP_from_lD;
                                std::vector<MomentumRouting> mrla_from_lm;
                                for (auto m : la_from_lm) mrla_from_lm.push_back(TransformOSMapMomRouting(m));
                                std::vector<MomentumRouting> mrlm_from_ld;
                                for (auto m : lm_from_ld) mrlm_from_ld.push_back(TransformOSMapMomRouting(m));
                                auto n = asc.get_xgraph().get_n_legs();
                                auto mrla_from_ld = ComposeMomentumRoutings(n, mrla_from_lm, mrlm_from_ld);
                                std::vector<info_OS_map> la_from_ld;
                                for (auto& mr : mrla_from_ld) la_from_ld.push_back(TransformMomRoutingOSMap(mr));
                                auto ld_from_la = invert_abstract_OnShellPointMap(n, mrla_from_ld);
                                if (md_relationship.propagators.size() != 1) {
                                    std::cerr << "ERROR: 1-parent middle-daughter relationship does not have exactly one factorization channel! "
                                              << md_relationship << std::endl;
                                    std::exit(1);
                                }
                                auto& extra_prop = md_relationship.propagators[0];
                                // add extra prop at the end (with momentum written interms of ancestor)
                                propagators.push_back({ComposeSingleMomRouting(n, extra_prop.mom, mrlm_from_la), extra_prop.mass});
                                // FIXME: should check if book entry exists, then check if new, and if so add!
                                add_relationship(asc, desc, {propagators, la_from_ld, ld_from_la});
                                DEBUG(
                                    std::cout << "Registered:\n PARENT: " << asc << " DAUGHTER: " << desc << " propagators: " << propagators << std::endl;
                                    std::cout << "lP=lP(lD)" << std::endl; print_vector_info_OS_map(la_from_ld); std::cout << "lD=lD(lP)" << std::endl;
                                    print_vector_info_OS_map(ld_from_la);
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}

void Genealogy::add_inverse_relationship(const Member& d, const Member& p, const std::vector<Registro>& vr) {
    size_t nparent_level = p.get_xgraph().get_n_edges() - d.get_xgraph().get_n_edges();

    auto toadd = portal.find(d);
    if (toadd == portal.end()) portal[d] = std::map<size_t, std::unordered_map<Member, Parenthood>>();
    auto& alllevelsparents = portal.at(d);

    auto toadd2 = alllevelsparents.find(nparent_level);
    if (toadd2 == alllevelsparents.end()) alllevelsparents[nparent_level] = std::unordered_map<Member, Parenthood>();
    auto& levelparents = alllevelsparents.at(nparent_level);

    auto toadd3 = levelparents.find(p);
    if (toadd3 == levelparents.end())
        levelparents[p] = Parenthood(d.get_xgraph(), p.get_xgraph(), vr);
    else {
        std::cerr<<"ERROR: called Genealogy::add_inverse_relationship(daughter, parent, vector<relationship>) when there was already a Parenthood!"<<std::endl;
        std::exit(1);
    }
}

Connection<Strand> automorphism_connection_transform(const Connection<Strand>& in, const xGraphPermute& perm) {
    auto& conn = perm.connection;
    auto& permute = perm.strand_permute_or_flip;

    // self connection
    if (conn.first == conn.second) {
        std::vector<Strand> toconst;
        for (size_t ii = 0; ii < in.get_edges().size(); ii++) {
            if (std::find(permute.begin(), permute.end(), ii) == permute.end()) {
                toconst.push_back(in.get_edges()[ii]);
                toconst.back().drop_routings();
            }
            else {
                auto strand = in.get_edges()[ii];
                strand.drop_routings();
                strand.reverse();
                toconst.push_back(strand);
            }
        }
        return Connection<Strand>(toconst);
    }
    else {
        // assert(in.size() == permute.size());
        std::cerr << "automorphism_connection_transform(.) incomplete!" << std::endl;
        std::exit(1);
    }
}

// for now, this just permute strands in the connection perm.connection according to perm.strand_permute_or_flip
xGraph automorphism_transform(const xGraph& xg, const xGraphPermute& perm) {
    auto& conn_map = xg.get_base_graph().get_connection_map();
    std::map<std::pair<size_t, size_t>, Connection<Strand>> newconnections;
    for (auto& [pair, conn] : conn_map) {
        if (pair != perm.connection)
            newconnections[pair] = conn;
        else
            newconnections[pair] = automorphism_connection_transform(conn, perm);
    }
    Graph<Node, Connection<Strand>> lg(xg.get_base_graph().get_nodes(), newconnections);
    return xGraph{lg};
}

void Genealogy::check_1p_register(const Member& parent, const Member& daughter) {
    auto& phoods = book.at(parent).at(1).at(daughter).get_relationships();
    std::vector<size_t> remove;
    for (size_t ii = 0; ii < phoods.size(); ii++) {
        if (settings::AmpEng::spurious_fact_check.value == settings::answer::yes)
            if (check_spurious_fact_channel(parent, daughter, phoods[ii], model, particlemap)) remove.push_back(ii);
    }

    for (size_t ii = remove.size(); ii > 0; --ii) phoods.erase(phoods.begin() + remove[ii - 1]);
    return;
}

void Genealogy::produce_1p_momentum_maps_and_register(const Member& mparent, const xGraph& pinched, const Member& mdaughter, const AmpEng::Propagator& prop_dropped_from_lP, size_t i_automorphism) {
    auto& parent = mparent.get_xgraph();
    auto& daughter = mdaughter.get_xgraph();
    using namespace OnShellStrategies;
    assert(parent.get_n_legs() == pinched.get_n_legs() && pinched.get_n_legs() == daughter.get_n_legs());
    // for now assuming parent has no automorphism
    auto pautomorphisms = parent.get_automorphisms();
    assert(pautomorphisms.size() == 0);
    // the method does not include the identity transformation
    auto automorphisms = daughter.get_automorphisms();
    if (i_automorphism > automorphisms.size()) { 
        // done all factorization channels
        check_1p_register(mparent, mdaughter);
        return;
    }

    auto n = parent.get_n_legs();
    auto pinched_props = pinched.get_propagators();
    auto match_parent_props = pinched_props;
    auto lisPar = parent.get_internal_momenta();
    auto lisPin = pinched.get_internal_momenta();
    std::vector<MomentumRouting> q0s;
    for(auto l: lisPin) q0s.push_back(pinched.get_shift(l.link));
    for(size_t ii = 0; ii<match_parent_props.size(); ++ii) {
        auto& prop = match_parent_props[ii];
        for(size_t ll=1; ll <= lisPin.size(); ++ll) {
            auto plus = prop.mom.get_internal().plus_momenta;
            auto minus = prop.mom.get_internal().minus_momenta;
            if(std::find(plus.begin(), plus.end(), ll) != plus.end())
                prop.mom = add_with_external_mom_conservation(n, prop.mom, q0s[ll-1]);
            else if(std::find(minus.begin(), minus.end(), ll) != minus.end())
                prop.mom = add_with_external_mom_conservation(n, prop.mom, -q0s[ll-1]);
        }
    }
    auto parent_props = parent.get_propagators();
    for(const auto& pp: match_parent_props)
        if(std::find(parent_props.begin(), parent_props.end(), pp) == parent_props.end()) {
            std::cerr<<"ERROR: produce_1p_momentum_maps_and_register(.) could not match the transformed pinched propagator "<<pp<<" from the parent ones: "<<parent_props<<std::endl;
            std::exit(1);
        }

    // lpinched = lpinched(ldaughter)
    auto lpinched_from_lD = abstract_OnShellPointMap(daughter, pinched);
    auto mrlpinched_from_lD = daughter.map_momenta_in_links(pinched.get_internal_momenta());
    // if non-trivial automorphism, re-evaluate lpinched = lpinched(ldaughter)
    if (i_automorphism > 0) {
        auto mapped_daughter = automorphism_transform(daughter, automorphisms[i_automorphism - 1]);
        lpinched_from_lD = abstract_OnShellPointMap(mapped_daughter, pinched);
        mrlpinched_from_lD = mapped_daughter.map_momenta_in_links(pinched.get_internal_momenta());
    }

    // lpinched = lpinched(lparent)
    std::vector<info_OS_map> lpinched_from_lP;
    std::vector<MomentumRouting> mrlpinched_from_lP;
    for(size_t ll = 0; ll < lisPin.size(); ll++) {
        auto pinchedprop = pinched.get_propagator(lisPin[ll].link);
        auto pointer = std::find(pinched_props.begin(), pinched_props.end(), pinchedprop);
        size_t entry = pointer-pinched_props.begin();
        auto pinchedpropmatch = match_parent_props[entry];
        for(size_t kk=0;kk<parent_props.size();kk++)
            if (parent_props[kk] == pinchedpropmatch) {
                if (lisPin[ll].direction) {
                    if (parent_props[kk].mom == pinchedpropmatch.mom) {
                        mrlpinched_from_lP.push_back(parent_props[kk].mom);
                        lpinched_from_lP.push_back(TransformMomRoutingOSMap(parent_props[kk].mom));
                    }
                    else {
                        mrlpinched_from_lP.push_back(-parent_props[kk].mom);
                        lpinched_from_lP.push_back(TransformMomRoutingOSMap(-parent_props[kk].mom));
                    }
                }
                else {
                    if (parent_props[kk].mom == pinchedpropmatch.mom) {
                        mrlpinched_from_lP.push_back(-parent_props[kk].mom);
                        lpinched_from_lP.push_back(TransformMomRoutingOSMap(-parent_props[kk].mom));
                    }
                    else {
                        mrlpinched_from_lP.push_back(parent_props[kk].mom);
                        lpinched_from_lP.push_back(TransformMomRoutingOSMap(parent_props[kk].mom));
                    }
                }
                break;
            }
    }
    auto lP_from_lpinched = invert_abstract_OnShellPointMap(n, mrlpinched_from_lP);
    DEBUG(
        auto lD_from_lpinched = invert_abstract_OnShellPointMap(n, mrlpinched_from_lD);

        std::cout<<"lpinched = lpinched(lD)"<<std::endl;
        print_vector_info_OS_map(lpinched_from_lD);
        std::cout<<"INVERSE lD = lD(lpinched)"<<std::endl;
        print_vector_info_OS_map(lD_from_lpinched);
        std::cout<<"lpinched = lpinched(lP)"<<std::endl;
        print_vector_info_OS_map(lpinched_from_lP);
        std::cout<<"INVERSE lP = lP(lpinched)"<<std::endl;
        print_vector_info_OS_map(lP_from_lpinched);
    
        std::vector<MomentumRouting> mrlD_from_lpinched;
        for(auto m: lD_from_lpinched) mrlD_from_lpinched.push_back(TransformOSMapMomRouting(m));
        auto invert_lpinched_from_lD = invert_abstract_OnShellPointMap(n, mrlD_from_lpinched);
        if(!(lpinched_from_lD == lpinched_from_lD)){
            std::cerr<<"ERROR: inverse of lD(lpinched) did not match lpinched(lD)!"<<std::endl;
            print_vector_info_OS_map(lpinched_from_lD);
            std::exit(1);
        }
    );
    std::vector<MomentumRouting> mrlP_from_lpinched;
    for(auto m: lP_from_lpinched) mrlP_from_lpinched.push_back(TransformOSMapMomRouting(m));
    DEBUG(
        // sanity check!
        auto inver_lpinched_from_lP = invert_abstract_OnShellPointMap(n, mrlP_from_lpinched);
        if(!(inver_lpinched_from_lP == lpinched_from_lP)){
            std::cerr<<"ERROR: inverse of lP(lpinched) did not match lpinched(lP)!"<<std::endl;
            print_vector_info_OS_map(inver_lpinched_from_lP);
            std::exit(1);
        }
    );
    // compose!
    auto mrlP_from_lD = ComposeMomentumRoutings(n, mrlP_from_lpinched, mrlpinched_from_lD);
    std::vector<info_OS_map> lP_from_lD;
    for(auto m: mrlP_from_lD) lP_from_lD.push_back(TransformMomRoutingOSMap(m));
    auto lD_from_lP = invert_abstract_OnShellPointMap(n, mrlP_from_lD);
    DEBUG(
        std::cout<<"lP(lD)"<<std::endl;
        print_vector_info_OS_map(lP_from_lD);
        std::cout<<"lD(lP)"<<std::endl;
        print_vector_info_OS_map(lD_from_lP);
    );
    add_relationship(mparent, mdaughter, {{prop_dropped_from_lP}, lP_from_lD, lD_from_lP});

    // continue with next automorphism
    produce_1p_momentum_maps_and_register(mparent, pinched, mdaughter, prop_dropped_from_lP, ++i_automorphism);
}

void Genealogy::register_1_parenthood(const Member& mparent, const Member& mdaughter) {
    auto& parent = mparent.get_xgraph();
    auto& daughter = mdaughter.get_xgraph();
    // few sanity checks
    if (parent.get_n_loops() != daughter.get_n_loops()) {
        std::cerr << "ERROR: different number of loops in parent-daughter check: " << parent.get_n_loops() << " vs. " << daughter.get_n_loops() << std::endl;
        std::exit(1);
    }
    if (parent.get_n_edges() != daughter.get_n_edges() + 1) {
        std::cerr << "ERROR: parent-daughter relationship should differ by a single propagator: " << parent.get_n_edges() << " vs. ( 1 + "
                  << daughter.get_n_edges() << " )" << std::endl;
        std::exit(1);
    }

    using namespace lGraph;
    static std::unordered_map<std::pair<Topology, Topology>, std::vector<Parenthood>> parenthood_1_container;

    auto it = parenthood_1_container.find( {parent.get_base_graph(), daughter.get_base_graph()} );

    // not in the container, check if it can be added and register
    if(it == parenthood_1_container.end()){
        auto links = parent.list_links();
        for(auto& l: links){
            auto pl = parent.pinch(l);
            if(pl.get_base_graph() == daughter.get_base_graph()) {
                DEBUG(
                    std::cout<<"1-parent register\n PARENT: "<<parent<<" DAUGTHER: "<<daughter<<std::endl;
                    std::cout<<"Parent propagators: "<<parent.get_propagators() <<std::endl;
                    std::cout<<"Daughter propagators: "<<daughter.get_propagators()<< std::endl;
    
                    // propagator written in terms of parent momenta
                    std::cout<<"Propagator dropped: "<<parent.get_propagator(l)<<std::endl;
                );
                produce_1p_momentum_maps_and_register(mparent, pl, mdaughter, parent.get_propagator(l));

                // no need to continue
                break;
            }
        }
    }
    else{
        std::cout<<"ERROR: Should register in *this!"<<std::endl;
        std::exit(1);
    }
}

void Genealogy::add_relationship(const Member& p, const Member& d, const Registro& r) {
    size_t nparent_level = p.get_xgraph().get_n_edges() - d.get_xgraph().get_n_edges();

    auto toadd = book.find(p);
    if (toadd == book.end()) book[p] = std::map<size_t, std::unordered_map<Member, Parenthood>>();
    auto& alllevelsdaughters = book.at(p);

    auto toadd2 = alllevelsdaughters.find(nparent_level);
    if (toadd2 == alllevelsdaughters.end()) alllevelsdaughters[nparent_level] = std::unordered_map<Member, Parenthood>();
    auto& leveldaughters = alllevelsdaughters.at(nparent_level);

    auto toadd3 = leveldaughters.find(d);
    if (toadd3 == leveldaughters.end())
        leveldaughters[d] = Parenthood(p.get_xgraph(), d.get_xgraph(), {r});
    else {
        auto& parenthood = leveldaughters.at(d);
        auto& relationships = parenthood.get_relationships();
        bool isnew(true);
        for (auto registro : relationships) {
            if (r == registro) {
                isnew = false;
                break;
            }
        }
        if (isnew) parenthood.add_relationship(r);
    }
}

Genealogy::~Genealogy() {}

} // namespace AmpEng
} // namespace Caravel

namespace std {
size_t hash<Caravel::AmpEng::Member>::operator()(const Caravel::AmpEng::Member& m) const {
    return m.get_key();
}
} // namespace std
