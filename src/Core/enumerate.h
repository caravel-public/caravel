#pragma once
#include <iterator>

#if __cplusplus <  201703L
#include <utility>
#else
#include <tuple>
#endif

#if __cplusplus <  201703L
namespace detail {
template <typename TIter, typename = std::enable_if_t<std::is_reference<TIter>::value>> struct _enumerate_iterator_helper {
    size_t i;  // will store index
    TIter obj; // will store a reference to the object
};
} // namespace detail
#endif

/**
 * Indexed iteration over an iterable container.
 * In c++14 to be used in ranged for loops like
 *
 * for(auto&& it : enumerate(container)){...}
 *
 * where "it" is a _enumerate_iterator_helper.
 *
 * In c++17 to be used with structured bindings:
 *
 * for(auto [i,thing] : enumerate(container)){...}
 *
 * Taken from
 * http://reedbeta.com/blog/python-like-enumerate-in-cpp17/
 * with a modification to comfortably work in c++14 through a helper
 * struct instead of structured binding.
 *
 * Should even be nearly "zero-overhead" in principle, but no guarantees.
 */
template <typename T, typename TIter = decltype(std::begin(std::declval<T>())), typename = decltype(std::end(std::declval<T>()))>
constexpr auto enumerate(T&& iterable) {
    struct iterator {
        size_t i;
        TIter iter;
        bool operator!=(const iterator& other) const { return iter != other.iter; }
        void operator++() {
            ++i;
            ++iter;
        }
#if __cplusplus <  201703L
        auto operator*() const { return ::detail::_enumerate_iterator_helper<decltype(*iter)&>{i, *iter}; }
#else
        auto operator*() const { return std::tie(i, *iter); }
#endif
    };
    struct iterable_wrapper {
        T iterable;
        auto begin() { return iterator{0, std::begin(iterable)}; }
        auto end() { return iterator{0, std::end(iterable)}; }
    };
    return iterable_wrapper{std::forward<T>(iterable)};
}
