#pragma once

#include "Core/settings.h"
#include "Core/type_traits_extra.h"

#include "FunctionalReconstruction/DensePolynomial.h"
#include "PhaseSpace/PhaseSpaceParameterizations.h"

#include "Core/Particle.h"
#include "Core/Utilities.h"
#include "Core/InputParser.h"

#include <string>


namespace Caravel {

struct ColourStructureInput {
    unsigned Nf_powers{0};
    unsigned Nh_powers{0};
    unsigned Nc_order{0}; //! power in expansion starting from leading power (0)

    bool adjoint_quarks{false};

    // TODO
    // should implement a proper colour structure understanding in the future
    // for now we only deal with  leading color partials which can be identified by the order of external particles

    ColourStructureInput(unsigned set_nf, unsigned set_nh) : Nf_powers{set_nf}, Nh_powers{set_nh} {}
    ColourStructureInput(unsigned set_nf, unsigned set_nh, unsigned set_nc) : Nf_powers{set_nf}, Nh_powers{set_nh}, Nc_order{set_nc} {}
    ColourStructureInput(unsigned set_nf, unsigned set_nh, unsigned set_nc, bool adj)
        : Nf_powers{set_nf}, Nh_powers{set_nh}, Nc_order{set_nc}, adjoint_quarks{adj} {}

    ColourStructureInput(const ColourStructureInput&) = default;
    ColourStructureInput& operator=(const ColourStructureInput&) = default;
    ColourStructureInput(ColourStructureInput&&) = default;

    ColourStructureInput(MathList);

    /**
     * Return a string which identifies the structure uniquely
     * To be e.g. in file names.
     */
    std::string get_id_string() const ;

  private:
    friend class PartialAmplitudeInput;
    ColourStructureInput() = default;
};


  /**
   * Simply counts the number of quarks in the given list of particles.
   */
  unsigned count_quarks(const std::vector<Particle>&);

/**
 * collection of particles as input of internal particles
 */
class InternalParticlesInput {
  private:
    std::vector<Particle> particles;

  public:
    InternalParticlesInput() = delete;

    explicit InternalParticlesInput(decltype(particles) s);
    explicit InternalParticlesInput(decltype(particles) s, ColourStructureInput scsi);

    InternalParticlesInput(MathList);
    explicit InternalParticlesInput(const char* s) : InternalParticlesInput{std::string(s)} {}

    InternalParticlesInput(const InternalParticlesInput&) = default;
    InternalParticlesInput& operator=(const InternalParticlesInput&) = default;
    InternalParticlesInput(InternalParticlesInput&&) = default;

    const decltype(particles)& get_particles() const { return particles; };
    unsigned get_multiplicity() const { return static_cast<unsigned>(particles.size()); };

    unsigned get_number_of_quarks() const;
    unsigned get_number_of_gluons() const;

    /**
     * Get a vector of momentum indices, which correspond to particles (in the same order)
     */
    std::vector<size_t> get_momentum_indices() const;

    /**
     * Return a string which identifies the amplitude uniquely. 
     * To be e.g. in file names.
     */
    std::string get_id_string() const ;

};

  
/**
 * The class which provides the main interface for 1- and 2-loop amplitudes
 * and to other related process-dependent functions.
 *
 * NOTE: for now we "specify" the partial amplitude just by the external order of particles.
 * This is enough for leading colour partials. Late ColourStructureInput class might become
 * more advanced.
 */
class PartialAmplitudeInput {
  private:
    std::vector<Particle> particles;
    ColourStructureInput cs;

  public:
    PartialAmplitudeInput() = delete;

    explicit PartialAmplitudeInput(decltype(particles) s);
    explicit PartialAmplitudeInput(decltype(particles) s, ColourStructureInput scsi);

    //PartialAmplitudeInput(std::initializer_list<Particle> l);
    //PartialAmplitudeInput(std::initializer_list<Particle> l, ColourStructureInput scsi);

    PartialAmplitudeInput(MathList);
    explicit PartialAmplitudeInput(const char* s) : PartialAmplitudeInput{std::string(s)} {}

    PartialAmplitudeInput(const PartialAmplitudeInput&) = default;
    PartialAmplitudeInput& operator=(const PartialAmplitudeInput&) = default;
    PartialAmplitudeInput(PartialAmplitudeInput&&) = default;

    const decltype(particles)& get_particles() const { return particles; };
    unsigned get_multiplicity() const { return static_cast<unsigned>(particles.size()); };

    unsigned get_number_of_quarks() const;
    unsigned get_number_of_light_quarks() const;
    unsigned get_number_of_heavy_quarks() const;
    unsigned get_number_of_gluons() const;
    unsigned get_number_of_photons() const;
    unsigned get_number_of_leptons() const;
    unsigned get_number_of_antileptons() const;
    bool switch_quarks_to_adjoint() const {return cs.adjoint_quarks;}

    unsigned get_nf_powers() const { return cs.Nf_powers; }
    unsigned get_nh_powers() const { return cs.Nh_powers; }
    unsigned get_nc_order() const { return cs.Nc_order; }

    PartialAmplitudeInput get_copy_with_nf_powers_set_to(unsigned) const;
    PartialAmplitudeInput get_copy_only_coloured() const;

    /**
     * Get a vector of momentum indices, which correspond to particles (in the same order)
     */
    std::vector<size_t> get_momentum_indices() const;

    /**
     * Get a count of 3-point vertices, which can be directly mapped to power counting
     * @param at a given loop order
     */
    unsigned count_3_point_vertices(unsigned loop_order = 0) const;

    /**
     * Return a string which identifies the amplitude uniquely. 
     * To be e.g. in file names.
     */
    std::string get_id_string() const ;

  private:
    void init();
    void canonical_order();
    void check_construction();
};

std::ostream& operator<< (std::ostream& , const PartialAmplitudeInput&);

namespace detail{

using couplings_t = std::map<std::string, int>;
/**
 *  Construct an ORDERED parent diagram.
 *  The order is given by particles in input.
 *  TODO: the order is given by the color structure
 *
 *  Assumptions:
 *  - no gluons sandwiched between quarks
 *  - quarks always route in the shortest way
 *  @param for which process
 *  @param output corners
 *  @param output propagators
 */
void construct_ordered_1L_parent_diagram(const PartialAmplitudeInput& input, std::vector<std::vector<Particle>>& external_out, std::vector<Particle>& internal_out, std::vector<couplings_t>&, couplings_t&);

  
#ifdef USE_FINITE_FIELDS
std::vector<BigRat> read_twistor_parameters(MathList);
#endif

}


/**
 * A struct which can grab some miscellaneous stuff from command line.
 * Intended to be used as an input reader.
 */
struct CaravelSettingsInput {
    std::vector<size_t> perm;
    DensePolynomial<int> Ds{{0}, "ep"};
    /**
     * This should basically correspond to available models.
     * Note that we don't actually construct the model here.
     */
    WISE_ENUM_CLASS_MEMBER(ModelType, YM, SM_color_ordered, CubicGravity)
    ModelType model;

    CaravelSettingsInput() = delete;
    CaravelSettingsInput(MathList);
};


} // namespace Caravel
