#pragma once
/**
 * @file Series.h
 *
 * @brief Defines objects representing Laurent series
 *
 * Definition of an object that holds several orders of a Laurent series in
 * a parameter, taken around 0.  The series is of the form
 *    a_{-m}/x^m + a_{-m+1}/x^(m-1) +...+a_n x^n + O(x^(n+1)).
 *
 * Based on the original source: David A. Kosower (from implementation in BlackHat)
 *
 */

#include "Core/typedefs.h"

#ifdef INSTANTIATE_GMP
#include "gmp_r.h"
#endif

#include "Core/MathInterface.h"
#include "Core/MathOutput.h"

#include <cmath>
#include <complex>
#include <stdexcept>
#include <vector>
#include <functional>

namespace Caravel {

// Forward declaration of Series class.
template <class T> class Series;

// --------------------- OPERATORS --------------------------------------------

// --- ADDITION ---

/**
 * Add two Series.
 * @param s1 The first Series of the sum.
 * @param s2 The second Series of the sum.
 * @return   The sum of Series 's1' and 's2'.
 */
template <class T>
Series<T> operator+(const Series<T>& s1, const Series<T>& s2);

/**
 * Add a 0-th power term, i.e. a scalar to the Series.
 *
 * 'v2' will only be added to the series if it contains a 0-th power term.
 *
 * @param s1 The series which the scalar shall be added to.
 * @param v2 The scalar which shall be added to the series.
 * @return The series with the 0-th power coefficient augmented by 'v2'.
 */
template <class T> Series<T> operator+(const Series<T>& s1, const T& v2);

/**
 * Add a 0-th power term, i.e. a scalar to the Series.
 *
 * 'v1' will only be added to the series if it contains a 0-th power term.
 *
 * @param v1 The scalar which shall be added to the series.
 * @param s2 The Series which the scalar shall be added to.
 * @param The Series with the 0-th power augmented by 'v1'.
 */
template <class T> Series<T> operator+(const T& v1, const Series<T>& s2);

// --- SUBTRACTION ----

/**
 * Subtract two Series from each other.
 * @param s1 The first Series in the difference.
 * @param s2 The second Series in the difference.
 * @return   The difference between Series 's1' and 's2'.
 */
template <class T>
Series<T> operator-(const Series<T>& s1, const Series<T>& s2);

/**
 * Subtract a 0-th power term, i.e. a scalar from the Series.
 *
 * 'v2' will only be subtracted from the Series if it contains a 0-th power
 * term.
 *
 * @param s1 The Series which the scalar shall be subtracted from.
 * @param v2 The scalar which shall be subtracted from the Series.
 * @return   The Series 's1' with the 0-th power term subtracted by 'v2'.
 */
template <class T> Series<T> operator-(const Series<T>& s1, const T& v2);

/**
 * Subtract a Series from a 0-th power term, i.e. a scalar.
 *
 * This means that every coefficient in the Series will become its opposite and
 * that the 0-th power term will be the difference between the 'v1' and the
 * initial 0-th power term. If the Series has no 0-th power term 'v1' will have
 * no effect.
 *
 * @param v1 The scalar the Series is subtracted from.
 * @param s2 The Series which is subtracted from the scalar.
 * @return   The Series with negative coefficients where the potential 0-th
 *           power term is the difference between 'v1' and the initial 0-th
 *           power term.
 */
template <class T> Series<T> operator-(const T& v1, const Series<T>& s2);

// --- NEGATIVE ---

/**
 * Take the negative of a Series.
 * @return The Series where every coefficient is replaced by its opposite.
 */
template <class T> Series<T> operator-(const Series<T>& s1);

// --- MULTIPLICATION ---

/**
 * Multiply two Series.
 * @param s1 The first Series in the product.
 * @param s2 The second Series in the product.
 * @return The product of Series 's1' and 's2'.
 */
template <class T>
Series<T> operator*(const Series<T>& s1, const Series<T>& s2);

/**
 * Multiply a Series with a scalar.
 * @param s1 The Series to be multiplied by a number.
 * @param v2 The scalar to multiply the Series.
 * @return   The Series 's1' with all coefficients multiplied by 'v2'.
 */
template <class T> Series<T> operator*(const Series<T>& s1, const T& v2);

/**
 * Multiply a Series with a scalar.
 * @param v1 The scalar to multiply the Series.
 * @param s2 The Series to be multiplied by a number.
 * @return   The Series 's2' with all coefficients multiplied by 'v1'.
 */
template <class T> Series<T> operator*(const T& v1, const Series<T>& s2);

// --- DIVISION ---

/**
 * Divide a Series by a scalar.
 * @param s The series to be divided by the scalar.
 * @param v The scalar dividing the series.
 * @return The Series 's' with all coefficients divided by 'v'.
 */
template <class T> Series<T> operator/(const Series<T>& s, const T& v);

// --- EXPONENTIATION ---

/**
 * Take a series to a certain power.
 * @param s The series to be taken to a certain power.
 * @param p The positive exponent specifying the power to exponentiate the
 *          series.
 * @return The series 's' taken to the power 'p'.
 */
template <class T> Series<T> operator^(const Series<T>& s, unsigned int p);

// --- OSTREAM ---

/**
 * Print the Series to a stream.
 * @param file The stream that the series shall be printed to.
 * @param s    The Series that shall be streamed.
 * @return     A reference to the stream.
 */
template <class T>
std::ostream& operator<<(std::ostream& file, const Series<T>& s);

// -----------------  THE SERIES CLASS ----------------------------------------

/**
 * Class for handling Series'.
 *
 * A series is of the form:
 *
 * \f[
 *      \frac{a_{-m}}{x^m} + \frac{a_{-m+1}}{x^{m-1}} + \dots +
 *      a^{n-1}\cdot x^{n-1} + a^n \cdot x^n
 * \f]
 */
template <class T> class Series {
  protected:
    short int startOrder{}; /**< The order at which the series starts.    */
    short int endOrder{};   /**< The order at which the series ends.      */
    std::vector<T> term{};  /**< Contains the coefficients of the series. */

  public:
    /// Access to the coefficient type.
    typedef T coeff_type;

    // Friend operators
    friend Series<T> operator+<>(const Series<T>&, const Series<T>&);
    friend Series<T> operator+<>(const Series<T>&, const T&);
    friend Series<T> operator+<>(const T& v1, const Series<T>&);
    friend Series<T> operator-<>(const Series<T>&, const Series<T>&);
    friend Series<T> operator-<>(const Series<T>&, const T&);
    friend Series<T> operator-<>(const T&, const Series<T>&);
    friend Series<T> operator-<>(const Series<T>&);
    friend Series<T> operator*<>(const Series<T>&, const Series<T>&);
    friend Series<T> operator*<>(const Series<T>&, const T&);
    friend Series<T> operator*<>(const T&, const Series<T>&);
    friend Series<T> operator/<>(const Series<T>&, const T&);
    friend Series<T> operator^<>(const Series<T>&, unsigned int);
    friend std::ostream& operator<<<>(std::ostream&, const Series<T>&);

    /// Default copy assignment operator.
    Series<T>& operator=(const Series<T>& s) = default;

    /// Default move assignment operator.
    Series<T>& operator=(Series<T>&& s) = default;

    // Compound assignment operators.

    /**
     * Add another series to *this.
     * @param s The Series to be added to *this.
     * @return  The sum of Series 's' and *this.
     */
    Series<T>& operator+=(const Series<T>& s);

    /**
     * Add a scalar to *this.
     *
     * The scalar 'v' will only be added to *this if it contains a 0-th power
     * term.
     *
     * @param v The scalar to be added to *this.
     * @return  The series with the 0-th order term augmented by 'v'.
     */
    Series<T>& operator+=(const T& v);

    /**
     * Subtract a series from *this.
     * @param s The Series to be subtracted from *this.
     * @return  The series subtracted by 's'.
     */
    Series<T>& operator-=(const Series<T>& s);

    /**
     * Subtract a 0-th power term, i.e. a scalar from *this.
     *
     * The scalar is only subtracted if *this has a 0-th power term ohterwise it
     * has no effect.
     *
     * @param v The scalar to be subtracted from *this.
     */
    Series<T>& operator-=(const T& v);

    /**
     * Multiply a another series to *this.
     * @param s The Series to be multiplied to *this.
     * @return  *this multiplied by 's'.
     */
    Series<T>& operator*=(const Series<T>& s);

    /**
     * Multiply *this by a scalar.
     *
     * Every coefficient in the series is multiplied by 'v'.
     *
     * @param v The scalar to multiply the series.
     * @return  *this with every coefficient multiplied by 'v'.
     */
    Series<T>& operator*=(const T& v);

    /**
     * Divide *this by a scalar.
     *
     * Every coefficient in the series is divided by 'v'.
     *
     * @param v The scalar to divide the series.
     * @return  *this with every coefficient divided by 'v'.
     */
    Series<T>& operator/=(const T& v);

    /**
     * Exponentiate the series to a given power.
     * @param p The non-negative exponent that is used to exponentiate the
     *          series.
     * @return  *this to the power 'p'.
     */
    Series<T>& operator^=(unsigned int p);

    // Constructors
    /// Default constructor.
    Series<T>() : term{{T{}}} {};

    /// Default copy constructor.
    Series<T>(const Series<T>& s) = default;

    /// Default move constructor.
    Series<T>(Series<T>&& s) = default;

    /// MathList interface
    Series<T>(const MathList&);

    /**
     * Construct a Series with zero coefficients.
     * @param s The starting power of the series expansion.
     * @param e The ending power of the series expansion.
     */
    Series<T>(int s, int e);

    /**
     * Construct a Series with an arbitrary number of coefficients.
     *
     * At least one coefficient needs to be provided in order to discriminate
     * the constructor from the one taking only the starting and ending power.
     *
     * @param s      The starting power of the series expansion.
     * @param e      The ending power of the series expansion.
     * @param first  The first coefficient (mandatory).
     * @param coeffs Additional coefficients (optional).
     */
    template <typename... Ts>
    Series<T>(short int s, short int e, const T& first, const Ts&... coeffs);

    /**
     * Construct a Series with an arbitary number of coefficients.
     *
     * At least one coefficient needs to be provided in order to discriminate
     * the constructor from the one taking only the starting and ending power.
     * <br>
     * In this case, no exception is thrown if the  number of coefficients does
     * not fit in the provided range between \a s and \a e. As the number
     * suggests, the additional non-fitting terms are just dropped. It is a
     * static method and no constructor since the user should explicitly call
     * this if this behaviour is desired.
     * @param  s      The lowest power of the Series.
     * @param  e      The highest power in the Series.
     * @param  first  The first coefficient.
     * @param  coeffs An arbitrary number of other coefficients.
     * @return        A Series in which possible non-fitting terms are dropped.
     */
    template <typename... Ts>
    static Series<T> drop_excess(short int s, short int e, const T& first, const Ts&... coeffs);

    /**
     * Contruct a Series with an arbitrary number of of coefficients.
     *
     * In this case the coefficients are provided by a vector, i.e. also an
     * initializer list is possible. Note - will crash if v does not
     * provide enough terms.
     *
     * @param s  The starting power of the expansion.
     * @param e  The ending power of the expansion.
     * @param v  The vector (or initializer_list) containing the coefficients of
     *           the expansion.
     */
    Series<T>(int s, int e, const std::vector<T>& v);

    /**
     * Extract term of a given order.
     *
     * Only read access.
     * Throws if the order is not between startOrder and endOrder.
     *
     * @param  order The order of the requested coefficient.
     * @return       The coefficient of term of power 'order' in the expansion.
     */
    T const& operator[](int order) const;
    /**
     * Extract term of a given order.
     *
     * Write access.
     * Throws if the order is not between startOrder and endOrder.
     *
     * @param  order The order of the requested coefficient.
     * @return       The coefficient of term of power 'order' in the expansion.
     */
    T& operator[](int order);
    /**
     * Return term of a given order.
     * 
     * Returns 0 (which is T{}) if order is less than startOrder. 
     * Throws if order is greater than endOrder
     *
     * @param  order The order of the requested coefficient.
     * @return       The coefficient of term of power 'order' in the expansion.
     */
    T operator()(int order) const;

    /**
     * Returns the starting power of the expansion.
     * @return The starting power of the expansion.
     */
    int leading() const { return startOrder; }

    /**
     * Returns the ending power of the expansion.
     * @return The ending power of the expansion.
     */
    int last() const { return endOrder; }

    /**
     * Returns the coefficients in the series expansion in a vector.
     * @return A vector containing the coefficients of the series expansion.
     */
    std::vector<T> get_term() const { return term; };

    /**
     * Multiplies series by the given power of the parameter
     * @return Returns the modified series.
     */

    Series<T> multiply_by_power(int n);

    /**
     * Apply a function to every element of the series.
     * Returns Series<T2> where T2 is the return type of f.
     */
    template <typename TFunc> auto map(TFunc&& f);

    /**
     * Truncate series to given order in epsilon
     */
    void truncate(int max_order);
    
};

template <typename T> bool operator==(const Series<T>& s1, const Series<T>& s2);
template <typename T> inline bool operator!=(const Series<T>& s1, const Series<T>& s2) { return !(s1 == s2); }
template <typename T> bool operator==(const Series<T>&, T);

/**
 * convert (trivially) Series to a different precision
 * can as well be used to cast from BigRat to any floating point type
 */
template <typename Thigh, typename Tlow> Series<Thigh> to_precision(const Series<Tlow>&);

/**
 * Convert elements of a vector to series, if they can be converted and it is not yet a series
 */
template <typename T, template <class...> class Container> std::vector<Series<T>> to_Series(const std::vector<Container<T>>& c, const int highest_power = 4);
template <typename T> std::vector<Series<T>> to_Series(const std::vector<Series<T>>& c){ return c;}

template <typename T> std::string math_string(const Series<T>&);

} // namespace Caravel

#include "Series.hpp"
