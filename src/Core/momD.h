/*
 * momD.h
 * First created on 5.3.2016
 *
 * Headers for D dim momentum class and related classes
 *
 */

#ifndef MOMD_H_
#define MOMD_H_

#include <array>
#include <cmath>
#include <complex>
#include <iostream>
#include <functional>
#include <vector>

#include "Core/Utilities.h"

#include "Core/typedefs.h"
#if INSTANTIATE_GMP
#include "gmp_r.h"
#endif

#include "precision_cast.h"
#include "Core/type_traits_extra.h"

namespace Caravel {

// Pure abstract base class for all momentumD classes
class momentumD_Base {
  public:
    virtual ~momentumD_Base() = 0;
};

template <class T, size_t D> class momentumD;

template <class T, size_t D> inline momentumD<T, D> operator+(momentumD<T, D>, const momentumD<T, D>&);
template <class T, size_t D> inline momentumD<T, D> operator-(momentumD<T, D>, const momentumD<T, D>&);
template <class T, size_t D> inline momentumD<T, D> operator-(momentumD<T, D>);
template <class T, size_t D> inline T operator*(const momentumD<T, D>& mom1, const momentumD<T, D>& mom2);
template <class T, size_t D> inline momentumD<T, D> operator*(const T&, momentumD<T, D>);
template <class T, size_t D> inline momentumD<T, D> operator*(momentumD<T, D>, const T&);
template <class T, size_t D> inline momentumD<T, D> operator/(momentumD<T, D>, const T&);
template <class T, size_t D> std::ostream& operator<<(std::ostream&, const momentumD<T, D>&);
template <typename T, size_t D> std::string math_string(const momentumD<T, D>&);


// Facility to use non unit square epsilon dimensional basis vectors.
template <typename T> struct EpsilonBasis {
    thread_local static T squares[2];
    /**
     * WILD hack related to the ridiculous fact, that thread_local static member apparently doesn't always get initialized,
     * although initialization is literally prescribed a line below the struct declaration.
     * If made not thread_local seems to not behave like this.
     * Should it actually behave like this for some obscure reason??
     * Is this a bug in the compiler???
     * Nobody knows...
     */
    template <typename FF = T> static std::enable_if_t<is_exact_v<FF>> _make_sure_squares_are_initialized() {
        EpsilonBasis<T>::squares[0] = T(1);
        EpsilonBasis<T>::squares[1] = T(1);
    }
    template <typename FF = T> static std::enable_if_t<!is_exact_v<FF>> _make_sure_squares_are_initialized() {}
};

template <typename T> thread_local T EpsilonBasis<T>::squares[2] = {T(1), T(1)};


// Template class for int/real/complex D dimensional momenta
template <class T, size_t D> class momentumD : momentumD_Base {
    static_assert(D >= 2, "Momentum should be at least two-dimensional");

  protected:
    std::array<T, D> components{}; // all values are default initialized to 0
  public:
    typedef typename std::array<T, D>::const_iterator iterator;
    typedef typename std::array<T, D>::value_type value_type;
    friend momentumD<T, D> operator-<>(momentumD<T, D>);
    friend T operator*<>(const momentumD<T, D>&, const momentumD<T, D>&);
    friend std::ostream& operator<<<>(std::ostream&, const momentumD<T, D>&);
    momentumD(){};
    momentumD(const T[]); // Construct by raw memory copy, dangerous
    momentumD(const std::array<T, D>& a) : components(a){};
    momentumD(std::array<T, D>&& a) : components(std::move(a)){};
    // Construct with any number of compomnents (will throw if it is > D)
    momentumD(const std::vector<T>&);
    template <class... Ts> momentumD(const T&, const Ts&... args);

    //! Conversion to a different type only explicit
    template <typename T2, typename std::enable_if_t<!std::is_same<T, T2>::value>* = nullptr> explicit momentumD(const momentumD<T2, D>& momd);
    //! Trivial embedding in higher dimensional momentum is allowed implicitly
    template <size_t D2, USE_IF(D2 < D)> momentumD(const momentumD<T, D2>& momd);
    //! Slicing momentum is only allowed explicitly
    template <size_t D2, USE_IF(D2 > D)> explicit momentumD(const momentumD<T, D2>& momd);

    momentumD<T, D>& operator=(const momentumD<T, D>&) = default;

    inline momentumD<T, D>& operator+=(const momentumD<T, D>&);
    inline momentumD<T, D>& operator-=(const momentumD<T, D>&);
    inline momentumD<T, D>& operator*=(const T&);
    inline momentumD<T, D>& operator/=(const T&);

    // Component access

    //! Get a vector filled with (first n) components of momentumD
    std::vector<T> get_vector(size_t n = D) const;
    T const& operator[](size_t i) const { return components[i]; }
    T const& operator()(size_t i) const { return components[i]; }
    typename std::array<T, D>::const_iterator begin() const { return components.begin(); }
    typename std::array<T, D>::const_iterator end() const { return components.end(); }
    T pi(int) const;

    /**
     * Get a parity-conjugated copy of *this
     */
    momentumD<T,D> get_parity_conjugated() const;

    const std::array<T, D>& get_components() const { return components; }
};

/**
 * Compute product of (Ds-4)-dimensional parts of momenta (including minus from the metric).
 * For finite fields we take into an account modified scalar product.
 */
template <typename T, size_t D> inline std::enable_if_t<(D <= 4), T> compute_Dsm4_prod(const Caravel::momentumD<T, D>& l1, const Caravel::momentumD<T, D>& l2);
template <typename T, size_t D> inline std::enable_if_t<(D > 4), T> compute_Dsm4_prod(const Caravel::momentumD<T, D>& l1, const Caravel::momentumD<T, D>& l2);

/**
 * Get (integer) metric tensor signature. This is NOT used in scalar product definition. It is used
 * in other signature dependent places like states and generation of phase space.
 */
template <size_t i, typename T> inline constexpr std::enable_if_t<i == 0, int> get_metric_signature() { return 1; }
template <size_t i, typename T> inline constexpr std::enable_if_t<is_complex<T>::value && (i > 0), int> get_metric_signature() { return -1; }
template <size_t i, typename T> inline constexpr std::enable_if_t<!is_complex<T>::value && (i > 0) && i % 2 == 0, int> get_metric_signature() { return 1; }
template <size_t i, typename T> inline constexpr std::enable_if_t<!is_complex<T>::value && (i > 0) && i % 2 != 0, int> get_metric_signature() { return -1; }

/**
 * Get (integer) metric tensor signature. Index as a non-template argument.
 */
template <typename T> inline constexpr std::enable_if_t<is_complex<T>::value, int> get_metric_signature(unsigned i) {
    if (i == 0) return 1;
    return -1;
}
template <typename T> inline constexpr std::enable_if_t<!is_complex<T>::value, int> get_metric_signature(unsigned i) {
    if (i % 2 == 0) return 1;
    return -1;
}

//! Precision cast overload
template <typename Thigh, size_t D, typename Tlow, typename = EnableIfFloat<Thigh>>
inline Caravel::momentumD<Thigh, D> to_precision(const Caravel::momentumD<Tlow, D>& in);

// typedefs
template <typename T, size_t D> using OnShellPoint = std::vector<momentumD<T, D>>;

// type alias / alias template
template <typename T, size_t D> using momD = momentumD<T, D>;

template <typename T> using momentum4D = momentumD<T, 4>;
template <typename T> using momentum5D = momentumD<T, 5>;
template <typename T> using momentum6D = momentumD<T, 6>;
template <typename T> using momentum7D = momentumD<T, 7>;
template <typename T> using momentum8D = momentumD<T, 8>;

typedef momentumD<R, 4> mom4D;
typedef momentumD<R, 5> mom5D;
typedef momentumD<R, 6> mom6D;
typedef momentumD<R, 7> mom7D;
typedef momentumD<R, 8> mom8D;

#ifdef HIGH_PRECISION
typedef momentumD<RHP, 4> mom4DHP;
typedef momentumD<RHP, 5> mom5DHP;
typedef momentumD<RHP, 6> mom6DHP;
typedef momentumD<RHP, 7> mom7DHP;
typedef momentumD<RHP, 8> mom8DHP;
#endif

#ifdef VERY_HIGH_PRECISION
typedef momentumD<RVHP, 4> mom4DVHP;
typedef momentumD<RVHP, 5> mom5DVHP;
typedef momentumD<RVHP, 6> mom6DVHP;
typedef momentumD<RVHP, 7> mom7DVHP;
typedef momentumD<RVHP, 8> mom8DVHP;
#endif

typedef momentumD<std::complex<double>, 4> momC4D;
typedef momentumD<std::complex<double>, 5> momC5D;
typedef momentumD<std::complex<double>, 6> momC6D;
typedef momentumD<std::complex<double>, 7> momC7D;
typedef momentumD<std::complex<double>, 8> momC8D;

#ifdef HIGH_PRECISION
typedef momentumD<std::complex<RHP>, 4> momC4DHP;
typedef momentumD<std::complex<RHP>, 5> momC5DHP;
typedef momentumD<std::complex<RHP>, 6> momC6DHP;
typedef momentumD<std::complex<RHP>, 7> momC7DHP;
typedef momentumD<std::complex<RHP>, 8> momC8DHP;
#endif

#ifdef VERY_HIGH_PRECISION
typedef momentumD<std::complex<RVHP>, 4> momC4DVHP;
typedef momentumD<std::complex<RVHP>, 5> momC5DVHP;
typedef momentumD<std::complex<RVHP>, 6> momC6DVHP;
typedef momentumD<std::complex<RVHP>, 7> momC7DVHP;
typedef momentumD<std::complex<RVHP>, 8> momC8DVHP;
#endif

} // namespace Caravel

#include "momD.hpp" // Template code

#endif // MOMD_H_
