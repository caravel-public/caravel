#include "Core/AlgebraicStructures.h"
#include "MathOutput.h"
#include "Debug.h"

#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include <numeric>
#include <complex>

#include <regex>
#include <exception>

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif
#include "Core/typedefs.h"

namespace Caravel{

// [BEGIN NColPower]
NColPower::NColPower(std::string schematic){
    MathList details(schematic, "NColPower");
    pow = std::stoi(details.tail.at(0));
}

NColPower::NColPower(int new_pow) : pow(new_pow) {}

std::ostream& operator <<(std::ostream& out, const NColPower& t){
   out << "N_c^" << t.pow;
   return out;
}
// [/END NColPower]

namespace {
template <typename T> std::pair<T, T> read_rational_number(const MathList& s_in) {
    T num;
    T den = 1;

    assert_head(s_in,"Rat");

    switch (s_in.size()) {
        case 1: {
            char ch = ' ';
            std::stringstream in(s_in[0]);

            in >> num;

            if (in.good()) {
                while ((ch == ' ') && (in)) in.get(ch);
                if (ch == '/') { in >> den; }
                else
                    in.putback(ch);
            }
            if (in.fail()) { throw std::invalid_argument("Cannot iterpret " + math_string(s_in) + " as a ratio of types \"" + _typeid(T)+"\""); }
        } break;
        case 2: {
            std::stringstream num_s(s_in[0]);
            std::stringstream den_s(s_in[1]);
            num_s >> num;
            den_s >> den;
            if (num_s.fail() or den_s.fail()) { throw std::invalid_argument("Cannot iterpret " + math_string(s_in) + " as a ratio of types \"" + _typeid(T)+"\""); }
        } break;
        default: throw std::runtime_error("Unrecongnized Constant format: " + math_string(s_in));
    }

    return {num, den};
}
} // namespace

// [BEGIN Constant]
Constant::Constant(std::string schematic) : val (schematic) {
  using std::stringstream; using std::string;
  MathList com(schematic);

  if( com.head == "Compl" ){
      std::tie(real_num,real_denom) = read_rational_number<int64_t>(com[0]);
      std::tie(imag_num,imag_denom) = read_rational_number<int64_t>(com[1]);
  }
  else if (com.head == "Rat"){
    imag_num = 0;
    imag_denom = 1;
    std::tie(real_num,real_denom) = read_rational_number<int64_t>(com);
  }
  else{
      throw std::runtime_error("Unrecongnized Constant format: "+schematic);
  }
}

#ifdef HIGH_PRECISION
template <> RHP LongRatio<RHP>(int64_t n, int64_t d) { return RHP(std::to_string(n).c_str()) / RHP(std::to_string(d).c_str()); }
#endif
#ifdef VERY_HIGH_PRECISION
template <> RVHP LongRatio<RVHP>(int64_t n, int64_t d) { return RVHP(std::to_string(n).c_str()) / RVHP(std::to_string(d).c_str()); }
#endif
#ifdef INSTANTIATE_GMP
template <> RGMP LongRatio<RGMP>(int64_t n, int64_t d) { return RGMP(std::to_string(n).c_str()) / RGMP(std::to_string(d).c_str()); }
#endif

void Constant::extract_I(){
    std::swap(real_num,imag_num);
    std::swap(real_denom,imag_denom);

    imag_num *= -1;
}

void Constant::negate(){
    real_num *= -1;
    imag_num *= -1;
}

bool Constant::is_imaginary() const {
    if (real_num == 0 and imag_num != 0)
        return true;
    else
        return false;
}
bool Constant::is_real() const {
    if (imag_num == 0)
        return true;
    else
        return false;
}

std::ostream& operator <<(std::ostream& out, const Constant& t){
   out << "(" << t.real_num;
   if (t.real_denom != 1){ out << "/" << t.real_denom; } 
   out << "," << t.imag_num;
   if (t.imag_denom != 1){ out << "/" << t.imag_denom; } 
   out << ")";
   return out;
}
// [/END CONST]
    
// [BEGIN ColourStructure]
ColourStructure::ColourStructure(std::string schematic){
    MathList cs(MathList(schematic,"ColourStructure")[0]);

    using namespace std;

    auto match_color_generators = [this](MathList g) {
        static const regex radj("ADJEX([0-9]+)$");
        static const regex rfun("FUNEX([0-9]+)$");

        if (g.head == "closedFundamental") {
            std::vector<size_t> to_push;
            for (auto& it : g) {
                smatch m;
                if (!regex_match(it, m, radj)) throw std::logic_error("Inconsitency in colour structure");
                to_push.push_back(stoi(m[1].str()) / 2);
            }

            // rotate to our convention
            size_t m = *std::min_element(to_push.begin(), to_push.end());
            while (to_push.front() != m) { std::rotate(to_push.begin(), to_push.begin() + 1, to_push.end()); };
            closed_funamental.push_back(to_push);
        }
        else if (g.head == "openFundamental") {

            std::vector<size_t> adj_ind{};
            std::vector<size_t> fun_ind{};

            for (auto& it : g) {
                smatch m;
                if (regex_match(it, m, radj)) {
                    int i = stoi(m[1].str()) / 2;
                    adj_ind.push_back(i);
                } else if (regex_match(it, m, rfun)) {
                    int i = stoi(m[1].str()) / 2;
                    fun_ind.push_back(i);
                } else {
                    throw std::logic_error("Inconsitency in colour structure");
                }
            }

            assert(fun_ind.size() == 2);

            std::vector<size_t> to_push;

            to_push.push_back(fun_ind.at(0));
            std::move(adj_ind.begin(), adj_ind.end(),back_inserter(to_push));
            to_push.push_back(fun_ind.at(1));

            open_fundamental.push_back(to_push);
        }
        else {
            throw std::logic_error("Unknown colour structure");
        }
    };

    if (cs.head == "Prod") {
        for (auto it : cs) {
            match_color_generators(MathList(it));
        }
    }
    else{
        match_color_generators(cs);
    }

    std::sort(open_fundamental.begin(),open_fundamental.end());
    std::sort(closed_funamental.begin(),closed_funamental.end());

    DEBUG_PRINT(schematic);
    DEBUG_PRINT(open_fundamental);
    DEBUG_PRINT(closed_funamental);

    // check if input makes sense
    //std::vector<size_t> check(external_order);
    //std::sort(check.begin(),check.end());
    //decltype(check) comp(check.size());
    //std::iota(comp.begin(),comp.end(),1);

    //if (check != comp) throw std::logic_error("External order obtained from colour structure doesn't make sense.");
}

std::ostream& operator <<(std::ostream& out, const ColourStructure& t){
   out << "CS[open:" << t.open_fundamental << " closed: " << t.closed_funamental << "]";
   return out;
}
// [/END ColourStructure]

}
