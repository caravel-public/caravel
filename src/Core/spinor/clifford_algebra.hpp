#include "Core/Utilities.h"
#include "spinor.h"
#include <cassert>
#include <complex>
#include <stdexcept>
#include <string>

#include "misc/DeltaZero.h"

#ifdef DEBUG_ON
#include "Core/settings.h"
#endif

namespace Caravel { namespace clifford_algebra{

namespace _internal {
using Caravel::particle::repr_partition;
using Caravel::particle::repr_of_first_spinor;

constexpr int get_sign_from_repr(repr r){
    switch(r){
        case repr::La : return 1;
        case repr::Lat : return -1;
        default: _WARNING_R("logic error"); exit(1);
    }
}

}

template <unsigned D> Gamma<D>::Gamma(Identity* id) : is_zero(false), i(~0) {
    g12=Gamma<D-2>(id);
    g21=Gamma<D-2>(id);
    g11=Gamma<D-2>(id);
    g22=Gamma<D-2>(id);

    se11 = sEntry::One;
    se12 =sEntry::Z;
    se22 = sEntry::One;
    se21 =sEntry::Z;
}

template <unsigned D> Gamma<D>::Gamma(const unsigned ii, bool only_real_components) {
    if(ii>D or ii<4){
        throw std::domain_error("Domain error in Gamma<D="+std::to_string(D)+">("+std::to_string(ii)+") constructor!");
    }

    // generic recursive construction

    // check which if matrix requested is zero
    //if(ii>D){
        //is_zero=true;
        //i=D+1;
        //g11=Gamma<D-2>();
        //g12=Gamma<D-2>();
        //g21=Gamma<D-2>();
        //g22=Gamma<D-2>();
        //se11=sEntry::Z; se12=sEntry::Z; se21=sEntry::Z; se22=sEntry::Z;
        //return;
    //}
    if(ii<D-2){
        is_zero=false;
        i=ii;
        // zero blocks
        g12=Gamma<D-2>();
        g21=Gamma<D-2>();
        se12=sEntry::Z; se21=sEntry::Z;
        // diagonal entries
        g11=Gamma<D-2>(ii,only_real_components);
        g22=Gamma<D-2>(ii,only_real_components);
        se11=sEntry::One; se22=sEntry::One;
        return;
    }
    else if(ii==D-2){
        is_zero=false;
        i=ii;
        // zero blocks
        g11=Gamma<D-2>();
        g22=Gamma<D-2>();
        se11=sEntry::Z; se22=sEntry::Z;
        // diagonal entries
        g12=Gamma<D-2>(D-2,only_real_components);
        g21=Gamma<D-2>(D-2,only_real_components);
        if(only_real_components){
            se12=sEntry::One; se21=sEntry::One;
        }
        else{
            se12=sEntry::I; se21=sEntry::I;
        }
        return;
    }
    else if(ii==D-1){
        is_zero=false;
        i=ii;
        // zero blocks
        g11=Gamma<D-2>();
        g22=Gamma<D-2>();
        se11=sEntry::Z; se22=sEntry::Z;
        // diagonal entries
        g12=Gamma<D-2>(D-2,only_real_components);
        g21=Gamma<D-2>(D-2,only_real_components);
        se12=sEntry::One; se21=sEntry::mOne;
        return;
    }
    else if(ii==D){
        is_zero=false;
        i=D;
        // zero blocks
        g11=Gamma<D-2>(D-2,only_real_components);
        g22=Gamma<D-2>(D-2,only_real_components);
        se11=sEntry::mOne; se22=sEntry::One;
        // diagonal entries
        g12=Gamma<D-2>();
        g21=Gamma<D-2>();
        se12=sEntry::Z; se21=sEntry::Z;
        return;
    }
    // Default
    is_zero=true;
    i=D+1;
    g11=Gamma<D-2>();
    g12=Gamma<D-2>();
    g21=Gamma<D-2>();
    g22=Gamma<D-2>();
    se11=sEntry::Z; se12=sEntry::Z; se21=sEntry::Z; se22=sEntry::Z;
}

template <unsigned D> sEntry Gamma<D>::entry(unsigned i,unsigned j) const {
    if(is_zero) return sEntry::Z;

    unsigned k((i-1)%(Dtt/2)),l((j-1)%(Dtt/2));
    //std::cout<<"(i,j): ("<<i<<","<<j<<") (k,l): ("<<k<<","<<l<<")\n";
    if(i<Dtt/2+1&&j<Dtt/2+1) return sEntry_multiply(se11,g11.entry(k+1,l+1));
    else if(i<Dtt/2+1&&j>Dtt/2) return sEntry_multiply(se12,g12.entry(k+1,l+1));
    else if(i>Dtt/2&&j<Dtt/2+1) return sEntry_multiply(se21,g21.entry(k+1,l+1));
    else return sEntry_multiply(se22,g22.entry(k+1,l+1));
}

namespace _internal{
    template <repr F, mult_from D, typename T,typename TE,USE_IF(F==repr::La && D==mult_from::L)> inline void sm2mult(T& out1, T& out2, const smatrix<TE>& m, const T& in1, const T& in2){
        (out1 -= (m(2,1)*in1)) -= (m(2,2)*in2);
        (out2 += (m(1,1)*in1)) += (m(1,2)*in2);
    }
    template <repr F, mult_from D, typename T,typename TE, USE_IF(F==repr::Lat && D==mult_from::L)> inline void sm2mult(T& out1, T& out2, const smatrix<TE>& m, const T& in1, const T& in2){
        (out1 += (m(1,2)*in1)) += (m(2,2)*in2);
        (out2 -= (m(1,1)*in1)) -= (m(2,1)*in2);
    }
    template <repr F, mult_from D, typename T,typename TE, USE_IF(F==repr::La && D==mult_from::R)> inline void sm2mult(T& out1, T& out2, const smatrix<TE>& m, const T& in1, const T& in2){
        (out1 += (m(2,1)*in1)) += (m(2,2)*in2);
        (out2 -= (m(1,1)*in1)) -= (m(1,2)*in2);
    }
    template <repr F, mult_from D, typename T,typename TE,USE_IF(F==repr::Lat && D==mult_from::R)> inline void sm2mult(T& out1, T& out2, const smatrix<TE>& m, const T& in1, const T& in2){
        (out1 -= (m(1,2)*in1)) -= (m(2,2)*in2);
        (out2 += (m(1,1)*in1)) += (m(2,1)*in2);
    }

    template<std::size_t> struct int_{};

    template <mult_from D, repr F, unsigned Dt,typename T,typename TE> inline void unroll_sm2mult(typename std::vector<T>::iterator out, const smatrix<TE>& m, typename std::vector<T>::const_iterator in, int_<0>){}

    template <mult_from D, repr F, unsigned Dt,typename T,typename TE,size_t Pos,USE_IF(Pos>0)> inline void unroll_sm2mult(typename std::vector<T>::iterator out, const smatrix<TE>& m, typename std::vector<T>::const_iterator in, int_<Pos>){
        _internal::sm2mult<_internal::repr_partition<Dt>{F}.entry(Pos),D,T,TE>(out[2*Pos-2],out[2*Pos-1],m,in[2*Pos-2],in[2*Pos-1]);
        unroll_sm2mult<D,F,Dt,T,TE>(out,m,in,int_<Pos-1>{});
    }


    template <repr F, typename T> inline std::enable_if_t<F==repr::La,T> sp_prod(const T& b10, const T& b11, const T& b20, const T& b21){ return _mult(b20,b11) - _mult(b10,b21); }
    template <repr F, typename T> inline std::enable_if_t<F==repr::Lat,T> sp_prod(const T& b10, const T& b11, const T& b20, const T& b21){ return _mult(b10,b21) - _mult(b20,b11); }

    template <repr F, unsigned Dt,typename T> inline void unroll_sp_prod(typename std::vector<T>::const_iterator b1, typename std::vector<T>::const_iterator b2, T& res, int_<0>){ }
    template <repr F, unsigned Dt,typename T,size_t Pos,USE_IF(Pos!=0)> inline void unroll_sp_prod(typename std::vector<T>::const_iterator b1, typename std::vector<T>::const_iterator b2, T& res, int_<Pos>){
        static_assert(Dt%2 == 0 && Pos>=1 && Pos<=Dt/2,"Domain error");
        res += _internal::sp_prod<_internal::repr_partition<Dt>{F}.entry(Pos),T>(b1[2*Pos-2],b1[2*Pos-1],b2[2*Pos-2],b2[2*Pos-1]);
        unroll_sp_prod<F,Dt,T>(b1,b2,res,int_<Pos-1>{});
    }
    template <repr F, unsigned Dt,typename T,size_t Pos> inline void unroll_sp_prod(const std::vector<T>& b1, const std::vector<T>& b2, T& res, int_<Pos>){
        unroll_sp_prod<F,Dt,T>(b1.begin(),b2.begin(),res,int_<Pos>{});
    }

    //! this is only for testing
    template <mult_from D,repr F,size_t Ds, typename T> std::vector<T> multiply_4D_sparse_smatrix_Weyl(const Caravel::momentumD<T,Ds>& l, const std::vector<T>& in){
        static constexpr unsigned Dt = two_to_the(Ds/2-1);
        static_assert(Dt>=2,"Representation error");

        assert(in.size()==Dt);

        //static constexpr _internal::repr_partition<Dt/2> part(F);

        std::vector<T> out(Dt,T(0));
        out.shrink_to_fit();

        smatrix<T> m(l);

        _internal::unroll_sm2mult<D,F,Dt/2,T>(out.begin(),m,in.begin(),_internal::int_<Dt/2>{});

        //// sm*la
        //for(unsigned i: ind_part.template get_t1<static_cast<bool>(F)>()){
        //out[2*i]    =  -m(2,1)*in[2*i+0] - m(2,2)*in[2*i+1]; 
        //out[2*i+1]  = m(1,1)*in[2*i+0] + m(1,2)*in[2*i+1];
        //}
        //// sm*lat
        //for(unsigned i: ind_part.template get_t2<static_cast<bool>(F)>()){
        //out[2*i] = in[2*i]*m(1,2) + in[2*i+1]*m(2,2);
        //out[2*i+1] = -in[2*i+0]*m(1,1) - in[2*i+1]*m(2,1);
        //}

        return out;
    }

    //! this is only for testing
    template <mult_from D,repr F, size_t Ds, typename T> std::vector<T> multiply_Dtt_sparse_smatrix_Weyl(const Caravel::momentumD<T,Ds>& l, const std::vector<T>& in){
        static constexpr unsigned Dt = two_to_the(Ds/2)/2;
        assert(in.size()==Dt);

        static constexpr _internal::repr_partition<Dt/2> part(F);

        std::vector<T> out(Dt,T(0));
        out.shrink_to_fit();

        using std::get;

        for(int mu=0;mu<=static_cast<int>(Ds)-5;mu++){
            const auto& vals = sparse_smatrix<Ds,T,!(static_cast<bool>(D))>::values[mu];
            for(unsigned i=0;i< Dt/2; i++){
                T t{l[mu+4]};
                t*=get<2>(vals[i]);
                t*=T(_internal::get_sign_from_repr(part.entry(i+1)));

                out[2*i] -= t*in[2*get<static_cast<size_t>(D)>(vals[i])];
                out[2*i+1] -= t*in[2*get<static_cast<size_t>(D)>(vals[i])+1];
            }
        }

        return out;
    }
}

template <mult_from D,repr F,size_t Ds, typename T, typename lT> std::vector<T> multiply_sparse_smatrix_Weyl(const lT& l, const std::vector<T>& in){
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    assert(in.size()==Dt);
    std::vector<T> out(Dt,T(0));
    out.shrink_to_fit();
    multiply_sparse_smatrix_Weyl_impl<D,F,Ds,T,lT>(l, in.begin(),out.begin());
    return out;
}

namespace _internal{
    template <typename T,typename InputIt> inline smatrix<T> get_smatrix(InputIt l){return smatrix<T>(Caravel::momentum4D<T>(l[0],l[1],l[2],l[3]));}

    template <mult_from D,repr F,size_t Ds, size_t Dt, typename TE,typename lIt,typename InputIt,typename OutputIt> inline void
    unroll_gammamu_Ds(lIt, InputIt, OutputIt, int_<Ds>){}

    template <mult_from D,repr F,size_t Ds, size_t Dt, typename TE,typename lIt,typename InputIt,typename OutputIt,size_t mu> inline
    std::enable_if_t<(mu<Ds) && is_extended<typename lIt::value_type>::value>
    unroll_gammamu_Ds(lIt l, InputIt in, OutputIt out, int_<mu>){

        static constexpr _internal::repr_partition<Dt/2> part(F);
        using std::get;

        const auto& vals = sparse_smatrix<Ds,TE,!(static_cast<bool>(D))>::values[mu-4];

        for(unsigned i=0;i< Dt/2; i++){
            auto t = l[mu]*(TE((_internal::get_sign_from_repr(part.entry(i+1)))*get_metric_signature<mu,TE>())*get<2>(vals[i]));

            out[2*i] += (in[2*get<static_cast<size_t>(D)>(vals[i])]*t);
            out[2*i+1] +=(in[2*get<static_cast<size_t>(D)>(vals[i])+1]*t);
        }

        unroll_gammamu_Ds<D,F,Ds,Dt,TE>(l, in, out,int_<mu+1>());
    }

    template <mult_from D,repr F,size_t Ds, size_t Dt, typename TE,typename lIt,typename InputIt,typename OutputIt,size_t mu> inline
    std::enable_if_t<(mu<Ds) && !is_extended<typename lIt::value_type>::value>
    unroll_gammamu_Ds(lIt l, InputIt in, OutputIt out, int_<mu>){
        static constexpr _internal::repr_partition<Dt/2> part(F);
        using std::get;

#ifdef DEBUG_ON
        if (is_extended_v<typename InputIt::value_type>){
            _WARNING_R("Reached broken code!");
            std::exit(1);
        }
#endif

        const auto& vals = sparse_smatrix<Ds,TE,!(static_cast<bool>(D))>::values[mu-4];

        for(unsigned i=0;i< Dt/2; i++){
            auto t = l[mu];
            t*=get<2>(vals[i]);
            t*=TE((_internal::get_sign_from_repr(part.entry(i+1)))*get_metric_signature<mu,TE>());

            out[2*i] += (in[2*get<static_cast<size_t>(D)>(vals[i])]*t);
            out[2*i+1] += (in[2*get<static_cast<size_t>(D)>(vals[i])+1]*t);
        }

        unroll_gammamu_Ds<D,F,Ds,Dt,TE>(l, in, out,int_<mu+1>());
    }

}



template <mult_from D,repr F,size_t Ds, typename T,typename lT> void multiply_sparse_smatrix_Weyl_impl(const lT& l, typename std::vector<T>::const_iterator in, typename std::vector<T>::iterator out){
    static_assert(Ds>=4,"Domain error");
    static constexpr unsigned Dt = two_to_the(Ds/2-1);

    typedef typename infer_unext_type<T>::type TE;

    // We split the action of the smatix in two parts:
    // 1) sparse elements from Dtt-matrices, which are contained in sparse_smatrix<Ds,T>
    // 2) 2x2 same smatrix blocks on the diagonal (dense)
    //
    // By doing this we never multiply any zero elements. And everything we need is constructed at compile time!

    // 1)
    _internal::unroll_gammamu_Ds<D,F,Ds,Dt,TE>(l, in, out,_internal::int_<4>());

    // 2)
    // here we need to correctly choose a representation (at compile time instead of at run time), so we unroll the loop manually
    // IMPORTANT: i am not sure if this actually hurts or improves performance!!
    _internal::unroll_sm2mult<D,F,Dt/2,T>(out,_internal::get_smatrix<typename lT::value_type>(l.begin()),in,_internal::int_<Dt/2>{});
}


namespace _internal{
    // F is the right spinor repr
    template <repr F, typename T> inline std::enable_if_t<!is_extended<T>::value && F==repr::La && is_complex<T>::value> to_vector(typename std::vector<T>::iterator v, T b10, T b11, const T& b20, const T& b21){
        T inv2=T(1)/T(2);
        T A11= (b10*=inv2)*b20;
        b10 *= b21;
        T A21= (b11*=inv2)*b20;
        b11 *= b21;

        (v[0] += b11) += A11;
        (v[1] += A21) += b10;
        (v[2] += (b10*=T(0,-1))) -= (A21*= T(0,-1));
        (v[3] += A11) -= b11;
    }
    template <repr F, typename T, typename TE = typename T::element_type> inline std::enable_if_t<F==repr::La && is_complex<T>::value> to_vector(typename std::vector<T>::iterator v, T b10, T b11, const T& b20, const T& b21){
        TE inv2=TE(1)/TE(2);
        T A11= _mult((b10.template multeq_by<0>(inv2)),b20);
        T b1021 = _mult(b10 , b21);
        T A21= _mult((b11.template multeq_by<0>(inv2)),b20);
        T b1121 = _mult(b11 , b21);

        (v[0] += b1121) += A11;
        (v[1] += A21) += b1021;
        (v[2] += b1021.template multeq_by<0>(TE(0,-1))) -= A21.template multeq_by<0>(TE(0,-1));
        (v[3] += A11) -= b1121;
    }
    template <repr F, typename T> inline std::enable_if_t<!is_extended<T>::value && F==repr::La && !is_complex<T>::value> to_vector(typename std::vector<T>::iterator v, T b10, T b11, const T& b20, const T& b21){
        T inv2=T(1)/T(2);
        T A11= (b10*=inv2)*b20;
        b10 *= b21;
        T A21= (b11*=inv2)*b20;
        b11 *= b21;

        (v[0] += b11) += A11;
        (v[1] += A21) += b10;
        (v[2] -= b10) += A21;
        (v[3] += A11) -= b11;
    }
    template <repr F, typename T, typename TE = typename T::element_type> inline std::enable_if_t<F==repr::La && !is_complex<T>::value> to_vector(typename std::vector<T>::iterator v, T b10, T b11, const T& b20, const T& b21){
        TE inv2=TE(1)/TE(2);
        T A11= _mult((b10.template multeq_by<0>(inv2)),b20);
        T b1021 = _mult(b10 , b21);
        T A21= _mult((b11.template multeq_by<0>(inv2)),b20);
        T b1121 = _mult(b11 , b21);

        (v[0] += b1121) += A11;
        (v[1] += A21) += b1021;
        (v[2] -= b1021) += A21;
        (v[3] += A11) -= b1121;
    }

    template <repr F, typename T, USE_IF(F==repr::Lat)> inline void to_vector(typename std::vector<T>::iterator v, const T& b10, const T& b11, const T& b20, const T& b21){ to_vector<flip(F)>(v,b20,b21,b10,b11); }

    template <repr F, unsigned Dt,typename T> inline void unroll_to_vector(typename std::vector<T>::const_iterator b1, typename std::vector<T>::const_iterator b2, typename std::vector<T>::iterator res, int_<0>){ }
    template <repr F, unsigned Dt,typename T,size_t Pos, USE_IF(Pos!=0)> inline void unroll_to_vector(typename std::vector<T>::const_iterator b1, typename std::vector<T>::const_iterator b2, typename std::vector<T>::iterator res, int_<Pos>){
        static_assert(Dt%2 == 0 && Pos>=1 && Pos<=Dt/2,"Domain error");
        _internal::to_vector<_internal::repr_partition<Dt>{F}.entry(Pos),T>(res,b1[2*Pos-2],b1[2*Pos-1],b2[2*Pos-2],b2[2*Pos-1]);
        unroll_to_vector<F,Dt,T>(b1,b2,res,int_<Pos-1>{});
    }
}

template <size_t Ds, typename T> std::vector<T> LvAB_Ds(const std::vector<T>& A, const std::vector<T>& B){
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    assert(A.size()==Dt);
    assert(B.size()==Dt);

    std::vector<T> out(Ds,T(0));
    out.shrink_to_fit();
    LvAB_Ds_impl<Ds,T>(A.begin(),B.begin(),out.begin());
    return out;
}
template <size_t Ds, typename T> std::vector<T> LvBA_Ds(const std::vector<T>& A, const std::vector<T>& B){
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    assert(A.size()==Dt);
    assert(B.size()==Dt);

    std::vector<T> out(Ds,T(0));
    out.shrink_to_fit();
    LvBA_Ds_impl<Ds,T>(A.begin(),B.begin(),out.begin());
    return out;
}

template <size_t Ds, typename T> void LvAB_Ds_impl(typename std::vector<T>::const_iterator A, typename std::vector<T>::const_iterator B, typename std::vector<T>::iterator out){
    static_assert(Ds>=4,"Domain error");
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    typedef typename infer_unext_type<T>::type TE;

    static constexpr _internal::repr_partition<Dt/2> part(repr::Lat);

    using std::get;

    // 1)
    for(int mu=0;mu<=static_cast<int>(Ds)-5;mu++){
        const auto& vals = sparse_smatrix<Ds,TE,!(static_cast<bool>(mult_from::L))>::values[mu];
        std::vector<T> temp(Dt);
        temp.shrink_to_fit();
        for(unsigned i=0;i< Dt/2; i++){
            TE t(get<2>(vals[i]));
            t*=TE(_internal::get_sign_from_repr(part.entry(i+1)));
            t/=TE(2);

            const auto ind = 2*get<static_cast<size_t>(mult_from::L)>(vals[i]);

            temp[2*i] = _mult_by(t,B[ind]);
            temp[2*i+1] = _mult_by(t,B[ind+1]);
        }
        _internal::unroll_sp_prod<repr::La,Dt,T>(A,temp.begin(),out[mu+4],_internal::int_<Dt/2>{});
        //out[mu+4] = sp_prod<flip(part.entry(i+1))>(A[2*i],A[2*i+1],t*B[2*ind],t*=B[2*ind+1]);
    }

    // 2)
    _internal::unroll_to_vector<repr::Lat,Dt,T>(A,B,out,_internal::int_<Dt/2>{});

}

template <size_t Ds, typename T> void LvBA_Ds_impl(typename std::vector<T>::const_iterator A, typename std::vector<T>::const_iterator B, typename std::vector<T>::iterator out){
    static_assert(Ds>=4,"Domain error");
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    typedef typename infer_unext_type<T>::type TE;

    static constexpr _internal::repr_partition<Dt/2> part(repr::La);

    using std::get;

    // 1)
    for(int mu=0;mu<=static_cast<int>(Ds)-5;mu++){
        const auto& vals = sparse_smatrix<Ds,TE,!(static_cast<bool>(mult_from::L))>::values[mu];
        std::vector<T> temp(Dt);
        temp.shrink_to_fit();
        for(unsigned i=0;i< Dt/2; i++){
            TE t(get<2>(vals[i]));
            t*=TE(_internal::get_sign_from_repr(part.entry(i+1)));
            t/=TE(2);

            const auto ind = 2*get<static_cast<size_t>(mult_from::L)>(vals[i]);

            temp[2*i] = _mult_by(t,B[ind]);
            temp[2*i+1] = _mult_by(t,B[ind+1]);
        }
        _internal::unroll_sp_prod<repr::Lat,Dt,T>(A,temp.begin(),out[mu+4],_internal::int_<Dt/2>{});
        //out[mu+4] = sp_prod<flip(part.entry(i+1))>(A[2*i],A[2*i+1],t*B[2*ind],t*=B[2*ind+1]);
    }

    // 2)
    _internal::unroll_to_vector<repr::La,Dt,T>(A,B,out,_internal::int_<Dt/2>{});

}

namespace _internal{

template <typename T> bool is_small(const T& x) {
    using std::abs;
    if constexpr (is_floating_point<T>::value && is_complex<T>::value)
        return (abs(x) < DeltaZeroSqr<typename T::value_type>());
    else if constexpr (is_floating_point<T>::value && !is_complex<T>::value)
        return (abs(x) < DeltaZeroSqr<T>());
    else
        return x == T(0);
}

    template <typename T> momentum4D<T> get_reference_vector(size_t i) {
        if constexpr (is_complex_v<T>) {
            switch (i) {
                case 1: return {T(1), T(0), T(1), T(0)};
                case 2: return {T(1), T(0), T(0), T(1)};
                case 3: return {T(1), T(-1), T(0), T(0)};
                case 4: return {T(1), T(0), T(-1), T(0)};
                case 5: return {T(1), T(0), T(0), T(-1)};
            }
        }
        else if (!is_complex_v<T>) {
            switch (i) {
                case 1: return {T(1), T(0), T(0), T(1)};
                case 2: return {T(1), T(1), T(1), T(1)};
                case 3: return {T(1), T(-1), T(0), T(0)};
                case 4: return {T(1), T(0), T(0), T(-1)};
                case 5: return {T(2), T(1), T(1), T(-2)};
            }
        }
        return {T(1), T(1), T(0), T(0)};
    }

    template <typename T, size_t Ds> inline Caravel::momentumD<T, 4> lightcone_decompose_cycle_refs(const Caravel::momentumD<T, Ds>& l) {
        static_assert(Ds >= 5, "Domain error");

        const auto l4 = static_cast<momentumD<T, 4>>(l);
        const T ltwsq(compute_Dsm4_prod(l, l));

        momentumD<T, 4> lq;

        for (size_t i = 1; i <= 5; ++i) {
            lq = get_reference_vector<T>(i);
            T prod_lq = l4 * lq;
            if (is_small(prod_lq)) {
#ifdef DEBUG_ON
                _WARNING_R("Reference vector ", lq, " resulted in small (l,q)!");
#endif //  DEBUG_ON
                continue;
            }
            prod_lq *= T(2);

            lq *= (ltwsq / prod_lq);
            lq += l4;

            return lq;
        }

        return lq;
    }

    template <typename T, size_t Ds> inline Caravel::momentumD<T,4> lightcone_decompose(const Caravel::momentumD<T,Ds>& l){
        static_assert(Ds>=5,"Domain error");

        // default reference momentum (1,1,0,0),
        T prod_lq = l[0]-l[1];

        // if the scalar product is small (or zero) try other references
        if (is_small(prod_lq)){
#ifdef DEBUG_ON
            _WARNING_R("Default reference vector resulted in small (l,q)! Switching reference.");
#endif //  DEBUG_ON
            return lightcone_decompose_cycle_refs(l);
        }

        prod_lq *= T(2);

        // l = l4 + ltw;  l4^2 = - ltw^2
        T ltwsq(compute_Dsm4_prod(l,l));

        ltwsq/=prod_lq;

        return Caravel::momentumD<T,4>(l[0]+ltwsq,l[1]+ltwsq,l[2],l[3]);
    }

    // for compatibility with Ds=4 (we assume momenta are in fact on-shell)
    template <typename T> inline Caravel::momentumD<T,4> lightcone_decompose(const Caravel::momentumD<T,4>& l){ return l;}

}


template <repr R, unsigned Dtt_index, typename T,size_t Ds, typename TE> std::vector<T> cut_WF_q(const Caravel::momentumD<TE,Ds>& l){
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    static_assert(Ds>=4,"Domain error");
    static_assert(Dtt_index>=1,"Domain error");
    static_assert(Dtt_index<=Dt,"Domain error");

    static constexpr _internal::repr_partition<Dt/2> part(flip(R));

    typedef typename sp_type<part.entry(Dtt_index),TE>::type sp;

    auto lfl(_internal::lightcone_decompose(l));

    sp s(lfl);

    s/=(TE(2)*(Caravel::momentumD<TE,4>(l[0],l[1],l[2],l[3])*lfl));

#ifdef DEBUG_ON
        if (_internal::is_small(Caravel::momentumD<TE,4>(l[0],l[1],l[2],l[3])*lfl)){
            _WARNING_R("Very small (l,lfl4d)");
        }
#endif

    std::vector<T> out(Dt,T(0));
    out.shrink_to_fit();

    using std::get;

    auto l_ext = to_extended_t<std::array<T,Ds>,Ds>(l.get_components());

    // only transform non-zero entry
    for(int mu=0;mu<=static_cast<int>(Ds)-5;mu++){
        const auto& vals = sparse_smatrix<Ds,TE,!(static_cast<bool>(mult_from::R))>::values[mu];
        for(unsigned i=0;i< Dt/2; i++){
            const unsigned ii = get<static_cast<size_t>(mult_from::R)>(vals[i]);
            if(ii != Dtt_index-1) continue;

            T t{l_ext[mu+4]};
            t*=get<2>(vals[i]);
            t*=TE((_internal::get_sign_from_repr(part.entry(i+1)))*get_metric_signature<TE>(mu+4));

            out[2 * i] += s[0] * t;
            out[2 * i + 1] += s[1] * t;
        }
    }

    // 2)
    _internal::sm2mult<part.entry(Dtt_index),mult_from::R>(out[2*Dtt_index-2],out[2*Dtt_index-1],smatrix<TE>(l),static_cast<T>(s[0]),static_cast<T>(s[1]));

    return out;
}


template <repr R, unsigned Dtt_index, typename T,size_t Ds, typename TE> std::vector<T> cut_WF_qb(const Caravel::momentumD<TE,Ds>& ml){
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    static_assert(Dtt_index>=1,"Domain error");
    static_assert(Dtt_index<=Dt,"Domain error");

    static constexpr _internal::repr_partition<Dt/2> part(flip(R));

    typedef typename sp_type<part.entry(Dtt_index),TE>::type sp;

    auto l(-ml);

    auto lfl(_internal::lightcone_decompose(l));

    sp s(lfl);

    std::vector<T> out(Dt,T(0));
    out.shrink_to_fit();

    using std::get;

    auto l_ext = to_extended_t<std::array<T,Ds>,Ds>(l.get_components());

    // only transform non-zero entry
    for(int mu=0;mu<=static_cast<int>(Ds)-5;mu++){
        const auto& vals = sparse_smatrix<Ds,TE,!(static_cast<bool>(mult_from::L))>::values[mu];
        for(unsigned i=0;i< Dt/2; i++){
            const unsigned ii = get<static_cast<size_t>(mult_from::L)>(vals[i]);
            if(ii != Dtt_index-1) continue;

            T t{l_ext[mu+4]};
            t*=get<2>(vals[i]);
            t*=TE((_internal::get_sign_from_repr(part.entry(i+1)))*get_metric_signature<TE>(mu+4));

            out[2 * i] += s[0] * t;
            out[2 * i + 1] += s[1] * t;
        }
    }

    // 2)
    _internal::sm2mult<part.entry(Dtt_index),mult_from::L>(out[2*Dtt_index-2],out[2*Dtt_index-1],smatrix<TE>(l),static_cast<T>(s[0]),static_cast<T>(s[1]));

    return out;
}

template <unsigned D> std::ostream& operator<<(std::ostream& o, const Caravel::clifford_algebra::Gamma<D>& g){
    const unsigned& limit = Caravel::clifford_algebra::Gamma<D>::Dtt;
    for(unsigned ii=1;ii<=limit;ii++){
        o<<"\t";
        for(unsigned jj=1;jj<=limit;jj++) o<<(g.entry(ii,jj))<<" ";
        o<<"\n";
    }
    return o;
}

template <unsigned Ds, typename T,bool t> std::ostream& operator<<(std::ostream& s, const Caravel::clifford_algebra::sparse_smatrix<Ds,T,t>& sm){
    s<<"sparse_smatrix<"<<Ds<<">{\n";
    for(unsigned i=0; i<sm.values.size();i++){
        s<<"[mu="<<i+4<<"] "<<sm.values.at(i)<<"\n";
    }
    return s<<"}";
}

template <unsigned Ds, typename T, bool transpose> void sparse_smatrix<Ds, T, transpose>::init_impl() {
    using namespace Caravel::clifford_algebra;
    for (int mu = 0; mu <= static_cast<int>(Dsm4) - 1; mu++) {
        const Gamma<Ds> g(mu + 4, !is_complex<T>::value);
        values.at(mu).reserve(Dt / 4);
        values[mu].shrink_to_fit();
        for (unsigned i = 1; i <= g.Dtt; i++) {
            for (unsigned j = 1; j <= g.Dtt; j++) {
                if (g.entry(i, j) != sEntry::Z) { values[mu].emplace_back(i - 1, j - 1, explicit_sEntry<T>(g.entry(i, j))); }
            }
        }
        using std::get;
        if (transpose) {
            std::sort(values[mu].begin(), values[mu].end(), [](const auto x1, const auto x2) { return get<1>(x2) > get<1>(x1); });
        }
    }

    DEBUG_MESSAGE("construced ", _typeid(T), " Ds=", Ds, " transpose:", transpose);
}

template <unsigned Ds, typename T, bool transpose>
template <typename TT>
std::enable_if_t<is_exact<TT>::value> sparse_smatrix<Ds, T, transpose>::update_values() {
    using namespace Caravel::clifford_algebra;
    for (int mu = 0; mu <= static_cast<int>(Dsm4) - 1; mu++) {
        const Gamma<Ds> g(mu + 4, !is_complex<T>::value);
        for (auto& it : values.at(mu)) {
            using std::get;
            get<2>(it) = explicit_sEntry<T>(g.entry(get<0>(it) + 1, get<1>(it) + 1));
        }
    }

    DEBUG_MESSAGE("updated ", _typeid(T), " Ds=", Ds, " transpose:", transpose);
}

namespace _internal{

template <clifford_algebra::repr R, size_t Ds, typename TE, typename T = TE> std::vector<T> embedded_spinor(const Caravel::momentumD<TE,Ds>& p, unsigned ind = 1){
    using namespace clifford_algebra;

    std::vector<T> ret(clifford_algebra::two_to_the(Ds/2-1),T(0));
    ret.shrink_to_fit();

    assert(ind <= ret.size()/2);

    typename sp_type<R,T>::type l(p);

    ret[2*ind-2] = l[0];
    ret[2*ind-1] = l[1];

    return ret;
}


// here rL and rR are repr of the FIRST spinor
template <repr rL, repr rR,size_t Ds,typename T,typename... rest> T recursive_contract(const std::vector<T>& l1, const SpinorData<T,Ds>& b2){
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    if(rL != _internal::repr_of_first_spinor<Dt/2>(rR,b2.second)){
        _WARNING("Mismatched representations!");
        return T(0);
    }
    auto l2 = embedded_spinor<rR>(b2.first,b2.second);
    T res(0);

    unroll_sp_prod<rL,Dt>(l1,l2,res,_internal::int_<Dt/2>{});

    return res;
}

template <repr rL, repr rR,size_t Ds,typename T,typename... rest> T recursive_contract(const std::vector<T>& b1, const momentumD<T,Ds>& p,rest... args){
    return recursive_contract<flip(rL),rR,Ds>(multiply_sparse_smatrix_Weyl<mult_from::R,rL,Ds>(p.get_vector(),b1),args...);
}


template <repr rL, repr rR, typename T, size_t Ds,typename... Ts> T sp(const SpinorData<T,Ds>& b1, Ts... args){
    static constexpr unsigned Dt = two_to_the(Ds/2-1);
    auto l1 = embedded_spinor<rL>(b1.first,b1.second);
    if(_internal::repr_of_first_spinor<Dt/2>(rL,b1.second) == repr::La) return recursive_contract<repr::La,rR>(l1,args...);
    else return recursive_contract<repr::Lat,rR>(l1,args...);
}

} // _internal




template <typename T, size_t Ds,typename... Ts> T spaa(const SpinorData<T,Ds>& s, Ts... args){ return _internal::sp<repr::La,repr::La>(s,args...);}
template <typename T, size_t Ds,typename... Ts> T spab(const SpinorData<T,Ds>& s, Ts... args){ return _internal::sp<repr::La,repr::Lat>(s,args...);}
template <typename T, size_t Ds,typename... Ts> T spba(const SpinorData<T,Ds>& s, Ts... args){ return _internal::sp<repr::Lat,repr::La>(s,args...);}
template <typename T, size_t Ds,typename... Ts> T spbb(const SpinorData<T,Ds>& s, Ts... args){ return _internal::sp<repr::Lat,repr::Lat>(s,args...); }

}}



namespace Caravel{ namespace clifford_algebra{

#define _INSTANTIATE_CLI_GEN(KEY,T,TE) \
    KEY template std::vector<T> cut_WF_q<repr::La, 1, T,4>(const Caravel::momentumD<TE,4>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 1, T,4>(const Caravel::momentumD<TE,4>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 1, T,4>(const Caravel::momentumD<TE,4>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 1, T,4>(const Caravel::momentumD<TE,4>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 1, T,6>(const Caravel::momentumD<TE,6>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 1, T,6>(const Caravel::momentumD<TE,6>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 1, T,6>(const Caravel::momentumD<TE,6>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 1, T,6>(const Caravel::momentumD<TE,6>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 2, T,6>(const Caravel::momentumD<TE,6>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 2, T,6>(const Caravel::momentumD<TE,6>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 2, T,6>(const Caravel::momentumD<TE,6>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 2, T,6>(const Caravel::momentumD<TE,6>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 1, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 1, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 1, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 1, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 2, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 2, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 2, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 2, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 3, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 3, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 3, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 3, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 4, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 4, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 4, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 4, T,8>(const Caravel::momentumD<TE,8>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 1, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 1, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 1, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 1, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 2, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 2, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 2, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 2, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 3, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 3, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 3, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 3, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 4, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 4, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 4, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 4, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 5, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 5, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 5, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 5, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 6, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 6, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 6, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 6, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 7, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 7, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 7, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 7, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::La, 8, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_q<repr::Lat, 8, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::La, 8, T,10>(const Caravel::momentumD<TE,10>&); \
    KEY template std::vector<T> cut_WF_qb<repr::Lat, 8, T,10>(const Caravel::momentumD<TE,10>&); \
    \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,6, T, std::array<T,6> > (const std::array<T,6> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,8, T, std::array<T,8> > (const std::array<T,8> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,10, T, std::array<T,10>> (const std::array<T,10>&, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,6, T, std::vector<T> > (const std::vector<T> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,8, T, std::vector<T> > (const std::vector<T> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::La,10, T, std::vector<T>> (const std::vector<T>&, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,6, T, std::array<T,6> > (const std::array<T,6> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,8, T, std::array<T,8> > (const std::array<T,8> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,10, T, std::array<T,10>> (const std::array<T,10>&, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,6, T, std::vector<T> > (const std::vector<T> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,8, T, std::vector<T> > (const std::vector<T> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::La,10, T, std::vector<T>> (const std::vector<T>&, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,6, T, std::array<T,6> > (const std::array<T,6> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,8, T, std::array<T,8> > (const std::array<T,8> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,10, T, std::array<T,10>> (const std::array<T,10>&, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,6, T, std::vector<T> > (const std::vector<T> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,8, T, std::vector<T> > (const std::vector<T> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::L,repr::Lat,10, T, std::vector<T>> (const std::vector<T>&, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,6, T, std::array<T,6> > (const std::array<T,6> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,8, T, std::array<T,8> > (const std::array<T,8> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,10, T, std::array<T,10>> (const std::array<T,10>&, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,6, T, std::vector<T> > (const std::vector<T> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,8, T, std::vector<T> > (const std::vector<T> &, const std::vector<T>&); \
    KEY template std::vector<T> multiply_sparse_smatrix_Weyl<mult_from::R,repr::Lat,10, T, std::vector<T>> (const std::vector<T>&, const std::vector<T>&); \
    \
    KEY template std::vector<T> LvAB_Ds<6,T>(const std::vector<T>&, const std::vector<T>&);\
    KEY template std::vector<T> LvAB_Ds<8,T>(const std::vector<T>&, const std::vector<T>&);\
    KEY template std::vector<T> LvAB_Ds<10,T>(const std::vector<T>&, const std::vector<T>&);\
    KEY template std::vector<T> LvBA_Ds<6,T>(const std::vector<T>&, const std::vector<T>&);\
    KEY template std::vector<T> LvBA_Ds<8,T>(const std::vector<T>&, const std::vector<T>&);\
    KEY template std::vector<T> LvBA_Ds<10,T>(const std::vector<T>&, const std::vector<T>&);\

_INSTANTIATE_CLI_GEN(extern,C,C)
_INSTANTIATE_CLI_GEN(extern,R,R)

#ifdef HIGH_PRECISION
_INSTANTIATE_CLI_GEN(extern,CHP,CHP)
_INSTANTIATE_CLI_GEN(extern,RHP,RHP)
#endif

#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_CLI_GEN(extern,CVHP,CVHP)
_INSTANTIATE_CLI_GEN(extern,RVHP,RVHP)
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_CLI_GEN(extern,RGMP,RGMP)
_INSTANTIATE_CLI_GEN(extern,CGMP,CGMP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_CLI_GEN(extern,extended_t<F32>,F32)
#endif


}}
