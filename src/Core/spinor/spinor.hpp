/**
 * @file spinor.hpp
 * @brief Contains templated implementations for spinor library.
 *
 * Needs to be complemented by spinor constructors for each specific type
 *
*/
#include <iostream>

#define _OLD_PHASE_CONVENTION 0   // with 1 the old phase convention is used

#ifdef INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

namespace Caravel{

    namespace _spinor_private{
        template <class T,size_t D> T plus(const momentumD<T,D>& l){
            static_assert(D>=4,"An attempt to use D<4 momentumD.");
            return l[0]+l[3];
        }
        template <class T,size_t D> T minus(const momentumD<T,D>& l){
            static_assert(D>=4,"An attempt to use D<4 momentumD.");
            return l[0]-l[3];
        }
    }

// Spinor inner products
template <class T> _INLINE T operator*(const la<T>& b1,const la<T>& b2){ return b2[0]*b1[1] - b1[0]*b2[1]; }
template <class T> _INLINE T operator*(const lat<T>& b1,const lat<T>& b2){ return b1[0]*b2[1] - b2[0]*b1[1]; }


//**************************************************
// These products are implemented to respect the "store only lower index" for spinors rule
// Do not touch this!
//**************************************************
template <class T> _INLINE lat<T> operator*(const smatrix<T>& m,const la<T>& b){
    return lat<T>(-m(2,1)*b[0] - m(2,2)*b[1], m(1,1)*b[0] + m(1,2)*b[1]);
}
template <class T> _INLINE lat<T> operator*(const la<T>& b,const smatrix<T>& m){
    return lat<T>(m(2,1)*b[0] + m(2,2)*b[1], -m(1,1)*b[0] - m(1,2)*b[1]);
}

template <class T> _INLINE la<T> operator*(const smatrix<T>& m,const lat<T>& b){
    return la<T>(b[0]*m(1,2) + b[1]*m(2,2), -b[0]*m(1,1) - b[1]*m(2,1));
}
template <class T> _INLINE la<T> operator*(const lat<T>& b,const smatrix<T>& m){
    return la<T>(-b[0]*m(1,2) - b[1]*m(2,2), b[0]*m(1,1) + b[1]*m(2,1));
}
//**************************************************

//! spinor product < * | * > for real momenta (overloaded)
/**
  \param p1 first  momentum
  \param p2 second  momentum
  \return spinor product <p1|p2>
  \sa spaa(momentumD<T,D> p1,momentumD<T,D> p2)
  */
template <class T, size_t D> _INLINE T spaa(const momentumD<T,D>& p1,const momentumD<T,D>& p2){ return la<T>(p1)*la<T>(p2); }
template <class T, size_t D> _INLINE T spaa(const la<T>& l1,const la<T>& l2){ return l1*l2; }
template <class T, size_t D> _INLINE T spaa(const momentumD<T,D>& l1,const la<T>& l2){ return la<T>(l1)*l2; }
template <class T, size_t D> _INLINE T spaa(const la<T>& l1,const momentumD<T,D>& l2){ return l1*la<T>(l2); }

template <class T, size_t D, class... Ts> T spaa(const momentumD<T,D>& first, const Ts&... rest){
    return spaa(la<T>(first),rest...);
}
template <class T, size_t D,class... Ts> T spaa(const la<T>& first, const momentumD<T,D>& second, const Ts&... rest){
    static_assert((sizeof...(rest))%2 == 0, "spaa with odd number of arguments called");
    return spba(first*smatrix<T>(second),rest...);
}

template <class T, size_t D,class... Ts> T spba(const lat<T>& first, const momentumD<T,D>& second, const Ts&... rest){
    static_assert((sizeof...(rest))%2 == 1, "spba with even number of arguments called");
    return spaa(first*smatrix<T>(second),rest...);
}
template <class T, size_t D,class... Ts> T spba(const momentumD<T,D>& first, const Ts&... rest){
    return spba(lat<T>(first),rest...);
}

//! spinor product [ * | * ] for real momenta (overloaded)
/**
  \param p1 first  momentum
  \param p2 second  momentum
  \return spinor product [p1|p2]
  \sa spbb(momentumD<T,D> p1,momentumD<T,D> p2)
  */

template <class T,size_t D> _INLINE T spbb(const momentumD<T,D>& p1,const momentumD<T,D>& p2){ return lat<T>(p1)*lat<T>(p2); }
template <class T,size_t D> _INLINE T spbb(const lat<T>& l1,const lat<T>& l2){ return l1*l2; }
template <class T,size_t D> _INLINE T spbb(const momentumD<T,D>& l1,const lat<T>& l2){ return lat<T>(l1)*l2; }
template <class T,size_t D> _INLINE T spbb(const lat<T>& l1,const momentumD<T,D>& l2){ return l1*lat<T>(l2); }


template <class T,size_t D,class... Ts> T spab(const momentumD<T,D>& first, const Ts&... rest){
    return spab(la<T>(first),rest...);
}
template <class T,size_t D, class... Ts> T spab(const la<T>& first, const momentumD<T,D>& second, const Ts&... rest){
    static_assert((sizeof...(rest))%2 == 1, "spab with even number of arguments called");
    return spbb(first*smatrix<T>(second),rest...);
}

template <class T,size_t D, class... Ts> T spbb(const momentumD<T,D>& first, const Ts&... rest){
    return spbb(lat<T>(first),rest...);
}
template <class T,size_t D, class... Ts> T spbb(const lat<T>& first, const momentumD<T,D>& second, const Ts&... rest){
    static_assert((sizeof...(rest))%2 == 0, "spba with odd number of arguments called");
    return spab(first*smatrix<T>(second),rest...);
}


// Constructors from real might be used later...
//template <class T> smatrix<T>::smatrix(const momentumD<T,D>& p): 
    //a({std::complex<T>(p.minus()),std::complex<T>(-p[1],p[2]), std::complex<T>(-p[1],-p[2]), std::complex<T>(p.plus())}) {} 
//template <class T> la<T>::la(const momentumD<T,D>& p) : 
    //la<T>(momentum<std::complex<T>>(std::complex<T>(p[0]),std::complex<T>(p[1]),std::complex<T>(p[2]),std::complex<T>(p[3]))) {}
//template <class T> lat<T>::lat(const momentumD<T,D>& p) : 
    //lat<T>(momentum<std::complex<T>>(std::complex<T>(p[0]),std::complex<T>(p[1]),std::complex<T>(p[2]),std::complex<T>(p[3]))) {}


template <class T, size_t D> _INLINE momentumD<T,D> LvBA(const lat<T>& l1,const la<T>& l2, typename std::enable_if<is_complex<T>::value>::type*){
    static_assert(D>=4,"An attempt to use D<4 momentumD.");
    T inv2=T(1)/T(2);
    T A11=inv2*l1[0]*l2[0];
    T A12=inv2*l1[0]*l2[1];
    T A21=inv2*l1[1]*l2[0];
    T A22=inv2*l1[1]*l2[1];
    return momentumD<T,D>(A22+A11,A21+A12,T(0,-1)*(A12-A21),A11-A22);
}

template <class T, size_t D> _INLINE momentumD<T,D> LvAB(const la<T>& l1,const lat<T>& l2){
    return LvBA(l2,l1);
}

template <class T,size_t D> momentumD<T,D> LvBA(const momentumD<T,D>& l1,const momentumD<T,D>& l2){ return LvBA<T,D>(lat<T>(l1), la<T>(l2));}
template <class T,size_t D> momentumD<T,D> LvAB(const momentumD<T,D>& l1,const momentumD<T,D>& l2){ return LvAB<T,D>(la<T>(l1), lat<T>(l2));}

template <class T,size_t D> momentumD<T,D>
LvBA(const lat<T>& l1,const la<T>& l2, typename std::enable_if<!is_complex<T>::value>::type*){
    static_assert(D>=4,"An attempt to use D<4 momentumD.");
    T inv2=T(1)/T(2);
    T A11=inv2*l1[0]*l2[0];
    T A12=inv2*l1[0]*l2[1];
    T A21=inv2*l1[1]*l2[0];
    T A22=inv2*l1[1]*l2[1];
    return momentumD<T,D>(A22+A11,A21+A12,A21-A12,A11-A22);
}

template <class T> std::ostream& operator<<(std::ostream& s, const la<T>& x){
    return s; 
    //return s << "la(" << x[0] << ',' << x[1]  << ')';
}
template <class T> std::ostream& operator<<(std::ostream& s, const lat<T>& x){
    return s; 
    //return s << "lat(" << x[0] << ',' << x[1]  << ')';
}
template <class T> std::ostream& operator<<(std::ostream& s, const smatrix<T>& x){
    return s; 
    //return s << '(' << x(1,1) << ',' << x(1,2)  << ','<<std::endl  << x(2,1)  <<',' << x(2,2)  << ')';
}


#define _INSTANTIATE_SPLIB_GEN_D(KEY,T,D) \
KEY template T spaa(const momentumD<T,D>&,const momentumD<T,D>&,const momentumD<T,D>&,const momentumD<T,4>&);\
KEY template T spab(const momentumD<T,D>&,const momentumD<T,D>&,const momentumD<T,D>&);\
KEY template T spba(const momentumD<T,D>&,const momentumD<T,D>&,const momentumD<T,D>&);\
KEY template T spbb(const momentumD<T,D>&,const momentumD<T,D>&,const momentumD<T,D>&,const momentumD<T,4>&);\
KEY template la<T>::la(const momentumD<T,D>&, void*);\
KEY template lat<T>::lat(const momentumD<T,D>&, void*);\
KEY template smatrix<T>::smatrix(const momentumD<T,D>&);

#define _INSTANTIATE_SPLIB_GEN(KEY,T) \
KEY template class spinor<T>;\
KEY template class la<T>;\
KEY template class lat<T>;\
KEY template class smatrix<T>;\
_INSTANTIATE_SPLIB_GEN_D(KEY,T,4)\
_INSTANTIATE_SPLIB_GEN_D(KEY,T,6)\
_INSTANTIATE_SPLIB_GEN_D(KEY,T,8)\
_INSTANTIATE_SPLIB_GEN_D(KEY,T,10)\
KEY template std::ostream& operator<<(std::ostream& s, const smatrix<T>& p);\
KEY template std::ostream& operator<<(std::ostream& s, const la<T>& p);\
KEY template std::ostream& operator<<(std::ostream& s, const lat<T>& p);\

#define _INSTANTIATE_SPLIB(T) _INSTANTIATE_SPLIB_GEN(, T)
#define _INSTANTIATE_SPLIB_EXTERN(T) _INSTANTIATE_SPLIB_GEN(extern, T)

_INSTANTIATE_SPLIB_EXTERN(C)
#ifdef HIGH_PRECISION
_INSTANTIATE_SPLIB_EXTERN(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_SPLIB_EXTERN(CVHP)
#endif
_INSTANTIATE_SPLIB_EXTERN(R)
#ifdef HIGH_PRECISION
_INSTANTIATE_SPLIB_EXTERN(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_SPLIB_EXTERN(RVHP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_SPLIB_EXTERN(F32)
#endif

#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_SPLIB_EXTERN(BigRat)
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_SPLIB_EXTERN(RGMP)
_INSTANTIATE_SPLIB_EXTERN(CGMP)
#endif

}
