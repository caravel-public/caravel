#include "dirac_algebra.h"

namespace Caravel {
namespace clifford_algebra {
/**
 * Instantiations for members
 */

_INSTANTIATE_DIRAC_GEN(,C) 
_INSTANTIATE_DIRAC_GEN(,R) 

#ifdef HIGH_PRECISON
_INSTANTIATE_DIRAC_GEN(,CHP) 
_INSTANTIATE_DIRAC_GEN(,RHP) 
#endif

#ifdef VERY_HIGH_PRECISON
_INSTANTIATE_DIRAC_GEN(,CVHP) 
_INSTANTIATE_DIRAC_GEN(,RVHP) 
#endif

#ifdef USE_GMP
_INSTANTIATE_DIRAC_GEN(,CGMP) 
_INSTANTIATE_DIRAC_GEN(,RGMP) 
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_DIRAC_GEN(,F32) 
_INSTANTIATE_DIRAC_GEN_EXACT(,F32) 
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_DIRAC_GEN(,BigRat) 
_INSTANTIATE_DIRAC_GEN_EXACT(,BigRat) 
#endif
#endif


} // namespace clifford_algebra
} // namespace Caravel
