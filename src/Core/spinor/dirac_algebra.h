/**
 * @file dirac_algebra.h
 *
 * @date 4.12.2019
 *
 * @brief Templated implementation of D dimensional Clifford algebra using Dirac Gamma matrices
 *
 * DiracGamma<D,T>(mu) is the Dirac Gamma matrix for the vector index mu with vector dimensions D.
 * These Dirac matrices are of size 2^(D/2) x 2^(D/2) and are sparse in higher dimensions.
 *
 * For a complex type T the usual Weyl or Dirac representation is provided, while
 * for a real type T a real valued representation is chosen that uses an alternating metric signature.
 * However, there is Weyl and a Dirac version of this representation as well.
 *
 * \class sparse_DGamma contains only non-zero elements of ALL Dirac Gamma matrices in a given
 * dimension D. This is used for an efficient implementation for P_slash.
 */
// Chose representation of 4D Gamma Matrices
// 1 is Weyl, 0 is Dirac
#define WEYL 1


//! This is a prefix which we use for the naming of MASSIVE quark states at the cut. 
//"b" is appended for anti-quarks and the Dtt_index is appended at the end.
#define Q_CUT_STATE_PREFIX std::string("QD")

#ifndef DIRAC_ALGEBRA_H
#define DIRAC_ALGEBRA_H

#include "Core/spinor/clifford_algebra.h"

namespace Caravel {
namespace clifford_algebra {

template <size_t D, typename T> class DiracGamma;

/**
 * Template specialisation for DiracGamma<0> as empty class
 */
template <typename T> class DiracGamma<0,T> {
  public:
    DiracGamma() = default;
};
/**
 * Template specialisation for DiracGamma<1> as empty class
 */
template <typename T> class DiracGamma<1,T> {
  public:
    DiracGamma() = default;
};
/**
 * Template specialisation for DiracGamma<2> in terms of Pauli Matrices
 * DiracGamma<2>(0) = 1, DiracGamma<2>(i) = PauliMatrix[i]
 */
template <typename T> class DiracGamma<2,T> {
  private:
    sEntry se11, se12, se21, se22;
    bool isZero;
    unsigned idx;

  public:
    DiracGamma() : se11(sEntry::Z), se12(sEntry::Z), se21(sEntry::Z), se22(sEntry::Z), isZero(true), idx(4){};
    /*
     * Creates the Pauli-Matrices in D=2
     * @param mu refers to PauliMatrix[mu], with PauliMatrix[0] = Identity
     */
    DiracGamma(const unsigned mu) {
        se11 = sEntry::Z;
        se12 = sEntry::Z;
        se21 = sEntry::Z;
        se22 = sEntry::Z;
        isZero = false;
        idx = mu;
        switch (mu) {
            case 0:
                se11 = sEntry::One;
                se22 = sEntry::One;
                break;
            case 1:
                se12 = sEntry::One;
                se21 = sEntry::One;
                break;
            case 2:
                if( is_complex<T>::value ) {
                  se12 = sEntry::mI;
                  se21 = sEntry::I;
                } else {
                  se12 = sEntry::mOne;
                  se21 = sEntry::One;
                } 
                break;
            case 3:
                se11 = sEntry::One;
                se22 = sEntry::mOne;
                break;
            default:
                isZero = true;
                idx = 4;
                break;
        }

        return;
    } // constructor
    bool is_zero() const { return isZero; };
    unsigned get_index() const { return idx; };
    sEntry entry(size_t i, size_t j) const {
        if (isZero) return sEntry::Z;

        if (i == 1 && j == 1)
            return se11;
        else if (i == 1 && j == 2)
            return se12;
        else if (i == 2 && j == 1)
            return se21;
        else
            return se22;
    }
};
/**
 * The general template implementation for D dimensional Dirac Gamma matrices
 * The index is interpreted according to:
 *
 * 0<=idx<Ds) The Dirac \f$\Gamma^i_{(Ds)} \f$ matrix
 *
 * idx==Ds) The special matrix \f$\hat \Gamma_{(Ds)} \f$ (which in Ds=4 coincides with gamma5)
 *
 */
template <size_t D, typename T> class DiracGamma {
    static_assert(D >= 4, "Starting Construction of Dirac Gamma matrices from D = 4");
    static_assert(D % 2 == 0, "Only even D can be constructed");

  private:
    // (D-2)x(D-2) sub matrices
    DiracGamma<D - 2,T> g11; // upper left  sub matrix
    DiracGamma<D - 2,T> g12; // upper right sub matrix
    DiracGamma<D - 2,T> g21; // lower left  sub matrix
    DiracGamma<D - 2,T> g22; // lower right sub matrix
    // prefactors for the sub matrices
    sEntry se11, se12, se21, se22; // seIJ is prefactor of gIJ
    bool isZero;
    unsigned idx, Dt;

  public:
    // Constructors
    DiracGamma() : isZero(true), idx(D + 1), Dt(two_to_the(D / 2)) {}
    /*
     * Generates Dirac Gamma matrices for even dimensions D >= 4
     * @param mu refers to the Vector index [0,D-1], while mu = D
     * is the special matrix that commutes with all other.
     */
    DiracGamma(const unsigned mu);
    /*
     * Get acess to the matrix element
     * @param i The row-index - 1-based
     * @param j The column-index - 1-based
     */
    sEntry entry(size_t i, size_t j) const;
    bool is_zero() const { return isZero; };
    unsigned get_index() const { return idx; };
};

// Make DiracGamma matrices printable
template <size_t D, typename T> std::ostream& operator<<(std::ostream& s, const DiracGamma<D,T>& g) {
    size_t Dt = two_to_the(D / 2);
    for (unsigned i = 1; i <= Dt; i++) {
        for (unsigned j = 1; j <= Dt; j++) {
            if (j == 1) s << "[ ";
            s << g.entry(i, j) << " ";
        }
        s << "]" << std::endl;
    }
    return s;
}

/**
 * \class sparse_DGamma holds all non-zero values of the Dirac Gamma matrices for a given
 * dimension D and number type T.
 *
 */
template <size_t D, typename T, bool transpose> struct sparse_DGamma;

template <size_t D, typename T, bool transpose = false> struct sparse_DGamma {
    static_assert(D >= 4, "Constructing sparse Dirac Gamma matrices only for even D >= 4!");

  private:
    static void init_values();
    static inline std::once_flag firstcall;

  public:
    constexpr static size_t Dt = two_to_the(D / 2);
    static inline std::array<std::vector<std::tuple<size_t, size_t, T>>, D + 1> values;
    static void init() { std::call_once(firstcall, init_values); };
    static void print();

    template <typename TT = T> static std::enable_if_t<!is_exact<TT>::value> update_values() {}
    template <typename TT = T> static std::enable_if_t<is_exact<TT>::value> update_values();

    sparse_DGamma() = delete;
};

/**
 * This function computes P_slash*|Psi> or <Psi|*P_slash for generic Ds dimensions
 * MF == mult_from::right refers to P_slash*|Psi>
 * MF == mult_from::left refers to <Psi|*P_slash 
 */
template <mult_from MF, typename T, size_t Ds, typename TE, typename TT> std::vector<T> multiply_sparse_DGamma(const std::array<TE,Ds>& p, const std::vector<TT>& in);
template <mult_from MF, typename T, size_t Ds, typename TE, typename TT> std::vector<T> multiply_sparse_DGamma(const std::vector<TE>& p, const std::vector<TT>& in) {
    std::array<TE, Ds> q;
    // FIXME: Work around, since the normal copy constructor does not work with F32
    // std::copy(p.begin(), p.end(), q);
    for (unsigned n = 0; n < p.size(); n++) q[n] = p[n];
    return multiply_sparse_DGamma<MF, T, Ds, TE, TT>(q, in);
}

/**
  * Computes <PsiBar|gamma^mu|Psi> for Ds dimensions
  */
template <size_t Ds, typename T> std::vector<T> vector_from_DSpinor(const std::vector<T>& psi, const std::vector<T>& psiBar);
} // namespace clifford_algebra
} // namespace Caravel

#include "dirac_algebra.hpp"

#endif
