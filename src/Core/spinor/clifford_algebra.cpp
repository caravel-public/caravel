#include "clifford_algebra.h"

#include "Core/typedefs.h"

namespace Caravel {
namespace clifford_algebra{

sEntry sEntry_multiply(const sEntry& prefactor,const sEntry& entry){
    if(prefactor==sEntry::Z||entry==sEntry::Z)
        return sEntry::Z;
    if(prefactor==sEntry::mOne){
        if(entry==sEntry::I)
            return sEntry::mI;
        else if(entry==sEntry::mI)
            return sEntry::I;
        else if(entry==sEntry::One)
            return sEntry::mOne;
        else if(entry==sEntry::mOne)
            return sEntry::One;
    }
    if(prefactor==sEntry::I){
        if(entry==sEntry::I)
            return sEntry::mOne;
        else if(entry==sEntry::mI)
            return sEntry::One;
        else if(entry==sEntry::One)
            return sEntry::I;
        else if(entry==sEntry::mOne)
            return sEntry::mI;
    }
    if(prefactor==sEntry::mI){
        if(entry==sEntry::I)
            return sEntry::One;
        else if(entry==sEntry::mI)
            return sEntry::mOne;
        else if(entry==sEntry::One)
            return sEntry::mI;
        else if(entry==sEntry::mOne)
            return sEntry::I;
    }
    return entry;
}

std::ostream& operator<<(std::ostream& o, const Caravel::clifford_algebra::sEntry& s){
    using Caravel::clifford_algebra::sEntry;
    switch(s){
        case sEntry::mOne: return o<<(int)s;
        case sEntry::mI: return o<< "-I";
        case sEntry::I: return o<<" I";
        case sEntry::Z: return o<<" .";
        case sEntry::One: return o<<" "<<(int)s;
    }
    return o;
}

_INSTANTIATE_CLI_GEN(,C,C)
_INSTANTIATE_CLI_GEN(,R,R)

#ifdef HIGH_PRECISION
_INSTANTIATE_CLI_GEN(,CHP,CHP)
_INSTANTIATE_CLI_GEN(,RHP,RHP)
#endif

#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_CLI_GEN(,CVHP,CVHP)
_INSTANTIATE_CLI_GEN(,RVHP,RVHP)
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_CLI_GEN(,RGMP,RGMP)
_INSTANTIATE_CLI_GEN(,CGMP,CGMP)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_CLI_GEN(,extended_t<F32>,F32)
#endif


}}
