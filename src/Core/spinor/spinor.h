/**
 * @file spinor.h
 *
 * @brief Implementations of Weyl spinor algebra
 * 
 * Contains implementations of 4d momenta, la and lat spinors, slashed matrices and all products.
 * 
 * Only 2-dimensional spinors are used and dotted and undotted representations are tracked consistently.
 * The notation closely follows the one of S@M paper arXiv:0710.5559v2
 * Identifications between spinor notations:
 *
 *              |*>   <-->  la      <-->    \lambda_a
 *              <*|   <-->  Cla     <-->    \lambda^a
 *              |*]   <-->  lat     <-->    \dot\lambda^a
 *              [*|   <-->  Clat    <-->    \dot\lambda_a
 *
 *
 * Action of \gamma^5:
 *
 *              \gamma^5 |*> = |*>
 *              \gamma^5 |*] = -\gamma^5 |*]
 *
 * We implement and store only la and lat. This means that after any operation stored values for spinors will be
 * converted to lower indices. However all operations are aware of this and unless a user tries to mess with elements
 * of spinors explicitly, all operations behave as one would expect in the bracket notation like < * | * | * ] and so on.
 *
 * Most likely you only need to use spinor products
 *  spaa(1,2,3,...,n) := <1|23...|n>
 *  spab(1,2,3,...,n) := <1|23...|n]
 *  spba(1,2,3,...,n) := [1|23...|n>
 *  spbb(1,2,3,...,n) := [1|23...|n]
 * which can be called with (any number of) momenta directly and without bothering of constructing spinors explicitly.
 * They are implemented in a way that strong type safety will allow you to compile only sensible things.
 *
 * Or vectors from spinors with
 *  LvBA = [l1| \gamma^mu |l2>/2
 *  LvAB = <l1| \gamma^mu |l2]/2
 *
 *
 *
 * WARNING!!!
 * Spinors for integral types are NOT normalized! Cut states take this into an account since
 * they share the same momentum.
 * This is not possible for external states, where anyway the spinor weight has to be removed.
 * 
*/
#ifndef _SPINOR_H
#define _SPINOR_H

#include "Core/momD.h"
#include "Core/type_traits_extra.h"
#include "Core/precision_cast.h"
#include "Core/CaravelException.h"

#define _INLINE inline

namespace Caravel {

template <class T> class spinor;
template <class T> class smatrix;
template <class T> class la;
template <class T> class lat;

/** 
 * Construct a vector [l1| \gamma^mu |l2>/2
 * \param l1 spinor [l1| or momentum
 * \param l2 spinor |l2> or momentum
 * \return vector [l1| \gamma^mu |l2>/2
 */
template <class T,size_t D=4> momentumD<T,D>
LvBA(const lat<T>& l1,const la<T>& l2, typename std::enable_if<is_complex<T>::value>::type* = 0);
template <class T,size_t D=4> momentumD<T,D>
LvBA(const lat<T>& l1,const la<T>& l2, typename std::enable_if<!is_complex<T>::value>::type* = 0);

template <class T,size_t D=4> momentumD<T,D> LvBA(const momentumD<T,D>& l1,const momentumD<T,D>& l2);


/** 
 * Construct a vector <l1| \gamma^mu |l2]/2
 * \param l1 spinor <l1| or momentum
 * \param l2 spinor |l2] or momentum
 * \return vector <l1| \gamma^mu |l2]/2
 */
template <class T,size_t D=4> momentumD<T,D> LvAB(const la<T>& l1,const lat<T>& l2);
template <class T,size_t D=4> momentumD<T,D> LvAB(const momentumD<T,D>& l1,const momentumD<T,D>& l2);

template <class T> _INLINE lat<T> operator*(const smatrix<T>&,const la<T>&);
template <class T> _INLINE la<T> operator*(const smatrix<T>&,const lat<T>&);
template <class T> _INLINE lat<T> operator*(const la<T>&,const smatrix<T>&);
template <class T> _INLINE la<T> operator*(const lat<T>&,const smatrix<T>&);
// Spinor inner products
template <class T> _INLINE T operator*(const la<T>&,const la<T>&);
template <class T> _INLINE T operator*(const lat<T>&,const lat<T>&);
// Print functions
template <class T> std::ostream& operator<<(std::ostream&, const smatrix<T>& );
template <class T> std::ostream& operator<<(std::ostream&, const la<T>& );
template <class T> std::ostream& operator<<(std::ostream&, const lat<T>& );


//! Template class for spinorial objects
template <class T> class spinor {
    protected:
        //typedef typename T::value_type TR; // real type
        std::array<T,2> a{};

        //friend lat<T> operator* <>(const smatrix<T>&,const la<T>&);
        //friend la<T> operator* <>(const smatrix<T>&,const lat<T>&);
        //friend lat<T> operator* <>(const la<T>&,const smatrix<T>&);
        //friend la<T> operator* <>(const lat<T>&,const smatrix<T>&);
        //friend T operator* <> (const la<T>&,const la<T>&);
        //friend T operator* <> (const lat<T>&,const lat<T>&);
        //friend momentum<T> PfLLt <> (const lat<T>&,const la<T>&);
        //friend std::ostream& operator<< <>(std::ostream&, const la<T>& );
        //friend std::ostream& operator<< <>(std::ostream&, const lat<T>& );

    public:
        //! safe access
        const T& at(unsigned i) const { return a.at(i); }
        //! unsafe access
        const T& operator()(unsigned i) const { return a[i]; }
        const T& operator[](unsigned i) const { return a[i]; }

        //! Constructor (usually not needed, use the constructors for la and lat instead)
        /** \sa la  lat smatrix */
        spinor() {}
        spinor(const T& A1,const T& A2): a({A1,A2}) {}
        spinor(const std::array<T,2>& seta): a(seta) {}

        std::vector<T> get_vector() const { return std::vector<T>{a[0],a[1]}; }

        //const std::array<T,2>& get_elements() const { return a; }
};

//! Template for spinorial objects of the type "la"
template <class T> class la : public spinor<T> {
    using spinor<T>::a;
    public:
	template <size_t D, typename TT=T> la(const momentumD<T,D>& p, typename std::enable_if<is_complex<TT>::value>::type* = nullptr);
	template <size_t D, typename TT=T> la(const momentumD<T,D>& p, typename std::enable_if<!is_complex<TT>::value>::type* = nullptr);
	la(const spinor<T>& s) : spinor<T>(s) {};
	la(const T& A1,const T& A2) : spinor<T>(A1,A2) {};

        la<T>& operator=(const la<T>& p)=default;

        la<T>& operator+=(const la<T>& p){a[0]+=p[0]; a[1]+=p[1]; return *this;}
        la<T>& operator-=(const la<T>& p){a[0]-=p[0]; a[1]-=p[1]; return *this;}
        la<T>& operator/=(const T& x){a[0]/=x; a[1]/=x; return *this;}
        la<T>& operator*=(const T& x){a[0]*=x; a[1]*=x; return *this;}
};

//! Template for spinorial objects of the type "la tilde"
/** \sa smatrix  */
template <class T> class lat : public spinor<T> {
    using spinor<T>::a;
    public:
	template <size_t D, typename TT=T> lat(const momentumD<T,D>& p, typename std::enable_if<is_complex<TT>::value>::type* = nullptr);
	template <size_t D, typename TT=T> lat(const momentumD<T,D>& p, typename std::enable_if<!is_complex<TT>::value>::type* = nullptr);
	lat(const spinor<T> s) : spinor<T>(s) {};
	lat(const T& A1,const T& A2) : spinor<T>(A1,A2) {};

        lat<T>& operator=(const lat<T>& p)=default;

        lat<T>& operator+=(const lat<T>& p){a[0]+=p[0]; a[1]+=p[1]; return *this;}
        lat<T>& operator-=(const lat<T>& p){a[0]-=p[0]; a[1]-=p[1]; return *this;}
        lat<T>& operator/=(const T& x){a[0]/=x; a[1]/=x; return *this;}
        lat<T>& operator*=(const T& x){a[0]*=x; a[1]*=x; return *this;}
};

// Basic algebra
template <class T> _INLINE la<T> operator+(la<T> b1,const la<T>& b2){ return b1+=b2; }
template <class T> _INLINE la<T> operator-(la<T> b1,const la<T>& b2){ return b1-=b2; }
template <class T> _INLINE la<T> operator-(const la<T>& b){ return la<T>(-b[0],-b[1]); }
template <class T> _INLINE la<T> operator*(const T& c, la<T> b){ return b*=c; }
template <class T> _INLINE la<T> operator*(la<T> b,const T& c){ return b*=c; }
template <class T> _INLINE la<T> operator/(la<T> b,const T& c){ return b/=c; }

template <class T> _INLINE lat<T> operator+(lat<T> b1,const lat<T>& b2){ return b1+=b2; }
template <class T> _INLINE lat<T> operator-(lat<T> b1,const lat<T>& b2){ return b1-=b2; }
template <class T> _INLINE lat<T> operator-(const lat<T>& b){ return lat<T>(-b[0],-b[1]); }
template <class T> _INLINE lat<T> operator*(const T& c, lat<T> b){ return b*=c; }
template <class T> _INLINE lat<T> operator*(lat<T> b,const T& c){ return b*=c; }
template <class T> _INLINE lat<T> operator/(lat<T> b,const T& c){ return b/=c; }

//! Template class for slashed matrices
/**
 * The slashed matrices are 2x2 matrices they correspond to the CLat[p].CLa[p] objects in S\@M.
 * Smatrix objects can be multiplied with la and lat objects. 
 * They can't be multiplied together since a smatrix object is supposed to have a dotted and an undotted index,
 * whereas the product of two slashed matrices has two dotted or two undotted indices.
 *
 * Slashed matrices are stored as p^{\dot{a}a} and all operations are aware of this.
 */
template <class T> class smatrix {
    protected:
        std::array<T,4> a; // {a11,a12,a21,a22}

    public:
        // No bound checking, 1-based
        const T& operator()(unsigned i, unsigned j) const{ return a[2*(i-1) + (j-1)];}
        // With bound checking 
        const T& at(unsigned i, unsigned j) const{ return a.at(2*(i-1) + (j-1));}

        //! explicit constructor
        smatrix(const T& A11,const T& A12,const T& A21,const T& A22) : a({A11, A12, A21, A22}){}
        //! Constructor from  momentum for complex types
        template <size_t D, typename U = T>
            smatrix(const momentumD<typename std::enable_if<is_complex<U>::value,U>::type,D>& p):
                a({p[0] - p[3], -(p[1]-U(0,1)*p[2]), -(p[1]+U(0,1)*p[2]) ,p[0] + p[3]}) {
                    static_assert(D>=4,"An attempt to use D<4 momentumD.");
        }
        //! Constructor from  momentum for real types
        template <size_t D, typename U = T>
            smatrix(const momentumD<typename std::enable_if<!is_complex<U>::value,U>::type,D>& p):
                a({p[0] - p[3], -(p[1]+p[2]), -(p[1]-p[2]) ,p[0] + p[3]}) {
                    static_assert(D>=4,"An attempt to use D<4 momentumD.");
        }
};





// Prohibit inter-representation mixing

template <class T> T operator*(const la<T>& b1,const lat<T>& b2) = delete;
template <class T> T operator*(const lat<T>& b1,const la<T>& b2) = delete;

template <class T> T operator+(const la<T>& b1,const lat<T>& b2) = delete;
template <class T> T operator+(const lat<T>& b1,const la<T>& b2) = delete;

template <class T> T operator-(const la<T>& b1,const lat<T>& b2) = delete;
template <class T> T operator-(const lat<T>& b1,const la<T>& b2) = delete;

template <typename Thigh, typename Tlow, typename = EnableIfFloat<Thigh>> inline la<Thigh>
to_precision(const la<Tlow>& a){
    return la<Thigh>(Thigh(a[0]),Thigh(a[1]));
}
template <typename Thigh, typename Tlow, typename = EnableIfFloat<Thigh>> inline lat<Thigh>
to_precision(const lat<Tlow>& a){
    return lat<Thigh>(Thigh(a[0]),Thigh(a[1]));
}


}


#include "spinor.hpp"
#undef _INLINE

#endif
