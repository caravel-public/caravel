/**
 * @file clifford_algebra.h
 *
 * @date 17.7.2017
 *
 * @brief Templated implementation of D dimensional Clifford algebra
 *
 *
 * Gamma<Ds>(i) is is the Dtt part of the ith matrix in Ds (vector!) dimensions.
 * Notice that 4d part is abstracted out and can be represented either by Weyl or Dirac spinors.
 *
 * \class sparse_smatrix contains all non-zero elements of Dtt-dim matrices necessary for action of
 * slashed smatrix in Ds dimensions on Ds-dimensional spinors.
 */
#ifndef CLIFFORD_H
#define CLIFFORD_H

#include <iosfwd>
#include "Core/Utilities.h"
#include "Core/type_traits_extra.h"
#include "Core/momD.h"

#include "Core/extended_t.h"

#include "Core/Particle.h"

#include <mutex>


//! This is a prefix which we use for the naming of quark states at the cut. 
//"b" is appended for anti-quarks and the Dtt_index is appended at the end.
#define q_CUT_STATE_PREFIX std::string("qD")


namespace Caravel {

template <typename T> class la;
template <typename T> class lat;

namespace clifford_algebra{

using Caravel::repr;

/**
 * We enumerate the possible entries of Gamma<D>
 */
enum class sEntry {mI=-2,I=2,One=1,mOne=-1,Z=0};

//! Convert sEntry to an explicit type
template <typename T> inline std::enable_if_t<is_complex<T>::value,T> explicit_sEntry (const sEntry s){
    switch(s){
        case sEntry::mI: return T(0,-1);
        case sEntry::I: return T(0,1);
        case sEntry::One: return T(1);
        case sEntry::mOne: return T(-1);
        case sEntry::Z: return T(0);
    }

    return T(0);
}
template <typename T> inline std::enable_if_t<!is_complex<T>::value,T> explicit_sEntry (const sEntry s){
    switch(s){
        case sEntry::One: return T(1);
        case sEntry::mOne: return T(-1);
        case sEntry::Z: return T(0);
        case sEntry::mI:
        case sEntry::I:
            throw std::logic_error("Encountered complex sEntry for real types!");
    }

    return T(0);
}

/**
 * Multiplication table for sEntry's
 */
sEntry sEntry_multiply(const sEntry& prefactor,const sEntry& entry);
struct Identity;

// Forward declaration
template <unsigned D> class Gamma;

template <> class Gamma<0> { public: Gamma() = default; };
template <> class Gamma<1> { public: Gamma() = default; };
template <> class Gamma<2> { public: Gamma() = default; };
template <> class Gamma<3> { public: Gamma() = default; };

template <> class Gamma<4> {
    /* const */ sEntry se11;
    public:
    constexpr static unsigned Dtt{1};
    bool is_zero;
    unsigned i;
    // Default constructor returns zero
    Gamma(): se11(sEntry::One), i(4) {}
    // sigma_0 is identity, sigma_i Pauli matrices, sigma_4 is zero
    Gamma(const unsigned ii,bool): se11(sEntry::One), is_zero(false), i(4) {}
    Gamma(Identity*):se11(sEntry::One), is_zero(false), i(~0) {}
    sEntry entry(const unsigned i,const unsigned j) const { return (i==1&&j==1)?se11: sEntry::Z;}
    bool zero() const { return is_zero; }
    unsigned index() const { return i; }
    void multiply(const sEntry& s) { se11=sEntry_multiply(s,se11);}
};

/**
 * Compute powers of two
 */
constexpr unsigned two_to_the(unsigned n){
    if(n==0)
        return 1;
    if(n==1)
        return 2;
    return 2*two_to_the(n-1);
}


template <unsigned Ds> class Gamma {
    static_assert(Ds>=4,"We start from Ds=4");
    static_assert(Ds%2==0,"Only even Ds are acceptable");
    protected:
        // (Ds-2)x(Ds-2) sub matrices
        Gamma<Ds-2> g11;		/**< Upper left (Ds-2)x(Ds-2) sub matrix */
        Gamma<Ds-2> g12;		/**< Upper right (Ds-2)x(Ds-2) sub matrix */
        Gamma<Ds-2> g21;		/**< Lower left (Ds-2)x(Ds-2) sub matrix */
        Gamma<Ds-2> g22;		/**< Lower right (Ds-2)x(Ds-2) sub matrix */
        // entries of sub matrices
        sEntry se11;		/**< sEntry prefactor to g11 */
        sEntry se12;		/**< sEntry prefactor to g12 */
        sEntry se21;		/**< sEntry prefactor to g21 */
        sEntry se22;		/**< sEntry prefactor to g22 */

    public:
        constexpr static unsigned Dtt{two_to_the(Ds/2)/4};
        bool is_zero;		/**< Exposed boolean that tags zero matrices (helpful as matrices are sparse) */
        unsigned i;		/**< Matrix index (following the implicit naming convention defined by the Gamma(unsigned) constructor) */
        /**
         * Default constructor returns a zero matrix
         */
        Gamma(): is_zero(true), i(Ds+1) {}
        /**
         * Main constructor of Gamma<Ds> matrices. The index is interpreted according to:
         *
         * 0<=i<Ds) The Dirac \f$\Gamma^i_{(Ds)} \f$ matrix
         *
         * i==Ds) The special matrix \f$\hat \Gamma_{(Ds)} \f$ (which in Ds=4 coincides is just one)
         *
         * if only_real_components is set to true we construct real matrices
         */
        Gamma(const unsigned ii, bool only_real_components = false);
        Gamma(Identity*);

        /**
         * Method to get access to entries of Gamma<Ds>
         */
        sEntry entry(unsigned i,unsigned j) const;

        /**
         * Method to multiply *this by a constant
         */
        void multiply(const sEntry& s) { se11=sEntry_multiply(s,se11); se12=sEntry_multiply(s,se12); se21=sEntry_multiply(s,se21); se22=sEntry_multiply(s,se22); }
};


/**
 * \class sparse_smatrix contains all non-zero elements of each gamma matrix in Ds dimensions
 *  It is a sparse matrix in a sense that only elements (and their positions) which are non-zero stored.
 *  It is then used to construct slashed matrix contracted with arbitrary momenta.
 *
 *  by default elements are sorted by a column index for easy access when it multiplies on the left
 *  to sort in row index set transpose = true
 */
template <unsigned Ds, typename T, bool transpose> struct sparse_smatrix;

template <unsigned Ds, typename T, bool transpose = false> struct sparse_smatrix {
  private:
    static void init_impl();
    static inline std::once_flag initInstanceFlag{};

  public:
    static_assert(Ds >= 4, "We construct smatrix only for Ds>4 here");
    constexpr static unsigned Dsm4 = Ds - 4;
    constexpr static unsigned Dt{two_to_the(Ds / 2)};

    static inline std::array<std::vector<std::tuple<unsigned, unsigned, T>>, Dsm4> values{};
    // std::array<std::vector<T>,Dsm4> values;

    constexpr static unsigned size = Dt / 4;

    static void init() { std::call_once(initInstanceFlag, init_impl); };

    template <typename TT = T> static std::enable_if_t<!is_exact<TT>::value> update_values() {}
    template <typename TT = T> static std::enable_if_t<is_exact<TT>::value> update_values();

    sparse_smatrix() = delete;
};

template <typename T, bool transpose> struct sparse_smatrix<4, T, transpose> {
    constexpr static unsigned Dsm4 = 0;
    constexpr static unsigned Dt = 2;
    static constexpr std::array<std::vector<std::tuple<unsigned, unsigned, T>>, 0> values{};
    static constexpr void init() {};
    static constexpr unsigned size{0};
    static constexpr void update_values(){};
};

// this is a very funny thing we have to put here explicitely before c++17 for static constexp memebers
#if __cplusplus <  201703L
template <typename T, bool transpose> constexpr std::array<std::vector<std::tuple<unsigned, unsigned, T>>, 0> sparse_smatrix<4, T, transpose>::values;
#endif

//! multiply from left or right
enum class mult_from{L=1, R=0 };


//! assign an explicit spinor type
template <repr R,typename T> struct sp_type{
    typedef typename std::conditional<R==repr::La,Caravel::la<T>,Caravel::lat<T>>::type type;
};



/**
 * Multiply a Dt/2-dim spinor by a Dt/2xDt/2 slashed matrix in Dt dimensions.
 * mult_from::left or mult_from::right
 * repr is a helicity of the Weyl spinors at the first entry
 * g5 |*> = |*>     -->     La (+1)
 * g5 |*] = - |*]   -->     Lat (-1)
 * Ds is the *vector* dimensionality of the space.
 * \param momentum to contract smatrix with
 * \param spinor to transform
 */
template <mult_from D,repr F,size_t Ds, typename T, typename lT> void multiply_sparse_smatrix_Weyl_impl(const lT& l, typename std::vector<T>::const_iterator in, typename std::vector<T>::iterator out);
template <mult_from D,repr F,size_t Ds, typename T, typename lT> std::vector<T> multiply_sparse_smatrix_Weyl(const lT& l, const std::vector<T>& in);

/**    Cut wave function with Dtt_index.
 *                -
 *        l      <
 *       <---   -
 *     |---<---*
 *              \
 *               \
 *
 *  R should be repr::La normally since it is a "quark", however I made it a template just in case. Which means
 *  that we use Lat 4d spinot and act on it from the right with the Dirac operator.
 *  [q| sm --> <l|_cut
 *  Where q we choose as a lightcone decomposed (flat) momentum from 4d part of the full loop momentum.
 *  We choose to devide by (2l*q) this state (we need do do either this or devide each by a square root).
 */
template <repr R, unsigned Dtt_index, typename T,size_t Ds, typename TE = typename infer_unext_type<T>::type> std::vector<T> cut_WF_q(const Caravel::momentumD<TE,Ds>& l);

/**    Cut wave function with Dtt_index.
 *                -
 *        l      >
 *       <---   -
 *     |--->---*
 *              \
 *               \
 *
 *  R should be repr::Lat normally since it is a "quark", however I made it a template just in case. Which means
 *  that we use La 4d spinot and act on it from the LEFT with the Dirac operator.
 *  sm |q] --> |l>_cut
 *  Where q we choose as a lightcone decomposed (flat) momentum from 4d part of the full loop momentum.
 *  We also flip the momentum sign here.
 */
template <repr R, unsigned Dtt_index, typename T,size_t Ds, typename TE = typename infer_unext_type<T>::type> std::vector<T> cut_WF_qb(const Caravel::momentumD<TE,Ds>& ml);

template <size_t Ds, typename T> std::vector<T> LvAB_Ds(const std::vector<T>& A, const std::vector<T>& B);
template <size_t Ds, typename T> void LvAB_Ds_impl(typename std::vector<T>::const_iterator A, typename std::vector<T>::const_iterator B,typename std::vector<T>::iterator out);
template <size_t Ds, typename T> std::vector<T> LvBA_Ds(const std::vector<T>& A, const std::vector<T>& B);
template <size_t Ds, typename T> void LvBA_Ds_impl(typename std::vector<T>::const_iterator A, typename std::vector<T>::const_iterator B,typename std::vector<T>::iterator out);


template <unsigned Ds, typename T,bool t> std::ostream& operator<<(std::ostream&, const Caravel::clifford_algebra::sparse_smatrix<Ds,T,t>&);
std::ostream& operator<<(std::ostream&, const Caravel::clifford_algebra::sEntry&);
template <unsigned D> std::ostream& operator<<(std::ostream&, const Caravel::clifford_algebra::Gamma<D>&);


/**
 * D-dimensional spinor products. SpinorData is a momentum of spinor and its Dtt-index (embedding in higher dimensions)
 * All external spinors here are (4D) Weyl spinors embedded into a tensor product representation, which assumes that momenta are in 4D!
 */
template <typename T,size_t Ds> using SpinorData = std::pair<momentumD<T,Ds>,int>;

template <typename T, size_t Ds,typename... Ts> T spaa(const SpinorData<T,Ds>& s, Ts... args);
template <typename T, size_t Ds,typename... Ts> T spab(const SpinorData<T,Ds>& s, Ts... args);
template <typename T, size_t Ds,typename... Ts> T spba(const SpinorData<T,Ds>& s, Ts... args);
template <typename T, size_t Ds,typename... Ts> T spbb(const SpinorData<T,Ds>& s, Ts... args);

}}

#include "clifford_algebra.hpp"

#endif
