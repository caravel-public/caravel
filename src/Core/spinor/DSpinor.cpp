#include "DSpinor.h"

namespace Caravel {
namespace clifford_algebra {
/**
 * Instantiations for static members
 */
#define _inst_a(D,T) \
  template <> std::vector<T> DSpinor_Up(const momentumD<T,D>& p, const unsigned& Dtt_index, void*); \
  template <> std::vector<T> DSpinor_Um(const momentumD<T,D>& p, const unsigned& Dtt_index, void*); \
  template <> std::vector<T> DSpinor_Vp(const momentumD<T,D>& p, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Vm(const momentumD<T,D>& p, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Ubarp(const momentumD<T,D>& p, const unsigned& Dtt_index, void*); \
  template <> std::vector<T> DSpinor_Ubarm(const momentumD<T,D>& p, const unsigned& Dtt_index, void*); \
  template <> std::vector<T> DSpinor_Vbarp(const momentumD<T,D>& p, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Vbarm(const momentumD<T,D>& p, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Up(const momentumD<T,D>& p, const momentumD<T,D>& q, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Um(const momentumD<T,D>& p, const momentumD<T,D>& q, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Vp(const momentumD<T,D>& p, const momentumD<T,D>& q, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Vm(const momentumD<T,D>& p, const momentumD<T,D>& q, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Ubarp(const momentumD<T,D>& p, const momentumD<T,D>& q, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Ubarm(const momentumD<T,D>& p, const momentumD<T,D>& q, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Vbarp(const momentumD<T,D>& p, const momentumD<T,D>& q, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_Vbarm(const momentumD<T,D>& p, const momentumD<T,D>& q, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_cut_Qp(const momentumD<T,D>& p, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_cut_Qm(const momentumD<T,D>& p, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_cut_Qbp(const momentumD<T,D>& p, const T& mass, const unsigned& Dtt_index); \
  template <> std::vector<T> DSpinor_cut_Qbm(const momentumD<T,D>& p, const T& mass, const unsigned& Dtt_index); 

#define _inst_ds(T) \
   _inst_a(4,T) \
   _inst_a(6,T) \
   _inst_a(8,T) \
   _inst_a(10,T) \
   _inst_a(12,T) \
   _inst_a(14,T) 

_inst_ds(R)
_inst_ds(C)

#ifdef HIGH_PRECISION
_inst_ds(RHP)
_inst_ds(CHP)
#endif

#ifdef VERY_HIGH_PRECISION
_inst_ds(RVHP)
_inst_ds(CVHP)
#endif

#ifdef USE_FINITE_FIELDS
_inst_ds(F32)
#ifdef INSTANTIATE_RATIONAL
_inst_ds(BigRat)
#endif
#endif


#undef _inst_a
#undef _inst_ds
} // namespace clifford_algebra
} // namespace Caravel
