/** 
 * @file splib_int.cpp
 *
 * @brief Implementations of spinor constructors and instantioations for floating point arithmetics
 *
 */
#include "spinor.h"
#include "Utilities.h"

namespace Caravel{

template <class T> template <size_t D,typename TT> la<T>::la(const momentumD<T,D>& p, typename std::enable_if<is_complex<TT>::value>::type*){
    static_assert(D>=4,"An attempt to use D<4 momentumD.");
    //_WARNING_R("Integer type spinors are not normalized!");
    using _spinor_private::plus;

    a[0]=plus(p);
    a[1]=p[1]+T(0,1)*p[2];
}

template <class T> template <size_t D,typename TT> lat<T>::lat(const momentumD<T,D>& p, typename std::enable_if<is_complex<TT>::value>::type*){
    static_assert(D>=4,"An attempt to use D<4 momentumD.");
    //_WARNING_R("Integer type spinors are not normalized!");
    using _spinor_private::plus;

    a[0]=plus(p);
    a[1]=p[1]-T(0,1)*p[2];
}

template <class T> template <size_t D, typename TT> la<T>::la(const momentumD<T, D>& p, typename std::enable_if<!is_complex<TT>::value>::type*) {
    static_assert(D >= 4, "An attempt to use D<4 momentumD.");
    using _spinor_private::plus;

    a[0] = plus(p);
    a[1] = p[1] - p[2];
}
template <class T> template <size_t D, typename TT> lat<T>::lat(const momentumD<T, D>& p, typename std::enable_if<!is_complex<TT>::value>::type*) {
    static_assert(D >= 4, "An attempt to use D<4 momentumD.");
    using _spinor_private::plus;

    a[0] = T(1);
    a[1] = (p[1] + p[2]) / plus(p);
}

#ifdef USE_FINITE_FIELDS
    _INSTANTIATE_SPLIB(F32)
#endif

#ifdef INSTANTIATE_RATIONAL
    _INSTANTIATE_SPLIB(BigRat)
#endif
}

