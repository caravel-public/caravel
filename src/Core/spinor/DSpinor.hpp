#include "Core/spinor/dirac_algebra.h"

#define BLACKHAT_PHASE 1
// This fixes V(p,s) and Ubar(p,s) to have the same phase as Blackhat
// The phases of Vbar(p,s) and U(p,s) are then fixed by the polarization sum.

namespace Caravel {
namespace clifford_algebra {
namespace DSpinor_internal {

/**
 *  Lightcone decomposition of p of mass 'mass' with respect to q (q^2=0)
 */
template <typename T, size_t Ds, typename TE> momentumD<T, Ds> lightcone_decompose(const momentumD<TE, Ds>& p, const momentumD<TE,Ds>& q, const TE& mass) {
    static_assert(Ds >= 4);

    // Fixed auxiliary momentum n
    // momentumD<T, Ds> n = get_helper_momD<T, Ds>(1);

    momentumD<T, Ds> p4(p[0], p[1], p[2], p[3]);
    momentumD<T, Ds> q4(q[0], q[1], q[2], q[3]);
    T p4sq = mass * mass - compute_Dsm4_prod(p, p);
 
    // The flattend momentum four momentum p4f with p4f^2 = 0
    momentumD<T, Ds> p4f = p4 - p4sq / (T(2) * T(q4 * p)) * q4;

    return p4f;
}

template <typename T, bool takesqrt = true> std::enable_if_t<!is_complex<T>::value, std::vector<T>> normalize(const std::vector<T>& in, const T& x) {
    return in;
}

template <typename T, bool takesqrt = true> std::enable_if_t<is_complex<T>::value, std::vector<T>> normalize(const std::vector<T>& in, const T& x) {
    std::vector<T> out = in;

    if (takesqrt)
        for (unsigned n = 0; n < in.size(); n++) out[n] /= sqrt(x);
    else
        for (unsigned n = 0; n < in.size(); n++) out[n] /= x;

    return out;
}

template <typename T, typename TE> std::enable_if_t<is_complex<T>::value, std::vector<T>> cut_normalize(const std::vector<T>& in, const TE& x) { return in; }
template <typename T, typename TE> std::enable_if_t<!is_complex<T>::value, std::vector<T>> cut_normalize(const std::vector<T>& in, const TE& x) {
    std::vector<T> out = in;
    for (unsigned n = 0; n < in.size(); n++) out[n] = out[n] / x;
    return out;
}

} // namespace DSpinor_internal

/**
 * Function that returns pre-defined massless auxilliary vectors depending on the signature of the metric
 */
template <typename T, size_t Ds, typename TT> momentumD<T, Ds> get_helper_momD(int n, typename std::enable_if<is_complex<TT>::value>::type*) {
    switch (n) {
        case 1: return momentumD<T, Ds>(sqrt(T(14) / T(9)), T(1), T(2) / T(3), T(1) / T(3)); // Standard BlackHat reference vector
        case 2: return momentumD<T, Ds>(T(5), T(4), T(3), T(0));
        default: std::cout << "[get_helper_momD]: Unkown index for helper momentum: n = " << n << std::endl; std::exit(1);
    }
}

template <typename T, size_t Ds, typename TT> momentumD<T, Ds> get_helper_momD(int n, typename std::enable_if<!is_complex<TT>::value>::type*) {
    switch (n) {
        case 1: return momentumD<T, Ds>(T(4), T(4), T(5), T(5));
        default: std::cout << "[get_helper_momD]: Unkown index for helper momentum: n = " << n << std::endl; std::exit(1);
    }
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Type: U, Polarisation: +
 *
 * Assumtions: The external massless momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TT> std::vector<T> DSpinor_Up(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<is_complex<TT>::value>::type*) {
    static_assert(Ds >= 4, "Dirac Spinors only implemented for Ds >=4!");
    using _spinor_private::plus;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    std::vector<T> out(Dt, T(0));

    // Constructing the 4D external state u_+(p) for p^2 = 0
    std::vector<T> s(4, T(0));
    const T sqpl = sqrt(abs(plus(p)));

    s[2] = sqpl;
    s[3] = sqpl * (p[1] + T(0, 1) * p[2]) / plus(p);
#if WEYL == 0
    // An overall factor of 1/sqrt(2) is factored out and reincluded in Builder_fixed_DS::get_overall_normalization_info(.) 
    s[0] = s[2];
    s[1] = s[3];
#endif

    // Constructing the Dt dimensional spinor for Ds vector dimensions
    out[4 * Dtt_index - 4] = s[0];
    out[4 * Dtt_index - 3] = s[1];
    out[4 * Dtt_index - 2] = s[2];
    out[4 * Dtt_index - 1] = s[3];

    return out;
}

template <typename T, size_t Ds, typename TT> std::vector<T> DSpinor_Up(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<!is_complex<TT>::value>::type*) {
    static_assert(Ds >= 4, "Dirac Spinors only implemented for Ds >=4!");
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    std::vector<T> out(Dt, T(0));

    // Constructing the 4D external state u_+(p) for p^2 = 0
    std::vector<T> s(4, T(0));

    // We do NOT include the normalisation of Sqrt[p0+p3]
    s[2] = p[0] + p[3];
    s[3] = p[1] - p[2];
#if WEYL == 0
    // An overall factor of 1/sqrt(2) is factored out and reincluded in Builder_fixed_DS::get_overall_normalization_info(.) 
    s[0] = s[2];
    s[1] = s[3];
#endif

#if DEBUG_ON
    if (s[2] == T(0) and s[3] == T(0)) { std::cout << "WARNING: DSpinor_Up is exactly zero! p = " << p << std::endl; }
#endif

    // Constructing the Dt dimensional spinor for Ds vector dimensions
    out[4 * Dtt_index - 4] = s[0];
    out[4 * Dtt_index - 3] = s[1];
    out[4 * Dtt_index - 2] = s[2];
    out[4 * Dtt_index - 1] = s[3];

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Type: U, Polarisation: -
 *
 * Assumtions: The external massless momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TT> std::vector<T> DSpinor_Um(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<is_complex<TT>::value>::type*) {
    static_assert(Ds>=4,"Dirac Spinors only implemented for Ds >=4!");
    using _spinor_private::plus;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    std::vector<T> out(Dt, T(0));


    // Constructing the 4D external state u_-(p) for p^2 = 0
    std::vector<T> s(4, T(0));
    const T sqpl = sqrt(abs(plus(p)));

    s[0] = (p[1] - T(0, 1) * p[2]) / sqpl;
    s[1] = -(p[0] + p[3]) / sqpl;
#if WEYL == 0
    // An overall factor of 1/sqrt(2) is factored out and reincluded in Builder_fixed_DS::get_overall_normalization_info(.) 
    s[2] = -s[0];
    s[3] = -s[1];
#endif

    // Constructing the Dt dimensional spinor for Ds vector dimensions
    out[4 * Dtt_index - 4] = s[0];
    out[4 * Dtt_index - 3] = s[1];
    out[4 * Dtt_index - 2] = s[2];
    out[4 * Dtt_index - 1] = s[3];

    return out;
}
template <typename T, size_t Ds, typename TT> std::vector<T> DSpinor_Um(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<!is_complex<TT>::value>::type*) {
    static_assert(Ds>=4,"Dirac Spinors only implemented for Ds >=4!");
    constexpr unsigned Dt = two_to_the(Ds / 2);
    std::vector<T> out(Dt, T(0));
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // Constructing the 4D external state u_-(p) for p^2 = 0
    std::vector<T> s(4, T(0));

    // We do NOT include the normalisation of Sqrt[p0+p3]
    s[0] = p[1] + p[2];
    s[1] = -(p[0] + p[3]);
#if WEYL == 0
    // An overall factor of 1/sqrt(2) is factored out and reincluded in Builder_fixed_DS::get_overall_normalization_info(.) 
    s[2] = -s[0];
    s[3] = -s[1];
#endif

#if DEBUG_ON
    if (s[0] == T(0) and s[1] == T(0)) { std::cout << "WARNING: DSpinor_Um is exactly zero! p = " << p << std::endl; }
#endif

    // Constructing the Dt dimensional spinor for Ds vector dimensions
    out[4 * Dtt_index - 4] = s[0];
    out[4 * Dtt_index - 3] = s[1];
    out[4 * Dtt_index - 2] = s[2];
    out[4 * Dtt_index - 1] = s[3];

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Type: Ubar, Polarisation: +
 *
 * Assumtions: The external massless momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TT> std::vector<T> DSpinor_Ubarp(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<is_complex<TT>::value>::type*) {
    static_assert(Ds>=4,"Dirac Spinors only implemented for Ds >=4!");
    using _spinor_private::plus;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    std::vector<T> out(Dt, T(0));
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // Constructing the 4D external state ubar_+(p) for p^2 = 0
    std::vector<T> s(4, T(0));
    const T sqpl = sqrt(abs(plus(p)));

    s[0] = plus(p) / sqpl;
    s[1] = (p[1] - T(0, 1) * p[2]) / sqpl;
#if WEYL == 0
    // An overall factor of 1/sqrt(2) is factored out and reincluded in Builder_fixed_DS::get_overall_normalization_info(.) 
    s[2] = -s[0];
    s[3] = -s[1];
#endif

    // Constructing the Dt dimensional spinor for Ds vector dimensions
    out[4 * Dtt_index - 4] = s[0];
    out[4 * Dtt_index - 3] = s[1];
    out[4 * Dtt_index - 2] = s[2];
    out[4 * Dtt_index - 1] = s[3];

    return out;
}

template <typename T, size_t Ds, typename TT> std::vector<T> DSpinor_Ubarp(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<!is_complex<TT>::value>::type*) {
    static_assert(Ds>=4,"Dirac Spinors only implemented for Ds >=4!");
    constexpr unsigned Dt = two_to_the(Ds / 2);
    std::vector<T> out(Dt, T(0));
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // Constructing the 4D external state ubar_+(p) for p^2 = 0
    std::vector<T> s(4, T(0));

    // We do NOT include the normalisation of Sqrt[p0+p3]
    s[0] = p[0] + p[3];
    s[1] = p[1] + p[2];
#if WEYL == 0
    // An overall factor of 1/sqrt(2) is factored out and reincluded in Builder_fixed_DS::get_overall_normalization_info(.) 
    s[2] = -s[0];
    s[3] = -s[1];
#endif

#if DEBUG_ON
    if (s[0] == T(0) and s[1] == T(0)) { std::cout << "WARNING: DSpinor_Ubarp is exactly zero! p = " << p << std::endl; }
#endif

    // Constructing the Dt dimensional spinor for Ds vector dimensions
    out[4 * Dtt_index - 4] = s[0];
    out[4 * Dtt_index - 3] = s[1];
    out[4 * Dtt_index - 2] = s[2];
    out[4 * Dtt_index - 1] = s[3];

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Type: Ubar, Polarisation: -
 *
 * Assumtions: The external massless momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TT> std::vector<T> DSpinor_Ubarm(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<is_complex<TT>::value>::type*) {
    static_assert(Ds>=4,"Dirac Spinors only implemented for Ds >=4!");
    using _spinor_private::plus;
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    std::vector<T> out(Dt, T(0));


    // Constructing the 4D external state ubar_-(p) for p^2 = 0
    std::vector<T> s(4, T(0));
    const T sqpl = sqrt(abs(plus(p)));

    s[2] = sqpl * (p[1] + T(0, 1) * p[2]) / plus(p);
    s[3] = -sqpl;
#if WEYL == 0
    // An overall factor of 1/sqrt(2) is factored out and reincluded in Builder_fixed_DS::get_overall_normalization_info(.) 
    s[0] = s[2];
    s[1] = s[3];
#endif

    // Constructing the Dt dimensional spinor for Ds vector dimensions
    out[4 * Dtt_index - 4] = s[0];
    out[4 * Dtt_index - 3] = s[1];
    out[4 * Dtt_index - 2] = s[2];
    out[4 * Dtt_index - 1] = s[3];

    return out;
}
template <typename T, size_t Ds, typename TT> std::vector<T> DSpinor_Ubarm(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<!is_complex<TT>::value>::type*) {
    static_assert(Ds>=4,"Dirac Spinors only implemented for Ds >=4!");
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    std::vector<T> out(Dt, T(0));


    // Constructing the 4D external state ubar_-(p) for p^2 = 0
    std::vector<T> s(4, T(0));

    // We do NOT include the normalisation of Sqrt[p0+p3]
    s[2] = p[1] - p[2];
    s[3] = -(p[0] + p[3]);
#if WEYL == 0
    // An overall factor of 1/sqrt(2) is factored out and reincluded in Builder_fixed_DS::get_overall_normalization_info(.) 
    s[0] = s[2];
    s[1] = s[3];
#endif


#if DEBUG_ON
    if (s[2] == T(0) and s[3] == T(0)) { std::cout << "WARNING: DSpinor_Ubarm is exactly zero! p = " << p << std::endl; }
#endif

    // Constructing the Dt dimensional spinor for Ds vector dimensions
    out[4 * Dtt_index - 4] = s[0];
    out[4 * Dtt_index - 3] = s[1];
    out[4 * Dtt_index - 2] = s[2];
    out[4 * Dtt_index - 1] = s[3];

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Implements: U_+(p) = (p_slash + m)*u_-(q)/sqrt(2*p*q)
 * Assumtions: The external massive momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = mass^2
 *             The reference vector q is confined in Ds = 4
 *             i.e q^2_[Ds] = q^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Up(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // 1) out_i = p_mu * [gamma^mu]_ij * ref[j]
    std::vector<T> ref = DSpinor_Um<T, Ds>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::R, T>(p.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) { out[i] += mass * ref[i]; }

    // 3) Normalize
#if BLACKHAT_PHASE == 0
    out = DSpinor_internal::normalize(out, T(2) * T(p * q));
#else
    const momentumD<T, 4> q4(q[0], q[1], q[2], q[3]);
    const momentumD<T, 4> pf = DSpinor_internal::lightcone_decompose<T, 4, T>(momentumD<T, 4>(p[0], p[1], p[2], p[3]), q4, mass);
    out = DSpinor_internal::normalize<T, false>(out, spbb(pf, q4));
#endif

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Implements: U_-(p) = (p_slash + m)*u_+(q)/sqrt(2*p*q)
 * Assumtions: The external massive momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = mass^2
 *             The reference vector q is confined in Ds = 4
 *             i.e q^2_[Ds] = q^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Um(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // 1) out_i = p_mu * [gamma^mu]_ij * ref[j]
    std::vector<T> ref = DSpinor_Up<T, Ds>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::R, T>(p.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) { out[i] += mass * ref[i]; }

    // 3) Normalize
#if BLACKHAT_PHASE == 0
    out = DSpinor_internal::normalize(out, T(2) * T(p * q));
#else
    const momentumD<T, 4> q4(q[0], q[1], q[2], q[3]);
    const momentumD<T, 4> pf = DSpinor_internal::lightcone_decompose<T, 4, T>(momentumD<T, 4>(p[0], p[1], p[2], p[3]), q4, mass);
    out = DSpinor_internal::normalize<T, false>(out, spaa(pf, q4));
#endif

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Implements: Ubar_+(p) = ubar_-(q)*(p_slash + m)/sqrt(2*p*q)
 * Assumtions: The external massive momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = mass^2
 *             The reference vector q is confined in Ds = 4
 *             i.e q^2_[Ds] = q^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Ubarp(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // 1) out_j = p_mu * aux[i] * [gamma^mu]_ij
    std::vector<TE> ref = DSpinor_Ubarm<TE>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::L,T>(p.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) { out[i] += mass * ref[i]; }

    // 3) Normalize
#if BLACKHAT_PHASE == 0
    out = DSpinor_internal::normalize(out, T(2) * T(p * q));
#else
    const momentumD<T, 4> q4(q[0], q[1], q[2], q[3]);
    const momentumD<T, 4> pf = DSpinor_internal::lightcone_decompose<T, 4, T>(momentumD<T, 4>(p[0], p[1], p[2], p[3]), q4, mass);
    out = DSpinor_internal::normalize<T, false>(out, spaa(q4, pf));
#endif

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Implements: Ubar_-(p) = ubar_+(q)*(p_slash + m)/sqrt(2*p*q)
 * Assumtions: The external massive momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = mass^2
 *             The reference vector q is confined in Ds = 4
 *             i.e q^2_[Ds] = q^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Ubarm(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // 1) out_j = p_mu * aux[i] * [gamma^mu]_ij
    std::vector<T> ref = DSpinor_Ubarp<T>(q,Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::L,T>(p.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) { out[i] += mass * ref[i]; }

    // 3) Normalize
#if BLACKHAT_PHASE == 0
    out = DSpinor_internal::normalize(out, T(2) * T(p * q));
#else
    const momentumD<T, 4> q4(q[0], q[1], q[2], q[3]);
    const momentumD<T, 4> pf = DSpinor_internal::lightcone_decompose<T, 4, T>(momentumD<T, 4>(p[0], p[1], p[2], p[3]), q4, mass);
    out = DSpinor_internal::normalize<T, false>(out, spbb(q4, pf));
#endif

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Implements: V_+(p) = (p_slash - m)*v_-(q)/sqrt(2*p*q)
 * Assumtions: The external massive momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = mass^2
 *             The reference vector q is confined in Ds = 4
 *             i.e q^2_[Ds] = q^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Vp(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // 1) out_i = p_mu * [gamma^mu]_ij * ref[j]
    std::vector<T> ref = DSpinor_Vm<T>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::R, T>(p.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) { out[i] -= mass * ref[i]; }

    // 3) Normalize
#if BLACKHAT_PHASE == 0
    out = DSpinor_internal::normalize(out, T(2) * T(p * q));
#else
    const momentumD<T, 4> q4(q[0], q[1], q[2], q[3]);
    const momentumD<T, 4> pf = DSpinor_internal::lightcone_decompose<T, 4, T>(momentumD<T, 4>(p[0], p[1], p[2], p[3]), q4, mass);
    out = DSpinor_internal::normalize<T, false>(out, spaa(q4, pf));
#endif

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Implements: V_-(p) = (p_slash - m)*v_+(q)/sqrt(2*p*q)
 * Assumtions: The external massive momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = mass^2
 *             The reference vector q is confined in Ds = 4
 *             i.e q^2_[Ds] = q^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Vm(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // 1) out_i = p_mu * [gamma^mu]_ij * ref[j]
    std::vector<T> ref = DSpinor_Vp<T, Ds>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::R, T>(p.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) { out[i] -= mass * ref[i]; }

    // 3) Normalize
#if BLACKHAT_PHASE == 0
    out = DSpinor_internal::normalize(out, T(2) * T(p * q));
#else
    const momentumD<T, 4> q4(q[0], q[1], q[2], q[3]);
    const momentumD<T, 4> pf = DSpinor_internal::lightcone_decompose<T, 4, T>(momentumD<T, 4>(p[0], p[1], p[2], p[3]), q4, mass);
    out = DSpinor_internal::normalize<T, false>(out, spbb(pf, q4));
#endif

    return out;
}

/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Implements: Vbar_+(p) = vbar_-(q)*(p_slash - m)/sqrt(2*p*q)
 * Assumtions: The external massive momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = mass^2
 *             The reference vector q is confined in Ds = 4
 *             i.e q^2_[Ds] = q^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Vbarp(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // 1) out_j = p_mu * aux[i] * [gamma^mu]_ij
    std::vector<T> ref = DSpinor_Vbarm<T>(q,Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::L,T>(p.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) { out[i] -= mass * ref[i]; }

    // 3) Normalize
#if BLACKHAT_PHASE == 0
    out = DSpinor_internal::normalize(out, T(2) * T(p * q));
#else
    const momentumD<T, 4> q4(q[0], q[1], q[2], q[3]);
    const momentumD<T, 4> pf = DSpinor_internal::lightcone_decompose<T, 4, T>(momentumD<T, 4>(p[0], p[1], p[2], p[3]), q4, mass);
    out = DSpinor_internal::normalize<T, false>(out, spbb(pf, q4));
#endif

    return out;
}
/**
 * Construction of a Dt dimensional spinor for Ds vector dimensions
 * Implements: Vbar_-(p) = vbar_+(q)*(p_slash - m)/sqrt(2*p*q)
 * Assumtions: The external massive momentum p is confined to Ds = 4
 *             i.e. p^2_[Ds] = p^2_[4] = mass^2
 *             The reference vector q is confined in Ds = 4
 *             i.e q^2_[Ds] = q^2_[4] = 0
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Vbarm(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);

    // 1) out_j = p_mu * aux[i] * [gamma^mu]_ij
    std::vector<TE> ref = DSpinor_Vbarp<TE>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::L,T>(p.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) { out[i] -= mass * ref[i]; }

    // 3) Normalize
#if BLACKHAT_PHASE == 0
    out = DSpinor_internal::normalize(out, T(2) * T(p * q));
#else
    const momentumD<T, 4> q4(q[0], q[1], q[2], q[3]);
    const momentumD<T, 4> pf = DSpinor_internal::lightcone_decompose<T, 4, T>(momentumD<T, 4>(p[0], p[1], p[2], p[3]), q4, mass);
    out = DSpinor_internal::normalize<T, false>(out, spaa(q4, pf));
#endif

    return out;
}

/**
 * Cut-Wavefunctions
 * outgoing cut-fermion Chibar(p,s) = Ubar(-p,s)
 * outgoing cut-anti-fermion Psi(p,s) = U(p,s)
 * Note: We push the normalization into Psi in order to avoid annoying phases
 * 
 * DSpinor_cut_Qp  = Psi(p,+),    DSpinor_cut_Qm  = Psi(p,-)
 * Dspinor_cut_Qbp = Chibar(p,+), DSpinor_cut_Qbm = Chibar(p,-)
 **/
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_cut_Qbp(const momentumD<TE, Ds>& p, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    const momentumD<TE, Ds> l = -p;
    const momentumD<TE, Ds> n = get_helper_momD<TE, Ds>(1);
    const momentumD<TE, Ds> q = DSpinor_internal::lightcone_decompose<TE, Ds, TE>(l, n, mass);

    // 1) out_i = p_mu * [gamma^mu]_ij * ref[j]
    std::vector<TE> ref = DSpinor_Um<TE>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::R,T>(l.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) {
        out[i] += mass * ref[i];
        out[i] /= (TE(2) * TE(l * q));
    }

    // 3) if T = non_complex we also have to divide by the normalisation of the aux spinor
#if WEYL
    out = DSpinor_internal::cut_normalize(out, TE(q[0] + q[3]));
#else
    out = DSpinor_internal::cut_normalize(out, TE(2)*TE(q[0] + q[3]));
#endif


    return out;
}
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_cut_Qbm(const momentumD<TE, Ds>& p, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    const momentumD<TE, Ds> l = -p;
    const momentumD<TE, Ds> n = get_helper_momD<TE, Ds>(1);
    const momentumD<TE, Ds> q = DSpinor_internal::lightcone_decompose<TE, Ds, TE>(l, n, mass);

    // 1) out_i = p_mu * [gamma^mu]_ij * ref[j]
    std::vector<TE> ref = DSpinor_Up<TE>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::R,T>(l.get_components(), ref);

    // 2) out_i += mass * ref[i] and normalize
    for (unsigned i = 0; i < Dt; i++) {
        out[i] += mass * ref[i];
        out[i] /= (TE(2) * TE(l * q));
    }

    // 3) if T = non_complex we also have to divide by the normalisation of the aux spinor
#if WEYL
    out = DSpinor_internal::cut_normalize(out, TE(q[0] + q[3]));
#else
    out = DSpinor_internal::cut_normalize(out, TE(2)*TE(q[0] + q[3]));
#endif

    return out;
}
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_cut_Qp(const momentumD<TE, Ds>& p, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    const momentumD<TE, Ds> l = p;
    const momentumD<TE, Ds> n = get_helper_momD<TE, Ds>(1);
    const momentumD<TE, Ds> q = DSpinor_internal::lightcone_decompose<TE, Ds, TE>(l, n, mass);

    // 1) out_j = p_mu * aux[i] * [gamma^mu]_ij
    std::vector<TE> ref = DSpinor_Ubarm<TE>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::L,T>(l.get_components(), ref);

    // 2) out_i += mass * ref[i]
    for (unsigned i = 0; i < Dt; i++) { out[i] += mass * ref[i]; }

    return out;
}
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_cut_Qm(const momentumD<TE, Ds>& p, const TE& mass, const unsigned& Dtt_index) {
    constexpr unsigned Dt = two_to_the(Ds / 2);
    assert(Dtt_index <= Dt/4 && Dtt_index > 0);
    const momentumD<TE, Ds> l = p;
    const momentumD<TE, Ds> n = get_helper_momD<TE, Ds>(1);
    const momentumD<TE, Ds> q = DSpinor_internal::lightcone_decompose<TE, Ds, TE>(l, n, mass);

    // 1) out_j = p_mu * aux[i] * [gamma^mu]_ij
    std::vector<TE> ref = DSpinor_Ubarp<TE>(q, Dtt_index);
    std::vector<T> out = multiply_sparse_DGamma<mult_from::L,T>(l.get_components(), ref);

    // 2) out_i += mass * ref[i]
    for (unsigned i = 0; i < Dt; i++) { out[i] += mass * ref[i]; }

    return out;
}

} // namespace clifford_algebra
} // namespace Caravel
