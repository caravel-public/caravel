/** 
 * @file splib_float.cpp
 *
 * @brief Implementations of spinor constructors and instantioations for floating point arithmetics
 *
 */
#include "spinor.h"
#include "Core/typedefs.h"
#include "misc/DeltaZero.h"
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif


namespace Caravel{

// Implementation of spinors from real momenta, might be useful later...

//template <class T> _INLINE void LaLat(const momentum<T>& p,la<T>& la_p,lat<T>& lat_p){
    //using std::abs;
    //if (abs(plus(p))<DeltaZeroSqr<T>()){
        ////		std::cout << "USING DANGEROUS PHASE!" << std::endl;
        //if (abs(minus(p))<DeltaZeroSqr<T>()) {
            //lat_p=lat<T>((p[1]+std::complex<T>(0.,1.)*p[2])/sqrt(T(2)*p[1]),(p[1]-std::complex<T>(0.,1.)*p[2])/sqrt(T(2)*p[1]));
            //la_p =la<T> ((p[1]-std::complex<T>(0.,1.)*p[2])/sqrt(T(2)*p[1]),(p[1]+std::complex<T>(0.,1.)*p[2])/sqrt(T(2)*p[1]));
        //}
        //else {
            //std::complex<T> sqm=sqrt(std::complex<T>(minus(p),0));
            //lat_p=lat<T>(std::complex<T>(1.,0.)/sqm*(p[1]+std::complex<T>(0.,1.)*p[2]),sqm);
            //la_p=la<T>(std::complex<T>(1.,0.)/sqm*(p[1]-std::complex<T>(0.,1.)*p[2]),sqm);
        //}
    //}
    //else {
//#if _OLD_PHASE_CONVENTION
        //std::complex<T> sqk1p=sqrt(std::complex<T>(plus(p),0));std::complex<T> inv=std::complex<T>(1.,0.)/sqk1p;
        //lat_p=lat<T>(sqk1p,inv*(p[1]-std::complex<T>(0.,1.)*p[2]));
        //la_p=la<T>(sqk1p,inv*(p[1]+std::complex<T>(0.,1.)*p[2]));
//#else
        //T sqk1p=sqrt(abs(plus(p))); T inv=T(1)/sqk1p;
        //lat_p=lat<T>(std::complex<T>(plus(p),T(0))*inv,std::complex<T>(inv*p[1],-inv*p[2]));
        //la_p=la<T>(std::complex<T>(sqk1p,T(0)),sqk1p*std::complex<T>(p[1],p[2])/plus(p));
//#endif
    //}
//}

template <class T> template <size_t D,typename TT> la<T>::la(const momentumD<T,D>& p, typename std::enable_if<is_complex<TT>::value>::type*){
    using _spinor_private::plus;
    using _spinor_private::minus;
    static_assert(D>=4,"An attempt to use D<4 momentumD.");

    if ((plus(p)*conj(plus(p))).real()<DeltaZero<typename T::value_type>())
        if ((minus(p)*conj(minus(p))).real()<DeltaZero<typename T::value_type>()){
            a[0] = (p[1]-T(0.,1.)*p[2])/sqrt(T(2)*p[1]);
            a[1] = (p[1]+T(0.,1.)*p[2])/sqrt(T(2)*p[1]);
        }
        else {
            T sqm=sqrt(minus(p)); 
            a[0] = T(1.,0.)/sqm*(p[1]-T(0.,1.)*p[2]);
            a[1] = sqm;
        }
    else {
#if _OLD_PHASE_CONVENTION
        T sqk1p=sqrt(plus(p));
        a[0] = sqk1p;
        a[1] = T(1.)/sqk1p*(p[1]+T(0.,1.)*p[2]);
#else
        T sqk1p=sqrt(abs(plus(p)));
        a[0] = sqk1p;
        a[1] = sqk1p*(p[1]+T(0.,1.)*p[2])/plus(p);
#endif
    }
}

template <class T> template <size_t D,typename TT> lat<T>::lat(const momentumD<T,D>& p, typename std::enable_if<is_complex<TT>::value>::type*){
    using _spinor_private::plus;
    using _spinor_private::minus;
    static_assert(D>=4,"An attempt to use D<4 momentumD.");

    if ((plus(p)*conj(plus(p))).real()< DeltaZero<typename T::value_type>()){
        if ((minus(p)*conj(minus(p))).real()<DeltaZero<typename T::value_type>()){
            a[0] = (p[1]+T(0.,1.)*p[2])/sqrt(T(2)*p[1]);
            a[1] = (p[1]-T(0.,1.)*p[2])/sqrt(T(2)*p[1]);
        }
        else {
            T sqm=sqrt(minus(p));
            a[0] = T(1.,0.)/sqm*(p[1]+T(0.,1.)*p[2]);
            a[1] = sqm;
        }
    }
    else {
#if _OLD_PHASE_CONVENTION
        T sqk1p=sqrt(plus(p));
        a[0] = sqk1p;
        a[1] = T(1.,0.)/sqk1p*(p[1]-T(0.,1.)*p[2]);
#else
        T sqk1p=sqrt(abs(plus(p)));
        T inv=T(1.)/sqk1p;
        a[0] = plus(p)*inv;
        a[1] = inv*(p[1]-T(0.,1.)*p[2]);
#endif
    }
}

template <class T> template <size_t D,typename TT> la<T>::la(const momentumD<T,D>& p, typename std::enable_if<!is_complex<TT>::value>::type*){
    using _spinor_private::plus;
    using _spinor_private::minus;
    static_assert(D>=4,"An attempt to use D<4 momentumD.");
    using std::abs;

    if (abs(plus(p))<DeltaZero<T>()){
        if (abs(minus(p))<DeltaZero<T>()){
            a[0] = (p[1]-p[2])/sqrt(T(2)*p[1]);
            a[1] = (p[1]+p[2])/sqrt(T(2)*p[1]);
        }
        else {
            _WARNING_R("Normalization is not checked!");
            T sqm=sqrt(abs(minus(p)));
            a[0] = T(1)/sqm*(p[1]-p[2]);
            a[1] = sqm;
        }
    }
    else {
        T sqk1p=sqrt(abs(plus(p)));
        a[0] = sqk1p;
        a[1] = sqk1p*(p[1]+p[2])/plus(p);
    }
}

template <class T> template <size_t D,typename TT> lat<T>::lat(const momentumD<T,D>& p, typename std::enable_if<!is_complex<TT>::value>::type*){
    using _spinor_private::plus;
    using _spinor_private::minus;
    static_assert(D>=4,"An attempt to use D<4 momentumD.");
    using std::abs;

    if (abs(plus(p))<DeltaZero<T>()){
        if (abs(minus(p))<DeltaZero<T>()){
            a[0] = (p[1]+p[2])/sqrt(T(2)*p[1]);
            a[1] = (p[1]-p[2])/sqrt(T(2)*p[1]);
        }
        else {
            _WARNING_R("Normalization is not checked!");
            T sqm=sqrt(abs(minus(p)));
            a[0] = T(1)/sqm*(p[1]+p[2]);
            a[1] = sqm;
        }
    }
    else {
        T sqk1p=sqrt(abs(plus(p)));
        T inv=T(1)/sqk1p;
        a[0] = plus(p)*inv;
        a[1] = inv*(p[1]-p[2]);
    }
}


// Instantiate
_INSTANTIATE_SPLIB(C)
#ifdef HIGH_PRECISION
_INSTANTIATE_SPLIB(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_SPLIB(CVHP)
#endif
_INSTANTIATE_SPLIB(R)
#ifdef HIGH_PRECISION
_INSTANTIATE_SPLIB(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_SPLIB(RVHP)
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_SPLIB(RGMP)
_INSTANTIATE_SPLIB(CGMP)
#endif

}

