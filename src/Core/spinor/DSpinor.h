/*
 * @file DSpinor.h
 *
 * @brief Implementation of Dirac spinors for generic Ds dimensions
 *
 * This class contains the implementation of massless and massive Dirac-Spinors
 * for all cases U, V, Ubar, Vbar and all polarisations.
 * Dirac Spinors are implemented for both representations: Weyl and Dirac (see dirac_algebra.h)
 *
 * The massive spinor implementation can be directly used to construct complex cut-wavefunctions.
 *
 * In order to derive the spinors for the real representation we take
 * (p.sigma) = {{ p0-p3, -p1-p2 }, { -p1+p2, p0+p3 }}
 */
#ifndef _DSPINOR_H
#define _DSPINOR_H
#include "Core/spinor/dirac_algebra.h"

namespace Caravel {
namespace clifford_algebra {

template <typename T, size_t Ds, typename TT = T> momentumD<T, Ds> get_helper_momD(int n, typename std::enable_if<is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds, typename TT = T> momentumD<T, Ds> get_helper_momD(int n, typename std::enable_if<!is_complex<TT>::value>::type* = nullptr);

/**
 * Dt dimensional massless spinors for Ds vector dimensions
 * @param p External massless momentum - has to be confined in Ds = 4
 * @param Dtt_index the chosen index in the [Ds-4] dimensional subspace used for the Tensor product
 */
template <typename T, size_t Ds, typename TT = T> std::vector<T> DSpinor_Up(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds, typename TT = T> std::vector<T> DSpinor_Up(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<!is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds, typename TT = T> std::vector<T> DSpinor_Um(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds, typename TT = T> std::vector<T> DSpinor_Um(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<!is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds, typename TT = T> std::vector<T> DSpinor_Ubarp(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds, typename TT = T> std::vector<T> DSpinor_Ubarp(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<!is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds, typename TT = T> std::vector<T> DSpinor_Ubarm(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds, typename TT = T> std::vector<T> DSpinor_Ubarm(const momentumD<T, Ds>& p, const unsigned& Dtt_index, typename std::enable_if<!is_complex<TT>::value>::type* = nullptr);
template <typename T, size_t Ds> std::vector<T> DSpinor_Vp(const momentumD<T, Ds>& p, const unsigned& Dtt_index) { return DSpinor_Um<T, Ds>(p, Dtt_index); }
template <typename T, size_t Ds> std::vector<T> DSpinor_Vm(const momentumD<T, Ds>& p, const unsigned& Dtt_index) { return DSpinor_Up<T, Ds>(p, Dtt_index); }
template <typename T, size_t Ds> std::vector<T> DSpinor_Vbarp(const momentumD<T, Ds>& p, const unsigned& Dtt_index) { return DSpinor_Ubarm<T, Ds>(p, Dtt_index); }
template <typename T, size_t Ds> std::vector<T> DSpinor_Vbarm(const momentumD<T, Ds>& p, const unsigned& Dtt_index) { return DSpinor_Ubarp<T, Ds>(p, Dtt_index); }

/**
 * Dt dimensional massive spinors for Ds vector dimensions
 * @param p External massive momentum p - has to be confined in Ds = 4
   @param q massless reference momentum q has to be confined in Ds = 4
   @param mass Mass of the external momentum p
 * @param Dtt_index the chosen index in the [Ds-4] dimensional subspace used for the Tensor product
 */
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Up(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Um(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Ubarp(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Ubarm(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Vp(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Vm(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Vbarp(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE> std::vector<T> DSpinor_Vbarm(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass, const unsigned& Dtt_index);

/**
 * Cut-wavefunctions
 **/
template <typename T, size_t Ds, typename TE> momentumD<T, Ds> lightcone_decompose(const momentumD<TE, Ds>& p, const momentumD<TE, Ds>& q, const TE& mass);
template <typename T, size_t Ds, typename TE = typename infer_unext_type<T>::type> std::vector<T> DSpinor_cut_Qp(const momentumD<TE,Ds>& p, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE = typename infer_unext_type<T>::type> std::vector<T> DSpinor_cut_Qm(const momentumD<TE,Ds>& p, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE = typename infer_unext_type<T>::type> std::vector<T> DSpinor_cut_Qbp(const momentumD<TE,Ds>& p, const TE& mass, const unsigned& Dtt_index);
template <typename T, size_t Ds, typename TE = typename infer_unext_type<T>::type> std::vector<T> DSpinor_cut_Qbm(const momentumD<TE,Ds>& p, const TE& mass, const unsigned& Dtt_index);

} // namespace clifford_algebra
} // namespace Caravel

#include "DSpinor.hpp"

#endif
