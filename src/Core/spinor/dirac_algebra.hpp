#include <iostream>

namespace Caravel {
namespace clifford_algebra {

template <size_t D, typename T> DiracGamma<D,T>::DiracGamma(const unsigned mu) {
    // We first initialize the whole matrix to zero
    se11 = sEntry::Z;
    se12 = sEntry::Z;
    se21 = sEntry::Z;
    se22 = sEntry::Z;
    g11 = DiracGamma<D - 2,T>();
    g12 = g11;
    g21 = g11;
    g22 = g11;
    Dt = clifford_algebra::two_to_the(D / 2);

    // catch first all zero matrices for any dimensions D
    if (mu > D) {
        isZero = true;
        idx = D + 1;
        return;
    }

    // From here on all matrices are non-zero
    isZero = false;
    idx = mu;

    // Fill explicitly the 4D gamma matrices
    if (D == 4) {
#if WEYL
        if (mu == 0) {
            se12 = sEntry::One;
            se21 = sEntry::One;
            g12 = DiracGamma<D - 2,T>(mu);
            g21 = DiracGamma<D - 2,T>(mu);
        }
        else if (0 < mu && mu <= 3) {
            se12 = sEntry::One;
            se21 = sEntry::mOne;
            g12 = DiracGamma<D - 2,T>(mu);
            g21 = DiracGamma<D - 2,T>(mu);
        }
        else if (mu == 4) {
            se11 = sEntry::mOne;
            se22 = sEntry::One;
            g11 = DiracGamma<D - 2,T>(0);
            g22 = DiracGamma<D - 2,T>(0);
        }
#else
        if (mu == 0) {
            se11 = sEntry::One;
            se22 = sEntry::mOne;
            g11 = DiracGamma<D - 2,T>(0);
            g22 = DiracGamma<D - 2,T>(0);
        }
        else if (0 < mu && mu <= 3) {
            se12 = sEntry::One;
            se21 = sEntry::mOne;
            g12 = DiracGamma<D - 2,T>(mu);
            g21 = DiracGamma<D - 2,T>(mu);
        }
        else if (mu == 4) {
            se12 = sEntry::One;
            se21 = sEntry::One;
            g12 = DiracGamma<D - 2,T>(0);
            g21 = DiracGamma<D - 2,T>(0);
        }
#endif
        return;
    }

    // At this point, the construction is recursive for generic D > 4
    // We use the recursive approach for D > 4 from Collins - Renormalization
    if (D > 4) {
        if (mu <= D - 3) {
            se11 = sEntry::One;
            se22 = sEntry::One;
            g11 = DiracGamma<D - 2,T>(mu);
            g22 = DiracGamma<D - 2,T>(mu);
        }
        else if (mu == D - 2) {
            if (is_complex<T>::value) {
                se12 = sEntry::One;
                se21 = sEntry::mOne;
            }
            else {
                se12 = sEntry::mOne;
                se21 = sEntry::mOne;
            }
            g12 = DiracGamma<D - 2,T>(D - 2);
            g21 = DiracGamma<D - 2,T>(D - 2);
        }
        else if (mu == D - 1) {
            if( is_complex<T>::value ) {
              se12 = sEntry::I;
              se21 = sEntry::I;
            } else {
              se12 = sEntry::One;
              se21 = sEntry::mOne;
            }
            g12 = DiracGamma<D - 2,T>(D - 2);
            g21 = DiracGamma<D - 2,T>(D - 2);
        }
        else if (mu == D) {
            se11 = sEntry::One;
            se22 = sEntry::mOne;
            g11 = DiracGamma<D - 2,T>(D - 2);
            g22 = DiracGamma<D - 2,T>(D - 2);
        }
    }
    return;

} // constructor

template <size_t D, typename T> sEntry DiracGamma<D,T>::entry(size_t i, size_t j) const {
    if (isZero) return sEntry::Z;

    // Determine in which quadrant the (i,j) indices lie
    size_t k = (i - 1) % (Dt / 2), l = (j - 1) % (Dt / 2);

    if (i <= Dt / 2 && j <= Dt / 2)
        return sEntry_multiply(se11, g11.entry(k + 1, l + 1));
    else if (i <= Dt / 2 && j > Dt / 2)
        return sEntry_multiply(se12, g12.entry(k + 1, l + 1));
    else if (i > Dt / 2 && j <= Dt / 2)
        return sEntry_multiply(se21, g21.entry(k + 1, l + 1));
    else
        return sEntry_multiply(se22, g22.entry(k + 1, l + 1));
}

template <size_t D, typename T, bool transpose> void sparse_DGamma<D, T, transpose>::init_values() {

    for (int mu = 0; mu < static_cast<int>(D) + 1; mu++) {
        const DiracGamma<D,T> g(mu);
        values.at(mu).clear();
        // Loop over all entries and save non-zero ones (we keep the 1-based counting)
        for (size_t i = 1; i <= Dt; i++) {
            for (size_t j = 1; j <= Dt; j++) {
                if (g.entry(i, j) == sEntry::Z) continue;
                if (!transpose)
                    values[mu].emplace_back(i, j, explicit_sEntry<T>(g.entry(i, j)));
                else
                    values[mu].emplace_back(j, i, explicit_sEntry<T>(g.entry(i, j)));
            }
        }
    }
}

template <size_t D, typename T, bool transpose> void sparse_DGamma<D, T, transpose>::print() {
    for (int mu = 0; mu < static_cast<int>(D) + 1; mu++) {
        std::cout << "Matrix: " << mu << "\t Transpose: " << transpose << std::endl;
	std::cout << "row \t col \t val" << std::endl;
        auto vals = values[mu];
        for (auto s : vals) std::cout << std::get<0>(s) << "\t" << std::get<1>(s) << "\t" << std::get<2>(s) << std::endl;
    }
}

template <size_t D, typename T, bool transpose> template <typename TT> std::enable_if_t<is_exact<TT>::value> sparse_DGamma<D, T, transpose>::update_values() {
    for (int mu = 0; mu <= static_cast<int>(D); mu++) {
        const DiracGamma<D, T> g(mu);
        for (auto& s : values.at(mu)) {
            using std::get;
            if (transpose) { get<2>(s) = explicit_sEntry<T>(g.entry(get<1>(s), get<0>(s))); }
            else {
                get<2>(s) = explicit_sEntry<T>(g.entry(get<0>(s), get<1>(s)));
            }
        }
    }

    DEBUG_MESSAGE("updated ", _typeid(T), " D: ", D, " transpose: ", transpose);
}

template <mult_from MF, typename T, size_t Ds, typename TE, typename TT> std::vector<T> multiply_sparse_DGamma(const std::array<TE,Ds>& p, const std::vector<TT>& in){
    static_assert(Ds >= 4, "Pslash implemented only for Ds >= 4");
    // all template types (T,TE,TT) can be of extended type, then we need the un-extended type to initialize the Dirac matrices
    typedef typename infer_unext_type<T>::type F;

    sparse_DGamma<Ds, F, static_cast<bool>(MF)>::init();
    std::vector<T> out(sparse_DGamma<Ds, F, static_cast<bool>(MF)>::Dt, T(0));

    auto p_ext = to_extended_t<std::array<T,Ds>,Ds>(p);

    for (int mu = 0; mu < static_cast<int>(Ds); mu++) {
        if (p_ext[mu] == T(0)) continue;
        auto const vals = sparse_DGamma<Ds, F,static_cast<bool>(MF)>::values[mu];
        for (auto s : vals) {
            int i = std::get<0>(s) - 1;
            int j = std::get<1>(s) - 1;
            T gij = T(std::get<2>(s)); // [gamma^mu]_ij
            if (in[j] == TT(0)) continue;
            out[i] += p_ext[mu] * T(get_metric_signature<T>(mu)) * gij * in[j];
        }
    }

    return out;
}

template <size_t Ds, typename T> std::vector<T> vector_from_DSpinor(const std::vector<T>& psi, const std::vector<T>& psiBar) {
    typedef typename infer_unext_type<T>::type F;
    sparse_DGamma<Ds, F, false>::init();
    std::vector<T> out(static_cast<int>(Ds), T(0));

    for (int mu = 0; mu < static_cast<int>(Ds); mu++) {
        auto const vals = sparse_DGamma<Ds, F, false>::values[mu];
        // out[mu] = psiBar_i * [gamma^\mu]_ij * psi_j
        for (auto s : vals) {
            int i = std::get<0>(s) - 1;
            int j = std::get<1>(s) - 1;
            T gij = T(std::get<2>(s)); // [gamma^mu]_ij
            out[mu] += psiBar[i] * gij * psi[j];
        }
    }

    return out;
}

#define _INSTANTIATE_DIRAC_GEN_Ds(KEY,Ds,T,TE,TT) \
   KEY template std::vector<T> multiply_sparse_DGamma<mult_from::R,T,Ds,TE,TT>(const std::array<TE,Ds>& p, const std::vector<TT>& in); \
   KEY template std::vector<T> multiply_sparse_DGamma<mult_from::L,T,Ds,TE,TT>(const std::array<TE,Ds>& p, const std::vector<TT>& in); 


#define _INSTANTIATE_DIRAC_GEN(KEY,T) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,4,T,T,T) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,6,T,T,T) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,8,T,T,T) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,10,T,T,T) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,12,T,T,T) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,14,T,T,T) \
   KEY template std::vector<T> vector_from_DSpinor<4,T>(const std::vector<T>& psi, const std::vector<T>& psiBar); \
   KEY template std::vector<T> vector_from_DSpinor<6,T>(const std::vector<T>& psi, const std::vector<T>& psiBar); \
   KEY template std::vector<T> vector_from_DSpinor<8,T>(const std::vector<T>& psi, const std::vector<T>& psiBar); \
   KEY template std::vector<T> vector_from_DSpinor<10,T>(const std::vector<T>& psi, const std::vector<T>& psiBar); \
   KEY template std::vector<T> vector_from_DSpinor<12,T>(const std::vector<T>& psi, const std::vector<T>& psiBar); \
   KEY template std::vector<T> vector_from_DSpinor<14,T>(const std::vector<T>& psi, const std::vector<T>& psiBar); \

#define _INSTANTIATE_DIRAC_GEN_EXACT_Ds(KEY,Ds,T) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,Ds,extended_t<T>,extended_t<T>,extended_t<T>) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,Ds,extended_t<T>,extended_t<T>,T) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,Ds,extended_t<T>,T,extended_t<T>) \
   _INSTANTIATE_DIRAC_GEN_Ds(KEY,Ds,extended_t<T>,T,T) \
  

#define _INSTANTIATE_DIRAC_GEN_EXACT(KEY,T) \
  _INSTANTIATE_DIRAC_GEN_EXACT_Ds(KEY,4,T) \
  _INSTANTIATE_DIRAC_GEN_EXACT_Ds(KEY,6,T) \
  _INSTANTIATE_DIRAC_GEN_EXACT_Ds(KEY,8,T) \
  _INSTANTIATE_DIRAC_GEN_EXACT_Ds(KEY,10,T) \
  _INSTANTIATE_DIRAC_GEN_EXACT_Ds(KEY,12,T) \
  _INSTANTIATE_DIRAC_GEN_EXACT_Ds(KEY,14,T) 

_INSTANTIATE_DIRAC_GEN(extern,C) 
_INSTANTIATE_DIRAC_GEN(extern,R) 

#ifdef HIGH_PRECISON
_INSTANTIATE_DIRAC_GEN(extern,CHP) 
_INSTANTIATE_DIRAC_GEN(extern,RHP) 
#endif

#ifdef VERY_HIGH_PRECISON
_INSTANTIATE_DIRAC_GEN(extern,CVHP) 
_INSTANTIATE_DIRAC_GEN(extern,RVHP) 
#endif

#ifdef USE_GMP
_INSTANTIATE_DIRAC_GEN(extern,CGMP) 
_INSTANTIATE_DIRAC_GEN(extern,RGMP) 
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_DIRAC_GEN(extern,F32) 
_INSTANTIATE_DIRAC_GEN_EXACT(extern,F32) 
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_DIRAC_GEN(extern,BigRat) 
_INSTANTIATE_DIRAC_GEN_EXACT(extern,BigRat) 
#endif
#endif
} // namespace clifford_algebra
} // namespace Caravel
