#ifndef _EIGEN_QD_H_
#define _EIGEN_QD_H_

#include <Eigen/Core>
#include <qd/qd_real.h>

inline dd_real abs2(const dd_real& x){return x*x;}
inline qd_real abs2(const qd_real& x){return x*x;}

namespace Eigen {

template <>
struct NumTraits<dd_real> {
    typedef dd_real Real;
    typedef dd_real NonInteger;
    typedef dd_real Literal;
    typedef dd_real Nested;

    enum {
        IsInteger = 0,
        IsSigned = 1,
        IsComplex = 0,
        ReadCost = 2,
        AddCost = 3,
        MulCost = 6,
        RequireInitialization = 0,
    };

    static Real epsilon() { return Real::_eps; }
    static Real dummy_precision() { return Real("1e-28"); }
    static Real highest() { return Real::_max; }
    static Real lowest() { return Real::_max; }
    static int digits() { return std::numeric_limits<Real>::digits; }
    static int digits10() { return std::numeric_limits<Real>::digits10; }
    static Real infinity() { return Real::_inf; }
    static Real quiet_NaN() { return Real::_nan; }
};

template <>
struct NumTraits<qd_real> {
    typedef qd_real Real;
    typedef qd_real NonInteger;
    typedef qd_real Literal;
    typedef qd_real Nested;

    enum {
        IsInteger = 0,
        IsSigned = 1,
        IsComplex = 0,
        ReadCost = 4,
        AddCost = 6,
        MulCost = 12,
        RequireInitialization = 0,
    };

    static Real epsilon() { return Real::_eps; }
    static Real dummy_precision() { return Real("1e-60"); }
    static Real highest() { return Real::_max; }
    static Real lowest() { return Real::_max; }
    static int digits() { return std::numeric_limits<Real>::digits; }
    static int digits10() { return std::numeric_limits<Real>::digits10; }
    static Real infinity() { return Real::_inf; }
    static Real quiet_NaN() { return Real::_nan; }
};

}

#endif // _EIGEN_QD_H_
