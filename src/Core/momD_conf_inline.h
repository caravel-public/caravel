/*
 * momD_conf_inline.h
 * Added in 9.3.2016
 *
 * Inherited from V1's mom_conf_inline.h from Daniel
 *
 * inline specializations for momD_conf
 *
 */

#ifndef MOMD_CONF_INLINE_H_
#define MOMD_CONF_INLINE_H_

template <class T, size_t D> inline const momD<T, D>& momentumD_configuration<T, D>::p(size_t n) const {
    if (n <= momentumD_configuration<T, D>::nbr) {
        if (_shift > 0) { return _parent->p(n + _shift - 1); }
        else if (n > _offset) {
            return momentumD_configuration<T, D>::ps[n - _offset - 1];
        }
        else {
            return _parent->p(n);
        }
    }
    else {
        //_WARNING("Too large momentum index in sub_momentumD_configuration::p: ",n," (max=",momentumD_configuration<T,D>::nbr,")");
        throw CaravelException("Mom_conf indexing error");
    }
}

template <class T, size_t D> inline void momentumD_configuration<T, D>::set_p(const momD<T, D>& pin, size_t n) {
    if (n <= momentumD_configuration<T, D>::nbr) {
        if (_shift > 0) { _parent->set_p(pin, n + _shift - 1); }
        else if (n > _offset) {
            momentumD_configuration<T, D>::ps[n - _offset - 1] = pin;
        }
        else {
            _parent->set_p(pin, n);
        }
    }
    else {
        //_WARNING("Too large momentum index in sub_momentumD_configuration::p: ",n," (max=",momentumD_configuration<T,D>::nbr,")");
        throw CaravelException("Mom_conf indexing error");
    }
}

template <class T, size_t D> inline T momentumD_configuration<T, D>::m2(size_t n) const {
    if (n <= momentumD_configuration<T, D>::nbr) {
        if (_shift > 0) { return _parent->m2(n + _shift - 1); }
        else if (n > _offset) {
            return momentumD_configuration<T, D>::ms[n - _offset - 1];
        }
        else {
            return _parent->m2(n);
        }
    }
    else {
        //_WARNING("Too large momentum index in sub_momentumD_configuration::ms: ",n," (max=",momentumD_configuration<T,D>::nbr,")");
        throw CaravelException("Mom_conf inexing error");
    }
}

template <class T, size_t D> const momentumD_configuration<T, D>& momentumD_configuration<T, D>::operator=(const std::vector<momD<T, D>>& ivm) {
    for (size_t ii = 0; ii < ivm.size(); ii++) this->set_p(ivm[ii], ii + 1);
    return *this;
}

template <class T, size_t D>
template <size_t Length>
const momentumD_configuration<T, D>& momentumD_configuration<T, D>::operator=(const std::array<momD<T, D>, Length>& iam) {
    for (size_t ii = 0; ii < iam.size(); ii++) this->set_p(iam[ii], ii + 1);
    return *this;
}

template <class T, size_t D> template <class... Ts> inline T momentumD_configuration<T, D>::s(Ts... is) const {
    auto sum = Sum(is...);
    return sum * sum;
}

template <class T, size_t D>
template <typename Itype>
inline std::enable_if_t<std::is_integral<Itype>::value, T> momentumD_configuration<T, D>::s(const std::vector<Itype>& v) const {
    auto sum = Sum(v);
    return sum * sum;
}

template <class T, size_t D> inline T momentumD_configuration<T, D>::sp(size_t i, size_t j) { return p(i) * p(j); }

//! spinor product <ij>
template <class T, size_t D> inline T spab_4D_R_type(const momD<T, D>& Pi, const momD<T, D>& Pj, int i, int j) {
    std::cout << "R types's mom_conf don't have implemented <ij> or [ij] products from momD_conf -- returned 0" << std::endl;
    return T(0);
}

template <class T, size_t D> inline T spa_4D_C_type(const momD<T, D>& Pi, const momD<T, D>& Pj, int i, int j, int nbr) {
    if (i < 1 || j < 1 || i > int(nbr) || j > int(nbr)) {
        std::cout << "Call for out of bounds spa_4D i: " << i << "; j: " << j << "; nbr: " << nbr << std::endl;
        return T(0);
    }
    return la<T>(Pi) * la<T>(Pj);
}

template <class T, size_t D> inline T spb_4D_C_type(const momD<T, D>& Pi, const momD<T, D>& Pj, int i, int j, int nbr) {
    if (i < 1 || j < 1 || i > int(nbr) || j > int(nbr)) {
        std::cout << "Call for out of bounds spb_4D i: " << i << "; j: " << j << "; nbr: " << nbr << std::endl;
        return T(0);
    }
    return lat<T>(Pi) * lat<T>(Pj);
}

template <> inline R momentumD_configuration<R, 4>::spa_4D(size_t i, size_t j) const { return spab_4D_R_type(this->p(size_t(i)), this->p(size_t(j)), i, j); }
#ifdef HIGH_PRECISION
template <> inline RHP momentumD_configuration<RHP, 4>::spa_4D(size_t i, size_t j) const {
    return spab_4D_R_type(this->p(size_t(i)), this->p(size_t(j)), i, j);
}
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline RVHP momentumD_configuration<RVHP, 4>::spa_4D(size_t i, size_t j) const {
    return spab_4D_R_type(this->p(size_t(i)), this->p(size_t(j)), i, j);
}
#endif
#ifdef INSTANTIATE_GMP
template <> inline RGMP momentumD_configuration<RGMP, 4>::spa_4D(size_t i, size_t j) const {
    return spab_4D_R_type(this->p(size_t(i)), this->p(size_t(j)), i, j);
}
#endif

template <> inline C momentumD_configuration<C, 4>::spa_4D(size_t i, size_t j) const {
    return spa_4D_C_type<C, 4>(this->p(size_t(i)), this->p(size_t(j)), i, j, nbr);
}
#ifdef HIGH_PRECISION
template <> inline CHP momentumD_configuration<CHP, 4>::spa_4D(size_t i, size_t j) const {
    return spa_4D_C_type<CHP, 4>(this->p(size_t(i)), this->p(size_t(j)), i, j, nbr);
}
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline CVHP momentumD_configuration<CVHP, 4>::spa_4D(size_t i, size_t j) const {
    return spa_4D_C_type<CVHP, 4>(this->p(size_t(i)), this->p(size_t(j)), i, j, nbr);
}
#endif
#ifdef INSTANTIATE_GMP
template <> inline CGMP momentumD_configuration<CGMP, 4>::spa_4D(size_t i, size_t j) const {
    return spa_4D_C_type<CGMP, 4>(this->p(size_t(i)), this->p(size_t(j)), i, j, nbr);
}
#endif

template <> inline C momentumD_configuration<C, 4>::spb_4D(size_t i, size_t j) const {
    return spb_4D_C_type<C, 4>(this->p(size_t(i)), this->p(size_t(j)), i, j, nbr);
}
#ifdef HIGH_PRECISION
template <> inline CHP momentumD_configuration<CHP, 4>::spb_4D(size_t i, size_t j) const {
    return spb_4D_C_type<CHP, 4>(this->p(size_t(i)), this->p(size_t(j)), i, j, nbr);
}
#endif
#ifdef VERY_HIGH_PRECISION
template <> inline CVHP momentumD_configuration<CVHP, 4>::spb_4D(size_t i, size_t j) const {
    return spb_4D_C_type<CVHP, 4>(this->p(size_t(i)), this->p(size_t(j)), i, j, nbr);
}
#endif
#ifdef INSTANTIATE_GMP
template <> inline CGMP momentumD_configuration<CGMP, 4>::spb_4D(size_t i, size_t j) const {
    return spb_4D_C_type<CGMP, 4>(this->p(size_t(i)), this->p(size_t(j)), i, j, nbr);
}
#endif

#endif /*MOMD_CONF_INLINE_H_*/
