/*
 * Explicit instantiations for momD classes to speed up compile time.
 */
#include "momD.h"

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "Core/qd_suppl.h"
#endif
namespace Caravel {

// Dummy virtual destructor for pure abstract class momentumD_Base
momentumD_Base::~momentumD_Base() {}

_INSTANTIATE_MOMD(R)
#ifdef HIGH_PRECISION
_INSTANTIATE_MOMD(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_MOMD(RVHP)
#endif
_INSTANTIATE_MOMD(std::complex<double>)
#ifdef HIGH_PRECISION
_INSTANTIATE_MOMD(std::complex<RHP>)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_MOMD(std::complex<RVHP>)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_MOMD(F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_MOMD(BigRat)
#endif
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_MOMD(RGMP)
_INSTANTIATE_MOMD(CGMP)
#endif
} // namespace Caravel
