#ifndef ALGEBRAICSTRUCTURES_H_INC
#define ALGEBRAICSTRUCTURES_H_INC

#include <string>
#include <vector>
#include <tuple>
#include "Core/MathInterface.h"
#include "Core/type_traits_extra.h"
#include "Core/typedefs.h"

#include <set>

#ifdef INSTANTIATE_GMP
#include "Core/gmp_r.h"
#endif

namespace Caravel {

// [BEGIN NColPower]
class NColPower{
    public:
        int pow;
        NColPower(std::string schematic);
        NColPower(int pow);
};

std::ostream& operator <<(std::ostream& out, const NColPower& col);

// [/END NColPower]

// [BEGIN Constant]
class Constant {
  private:
    int64_t real_num;
    int64_t real_denom;
    int64_t imag_num;
    int64_t imag_denom;

  public:
    std::string val;
    Constant(std::string schematic);

    bool is_imaginary() const;
    bool is_real() const;

    /**
     * modify *this to be the output of the map (a+I*b) --> (b - I*a)
     * Equivalently: divide (*this) by I
     */
    void extract_I();
    /**
     * multiplicity by -1
     */
    void negate();

    template <typename T> std::enable_if_t<is_complex<T>::value, T> get_num() const;
    template <typename T> std::enable_if_t<!is_complex<T>::value, T> get_num() const;

    friend std::ostream& operator<<(std::ostream&, const Constant&);
};

template <typename R> R LongRatio(int64_t n, int64_t d) { return R(n) / R(d); }
#ifdef HIGH_PRECISION
template <> RHP LongRatio<RHP>(int64_t n, int64_t d);
#endif
#ifdef VERY_HIGH_PRECISION
template <> RVHP LongRatio<RVHP>(int64_t n, int64_t d);
#endif
#ifdef INSTANTIATE_GMP
template <> RGMP LongRatio<RGMP>(int64_t n, int64_t d);
#endif

template <typename T> std::enable_if_t<is_complex<T>::value, T> Constant::get_num() const {
    using RealType = typename T::value_type;

    RealType real_part = LongRatio<RealType>(real_num,real_denom);
    RealType imag_part = LongRatio<RealType>(imag_num,imag_denom);

    return T(real_part, imag_part);
}

template <typename T> std::enable_if_t<!is_complex<T>::value, T> Constant::get_num() const {
    if (imag_num!=0) throw std::runtime_error("Attempted to cast a Constant with non-vanishing imaginary part to a real type");
    T real_part = LongRatio<T>(real_num,real_denom);
    return real_part;
}

std::ostream& operator <<(std::ostream& out, const Constant& col);

// [/END CONST]

// [BEGIN ColourStructure]
class ColourStructure {
  public:
    /**
     * product of open fundamental strings, sorted
     * each string is cycled such that the index with lowest value is at the first position
     */
     std::vector<std::vector<size_t>> open_fundamental;
    /**
     * products of closed fundamental strings organized like:
     * < anti-quark | .... | quark >
     */
     std::vector<std::vector<size_t>> closed_funamental;

    ColourStructure(std::string schematic);
};

 std::ostream& operator <<(std::ostream& out, const ColourStructure& col);
// [/END ColourStructure]

// [BEGIN LinearCombination]
// Templated LinearCombination code, must stay in header.
template< class A, class B >
class LinearCombination{
    public:
        using product = std::pair<A, B>;
        std::vector< product > elements;
        product& operator[](size_t pos){
            return elements.at(pos);
        }

	const product operator[](size_t pos) const{
            return elements.at(pos);
	}

	size_t size() const{
	  return elements.size();
	}

    LinearCombination()=default;
    LinearCombination(const std::string& schematic){
        MathList math_schematic(schematic, "LC");

        for (auto& prod : math_schematic){
            MathList prod_info(prod, "Prod");

            A left(prod_info.tail.at(0));
            B right(prod_info.tail.at(1));

            elements.emplace_back(left,right);
        }
    }

    typename std::vector< product >::const_iterator begin() const{
        return elements.begin();
    }

    typename std::vector< product >::const_iterator end() const{
        return elements.end();
    }
};


template< class A, class B >
std::ostream& operator <<(std::ostream& out, const LinearCombination<A, B>& lc){

   out << "(";

   bool first = true;
   for (auto& element : lc.elements){

       if (!first){
       out << " + ";
       }


       auto left = std::get<0>(element);
       auto right = std::get<1>(element);

       out << left << " * " << right;

       first = false;
   }

   out << ")";

   return out;
}

// [/END LinearCombination]

}

#endif //ALGEBRAICSTRUCTURES_H_INC
