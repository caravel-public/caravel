#pragma once

#include "precision_cast.h"

#include "Core/type_traits_extra.h"
#include "Core/Debug.h"
#include <exception>

#ifdef INSTANTIATE_RATIONAL
namespace Caravel{ struct BigRat; }
#endif

namespace Caravel {

// --------------------- OPERATORS --------------------------------------------

// --- ADDITION ---

template <class T>
Series<T> operator+(const Series<T>& s1, const Series<T>& s2) {
    Series<T> r(std::min(s1.leading(), s2.leading()), std::min(s1.last(), s2.last()));
    int j;

    for (j = s1.startOrder; j < s2.startOrder && j <= s1.endOrder; j += 1)
        r[j] = s1[j];
    for (j = s2.startOrder; j < s1.startOrder && j <= s2.endOrder; j += 1)
        r[j] = s2[j];
    for (j = std::max(s1.startOrder, s2.startOrder);
         j <= std::min(s1.endOrder, s2.endOrder); j += 1) {
        r[j] = s1[j] + s2[j];
    }

    return r;
}

template <class T> Series<T> operator+(const Series<T>& s1, const T& v2) {
    Series<T> r(s1);

    if (r.startOrder <= 0 and r.endOrder >= 0)
        r[0] += v2;
    return r;
}

template <class T> Series<T> operator+(const T& v1, const Series<T>& s2) {
    Series<T> r(s2);

    if (r.startOrder <= 0 and r.endOrder >= 0)
        r[0] += v1;
    return r;
}

// --- SUBTRACTION ---

template <class T>
Series<T> operator-(const Series<T>& s1, const Series<T>& s2) {

    Series<T> r(std::min(s1.startOrder, s2.startOrder),
                std::min(s1.endOrder, s2.endOrder));
    int j;

    for (j = s1.startOrder; j < s2.startOrder && j <= s1.endOrder; j += 1)
        r[j] = s1[j];
    for (j = s2.startOrder; j < s1.startOrder && j <= s2.endOrder; j += 1)
        r[j] = -s2[j];
    for (j = std::max(s1.startOrder, s2.startOrder);
         j <= std::min(s1.endOrder, s2.endOrder); j += 1) {
        r[j] = s1[j] - s2[j];
    }

    return r;
}

template <class T> Series<T> operator-(const Series<T>& s1, const T& v2) {
    Series<T> r(s1);

    if (r.startOrder <= 0 and r.endOrder >= 0)
        r[0] -= v2;
    return r;
}

template <class T> Series<T> operator-(const T& v1, const Series<T>& s2) {
    Series<T> r(-s2);

    if (r.startOrder <= 0 and r.endOrder >= 0)
        r[0] += v1;
    return r;
}

// --- NEGATIVE ---

template <class T> Series<T> operator-(const Series<T>& s) {
    Series<T> r(s);
    int j;

    for (j = r.startOrder; j <= r.endOrder; j += 1)
        r[j] = -r[j];

    return r;
}

// --- MULTIPLICATION ---

template <class T>
Series<T> operator*(const Series<T>& s1, const Series<T>& s2) {
    int s1FirstNonZero = s1.startOrder;
    while(s1[s1FirstNonZero] == T{} && s1FirstNonZero < s1.endOrder){s1FirstNonZero++;}
    int s2FirstNonZero = s2.startOrder;
    while(s2[s2FirstNonZero] == T{} && s2FirstNonZero < s2.endOrder){s2FirstNonZero++;}
  
    Series<T> r(s1.startOrder + s2.startOrder,
                std::min(s1FirstNonZero + s2.endOrder, s2FirstNonZero + s1.endOrder));

    for (int j1 = s1FirstNonZero; j1 <= s1.endOrder; j1 += 1){
        for (int j2 = s2FirstNonZero; j2 <= s2.endOrder; j2 += 1) {
            if (j1 + j2 > r.endOrder)
                continue;
            r[j1 + j2] += s1[j1] * s2[j2];
        }
    }

    return r;
}

template <class T> Series<T> operator*(const Series<T>& s1, const T& v2) {
    Series<T> r(s1);

    for (int j = r.startOrder; j <= r.endOrder; j += 1)
        r[j] *= v2;

    return r;
}

template <class T> Series<T> operator*(const T& v1, const Series<T>& s2) {
    Series<T> r(s2);

    for (int j = r.startOrder; j <= r.endOrder; j += 1)
        r[j] *= v1;
    return r;
}

// --- DIVISION ---

template <class T> Series<T> operator/(const Series<T>& s1, const T& v2) {
    if constexpr (op_valid_v<T&, T&, std::divides<T>>) {
        Series<T> r(s1);

        for (int j = r.startOrder; j <= r.endOrder; j += 1) r[j] /= v2;
        return r;
    }
    else {
        throw std::invalid_argument("Tried to use operator/ for "+_typeid(T)+" which is not implemented!");
    }
}

// --- POWER ---

template <class T> Series<T> operator^(const Series<T>& s, unsigned int p) {
    Series<T> r(p * s.startOrder, (p - 1) * s.startOrder + s.endOrder);
#if 0
 double coeff = 1; // Really want the real base type of T
 int index;
#endif

    switch (p) {
    case 0:
        r.startOrder = r.endOrder = 0;
        r.term.push_back(T(1));
        r.term.push_back(static_cast<T>(1));

        break;
    case 1:
        r.term = s.term;
        break;
    case 2:
        // Quadratic terms
        for (int j = s.startOrder; j <= s.endOrder; j += 1) {
            if (2 * j > r.endOrder)
                break;
            r[2 * j] += s[j] * s[j];
        }
        // Cross terms
        for (int j1 = s.startOrder; j1 <= s.endOrder; j1 += 1)
            for (int j2 = j1 + 1; j2 <= s.endOrder; j2 += 1) {
                if (j1 + j2 > r.endOrder)
                    break;
                r[j1 + j2] += T(2) * s[j1] * s[j2];
            }
        break;
    default:
        // Divide and conquer
        if (p & 1) // if odd
            r = ((s ^ ((p - 1) / 2)) ^ 2) * s;
        else
            r = (s ^ (p / 2)) ^ 2;
        break;
    }

    return r;
}

  template<typename T>
  bool operator == (const Series<T>& s, int constant){
    bool equivalent = true;
    for (int j = s.leading(); j <= s.last(); j++){

      if (j != 0){if (s[j] != T{}) equivalent = false;}
      else if (s[0] != T(constant)){equivalent = false;}
    }
    return equivalent;
  }

  template<typename T>
  bool operator == (const Series<T>& s, T constant){
    bool equivalent = true;
    for (int j = s.leading(); j <= s.last(); j++){

      if (j != 0){if (s[j] != T{}) equivalent = false;}
      else if (s[0] != constant){equivalent = false;}
    }
    return equivalent;
  }

  template<typename T>
  bool operator == (T constant, const Series<T>& series){
    return series == constant;
  }

// --- OUTPUT ---

template <class T>
std::ostream& operator<<(std::ostream& os, const Series<T>& s) {

  os << "( ";
  bool first = true;
  for (int j = s.startOrder; j <= s.endOrder; j++){
    if (s[j] == T{}){continue;}
    if (!first){os << " + ";}
    first = false;
    os << s[j]<< "*" << "ep^" << j;
  }
  if (!first){ os << " + "; }
  os << "O[ep]^" << s.endOrder + 1;

  os << " )";

  return os;
}

template <class T>
Series<T>::Series(const MathList& input) {
    assert_head(input,"SeriesData");

    if(input.size() != 6 or std::stoi(input[1]) != 0 or std::stoi(input[5]) != 1) throw std::runtime_error("Cannot construct Series from  "+math_string(input));

    startOrder = std::stoi(input[3]);
    endOrder = std::stoi(input[4]);

    // The following encodes Mathematica's conventions.
    // There's always -1 to the endOrder compared.
    // However if startOrder is same to endOrder it means the series is zero up until endOrder-1,
    if (startOrder == endOrder) {
        endOrder -= 1;
        startOrder = endOrder;
    }
    else if (startOrder > endOrder)
        throw std::runtime_error("Cannot construct Series from  " + math_string(input));
    else {
        endOrder -= 1;
    }

    for(auto&& el : MathList(input[2])) {
        T coeff;
        if constexpr (std::is_constructible_v<T,MathList>){
            coeff = T{std::move(el)};
        }
        else {
            using ::Caravel::operator>>;
            std::istringstream s(std::move(el));
            s >> coeff;
        }
        term.push_back(std::move(coeff));
    }

    // if we received not enough coefficients to fill the whole order range, we asssume that the higher order ones are zero
    for (size_t i = term.size(); i < static_cast<size_t>(endOrder - startOrder + 1); ++i) { term.emplace_back(); }

    if(term.size() != static_cast<size_t>(endOrder-startOrder+1)) throw std::range_error("Number of provided coefficients is incompatible with range.");
}


template <typename T> std::string math_string(const Series<T>& s) {
    using ::Caravel::math_string;
    return math_list_with_head("SeriesData","ep",0, s.get_term(),s.leading(),s.last()+1,1);
}


// -----------------  THE SERIES CLASS ----------------------------------------

// CONSTRUCTORS
template <class T>
Series<T>::Series(int s, int e)
    : startOrder(s), endOrder(e), term(e - s + 1) {}

template <class T>
template <typename... Ts>
Series<T>::Series(short int s, short int e, const T& first,
                     const Ts&... coeffs)
    : startOrder{s}, endOrder{e} {

    term = {first, coeffs...};
    if (static_cast<size_t>(endOrder - startOrder + 1) != term.size()) {
        throw std::range_error(
            "Number of provided coefficients is incompatible with range.");
    }
}

template <class T>
template <typename... Ts>
Series<T> Series<T>::drop_excess(short int s, short int e, const T& first, const Ts&... coeffs){
    std::vector<T> to_crop = {first, coeffs...};
    return Series<T>(s, e, std::vector<T>(to_crop.begin(), to_crop.begin() + (e-s+1)));
}

template <class T>
Series<T>::Series(int s, int e, const std::vector<T>& v)
    : startOrder(s), endOrder(e) {
    if (startOrder > endOrder){
      throw std::runtime_error("ERROR IN SERIES CONSTRUCTION. start order bigger than end order.");
    }

    int order = s, i = 0;
    while (order++ <= e){
      if (i < int(v.size())){
        term.push_back(v[i++]);
      }
      else{
        term.emplace_back();
      }
    }

}

// OPERATORS

template <class T> T const& Series<T>::operator[](int order) const {
    if (order < startOrder)
        throw std::range_error("Request for a coefficient of lower order!");
    if (order > endOrder)
        throw std::range_error("Request for a coefficient of higher order!");
    return term[order - startOrder];
}

template <class T> T& Series<T>::operator[](int order) {
    if (order < startOrder)
        throw std::range_error("Request for a coefficient of lower order!");
    if (order > endOrder)
        throw std::range_error("Request for a coefficient of higher order!");
    return term[order - startOrder];
}

template <class T> T Series<T>::operator()(int order) const {
    if (order < startOrder)
        return T{};
    if (order > endOrder)
        throw std::range_error("Request for a coefficient of higher order!");
    return term[order - startOrder];
}


template <class T> Series<T>& Series<T>::operator+=(const Series<T>& s) {

    Series<T> r(*this + s);

    this->term = r.term;
    this->startOrder = r.startOrder;
    this->endOrder = r.endOrder;
    return *this;
}

template <class T> Series<T>& Series<T>::operator+=(const T& v) {
    if (this->startOrder <= 0 and this->endOrder >= 0)
        (*this)[0] += v;
    return *this;
}
template <class T> Series<T>& Series<T>::operator-=(const Series<T>& s) {
    Series<T> r(*this - s);

    this->term = r.term;
    this->startOrder = r.startOrder;
    this->endOrder = r.endOrder;
    return *this;
}

template <class T> Series<T>& Series<T>::operator-=(const T& v) {
    if (this->startOrder <= 0 and this->endOrder >= 0)
        (*this)[0] -= v;
    return *this;
}
template <class T> Series<T>& Series<T>::operator*=(const Series<T>& s) {
    Series<T> r(*this * s);

    this->term = r.term;
    this->startOrder = r.startOrder;
    this->endOrder = r.endOrder;
    return *this;
}

template <class T> Series<T>& Series<T>::operator*=(const T& v) {
    for (int j = this->startOrder; j <= this->endOrder; j += 1)
        (*this)[j] *= v;
    return *this;
}
template <class T> Series<T>& Series<T>::operator/=(const T& v) {
    if constexpr (op_valid_v<T&, T&, std::divides<T>>) {
        for (int j = this->startOrder; j <= this->endOrder; j += 1) (*this)[j] /= v;
        return *this;
    }
    else {
        throw std::invalid_argument("Tried to use operator/ for "+_typeid(T)+" which is not implemented!");
    }
}

template <class T> Series<T>& Series<T>::operator^=(unsigned int p) {
    Series<T> r(*this ^ p);

    this->term = r.term;
    this->startOrder = r.startOrder;
    this->endOrder = r.endOrder;
    return *this;
}

template <typename T> bool operator==(const Series<T>& s1, const Series<T>& s2) {
    if (s1.leading() != s2.leading()) return false;
    if (s1.last() != s2.last()) return false;

    for (int i = s1.leading(); i <= s2.last(); ++i) {
        if (s1[i] != s2[i]) return false;
    }

    return true;
}

template<class T> Series<T> Series<T>::multiply_by_power(int n){
    return Series<T>(this->leading() + n, this->last() + n, this -> get_term());
}

template <class T> template <typename TFunc> auto Series<T>::map(TFunc&& f){
    using T2 = std::remove_cv_t<decltype(f(std::declval<T>()))>;
    std::vector<T2> new_coeffs;
    new_coeffs.reserve(get_term().size());

    for (auto& x : get_term()){
      new_coeffs.push_back(f(x));
    }

    return Series<T2>(leading(), last(), new_coeffs);
}

template<typename T> void Series<T>::truncate(int max_order){

  if (max_order >= endOrder){return;}
  if (max_order < startOrder){
    startOrder = max_order;
    endOrder = max_order;
    term = {T{}};
  }
  else{

    endOrder = max_order;

    int order = startOrder, i = 0;
    std::vector<T> new_term;
    while (order++ <= endOrder){
      if (i < int(term.size())){
        new_term.push_back(term[i++]);
      }
      else{
        new_term.emplace_back();
      }
    }

    term = new_term;
  }
}

template <typename Thigh, typename Tlow> Series<Thigh> to_precision(const Series<Tlow>& in){
    Series<Thigh> out(in.leading(),in.last());

    for(int c = in.leading(); c<= in.last(); ++c){
        out[c] = to_precision<Thigh>(in[c]);
    }

    return out;
}

template <typename T, template <class...> class Container> std::vector<Series<T>> to_Series(const std::vector<Container<T>>& c, const int highest_power){
    std::vector<Series<T>> series_coeffs;
    //for (size_t ii=0;ii<c.size();ii++) { std::cout<<"The coeff_"<<ii<<": "<< c[ii]<<std::endl; }
    for (auto& it : c) { series_coeffs.push_back(it.taylor_expand_around_zero(highest_power)); }
    return series_coeffs;
}

// g++ still requires explicit instantiation -- we do it for complex series

#define EXTERN_INSTANTIATE(TYPE)                                               \
    extern template class Series<TYPE>;                                        \
    extern template Series<TYPE> operator+(const Series<TYPE>&,                \
                                           const Series<TYPE>&);               \
    extern template Series<TYPE> operator+(const Series<TYPE>&, const TYPE&);  \
    extern template Series<TYPE> operator+(const TYPE& v1,                     \
                                           const Series<TYPE>& s2);            \
    extern template Series<TYPE> operator-(const Series<TYPE>& s1,             \
                                           const Series<TYPE>& s2);            \
    extern template Series<TYPE> operator-(const Series<TYPE>& s1,             \
                                           const TYPE& v2);                    \
    extern template Series<TYPE> operator-(const TYPE& v1,                     \
                                           const Series<TYPE>& s2);            \
    extern template Series<TYPE> operator-(const Series<TYPE>& s1);            \
    extern template Series<TYPE> operator*(const Series<TYPE>& s1,             \
                                           const Series<TYPE>& s2);            \
    extern template Series<TYPE> operator*(const Series<TYPE>& s1,             \
                                           const TYPE& v2);                    \
    extern template Series<TYPE> operator*(const TYPE& v1,                     \
                                           const Series<TYPE>& s2);            \
    extern template Series<TYPE> operator/(const Series<TYPE>& s,              \
                                           const TYPE& v);                     \
    extern template Series<TYPE> operator^(const Series<TYPE>& s,              \
                                           unsigned int p);                    \
    extern template std::ostream& operator<<(std::ostream&,                    \
                                             const Series<TYPE>&);

EXTERN_INSTANTIATE(C)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE(CVHP)
#endif

EXTERN_INSTANTIATE(R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE(RVHP)
#endif

#if INSTANTIATE_GMP
EXTERN_INSTANTIATE(CGMP)
EXTERN_INSTANTIATE(RGMP)
#endif

#ifdef USE_FINITE_FIELDS 
EXTERN_INSTANTIATE(F32)
#endif
    

#ifdef INSTANTIATE_RATIONAL
EXTERN_INSTANTIATE(BigRat)
#endif

#undef EXTERN_INSTANTIATE

} // namespace Caravel
