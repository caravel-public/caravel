#include "LAPACK.h"

#include "Timing.h"

#include "Core/typedefs.h"
#include "Vec.h"
#include "Core/type_traits_extra.h"

#ifdef USE_EIGEN
#include <Eigen/Core>
#include <Eigen/Dense>
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "Core/eigen-qd.h"
#endif
#endif

#define _TRY_PARALLEL_THREADS_IN_LUPDecompose 0

#if _TRY_PARALLEL_THREADS_IN_LUPDecompose
#include <future>
#include <thread>
#endif

#include <chrono>
#include <ratio>

namespace Caravel {

namespace InversionStrategies {

/**
 * Exception thrown if a degenerate matrix encountered in PLU decomposition
 */
class DegenerateMatrixException : public std::exception {};

} // namespace InversionStrategies

// Anonymous namespace
namespace {

// Whether or not this function is used depends on the configuration flags. 
// This avoids having to wrap it in complex header guards.
[[maybe_unused]] 
// How many additional equations to generate for the qr system solving.
// It is more convenient to have it in one place.
int _qr_number_of_equations(int basis_dim) {
    // int num_equations = 2*basis_dim; //100%
    // int num_equations = 3*basis_dim/2; // 50%
    // int num_equations = (13*basis_dim)/10; // 30%
    // int num_equations = (5*basis_dim)/4; // 25%
    int num_equations = (6 * basis_dim) / 5; // 20%
    // int num_equations = (23*basis_dim)/20; // 15%
    // int num_equations = basis_dim; //0%
    // int num_equations = (11*basis_dim)/10; // 10%
    // int num_equations = (21*basis_dim)/20; // 5%
    return num_equations;
}

} // namespace


// ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
//                                                          PLU linear solver.
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------------------

/**
 * In house implementation of linear system solving (A.x = b) through
 * PLU factorization.
 */
template <typename F, typename TF> std::vector<TF> plu_solve(std::vector<F>&& matrix, std::vector<TF>&& vec);


template <typename T, typename TF = T> std::enable_if_t<is_exact<T>::value, std::vector<TF>> PLU_solve(T* A, std::vector<TF>&& b) {
    int N = b.size();
    std::vector<T> Avec(A, A + N * N);
    return plu_solve(std::move(Avec), std::move(b));
}

// PLU interface function overload for exact types.
template <typename T, typename TF> std::enable_if_t<is_exact<T>::value, std::vector<TF>> plu_solver(std::vector<T>&& A, std::vector<TF>&& b) {
    return plu_solve(std::move(A), std::move(b));
}

template <typename T> std::enable_if_t<!is_exact<T>::value, std::vector<T>> PLU_solve(T* A, std::vector<T>&& b) {
    const size_t basis_dim = b.size();

    // LAPACK CALL INFORMATION
    char trans = 'N';
    int dim(basis_dim);
    int nrhs = 1;
    int LDA = dim;
    int LDB = dim;
    int info(0);

    // store triangular form
    std::vector<int> ipiv(basis_dim * (basis_dim + 1) / 2);

    // solve for coeffs with lapack
    // produces LU factorized form for A
    ZGETRF(&dim, &dim, A, &LDA, &ipiv[0], &info);

    // from LU factorized form, solves for x in A x = b
    ZGETRS(&trans, &dim, &nrhs, A, &LDA, &ipiv[0], b.data(), &LDB, &info);

    if (info != 0) {
        std::cerr << "LAPACK ERROR. info parameter non-zero." << std::endl;
        exit(1);
    }

#ifdef DEBUG_ON
    for (auto& coeff : b) {
        if (coeff != coeff) {
            std::cerr << "ERROR (InversionStrategies.cpp): LAPACK inversion returns NaN coefficient" << std::endl;
            std::cerr << "Coeffs: " << b << std::endl;
            exit(1);
        }
    }
#endif

    // NOTE: LAPACK returns x in b. This notation, of course, becomes confusing.
    return std::move(b);
}


template <typename T> std::enable_if_t<!is_exact<T>::value, std::vector<Vec<T>>> PLU_solve(T* A, std::vector<Vec<T>>&& b) {
    const size_t basis_dim = b.size();

    // LAPACK CALL INFORMATION
    char trans = 'N';
    int dim(basis_dim);
    size_t D = b[0].size();
    int nrhs = D;
    int LDA = dim;
    int LDB = dim;
    int info(0);

    // store triangular form
    std::vector<int> ipiv(basis_dim * (basis_dim + 1) / 2);

    // solve for coeffs with lapack
    // produces LU factorized form for A
    ZGETRF(&dim, &dim, A, &LDA, &ipiv[0], &info);

    // Flatten b
    // Concatenate the different right-hand sides one after the other in the vector.
    std::vector<T> rhs_D;
    rhs_D.reserve(b.size() * D);
    for (unsigned i = 0; i < D; i++) {
        for (auto& it : b) rhs_D.push_back(std::move(it[i]));
    }

    // from LU factorized form, solves for x in A x = b
    ZGETRS(&trans, &dim, &nrhs, A, &LDA, &ipiv[0], rhs_D.data(), &LDB, &info);

    if (info != 0) {
        std::cerr << "LAPACK ERROR. info parameter non-zero." << std::endl;
        exit(1);
    }

    // Unflatten the result.
    std::vector<Vec<T>> result_pack(basis_dim, Vec<T>(D));
    for (unsigned c = 0; c < D; c++) {
        for (unsigned r =0; r < basis_dim; ++r) result_pack[r][c] = rhs_D[r + c * basis_dim];
    }

    return result_pack;
}

/* INPUT: A - array of pointers to rows of a square matrix having dimension N
 * OUTPUT: Matrix A is changed, it contains both matrices L-E and U as A=(L-E)+U such that P*A=L*U.
 *        The permutation matrix is not stored as a matrix, but in an integer vector P of size N+1
 *        containing column indexes where the permutation matrix has "1".
 */
template <typename F> void LUPDecompose(std::vector<F*>& A, const int N, std::vector<int>& P) {

    auto initialA = A;

    for (int i = 0; i < N; i++) {
        P[i] = i; // Unit permutation matrix, P[N] initialized with N
    }

    for (int i = 0; i < N; i++) {

        int inonzero = -1;

        for (int k = i; k < N; k++) {
            if (A[k][i] != F(0)) {
                inonzero = k;
                break;
            }
        }
        if (inonzero == -1) {
#ifdef USE_FINITE_FIELDS
            throw DivisionByZeroException{};
#else
            std::cerr << "{";
            bool firstr = true;
            for (auto& row : initialA) {
                if (!firstr) { std::cerr << "," << std::endl; }
                bool firstc = true;
                std::cerr << "{";
                for (int n = 0; n < N; n++) {
                    if (!firstc) { std::cerr << ","; }
                    std::cerr << row[n];
                    firstc = false;
                }
                std::cerr << "}";
                firstr = false;
            }
            std::cerr << "}";
            exit(1);
#endif
        }

        if (inonzero != i) {
            // pivoting P
            std::swap(P[i], P[inonzero]);

            // pivoting rows of A
            std::swap(A[i], A[inonzero]);
        }

        F pivotinv = F(1) / A[i][i];
#if _TRY_PARALLEL_THREADS_IN_LUPDecompose
        // dynamical assignment
        // int nthreads = int( std::thread::hardware_concurrency() );
        int nthreads(10);
        int split((N - (i + 1)) / nthreads);
        auto lambda = [&A, i, N](int jj_min, int jj_max) {
            for (int jj = jj_min; jj < jj_max; jj++)
                for (int k = i + 1; k < N; k++) A[jj][k] -= A[jj][i] * A[i][k];
        };
        std::vector<std::future<void>> vf;
        for (int j = i + 1; j < N; j++) { A[j][i] *= pivotinv; }
        // avoid parallelizing in small sectors
        if (split > 10) {
            for (int j = 0; j < nthreads; j++) {
                int start = j * split + (i + 1);
                int end = (j + 1) * split + (i + 1);
                if (j + 1 == nthreads) end = N;
                vf.push_back(std::async(lambda, start, end));
            }
        }
        else {
            for (int j = i + 1; j < N; j++) {
                for (int k = i + 1; k < N; k++) A[j][k] -= A[j][i] * A[i][k];
            }
        }
#else
        for (int j = i + 1; j < N; j++) {
            A[j][i] *= pivotinv;

            for (int k = i + 1; k < N; k++) A[j][k] -= A[j][i] * A[i][k];
        }
#endif
    }
}

/* INPUT: A,P filled in LUPDecompose; b - rhs vector; N - dimension
 * OUTPUT: x - solution vector of A*x=b
 */
template <typename F, typename TF> std::vector<TF> LUPSolve(const std::vector<F*>& A, const std::vector<int>& P, const std::vector<TF>& b, const int N) {

    std::vector<TF> x(N);
    for (int i = 0; i < N; i++) {
        x[i] = b[P[i]];

        for (int k = 0; k < i; k++) x[i] -= A[i][k] * x[k];
    }

    for (int i = N - 1; i >= 0; i--) {
        for (int k = i + 1; k < N; k++) x[i] -= A[i][k] * x[k];

        // Note. Implementing like this achieves the minimal number of
        // divisions in the back/forward substitution.
        x[i] /= A[i][i];
    }

    return x;
}

template <typename F, typename TF> std::vector<TF> plu_solve(std::vector<F>&& matrix, std::vector<TF>&& vec) {
    using std::vector;

    const int N = vec.size();

    // Transpose to column major
    // TODO: can we avoid this step?
    for (int i = 0; i < N; i++) {
        for (int j = i+1; j < N; j++) { 
            F store = matrix[i + j * N]; 
            matrix[i + j * N] = matrix[j + i * N]; 
            matrix[j + i * N] = store; 
        }
    }

    vector<F*> A(N);
    for (int i = 0; i < N; i++) { A[i] = &matrix.at(i * N); }

    vector<int> P(N);
    LUPDecompose(A, N, P);

    return LUPSolve(A, P, vec, N);
}

template <typename T> bool is_zero(T x) {
    if constexpr (is_floating_point_v<T>) {
        using std::abs;
        // TODO: the threshold is rather random here, can we do better?
        return abs(x) < std::numeric_limits<remove_complex_t<T>>::epsilon()*remove_complex_t<T>{10000};
    }
    else if constexpr (is_exact_v<T>) {
        return x == T(0);
    }
    else{
        static_assert(_delayed_static_false_v<T>, "Not implemented!");
    }
}

template <typename T> std::vector<T> potentially_transpose(const std::vector<T>& vec) { return vec; }

template <typename T> std::vector<std::vector<T>> potentially_transpose(const std::vector<std::vector<T>>& vec) {
    std::vector<std::vector<T>> transpose(vec.at(0).size(), std::vector<T>(vec.size(), T(0)));
    for (size_t i = 0; i < vec.size(); i++) {
        for (size_t j = 0; j < vec.at(i).size(); i++) { transpose.at(j).at(i) = vec.at(i).at(j); }
    }

    return transpose;
}



// plu from Eigen for vector right-hand side.
template <typename T>
std::enable_if_t<!is_exact<T>::value, std::vector<T>>
plu_solve_eigen(std::vector<T>&& A, std::vector<T>&& b) {
#ifdef USE_EIGEN
    using namespace Eigen;

    // Construct the matrix of the linear system.
    // To be consistent with the data that is being passed to the LAPACK routines we use a column-major
    // alignment of the data. If in the future, LAPACK is dropped, it might pay-off to use the more 
    // intuitive row-major alignment. In this case, just comment out the RowMajor below and the construction
    // from the raw buffer will work correctly.
    Map<Matrix<T, Dynamic, Dynamic/*, RowMajor*/>> matrix(A.data(), b.size(), (int)(A.size() / b.size()));

    // Construct the right-hand side of the equation.
    Map<Matrix<T, Dynamic, 1>> rhs(b.data(), b.size());

    Matrix<T, 1, Dynamic> solution = matrix.lu().solve(std::move(rhs));

    return std::vector<T>(solution.data(), solution.data() + solution.size());
#else 
    throw std::runtime_error("Eigen not properly linked");
#endif
}

// plu from Eigen for matrix right-hand side.
template <typename T>
std::enable_if_t<!is_exact<T>::value, std::vector<Vec<T>>>
plu_solve_eigen(std::vector<T>&& A, std::vector<Vec<T>>&& b) {
#ifdef USE_EIGEN
    using namespace Eigen;

    // Construct the matrix of the linear system.
    // To be consistent with the data that is being passed to the LAPACK routines we use a column-major
    // alignment of the data. If in the future, LAPACK is dropped, it might pay-off to use the more 
    // intuitive row-major alignment. In this case, just comment out the RowMajor below and the construction
    // from the raw buffer will work correctly.
    Map<Matrix<T, Dynamic, Dynamic/*, RowMajor*/>> matrix(A.data(), b.size(), (int)(A.size() / b.size()));
  
    // Construct the rhs of the system.
    size_t D = b[0].size();
    Matrix<T, Dynamic, Dynamic> rhs(b.size(), D);

    for (std::size_t r = 0; r < b.size(); ++r) {
        for (std::size_t c = 0; c < D; ++c) { rhs(r, c) = b[r][c]; }
    }

    // If PLU should be used to solve the system, uncomment the following line. In that case 
    // make sure that the passed matrix is a square matrix.
    Matrix<T, Dynamic, Dynamic> solution = matrix.lu().solve(std::move(rhs));

    std::vector<Vec<T>> to_return;

    // Unfold the result
    for (int r = 0; r < solution.rows(); ++r) {
        std::vector<T> tmp;
        for (int c = 0; c < solution.row(r).cols(); ++c) { tmp.push_back(solution(r, c)); }
        to_return.push_back(std::move(tmp));
    }

    return to_return;
#else
    throw std::runtime_error("Eigen not properly linked!");
#endif
}

// For double precision types allow to choose between eigen and Lapack
template <typename T>
std::enable_if_t<!is_exact<T>::value && (std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<T>>
plu_solver(std::vector<T>&& A, std::vector<T>&& b) {
#ifdef EIGEN_FOR_DOUBLE_PRECISION
    return plu_solve_eigen(std::move(A), std::move(b));
#else 
    return PLU_solve(A.data(), std::move(b));
#endif
}

// For high precision types only use eigen
template <typename T>
std::enable_if_t<!is_exact<T>::value && !(std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<T>>
plu_solver(std::vector<T>&& A, std::vector<T>&& b) {
    return plu_solve_eigen(std::move(A), std::move(b));
}

// For double precision types allow to choose between eigen and Lapack
template <typename T>
std::enable_if_t<!is_exact<T>::value && (std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<Vec<T>>>
plu_solver(std::vector<T>&& A, std::vector<Vec<T>>&& b) {
#ifdef EIGEN_FOR_DOUBLE_PRECISION
    return plu_solve_eigen(std::move(A), std::move(b));
#else 
    return PLU_solve(A.data(), std::move(b));
#endif
}

// For high precision types only allow Eigen
template <typename T>
std::enable_if_t<!is_exact<T>::value && !(std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<Vec<T>>>
plu_solver(std::vector<T>&& A, std::vector<Vec<T>>&& b) {
    return plu_solve_eigen(std::move(A), std::move(b));

}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
//                                                              QR linear solver.
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------------------

// qr solver from lapack for double precision and matrix right-hand side.
template <typename T> std::enable_if_t<!is_exact<T>::value, std::vector<Vec<T>>> qr_solve_lapack(std::vector<T>&& A, std::vector<Vec<T>>&& b) {
    // LAPACK CALL INFORMATION
    char trans = 'N';
    size_t D = b[0].size();
    int nrhs = D;
    int num_equations = b.size();
    int basis_dim = A.size() / b.size();
    assert(A.size() % b.size() == 0);
    int LDA = num_equations;
    int LDB = num_equations;
    int info(0);

    std::vector<T> rhs_D;
    rhs_D.reserve(b.size() * D);

    // Despite LAPACK docmentation says that the RHS is arranged in a matrix with columns
    // being one Ds, which one would think means row-major order in FORTRAN, apparently
    // they are arranged consequently instead. So we have to rearrange the values and
    // later pack them back.
    for (unsigned i = 0; i < D; i++) {
        for (auto& it : b) rhs_D.push_back(std::move(it[i]));
    }

    {
        int MN = std::min(num_equations, basis_dim);
        int lwork = MN + std::max(MN, int(D));
        std::vector<T> work(lwork);
        work.shrink_to_fit();

        ZGELS(&trans, &num_equations, &basis_dim, &nrhs, A.data(), &LDA, rhs_D.data(), &LDB, &work[0], &lwork, &info);
    }

    if (info != 0) {
        std::cerr << "LAPACK ERROR. Info parameter = " << info << std::endl;
        exit(1);
    }

    // Lapack writes the result into b. However, since the 
    // dimensionality of the result `basis_dim` is smaller
    // than the dimensionality of the right-hand side 
    // `num_equations`, the excess entries are filled by some 
    // sums of squares according to the docs.
    std::vector<Vec<T>> to_return(basis_dim, Vec<T>(D));
    unsigned g = 0;
    for (size_t i = 0; i < D; i++) {
        for (int j = 0; j < (int) b.size(); j++) {
            // In each result vector, only take the entries up to `basis_dim` 
            // and drop the entries between `basis_dim` + 1 and `num_equatios`.
            if (j < basis_dim){
                to_return[j][i] = std::move(rhs_D[g]);
            }
            ++g;
        }
    }

    return to_return;
}

// qr solver from Lapack for double precision with vector right-hand side.
template <typename T> std::enable_if_t<!is_exact<T>::value, std::vector<T>> qr_solve_lapack(std::vector<T>&& A, std::vector<T>&& b) {
    
    int num_equations = b.size();
    int basis_dim = A.size() / b.size();
    
    if (basis_dim == 0) { return {}; }

#ifdef DEBUG_ON
    if (basis_dim > 1000) { std::cout << "WARNING: Inverting large matrix of side " << basis_dim << std::endl; }
#endif

    // LAPACK CALL INFORMATION
    char trans = 'N';
    int nrhs = 1;
    int LDA = num_equations;
    int LDB = num_equations;
    int info(0);

    int lwork = 2 * std::min(num_equations, basis_dim);
    std::vector<T> work(lwork);

    ZGELS(&trans, &num_equations, &basis_dim, &nrhs, &A[0], &LDA, &b[0], &LDB, &work[0], &lwork, &info);

    if (info != 0) {
        std::cerr << "LAPACK ERROR. Info parameter = " << info << std::endl;
        exit(1);
    }

#ifdef DEBUG_ON
    for (auto& coeff : b) {
        if (coeff != coeff) { throw std::runtime_error("ERROR (InversionStrategies.cpp): LAPACK inversion returns NaN coefficient"); }
    }
#endif

    std::vector<T> to_return(basis_dim);

    // Cut away end of return vector and only keep entries 
    // up to basis_dim.
    for (int i = 0; i < basis_dim; ++i) {
        to_return[i] = b[i];
    }

    return to_return;
}

// QR solver from Eigen for vector right-hand side.
template <typename T>
std::enable_if_t<!is_exact<T>::value, std::vector<T>>
qr_solve_eigen(std::vector<T>&& A, std::vector<T>&& b) {
#ifdef USE_EIGEN
    using namespace Eigen;

    // Construct the matrix of the linear system.
    // To be consistent with the data that is being passed to the LAPACK routines we use a column-major
    // alignment of the data. If in the future, LAPACK is dropped, it might pay-off to use the more 
    // intuitive row-major alignment. In this case, just comment out the RowMajor below and the construction
    // from the raw buffer will work correctly.
    Map<Matrix<T, Dynamic, Dynamic/*, RowMajor*/>> matrix(A.data(), b.size(), (int)(A.size() / b.size()));

    // Construct the right-hand side of the equation.
    Map<Matrix<T, Dynamic, 1>> rhs(b.data(), b.size());

    Matrix<T, 1, Dynamic> solution = matrix.householderQr().solve(std::move(rhs));

    return std::vector<T>(solution.data(), solution.data() + solution.size());
#else 
    throw std::runtime_error("Eigen not properly linked!");
#endif
}

// QR solver from Eigen for matrix right hand side.
template <typename T>
std::enable_if_t<!is_exact<T>::value, std::vector<Vec<T>>>
qr_solve_eigen(std::vector<T>&& A, std::vector<Vec<T>>&& b) {
#ifdef USE_EIGEN
    using namespace Eigen;

    // Construct the matrix of the linear system.
    // To be consistent with the data that is being passed to the LAPACK routines we use a column-major
    // alignment of the data. If in the future, LAPACK is dropped, it might pay-off to use the more 
    // intuitive row-major alignment. In this case, just comment out the RowMajor below and the construction
    // from the raw buffer will work correctly.
    Map<Matrix<T, Dynamic, Dynamic/*, RowMajor*/>> matrix(A.data(), b.size(), (int)(A.size() / b.size()));
  
    // Construct the rhs of the system.
    size_t D = b[0].size();
    Matrix<T, Dynamic, Dynamic> rhs(b.size(), D);

    for (std::size_t r = 0; r < b.size(); ++r) {
        for (std::size_t c = 0; c < D; ++c) { rhs(r, c) = b[r][c]; }
    }

    // If PLU should be used to solve the system, uncomment the following line. In that case 
    // make sure that the passed matrix is a square matrix.
    // Matrix<T, Dynamic, Dynamic> solution = linear_system.first.lu().solve(linear_system.second);
    Matrix<T, Dynamic, Dynamic> solution = matrix.householderQr().solve(std::move(rhs));

    std::vector<Vec<T>> to_return;

    // Unfold the result
    for (int r = 0; r < solution.rows(); ++r) {
        std::vector<T> tmp;
        for (int c = 0; c < solution.row(r).cols(); ++c) { tmp.push_back(solution(r, c)); }
        to_return.push_back(std::move(tmp));
    }

    return to_return;
#else 
    throw std::runtime_error("Eigen not properly installed!");
#endif
}

// For double precision types allow Eigen or Lapack
template <typename T>
std::enable_if_t<!is_exact<T>::value && (std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<Vec<T>>>
qr_solver(std::vector<T>&& A, std::vector<Vec<T>>&& b) {
#ifdef EIGEN_FOR_DOUBLE_PRECISION
    return qr_solve_eigen(std::move(A), std::move(b));
#else 
    return qr_solve_lapack(std::move(A), std::move(b));
#endif
}

// For high-precision types only allow eigen
template <typename T>
std::enable_if_t<!is_exact<T>::value && !(std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<Vec<T>>>
qr_solver(std::vector<T>&& A, std::vector<Vec<T>>&& b) {
    return qr_solve_eigen(std::move(A), std::move(b));
}


// For double precision types allow Eigen or Lapack
template <typename T>
std::enable_if_t<!is_exact<T>::value && (std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<T>>
qr_solver(std::vector<T>&& A, std::vector<T>&& b) {
#ifdef EIGEN_FOR_DOUBLE_PRECISION
    return qr_solve_eigen(std::move(A), std::move(b));
#else 
    return qr_solve_lapack(std::move(A), std::move(b));
#endif
}

// For high-precision types only allow eigen
template <typename T>
std::enable_if_t<!is_exact<T>::value && !(std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<T>>
qr_solver(std::vector<T>&& A, std::vector<T>&& b) {
    return qr_solve_eigen(std::move(A), std::move(b));
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
//                                  Construct linear system of given dimension.
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------------------

/**
 * NOTE that the point_gen is passed by reference such that it modifies the original one.
 * This is done to be able to refill different values with the same generator.
 * If a copy is done, the original generator will never know if the point have been generated.
 */
template <typename T1, typename T2, typename X>
std::pair<std::vector<T1>, std::vector<T2>> build_linear_system_row_major(std::function<T2(X)> function_eval, std::function<std::vector<T1>(X)> basis_eval,
                                                                          int basis_dim, std::function<X()>& point_gen, int num_equations) {

#ifdef DEBUG_ON
    // Zero counter for basis elements
    std::vector<size_t> zero_counts(basis_dim, 0);
#endif

    // Build the matrix and vector. (Equation A.x = b)
    std::vector<T1> A(num_equations * basis_dim, T1(0));
    std::vector<T2> b(num_equations);

    // Fill arrays
    for (size_t i = 0; i < (size_t)(num_equations); i++) {
        auto random_point = point_gen();
        b[i] = function_eval(random_point);

#ifdef DEBUG_ON
        if (b[i] != b[i]) {
            std::cerr << "ERROR (InversionStrategies.cpp): function_eval returns NaN" << std::endl;
            exit(1);
        }
#endif

        // Fill matrix row, as column because FORTRAN. (Row-major ordering)
#ifdef TIMING_ON
        timing::surface_terms_timing.start();
#endif
        auto matrix_row = basis_eval(random_point);
#ifdef TIMING_ON
        timing::surface_terms_timing.stop();
#endif
        for (size_t j = 0; j < (size_t)(basis_dim); j++) {

#ifdef DEBUG_ON
            if (is_zero(matrix_row[j])) { zero_counts.at(j)++; }
            if (matrix_row[j] != matrix_row[j]) {
                std::cerr << "ERROR (InversionStrategies.cpp): basis_eval returns NaN when evaluating inversion matrix element (" << i << "," << j << ")"
                          << std::endl;
                exit(1);
            }
#endif

            A[i + j * num_equations] = matrix_row[j];
        }
    }

    // Transpose b if T2 is vector valued.
    potentially_transpose(b);

#ifdef DEBUG_ON
    // Zero basis elements.
    std::vector<size_t> found_always_zeros;
    for (size_t i = 0; i < zero_counts.size(); i++) {
        if (zero_counts.at(i) == (size_t)(basis_dim)) { found_always_zeros.push_back(i); }
    }
    if (found_always_zeros.size() > 0) {
        for (auto& i : found_always_zeros) { std::cout << "ERROR: basis element " << i << " is  zero on all sampled points." << std::endl; }
        exit(1);
    }
#endif

    return make_pair(A, b);
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
//                                  Interface to compute the coefficients.
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------------------

template <typename TF, typename F, typename X>
std::enable_if_t<is_exact<F>::value, std::vector<TF>>
get_coeffs_default_strategy(std::function<TF(X)> function_eval, std::function<std::vector<F>(X)> basis_eval, int basis_dim, std::function<X()> point_gen) {
    if (basis_dim == 0) { return {}; }

#ifdef DEBUG_ON
    if (basis_dim > 1000) { std::cout << "WARNING: Inverting large matrix of size " << basis_dim << std::endl; }
#endif

    std::vector<TF> result;
    for (unsigned i = 0; i <= 2; ++i) {
        try {
            auto linear_system = build_linear_system_row_major(function_eval, basis_eval, basis_dim, point_gen, basis_dim);
            auto& A = linear_system.first;
            auto& b = linear_system.second;

            START_TIMER_T(PLU_solve, F)
            result = plu_solve(std::move(A), std::move(b));
            STOP_TIMER(PLU_solve);
        }
#ifdef USE_FINITE_FIELDS
        catch (const DivisionByZeroException&) {
#ifdef DEBUG_ON
            _WARNING("Caught DivisionByZeroException while fitting! attempt #", i + 1);
#endif
            if (i < 2) continue;

            throw DivisionByZeroException();
        }
#else
        catch (...) {
            throw;
        }
#endif
        return result;
    }
    throw std::runtime_error("Failed to fit the function!");
}

// Currently a wrapper over ZGELS.
template <typename T, typename X>
std::enable_if_t<is_floating_point<T>::value, std::vector<Vec<T>>> get_coeffs_default_strategy(std::function<Vec<T>(X)> function_eval,
                                                                                                  std::function<std::vector<T>(X)> basis_eval, int basis_dim,
                                                                                                  std::function<X()> point_gen) {

    if (basis_dim == 0) { return {}; }

#ifdef DEBUG_ON
    if (basis_dim > 1000) { std::cout << "WARNING: Inverting large matrix of side " << basis_dim << std::endl; }
#endif

    // Uncomment the following block if PLU shall be used on the high-precision types for speed-up.
    // MPACK QR factorization is broken as well as having multiple RHS (for many Ds), so we have to do this
    // {
    //     constexpr bool is_higher_precision =
    //         std::is_same<T, RHP>::value or std::is_same<T, CHP>::value or std::is_same<T, CVHP>::value or std::is_same<T, RVHP>::value;

    //     if (is_higher_precision) {
    //         auto linear_system = build_linear_system_row_major(function_eval, basis_eval, basis_dim, point_gen, basis_dim);
    //         return plu_solver(std::move(linear_system.first), std::move(linear_system.second));
    //     }
    // }

    // Number of additional equations for the qr solver.
    int num_equations = _qr_number_of_equations(basis_dim);

    auto linear_system = build_linear_system_row_major(function_eval, basis_eval, basis_dim, point_gen, num_equations);

    // return qr_solve_eigen(std::move(linear_system.first), std::move(linear_system.second));
    return qr_solver(std::move(linear_system.first), std::move(linear_system.second));
}

// Currently a wrapper over ZGELS.
template <typename T, typename X>
std::enable_if_t<is_floating_point<T>::value, std::vector<T>>
get_coeffs_default_strategy(std::function<T(X)> function_eval, std::function<std::vector<T>(X)> basis_eval, int basis_dim, std::function<X()> point_gen) {

    if (basis_dim == 0) { return {}; }

#ifdef DEBUG_ON
    if (basis_dim > 1000) { std::cout << "WARNING: Inverting large matrix of side " << basis_dim << std::endl; }
#endif

    // Number of additional equations for the qr solver.
    int num_equations = _qr_number_of_equations(basis_dim);
    

    auto linear_system = build_linear_system_row_major(function_eval, basis_eval, basis_dim, point_gen, num_equations);
    return qr_solver(std::move(linear_system.first), std::move(linear_system.second));
}

// Simple fourier transform
template <typename T> T fourier_coefficient(std::function<T(T)> f, int coeff_i, int max_power) {
    if (coeff_i >= max_power) {
        std::cerr << "ERROR. Asking for too high a power" << std::endl;
        exit(1);
    }

    T result = 0;
    for (int i = 0; i < max_power; i++) {
        T angle = (T(2 * i * M_PI, 0)) / ((T)max_power);
        auto phase = std::exp(T(0, 1) * angle);
        auto twiddle = std::pow(phase, max_power - coeff_i);
        auto f_val = f(phase);
        result += twiddle * f_val;
    }
    return result / ((T)max_power);
}

} // namespace Caravel
