/*
 * momD_conf.hpp
 * Added in 9.3.2016
 *
 * Inherited from V1's mom_conf.hpp from Daniel
 *
 * Header file for classes to deal with multi momentumD configurations
 *
 */

#ifndef MOMD_CONF_HPP_
#define MOMD_CONF_HPP_

#include "CaravelException.h"
#include <iostream>

using std::complex;

namespace Caravel {

template <class T, size_t D> momentumD_configuration<T, D>::momentumD_configuration(size_t n) : nbr(n), ps(n, momD<T, D>{}), ms(n, T{}) {
    ps.shrink_to_fit();
    ms.shrink_to_fit();
}

template <class T, size_t D> momentumD_configuration<T, D>::momentumD_configuration(const std::vector<momD<T, D>>& pl) : nbr(pl.size()), ps(pl) {
    ms.reserve(nbr);
    for (size_t j = 0; j < nbr; ++j) {
        ms.push_back(ps[j] * ps[j]);
    }
    ps.shrink_to_fit();
    ms.shrink_to_fit();
}

template <class T, size_t D> momentumD_configuration<T, D>::momentumD_configuration(std::vector<momD<T, D>>&& pl) : nbr(pl.size()), ps(std::move(pl)) {
    ms.reserve(nbr);
    for (size_t j = 0; j < nbr; ++j) {
        ms.push_back(ps[j] * ps[j]);
    }
    ps.shrink_to_fit();
    ms.shrink_to_fit();
}

template <class T, size_t D> template <size_t Length> momentumD_configuration<T, D>::momentumD_configuration(const std::array<momD<T, D>, Length>& pl) : nbr(Length) {
    for (size_t j = 0; j < Length; j++) {
        ps.push_back(pl[j]);
        ms.push_back(pl[j] * pl[j]);
    }
    ps.shrink_to_fit();
    ms.shrink_to_fit();
}

template <class T, size_t D> template <class... Ts> momentumD_configuration<T, D>::momentumD_configuration(const momD<T, D>& a0, const Ts&... args) {
    constexpr size_t isize = sizeof...(args);
    nbr = isize + 1;

    ps = {a0, args...};

    for (const auto& it : ps) { ms.push_back(it * it); }
    ps.shrink_to_fit();
    ms.shrink_to_fit();
}

template <class T, size_t D> template <typename Itype> std::enable_if_t<std::is_integral<Itype>::value, momentumD_configuration<T,D>> momentumD_configuration<T,D>::get_permuted(const std::vector<Itype>& perm) const {
    if (perm.size() != nbr) throw std::invalid_argument("Invalid permutation in momentumD_configuration::get_permuted");

    std::vector<momentumD<T,D>> new_ps;
    new_ps.reserve(nbr);

    for (auto i : perm) { new_ps.push_back(ps[i-1]); }

    return momentumD_configuration<T,D>{std::move(new_ps)};
}


template <class T, size_t D> momentumD_configuration<T, D> momentumD_configuration<T, D>::get_parity_conjugated() const {
    auto new_moms = this->ps;
    for(auto& pi : new_moms){
        pi = pi.get_parity_conjugated();
    }
    return new_moms;
}

template <class T, size_t D> momentumD_configuration<T, D> momentumD_configuration<T, D>::rescale(T factor) const {
    auto new_moms = this->ps;
    for(auto& pi : new_moms){
        pi *= factor;
    }
    return new_moms;
}

template <class T, size_t D> std::ostream& operator<<(std::ostream& s, const momentumD_configuration<T, D>& mc) {
    s << "(";
    for (size_t j = 1; j < mc.n(); j++) { s << j << ": " << mc.p(j) << "," << std::endl; }
    return s << mc.n() << ": " << mc.p(mc.n()) << ")";
}

template <class T, size_t D>
template <typename Itype>
std::enable_if_t<std::is_integral<Itype>::value, momentumD<T, D>> momentumD_configuration<T, D>::Sum(const std::vector<Itype>& vi) const {
    momD<T, D> sum{};
    for (Itype i : vi) { sum += p(i); }
    return sum;
}

template <class T, size_t D> template <class... Ts> momentumD<T,D> momentumD_configuration<T, D>::Sum(Ts... is) const {
    std::array<size_t, sizeof...(is)> a = {static_cast<size_t>(is)...};
    momD<T, D> sum{};
    for (auto i : a) { sum += p(i); }
    return sum;
}

template <class T, size_t D>
momD_conf_reader<T, D>::momD_conf_reader(const char* path, size_t nbr_p) : Caravel::momentumD_configuration<T, D>(0), nth(0), nbr_particles(nbr_p) {
    //_WARNING("momD_conf_reader only implemented so far for D=4!!!");
    input.open(path);
    if (!input) {
        std::string errorStr = "No file ";
        errorStr += path;
        errorStr += " for the constructor mDc_reader::mDc_reader.";
        throw Caravel::CaravelException(errorStr.c_str());
    }
}

template <class T, size_t D> bool momD_conf_reader<T, D>::go_to_pos(std::ios::pos_type pos, size_t n) {
    if (nth == n) return true;
    input.seekg(pos);
    nth = n - 1;
    return next();
}

template <class T, size_t D> bool momD_conf_reader<T, D>::go_to(size_t n) {
    T dummy;
    if (nth == n) return true;
    if (n > nth) {
        for (size_t i = 1; i < n - nth; i++) {
            for (size_t j = 1; j <= nbr_particles; j++) {
                if (!(input >> dummy && input >> dummy && input >> dummy && input >> dummy)) return false;
            }
        }
        nth = n - 1;
        return next();
    }
    else if (n < nth) {
        input.seekg(0, std::ios::beg);
        for (size_t i = 1; i < n; i++) {
            if (!(input >> dummy && input >> dummy && input >> dummy && input >> dummy)) return false;
        }
        nth = n - 1;
    }
    return next();
}

template <class T, size_t D> bool momD_conf_reader<T, D>::next() {
    T e, x, y, z;
    start_pos = input.tellg();
    momentumD_configuration<T, D>::clear();
    momentumD_configuration_base::renew_ID();
    for (size_t j = 1; j <= nbr_particles; j++) {
       std::stringstream ss;
       std::string str;

       // Pull out components, commenting when something gets read wrong.
       input >> str; ss << str;
       if (!(ss >> e)){ 
           std::cout << "Badly formatted energy = " << str << std::endl;
           return false;
       }
       ss.clear();

       input >> str; ss << str;
       if (!(ss >> x)){ 
           std::cout << "Badly formatted px = " << str << std::endl;
           return false;
       }
       ss.clear();

       input >> str; ss << str;
       if (!(ss >> y)){ 
           std::cout << "Badly formatted py = " << str << std::endl;
           return false;
       }
       ss.clear();

       input >> str; ss << str;
       if (!(ss >> z)){ 
           std::cout << "Badly formatted pz = " << str << std::endl;
           return false;
       }
       ss.clear();

       this->insert(Caravel::momD<T, D>(e, x, y, z));
    }
    nth++;
    return true;
}

template <class T, size_t D> void momentumD_configuration<T, D>::clear() {
    ps.clear();
    ms.clear();
    nbr = 0;
    renew_ID();
}

template <class T, size_t D>
sub_momentumD_configuration<T, D>::sub_momentumD_configuration(momentumD_configuration<T, D>& mc) : momentumD_configuration<T, D>(1000) {
    momentumD_configuration<T, D>::nbr = mc.n();
    momentumD_configuration<T, D>::_offset = (mc.n());
    momentumD_configuration<T, D>::_parent = &mc;
}

template <class T, size_t D>
partial_momentumD_configuration<T, D>::partial_momentumD_configuration(momentumD_configuration<T, D>& mc, size_t start_point, size_t max_index)
    : momentumD_configuration<T, D>(1) {
    momentumD_configuration<T, D>::nbr = max_index;
    momentumD_configuration<T, D>::_offset = 0;
    momentumD_configuration<T, D>::_shift = start_point;
    if (start_point == 0) _WARNING("Careful, start_point for partial_momentumD_configuration constructor should be greater than 0!");
    momentumD_configuration<T, D>::_parent = &mc;
}

template <class Te, class T0, size_t D>
momentumD_configuration<Te, D> extend(momentumD_configuration<T0, D>& k, const std::vector<size_t>& indices /* of the momenta within "k" */,
                                      const Te& dummy /* To signal target type */);

template <class T, size_t D> size_t momentumD_configuration<T, D>::insert(const momD<T, D>& m) {
    ps.push_back(m);
    // if (m.type() == _mt_massless) { ms.push_back(T(0));} else { ms.push_back(m*m);}
    ms.push_back(m * m);
    size_t new_position = ++nbr;
    return new_position;
}

template <typename Thigh, size_t D, typename Tlow> inline momentumD_configuration<Thigh, D> to_precision(const momentumD_configuration<Tlow, D>& in, bool f) {
    std::vector<momentumD<Thigh, D>> ms;
    ms.reserve(in.size());
    for (auto& it : in) { ms.push_back(to_precision<Thigh, D>(it)); }

    if (f or (is_complex<Thigh>::value and !is_complex<Tlow>::value)) {
        std::vector<momentumD<Thigh, D>> ms_new;
        for (auto& it : ms) {
            std::vector<Thigh> v(it.get_vector());
            for (size_t i = 2; i < v.size(); i += 2) { v[i] *= Thigh(0, 1); }
            ms_new.emplace_back(v);
        }
        return momentumD_configuration<Thigh, D>(ms_new);
    }

    return momentumD_configuration<Thigh, D>(ms);
}

#define _INSTANTIATE_MOMDCONF_GEN(KEY, TYPE)                                                                                                                   \
    KEY template class momentumD_configuration<TYPE, 4>;                                                                                                       \
    KEY template class momentumD_configuration<TYPE, 5>;                                                                                                       \
    KEY template class momentumD_configuration<TYPE, 6>;                                                                                                       \
    KEY template class momentumD_configuration<TYPE, 7>;                                                                                                       \
    KEY template class momentumD_configuration<TYPE, 8>;                                                                                                       \
    KEY template class sub_momentumD_configuration<TYPE, 4>;                                                                                                   \
    KEY template class sub_momentumD_configuration<TYPE, 5>;                                                                                                   \
    KEY template class sub_momentumD_configuration<TYPE, 6>;                                                                                                   \
    KEY template class sub_momentumD_configuration<TYPE, 7>;                                                                                                   \
    KEY template class sub_momentumD_configuration<TYPE, 8>;                                                                                                   \
    KEY template class partial_momentumD_configuration<TYPE, 4>;                                                                                               \
    KEY template class partial_momentumD_configuration<TYPE, 5>;                                                                                               \
    KEY template class partial_momentumD_configuration<TYPE, 6>;                                                                                               \
    KEY template class partial_momentumD_configuration<TYPE, 7>;                                                                                               \
    KEY template class partial_momentumD_configuration<TYPE, 8>;                                                                                               \
    KEY template class momD_conf_reader<TYPE, 4>;
// mom_conf_reader instantiated so far only for D=4!!!

#define _INSTANTIATE_MOMDCONF(TYPE) _INSTANTIATE_MOMDCONF_GEN(, TYPE)
#define _INSTANTIATE_MOMDCONF_EXTERN(TYPE) _INSTANTIATE_MOMDCONF_GEN(extern, TYPE)

_INSTANTIATE_MOMDCONF_EXTERN(R)
#ifdef HIGH_PRECISION
_INSTANTIATE_MOMDCONF_EXTERN(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_MOMDCONF_EXTERN(RVHP)
#endif
_INSTANTIATE_MOMDCONF_EXTERN(C)
#ifdef HIGH_PRECISION
_INSTANTIATE_MOMDCONF_EXTERN(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_MOMDCONF_EXTERN(CVHP)
#endif
#ifdef USE_FINITE_FIELDS
_INSTANTIATE_MOMDCONF_EXTERN(F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_MOMDCONF_EXTERN(BigRat)
#endif
#endif
#ifdef INSTANTIATE_GMP
_INSTANTIATE_MOMDCONF_EXTERN(RGMP)
_INSTANTIATE_MOMDCONF_EXTERN(CGMP)
#endif

} // namespace Caravel

#endif /* MOMD_CONF_HPP_ */
