#include "Timing.h"
#include "Utilities.h"

namespace Caravel{ namespace timing{


void counter::start(){
    interval_begin = clock::now();
}
void counter::stop(){
    auto end = clock::now();
    total += std::chrono::duration_cast<duration_type>(end-interval_begin);
}

void counter::report(){
    using namespace std::chrono;
    auto ns = total;
    auto h = duration_cast<hours>(ns);
    ns -= h;
    auto m = duration_cast<minutes>(ns);
    ns -= m;
    auto s = duration_cast<seconds>(ns);
    ns -= s;
    auto ms = duration_cast<milliseconds>(ns);

    _MESSAGE("||Timing report||\t",name,"<",type,">:\t\t", h.count(),"h ",m.count(),"m ",s.count(),".",ms.count(),"s");
}

counter::duration_type counter::get_duration() const {
    return total;
}

counter::~counter(){
    report();
}

#ifdef TIMING_ON
counter surface_terms_timing("surface terms evaluation");
#endif


}}
