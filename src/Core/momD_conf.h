/*
 * momD_conf.h
 * Added in 9.3.2016
 *
 * Inherited from V1's mom_conf.h from Daniel
 *
 * Header file for classes to deal with multi momentumD configurations
 *
 */

#ifndef MOMD_CONF_H_
#define MOMD_CONF_H_

#include "CaravelException.h"
#include "Core/Utilities.h"
#include "momD.h"
#include "spinor/spinor.h"
#include "Core/typedefs.h"
#include <algorithm>
#include <array>
#include <complex>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <type_traits>
#include <vector>

namespace Caravel {

//! template base class for momentum configurations
/** The momentumD_configuration_base carries an ID number. This number changes for each new momentum configuration so that it can be used to check that the
 * momD_conf is still the same as the one cached previously.   */
class momentumD_configuration_base {
    long int _mDc_ID;
    static long int momD_conf_next_ID;

  public:
    long int get_ID() const { return _mDc_ID; }
    void renew_ID() {
        _mDc_ID = momD_conf_next_ID;
        momD_conf_next_ID++;
    }
    momentumD_configuration_base() : _mDc_ID(momD_conf_next_ID) { momD_conf_next_ID++; }
    virtual ~momentumD_configuration_base() {}
};

/**
 * Contains a collection of momenta.
 */
template <class T, size_t D> class momentumD_configuration : public momentumD_configuration_base {
  protected:
    size_t nbr{0};
    std::vector<momD<T, D>> ps;
    std::vector<T> ms;

    size_t _offset{0};
    // _shift>0 allows to tag partial_momD_conf
    size_t _shift{0};
    momentumD_configuration<T, D>* _parent{nullptr};

  public:
    /*! \return number of momenta  */
    size_t n() const { return nbr; };
    /*! \return number of momenta  */
    size_t size() const { return nbr; };

    /**
     * Get the momentum with
     * @param index (1-based)
     */
    const momD<T, D>& p(size_t n) const;
    const momD<T, D>& operator[](const size_t n) const { return p(n); };
    const momD<T, D>& operator()(const size_t n) const { return this->p(n); };

    /**
     * Set the
     * @momentum
     * at
     * @param index (1-based)
     */
    void set_p(const momD<T, D>&, size_t n);

    typename std::vector<momD<T, D>>::const_iterator begin() const { return ps.begin(); }
    typename std::vector<momD<T, D>>::const_iterator end() const { return ps.end(); }

    const momentumD_configuration<T, D>& operator=(const std::vector<momD<T, D>>&);
    template <size_t Length> const momentumD_configuration<T, D>& operator=(const std::array<momD<T, D>, Length>&);
    momentumD_configuration() = default;
    /**
     * Construct configuration of size n. All momenta are default constructed.
     */
    momentumD_configuration(size_t n);
    momentumD_configuration(const std::vector<momD<T, D>>&);
    momentumD_configuration(std::vector<momD<T, D>>&&);
    template <size_t Length> momentumD_configuration(const std::array<momD<T, D>, Length>&);
    template <class... Ts> momentumD_configuration(const momD<T, D>&, const Ts&...);

    /**
     * Constructing from other types of momD_conf (i.e. change in
     * dimensionality, or base type).
     */
    template <typename T2, USE_IF((!std::is_same<T2, T>::value))> explicit momentumD_configuration(const momentumD_configuration<T2, D>& other) {
        for (size_t ii = 0; ii < other.n(); ii++) { insert(static_cast<momentumD<T, D>>(other[ii + 1])); }
    }
    template <size_t D2, USE_IF((D2 > D))> explicit momentumD_configuration(const momentumD_configuration<T, D2>& other) {
        for (size_t ii = 0; ii < other.n(); ii++) { insert(static_cast<momentumD<T, D>>(other[ii + 1])); }
    }
    template <size_t D2, USE_IF((D2 < D))> momentumD_configuration(const momentumD_configuration<T, D2>& other) {
        for (size_t ii = 0; ii < other.n(); ii++) { insert(other[ii + 1]); }
    }

    /**
     * Insert a new momentum into the configuration.
     * Will also push the squared masses
     */
    size_t insert(const momD<T, D>&);

    /**
     * Return a new momentum configuration with permuted momenta
     * @param permutation (with 1-based indices)
     */
    template <typename Itype> std::enable_if_t<std::is_integral<Itype>::value, momentumD_configuration<T, D>> get_permuted(const std::vector<Itype>& perm) const;

    /**
     * Returns a copy of *this with all momenta parity conjugated
     */
    momentumD_configuration<T,D> get_parity_conjugated() const;
    /**
     * Returns a copy of *this with all momenta rescaled by a factor
     */
    momentumD_configuration<T,D> rescale(T) const;

    /**
     * Returns invariant s(i1,i2,...)
     */
    template <typename Itype> std::enable_if_t<std::is_integral<Itype>::value, T> s(const std::vector<Itype>&) const;
    /**
     * Returns invariant s(i1,i2,...)
     */
    template <class... Ts> T s(Ts...) const;
    T sp(size_t i, size_t j); /**< scalar product p(i).p(j) */

    //! Squared mass of the momentum with index i
    T m2(size_t i) const;

    template <typename Itype>
    std::enable_if_t<std::is_integral<Itype>::value, momentumD<T, D>>
    Sum(const std::vector<Itype>&) const;                    /**< returns sums of momenta with indices in a vector */
    template <class... Ts> momentumD<T, D> Sum(Ts...) const; /**< returns sums of momenta with indices as arguments */

    void clear();

    // 4-D helicity products <ij> spa_4D, and [ij] spb_4D
    T spa_4D(size_t, size_t) const;
    T spb_4D(size_t, size_t) const;
    typedef T value_type;
};

template <class T, size_t D> class sub_momentumD_configuration : public momentumD_configuration<T, D> {
  public:
    sub_momentumD_configuration(momentumD_configuration<T, D>& mc);
    virtual ~sub_momentumD_configuration(){};
};

template <class T, size_t D> std::ostream& operator<<(std::ostream&, const momentumD_configuration<T, D>&);

// typedefs

// type alias / alias template
template <typename T, size_t D> using momD_conf = momentumD_configuration<T, D>;

template <typename T> using mom4D_conf = momentumD_configuration<T, 4>;
template <typename T> using mom5D_conf = momentumD_configuration<T, 5>;
template <typename T> using mom6D_conf = momentumD_configuration<T, 6>;
template <typename T> using mom7D_conf = momentumD_configuration<T, 7>;
template <typename T> using mom8D_conf = momentumD_configuration<T, 8>;

template <typename T, size_t D> using sub_momD_conf = sub_momentumD_configuration<T, D>;

template <typename T> using sub_mom4D_conf = sub_momentumD_configuration<T, 4>;
template <typename T> using sub_mom5D_conf = sub_momentumD_configuration<T, 5>;
template <typename T> using sub_mom6D_conf = sub_momentumD_configuration<T, 6>;
template <typename T> using sub_mom7D_conf = sub_momentumD_configuration<T, 7>;
template <typename T> using sub_mom8D_conf = sub_momentumD_configuration<T, 8>;

// this sub class gives access to limited range of the _parent momD_conf
template <class T, size_t D> class partial_momentumD_configuration : public momentumD_configuration<T, D> {
  public:
    partial_momentumD_configuration(){};
    partial_momentumD_configuration(momentumD_configuration<T, D>& mc, size_t, size_t);
    virtual ~partial_momentumD_configuration(){};
};

// typedefs

template <typename T, size_t D> using partial_momD_conf = partial_momentumD_configuration<T, D>;

template <typename T> using partial_mom4D_conf = partial_momentumD_configuration<T, 4>;
template <typename T> using partial_mom5D_conf = partial_momentumD_configuration<T, 5>;
template <typename T> using partial_mom6D_conf = partial_momentumD_configuration<T, 6>;
template <typename T> using partial_mom7D_conf = partial_momentumD_configuration<T, 7>;
template <typename T> using partial_mom8D_conf = partial_momentumD_configuration<T, 8>;

//! Abstract class for reading momentum configurations from a file. This allows to treat stepping through a file independently from the type (precision) of the
//! points read.
class momD_conf_reader_base {
  public:
    virtual bool next() = 0;
    //! reads the n th configuration from the file
    /** \param n One based index, for  n=1 go_to reads the first entry \return true if the reading has been successful, false if not (for example when not all
     * momentum configurations have been read) This member function reads through the file until it arrives at the required position and is therefore slow. If
     * the position where the momentum configuration to be read is known, one should use go_to_pos instead. \sa go_to_pos*/
    virtual bool go_to(size_t n) = 0;
    //! reads the n th configuration from the file knowing the position from which to start reading
    /** This function is more efficient than go_to \sa go_to \param pos is the position in the file of the momentum to be read. \param n One based index, for
     * n=1 go_to reads the first entry. This is necessary so that the mc reader knows where it is in the file, in case the go_to member is called. \return true
     * if the reading has been successful, false if not (for example when not all momentum configurations have been read)  */
    virtual bool go_to_pos(std::ios::pos_type pos, size_t n) = 0;
    virtual ~momD_conf_reader_base(){};
};

//! Class for reading momentum configurations with massless momenta from a file
/** A mDc_reader object is constructed by giving it the name of the file to read and the number of momenta to read in for each new momentum configuration. When
 * constructed, a new momentum configuration is read from the file by using the next() member function. */
template <class T, size_t D> class momD_conf_reader : public momD_conf_reader_base, public momentumD_configuration<T, D> {
  protected:
    std::ifstream input;
    size_t nth;
    size_t nbr_particles;
    std::ios::pos_type start_pos;

  public:
    //! constructor
    momD_conf_reader(const char* path, size_t nbr_p);
    //! reads the next configuration from the file
    /** \return true if the reading has been successful, false if not (for example when not all momentum configurations have been read) */
    virtual bool next();
    //! reads the n th configuration from the file
    /** \param n One based index, for  n=1 go_to reads the first entry \return true if the reading has been successful, false if not (for example when not all
     * momentum configurations have been read) This member function reads through the file until it arrives at the required position and is therefore slow. If
     * the position where the momentum configuration to be read is known, one should use go_to_pos instead. \sa go_to_pos*/
    virtual bool go_to(size_t n);
    //! reads the n th configuration from the file knowing the position from which to start reading
    /** This function is more efficient than go_to \sa go_to \param pos is the position in the file of the momentum to be read. \param n One based index, for
     * n=1 go_to reads the first entry. This is necessary so that the mc reader knows where it is in the file, in case the go_to member is called. \return true
     * if the reading has been successful, false if not (for example when not all momentum configurations have been read)  */
    virtual bool go_to_pos(std::ios::pos_type pos, size_t n);
};

//! class for multiple precision momentum configuration readers
//** multi_precision_reader_HP objects are derived from momD_conf_reader<RHP,4> so they can be used as such, but they contain a VHP momD_conf that allow to
//"jump" to a higher precision when required. The VHP momD_conf only uses next() for the requested momentum configuration. */
#ifdef HIGH_PRECISION
class multi_precision_reader_HP : public momD_conf_reader<RHP, 4> {
    momD_conf_reader<RVHP, 4> _mDc_VHP;

  public:
    //! constructor
    multi_precision_reader_HP(const char* path, size_t nbr_p) : momD_conf_reader<RHP, 4>(path, nbr_p), _mDc_VHP(path, nbr_p){};
    //! reference to the corresponding VHP momentum configuration
    momD_conf<RVHP, 4>& mDc_VHP() {
        _mDc_VHP.go_to_pos(start_pos, nth);
        return _mDc_VHP;
    };
};
#endif

//! class for multiple precision momentum configuration readers
//** multi_precision_reader objects are derived from momD_conf_reader<R> so they can be used as such, but they contain a HP and VHP momD_conf that allow to
//"jump" to a higher precision when required. The HP and VHP momD_conf only use next() for the requested momentum configuration. */

#ifdef HIGH_PRECISION
class multi_precision_reader : public momD_conf_reader<R, 4> {
    multi_precision_reader_HP _mDc_HP;

  public:
    //! constructor
    multi_precision_reader(const char* path, size_t nbr_p) : momD_conf_reader<R, 4>(path, nbr_p), _mDc_HP(path, nbr_p){};
    //! reference to the corresponding HP momentum configuration
    momD_conf<RHP, 4>& mDc_HP() {
        _mDc_HP.go_to_pos(start_pos, nth);
        return _mDc_HP;
    };
};
#endif

/**
 * Cast configuration to a different floating point type. Tlow can be BigRat
 * @param mom_conf
 * @param force to switch signature or not
 * By default if Thigh is complex and Tlow is real conversion will be done
 */
template <typename Thigh, size_t D, typename Tlow>
inline momentumD_configuration<Thigh, D> to_precision(const momentumD_configuration<Tlow, D>& in, bool force_convert_to_standard_signature = false);

typedef momD_conf_reader<R, 4> mDc_reader;
#ifdef HIGH_PRECISION
typedef momD_conf_reader<RHP, 4> mDc_reader_HP;
#endif
#ifdef VERY_HIGH_PRECISION
typedef momD_conf_reader<RVHP, 4> mDc_reader_VHP;
#endif

#include "momD_conf_inline.h"

} // namespace Caravel

#include "momD_conf.hpp" // Template code

#endif /*MOMD_CONF_H_*/
