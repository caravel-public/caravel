#include "BigInt.h"
#include "misc/MiscMath.h"
#include <iostream>
#include <string_view>

namespace Caravel{

  BigInt pow(const BigInt& x, int n) { return prod_pow(x, n); }
  std::istream& operator>>(std::istream& i, BigInt& a) { return (i >> a.val); }
  std::ostream& operator<<(std::ostream& o, const BigInt& a) { return (o << a.val); }

  BigInt abs(const BigInt& x) { return BigInt(abs(x.val)); }

}

namespace std {
size_t hash<mpz_srcptr>::operator()(const mpz_srcptr x) const {
    std::string_view view{reinterpret_cast<char*>(x->_mp_d), abs(x->_mp_size) * sizeof(mp_limb_t)};
    return std::hash<std::string_view>{}(view);
}
} // namespace std
