/**
 * @file Series.cpp
 *
 * @brief Instantiation for the building of the Caravel library of Series class templates
 */

#include "Core/Series.h"

#ifdef INSTANTIATE_RATIONAL
#include "Core/BigRat.h"
#endif

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "Core/qd_suppl.h"
#endif

namespace Caravel {

// g++ still requires explicit instantiation -- we do it for complex series

#define INSTANTIATE(TYPE)  \
template class Series<TYPE>;\
template Series<TYPE> operator+(const Series<TYPE>&,const Series<TYPE>&);\
template Series<TYPE> operator+(const Series<TYPE>&,const TYPE&);\
template Series<TYPE> operator+(const TYPE& v1,const Series<TYPE>& s2);\
template Series<TYPE> operator-(const Series<TYPE>& s1,const Series<TYPE>& s2);\
template Series<TYPE> operator-(const Series<TYPE>& s1,const TYPE& v2);\
template Series<TYPE> operator-(const TYPE& v1,const Series<TYPE>& s2);\
template Series<TYPE> operator-(const Series<TYPE>& s1);\
template Series<TYPE> operator*(const Series<TYPE>& s1,const Series<TYPE>& s2);\
template Series<TYPE> operator*(const Series<TYPE>& s1,const TYPE& v2);\
template Series<TYPE> operator*(const TYPE& v1,const Series<TYPE>& s2);\
template Series<TYPE> operator/(const Series<TYPE>& s,const TYPE& v);\
template Series<TYPE> operator^(const Series<TYPE>& s,unsigned int p);\
template std::ostream& operator<<(std::ostream&, const Series<TYPE>&);


INSTANTIATE(C)
#ifdef HIGH_PRECISION
INSTANTIATE(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE(CVHP)
#endif


INSTANTIATE(R)
#ifdef HIGH_PRECISION
INSTANTIATE(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE(RVHP)
#endif

#if INSTANTIATE_GMP
INSTANTIATE(CGMP)
INSTANTIATE(RGMP)
#endif

#ifdef USE_FINITE_FIELDS 
INSTANTIATE(F32)
#endif // USE_FINITE_FIELDS


#ifdef INSTANTIATE_RATIONAL
INSTANTIATE(BigRat)
#endif


} // namespace Caravel
