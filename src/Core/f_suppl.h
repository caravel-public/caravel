#ifndef F_SUPPL_H_
#define F_SUPPL_H_

#define _USE_SUN_CC __SUNPRO_CC
#define _USE_GCC __GNUC__
#define _USE_PGCC __PGI

#include <complex>
#include <functional>


#if _USE_PGCC
// 26.2.8/5 log(__z): Reurns the natural complex logaritm of __z.
//                    The branch cut is along the negative axis.
template<typename _Tp>
  inline complex<_Tp>
  log(const complex<_Tp>& __z)
  { return complex<_Tp>(log(std::abs(__z)), std::arg(__z)); }

#endif

namespace Caravel {
inline double to_double(const double& r){return r;}
}
#endif /*F_SUPPL_H_*/
