#include "Core/CaravelInput.h"
#include "misc/TestUtilities.hpp"
#include "wise_enum.h"
#include "Core/enumerate.h"

#include <numeric>
#include <cassert>
#include <vector>
#include <set>

namespace Caravel {

void PartialAmplitudeInput::canonical_order(){
    size_t m = std::min_element(particles.begin(), particles.end(),[](Particle& p1, Particle& p2){return p1.mom_index() < p2.mom_index();})->mom_index();
    while (particles.front().mom_index() != m) { std::rotate(particles.begin(), particles.begin() + 1, particles.end()); };
}

void PartialAmplitudeInput::init(){
  //canonical_order();
    check_construction();
}

PartialAmplitudeInput::PartialAmplitudeInput(MathList all) {

    if(all.head != "PartialAmplitudeInput") {
        throw std::invalid_argument("Expected PartialAmplitudeInput, got " + all.head);
    }

    for (auto it : MathList(all[0], "Particles")) {
        particles.push_back(Particle(MathList(it, "Particle")));
    }

    if(all.size()==2){
        cs = ColourStructureInput{MathList{all[1]}};
    }

    if(all.size() >= 3){
        _WARNING_RED("Did not expect more than 2 elements in ", all); 
        std::exit(1);
    }

    init();
}

PartialAmplitudeInput::PartialAmplitudeInput(std::vector<Particle> s) : particles(std::move(s)) {
    init();
}
PartialAmplitudeInput::PartialAmplitudeInput(std::vector<Particle> s, ColourStructureInput scsi) : particles(std::move(s)), cs(std::move(scsi)) {
    init();
}

void PartialAmplitudeInput::check_construction() {
    if (particles.size() <= 2) {
        _WARNING_R("Amplitudes should have >=3 external particles!");
        std::exit(1);
    }

    for (const auto& p : particles) {
        if (p.mom_index() == 0) {
            _WARNING("The momentum index of at least one particle was not provided! Will assign default indices!");
            size_t mind = 1;
            for (auto& p : particles) {
                p.set_momentum_index(mind);
                ++mind;
            }
            break;
        }
    }

    // TODO: order in momenta indices and later in colour

    // check if momenta indices make sense
    {
        std::vector<size_t> ordered_mom_indices(get_multiplicity());
        std::iota(ordered_mom_indices.begin(), ordered_mom_indices.end(), 1);

        std::vector<size_t> all_ind;
        for (const auto& it : particles) all_ind.push_back(it.mom_index());
        std::sort(all_ind.begin(),all_ind.end());

        if (all_ind != ordered_mom_indices) {
            _WARNING_R("Momenta indices ", all_ind, " do not seem to make sense!");
            std::exit(1);
        }
    }

    // check n fermions == n antifermions
    {
        unsigned n_ferm = 0;
        unsigned n_antiferm = 0;
        for (const auto& p : particles) {
            if (p.get_statistics() == ParticleStatistics::fermion)
                ++n_ferm;
            else if (p.get_statistics() == ParticleStatistics::antifermion)
                ++n_antiferm;
        }
        if (n_ferm != n_antiferm) {
            _WARNING_R("Number fermions != number antifermions: ", n_ferm, " != ", n_antiferm);
            std::exit(1);
        }
    }

    {
        std::set<ParticleFlavor> all_flavours;
        for (const auto& p : particles) {
            if (p.get_statistics() == ParticleStatistics::fermion or p.get_statistics() == ParticleStatistics::antifermion) {
                if (all_flavours.count(p.get_flavor()) > 0) {
                    _WARNING_R("Identical fermion flavors are not supported!");
                    std::exit(1);
                }
                all_flavours.insert(p.get_flavor());
            }
        }
    }
}

unsigned PartialAmplitudeInput::get_number_of_gluons() const {
    unsigned n = 0;
    for (const auto& p : particles) {
        if (p.get_flavor() == ParticleFlavor::gluon) ++n;
    }
    return n;
}

unsigned PartialAmplitudeInput::get_number_of_photons() const {
    unsigned n = 0;
    for (const auto& p : particles) {
      if (p.get_flavor() == ParticleFlavor::photon) ++n;
    }
    return n;
}


unsigned PartialAmplitudeInput::get_number_of_quarks() const {
  return count_quarks(particles);
}

unsigned PartialAmplitudeInput::get_number_of_light_quarks() const {
    unsigned n = 0;
    for (const auto& p : particles) {
        if (in_container<ParticleFlavor::q>(p.get_flavor()) and p.is_massless()) ++n;
    }
    return n;
}

unsigned PartialAmplitudeInput::get_number_of_heavy_quarks() const {
    unsigned n = 0;
    for (const auto& p : particles) {
        if (in_container<ParticleFlavor::q>(p.get_flavor()) and !p.is_massless()) ++n;
    }
    return n;
}

unsigned PartialAmplitudeInput::get_number_of_leptons() const {
    unsigned n(0);
    for(auto& p: particles) {
        if (in_container<ParticleFlavor::l>(p.get_flavor())) ++n;
    }
    return n;
}

unsigned PartialAmplitudeInput::get_number_of_antileptons() const {
    unsigned n(0);
    for(auto& p: particles) {
        if (in_container<ParticleFlavor::lb>(p.get_flavor())) ++n;
    }
    return n;
}

PartialAmplitudeInput PartialAmplitudeInput::get_copy_with_nf_powers_set_to(unsigned set_nf) const {
    PartialAmplitudeInput pa{*this};
    pa.cs.Nf_powers=set_nf;
    return pa;
}

PartialAmplitudeInput PartialAmplitudeInput::get_copy_only_coloured() const {
    std::vector<Particle> new_particles;
    for(auto& p : particles) if(p.is_colored()) new_particles.push_back(p);

    PartialAmplitudeInput new_input = *this;
    new_input.particles = new_particles;

    return new_input;
}

unsigned PartialAmplitudeInput::count_3_point_vertices(unsigned loop_order) const { return static_cast<unsigned>(particles.size()) + 2 * (loop_order - 1); }

std::vector<size_t> PartialAmplitudeInput::get_momentum_indices() const {
    std::vector<size_t> result;
    for(const auto& p : particles){
        result.push_back(p.mom_index());
    }
    return result;
}

std::string ColourStructureInput::get_id_string() const {
    std::string result;

    if (Nc_order != 0) result += "Nc:" + std::to_string(Nc_order);
    if (Nf_powers != 0) result += "Nf:" + std::to_string(Nf_powers);
    if (Nh_powers != 0) result += "Nh:" + std::to_string(Nh_powers);
    if (adjoint_quarks) result += "A";

    return result;
}

std::string PartialAmplitudeInput::get_id_string() const {
    std::string result;

    result += "(";


    for (auto& p : particles) {
        if (p.get_flavor() == ParticleFlavor::gluon)
            result += "g";
	else if (p.get_flavor() == ParticleFlavor::photon)
            result += "y";
        else {
            result += wise_enum::to_string(p.get_flavor());
        }
        result += std::string("[") + std::string(wise_enum::to_string(p.external_state().get_helicity())) + "]";
        result += ",";
    }
    result.pop_back();
    result += ")";

    std::string csstring = cs.get_id_string();

    if(!csstring.empty()){
        result += "_";
        result += cs.get_id_string();
    }

    return result;
}


unsigned count_quarks(const std::vector<Particle>& particles){
  unsigned n = 0;
  for (const auto& p : particles) {
    if (in_container<ParticleFlavor::q>(p.get_flavor())) ++n;
  }
  return n;
}

std::string InternalParticlesInput::get_id_string() const {
    std::string result;

    result += "(";


    for (auto& p : particles) {
        if (p.get_flavor() == ParticleFlavor::gluon)
            result += "g";
	else if (p.get_flavor() == ParticleFlavor::photon)
            result += "y";
        else {
            result += wise_enum::to_string(p.get_flavor());
        }
        result += ",";
    }
    result.pop_back();
    result += ")";

    return result;
}

  unsigned InternalParticlesInput::get_number_of_gluons() const {
    unsigned n = 0;
    for (const auto& p : particles) {
        if (p.get_flavor() == ParticleFlavor::gluon) ++n;
    }
    return n;
}

unsigned InternalParticlesInput::get_number_of_quarks() const {
  return count_quarks(particles);
}

  InternalParticlesInput::InternalParticlesInput(MathList all) {

    if(all.head != "InternalParticlesInput"){
        throw std::invalid_argument("Expected InternalParticlesInput, got "+all.head);
    }

    for (auto it : MathList(all[0], "Particles")) {
      particles.push_back(Particle(MathList(it, "Particle")));
    }

    if(all.size() >= 3){
        _WARNING_RED("Did not expect more than 2 elements in ",all); 
        std::exit(1);
    }

}

std::ostream& operator<< (std::ostream& s , const PartialAmplitudeInput& in){
    s << "PartialAmplitudeInput[" << in.get_particles() << ", (nf,nh) = (" << in.get_nf_powers() << "," << in.get_nh_powers() << ")]";
    return s;
}

namespace detail {

namespace {
bool is_quark(ParticleFlavor p) { return in_container<ParticleFlavor::q>(p); }
bool is_anti_quark(ParticleFlavor p) { return in_container<ParticleFlavor::qb>(p); }
bool is_quark(const Particle& p) { return is_quark(p.get_flavor()); }
bool is_anti_quark(const Particle& p) { return is_anti_quark(p.get_flavor()); }
bool is_gluon(const Particle& p) { return p.get_flavor() == ParticleFlavor::gluon; }
bool is_photon(const Particle& p) { return p.get_flavor() == ParticleFlavor::photon; }
bool is_matching_quark_pair(const Particle& p1, const Particle& p2) {
    if (((is_quark(p1) and is_anti_quark(p2)) or (is_anti_quark(p1) and is_quark(p2))) and p1.get_flavor() == p2.get_anti_flavor())
        return true;
    else
        return false;
}
} // namespace

void set_1L_couplings(const PartialAmplitudeInput& input, const std::vector<std::vector<Particle>>& entryExternal, std::vector<couplings_t>& cps_parent, couplings_t& cps_tree) {
    if(!input.get_number_of_photons()) {
	// TODO: smarter coupling guessing
	std::string coupling_name;
        if(entryExternal.front().front().get_flavor() == ParticleFlavor::G)
            coupling_name = "K";
        else
            coupling_name = "gs";
	cps_tree = { {{coupling_name, input.count_3_point_vertices()}} };
	std::vector<couplings_t> entry_cps_parent;
	for (const auto& v : entryExternal) { entry_cps_parent.push_back({{coupling_name, v.size()}}); }
	cps_parent = { entry_cps_parent };
    } else {
	size_t n_colored=input.count_3_point_vertices()-input.get_number_of_photons();
	if(n_colored) cps_tree["gs"]=n_colored;
	size_t n_photons=input.get_number_of_photons();
	if(n_photons) cps_tree["gw"]=n_photons;
	std::vector<couplings_t> entry_cps_parent;
	for (const auto& v : entryExternal) {
	    n_photons=0;
	    for(const auto& it: v) if(it.get_flavor() == ParticleFlavor::photon) ++n_photons;
	    n_colored=v.size()-n_photons;
	    entry_cps_parent.push_back(couplings_t());
	    if(n_colored)entry_cps_parent.back()["gs"]=n_colored;
	    if(n_photons)entry_cps_parent.back()["gw"]=n_photons;
	}
	cps_parent = { entry_cps_parent };
    }
}

size_t locate_flavor(const PartialAmplitudeInput& input, const ParticleFlavor& v) {
    for(size_t ii = 0; ii < input.get_particles().size(); ii++) {
        if(v == input.get_particles()[ii].get_flavor())
            return ii;
    }
    std::cerr<<"ERROR: could not located particle flavor "<<v<<" in "<<input<<std::endl;
    std::exit(1);
}

std::pair<bool, size_t> locate_flavor_antiflavor(const PartialAmplitudeInput& input, const ParticleFlavor& v) {
    for(size_t ii = 0; ii < input.get_particles().size(); ii++) {
        if(v == input.get_particles()[ii].get_flavor())
            return {true, ii};
    }
    auto va = anti_flavor(v);
    for(size_t ii = 0; ii < input.get_particles().size(); ii++) {
        if(va == input.get_particles()[ii].get_flavor())
            return {false, ii};
    }
    std::cerr<<"ERROR: could not located particle flavor/antiflavor "<<v<<" in "<<input<<std::endl;
    std::exit(1);
}

// we just accept a e-veb pair surrounded by u-db (or conjugate) and return location of the first of the four particles
std::pair<bool, size_t> check_lepton_antilepton_pair_color_trapped(const PartialAmplitudeInput& input) {
    auto etest = locate_flavor_antiflavor(input, ParticleFlavor::e);
    size_t e(etest.second), veb, u, db;
    if(etest.first) {
        veb = locate_flavor(input, ParticleFlavor::veb);
        u = locate_flavor(input, ParticleFlavor::u);
        db = locate_flavor(input, ParticleFlavor::db);
    }
    else {
        veb = locate_flavor(input, ParticleFlavor::ve);
        u = locate_flavor(input, ParticleFlavor::ub);
        db = locate_flavor(input, ParticleFlavor::d);
    }
    size_t first(u), last(db);
    if(db < first) { first = db; last = u; }

    const size_t N = input.get_multiplicity();
    // avoid break up at the end of particle list
    if((last + 1)%N == veb || (last + 1)%N == e) { std::swap(first, last); }

    // check adjacency
    if((e + 1)%N != veb && (veb + 1)%N != e)
        return {false, 0};
    if((u + 3)%N != db && (db + 3)%N != u)
        return {false, 0};
    if((first + 1)%N != e && (first + 1)%N != veb)
        return {false, 0};
    if((e + 1)%N != last && (veb + 1)%N != last)
        return {false, 0};
    return {true ,first};
}

void construct_ordered_1L_parent_diagram_leptons(const PartialAmplitudeInput& input, std::vector<std::vector<Particle>>& external_out,
                                               std::vector<Particle>& internal_out, std::vector<couplings_t>& loopcoups, couplings_t& treecoups) {
    assert(input.get_number_of_leptons() > 0);
    assert(input.get_number_of_leptons() == input.get_number_of_antileptons());
    assert(input.get_number_of_photons() == 0);
    assert(input.get_nf_powers() <= 1);

    if (input.get_number_of_leptons() > 1) {
        std::cerr << "ERROR: only 1-lepton processes implemented: " << input << std::endl;
        std::exit(1);
    }
    auto check = check_lepton_antilepton_pair_color_trapped(input);
    if (!check.first) {
        std::cerr << "ERROR: Only {e,nub} around {u,db} implemented (or conjugate)! " << input << std::endl;
        std::exit(1);
    }
    size_t first = check.second;

    const size_t N = input.get_multiplicity();

    treecoups["gw"] = 2;
    treecoups["gs"] = N - 4;

    auto& particles = input.get_particles();
    if (input.get_nf_powers() == 1) {
        static const Particle qnfL("qL", ParticleType(ParticleStatistics::fermion, ParticleFlavor::q_nf1, ParticleMass{}, true), SingleState("default"));
        // group udbeveb system
        std::vector<Particle> group;
        for (size_t ii = 0; ii < 4; ii++) group.push_back(particles[(first + ii) % N]);
        external_out.push_back(group);
        internal_out.push_back(qnfL);
        loopcoups.push_back(couplings_t());
        loopcoups.back()["gw"] = 2;
        loopcoups.back()["gs"] = 2;

        // now we continue through rest of particles
        for (size_t ii = 4; ii < N; ii++) {
            const auto& p = particles.at((first + ii) % N);
            if (is_gluon(p) or is_photon(p)) {
                external_out.push_back({p});
                loopcoups.push_back(couplings_t());
                loopcoups.back()["gs"] = 1;
            }
            else if (is_matching_quark_pair(p, particles.at((first + ii + 1)%N))) {
                external_out.push_back({p, particles[(first + ii + 1) % N]});
                ++ii;
                loopcoups.push_back(couplings_t());
                loopcoups.back()["gs"] = 2;
            }
            else {
                _WARNING_R("Not implemented!");
                std::exit(1);
            }
            internal_out.push_back(qnfL);
        }

        return;
    }

    // Now nf_powers is 0

    static const Particle gL("gL", ParticleType(ParticleStatistics::vector, ParticleFlavor::gluon, ParticleMass{}, true), SingleState("default"));

    auto q_or_qbL = [](ParticleStatistics s, ParticleFlavor f) -> Particle {
        return Particle{std::string(wise_enum::to_string(f)) + std::string("L"), ParticleType(s, f, ParticleMass{}, true), SingleState("default")};
    };

    // treat udbeveb system
    external_out.push_back({particles[(first) % N]});
    internal_out.push_back(gL);
    loopcoups.push_back(couplings_t());
    loopcoups.back()["gs"] = 1;

    std::vector<Particle> group;
    for (size_t ii = 1; ii < 3; ii++) group.push_back(particles[(first + ii) % N]);
    external_out.push_back(group);
    internal_out.push_back(q_or_qbL(particles[(first) % N].get_type().get_statistics(), particles[(first) % N].get_type().get_flavor()));
    loopcoups.push_back(couplings_t());
    loopcoups.back()["gw"] = 2;

    external_out.push_back({particles[(first + 3) % N]});
    internal_out.push_back(q_or_qbL(particles[(first + 3) % N].get_anti_type().get_statistics(), particles[(first + 3) % N].get_anti_type().get_flavor()));
    loopcoups.push_back(couplings_t());
    loopcoups.back()["gs"] = 1;
    // done with udbeveb system

    for (size_t ii = 4; ii < N; ii++) { 
        auto& p = particles[(first + ii) % N];
        external_out.push_back({p}); 
        loopcoups.push_back(couplings_t());
        loopcoups.back()["gs"] = 1;
    }

    for (size_t ii = 4; ii < N; ii++) {
        const auto& p = particles.at((first + ii) % N);
        if (is_gluon(p)) { internal_out.push_back(gL); }
        else if (is_quark(p) or is_anti_quark(p)) {
            auto& previous = particles.at((N + first + ii - 1) % N);
            if (is_matching_quark_pair(p, previous)) {
                ParticleType ap = p.get_anti_type();
                internal_out.push_back(q_or_qbL(ap.get_statistics(), ap.get_flavor()));
            }
            else
                internal_out.push_back(gL);
        }
        else {
            _WARNING_R("Not implemented!");
            std::exit(1);
        }
    }
}

void construct_ordered_1L_parent_diagram(const PartialAmplitudeInput& input, std::vector<std::vector<Particle>>& external_out,
                                               std::vector<Particle>& internal_out, std::vector<couplings_t>& loopcoups, couplings_t& treecoups) {
    assert(external_out.empty());
    assert(internal_out.empty());
    assert(input.get_nf_powers() <= 1);
    assert(input.get_nh_powers() <= 1);

    if(input.get_number_of_leptons() > 0) {
        if(input.get_number_of_photons() > 0) {
            std::cerr << "ERROR: received partial amplitude with leptons and photons (not implemented!): " << input << std::endl;
            std::exit(1);
        }
        construct_ordered_1L_parent_diagram_leptons(input, external_out, internal_out, loopcoups, treecoups);
        DEBUG(
            std::cout<<"Parent diagram information for: "<< input <<std::endl;
            _PRINT(external_out);
            _PRINT(internal_out);
            _PRINT(loopcoups);
            _PRINT(treecoups);
        );
        return;
    }

    const size_t N = input.get_multiplicity();

    
    // to handle gravity computations in 1-loop calculations
    {
        bool all_gravitons = true;
        for(auto& p : input.get_particles()){
            if (p.get_type() != ParticleType("G")) {
                all_gravitons = false;
                break;
            }
        }
        if(all_gravitons){
            static const Particle GL("GL", ParticleType("G"), SingleState("default"));
            for (auto& p : input.get_particles()) { external_out.push_back({p}); }
            internal_out = std::vector<Particle>{N, GL};
            set_1L_couplings(input, external_out, loopcoups, treecoups);
            return;
        }
    }

    static const Particle gL("gL", ParticleType(ParticleStatistics::vector, ParticleFlavor::gluon, ParticleMass{}, true), SingleState("default"));

    // closed light fermion loops
    if (input.get_nf_powers() == 1) {
        static const Particle qnfL("qL", ParticleType(ParticleStatistics::fermion, ParticleFlavor::q_nf1, ParticleMass{}, true), SingleState("default"));

        if (N == (input.get_number_of_gluons()+input.get_number_of_photons())) {
            for (auto& p : input.get_particles()) { external_out.push_back({p}); }
            internal_out = std::vector<Particle>{N, qnfL};
            set_1L_couplings(input, external_out, loopcoups, treecoups);
            return;
        }

        auto particles = input.get_particles();

        while (!(is_gluon(particles.at(0)) or is_photon(particles.at(0)) or is_matching_quark_pair(particles.at(0), particles.at(1)))) {
            std::rotate(particles.rbegin(), particles.rbegin() + 1, particles.rend());
        }

        // now all quark pairs will be ordered
        for (size_t i = 0; i < N; ++i) {
            const auto& p = particles.at(i);
            if (is_gluon(p) or is_photon(p)) { external_out.push_back({p}); }
            else if (is_matching_quark_pair(p, particles.at(i + 1))) {
                external_out.push_back({p, particles[i + 1]});
                ++i;
            }
            else {
                _WARNING_R("Not implemented!");
                std::exit(1);
            }
            internal_out.push_back(qnfL);
        }

        set_1L_couplings(input, external_out, loopcoups, treecoups);
        return;
    }

    // closed heavy fermion loops
    if (input.get_nh_powers() == 1) {
        // TODO: For now heavy loop is always a top-quark
        // FIXME: if Top-quark is purely internal, then the mass might not be initialied
        const Particle Top("t",ParticleType("t"), SingleState("default"));
        const Particle QnfL("QnfL", ParticleType(ParticleStatistics::fermion, ParticleFlavor::Q_nf1, Top.get_ParticleMass(), true), SingleState("default"));

        if (N == input.get_number_of_gluons()) {
            for (auto& p : input.get_particles()) { external_out.push_back({p}); }
            internal_out = std::vector<Particle>{N, QnfL};
            set_1L_couplings(input, external_out, loopcoups, treecoups);
            return;
        }

        auto particles = input.get_particles();

        while (!(is_gluon(particles.at(0)) or is_matching_quark_pair(particles.at(0), particles.at(1)))) {
            std::rotate(particles.rbegin(), particles.rbegin() + 1, particles.rend());
        }

        // now all quark pairs will be ordered
        for (size_t i = 0; i < N; ++i) {
            const auto& p = particles.at(i);
            if (is_gluon(p)) { external_out.push_back({p}); }
            else if (is_matching_quark_pair(p, particles.at(i + 1))) {
                external_out.push_back({p, particles[i + 1]});
                ++i;
            }
            else {
                _WARNING_R("Not implemented!");
                std::exit(1);
            }
            internal_out.push_back(QnfL);
        }

        set_1L_couplings(input, external_out, loopcoups, treecoups);
        return;
    }

    // Now nf_powers is 0
    const auto& particles = input.get_particles();

    for (auto& p : input.get_particles()) { external_out.push_back({p}); }

    auto q_or_qbL = [](ParticleStatistics s, ParticleFlavor f, ParticleMass m) -> Particle {
        return Particle{std::string(wise_enum::to_string(f)) + std::string("L"), ParticleType(s, f, m, true), SingleState("default")};
    };

    if (N == input.get_number_of_gluons()) { internal_out = std::vector<Particle>{N, gL}; }
    else if (input.get_number_of_quarks() > 0) {
        for (size_t i = 0; i < N; ++i) {
            const auto& p = particles.at(i);
            if (is_gluon(p)) { internal_out.push_back(gL); }
            else if (is_quark(p) or is_anti_quark(p)) {
	        if ((is_matching_quark_pair(p, particles.at((N + i - 1) % N)) and 2*input.get_number_of_quarks() + input.get_number_of_gluons() == N) or is_photon(particles.at((N + i - 1) % N))) {
                    ParticleType ap = p.get_anti_type();
                    internal_out.push_back(q_or_qbL(ap.get_statistics(), ap.get_flavor(), ap.get_ParticleMass()));
                }
                else
                    internal_out.push_back(gL);
            }
            else if (is_photon(p)) {
                for(size_t j = 1; j < N; ++j) {
                    const auto& pq = particles.at((N + i - j) % N);
                    if(is_quark(pq) or is_anti_quark(pq)) {
                        internal_out.push_back(q_or_qbL(pq.get_type().get_statistics(), pq.get_type().get_flavor(), pq.get_ParticleMass()));
                        break;
                    }
                }
            }
            else {
                _WARNING_R("Not implemented!");
                std::exit(1);
            }
        }
    }
    else {
        _WARNING_R("Not implemented!");
        std::exit(1);
    }

    set_1L_couplings(input, external_out, loopcoups, treecoups);
}


#ifdef USE_FINITE_FIELDS
std::vector<BigRat> read_twistor_parameters(MathList list){
    std::vector<BigRat> xs;
    for (auto it : list) {
        try {
            MathList rat(it, "Rational");
            if(rat.size() == 1){
                xs.emplace_back(std::stoi(rat[0]),1);
            }
            else if (rat.size()==2){
                xs.emplace_back(std::stoi(rat[0]),std::stoi(rat[1]));
            }
            else{
                _WARNING_R("Can't recognize a rational number from: ",it);
                std::exit(1);
            }
        }
        catch (std::invalid_argument& e) {
            std::stringstream ss;
            ss << it;
            BigRat r;
            ss >> r;
            if(ss.fail()){
                _WARNING_R("Can't recognize a rational number from: ",it ,"\n",e.what());
                std::exit(1);
            }
            xs.push_back(r);
        }
    }
    return xs;
}
#endif

} // namespace detail

ColourStructureInput::ColourStructureInput(MathList input) {
    if(input.head != "ColourStructure" && input.head != "NfPower"){
        throw std::invalid_argument("Expected ColourStructure or NfPower, got "+input.head);
    }

    if (input.head == "NfPower") {
        Nf_powers = std::stoi(input[0]);
        if (input.size() == 2) Nh_powers = std::stoi(input[1]);
        assert(Nf_powers <= 2);
        assert(Nh_powers <= 2);
        return;
    }

    if (input.head == "ColourStructure"){
        for (auto& it : input) {
            MathList list(it);
            if (list.head == "NfPowers") {
                Nf_powers = std::stoi(list[0]);
                assert(Nf_powers <= 2);
                continue;
            }
            if (list.head == "NcOrder") {
                Nc_order = std::stoi(list[0]);
                assert(Nc_order <= 1);
                continue;
            }
            if (list.head == "AdjointQuarks") {
                assert(list.size() == 0);
                adjoint_quarks = true;
                continue;
            }
            _WARNING_RED("Unrecognized ColourStructureInput!");
            std::exit(1);
        }
    }
}

CaravelSettingsInput::CaravelSettingsInput(MathList all) {

    if(all.head != "CaravelSettingsInput"){
        throw std::invalid_argument("Expected CaravelSettingsInput, got "+all.head);
    }

    for (auto it : all) {
        MathList list(it);

        if (list.head == "Permutation") {
            for (auto jt : list) perm.push_back(std::stoi(jt));
            continue;
        }
        if (list.head == "Setting") {
            assert(list.size() == 2);
            settings::use_setting(list[0] + " " + list[1]);
            continue;
        }
        if (list.head == "Ds") {
            assert(list.size() == 1);
            std::stringstream read_Ds(list[0]);
            read_Ds >> Ds;
            Ds.var_name = "ep";
            assert(read_Ds);
            continue;
        }
        if (list.head == "Model") {
            assert(list.size() == 1);
            auto try_get = wise_enum::from_string<ModelType>(list[0].c_str());
            model = try_get.value();
        }

        _WARNING_RED("Unrecognized input ", it);
        throw std::runtime_error("Unrecognized input in InputParser");
    }
}

void InputParser::check_input() const {
    for (auto&& [k, obj] : enumerate(tokens)) {
        for (size_t i = k + 1; i < tokens.size(); ++i) {
            if (obj.head == tokens[i].head) {
                _WARNING_RED("Duplicate argument with head \"", obj.head, "\"");
                throw std::runtime_error("Duplicate argument in InputParser");
            }
        }
    }
}

InputParser::InputParser(int argc, char** argv) {
    for (int i = 1; i < argc; ++i) tokens.emplace_back(std::string(argv[i]));
    check_input();
}

InputParser::InputParser(std::string filename) {
    {
        std::ifstream file(filename);

        if(!file) {
            throw std::runtime_error("InputParser could not open file "+filename);
        }

        std::string line;
        while (std::getline(file,line)) {
            if(line.empty()) continue;
            if(line.at(0) == '#') continue;
            tokens.push_back(line);
        }
    }

    if(tokens.empty()){
        _WARNING_RED("Warning: file ",filename," give to InputParser is empty!");
    }

    check_input();
}

bool InputParser::cmd_option_exists(std::string head) {
    bool r = std::find_if(tokens.begin(), tokens.end(), [&head](auto l) { return l.head == head; }) != tokens.end();
    if (r) {
        accessed_tokens.insert(head);
    }
    return r;
}

std::vector<MathList> InputParser::get_unused_tokens() const {
    std::vector<MathList> result;
    for(const auto& it : tokens){
        if (accessed_tokens.count(it.head) == 0) result.push_back(it);
    }
    return result;
}

void InputParser::report(const MathList& l) const { _MESSAGE("InputParser: ", Color::Code::FG_BLUE, l, Color::Code::FG_DEFAULT); }

std::string read_file(const std::string& filename) {
    std::ifstream f(filename);
    if (!f) throw std::runtime_error("File " + filename + " cannot be open!");
    std::stringstream b;
    b << f.rdbuf();
    return b.str();
}

} // namespace Caravel
