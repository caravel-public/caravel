#include "qd_suppl.h"
#include "Core/typedefs.h"
#include "Utilities.h"
namespace Caravel {

std::complex<Caravel::RHP> pow(std::complex<Caravel::RHP> __x,int __n)    {
	if ( __n<0 ) {return pow(CHP(1.,0.)/__x,-__n);}
	std::complex<Caravel::RHP> __y;
	if (__n % 2 ) {__y=__x;} else {__y=CHP(1.,0.);}

    while (__n >>= 1)
      {
        __x = __x * __x;
        if (__n % 2)
          __y = __y * __x;
      }

    return __y;
  }
std::complex<Caravel::RVHP> pow(std::complex<Caravel::RVHP> __x,int __n)    {
	if ( __n<0 ) {return pow(CVHP(1.,0.)/__x,-__n);}
	std::complex<Caravel::RVHP> __y;
	if (__n % 2 ) {__y=__x;} else {__y=CVHP(1.,0.);}

    while (__n >>= 1)
      {
        __x = __x * __x;
        if (__n % 2)
          __y = __y * __x;
      }

    return __y;
  }
}

// specialization for complex<qd_real>

namespace std {
#if _USE_GCC
template<> Caravel::CHP sqrt<Caravel::RHP>(const Caravel::CHP& __z)
{
  Caravel::RHP __x = __z.real();
  Caravel::RHP __y = __z.imag();

  if (__x == Caravel::RHP(0))
    {
	  Caravel::RHP __t = sqrt(abs(__y) / Caravel::RHP(2));
      return Caravel::CHP(__t, __y < Caravel::RHP(0) ? -__t : __t);
    }
  else
    {
	  Caravel::RHP __t = sqrt(Caravel::RHP(2) * (std::abs(__z) + abs(__x)));
	  Caravel::RHP __u = __t / Caravel::RHP(2);
      return __x > Caravel::RHP(0)
        ? Caravel::CHP(__u, __y / __t)
        : Caravel::CHP(abs(__y) / __t, __y < Caravel::RHP(0) ? -__u : __u);
    }
}

template<> Caravel::CVHP sqrt<Caravel::RVHP>(const Caravel::CVHP& __z)
{
  Caravel::RVHP __x = __z.real();
  Caravel::RVHP __y = __z.imag();

  if (__x == Caravel::RVHP(0.))
    {
	  Caravel::RVHP __t = sqrt(abs(__y) / Caravel::RVHP(2));
      return Caravel::CVHP(__t, __y < Caravel::RVHP(0) ? -__t : __t);
    }
  else
    {
	  Caravel::RVHP __t = sqrt(Caravel::RVHP(2) * (std::abs(__z) + abs(__x)));
	  Caravel::RVHP __u = __t / Caravel::RVHP(2);
      return __x > Caravel::RVHP(0)
        ? Caravel::CVHP(__u, __y / __t)
        : Caravel::CVHP(abs(__y) / __t, __y < Caravel::RVHP(0) ? -__u : __u);
    }
}
#endif
}
#if _USE_SUN_CC || _USE_PGCC
Caravel::CHP sqrt(const Caravel::CHP& __z)
{
  Caravel::RHP __x = __z.real();
  Caravel::RHP __y = __z.imag();

  if (__x == Caravel::RHP(0.))
    {
	  Caravel::RHP __t = sqrt(abs(__y) / Caravel::RHP(2.));
      return Caravel::CHP(__t, __y < Caravel::RHP(0.) ? -__t : __t);
    }
  else
    {
	  Caravel::RHP __t = sqrt(2 * ( std::abs(__z) + abs(__x)));
	  Caravel::RHP __u = __t / Caravel::RHP(2.);
      return __x > Caravel::RHP(0.)
        ? Caravel::CHP(__u, __y / __t)
        : Caravel::CHP(abs(__y) / __t, __y < Caravel::RHP(0.) ? -__u : __u);
    }
}

Caravel::CVHP sqrt(const Caravel::CVHP& __z)
{
  Caravel::RVHP __x = __z.real();
  Caravel::RVHP __y = __z.imag();

  if (__x == Caravel::RVHP(0.))
    {
	  Caravel::RVHP __t = sqrt(abs(__y) / Caravel::RVHP(2.));
      return Caravel::CVHP(__t, __y < Caravel::RVHP(0.) ? -__t : __t);
    }
  else
    {
	  Caravel::RVHP __t = sqrt(2 * ( std::abs(__z) + abs(__x)));
	  Caravel::RVHP __u = __t / Caravel::RVHP(2.);
      return __x > Caravel::RVHP(0.)
        ? Caravel::CVHP(__u, __y / __t)
        : Caravel::CVHP(abs(__y) / __t, __y < Caravel::RVHP(0.) ? -__u : __u);
    }
}

Caravel::RHP sqrt(const Caravel::RHP& __z);
Caravel::RVHP sqrt(const Caravel::RVHP& __z);

#endif


namespace std{
    size_t hash<Caravel::RHP>::operator() (const Caravel::RHP& y) const{
	hash<double> hasher;
	size_t result = hasher(y.x[0]);
	Caravel::hash_combine(result, y.x[1]);
	return result;
    }

    size_t hash<Caravel::RVHP>::operator() (const Caravel::RVHP& y) const{
	hash<double> hasher;
	size_t result = hasher(y.x[0]);
	Caravel::hash_combine(result, y.x[1]);
	Caravel::hash_combine(result, y.x[2]);
	Caravel::hash_combine(result, y.x[3]);
	return result;
    }
}
