/**
 * @file Particle.h
 *
 * @date 24.11.2015
 *
 * @brief Headers for Particle class and related classes
 *
*/

#ifndef PARTICLE_H_
#define PARTICLE_H_

#include <vector>
#include <string>
#include <unordered_map>
#include <complex>
#include <iostream>
#include <utility>
#include <cstdlib>
#include <cassert>

#include "wise_enum.h"
#include "Core/MathInterface.h"

namespace Caravel {

class ParticleMass;
std::ostream& operator<<(std::ostream& s,const ParticleMass& p );
bool operator==(const ParticleMass&,const ParticleMass&);



/**
 * Class to hold mass information of a given particle. Masses can be complex and internally are represented by
 * a rational number of complexified integers.
 *
 * Mass is represented as
 *
 * \f[
 *
 * m=\frac{n_r}{d_r}+i\frac{n_i}{d_i}\ ,
 *
 * \f]
 *
 * where \f$n_r\f$, \f$n_i\f$, \f$d_r\f$ and \f$d_i\f$ are all integers and \f$i\f$ is the imaginary unit.
 *
 */

struct MassStruct {
    int n_mass_real; /**< Numerator of the real part of the mass */
    int n_mass_imag; /**< Numerator of the imaginary part of the mass */
    int d_mass_real; /**< Denominator of the real part of the mass */
    int d_mass_imag; /**< Denominator of the imaginary part of the mass */
  public:
    MassStruct() : n_mass_real(0), n_mass_imag(0), d_mass_real(1), d_mass_imag(1) {}
    MassStruct(int n_r, int d_r, int n_i = 0, int d_i = 1) : n_mass_real(n_r), n_mass_imag(n_i), d_mass_real(d_r), d_mass_imag(d_i) {}

    /* Templated method to get the mass of the particle, full complex information passed */
    template <class R> std::complex<R> get_mass(const std::complex<R>& dummy) const {
        return std::complex<R>(n_mass_real) / std::complex<R>(d_mass_real) +
               std::complex<R>(0, 1) * std::complex<R>(n_mass_imag) / std::complex<R>(d_mass_imag);
    }

    /* Templated method to get the mass of the particle, only real type passed */
    template <class T> T get_mass(const T& dummy) const {
        if (n_mass_imag != 0)
            std::cout << "MassStruct::get_mass() called with real template type, but found an imaginary contribution: I*" << n_mass_imag << "/" << d_mass_imag
                      << " which has been dropped!" << std::endl;
        return T(n_mass_real) / T(d_mass_real);
    }

    bool is_massive() const { return n_mass_real != 0 || n_mass_imag != 0; }
    bool is_massless() const { return n_mass_real == 0 && d_mass_real != 0 && n_mass_imag == 0 && d_mass_imag != 0; }

    // set mass value
    void set_mass(int n_r, int d_r, int n_i = 0, int d_i = 1) {
        assert(d_r != 0 && d_i != 0);
        n_mass_real = n_r;
        d_mass_real = d_r;
        n_mass_imag = n_i;
        d_mass_imag = d_i;
    }
};

std::ostream& operator<<(std::ostream& s, const MassStruct& m);
bool operator==(const MassStruct&, const MassStruct&);

class ParticleMass {
    MassStruct mass_identity; // fixed mass value to correctly identify the mass index at all stages
    MassStruct mass_value;    // value of the mass that can be changed
    size_t mass_index;        /**< Tracks which mass *this correspond w.r.t. global particle_mass_container (static member of this class) */
  public:
    static size_t mass_index_global;                                       /**< Allows dynamic tracking of particle masses added */
    static thread_local std::vector<ParticleMass> particle_mass_container; /**< Static member to track corresponding mass */
    /**
     * Produces vector of (real) masses in the number field 'T'
     */
    template <typename T> static std::vector<T> get_vector_of_real_masses() {
        std::vector<T> r{T(0)};
        for (const auto& m : particle_mass_container) r.push_back(m.mass_value.get_mass(T(0)));
        return r;
    }
    // modifies the current value of the mass without changing its index!
    static void set_container_mass(size_t midx, int n_r, int d_r, int n_i = 0, int d_i = 1) {
        assert(d_r != 0 && d_i != 0);
        assert(midx <= mass_index_global);
        particle_mass_container[midx - 1].mass_value.set_mass(n_r, d_r, n_i, d_i);
    }
    // Reset all masses to their default values
    static void set_default_container_masses() {
        for (size_t n = 0; n < particle_mass_container.size(); n++) particle_mass_container[n].mass_value = particle_mass_container[n].mass_identity;
    }

  private:
    size_t get_corresponding_mass_index()
        const; /**< This private method will return the mass_index for *this needs (creating new entry if necessary in particle_mass_container) */
  public:
    /**
     * Easy way for printing mass info
     */
    friend std::ostream& operator<<(std::ostream& s, const ParticleMass& p);
    /**
     * This identity will be considered true when ALL integer containers match! Notice that this will make 2/1 not to be equivalent to 4/2 and similar!!
     */
    friend bool operator==(const ParticleMass&, const ParticleMass&);
    /**
     * The default constructor will define a massless particle
     */
    ParticleMass();
    /**
     * Constructor to define a particle with a given mass
     *
     * @param int defining \f$n_r\f$ (see class detailed description above)
     * @param int defining \f$d_r\f$ (see class detailed description above)
     * @param int defaulted to zero, defines \f$n_i\f$ (see class detailed description above)
     * @param int defaulted to one, defines \f$d_i\f$ (see class detailed description above)
     */
    ParticleMass(int, int, int = 0, int = 1);
    /**
     * Allows setting the mass of the particle after created
     *
     * @param int defining \f$n_r\f$ (see class detailed description above)
     * @param int defining \f$d_r\f$ (see class detailed description above)
     * @param int defaulted to zero, defines \f$n_i\f$ (see class detailed description above)
     * @param int defaulted to one, defines \f$d_i\f$ (see class detailed description above)
     */
    void set_mass(int, int, int = 0, int = 1);
    /**
     * Method to get if particle is massive
     */
    bool is_massive() const { return mass_identity.is_massive(); }
    /**
     * Method to get if particle is massless
     */
    bool is_massless() const { return mass_identity.is_massless(); }
    /**
     * Returns the corresponding mass_index of *this (if massless, returns zero)
     */
    size_t get_mass_index() const { return mass_index; }
    /**
     * Templated method to get the mass of the particle, full complex information passed
     */
    template <class R> std::complex<R> get_default_mass(const std::complex<R>& dummy) const { return mass_identity.get_mass(dummy); }
    template <class R> std::complex<R> get_mass(const std::complex<R>& dummy) const { return mass_value.get_mass(dummy); }

    /**
     * Templated method to get the mass of the particle, only real information passed
     */
    template <class T> T get_default_mass(const T& dummy) const { return mass_identity.get_mass(dummy); }
    template <class T> T get_mass(const T& dummy) const { return mass_value.get_mass(dummy); }

    /**
     * This static method gives an overview of the masses included so far at run time
     */
    static void show_all_masses();
};

class Particle;
bool operator==(const Particle&,const Particle&);

/**
 * \enum ParticleStatistics
 *
 * \brief An enumeration of the particle statistics considered in Caravel
 */
WISE_ENUM_CLASS(ParticleStatistics,
	(scalar , 0),	/**< Scalar particle */
	(fermion , 1),	/**< Fermion particle */
	(antifermion , -1),	/**< Fermion anti particle */
	(vector , 2),	/**< Vector bosons (in principle massless...) */
	(tensor , 4),	/**< Tensor bosons (representing a graviton for example) */
	(cubictensor , 6),	/**< Cubic tensor bosons (representing the auxiliary field of CubicGravity for example) */
	(undefined , 999)	/**< Just to identify cases without defined statistics */
)

/**
 * \enum ParticleFlavor
 *
 * \brief An enumeration of the particle flavors following the PDG IDs for partons together with internal containers for quarks, leptons, etc
 */
WISE_ENUM_CLASS(ParticleFlavor,
	// first family quarks
	(d , 1),	/**< down quark */
	(u , 2),	/**< up quark */
	(db , -1),	/**< down antiquark */
	(ub , -2),	/**< up antiquark */

	// second family quarks
	(s , 3),	/**< strange quark */
	(c , 4),	/**< charm quark */
	(sb , -3),	/**< strange antiquark */
	(cb , -4),	/**< charm antiquark */

	// third family quarks
	(b , 5),	/**< bottom quark */
	(t , 6),	/**< top quark */
	(bb , -5),	/**< bottom antiquark */
	(tb , -6),	/**< top antiquark */


	// first family leptons
	(e , 11),	/**< electron */
	(ve , 12),	/**< electron neutrino */
	(eb , -11),	/**< positron */
	(veb , -12),	/**< electron antineutrino */

	// second family leptons
	(mu , 13),	/**< muon */
	(vmu , 14),	/**< muon neutrino */
	(mub , -13),	/**< antimuon */
	(vmub , -14),	/**< muon antineutrino */

	// third family leptons
	(tau , 15),	/**< tau */
	(vtau , 16),	/**< tau neutrino */
	(taub , -15),	/**< antitau */
	(vtaub , -16),	/**< tau antineutrino */


	// Bosons
	(gluon , 21),	/**< gluon */
	(photon , 22),	/**< photon */
	(Z , 23),		/**< Z boson */
	(Wp , 24),	/**< W+ boson */
	(Wm , -24),	/**< W- boson */
	(H , 25),		/**< Higgs boson */
	(G , 39),		/**< graviton */
        (Phi1, 40), 		/**< Scalar 1 */
        (Phi2, 41), 		/**< Scalar 2 */
        (V1, 42), 		/**< Vector 1 */
        (V2, 43), 		/**< Vector 2 */

	// containers
	(q , 1001),	/**< generic quark */
	(qb , -1001),	/**< generic antiquark */
	(l , 10001),	/**< generic lepton */
	(lb , -10001),	/**< generic antilepton */

        (q_nf1 , 2001),     /**< container for closed light quark loop contributions */
        (qb_nf1 , -2001),     /**< container for closed light quark loop contributions */
        (q_nf2 , 2002),     /**< container for closed light quark loop contributions */
        (qb_nf2 , -2002),     /**< container for closed light quark loop contributions */

        (Q_nf1 , 3001),     /**< container for closed heavy quark loop contributions */
        (Qb_nf1 , -3001),     /**< container for closed heavy quark loop contributions */
        (Q_nf2 , 3002),     /**< container for closed heavy quark loop contributions */
        (Qb_nf2 , -3002),     /**< container for closed heavy quark loop contributions */

        (q_nf1_y , 2011),     /**< container for closed quark loop contributions, no QED charge */
        (qb_nf1_y , -2011),     /**< container for closed quark loop contributions, no QED charge */
        (q_nf2_y , 2012),     /**< container for closed quark loop contributions, no QED charge */
        (qb_nf2_y , -2012),     /**< container for closed quark loop contributions, no QED charge */

        (ds_scalar , 3000), /**< scalars from decomposition by particle content */
        (renormalon , 4000), // auxiliary scalar for mass renormalisation insertions of heavy fermions

	// auxiliary particles
	(T , 100000),	/**< auxiliary tensor particle for QCD */
	(A , 100001),	/**< auxiliary particle for CubicGravity */

	// Unknown
	(undefined , 99999)	/**< undefined cases */
)

/**
 * Return a flavor of anti-particle of given flavor
 */
ParticleFlavor anti_flavor(ParticleFlavor);

/**
 * Container definitions
 */
constexpr inline bool is_dirac(ParticleFlavor f) {
    // add here fermion flavors that should be treated as Dirac spinors and not Weyl spinors
    // Massive fermions MUST be Dirac
    switch (f) {
        case ParticleFlavor::t:
        case ParticleFlavor::tb:
        case ParticleFlavor::Q_nf1:
        case ParticleFlavor::Q_nf2:
        case ParticleFlavor::Qb_nf1:
        case ParticleFlavor::Qb_nf2: return true;
        default: return false;
    }
}

template <ParticleFlavor f> constexpr inline bool in_container(ParticleFlavor){return false;}

template <> constexpr inline bool in_container<ParticleFlavor::q>(ParticleFlavor f) {
    switch (f) {
        case ParticleFlavor::q:
        case ParticleFlavor::u: case ParticleFlavor::d:
        case ParticleFlavor::s: case ParticleFlavor::c:
        case ParticleFlavor::b: case ParticleFlavor::t:
        case ParticleFlavor::q_nf1: case ParticleFlavor::q_nf2:
        case ParticleFlavor::Q_nf1: case ParticleFlavor::Q_nf2:
        case ParticleFlavor::q_nf1_y: case ParticleFlavor::q_nf2_y:
            return true;
        default: return false;
    }
}
template <> constexpr inline bool in_container<ParticleFlavor::qb>(ParticleFlavor f) {
    switch (f) {
        case ParticleFlavor::qb:
        case ParticleFlavor::ub: case ParticleFlavor::db:
        case ParticleFlavor::sb: case ParticleFlavor::cb:
        case ParticleFlavor::bb: case ParticleFlavor::tb:
        case ParticleFlavor::qb_nf1: case ParticleFlavor::qb_nf2:
        case ParticleFlavor::Qb_nf1: case ParticleFlavor::Qb_nf2:
        case ParticleFlavor::qb_nf1_y: case ParticleFlavor::qb_nf2_y:
            return true;
        default: return false;
    }
}
template <> constexpr inline bool in_container<ParticleFlavor::l>(ParticleFlavor f) {
    switch (f) {
        case ParticleFlavor::l:
        case ParticleFlavor::e: case ParticleFlavor::ve:
        case ParticleFlavor::mu: case ParticleFlavor::vmu:
        case ParticleFlavor::tau: case ParticleFlavor::vtau: return true;
        default: return false;
    }
}
template <> constexpr inline bool in_container<ParticleFlavor::lb>(ParticleFlavor f) {
    switch (f) {
        case ParticleFlavor::lb:
        case ParticleFlavor::eb: case ParticleFlavor::veb:
        case ParticleFlavor::mub: case ParticleFlavor::vmub:
        case ParticleFlavor::taub: case ParticleFlavor::vtaub: return true;
        default: return false;
    }
}

// checks if f is a light closed fermion flavor
constexpr inline bool is_nl(ParticleFlavor f) {
    switch (f) {
        case ParticleFlavor::q_nf1:
        case ParticleFlavor::q_nf2:
        case ParticleFlavor::qb_nf1:
        case ParticleFlavor::qb_nf2:
        case ParticleFlavor::q_nf1_y:
        case ParticleFlavor::q_nf2_y:
        case ParticleFlavor::qb_nf1_y:
        case ParticleFlavor::qb_nf2_y: return true;
        default: return false;
    }
}

// checks if f is a heavy closed fermion flavor
constexpr inline bool is_nh(ParticleFlavor f) {
    switch (f) {
        case ParticleFlavor::Q_nf1:
        case ParticleFlavor::Q_nf2:
        case ParticleFlavor::Qb_nf1:
        case ParticleFlavor::Qb_nf2: return true;
        default: return false;
    }
}

/**
 * Return QED charge
 */
constexpr std::pair<int,int> QED_charge(ParticleFlavor fl){
    if (in_container<ParticleFlavor::q>(fl) and fl != ParticleFlavor::q_nf1_y and fl != ParticleFlavor::q_nf2_y) return {2, 3};
    if (in_container<ParticleFlavor::qb>(fl) and fl != ParticleFlavor::qb_nf1_y and fl != ParticleFlavor::qb_nf2_y) return {-1, 3};
    switch(fl){
        case ParticleFlavor::e:
        case ParticleFlavor::mu:
        case ParticleFlavor::tau: return {-1, 1};
        case ParticleFlavor::eb:
        case ParticleFlavor::mub:
        case ParticleFlavor::taub: return {1, 1};
        default: return {0, 1};
    }
}


/**
 * \enum helicity
 *
 * \brief An enumeration of helicities for particles
 */
WISE_ENUM_CLASS(helicity,
	(p , 1),	/**< Plus (either for vectors, fermions or tensor particles) */
	(L , 0),	/**< Longitudinal (for massive vectors) */
	(m , -1),	/**< Minus (either for vectors, fermions or tensor particles) */
	(undefined , 999)	/**< Just to identify cases without defined helicity */
)

/**
 * A function to infer the helicity of a SingleState build from a string. This whole thing might be modified when the string is removed from SingleState
 */
helicity infer_helicity(std::string is);

//! which Weyl representation
WISE_ENUM_CLASS(repr,
        (La,1),
        (Lat,-1),
        (undefined,0)
)

constexpr inline repr flip(repr l){
    switch(l){
        case repr::La: return repr::Lat;
        case repr::Lat: return repr::La;
        case repr::undefined: return repr::undefined;
    }
    return repr::undefined;
}

namespace particle{
    //! Keep track of representations (at compile time!)
    template <unsigned Dtt> struct repr_partition;

    template <> struct repr_partition<1>{
        repr r1;
        constexpr repr_partition(repr in): r1(in){}
        constexpr repr entry(unsigned i) const {
            if (i==1) return r1;
            else throw std::logic_error("This should never be reached.");
        }
    };

    template <> struct repr_partition<2>{
        repr r1;
        repr r2;

        constexpr repr_partition(repr in): r1(in), r2(flip(in)) {}
        constexpr repr entry(unsigned i) const {return i==2? r2 : r1;}
    };

    template <unsigned Dtt> struct repr_partition{
        static_assert(Dtt%2==0,"Representation error");
        repr_partition<Dtt/2> r1;
        repr_partition<Dtt/2> r2;

        constexpr repr_partition(repr first): r1(first),r2(flip(first)) {};
        constexpr repr entry(unsigned i) const{
            if(i>=1 && i<=Dtt/2) return r1.entry(i);
            else if (i>Dtt/2 && i<=Dtt) return r2.entry(i-Dtt/2);
            else throw std::logic_error("Out of bounds");
        }
    };

    template <unsigned Dtt> std::ostream& operator<<(std::ostream& s, const repr_partition<Dtt>& p){
        s<<"[";
        s<<p.r1;
        s<<",";
        s<<p.r2;
        s<<"]";

        return s;
    }

    //! Returns a representation of the first Weyl spinor in a "multiplet" from the given representation of i-th spinor
    template <unsigned Dtt> constexpr repr repr_of_first_spinor(repr r, unsigned ind) {
        if (ind == 0) return r;
        if (ind > Dtt) throw std::runtime_error("Dtt index is out of range!");
        constexpr repr_partition<Dtt> part_la(repr::La);
        constexpr repr_partition<Dtt> part_lat(repr::Lat);

        if (part_la.entry(ind) == r)
            return part_la.entry(1);
        else
            return part_lat.entry(1);
    }
}


/**
 * This class will allow to define a single state in which a external or loop particle can be in. For example:
 *
 * gluon: m, p, s, etc
 * q: qD1, qD2, etc
 */
class SingleState {
  public:
    static constexpr int rLa = 100; /**< The value of index which means La for internal states */
    static constexpr int rLat = -100; /**< The value of index which means Lat for internal states */

  public:
    SingleState() = default;
    SingleState(const std::string& is) : state(is), hel(infer_helicity(is)) {};
    SingleState(const std::string& is, int s) : state(is), dtt_index(s), hel(infer_helicity(is)) {};

    std::string get_name() const { return state; };
    int get_dtt_index() const { return dtt_index; };
    void set_dtt_index(int s) { dtt_index = s; };
    void set_dtt_index(repr); /**< Set dtt_index to the value which corresponds to the given representation */
    bool is_external() const; /**< True if the name of the state does NOT correspond to one of the known internal state names */
    repr get_representation() const; /**< Determine the representatino for Weyl spinors */
    helicity get_helicity() const { return hel; };

  private:
    std::string state; /**< A string to name the state */
    int dtt_index{0}; /**< An index which determines an embedding of fermion states. For internal states it tracks the representaion if applicable. */
    helicity hel{helicity::undefined};
};

bool operator==(const SingleState& a, const SingleState& b);
bool operator!=(const SingleState& a, const SingleState& b);
//! Defines a strict ordering
bool operator<(const SingleState& a, const SingleState& b);

std::ostream& operator<<(std::ostream& s,const SingleState& a);

/**
 * This class will track all information of particle type including its ParticleStatistic, ParticleFlavor, ParticleMass and whether or not it carries color
 */
class ParticleType {
		ParticleStatistics spin;	/**< Defines the statistics of the particle */
		ParticleFlavor flavor;	/**< Defines the flavor of the particle (eg. up quark, muon antineutrino, etc */
		ParticleMass mass;	/**< A container to hold mass information */
		bool colored;	/**< Tracks if the particle is colored or not */
	public:
		friend std::ostream& operator<<(std::ostream&,const ParticleType&);
		friend bool operator==(const ParticleType&,const ParticleType&);
		friend bool almost_equal(const ParticleType&,const ParticleType&);
		friend bool operator!=(const ParticleType&,const ParticleType&);
		/**
		 * The default constructor will create a particle type corresponding to unknown spin and flavor, massless and colorless
		 */
		ParticleType(): spin(ParticleStatistics::undefined), flavor(ParticleFlavor::undefined), mass(ParticleMass()), colored(false) {};
		/**
		 * Constructor to facilitate construction from a string literal: it is depending on a map defined in ParticleType.cpp
		 */
		explicit ParticleType(const std::string&);
		/**
		 * Full control for construction, where all members are passed explicitly
		 */
		ParticleType(ParticleStatistics s,ParticleFlavor f,const ParticleMass& m,bool c): spin(s), flavor(f), mass(m), colored(c) {};
		/**
		 * This method delivers a type of the antiparticle of *this
		 */
		ParticleType get_anti_type() const;
		/**
		 * Method that returns particle flavor
		 */
		ParticleFlavor get_flavor() const;
		/**
		 * Returns the flavor associated with the antiparticle of *this
		 */
		ParticleFlavor get_anti_flavor() const;
		/**
		 * Allows to access information on spin-statistics
		 */
		ParticleStatistics get_statistics() const;
		/**
		 * Returns the spin-statistics associated with its anti particle
		 */
		ParticleStatistics get_flipped_statistics() const;
		/**
		 * Method that returns a bool, true if colored, false otherwise
		 */
		bool is_colored() const;
		/**
		 * Method that returns the ParticleMass assigned to the particle
		 */
		ParticleMass get_ParticleMass() const;
		/**
		 * Allows setting the mass of the particle after created
		 *
		 * @param int defining \f$n_r\f$ (see ParticleMass detailed description)
		 * @param int defining \f$d_r\f$ (see ParticleMass detailed description)
		 * @param int defaulted to zero, defines \f$n_i\f$ (see ParticleMass detailed description)
		 * @param int defaulted to one, defines \f$d_i\f$ (see ParticleMass detailed description)
		 */
		void set_mass(int,int,int = 0,int = 1);
		/**
		 * Method to get if particle is massive
		 */
		bool is_massive() const;

                void set_flavor(ParticleFlavor set){flavor=set;}
		/**
		 * Method to get if particle is massless
		 */
		bool is_massless() const;
		/**
		 * Returns the corresponding mass_index of *this (if massless, returns zero)
		 */
		size_t get_mass_index() const;
		/**
		 * Returns a string associated with *this (for now, only flavor related)
		 */
		std::string to_string() const;
		/**
		 * Templated method to get the mass of the particle, full complex information passed
		 */
		template <class R> std::complex<R> get_mass(const std::complex<R>& dummy) const { return mass.get_mass(dummy); }
		template <class R> std::complex<R> get_default_mass(const std::complex<R>& dummy) const { return mass.get_default_mass(dummy); }
		/**
		 * Templated method to get the mass of the particle, only real type passed
		 */
		template <class T> T get_mass(const T& dummy) const {  return mass.get_mass(dummy); }
		template <class T> T get_default_mass(const T& dummy) const {  return mass.get_default_mass(dummy); }
};



/**
 * Compare flavors taking into an account containers, e.g.
 * almost_equal(q,u) returns true
 */
bool almost_equal(ParticleFlavor,ParticleFlavor);
bool almost_equal(const ParticleType&, const ParticleType&);


/**
 * The Particle class is a container the allows to define new particles to be handled
 * by the Forest engine through the corresponding Model's contained in the library.
 * So far it keeps the following information:
 *
 * 1) A string to give a name to the particle (e.g. "1m")
 *
 * 2) A ParticleType which contains all information related to spin-statistics, flavor and mass
 *
 * 3) A SingleState defining the particle state (e.g. "m" for a gluon). The value "default" is special for internal loop particles
 *
 * 4) An unsigned integer to define the momenta assigned to the particle
 *
 * 5) An unsigned integer to define a reference momenta for the particle (defaulted to 0)
 *
 * We might add in the future more information for kinematical handling
 *
 */
class Particle {
		std::string name;	/**< String naming the particle */
		ParticleType type;	/**< Information about the type of particle (spin, flavor, mass and color info) */
		SingleState state;	/**< Physical state associated to the particle. The "default" state has special meaning, used for internal loop particles */
		size_t momindex;	/**< Unsigned integer to define the momenta of a particle (1 based) */
		size_t refindex;	/**< Unsigned integer to define a reference momenta for the particle (0 is default value) */
                int internal_index{0};     /**< An index used for internal purposes (e.g. labeling quark lines). IGNORED BY operator== */
	public:
		friend bool operator==(const Particle&,const Particle&);
		/**
		 * Provides default constructor
		 */
		Particle() = default;

   /**
    * Constructs a particle from a math list.
    * Example string for gluon 1 with minus helicity: Particle[gluon,1,m]
    */
    Particle(const MathList& list);

		/**
		 * Particle constructor passing explicitly all members
		 *
		 * @param std::string to name the particle
		 * @param ParticleType to define the particle type
		 * @param SingleState defining the particle state (defaulted to "NOSTATE")
		 * @param size_t for particle momenta, 1 based (as in momD_conf (defaulted to 0)
		 * @param size_t for reference momentum value (defaulted to 0)
		 */
		Particle(std::string,const ParticleType&,const SingleState& = SingleState("NOSTATE"),size_t = 0,size_t = 0);
                /**
                 * Particle constructor from a string state for backwards compatibility.
                 * Simply constructs SingleState from given string.
                 */
                [[deprecated("Consider providing particle type and state by using ParticleType and SingleState classes instead of strings.")]]
                Particle(std::string n,std::string tp,const std::string& state,size_t index=0,size_t rindex=0): Particle(n,ParticleType(tp),SingleState(state),index,rindex) {}
		/**
		 * Method that returns particle name
		 */
		std::string get_name() const;
		/**
		 * Method that returns particle a long description of the particle (equivalent to the output of operator<<)
		 */
		std::string get_long_name() const;
		/**
		 * Method that returns particle type
		 */
		ParticleType get_type() const;
		/**
		 * Method that returns antiparticle type
		 */
		ParticleType get_anti_type() const;
		/**
		 * Method that returns particle flavor
		 */
		ParticleFlavor get_flavor() const;
		/**
		 * Method that returns anti-particle flavor
		 */
		ParticleFlavor get_anti_flavor() const;
		/**
		 * Allows to access information on spin-statistics
		 */
		ParticleStatistics get_statistics() const;
		/**
		 * Given a particle it returns the spin-statistics associated with its anti particle
		 */
		ParticleStatistics get_flipped_statistics() const;
		/**
		 * Method that returns a bool, true if colored, false otherwise
		 */
		bool is_colored() const;
		/**
		 * Method that returns the state of the particle
		 */
		const SingleState& external_state() const;
		/**
		 * Method that returns the momentum index assigned to the particle
		 */
		size_t mom_index() const;
		/**
		 * Method that returns the reference momentum assigned to the particle
		 */
		size_t ref_index() const;
		/**
		 * Method that returns the ParticleMass assigned to the particle
		 */
		ParticleMass get_ParticleMass() const;
		/**
		 * Allows setting the mass of the particle after created
		 *
		 * @param int defining \f$n_r\f$ (see ParticleMass detailed description)
		 * @param int defining \f$d_r\f$ (see ParticleMass detailed description)
		 * @param int defaulted to zero, defines \f$n_i\f$ (see ParticleMass detailed description)
		 * @param int defaulted to one, defines \f$d_i\f$ (see ParticleMass detailed description)
		 */
		void set_mass(int,int,int = 0,int = 1);
		/**
		 * Sets the momentum index
		 *
		 * @param size_t particle momentum (1 based as is in momD_conf)
		 */
		void set_momentum_index(size_t m) {momindex=m;};

                void set_flavor(ParticleFlavor set){type.set_flavor(set);}

		/**
		 * Sets 'internal_index', an index used for colored particles in order to control color structures of trees to which they contribute
		 */
                void set_internal_index(int set){internal_index = set;}
		/**
		 * Gets the 'internal_index' of *this, an index used for colored particles in order to control color structures of trees to which they contribute
		 */
                int get_internal_index() const {return internal_index;}

                template <typename T> void set_dtt_index(T set){ state.set_dtt_index(set);}
		/**
		 * Method to get if particle is massive
		 */
		bool is_massive() const;
		/**
		 * Method to get if particle is massless
		 */
		bool is_massless() const;
		/**
		 * Returns the corresponding mass_index of *this (if massless, returns zero)
		 */
		size_t get_mass_index() const;
		/**
		 * Templated method to get the mass of the particle. Call forwarded to ParticleType member
		 */
		template <class T> T get_mass(const T& dummy) const { return type.get_mass(dummy); }
		template <class T> T get_default_mass(const T& dummy) const { return type.get_default_mass(dummy); }
		/**
		 * A method to print all information about *this into the standard output
		 */
		void show() const;
};

std::ostream& operator<<(std::ostream& s, Particle p );


/**
 * Process is a class to contain a vector of Particle pointers, not owned by the instance.
 * The colored particles pointed are assumed to come color ordered, at least in what respects
 * to Forest
 */
class Process {
		std::vector<Particle *> allp;	/**< Container of Particle pointers, not owned by *this */
		std::string procstring;		/**< String to identify *this */
	public:
		/**
		 * Provides default constructor
		 */
                Process() = default;
		/**
		 * Constructor, vector will be copied
		 */
		explicit Process(const std::vector<Particle*>& particles);


  /**
   * Builds from a MathList. Example input string is:
   * Particles[Particle[gluon,1,m],Particle[gluon,2,p],Particle[gluon,3,p],Particle[gluon,4,p]]
   * NOTE: This leaks memory due to the design of process. There's no sane way to write this, but the memory leak is tiny.
   */
  Process(const MathList& list);

		/**
		 * Method to get access to vector of Particle pointers
		 */
		const std::vector<Particle *>& get_process() const;
		/**
		 * Returns string representing the process
		 */
		std::string show_process() const;
};

std::ostream& operator<<(std::ostream& s, Process p );

class ColorHandledProcess;
bool operator==(const ColorHandledProcess&,const ColorHandledProcess&);

/**
 * This class helps organizing all information of index routing in a ColorHandledProcess
 */
class InternalIndexHandler {
		bool handling_needed;	/**< Determines if *this needs special handling of the internal_index'es */
		int internal_index_left;	/**< Keeps index associated to pleft */
		int internal_index_right;	/**< Keeps index associated to pright */
		std::vector<int> internal_index_colored;	/**< Container of indices of colored particles */
		std::vector<int> internal_index_colorless;	/**< Container of indices of colorless particles */
	public:
		InternalIndexHandler();
		/**
		 * ColorHandledProcess passes pleft, pright, and colored and colorless vector for arranging information
		 */
		InternalIndexHandler(const Particle&,const Particle&,const std::vector<Particle*>&,const std::vector<Particle*>&);
		/**
		 * Get internal_index of the left particle
		 */
		int get_internal_index_left() const;
		/**
		 * Get internal_index of the right particle
		 */
		int get_internal_index_right() const;
		/**
		 * Get internal_index of the ith colored particle in the vertex
		 */
		int get_colored_internal_index_in_vertex(size_t) const;
		/**
		 * Get internal_index of the ith colorless particle in the vertex
		 */
		int get_colorless_internal_index_in_vertex(size_t) const;
		/**
		 * Get internal_index for all particles in the vertex, as a pair for <colored,colorless>
		 */
		std::pair<std::vector<int>,std::vector<int>> get_all_internal_index() const;
		/**
		 * Tells if special color handling is required (when left and right particles have non-vanishing indices)
		 */
		bool needs_internal_index_handling() const;
		/**
		 * Explicit show of internal_index stored
		 */
		std::string show_internal_indices() const;
};

/**
 * Class for splitted storage of processes into colorless and colored particle containers
 */
class ColorHandledProcess {
		Particle pleft;	/**< In case this process represents an internal vertex of a cutTopology, this represents particle to the left */
		Particle pright;	/**< In case this process represents an internal vertex of a cutTopology, this represents particle to the right */
		std::vector<Particle*> colorless;	/**< Container for colorless particles, always sorted at construction time */
		std::vector<Particle*> colored;	/**< Container for colored particles, color ordering assumed implicitly from vector's content */
		InternalIndexHandler internal_indices;	/**< This container handles internal_index of particles in *this */
	public:
		/**
		 * We included a default constructor for ColorHandledProcess
		 */
		ColorHandledProcess() = default;
		/**
		 * This constructor takes directly the vectors that will be copied into colorless and colored
		 *
		 * @param vector to be copied to colorless
		 * @param vector to be copied to colored
		 *
		 */
		ColorHandledProcess(const std::vector<Particle*>&,const std::vector<Particle*>&);
		/**
		 * Constructor from a given Process
		 */
		ColorHandledProcess(Process&);
		/**
		 * Constructor from a vector of particle pointers and values for left and right (internal/loop) particles
		 */
		ColorHandledProcess(const Particle&,const std::vector<Particle*>&,const Particle&);
		/**
		 * The key operator to make ColorHandledProcess's objects useful. Checks matching betwen the (sorted) colorless and (unsorted) colored vectors
		 */
		friend bool operator==(const ColorHandledProcess&,const ColorHandledProcess&);
		/**
		 * Get access to left Particle
		 *
		 * @return Returns constant reference
		 */
		const Particle& get_left() const;
		/**
		 * Get access to right Particle
		 *
		 * @return Returns constant reference
		 */
		const Particle& get_right() const;
		/**
		 * Get access to vector of pointers to colorless particles
		 *
		 * @return Returns constant reference
		 */
		const std::vector<Particle *>& get_colorless() const;
		/**
		 * Get access to vector of pointers to colored particles
		 *
		 * @return Returns constant reference
		 */
		const std::vector<Particle *>& get_colored() const;
		/**
		 * Modifies *this by replacing a given particle pointer by a new one (NOTICE: indices in internal_indices are not redefined!)
		 *
		 * @param Particle* to be replaced
		 * @param Particle* new value
		 */
		void particle_substitute(Particle*,Particle*);
		/**
		 * Sets pleft with a dynamic cast to Particle of the argument (NOTICE: indices in internal_indices are not redefined!)
		 *
		 * @param Particle* from which we will replace pleft
		 */
		void set_left(Particle*);
		/**
		 * Sets pright with a dynamic cast to Particle of the argument (NOTICE: indices in internal_indices are not redefined!)
		 *
		 * @param Particle* from which we will replace pright
		 */
		void set_right(Particle*);
		/**
		 * Returns std::vector of Particle* corresponding to colored particles
		 *
		 * @return std::vector of colored Particle pointers
		 */
		std::vector<Particle*> get_colored();
		/**
		 * Returns std::vector of Particle* corresponding to colorless particles
		 *
		 * @return std::vector of colorless Particle pointers
		 */
		std::vector<Particle*> get_colorless();
		/**
		 * Get internal_index of the left particle
		 */
		int get_internal_index_left() const;
		/**
		 * Get internal_index of the right particle
		 */
		int get_internal_index_right() const;
		/**
		 * Get internal_index of the pointed particle in the vertex
		 */
		int get_particle_internal_index_in_vertex(Particle*) const;
		/**
		 * Get internal_index for all particles in the vertex, as a pair for <colored,colorless>
		 */
		std::pair<std::vector<int>,std::vector<int>> get_all_internal_index() const;
		/**
		 * Tells if special color handling is required (when left and right particles have non-vanishing indices)
		 */
		bool needs_internal_index_handling() const;
		/**
		 * Explicit show of internal_index stored
		 */
		std::string show_internal_indices() const;
};


/**
 * A memory managing wrapper for the Particles/Processes
 * TODO - Make the helicities fit into the type system, somehow.
 */
class ProcessFactory{
    private:
        void build_all_helicities();
	std::vector<std::string> possible_helicities;
        std::vector<Particle> base_particles;
        std::vector<std::vector<Particle>> all_helicity_particles;
        std::unordered_map<std::string, Process> helicity_processes;

        Process build_helicity_configuration(std::vector<std::string> helicities);


    public:

    // This factory is movable, not copyable. The processes would become
    // invalid on copy, and besides that copies the particles which is
    // undesirable.
    ProcessFactory(ProcessFactory const&) = delete;
    ProcessFactory(ProcessFactory &&) = default;

    ProcessFactory(std::string schematic, std::vector<std::string> possible_helicities);
    Process get_process(std::vector<std::string> helicities);
};

}

#endif // PARTICLE_H_
