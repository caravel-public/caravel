#include "settings.h"
#include "Utilities.h"
#include <optional>
#include <fstream>
#include <sstream>
#include <tuple>
#include <functional>
#include <exception>
#include <algorithm>
#include <thread>


namespace Caravel{ namespace settings{

/*********************************************************************
 * GLOBAL SETTINGS
 *
 * The (string) name can be anything in principle (without spaces). It is
 * better however if it corresponds to the variable name (including subnamespace)
 * Set reasonable default values.
 *********************************************************************/


namespace general{
    setting<cut_states> cut_propagator_numerators("general::cut_propagator_numerators",cut_states::states);
    setting<IntegralBasis> numerator_decomposition("general::numerator_decomposition",IntegralBasis::master_surface);
    setting<answer> do_n_eq_n_test("general::do_n_eq_n_test",answer::no);

    setting<int> Nf("general::Nf",1);
    setting<int> Nh("general::Nh",1);

    setting<power_counting> power_counting_of_theory("general::power_counting_of_theory",power_counting::renormalizable);

    setting<MasterBasis> master_basis("general::master_basis",MasterBasis::pure);

    setting<OneLoopMasterBasis> one_loop_master_basis("general::one_loop_master_basis",OneLoopMasterBasis::GKM);

    setting<size_t> max_moduli("general::max_moduli",35);
}

namespace integrals {
    // bY default use the integral family expressed in terms of Goncharovs,
    // which is the one used for the 4- and 5-parton papers.
    setting<family_types_1l> integral_family_1L("integrals::integral_family_1L", family_types_1l::goncharovs);
    setting<family_types_2l> integral_family_2L("integrals::integral_family_2L", family_types_2l::goncharovs);

#ifdef WITH_PENTAGONS
    setting<double> pentagon_functions_integration_precision("integrals::pentagon_functions_integration_precision", 1e-5);
    setting<size_t> pentagon_functions_max_subintervals("integrals::pentagon_functions_max_subintervals", 1e7);
#endif
}

namespace integrals{
    setting<answer> include_incomplete_integrals("integrals::include_incomplete_integrals",answer::yes);
}

namespace AmpEng{
    setting<answer> spurious_fact_check("AmpEng::spurious_fact_check",answer::yes);
}

namespace OneLoopIntegrals{
    setting<size_t> pentagon_mu2_insertion("OneLoopIntegrals::pentagon_mu2_insertion",1);
}


namespace BG{
#ifdef DS_BY_PARTICLE_CONTENT
    setting<trI> partial_trace_normalization("BG::partial_trace_normalization",trI::two);
#else
    setting<trI> partial_trace_normalization("BG::partial_trace_normalization",trI::full_Ds);
#endif
    thread_local setting<trace_scheme> partial_trace_scheme("BG::partial_trace_scheme",trace_scheme::reduced);
}

namespace IntegrandHierarchy{
    setting<answer> optimize_warmup_for_cuts("IntegrandHierarchy::optimize_warmup_for_cuts",answer::yes);
    setting<norm_type> amplitude_normalization("IntegrandHierarchy::amplitude_normalization",norm_type::automatic);
    setting<parallelize_topologies_options> parallelize_topology_fitting("IntegrandHierarchy::parallelize_topology_fitting",parallelize_topologies_options::never);
    setting<int> topology_fitting_max_nthreads("IntegrandHierarchy::topology_fitting_max_nthreads",std::thread::hardware_concurrency());
    setting<size_t> number_of_Ds_values("IntegrandHierarchy::number_of_Ds_values",3);
    setting<size_t> n_cached_D_values_warmup("IntegrandHierarchy::n_cached_D_values_warmup",30);
}


/*********************************************************************/

namespace{

template <typename T> std::enable_if_t<!wise_enum::is_wise_enum_v<T>,bool> read_answer(std::istream&, setting<T>*);
template <typename T> std::enable_if_t<wise_enum::is_wise_enum_v<T>,bool> read_answer(std::istream& in, setting<T>* a);

template<> bool read_answer(std::istream& in, setting<answer>* a) {

    using std::string;
    using std::vector;

    string the_command;

    in >> the_command;

    static const vector<string> yes_answers = {"yes", "YES", "Yes","y","Y"};
    static const vector<string> no_answers = {"no", "No", "NO","n","N"};

    auto the_answer = std::find(yes_answers.begin(), yes_answers.end(), the_command);
    if (the_answer != yes_answers.end()) {
        a->value = answer::yes;

        if (verbosity_settings.report_settings){
            _MESSAGE(a->name," set to YES");
        }
        return true;
    }

    the_answer = find(no_answers.begin(), no_answers.end(), the_command);

    if (the_answer != no_answers.end()) {
        a->value = answer::no;
        if (verbosity_settings.report_settings){
            _MESSAGE(a->name," set to NO");
        }
        return true;
    }

    _MESSAGE("Could not understand your answer: ", the_command, ". Setting ",a->name," has not been changed!");

    return false;
}

//! Read an answer for any WISE_ENUM_CLASS type
template <typename T> std::enable_if_t<wise_enum::is_wise_enum_v<T>,bool> read_answer(std::istream& in, setting<T>* a){
    std::string v;
    in >> v;

    if(in.fail()){
        _WARNING("Error during reading the setting ", a->name);
        return false;
    }

    auto res = wise_enum::from_string<T>(v.c_str());

    if(!res){
        _WARNING("Requested value \"",v,"\" is not one of the allowed values for ",a->name,":");
        for(auto it : wise_enum::range<T>){
            _WARNING("\t",it.name);
        }
        return false;
    }

    a->value = res.value();

    if (verbosity_settings.report_settings){
        _MESSAGE(a->name," set to ",wise_enum::to_string(a->value));
    }
    return true;
}



//! A generic function which basically requires only correctly implemented operator>>(std::istream&, T&)
template <typename T> std::enable_if_t<!wise_enum::is_wise_enum_v<T>,bool> read_answer(std::istream& in, setting<T>* a){
    T v;

    in >> v;

    if(in.fail()){
        _MESSAGE("Sorry, could not understand your answer. Setting ",a->name," is not changed.");
        return false;
    }

    a->value = v;

    if (verbosity_settings.report_settings){
        _MESSAGE(a->name," set to ",v);
    }
    return true;
}




template <class charT, class traits>
inline std::basic_istream<charT, traits>& ignoreLine(std::basic_istream<charT, traits>& strm) {
    // skip until end-of-line
    strm.ignore(std::numeric_limits<std::streamsize>::max(), strm.widen('\n'));

    // return stream for concatenation
    return strm;
}


class settings_map{
    std::map<std::string,std::function<bool(std::istream& in)>> all{};

    settings_map(const settings_map&)=delete;

    public:

    const auto& get() const {return all;}

    settings_map(){
	//// REGISTER SETTINGS HERE ////

        add(general::cut_propagator_numerators);
        add(general::power_counting_of_theory);
        add(general::numerator_decomposition);
        add(general::do_n_eq_n_test);
        add(general::Nf);
        add(general::Nh);
        add(general::master_basis);
        add(general::one_loop_master_basis);
        add(general::max_moduli);

        add(BG::partial_trace_normalization);
        add(BG::partial_trace_scheme);

        add(IntegrandHierarchy::optimize_warmup_for_cuts);
        add(IntegrandHierarchy::amplitude_normalization);
        add(IntegrandHierarchy::parallelize_topology_fitting);
        add(IntegrandHierarchy::topology_fitting_max_nthreads);
        add(IntegrandHierarchy::number_of_Ds_values);
        add(IntegrandHierarchy::n_cached_D_values_warmup);

        add(integrals::include_incomplete_integrals);

        add(integrals::integral_family_1L);
        add(integrals::integral_family_2L);

        add(AmpEng::spurious_fact_check);

#ifdef WITH_PENTAGONS
        add(integrals::pentagon_functions_integration_precision);
        add(integrals::pentagon_functions_max_subintervals);
#endif

        add(OneLoopIntegrals::pentagon_mu2_insertion);
    }

    /**  Add with default reader */
    template<typename T> void add(setting<T>& set){
        auto t = all.find(set.name);
        if (t!=all.end()){
            throw std::logic_error("A setting with name "+set.name+" has already been registered! No duplciates are allowed!");
        }
        else{
            all.emplace(set.name,std::bind(read_answer<T>,std::placeholders::_1,&set));
        }
    }
    /**  You can pass any custom reader f */
    template<typename T> void add(setting<T>& set, std::function<bool(std::istream& in)> f ){
        auto t = all.find(set.name);
        if (t!=all.end()){
            throw std::logic_error("A setting with name "+set.name+" has already been registered! No duplciates are allowed!");
        }
        else{
            all.emplace(set.name,f);
        }
    }
};

void read_settings_from_stream(std::istream& in){

    using std::bind;

    /**
     * Register all settings to be read from file in constructor of settings_map.
     */
    static const thread_local settings_map mm{};

    while (!in.fail() && !in.eof()) {
        std::string the_command;
        in >> the_command;
        if (!in.fail() && !in.eof()){
            if (the_command.c_str()[0] == '#') {
                in >> ignoreLine;
            }
            else{
                auto s = mm.get().find(the_command);
                if(s==mm.get().end()){
                    _WARNING("Did not recognize setting ",the_command);
                    std::exit(1);
                    in >> ignoreLine;
                }
                else  {
                    bool sucess = (std::get<1>(*s))(in);
                    if(!sucess){
                        _WARNING("Failed to set ",the_command," to an allowed value! Exiting!");
                        std::exit(1);
                    }
                    in >> ignoreLine;
                }
            }
        }
    }
}

}

void use_setting(const std::string& in){
    std::stringstream s(in);
    read_settings_from_stream(s);
}


void read_settings_from_file(const std::string& filename){
    std::ifstream file;
    file.open(filename.c_str());
    if (file.fail()) {
        _WARNING("Could not open ", filename, ": done nothing. ");
    } else {
        _MESSAGE("#-#-#-#-#  Reading settings from file ", filename);
        read_settings_from_stream(file);
        _MESSAGE("#-#-#-#-#  Done");
    }
}

std::ostream& operator<<(std::ostream& o, const Caravel::settings::answer& a){
    if(a==Caravel::settings::answer::yes) return o<<"yes";
    return o<<"no";
}

}

bool verbosity_settings_container::instatiated = false;

verbosity_settings_container::verbosity_settings_container(){
    if(instatiated) throw std::logic_error("Do not create objects of class verbosity_settings_container!");
    instatiated = true;
}

  void verbosity_settings_container::show_all(){
    subtraction_engine = true;
    fit_function_removing_zeros = true;
    process_library_construction = true;
    remainder_pole_check = true;
    report_settings = true;
    report_integral_basis = true;
  }

  void verbosity_settings_container::go_quiet(){
    subtraction_engine = false;
    fit_function_removing_zeros = false;
    process_library_construction = false;
    remainder_pole_check = false;
    report_settings = false;
    report_integral_basis = false;
  }

  void verbosity_settings_container::silence_all(){
    subtraction_engine = false;
    fit_function_removing_zeros = false;
    process_library_construction = false;
    remainder_pole_check = false;
    report_settings = false;
    report_integral_basis = false;
  }

decltype(verbosity_settings) verbosity_settings{};

}
