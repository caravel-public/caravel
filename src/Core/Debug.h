/*
 * debug.h
 *
 *  Created on: Oct 13, 2009
 *      Author: daniel
 */

#ifndef DEBUG_H_
#define DEBUG_H_



#include "Core/Utilities.h"

#ifdef DEBUG_ON

// the two functions are necessary to make sure __LINE__ is replaced before being concatenated with unique_ID_
#define UNIQUE_ID(X)  unique_ID_ ## X
#define UNIQUE_STR_ID(X)  unique_ID_str ## X

#define DEBUG_MESSAGEIMPL(UNIQUE,...)\
    static bool  UNIQUE_ID(UNIQUE) = need_debug(__FILE__,__FUNCTION__);\
    static std::string UNIQUE_STR_ID(UNIQUE)=get_info_str(__FILE__,__FUNCTION__,UNIQUE);\
    if (UNIQUE_ID(UNIQUE)){Caravel::_MESSAGE(UNIQUE_STR_ID(UNIQUE),__VA_ARGS__);}

#define DEBUG_MESSAGE(...) DEBUG_MESSAGEIMPL(__LINE__,__VA_ARGS__)

#define DEBUG_PRINTIMPL(X,UNIQUE)\
    static bool  UNIQUE_ID(UNIQUE) = need_debug(__FILE__,__FUNCTION__);\
    std::string UNIQUE_STR_ID(UNIQUE)=get_info_str(__FILE__,__FUNCTION__,UNIQUE);\
    if (UNIQUE_ID(UNIQUE)){ std::cout << UNIQUE_STR_ID(UNIQUE); _PRINT(X);}

#define DEBUG_PRINT(X) DEBUG_PRINTIMPL(X,__LINE__)

#define DEBUG_IMPL(X,UNIQUE)\
    static bool  UNIQUE_ID(UNIQUE) = need_debug(__FILE__,__FUNCTION__);\
    std::string UNIQUE_STR_ID(UNIQUE)=get_info_str(__FILE__,__FUNCTION__,UNIQUE);\
    if (UNIQUE_ID(UNIQUE)){\
        if (UNIQUE_STR_ID(UNIQUE).size()!=0){\
            Caravel::_MESSAGE("::begin of ",UNIQUE_STR_ID(UNIQUE));\
        };\
        X;\
        if (UNIQUE_STR_ID(UNIQUE).size()!=0){\
            Caravel::_MESSAGE("::end of ",UNIQUE_STR_ID(UNIQUE));\
        };\
    }

#define DEBUG(X) DEBUG_IMPL(X,__LINE__)

//! A utility that prints demangled typeids
#include <cxxabi.h>
#include <memory>
#include <typeinfo>
namespace Caravel {
#ifdef __GNUG__
inline std::string demangle(const char* name) {
    int status = -4; // some arbitrary value to eliminate the compiler warning
    std::unique_ptr<char, void (*)(void*)> res{abi::__cxa_demangle(name, NULL, NULL, &status),
                                               std::free};
    return (status == 0) ? res.get() : name;
}
#else
inline std::string demangle(const char* name) { return name; };
#endif
}
#define _typeid(x) Caravel::demangle(typeid(x).name())

#else //DEBUG_ON

#define DEBUG_MESSAGE(...)

#define DEBUG_PRINT(X)
#define DEBUG(X)
#define _typeid(x) std::string("<DEBUG IS DISABLED>")

#endif //DEBUG_ON

namespace Caravel {

bool need_debug(const char* FileName,const char* FuncName);
std::string get_info_str(const char* FileName,const char* FuncName,int line);

}

#endif /* DEBUG_H_ */
