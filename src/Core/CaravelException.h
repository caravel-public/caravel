/*!\file CaravelException.h
  \brief Header for error handling in Caravel
*/
#ifndef ERROR_H_
#define ERROR_H_

#include <stdexcept>
#include <string>

namespace Caravel {

class CaravelException : public std::runtime_error {
  public:
    CaravelException() : std::runtime_error("unknown"){};
    CaravelException(const char* d) : std::runtime_error{d}{};
    CaravelException(const std::string& d) : std::runtime_error{d}{};
};





  /**
    * Exception thrown if there is a division by zero in the finite field.
    */
  class DivisionByZeroException : public std::exception{};

  /**
    * Exception thrown if there is a non quadratic residue in the finite field.
    */
  class NonQuadraticResidueException : public std::exception{};
}
#endif /*ERROR_H_*/
