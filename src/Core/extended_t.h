/*
 * @file extended_t.h
 * @date 14.03.2018 original source
 *
 * @brief Implementation of an extended algebraic type to handle slashed matrices with square roots
 *
 * Define number z = a_0  +  a_1 * e1, where
 * e1 = sqrt(mu12^2-mu11*mu22)
 * is a generating element. See multiplication table in the implementation of operator*
 *
 * This forms an algebra and allows to temporarily hold square roots of loop momenta.
 * All operators constructed such that when interacted with type of coefficients only a0 is
 * modified to reduce overhead where operation the full algebra is not required.
 *
 * IMPORANT: the above applies to operator*= and operator*. This might be unexpected behaviour.
 * In order to do the s-multiplication in the full algebra use member functions multeq_by or _mult_by functions.
 *
*/
#ifndef EXTENDED_T_H
#define EXTENDED_T_H

#include "Core/type_traits_extra.h"
#include "momD.h"
#include "Core/settings.h"

namespace Caravel{

template <typename T> inline extended_t<T> _mult(const extended_t<T>& s1, const extended_t<T>& s2);

template <typename T> struct extended_t{
    static_assert(is_exact<T>::value,"Extended algebra makes sense only for exact types!");
    typedef T element_type;
    T e0{};
    T e1{};

    extended_t() = default;
    extended_t(const T& se0) : e0{se0} {}
    extended_t(const T& se0, const T& se1) : e0{se0}, e1{se1} {}
    extended_t(T&& se0) : e0{std::move(se0)} {}
    extended_t(T&& se0, T&& se1) : e0{std::move(se0)}, e1{std::move(se1)} {}

    // if there is implicit converstion from T2 to T, forward it
    template <typename T2> extended_t(const T2& se0) : e0{(T)se0} {}
    template <typename T2> extended_t(const T2& se0, const T2& se1) : e0{(T)se0}, e1{(T)se1} {}

    extended_t& operator+=(const T& s){ e0+=s; return *this; }
    extended_t& operator-=(const T& s){ e0-=s; return *this; }
    extended_t& operator*=(const T& s){
        e0 *= s; e1*=s;
        return *this;
    }
    extended_t& operator/=(const T& s) {
        e0 /= s; e1/=s;
        return *this;
    }

    extended_t& operator+=(const extended_t& s){
        e0 += s.e0; e1+=s.e1;
        return *this;
    }
    extended_t& operator-=(const extended_t& s){
        e0 -= s.e0; e1-=s.e1;
        return *this;
    }

    //! add a number which is a multiple of e1
    extended_t& pluseq_by_e1(const T& s){
        e1 += s;
        return *this;
    }

    //! multiply by a number which is a multiple of e0 (same as operator *=)
    template <unsigned n> std::enable_if_t<n==0,extended_t&> multeq_by(const T& s){
        e0 *= s; e1*=s;
        return *this;
    }
    //! multiply by a number which is a multiple of e1 
    template <unsigned n> std::enable_if_t<n==1,extended_t&> multeq_by(const T& s){
        e0 *= (s*Caravel::EpsilonBasis<T>::squares[0]*Caravel::EpsilonBasis<T>::squares[1]);
        e1 *= s;
        return *this;
    }
};

template <typename T> extended_t<T> inline operator+(extended_t<T> a1, const extended_t<T>&a2){ return a1+=a2; }
template <typename T> extended_t<T> inline operator-(extended_t<T> a1, const extended_t<T>&a2){ return a1-=a2; }
template <typename T,unsigned n=0> inline extended_t<T> _mult_by(extended_t<T> a, const T& s){ return a.template multeq_by<n>(s); }
template <typename T,unsigned n=0> inline extended_t<T> _mult_by(const T&s, extended_t<T> a){ return a.template multeq_by<n>(s); }

template <typename T> extended_t<T> inline operator+(extended_t<T> a1, const T& x){ return a1+=x; }
template <typename T> extended_t<T> inline operator+(const T& x,extended_t<T> a1){ return a1+=x; }
template <typename T> extended_t<T> inline operator-(extended_t<T> a1, const T& x){ return a1-=x; }
template <typename T> extended_t<T> inline operator*(extended_t<T> a1, const T& x){ return a1*=x; }
template <typename T> extended_t<T> inline operator*(const T& x, extended_t<T> a1){ return a1*=x; }
template <typename T> extended_t<T> inline operator/(extended_t<T> a1, const T& x){ return a1/=x; }


template <typename T,unsigned n=0> inline extended_t<T>& _multeq_by(extended_t<T>& a, const T& s){ return a.template multeq_by<n>(s); }
template <typename T,unsigned n=0> inline std::enable_if_t<!is_extended<T>::value,T&> _multeq_by(T& a, const T& s){ return a*=s; }

template <typename T> inline extended_t<T>& _pluseq_by(extended_t<T>& a, const T& s, unsigned i=0){ return a.pluseq_by(s,i); }
template <typename T> inline std::enable_if_t<!is_extended<T>::value,T&> _pluseq_by(T& a, const T& s,unsigned i=0){ return a+=s; }

template <typename T> extended_t<T> inline operator-(extended_t<T> a){ return a.template multeq_by<0>(T(-1)); }

template <typename T> inline bool operator==(const extended_t<T>& a1, const extended_t<T>& a2){
    return a1.e0==a2.e0 && a1.e1 == a2.e1;
}

template <typename T> inline extended_t<T> _mult(const extended_t<T>& s1, const extended_t<T>& s2){
    extended_t<T> res{s1.e0*s2.e0,s1.e0*s2.e1};

    res.e0 += s1.e1*s2.e1*Caravel::EpsilonBasis<T>::squares[0]*Caravel::EpsilonBasis<T>::squares[1];
    res.e1 += s1.e1*s2.e0;

    return res;
}

template <typename T> inline extended_t<T> operator*(const extended_t<T>& s1, const extended_t<T>& s2){ return _mult(s1,s2);}

template <typename T,unsigned n=0> inline std::enable_if_t<!is_extended<T>::value,T> _mult_by(T a, const T& b){ return a*=b;}
template <typename T> inline std::enable_if_t<!is_extended<T>::value,T> _mult_by(T a, const T& b,unsigned){ return a*=b;}
template <typename T> inline std::enable_if_t<!is_extended<T>::value,T> _mult(T a, const T& b){ return a*=b;}

template <typename T> inline std::enable_if_t<!is_extended<T>::value, T> get_e0(const T& x) { return x; }
template <typename T, typename TE = typename T::element_type> inline TE get_e0(const T& x) { return x.e0; }

namespace detail{
    template <typename T, size_t D> void set_size(std::array<T,D>&){}
    template <typename T, size_t D> void set_size(Caravel::momentumD<T,D>&){}
    template <typename T, size_t D> void set_size(std::vector<T>& v){v.resize(D); v.shrink_to_fit();}
}

/**
 * This function implements transforms vectors from the "modified" non-normalized basis
 * arXiv:1712.03946v2  eqs. (II.21 -- II.23)
 *
 * to a particular detalization of loop momentum parameterization in the standard basis. This will be valid only for
 * the alternating metric, and verified by a unit test.
 *
 * The parameterization conerns only components {4,5} of Ds-dimensional vectors and reads
 * l = a1 n1 + a2 n2,    with a1,a2 --- components in a "modified" basis.
 *
 * n1 = {(mu11+1)/2,(1-mu11)/2};
 * n2 = Sqrt[mu12^2 - mu11 mu22]/(2 mu11) {-1 + mu11, -1 - mu11};
 *
 * This functions returns l with its components in the extended algebra.
 */
template <typename OutContainer, size_t D, typename InContainer>
std::enable_if_t<std::is_same<InContainer, OutContainer>::value, OutContainer> to_extended_t(InContainer m) {
    return m;
}

template <typename OutContainer, size_t D, typename InContainer>
std::enable_if_t<is_extended_v<typename OutContainer::value_type> && !is_extended_v<typename InContainer::value_type> && D <= 4, OutContainer>
to_extended_t(const InContainer& m) {
    OutContainer res;
    detail::set_size<typename OutContainer::value_type, D>(res);
    for (size_t i = 0; i < D; ++i) res[i] = m[i];
    return res;
}
template <typename OutContainer, size_t D, typename InContainer>
std::enable_if_t<is_extended_v<typename OutContainer::value_type> && !is_extended_v<typename InContainer::value_type> && D == 5, OutContainer>
to_extended_t(const InContainer& m) {
    using T = typename InContainer::value_type;
    OutContainer res;
    detail::set_size<typename OutContainer::value_type, D>(res);
    for (size_t i = 0; i < 4; ++i) res[i] = m[i];

    res[4] = {T(0), m[4]};

    return res;
}

template <typename OutContainer, size_t D, typename InContainer>
std::enable_if_t<is_extended<typename OutContainer::value_type>::value && !is_extended_v<typename InContainer::value_type> && D >= 6, OutContainer>
to_extended_t(const InContainer& m) {
    using T = typename InContainer::value_type;

    OutContainer res;
    detail::set_size<typename OutContainer::value_type, D>(res);

    for (size_t i = 0; i < 4; ++i) res[i] = m[i];

    const T& mu11 = EpsilonBasis<T>::squares[0];
    T invmu11 = T(1) / mu11;
    T half = T(1) / T(2);

    res[4] = {m[4] * half * (T(1) + mu11), m[5] * half * (T(1) - invmu11)};
    res[5] = {m[4] * half * (T(1) - mu11),-m[5] * half * (T(1) + invmu11)};

    for (size_t i = 6; i < D; ++i) res[i] = m[i];

    return res;
}


template <typename T> std::ostream& operator<<(std::ostream& s,const extended_t<T>& a){
    s<<a.e0 << "+" << a.e1<< "*e1";
    return s;
}


}
#endif
