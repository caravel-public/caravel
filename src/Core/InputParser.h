#pragma once

#include "Core/settings.h"
#include "Core/type_traits_extra.h"
#include "Core/MathInterface.h"
#include "Core/Debug.h"

#include <set>

namespace Caravel {

class InputParser {
  public:
    /**
     * Parse command line input which should be any number of MathLists
     */
    InputParser(int argc, char** argv);
    /**
     * Parse input from file which should be any number of MathLists, each in a new line.
     * Lines starting with "#" are ignored.
     */
    InputParser(std::string filename);

    /**
     * Look for an option with a given head.
     * If a template parameter is specified, will return
     * an object of the type constructed from a MathList corresponding to the given head.
     * Throws if the head is not found.
     */
    template <typename T = MathList> T get_cmd_option(std::string head);
    /**
     * The same as before, except return the provided defualt values,
     * if the head is not found.
     */
    template <typename T = MathList> T get_cmd_option(std::string head, T def);

    /**
     * Check if an option with a given head was set by the command line input.
     */
    bool cmd_option_exists(std::string head);

    /**
     * Check if there are any unused tokens left, and return them
     */
    std::vector<MathList> get_unused_tokens() const;

    template <typename T> static T parse_MathList(const MathList& l);

  private:
    std::vector<MathList> tokens;
    std::set<std::string> accessed_tokens;
    void report(const MathList& l) const;
    void check_input() const;
    template <typename T> T parse(const MathList& l);
};

template <typename T> T InputParser::get_cmd_option(std::string head) {
    auto f = std::find_if(tokens.begin(), tokens.end(), [&head](auto l) { return l.head == head; });
    if (f == tokens.end()) { throw std::runtime_error("Argument " + head + " does not exist!"); }
    return parse<T>(*f);
}

template <typename T> T InputParser::get_cmd_option(std::string head, T def) {
    auto f = std::find_if(tokens.begin(), tokens.end(), [&head](auto l) { return l.head == head; });
    if (f == tokens.end()) { return def; }
    return parse<T>(*f);
}

template <typename T> T InputParser::parse(const MathList& l) {
    accessed_tokens.insert(l.head);
    report(l);
    return parse_MathList<T>(l);
}

namespace detail {

template <typename T> T interpret_token(const std::string& it) {
    if constexpr (std::is_same_v<T, std::string>) { return it; }
    else {
        try {
            return InputParser::parse_MathList<T>(it);
        }
        catch (const std::invalid_argument&) {
            if constexpr (op_valid_v<std::istream&, T&, _utilities_private::right_shift>) {
                std::istringstream s(it);
                T el;
                s >> el;
                if (s.fail()) { throw std::runtime_error(it + " cannot be interpreted as " + _typeid(T)); }
                return el;
            }
            else {
                throw std::runtime_error(it + " cannot be interpreted as " + _typeid(T));
            }
        }
    }
}
} // namespace detail

template <typename T> T InputParser::parse_MathList(const MathList& l) {
    const auto& head = l.head;

    if constexpr (std::is_same_v<T, MathList>) { return l; }
    else if constexpr (std::is_integral_v<T>) {
        if (l.size() != 1) { throw std::runtime_error("MathList " + head + " cannot be interpreted as containing a " + _typeid(T)); }
        return std::stoi(l[0]);
    }
    else if constexpr (std::is_same_v<T, std::string>) {
        if (l.size() != 1) { throw std::runtime_error("MathList " + head + " cannot be interpreted as containing a string!"); }
        return l[0];
    }
    else if constexpr (std::is_constructible_v<T, MathList>) {
        return T{l};
    }
    else if constexpr (is_pair_v<T>) {
        if (l.size() != 2) { throw std::runtime_error("MathList " + head + " cannot be interpreted as containing a " + _typeid(T)); }
        return {detail::interpret_token<std::remove_cv_t<typename T::first_type>>(l[0]),
                detail::interpret_token<std::remove_cv_t<typename T::second_type>>(l[1])};
    }
    else if constexpr (is_iterable_v<T>) {
        using Tval = std::remove_cv_t<typename T::value_type>;
        std::vector<Tval> result;
        result.reserve(l.size());

        for (auto& it : l) { result.push_back(detail::interpret_token<Tval>(it)); }

        T toreturn(result.begin(), result.end());
        return toreturn;
    }
    else if constexpr (op_valid_v<std::istream&, T&, _utilities_private::right_shift>) {
        if (l.size() != 1) { throw std::runtime_error("MathList " + head + " cannot be interpreted as containing a " + _typeid(T)); }
        std::istringstream s(l[0]);
        T result;
        s >> result;
        if (s.fail()) { throw std::runtime_error("MathList " + head + " cannot be interpreted as containing a " + _typeid(T)); }
        return result;
    }
    else {
        static_assert(_delayed_static_false_v<T>, "Do not know how to parse the type!");
    }
}

/**
 * Read the whole into a string.
 * Throws if the file could not be open.
 *
 * @param filename
 *
 */
std::string read_file(const std::string& filename);

} // namespace Caravel

