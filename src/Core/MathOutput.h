#ifndef MATHOUTPUT_H
#define MATHOUTPUT_H

#include "Core/Utilities.h"
#include "MathInterface.h"

#include <iostream>
#include <complex>
#include <vector>
#include <string>

/****
 *  NOTE for developers:
 *
 *  Output to mathematica readable format is implemented by overload and specializing the (recursive) function
 *
 *  math_string
 *
 *  with custom types. The fallback is to use std::ostream<< operator overload
 *  if it is implemented (this might not give nice results of course).
 *  A default behaviour for iterable containers is also implemented, it makes a {...} list of elemenets.
 *
 *  DO NOT put your custom implementation in this file. Instead, include this file into your header,
 *  and implement there. In this file only basica overloads are provided.
 *
 *  If your type is not in namespace Caravel (e.g. nested inside), put the implementation
 *  of math_string into the namespace where the type is implemented.
 *  This will enable ADL to find it and prefer it over the catch-all overload.
 *
 */

namespace Caravel{

namespace detail {
template <typename T> std::string math_string_from_ostream(const T& c) {
    using Caravel::operator<<;
    std::stringstream out;
    out << c;
    return out.str();
}
} // namespace detail


/**
 * Catch-all overload.
 * will try to use operator<< if nothing else works.
 */
template <typename T> std::string math_string(const T&);

/**
 * Implemented specifications of math_string
 */
inline std::string math_string(std::string&& s) { return std::move(s); }
inline std::string math_string(const std::string& s) { return s; }
inline std::string math_string(const MathList& s) { return detail::math_string_from_ostream(s); }
inline std::string math_string(const char* s) { return detail::math_string_from_ostream(s); }
template <typename T> std::string math_string(const std::complex<T>&);
template <typename T1, typename T2> std::string math_string(const std::pair<T1,T2>&);

#ifdef VERY_HIGH_PRECISION
template <> std::string math_string(const RVHP& x);
#endif
#ifdef HIGH_PRECISION
template <> std::string math_string(const RHP& x);
#endif
template <> std::string math_string(const R& x);

template <typename T> std::string math_string(const T& in) {
    // we can make a normal list from any iterable type
    if constexpr (is_iterable_v<T>) {
        std::string str;
        str += "{";
        bool first = true;
        for (auto& el : in) {
            if(!first) {
                str += ",";
            }
            else {
                first = false;
            }
            str += math_string(el);
        }
        str += "}";
        return str;
    }
    // forward to operator<< in the worst case
    else {
        return detail::math_string_from_ostream(in);
    }
}

template <typename T> std::string math_string(const std::complex<T>& z) {
    return (std::string("Complex[") + math_string(std::real(z)) + "," + math_string(std::imag(z)) + "]");
}


template <typename T1, typename T2> std::string math_string(const std::pair<T1,T2>& p) {
    return  "{" + math_string(p.first) +","+ math_string(p.second) + "}";
}

/**
 * Some very useful utilities for extension
 */
namespace detail {
template <typename T> void append_helper(std::string& s, const T& x) { s += math_string(x); }
template <typename... args, typename T> void append_helper(std::string& s, const T& first, const args&... rest) {
    s += math_string(first);
    s += ",";
    append_helper(s, rest...);
}
} // namespace detail

template <typename... Ts> std::string math_list_with_head(std::string head, const Ts&... args) {
    std::string result = head + "[";
    detail::append_helper(result, args...);
    result += "]";
    return result;
}

template <typename TC, typename = std::enable_if_t<is_iterable_v<TC>>> std::string math_list_with_head(std::string head, const TC& cont) {
    std::string result = head + "[";
    for(auto& it : cont){
        detail::append_helper(result, it);
        result += ",";
    }
    if(result.back() == *",") result.pop_back();
    result += "]";
    return result;
}

} // namespace Caravel
#endif
