/**
 * @file Forest_utils.cpp 
 *
 * @date 5.5.2017
 *
 * @brief Implementing several classes used as members in Forest
 *
 * Keeping accessory utilities for the off-shell recursions
 *
*/

#include <iostream>
#include <algorithm>
#include <cassert>

#include "cutTopology.h"
#include "Debug.h"

namespace Caravel {

cutTopology::cutTopology(): number_of_vertices(0) {
}

cutTopology::cutTopology(Process& p1,const couplings_t& coups, const SelectionRule& sr): number_of_vertices(1) {
	count_particles_in_vertex.push_back(p1.get_process().size());
	cummulative_particles_in_vertices.push_back(p1.get_process().size());
	vertex_subprocess.push_back(ColorHandledProcess(p1));
	couplings.push_back(coups);
	selection_rules.push_back(sr);
	all_particles_in_vertices.insert(all_particles_in_vertices.end(),p1.get_process().begin(),p1.get_process().end());
}

void cutTopology::add_vertex(Particle& p1,const std::vector<Particle*>& vp,Particle& p2,const couplings_t& coups, const SelectionRule& sr){
	count_particles_in_vertex.push_back(vp.size());
	if(cummulative_particles_in_vertices.empty())
		cummulative_particles_in_vertices.push_back(vp.size());
	else
		cummulative_particles_in_vertices.push_back(cummulative_particles_in_vertices.back()+vp.size());
	vertex_subprocess.push_back(ColorHandledProcess(p1,vp,p2));
	couplings.push_back(coups);
	selection_rules.push_back(sr);
	number_of_vertices++;
	all_particles_in_vertices.insert(all_particles_in_vertices.end(),vp.begin(),vp.end());
}

Particle const* cutTopology::get_left() const{
	if(number_of_vertices==0)
		return nullptr;
	else
		return &vertex_subprocess[0].get_left();
}

Particle const* cutTopology::get_right() const{
	if(number_of_vertices==0)
		return nullptr;
	else
		return &vertex_subprocess.back().get_right();
}

std::vector<Particle*>& cutTopology::get_particles_vertices() {
	return all_particles_in_vertices;
}

const std::vector<Particle*>& cutTopology::get_particles_vertices() const {
	return all_particles_in_vertices;
}

std::string central_info(const ColorHandledProcess& ch){
	std::string toret("");
	if(ch.get_colorless().size()>0){
		toret+="{"+ch.get_colorless()[0]->get_name();
		for(size_t ii=1;ii<ch.get_colorless().size();ii++)
			toret+=","+ch.get_colorless()[ii]->get_name();
		toret+="}";
		if(ch.get_colored().size()>0)
			toret+="+";
	}
	if(ch.get_colored().size()>0){
		toret+="["+ch.get_colored()[0]->get_name();
		for(size_t ii=1;ii<ch.get_colored().size();ii++)
			toret+=","+ch.get_colored()[ii]->get_name();
		toret+="]";
	}
	return toret;
}

std::string coupling_info(const cutTopology::couplings_t& coups) {
    if (coups.size() == 0)
        return "()";
    else {
        std::string toret{"("};
        bool notfirst = false;
        for (const auto& it : coups) {
            if (notfirst) toret += ",";
            toret += it.first + ":" + std::to_string(it.second);
            notfirst = true;
        }
        toret += ")";

        return toret;
    }
}

std::tuple<int,int,Particle*> cutTopology::get_location_in_vertex(int location) const {
	std::tuple<int,int,Particle*> result(-1,-1,nullptr);
	// sanity check
	if(location<1){
		DEBUG_MESSAGE("cutTopology::get_location_in_vertex(i) called with i<1 !! Returned (-1,nullptr)");
		return result;
	}
	for(size_t ii=0;ii<cummulative_particles_in_vertices.size();ii++){
		if(location<=int(cummulative_particles_in_vertices[ii])){
			std::get<0>(result)=int(ii);
			std::get<1>(result)=location-1;
			if(ii>0)
				std::get<1>(result)-=int(cummulative_particles_in_vertices[ii-1]);
			std::get<2>(result)=all_particles_in_vertices[location-1];
			return result;
		}
	}
	DEBUG_MESSAGE("cutTopology::get_location_in_vertex(i) called with i=",location," failed to find location -- Returned (-1,nullptr)");
	return result;
}

cutTopology cutTopology::particle_substitute(const std::vector<std::pair<int,Particle*>>& location) const {
	cutTopology result(*this);

	// make corresponding substitution for each entry
	for(size_t ii=0;ii<location.size();ii++){
		// when location[ii].first corresponds to first or last particle, nothing to be done as cutTopology doesn't keep track of those pointers (has copies!)
		if(location[ii].first!=0&&location[ii].first!=int(all_particles_in_vertices.size()+1)){
			// get the vertex and the corresponding entry in ColorHandledProcess to make substitution
			std::tuple<int,int,Particle*> tosubs(this->get_location_in_vertex(location[ii].first));
			if(std::get<0>(tosubs)>-1){
				result.vertex_subprocess[std::get<0>(tosubs)].particle_substitute(std::get<2>(tosubs),location[ii].second);
				std::replace(result.all_particles_in_vertices.begin(),result.all_particles_in_vertices.end(),std::get<2>(tosubs),location[ii].second);
			}
		}
		else if(location[ii].first==0){
			if(!result.vertex_subprocess.empty())
				result.vertex_subprocess[0].set_left(location[ii].second);
		}
		else if(location[ii].first==int(all_particles_in_vertices.size()+1)){
			if(!result.vertex_subprocess.empty())
				result.vertex_subprocess.back().set_right(location[ii].second);
		}
	}

	return result;
}

size_t cutTopology::size() const {
	return vertex_subprocess.size();
}

std::vector<Particle*> cutTopology::get_colored(size_t i){
	if(i>=number_of_vertices){
		std::cout<<"Call to cutTopology::get_colored("<<i<<") when there are only "<<number_of_vertices<<" vertices -- Returned empty vector"<<std::endl;
		return std::vector<Particle*>();
	}
	return vertex_subprocess[i].get_colored();
}

std::vector<Particle*> cutTopology::get_colorless(size_t i){
	if(i>=number_of_vertices){
		std::cout<<"Call to cutTopology::get_colorless("<<i<<") when there are only "<<number_of_vertices<<" vertices -- Returned empty vector"<<std::endl;
		return std::vector<Particle*>();
	}
	return vertex_subprocess[i].get_colorless();
}

const ColorHandledProcess& cutTopology::get_vertex(size_t i) const{
	return vertex_subprocess[i];
}

cutTopology::couplings_t cutTopology::get_couplings_up_to(size_t i) const {
	if(i>number_of_vertices){
		DEBUG_MESSAGE("Called cutTopology::get_couplings_up_to(",i,") for a cutTopology with ",number_of_vertices," vertices!! -- Returned empty vector");
		return {};
	}

        couplings_t toret;

        for(size_t n =0; n<= i; ++n){
            const auto& cs = couplings.at(n);
            for(auto& j : cs){
                toret[j.first] += j.second;
            }
        }
	return toret;
}

cutTopology::couplings_t cutTopology::get_couplings_of_v(size_t i) const {
	return couplings.at(i);
}

SelectionRule cutTopology::get_selection_rules_of_v(size_t i) const {
	return selection_rules.at(i);
}

std::string cutTopology::get_short_name() const {
	// check if there is no vertices
	if(number_of_vertices==0)	return "--NULL--";

	std::string toreturn("");
	// check if it's only one vertex with no left/right particles
	if(number_of_vertices==1){
		const ColorHandledProcess& lch0(vertex_subprocess[0]);
		// check if is a simple tree
		if(lch0.get_left().get_name()=="NOPARTICLE"&&lch0.get_right().get_name()=="NOPARTICLE"){
			toreturn+=central_info(lch0);
			toreturn+=coupling_info(couplings[0]);
			return toreturn;
		}
	}
	for(size_t ii=0;ii<number_of_vertices;ii++){
		const ColorHandledProcess& lchi(vertex_subprocess[ii]);

		toreturn+="(";
		toreturn+=lchi.get_left().get_type().to_string();
		toreturn+="-";
		toreturn+=central_info(lchi);
		toreturn+=coupling_info(couplings[ii]);
		toreturn+="-";
		toreturn+=lchi.get_right().get_type().to_string();
		toreturn+=")";
		if(ii+1<number_of_vertices)
			toreturn+="|";
	}
	std::string infoleft("(");
	infoleft+=vertex_subprocess[0].get_left().get_name()+")";
	std::string inforight("(");
	inforight+=vertex_subprocess.back().get_right().get_name()+")";
	return infoleft+toreturn+inforight;
}

std::string cutTopology::show_internal_indices() const {
	std::string toret;
	for(size_t ii=0;ii<vertex_subprocess.size();ii++){
		toret+=vertex_subprocess[ii].show_internal_indices();
		if(ii+1!=vertex_subprocess.size())
			toret+="|";
	}
	return toret;
}

void cutTopology::show() const {
	std::cout<<"cutTopology: "<<get_short_name()<<std::endl;
	std::cout<<"	Tracking internal_index: "<<show_internal_indices()<<std::endl;
}

std::ostream& operator<<(std::ostream& o, const cutTopology& ct) {
    o << ct.get_short_name();
    return o;
}

void SelectionRule::add_qqs(internal_index_t i, const allowed_couplings& ps){
    assert(std::find(ps.begin(),ps.end(),0)==ps.end());
    qqs_coupling.insert({i,ps});
}

bool SelectionRule::is_match(internal_index_t i, const allowed_couplings& p) const{
    auto found = qqs_coupling.find(i);
    if(found == qqs_coupling.end()) return false;
    // if we found a record check that the coupling power is allowed
    return found->second == p;
}

bool SelectionRule::is_empty() const {
    return qqs_coupling.empty();
}



}
