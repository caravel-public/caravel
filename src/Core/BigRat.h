#pragma once

#include <gmpxx.h>
#include "Core/BigInt.h"
#include "Core/CaravelException.h"
#include <complex>
#include <iosfwd>

namespace Caravel{
  /**
   * Wrapper type for GMP BigRat, offering explicit conversion operators and
   * canonicalizing after each operation. (The expectation is that this will
   * never be used for heavy computation).
   */
struct BigRat {

protected:
    /**
     * Internal representation of value. 
     */
    mpq_class val;

public:

    BigRat() : val(0) {}
    explicit BigRat(const char* i)  : val(i) { }
    BigRat(const int32_t i) : val(i) {}
    BigRat(const uint32_t i) : val(i) {}
    // Not explicitly supported by GMP so we lean on a hack implemented
    // in BigInt
    BigRat(const uint64_t i) : BigRat(BigInt(i)) {}
    // Not explicitly supported by GMP, so we lean on a hack implemented
    // in BigInt
    BigRat(const int64_t i) : BigRat(BigInt(i)){ }

    BigRat(const BigInt& i) : val(i.val){}
    BigRat(const BigInt& a, const BigInt& b) : val(a.val, b.val){ val.canonicalize(); }

    explicit operator double() const	{ double tmp(val.get_d()); return tmp; }
    explicit operator std::complex<double>() const	{ std::complex<double> tmp(val.get_d()); return tmp; }

    BigRat& operator=( const BigRat& e) {
        val = e.val;
        return *this;
    }

    bool operator<=(const BigRat& e) const{ return val <= e.val; }
    bool operator<(const BigRat& e) const{ return val < e.val; }
    bool operator>=(const BigRat& e) const{ return val >= e.val; }
    bool operator>(const BigRat& e) const{ return val > e.val; }

    bool operator==(const BigRat& e) const{ return val == e.val; }

    bool operator!=(const BigRat& e) const{
        return !this->operator==(e);
    }

    inline BigRat& operator*= (const BigRat& e) {
      val *= e.val;
      val.canonicalize();
      return *this;
    }

  inline BigRat& operator/= (const BigRat& e) {
    if (e.val == 0){
      throw DivisionByZeroException();
    }

    val = val / e.val;
    val.canonicalize();

    return (*this);
  }

    inline BigRat& operator+= (const BigRat& e) {
      val += e.val;
      val.canonicalize();
      return *this;
    }

    inline BigRat& operator-= (const BigRat& e) {
      val -= e.val;
      val.canonicalize();
      return *this;
    }

    inline const BigRat operator* (const BigRat& e) const {
      BigRat tmp = (*this);
      tmp *= e;
      return tmp;
    }

    inline const BigRat operator/ (const BigRat& e) const {
      BigRat tmp = (*this);
      tmp /= e;
      return tmp;
    }

    inline const BigRat operator+ (const BigRat& e) const {
      BigRat tmp = (*this);
      tmp += e;
      return tmp;
    }

    inline const BigRat operator- (const BigRat& e) const {
      BigRat tmp = (*this);
      tmp -= e;
      return tmp;
    }

    inline const BigRat operator- () const {
      BigRat tmp;
      tmp.val = -val;
      return tmp;
    }

  /**
   *  Returns numerator of rational number as BigInt
   */
  BigInt num() const;

  /**
   *  Returns denominator of rational number as BigInt
   */
  BigInt den() const;

  friend std::istream& operator>> (std::istream& i, BigRat& a);
  friend std::ostream& operator<< (std::ostream& o, const BigRat& a);
};

  BigRat pow(const BigRat& arg, int power);

}

// specialize hasher
namespace std {
template <> struct hash<Caravel::BigRat> { size_t operator()(const Caravel::BigRat& xs) const; };
} // namespace std
