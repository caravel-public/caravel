#pragma once

#include "Core/Series.h"
#include "Core/type_traits_extra.h"
#include <assert.h>
#include <functional>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>
#include <map>
#include <string>
#include <sstream>

namespace Caravel {

// ------------------------------------------------------------------------------------------------------------------------------------------

/**
  * A Series where not all elements between the leading and last term are non-zero.
  */
template <typename T>
class SparseSeries {
public:

    SparseSeries() = default;

    /**
      * Construct a Series from a map.
      * 
      * @param map Contains as key the power and as value the coefficient associated to the given power.
      * @param var The value used for the Laurent Series parameter. Defaulted to "ep".
      */
    SparseSeries(const std::map<int, T>& map, const std::string& var = "ep") : m_data{std::move(map)}, m_leading{m_data.begin()->first}, m_last{m_data.rbegin()->first}, m_var{var} {}

    /**
      * Obtain a string representation of the SparseSeries.
      *
      * @return A string containing a representation of the SparseSeries.
      */
    std::string as_string() const {


        std::ostringstream os;
        os << "(";

        bool first = true;
        for (auto it = m_data.cbegin(); it != m_data.cend(); ++it) {
            if (!first) {
                os << " + ";
            }
            first = false;
            
            os << it->second << "*" << m_var << "^" << it->first;

        }

        os << " + O[" << m_var << "]^" << m_last + 1 << ")";

        return os.str();
    }


private:
    // Store the coefficients for each contributing power.
    std::map<int, T> m_data;

    /// The leading power 
    int m_leading; 

    /// The last power
    int m_last;

    /// Store the variable name
    std::string m_var;
};

/**
  * Allow to write a SparseSeries to a stream.
  * 
  * @param stream  The stream the SparseSeries shall be written to.
  * @param series  The SparseSeries instance to be written to the stream.
  * @return        The modified stream.
  */
template<typename T>
std::ostream& operator<<(std::ostream& stream, const SparseSeries<T>& series) {
    return stream << series.as_string();
}


/**
 * Flatten a container of Series<ValType>.
 *
 * This is usually required in reconstruction algorithms which require a container
 * of numerical values as input, while the master integral coefficients computed for
 * the amplitudes are containers of series of values.
 *
 * This class is constructed with an evaluation of coefficients. It will then map the result
 * to a one-dimensional vector that does not contain zero entries. The maps are stored such
 * that subsequent inputs can be mapped in the same manner and the reconstructed result can
 * be mapped back.
 *
 * If one wants to make sure that no terms are dropped that vanish by accident at construction,
 * one can construct two SeriesContainerFlatteners with different inputs and compare them for
 * equality.
 */
template <typename Container, typename KeyContainer, typename IsZero,
          typename ValType = std::enable_if_t<is_series<typename Container::value_type>::value, typename Container::value_type::coeff_type>,
          typename KeyType = typename KeyContainer::value_type>
class SeriesContainerFlattener {

  private:
    // Combine index and power
    struct IndexVsPower {
        unsigned long index;
        int power;

        bool operator==(const IndexVsPower& other) const { return index == other.index && power == other.power; }
    };

  public:
    /**
     * Construct a SeriesContainerFlattener.
     *
     * It is the responsibility of the user to assure that the ordering in `keys`
     * is equivalent to the ordering of their corresponding coefficients in `container`.
     *
     * @param container Contains the various Series that should be flattened.
     * @param keys      Contains the Keys that label which term the Series in container belong to.
     *                  They will also be used in the unflattening process.
     * @param is_zero   A function that checks whether a Series coefficient is zero. For exact types
     *                  the default is fine. For floating point types, a fuzzy equal function should be provided.
     */
    explicit SeriesContainerFlattener(const Container& container, const KeyContainer& keys,
                                      IsZero is_zero = [](const ValType& val) { return val == ValType(0); })
        : m_is_zero(is_zero) {

        // Check that number of keys matches number of elements in the container
        assert(container.size() == keys.size() && "Number of keys does not match number of elements in the container!");

        unsigned long position_counter = 0;

        for (std::size_t i = 0; i < keys.size(); ++i) {
            const auto key = keys[i];
            const auto elem = container[i];
            std::vector<IndexVsPower> index_vs_power;
            for (int power = elem.leading(); power <= elem.last(); ++power) {
                if (!m_is_zero(elem[power])) {
                    IndexVsPower tmp;
                    tmp.index = position_counter;
                    tmp.power = power;
                    index_vs_power.push_back(tmp);
                    position_counter++;
                }
            }
            // Only insert if elements are contained
            if (index_vs_power.size() > 0) {
                m_map.insert({key, index_vs_power});
            }
        }

        m_number_in_flattened = position_counter;

    }

    SeriesContainerFlattener(SeriesContainerFlattener&&) = default;
    SeriesContainerFlattener(const SeriesContainerFlattener&) = default;

    /**
     * Flatten a new container of Series.
     *
     * It is the responsibility of the user to assure that the ordering in `keys`
     * is equivalent to the ordering of their corresponding coefficients in `container`.
     *
     * @param container Contains the various Series.
     * @param keys      Contains the keys that label which term the Series in the container belongs to.
     * @return          A flattened version of `container` where all the non-vanishing series coefficients
     *                  are aligned in a one-dimensional vector.
     */
    std::vector<ValType> operator()(const Container& container, const KeyContainer& keys) const {

        // Check that number of keys matches number of elements in the Container
        assert(container.size() == keys.size() && "Number of keys does not match number of elements in the container!");

        // Create the empty output vector with enough capacity to hold all the values.
        std::vector<ValType> output(m_number_in_flattened);

        for (std::size_t i = 0; i < keys.size(); ++i) {
            const auto key = keys[i];
            const auto elem = container[i];
            auto find = m_map.find(key);
            if (find != m_map.end()) {
                for (const auto& pair : find->second) { 
                    output[pair.index] = elem[pair.power]; 
                }
            }
        }

        return output;
    }

    /**
     * Given a vector of some types, the original vector of series is constructed.
     *
     * Since the type of the container is a new template parameter, also containers
     * of new types can be constructed.
     *
     * This is especially useful since it allows to associate the analytically reconstructed
     * results to the functions.
     */
    template <typename NewValTypeContainer, typename NewValType = typename NewValTypeContainer::value_type>
    std::vector<std::pair<KeyType,SparseSeries<NewValType>>> unflatten(const NewValTypeContainer& vals, const KeyContainer& keys) const {

        // Create the empty output vector with enough capacity to hold all the values.
        std::vector<std::pair<KeyType,SparseSeries<NewValType>>> output;

        for (std::size_t i = 0; i < keys.size(); ++i) {

            const auto key = keys[i];

            auto find = m_map.find(key);
            if (find != m_map.end()) {
                std::map<int,NewValType> tmp;
                auto pairs = find->second;
                for (const auto& pair : pairs) {
                    tmp.insert({pair.power, vals[pair.index]});
                } 
                output.push_back(make_pair(key, SparseSeries<NewValType>(std::move(tmp))));
            }
        }

        return output;
    }

    /**
     * Compare two SeriesContainerFlattener for equality.
     *
     * Two SeriesContainerFlattener are considered equal if the maps they store are equal.
     *
     * @param other The SeriesContainerFlattener to be compared against *this.
     * @return      Result of the equality test.
     */
    bool operator==(const SeriesContainerFlattener<Container, KeyContainer, IsZero>& other) const { return m_map == other.m_map; }

    /**
     * Return the number of non-vanishing values.
     */
    unsigned long size() const {
        return m_number_in_flattened;
    }

  private:
    /// Store the function that checks whether a `ValueType` is vanishing.
    IsZero m_is_zero;

    /**
     * For each contributing key, store the locations of its non-vanishing coefficients.
     */
    std::unordered_map<KeyType, std::vector<IndexVsPower>> m_map;

    /// Number of coefficients to appear in the flattened output vector
    unsigned long m_number_in_flattened;
};

// -----------------------------------------------------------------------------------------------------------------------------------------

// Constructor functions to avoid specifying the templates


/**
 * Create a SeriesContainerFlattener by also providing the IsZero type.
 *
 * This should be used if the series coefficients are floating point numbers
 * to provide a fuzzy zero comparison.
 *
 * @param container Contains the series which shall be flattened.
 * @param keys      Contains the keys which label the different coefficients the Series are associated to.
 * @param is_zero   The unary function that checks whether its argument is to be considered zero.
 * @return          A SeriesContainerFlattener.
 */
template <typename Container, typename KeyContainer, typename IsZero>
auto make_series_flattener(const Container& container, const KeyContainer& keys, IsZero is_zero) {
    return SeriesContainerFlattener<Container, KeyContainer, IsZero>(container, keys, is_zero);
}

/**
 * Create a SeriesContainerFlattener.
 *
 * A default version is used for the function that checks whether a Series coefficient is zero,
 * so this should only be used if these coefficients are exact types since otherwise the
 * comparison for zero might fail to floating point representation.
 *
 * @param container Contains the series which shall be flattened.
 * @param keys      Contains the keys which label the different coefficients the Series are associated to.
 * @return          A SeriesContainerFlattener.
 */
template <typename Container, typename KeyContainer,
          typename ValType = std::enable_if_t<is_series<typename Container::value_type>::value, typename Container::value_type::coeff_type>>
auto make_series_flattener(const Container& container, const KeyContainer& keys) {
    auto is_zero = [](const ValType& val) { return val == ValType(0); };
    return make_series_flattener(container, keys, is_zero);
}

} // namespace Caravel
