#pragma once

namespace Caravel{
  namespace Core{
    template<typename T>
    T modular_inverse(const T a, const T p){

      T r0 = a;
      T s0 = 1;
      T t0 = 0;

      T r1 = p;
      T s1 = 0;
      T t1 = 1;

      while (r0 * r0 != T(1)) {
        T q = r0 / r1;
        T new_r = r0 - q * r1;
        T new_s = s0 - q * s1;
        T new_t = t0 - q * t1;

        r0 = r1;
        s0 = s1;
        t0 = t1;

        r1 = new_r;
        s1 = new_s;
        t1 = new_t;
      }

      if (s0 < 0){s0 += p;}

      return s0;
    }
  }
}
