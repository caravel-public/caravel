#include "MathInterface.h"
#include "Utilities.h"
#include "MathOutput.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>
#include <exception>

namespace Caravel {

// BEGIN[MathList]
MathList::MathList(const std::string& parent_string, const std::string& forced_head){
    size_t bracket_count = 0;
    std::stringstream buffer(parent_string);

    // Get the head (and check it's the correct one)
    getline(buffer, head, '[');
    bracket_count = 1;
    if ((forced_head != "" ) && (head != forced_head)){
        throw std::invalid_argument("Error while reading MathList:\n\tExpected head \"" + forced_head + "\"\n\tReceived head \"" + head + "\"");
    }

    std::stringstream current_stream;

    char c;
    while (buffer.get(c)){
        // Reckless disreguarding of whitespace (This would break
        // text arguments, if this ever got more advanced)
        switch (c) {
            //case '\"': _WARNING("Characters \" are ignored while reading MathList."); [[fallthrough]];
            case '\n':
            case ' ': continue;
            case '[': bracket_count++; break;
            case ']': bracket_count--; break;
        }

        if (bracket_count == 0){
            // We have finished all that is relevant of buffer, so
            // save and leave.
            auto final_string = current_stream.str();
            if (final_string.size() > 0){
                tail.push_back(std::move(final_string));
            }
            break;
        }

        if (((bracket_count == 1) && (c ==','))){
            // Push back, and clear the stream
            tail.push_back(current_stream.str());
            current_stream.clear(); // Clears state bits
            current_stream.str(""); // Empties
        }
        else current_stream << c;
    }
    if(bracket_count != 0){
        throw std::invalid_argument("ERROR while reading MathList: Missing closing ] while reading head \""+head+ "\" from \n\t\""+parent_string+"\"\n");
    }
    std::string test_empty;
    buffer >> test_empty;
    if(!test_empty.empty()){
        std::stringstream s;
        s << "ERROR while reading MathList: Unused characters after obtaining \n\t\"" << *this << "\"\n\tfrom\n\t\"" << parent_string << "\"\n";
        throw std::invalid_argument(s.str());
    }
}

std::istream& operator>>(std::istream& buffer, MathList& list) {
    size_t bracket_count = 0;

    list.head = std::string{};

    // Get the head (and check it's the correct one)
    getline(buffer, list.head, '[');
    bracket_count = 1;
    if (buffer.fail()){
        _WARNING("ERROR: Received head " , list.head );
        return buffer;
    }

    std::stringstream current_stream;

    char c;
    while (buffer.get(c)){
        // Reckless disreguarding of whitespace (This would break
        // text arguments, if this ever got more advanced)
        if (c == '\n' || c == ' ') continue;
        if (c == '[') bracket_count++;
        if (c == ']') bracket_count--;

        if (bracket_count == 0){
            // We have finished all that is relevant of buffer, so
            // save and leave.
            auto final_string = current_stream.str();
            if (final_string.size() > 0){
                list.tail.push_back(final_string);
            }
            break;
        }

        if (((bracket_count == 1) && (c ==','))){
            // Push back, and clear the stream
            list.tail.push_back(current_stream.str());
            current_stream.clear(); // Clears state bits
            current_stream.str(""); // Empties
        }
        else current_stream << c;
    }
    if(bracket_count != 0){
        _WARNING("SYNTAX ERROR: Missing closing ] while reading head \"",list.head);
        buffer.setstate(std::ios::failbit);
    }

    return buffer;
}

std::string MathList::operator[](size_t position) const{
    return tail.at(position);
}

size_t MathList::size() const  {
  return tail.size();
}

// END[MathList]

// Utility function takes mathematica string representing array index
// and turns it into cpp index
size_t math_string_to_cpp_index(std::string x){
    return std::stoi(x) - 1;
}

template <> std::string math_string(const R& x){
  std::ostringstream ss;
  ss << std::setprecision(16) << x;
  auto s = ss.str();
  auto pos = s.find("e");
  if (pos != std::string::npos){
    s.replace(s.find("e"), 1, "`16*^");
  }
  else{
    s+= "`16";
  }
  return s;
}

#ifdef VERY_HIGH_PRECISION
template <> std::string math_string(const RVHP& x){
  std::ostringstream ss;
  ss << std::setprecision(64) << x;
  auto s = ss.str();
  auto pos = s.find("e");
  if (pos != std::string::npos){
    s.replace(s.find("e"), 1, "`64*^");
  }
  else{
    s+= "`64";
  }
  return s;
}
#endif


#ifdef HIGH_PRECISION
template <> std::string math_string(const RHP& x){
  std::ostringstream ss;
  ss << std::setprecision(32) << x;
  auto s = ss.str();
  auto pos = s.find("e");
  if (pos != std::string::npos){
    s.replace(s.find("e"), 1, "`32*^");
  }
  else{
    s+= "`32";
  }
  return s;
}
#endif

std::ostream& operator<<(std::ostream& s, const MathList& l){
    s<<l.head << "[";
    if(!l.tail.empty()){
        for (size_t i = 0; i < l.size() - 1; i++) { s << l[i] << ","; }
        s << l[l.size() - 1];
    }
    s<<"]";

    return s;
}

void assert_head(const MathList& l, std::string h){
    if (l.head != h) {
        _WARNING("Expected head \"",h,"\", got \"",l.head,"\"\n\tin MathList : ",l);
        throw std::invalid_argument("MathList head assertion failure");
    }
}

std::string to_string(const MathList& l) {
    std::stringstream is;
    is << l;
    return is.str();
}

}
