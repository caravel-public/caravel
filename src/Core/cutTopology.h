/**
 * @file cutTopology.h 
 *
 * @date 26.3.2018 (coming previously for Forest.h)
 *
 * @brief Header file for basic class cutTopology
 *
*/
#ifndef CUTTOPOLOGY_H_
#define CUTTOPOLOGY_H_

#include <vector>
#include <string>
#include <tuple>
#include <utility>
#include <map>
#include <set>
#include <ostream>

#include "Particle.h"

namespace Caravel {

/**
 * Allows to put additional restrictions on couplings in cut vertices. So far only for qqs coupling.
 * The rule represents the ordered list of how scalar couplings are arranged for each quark line.
 * The convention is to always start from AntiQuark end end at Quark.
 * Essentially it represents a skeleton diagram where all couplings of scalars to quarks are specified.
 *
 * The rule represents chains like this:
 *  <1|---|2|---|3|---|1]
 *  <2|---|3|---|2]
 *  where on the left and right are internal indices of the chain,
 *  and in the middle are internal indices of external scalars which should be constracted to the line.
 *
 *  If there is a contraction between two chains, it looks like this:
 *  <1|---|1002|---|3|---|1]
 *  <2|---|1001|---|2]
 *  The indices of scalars contracting two lines are computed like 1000+[index of the other chain this scalar goes to]
 *
 *  Note that from the point of view of this selection rules
 *  <1|---|1002|---|3|---|1] != <1|---|3|---|1002|---|1]
 */
class SelectionRule {
  public:
    using internal_index_t = int;
    using coupling_name_t = std::string;
    using allowed_couplings = std::vector<int>;

    /**
     * Check if this selection rule allowes given
     * @param internal index
     * @param coupling constant
     * @param power of the coupling
     */
    bool is_match(internal_index_t, const allowed_couplings&) const;

    /**
     * Check if no scalar couplings are allowed.
     */
    bool is_empty() const;

    void add_qqs(internal_index_t, const allowed_couplings&);

    SelectionRule() = default;

  private:
    /**
     * For each internal index representing a chain,
     * gives a chain-ordered list of scalars coupling to this chain
     */
    std::map<internal_index_t, allowed_couplings> qqs_coupling;

  public:
    decltype(qqs_coupling) get_qqs_coupling() const { return qqs_coupling; }

    template <size_t> friend class Cut;
};

/**
 * Class for defining the topology of a cut, which is a main input to Forest::add_cut(.)
 *
 */
class cutTopology {
  public:
    /**
     * coupling (identified by name) --> it's power (int)
     */
    using couplings_t = std::map<std::string, int>;

  private:
    std::vector<size_t> count_particles_in_vertex;         /**< Stores the number of external particles on each vertex */
    std::vector<size_t> cummulative_particles_in_vertices; /**< Stores the number of all external particles up to given vertex */
    std::vector<ColorHandledProcess> vertex_subprocess;    /**< Each ColorHandledProcess for all vertices including internal/loop particles (if any) */
    std::vector<couplings_t> couplings;                    /**< Vertex by vertex couplings */
    std::vector<SelectionRule> selection_rules;            /**< Vertex by vertex additional information on how to select couplings */
    size_t number_of_vertices;                             /**< Counts how many vertices *this has */
    std::vector<Particle*> all_particles_in_vertices;      /**< local container of all particles included in vertex_subprocess keeping initial ordering */
    unsigned n_closed_light_fermion_loops{0};                    /**< Number of closed light fermion loops. Set from outside. */
    unsigned n_closed_heavy_fermion_loops{0};                    /**< Number of closed heavy fermion loops. Set from outside. */
    unsigned n_external_embeddings{
        1};                     /**< Number of copies of this cut topology in the forest with different embeddings of external fermions. Managed externally. */
    int n_copies{1};            /**< Pass information about multiplicative factor for this cutTopology. */
    bool pure_imaginary{false}; /** If true, means that the cut is multiplied by I globally */

  public:
    /**
     * Default constructor, basically an empty cut topology
     */
    cutTopology();
    /**
     * Trivial constructor for the case of tree-level (color ordered) amplitudes
     *
     * @param Process associated to local tree
     * @param couplings
     */
    cutTopology(Process&, const couplings_t&, const SelectionRule& = {});
    /**
     * Method to add a vertex to cutTopology. It is assumed this is the way general objects for describing cutTopology's will be built.
     *
     * @param Internal/loop particle to the left of new vertex
     * @param Vector of Particle pointers to external particles in the vertex
     * @param Internal/loop particle to the right of new vertex
     * @param coupling associated to new vertex
     */
    void add_vertex(Particle&, const std::vector<Particle*>&, Particle&, const couplings_t&, const SelectionRule& = {});
    /**
     * Gives access to left most particle in cutTopology
     */
    Particle const* get_left() const;
    /**
     * Gives access to right most particle in cutTopology
     */
    Particle const* get_right() const;
    /**
     * Gives access to Particle's contained in all vertices (with the order in which they were added)
     */
    std::vector<Particle*>& get_particles_vertices();
    const std::vector<Particle*>& get_particles_vertices() const ;
    /**
     * Method to return a modified copy of *this in which the particle pointer in position param[i].first is changed by param[i].second
     * (NOTICE: color indices in color_internal_indices of associated ColorHandledProcess'es are not redefined!)
     *
     * @param std::vector of std::pair's each entry describing a location and particle pointer
     * @return cutTopology which is the corresponding modified version of *this
     */
    cutTopology particle_substitute(const std::vector<std::pair<int, Particle*>>&) const;
    /**
     * Method that returns an std::tuple in which first labels the corresponding vertex,d second the entry in the vertex in which the integer argument is
     * located and finally the corresponding Particle*
     *
     * @param int which labels the required location
     * @return std::tuple with (vertex index,entry in vertex,Particle*) (if error, set to (-1,-1,nullptr)
     */
    std::tuple<int, int, Particle*> get_location_in_vertex(int) const;
    /**
     * Returns the number of vertices in the cutTopology
     *
     * @return size_t
     */
    size_t size() const;
    /**
     * Returns std::vector of Particle* corresponding to colored particles in vertex param
     *
     * @param size_t to label corresponding vertex
     * @return std::vector of colored Particle pointers in vertex
     */
    std::vector<Particle*> get_colored(size_t);
    /**
     * Returns std::vector of Particle* corresponding to colorless particles in vertex param
     *
     * @param size_t to label corresponding vertex
     * @return std::vector of colorless Particle pointers in vertex
     */
    std::vector<Particle*> get_colorless(size_t);
    /**
     * Give access to corresponding member in vertex_subprocess
     *
     * @return ColorHandledProcess associated with size_t
     */
    const ColorHandledProcess& get_vertex(size_t) const;

    typename decltype(vertex_subprocess)::const_iterator begin() const { return vertex_subprocess.cbegin(); }
    typename decltype(vertex_subprocess)::const_iterator end() const { return vertex_subprocess.cend(); }
    /**
     * Method to return coupling counting up to vertex asked for
     *
     * @param size_t to reflect vertices to add from 0 till param-1
     * @return std::vector of integers corresponding to coupling counting
     */
    couplings_t get_couplings_up_to(size_t) const;

    /**
     * Method to return coupling counting of the vertex
     */
    couplings_t get_couplings_of_v(size_t) const;
    SelectionRule get_selection_rules_of_v(size_t) const;

    /**
     * Short string description
     */
    std::string get_short_name() const;
    /**
     * Show structure of internal indices
     */
    std::string show_internal_indices() const;

    unsigned get_n_closed_light_fermion_loops() const { return n_closed_light_fermion_loops; }
    void set_n_closed_light_fermion_loops(unsigned n) { n_closed_light_fermion_loops = n; }

    unsigned get_n_closed_heavy_fermion_loops() const { return n_closed_heavy_fermion_loops; }
    void set_n_closed_heavy_fermion_loops(unsigned n) { n_closed_heavy_fermion_loops = n; }

    unsigned get_n_external_embeddings() const { return n_external_embeddings; }
    void set_n_external_embeddings(unsigned s) { n_external_embeddings = s; }

    int get_n_copies() const { return n_copies; }
    void set_n_copies(int s) { n_copies = s; }

    void show() const;

    void set_pure_imaginary(bool s = true) { pure_imaginary = s; }
    bool is_multiplied_by_I() { return pure_imaginary; }
};

std::ostream& operator<<(std::ostream&, const cutTopology&);

} // namespace Caravel
#endif	// CUTTOPOLOGY_H_
