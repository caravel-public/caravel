#ifndef MATHINTERFACE_H_INC
#define MATHINTERFACE_H_INC

#include "Core/Utilities.h"

#include <sstream>
#include <string>
#include <vector>

namespace Caravel {

struct MathList {
    MathList() = default;
    MathList(const std::string& parent_string, const std::string& forced_head = "");
    MathList(const char* parent_string) : MathList(std::string{parent_string}) {}
    std::string head{"MathList"};
    std::vector<std::string> tail{};
    std::string operator[] (size_t position) const; // Direct accessor of tail
    size_t size() const;
    auto begin() const { return tail.begin(); }
    auto end() const { return tail.end(); }
    auto begin() { return tail.begin(); }
    auto end() { return tail.end(); }
    auto cbegin() const { return tail.cbegin(); }
    auto cend() const { return tail.cend(); }
};

size_t math_string_to_cpp_index(std::string x);

/**
 * Throws std::invalid_argument if the head of the MathList doesn't match the expected one.
 * @param the list which nees to have the head
 * @param head to match
 */
void assert_head(const MathList&, std::string);

std::ostream& operator<<(std::ostream&, const MathList&);
std::istream& operator>>(std::istream&, MathList&);

/**
 * Returns a std::string corresponding to MathList
 */
std::string to_string(const MathList&);

}

#endif //MATHINTERFACE_H_INC
