#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <vector>
#include <complex>
#include <tuple>
#include <iostream>
#include <iomanip>
#include <map>
#include <unordered_map>
#include <functional>
#include <mutex>
#include "Core/type_traits_extra.h"
#include "Core/typedefs.h"

#include "wise_enum.h"

#if INSTANTIATE_GMP
#include "gmp_r.h"
#endif


// this is a ridiculous workaround for the gcc < 7 bug
#if __cplusplus <  201703L
#if defined(__clang__)
#define __FALLTHROUGH [[clang::fallthrough]]
#elif defined(__GNUC__)
#if __GNUC__<7
#define __FALLTHROUGH /* fallthrough */
#else
#define __FALLTHROUGH [[gnu::fallthrough]]
#endif
#endif
#else
#define __FALLTHROUGH [[fallthrough]]
#endif


//! A shortcut for very commonly used conditional compilation
#define USE_IF(cond) typename std::enable_if_t<(cond)>* = nullptr


namespace Caravel {

namespace _utilities_private{
    // helper to detect if opertator<< has been already implemented
    struct left_shift {
        template <class L, class R>
        constexpr auto operator()(L&& l, R&& r) const noexcept(noexcept(std::forward<L>(l) << std::forward<R>(r)))
            -> decltype(std::forward<L>(l) << std::forward<R>(r)) {
            return std::forward<L>(l) << std::forward<R>(r);
        }
    };
    // helper to detect if opertator>> has been already implemented
    struct right_shift {
        template <class L, class R>
        constexpr auto operator()(L&& l, R&& r) const noexcept(noexcept(std::forward<L>(l) >> std::forward<R>(r)))
            -> decltype(std::forward<L>(l) >> std::forward<R>(r)) {
            return std::forward<L>(l) >> std::forward<R>(r);
        }
    };


    // use this to dereference if pointer
    template <typename T> inline std::enable_if_t<std::is_pointer<T>::value, typename std::remove_pointer<T>::type> _dereference(T x) { return *x; }
    template <typename T> inline std::enable_if_t<!std::is_pointer<T>::value, T> _dereference(T x) { return x; }
}

template <class... Args> std::ostream& operator<<(std::ostream& out, const std::tuple<Args...>& t);
template <class A, class B> std::ostream& operator<<(std::ostream& s, const std::pair<A, B>& p);

//! operator<< for all contiguous containers, which do not have it defined in std:: already
template <typename T, typename = std::enable_if_t<is_iterable_v<T>>,
          typename = std::enable_if_t<!op_valid<std::ostream&, T, _utilities_private::left_shift>::value>>
std::ostream& operator<<(std::ostream& s, const T& v) {
    s << "{";
    auto first = true;
    for (auto& el : v) {
        if (first == false) { s << ","; }
        s << _utilities_private::_dereference(el);
        first = false;
    }
    return s << "}";
}

namespace _utilities_private{
    template<std::size_t> struct int_{};

    template <class Tuple, size_t Pos>
    std::ostream& print_tuple(std::ostream& out, const Tuple& t, int_<Pos> ) {
    out << _dereference(std::get< std::tuple_size<Tuple>::value-Pos >(t)) << ',';
    return print_tuple(out, t, int_<Pos-1>());
    }

    template <class Tuple>
    std::ostream& print_tuple(std::ostream& out, const Tuple& t, int_<1> ) {
    return out << _dereference(std::get<std::tuple_size<Tuple>::value-1>(t));
    }
}

template <class... Args>
std::ostream& operator<<(std::ostream& out, const std::tuple<Args...>& t) {
  out << '{';
  _utilities_private::print_tuple(out, t, _utilities_private::int_<sizeof...(Args)>());
  return out << '}';
}

template <typename T> std::enable_if_t<wise_enum::is_wise_enum_v<T>,std::ostream&> operator<< (std::ostream& s, T e){
    return s << wise_enum::to_string(e);
}

template <class A, class B> std::ostream& operator<<(std::ostream& s, const std::pair<A, B>& p){
    s << "(" << _utilities_private::_dereference(p.first) << "," << _utilities_private::_dereference(p.second) << ")" ;
    return s;
}

//! Print consecutively any number of printable (with operator<< defined) objects to cout.
template <typename T> inline void _MESSAGE(const T& x) { std::cout << x << std::endl; }
template <typename T, typename... Ts> inline void _MESSAGE(const T& first, const Ts&... args) {
    std::cout << first;
    _MESSAGE(args...);
}

//! Print consecutively any number of printable (with operator<< defined) objects to cerr.
template <typename T> inline void _WARNING(const T& x) { std::cerr << x << std::endl; }
template <typename T, typename... Ts> inline void _WARNING(const T& first, const Ts&... args) {
    std::cerr << first;
    _WARNING(args...);
}

//! Print consecutively any number of printable (with operator<< defined) objects to cout and report the filne and the filename
template <typename... Ts> inline void
_MESSAGE_R_impl(const char* filename, unsigned line,const Ts&... args) {
    std::cout << "From file " << filename << ":" << line << "\tMESSAGE:\n";
    _MESSAGE(args...);
}
#define _MESSAGE_R(...) Caravel::_MESSAGE_R_impl(__FILE__,__LINE__,__VA_ARGS__)

//! Print consecutively any number of printable (with operator<< defined) objects to cerr.
template <typename... Ts> inline void
_WARNING_R_impl(const char* filename, unsigned line,const Ts&... args) {
    std::cerr << "From file " << filename << ":" << line << "\tWARNING:\n";
    _WARNING(args...);
}


//! Same as _WARNING, but print file and line from where it is called 
#define _WARNING_R(...) Caravel::_WARNING_R_impl(__FILE__,__LINE__,__VA_ARGS__)
//! Same as _WARNING, but suppress the warning after Ntimes repetitions
#define _WARNING_N(Ntimes,...) { static size_t nwarns = 0; ++nwarns; if(nwarns <= Ntimes) {_WARNING(__VA_ARGS__); if (nwarns == Ntimes) _WARNING("---- further warnings of this type will be suppressed ----");} }
//! Same as _WARNING_R, but suppress the warning after Ntimes repetitions
#define _WARNING_R_N(Ntimes,...) { static size_t nwarns = 0; ++nwarns; if(nwarns <= Ntimes) {_WARNING_R_impl(__FILE__,__LINE__,__VA_ARGS__); if (nwarns == Ntimes) _WARNING("---- further warnings of this type will be suppressed ----");} }


namespace _utilities_private{

template <typename T> inline void _print_impl(const T& x) { std::cout << x << std::endl; }
template <typename T, typename... Ts> inline void _print_impl(const T& first, const Ts&... args) {
    std::cout << first << "\n";
    _print_impl(args...);
}
template <typename T> inline void _print_impl_enter(const char* names, const T& arg ) {
    std::cout << names << ": " << arg << std::endl;
}
template <typename... Ts> inline void _print_impl_enter(const char* names, const Ts&... args ) {
    std::cout << "Print " << names << ":\n";
    _print_impl(args...);
}

}

//! Macro that prints its argument and its value (useful to remember what we intended to print)
#define _PRINT(...) Caravel::_utilities_private::_print_impl_enter(#__VA_ARGS__, __VA_ARGS__)

}



namespace Caravel{

template <class T>
inline void hash_combine(std::size_t& seed, const T& v) {
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

template<typename A, typename... B>
class MemoizedFunction{
  private:
    std::mutex mutex;
    std::function<A(B...)> f;
  public:
  std::unordered_map<std::tuple<std::remove_cv_t<std::remove_reference_t<B>>...>,A> table;
  MemoizedFunction(const std::function<A(B...)>& _f): f(_f){}
  // Copy constructor grabs details, but will build own mutex, hence lock scope is local.
  MemoizedFunction(const MemoizedFunction<A,B...>& other) : f(other.f), table(other.table){}
  A operator()(B... b) {
      auto key = std::make_tuple(b...);
      auto memoized = table.find(key);
      if (memoized == table.end()) {
          auto result = f(b...);
          mutex.lock();
          table.emplace(std::move(key), result);
          mutex.unlock();
          return result;
      }
      else {
          return memoized->second;
      }
  }
};

/**
 * Returns a memoized version of the provided function. Copies the
 * function so that the memoized version can outlive the original. If
 * copying is heavy, then the suggestion is to pass a lambda capturing
 * a reference, so that it is obvious at the call site that the
 * memoized version captures a reference. Thread safe through mutexes.
 */
template<typename A, typename... B>
std::function<A(B...)> memoize(const std::function<A(B...)>& f){
  return [memoized = MemoizedFunction<A,B...>{f}](B... b) mutable -> A {return memoized(b...);};
}

/**
 * Applies f, pairwise to the input vectors. Python zipwith. Lifts f to the vector functor world.
 */
template<typename A, typename B, typename C>
std::vector<C> lift(std::function<C(A,B)> f, std::vector<A> xs, std::vector<B> ys){
    if (xs.size() != ys.size()){
	std::cerr << "Error in lift. The two input vectors are not the same size." << std::endl;
	exit(1);
    }

    // Uninitialized as "C" may not have default constructor
    std::vector<C> result;
    for (size_t i = 0; i < xs.size(); i++){
      result.push_back(f(xs[i], ys[i]));
    }

    return result;
}

/**
 * Applies f, tripletwise, to the input vectors. Python zipwith. Lifts f to vector functor world.
 */
template<typename A, typename B, typename C, typename D>
std::vector<D> lift(std::function<D(A,B,C)> f, std::vector<A> xs, std::vector<B> ys, std::vector<C> zs){
      if ((xs.size() != ys.size()) || (zs.size() != ys.size())){
	std::cerr << "Error in lift. Two input vectors are not the same size." << std::endl;
	exit(1);
    }

    // Uninitialized as "D" may not have default constructor
      std::vector<D> result;
    for (size_t i = 0; i < xs.size(); i++){
      result.push_back(f(xs[i], ys[i], zs[i]));
    }

    return result;
}


}

namespace std {
    template<typename T>
    struct hash<vector<T>>{
	public:
	size_t operator() (const vector<T>& xs) const{
	    hash<size_t> hasher;
	    size_t result = hasher(xs.size());
	    for (auto& x : xs){
	      Caravel::hash_combine(result, x);
	    }
	    return result;
	}

    };

    template<typename T, size_t N>
      struct hash<array<T,N>>{
	public:
        size_t operator() (const array<T,N>& xs) const{
	    hash<size_t> hasher;
	    size_t result = hasher(N);
	    for (auto& x : xs){
	      Caravel::hash_combine(result, x);
	    }
	    return result;
	}
    };

    template <typename A, typename B> struct hash<map<A, B>> {
      public:
        size_t operator()(const map<A, B>& x) const {
            size_t result = 0;
            for (const auto& p : x) { Caravel::hash_combine(result, p); }
            return result;
        }
    };

    template<typename A, typename B>
      struct hash<pair<A,B>>{
	public:
        size_t operator() (const pair<A,B>& x) const{
	    hash<std::remove_cv_t<A>> a_hasher;
	    size_t result = a_hasher(x.first);
	    Caravel::hash_combine(result, x.second);
	    return result;
	}

    };

    template<typename T>
      struct hash<std::complex<T>>{
	public:
	size_t operator() (const std::complex<T>& z) const{
	    hash<T> hasher;
	    size_t result = hasher(z.real());
	    Caravel::hash_combine(result, z.imag());
	    return result;
	}

    };

    namespace detail {
    template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1> struct HashValueImpl {
        static void apply(size_t& seed, Tuple const& tuple) {
            HashValueImpl<Tuple, Index - 1>::apply(seed, tuple);
            Caravel::hash_combine(seed, get<Index>(tuple));
        }
    };

    template <class Tuple> struct HashValueImpl<Tuple, 0> {
        static void apply(size_t& seed, Tuple const& tuple) { Caravel::hash_combine(seed, get<0>(tuple)); }
    };
    } // namespace detail

    template <typename... TT> struct hash<std::tuple<TT...>> {
        size_t operator()(std::tuple<TT...> const& tt) const {
            size_t seed = 0;
            detail::HashValueImpl<std::tuple<TT...>>::apply(seed, tt);
            return seed;
        }
    };
}

#endif /*UTILITIES_H_*/
