/**
 * @file precision_cast.h
 *
 * @brief Implements utilities to (trivially) cast floating point type containers to higher precision
 *
 * to_precision<T>(<container> A)
 * constructs a new container of the same type as A with each element replaced to T(A).
 * T is supposed to be a desired level of precision (complex) type (R(C), RHP(CHP), RVHP(CVHP)).
 *
 * Complex, vector, array and generic iterators are supported here.
 *
 * The generic cast from iterators can be used as a base to implement to_precision for
 * any other contiguous container (see e.g. an implementation for momentumD).
 * These implementations should be then put in the corresponding header file to keep this file without dependencies.
 *
 */
#ifndef PRECISION_CAST_H
#define PRECISION_CAST_H

#include "Core/type_traits_extra.h"
#include <vector>
#include <array>
#include <complex>

#ifdef USE_FINITE_FIELDS
namespace Caravel{ struct BigRat; }
#endif

namespace Caravel{

template <typename T> using EnableIfFloat = typename std::enable_if<is_floating_point<T>::value>::type;

template <typename Thigh, typename Tlow> inline std::enable_if_t<is_floating_point<Tlow>::value && is_floating_point<Thigh>::value,Thigh>
to_precision(const Tlow& a){
    static_assert(
            (is_complex<Thigh>::value && is_complex<Tlow>::value) or 
            (!is_complex<Thigh>::value && !is_complex<Tlow>::value),
            "to_precision should not be used to convert between complex and real types,"
            );
    return Thigh(a);
}

#ifdef USE_FINITE_FIELDS
template <typename T> T to_precision(const BigRat& a);
template <typename T> T to_precision(const std::complex<BigRat>& a);
#endif

//! Generic from iterators
template <typename Thigh, class InputIt, class OutputIt,
         typename=EnableIfFloat<Thigh>,
         typename=typename std::iterator_traits<InputIt>::difference_type,
         typename=typename std::iterator_traits<InputIt>::pointer,
         typename=typename std::iterator_traits<InputIt>::reference,
         typename=typename std::iterator_traits<InputIt>::value_type,
         typename=typename std::iterator_traits<OutputIt>::difference_type,
         typename=typename std::iterator_traits<OutputIt>::pointer,
         typename=typename std::iterator_traits<OutputIt>::reference,
         typename=typename std::iterator_traits<OutputIt>::value_type
> inline OutputIt
to_precision(InputIt first, InputIt last, OutputIt d_first){
    while (first != last){
        *d_first++ = to_precision<Thigh>(*first++);
    }
    return d_first;
}


//! Specialization for vectors
template <typename Thigh, typename Tlow, template <typename...> class Container, typename = EnableIfFloat<Thigh>, typename... ts>
inline std::enable_if_t<is_iterable_v<Container<Thigh>>, Container<Thigh>> to_precision(const Container<Tlow, ts...>& in) {
    Container<Thigh> r(in.size());
    to_precision<Thigh>(in.begin(),in.end(),r.begin());
    return r;
}

}
#endif
