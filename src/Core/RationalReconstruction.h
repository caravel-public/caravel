#pragma once 

#include "Core/typedefs.h"
#include <complex>
#include <functional>
#include <vector>

#include "Forest/cardinality.h"

namespace Caravel {
namespace RationalReconstruction{

    /**
     * Uses the chinese remainder theorem to compute the image of A
     * mod n*m from the image of A mod n (A1) and the image of A mod m (A2).
     * @return BigInt - Image of A mod n*m.
     */
    BigInt chinese_remainder(std::pair<BigInt, BigInt> A1, F32 A2);
    /**
     * Uses a rational reconstruction algorithm, to guess the rational number from an image.
     * @parameter pair<BigInt,BigInt> The image of x modulo n, provided in that order.
     * @return Rational - The guessed rational number.
     */
    BigRat rational_guess(const std::pair<BigInt, BigInt>& num);
    BigRat rational_guess(const F32& x);

    /**
     * Uses a rational reconstruction algorithm, to guess the rational number from an image.
     * Optimized to better guess highly composite numbers.
     * WARNING: scales exponentially because of the integer factorization algorithm with the size of the number!
     * So probably not a good idea to use if large numbers are expected.
     * @parameter pair<BigInt,BigInt> The image of x modulo n, provided in that order.
     * @return BigRat - The guessed rational number.
     */
    BigRat rational_guess_alternative(const std::pair<BigInt, BigInt>& num);
    BigRat rational_guess_alternative(const F32& x);

    std::complex<BigRat> rational_guess(const std::complex<F32>& x);

    /**
    * Rational lifting function. Abstracted for any function
    * types. Requires that the InTypePrime and InTypeRational are both
    * the same container, implemented over different numeric types.
    */
    template <template <typename...> class OutContainer, typename InTypeRational, typename InTypePrime>
    std::vector<OutContainer<BigRat>>
    rational_reconstruct(const InTypeRational& rational_x, std::function<std::vector<OutContainer<F32>>(const InTypePrime&)> modulo_f);

    /**
     * Rational reconstruct a function which evaluates someting in F = finite fields
     * ItContainer: container of F
     * InTypeRational: input mom_conf of rational type
     * OutContainer: container of BigRat (output)
     * guesser: a function which implements rational guess
     */
    template <typename ItContainer, typename OutContainer, typename InTypeRational, typename InTypePrime, typename GuessType>
    void rational_reconstruct(const InTypeRational& rational_x, std::function<ItContainer(const InTypePrime&)> modulo_f, OutContainer& out, GuessType guesser) ;

    template <typename ItContainer, typename OutContainer, typename InTypeRational, typename InTypePrime>
    void rational_reconstruct(const InTypeRational& rational_x, std::function<ItContainer(const InTypePrime&)> modulo_f, OutContainer& out) {
        rational_reconstruct<ItContainer, OutContainer, InTypeRational, InTypePrime>(
            rational_x, modulo_f, out, static_cast<BigRat (*)(const std::pair<BigInt, BigInt>&)>(rational_guess));
    }

} // namespace RationalReconstruction
} // namespace Caravel

#include "RationalReconstruction.hpp"

