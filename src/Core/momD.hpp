/*
 * momD.hpp
 * First created on 5.3.2016
 *
 * Template implementation of classes related to D dim momenta
 *
 */

#include <algorithm>
#include <cassert>
#include <numeric>
#include <string>
#include <functional>

#include "Core/CaravelException.h"
#include "Core/Debug.h"
#include "Core/MathOutput.h"

namespace Caravel {

template <class T, size_t D> momentumD<T, D>::momentumD(const T input[]) {
    //_WARNING("Construction of momD by raw memory copy. Dangerous!");
    std::copy(input, input + D, components.begin());
}

template <class T, size_t D> momentumD<T, D>::momentumD(const std::vector<T>& v) {
    const size_t isize = v.size();
    if (D < isize) throw CaravelException("Dimensional mismatch in momD constructor");
    for (size_t i = 0; i < isize; i++) { components[i] = v[i]; }
}

template <class T, size_t D> template <class... Ts> momentumD<T, D>::momentumD(const T& a0, const Ts&... args) {
    constexpr size_t isize = sizeof...(args);
    static_assert(D >= isize + 1, "Dimensional mismatch in momD constructor");

    T arr[isize] = {args...};

    components[0] = a0;
    for (size_t i = 0; i < isize; i++) { components[i + 1] = std::move(arr[i]); }
}

template <class T, size_t D> T momentumD<T, D>::pi(int i) const {
    if (i < int(D))
        return components[i];
    else {
        _WARNING("Wrong call for entry ", i, " for ", D, " dim vector --- returned 0");
        return T(0);
    }
}

template <class T, size_t D> std::vector<T> momentumD<T, D>::get_vector(size_t n) const {
    if (n > D) throw std::range_error("Requested a vector with too much elements from momentumD");
    std::vector<T> v(n);
    std::copy(components.begin(), components.begin() + n, v.begin());
    return v;
}

template <class T, size_t D> inline momentumD<T, D>& momentumD<T, D>::operator+=(const momentumD<T, D>& pin) {
    std::transform(components.begin(), components.end(), pin.components.begin(), components.begin(), std::plus<T>());
    return *this;
}

template <class T, size_t D> inline momentumD<T, D>& momentumD<T, D>::operator-=(const momentumD<T, D>& pin) {
    std::transform(components.begin(), components.end(), pin.components.begin(), components.begin(), std::minus<T>());
    return *this;
}

template <class T, size_t D> inline momentumD<T, D>& momentumD<T, D>::operator*=(const T& cin) {
    using namespace std::placeholders;
    std::transform(components.begin(), components.end(), components.begin(), std::bind(std::multiplies<T>(), cin, _1));
    return *this;
}

template <class T, size_t D> inline momentumD<T, D>& momentumD<T, D>::operator/=(const T& cin) {
    using namespace std::placeholders;
    std::transform(components.begin(), components.end(), components.begin(), std::bind(std::divides<T>(), _1, cin));
    return *this;
}

// template operations
template <class T, size_t D> inline momentumD<T, D> operator+(momentumD<T, D> mom1, const momentumD<T, D>& mom2) {
    return mom1 += mom2;
}

template <class T, size_t D> inline momentumD<T, D> operator-(momentumD<T, D> mom1, const momentumD<T, D>& mom2) {
    return mom1 -= mom2;
}

template <class T, size_t D> inline momentumD<T, D> operator-(momentumD<T, D> mom) {
    std::transform(mom.components.begin(), mom.components.end(), mom.components.begin(), std::negate<T>());
    return mom;
}

namespace _momD_internal {
using Caravel::_utilities_private::int_;
/**
 * Here we implement scalar products of Lorentz vectors. We iterate at compile time with the
 * help of types _internal::int_<i> over all components. This allows to define action for each index separately
 * with zero run time overhead.
 * unroll_scalar_product functions are helpers and are not supposed to be used anywhere
 */
template <size_t D, typename T, typename InputIt> inline void unroll_scalar_product(InputIt, InputIt, T&, int_<D>) {}

template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos >= 6 && is_complex<T>::value> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res -= p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}

template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && (Pos >= 6) && Pos % 2 != 0 && !is_complex<T>::value> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>);

template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && (Pos >= 6) && Pos % 2 == 0 && !is_complex<T>::value> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res += p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}
template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && (Pos >= 6) && Pos % 2 != 0 && !is_complex<T>::value> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res -= p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}

template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 5 && (!is_exact<T>::value or is_extended<T>::value)> unroll_scalar_product(InputIt p1, InputIt p2, T& res,
                                                                                                                       int_<Pos>) {
    res -= p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}
template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 5 && (is_exact<T>::value && !is_extended<T>::value)> unroll_scalar_product(InputIt p1, InputIt p2, T& res,
                                                                                                                       int_<Pos>) {
    typedef typename infer_unext_type<T>::type TE;
    res -= p1[Pos] * p2[Pos] * EpsilonBasis<TE>::squares[Pos - 4];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}
template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 4 && (!is_exact<T>::value or is_extended<T>::value) && is_complex<T>::value>
unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res -= p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}
template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 4 && (!is_exact<T>::value or is_extended<T>::value) && !is_complex<T>::value>
unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res += p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}
template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 4 && (is_exact<T>::value && !is_extended<T>::value) && is_complex<T>::value>
unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    typedef typename infer_unext_type<T>::type TE;
    res -= p1[Pos] * p2[Pos] * EpsilonBasis<TE>::squares[Pos - 4];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}
template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 4 && (is_exact<T>::value && !is_extended<T>::value) && !is_complex<T>::value>
unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    typedef typename infer_unext_type<T>::type TE;
    res += p1[Pos] * p2[Pos] * EpsilonBasis<TE>::squares[Pos - 4];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}

template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && (Pos >= 1 && Pos <= 3) && is_complex<T>::value> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res -= p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}

template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 3 && !is_complex<T>::value> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res -= p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}
template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 2 && !is_complex<T>::value> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res += p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}
template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<(Pos < D) && Pos == 1 && !is_complex<T>::value> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res -= p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}

template <size_t D, typename T, typename InputIt, size_t Pos>
inline std::enable_if_t<Pos == 0> unroll_scalar_product(InputIt p1, InputIt p2, T& res, int_<Pos>) {
    res += p1[Pos] * p2[Pos];
    unroll_scalar_product<D>(p1, p2, res, int_<Pos + 1>{});
}

/**
 * The functions that implements vector contraction and selects metric tensor components
 * correctly for each type. For complex types we use +-----... signature.
 * For real types with use alternating singature +-+-+-+-...
 * For integrals types we additionally mutiply in "squares" for 4th and 5th components.
 * Generic iterator is used here, so any container which has an iterator can be used with this implementation.
 * Other high level interface can be wrappers around this function for specific types.
 */
template <size_t D, size_t first = 0, typename T, typename InputIt1, typename InputIt2, typename = typename std::iterator_traits<InputIt1>::difference_type,
          typename = typename std::iterator_traits<InputIt1>::pointer, typename = typename std::iterator_traits<InputIt1>::reference,
          typename = typename std::iterator_traits<InputIt1>::value_type, typename = typename std::iterator_traits<InputIt2>::difference_type,
          typename = typename std::iterator_traits<InputIt2>::pointer, typename = typename std::iterator_traits<InputIt2>::reference,
          typename = typename std::iterator_traits<InputIt2>::value_type>
inline void scalar_product_impl(InputIt1 p1, InputIt2 p2, T& res) {
    static_assert(first < D, "Out of bounds");
    static_assert(std::is_same<typename std::iterator_traits<InputIt1>::value_type, T>::value, "Iterator value type does not match the scalar product type!");
    static_assert(std::is_same<typename std::iterator_traits<InputIt2>::value_type, T>::value, "Iterator value type does not match the scalar product type!");
    unroll_scalar_product<D, T, decltype(&*p1)>(&*p1, &*p2, res, int_<first>{});
}

} // namespace _momD_internal

template <class T, size_t D> inline T operator*(const momentumD<T, D>& mom1, const momentumD<T, D>& mom2) {
    T res(mom1[0] * mom2[0]);
    _momD_internal::scalar_product_impl<D, 1>(mom1.begin(), mom2.begin(), res);
    return res;
}

template <typename T, size_t D> inline std::enable_if_t<(D <= 4), T> compute_Dsm4_prod(const Caravel::momentumD<T, D>& l1, const Caravel::momentumD<T, D>& l2) {
    return T(0);
}
template <typename T, size_t D> inline std::enable_if_t<(D > 4), T> compute_Dsm4_prod(const Caravel::momentumD<T, D>& l1, const Caravel::momentumD<T, D>& l2) {
    T res(0);
    _momD_internal::scalar_product_impl<D, 4>(l1.begin(), l2.begin(), res);
    return res;
}

template <class T, size_t D> momentumD<T, D> inline operator*(const T& c, momentumD<T, D> mom) {
    return mom *= c;
}

template <class T, size_t D> momentumD<T, D> inline operator*(momentumD<T, D> mom, const T& c) {
    return mom *= c;
}

template <class T, size_t D> momentumD<T, D> inline operator/(momentumD<T, D> mom, const T& c) {
    return mom /= c;
}

template <typename T, size_t D> momentumD<T,D> momentumD<T, D>::get_parity_conjugated() const {
    auto new_elements = this->components;
    for (size_t i = 1; i < D; ++i) { new_elements[i] *= T(-1); }
    return new_elements;
}

template <class T, size_t D> std::ostream& operator<<(std::ostream& s, const momentumD<T, D>& p) {
    s << "(";
    if constexpr (is_finite_field_v<remove_complex_t<T>>) {
        for (size_t ii = 0; ii < D - 1; ii++) {
            s << p.pi(ii);
            if (ii == 4 or ii == 5) { s << "*Sqrt[" << EpsilonBasis<T>::squares[ii - 4] << "]"; }
            s << ",";
        }
        s << p.pi(D - 1);
        if constexpr (D == 5 or D == 6) { s << "*Sqrt[" << EpsilonBasis<T>::squares[D - 5] << "]"; }
    }
    else {
        for (size_t ii = 0; ii < D - 1; ii++) s << p.components[ii] << ",";
        s << p.components[D - 1];
    }
    return s << ")";
}

template <typename T, size_t D> std::string math_string(const momentumD<T, D>& momD) {
    bool first = true;
    std::string str;
    str += "{";
    for (size_t i = 0; i < D; i++) {
        if (first == false) { str += ","; }
        str += math_string(momD.pi(i));
        if (is_finite_field_v<remove_complex_t<T>> and (i == 4 or i == 5)) { str += "*Sqrt[" + math_string(EpsilonBasis<T>::squares[i - 4]) + "]"; }
        first = false;
    }
    str += "}";
    return str;
}

template <typename T, size_t D> bool operator==(const Caravel::momentumD<T, D>& k1, const Caravel::momentumD<T, D>& k2) {
    for (size_t i = 0; i < D; i++) {
        if (k1.pi(i) != k2.pi(i)) { return false; }
    }
    return true;
}

template <typename T, size_t D>
template <typename T2, typename std::enable_if_t<!std::is_same<T, T2>::value>*>
momentumD<T, D>::momentumD(const momentumD<T2, D>& momd) {
    for (size_t ii = 0; ii < D; ii++) { components[ii] = T(momd.pi(ii)); }
}

template <typename T, size_t D> template <size_t D2, typename std::enable_if_t<(D2 < D)>*> momentumD<T, D>::momentumD(const momentumD<T, D2>& momd) {
    for (size_t ii = 0; ii < D2; ii++) { components[ii] = momd.pi(ii); }
    for (size_t ii = D2; ii < D; ii++) { components[ii] = T(0); }
}
template <typename T, size_t D> template <size_t D2, typename std::enable_if_t<(D2 > D)>*> momentumD<T, D>::momentumD(const momentumD<T, D2>& momd) {
    for (size_t ii = 0; ii < D; ii++) { components[ii] = momd.pi(ii); }
}

template <typename Thigh, size_t D, typename Tlow, typename> inline Caravel::momentumD<Thigh, D> to_precision(const Caravel::momentumD<Tlow, D>& in) {
    std::array<Thigh, D> r;
    to_precision<Thigh>(in.begin(), in.end(), r.begin());
    return Caravel::momentumD<Thigh, D>(r);
}

// Instantiations (from D=4 to 8 and for int/real/complex with multiprecision)
#define _INSTANTIATE_MOMD_GEN(KEY, TYPE)                                                                                                                       \
    KEY template class momentumD<TYPE, 4>;                                                                                                                     \
    KEY template class momentumD<TYPE, 5>;                                                                                                                     \
    KEY template class momentumD<TYPE, 6>;                                                                                                                     \
    KEY template class momentumD<TYPE, 7>;                                                                                                                     \
    KEY template class momentumD<TYPE, 8>;                                                                                                                     \
    KEY template std::string math_string(const momentumD<TYPE, 4>&);                                                                                           \
    KEY template std::string math_string(const momentumD<TYPE, 6>&);                                                                                           \
    KEY template std::ostream& operator<<(std::ostream&, const momentumD<TYPE, 4>&);                                                                           \
    KEY template std::ostream& operator<<(std::ostream&, const momentumD<TYPE, 5>&);                                                                           \
    KEY template std::ostream& operator<<(std::ostream&, const momentumD<TYPE, 6>&);                                                                           \
    KEY template std::ostream& operator<<(std::ostream&, const momentumD<TYPE, 7>&);                                                                           \
    KEY template std::ostream& operator<<(std::ostream&, const momentumD<TYPE, 8>&);

#define _INSTANTIATE_MOMD(TYPE) _INSTANTIATE_MOMD_GEN(, TYPE)
#define _INSTANTIATE_MOMD_EXTERN(TYPE) _INSTANTIATE_MOMD_GEN(extern, TYPE)

_INSTANTIATE_MOMD_EXTERN(R)
#ifdef HIGH_PRECISION
_INSTANTIATE_MOMD_EXTERN(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_MOMD_EXTERN(RVHP)
#endif
_INSTANTIATE_MOMD_EXTERN(std::complex<double>)
#ifdef HIGH_PRECISION
_INSTANTIATE_MOMD_EXTERN(std::complex<RHP>)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_MOMD_EXTERN(std::complex<RVHP>)
#endif

#ifdef USE_FINITE_FIELDS
_INSTANTIATE_MOMD_EXTERN(F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_MOMD_EXTERN(BigRat)
#endif
#endif

#ifdef INSTANTIATE_GMP
_INSTANTIATE_MOMD_EXTERN(RGMP)
_INSTANTIATE_MOMD_EXTERN(CGMP)
#endif

} // namespace Caravel

// Hash and equality functions for unordered_map
namespace std {
template <typename T, size_t D> struct hash<Caravel::momentumD<T, D>> { size_t operator()(const Caravel::momentumD<T, D>& k) const; };

template <typename T, size_t D> size_t hash<Caravel::momentumD<T, D>>::operator()(const Caravel::momentumD<T, D>& k) const {
    hash<T> hasher;
    size_t result = hasher(k.pi(0));
    for (size_t i = 1; i < D; i++) { Caravel::hash_combine(result, k.pi(i)); }
    return result;
}

} // namespace std

