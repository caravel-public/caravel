#include "RationalReconstruction.h"
#include "Core/ModularInverse.h"
#include <algorithm>
#include <numeric>

namespace Caravel {
namespace RationalReconstruction{

/**
 * Given a number n, return a set of it's prime factors.
 * This is a very simple "trial division" algorithm.
 */
std::set<BigInt> get_prime_factors(BigInt n) {
    std::set<BigInt> all;
    // Print the number of 2s that divide n

    while (n % 2 == 0) {
        all.insert(2);
        n /= 2;
    }

    BigInt f = 3;

    while (f * f <= n) {
        if (n % f == 0) {
            all.insert(f);
            n /= f;
        }
        else {
            f += 2;
        }
    }

    if (n != 1) all.insert(n);

    return all;
}

BigRat rational_guess(const std::pair<BigInt, BigInt>& num) {
    auto a = num.first;
    auto p = num.second;
    BigInt r0 = a;
    BigInt s0 = 1;
    BigInt t0 = 0;

    BigInt r1 = p;
    BigInt s1 = 0;
    BigInt t1 = 1;

    while (r1 * r1 > p) {
        BigInt q = r0 / r1;
        BigInt new_r = r0 - q * r1;
        BigInt new_s = s0 - q * s1;
        BigInt new_t = t0 - q * t1;

        r0 = r1;
        s0 = s1;
        t0 = t1;

        r1 = new_r;
        s1 = new_s;
        t1 = new_t;
    }

    return BigRat(r1)/BigRat(s1);
}

BigRat rational_guess_alternative(const std::pair<BigInt, BigInt>& num) {

    auto a = num.first;
    auto p = num.second;
    BigInt r0 = a;
    BigInt s0 = 1;
    BigInt t0 = 0;

    BigInt r1 = p;
    BigInt s1 = 0;
    BigInt t1 = 1;

    std::vector<std::pair<BigInt, BigInt>> candidates;

    while (r1 * r1 > p) {
        BigInt q = r0 / r1;
        BigInt new_r = r0 - q * r1;
        BigInt new_s = s0 - q * s1;
        BigInt new_t = t0 - q * t1;

        r0 = r1;
        s0 = s1;
        t0 = t1;

        r1 = new_r;
        s1 = new_s;
        t1 = new_t;

        candidates.emplace_back(r1, s1);
    }

    auto compute_score = [](const std::pair<BigInt, BigInt>& p) -> BigInt {
        auto all_factors = get_prime_factors(abs(p.second));
        return abs(p.first * std::accumulate(all_factors.begin(), all_factors.end(), BigInt{1}, std::multiplies<BigInt>()));
    };

    std::vector<BigInt> scores;
    scores.reserve(candidates.size());

    for (const auto& it : candidates) { scores.push_back(compute_score(it)); }

    auto res = candidates[std::distance(scores.begin(), std::min_element(scores.begin(), scores.end()))];

    // make sure the denominator is a positive BigInt
    if(res.second < BigInt(0)){
        res.first=-res.first;
        res.second=-res.second;
    }

    return BigRat(res.first, res.second);
}

  BigInt chinese_remainder(std::pair<BigInt, BigInt> A1, F32 A2){
    auto [a1, n1] = A1;

    uint32_t A2int(A2);
    BigInt a2(A2int);
    BigInt n2(F32::get_p());

    if (n1 == 1){
      return a2;
    }

    BigInt n2inv = Core::modular_inverse(n2, n1);
    BigInt m1 = n2inv*n2;
    BigInt m2 = (BigInt(1)-m1) % (n1*n2);

    BigInt res = (m1*a1+m2*a2) % (n1*n2);
    return res;
  }

  BigRat rational_guess(const F32& x){
    uint64_t p = F32::get_p();
    uint64_t xx(x);
    std::pair<BigInt, BigInt> residue = std::make_pair(BigInt(xx), BigInt(p));
    return rational_guess(residue);
  }

  BigRat rational_guess_alternative(const F32& x){
    uint64_t p = F32::get_p();
    uint64_t xx(x);
    std::pair<BigInt, BigInt> residue = std::make_pair(BigInt(xx), BigInt(p));
    return rational_guess_alternative(residue);
  }

  std::complex<BigRat> rational_guess(const std::complex<F32>& x){
    return std::complex<BigRat>(rational_guess(x.real()),rational_guess(x.imag()));
  }

} // namespace RationalReconstruction
} // namespace Caravel
