/*
 * extend.cpp
 * Added on 22.6.2016
 *
 * Inherited from V1's extend.cpp from Daniel, David, Fernando and Harald
 *
 */

#include "extend.h"
#include "Debug.h"
#include "spinor/spinor.h"
#include "precision_cast.h"
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd_suppl.h"
#endif
#include "Core/typedefs.h"

#if INSTANTIATE_GMP
#include "gmp_r.h"
#endif


namespace Caravel {
namespace _extend_impl{

using namespace std;


template <class T> using momentum = momentumD<T,4>;


template <class T_low,class T_high> momentumD_configuration<T_high,4> extend_daniel(const momentumD_configuration<T_low,4>& mc,const vector<int>& ind){
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
	CPU_FIX CF;
#endif
	vector<momentum<T_high>> momenta;

	momentum<T_high> sum;

	size_t n=ind.size();
	for (size_t i=0;i<n-2;i++){
		momenta.push_back(to_precision<T_high>(mc.p(ind[i])));
	}

	smatrix<T_high> P(-sum);

	const auto& mcnm2 = mc.p(ind[n-2]);
	const auto& mcnm1 = mc.p(ind[n-1]);

	auto LA1 = to_precision<T_high>(la<T_low>(mcnm2));
	auto LA2 = to_precision<T_high>(la<T_low>(mcnm1));

	la<T_high> Z1(T_high(1,0),T_high(0,0));
	la<T_high> Z2(T_high(0,0),T_high(1,0));
	lat<T_high> Z3(T_high(1,0),T_high(0,0));
	lat<T_high> Z4(T_high(0,0),T_high(1,0));


	T_high a1=LA1*Z2;
	T_high a2=-LA1*Z1;
	T_high A1=LA2*Z2;
	T_high A2=-LA2*Z1;

	T_high P1=Z4*P*Z2;
	T_high P2=-Z4*P*Z1;
	T_high P3=-Z3*P*Z2;
	T_high P4=Z3*P*Z1;


	T_high b1=-((-(A2*P1) + A1*P3)/(-(A1*a2) + a1*A2));
	T_high b2=-((-(A2*P2) + A1*P4)/(-(A1*a2) + a1*A2));
	T_high B1=-((-(a2*P1) + a1*P3)/(A1*a2 - a1*A2));
	T_high B2=-((-(a2*P2) + a1*P4)/(A1*a2 - a1*A2));


	momenta.push_back(LvBA(lat<T_high>(b1,b2),la<T_high>(a1,a2)));
	momenta.push_back(LvBA(lat<T_high>(B1,B2),la<T_high>(A1,A2)));

	return momentumD_configuration<T_high,4>(momenta);
}



// Fernando and harald's extend
template <class T_low,class T_high,size_t D> momentumD_configuration<T_high,D> extend_real(const momentumD_configuration<T_low,D>& mc,const std::vector<int>& ind){
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
	CPU_FIX CF;
#endif

	la<T_high> Z1(T_high(1,0),T_high(0,0));
	la<T_high> Z2(T_high(0,0),T_high(1,0));
	lat<T_high> Z3(T_high(1,0),T_high(0,0));
	lat<T_high> Z4(T_high(0,0),T_high(1,0));

	vector<momentum<T_high> > momenta;

	momentum<T_high > sum;
	size_t n=ind.size();
	for (size_t i=0;i<n-2;i++){
		// check for complex momenta
#define SMALL_COMPLEX_PART 1e-10
		if(abs(imag(mc.p(ind[i]).pi(0)))>SMALL_COMPLEX_PART ||
				abs(imag(mc.p(ind[i]).pi(1)))>SMALL_COMPLEX_PART ||
				abs(imag(mc.p(ind[i]).pi(2)))>SMALL_COMPLEX_PART ||
				abs(imag(mc.p(ind[i]).pi(3)))>SMALL_COMPLEX_PART){
			_WARNING("CAREFUL: Using extend_real for a momentum with large complex part");
                        momenta.push_back(to_precision<T_high>(mc.p(ind[i])));
		}
		else{
			lat<T_high> mci(to_precision<T_high>(mc.p(ind[i])));

			if(real(mc.p(ind[i]).pi(0))<=0){
				momenta.push_back(LvBA(mci,
						la<T_high>(conj(Z4*mci),-conj(Z3*mci))
                                                )
                                        );
			}
			else{
				momenta.push_back(LvBA(mci,
						la<T_high>(-conj(Z4*mci),conj(Z3*mci))));
			}
		}
		sum+=momenta.back();
	}

	momentum<T_high> knm1c;
	momentum<T_low> mcnm2(mc.p(ind[n-2]).pi(0),mc.p(ind[n-2]).pi(1),mc.p(ind[n-2]).pi(2),mc.p(ind[n-2]).pi(3));
        lat<T_high> mcnm2Lt = to_precision<T_high>(lat<T_low>(mcnm2));
	if(abs(imag(mc.p(ind[n-2]).pi(0)))>SMALL_COMPLEX_PART ||
			abs(imag(mc.p(ind[n-2]).pi(1)))>SMALL_COMPLEX_PART ||
			abs(imag(mc.p(ind[n-2]).pi(2)))>SMALL_COMPLEX_PART ||
			abs(imag(mc.p(ind[n-2]).pi(3)))>SMALL_COMPLEX_PART){
		_WARNING("CAREFUL: Using extend_real for a momentum with large complex part");
		knm1c=to_precision<T_high>(mc.p(ind[n-2]));
	}
	else{
		if(real(mc.p(ind[n-2]).pi(0))<=0){
			knm1c=LvBA(mcnm2Lt, la<T_high>(conj(Z4*mcnm2Lt),-conj(Z3*mcnm2Lt)));
		}
		else{
			knm1c=LvBA(mcnm2Lt, la<T_high>(-conj(Z4*mcnm2Lt),conj(Z3*mcnm2Lt)));
		}
	}
	// notice that the sqrt is always taken in the vicinity of 1, away of its branch cut
	T_high sqrtt=sqrt(-sum*sum/(T_high(2,0)*sum*knm1c));
	// final extended k_{n-1}
	momentum<T_high> knm1f;
	if(abs(imag(mc.p(ind[n-2]).pi(0)))>SMALL_COMPLEX_PART ||
			abs(imag(mc.p(ind[n-2]).pi(1)))>SMALL_COMPLEX_PART ||
			abs(imag(mc.p(ind[n-2]).pi(2)))>SMALL_COMPLEX_PART ||
			abs(imag(mc.p(ind[n-2]).pi(3)))>SMALL_COMPLEX_PART){
		_WARNING("CAREFUL: Using extend_real for a momentum with large complex part");
		knm1f=LvBA(sqrtt*mcnm2Lt,sqrtt*to_precision<T_high>(la<T_low>(mcnm2)));
	}
	else{
		if(real(mc.p(ind[n-2]).pi(0))<=0){
			knm1f=LvBA(sqrtt*mcnm2Lt, sqrtt*la<T_high>(conj(Z4*mcnm2Lt),-conj(Z3*mcnm2Lt)));
		}
		else{
			knm1f=LvBA(sqrtt*mcnm2Lt, sqrtt*la<T_high>(-conj(Z4*mcnm2Lt),conj(Z3*mcnm2Lt)));
		}
	}
	sum+=knm1f;
	// extended k_{n}, just by momentum conservation
	momentum<T_high> knf=(-sum);
	// to make sure we didn't hit a branch cut
	// completed k_{n}
	momentum<T_low> mcnm1(mc.p(ind[n-1]).pi(0),mc.p(ind[n-1]).pi(1),mc.p(ind[n-1]).pi(2),mc.p(ind[n-1]).pi(3));
	momentum<T_high> knc(to_precision<T_high>(mcnm1));

        la<T_high> knfL(knf);
        lat<T_high> knfLt(knf);
        la<T_high> kncL(knf);
        lat<T_high> kncLt(knf);


	T_high phase(1,0);
	T_high phaseinv(1,0);

        using R_high = typename T_high::value_type;
        static const R_high thr = R_high(1)/R_high(100000);

	if( (abs((knfL-kncL)*Z1)>thr ||
		abs((knfL-kncL)*Z2)>thr ||
		abs(Z3*(knfLt-kncLt))>thr ||
		abs(Z4*(knfLt-kncLt))>thr )
	){
		DEBUG_MESSAGE("\nfixing phases in extended momentum --- extend of complex momenta\n");
		T_high knca=(kncL*Z1+kncL*Z2)/T_high(2);
		T_high knfa=(knfL*Z1+knfL*Z2)/T_high(2);
		phase=knca/knfa;
		phaseinv=knfa/knca;

		DEBUG_PRINT(kncL);
		DEBUG_PRINT(kncLt);
		DEBUG_PRINT(knfL);
		DEBUG_PRINT(knfLt);
		DEBUG_PRINT(phase*knfL);
		DEBUG_PRINT(phaseinv*knfLt);
		DEBUG_PRINT(mc.p(ind[n-1]));
		DEBUG_MESSAGE("\n");
	}

	momenta.push_back(knm1f);
	momenta.push_back(LvBA(phaseinv*knfLt,phase*knfL));

	vector<momD<T_high,D> > momentaD;
        for(auto& it: momenta) momentaD.emplace_back(it);

	return momentumD_configuration<T_high,D>(momentaD);
}

#ifdef HIGH_PRECISION
template momentumD_configuration<CHP,4> extend_daniel<C,CHP>(const momentumD_configuration<C,4>& mc,const vector<int>& ind);
#endif
#ifdef VERY_HIGH_PRECISION
template momentumD_configuration<CVHP,4> extend_daniel<C,CVHP>(const momentumD_configuration<C,4>& mc,const vector<int>& ind);
#endif
#if defined(HIGH_PRECISION) && defined(VERY_HIGH_PRECISION)
template momentumD_configuration<CVHP,4> extend_daniel<CHP,CVHP>(const momentumD_configuration<CHP,4>& mc,const vector<int>& ind);
#endif
#ifdef HIGH_PRECISION
template momentumD_configuration<CHP,4> extend_real<C,CHP,4>(const momentumD_configuration<C,4>& mc,const vector<int>& ind);
#endif
#ifdef VERY_HIGH_PRECISION
template momentumD_configuration<CVHP,4> extend_real<C,CVHP,4>(const momentumD_configuration<C,4>& mc,const vector<int>& ind);
#endif
#if defined(HIGH_PRECISION) && defined(VERY_HIGH_PRECISION)
template momentumD_configuration<CVHP,4> extend_real<CHP,CVHP,4>(const momentumD_configuration<CHP,4>& mc,const vector<int>& ind);
#endif

#if INSTANTIATE_GMP
template momentumD_configuration<CGMP,4> extend_real<CGMP,CGMP,4>(const momentumD_configuration<CGMP,4>& mc,const vector<int>& ind);
template momentumD_configuration<CGMP,4> extend_real<C,CGMP,4>(const momentumD_configuration<C,4>& mc,const vector<int>& ind);
#endif

}
}

