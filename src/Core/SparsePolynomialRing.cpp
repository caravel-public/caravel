#include "SparsePolynomialRing.h"

namespace  Caravel {

#ifdef USE_FINITE_FIELDS
template struct SparsePolynomialRing<F32>;
#endif // USE_FINITE_FIELDS

} //  Caravel
