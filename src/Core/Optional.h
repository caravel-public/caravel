#pragma once

#if __cplusplus <  201703L
/**
 * Bad implementation of std::optional. Whenever we upgrade to c++17,
 * then this can be removed.
 */
#ifndef OPTIONAL_H_INC
#define OPTIONAL_H_INC
#include <exception>
template<typename T>
class optional{
  T val;
  bool empty;

  public:
  optional( const T& _val ) : val(_val), empty(false) {}
  optional() : empty(true) {}

  operator bool() const{return !empty;}
  optional& operator =(const T& _val){
    empty = false;
    val = _val;

    return *this;
  }

  T value() const{
    if (empty){throw std::runtime_error("Bad optional access.");}
    return val;
  }

};

template<typename T>
std::ostream& operator<<(std::ostream& s, const optional<T>& o){
  if (!o){s << "Empty";}
  else{s << o.value();}
  return s;
}
#endif

#else

///FIXME: this is left here for backwards compatibility, should be removed later
#pragma message "Deprecated header Optional.h is included! std::optional is available instead."
#include <optional>
using std::optional;

#endif
