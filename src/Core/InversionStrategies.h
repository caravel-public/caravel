#ifndef INVERSION_STRATEGIES_H_INC
#define INVERSION_STRATEGIES_H_INC

#include "Core/typedefs.h"
#include "Core/type_traits_extra.h"
#include <functional>
#include <vector>
#include <algorithm>

namespace Caravel {


// NOTE: Naive fourier transform. Will overcompute...
template <typename T> T fourier_coefficient(std::function<T(T)> f, int coeff_i, int max_power);

/**
 * Default strategy interface for solving linear function systems for high precision numerics.
 * Currently PLU decomposition with MPACK
 */
/**
 * Default strategy interface for solving linear function systems for
 * double precision numerics. Currently QR decomposition with LAPACK.
 */

template <typename T> class Vec;

template <typename T, typename X>
std::enable_if_t<is_floating_point<T>::value, std::vector<Vec<T>>> get_coeffs_default_strategy(std::function<Vec<T>(X)> function_eval,
                                                                                                  std::function<std::vector<T>(X)> basis_eval, int basis_dim,
                                                                                                  std::function<X()> point_gen);

/**
 * Default strategy interface for solving linear function systems for finite field numerics.
 * Currently in house implemented PLU. Works for both vector valued
 * functions and scalar valued functions, i.e. "TF" can be either
 * vector<F> or just F.
 */
template <typename TF, typename F, typename X>
std::enable_if_t<is_exact<F>::value, std::vector<TF>>
get_coeffs_default_strategy(std::function<TF(X)> function_eval, std::function<std::vector<F>(X)> basis_eval, int basis_dim, std::function<X()> point_gen);


/**
 * PLU solver interface for exact types to solve linear equations of the form
 * 
 * \f[
 *  A \cdot \vec{x} = \vec{b}
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$\vec{b}\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T, typename TF = T> std::enable_if_t<is_exact<T>::value, std::vector<TF>> plu_solver(std::vector<T>&& A, std::vector<TF>&& b);

/**
 * PLU solver interface for double-precision floating-point types (RHS is a vector).
 * 
 * At configuration time it can be chosen whether `Eigen` or `Lapack` is used to perform the inversion 
 * of the system.
 * 
 * \f[
 *  A \cdot \vec{x} = \vec{b}
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$\vec{b}\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T>
std::enable_if_t<!is_exact<T>::value && (std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<T>>
plu_solver(std::vector<T>&& A, std::vector<T>&& b);

/**
 * PLU solver interface for high-precision floating-point types (RHS is a vector).
 * 
 * For high-precision types the inversion can only be performed using `Eigen`.
 * 
 * \f[
 *  A \cdot \vec{x} = \vec{b}
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$\vec{b}\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T>
std::enable_if_t<!is_exact<T>::value && !(std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<T>>
plu_solver(std::vector<T>&& A, std::vector<T>&& b);

/**
 * PLU solver interface for double-precision floating-point types (RHS is a matrix).
 * 
 * At configuration time it can be chosen whether `Eigen` or `Lapack` is used to perform the inversion 
 * of the system.
 * 
 * \f[
 *  A \cdot \vec{x} = b
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$b\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T>
std::enable_if_t<!is_exact<T>::value && (std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<Vec<T>>>
plu_solver(std::vector<T>&& A, std::vector<Vec<T>>&& b);

/**
 * PLU solver interface for high-precision floating-point types (RHS is a matrix).
 * 
 * For high-precision types the inversion can only be performed using `Eigen`.
 * 
 * \f[
 *  A \cdot \vec{x} = b
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$b\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T>
std::enable_if_t<!is_exact<T>::value && !(std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<Vec<T>>>
plu_solver(std::vector<T>&& A, std::vector<Vec<T>>&& b);

/**
 * QR solver interface for double-precision floating-point types (RHS is a vector).
 * 
 * At configuration time it can be chosen whether `Eigen` or `Lapack` is used to perform the inversion 
 * of the system.
 * 
 * \f[
 *  A \cdot \vec{x} = \vec{b}
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$\vec{b}\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T>
std::enable_if_t<!is_exact<T>::value && (std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<T>>
qr_solver(std::vector<T>&& A, std::vector<T>&& b);

/**
 * QR solver interface for high-precision floating-point types (RHS is a matrix).
 * 
 * For high-precision types the inversion can only be performed using `Eigen`.
 * 
 * \f[
 *  A \cdot \vec{x} = \vec{b}
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$\vec{b}\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T>
std::enable_if_t<!is_exact<T>::value && !(std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<T>>
qr_solver(std::vector<T>&& A, std::vector<T>&& b);

/**
 * QR solver interface for double-precision floating-point types (RHS is a matrix).
 * 
 * At configuration time it can be chosen whether `Eigen` or `Lapack` is used to perform the inversion 
 * of the system.
 * 
 * \f[
 *  A \cdot \vec{x} = b
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$b\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T>
std::enable_if_t<!is_exact<T>::value && (std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<Vec<T>>>
qr_solver(std::vector<T>&& A, std::vector<Vec<T>>&& b);

/**
 * QR solver interface for high-precision floating-point types (RHS is a matrix).
 * 
 * For high-precision types the inversion can only be performed using `Eigen`.
 * 
 * \f[
 *  A \cdot \vec{x} = b
 * \f]
 * 
 * NOTE: To have a common interface between Lapack and Eigen, the vector contains the columns of the 
 *       matrix attached one after the other. In case Lapack should be dropped in the future it might
 *       be more intuitive to switch to a major-row representation, where the rows of the matrix are
 *       concatenated in the vector. The implementation contains the necessary code already commented 
 *       out, such that the transition should be easy to make if needed.
 * 
 * @param A Contains a flattened version of the matrix \f$A\f$.
 * @param b Contains the right-hand side \f$b\f$ of the equation.
 * @return  The solution \f$\vec{x}\f$ to the linear equation.
 */
template <typename T>
std::enable_if_t<!is_exact<T>::value && !(std::is_same<T, R>::value || std::is_same<T, C>::value), std::vector<Vec<T>>>
qr_solver(std::vector<T>&& A, std::vector<Vec<T>>&& b);

} // namespace Caravel

#include "InversionStrategies.hpp" // Implementations

#endif // INVERSION_STRATEGIES_H_INC
