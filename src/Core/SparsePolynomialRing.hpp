#pragma once


namespace Caravel {

template <typename Tcoeffs, typename Tvars> void SparsePolynomialRing<Tcoeffs, Tvars>::set_variables(std::shared_ptr<std::vector<Tvars>> vars_in) {
    variables_ptr = vars_in;
    hash_variables = std::hash<std::vector<Tvars>>{}(*variables_ptr);
    for (auto& [mi, ci] : ring_element) {
        for (auto& [vari, pi] : mi) {
            if (vari >= variables_ptr->size()) throw std::invalid_argument("SparsePolynomialRing received too few variables!");
        }
    }
}

template <typename Tcoeffs, typename Tvars>
SparsePolynomialRing<Tcoeffs, Tvars>::SparsePolynomialRing(const std::unordered_map<monomial_t, Tcoeffs>& coefficients_in,
                                                           std::shared_ptr<std::vector<Tvars>> variables_in)
    : ring_element(coefficients_in) {

    auto sc_p = ring_element.find(monomial_t{});

    if (sc_p != ring_element.end()) {
        std::swap(scalar, sc_p->second);
        ring_element.erase(sc_p);
    }

    set_variables(variables_in);
}

template <typename Tcoeffs, typename Tvars> SparsePolynomialRing<Tcoeffs, Tvars>::SparsePolynomialRing(const MathList& input) {
    assert_head(input, "SparsePolynomialRing");

    for (MathList m_pair : input) {
        assert_head(m_pair, "Prod");

        Tcoeffs coeff;

        if constexpr (std::is_constructible_v<Tcoeffs, MathList>) { coeff = Tcoeffs{m_pair[1]}; }
        else {
            using ::Caravel::operator>>;
            std::istringstream s(m_pair[1]);
            s >> coeff;
            if (s.fail()) { throw std::runtime_error(m_pair[1] + " cannot be interpreted as " + _typeid(Tcoeffs)); }
        }

        monomial_t mono;

        for (MathList vari : MathList(m_pair[0], "mono")) {
            assert_head(vari, "x");
            size_t var_ind = std::stol(vari[0]);
            if (mono.count(var_ind) > 0) { throw std::invalid_argument("Duplicated entries for a variable in SparsePolynomialRing in monomial " + m_pair[0]); }
            mono.emplace(var_ind, std::stoi(vari[1]));
        }
        ring_element.emplace(std::move(mono), std::move(coeff));
    }

    auto sc_p = ring_element.find(monomial_t{});
    if (sc_p != ring_element.end()) {
        std::swap(scalar, sc_p->second);
        ring_element.erase(sc_p);
    }
}

template <typename Tcoeffs, typename Tvars> template <typename F> auto SparsePolynomialRing<Tcoeffs, Tvars>::map_over_coefficients(F&& f) const {
    using Tout = std::remove_cv_t<decltype(f(std::declval<Tcoeffs>()))>;

    SparsePolynomialRing<Tout, Tvars> res;

    res.scalar = f(scalar);
    std::transform(ring_element.cbegin(), ring_element.cend(), std::inserter(res.ring_element, res.ring_element.begin()),
                   [f = std::move(f)](const auto& it) { return std::make_pair(it.first, f(it.second)); });

    res.drop_zeroes();

    return res;
}

template <typename Tcoeffs, typename Tvars>
template <typename Tvn>
auto SparsePolynomialRing<Tcoeffs, Tvars>::operator()(const std::vector<Tvn>& var_values) const {
    if (var_values.size() != variables_ptr->size()) throw std::invalid_argument("Number of values for variables doesn't match the number of variables");
    Tvn result = scalar;

    for (auto& [mono, coeff] : ring_element) {
        Tvn mc = coeff;
        for (auto& [vari, pi] : mono) { mc *= prod_pow(var_values.at(vari), pi); }

        result += mc;
    }

    return result;
}

template <typename Tcoeffs, typename Tvars>
bool same_variables_Q(const SparsePolynomialRing<Tcoeffs, Tvars>& p1, const SparsePolynomialRing<Tcoeffs, Tvars>& p2) {
    // since we do not own variables, they should have the same raw pointer
    if (p1.variables_ptr != p2.variables_ptr) return false;

    if (p1.variables_ptr and p2.variables_ptr) {
        if (p1.variables_ptr->size() != p2.variables_ptr->size()) return false;
        //(*p1.variables_ptr) == (*p2.variables_ptr);
        return p1.hash_variables == p2.hash_variables;
    }

    return true;
}

template <typename Tcoeffs, typename Tvars> SparsePolynomialRing<Tcoeffs, Tvars> operator-(SparsePolynomialRing<Tcoeffs, Tvars> p) {
    p.scalar = -p.scalar;
    for (auto& [mi, ci] : p.ring_element) { ci = -ci; }
    return p;
}

template <typename Tcoeffs, typename Tvars>
SparsePolynomialRing<Tcoeffs, Tvars> operator+(SparsePolynomialRing<Tcoeffs, Tvars> p1, const SparsePolynomialRing<Tcoeffs, Tvars>& p2) {
    return p1 += p2;
}
template <typename Tcoeffs, typename Tvars>
SparsePolynomialRing<Tcoeffs, Tvars> operator-(SparsePolynomialRing<Tcoeffs, Tvars> p1, const SparsePolynomialRing<Tcoeffs, Tvars>& p2) {
    return p1 -= p2;
}


template <typename Tcoeffs, typename Tvars> std::string math_string(const SparsePolynomialRing<Tcoeffs, Tvars>& re) {
    if (re.ring_element.empty()) return math_string(re.scalar);
    using monomial_t = typename SparsePolynomialRing<Tcoeffs, Tvars>::monomial_t;

    std::map<monomial_t, Tcoeffs> ordered_elements(re.ring_element.begin(), re.ring_element.end());

    std::stringstream s;

    bool firstm = true;
    if (!re.is_zero_coeff(re.scalar)) {
        s << math_string(re.scalar);
        firstm = false;
    }

    for (auto& [mono, coeff] : ordered_elements) {

        if (!firstm) { s << "  +  "; }

        s << math_string(coeff);
        s << "*(";
        bool first = true;
        for (auto& [vari, power] : mono) {
            if (!first) { s << "*"; }
            first = false;
            if (re.variables_ptr) { s << (re.variables_ptr->at(vari)); }
            else {
                s << "x" << std::to_string(vari);
            }
            if (power > 1) { s << "^" << std::to_string(power); }
        }
        s << ")";
        firstm = false;
    }

    return s.str();
}

template <typename Tcoeffs, typename Tvars> std::ostream& operator<<(std::ostream& s, const SparsePolynomialRing<Tcoeffs, Tvars>& re) {
    return s << math_string(re);
}

template <typename Tcoeffs, typename Tvars> bool operator==(const SparsePolynomialRing<Tcoeffs, Tvars>& p1, const SparsePolynomialRing<Tcoeffs, Tvars>& p2) {

    if (p1.scalar != p2.scalar) return false;

    if (!same_variables_Q(p1, p2)) return false;

    if (p1.ring_element != p2.ring_element) return false;

    return true;
}

template <typename Tcoeffs, typename Tvars> void SparsePolynomialRing<Tcoeffs, Tvars>::set_to_zero() {
    scalar = Tcoeffs{};
    ring_element.clear();
}

template <typename Tcoeffs, typename Tvars> SparsePolynomialRing<Tcoeffs, Tvars>& SparsePolynomialRing<Tcoeffs, Tvars>::operator*=(const Tcoeffs& x) {
    if (is_zero_coeff(x)) { set_to_zero(); }
    else {
        scalar *= x;
        for (auto& it : ring_element) { it.second *= x; }
    }
    return *this;
}

template <typename Tcoeffs, typename Tvars> SparsePolynomialRing<Tcoeffs, Tvars>& SparsePolynomialRing<Tcoeffs, Tvars>::operator/=(const Tcoeffs& x) {
    if constexpr (op_valid_v<Tcoeffs&, Tcoeffs&, std::divides<Tcoeffs>>) {
        if (is_zero_coeff(x)) { throw Caravel::DivisionByZeroException{}; }
        else {
            scalar /= x;
            for (auto& it : ring_element) { it.second /= x; }
        }
        return *this;
    }
    else {
        throw std::invalid_argument("Coefficients of type " + _typeid(Tcoeffs) + " cannot be devided!");
    }
}

template <typename Tcoeffs, typename Tvars> SparsePolynomialRing<Tcoeffs, Tvars>& SparsePolynomialRing<Tcoeffs, Tvars>::operator+=(const Tcoeffs& x) {
    scalar += x;
    return *this;
}

template <typename Tcoeffs, typename Tvars> SparsePolynomialRing<Tcoeffs, Tvars>& SparsePolynomialRing<Tcoeffs, Tvars>::operator-=(const Tcoeffs& x) {
    scalar -= x;
    return *this;
}

template <typename Tcoeffs, typename Tvars> bool SparsePolynomialRing<Tcoeffs, Tvars>::is_zero_coeff(const Tcoeffs& x) { return x == Tcoeffs{}; }

template <typename Tcoeffs, typename Tvars> void SparsePolynomialRing<Tcoeffs, Tvars>::drop_zeroes() {
    for (auto it = ring_element.begin(); it != ring_element.end();) {
        if (it->first != monomial_t{} and is_zero_coeff(it->second)) { it = ring_element.erase(it); }
        else {
            ++it;
        }
    }
}

template <typename Tcoeffs, typename Tvars>
SparsePolynomialRing<Tcoeffs, Tvars>& SparsePolynomialRing<Tcoeffs, Tvars>::operator+=(const SparsePolynomialRing<Tcoeffs, Tvars>& other) {

    if (!same_variables_Q(*this, other)) throw std::invalid_argument("Attempted to sum SparsePolynomialRings with different variables");

    scalar += other.scalar;
    for (auto& [m, c] : other.ring_element) { ring_element[m] += c; }
    drop_zeroes();
    return *this;
}

template <typename Tcoeffs, typename Tvars>
SparsePolynomialRing<Tcoeffs, Tvars>& SparsePolynomialRing<Tcoeffs, Tvars>::operator-=(const SparsePolynomialRing<Tcoeffs, Tvars>& other) {

    if (!same_variables_Q(*this, other)) throw std::invalid_argument("Attempted to sum SparsePolynomialRings with different variables");

    scalar -= other.scalar;
    for (auto& [m, c] : other.ring_element) { ring_element[m] -= c; }
    drop_zeroes();
    return *this;
}

template <typename Tcoeffs, typename Tvars>
SparsePolynomialRing<Tcoeffs, Tvars>& SparsePolynomialRing<Tcoeffs, Tvars>::operator*=(const SparsePolynomialRing<Tcoeffs, Tvars>& other) {
    if (!same_variables_Q(*this, other)) throw std::invalid_argument("Attempted to multiply SparsePolynomialRings with different variables");

    SparsePolynomialRing<Tcoeffs, Tvars> leftXscalar = *this;
    leftXscalar *= other.scalar;
    leftXscalar.scalar = Tcoeffs{};

    SparsePolynomialRing<Tcoeffs, Tvars> rightXscalar = other;
    rightXscalar *= scalar;
    rightXscalar.scalar = Tcoeffs{};

    scalar *= other.scalar;

    decltype(ring_element) new_ring_element{};

    for (const auto& [mL, cL] : ring_element) {
        for (auto [mR, cR] : other.ring_element) {
            auto mRR = mR;
            for (auto& [mi, p] : mL) mRR[mi] += p;
            new_ring_element[std::move(mRR)] += std::move((cR *= cL));
        }
    }

    ring_element.swap(new_ring_element);

    this->operator+=(leftXscalar);
    this->operator+=(rightXscalar);

    drop_zeroes();
    return *this;
}

template <typename Tcoeffs, typename Tvars>
SparsePolynomialRing<Tcoeffs, Tvars> operator*(SparsePolynomialRing<Tcoeffs, Tvars> p1, const SparsePolynomialRing<Tcoeffs, Tvars>& p2) {
    return p1 *= p2;
}

template <typename Tcoeffs, typename Tvars> SparsePolynomialRing<Tcoeffs, Tvars> operator*(SparsePolynomialRing<Tcoeffs, Tvars> p1, const Tcoeffs& x) {
    return p1 *= x;
}

template <typename Tcoeffs, typename Tvars> SparsePolynomialRing<Tcoeffs, Tvars> operator*(const Tcoeffs& x, SparsePolynomialRing<Tcoeffs, Tvars> p1) {
    return p1 *= x;
}

template <typename Tcoeffs, typename Tvars> std::set<typename SparsePolynomialRing<Tcoeffs,Tvars>::monomial_t> SparsePolynomialRing<Tcoeffs, Tvars>::get_vector_space() const {

    std::set<monomial_t> monomials;

    for (auto& [mi, ci] : ring_element) { monomials.insert(mi); }

    if(!is_zero_coeff(scalar)){
        monomials.insert(monomial_t{});
    }

    return monomials;
}

template <typename Tcoeffs, typename Tvars>
Vec<Tcoeffs> SparsePolynomialRing<Tcoeffs, Tvars>::to_vector_space(const std::set<typename SparsePolynomialRing<Tcoeffs,Tvars>::monomial_t>& vector_space) const {

    Vec<Tcoeffs> toreturn(vector_space.size());

    if(!is_zero_coeff(scalar)) {
        if (vector_space.count(monomial_t{})==0) {
            throw std::invalid_argument("Ring unit is not in the requested vector space, but its coefficient is not zero.");
        }
        toreturn[0] = scalar;
    }

    // Here we figure out at what position to insert each monomial and check if the vector space actually covers all of them.
    // Elements of Vec are zero-initialized, so if we do not assign anything they'll stay zero.
    for (auto& [mi, ci] : ring_element) {

        auto it = vector_space.find(mi);

        if (it == vector_space.end()) {
            throw std::invalid_argument("Monomial "+math_string(mi)+" is not in the requested vector space, but its coefficient is not zero.");
        }

        size_t index = std::distance(vector_space.begin(),it);

        toreturn[index] = ci;
    }

    return toreturn;
}

#ifdef USE_FINITE_FIELDS 
extern template struct SparsePolynomialRing<F32>;
#endif // USE_FINITE_FIELDS


} // namespace Caravel
