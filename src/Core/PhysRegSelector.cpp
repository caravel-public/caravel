#include "PhysRegSelector.h"

namespace Caravel {

size_t PhysRegSelector::n = 0;
std::vector<size_t> PhysRegSelector::initial = {};
std::vector<size_t> PhysRegSelector::out = {};

size_t PhysRegSelector::get_n() { return n; }
const std::vector<size_t>& PhysRegSelector::get_initial() { return initial; }
const std::vector<size_t>& PhysRegSelector::get_final() {
    if(n==0)
        std::cout<<"WARNING: calling PhysRegSelector::get_final() when a 0-pt configuration defined!"<<std::endl;
    return out; 
}
void PhysRegSelector::set_region(size_t nin, const std::vector<size_t>& vin) {
    n=nin;
    // sanity check
    for(const auto& i:vin){
        if(i==0){
            std::cerr<<"ERROR: PhysRegSelector::set_region(.) received '0' momentum (should be 1-based): "<<nin<<" "<<vin<<std::endl;
            std::exit(1);
        }
        else if(i>n){
            std::cerr<<"ERROR: PhysRegSelector::set_region(.) received momentum greater than maximum: "<<nin<<" "<<vin<<std::endl;
            std::exit(1);
        }
    }
    initial=vin;
    out.clear();
    for(size_t ii=1;ii<=n;ii++){
        if(std::find(initial.begin(),initial.end(),ii)==initial.end())
            out.push_back(ii);
    }
    std::sort(initial.begin(),initial.end());
    std::sort(out.begin(),out.end());
    std::cout<<n<<"-point physical phase space defined as "<<initial<<" ---> "<<out<<std::endl;
}

} // namespace Caravel
