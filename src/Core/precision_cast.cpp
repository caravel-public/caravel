#include <complex>
#include "precision_cast.h"
#include <sstream>
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif
#include "Core/typedefs.h"

#ifdef USE_FINITE_FIELDS
#include "Core/BigRat.h"
#endif

#if INSTANTIATE_GMP
#include <gmp_r.h>
#endif


namespace Caravel{

#ifdef USE_FINITE_FIELDS
template <typename T> T to_precision(const BigRat& a){
    static_assert(is_floating_point<T>::value,"This function is supposed to be used to cast to floating point");
    // cast to strings when no native interface is availible
    std::stringstream num,den;
    num << a.num();
    den << a.den();
    return T(num.str().c_str())/T(den.str().c_str());
}

template <typename T> T to_precision(const std::complex<BigRat>& a){
    static_assert(is_complex_v<T>);
    using TR = typename T::value_type;
    return {to_precision<TR>(a.real()),to_precision<TR>(a.imag())};
}


template <> double to_precision(const BigRat& a){ return (double)a; }

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
template dd_real to_precision(const BigRat& a);
template qd_real to_precision(const BigRat& a);
#endif

template <> C to_precision(const BigRat& a){ return (double)a; }

#ifdef HIGH_PRECISION
template <> CHP to_precision(const BigRat& a){ return CHP(to_precision<RHP>(a),0); }
#endif
#ifdef VERY_HIGH_PRECISION
template <> CVHP to_precision(const BigRat& a){ return CVHP(to_precision<RVHP>(a),0); }
#endif

template C to_precision(const std::complex<BigRat>&);
#ifdef HIGH_PRECISION
template CHP to_precision(const std::complex<BigRat>&);
#endif
#ifdef VERY_HIGH_PRECISION
template CVHP to_precision(const std::complex<BigRat>&);
#endif

#if INSTANTIATE_GMP
template RGMP to_precision(const BigRat& a);
template CGMP to_precision(const std::complex<BigRat>& a);
template <> CGMP to_precision(const BigRat& a){ return CGMP(to_precision<RGMP>(a),0); }
#endif

#endif


}
