#pragma once

#include "Core/CaravelException.h"
#include "Core/MathInterface.h"
#include "Core/MathOutput.h"
#include "Core/Vec.h"
#include "Debug.h"

#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>
#include <memory>

namespace Caravel {
template <typename Tcoeffs, typename Tvars = std::string> struct SparsePolynomialRing {
    // {{index of var (0-based),power}..}
    using monomial_t = std::map<size_t, unsigned>;
    // TODO: exponents should be BigInt to prevent potential overflow, but need hash
    // for them because of unordered_map

    Tcoeffs scalar{};
    std::unordered_map<monomial_t, Tcoeffs> ring_element = {};

    std::shared_ptr<std::vector<Tvars>> variables_ptr;

    SparsePolynomialRing<Tcoeffs, Tvars>& operator+=(const Tcoeffs& x);
    SparsePolynomialRing<Tcoeffs, Tvars>& operator-=(const Tcoeffs& x);
    SparsePolynomialRing<Tcoeffs, Tvars>& operator*=(const Tcoeffs& x);
    SparsePolynomialRing<Tcoeffs, Tvars>& operator/=(const Tcoeffs& x);

    SparsePolynomialRing<Tcoeffs, Tvars>& operator+=(const SparsePolynomialRing<Tcoeffs, Tvars>& x);
    SparsePolynomialRing<Tcoeffs, Tvars>& operator-=(const SparsePolynomialRing<Tcoeffs, Tvars>& x);
    SparsePolynomialRing<Tcoeffs, Tvars>& operator*=(const SparsePolynomialRing<Tcoeffs, Tvars>& x);

    /**
     * Default constructor makes a 0-valued element
     */
    SparsePolynomialRing() = default;
    SparsePolynomialRing(const SparsePolynomialRing&) = default;
    SparsePolynomialRing& operator=(const SparsePolynomialRing&) = default;
    SparsePolynomialRing(SparsePolynomialRing&&) = default;
    SparsePolynomialRing& operator=(SparsePolynomialRing&&) = default;
    /**
     * Constructor makes an element with non-zero scalar
     */
    SparsePolynomialRing(const Tcoeffs& sc_in) : scalar(sc_in) {}
    /**
     * Construct from explicit list of coefficients and variables
     */
    SparsePolynomialRing(const std::unordered_map<monomial_t, Tcoeffs>& coefficients_in, std::shared_ptr<std::vector<Tvars>> variables_in);
    /**
     * MathList interface.
     *
     * Variables are not given in this interface explicitly.
     * If needed should be assigned manually.
     *
     * Format:
     * SparsePolynomialRing[Prod[mono[x[index,power]..], coeff]..]
     */
    SparsePolynomialRing(const MathList& input);

    /**
     * Evaluate by substituting values of variables
     */
    template <typename Tvn> auto operator()(const std::vector<Tvn>& var_values) const;

    /**
     * Return a new SparsePolynomialRing with coefficients map with a function f. The type is determined from the function output type.
     */
    template <typename F> auto map_over_coefficients(F&& f) const;
    /**
     * Return a list of monomials which (together with element 1)
     * can span a vector space sufficient to cover current ring element.
     */
    std::set<monomial_t> get_vector_space() const;
    /**
     * Return vector of coefficients of the given vector space of monomials.
     *
     * Entries of the vector that are not in the current ring element will be set to zero.
     *
     * If the current ring element cannot be covered by the given vector space, an exception is thrown.
     */
    Vec<Tcoeffs> to_vector_space(const std::set<monomial_t>& vector_space) const;
    /**
     * Define what ring variables are. In principle this is mandatory only to get a nice output or to make sure not to combine rings
     * in different variables.
     */
    void set_variables(std::shared_ptr<std::vector<Tvars>>);

    void drop_zeroes();

    static bool is_zero_coeff(const Tcoeffs& x);
  private:
    void set_to_zero();
    uint64_t hash_variables{0};
    template <typename tc, typename tv> friend bool same_variables_Q(const SparsePolynomialRing<tc, tv>&, const SparsePolynomialRing<tc, tv>&);
};

template <typename Tcoeffs, typename Tvars> bool operator==(const SparsePolynomialRing<Tcoeffs, Tvars>&, const SparsePolynomialRing<Tcoeffs, Tvars>&);
template <typename Tcoeffs, typename Tvars> bool operator!=(const SparsePolynomialRing<Tcoeffs, Tvars>& p1, const SparsePolynomialRing<Tcoeffs, Tvars>& p2) {
    return !(p1 == p2);
}

template <typename Tcoeffs, typename Tvars> std::string math_string(const SparsePolynomialRing<Tcoeffs, Tvars>&);
template <typename Tcoeffs, typename Tvars> std::ostream& operator<<(std::ostream& s, const SparsePolynomialRing<Tcoeffs, Tvars>& re);

} // namespace Caravel

#include "SparsePolynomialRing.hpp"
