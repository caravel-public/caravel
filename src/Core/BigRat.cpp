#include "BigRat.h"
#include "misc/MiscMath.h"
#include "Utilities.h"

#include <iostream>

namespace Caravel{

  BigRat pow(const BigRat& x, int n) { return prod_pow(x, n); }
  std::istream& operator>>(std::istream& i, BigRat& a) { return (i >> a.val); }
  std::ostream& operator<<(std::ostream& o, const BigRat& a) { return (o << a.val); }

  BigInt BigRat::num() const {return BigInt(val.get_num());}

  BigInt BigRat::den() const {return BigInt(val.get_den());}

}

// specialize hasher
namespace std{
size_t hash<Caravel::BigRat>::operator()(const Caravel::BigRat& xs) const {
    size_t result = 2;
    Caravel::hash_combine(result, xs.num());
    Caravel::hash_combine(result, xs.den());
    return result;
}
}
