/*!\file typedefs.h
 * \brief Includes typedefs for common number types used in Caravel
 *
 * Typedefs for:
 *
 * 1) floating point types: RHP, CHP, RVHP and CVHP,
 *
 * 2) the finite field type: F32. 
 *
 * It's considered good practice to include this header 
 * file whenever a related instantiation is employed
*/
#pragma once

#include <complex>

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif

#include "Core/BigInt.h"
#include "Core/BigRat.h"
#include "Core/Barrett32.h"


namespace Caravel {

//! double precision real
typedef double R;
//! high precision real (2 x double)
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
typedef dd_real RHP;
//! higher precision real (4 x double)
typedef qd_real RVHP;
#endif
//! double precision complex
typedef std::complex<R> C;
//! high precision complex (2 x double)
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
typedef std::complex<RHP> CHP;
//! higher precision complex (4 x double)
typedef std::complex<RVHP> CVHP;
#endif

/**
 * A typedef for finite field type
 */
using F32 = Barrett32;

}

#include "Core/gmp_r.h"
