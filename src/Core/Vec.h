#pragma once

#include <algorithm>
#include <cassert>
#include <functional>
#include <ostream>
#include <vector>

#include "Core/MathOutput.h"
#include "Core/Debug.h"
#include "Core/type_traits_extra.h"

#include "Core/typedefs.h"

namespace Caravel {

/**
 * Simple vector class. Should implement all standard field
 * operations in a vector space.
 */
template <class T> class Vec {
  private:
    std::vector<T> components{T{}};

  public:
    /**
     * Default constructor will construct a one-dimensional zero vector.
     */
    Vec() = default;
    Vec(const Vec&) = default;
    Vec(Vec&&) = default;
    Vec& operator=(const Vec&) = default;
    Vec& operator=(Vec&&) = default;
    /**
     * This constructor creates a zero Vec of n (the argument) dimensions
     * TODO: it makes sense for this to be a constructor from size_t, but we keep it
     *       for now as int (with an assert) to avoid a conflict with QD types
     */
    Vec(int n) : components(static_cast<size_t>(n), T{}) {}
    Vec(size_t n) : components(n, T{}) {}
    Vec(const std::vector<T>& other) : components(other) {}
    Vec(std::vector<T>&& other) : components(std::move(other)) {}

    const auto& get_components() const { return components; }

    T& operator[](size_t i) { return components.at(i); }
    const T& operator[](size_t i) const { return components.at(i); }

    Vec<T>& operator+=(const Vec<T>& other) {
        const size_t n = components.size();
        for (size_t i = 0; i < n; ++i) { components[i] += other[i]; }
        return *this;
    }

    Vec<T>& operator-=(const Vec<T>& other) {
        const size_t n = components.size();
        for (size_t i = 0; i < n; ++i) { components[i] -= other[i]; }
        return *this;
    }

    Vec<T>& operator*=(const T& lam) {
        for (auto& it : components) { it *= lam; }
        return *this;
    }

    Vec<T>& operator/=(const T& lam) {
        if constexpr (op_valid_v<T&, T&, std::divides<T>>) {
            auto ilam = T(1) / lam;
            for (auto& it : components) { it *= ilam; }
            return *this;
        }
        else {
            throw std::invalid_argument("Tried to use operator/ for "+_typeid(T)+" which is not implemented!");
        }
    }

    size_t size() const { return components.size(); }
    typedef typename std::vector<T>::iterator iterator;
    typedef typename std::vector<T>::value_type value_type;
    auto begin() { return components.begin(); }
    auto end() { return components.end(); }
    auto begin() const { return components.begin(); }
    auto end() const { return components.end(); }
    auto cbegin() const { return components.cbegin(); }
    auto cend() const { return components.cend(); }
};

template <class T> Vec<T> operator+(Vec<T> v1, const Vec<T>& v2) { return v1 += v2; }
template <class T> Vec<T> operator-(Vec<T> v1, const Vec<T>& v2) { return v1 -= v2; }

template <class T> Vec<T> operator*(const T& lam, Vec<T> v) { return v *= lam; }
template <class T> Vec<T> operator*(Vec<T> v, const T& lam) { return v *= lam; }
template <class T> Vec<T> operator-(Vec<T> v) { return v *= T(-1); }
template <class T> Vec<T> operator/(Vec<T> v, const T& lam) { return v /= lam; }

template <class T> bool operator==(const Vec<T>& v1, const Vec<T>& v2) {
    if (v1.size() != v2.size()) return false;
    for (size_t i = 0; i < v1.size(); i++) {
        if (!(v1[i] == v2[i])) { return false; }
    }
    return true;
}

template <class T> bool operator!=(const Vec<T>& v1, const Vec<T>& v2) { return !(v1 == v2); }

template <class T> std::ostream& operator<<(std::ostream& s, const Vec<T>& v) {
    s << math_list_with_head("Vec",v.get_components());
    return s;
}

#define _INSTANTIATE_VEC_GEN__(ext,T)\
ext template class Vec<T>; \
ext template Vec<T> operator+(Vec<T> v1, const Vec<T>& v2); \
ext template Vec<T> operator-(Vec<T> v1, const Vec<T>& v2); \
ext template Vec<T> operator*(const T& lam, Vec<T> v); \
ext template Vec<T> operator*(Vec<T> v, const T& lam); \
ext template Vec<T> operator-(Vec<T> v); \
ext template Vec<T> operator/(Vec<T> v, const T& lam); \
ext template bool operator==(const Vec<T>& v1, const Vec<T>& v2);\
ext template bool operator!=(const Vec<T>& v1, const Vec<T>& v2);\
ext template std::ostream& operator<<(std::ostream& s, const Vec<T>& v) ;\

_INSTANTIATE_VEC_GEN__(extern,R)
#ifdef HIGH_PRECISION 
_INSTANTIATE_VEC_GEN__(extern,RHP)
_INSTANTIATE_VEC_GEN__(extern,CHP)
#endif // HIGH_PRECISION
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_VEC_GEN__(extern, RVHP)
_INSTANTIATE_VEC_GEN__(extern, CVHP)
#endif // VERY_HIGH_PRECISION
#ifdef USE_FINITE_FIELDS
_INSTANTIATE_VEC_GEN__(extern, F32)
#endif // USE_FINITE_FIELDS

} // namespace Caravel
