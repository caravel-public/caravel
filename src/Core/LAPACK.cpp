/*
 * LAPACK.cpp
 * First created on 19.1.2017
 *
 * Simple interface for functionalities needed from LAPACK and MPACK for Caravel linear regressions
 *
*/

#include <algorithm>
#include <stdexcept>

#include "LAPACK.h"

#ifdef USE_LAPACK
void ZGETRF(int* dim1, int* dim2, Caravel::C* a, int* lda, int* ipiv, int* info){
	// forward order to LAPACK
	zgetrf_(dim1,dim2,a,lda,ipiv,info);
}

void ZGETRS(char *TRANS, int *N, int *NRHS, Caravel::C *A, int *LDA, int *IPIV, Caravel::C *B, int *LDB, int *INFO ){
	// forward order to LAPACK
	zgetrs_(TRANS,N,NRHS,A,LDA,IPIV,B,LDB,INFO );
}

void ZGELS(char *TRANS, int *M, int *N, int *NRHS, Caravel::C *A, int *LDA, Caravel::C *B, int *LDB, Caravel::C* WORK, int* LWORK, int *INFO ){
    // forward order to LAPACK
    zgels_(TRANS, M, N, NRHS, A, LDA, B, LDB, WORK, LWORK, INFO);
}

void ZGETRF(int* dim1, int* dim2, Caravel::R*  a, int* lda, int* ipiv, int* info){
	// forward order to LAPACK
	dgetrf_(dim1,dim2,a,lda,ipiv,info);
}

void ZGETRS(char *TRANS, int *N, int *NRHS, Caravel::R* A, int *LDA, int *IPIV, Caravel::R* B, int *LDB, int *INFO ){
	// forward order to LAPACK
	dgetrs_(TRANS,N,NRHS,A,LDA,IPIV,B,LDB,INFO );
}

void ZGELS(char *TRANS, int *M, int *N, int *NRHS, Caravel::R* A, int *LDA, Caravel::R* B, int *LDB, Caravel::R*  WORK, int* LWORK, int *INFO ){
    // forward order to LAPACK
    dgels_(TRANS, M, N, NRHS, A, LDA, B, LDB, WORK, LWORK, INFO);
}
#else
void ZGETRF(int* dim1, int* dim2, Caravel::C* a, int* lda, int* ipiv, int* info){
	throw std::runtime_error("ZGETRF not implemented for C class!!");
}
void ZGETRS(char *TRANS, int *N, int *NRHS, Caravel::C *A, int *LDA, int *IPIV, Caravel::C *B, int *LDB, int *INFO ){
	throw std::runtime_error("ZGETRS not implemented for C class!!");
}
void ZGELS(char *TRANS, int *M, int *N, int *NRHS, Caravel::C *A, int *LDA, Caravel::C *B, int *LDB, Caravel::C *WORK, int *LWORK, int *INFO ){
	throw std::runtime_error("ZGELS not implemented for C class!!");
}
#endif

#if INSTANTIATE_GMP
void ZGETRF(int* dim1, int* dim2, Caravel::CGMP* a, int* lda, int* ipiv, int* info){
	throw std::runtime_error("ZGETRF not implemented for CGMP class!!");
}
void ZGETRS(char *TRANS, int *N, int *NRHS, Caravel::CGMP *A, int *LDA, int *IPIV, Caravel::CGMP *B, int *LDB, int *INFO ){
	throw std::runtime_error("ZGETRS not implemented for CGMP class!!");
}
void ZGELS(char *TRANS, int *M, int *N, int *NRHS, Caravel::CGMP *A, int *LDA, Caravel::CGMP *B, int *LDB, Caravel::CGMP *WORK, int *LWORK, int *INFO ){
	throw std::runtime_error("ZGELS not implemented for CGMP class!!");
}
#endif

