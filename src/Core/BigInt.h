#pragma once

#include <stdint.h>
#include <gmpxx.h>

#if defined HIGH_PRECISION || defined VERY_HIGH_PRECISION
#include <qd/qd_real.h>
#endif


namespace Caravel{

  struct BigRat;

  /**
   * Wrapper type for GMP BigInt, offering explicit conversion operators.
   */
struct BigInt {

protected:
    /**
     * Internal representation of value. 
     */
    mpz_class val;

public:

    BigInt() : val(0) {}
    BigInt(const mpz_class& i) : val(i) {}
    explicit BigInt(const char* i)  : val(i) { }
    BigInt(const int32_t i)  :  val(i) { }
    BigInt(const uint32_t i)  : val(i) { }
    BigInt(const uint64_t i)  : val(0) { 
        // GMP doesn't support 64 bit integers, so we hack our way
        // around.
        uint32_t top_bits = i >> 32;
        uint32_t bottom_bits = i - (uint64_t(top_bits) << 32);
        val = top_bits;
        val = val << 32; 
        val += bottom_bits;
    }
    // Not supported by GMP, so delgate to unsigned constructor
    BigInt(const int64_t i) : BigInt(uint64_t((i>0) ? i : -i)){
       if (i<0){val = -val;}
    }

  explicit operator uint32_t() const	{ uint64_t tmp(val.get_si()); return tmp; }
  explicit operator int() const	{ int64_t tmp(val.get_si()); return tmp; }
  explicit operator uint64_t() const	{ uint64_t tmp(val.get_si()); return tmp; }
  explicit operator int64_t() const	{ int64_t tmp(val.get_si()); return tmp; }
  explicit operator std::string() const	{ return val.get_str(); }

  explicit operator double() const { return val.get_d(); }
  // This is safe but inefficient in principle
  // TODO: more efficient conversion?
#if defined HIGH_PRECISION || defined VERY_HIGH_PRECISION
  explicit operator dd_real() const { return val.get_str().c_str(); };
  explicit operator qd_real() const { return val.get_str().c_str(); };
#endif

    BigInt& operator=( const BigInt& e) {
        val = e.val;
        return *this;
    }


    bool operator<=(const BigInt& e) const{ return val <= e.val; }
    bool operator<(const BigInt& e) const{ return val < e.val; }
    bool operator>=(const BigInt& e) const{ return val >= e.val; }
    bool operator>(const BigInt& e) const{ return val > e.val; }

    bool operator==(const BigInt& e) const{ return val == e.val; }

    bool operator!=(const BigInt& e) const{
        return !this->operator==(e);
    }

    inline BigInt& operator*= (const BigInt& e) {
      val *= e.val;
      return *this;
    }

   inline BigInt& operator/= (const BigInt& e) {
     val /= e.val;
     return *this;
   }

   inline BigInt& operator%= (const BigInt& e) {
     val %= e.val;
     return *this;
   }

    inline BigInt& operator+= (const BigInt& e) {
      val += e.val;
      return *this;
    }

    inline BigInt& operator-= (const BigInt& e) {
      val -= e.val;
      return *this;
    }

    inline const BigInt operator* (const BigInt& e) const {
      BigInt tmp = (*this);
      tmp *= e;
      return tmp;
    }

    inline const BigInt operator/ (const BigInt& e) const {
      BigInt tmp = (*this);
      tmp /= e;
      return tmp;
    }

    inline const BigInt operator% (const BigInt& e) const {
      BigInt tmp = (*this);
      tmp %= e;
      return tmp;
    }

    inline const BigInt operator+ (const BigInt& e) const {
      BigInt tmp = (*this);
      tmp += e;
      return tmp;
    }

    inline const BigInt operator- (const BigInt& e) const {
      BigInt tmp = (*this);
      tmp -= e;
      return tmp;
    }

    inline const BigInt operator- () const {
      BigInt tmp;
      tmp.val = -val;
      return tmp;
    }

  friend std::istream& operator>> (std::istream& i, BigInt& a);
  friend std::ostream& operator<< (std::ostream& o, const BigInt& a);
  friend std::hash<Caravel::BigInt>;

  friend BigRat;
  friend BigInt abs(const BigInt& x);

};


}

// specialize hasher
namespace std {

template <> struct hash<mpz_srcptr> { size_t operator()(const mpz_srcptr x) const; };

template <> struct hash<mpz_class> {
    size_t operator()(const mpz_class& x) const { return hash<mpz_srcptr>{}(x.get_mpz_t()); }
};

template <> struct hash<Caravel::BigInt> {
    size_t operator()(const Caravel::BigInt& xs) const { return hash<mpz_class>{}(xs.val); }
};
} // namespace std
