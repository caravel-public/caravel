/*
 * extend.h
 * Added on 22.6.2016
 *
 * Inherited from V1's extend.cpp from Daniel, David, Fernando and Harald
 *
 */

#include "momD_conf.h"


namespace Caravel {

namespace _extend_impl{
    template <class T_low,class T_high,size_t D>
        momentumD_configuration<T_high,D> extend_real(const momentumD_configuration<T_low,D>&,const std::vector<int>&);

    template <class T_low,class T_high>
        momentumD_configuration<T_high,4> extend_daniel(const momentumD_configuration<T_low,4>&,const std::vector<int>&);
}

// More convenient interface to extend_real. Call as
// change_precision<T_New>(momentumD_configuration<T_Old,D>)
// T is assumed to be complex
template<typename T_new, typename T_old, size_t D>
momentumD_configuration<T_new,D> change_precision(const momentumD_configuration<T_old, D>& mom_conf){
    static_assert(!std::is_same<T_new,T_old>::value,"It makes no sense to convert into the same type.");
    static_assert(is_complex<T_new>::value,"Only complex types are supported.");
    static_assert(is_complex<T_old>::value,"Only complex types are supported.");

    std::vector<int> indices;
    for (size_t i = 1; i <= mom_conf.n(); i++){indices.push_back(i);}

    return _extend_impl::extend_real<T_old,T_new,D>(mom_conf, indices);
}


}

