/*
 * Particle.cpp 
 *
 * First created on 25.11.2015
 *
 * Implementation of classes related to Particle
 *
*/

#include <iostream>
#include <algorithm>
#include <set>

#include "Particle.h"
#include "Debug.h"
#include "MathInterface.h"

namespace Caravel {

std::ostream& operator<<(std::ostream& s, const MassStruct& m) {
    if (m.n_mass_real == 0 && m.n_mass_imag == 0) {
        s << "M = 0 ";
        return s;
    }

    if (m.n_mass_real != 0) s << "real(M) = " << m.n_mass_real << "/" << m.d_mass_real << " ";
    if (m.n_mass_imag != 0) s << "imag(M) = " << m.n_mass_imag << "/" << m.d_mass_imag << " ";
    return s;
}

std::ostream& operator<<(std::ostream& s, const ParticleMass& p) {
    s << p.mass_value;
    s << "[mass index = " << p.mass_index << "]";
    return s;
}

bool operator==(const MassStruct& m1, const MassStruct& m2) {
    // this identity will be considered true IF all integer containers exactly match
    return m1.n_mass_real == m2.n_mass_real && m1.n_mass_imag == m2.n_mass_imag && m1.d_mass_real == m2.d_mass_real && m1.d_mass_imag == m2.d_mass_imag;
}

bool operator==(const ParticleMass& m1, const ParticleMass& m2) { return m1.mass_identity == m2.mass_identity && m1.mass_value == m2.mass_value; }

// massless particles get assigned zero mass_index_global
size_t ParticleMass::mass_index_global=size_t(0);

// by default it is an empty vector, and only it will grow for massive particles added
thread_local std::vector<ParticleMass> ParticleMass::particle_mass_container={};

ParticleMass::ParticleMass(): mass_identity(), mass_value(), mass_index(0) {}

size_t ParticleMass::get_corresponding_mass_index() const {
  auto& ident = this->mass_identity;

	// massless particle, assign zero entry
	if(ident.n_mass_real==0&&ident.n_mass_imag==0){
		return 0;
	}
	// check if mass already added
	for(size_t ii=1;ii<=particle_mass_container.size();ii++){
		auto& cont_ident = particle_mass_container[ii-1].mass_identity;
		if(cont_ident.n_mass_real==ident.n_mass_real&&
				cont_ident.n_mass_imag==ident.n_mass_imag&&
				cont_ident.d_mass_real==ident.d_mass_real&&
				cont_ident.d_mass_imag==ident.d_mass_imag){
			return ii;
		}
	}
	// new mass
	mass_index_global++;
	particle_mass_container.push_back(*this);
	// and fix its mass_index
	particle_mass_container.back().mass_index=mass_index_global;
	return mass_index_global;
}

ParticleMass::ParticleMass(int nr,int dr,int ni,int di): mass_identity(nr,dr,ni,di), mass_value(nr,dr,ni,di) {
	// check that no denominator was set to zero
	if(dr==0){
		std::cout<<"Denominator of the real part of ParticleMass set as zero, but this isn't accepted. Changed to 1! real(m)= "<<nr<<"/"<<1<<std::endl;
		mass_identity.d_mass_real=1;
		mass_value.d_mass_real=1;
	}
	if(di==0){
		std::cout<<"Denominator of the imaginary part of ParticleMass set as zero, but this isn't accepted. Changed to 1! imag(m)= "<<ni<<"/"<<1<<std::endl;
		mass_identity.d_mass_imag=1;
		mass_value.d_mass_imag=1;
	}
	// fix mass index:
	mass_index=this->get_corresponding_mass_index();
}

void ParticleMass::set_mass(int nr,int dr,int ni,int di){
	mass_identity.n_mass_real=nr;
	mass_identity.n_mass_imag=ni;
	mass_identity.d_mass_real=dr;
	mass_identity.d_mass_imag=di; 
	// check that no denominator was set to zero
	if(dr==0){
		std::cout<<"Denominator of the real part in ParticleMass::set_mass(.) set as zero, but this isn't accepted. Changed to 1! real(m)= "<<nr<<"/"<<1<<std::endl;
		mass_identity.d_mass_real=1;
	}
	if(di==0){
		std::cout<<"Denominator of the imaginary part in ParticleMass::set_mass(.) set as zero, but this isn't accepted. Changed to 1! imag(m)= "<<ni<<"/"<<1<<std::endl;
		mass_identity.d_mass_imag=1;
	}
	// fix mass index:
	mass_index=this->get_corresponding_mass_index();
}

helicity infer_helicity(std::string is){
    if(is=="m"||is=="qm"||is=="Qm"||is=="Qbm"||is=="hmm") return helicity::m; 
    else if(is=="p"||is=="qbp"||is=="Qp"||is=="Qbp"||is=="hpp") return helicity::p;
    else return helicity::undefined;
}

bool SingleState::is_external() const { return state != "default" and state != "NONMOD"; }

repr SingleState::get_representation() const {
    if (is_external()) {
        if (state == "qm")
            return particle::repr_of_first_spinor<16>(repr::La, dtt_index);
        else if (state == "qbp")
            return particle::repr_of_first_spinor<16>(repr::Lat, dtt_index);
        else
            return repr::undefined;
    } else {
        if (dtt_index == SingleState::rLa/* or state == "qm"*/)
            return repr::La;
        else if (dtt_index == SingleState::rLat/* or state == "qbp"*/)
            return repr::Lat;
        else
            return repr::undefined;
    }
}

void SingleState::set_dtt_index(repr r){
    switch(r){
        case repr::La: dtt_index = SingleState::rLa; break;
        case repr::Lat: dtt_index = SingleState::rLat; break;
        default: dtt_index = 0;
    }
}

std::ostream& operator<<(std::ostream& s,const SingleState& a) {
    s << a.get_name();
    if (a.get_dtt_index()!= 0) s <<":" << a.get_dtt_index();
    return s;
}

bool operator==(const SingleState& a, const SingleState& b) {
    return (a.get_name() == b.get_name()) && a.get_dtt_index() == b.get_dtt_index();
}
bool operator!=(const SingleState& a, const SingleState& b) {return !(a==b);}

bool operator<(const SingleState& a, const SingleState& b) {
    if (a.get_name() < b.get_name()) return true;
    if (a.get_name() == b.get_name() and a.get_dtt_index() < b.get_dtt_index()) return true;
    return false;
}



namespace detail {
namespace {
inline ParticleStatistics statistics_from_flavor(ParticleFlavor fl) {
    if (in_container<ParticleFlavor::q>(fl) or in_container<ParticleFlavor::l>(fl)) return ParticleStatistics::fermion;
    if (in_container<ParticleFlavor::qb>(fl) or in_container<ParticleFlavor::lb>(fl)) return ParticleStatistics::antifermion;

    switch (fl) {
        case ParticleFlavor::G:
        case ParticleFlavor::T: return ParticleStatistics::tensor;
        case ParticleFlavor::A: return ParticleStatistics::cubictensor;
        case ParticleFlavor::H: return ParticleStatistics::scalar;
        case ParticleFlavor::gluon:
        case ParticleFlavor::photon:
        case ParticleFlavor::Wm:
        case ParticleFlavor::Wp:
        case ParticleFlavor::Z: return ParticleStatistics::vector;
        default: break;
    }

    _WARNING_R("Can not guess statistics for flavor ", wise_enum::to_string(fl), "!");
    std::exit(1);

    return ParticleStatistics::undefined;
}

inline bool is_colored_guess(ParticleFlavor fl){
    if (fl==ParticleFlavor::gluon or in_container<ParticleFlavor::q>(fl) or in_container<ParticleFlavor::qb>(fl)) return true;
    return false;
}

inline ParticleFlavor recognize_flavor(std::string in) {
    auto try_get_flavor = wise_enum::from_string<ParticleFlavor>(in.c_str());
    if (try_get_flavor == std::optional<ParticleFlavor>{}) {
        _WARNING_R("Unrecognized particle flavor: ", in);
        std::exit(1);
    }
    ParticleFlavor fl = try_get_flavor.value();
    return fl;
}

} // namespace
} // namespace detail

  Particle::Particle(const MathList& list){

    if (list.head != "Particle"){
      throw std::invalid_argument("Expected head Particle, got " + list.head);
    }

    int mi = std::stoi(list[1]);
    std::string st = list[2];
    ParticleFlavor fl = detail::recognize_flavor(list[0]);

    name = std::string(wise_enum::to_string(fl)) + std::to_string(mi);
    type = ParticleType(detail::statistics_from_flavor(fl), fl, ParticleMass(), detail::is_colored_guess(fl));
    state = SingleState(st);

    // Check if external helicites make sense
    switch (type.get_statistics()) {
        case ParticleStatistics::fermion:
        case ParticleStatistics::antifermion:
            if (st == "qm" or st == "qbp")
                break;
            else
                goto unrecognized;
        case ParticleStatistics::vector:
            if (st == "m" or st == "p")
                break;
            else
                goto unrecognized;
        case ParticleStatistics::tensor:
            if (st == "hmm" or st == "hpp")
                break;
            else
                goto unrecognized;
        default:
        unrecognized:
            _WARNING_R("Unrecognized helicity state: ", st, " for flavor: ", fl);
            std::exit(1);
    }

    momindex = mi;
    refindex = 0;
  }

  Particle::Particle(std::string n, const ParticleType& tp, const SingleState& wf, size_t index, size_t rindex)
      : name(n), type(tp), state(wf), momindex(index), refindex(rindex) {
      DEBUG_MESSAGE("Constructed a Particle: ", n, " of type ", type, " with state: ", state);
      DEBUG_MESSAGE("	with momentum and reference: (", momindex, ",", refindex, ")");
}

void ParticleMass::show_all_masses() {
	std::cout<<"##########################################"<<std::endl;
	std::cout<<"Now will show all particle masses considered so far"<<std::endl;
	std::cout<<"##########################################"<<std::endl;
	std::cout<<ParticleMass()<<std::endl;
	for(size_t ii=0;ii<particle_mass_container.size();ii++)
		std::cout<<particle_mass_container[ii]<<std::endl;
	std::cout<<"##########################################"<<std::endl;
}

std::string Particle::get_name() const {
	return name;
}

std::string Particle::get_long_name() const {
	std::stringstream buffer;
	buffer<<*this;
	return buffer.str();
}

ParticleType Particle::get_type() const{
	return type;
}

ParticleType Particle::get_anti_type() const{
	return type.get_anti_type();
}

ParticleFlavor Particle::get_flavor() const {
	return type.get_flavor();
}

ParticleFlavor Particle::get_anti_flavor() const {
	return type.get_anti_flavor();
}

ParticleStatistics Particle::get_statistics() const {
	return type.get_statistics();
}

ParticleStatistics Particle::get_flipped_statistics() const {
	return type.get_flipped_statistics();
}

bool Particle::is_colored() const {
	return type.is_colored();
}

const SingleState& Particle::external_state() const {
	return state;
}

size_t Particle::mom_index() const {
	return momindex;
}

size_t Particle::ref_index() const {
	return refindex;
}

ParticleMass Particle::get_ParticleMass() const{
	return type.get_ParticleMass();
}

void Particle::set_mass(int nr,int dr,int ni,int di){
	return type.set_mass(nr,dr,ni,di);
}

bool Particle::is_massive() const {
	return type.is_massive();
}

bool Particle::is_massless() const {
	return type.is_massless();
}

size_t Particle::get_mass_index() const {
	return type.get_mass_index();
}

void Particle::show() const {
	std::cout<<"============"<<std::endl;
	std::cout<<"Particle with name "<<name<<" contains the following information:"<<std::endl;
	std::cout<<"	Type: "<<type<<std::endl;
	std::cout<<"	Associated state: "<<state<<std::endl;
	std::cout<<"	Momentum index and reference: "<<momindex<<" "<<refindex<<std::endl;
	std::cout<<"============"<<std::endl;
}

  Process::Process(const MathList& l) :
    Process([](const MathList& list){
              if (list.head != "Particles"){
                throw std::invalid_argument("Expected head Particles, got " + list.head);
              }
              std::vector<Particle*> particles;

              for (const auto& it : list) {
                particles.push_back(new Particle(MathList(it, "Particle")));
              }

              return particles;
    }(l)){
}

Process::Process(const std::vector<Particle*>& particles) {
    allp = particles;
    for(auto particle : allp) {
    	procstring+= particle ->get_name()+" ";
    }
    DEBUG_MESSAGE("Constructed a Process: ",procstring);
}

const std::vector<Particle*>& Process::get_process() const {
	return allp;
}

std::string Process::show_process() const {
	return procstring;
}

InternalIndexHandler::InternalIndexHandler(): handling_needed(false), internal_index_left(0), internal_index_right(0), internal_index_colored({}), internal_index_colorless({}) {
}

InternalIndexHandler::InternalIndexHandler(const Particle& pL,const Particle& pR,const std::vector<Particle*>& colored,const std::vector<Particle*>& colorless): InternalIndexHandler() {
	std::set<int> all;
	if(pL.get_internal_index()!=0){
		internal_index_left=pL.get_internal_index();
		all.insert(internal_index_left);
	}
	if(pR.get_internal_index()!=0){
		internal_index_right=pR.get_internal_index();
		all.insert(internal_index_right);
	}
	for(int ii=0;ii<int(colored.size());ii++){
		internal_index_colored.push_back(colored[ii]->get_internal_index());
		if(colored[ii]->get_internal_index()!=0){
			all.insert(internal_index_colored.back());
		}
	}
	for(int ii=0;ii<int(colorless.size());ii++){
		internal_index_colorless.push_back(colorless[ii]->get_internal_index());
		if(colorless[ii]->get_internal_index()!=0){
			all.insert(internal_index_colorless.back());
		}
	}
	// if we have non-trivial indices, but it is not a tree-level calculation, indices we track routing assignments
	if(all.size()>0 &&
		pL.get_name()!="NOPARTICLE" && pR.get_name()!="NOPARTICLE")
		handling_needed=true;
#if 0
std::cout<<"InternalIndexHandler received: pL: "<<pL.get_internal_index()<<" {";
for(int ii=0;ii<int(colored.size());ii++)
	std::cout<<colored[ii]->get_internal_index()<<" ";
std::cout<<"} {";
for(int ii=0;ii<int(colorless.size());ii++)
	std::cout<<colorless[ii]->get_internal_index()<<" ";
std::cout<<"} "<<pR.get_internal_index();
std::cout<<" "<<(handling_needed ? "true" : "false" )<<std::endl;
#endif
}

int InternalIndexHandler::get_internal_index_left() const { return internal_index_left; }
int InternalIndexHandler::get_internal_index_right() const { return internal_index_right; }
int InternalIndexHandler::get_colored_internal_index_in_vertex(size_t i) const { return internal_index_colored[i]; }
int InternalIndexHandler::get_colorless_internal_index_in_vertex(size_t i) const { return internal_index_colorless[i]; }
std::pair<std::vector<int>,std::vector<int>> InternalIndexHandler::get_all_internal_index() const { return std::make_pair(internal_index_colored,internal_index_colorless); }

bool InternalIndexHandler::needs_internal_index_handling() const { 
	return handling_needed;
}

std::string InternalIndexHandler::show_internal_indices() const {
	std::string toret("("+std::to_string(internal_index_left)+")");
	toret+="{";
	for(size_t ii=0;ii<internal_index_colored.size();ii++){
		toret+=std::to_string(internal_index_colored[ii]);
		if(ii+1!=internal_index_colored.size())
			toret+=",";
	}
	toret+="}";
	toret+="("+std::to_string(internal_index_right)+")";
	return toret;
}


// The string "NOPARTICLE" is employed to track unassigned left/right particles (tree cases)
ColorHandledProcess::ColorHandledProcess(const std::vector<Particle*>& v1,const std::vector<Particle*>& v2): 
		pleft("NOPARTICLE",ParticleType("notype")), pright("NOPARTICLE",ParticleType("notype")), colorless(v1), colored(v2) {
	std::sort(colorless.begin(),colorless.end());
	internal_indices=InternalIndexHandler(pleft,pright,colored,colorless);
}

// The string "NOPARTICLE" is employed to track unassigned left/right particles (tree cases)
ColorHandledProcess::ColorHandledProcess(Process& p1):  pleft("NOPARTICLE",ParticleType("notype")), pright("NOPARTICLE",ParticleType("notype")) {
	const std::vector<Particle *>& procs(p1.get_process());
	for(size_t ii=0;ii<procs.size();ii++){
		if(procs[ii]->is_colored()){
			colored.push_back(procs[ii]);
		}
		else{
			colorless.push_back(procs[ii]);
		}
	}
	std::sort(colorless.begin(),colorless.end());
	internal_indices=InternalIndexHandler(pleft,pright,colored,colorless);
}

ColorHandledProcess::ColorHandledProcess(const Particle& pL,const std::vector<Particle*>& procs,const Particle& pR): pleft(pL), pright(pR) {
	for(size_t ii=0;ii<procs.size();ii++){
		if(procs[ii]->is_colored()){
			colored.push_back(procs[ii]);
		}
		else{
			colorless.push_back(procs[ii]);
		}
	}
	std::sort(colorless.begin(),colorless.end());
	internal_indices=InternalIndexHandler(pleft,pright,colored,colorless);
}

const Particle& ColorHandledProcess::get_left() const{
	return pleft;
}

const Particle& ColorHandledProcess::get_right() const{
	return pright;
}

const std::vector<Particle *>& ColorHandledProcess::get_colorless() const{
	return colorless;
}

const std::vector<Particle *>& ColorHandledProcess::get_colored() const{
	return colored;
}

void ColorHandledProcess::particle_substitute(Particle* pold,Particle* pnew){
	if(pnew->is_colored())
		std::replace(colored.begin(),colored.end(),pold,pnew);
	else
		std::replace(colorless.begin(),colorless.end(),pold,pnew);
}

void ColorHandledProcess::set_left(Particle* p){
	if(p==nullptr)
		return;
	Particle* holder(dynamic_cast<Particle*>(p));
	if(holder!=nullptr)
		pleft=*holder;
	else
		std::cout<<"ERROR while executing ColorHandledProcess::set_left(.) -- Did nothing"<<std::endl;
}

void ColorHandledProcess::set_right(Particle* p){
	if(p==nullptr)
		return;
	Particle* holder(dynamic_cast<Particle*>(p));
	if(holder!=nullptr)
		pright=*holder;
	else
		std::cout<<"ERROR while executing ColorHandledProcess::set_right(.) -- Did nothing"<<std::endl;
}

std::vector<Particle*> ColorHandledProcess::get_colored(){
	return colored;
}

std::vector<Particle*> ColorHandledProcess::get_colorless(){
	return colorless;
}

int ColorHandledProcess::get_internal_index_left() const { return internal_indices.get_internal_index_left(); }
int ColorHandledProcess::get_internal_index_right() const { return internal_indices.get_internal_index_right(); }

int ColorHandledProcess::get_particle_internal_index_in_vertex(Particle* pp) const { 
    auto r_colorless = std::find(colorless.begin(),colorless.end(),pp);
    if(r_colorless!=colorless.end())
        return internal_indices.get_colorless_internal_index_in_vertex(r_colorless-colorless.begin());
    auto r_colored = std::find(colored.begin(),colored.end(),pp);
    if(r_colored!=colored.end())
        return internal_indices.get_colored_internal_index_in_vertex(r_colored-colored.begin());
    else{
        std::cout<<"ERROR: asked ColorHandledProcess::get_particle_internal_index_in_vertex(.) for particle not in *this! The particle: "<<*pp<<std::endl;
        exit(101);
    }
}

std::pair<std::vector<int>,std::vector<int>> ColorHandledProcess::get_all_internal_index() const { return internal_indices.get_all_internal_index(); }
bool ColorHandledProcess::needs_internal_index_handling() const { return internal_indices.needs_internal_index_handling(); }
std::string ColorHandledProcess::show_internal_indices() const { return internal_indices.show_internal_indices(); }

bool operator==(const ColorHandledProcess& p1,const ColorHandledProcess& p2){
	if(!(p1.pleft==p2.pleft))
		return false;
	if(!(p1.pright==p2.pright))
		return false;
	if(p1.colored!=p2.colored)
		return false;
	if(p1.colorless!=p2.colorless)
		return false;
	return true;
}

Particle particle_from_schematic(std::string schematic){
    MathList field(schematic, "Field");
    if (field[0] != "g"){
        std::cout << "ERROR, currently only set up for gluonic fields." << std::endl;
        exit(1);
    }

    auto name = field[1];
    auto mom_index = std::stoi(field[2]);

    return Particle(name,ParticleType("gluon"),SingleState("NOSTATE"),mom_index);

}


Particle add_helicity(Particle& p, std::string helicity){
    return Particle(p.get_name()+helicity,p.get_type(),SingleState(helicity),p.mom_index(),0);
}

std::ostream& operator<<(std::ostream& s, Particle p) {
    // s << p.get_type() << " ["<< p.get_name() <<"]";
    s << p.get_name() << "<" << p.get_flavor();
    if (p.get_internal_index() != 0) s << "|" << p.get_internal_index();
    s << ">[" << p.external_state() << "|" << p.external_state().get_helicity() << "]";
    s << "(" << p.mom_index() << ")";
    if (p.get_mass_index() != 0) s << "[m" << p.get_mass_index() << "]";
    return s;
}

std::ostream& operator<<(std::ostream& s,Process proc){
  s <<"[ "<<proc.show_process()<<" ]";
  return s;
}

void ProcessFactory::build_all_helicities(){
    all_helicity_particles.clear(); // Empty the current one.
    for (auto& particle : base_particles){
        std::vector<Particle> all_helicities;
        for (auto& helicity : possible_helicities){
            all_helicities.push_back(add_helicity(particle, helicity));
        }
        all_helicity_particles.push_back(all_helicities);
    }
}

  ProcessFactory::ProcessFactory(std::string schematic, std::vector<std::string> _possible_helicities) : possible_helicities(_possible_helicities){
    MathList info(schematic, "Process");
    if (info[0] != "QCD"){
        std::cout << "ERROR, currently only set up for QCD processes." << std::endl;
        exit(1);
    }

    MathList fields(info[1], "Fields");

    for (auto field : fields){
        base_particles.push_back(particle_from_schematic(field));
    }

    build_all_helicities();
}

Process ProcessFactory::build_helicity_configuration(std::vector<std::string> helicities){
    std::vector<Particle*> particle_pointers;

    for (size_t i = 0; i < helicities.size(); i++){
       if (helicities.at(i) == possible_helicities.at(0)){
           particle_pointers.push_back(&all_helicity_particles.at(i).at(0));
       }
       if (helicities.at(i) == possible_helicities.at(1)){
           particle_pointers.push_back(&all_helicity_particles.at(i).at(1));
       }
    }
    auto new_config = Process(particle_pointers);
    return new_config;
}

Process ProcessFactory::get_process(std::vector<std::string> helicities){
    std::string hash_key;
    for (auto& helicity : helicities){hash_key += helicity;}

    auto maybe_process = helicity_processes.find(hash_key);
    if (maybe_process == helicity_processes.end()){
        // Build a new one, store it and then return the process
        auto new_config = build_helicity_configuration(helicities);
        helicity_processes[hash_key] = new_config;
        return helicity_processes[hash_key];
    }
    else{
        // It exists.
        return (*maybe_process).second;
    }
}

bool operator==(const Particle& p1,const Particle& p2){
	if(p1.name!=p2.name)
		return false;
	if(p1.type!=p2.type)
		return false;
	if(p1.state!=p2.state)
		return false;
	if(p1.momindex!=p2.momindex)
		return false;
	return true;
}

}
