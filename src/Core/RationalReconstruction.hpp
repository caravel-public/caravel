#include "Debug.h"
#include "misc/Moduli.hpp"
#include "settings.h"

namespace Caravel{
namespace RationalReconstruction{

    template <typename ItContainer, typename OutContainer, typename InTypeRational, typename InTypePrime, typename GuessType>
    void rational_reconstruct(const InTypeRational& rational_x, std::function<ItContainer(const InTypePrime&)> modulo_f, OutContainer& out, GuessType guesser) {

        using std::vector;

        using F = typename ItContainer::value_type;
        using T = typename OutContainer::value_type;

        static_assert(std::is_same<typename InTypePrime::value_type, F>::value, "InTypePrime must be a container of finite fields");
        static_assert(std::is_same<typename InTypeRational::value_type, BigRat>::value, "InTypeRational must be a container of rationals.");
        static_assert(std::is_same<T, BigRat>::value, "OutContainer must be a container of rationals.");

        auto original_cardinality = cardinality_update_manager<F>::get_instance()->get_current_cardinality();

        BigInt composite_modulus(1);
        vector<BigInt> composite_modulus_result;

        for (size_t modulus_i = 0; modulus_i < settings::general::max_moduli.value; modulus_i++) {
            auto modulus = moduli.at(modulus_i);
            cardinality_update_manager<F>::get_instance()->set_cardinality(modulus);

            InTypePrime modulo_x{rational_x};

            decltype(modulo_f(modulo_x)) modulo_result;

            try {
                modulo_result = modulo_f(modulo_x);
            } catch (const Caravel::DivisionByZeroException&) {
                _WARNING("Caught DivisionByZeroException while evaluating mod ", modulus, "! Trying next mod...");
                continue;
            }

            std::function<BigInt(BigInt, F)> chinese_remainder_composite_modulus = [composite_modulus](BigInt x, F y) {
                return chinese_remainder({x, composite_modulus}, y);
            };

            _MESSAGE("Finished computation mod ", modulus, " (", modulus_i+1,"/", settings::general::max_moduli.value,")");

            // Chinese remainder the corresponding coefficients in corresponding polynomials
            for (size_t i = 0; i < modulo_result.size(); i++) {
                F yi = modulo_result[i];

                // If this is the first time, we just accept the calculation
                if (i >= composite_modulus_result.size()) {
                    composite_modulus_result.emplace_back(yi);
                    continue;
                }

                composite_modulus_result[i] = chinese_remainder_composite_modulus(composite_modulus_result[i], yi);
            }

            composite_modulus *= modulus;

            // Lift to rational result, and see if it remains constant
            std::function<BigRat(BigInt)> rational_guesser = [composite_modulus,guesser](const BigInt& x) { return guesser({x, composite_modulus}); };

            bool unchanged = true;

            for (size_t i = 0; i < composite_modulus_result.size(); i++) {
                auto result = rational_guesser(composite_modulus_result.at(i));

                if (i >= out.size()) {
                    unchanged = false;
                    out.push_back(result);
                    continue;
                }

                auto& current_rational_f = out.at(i);
                if (result != current_rational_f) { unchanged = false; }
                current_rational_f = result;
            }
            if (unchanged) {
                _MESSAGE("Required ", modulus_i + 1, " finite fields for rational reconstruction.");
                cardinality_update_manager<F>::get_instance()->set_cardinality(original_cardinality);
                return;
            }
        }

        _WARNING("WARNING: Rational reconstruction has failed to terminate after trying ", settings::general::max_moduli, " finite fields.");
        cardinality_update_manager<F>::get_instance()->set_cardinality(original_cardinality);
        return;
    }

    template <template <typename...> class OutContainer, typename InTypeRational, typename InTypePrime>
    std::vector<OutContainer<BigRat>>
    rational_reconstruct(const InTypeRational& rational_x, std::function<std::vector<OutContainer<F32>>(const InTypePrime&)> modulo_f) {

         using F = F32;
	using std::vector;

	static_assert(std::is_same<typename InTypePrime::value_type, F>::value, "InTypePrime must be a container of finite fields");
	static_assert(std::is_same<typename InTypeRational::value_type, BigRat>::value, "InTypeRational must be a container of rationals.");

	BigInt composite_modulus(1);
	vector<OutContainer<BigInt>> composite_modulus_result;
	vector<OutContainer<BigRat>> current_rational_guess;

        auto original_cardinality = cardinality_update_manager<F>::get_instance()->get_current_cardinality();

	for (size_t modulus_i = 0; modulus_i < settings::general::max_moduli.value; modulus_i++){
	    auto modulus = moduli.at(modulus_i);
            cardinality_update_manager<F>::get_instance()->set_cardinality(modulus);

	    InTypePrime modulo_x(rational_x);

            decltype(modulo_f(modulo_x)) modulo_result;

            try {
                modulo_result = modulo_f(modulo_x);
            } catch (const Caravel::DivisionByZeroException&) {
                _WARNING("Caught DivisionByZeroException while evaluating mod ", modulus, "! Trying next mod...");
                continue;
            }

	    std::function<BigInt(BigInt,F)> chinese_remainder_composite_modulus
		= [composite_modulus](BigInt x, F y){
		return chinese_remainder({x, composite_modulus}, y);
	    };

            _MESSAGE("Finished computation mod ", modulus, " (", modulus_i+1,"/", settings::general::max_moduli.value,")");

	    // Chinese remainder the corresponding coefficients in corresponding polynomials
	    for (size_t i = 0; i < modulo_result.size(); i++){
		auto& yi = modulo_result.at(i);

		// If this is the first time, we just accept the calculation
		if (i >= composite_modulus_result.size()){
		composite_modulus_result.push_back(OutContainer<BigInt>(yi));
		continue;
		}

		auto& xi = composite_modulus_result.at(i);

		for (size_t j = 0; j < xi.numerator.coefficients.size(); j++){
		    auto coeff = chinese_remainder_composite_modulus(xi.numerator.coefficients.at(j),
								     yi.numerator.coefficients.at(j));
		    xi.numerator.coefficients.at(j) = coeff;
		}

		for (size_t j = 0; j < xi.denominator.coefficients.size(); j++){
		    auto coeff = chinese_remainder_composite_modulus(xi.denominator.coefficients.at(j),
								     yi.denominator.coefficients.at(j));
		    xi.denominator.coefficients.at(j) = coeff;
		}
	    }

	    composite_modulus *= modulus;

	    // Lift to rational result, and see if it remains constant
	    std::function<BigRat(BigInt)> rational_guesser
		= [composite_modulus](const BigInt& x){
        auto residue = std::make_pair(x, composite_modulus);
		return rational_guess(residue);
	    };

	    bool unchanged = true;

	    for (size_t i = 0; i < composite_modulus_result.size(); i++){
		auto& modulo_f = composite_modulus_result.at(i);

		auto num = fmap(rational_guesser, modulo_f.numerator);
		auto denom = fmap(rational_guesser, modulo_f.denominator);

		OutContainer<BigRat> result(num, denom);

		if (i >= current_rational_guess.size()){
		unchanged = false;
		current_rational_guess.push_back(result);
		continue;
		}

		auto& current_rational_f = current_rational_guess.at(i);
		if (result != current_rational_f){unchanged = false;}
		current_rational_f = result;
	    }
	    if (unchanged){
	    std::cout << "Required " << modulus_i+1 << " finite fields for rational reconstruction." << std::endl;


            cardinality_update_manager<F>::get_instance()->set_cardinality(original_cardinality);
	    return current_rational_guess;
	    }
	}

        _WARNING("WARNING: Rational reconstruction has failed to terminate after trying ", settings::general::max_moduli, " finite fields.");
        cardinality_update_manager<F>::get_instance()->set_cardinality(original_cardinality);
	return current_rational_guess;
    }

} // namespace RationalReconstruction
} // namespace Caravel
