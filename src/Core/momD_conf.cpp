/*
 * momD_conf.cpp
 * Added in 9.3.2016
 *
 * Inherited from V1's mom_conf.cpp from Daniel
 *
 * Implementation of class momD_conf
 *
 */

#include "momD_conf.h"
#include "CaravelException.h"
#include <cstdarg>
#include <stdio.h>
#include <string.h>
#if INSTANTIATE_GMP
#include "gmp_r.h"
#endif

using std::cout;
using std::endl;
using std::string;
using std::vector;

namespace Caravel {

// EXPLICIT INSTANTIATIONS
_INSTANTIATE_MOMDCONF(R)
#ifdef HIGH_PRECISION
_INSTANTIATE_MOMDCONF(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_MOMDCONF(RVHP)
#endif
_INSTANTIATE_MOMDCONF(C)
#ifdef HIGH_PRECISION
_INSTANTIATE_MOMDCONF(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
_INSTANTIATE_MOMDCONF(CVHP)
#endif
#ifdef INSTANTIATE_GMP
_INSTANTIATE_MOMDCONF(RGMP)
_INSTANTIATE_MOMDCONF(CGMP)
#endif
#ifdef USE_FINITE_FIELDS
_INSTANTIATE_MOMDCONF(F32)
#ifdef INSTANTIATE_RATIONAL
_INSTANTIATE_MOMDCONF(BigRat)
#endif
#endif

long int momentumD_configuration_base::momD_conf_next_ID = 0;

} // namespace Caravel
