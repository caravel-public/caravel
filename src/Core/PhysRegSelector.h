/**
 * @file PhysRegSelector.h
 *
 * @date 15.7.2019
 *
 * @brief Physical region selector.

 * A global container for information on physical regions of phase-space sampled. 
 * For now this is done globally through static functions, can be made local if 
 * required.
 */
#ifndef PHYSREGSELECTOR_H_
#define PHYSREGSELECTOR_H_

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include "Core/Utilities.h"

namespace Caravel {

/**
 * A singleton to store global information on physical regions. We could
 * promote to a general class in the future with different instances
 * to run parallel configurations if ever needed.
 */
class PhysRegSelector {
    static size_t n;    /**< Number of particles */
    static std::vector<size_t> initial;    /**< Particles in the initial state (1-based) */
    static std::vector<size_t> out;    /**< Particles in the final state (1-based) */
  protected:
    PhysRegSelector() = default;
  public:
    static size_t get_n();
    static const std::vector<size_t>& get_initial();
    static const std::vector<size_t>& get_final();
    /**
     * Sets the global physical region
     *
     * @param size_t representing the number of external particles
     * @param vector of indices specifying the initial-state particles
     */
    static void set_region(size_t, const std::vector<size_t>&);
};

} // namespace Caravel

#endif // PHYSREGSELECTOR_H_

