// Finite field class based upon Barrett reduction. 32bit storage type.

#pragma once

#include "Core/BigRat.h"
#include "Core/BigInt.h"
#include "Core/CaravelException.h"
#include "misc/MiscMath.h"

#include <iosfwd>

//static inline Residu_t minCardinality() { return 1073741823u; }
//Barrettt<uint64_t, uint64_t>::maxCardinality() { return 2147483647u; } // 2^31 - 1

namespace Caravel {

  /**
   * Implementation of finite field type based on 32bit integers and
   * barret reduction.
   */
struct Barrett32 {

protected:
    /**
     * Integer representative of finite field value.
     */
    uint64_t val;
    inline uint64_t breduce(const uint64_t& _a) const {
      // Barrett reduce
      uint64_t _b = _a >> s;
      uint64_t _c = (_b * _q) >> t;
      uint64_t _d = _a - _c * _p;
      _d = std::min(_d, _d - _p);
      _d = std::min(_d, _d - _p);
      return _d;
    }

    /**
     * Global representation of prime modulus. Unsafe to access it here, should
     * be accessed by set_p method.
     */
    inline static uint64_t _p = 2147483629;
    static const uint64_t s = 30;
    static const uint64_t t = 32;
  /**
   *  Approximate inverse of _p to be used in barret reduction.
   */
    inline static uint64_t _q = static_cast<uint64_t>((uint64_t(1) << ( s + t ))/_p);


public:

    Barrett32() : val(0) {}

    Barrett32(int64_t i) {
        // Both operands to mod must be signed or unsigned.
        i %= int64_t(_p);
        if (i < 0) { i += _p; }
        // Val is unsigned
        val = static_cast<uint64_t>(i);
    }
    Barrett32(const int32_t i) : Barrett32(int64_t(i)) {}  // no overflow can happen from the cast
    Barrett32(const uint32_t i) : Barrett32(int64_t(i)) {} // no overflow can happen from the cast

    Barrett32(uint64_t i)  : val(i%=_p) {} // can be only positive, so no need for check as for signed integer

    Barrett32(BigInt i) {
        i %= BigInt(_p); // we have to use the %= operator of BigInt, otherwise overflow can happen
        int64_t ii = static_cast<int64_t>(i);
        if (ii < 0) ii += _p;
        val = static_cast<uint64_t>(ii);
    }

    Barrett32(const BigRat& i) {
      Barrett32 a(i.num()), b(i.den());
      a/=b;
      val = a.val;
    }

    explicit operator uint32_t() const	{ return static_cast<uint32_t>(val); }
    explicit operator int() const	{ return static_cast<int>(val); }
    explicit operator uint64_t() const	{ return val; }
    explicit operator int64_t() const	{ return static_cast<int64_t>(val);  }
    explicit operator BigInt() const	{ return BigInt(val); }

    static void set_p(uint64_t p){

      // If p i not in the range (2^30, 2^31] then the constants s and t
      // are not correctly chosen, hence the algorithm fails.

      if ((p > (uint64_t(1) << ( 31 ))) || (p < (uint64_t(1) << ( 30)))){
         throw CaravelException("Prime out of Range");
      }

      _p = p;
      _q = static_cast<uint64_t>((uint64_t(1) << ( s + t ))/p);
    }

    static uint64_t get_p(){
      return _p;
    }

    Barrett32& operator=( const Barrett32& e) {
        val = e.val;
        return *this;
    }

    bool operator==(const Barrett32 e) const{ return val == e.val; }

    bool operator!=(const Barrett32 e) const {
        return val!=e.val;
    }

    inline Barrett32& operator*= (const Barrett32 e) {
      val = breduce(val*=e.val);
      return *this;
    }

    inline Barrett32& operator/=(const Barrett32& e) {
        if (e.val == 0) { throw DivisionByZeroException(); }

        // The extended Euclidean algorithm
        int64_t x_int = int64_t(_p);
        int64_t y_int = e.val;
        int64_t tx = 0;
        int64_t ty = 1;

        while (y_int != 0) {
            // always: gcd (modulus,residue) = gcd (x_int,y_int)
            //         sx*modulus + tx*residue = x_int
            //         sy*modulus + ty*residue = y_int
            int64_t q = x_int / y_int; // integer quotient

            int64_t temp = y_int;
            y_int = x_int - q * y_int;
            x_int = temp;

            temp = ty;
            ty = tx - q * ty;
            tx = temp;
        }

        if (tx < 0) tx += _p;

        val = breduce(val *= static_cast<uint64_t>(tx));

        return (*this);
    }

    inline Barrett32& operator+= (const Barrett32& e) {
      val += e.val;
      if (val >= _p) {val -= _p;}
      return *this;
    }

    inline Barrett32& operator-= (const Barrett32 e) {
      if (val < e.val){val += _p;}
      val -= e.val;
      return *this;
    }

    inline const Barrett32 operator- () const {
      if (val==0) return (*this);
      Barrett32 tmp;
      tmp.val = _p - val;
      return tmp;
    }

  friend std::istream& operator>> (std::istream& i, Barrett32& a);
  friend std::ostream& operator<< (std::ostream& o, const Barrett32& a);
};

// x^n computed via repeated x*x*x....x*x
Barrett32 pow(const Barrett32& x, int n);
// more efficient x^n for large n
Barrett32 mod_pow(const Barrett32& x, uint64_t);
// checks if x can be represented as x = t^2
bool is_quadratic_residue(const Barrett32& x);
// returns the minimal number t such that t^2 = x
Barrett32 mod_sqrt(const Barrett32& x);

inline Barrett32 operator*(Barrett32 a, const Barrett32 b) { return a *= b; }
inline Barrett32 operator/(Barrett32 a, const Barrett32 b) { return a /= b; }
inline Barrett32 operator+(Barrett32 a, const Barrett32 b) { return a += b; }
inline Barrett32 operator-(Barrett32 a, const Barrett32 b) { return a -= b; }

}

namespace std{
  template<>
  struct hash<Caravel::Barrett32>{
	  public:
    inline size_t operator() (const Caravel::Barrett32& x) const{
	    hash<uint64_t> hasher;
	    return hasher(uint64_t(x));
    }
  };
}

