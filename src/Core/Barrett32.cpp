#include "Barrett32.h"

#include <iostream>

namespace Caravel {

Barrett32 pow(const Barrett32& x, int n) { return prod_pow(x, n); }
std::istream& operator>>(std::istream& i, Barrett32& a) {
    BigRat temp;
    auto& s = (i >> temp);
    a = Barrett32(temp);
    return s;
}
std::ostream& operator<<(std::ostream& o, const Barrett32& a) { return (o << a.val); }

// x^n for large exponents n
Barrett32 mod_pow(const Barrett32& x, uint64_t n) {
    Barrett32 result(1);
    Barrett32 base(x);

    while (n > 0) {
        if (n % 2 == 1) result = result * base;
        n = n >> 1;
        base = base * base;
    }

    return result;
}

bool is_quadratic_residue(const Barrett32& x) {
    uint64_t p = x.get_p();

    if (mod_pow(x, (p - 1) / 2) == Barrett32(1))
        return true;
    else
        return false;
}

// Taken from https://rosettacode.org/wiki/Tonelli-Shanks_algorithm
Barrett32 mod_sqrt(const Barrett32& x) {
    if (!is_quadratic_residue(x)) throw NonQuadraticResidueException();

    uint64_t p = x.get_p();

    // 1) find (q,s) such that p-1 = q*2^s with q odd
    uint64_t q = p - 1;
    uint64_t s = 0;

    while (q % 2 == 0) {
        s++;
        q /= 2;
    }

    if (s == 1) return Barrett32(std::min(uint64_t(mod_pow(x, (p + 1) / 4)), uint64_t(mod_pow(-x, (p + 1) / 4))));

    // 2) select non-square z such that (z|p) = -1
    Barrett32 z(2);
    while (is_quadratic_residue(z)) z += Barrett32(1);

    // 3) Set initial values
    Barrett32 c = mod_pow(z, q);
    Barrett32 r = mod_pow(x, (q + 1) / 2);
    Barrett32 t = mod_pow(x, q);
    uint64_t m = s;

    // 4) Loop until solution is found
    while (true) {
        if (t == Barrett32(1)) return Barrett32(std::min(uint64_t(r), uint64_t(Barrett32(p) - r)));

        uint64_t i(0);
        Barrett32 t2 = t;
        while (t2 != Barrett32(1) && i < m) {
            t2 = t2 * t2;
            i++;
        }

        uint64_t e = m - i - 1;
        Barrett32 b = c;
        while (e > 0) {
            b = b * b;
            e--;
        }

        r = r * b;
        c = b * b;
        t = t * c;
        m = i;
    }
}
} // namespace Caravel
