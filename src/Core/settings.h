/**
 * @file settings.h
 *
 * @date 22.02.2018
 *
 * @brief Global settings
 *
 * Global settings are designed to implement run-time branching in the program (without recompilation),
 * which e.g. sets important constants (like precision thresholds, Ds and so on) or selects certain types of implementations when several are
 * available. Basically everything which user might potentially want to change without modifying the source code.
 *
 * It is important to set reasonable default values.
 *
 * Since settings are simply global variables, they can be easily used as such.
 *
 * Reading settings from file is implemented, see function read_settings_from_file().
 *
 */
#include <tuple>
#include <map>
#include <iostream>

#include <optional>
#include "wise_enum.h"

#ifndef SETTINGS_H_
#define SETTINGS_H_

namespace Caravel{ namespace settings{


//! If you need only yes/no use the following
enum class answer{ yes, no };

/// If you have something with multiple possibilities, create a enum class
/// like this one and specialize a read function or operator>> in settings.cpp
/// Using bool or int types for this purpose is not recommended.

template <typename T> struct setting{
    const std::string name; /**< Name of the setting (also used when readin from file) */
    T value; /**<  Stores current value of the setting */

    typedef T type; /**< Value type */

    /**
     * Construct passing the default value of the setting.
     * @param name
     * @param default value
     */
    setting(std::string&& set_name, const T& default_value) : name(std::move(set_name)),value(std::move(default_value)) {};
    setting() = delete; /// no construction without default
    setting(const setting&) = delete; /// no copy
    operator T() const{return value;}
};


// Create namespaces for logically distinct settings. Do not forget extern.


namespace general{
    WISE_ENUM_CLASS(cut_states, projective, states )

    // power counting assumed for building function space for integrands
    WISE_ENUM_CLASS(power_counting,
		renormalizable,		/**< Up to renormalizable power counting like needed for example for QCD */
		Higgs_EFT_plus_one,	/**< This is what's necessary for a HEFT calculation (mt->\infty) with a single external Higgs (renormalizable+1) */
		Gravity			/**< For gravity calculations. Equivalent to twice the renormalizable power counting */
    )

    WISE_ENUM_CLASS(IntegralBasis,
            master_surface,
            tensors,
            scattering_plane_tensors 	/**< Same as 'tensors', but all monomials with transverse variables are converted to surface terms 
						(therefore, only monomials with variables from the scattering plane remain as masters) */
    )

    WISE_ENUM_CLASS(MasterBasis,
            simple_tensors,
            PTW,
            pure,
            pure_finite,
            standard,
            alternative,
            pentagon_functions_new
    )

    WISE_ENUM_CLASS(OneLoopMasterBasis,
            GKM,		/**< Integrand parametrization equivalent to arXiv:0801.2237 */
            only_scalar,	/**< As 'GKM' but drops (mu^2)^n insertions in favor of surface terms */
            pure		/**< As 'only_scalar' but adds IBPs and pure integrands for (available) master integrands */
    )
}

namespace general{
    extern setting<cut_states> cut_propagator_numerators;
    extern setting<power_counting> power_counting_of_theory;
    extern setting<IntegralBasis> numerator_decomposition;
    extern setting<answer> do_n_eq_n_test;
    extern setting<int> Nf;
    extern setting<int> Nh;
    extern setting<MasterBasis> master_basis;
    extern setting<OneLoopMasterBasis> one_loop_master_basis;
    extern setting<size_t> max_moduli; /**< Maximal number of finite fields to try in rational reconstruction until giving up */
}


namespace integrals {

    /// Possible integral families at 1-loop
    WISE_ENUM_CLASS(family_types_1l,
        /// One-loop master integrals originally inherited from  BHlib_Massive
        integrals_BH,
        /// Master integrals expressed in terms of pentagon functions.
        pentagon_functions,
        /// Master integrals expressed in terms of Goncharov's.
        goncharovs,
        /// Master integrals expressed in terms of new pentagon functions.
        pentagon_functions_new
    )

    extern setting<family_types_1l> integral_family_1L;

    /// Possible integral families at 2-loop
    WISE_ENUM_CLASS(family_types_2l,
        /// Master integrals expressed in terms of pentagon functions.
        pentagon_functions,
        /// Master integrals expressed in terms of Goncharov's.
        goncharovs,
        /// Master integrals expressed in terms of new pentagon functions.
        pentagon_functions_new
    )

    extern setting<family_types_2l> integral_family_2L;

#ifdef WITH_PENTAGONS
    /**
     * Set the (relative) precision to which weight-4 pentagon functions
     * are evaluated through numerical 1-dimensional integration
     */
    extern setting<double> pentagon_functions_integration_precision;
    /**
     * The maximal number of subdivision of integration region
     */
    extern setting<size_t> pentagon_functions_max_subintervals;
#endif

}

namespace BG{
WISE_ENUM_CLASS(trI,
                none,    // do not normalize
                full_Ds, // normalize by 2^(Ds/2-2)
                two,     // normalize bt 2 (when only 6->4 trace is done for all Ds)
                automatic
)

WISE_ENUM_CLASS(trace_scheme,
        full,
        reduced,
        single_trace // normalize by one Ds-dependent tr[I] (to be use for single trace)
)

extern setting<trI> partial_trace_normalization;
extern thread_local setting<trace_scheme> partial_trace_scheme;
}

namespace IntegrandHierarchy{

WISE_ENUM_CLASS(norm_type,
    none,
    spinor_weight,
    tree,
    automatic
)

WISE_ENUM_CLASS(parallelize_topologies_options,
    never,
    warmup,
    always
)

extern setting<answer> optimize_warmup_for_cuts;
extern setting<parallelize_topologies_options> parallelize_topology_fitting;
extern setting<int> topology_fitting_max_nthreads;
extern setting<norm_type> amplitude_normalization;
extern setting<size_t> number_of_Ds_values;
extern setting<size_t> n_cached_D_values_warmup;  // number of D values which are cached when doing warmup runs

}

namespace integrals{
    extern setting<answer> include_incomplete_integrals;
}

namespace OneLoopIntegrals{
    extern setting<size_t> pentagon_mu2_insertion;
}

namespace AmpEng{

extern setting<answer> spurious_fact_check;

}

/**
 * Read settings from file
 * @param path to the file
 */
void read_settings_from_file(const std::string& filename="settings.dat");
void use_setting(const std::string&);

std::ostream& operator<<(std::ostream&, const Caravel::settings::answer&);
template <typename T> std::ostream& operator<<(std::ostream& o, const Caravel::settings::setting<T>& set){
    return o << "Setting "<<set.name<<": "<<set.value;
}

template <typename T> bool operator==(const settings::setting<T>& s, const T& v){return s.value == v;}
template <typename T> bool operator<=(const settings::setting<T>& s, const T& v){return s.value <= v;}
template <typename T> bool operator>=(const settings::setting<T>& s, const T& v){return s.value >= v;}
template <typename T> bool operator<(const settings::setting<T>& s, const T& v){return s.value < v;}
template <typename T> bool operator>(const settings::setting<T>& s, const T& v){return s.value > v;}

}}

namespace Caravel{

struct verbosity_settings_container {
    bool subtraction_engine{false}; /**< show evaluation progress from subtraction engine */
    bool fit_function_removing_zeros{false}; /**< show dropping zeroes during warmup */
    bool process_library_construction{false}; /**< show some info about constructed processes from process library */
    bool remainder_pole_check{false}; /**< show information about cancelling of poles in a remainder */
    bool report_settings{true}; /** < Show which settings are being used when they are set. */
    bool report_integral_basis{true}; /** < Print choice of integral basis. */

    void go_quiet(); /** < Disables most output */
    void show_all(); /**< enable all output */
    void silence_all(); /** disable all output */

    verbosity_settings_container();

    private:
        static bool instatiated;
};

//! struct managing verbosity
extern verbosity_settings_container verbosity_settings;
}

#endif
