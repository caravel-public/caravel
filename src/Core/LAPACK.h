#ifndef LAPACK_H_
#define LAPACK_H_

#include <complex>
#include "Core/typedefs.h"

#if INSTANTIATE_GMP
#include "gmp_r.h"
#endif

extern "C" void zgetrf_(int* dim1, int* dim2, std::complex<double>* a, int* lda, int* ipiv, int* info);
extern "C" void zgetrs_(char *TRANS, int *N, int *NRHS, std::complex<double> *A, int *LDA, int *IPIV, std::complex<double> *B, int *LDB, int *INFO );
extern "C" void zgels_(char *TRANS, int *M, int *N, int *NRHS, std::complex<double> *A, int *LDA, std::complex<double> *B, int *LDB, std::complex<double>* WORK, int* LWORK, int *INFO );

extern "C" void zgels_(char *TRANS, int *M, int *N, int *NRHS, std::complex<double> *A, int *LDA, std::complex<double> *B, int *LDB, std::complex<double> *WORK, int *LWORK, int *INFO );


extern "C" void dgetrf_(int* dim1, int* dim2, double* a, int* lda, int* ipiv, int* info);
extern "C" void dgetrs_(char *TRANS, int *N, int *NRHS, double *A, int *LDA, int *IPIV, double *B, int *LDB, int *INFO );
extern "C" void dgels_(char *TRANS, int *M, int *N, int *NRHS, double *A, int *LDA, double *B, int *LDB, double* WORK, int* LWORK, int *INFO );

extern "C" void dgels_(char *TRANS, int *M, int *N, int *NRHS, double *A, int *LDA, double *B, int *LDB, double *WORK, int *LWORK, int *INFO );


void ZGETRF(int* dim1, int* dim2, Caravel::C* a, int* lda, int* ipiv, int* info);
void ZGETRS(char *TRANS, int *N, int *NRHS, Caravel::C *A, int *LDA, int *IPIV, Caravel::C *B, int *LDB, int *INFO );
void ZGELS(char *TRANS, int *M, int *N, int *NRHS, Caravel::C *A, int *LDA, Caravel::C *B, int *LDB, Caravel::C *WORK, int *LWORK, int *INFO );

void ZGETRF(int* dim1, int* dim2, Caravel::R*  a, int* lda, int* ipiv, int* info);
void ZGETRS(char *TRANS, int *N, int *NRHS, Caravel::R* A, int *LDA, int *IPIV, Caravel::R* B, int *LDB, int *INFO );
void ZGELS(char *TRANS, int *M, int *N, int *NRHS, Caravel::R* A, int *LDA, Caravel::R* B, int *LDB, Caravel::R* WORK, int *LWORK, int *INFO );

#if INSTANTIATE_GMP
void ZGETRF(int* dim1, int* dim2, Caravel::CGMP* a, int* lda, int* ipiv, int* info);
void ZGETRS(char *TRANS, int *N, int *NRHS, Caravel::CGMP *A, int *LDA, int *IPIV, Caravel::CGMP *B, int *LDB, int *INFO );
void ZGELS(char *TRANS, int *M, int *N, int *NRHS, Caravel::CGMP *A, int *LDA, Caravel::CGMP *B, int *LDB, Caravel::CGMP *WORK, int *LWORK, int *INFO );
#endif

#endif	// LAPACK_H_
