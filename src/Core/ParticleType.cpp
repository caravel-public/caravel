/*
 * ParticleType.cpp 
 *
 * First created on 13.3.2018
 *
 * Implementation of class ParticleType
 *
*/

#include <iostream>
#include <algorithm>
#include <map>

#include "Particle.h"
#include "Debug.h"

namespace Caravel {

ParticleType::ParticleType(const std::string& inparticle){
	// just cover few cases to help transition
	if(inparticle=="gluon"){
		spin=ParticleStatistics::vector;
		flavor=ParticleFlavor::gluon;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="photon"){
		spin=ParticleStatistics::vector;
		flavor=ParticleFlavor::photon;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="Wp"){
		spin=ParticleStatistics::vector;
		flavor=ParticleFlavor::Wp;
		mass=ParticleMass(80385,1000);
		colored=false;
	}
	else if(inparticle=="Wm"){
		spin=ParticleStatistics::vector;
		flavor=ParticleFlavor::Wm;
		mass=ParticleMass(80385,1000);
		colored=false;
	}
	else if(inparticle=="G"){
		spin=ParticleStatistics::tensor;
		flavor=ParticleFlavor::G;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="T"){
		spin=ParticleStatistics::tensor;
		flavor=ParticleFlavor::T;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="A"){
		spin=ParticleStatistics::cubictensor;
		flavor=ParticleFlavor::A;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="H"){
		spin=ParticleStatistics::scalar;
		flavor=ParticleFlavor::H;
		// no width added. Mass approximated to 125 GeV
		mass=ParticleMass(125,1);
		colored=false;
	}
	else if(inparticle=="q"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::q;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="qb"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::qb;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="u"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::u;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="ub"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::ub;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="d"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::d;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="db"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::db;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="s"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::s;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="sb"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::sb;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="c"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::c;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="cb"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::cb;
		mass=ParticleMass();
		colored=true;
	}
	else if(inparticle=="b"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::b;
		mass=ParticleMass();	// massless by default
		colored=true;
	}
	else if(inparticle=="bb"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::bb;
		mass=ParticleMass();	// massless by default
		colored=true;
	}
	else if(inparticle=="t"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::t;
		mass=ParticleMass(173,1);
		colored=true;
	}
	else if(inparticle=="tb"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::tb;
		mass=ParticleMass(173,1);
		colored=true;
	}
	else if(inparticle=="l"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::l;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="lb"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::lb;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="e"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::e;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="eb"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::eb;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="ve"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::ve;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="veb"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::veb;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="mu"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::mu;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="mub"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::mub;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="vmu"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::vmu;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="vmub"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::vmub;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="tau"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::tau;
		mass=ParticleMass();	// massless by default
		colored=false;
	}
	else if(inparticle=="taub"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::taub;
		mass=ParticleMass();	// massless by default
		colored=false;
	}
	else if(inparticle=="vtau"){
		spin=ParticleStatistics::fermion;
		flavor=ParticleFlavor::vtau;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="vtaub"){
		spin=ParticleStatistics::antifermion;
		flavor=ParticleFlavor::vtaub;
		mass=ParticleMass();
		colored=false;
	}
	else if(inparticle=="Phi1"){	// Dummy massive scalar Phi1
		spin=ParticleStatistics::scalar;
		flavor=ParticleFlavor::Phi1;
		mass=ParticleMass(15,1);
		colored=false;
	}
	else if(inparticle=="Phi2"){ 	// Dummy massive scalar Phi2
		spin=ParticleStatistics::scalar;
		flavor=ParticleFlavor::Phi2;
		mass=ParticleMass(25,1);
		colored=false;
	}
	else if(inparticle=="V1"){	// Dummy massive vector V1
		spin=ParticleStatistics::vector;
		flavor=ParticleFlavor::V1;
		mass=ParticleMass(5,1);
		colored=false;
	}
	else if(inparticle=="V2"){	// Dummy massive vector V2
		spin=ParticleStatistics::vector;
		flavor=ParticleFlavor::V2;
		mass=ParticleMass(10,1);
		colored=false;
	}
	else if(inparticle=="notype"){
		spin=ParticleStatistics::undefined;
		flavor=ParticleFlavor::undefined;
		mass=ParticleMass();
		colored=false;
	}
	else{
		std::cerr<<"ERROR: there was a call to the helper contructor ParticleType(std::string) and did not recognize the input: "<<inparticle<<" --- Exit 1"<<std::endl;
                std::exit(1);
	}
}

std::ostream& operator<<(std::ostream& os,const ParticleType& p){
	os<<"spin: "<<p.spin<<", flavor: "<< p.flavor <<", mass: "<<p.mass<<", colored: ";
	if(p.colored)
		os<<"true";
	else
		os<<"false";
	return os;
}

bool almost_equal(ParticleFlavor f1,ParticleFlavor f2){
    // Weyl Fermions
    if (f1 == ParticleFlavor::q) return in_container<ParticleFlavor::q>(f2) and !is_dirac(f2);
    if (f1 == ParticleFlavor::qb) return in_container<ParticleFlavor::qb>(f2) and !is_dirac(f2);
    if (f2 == ParticleFlavor::q) return in_container<ParticleFlavor::q>(f1) and !is_dirac(f1);
    if (f2 == ParticleFlavor::qb) return in_container<ParticleFlavor::qb>(f1) and !is_dirac(f1);

    // Weyl Leptons
    if (f1 == ParticleFlavor::l) return in_container<ParticleFlavor::l>(f2);
    if (f1 == ParticleFlavor::lb) return in_container<ParticleFlavor::lb>(f2);
    if (f2 == ParticleFlavor::l) return in_container<ParticleFlavor::l>(f1);
    if (f2 == ParticleFlavor::lb) return in_container<ParticleFlavor::lb>(f1);
 
    // Dirac Fermions
    if (f1 == ParticleFlavor::t and f2 == ParticleFlavor::Q_nf1) return true;
    if (f2 == ParticleFlavor::t and f1 == ParticleFlavor::Q_nf1) return true;
    if (f1 == ParticleFlavor::tb and f2 == ParticleFlavor::Qb_nf1) return true;
    if (f2 == ParticleFlavor::tb and f1 == ParticleFlavor::Qb_nf1) return true;

    return f1==f2;
}

ParticleFlavor anti_flavor(ParticleFlavor flavor) {
    switch (flavor) {
        // antiparticle is same
        case ParticleFlavor::gluon:
        case ParticleFlavor::photon:
        case ParticleFlavor::Z:
        case ParticleFlavor::H:
        case ParticleFlavor::G:
        case ParticleFlavor::T:
        case ParticleFlavor::A:
        case ParticleFlavor::Phi1:
        case ParticleFlavor::Phi2:
        case ParticleFlavor::V1:
        case ParticleFlavor::V2:
        case ParticleFlavor::ds_scalar:
        case ParticleFlavor::renormalon:
        case ParticleFlavor::undefined:
            return flavor;
        // negative
        default: int ipf = (int)flavor; return ParticleFlavor(-ipf);
    }
}


bool operator==(const ParticleType& pt1,const ParticleType& pt2){
	return pt1.spin==pt2.spin&&pt1.flavor==pt2.flavor&&pt1.mass==pt2.mass&&pt1.colored==pt2.colored;
}

bool almost_equal(const ParticleType& pt1,const ParticleType& pt2){
	return pt1.spin==pt2.spin&& almost_equal(pt1.flavor,pt2.flavor) &&pt1.mass==pt2.mass&&pt1.colored==pt2.colored;
}

bool operator!=(const ParticleType& pt1,const ParticleType& pt2){
	return !(pt1==pt2);
}

ParticleType ParticleType::get_anti_type() const {
	return ParticleType(this->get_flipped_statistics(),this->get_anti_flavor(),mass,colored);
}

ParticleFlavor ParticleType::get_flavor() const {
	return flavor;
}

ParticleFlavor ParticleType::get_anti_flavor() const {
    return anti_flavor(flavor);
}

ParticleStatistics ParticleType::get_statistics() const {
	return spin;
}

ParticleStatistics ParticleType::get_flipped_statistics() const {
	if(spin==ParticleStatistics::fermion)
		return ParticleStatistics::antifermion;
	if(spin==ParticleStatistics::antifermion)
		return ParticleStatistics::fermion;
	return spin;
}

bool ParticleType::is_colored() const {
	return colored;
}

ParticleMass ParticleType::get_ParticleMass() const {
	return mass;
}

void ParticleType::set_mass(int nr,int dr,int ni,int di){
	return mass.set_mass(nr,dr,ni,di);
}

bool ParticleType::is_massive() const {
	return mass.is_massive();
}

bool ParticleType::is_massless() const {
	return mass.is_massless();
}

size_t ParticleType::get_mass_index() const {
	return mass.get_mass_index();
}

std::string ParticleType::to_string() const {
	return std::string(wise_enum::to_string(flavor));
}


}
