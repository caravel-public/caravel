/**
 * @file type_traits_extra.h
 *
 * @brief Defines some useful type traits
 *
 */
#ifndef TYPE_TRAITS_H_
#define TYPE_TRAITS_H_

#include <type_traits>
#include <iterator>
#include <complex>

namespace Caravel{
    struct Barrett32;
    struct BigRat;
}

struct dd_real;
struct qd_real;
class RGMP;

//namespace std{
//    // forward declare
//    template <typename T> struct complex;
//}

namespace Caravel {

using std::false_type;
using std::true_type;

/**
 * is_rational checks if the type is rational
 */
template <typename T> struct is_rational : std::false_type {};

/**
 * is_complex checks if the type is complex
 */
template <typename T> struct is_complex : std::false_type {};
template <typename T> struct is_complex<std::complex<T>> : std::true_type {};
template <typename T> inline constexpr bool is_complex_v = is_complex<T>::value;

/**
 * Make non-complex type if it was complex
 */
template <typename T, typename = void> struct remove_complex;
template <typename T> struct remove_complex<T, typename std::enable_if_t<!is_complex<T>::value>> { typedef T type; };
template <typename T> struct remove_complex<T, typename std::enable_if_t<is_complex<T>::value>> { typedef typename T::value_type type; };
template <typename T> using remove_complex_t = typename remove_complex<T>::type;

/**
 * Make complex type if it was non-complex
 */
template <typename T, typename = void> struct add_complex;
template <typename T> struct add_complex<T, typename std::enable_if_t<!is_complex<T>::value>> { typedef std::complex<T> type; };
template <typename T> struct add_complex<T, typename std::enable_if_t<is_complex<T>::value>> { typedef T type; };
template <typename T> using add_complex_t = typename add_complex<T>::type;

// Forward declaration
template <class T> class Series;
/**
 * is_series checks if the type is a Series.
 */
template <typename T> struct is_series : std::false_type {};
template <typename T> struct is_series<Series<T>> : std::true_type {};

/**
 * is_finite_field checks if the type is a finite field type
 */
template <typename T> struct is_finite_field : std::false_type {};
template <> struct is_finite_field<Caravel::Barrett32> : std::true_type {};
template <typename T> inline constexpr bool is_finite_field_v = is_finite_field<T>::value;

//--------
namespace detail {
template <typename T> struct __is_exact_helper : public std::conditional<is_finite_field_v<T>, true_type, false_type> {};

template <> struct __is_exact_helper<short> : public true_type {};

template <> struct __is_exact_helper<unsigned short> : public true_type {};

template <> struct __is_exact_helper<int> : public true_type {};

template <> struct __is_exact_helper<unsigned int> : public true_type {};

template <> struct __is_exact_helper<long> : public true_type {};

template <> struct __is_exact_helper<unsigned long> : public true_type {};

template <> struct __is_exact_helper<long long> : public true_type {};

template <> struct __is_exact_helper<unsigned long long> : public true_type {};
}

/**
 * is_exact
 * is a type trait which determines if the type is an exact (algebraic) type
 * All finite field types are automatically exact
 * WARNING: we do not require it to be a field here and do not test this!
 * For example c++ built-in int types are included here.
 */
template <typename _Tp> struct is_exact : public detail::__is_exact_helper<std::remove_cv_t<_Tp>>::type {};
template <typename T> inline constexpr bool is_exact_v = is_exact<T>::value;


namespace detail {
template <typename> struct __is_floating_point_helper : public false_type {};

template <> struct __is_floating_point_helper<float> : public true_type {};

template <> struct __is_floating_point_helper<double> : public true_type {};

template <> struct __is_floating_point_helper<long double> : public true_type {};

#if !defined(__STRICT_ANSI__) && defined(_GLIBCXX_USE_FLOAT128)
template <> struct __is_floating_point_helper<__float128> : public true_type {};
#endif
}

/**
 * is_floating_point
 * true for types which represent floating point arithmetics
 */
template <typename _Tp> struct is_floating_point : public detail::__is_floating_point_helper<typename std::remove_cv<_Tp>::type>::type {};
template <typename T> inline constexpr bool is_floating_point_v = is_floating_point<T>::value;

//! propagate is_integer and is_floating_point throuh complex, such that e.g. is_integer<complex<int>> is true
namespace detail {
template <typename T> struct __is_exact_helper<std::complex<T>> : public std::conditional<is_exact_v<T>, std::true_type, std::false_type> {};
template <typename T>
struct __is_floating_point_helper<std::complex<T>> : public std::conditional<is_floating_point<T>::value, std::true_type, std::false_type> {};
}

namespace detail {
//! define dd_real and qd_real as floating point type
template <> struct __is_floating_point_helper<dd_real> : public std::true_type {};
template <> struct __is_floating_point_helper<qd_real> : public std::true_type {};
template <> struct __is_floating_point_helper<RGMP> : public std::true_type {};
}

namespace detail {
template <> struct __is_exact_helper<BigRat> : public true_type {};
}

// type traits for extended_t
template <typename T> struct extended_t;
template <typename T> struct is_extended : std::false_type {};
template <typename T> struct is_extended<extended_t<T>> : std::true_type {};
template <typename T> struct is_complex<extended_t<std::complex<T>>> : std::true_type {};

template <typename T> inline constexpr bool is_extended_v = is_extended<T>::value;

namespace detail {
template <typename T> struct __is_exact_helper<extended_t<T>> : public std::conditional<is_exact<T>::value, std::true_type, std::false_type> {};
template <typename T>
struct __is_floating_point_helper<extended_t<T>> : public std::conditional<is_floating_point<T>::value, std::true_type, std::false_type> {};
}

//! Infer the unexetended type. Returns the type itself if it is not extended.
template <typename T, typename Enable = void> struct infer_unext_type { typedef T type; };
template <typename T> struct infer_unext_type<T, std::enable_if_t<is_extended<T>::value>> { typedef typename T::element_type type; };

namespace detail {
template <class X, class Y, class Op> class op_valid_impl {
  private:
    template <class U, class L, class R> static auto test(int) -> decltype(std::declval<U>()(std::declval<L>(), std::declval<R>()), void(), std::true_type());

    template <class U, class L, class R> static auto test(...) -> std::false_type;

  public:
    using type = decltype(test<Op, X, Y>(0));
};
}

//! check if operation Op(X,Y) is implemented
template <class X, class Y, class Op> using op_valid = typename detail::op_valid_impl<X, Y, Op>::type;
template <typename X, class Y, class Op> inline constexpr bool op_valid_v = detail::op_valid_impl<X, Y, Op>::type::value;

namespace detail {
// To allow ADL with custom begin/end
using std::begin;
using std::end;

template <typename T>
auto is_iterable_impl(int) -> decltype(begin(std::declval<T&>()) != end(std::declval<T&>()),    // begin/end and operator !=
                                       ++std::declval<decltype(begin(std::declval<T&>())) &>(), // operator ++
                                       *begin(std::declval<T&>()),                              // operator*
                                       std::true_type{});

template <typename T> std::false_type is_iterable_impl(...);
template <typename T> using is_iterable = decltype(detail::is_iterable_impl<T>(0));
}

/**
 * Check if a given type can be iterated (e.g. used in a ranged for loop)
 * Credit:
 * https://stackoverflow.com/a/29634934
 */
template <typename T> inline constexpr bool is_iterable_v = detail::is_iterable<T>::value;


/**
 * This struct can be used with `static_assert` and `if constexpr` to fail the compilation only when it reaches
 * a particular branch of `if constexpr`.
 * If this branch is not reached statically, the compilation will go on.
 */
template <typename T> struct _delayed_static_false : std::false_type {};
template <typename T> inline constexpr bool _delayed_static_false_v = _delayed_static_false<T>::value;

template <typename T>
struct is_pair : std::false_type { };

template <typename T, typename U>
struct is_pair<std::pair<T, U>> : std::true_type { };

template <typename T>
constexpr bool is_pair_v = is_pair<T>::value;

}

#endif
