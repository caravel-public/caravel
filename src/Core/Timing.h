#ifndef TIMING_H_
#define TIMING_H_

#include <chrono>
#include <string>
#include "Debug.h"

#ifdef TIMING_ON


//! start (create if necessary) counter 
#define START_TIMER(NAME)                                                                       \
    static Caravel::timing::counter __timing_counter_##NAME(#NAME);                                                    \
    __timing_counter_##NAME.start();

//! as above, but additionally pass numerical type information, which will be reported
#define START_TIMER_T(NAME,T)                                                                       \
    static Caravel::timing::counter __timing_counter_##NAME(#NAME,_typeid(T));                                                    \
    __timing_counter_##NAME.start();


#define STOP_TIMER(NAME) __timing_counter_##NAME.stop();
#define REPORT_TIMER(NAME) __timing_counter_##NAME.report();

#else

#define START_TIMER_T(NAME,T)
#define START_TIMER(NAME)
#define STOP_TIMER(NAME)
#define REPORT_TIMER(NAME)

#endif

namespace Caravel{ namespace timing{

class counter{
  private:
    using clock = std::chrono::high_resolution_clock;
    using duration_type = std::chrono::duration<double>;

    std::string name;
    std::string type{};

    duration_type total{0};
    decltype(clock::now()) interval_begin;

  public:

    void start();
    void stop();
    void report();
    duration_type get_duration() const;

    counter() = delete;
    counter(std::string&& n) : name(n) {}
    counter(std::string&& n, std::string&& t) : name(n), type(t) {}
    ~counter();
};

#ifdef TIMING_ON
extern counter surface_terms_timing;
#endif

}
}

#endif
