#ifndef _MAP_TO_BASIS_H_
#define _MAP_TO_BASIS_H_

#include <string>
#include <vector>
#include <utility>
#include <type_traits>
#include <unordered_map>

#include "Core/Particle.h"
#include "Core/MathInterface.h"
#include "IntegralLibrary/FunctionBasisExpansion.h"

#include "Core/CaravelInput.h"

namespace Caravel {
namespace Integrals {

using std::false_type;
using std::true_type;

namespace _helper {

template <typename T, typename = std::string>
struct has_as_string_method : std::false_type {};

template <typename T>
struct has_as_string_method<T, decltype(std::declval<T>().as_string())> : std::true_type {};

} // namespace _helper

/**
 * Take the representation of an Amplitude as a NumericFunctionBasisExpansion
 * and map everything to a unique basis.
 *
 * This step is performed in order to find a simple form of the amplitude such
 * that the functional reconstruction of basis function coefficients is
 * simplified.
 * <br>
 * The template parameter T indicates the finite field type over which the
 * functional reconstruction is performed.
 * <br>
 * The template parameter FType is the type of the basis functions of the amplitude
 * They need to have an "as_string" method which is checked at compile time by SFINAE.
 */
template <typename T, typename FType>
class MapToBasis {

    // Only works for FTypes that have a as_string() method
    static_assert(_helper::has_as_string_method<FType>::value, "FType needs to have an 'as_string()' method!");

public:
    /// Default constructor
    MapToBasis() = default;

    /**
     * Construct a MapToBasis with a given set of replacement rules.
     * @param replacements Contains the replacement rules from functions that do not belong to the basis
     *                     to functions that do belong to the basis.
     */
  MapToBasis(const std::unordered_map<std::string, std::vector<std::pair<std::string, BigRat>>>& replacements);

    /**
     * Construct a MapToBasis with a given set of replacement rules.
     * @param replacements Contains the replacement rules from functions that do not belong to the basis
     *                     to functions that do belong to the basis.
     *                     In this constructor the coefficients of the integral functions depend on external
     *                     kinematics. The boolean parameter tells whether tr5 is negative.
     */
    MapToBasis(const std::unordered_map<std::string, std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>>>& replacements);

    /**
     * Construct a MapToBasis by providing the path to a file that contains the replacement rules.
     * @param filename The name of the file containing the replacement rules.
     */
    MapToBasis(const std::string& filename);

    /**
     * Construct a MapToBasis by providing the process under consideration.
     *
     * Notice that this only works for processes where the mappings are delivered
     * within Caravel in the share/Caravel directory of the installation.
     * @param process The process under consideration.
     */
    MapToBasis(const PartialAmplitudeInput& process);

    /**
     * Map an ampltiude in a function basis expansion to a basis.
     * @param amplitude The amplitude that needs to be mapped onto the basis.
     * @return          A NumericFunctionBasisExpansion where all the functions are indeed basis functions.
     */
    NumericFunctionBasisExpansion<Series<T>, std::string>
    operator()(const NumericFunctionBasisExpansion<Series<T>, FType>& amplitude,momentumD_configuration<T,4> mcf=momentumD_configuration<T,4>(),bool negative_tr5=false) const;

    /**
     * Get the replacement rule for a given function.
     *
     * Especially useful for debugging to check whether the rules match the ones from the file.
     * @param func The function the replacement rule is being requested for.
     * @return The repacement rule for the requested function as a linear combination of basis functions and
     * coefficients.
     */
    std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>> get_replacement_rule_for_function(const std::string& func) const;

    /**
     * For a given Process look for a default file to read the basis replacements from.
     * @param  process The process (vector of particle pointers) under consideration.
     * @return         If a default file exists in the in the share/Caravel directory of the
     *                 installation, return its path, else an empty string.
     */
    static std::string get_filename(const PartialAmplitudeInput& process);

    /**
     * Return the number of replacements in the map.
     * @return The number of special functions that are replaced by the map.
     */
    size_t n_replacements() const;

private:
    /// Store the replacement rules.
    std::unordered_map<std::string, std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>>> replacements;

    /**
     * Extract the content of String[...] from the input file.
     *
     * Since this content contains commas, as in the case for "G(0,s)", MathList cannot deal with
     * it, so we use this function to extract the corresponding string.
     * @param  s The expression containing the combination of special functions in the form String[Product of special
     * functions];
     * @return   The string contained inside String[...]
     */
    std::string extract_String_content(const std::string& s);

    /**
     * Transform a transformation rule in form of a MathList into something C++ can deal with.
     * @param reps The replacement rule for a given special function in form of a MathList (string).
     * @return     The replacement rule as a linear combination (vector) of basis function names and
     *             coefficients (pairs).
     */
    std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>> extract_replacing_lc(MathList&& reps);

    /**
     * Transform a transformation rule in form of a MathList into something C++ can deal with.
     * @param reps The replacement rule for a given special function in form of a MathList (string).
     * @return     The replacement rule as a linear combination (vector) of basis function names and
     *             coefficients (pairs) associated to the name of the function that is to be replaced (string).
     */
    std::pair<std::string, std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>>>
    extract_replacement_rule(const std::string& rule);
};

} // namespace Integrals
} // namespace Caravel

// Implementations
#include "MapToBasis.hpp"

#endif // _MAP_TO_BASIS_H_
