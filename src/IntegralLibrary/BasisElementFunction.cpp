#include "BasisElementFunction.h"

namespace std {

// Specialization of the hash function

size_t hash<::Caravel::Integrals::DummyBasisFunction>::operator()(const ::Caravel::Integrals::DummyBasisFunction& dbf) const {
    static hash<string> hasher;
    return hasher(dbf.as_string());
}

} // namespace std

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// ----------- Replace all occurrences of a substring in a string -------------
// ----------------------------------------------------------------------------

void find_n_replace_all(std::string& in, const std::string& target, const std::string& repl) {
    int pos = in.find(target);
    auto len = target.size();
    while (pos != -1) {
        in.replace(pos, len, repl);
        pos = in.find(target);
    }
}

// ----------------------------------------------------------------------------
// -------------------------- DummyBasisFunction ------------------------------
// ----------------------------------------------------------------------------

DummyBasisFunction::DummyBasisFunction(std::string n) : name{n} {}

std::string DummyBasisFunction::as_string() const { return name; }

// Compare Dummy basis functions
bool operator==(const DummyBasisFunction& db1, const DummyBasisFunction& db2) {
    return db1.as_string() == db2.as_string();
}

C DummyBasisFunction::operator()(const std::vector<R>& args){
    return C(1.0);
}

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BASISFUNC(OUT, IN)                                                                                    \
    template class StdBasisFunction<OUT, IN>;                                                                             \
    template bool operator==(const StdBasisFunction<OUT, IN>& b1, const StdBasisFunction<OUT, IN>& b2);                      \
    template std::ostream& operator<<(std::ostream& stream, const StdBasisFunction<OUT, IN>& basis);

INSTANTIATE_BASISFUNC(C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_BASISFUNC(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_BASISFUNC(CVHP, RVHP)
#endif

} // namespace Integrals
} // namespace Caravel

namespace std {

#define INSTANTIATE_BFHASH(OUT, IN) template struct hash<::Caravel::Integrals::StdBasisFunction<OUT, IN>>;

INSTANTIATE_BFHASH(::Caravel::C, ::Caravel::R)
#ifdef HIGH_PRECISION
INSTANTIATE_BFHASH(::Caravel::CHP, ::Caravel::RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_BFHASH(::Caravel::CVHP, ::Caravel::RVHP)
#endif

} // namespace std
