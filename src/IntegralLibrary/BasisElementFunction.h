#ifndef _BASISELEMENTFUNCTION_H_
#define _BASISELEMENTFUNCTION_H_

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <functional>

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "Core/qd_suppl.h"
#endif
#include "Core/f_suppl.h"
#include "Core/typedefs.h"
#include "IntegralLibrary/IntegralTraits.h"
#include "IntegralLibrary/CanonicalOrderingBasisFunctions.h"

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// ----------- Replace all occurrences of a substring in a string -------------
// ----------------------------------------------------------------------------

/**
 * Replace all the occurrences of \a target in \a in by \a repl.
 * @param in     The string in which the replacements are performed.
 * @param target The string that is to be replaced.
 * @param repl   The string that is inserted for \a target.
 * @return The initial string with \a target replaced by \@ repl.
 */
void find_n_replace_all(std::string& in, const std::string& target, const std::string& repl);

// ----------------------------------------------------------------------------
// -------------------------- DummyBasisFunction ------------------------------
// ----------------------------------------------------------------------------

/**
 * Dummy Basis Function that only has a name.
 *
 * This can be used to test the infrastructure that only depends on
 * being able to differentiate basis functions but not on their
 * explicit form or the possibility to evaluate them numerically.
 */
class DummyBasisFunction : public InOutTrait<R, C> {
public:
    /**
     * Construct a basis function with name 'n'.
     * @param n The name of the basis function.
     */
    DummyBasisFunction(std::string n);

    /**
     * Return the string representation of the dummy basis function.
     * @return A string with the name of the basis function.
     */
    std::string as_string() const;

    /// Dummy evaluation function. Always returns 1.
    C operator()(const std::vector<R>& args);

private:
    std::string name; /**< The name of the dummy basis function, */
};

// Compare DummyBasisFunctions
bool operator==(const DummyBasisFunction& db1, const DummyBasisFunction& db2);

// ----------------------------------------------------------------------------
// --------------------- StdBasisFunction -------------------------------------
// ----------------------------------------------------------------------------

/**
 * The Standard Master Integral Basis Function.
 *
 * It is constructed from a std::function that takes a vector of invariants
 * and a string which contains its expression which is used for hashing.
 * <br>
 * The template parameter \a Out refers to the type of the result of the numerical
 * evaluation of the basis function. The template parameter \a In refers to the
 * type of the input parameters for the numerical evaluation.
 *
 */
template <typename Out, typename In>
class StdBasisFunction : public InOutTrait<In, Out> {
public:
    /**
     * Construct a basis function from an std::function and its expression in string form.
     *
     * The names of the parameters \a param_names need to be provided since in general
     * the hash string \a will be the expression provided by Mathematica. If we have two
     * functions as follows,
     *
     * \f[
     *  f1(x, y) = log(x) / y = log(args[0]) / args[1]
     * \f]
     * \f[
     * f2(x, y) = log(y) / x = log(args[1]) / args[0]
     * \f]
     *
     * Then there is no problem in the numerical evaluation if we call this with
     * f1("t1", "t2") and f2("t2", "t1") but the caching needs to know that in this
     * case both functions are equal. Therefore the parameter names are used to update
     * the hash string and replace args[i] -> \a param_name[i].
     *
     * @param func        The function that evaluates the basis function as a function of the
     *                    external kinematical invariants.
     * @param hash        The string that is used to hash the function.
     * @param param_names The user-defined names for the parameters. They are used to replace
     *                    the variables in the hash string.
     */
    StdBasisFunction(const std::function<Out(const std::vector<In>&)>& func, const std::string& hash,
                     const std::vector<std::string>& param_names);

    /**
     * Default constructor will assign function which is 1.
     */
    StdBasisFunction();

    /**
     * Return the string representation of the function.
     * @return Contains the expression of the function.
     */
    std::string as_string() const;

    /**
     * Evaluate the function with a specific set of kinematical invariants.
     *
     * @param args Contains the external kinematical invariants.
     * @return The value of the function for a given set of kinematical invariants.
     */
    Out operator()(const std::vector<In>& args) const;

    /**
     * Evaluate the function with a specific set of kinematical invariants.
     *
     * In this overload of the operator() it might be that the function obtains
     * more parameters than it is defined with. In this case, it used the stored
     * list of invariants to find their occurences in `param_names` and uses to
     * corresponding indexes to extract their numerical values from `args` and
     * evaluate.
     *
     * @param args Contains the external kinematical invariants.
     * @return The value of the function for a given set of kinematical invariants.
     */
    Out operator()(const std::vector<In>& args, const std::vector<std::string>& param_names) const;

    // Begin operators

    /**
     * Take product with other basis function.
     * NOTE: Only works at abstract level. Result is not evaluatable.
     * @param other The function to take the product with
     * @return A reference to *this, having been multiplied by other.
     */
    StdBasisFunction<Out, In>& operator*=(const StdBasisFunction<Out,In>& other);
private:
    /// String used to hash the basis function.
    std::string hashstring;
    /// Evaluates the basis function numerically.
    std::function<Out(const std::vector<In>&)> func;
    /// Store the names of the parameters.
    std::vector<std::string> parameter_names;

    /// Find the positions of the stores parameter names in `names`.
    std::vector<std::size_t> get_indexes(const std::vector<std::string>& names) const;

public:
    template <typename A> void serialize(A& a) {a(hashstring,parameter_names);}
};

template <typename Out, typename In>
std::ostream& operator<<(std::ostream& stream, const StdBasisFunction<Out, In>& function);

template<typename Out, typename In>
StdBasisFunction<Out, In> operator *(StdBasisFunction<Out,In> a, const StdBasisFunction<Out,In>& b);

/**
 * Compares two StdBasisFunctions through their string representation. Disregards * any other information.
 */
template <typename Out, typename In>
bool operator==(const StdBasisFunction<Out, In>& b1, const StdBasisFunction<Out, In>& b2);

/**
 * Compares two StdBasisFunctions through their string representation. Disregards * any other information.
 */
template <typename Out, typename In>
bool operator<(const StdBasisFunction<Out, In>& b1, const StdBasisFunction<Out, In>& b2);

} // namespace Integrals
} // namespace Caravel

// ----------------------------------------------------------------------------
// ---------------- Specialize hash functions for BasisFunctions --------------
// ----------------------------------------------------------------------------

namespace std {

/**
 * Specialize std::hash<> for DummyBasisFunction.
 */
template <>
struct hash<::Caravel::Integrals::DummyBasisFunction> {
    /**
     * Return the hash value of the DummyBasisFunction.
     * @param dbf The DummyBasisFunction to be hashed.
     * @return    The hash value of the DummyBasisFunction.
     */
    size_t operator()(const ::Caravel::Integrals::DummyBasisFunction& dbf) const;
};

/**
 * Specialize std::hash<> for StdBasisFunction.
 */
template <typename Out, typename In>
struct hash<::Caravel::Integrals::StdBasisFunction<Out, In>> {
    /**
     * Return the hash value of the StdBasisFunction.
     * @param dbf The StdBasisFunction to be hashed.
     * @return    The hash value of the StdBasisFunction.
     */
    size_t operator()(const ::Caravel::Integrals::StdBasisFunction<Out, In>& dbf) const;
};

} // namespace std

// ----------------------------------------------------------------------------
// -------------------------- IMPLEMENTATIONS ---------------------------------
// ----------------------------------------------------------------------------

#include "BasisElementFunction.hpp"

#endif
