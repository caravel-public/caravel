#ifndef _FUNCTIONBASISEXPANSION_H_
#define _FUNCTIONBASISEXPANSION_H_

#include <utility>
#include <stdexcept>
#include <type_traits>
#include <unordered_map>

#include "Core/Series.h"
#include "FBCoefficient.h"
#include "BasisElementFunction.h"
#include "Core/type_traits_extra.h"

#include "Core/typedefs.h"

#include "Core/MathOutput.h"

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// ------------------- NumericFunctionBasisExpansion --------------------------
// ----------------------------------------------------------------------------

/**
 * Class to handle the numeric coefficients in basis of arbitrary functions.
 *
 * The function type FType can be arbitrary as long as it is hashable, by
 * specializing std::hash<FType> and hence it needs to be comparable ("==").
 *
 * Regarding the coefficients (of type CType) of the basis, they need to be addable
 * and subtractable and it must be possible to multiply or divide them by scalars.
 */
template <typename CType, typename FType>
class NumericFunctionBasisExpansion {
public:
    using FunctionsType = FType;
    using CoefficientsType = CType;

    /// Construct empty basis
    NumericFunctionBasisExpansion() = default;

    /// Construct it from a given basis, arranged as a vector of
    /// functions and a vector of coefficients
    NumericFunctionBasisExpansion(const std::vector<FType>& fs, const std::vector<CType>& cs);

    /// Construct it from a given basis arranged as an unordered_map
    NumericFunctionBasisExpansion(const std::unordered_map<FType, CType>& b);

    /// Default copy constructor.
    NumericFunctionBasisExpansion(const NumericFunctionBasisExpansion<CType, FType>& other) = default;

    /// Default copy assignment operator.
    NumericFunctionBasisExpansion<CType, FType>&
    operator=(const NumericFunctionBasisExpansion<CType, FType>& other) = default;

    /**
     * Return the number of functions in the basis.
     * @return The number of functions in the basis.
     */
    unsigned long n_basis_funcs() const;

    /**
     * Check whether a given basis function is contained in the basis.
     * @param  basis_idx ID of the function the check is performed for.
     * @return           true if the requested function is in the basis, else false.
     */
    bool contains(const FType& basis_func) const;

    /**
     * Get the numerical value of the coefficient of a specific basis function.
     * @param  basis_func The basis function the coefficient is requested for.
     * @return            The coefficient of the requested basis function as series
     *                    expansion in the dimensional regulator.
     */
    CType get_coeff(const FType& basis_func) const;

    /**
     * Return a vector with all the Keys
     * @returns A vector with all the functions appearing in the basis.
     */
    std::vector<FType> get_all_basis_functions() const;

    /**
     * Return a vector with all coefficients
     * @returns A vector with all the functions appearing in the basis.
     */
    std::vector<CType> get_all_coeffs() const;


    // Operators

    /**
     * Add two numeric basis expansions.
     *
     * Note that this is only possible at the numeric level and not for the abstract
     * FunctionBasisExpansion.
     *
     * @param other The NumericFunctionBasisExpansion to be added to *this.
     * @return      The sum of *this and other.
     */
    NumericFunctionBasisExpansion<CType, FType>& operator+=(const NumericFunctionBasisExpansion<CType, FType>& other);

    /**
     * Subtract two numeric basis expansions.
     *
     * Note that this is only possible at the numeric level and not for the abstract
     * FunctionBasisExpansion.
     *
     * @param other The NumericFunctionBasisExpansion to be subtracted from *this.
     * @return      The difference of *this and other.
     */
    NumericFunctionBasisExpansion<CType, FType>& operator-=(const NumericFunctionBasisExpansion<CType, FType>& other);

    /**
     * Multiply all basis coefficients by a scalar.
     * @param scal The scalar that is to multiply the basis coefficients.
     * @return     The basis with all coefficients multiplied by scal.
     */
    template <typename T>
    NumericFunctionBasisExpansion<CType, FType>& operator*=(const T& scal);

    /**
     * Multiply the expression by a basis function
     * @param func The basis function with which we multiply the expression
     * @return     The expanded result of the multipliciation
     */
    NumericFunctionBasisExpansion<CType, FType>& operator*=(const FType& func);

    /**
     * Take the product of two NumericFunctionBasisExpansions
     * @param other The other expression to multiply *this by.
     * @return     The modified *this.
     */
    NumericFunctionBasisExpansion<CType, FType>& operator*=(const NumericFunctionBasisExpansion<CType, FType>& other);

    /**
     * Divide all basis coefficients by a scalar.
     * @param scal The scalar that is to divide the basis coefficients.
     * @return     The basis with all coefficients divided by scal.
     */
    template <typename T>
    NumericFunctionBasisExpansion<CType, FType>& operator/=(const T& scal);

    /**
     * Evaluate the complete basis.
     *
     * In addition to the numerical values of the parameters, also their names
     * have to be provided, such that each basis function can use its stored list
     * of parameter names to check which numerical values it needs as input.
     */
    template <typename CT = CType, typename FT = FType>
    std::enable_if_t<
        is_series<CT>::value && is_std_basis_function<FT>::value && !is_exact<typename CT::coeff_type>::value, CT>
    operator()(const std::vector<typename FT::input_type>& args, const std::vector<std::string>& names);

    /**
     * Make it range-based-for-loopable.
     * @return Iterator to the beginning of the underlying unordered_map.
     */
    auto begin();

    /**
     * Make it range-based-for-loopable.
     * @return Iterator to the beginning of the underlying unordered_map.
     */
    auto begin() const;

    /**
     * Make it range-based-for-loopable.
     * @return Iterator to the end of the underlying unordered_map.
     */
    auto end();

    /**
     * Make it range-based-for-loopable.
     * @return Iterator to the beginning of the underlying unordered_map.
     */
    auto end() const;

    /**
     * Obtain the coefficient of a specific power in \f$ \epsilon \f$ in the expansion.
     *
     * This method is only enabled if the return type of the coefficient is of Series type.
     *
     * @param  pow The power of \f$ \epsilon \f$ which the coefficient is requested for.
     * @return     Contains the functions and their coefficients for the requested power in  \f$ \epsilon \f$.
     */
    template <typename TT = CType>
    typename std::enable_if<is_series<TT>::value, NumericFunctionBasisExpansion<typename TT::coeff_type, FType>>::type
    get_eps_pow_coeff(int power) const;

    /**
     * Obtain the leading pole appearing in the basis.
     *
     * @return The power of the leading pole.
     */
    template <typename TT = CType>
    typename std::enable_if<is_series<TT>::value, int>::type leading() const;

    /**
     * Obtain the highest power appearing in the basis.
     *
     * @return The highest power in the expansion parameter.
     */
    template <typename TT = CType>
    typename std::enable_if<is_series<TT>::value, int>::type last() const;

    /**
     * Drops all terms which do not match the predicate
     * @param predicate A function which returns true for the terms
     * which should be kept
     */
    void select(const std::function<bool(const std::pair<FType, CType>&)>& predicate);

    /**
     * Applies the given function to the coefficients, creating a new NumericFunctionBasisExpansion
     */
    template <typename NewCType>
    NumericFunctionBasisExpansion<NewCType, FType> coeff_map(const std::function<NewCType(CType)>& f);

    CType& operator[](const FType& f) {return basis[f];}
    CType& operator[](FType&& f) {return basis[f];}

private:
    std::unordered_map<FType, CType> basis{}; /**< Stores the numerical value for each basis function coefficient. */
};

// Operators

/**
 * Add two bases.
 *
 * This corresponds to adding the coefficients that appear in both bases
 * and merging those that don't.
 *
 * @param b1 The first basis in the sum.
 * @param b2 The second basis in the sum.
 * @return   The sum of b1 and b2.
 */
template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType> operator+(const NumericFunctionBasisExpansion<CType, FType>& b1,
                                                      const NumericFunctionBasisExpansion<CType, FType>& b2);

/**
 * Subtract two bases.
 *
 * This corresponds to subtracting the coefficients that appear in both bases
 * and merging those that don't.
 *
 * @param b1 The first basis in the difference.
 * @param b2 The second basis in the difference.
 * @return   The difference between b1 and b2.
 */
template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType> operator-(const NumericFunctionBasisExpansion<CType, FType>& b1,
                                                      const NumericFunctionBasisExpansion<CType, FType>& b2);

/**
 * Multiply all the coefficients in the basis by a scalar.
 *
 * @param scal   The scalar to multiply all the basis coefficients.
 * @param basis  The basis that is being multiplied by a scalar.
 * @return       The basis where all coefficients are multiplied by scal.
 */
template <typename CType, typename FType, typename T>
NumericFunctionBasisExpansion<CType, FType> operator*(const T& scal,
                                                      const NumericFunctionBasisExpansion<CType, FType>& basis);

/**
 * Multiply all the coefficients in the basis by a scalar.
 *
 * @param basis  The basis that is being multiplied by a scalar.
 * @param scal   The scalar to multiply all the basis coefficients.
 * @return       The basis where all coefficients are multiplied by scal.
 */
template <typename CType, typename FType, typename T>
NumericFunctionBasisExpansion<CType, FType> operator*(const NumericFunctionBasisExpansion<CType, FType>& basis,
                                                      const T& scal);

/**
 * Multiply two expressions
 *
 * @param expr1  The first expression in the product.
 * @param expr2  The second expression in the product.
 * @return      The expanded result of the multiplication
 */
template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType> operator*(NumericFunctionBasisExpansion<CType, FType> expr1,
                                                      const NumericFunctionBasisExpansion<CType, FType>& expr2);

/**
 * Multiply all terms in the expression by a function
 *
 * @param func  The function to multiply the expression with.
 * @param expr  The expression to multiply with the function.
 * @return      The expanded result of the multiplication
 */
template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType> operator*(const FType& func,
                                                      NumericFunctionBasisExpansion<CType, FType> expr);

/**
 * Divide all the coefficients in the basis by a scalar.
 *
 * @param scal   The scalar to divide all the basis coefficients.
 * @param basis  The basis that is being divided by a scalar.
 * @return       The basis where all coefficients are divided by scal.
 */
template <typename CType, typename FType, typename T>
NumericFunctionBasisExpansion<CType, FType> operator/(const NumericFunctionBasisExpansion<CType, FType>& basis,
                                                      const T& scal);

template <typename CType, typename FType>
std::ostream& operator<<(std::ostream& stream, const NumericFunctionBasisExpansion<CType, FType>& function);

template <typename CType, typename FType>
std::string math_string(const NumericFunctionBasisExpansion<CType, FType>& expression);

// ----------------------------------------------------------------------------
// ---------------------- FunctionBasisExpansion ------------------------------
// ----------------------------------------------------------------------------

/**
 * Class to handle vectors in a basis of functions taylored to Master Integrals.
 *
 * An \f$L\f$ - Loop amplitude can be decompised into Master Integrals
 * \f$ \mathcal{I}_i \f$ and coefficients that encode the kinematics:
 *
 * \f[
 * \mathcal{A}^{(L)} = c_i \cdot \mathcal{I}^{(L)}_i.
 * \f]
 *
 * In general the Master integrals can be evaluated by brute force integration
 * but in order to perform a numerical reconstruction of the amplitude it is
 * beneficial to express each Master Integral as a vector in a space some
 * basis functions. The coefficients of these basis elements are then
 * Series expansions in the dimensional regulator \f$ \epsilon \f$:
 *
 * \f[
 * \mathcal{I}^{(L)}_i = b_{ij}(\epsilon)\, \mathcal{B}_j
 * \f]
 *
 * where \f$ \mathcal{B}_j \f$ are the basis functions.
 * The basis function type is by FType. The type T denotes the type of the
 * coefficients in the series expasion in the dimensional regulator. <b>
 *
 * In the application case of Master Integrals in Loop calculations in QFT
 * one would express the Master Integral in a basis of special functions
 * among which Logarithms, Polylogarithms, multiple Polylogarithms, generalized
 * Polylogarithms, Goncharov Polylogarithms or Elliptic Functions appear.
 */
template <typename CType, typename FType>
class FunctionBasisExpansion {
public:
    /// Default constructor
    FunctionBasisExpansion() = default;

    /// Default copy constructor
    FunctionBasisExpansion(const FunctionBasisExpansion<CType, FType>& other) = default;

    /// Default copy assignment operator.
    FunctionBasisExpansion<CType, FType>& operator=(const FunctionBasisExpansion<CType, FType>& other) = default;

    /**
     * Return the number of functions in the basis.
     * @return The number of functions in the basis.
     */
    unsigned long n_basis_funcs() const;

    /**
     * Check whether a given basis function is contained in the basis.
     * @param  basis_func The basis function the check is performed for.
     * @return            true if the requested function is in the basis, else false.
     */
    bool contains(const FType& basis_func) const;

    /**
     * Add a new basis function with its coefficient to the basis.
     *
     * NOTE: In the future, this function might become private or be replaced
     * by a dedicated constructor.
     *
     * @param basis_func The basis function to add.
     * @param coeff      The coefficient of the basis function.
     */
    void add_basis_element(const FType& basis_func, const CType& coeff);

    /**
     * Compute the numerical values of all the coefficients of the basis functions.
     *
     * @param args Contains the numerical values of the kinematic invariants used
     *             to evaluate the coefficients.
     * @return     A numerical function basis expansion that can be
     *             further processed.
     */
    NumericFunctionBasisExpansion<typename CType::output_type, FType>
    eval_coeffs(const std::vector<typename CType::input_type>& args) const;

    /**
     * Compute the full expression in the basis as linear combination of
     * coefficients times basis elements.
     *
     * If the basis corresponds to a Master Integral, this corresponds to
     * evaluating the Master Integral numerically.
     *
     * @param args Contains the numerical values of the kinematic invariants used
     *             to evaluate the coefficients.
     * @return     The numerical result of the linear combination of the numerical
     *             results of the coefficients and basis functions.
     */
    template <typename TT = typename FType::input_type>
    typename std::enable_if<!is_exact<TT>::value &&
                                std::is_same<typename FType::input_type, typename CType::input_type>::value,
                            typename CType::output_type>::type
    eval_complete(const std::vector<typename FType::input_type>& args) const;

private:
    std::unordered_map<FType, CType> basis{}; /**< Stores the coefficient for each basis function. */
};

/**
 * Typedef for most typical use case of NumericFunctionBasisExpansion, where the coefficients are in some field and the
 * StdBasisFunctions are evaluatable.
 */
template <typename TC, typename TF>
using SemiAnalyticExpression = NumericFunctionBasisExpansion<Series<TC>, StdBasisFunction<TF, TF>>;

} // namespace Integrals
} // namespace Caravel

// ----------------------------------------------------------------------------
// ------------------------- IMPLEMENTATIONS ----------------------------------
// ----------------------------------------------------------------------------
#include "FunctionBasisExpansion.hpp"

#endif // _FUNCTIONBASISEXPANSION_H_
