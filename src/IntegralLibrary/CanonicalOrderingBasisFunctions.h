#ifndef _CANONICAL_ORDERING_BASIS_FUNCTIONS_H_
#define _CANONICAL_ORDERING_BASIS_FUNCTIONS_H_

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include "Core/settings.h"

namespace Caravel {
namespace Integrals {

/**
 * Concentate the elements of a vector of strings to form a single string.
 *
 * By default a white space is added between the different components but any
 * other character of type "char" can be chosen.
 * @param  factors The vector containing the strings to be concatenated.
 * @param  sep     The separator character to be inserted between the different
 *                 elements in the concatenated string.
 * @return         A single string being the concatenation of the strings
 *                 contained in the input vector.
 */
std::string concatenate(const std::vector<std::string>& factors, char sep = ' ');

/**
 * Check whether a string of a function representation designates a Goncharov.
 * @param  function The string representation of the function.
 * @return          True if the function is a Goncharov, else false.
 */
bool is_goncharov(const std::string& function);

/**
 * Bring the arguments of a Goncharov into a canonical ordering.
 *
 * TODO: For now this only works when the arguments are single invariants
 * or linear combinations thereof.
 * @param  function A string representation of the function.
 * @return          A string representation of the function where the arguments
 *                  are in a canonical ordering.
 */
std::string clean_goncharov_args(const std::string& function);

/**
 * Bring basis functions into canonical ordering.
 *
 * This is required for the caching to work properly. Otherwise one will have
 * identical functions that will be considered as different.
 * <br>
 * Example: Assume an integral depends on two invariant 'arg1' and 'arg2' in a
 * for that is for example given by
 * \f[
 *  I_1 = \log(arg_1)\cdot \log(arg_2).
 * \f]
 * If we have two integrals, where one depends on the invariants 's' and 't'
 * and the other one depends on the invariants 't' and 's' then the hashstring
 * of the first integral will be
 * \f[
 *  \log(s) \cdot \log(t)
 * \f]
 * and the one of the second will be
 * \f[
 *  \log(t) \cdot \log(s)
 * \f]
 * Consequently, they will be considered as different functions, even  though
 * they are identical due to commutativity. This function therefore decomposes
 * the product of basis functions into individual basis functions and sorts
 * them such that they have a definite ordering and the above problem does not
 * appear.
 *
 * @param  hashstring The hashstring that is being brought into canonical ordering.
 * @return            The hashstring brought into canonical ordering.
 */
std::string canonical_ordering_hash_string(const std::string& hashstring);

} // namespace Integrals
} // namespace Caravel

#endif // _CANONICAL_ORDERING_BASIS_FUNCTIONS_H_
