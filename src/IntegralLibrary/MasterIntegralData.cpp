#include "IntegralLibrary/MasterIntegralData.h"

namespace Caravel {
namespace Integrals {
decltype(master_integral_data_holder) master_integral_data_holder;
} // namespace Integrals
} // namespace Caravel