#pragma once

#include <functional>
#include <stdexcept>
#include <string>
#include <vector>

#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/MasterIntegral.h"
#include "Core/Series.h"
// #include "Core/type_traits_extra.h"


namespace Caravel {
namespace Integrals {

/// Type for missing integrals that cannot be instantiated.
template <typename CType, typename FType> class NotImplementedIntegral : public MasterIntegral<CType, FType> {
  public:
    NotImplementedIntegral() {
        throw std::runtime_error("Integral is not available for the requested type!");
    }
};

} // namespace Integrals
} // namespace Caravel
