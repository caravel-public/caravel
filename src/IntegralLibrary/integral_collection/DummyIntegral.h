#ifndef _DUMMY_INTEGRAL_H_
#define _DUMMY_INTEGRAL_H_

#include <vector>
#include <string>
#include <stdexcept>
#include <functional>

#include "Core/Series.h"
#include "Core/type_traits_extra.h"
#include "IntegralLibrary/MasterIntegral.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/SpecialFunctions/access_special_functions.h"

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

/**
 * Master integral : DummyIntegral.
 *
 * A master integral consisting only of the basis function 1 with a Series
 * whose coefficients are all 0.
 */
template <typename CType, typename FType>
class DummyIntegral : public MasterIntegral<CType, FType> {
    
    typedef typename FType::input_type T;
    typedef typename CType::input_type TI;
    typedef typename FType::output_type Out;
    typedef typename CType::output_type OutC;
    using MasterIntegral<CType, FType>::abstract_basis;

public:
    /**
     * Construct the Master Integral.
     *
     * The user has to provide the names of the invariants in the integral.
     * This is important for correct caching!
     *
     * @param min_eps_power The requested minimal power of the integral.
     * @param max_eps_power The requested maximal power in the dimensional regulator
                            \f$ \epsilon \f$
     */
    DummyIntegral(int min_eps_power, int max_eps_power = 4, std::string name = "T(1)");

    /**
     * Get the number of loops related to the integral.
     * @return The number of loops.
     */
    std::size_t get_number_of_loops() const;

    /**
     * Get the leading power in epsilon.
     * @return The leading power in epsilon.
     */
    int get_leading_eps() const;

    /**
     * Get the last power in epsilon.
     * @return The highest power in epsilon.
     */
    int get_last_eps() const;

    /**
     * Check whether the integral was successfully tested numerically.
     * @return True if the integral was successfully tested numerically, else False.
     */
    bool successfully_tested_numerically() const;

private:
    /// The number of loops.
    std::size_t number_of_loops;

    /// The leading power in epsilon
    int leading_eps;

    /// The highest power in epsilon
    int last_eps;

    /// Track whether integral has been successfully numerically checked
    bool successful_numerical_test;
};

} // namespace Integrals
} // namespace Caravel

// Implementations
#include "DummyIntegral.hpp"

#endif // _DUMMY_INTEGRAL_H_
