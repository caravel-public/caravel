
namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace PentagonFunctions {

// ----------------------------------------------------------------------------
// OneMassBox_mu6_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
OneMassBox_mu6_Eu<CType, FType>::OneMassBox_mu6_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 2 || max_eps_power < 0){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(2)
        );
    }

    number_of_loops = 1;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = 0;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        T(1)
                    ;},
                "T(1)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        (TI(1)/TI(60))*(args[0]+args[1]+args[2]),
                        (TI(1)/TI(900))*(TI(1)/(args[0]+args[1]+-args[2]))*
(TI(31)*prodpow(args[0],2)+TI(77)*args[0]*args[1]+TI(31)*prodpow(args[1],2)+
TI(-31)*prodpow(args[2],2)),
                        (TI(1)/TI(6750))*(TI(1)/(args[0]+args[1]+-args[2]))*
(TI(443)*prodpow(args[0],2)+TI(1231)*args[0]*args[1]+TI(443)*prodpow(args[1],2)+
TI(-443)*prodpow(args[2],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)
                    ;},
                "prodpow(Pi<Out>,2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(180))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3)),
                        (TI(1)/TI(10800))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(-15)*prodpow(args[0],4)+TI(-60)*prodpow(args[0],3)*args[1]+
TI(-184)*prodpow(args[0],2)*prodpow(args[1],2)+
TI(-60)*args[0]*prodpow(args[1],3)+TI(-15)*prodpow(args[1],4)+
TI(30)*prodpow(args[0],3)*args[2]+TI(90)*prodpow(args[0],2)*args[1]*args[2]+
TI(90)*args[0]*prodpow(args[1],2)*args[2]+TI(30)*prodpow(args[1],3)*args[2]+
TI(-30)*args[0]*prodpow(args[2],3)+TI(-30)*args[1]*prodpow(args[2],3)+
TI(15)*prodpow(args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(60))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(-1)*prodpow(args[0],3)+TI(-3)*prodpow(args[0],2)*args[1]+
prodpow(args[0],2)*args[2]),
                        (TI(1)/TI(900))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(-31)*prodpow(args[0],3)+TI(-108)*prodpow(args[0],2)*args[1]+
TI(31)*prodpow(args[0],2)*args[2])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(180))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(60))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3)),
                        (TI(1)/TI(1800))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(15)*prodpow(args[0],4)+TI(60)*prodpow(args[0],3)*args[1]+
TI(-2)*prodpow(args[0],2)*prodpow(args[1],2)+TI(-30)*prodpow(args[0],3)*args[2]+
TI(-60)*prodpow(args[0],2)*args[1]*args[2]+
TI(15)*prodpow(args[0],2)*prodpow(args[2],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),3)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(180))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(60))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(-3)*args[0]*prodpow(args[1],2)+TI(-1)*prodpow(args[1],3)+
prodpow(args[1],2)*args[2]),
                        (TI(1)/TI(900))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(-108)*args[0]*prodpow(args[1],2)+TI(-31)*prodpow(args[1],3)+
TI(31)*prodpow(args[1],2)*args[2])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(180))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3)),
                        (TI(47)/TI(900))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[1])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(60))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(120))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(3)*args[0]*prodpow(args[1],2)+prodpow(args[1],3)+
TI(-1)*prodpow(args[1],2)*args[2])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(60))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),3)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(180))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(60))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(2)*args[0]*args[1]*args[2]+args[0]*prodpow(args[2],2)+
args[1]*prodpow(args[2],2)+TI(-1)*prodpow(args[2],3)),
                        (TI(1)/TI(900))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(77)*args[0]*args[1]*args[2]+TI(31)*args[0]*prodpow(args[2],2)+
TI(31)*args[1]*prodpow(args[2],2)+TI(-31)*prodpow(args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(180))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(60))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3)),
                        (TI(-47)/TI(900))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(60))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3)),
                        (TI(1)/TI(1800))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(47)*prodpow(args[0],2)*prodpow(args[1],2)+
TI(-30)*prodpow(args[0],2)*args[1]*args[2]+
TI(-30)*args[0]*prodpow(args[1],2)*args[2]+
TI(-15)*prodpow(args[0],2)*prodpow(args[2],2)+
TI(-15)*prodpow(args[1],2)*prodpow(args[2],2)+TI(30)*args[0]*prodpow(args[2],3)+
TI(30)*args[1]*prodpow(args[2],3)+TI(-15)*prodpow(args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[2]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[2]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),3)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(180))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_2_1<Out>(args[1],args[2])
                    ;},
                "pentagon_f_2_1<Out>(args[1],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3)),
                        (TI(-47)/TI(900))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[1],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[1],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_2_1<Out>(args[2],args[0])
                    ;},
                "pentagon_f_2_1<Out>(args[2],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3)),
                        (TI(47)/TI(900))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[2],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[2],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_1<Out>(args[1],args[2])
                    ;},
                "pentagon_f_3_1<Out>(args[1],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_1<Out>(args[2],args[0])
                    ;},
                "pentagon_f_3_1<Out>(args[2],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_2<Out>(args[1],args[2])
                    ;},
                "pentagon_f_3_2<Out>(args[1],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_2<Out>(args[2],args[0])
                    ;},
                "pentagon_f_3_2<Out>(args[2],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_3<Out>(args[0],args[1],args[2])
                    ;},
                "pentagon_f_3_3<Out>(args[0],args[1],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        zeta<Out>(3)
                    ;},
                "zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(30))*prodpow(args[0],2)*prodpow(args[1],2)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],3))
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t OneMassBox_mu6_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int OneMassBox_mu6_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int OneMassBox_mu6_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool OneMassBox_mu6_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace PentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel