#pragma once

#include "IntegralLibrary/MasterIntegralInputGenerator.h"
#include "FunctionSpace/MasterIntegrands.h"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace PentagonFunctions {
namespace InputEvaluator {

using FunctionSpace::MasterIntegrands::sq;
using FunctionSpace::MasterIntegrands::levi;
using FunctionSpace::MasterIntegrands::sign_tr5;

#include "IntegralInputImplementation.hpp"

} // namespace InputEvaluator
} // namespace PentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel