#ifdef USE_FINITE_FIELDS
#include "../Bubble_scalar_Eu.h"
#include "Bubble_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace PentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBBLE_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class Bubble_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBBLE_SCALAR_EU(F32, F32, C, R)

} // namespace PentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif