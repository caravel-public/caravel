#ifdef USE_FINITE_FIELDS
#include "../Bubble_mu2_Eu.h"
#include "Bubble_mu2_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace PentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBBLE_MU2_EU(COUT, CIN, FOUT, FIN)   \
    template class Bubble_mu2_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBBLE_MU2_EU(F32, F32, C, R)

} // namespace PentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif