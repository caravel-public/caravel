#include <iostream>

#include "IntegralLibrary/integral_collection/1_loop/PentagonFunctions/init.h"

#include "IntegralLibrary/MasterIntegralData.h"
#include "IntegralLibrary/MasterIntegralPointer.h"
#include "IntegralLibrary/integral_collection/1_loop/PentagonFunctions/PentagonFunctions.h"
#include "IntegralLibrary/integral_collection/1_loop/PentagonFunctions/IntegralInput.h"
#include "IntegralLibrary/integral_collection/NotImplementedIntegral.h"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace PentagonFunctions {

template <typename COUT, typename CIN> using Coeff = FBCoefficient<Series<COUT>, CIN>;

template <typename FOUT, typename FIN> using Func = StdBasisFunction<FOUT, FIN>;

void load() {
    using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
    std::cout<<"Loading 1-loop:PentagonFunctions integrals to MasterIntegralData map"<<std::endl;
#include "IntegralLibrary/integral_collection/1_loop/PentagonFunctions/init_helper.hpp"
}

} // namespace PentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel