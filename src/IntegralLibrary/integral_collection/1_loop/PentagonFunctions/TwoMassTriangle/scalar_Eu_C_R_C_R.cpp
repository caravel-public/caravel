#include "../TwoMassTriangle_scalar_Eu.h"
#include "TwoMassTriangle_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace PentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_TWOMASSTRIANGLE_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class TwoMassTriangle_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_TWOMASSTRIANGLE_SCALAR_EU(C, R, C, R)

} // namespace PentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel