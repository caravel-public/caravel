
namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace PentagonFunctions {

// ----------------------------------------------------------------------------
// MasslessPentagon_mu2_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
MasslessPentagon_mu2_Eu<CType, FType>::MasslessPentagon_mu2_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 2 || max_eps_power < 1){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(2)
        );
    }

    number_of_loops = 1;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = 1;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])
                    ;},
                "pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(-1)*(TI(1)/args[5]),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        TI(1)/args[5]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        TI(1)/args[5]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        TI(1)/args[5]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        TI(1)/args[5]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_4<Out>(args[0],args[1],args[2],args[3],args[4],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        TI(1)/args[5]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_10<Out>(args[0],args[1],args[2],args[3],args[4],args[6])
                    ;},
                "pentagon_f_4_10<Out>(args[0],args[1],args[2],args[3],args[4],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(15))*(TI(1)/args[5])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_10<Out>(args[1],args[2],args[3],args[4],args[0],args[6])
                    ;},
                "pentagon_f_4_10<Out>(args[1],args[2],args[3],args[4],args[0],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(15))*(TI(1)/args[5])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_10<Out>(args[2],args[3],args[4],args[0],args[1],args[6])
                    ;},
                "pentagon_f_4_10<Out>(args[2],args[3],args[4],args[0],args[1],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(15))*(TI(1)/args[5])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_10<Out>(args[3],args[4],args[0],args[1],args[2],args[6])
                    ;},
                "pentagon_f_4_10<Out>(args[3],args[4],args[0],args[1],args[2],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(15))*(TI(1)/args[5])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_10<Out>(args[4],args[0],args[1],args[2],args[3],args[6])
                    ;},
                "pentagon_f_4_10<Out>(args[4],args[0],args[1],args[2],args[3],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(15))*(TI(1)/args[5])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_12<Out>(args[0],args[1],args[2],args[3],args[4],args[6])
                    ;},
                "pentagon_f_4_12<Out>(args[0],args[1],args[2],args[3],args[4],args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(15))*(TI(1)/args[5])
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentw4Init<Out>(args[6])
                    ;},
                "pentw4Init<Out>(args[6])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 1, max_eps_power,
                        
                        TI(0),
                        TI(-1)*(TI(1)/args[5])
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t MasslessPentagon_mu2_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int MasslessPentagon_mu2_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int MasslessPentagon_mu2_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool MasslessPentagon_mu2_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace PentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel