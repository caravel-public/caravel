#include "../MasslessPentagon_mu2_Eu.h"
#include "MasslessPentagon_mu2_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace PentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSPENTAGON_MU2_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessPentagon_mu2_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSPENTAGON_MU2_EU(C, C, C, C)

} // namespace PentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel