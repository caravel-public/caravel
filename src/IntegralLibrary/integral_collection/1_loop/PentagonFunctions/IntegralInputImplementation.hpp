
template <typename T>
std::vector<T> Bubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1])
    });
}

template <typename T>
std::vector<T> MasslessPentagon(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(4, 5),
mc.s(1, 5),
T(4)*levi(mc[1], mc[2], mc[3], mc[4]),
sign_tr5(mc[1], mc[2], mc[3], mc[4])
    });
}

template <typename T>
std::vector<T> OneMassBox(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
sq(mc[4])
    });
}

template <typename T>
std::vector<T> OneMassTriangle(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1])
    });
}

template <typename T>
std::vector<T> TwoMassTriangle(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1]),
sq(mc[2])
    });
}
