#ifdef HIGH_PRECISION
#include "../OneMassBox_scalar_All.h"
#include "OneMassBox_scalar_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace OneLoopPentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSBOX_SCALAR_ALL(COUT, CIN, FOUT, FIN)   \
    template class OneMassBox_scalar_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSBOX_SCALAR_ALL(CHP, RHP, CHP, RHP)
INSTANTIATE_ONEMASSBOX_SCALAR_ALL(CHP, CHP, CHP, CHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_ONEMASSBOX_SCALAR_ALL(F32, F32, CHP, RHP)
INSTANTIATE_ONEMASSBOX_SCALAR_ALL(F32, F32, CHP, CHP)
#endif

} // namespace OneLoopPentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif