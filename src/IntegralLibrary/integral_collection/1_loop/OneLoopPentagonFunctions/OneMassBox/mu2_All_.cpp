#include "../OneMassBox_mu2_All.h"
#include "OneMassBox_mu2_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace OneLoopPentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSBOX_MU2_ALL(COUT, CIN, FOUT, FIN)   \
    template class OneMassBox_mu2_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSBOX_MU2_ALL(C, R, C, R)
INSTANTIATE_ONEMASSBOX_MU2_ALL(C, C, C, C)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_ONEMASSBOX_MU2_ALL(F32, F32, C, R)
INSTANTIATE_ONEMASSBOX_MU2_ALL(F32, F32, C, C)
#endif

} // namespace OneLoopPentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel