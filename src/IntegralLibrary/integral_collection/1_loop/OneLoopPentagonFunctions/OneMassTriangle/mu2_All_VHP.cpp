#ifdef HIGH_PRECISION
#include "../OneMassTriangle_mu2_All.h"
#include "OneMassTriangle_mu2_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace OneLoopPentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSTRIANGLE_MU2_ALL(COUT, CIN, FOUT, FIN)   \
    template class OneMassTriangle_mu2_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSTRIANGLE_MU2_ALL(CVHP, RVHP, CVHP, RVHP)
INSTANTIATE_ONEMASSTRIANGLE_MU2_ALL(CVHP, CVHP, CVHP, CVHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_ONEMASSTRIANGLE_MU2_ALL(F32, F32, CVHP, RVHP)
INSTANTIATE_ONEMASSTRIANGLE_MU2_ALL(F32, F32, CVHP, CVHP)
#endif

} // namespace OneLoopPentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif