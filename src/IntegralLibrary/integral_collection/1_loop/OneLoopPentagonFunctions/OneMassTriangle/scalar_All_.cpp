#include "../OneMassTriangle_scalar_All.h"
#include "OneMassTriangle_scalar_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace OneLoopPentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSTRIANGLE_SCALAR_ALL(COUT, CIN, FOUT, FIN)   \
    template class OneMassTriangle_scalar_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSTRIANGLE_SCALAR_ALL(C, R, C, R)
INSTANTIATE_ONEMASSTRIANGLE_SCALAR_ALL(C, C, C, C)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_ONEMASSTRIANGLE_SCALAR_ALL(F32, F32, C, R)
INSTANTIATE_ONEMASSTRIANGLE_SCALAR_ALL(F32, F32, C, C)
#endif

} // namespace OneLoopPentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel