#include <iostream>

#include "IntegralLibrary/integral_collection/1_loop/OneLoopPentagonFunctions/init.h"

#include "IntegralLibrary/MasterIntegralData.h"
#include "IntegralLibrary/MasterIntegralPointer.h"
#include "IntegralLibrary/integral_collection/1_loop/OneLoopPentagonFunctions/OneLoopPentagonFunctions.h"
#include "IntegralLibrary/integral_collection/1_loop/OneLoopPentagonFunctions/IntegralInput.h"
#include "IntegralLibrary/integral_collection/NotImplementedIntegral.h"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace OneLoopPentagonFunctions {

template <typename COUT, typename CIN> using Coeff = FBCoefficient<Series<COUT>, CIN>;

template <typename FOUT, typename FIN> using Func = StdBasisFunction<FOUT, FIN>;

void load() {
    using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
    std::cout<<"Loading 1-loop:OneLoopPentagonFunctions integrals to MasterIntegralData map"<<std::endl;
#include "IntegralLibrary/integral_collection/1_loop/OneLoopPentagonFunctions/init_helper.hpp"
}

} // namespace OneLoopPentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel