{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[], Bead[Leg[2]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Bubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Bubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Bubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Bubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Bubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Bubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Bubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_scalar_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[], Bead[Leg[2]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Bubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Bubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Bubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Bubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Bubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Bubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Bubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Bubble_mu2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[], Bead[Leg[4]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_scalar_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[], Bead[Leg[4]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[], Bead[Leg[4]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return OneMassBox_mu4_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu4"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_scalar_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return OneMassTriangle_mu2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[], Bead[Leg[4, 0]], Link[], Bead[Leg[5, 0]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Pentagon<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Pentagon<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Pentagon<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Pentagon<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Pentagon<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Pentagon<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Pentagon<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return Pentagon_mu2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Tr5,{1, 2, 3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TwoMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TwoMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TwoMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TwoMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TwoMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TwoMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TwoMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_scalar_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3]], Link[LoopMomentum[1]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TwoMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TwoMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TwoMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TwoMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TwoMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TwoMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TwoMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TwoMassTriangle_mu2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}
