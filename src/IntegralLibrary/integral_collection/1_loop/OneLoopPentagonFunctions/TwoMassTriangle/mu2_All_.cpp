#include "../TwoMassTriangle_mu2_All.h"
#include "TwoMassTriangle_mu2_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace OneLoopPentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_TWOMASSTRIANGLE_MU2_ALL(COUT, CIN, FOUT, FIN)   \
    template class TwoMassTriangle_mu2_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_TWOMASSTRIANGLE_MU2_ALL(C, R, C, R)
INSTANTIATE_TWOMASSTRIANGLE_MU2_ALL(C, C, C, C)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_TWOMASSTRIANGLE_MU2_ALL(F32, F32, C, R)
INSTANTIATE_TWOMASSTRIANGLE_MU2_ALL(F32, F32, C, C)
#endif

} // namespace OneLoopPentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel