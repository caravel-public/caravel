#ifdef HIGH_PRECISION
#include "../Bubble_scalar_All.h"
#include "Bubble_scalar_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace OneLoopPentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBBLE_SCALAR_ALL(COUT, CIN, FOUT, FIN)   \
    template class Bubble_scalar_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBBLE_SCALAR_ALL(CHP, RHP, CHP, RHP)
INSTANTIATE_BUBBLE_SCALAR_ALL(CHP, CHP, CHP, CHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_BUBBLE_SCALAR_ALL(F32, F32, CHP, RHP)
INSTANTIATE_BUBBLE_SCALAR_ALL(F32, F32, CHP, CHP)
#endif

} // namespace OneLoopPentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif