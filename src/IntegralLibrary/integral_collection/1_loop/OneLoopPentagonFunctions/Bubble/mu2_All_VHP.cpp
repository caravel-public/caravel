#ifdef HIGH_PRECISION
#include "../Bubble_mu2_All.h"
#include "Bubble_mu2_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace OneLoopPentagonFunctions {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBBLE_MU2_ALL(COUT, CIN, FOUT, FIN)   \
    template class Bubble_mu2_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBBLE_MU2_ALL(CVHP, RVHP, CVHP, RVHP)
INSTANTIATE_BUBBLE_MU2_ALL(CVHP, CVHP, CVHP, CVHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_BUBBLE_MU2_ALL(F32, F32, CVHP, RVHP)
INSTANTIATE_BUBBLE_MU2_ALL(F32, F32, CVHP, CVHP)
#endif

} // namespace OneLoopPentagonFunctions
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif