#ifdef USE_FINITE_FIELDS
#include "../TwoMassTriangle_mu4_Eu.h"
#include "TwoMassTriangle_mu4_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_TWOMASSTRIANGLE_MU4_EU(COUT, CIN, FOUT, FIN)   \
    template class TwoMassTriangle_mu4_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_TWOMASSTRIANGLE_MU4_EU(F32, F32, C, R)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif