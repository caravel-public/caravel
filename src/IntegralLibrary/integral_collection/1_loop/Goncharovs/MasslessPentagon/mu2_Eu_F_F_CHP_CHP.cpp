#ifdef USE_FINITE_FIELDS
#ifdef HIGH_PRECISION
#include "../MasslessPentagon_mu2_Eu.h"
#include "MasslessPentagon_mu2_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSPENTAGON_MU2_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessPentagon_mu2_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSPENTAGON_MU2_EU(F32, F32, CHP, CHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif
#endif