#ifdef USE_FINITE_FIELDS
#include "../OneMassBox_scalar_Eu.h"
#include "OneMassBox_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSBOX_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class OneMassBox_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSBOX_SCALAR_EU(F32, F32, C, C)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif