#ifdef VERY_HIGH_PRECISION
#include "../OneMassBox_mu6_Eu.h"
#include "OneMassBox_mu6_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSBOX_MU6_EU(COUT, CIN, FOUT, FIN)   \
    template class OneMassBox_mu6_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSBOX_MU6_EU(CVHP, CVHP, CVHP, CVHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif