
namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// OneMassBox_mu8_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
OneMassBox_mu8_Eu<CType, FType>::OneMassBox_mu8_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 2 || max_eps_power < 0){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(2)
        );
    }

    number_of_loops = 1;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = 0;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        T(1)
                    ;},
                "T(1)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        (TI(1)/TI(840))*
(TI(-2)*prodpow(args[0],2)+TI(-1)*args[0]*args[1]+TI(-2)*prodpow(args[1],2)+
TI(-2)*args[0]*args[2]+TI(-2)*args[1]*args[2]+TI(-2)*prodpow(args[2],2)),
                        (TI(1)/TI(88200))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(-389)*prodpow(args[0],4)+TI(-1025)*prodpow(args[0],3)*args[1]+
TI(-1587)*prodpow(args[0],2)*prodpow(args[1],2)+
TI(-1025)*args[0]*prodpow(args[1],3)+TI(-389)*prodpow(args[1],4)+
TI(389)*prodpow(args[0],3)*args[2]+TI(389)*prodpow(args[1],3)*args[2]+
TI(636)*args[0]*args[1]*prodpow(args[2],2)+TI(389)*args[0]*prodpow(args[2],3)+
TI(389)*args[1]*prodpow(args[2],3)+TI(-389)*prodpow(args[2],4)),
                        (TI(1)/TI(9261000))*(TI(1)/prodpow(args[0]+args[1]+-args[2],2))*
(TI(-75713)*prodpow(args[0],4)+TI(-202250)*prodpow(args[0],3)*args[1]+
TI(-352929)*prodpow(args[0],2)*prodpow(args[1],2)+
TI(-202250)*args[0]*prodpow(args[1],3)+TI(-75713)*prodpow(args[1],4)+
TI(75713)*prodpow(args[0],3)*args[2]+TI(75713)*prodpow(args[1],3)*args[2]+
TI(126537)*args[0]*args[1]*prodpow(args[2],2)+
TI(75713)*args[0]*prodpow(args[2],3)+TI(75713)*args[1]*prodpow(args[2],3)+
TI(-75713)*prodpow(args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)
                    ;},
                "prodpow(Pi<Out>,2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(280))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4)),
                        (TI(1)/TI(352800))*(TI(1)/prodpow(args[0]+args[1]+-args[2],4))*
(TI(70)*prodpow(args[0],6)+TI(315)*prodpow(args[0],5)*args[1]+
TI(630)*prodpow(args[0],4)*prodpow(args[1],2)+
TI(2684)*prodpow(args[0],3)*prodpow(args[1],3)+
TI(630)*prodpow(args[0],2)*prodpow(args[1],4)+TI(315)*args[0]*prodpow(args[1],5)+
TI(70)*prodpow(args[1],6)+TI(-210)*prodpow(args[0],5)*args[2]+
TI(-630)*prodpow(args[0],4)*args[1]*args[2]+
TI(-840)*prodpow(args[0],3)*prodpow(args[1],2)*args[2]+
TI(-840)*prodpow(args[0],2)*prodpow(args[1],3)*args[2]+
TI(-630)*args[0]*prodpow(args[1],4)*args[2]+TI(-210)*prodpow(args[1],5)*args[2]+
TI(210)*prodpow(args[0],4)*prodpow(args[2],2)+
TI(210)*prodpow(args[0],3)*args[1]*prodpow(args[2],2)+
TI(210)*args[0]*prodpow(args[1],3)*prodpow(args[2],2)+
TI(210)*prodpow(args[1],4)*prodpow(args[2],2)+
TI(-140)*prodpow(args[0],3)*prodpow(args[2],3)+
TI(-140)*prodpow(args[1],3)*prodpow(args[2],3)+
TI(210)*prodpow(args[0],2)*prodpow(args[2],4)+
TI(315)*args[0]*args[1]*prodpow(args[2],4)+
TI(210)*prodpow(args[1],2)*prodpow(args[2],4)+
TI(-210)*args[0]*prodpow(args[2],5)+TI(-210)*args[1]*prodpow(args[2],5)+
TI(70)*prodpow(args[2],6))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(840))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(2)*prodpow(args[0],5)+TI(7)*prodpow(args[0],4)*args[1]+
TI(11)*prodpow(args[0],3)*prodpow(args[1],2)+TI(-4)*prodpow(args[0],4)*args[2]+
TI(-7)*prodpow(args[0],3)*args[1]*args[2]+
TI(2)*prodpow(args[0],3)*prodpow(args[2],2)),
                        (TI(1)/TI(88200))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(389)*prodpow(args[0],5)+TI(1414)*prodpow(args[0],4)*args[1]+
TI(2612)*prodpow(args[0],3)*prodpow(args[1],2)+
TI(-778)*prodpow(args[0],4)*args[2]+
TI(-1414)*prodpow(args[0],3)*args[1]*args[2]+
TI(389)*prodpow(args[0],3)*prodpow(args[2],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(280))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(840))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(11)*prodpow(args[0],2)*prodpow(args[1],3)+TI(7)*args[0]*prodpow(args[1],4)+
TI(2)*prodpow(args[1],5)+TI(-7)*args[0]*prodpow(args[1],3)*args[2]+
TI(-4)*prodpow(args[1],4)*args[2]+TI(2)*prodpow(args[1],3)*prodpow(args[2],2)),
                        (TI(1)/TI(88200))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(2612)*prodpow(args[0],2)*prodpow(args[1],3)+
TI(1414)*args[0]*prodpow(args[1],4)+TI(389)*prodpow(args[1],5)+
TI(-1414)*args[0]*prodpow(args[1],3)*args[2]+
TI(-778)*prodpow(args[1],4)*args[2]+TI(389)*prodpow(args[1],3)*prodpow(args[2],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4)),
                        (TI(-319)/TI(29400))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[2])
                    ;},
                "G<Out>(T(0),T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(840))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(-6)*prodpow(args[0],2)*prodpow(args[1],2)*args[2]+
TI(-3)*prodpow(args[0],2)*args[1]*prodpow(args[2],2)+
TI(-3)*args[0]*prodpow(args[1],2)*prodpow(args[2],2)+
TI(-2)*prodpow(args[0],2)*prodpow(args[2],3)+
TI(-1)*args[0]*args[1]*prodpow(args[2],3)+
TI(-2)*prodpow(args[1],2)*prodpow(args[2],3)+TI(4)*args[0]*prodpow(args[2],4)+
TI(4)*args[1]*prodpow(args[2],4)+TI(-2)*prodpow(args[2],5)),
                        (TI(1)/TI(88200))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(-1587)*prodpow(args[0],2)*prodpow(args[1],2)*args[2]+
TI(-636)*prodpow(args[0],2)*args[1]*prodpow(args[2],2)+
TI(-636)*args[0]*prodpow(args[1],2)*prodpow(args[2],2)+
TI(-389)*prodpow(args[0],2)*prodpow(args[2],3)+
TI(-142)*args[0]*args[1]*prodpow(args[2],3)+
TI(-389)*prodpow(args[1],2)*prodpow(args[2],3)+
TI(778)*args[0]*prodpow(args[2],4)+TI(778)*args[1]*prodpow(args[2],4)+
TI(-389)*prodpow(args[2],5))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*G<Out>(T(-1)*args[0],T(-1)*args[2])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*G<Out>(T(-1)*args[0],T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4)),
                        (TI(319)/TI(29400))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[2])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(280))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])*G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[2])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])*G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(T(-1)*args[1],T(-1)*args[2])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(T(-1)*args[1],T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4)),
                        (TI(319)/TI(29400))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(args[1],T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(args[1],T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(280))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4)),
                        (TI(1)/TI(29400))*(TI(1)/prodpow(args[0]+args[1]+-args[2],4))*
(TI(-70)*prodpow(args[0],6)+TI(-315)*prodpow(args[0],5)*args[1]+
TI(-630)*prodpow(args[0],4)*prodpow(args[1],2)+
TI(-66)*prodpow(args[0],3)*prodpow(args[1],3)+TI(210)*prodpow(args[0],5)*args[2]+
TI(630)*prodpow(args[0],4)*args[1]*args[2]+
TI(630)*prodpow(args[0],3)*prodpow(args[1],2)*args[2]+
TI(-210)*prodpow(args[0],4)*prodpow(args[2],2)+
TI(-315)*prodpow(args[0],3)*args[1]*prodpow(args[2],2)+
TI(70)*prodpow(args[0],3)*prodpow(args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[0],T(-1)*args[2])*G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(-1)*args[0],T(-1)*args[2])*G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[2])*G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[2])*G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4)),
                        (TI(1)/TI(29400))*(TI(1)/prodpow(args[0]+args[1]+-args[2],4))*
(TI(-66)*prodpow(args[0],3)*prodpow(args[1],3)+
TI(-630)*prodpow(args[0],2)*prodpow(args[1],4)+
TI(-315)*args[0]*prodpow(args[1],5)+TI(-70)*prodpow(args[1],6)+
TI(630)*prodpow(args[0],2)*prodpow(args[1],3)*args[2]+
TI(630)*args[0]*prodpow(args[1],4)*args[2]+TI(210)*prodpow(args[1],5)*args[2]+
TI(-315)*args[0]*prodpow(args[1],3)*prodpow(args[2],2)+
TI(-210)*prodpow(args[1],4)*prodpow(args[2],2)+
TI(70)*prodpow(args[1],3)*prodpow(args[2],3))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[2])*G<Out>(T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[2])*G<Out>(T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[1],T(-1)*args[2])*G<Out>(T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(-1)*args[1],T(-1)*args[2])*G<Out>(T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[2])
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(840))*(TI(1)/prodpow(args[0]+args[1]+-args[2],3))*
(TI(6)*prodpow(args[0],2)*prodpow(args[1],2)*args[2]+
TI(3)*prodpow(args[0],2)*args[1]*prodpow(args[2],2)+
TI(3)*args[0]*prodpow(args[1],2)*prodpow(args[2],2)+
TI(2)*prodpow(args[0],2)*prodpow(args[2],3)+args[0]*args[1]*prodpow(args[2],3)+
TI(2)*prodpow(args[1],2)*prodpow(args[2],3)+TI(-4)*args[0]*prodpow(args[2],4)+
TI(-4)*args[1]*prodpow(args[2],4)+TI(2)*prodpow(args[2],5))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[0],T(0),T(-1)*args[2])
                    ;},
                "G<Out>(T(-1)*args[0],T(0),T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4)),
                        (TI(-319)/TI(29400))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[0],T(-1)*args[2])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[0],T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[1],T(-1)*args[2])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[1],T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[1],T(0),T(-1)*args[2])
                    ;},
                "G<Out>(T(-1)*args[1],T(0),T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4)),
                        (TI(-319)/TI(29400))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(args[1],T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(args[1],T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(70))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[0],T(0),T(0),T(-1)*args[2])
                    ;},
                "G<Out>(T(-1)*args[0],T(0),T(0),T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[0],T(0),T(-1)*args[2])
                    ;},
                "G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[0],T(0),T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[1],T(0),T(-1)*args[2])
                    ;},
                "G<Out>(T(-1)*args[0]+T(-1)*args[1],T(-1)*args[1],T(0),T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(-1)*args[1],T(0),T(0),T(-1)*args[2])
                    ;},
                "G<Out>(T(-1)*args[1],T(0),T(0),T(-1)*args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(args[1],T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        zeta<Out>(3)
                    ;},
                "zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(140))*prodpow(args[0],3)*prodpow(args[1],3)*
(TI(1)/prodpow(args[0]+args[1]+-args[2],4))
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t OneMassBox_mu8_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int OneMassBox_mu8_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int OneMassBox_mu8_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool OneMassBox_mu8_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel