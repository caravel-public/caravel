#ifdef USE_FINITE_FIELDS
#ifdef VERY_HIGH_PRECISION
#include "../Bubble_mu4_Eu.h"
#include "Bubble_mu4_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBBLE_MU4_EU(COUT, CIN, FOUT, FIN)   \
    template class Bubble_mu4_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBBLE_MU4_EU(F32, F32, CVHP, CVHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif
#endif