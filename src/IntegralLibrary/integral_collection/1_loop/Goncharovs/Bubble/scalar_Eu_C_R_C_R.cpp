#include "../Bubble_scalar_Eu.h"
#include "Bubble_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBBLE_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class Bubble_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBBLE_SCALAR_EU(C, R, C, R)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel