#ifdef VERY_HIGH_PRECISION
#include "../MasslessBox_mu8_Eu.h"
#include "MasslessBox_mu8_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSBOX_MU8_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessBox_mu8_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSBOX_MU8_EU(CVHP, RVHP, CVHP, RVHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif