#include "../MasslessBox_scalar_Eu.h"
#include "MasslessBox_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSBOX_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessBox_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSBOX_SCALAR_EU(C, C, C, C)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel