#ifdef HIGH_PRECISION
#include "../MasslessBox_mu4_Eu.h"
#include "MasslessBox_mu4_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSBOX_MU4_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessBox_mu4_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSBOX_MU4_EU(CHP, CHP, CHP, CHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif