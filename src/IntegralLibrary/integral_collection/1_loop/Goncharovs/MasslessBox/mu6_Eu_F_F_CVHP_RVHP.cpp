#ifdef USE_FINITE_FIELDS
#ifdef VERY_HIGH_PRECISION
#include "../MasslessBox_mu6_Eu.h"
#include "MasslessBox_mu6_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSBOX_MU6_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessBox_mu6_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSBOX_MU6_EU(F32, F32, CVHP, RVHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif
#endif