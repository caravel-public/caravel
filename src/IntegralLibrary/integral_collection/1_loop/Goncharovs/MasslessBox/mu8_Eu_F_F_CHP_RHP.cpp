#ifdef USE_FINITE_FIELDS
#ifdef HIGH_PRECISION
#include "../MasslessBox_mu8_Eu.h"
#include "MasslessBox_mu8_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSBOX_MU8_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessBox_mu8_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSBOX_MU8_EU(F32, F32, CHP, RHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif
#endif