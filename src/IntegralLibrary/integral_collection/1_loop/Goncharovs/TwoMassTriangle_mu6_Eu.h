/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#pragma once

#include <vector>
#include <string>
#include <stdexcept>
#include <functional>

#include "Core/Series.h"
#include "Core/type_traits_extra.h"
#include "IntegralLibrary/MasterIntegral.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/SpecialFunctions/access_special_functions.h"

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

/**
 * Master integral : TwoMassTriangle_mu6_Eu.
 *
 * It corresponds to a 1 - loop integral.
 * <br>
 * Date of Json file generation from Mathematica:
 * 09 January 2020
 * <br>
 * The maximal power of \f$ \epsilon \f$ is: 2.
 * <br>
 * The integral has 6 basis functions.
 * <br>
 * mu6 insertion for triangle with 2 massive external legs and massless propagators.  
 * <br>
 * The normalisation is:  
 * <br>
 *  \f[ e^{\gamma_E \epsilon} \int \frac{d^Dk}{i\pi^{\frac{D}{2}}} \f] 
 * <br>
 * Graph info: CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[], Leg[3, 0], Link[]]]] 
 * <br>
 * Variables: sq[p[1]], sq[p[2]] 
 * <br>
 * Insertion name: mu6 
 * <br>
 * Insertion: 
 * <br>
 *               3 
 * <br>
 * mu[l[1], l[1]] 
 * <br>
 * Region: Eu 
 * <br>
 * Signs of invariants for this region: InvariantSigns::negative, InvariantSigns::negative
 * <br>
 * For further information consider the comment at the top of the header file.
 */
template <typename CType, typename FType>
class TwoMassTriangle_mu6_Eu : public MasterIntegral<CType, FType> {
    typedef typename FType::input_type T;
    typedef typename CType::input_type TI;
    typedef typename FType::output_type Out;
    typedef typename CType::output_type OutC;
    using MasterIntegral<CType, FType>::abstract_basis;

public:
    /**
     * Construct the Master Integral.
     *
     * The user has to provide the names of the invariants in the integral.
     * This is important for correct caching!
     *
     * @param The names for the variables in the integral.
     * @param The requested maximal power in the dimensional regulator
              \f$ \epsilon \f$
     */
    TwoMassTriangle_mu6_Eu(const std::vector<std::string>& invariant_names, int max_eps_power = 2);

    /**
     * Get the number of loops related to the integral.
     * @return The number of loops.
     */
    std::size_t get_number_of_loops() const;

    /**
     * Get the leading power in epsilon.
     * @return The leading power in epsilon.
     */
    int get_leading_eps() const;

    /**
     * Get the last power in epsilon.
     * @return The highest power in epsilon.
     */
    int get_last_eps() const;

    /**
     * Check whether the integral was successfully tested numerically.
     * @return True if the integral was successfully tested numerically, else False.
     */
    bool successfully_tested_numerically() const;

private:
    /// The number of loops.
    std::size_t number_of_loops;

    /// The leading power in epsilon
    int leading_eps;

    /// The highest power in epsilon
    int last_eps;

    /// Track whether integral has been successfully numerically checked
    bool successful_numerical_test;
};

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
