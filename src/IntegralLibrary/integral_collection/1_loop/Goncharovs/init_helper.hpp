{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Bubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Bubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Bubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Bubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Bubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Bubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Bubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Bubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Bubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Bubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Bubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Bubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Bubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Bubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu2_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Bubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Bubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Bubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Bubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Bubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Bubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Bubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bubble_mu4_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu4"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu2_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu4_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu4"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu6_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu6"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessBox_mu8_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu8"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[], Leg[5, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessPentagon<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessPentagon<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessPentagon<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessPentagon<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessPentagon<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessPentagon<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessPentagon<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessPentagon_mu2_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Twistor5,{0}),InputVariable(InvariantType::Twistor5,{1}),InputVariable(InvariantType::Twistor5,{2}),InputVariable(InvariantType::Twistor5,{3}),InputVariable(InvariantType::Twistor5,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu2_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu4_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu4"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu6_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu6"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return OneMassBox_mu8_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu8"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu4"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::OneMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::OneMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::OneMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::OneMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::OneMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::OneMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::OneMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return OneMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu6"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TwoMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TwoMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TwoMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TwoMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TwoMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TwoMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TwoMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TwoMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TwoMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TwoMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TwoMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TwoMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TwoMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TwoMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu2_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TwoMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TwoMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TwoMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TwoMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TwoMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TwoMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TwoMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu4_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu4"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]]], Connection[Strand[LoopMomentum[1], Leg[2], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TwoMassTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TwoMassTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TwoMassTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TwoMassTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TwoMassTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TwoMassTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TwoMassTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return TwoMassTriangle_mu6_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu6"))), data});

}