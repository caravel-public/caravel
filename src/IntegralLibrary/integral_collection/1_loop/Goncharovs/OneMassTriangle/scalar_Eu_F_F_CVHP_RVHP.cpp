#ifdef USE_FINITE_FIELDS
#ifdef VERY_HIGH_PRECISION
#include "../OneMassTriangle_scalar_Eu.h"
#include "OneMassTriangle_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSTRIANGLE_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class OneMassTriangle_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSTRIANGLE_SCALAR_EU(F32, F32, CVHP, RVHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif
#endif