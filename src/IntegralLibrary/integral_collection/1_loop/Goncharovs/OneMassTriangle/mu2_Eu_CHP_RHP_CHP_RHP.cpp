#ifdef HIGH_PRECISION
#include "../OneMassTriangle_mu2_Eu.h"
#include "OneMassTriangle_mu2_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSTRIANGLE_MU2_EU(COUT, CIN, FOUT, FIN)   \
    template class OneMassTriangle_mu2_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSTRIANGLE_MU2_EU(CHP, RHP, CHP, RHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif