
namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// OneMassTriangle_mu2_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
OneMassTriangle_mu2_Eu<CType, FType>::OneMassTriangle_mu2_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 2 || max_eps_power < 0){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(2)
        );
    }

    number_of_loops = 1;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = 0;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        T(1)
                    ;},
                "T(1)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-1)/TI(2),
                        TI(-3)/TI(2),
                        TI(-7)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)
                    ;},
                "prodpow(Pi<Out>,2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t OneMassTriangle_mu2_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int OneMassTriangle_mu2_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int OneMassTriangle_mu2_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool OneMassTriangle_mu2_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel