#ifdef USE_FINITE_FIELDS
#ifdef VERY_HIGH_PRECISION
#include "../OneMassTriangle_mu4_Eu.h"
#include "OneMassTriangle_mu4_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSTRIANGLE_MU4_EU(COUT, CIN, FOUT, FIN)   \
    template class OneMassTriangle_mu4_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSTRIANGLE_MU4_EU(F32, F32, CVHP, CVHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif
#endif