#ifdef USE_FINITE_FIELDS
#ifdef HIGH_PRECISION
#include "../OneMassTriangle_mu6_Eu.h"
#include "OneMassTriangle_mu6_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_1 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_ONEMASSTRIANGLE_MU6_EU(COUT, CIN, FOUT, FIN)   \
    template class OneMassTriangle_mu6_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_ONEMASSTRIANGLE_MU6_EU(F32, F32, CHP, RHP)

} // namespace Goncharovs
} // namespace loop_1
} // namespace Integrals
} // namespace Caravel
#endif
#endif