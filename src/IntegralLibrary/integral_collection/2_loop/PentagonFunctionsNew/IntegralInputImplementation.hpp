
template <typename T>
std::vector<T> BoxBoxSemiSimple(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(1, 5),
mc.s(4, 5)
    });
}

template <typename T>
std::vector<T> BoxBoxSimple(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1]),
mc.s(2, 3),
mc.s(3, 4)
    });
}

template <typename T>
std::vector<T> BoxBubble1(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[2]),
mc.s(2, 4),
mc.s(3, 4)
    });
}

template <typename T>
std::vector<T> BoxBubble2(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1]),
mc.s(2, 3),
mc.s(3, 4)
    });
}

template <typename T>
std::vector<T> BoxTriangleGeneric(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(1, 5),
mc.s(4, 5)
    });
}

template <typename T>
std::vector<T> BoxTriangleSemisimple(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[2]),
mc.s(2, 4),
mc.s(3, 4)
    });
}

template <typename T>
std::vector<T> FactorizedBoxBubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[4]),
mc.s(3, 4),
mc.s(2, 3)
    });
}

template <typename T>
std::vector<T> FactorizedBubbleBubbleGeneric(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[2]),
sq(mc[3])
    });
}

template <typename T>
std::vector<T> FactorizedBubbleBubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[2])
    });
}

template <typename T>
std::vector<T> PentagonBox(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(1, 5),
mc.s(4, 5)
    });
}

template <typename T>
std::vector<T> PentagonBubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(1, 5),
mc.s(4, 5)
    });
}

template <typename T>
std::vector<T> Sunrise(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1])
    });
}

template <typename T>
std::vector<T> TriangleBubble1(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1]),
sq(mc[3])
    });
}

template <typename T>
std::vector<T> TriangleBubble2(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(2, 3)
    });
}

template <typename T>
std::vector<T> TriangleTriangle2MastersDot(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[4]),
mc.s(3, 4),
mc.s(2, 3)
    });
}

template <typename T>
std::vector<T> TriangleTriangleGeneric1Master(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1]),
mc.s(2, 3),
mc.s(3, 4)
    });
}

template <typename T>
std::vector<T> TriangleTriangle(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[2]),
sq(mc[3])
    });
}
