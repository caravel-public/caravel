
namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// PentagonBox_1_All
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
PentagonBox_1_All<CType, FType>::PentagonBox_1_All(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 4 || max_eps_power < 0){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(4)
        );
    }

    number_of_loops = 2;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = 0;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        MI<Out>("PentagonBox",1,0,args[0],args[1],args[2],args[3],args[4])
                    ;},
                "MI\u003cOut\u003e(\"PentagonBox\",1,0,args[0],args[1],args[2],args[3],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(1),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        MI<Out>("PentagonBox",1,1,args[0],args[1],args[2],args[3],args[4])
                    ;},
                "MI\u003cOut\u003e(\"PentagonBox\",1,1,args[0],args[1],args[2],args[3],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        MI<Out>("PentagonBox",1,2,args[0],args[1],args[2],args[3],args[4])
                    ;},
                "MI\u003cOut\u003e(\"PentagonBox\",1,2,args[0],args[1],args[2],args[3],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        MI<Out>("PentagonBox",1,3,args[0],args[1],args[2],args[3],args[4])
                    ;},
                "MI\u003cOut\u003e(\"PentagonBox\",1,3,args[0],args[1],args[2],args[3],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        MI<Out>("PentagonBox",1,4,args[0],args[1],args[2],args[3],args[4])
                    ;},
                "MI\u003cOut\u003e(\"PentagonBox\",1,4,args[0],args[1],args[2],args[3],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(1)
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t PentagonBox_1_All<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int PentagonBox_1_All<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int PentagonBox_1_All<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool PentagonBox_1_All<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel