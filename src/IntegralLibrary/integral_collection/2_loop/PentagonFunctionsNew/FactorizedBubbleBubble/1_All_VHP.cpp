#ifdef HIGH_PRECISION
#include "../FactorizedBubbleBubble_1_All.h"
#include "FactorizedBubbleBubble_1_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_FACTORIZEDBUBBLEBUBBLE_1_ALL(COUT, CIN, FOUT, FIN)   \
    template class FactorizedBubbleBubble_1_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_FACTORIZEDBUBBLEBUBBLE_1_ALL(CVHP, RVHP, CVHP, RVHP)
INSTANTIATE_FACTORIZEDBUBBLEBUBBLE_1_ALL(CVHP, CVHP, CVHP, CVHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_FACTORIZEDBUBBLEBUBBLE_1_ALL(F32, F32, CVHP, RVHP)
INSTANTIATE_FACTORIZEDBUBBLEBUBBLE_1_ALL(F32, F32, CVHP, CVHP)
#endif

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif