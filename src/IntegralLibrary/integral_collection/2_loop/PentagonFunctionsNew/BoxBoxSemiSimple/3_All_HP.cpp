#ifdef HIGH_PRECISION
#include "../BoxBoxSemiSimple_3_All.h"
#include "BoxBoxSemiSimple_3_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(COUT, CIN, FOUT, FIN)   \
    template class BoxBoxSemiSimple_3_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(CHP, RHP, CHP, RHP)
INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(CHP, CHP, CHP, CHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(F32, F32, CHP, RHP)
INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(F32, F32, CHP, CHP)
#endif

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif