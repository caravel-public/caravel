#ifdef HIGH_PRECISION
#include "../BoxBoxSemiSimple_3_All.h"
#include "BoxBoxSemiSimple_3_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(COUT, CIN, FOUT, FIN)   \
    template class BoxBoxSemiSimple_3_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(CVHP, RVHP, CVHP, RVHP)
INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(CVHP, CVHP, CVHP, CVHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(F32, F32, CVHP, RVHP)
INSTANTIATE_BOXBOXSEMISIMPLE_3_ALL(F32, F32, CVHP, CVHP)
#endif

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif