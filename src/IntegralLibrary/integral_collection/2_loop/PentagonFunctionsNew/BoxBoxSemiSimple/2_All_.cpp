#include "../BoxBoxSemiSimple_2_All.h"
#include "BoxBoxSemiSimple_2_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BOXBOXSEMISIMPLE_2_ALL(COUT, CIN, FOUT, FIN)   \
    template class BoxBoxSemiSimple_2_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BOXBOXSEMISIMPLE_2_ALL(C, R, C, R)
INSTANTIATE_BOXBOXSEMISIMPLE_2_ALL(C, C, C, C)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_BOXBOXSEMISIMPLE_2_ALL(F32, F32, C, R)
INSTANTIATE_BOXBOXSEMISIMPLE_2_ALL(F32, F32, C, C)
#endif

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel