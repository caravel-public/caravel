#ifdef HIGH_PRECISION
#include "../BoxTriangleSemisimple_1_All.h"
#include "BoxTriangleSemisimple_1_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BOXTRIANGLESEMISIMPLE_1_ALL(COUT, CIN, FOUT, FIN)   \
    template class BoxTriangleSemisimple_1_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BOXTRIANGLESEMISIMPLE_1_ALL(CHP, RHP, CHP, RHP)
INSTANTIATE_BOXTRIANGLESEMISIMPLE_1_ALL(CHP, CHP, CHP, CHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_BOXTRIANGLESEMISIMPLE_1_ALL(F32, F32, CHP, RHP)
INSTANTIATE_BOXTRIANGLESEMISIMPLE_1_ALL(F32, F32, CHP, CHP)
#endif

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif