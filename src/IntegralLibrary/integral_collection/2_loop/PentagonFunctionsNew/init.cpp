#include <iostream>

#include "IntegralLibrary/integral_collection/2_loop/PentagonFunctionsNew/init.h"

#include "IntegralLibrary/MasterIntegralData.h"
#include "IntegralLibrary/MasterIntegralPointer.h"
#include "IntegralLibrary/integral_collection/2_loop/PentagonFunctionsNew/PentagonFunctionsNew.h"
#include "IntegralLibrary/integral_collection/2_loop/PentagonFunctionsNew/IntegralInput.h"
#include "IntegralLibrary/integral_collection/NotImplementedIntegral.h"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

template <typename COUT, typename CIN> using Coeff = FBCoefficient<Series<COUT>, CIN>;

template <typename FOUT, typename FIN> using Func = StdBasisFunction<FOUT, FIN>;

void load() {
    using gType = lGraph::Graph<lGraph::Node, lGraph::Connection<lGraph::Strand>>;
    std::cout<<"Loading 2-loop:PentagonFunctionsNew integrals to MasterIntegralData map"<<std::endl;
#include "IntegralLibrary/integral_collection/2_loop/PentagonFunctionsNew/init_helper.hpp"
}

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel