#ifdef HIGH_PRECISION
#include "../BoxBoxSimple_1_All.h"
#include "BoxBoxSimple_1_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BOXBOXSIMPLE_1_ALL(COUT, CIN, FOUT, FIN)   \
    template class BoxBoxSimple_1_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BOXBOXSIMPLE_1_ALL(CVHP, RVHP, CVHP, RVHP)
INSTANTIATE_BOXBOXSIMPLE_1_ALL(CVHP, CVHP, CVHP, CVHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_BOXBOXSIMPLE_1_ALL(F32, F32, CVHP, RVHP)
INSTANTIATE_BOXBOXSIMPLE_1_ALL(F32, F32, CVHP, CVHP)
#endif

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif