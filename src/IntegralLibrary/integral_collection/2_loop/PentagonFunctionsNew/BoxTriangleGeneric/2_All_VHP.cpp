#ifdef HIGH_PRECISION
#include "../BoxTriangleGeneric_2_All.h"
#include "BoxTriangleGeneric_2_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BOXTRIANGLEGENERIC_2_ALL(COUT, CIN, FOUT, FIN)   \
    template class BoxTriangleGeneric_2_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BOXTRIANGLEGENERIC_2_ALL(CVHP, RVHP, CVHP, RVHP)
INSTANTIATE_BOXTRIANGLEGENERIC_2_ALL(CVHP, CVHP, CVHP, CVHP)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_BOXTRIANGLEGENERIC_2_ALL(F32, F32, CVHP, RVHP)
INSTANTIATE_BOXTRIANGLEGENERIC_2_ALL(F32, F32, CVHP, CVHP)
#endif

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif