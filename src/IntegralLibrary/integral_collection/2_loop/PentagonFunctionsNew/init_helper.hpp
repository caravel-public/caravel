{
    gType input{ "CaravelGraph[Nodes[Node[], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBoxSemiSimple<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBoxSemiSimple<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBoxSemiSimple<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBoxSemiSimple<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBoxSemiSimple<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBoxSemiSimple<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBoxSemiSimple<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBoxSemiSimple<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBoxSemiSimple<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBoxSemiSimple<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBoxSemiSimple<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBoxSemiSimple<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBoxSemiSimple<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBoxSemiSimple<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBoxSemiSimple<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBoxSemiSimple<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBoxSemiSimple<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBoxSemiSimple<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBoxSemiSimple<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBoxSemiSimple<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBoxSemiSimple<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSemiSimple_3_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("3"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1]], Link[], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[], Bead[Leg[3, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBoxSimple<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBoxSimple<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBoxSimple<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBoxSimple<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBoxSimple<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBoxSimple<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBoxSimple<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1]], Link[], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[], Bead[Leg[3, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBoxSimple<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBoxSimple<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBoxSimple<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBoxSimple<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBoxSimple<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBoxSimple<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBoxSimple<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBoxSimple_2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[4, 0]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2]], Link[]], Strand[Link[LoopMomentum[2]]], Strand[Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBubble1<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBubble1<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBubble1<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBubble1<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBubble1<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBubble1<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBubble1<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble1_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2, 0]]], Connection[Strand[Link[]], Strand[Link[LoopMomentum[-1]]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[], Bead[Leg[3, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBubble2<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBubble2<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBubble2<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBubble2<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBubble2<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBubble2<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBubble2<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxBubble2_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxTriangleGeneric<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxTriangleGeneric<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxTriangleGeneric<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxTriangleGeneric<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxTriangleGeneric<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxTriangleGeneric<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxTriangleGeneric<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxTriangleGeneric<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxTriangleGeneric<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxTriangleGeneric<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxTriangleGeneric<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxTriangleGeneric<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxTriangleGeneric<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxTriangleGeneric<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleGeneric_2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[], Bead[Leg[3, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxTriangleSemisimple<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxTriangleSemisimple<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxTriangleSemisimple<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxTriangleSemisimple<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxTriangleSemisimple<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxTriangleSemisimple<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxTriangleSemisimple<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return BoxTriangleSemisimple_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mandelstam,{2, 4}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::FactorizedBoxBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::FactorizedBoxBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::FactorizedBoxBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::FactorizedBoxBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::FactorizedBoxBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::FactorizedBoxBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::FactorizedBoxBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBoxBubble_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[3]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::FactorizedBubbleBubbleGeneric<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::FactorizedBubbleBubbleGeneric<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::FactorizedBubbleBubbleGeneric<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::FactorizedBubbleBubbleGeneric<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::FactorizedBubbleBubbleGeneric<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::FactorizedBubbleBubbleGeneric<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::FactorizedBubbleBubbleGeneric<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubbleGeneric_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1]], Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[2]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::FactorizedBubbleBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::FactorizedBubbleBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::FactorizedBubbleBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::FactorizedBubbleBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::FactorizedBubbleBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::FactorizedBubbleBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::FactorizedBubbleBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return FactorizedBubbleBubble_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::PentagonBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::PentagonBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::PentagonBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::PentagonBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::PentagonBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::PentagonBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::PentagonBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::PentagonBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::PentagonBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::PentagonBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::PentagonBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::PentagonBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::PentagonBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::PentagonBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[5, 0]], Link[], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::PentagonBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::PentagonBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::PentagonBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::PentagonBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::PentagonBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::PentagonBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::PentagonBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBox_3_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("3"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[5, 0]], Node[Leg[4, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[LoopMomentum[2]]], Strand[Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::PentagonBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::PentagonBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::PentagonBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::PentagonBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::PentagonBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::PentagonBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::PentagonBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[5, 0]], Node[Leg[4, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1, 0]], Link[], Bead[Leg[2, 0]], Link[], Bead[Leg[3, 0]], Link[]], Strand[Link[LoopMomentum[2]]], Strand[Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::PentagonBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::PentagonBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::PentagonBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::PentagonBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::PentagonBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::PentagonBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::PentagonBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return PentagonBubble_2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::Mandelstam,{4, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[2]], Node[Leg[1]]], Connection[Strand[Link[]], Strand[Link[LoopMomentum[-1]]], Strand[Link[LoopMomentum[-2]]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Sunrise<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Sunrise<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Sunrise<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Sunrise<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Sunrise<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Sunrise<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Sunrise<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return Sunrise_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2, 0]]], Connection[Strand[Link[]], Strand[Link[LoopMomentum[-1]]], Strand[Link[LoopMomentum[-2]], Bead[Leg[3]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TriangleBubble1<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TriangleBubble1<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TriangleBubble1<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TriangleBubble1<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TriangleBubble1<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TriangleBubble1<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TriangleBubble1<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleBubble1_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[Leg[2, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[1]], Link[]], Strand[Link[LoopMomentum[2]]], Strand[Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TriangleBubble2<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TriangleBubble2<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TriangleBubble2<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TriangleBubble2<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TriangleBubble2<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TriangleBubble2<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TriangleBubble2<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything } ) ) {
                return TriangleBubble2_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TriangleTriangle2MastersDot<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TriangleTriangle2MastersDot<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TriangleTriangle2MastersDot<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TriangleTriangle2MastersDot<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TriangleTriangle2MastersDot<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TriangleTriangle2MastersDot<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TriangleTriangle2MastersDot<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TriangleTriangle2MastersDot<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TriangleTriangle2MastersDot<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TriangleTriangle2MastersDot<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TriangleTriangle2MastersDot<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TriangleTriangle2MastersDot<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TriangleTriangle2MastersDot<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TriangleTriangle2MastersDot<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle2MastersDot_2_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{4}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[3, 0]]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2, 0]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[4, 0]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TriangleTriangleGeneric1Master<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TriangleTriangleGeneric1Master<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TriangleTriangleGeneric1Master<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TriangleTriangleGeneric1Master<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TriangleTriangleGeneric1Master<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TriangleTriangleGeneric1Master<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TriangleTriangleGeneric1Master<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangleGeneric1Master_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[]], Connection[Strand[Link[LoopMomentum[1]], Bead[Leg[2]], Link[]], Strand[Link[]], Strand[Link[LoopMomentum[-2]], Bead[Leg[3]], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::TriangleTriangle<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::TriangleTriangle<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::TriangleTriangle<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::TriangleTriangle<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::TriangleTriangle<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::TriangleTriangle<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::TriangleTriangle<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::anything,InvariantSigns::anything } ) ) {
                return TriangleTriangle_1_All<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("1"))), data});

}