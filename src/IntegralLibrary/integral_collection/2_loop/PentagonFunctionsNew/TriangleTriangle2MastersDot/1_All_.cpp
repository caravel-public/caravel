#include "../TriangleTriangle2MastersDot_1_All.h"
#include "TriangleTriangle2MastersDot_1_All.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace PentagonFunctionsNew {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_TRIANGLETRIANGLE2MASTERSDOT_1_ALL(COUT, CIN, FOUT, FIN)   \
    template class TriangleTriangle2MastersDot_1_All<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_TRIANGLETRIANGLE2MASTERSDOT_1_ALL(C, R, C, R)
INSTANTIATE_TRIANGLETRIANGLE2MASTERSDOT_1_ALL(C, C, C, C)
#ifdef USE_FINITE_FIELDS
INSTANTIATE_TRIANGLETRIANGLE2MASTERSDOT_1_ALL(F32, F32, C, R)
INSTANTIATE_TRIANGLETRIANGLE2MASTERSDOT_1_ALL(F32, F32, C, C)
#endif

} // namespace PentagonFunctionsNew
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel