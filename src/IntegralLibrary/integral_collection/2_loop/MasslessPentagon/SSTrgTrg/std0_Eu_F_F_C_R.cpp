#ifdef USE_FINITE_FIELDS
#include "../SSTrgTrg_std0_Eu.h"
#include "SSTrgTrg_std0_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_SSTRGTRG_STD0_EU(COUT, CIN, FOUT, FIN)   \
    template class SSTrgTrg_std0_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_SSTRGTRG_STD0_EU(F32, F32, C, R)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif