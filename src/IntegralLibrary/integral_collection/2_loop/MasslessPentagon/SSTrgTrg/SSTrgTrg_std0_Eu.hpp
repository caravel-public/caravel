
namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// SSTrgTrg_std0_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
SSTrgTrg_std0_Eu<CType, FType>::SSTrgTrg_std0_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 0 || max_eps_power < 0){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(0)
        );
    }

    number_of_loops = 2;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = 0;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),4)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-5)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[1])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(5)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[1]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[1]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-15)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(5)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),4)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-5)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_2_1<Out>(args[1],args[0])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_2_1<Out>(args[1],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(5)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_2_1<Out>(args[1],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_2_1<Out>(args[1],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-5)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[1],args[0])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[1],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(5)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_1<Out>(args[1],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_1<Out>(args[1],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[1],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[1],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_2<Out>(args[1],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_2<Out>(args[1],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[1],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[1],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_3<Out>(args[1],args[0],invariant_names[1],invariant_names[1])
                    ;},
                "pentagon_f_4_3<Out>(args[1],args[0],invariant_names[1],invariant_names[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*zeta<Out>(3)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*zeta<Out>(3)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( 0, max_eps_power,
                        
                        TI(-6)
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t SSTrgTrg_std0_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int SSTrgTrg_std0_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int SSTrgTrg_std0_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool SSTrgTrg_std0_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel