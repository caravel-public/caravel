#include "../GenTrgBubble_std0_Eu.h"
#include "GenTrgBubble_std0_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_GENTRGBUBBLE_STD0_EU(COUT, CIN, FOUT, FIN)   \
    template class GenTrgBubble_std0_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_GENTRGBUBBLE_STD0_EU(C, R, C, R)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel