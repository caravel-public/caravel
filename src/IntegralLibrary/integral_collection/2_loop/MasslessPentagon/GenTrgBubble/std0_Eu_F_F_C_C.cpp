#ifdef USE_FINITE_FIELDS
#include "../GenTrgBubble_std0_Eu.h"
#include "GenTrgBubble_std0_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_GENTRGBUBBLE_STD0_EU(COUT, CIN, FOUT, FIN)   \
    template class GenTrgBubble_std0_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_GENTRGBUBBLE_STD0_EU(F32, F32, C, C)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif