#include "../GenTrgTrg1Ma_scalar_Eu.h"
#include "GenTrgTrg1Ma_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_GENTRGTRG1MA_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class GenTrgTrg1Ma_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_GENTRGTRG1MA_SCALAR_EU(C, C, C, C)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel