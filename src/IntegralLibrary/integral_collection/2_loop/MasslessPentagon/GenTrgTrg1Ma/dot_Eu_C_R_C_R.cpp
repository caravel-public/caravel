#include "../GenTrgTrg1Ma_dot_Eu.h"
#include "GenTrgTrg1Ma_dot_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_GENTRGTRG1MA_DOT_EU(COUT, CIN, FOUT, FIN)   \
    template class GenTrgTrg1Ma_dot_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_GENTRGTRG1MA_DOT_EU(C, R, C, R)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel