{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[], Leg[1, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BoxBox4pt1M_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBox4pt1M<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BoxBox4pt1M_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBox4pt1M<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BoxBox4pt1M_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBox4pt1M<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BoxBox4pt1M_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBox4pt1M<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBox4pt1M<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBox4pt1M<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBox4pt1M<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[3, 0], Link[], Leg[2, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[], Leg[1, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BoxBox4pt1M_std1_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BoxBox4pt1M<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BoxBox4pt1M_std1_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BoxBox4pt1M<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BoxBox4pt1M_std1_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BoxBox4pt1M<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BoxBox4pt1M_std1_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BoxBox4pt1M<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BoxBox4pt1M<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BoxBox4pt1M<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BoxBox4pt1M<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[], Leg[2, 0], Link[], Leg[3, 0], Link[]], Strand[LoopMomentum[2], Leg[4], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return FactBoxBubble_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::FactBoxBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return FactBoxBubble_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::FactBoxBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return FactBoxBubble_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::FactBoxBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return FactBoxBubble_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::FactBoxBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::FactBoxBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::FactBoxBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::FactBoxBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return FactBubbleBubble_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{1})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::FactBubbleBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return FactBubbleBubble_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::FactBubbleBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return FactBubbleBubble_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::FactBubbleBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return FactBubbleBubble_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{1})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::FactBubbleBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::FactBubbleBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::FactBubbleBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::FactBubbleBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return FactSSBubbleBubble_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::FactSSBubbleBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return FactSSBubbleBubble_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::FactSSBubbleBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return FactSSBubbleBubble_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::FactSSBubbleBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return FactSSBubbleBubble_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::FactSSBubbleBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::FactSSBubbleBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::FactSSBubbleBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::FactSSBubbleBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[Leg[4]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2], Leg[2, 0], Link[], Leg[1, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenBoxBubble1Ma_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::GenBoxBubble1Ma<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenBoxBubble1Ma_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::GenBoxBubble1Ma<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenBoxBubble1Ma_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::GenBoxBubble1Ma<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenBoxBubble1Ma_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::GenBoxBubble1Ma<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::GenBoxBubble1Ma<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::GenBoxBubble1Ma<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::GenBoxBubble1Ma<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenBoxBubble1Mb_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::GenBoxBubble1Mb<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenBoxBubble1Mb_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::GenBoxBubble1Mb<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenBoxBubble1Mb_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::GenBoxBubble1Mb<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenBoxBubble1Mb_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::GenBoxBubble1Mb<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::GenBoxBubble1Mb<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::GenBoxBubble1Mb<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::GenBoxBubble1Mb<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgBubble2M_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::GenTrgBubble2M<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgBubble2M_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::GenTrgBubble2M<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgBubble2M_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::GenTrgBubble2M<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgBubble2M_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::GenTrgBubble2M<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::GenTrgBubble2M<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::GenTrgBubble2M<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::GenTrgBubble2M<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return GenTrgBubble_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{2})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::GenTrgBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return GenTrgBubble_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::GenTrgBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return GenTrgBubble_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::GenTrgBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return GenTrgBubble_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{2})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::GenTrgBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::GenTrgBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::GenTrgBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::GenTrgBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{2})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Ma_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::GenTrgTrg1Ma<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Ma_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::GenTrgTrg1Ma<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Ma_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::GenTrgTrg1Ma<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Ma_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::GenTrgTrg1Ma<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::GenTrgTrg1Ma<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::GenTrgTrg1Ma<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::GenTrgTrg1Ma<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[3, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Ma_dot_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::GenTrgTrg1Ma<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Ma_dot_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::GenTrgTrg1Ma<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Ma_dot_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::GenTrgTrg1Ma<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Ma_dot_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::GenTrgTrg1Ma<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::GenTrgTrg1Ma<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::GenTrgTrg1Ma<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::GenTrgTrg1Ma<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("dot"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[2, 0]], Node[Leg[4]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Mb_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::GenTrgTrg1Mb<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Mb_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::GenTrgTrg1Mb<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Mb_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::GenTrgTrg1Mb<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return GenTrgTrg1Mb_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::GenTrgTrg1Mb<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::GenTrgTrg1Mb<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::GenTrgTrg1Mb<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::GenTrgTrg1Mb<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[2, 0]], Node[Leg[5, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessGenBoxTriang_div_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessGenBoxTriang<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessGenBoxTriang_div_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessGenBoxTriang<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessGenBoxTriang_div_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessGenBoxTriang<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessGenBoxTriang_div_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessGenBoxTriang<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessGenBoxTriang<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessGenBoxTriang<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessGenBoxTriang<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("div"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[2, 0]], Node[Leg[5, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessGenBoxTriang_fin_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessGenBoxTriang<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessGenBoxTriang_fin_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessGenBoxTriang<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessGenBoxTriang_fin_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessGenBoxTriang<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessGenBoxTriang_fin_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessGenBoxTriang<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessGenBoxTriang<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessGenBoxTriang<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessGenBoxTriang<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("fin"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[], Leg[5, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_double_wiggly_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessPentaBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_double_wiggly_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessPentaBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_double_wiggly_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessPentaBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_double_wiggly_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessPentaBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessPentaBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessPentaBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessPentaBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("double_wiggly"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[], Leg[5, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_mu12_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessPentaBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_mu12_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessPentaBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_mu12_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessPentaBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_mu12_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessPentaBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessPentaBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessPentaBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessPentaBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("mu12"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[], Leg[5, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_ls_mu12_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessPentaBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_ls_mu12_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessPentaBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_ls_mu12_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessPentaBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBox_ls_mu12_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessPentaBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessPentaBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessPentaBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessPentaBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("ls_mu12"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[5, 0]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBubble_div_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessPentaBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBubble_div_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessPentaBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBubble_div_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessPentaBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBubble_div_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessPentaBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessPentaBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessPentaBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessPentaBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("div"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[5, 0]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2], Leg[2, 0], Link[], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBubble_fin_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessPentaBubble<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBubble_fin_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessPentaBubble<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBubble_fin_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessPentaBubble<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::anything } ) ) {
                return MasslessPentaBubble_fin_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessPentaBubble<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessPentaBubble<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessPentaBubble<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessPentaBubble<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5}),InputVariable(InvariantType::SignTr5,{1, 2, 3, 4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("fin"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4, 0], Link[], Leg[5, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_finite_1_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessSSBoxBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_finite_1_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessSSBoxBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_finite_1_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessSSBoxBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_finite_1_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessSSBoxBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessSSBoxBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessSSBoxBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessSSBoxBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("pure_finite_1"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4, 0], Link[], Leg[5, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_finite_2_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessSSBoxBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_finite_2_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessSSBoxBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_finite_2_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessSSBoxBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_finite_2_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessSSBoxBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessSSBoxBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessSSBoxBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessSSBoxBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("pure_finite_2"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[4, 0], Link[], Leg[5, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_mu12_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::MasslessSSBoxBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_mu12_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::MasslessSSBoxBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_mu12_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::MasslessSSBoxBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return MasslessSSBoxBox_pure_mu12_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::MasslessSSBoxBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::MasslessSSBoxBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::MasslessSSBoxBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::MasslessSSBoxBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mandelstam,{3, 4}),InputVariable(InvariantType::Mandelstam,{4, 5}),InputVariable(InvariantType::Mandelstam,{1, 5})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("pure_mu12"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[]], Connection[Strand[LoopMomentum[1], Leg[4], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[2, 0], Link[], Leg[1, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return SSBoxTrg_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::SSBoxTrg<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return SSBoxTrg_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::SSBoxTrg<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return SSBoxTrg_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::SSBoxTrg<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return SSBoxTrg_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::SSBoxTrg<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::SSBoxTrg<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::SSBoxTrg<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::SSBoxTrg<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3}),InputVariable(InvariantType::Mass2,{4})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[2, 0]], Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return SSTrgTrg_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::SSTrgTrg<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return SSTrgTrg_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::SSTrgTrg<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return SSTrgTrg_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::SSTrgTrg<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return SSTrgTrg_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::SSTrgTrg<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::SSTrgTrg<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::SSTrgTrg<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::SSTrgTrg<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1}),InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_std0_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{1})} })
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Sunrise<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_std0_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Sunrise<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_std0_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Sunrise<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_std0_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({ {InputVariable(InvariantType::Mass2,{1})} })
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,RHP>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Sunrise<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CHP,CHP>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Sunrise<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,RHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CHP,CHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Sunrise<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Sunrise<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,RVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            return NotImplementedIntegral<Coeff<F32,F32>, Func<CVHP,CVHP>>().get_basis();
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("std0"))), data});

}