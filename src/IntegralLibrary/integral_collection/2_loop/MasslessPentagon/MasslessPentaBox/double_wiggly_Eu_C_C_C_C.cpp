#include "../MasslessPentaBox_double_wiggly_Eu.h"
#include "MasslessPentaBox_double_wiggly_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSPENTABOX_DOUBLE_WIGGLY_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessPentaBox_double_wiggly_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSPENTABOX_DOUBLE_WIGGLY_EU(C, C, C, C)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel