#ifdef USE_FINITE_FIELDS
#include "../MasslessPentaBox_ls_mu12_Eu.h"
#include "MasslessPentaBox_ls_mu12_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSPENTABOX_LS_MU12_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessPentaBox_ls_mu12_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSPENTABOX_LS_MU12_EU(F32, F32, C, C)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif