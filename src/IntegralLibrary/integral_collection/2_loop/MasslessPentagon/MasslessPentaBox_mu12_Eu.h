/*
###############################################################################
#             This code is automatically generated.                           #
#             Only modify if you know what you are doing.                     #
###############################################################################
 */

#pragma once

#include <vector>
#include <string>
#include <stdexcept>
#include <functional>

#include "Core/Series.h"
#include "Core/type_traits_extra.h"
#include "IntegralLibrary/MasterIntegral.h"
#include "IntegralLibrary/BasisElementFunction.h"
#include "IntegralLibrary/SpecialFunctions/access_special_functions.h"

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

/**
 * Master integral : MasslessPentaBox_mu12_Eu.
 *
 * It corresponds to a 2 - loop integral.
 * <br>
 * Date of Json file generation from Mathematica:
 * 08 October 2019
 * <br>
 * The maximal power of \f$ \epsilon \f$ is: 1.
 * <br>
 * The integral has 1 basis functions.
 * <br>
 * O(ep) massless pentagon-box integral.  
 * <br>
 * The normalisation is:  
 * <br>
 *  * \f[ e^{\gamma_E \epsilon} \int \frac{d^Dk}{i\pi^{\frac{D}{2}}} \f] 
 * <br>
 * Graph info: CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[], Leg[5, 0], Link[]]]] 
 * <br>
 * Variables: s[1, 2], s[2, 3], s[3, 4], s[4, 5], s[1, 5], signtr5[1, 2, 3, 4] 
 * <br>
 * Insertion name: mu12 
 * <br>
 * Insertion: 
 * <br>
 * levi[p[1], p[2], p[3], p[4]] mu[l[1], l[2]] s[1, 2] 
 * <br>
 * Region: Eu 
 * <br>
 * Signs of invariants for this region: InvariantSigns::negative, InvariantSigns::negative, InvariantSigns::negative, InvariantSigns::negative, InvariantSigns::negative, InvariantSigns::anything
 * <br>
 * For further information consider the comment at the top of the header file.
 */
template <typename CType, typename FType>
class MasslessPentaBox_mu12_Eu : public MasterIntegral<CType, FType> {
    typedef typename FType::input_type T;
    typedef typename CType::input_type TI;
    typedef typename FType::output_type Out;
    typedef typename CType::output_type OutC;
    using MasterIntegral<CType, FType>::abstract_basis;

public:
    /**
     * Construct the Master Integral.
     *
     * The user has to provide the names of the invariants in the integral.
     * This is important for correct caching!
     *
     * @param The names for the variables in the integral.
     * @param The requested maximal power in the dimensional regulator
              \f$ \epsilon \f$
     */
    MasslessPentaBox_mu12_Eu(const std::vector<std::string>& invariant_names, int max_eps_power = 1);

    /**
     * Get the number of loops related to the integral.
     * @return The number of loops.
     */
    std::size_t get_number_of_loops() const;

    /**
     * Get the leading power in epsilon.
     * @return The leading power in epsilon.
     */
    int get_leading_eps() const;

    /**
     * Get the last power in epsilon.
     * @return The highest power in epsilon.
     */
    int get_last_eps() const;

    /**
     * Check whether the integral was successfully tested numerically.
     * @return True if the integral was successfully tested numerically, else False.
     */
    bool successfully_tested_numerically() const;

private:
    /// The number of loops.
    std::size_t number_of_loops;

    /// The leading power in epsilon
    int leading_eps;

    /// The highest power in epsilon
    int last_eps;

    /// Track whether integral has been successfully numerically checked
    bool successful_numerical_test;
};

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
