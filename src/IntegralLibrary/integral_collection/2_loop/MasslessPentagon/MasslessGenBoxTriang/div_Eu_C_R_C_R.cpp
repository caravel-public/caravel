#include "../MasslessGenBoxTriang_div_Eu.h"
#include "MasslessGenBoxTriang_div_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSGENBOXTRIANG_DIV_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessGenBoxTriang_div_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSGENBOXTRIANG_DIV_EU(C, R, C, R)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel