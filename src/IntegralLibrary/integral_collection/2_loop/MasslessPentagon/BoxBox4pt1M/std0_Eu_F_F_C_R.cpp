#ifdef USE_FINITE_FIELDS
#include "../BoxBox4pt1M_std0_Eu.h"
#include "BoxBox4pt1M_std0_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BOXBOX4PT1M_STD0_EU(COUT, CIN, FOUT, FIN)   \
    template class BoxBox4pt1M_std0_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BOXBOX4PT1M_STD0_EU(F32, F32, C, R)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif