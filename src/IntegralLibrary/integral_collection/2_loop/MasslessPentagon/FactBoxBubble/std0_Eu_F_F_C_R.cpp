#ifdef USE_FINITE_FIELDS
#include "../FactBoxBubble_std0_Eu.h"
#include "FactBoxBubble_std0_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_FACTBOXBUBBLE_STD0_EU(COUT, CIN, FOUT, FIN)   \
    template class FactBoxBubble_std0_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_FACTBOXBUBBLE_STD0_EU(F32, F32, C, R)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif