#include "../MasslessSSBoxBox_pure_finite_2_Eu.h"
#include "MasslessSSBoxBox_pure_finite_2_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSSSBOXBOX_PURE_FINITE_2_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessSSBoxBox_pure_finite_2_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSSSBOXBOX_PURE_FINITE_2_EU(C, R, C, R)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel