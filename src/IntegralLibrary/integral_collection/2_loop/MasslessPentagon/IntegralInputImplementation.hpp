
template <typename T>
std::vector<T> BoxBox4pt1M(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
sq(mc[4])
    });
}

template <typename T>
std::vector<T> FactBoxBubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
sq(mc[4])
    });
}

template <typename T>
std::vector<T> FactBubbleBubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1])
    });
}

template <typename T>
std::vector<T> FactSSBubbleBubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1]),
sq(mc[2])
    });
}

template <typename T>
std::vector<T> GenBoxBubble1Ma(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
sq(mc[4])
    });
}

template <typename T>
std::vector<T> GenBoxBubble1Mb(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
sq(mc[4])
    });
}

template <typename T>
std::vector<T> GenTrgBubble2M(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[2]),
sq(mc[3])
    });
}

template <typename T>
std::vector<T> GenTrgBubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[2])
    });
}

template <typename T>
std::vector<T> GenTrgTrg1Ma(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
sq(mc[4])
    });
}

template <typename T>
std::vector<T> GenTrgTrg1Mb(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
sq(mc[4])
    });
}

template <typename T>
std::vector<T> MasslessGenBoxTriang(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(4, 5),
mc.s(1, 5),
sign_tr5(mc[1], mc[2], mc[3], mc[4])
    });
}

template <typename T>
std::vector<T> MasslessPentaBox(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(4, 5),
mc.s(1, 5),
sign_tr5(mc[1], mc[2], mc[3], mc[4])
    });
}

template <typename T>
std::vector<T> MasslessPentaBubble(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(4, 5),
mc.s(1, 5),
sign_tr5(mc[1], mc[2], mc[3], mc[4])
    });
}

template <typename T>
std::vector<T> MasslessSSBoxBox(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
mc.s(3, 4),
mc.s(4, 5),
mc.s(1, 5)
    });
}

template <typename T>
std::vector<T> SSBoxTrg(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3),
sq(mc[4])
    });
}

template <typename T>
std::vector<T> SSTrgTrg(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1]),
sq(mc[3])
    });
}

template <typename T>
std::vector<T> Sunrise(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1])
    });
}
