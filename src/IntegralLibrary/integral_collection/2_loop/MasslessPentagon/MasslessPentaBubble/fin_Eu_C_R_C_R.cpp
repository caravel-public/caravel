#include "../MasslessPentaBubble_fin_Eu.h"
#include "MasslessPentaBubble_fin_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSPENTABUBBLE_FIN_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessPentaBubble_fin_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSPENTABUBBLE_FIN_EU(C, R, C, R)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel