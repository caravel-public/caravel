#ifdef USE_FINITE_FIELDS
#include "../MasslessPentaBubble_div_Eu.h"
#include "MasslessPentaBubble_div_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MASSLESSPENTABUBBLE_DIV_EU(COUT, CIN, FOUT, FIN)   \
    template class MasslessPentaBubble_div_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_MASSLESSPENTABUBBLE_DIV_EU(F32, F32, C, C)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif