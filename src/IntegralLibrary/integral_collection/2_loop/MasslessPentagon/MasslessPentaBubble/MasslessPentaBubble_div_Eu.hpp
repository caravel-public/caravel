
namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// MasslessPentaBubble_div_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
MasslessPentaBubble_div_Eu<CType, FType>::MasslessPentaBubble_div_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 1 || max_eps_power < -1){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(1)
        );
    }

    number_of_loops = 2;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = -1;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        divPentaBubInit<Out>
                    ;},
                "divPentaBubInit<Out>",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)
                    ;},
                "prodpow(Pi<Out>,2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(-1)/TI(6),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,4)
                    ;},
                "prodpow(Pi<Out>,4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(17)/TI(120)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(6),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[0]),2)
                    ;},
                "prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[0]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-7)/TI(48)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),3)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(12),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),4)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(23)/TI(48)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(6),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[1])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(19)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(-1)/TI(2),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[1]),2)
                    ;},
                "prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[1]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-7)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[1]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[1]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),3)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(5)/TI(12),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),4)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-67)/TI(64)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(6),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(4),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-61)/TI(48)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(1),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),3)*pentagon_f_1_1<Out>(args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),3)*pentagon_f_1_1<Out>(args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-13)/TI(12)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[2]),2)
                    ;},
                "prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[2]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[2]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[2]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(61)/TI(32)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*prodpow(pentagon_f_1_1<Out>(args[2]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*prodpow(pentagon_f_1_1<Out>(args[2]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),3)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(5)/TI(12),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[2]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[2]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-43)/TI(48)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(13)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),4)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(35)/TI(64)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(6),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(48)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-25)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(4),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),3)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),3)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(61)/TI(16)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),3)*pentagon_f_1_1<Out>(args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),3)*pentagon_f_1_1<Out>(args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(48)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(4),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(27)/TI(32)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(31)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-137)/TI(32)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(9)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)*prodpow(pentagon_f_1_1<Out>(args[3]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)*prodpow(pentagon_f_1_1<Out>(args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(16)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[3]),3)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[3]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(12),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[3]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[3]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-79)/TI(48)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[3]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[3]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(29)/TI(48)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[3]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[3]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[3]),4)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[3]),4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(3),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),3)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(11)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-7)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),3)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),3)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(43)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(-1),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(13)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(25)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-3)/TI(4),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(23)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),3)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),3)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-73)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(7)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-27)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(21)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[3]),3)*pentagon_f_1_1<Out>(args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[3]),3)*pentagon_f_1_1<Out>(args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(1)/TI(2),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "prodpow(Pi<Out>,2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-5)/TI(4),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-43)/TI(16)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-65)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(27)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(11)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-7)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[3]),2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[3]),2)*prodpow(pentagon_f_1_1<Out>(args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-13)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[4]),3)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[4]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(12),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[4]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*prodpow(pentagon_f_1_1<Out>(args[4]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(12)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[4]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*prodpow(pentagon_f_1_1<Out>(args[4]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[4]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[2])*prodpow(pentagon_f_1_1<Out>(args[4]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*prodpow(pentagon_f_1_1<Out>(args[4]),3)
                    ;},
                "pentagon_f_1_1<Out>(args[3])*prodpow(pentagon_f_1_1<Out>(args[4]),3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[4]),4)
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[4]),4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-11)/TI(24)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-9)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[4]),2)*pentagon_f_2_1<Out>(args[0],args[2])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[4]),2)*pentagon_f_2_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-11)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_2_1<Out>(args[0],args[2]),2)
                    ;},
                "prodpow(pentagon_f_2_1<Out>(args[0],args[2]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_2_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_2_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[1],args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-11)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_2_1<Out>(args[1],args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_2_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(21)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[4]),2)*pentagon_f_2_1<Out>(args[1],args[3])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[4]),2)*pentagon_f_2_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(7)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_2_1<Out>(args[1],args[3]),2)
                    ;},
                "prodpow(pentagon_f_2_1<Out>(args[1],args[3]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(-1),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-9)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-11)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(3)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(15)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[4]),2)*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[4]),2)*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_2_1<Out>(args[0],args[2])*pentagon_f_2_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_2_1<Out>(args[0],args[2])*pentagon_f_2_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_2_1<Out>(args[2],args[4]),2)
                    ;},
                "prodpow(pentagon_f_2_1<Out>(args[2],args[4]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[0]),2)*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[1])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-11)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(11)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-11)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(1),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "prodpow(Pi<Out>,2)*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[1]),2)*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[2])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(15)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[2]),2)*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(15)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[3])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[3]),2)*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-3)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-13)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-9)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_1_1<Out>(args[4])*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_1_1<Out>(args[4]),2)*pentagon_f_2_1<Out>(args[4],args[1])
                    ;},
                "prodpow(pentagon_f_1_1<Out>(args[4]),2)*pentagon_f_2_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(11)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(pentagon_f_2_1<Out>(args[4],args[1]),2)
                    ;},
                "prodpow(pentagon_f_2_1<Out>(args[4],args[1]),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_3_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_3_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-11)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_3_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(9)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(17)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-27)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_3_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-7)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_3_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(15)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_1<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-13)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_2<Out>(args[0],args[2])
                    ;},
                "pentagon_f_3_2<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_2<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_2<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_2<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_2<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_2<Out>(args[1],args[3])
                    ;},
                "pentagon_f_3_2<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(9)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_2<Out>(args[2],args[4])
                    ;},
                "pentagon_f_3_2<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_2<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_2<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(9)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_2<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_2<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(7)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-21)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_2<Out>(args[3],args[0])
                    ;},
                "pentagon_f_3_2<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_2<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_2<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-7)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_2<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_2<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_2<Out>(args[4],args[1])
                    ;},
                "pentagon_f_3_2<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_2<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(9)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_2<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_2<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(21)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_2<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_2<Out>(args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-19)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_3<Out>(args[0],args[1],args[3])
                    ;},
                "pentagon_f_3_3<Out>(args[0],args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_3<Out>(args[0],args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_3<Out>(args[0],args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_3<Out>(args[0],args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_3<Out>(args[0],args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[0],args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[0],args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(9)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[0],args[1],args[3])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[0],args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_3<Out>(args[1],args[2],args[4])
                    ;},
                "pentagon_f_3_3<Out>(args[1],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(3)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_3<Out>(args[1],args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_3<Out>(args[1],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_3<Out>(args[1],args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_3<Out>(args[1],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(15)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_3<Out>(args[1],args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_3<Out>(args[1],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(15)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[1],args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[1],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[1],args[2],args[4])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[1],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_3<Out>(args[2],args[3],args[0])
                    ;},
                "pentagon_f_3_3<Out>(args[2],args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(-1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_3<Out>(args[2],args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_3<Out>(args[2],args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_3<Out>(args[2],args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_3<Out>(args[2],args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[2],args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[2],args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[2],args[3],args[0])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[2],args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_3<Out>(args[3],args[4],args[1])
                    ;},
                "pentagon_f_3_3<Out>(args[3],args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*pentagon_f_3_3<Out>(args[3],args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[1])*pentagon_f_3_3<Out>(args[3],args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[3],args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[3],args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(11)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[3],args[4],args[1])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[3],args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(11)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_3_3<Out>(args[4],args[0],args[2])
                    ;},
                "pentagon_f_3_3<Out>(args[4],args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(1)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*pentagon_f_3_3<Out>(args[4],args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[0])*pentagon_f_3_3<Out>(args[4],args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*pentagon_f_3_3<Out>(args[4],args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[2])*pentagon_f_3_3<Out>(args[4],args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[4],args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[3])*pentagon_f_3_3<Out>(args[4],args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[4],args[0],args[2])
                    ;},
                "pentagon_f_1_1<Out>(args[4])*pentagon_f_3_3<Out>(args[4],args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(3)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_1<Out>(args[2],args[4],invariant_names[2],invariant_names[4])
                    ;},
                "pentagon_f_4_1<Out>(args[2],args[4],invariant_names[2],invariant_names[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_1<Out>(args[4],args[1],invariant_names[4],invariant_names[1])
                    ;},
                "pentagon_f_4_1<Out>(args[4],args[1],invariant_names[4],invariant_names[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_11<Out>(args[1],args[2],args[3],args[4],args[0])
                    ;},
                "pentagon_f_4_11<Out>(args[1],args[2],args[3],args[4],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_2<Out>(args[2],args[4],invariant_names[2],invariant_names[4])
                    ;},
                "pentagon_f_4_2<Out>(args[2],args[4],invariant_names[2],invariant_names[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_2<Out>(args[4],args[1],invariant_names[4],invariant_names[1])
                    ;},
                "pentagon_f_4_2<Out>(args[4],args[1],invariant_names[4],invariant_names[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_3<Out>(args[0],args[2],invariant_names[0],invariant_names[2])
                    ;},
                "pentagon_f_4_3<Out>(args[0],args[2],invariant_names[0],invariant_names[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(13)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_3<Out>(args[1],args[3],invariant_names[1],invariant_names[3])
                    ;},
                "pentagon_f_4_3<Out>(args[1],args[3],invariant_names[1],invariant_names[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-27)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_3<Out>(args[2],args[4],invariant_names[2],invariant_names[4])
                    ;},
                "pentagon_f_4_3<Out>(args[2],args[4],invariant_names[2],invariant_names[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(9)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_3<Out>(args[3],args[0],invariant_names[3],invariant_names[0])
                    ;},
                "pentagon_f_4_3<Out>(args[3],args[0],invariant_names[3],invariant_names[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-3)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_3<Out>(args[4],args[1],invariant_names[4],invariant_names[1])
                    ;},
                "pentagon_f_4_3<Out>(args[4],args[1],invariant_names[4],invariant_names[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_4<Out>(args[0],args[1],args[3])
                    ;},
                "pentagon_f_4_4<Out>(args[0],args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_4<Out>(args[1],args[2],args[4])
                    ;},
                "pentagon_f_4_4<Out>(args[1],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-7)/TI(8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_4<Out>(args[3],args[4],args[1])
                    ;},
                "pentagon_f_4_4<Out>(args[3],args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_4<Out>(args[4],args[0],args[2])
                    ;},
                "pentagon_f_4_4<Out>(args[4],args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_5<Out>(args[1],args[2],args[4])
                    ;},
                "pentagon_f_4_5<Out>(args[1],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_5<Out>(args[2],args[3],args[0])
                    ;},
                "pentagon_f_4_5<Out>(args[2],args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_5<Out>(args[3],args[4],args[1])
                    ;},
                "pentagon_f_4_5<Out>(args[3],args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_5<Out>(args[4],args[0],args[2])
                    ;},
                "pentagon_f_4_5<Out>(args[4],args[0],args[2])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_6<Out>(args[0],args[2],args[4])
                    ;},
                "pentagon_f_4_6<Out>(args[0],args[2],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(6)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_6<Out>(args[1],args[3],args[0])
                    ;},
                "pentagon_f_4_6<Out>(args[1],args[3],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_6<Out>(args[2],args[4],args[1])
                    ;},
                "pentagon_f_4_6<Out>(args[2],args[4],args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_6<Out>(args[4],args[1],args[3])
                    ;},
                "pentagon_f_4_6<Out>(args[4],args[1],args[3])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_7<Out>(args[1],args[3],args[4])
                    ;},
                "pentagon_f_4_7<Out>(args[1],args[3],args[4])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_4_8<Out>(args[2],args[4],args[0])
                    ;},
                "pentagon_f_4_8<Out>(args[2],args[4],args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        zeta<Out>(3)
                    ;},
                "zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(3)/TI(2),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[0])*zeta<Out>(3)
                    ;},
                "pentagon_f_1_1<Out>(args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[1])*zeta<Out>(3)
                    ;},
                "pentagon_f_1_1<Out>(args[1])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-9)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[2])*zeta<Out>(3)
                    ;},
                "pentagon_f_1_1<Out>(args[2])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-5)/TI(2)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[3])*zeta<Out>(3)
                    ;},
                "pentagon_f_1_1<Out>(args[3])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(5)/TI(4)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        pentagon_f_1_1<Out>(args[4])*zeta<Out>(3)
                    ;},
                "pentagon_f_1_1<Out>(args[4])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-6)
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t MasslessPentaBubble_div_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int MasslessPentaBubble_div_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int MasslessPentaBubble_div_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool MasslessPentaBubble_div_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel