#include "../GenBoxBubble1Mb_std0_Eu.h"
#include "GenBoxBubble1Mb_std0_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace MasslessPentagon {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_GENBOXBUBBLE1MB_STD0_EU(COUT, CIN, FOUT, FIN)   \
    template class GenBoxBubble1Mb_std0_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_GENBOXBUBBLE1MB_STD0_EU(C, C, C, C)

} // namespace MasslessPentagon
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel