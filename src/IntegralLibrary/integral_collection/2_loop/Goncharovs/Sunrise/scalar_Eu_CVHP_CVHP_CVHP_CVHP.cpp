#ifdef VERY_HIGH_PRECISION
#include "../Sunrise_scalar_Eu.h"
#include "Sunrise_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_SUNRISE_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class Sunrise_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_SUNRISE_SCALAR_EU(CVHP, CVHP, CVHP, CVHP)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif