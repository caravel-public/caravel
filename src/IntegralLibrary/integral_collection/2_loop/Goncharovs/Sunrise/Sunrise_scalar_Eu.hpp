
namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// Sunrise_scalar_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
Sunrise_scalar_Eu<CType, FType>::Sunrise_scalar_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 4 || max_eps_power < -1){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(4)
        );
    }

    number_of_loops = 2;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = -1;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        T(1)
                    ;},
                "T(1)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        (TI(-1)/TI(4))*args[0],
                        (TI(-13)/TI(8))*args[0],
                        (TI(-115)/TI(16))*args[0],
                        (TI(-865)/TI(32))*args[0],
                        (TI(-5971)/TI(64))*args[0],
                        (TI(-39193)/TI(128))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)
                    ;},
                "prodpow(Pi<Out>,2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(1)/TI(24))*args[0],
                        (TI(13)/TI(48))*args[0],
                        (TI(115)/TI(96))*args[0],
                        (TI(865)/TI(192))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,4)
                    ;},
                "prodpow(Pi<Out>,4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(19)/TI(480))*args[0],
                        (TI(247)/TI(960))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        (TI(1)/TI(2))*args[0],
                        (TI(13)/TI(4))*args[0],
                        (TI(115)/TI(8))*args[0],
                        (TI(865)/TI(16))*args[0],
                        (TI(5971)/TI(32))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(12))*args[0],
                        (TI(-13)/TI(24))*args[0],
                        (TI(-115)/TI(48))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,4)*G<Out>(T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,4)*G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-19)/TI(240))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)*args[0],
                        (TI(-13)/TI(2))*args[0],
                        (TI(-115)/TI(4))*args[0],
                        (TI(-865)/TI(8))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(1)/TI(6))*args[0],
                        (TI(13)/TI(12))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(2)*args[0],
                        TI(13)*args[0],
                        (TI(115)/TI(2))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-1)/TI(3))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-4)*args[0],
                        TI(-26)*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(8)*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        zeta<Out>(3)
                    ;},
                "zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(8)/TI(3))*args[0],
                        (TI(52)/TI(3))*args[0],
                        (TI(230)/TI(3))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*zeta<Out>(3)
                    ;},
                "prodpow(Pi<Out>,2)*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-4)/TI(9))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*zeta<Out>(3)
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-16)/TI(3))*args[0],
                        (TI(-104)/TI(3))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[0])*zeta<Out>(3)
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(32)/TI(3))*args[0]
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        zeta<Out>(5)
                    ;},
                "zeta<Out>(5)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -1, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(68)/TI(5))*args[0]
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t Sunrise_scalar_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int Sunrise_scalar_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int Sunrise_scalar_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool Sunrise_scalar_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel