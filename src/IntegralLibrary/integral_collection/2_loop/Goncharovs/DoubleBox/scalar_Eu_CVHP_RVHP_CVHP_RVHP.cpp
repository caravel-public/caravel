#ifdef VERY_HIGH_PRECISION
#include "../DoubleBox_scalar_Eu.h"
#include "DoubleBox_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_DOUBLEBOX_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class DoubleBox_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_DOUBLEBOX_SCALAR_EU(CVHP, RVHP, CVHP, RVHP)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif