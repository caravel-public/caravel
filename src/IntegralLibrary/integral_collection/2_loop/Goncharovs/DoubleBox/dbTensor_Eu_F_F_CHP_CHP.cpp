#ifdef USE_FINITE_FIELDS
#ifdef HIGH_PRECISION
#include "../DoubleBox_dbTensor_Eu.h"
#include "DoubleBox_dbTensor_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_DOUBLEBOX_DBTENSOR_EU(COUT, CIN, FOUT, FIN)   \
    template class DoubleBox_dbTensor_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_DOUBLEBOX_DBTENSOR_EU(F32, F32, CHP, CHP)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif
#endif