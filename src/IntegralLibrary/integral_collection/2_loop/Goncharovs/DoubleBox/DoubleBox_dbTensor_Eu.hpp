
namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// DoubleBox_dbTensor_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
DoubleBox_dbTensor_Eu<CType, FType>::DoubleBox_dbTensor_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 0 || max_eps_power < -4){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(0)
        );
    }

    number_of_loops = 2;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = -4;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        T(1)
                    ;},
                "T(1)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        (TI(9)/TI(4))*(TI(1)/prodpow(args[0],2)),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)
                    ;},
                "prodpow(Pi<Out>,2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        (TI(-47)/TI(24))*(TI(1)/prodpow(args[0],2)),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,4)
                    ;},
                "prodpow(Pi<Out>,4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-59)/TI(480))*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        (TI(-5)/TI(2))*(TI(1)/prodpow(args[0],2)),
                        TI(0),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(43)/TI(12))*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(-2)*(TI(1)/prodpow(args[0],2)),
                        TI(0),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[1])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(1)/TI(3))*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(4)*(TI(1)/prodpow(args[0],2)),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-2)/TI(3))*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(args[1],T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(args[1],T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-4)*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(args[1],T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(args[1],T(-1)*args[0])*G<Out>(T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-4)/TI(3))*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(1)/prodpow(args[0],2),
                        TI(0),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-13)/TI(2))*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-8)*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-8)*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),args[1],T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),args[1],T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(8)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(T(0),args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(16)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(args[1],T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(args[1],T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(28)/TI(3))*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(args[1],T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(args[1],T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(8)*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],T(0),T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(args[1],T(0),T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(4)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(args[1],args[1],T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(args[1],args[1],T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-10)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(args[1],args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-20)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(6)*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(16)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(0),T(-1)*args[1])
                    ;},
                "G<Out>(args[1],T(-1)*args[0])*G<Out>(T(0),T(0),T(0),T(-1)*args[1])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(12)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(T(0),args[1],T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(T(0),args[1],T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-16)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(args[1],T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-8)*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(args[1],T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(args[1],T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-20)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*G<Out>(args[1],args[1],T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*G<Out>(args[1],args[1],T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(20)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-28)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),args[1],T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),args[1],T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(16)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(args[1],T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(36)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],args[1],T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(args[1],args[1],T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-20)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        zeta<Out>(3)
                    ;},
                "zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(-35)/TI(2))*(TI(1)/prodpow(args[0],2)),
                        TI(0)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*zeta<Out>(3)
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(77)/TI(3))*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[1])*zeta<Out>(3)
                    ;},
                "G<Out>(T(0),T(-1)*args[1])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        (TI(28)/TI(3))*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(args[1],T(-1)*args[0])*zeta<Out>(3)
                    ;},
                "G<Out>(args[1],T(-1)*args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -4, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-20)*(TI(1)/prodpow(args[0],2))
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t DoubleBox_dbTensor_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int DoubleBox_dbTensor_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int DoubleBox_dbTensor_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool DoubleBox_dbTensor_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel