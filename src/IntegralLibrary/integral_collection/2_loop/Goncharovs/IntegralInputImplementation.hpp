
template <typename T>
std::vector<T> Bowtie(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1])
    });
}

template <typename T>
std::vector<T> BubbleBox(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3)
    });
}

template <typename T>
std::vector<T> BubTri(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[3])
    });
}

template <typename T>
std::vector<T> DoubleBox(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3)
    });
}

template <typename T>
std::vector<T> DoubleTri(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        mc.s(1, 2),
mc.s(2, 3)
    });
}

template <typename T>
std::vector<T> Sunrise(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1])
    });
}
