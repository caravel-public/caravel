#ifdef USE_FINITE_FIELDS
#ifdef HIGH_PRECISION
#include "../Bowtie_scalar_Eu.h"
#include "Bowtie_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BOWTIE_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class Bowtie_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BOWTIE_SCALAR_EU(F32, F32, CHP, CHP)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif
#endif