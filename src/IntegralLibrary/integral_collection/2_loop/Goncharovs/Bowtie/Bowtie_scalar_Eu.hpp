
namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// Bowtie_scalar_Eu
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
Bowtie_scalar_Eu<CType, FType>::Bowtie_scalar_Eu(const std::vector<std::string>& invariant_names, int max_eps_power){

    

    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");

    // Check whether the requested powers in epsilon can be provided.
    if (max_eps_power > 4 || max_eps_power < -2){
        throw std::runtime_error(
              std::string("Requested integral up to eps^")
            + std::to_string(max_eps_power)
            + " but integral is only available up to eps^"
            + std::to_string(4)
        );
    }

    number_of_loops = 2;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = false;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = -2;
    last_eps = max_eps_power;

    // Add the functions to the basis
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        T(1)
                    ;},
                "T(1)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(1),
                        TI(4),
                        TI(12),
                        TI(32),
                        TI(80),
                        TI(192),
                        TI(448)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)
                    ;},
                "prodpow(Pi<Out>,2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(-1)/TI(6),
                        TI(-2)/TI(3),
                        TI(-2),
                        TI(-16)/TI(3),
                        TI(-40)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,4)
                    ;},
                "prodpow(Pi<Out>,4)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-7)/TI(120),
                        TI(-7)/TI(30),
                        TI(-7)/TI(10)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,6)
                    ;},
                "prodpow(Pi<Out>,6)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-31)/TI(3024)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(-2),
                        TI(-8),
                        TI(-24),
                        TI(-64),
                        TI(-160),
                        TI(-384)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(1)/TI(3),
                        TI(4)/TI(3),
                        TI(4),
                        TI(32)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,4)*G<Out>(T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,4)*G<Out>(T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(7)/TI(60),
                        TI(7)/TI(15)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(4),
                        TI(16),
                        TI(48),
                        TI(128),
                        TI(320)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-2)/TI(3),
                        TI(-8)/TI(3),
                        TI(-8)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,4)*G<Out>(T(0),T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,4)*G<Out>(T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-7)/TI(30)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-8),
                        TI(-32),
                        TI(-96),
                        TI(-256)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(4)/TI(3),
                        TI(16)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(16),
                        TI(64),
                        TI(192)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-8)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-32),
                        TI(-128)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(0),T(0),T(0),T(-1)*args[0])
                    ;},
                "G<Out>(T(0),T(0),T(0),T(0),T(0),T(0),T(-1)*args[0])",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(64)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        zeta<Out>(3)
                    ;},
                "zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-14)/TI(3),
                        TI(-56)/TI(3),
                        TI(-56),
                        TI(-448)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*zeta<Out>(3)
                    ;},
                "prodpow(Pi<Out>,2)*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(7)/TI(9),
                        TI(28)/TI(9)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*zeta<Out>(3)
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(28)/TI(3),
                        TI(112)/TI(3),
                        TI(112)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])*zeta<Out>(3)
                    ;},
                "prodpow(Pi<Out>,2)*G<Out>(T(0),T(-1)*args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-14)/TI(9)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(-1)*args[0])*zeta<Out>(3)
                    ;},
                "G<Out>(T(0),T(0),T(-1)*args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-56)/TI(3),
                        TI(-224)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(0),T(0),T(-1)*args[0])*zeta<Out>(3)
                    ;},
                "G<Out>(T(0),T(0),T(0),T(-1)*args[0])*zeta<Out>(3)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(112)/TI(3)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        prodpow(zeta<Out>(3),2)
                    ;},
                "prodpow(zeta<Out>(3),2)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(98)/TI(9)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        zeta<Out>(5)
                    ;},
                "zeta<Out>(5)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(-62)/TI(5),
                        TI(-248)/TI(5)
                        
                        
                );}
            )
        );
    
        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [invariant_names](const std::vector<T>& args){ return
                        G<Out>(T(0),T(-1)*args[0])*zeta<Out>(5)
                    ;},
                "G<Out>(T(0),T(-1)*args[0])*zeta<Out>(5)",
                invariant_names
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [max_eps_power](const std::vector<TI>& args){
                    return OutC::drop_excess( -2, max_eps_power,
                        
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(0),
                        TI(124)/TI(5)
                        
                        
                );}
            )
        );
    



}

template <typename CType, typename FType>
std::size_t Bowtie_scalar_Eu<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int Bowtie_scalar_Eu<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int Bowtie_scalar_Eu<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool Bowtie_scalar_Eu<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel