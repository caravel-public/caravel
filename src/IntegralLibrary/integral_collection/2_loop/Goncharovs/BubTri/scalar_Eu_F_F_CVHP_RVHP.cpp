#ifdef USE_FINITE_FIELDS
#ifdef VERY_HIGH_PRECISION
#include "../BubTri_scalar_Eu.h"
#include "BubTri_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBTRI_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class BubTri_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBTRI_SCALAR_EU(F32, F32, CVHP, RVHP)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif
#endif