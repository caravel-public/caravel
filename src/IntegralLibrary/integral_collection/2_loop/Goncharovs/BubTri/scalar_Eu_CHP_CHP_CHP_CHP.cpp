#ifdef HIGH_PRECISION
#include "../BubTri_scalar_Eu.h"
#include "BubTri_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBTRI_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class BubTri_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBTRI_SCALAR_EU(CHP, CHP, CHP, CHP)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif