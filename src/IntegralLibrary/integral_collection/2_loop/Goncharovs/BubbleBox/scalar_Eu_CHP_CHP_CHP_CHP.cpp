#ifdef HIGH_PRECISION
#include "../BubbleBox_scalar_Eu.h"
#include "BubbleBox_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBBLEBOX_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class BubbleBox_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBBLEBOX_SCALAR_EU(CHP, CHP, CHP, CHP)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif