#ifdef USE_FINITE_FIELDS
#include "../BubbleBox_scalar_Eu.h"
#include "BubbleBox_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_BUBBLEBOX_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class BubbleBox_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_BUBBLEBOX_SCALAR_EU(F32, F32, C, R)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif