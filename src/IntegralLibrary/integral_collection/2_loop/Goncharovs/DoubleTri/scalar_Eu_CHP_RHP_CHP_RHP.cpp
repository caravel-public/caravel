#ifdef HIGH_PRECISION
#include "../DoubleTri_scalar_Eu.h"
#include "DoubleTri_scalar_Eu.hpp"

namespace Caravel {
namespace Integrals {
namespace loop_2 {
namespace Goncharovs {

// ----------------------------------------------------------------------------
// ---------------------- Explicit Instantiations -----------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_DOUBLETRI_SCALAR_EU(COUT, CIN, FOUT, FIN)   \
    template class DoubleTri_scalar_Eu<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

INSTANTIATE_DOUBLETRI_SCALAR_EU(CHP, RHP, CHP, RHP)

} // namespace Goncharovs
} // namespace loop_2
} // namespace Integrals
} // namespace Caravel
#endif