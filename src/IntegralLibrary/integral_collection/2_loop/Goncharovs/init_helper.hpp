{
    gType input{ "CaravelGraph[Nodes[Node[]], Connection[Strand[LoopMomentum[1], Leg[1], Link[]], Strand[LoopMomentum[2], Leg[2], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Bowtie<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Bowtie<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Bowtie<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Bowtie<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Bowtie<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Bowtie<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Bowtie<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Bowtie_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[3, 0]], Node[Leg[4, 0]]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BubbleBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BubbleBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BubbleBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BubbleBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BubbleBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BubbleBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BubbleBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return BubbleBox_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1, 0]], Node[Leg[2, 0]]], Connection[Strand[LoopMomentum[1], Leg[3], Link[]], Strand[Link[]], Strand[LoopMomentum[2]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::BubTri<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::BubTri<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::BubTri<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::BubTri<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::BubTri<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::BubTri<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::BubTri<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return BubTri_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::DoubleBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::DoubleBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::DoubleBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::DoubleBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::DoubleBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::DoubleBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::DoubleBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[], Node[]], Connection[Strand[LoopMomentum[1], Leg[2, 0], Link[], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[], Leg[4, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::DoubleBox<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::DoubleBox<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::DoubleBox<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::DoubleBox<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::DoubleBox<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::DoubleBox<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::DoubleBox<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleBox_dbTensor_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("dbTensor"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[2, 0]], Node[Leg[4, 0]]], Connection[Strand[LoopMomentum[1], Leg[1, 0], Link[]], Strand[Link[]], Strand[LoopMomentum[2], Leg[3, 0], Link[]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::DoubleTri<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::DoubleTri<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::DoubleTri<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::DoubleTri<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::DoubleTri<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::DoubleTri<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::DoubleTri<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative,InvariantSigns::negative } ) ) {
                return DoubleTri_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mandelstam,{1, 2}),InputVariable(InvariantType::Mandelstam,{2, 3})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}{
    gType input{ "CaravelGraph[Nodes[Node[Leg[1]], Node[Leg[2]]], Connection[Strand[LoopMomentum[1]], Strand[Link[]], Strand[LoopMomentum[2]]]]" };
    MasterIntegralData data;
    std::get<lGraph::xGraph>(data) = input;
    
    // C R C R
    std::get<MasterIntegralPointer<C,R,C,R>>(data) = MasterIntegralPointer<C,R,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<C,R>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<R>>(data) = MasterIntegralInputGenerator<R>(InputEvaluator::Sunrise<R>);
    
    // C C C C
    std::get<MasterIntegralPointer<C,C,C,C>>(data) = MasterIntegralPointer<C,C,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<C,C>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<C>>(data) = MasterIntegralInputGenerator<C>(InputEvaluator::Sunrise<C>);

#ifdef USE_FINITE_FIELDS
    // F F C R
    std::get<MasterIntegralPointer<F32,F32,C,R>>(data) = MasterIntegralPointer<F32,F32,C,R>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<F32,F32>, Func<C,R>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<F32>>(data) = MasterIntegralInputGenerator<F32>(InputEvaluator::Sunrise<F32>);

    // F F C C
    std::get<MasterIntegralPointer<F32,F32,C,C>>(data) = MasterIntegralPointer<F32,F32,C,C>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<F32,F32>, Func<C,C>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

#endif

#ifdef HIGH_PRECISION

    // CHP RHP CHP RHP
    std::get<MasterIntegralPointer<CHP,RHP,CHP,RHP>>(data) = MasterIntegralPointer<CHP,RHP,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<CHP,RHP>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RHP>>(data) = MasterIntegralInputGenerator<RHP>(InputEvaluator::Sunrise<RHP>);

    // CHP CHP CHP CHP
    std::get<MasterIntegralPointer<CHP,CHP,CHP,CHP>>(data) = MasterIntegralPointer<CHP,CHP,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<CHP,CHP>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<CHP>>(data) = MasterIntegralInputGenerator<CHP>(InputEvaluator::Sunrise<CHP>);

#ifdef USE_FINITE_FIELDS
    // F F CHP RHP
    std::get<MasterIntegralPointer<F32,F32,CHP,RHP>>(data) = MasterIntegralPointer<F32,F32,CHP,RHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<F32,F32>, Func<CHP,RHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CHP CHP
    std::get<MasterIntegralPointer<F32,F32,CHP,CHP>>(data) = MasterIntegralPointer<F32,F32,CHP,CHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<F32,F32>, Func<CHP,CHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

#ifdef VERY_HIGH_PRECISION

    // CVHP RVHP CVHP RVHP
    std::get<MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>>(data) = MasterIntegralPointer<CVHP,RVHP,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<CVHP,RVHP>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );

    std::get<MasterIntegralInputGenerator<RVHP>>(data) = MasterIntegralInputGenerator<RVHP>(InputEvaluator::Sunrise<RVHP>);

    // CVHP CVHP CVHP CVHP
    std::get<MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>>(data) = MasterIntegralPointer<CVHP,CVHP,CVHP,CVHP>(
       [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<CVHP,CVHP>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})}) 
    );

    std::get<MasterIntegralInputGenerator<CVHP>>(data) = MasterIntegralInputGenerator<CVHP>(InputEvaluator::Sunrise<CVHP>);

#ifdef USE_FINITE_FIELDS
    // F F CVHP RVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,RVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,RVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<F32,F32>, Func<CVHP,RVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );
    
    // F F CVHP CVHP
    std::get<MasterIntegralPointer<F32,F32,CVHP,CVHP>>(data) = MasterIntegralPointer<F32,F32,CVHP,CVHP>(
        [](const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) {
            
            if ( compare( signs, std::vector<InvariantSigns>{ InvariantSigns::negative } ) ) {
                return Sunrise_scalar_Eu<Coeff<F32,F32>, Func<CVHP,CVHP>>(names).get_basis();
            }
            
            throw std::runtime_error("Invalid region");
        },
        std::vector<InputVariable>({InputVariable(InvariantType::Mass2,{1})})
    );


#endif 
#endif

master_integral_data_holder.insert({lGraph::IntegralGraph(std::make_pair(input, std::string("scalar"))), data});

}
