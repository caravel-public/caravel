
template <typename T>
std::vector<T> III(const momD_conf<T, 4>& mc) {
    return std::vector<T>({
        sq(mc[1]),
sq(mc[4]),
mc.s(1, 2),
mc.s(1, 4)
    });
}
