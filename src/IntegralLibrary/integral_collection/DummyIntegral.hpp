
namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// SunRiseGsS
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
DummyIntegral<CType, FType>::DummyIntegral(int min_eps_power, int max_eps_power, std::string name){



    // Check whether the output of the provided coefficient is a Series
    static_assert(is_series<typename CType::output_type>::value,
                  "Return type of Coefficient must be a Series<?> for this integral!");


    number_of_loops = 2;

    // Track whether the integral has been successfully tested numerically
    successful_numerical_test = true;

    // Save the minimal and maximal powers in epsilon appearing in the integral.
    leading_eps = min_eps_power;
    last_eps = max_eps_power;

    // Add the functions to the basis

        abstract_basis.add_basis_element(

            // The basis function
            StdBasisFunction<Out, T>(
                [](std::vector<T> args){ return
                        T(1)
                    ;},
                name,
                {}
            ),

            // The coefficient
            FBCoefficient<OutC, TI>(
                [min_eps_power, max_eps_power](std::vector<TI> args){
                  return OutC{ min_eps_power, max_eps_power};
                }
            )
        );

}

template <typename CType, typename FType>
std::size_t DummyIntegral<CType, FType>::get_number_of_loops() const {
    return number_of_loops;
}

template <typename CType, typename FType>
int DummyIntegral<CType, FType>::get_leading_eps() const {
    return leading_eps;
}

template <typename CType, typename FType>
int DummyIntegral<CType, FType>::get_last_eps() const {
    return last_eps;
}

template <typename CType, typename FType>
bool DummyIntegral<CType, FType>::successfully_tested_numerically() const {
    return successful_numerical_test;
}

// ----------------------------------------------------------------------------
// ------------------------- Explicit Instantiation ---------------------------
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_DUMMYINTEGRAL(COUT, CIN, FOUT, FIN)   \
    extern template class DummyIntegral<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;

EXTERN_INSTANTIATE_DUMMYINTEGRAL(C, R, C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_DUMMYINTEGRAL(CHP, RHP, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_DUMMYINTEGRAL(CVHP, RVHP, CVHP, RVHP)
#endif
EXTERN_INSTANTIATE_DUMMYINTEGRAL(C, C, C, C)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_DUMMYINTEGRAL(CHP, CHP, CHP, CHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_DUMMYINTEGRAL(CVHP, CVHP, CVHP, CVHP)
#endif
#ifdef USE_FINITE_FIELDS
EXTERN_INSTANTIATE_DUMMYINTEGRAL(F32, F32, C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_DUMMYINTEGRAL(F32, F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_DUMMYINTEGRAL(F32, F32, CVHP, RVHP)
#endif
EXTERN_INSTANTIATE_DUMMYINTEGRAL(F32,F32, C, C)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_DUMMYINTEGRAL(F32, F32, CHP, CHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_DUMMYINTEGRAL(F32, F32, CVHP, CVHP)
#endif
#endif

} // namespace Integrals
} // namespace Caravel
